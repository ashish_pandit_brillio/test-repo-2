var snd_mail_trigger_jd = 0;

function Practice_head_ticker(request, response) {
    try {
        //nlapiLogExecution('debug', 'practice_test',JSON.stringify(getPracticeMailingList()));

        //return;
        var context = nlapiGetContext();

        var billing_from_date = nlapiStringToDate(nlapiDateToString(new Date()));
        var total_billable_head = 0;
        var total_non_billable_head = 0;
        var sr_no = 0;
        var sr_prac = 0;
        var practice_arr_list = new Array();
        var practice_unique_list = new Array();
        var practice_srch_list = new Array();
        var practice_emp_head_count_detail = new Array();
        var proj_unique_list = new Array();
        var proj_data = new Array();
        var practice_array = new Array();
        var practice_detail = new Array();
        var sr_practice_id = 0;

        /*var filters_search_practice = new Array();
        filters_search_practice[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');*/
        var filters_search_practice = [
            ['isinactive', 'is', 'F'], 'and',
            ['internalid', 'noneof', '492', '498'], 'and',
            ['formulanumeric: CASE WHEN {internalid} = {custrecord_parent_practice.internalid} THEN 1 ELSE 0 END', 'equalto', '1'], 'and',
            ['custrecord_practice_type_ticker', 'anyof', parseInt(2)]
        ];

        //var filters_search_practice = [['isinactive', 'is', 'F'], 'and',
        //						['internalid','anyof','602']]
        var columns_practice = new Array();
        columns_practice[0] = new nlobjSearchColumn('internalid');
        columns_practice[1] = new nlobjSearchColumn('custrecord_practicehead');
        columns_practice[2] = new nlobjSearchColumn('email', 'custrecord_practicehead');
        columns_practice[3] = new nlobjSearchColumn('custrecord_parent_practice');
        columns_practice[4] = new nlobjSearchColumn('name');
        columns_practice[5] = new nlobjSearchColumn('email', 'custrecord_onsite_practice_head');

        var parctice_srch = nlapiSearchRecord('department', null, filters_search_practice, columns_practice);           
        if (_logValidation(parctice_srch)) {
            for (var i_indx = 0; i_indx < parctice_srch.length; i_indx++) {
				try{
				
                yieldScript(context);

                var practice_internal_id = parctice_srch[i_indx].getId();
				
                var practice_head = parctice_srch[i_indx].getValue('custrecord_practicehead');
                var practice_head_mail = parctice_srch[i_indx].getValue('email', 'custrecord_practicehead');
                var practice_parent_id = parctice_srch[i_indx].getValue('custrecord_parent_practice');
                var practice_parent_name = parctice_srch[i_indx].getValue('name');
                var onsite_prac_head_mail = parctice_srch[i_indx].getValue('email', 'custrecord_onsite_practice_head');
                //if (parseInt(practice_internal_id) == parseInt(practice_parent_id))
                //{
                var sr_prac = 0;
                var sr_no = 0;
                var sr_practice_id = 0;
                snd_mail_trigger_jd++;
                var practice_arr_list = new Array();
                var practice_srch_list = new Array();
                var practice_detail = new Array();
                var practice_array = new Array();
                var proj_unique_list = new Array();
                var proj_data = new Array();
                var employee_allocated_list = new Array();

                //nlapiLogExecution('audit','practice_parent_id:- '+practice_internal_id,practice_parent_id);
                practice_arr_list[sr_prac] = {
                    'internal_id': practice_internal_id,
                    'mail_id': practice_head_mail,
                    'parent_practice_name': practice_parent_name,
                    'onsite_prac_head_mail': onsite_prac_head_mail
                };
                sr_prac++;

                practice_srch_list.push(practice_internal_id);

                var filters_allocation = [
                    ['startdate', 'notafter', billing_from_date], 'and',
                    ['enddate', 'notbefore', billing_from_date], 'and',
                    ['employee.custentity_employee_inactive', 'is', 'F'], 'and',
                    [
                        ['custevent_practice', 'anyof', practice_internal_id], 'or',
                        ['custevent_practice.custrecord_parent_practice', 'anyof', practice_internal_id]
                    ]
                ];

                //filters_allocation[2] = new nlobjSearchFilter('project', null, 'anyof', 33955);
                var columns = new Array();
                columns[0] = new nlobjSearchColumn('internalid').setSort(true);
                columns[1] = new nlobjSearchColumn('custeventrbillable');
                columns[2] = new nlobjSearchColumn('jobbillingtype', 'job');
                columns[3] = new nlobjSearchColumn('custentity_clientpartner', 'job');
                columns[4] = new nlobjSearchColumn('custentity_projectmanager', 'job');
                columns[5] = new nlobjSearchColumn('custentity_deliverymanager', 'job');
                columns[6] = new nlobjSearchColumn('custentity_verticalhead', 'job');
                columns[7] = new nlobjSearchColumn('custevent_practice');
                columns[8] = new nlobjSearchColumn('customer', 'job');
                columns[9] = new nlobjSearchColumn('company');
                columns[10] = new nlobjSearchColumn('custentity_project_allocation_category', 'job');
                columns[11] = new nlobjSearchColumn('subsidiary', 'employee');
                columns[12] = new nlobjSearchColumn('custentity_clientpartner', 'customer');
                columns[13] = new nlobjSearchColumn('custentity_region', 'customer');
                columns[14] = new nlobjSearchColumn('custentity_region', 'job');
                columns[15] = new nlobjSearchColumn('percentoftime');
                columns[16] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
                columns[17] = new nlobjSearchColumn('custentity_practice', 'job');
                columns[18] = new nlobjSearchColumn('department', 'employee');
                columns[19] = new nlobjSearchColumn('resource');
                columns[20] = new nlobjSearchColumn("formulatext").setFormula("TO_CHAR({startdate},'MM/DD/YYYY HH:MI:SS AM')");
                columns[21] = new nlobjSearchColumn("formulatext").setFormula("TO_CHAR({enddate},'MM/DD/YYYY HH:MI:SS AM')");
                columns[22] = new nlobjSearchColumn('employeestatus', 'employee');
                columns[23] = new nlobjSearchColumn('title', 'employee');
                columns[24] = new nlobjSearchColumn('custeventbstartdate');
                columns[25] = new nlobjSearchColumn('custeventbenddate');
                columns[26] = new nlobjSearchColumn('custentity_reportingmanager', 'employee');
                columns[27] = new nlobjSearchColumn('employeetype', 'employee');
                columns[28] = new nlobjSearchColumn('hiredate', 'employee');
                columns[29] = new nlobjSearchColumn('approver', 'employee');
                columns[30] = new nlobjSearchColumn('timeapprover', 'employee');
                columns[31] = new nlobjSearchColumn('custevent3');
                columns[32] = new nlobjSearchColumn('custevent_otrate');
                columns[33] = new nlobjSearchColumn('custevent_monthly_rate');
                columns[34] = new nlobjSearchColumn('startdate', 'job');
                columns[35] = new nlobjSearchColumn('enddate', 'job');
                columns[36] = new nlobjSearchColumn('custentity_employee_primary_skill', 'employee');
                columns[37] = new nlobjSearchColumn('custentity_employee_secondary_skill', 'employee');
                columns[38] = new nlobjSearchColumn('gender', 'employee');
                columns[39] = new nlobjSearchColumn('location', 'employee');
                columns[40] = new nlobjSearchColumn('custentity_legal_entity_fusion', 'employee');


                var project_allocation_result = searchRecord('resourceallocation', null, filters_allocation, columns); // changed the search function to get more result then 1000 prabhat gupta 27/10/2020 NIS-1795

				var excel_file_obj=""; //Variable should store the resource allocation details for each practice
                if (_logValidation(project_allocation_result)) {

                    /*
                    if (project_allocation_result.length >= 1000) {
                        var completeResultSet = project_allocation_result; //container of the complete result set
                        while (project_allocation_result.length == 1000) {
                            //re-run the search if limit has been reached
                            var lastId = project_allocation_result[999].getValue('internalid'); //note the last record retrieved
                            filters_allocation[0] = new nlobjSearchFilter('enddate', null, 'notbefore', billing_from_date);
                            filters_allocation[1] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastId); //create new filter to restrict the next search based on the last record returned
                            project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_allocation, columns);
                            completeResultSet = completeResultSet.concat(project_allocation_result); //add the result to the complete result set 
                        } //while(search_item.length == 1000)
                        project_allocation_result = completeResultSet;
                    } //  if (search_item.length >= 1000)
*/
                    //nlapiLogExecution('audit','len:- ',project_allocation_result.length);
					
					
                    for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {


                        var is_resource_billable = project_allocation_result[i_search_indx].getValue('custeventrbillable');
                        var billing_type = project_allocation_result[i_search_indx].getText('jobbillingtype', 'job');
                        var client_partner = project_allocation_result[i_search_indx].getText('custentity_clientpartner', 'job');
                        var proj_manager = project_allocation_result[i_search_indx].getText('custentity_projectmanager', 'job');
                        var delivery_manager = project_allocation_result[i_search_indx].getText('custentity_deliverymanager', 'job');
                        var vertical_head = project_allocation_result[i_search_indx].getText('custentity_verticalhead', 'job');
                        var parctice_id = project_allocation_result[i_search_indx].getValue('custevent_practice');
                        var parctice_name = project_allocation_result[i_search_indx].getText('custevent_practice');
                        var parctice_id_parent = project_allocation_result[i_search_indx].getValue('custrecord_parent_practice', 'custevent_practice');
                        var parctice_parent_name = project_allocation_result[i_search_indx].getText('custrecord_parent_practice', 'custevent_practice');
                        var project_cust = project_allocation_result[i_search_indx].getValue('customer', 'job');
                        var proj_cust_name = project_allocation_result[i_search_indx].getText('customer', 'job');
                        var project_id = project_allocation_result[i_search_indx].getValue('company');
                        var project_name = project_allocation_result[i_search_indx].getText('company');
                        var is_proj_bench_category = project_allocation_result[i_search_indx].getValue('custentity_project_allocation_category', 'job');
                        var emp_susidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
                        var emp_level_practice = project_allocation_result[i_search_indx].getValue('department', 'employee');
                        var customer_CP_id = project_allocation_result[i_search_indx].getValue('custentity_clientpartner', 'customer');
                        var customer_CP_name = project_allocation_result[i_search_indx].getText('custentity_clientpartner', 'customer');
                        var customer_region = project_allocation_result[i_search_indx].getValue('custentity_region', 'customer');
                        var project_region = project_allocation_result[i_search_indx].getValue('custentity_region', 'job');
                        var execusting_practice = project_allocation_result[i_search_indx].getText('custentity_practice', 'job');
                        var resource_id = project_allocation_result[i_search_indx].getValue('resource');
                        var prcnt_allocated = project_allocation_result[i_search_indx].getValue('percentoftime');
                        prcnt_allocated = prcnt_allocated.toString();
                        prcnt_allocated = prcnt_allocated.split('.');
                        prcnt_allocated = prcnt_allocated[0].toString().split('%');
                        var final_prcnt_allocation = parseFloat(prcnt_allocated) / parseFloat(100);
                        final_prcnt_allocation = parseFloat(final_prcnt_allocation);

                        employee_allocated_list.push(resource_id);
						
						if(i_search_indx==0)//Added condition to to avoid iterations for same practice with allocations
						{ 
                        var strVar_excel = '';

                        strVar_excel += '<table>';

                        strVar_excel += '	<tr>';
                        strVar_excel += ' <td width="100%">';
                        strVar_excel += '<table width="100%" border="1">';

                        strVar_excel += '	<tr>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Region</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Customer Name</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Project Name</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Type</td>';
                        strVar_excel += ' <td width="10%" font-size="11" align="left">Employee Name</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Employeee Subsidiary</td>';
                        //strVar_excel += ' <td width="6%" font-size="11" align="center">Primary Skill</td>';
                        //strVar_excel += ' <td width="6%" font-size="11" align="center">Secondary Skill</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Gender</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Percent Allocated</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Billable Status</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation Start Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation End Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Project Start Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Project End Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Level</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Designation</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Onsite/Offshore</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Location</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Start Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Billing End Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Reporting Manager</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Expense Approver</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Time Approver</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Type</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Hire Date</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Bill Rate</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">OT Rate</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Monthly Rate</td>';

                        strVar_excel += ' <td width="6%" font-size="11" align="center">Client Partner</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Project Manager</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Delivery Manager</td>';
                        strVar_excel += ' <td width="6%" font-size="11" align="center">Project Category</td>';
                        strVar_excel += '	</tr>';

                        for (var index_no_dump = 0; index_no_dump < project_allocation_result.length; index_no_dump++) {

                            var emp_id = project_allocation_result[index_no_dump].getValue('resource');
                            var region_dump = project_allocation_result[index_no_dump].getText('custentity_region', 'customer');
                            var customer_dump = project_allocation_result[index_no_dump].getText('customer', 'job');
                            var project_dump = project_allocation_result[index_no_dump].getText('company');
                            var excuting_pract_proj_dump = project_allocation_result[index_no_dump].getText('custentity_practice', 'job');
                            var proj_billing_type_dump = project_allocation_result[index_no_dump].getText('jobbillingtype', 'job');
                            var employee_dump = project_allocation_result[index_no_dump].getText('resource');
                            var emp_practice_dump = project_allocation_result[index_no_dump].getText('custevent_practice');
                            var emp_subsidiary_dump = project_allocation_result[index_no_dump].getText('subsidiary', 'employee');
                            var emp_subsidiary_id_dump = project_allocation_result[index_no_dump].getValue('subsidiary', 'employee');
                            //nlapiLogExecution('audit','custentity_legal_entity_fusion',project_allocation_result[index_no_dump].getValue('custentity_legal_entity_fusion', 'employee'));
                            var onsite_offsite_dump = '';
                          if(emp_subsidiary_id_dump == 3 && project_allocation_result[index_no_dump].getValue('custentity_legal_entity_fusion', 'employee') == 'Brillio Technologies Private Limited UK')
											{
												onsite_offsite_dump = 'Onsite';
											}else if(emp_subsidiary_id_dump == 3 || emp_subsidiary_id_dump == 9 || emp_subsidiary_id_dump == 12)//emp_subsidiary_id_dump == 23(in sb)
											{
												onsite_offsite_dump = 'Offshore';
											}
											else
											{
												onsite_offsite_dump = 'Onsite';
											}
                            
                          
                           // if (emp_subsidiary_id_dump == 3 || emp_subsidiary_id_dump == 9) {
                           //     onsite_offsite_dump = 'Offshore';
                           // } else {
                           //     onsite_offsite_dump = 'Onsite';
                           // }
                            var emp_location = project_allocation_result[index_no_dump].getText('location', 'employee');
                            var prcnt_of_tym_dump = project_allocation_result[index_no_dump].getValue('percentoftime');
                            prcnt_of_tym_dump = prcnt_of_tym_dump.toString();
                            prcnt_of_tym_dump = prcnt_of_tym_dump.split('.');
                            prcnt_of_tym_dump = prcnt_of_tym_dump[0].toString().split('%');
                            var final_prcnt_allocation_dump = parseFloat(prcnt_of_tym_dump) / parseFloat(100);
                            final_prcnt_allocation_dump = parseFloat(final_prcnt_allocation_dump);
                            var is_resource_billable_dump = project_allocation_result[index_no_dump].getValue('custeventrbillable');
                            var allo_strt_date_dump = project_allocation_result[index_no_dump].getValue(columns[20]);
                            var allo_end_date_dump = project_allocation_result[index_no_dump].getValue(columns[21]);
							allo_strt_date_dump = moment(allo_strt_date_dump).utcOffset("+05:30").format("MM/DD/YYYY");
							allo_end_date_dump = moment(allo_end_date_dump).utcOffset("+05:30").format("MM/DD/YYYY");
							allo_strt_date_dump = dateString(allo_strt_date_dump);
							allo_end_date_dump = dateString(allo_end_date_dump)
                            var d_proj_strt_date = project_allocation_result[index_no_dump].getValue('startdate', 'job');
							d_proj_strt_date = dateString(d_proj_strt_date);
                            var d_proj_end_date = project_allocation_result[index_no_dump].getValue('enddate', 'job');
                            d_proj_end_date = dateString(d_proj_end_date);
							var client_partner_dump = project_allocation_result[index_no_dump].getText('custentity_clientpartner', 'customer');
                            var prj_manager_dump = project_allocation_result[index_no_dump].getText('custentity_projectmanager', 'job');
                            var delivery_manager_dump = project_allocation_result[index_no_dump].getText('custentity_deliverymanager', 'job');
                            var proj_category_dump = project_allocation_result[index_no_dump].getText('custentity_project_allocation_category', 'job');
                            var emp_level_dump = project_allocation_result[index_no_dump].getText('employeestatus', 'employee');
                            var emp_designation_dump = project_allocation_result[index_no_dump].getValue('title', 'employee');
                            var billing_strt_date_dump = project_allocation_result[index_no_dump].getValue('custeventbstartdate');
                            billing_strt_date_dump = dateString(billing_strt_date_dump);
							var billing_end_date_dump = project_allocation_result[index_no_dump].getValue('custeventbenddate');
                            billing_end_date_dump = dateString(billing_end_date_dump);
							var reporting_manager_dump = project_allocation_result[index_no_dump].getText('custentity_reportingmanager', 'employee');
                            var emp_type_dump = project_allocation_result[index_no_dump].getText('employeetype', 'employee');
                            var emp_hire_date_dump = project_allocation_result[index_no_dump].getValue('hiredate', 'employee');
                            emp_hire_date_dump = dateString(emp_hire_date_dump);
							var expense_approver_dump = project_allocation_result[index_no_dump].getText('approver', 'employee');
                            var time_approver_dump = project_allocation_result[index_no_dump].getText('timeapprover', 'employee');
                            var bill_rate_dump = project_allocation_result[index_no_dump].getValue('custevent3');
                            if (!bill_rate_dump)
                                bill_rate_dump = 0;

                            var ot_rate_dump = project_allocation_result[index_no_dump].getValue('custevent_otrate');
                            if (!ot_rate_dump)
                                ot_rate_dump = 0;

                            var monthly_rate_dump = project_allocation_result[index_no_dump].getValue('custevent_monthly_rate');
                            if (!monthly_rate_dump)
                                monthly_rate_dump = 0;

                            if (is_resource_billable_dump == 'T') {
                                is_resource_billable_dump = 'Yes';
                            } else
                                is_resource_billable_dump = 'No';

                            //var s_emp_primary_skill = project_allocation_result[index_no_dump].getValue('custentity_employee_primary_skill','employee');
                            //var s_emp_secondary_skill = project_allocation_result[index_no_dump].getValue('custentity_employee_secondary_skill','employee');
                            var s_emp_gender = project_allocation_result[index_no_dump].getText('gender', 'employee');
                            strVar_excel += '	<tr>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + region_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + customer_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + project_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + excuting_pract_proj_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + proj_billing_type_dump + '</td>';
                            strVar_excel += ' <td width="10%" font-size="11" align="left">' + employee_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_practice_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_subsidiary_dump + '</td>';
                            //strVar_excel += ' <td width="6%" font-size="11" align="center">' +a_primarySkill+ '</td>';
                            //strVar_excel += ' <td width="6%" font-size="11" align="center">' +sec_skills+ '</td>';	
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + s_emp_gender + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + final_prcnt_allocation_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + is_resource_billable_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + allo_strt_date_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + allo_end_date_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + d_proj_strt_date + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + d_proj_end_date + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_level_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_designation_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + onsite_offsite_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_location + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + billing_strt_date_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + billing_end_date_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + reporting_manager_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + expense_approver_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + time_approver_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_type_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + emp_hire_date_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + bill_rate_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + ot_rate_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + monthly_rate_dump + '</td>';

                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + client_partner_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + prj_manager_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + delivery_manager_dump + '</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">' + proj_category_dump + '</td>';
                            strVar_excel += '	</tr>';
                        }

                        strVar_excel += '</table>';
                        strVar_excel += ' </td>';
                        strVar_excel += '	</tr>';

                        strVar_excel += '</table>';

                        excel_file_obj = generate_excel(strVar_excel);
						
						} //Genarate file each practice at one time
                        if (is_resource_billable == 'T') {
                            if (practice_array.indexOf(parctice_id) >= 0) {
                                var practice_already_exist_index = practice_array.indexOf(parctice_id);

                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    trainee_cnt = parseFloat(trainee_cnt) + parseFloat(final_prcnt_allocation);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    mngmnt_cnt = parseFloat(mngmnt_cnt) + parseFloat(final_prcnt_allocation);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    bench_cnt = parseFloat(bench_cnt) + parseFloat(final_prcnt_allocation);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    invstmnt_cnt = parseFloat(invstmnt_cnt) + parseFloat(final_prcnt_allocation);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else {
                                    if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                        var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                        var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                        var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                        var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                        var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                        bill_alo_cnt_offsite = parseFloat(bill_alo_cnt_offsite) + parseFloat(final_prcnt_allocation);
                                        var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                        var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                        practice_detail[practice_already_exist_index] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': bench_cnt,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': bill_alo_cnt_onsite,
                                            'billable_allocation_offsite': bill_alo_cnt_offsite,
                                            'unbillabel_allo_onsite': unbill_alo_onsite,
                                            'unbillable_allo_offsite': unbill_alo_offsite
                                        };
                                    } else {
                                        var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                        var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                        var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                        var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                        var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                        bill_alo_cnt_onsite = parseFloat(bill_alo_cnt_onsite) + parseFloat(final_prcnt_allocation);
                                        var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                        var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                        var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                        practice_detail[practice_already_exist_index] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': bench_cnt,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': bill_alo_cnt_onsite,
                                            'billable_allocation_offsite': bill_alo_cnt_offsite,
                                            'unbillabel_allo_onsite': unbill_alo_onsite,
                                            'unbillable_allo_offsite': unbill_alo_offsite
                                        };
                                    }
                                }

                            } else {
                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': 0,
                                        'trainee_count': final_prcnt_allocation,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': final_prcnt_allocation,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': final_prcnt_allocation,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': final_prcnt_allocation,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else {
                                    practice_array.push(parctice_id);
                                    //if (parseInt(emp_susidiary) == 3) {
                                     if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        //cust_unique_list.push(project_cust);
                                        //proj_unique_list.push(project_id);
                                        practice_detail[sr_practice_id] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': 0,
                                            'billable_allocation_offsite': final_prcnt_allocation,
                                            'unbillabel_allo_onsite': 0,
                                            'unbillable_allo_offsite': 0
                                        };
                                        sr_practice_id++;
                                    } else {
                                        //cust_unique_list.push(project_cust);
                                        //proj_unique_list.push(project_id);
                                        practice_detail[sr_practice_id] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': final_prcnt_allocation,
                                            'billable_allocation_offsite': 0,
                                            'unbillabel_allo_onsite': 0,
                                            'unbillable_allo_offsite': 0
                                        };
                                        sr_practice_id++;
                                    }
                                }
                            }
                        } else {
                            if (practice_array.indexOf(parctice_id) >= 0) {
                                var practice_already_exist_index = practice_array.indexOf(parctice_id);

                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    trainee_cnt = parseFloat(trainee_cnt) + parseFloat(final_prcnt_allocation);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    mngmnt_cnt = parseFloat(mngmnt_cnt) + parseFloat(final_prcnt_allocation);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    bench_cnt = parseFloat(bench_cnt) + parseFloat(final_prcnt_allocation);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                    var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                    var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                    var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                    invstmnt_cnt = parseFloat(invstmnt_cnt) + parseFloat(final_prcnt_allocation);
                                    var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                    var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                    var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                    var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                    practice_detail[practice_already_exist_index] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': bench_cnt,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': bill_alo_cnt_onsite,
                                        'billable_allocation_offsite': bill_alo_cnt_offsite,
                                        'unbillabel_allo_onsite': unbill_alo_onsite,
                                        'unbillable_allo_offsite': unbill_alo_offsite
                                    };
                                } else {
                                    //if (parseInt(emp_susidiary) == 3) {
                                     if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                        var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                        var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                        var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                        var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                        var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                        var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                        var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);
                                        unbill_alo_offsite = parseFloat(unbill_alo_offsite) + parseFloat(final_prcnt_allocation);

                                        practice_detail[practice_already_exist_index] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': bench_cnt,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': bill_alo_cnt_onsite,
                                            'billable_allocation_offsite': bill_alo_cnt_offsite,
                                            'unbillabel_allo_onsite': unbill_alo_onsite,
                                            'unbillable_allo_offsite': unbill_alo_offsite
                                        };
                                    } else {
                                        var bench_cnt = parseFloat(practice_detail[practice_already_exist_index].bench_count);
                                        var trainee_cnt = parseFloat(practice_detail[practice_already_exist_index].trainee_count);
                                        var mngmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].management_count);
                                        var invstmnt_cnt = parseFloat(practice_detail[practice_already_exist_index].investment_count);
                                        var bill_alo_cnt_onsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_onsite);
                                        var bill_alo_cnt_offsite = parseFloat(practice_detail[practice_already_exist_index].billable_allocation_offsite);
                                        var unbill_alo_onsite = parseFloat(practice_detail[practice_already_exist_index].unbillabel_allo_onsite);
                                        unbill_alo_onsite = parseFloat(unbill_alo_onsite) + parseFloat(final_prcnt_allocation);
                                        var unbill_alo_offsite = parseFloat(practice_detail[practice_already_exist_index].unbillable_allo_offsite);

                                        practice_detail[practice_already_exist_index] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': bench_cnt,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': bill_alo_cnt_onsite,
                                            'billable_allocation_offsite': bill_alo_cnt_offsite,
                                            'unbillabel_allo_onsite': unbill_alo_onsite,
                                            'unbillable_allo_offsite': unbill_alo_offsite
                                        };
                                    }
                                }

                            } else {
                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': 0,
                                        'trainee_count': final_prcnt_allocation,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': final_prcnt_allocation,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': final_prcnt_allocation,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    practice_array.push(parctice_id);

                                    practice_detail[sr_practice_id] = {
                                        'practice_child': parctice_id,
                                        'practice_child_name': parctice_name,
                                        'practice_parent_id': parctice_id_parent,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': final_prcnt_allocation,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0
                                    };
                                    sr_practice_id++;
                                } else {
                                    practice_array.push(parctice_id);
                                   // if (parseInt(emp_susidiary) == 3) {
                                    if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        //cust_unique_list.push(project_cust);
                                        //proj_unique_list.push(project_id);
                                        practice_detail[sr_practice_id] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': 0,
                                            'billable_allocation_offsite': 0,
                                            'unbillabel_allo_onsite': 0,
                                            'unbillable_allo_offsite': final_prcnt_allocation
                                        };
                                        sr_practice_id++;
                                    } else {
                                        //cust_unique_list.push(project_cust);
                                        //proj_unique_list.push(project_id);
                                        practice_detail[sr_practice_id] = {
                                            'practice_child': parctice_id,
                                            'practice_child_name': parctice_name,
                                            'practice_parent_id': parctice_id_parent,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': 0,
                                            'billable_allocation_offsite': 0,
                                            'unbillabel_allo_onsite': final_prcnt_allocation,
                                            'unbillable_allo_offsite': 0
                                        };
                                        sr_practice_id++;
                                    }
                                }
                            }
                        }


                        if (is_resource_billable == 'T') {
                            total_billable_head++;

                            var is_available_index = find_proj_practice(parctice_id, project_id, proj_data);
                            is_available_index = proj_unique_list.indexOf(project_id);
                            //if(is_available_index == 0.0)
                            if (proj_unique_list.indexOf(project_id) < 0) {
                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': 0,
                                        'trainee_count': final_prcnt_allocation,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': final_prcnt_allocation,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    //cust_unique_list.push(project_cust);
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': final_prcnt_allocation,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': final_prcnt_allocation,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else {
                                    //if (parseInt(emp_susidiary) == 3) {
                                     if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        //cust_unique_list.push(project_cust);
                                        proj_unique_list.push(project_id);
                                        proj_data[sr_no] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': 0,
                                            'billable_count_offsite': 1,
                                            'unbillable_count_onsite': 0,
                                            'unbillable_count_offsite': 0,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': 0,
                                            'billable_allocation_offsite': final_prcnt_allocation,
                                            'unbillabel_allo_onsite': 0,
                                            'unbillable_allo_offsite': 0,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                        sr_no++;
                                    } else {
                                        //cust_unique_list.push(project_cust);
                                        proj_unique_list.push(project_id);
                                        proj_data[sr_no] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': 1,
                                            'billable_count_offsite': 0,
                                            'unbillable_count_onsite': 0,
                                            'unbillable_count_offsite': 0,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': final_prcnt_allocation,
                                            'billable_allocation_offsite': 0,
                                            'unbillabel_allo_onsite': 0,
                                            'unbillable_allo_offsite': 0,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                        sr_no++;
                                    }
                                }
                            } else {
                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    trainee_cnt = parseFloat(trainee_cnt) + parseFloat(final_prcnt_allocation);
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    mngmnt_cnt = parseFloat(mngmnt_cnt) + parseFloat(final_prcnt_allocation);
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    bench_count_final = parseFloat(bench_count_final) + parseFloat(final_prcnt_allocation);
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;
                                    invstmnt_cnt = parseFloat(invstmnt_cnt) + parseFloat(final_prcnt_allocation);

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else {
                                   // if (parseInt(emp_susidiary) == 3) {
                                    if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        var proj_already_index = is_available_index;
                                        var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                        var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                        billable_count_offsite = parseInt(billable_count_offsite) + parseInt(1);
                                        var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                        var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                        var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                        var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                        billable_allo_offsite = parseFloat(billable_allo_offsite) + parseFloat(final_prcnt_allocation);
                                        var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                        var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                        var bench_count_final = proj_data[proj_already_index].bench_count;
                                        var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                        var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                        var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                        proj_data[proj_already_index] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': billable_count_onsite,
                                            'billable_count_offsite': billable_count_offsite,
                                            'unbillable_count_onsite': unbillabl_count_onsite,
                                            'unbillable_count_offsite': unbillabl_count_offsite,
                                            'bench_count': bench_count_final,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': billable_allo_onsite,
                                            'billable_allocation_offsite': billable_allo_offsite,
                                            'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                            'unbillable_allo_offsite': unbillabl_allo_offsite,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                    } else {
                                        var proj_already_index = is_available_index;
                                        var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                        billable_count_onsite = parseInt(billable_count_onsite) + parseInt(1);
                                        var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                        var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                        var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                        var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                        billable_allo_onsite = parseFloat(billable_allo_onsite) + parseFloat(final_prcnt_allocation);
                                        var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                        var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                        var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                        var bench_count_final = proj_data[proj_already_index].bench_count;
                                        var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                        var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                        var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                        proj_data[proj_already_index] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': billable_count_onsite,
                                            'billable_count_offsite': billable_count_offsite,
                                            'unbillable_count_onsite': unbillabl_count_onsite,
                                            'unbillable_count_offsite': unbillabl_count_offsite,
                                            'bench_count': bench_count_final,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': billable_allo_onsite,
                                            'billable_allocation_offsite': billable_allo_offsite,
                                            'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                            'unbillable_allo_offsite': unbillabl_allo_offsite,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                    }
                                }
                            }


                        } // resource billable if ends
                        else {
                            var is_available_index = find_proj_practice(parctice_id, project_id, proj_data);
                            is_available_index = proj_unique_list.indexOf(project_id);
                            //if (is_available_index == 0.0)
                            if (proj_unique_list.indexOf(project_id) < 0) {
                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': 0,
                                        'trainee_count': final_prcnt_allocation,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': final_prcnt_allocation,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    //cust_unique_list.push(project_cust);
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': final_prcnt_allocation,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': 0,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    proj_unique_list.push(project_id);
                                    proj_data[sr_no] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': 0,
                                        'billable_count_offsite': 0,
                                        'unbillable_count_onsite': 0,
                                        'unbillable_count_offsite': 0,
                                        'bench_count': 0,
                                        'trainee_count': 0,
                                        'management_count': 0,
                                        'investment_count': final_prcnt_allocation,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': 0,
                                        'billable_allocation_offsite': 0,
                                        'unbillabel_allo_onsite': 0,
                                        'unbillable_allo_offsite': 0,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                    sr_no++;
                                } else {
                                  //  if (parseInt(emp_susidiary) == 3) {
                                   if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        //cust_unique_list.push(project_cust);
                                        proj_unique_list.push(project_id);
                                        proj_data[sr_no] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': 0,
                                            'billable_count_offsite': 0,
                                            'unbillable_count_onsite': 0,
                                            'unbillable_count_offsite': 1,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': 0,
                                            'billable_allocation_offsite': 0,
                                            'unbillabel_allo_onsite': 0,
                                            'unbillable_allo_offsite': final_prcnt_allocation,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                        sr_no++;
                                    } else {
                                        //cust_unique_list.push(project_cust);
                                        proj_unique_list.push(project_id);
                                        proj_data[sr_no] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': 0,
                                            'billable_count_offsite': 0,
                                            'unbillable_count_onsite': 1,
                                            'unbillable_count_offsite': 0,
                                            'bench_count': 0,
                                            'trainee_count': 0,
                                            'management_count': 0,
                                            'investment_count': 0,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': 0,
                                            'billable_allocation_offsite': 0,
                                            'unbillabel_allo_onsite': final_prcnt_allocation,
                                            'unbillable_allo_offsite': 0,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                        sr_no++;
                                    }
                                }
                            } else {
                                if (parseInt(is_proj_bench_category) == 2) //Trainee
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    trainee_cnt = parseFloat(trainee_cnt) + parseFloat(final_prcnt_allocation);
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else if (parseInt(is_proj_bench_category) == 11 || parseInt(is_proj_bench_category) == 17) //Management
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    mngmnt_cnt = parseFloat(mngmnt_cnt) + parseFloat(final_prcnt_allocation);
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else if (parseInt(is_proj_bench_category) == 4 || parseInt(is_proj_bench_category) == 16) //Bench
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    bench_count_final = parseFloat(bench_count_final) + parseFloat(final_prcnt_allocation);
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else if (parseInt(is_proj_bench_category) == 5) //Investment
                                {
                                    var proj_already_index = is_available_index;
                                    var billable_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                    var billable_count_offsite = proj_data[proj_already_index].billable_count_offsite;
                                    var unbillabl_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                    var unbillabl_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;

                                    var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                    var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;
                                    var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                    var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;

                                    var bench_count_final = proj_data[proj_already_index].bench_count;
                                    var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                    var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                    var invstmnt_cnt = proj_data[proj_already_index].investment_count;
                                    invstmnt_cnt = parseFloat(invstmnt_cnt) + parseFloat(final_prcnt_allocation);

                                    proj_data[proj_already_index] = {
                                        'project_id': project_id,
                                        'project_name': project_name,
                                        'cust_name': proj_cust_name,
                                        'cust_id': project_cust,
                                        'billable_count_onsite': billable_count_onsite,
                                        'billable_count_offsite': billable_count_offsite,
                                        'unbillable_count_onsite': unbillabl_count_onsite,
                                        'unbillable_count_offsite': unbillabl_count_offsite,
                                        'bench_count': bench_count_final,
                                        'trainee_count': trainee_cnt,
                                        'management_count': mngmnt_cnt,
                                        'investment_count': invstmnt_cnt,
                                        'billing_type': billing_type,
                                        'client_part': client_partner,
                                        'proj_manager': proj_manager,
                                        'delivery_mana': delivery_manager,
                                        'vertical_head': vertical_head,
                                        'customer_CP_name': customer_CP_name,
                                        'customer_CP_id': customer_CP_id,
                                        'billable_allocation_onsite': billable_allo_onsite,
                                        'billable_allocation_offsite': billable_allo_offsite,
                                        'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                        'unbillable_allo_offsite': unbillabl_allo_offsite,
                                        'practice_id': parctice_id,
                                        'parent_practice_id': parctice_id_parent,
                                        'practice_name': parctice_name,
                                        'parctice_parent_name': parctice_parent_name,
                                        'execusting_practice': execusting_practice
                                    };
                                } else {
                                    //if (parseInt(emp_susidiary) == 3) {
                                     if (parseInt(emp_susidiary) == 3 || parseInt(emp_susidiary) == 12) {
                                        var proj_already_index = is_available_index;
                                        var unbillable_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                        var unbillable_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;
                                        unbillable_count_offsite = parseInt(unbillable_count_offsite) + parseInt(1);
                                        var billabe_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                        var billabe_count_offsite = proj_data[proj_already_index].billable_count_offsite;

                                        var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                        var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;
                                        unbillabl_allo_offsite = parseFloat(unbillabl_allo_offsite) + parseFloat(final_prcnt_allocation);
                                        var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                        var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;

                                        var bench_count_final = proj_data[proj_already_index].bench_count;
                                        var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                        var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                        var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                        proj_data[proj_already_index] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': billabe_count_onsite,
                                            'billable_count_offsite': billabe_count_offsite,
                                            'unbillable_count_onsite': unbillable_count_onsite,
                                            'unbillable_count_offsite': unbillable_count_offsite,
                                            'bench_count': bench_count_final,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': billable_allo_onsite,
                                            'billable_allocation_offsite': billable_allo_offsite,
                                            'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                            'unbillable_allo_offsite': unbillabl_allo_offsite,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                    } else {
                                        var proj_already_index = is_available_index;
                                        var unbillable_count_onsite = proj_data[proj_already_index].unbillable_count_onsite;
                                        unbillable_count_onsite = parseInt(unbillable_count_onsite) + parseInt(1);
                                        var unbillable_count_offsite = proj_data[proj_already_index].unbillable_count_offsite;
                                        var billabe_count_onsite = proj_data[proj_already_index].billable_count_onsite;
                                        var billabe_count_offsite = proj_data[proj_already_index].billable_count_offsite;

                                        var unbillabl_allo_onsite = proj_data[proj_already_index].unbillabel_allo_onsite;
                                        unbillabl_allo_onsite = parseFloat(unbillabl_allo_onsite) + parseFloat(final_prcnt_allocation);
                                        var unbillabl_allo_offsite = proj_data[proj_already_index].unbillable_allo_offsite;
                                        var billable_allo_onsite = proj_data[proj_already_index].billable_allocation_onsite;
                                        var billable_allo_offsite = proj_data[proj_already_index].billable_allocation_offsite;

                                        var bench_count_final = proj_data[proj_already_index].bench_count;
                                        var trainee_cnt = proj_data[proj_already_index].trainee_count;
                                        var mngmnt_cnt = proj_data[proj_already_index].management_count;
                                        var invstmnt_cnt = proj_data[proj_already_index].investment_count;

                                        proj_data[proj_already_index] = {
                                            'project_id': project_id,
                                            'project_name': project_name,
                                            'cust_name': proj_cust_name,
                                            'cust_id': project_cust,
                                            'billable_count_onsite': billabe_count_onsite,
                                            'billable_count_offsite': billabe_count_offsite,
                                            'unbillable_count_onsite': unbillable_count_onsite,
                                            'unbillable_count_offsite': unbillable_count_offsite,
                                            'bench_count': bench_count_final,
                                            'trainee_count': trainee_cnt,
                                            'management_count': mngmnt_cnt,
                                            'investment_count': invstmnt_cnt,
                                            'billing_type': billing_type,
                                            'client_part': client_partner,
                                            'proj_manager': proj_manager,
                                            'delivery_mana': delivery_manager,
                                            'vertical_head': vertical_head,
                                            'customer_CP_name': customer_CP_name,
                                            'customer_CP_id': customer_CP_id,
                                            'billable_allocation_onsite': billable_allo_onsite,
                                            'billable_allocation_offsite': billable_allo_offsite,
                                            'unbillabel_allo_onsite': unbillabl_allo_onsite,
                                            'unbillable_allo_offsite': unbillabl_allo_offsite,
                                            'practice_id': parctice_id,
                                            'parent_practice_id': parctice_id_parent,
                                            'practice_name': parctice_name,
                                            'parctice_parent_name': parctice_parent_name,
                                            'execusting_practice': execusting_practice
                                        };
                                    }
                                }
                            }
                        }
                    }

                    // get total sub-practices under parent practice
                    var a_child_practice_not_allocated_count = new Array();
                    var a_child_practice_not_allocated_count_details = new Array();
                    var filters_search_child_practice = [
                        ['isinactive', 'is', 'F'], 'and',
                        ['custrecord_parent_practice', 'anyof', practice_internal_id], 'and',
                        ['custrecord_practice_type_ticker', 'anyof', parseInt(2)]
                    ];

                    var columns_sub_practice_involved = new Array();
                    columns_sub_practice_involved[0] = new nlobjSearchColumn('name');

                    var a_subPracticeInvolved = nlapiSearchRecord('department', null, filters_search_child_practice, columns_sub_practice_involved);
                    if (a_subPracticeInvolved) {
                        for (var i_sub_prac_count = 0; i_sub_prac_count < a_subPracticeInvolved.length; i_sub_prac_count++) {
                            a_child_practice_not_allocated_count.push(a_subPracticeInvolved[i_sub_prac_count].getId());

                            a_child_practice_not_allocated_count_details.push({
                                'Child_Practice_Name': a_subPracticeInvolved[i_sub_prac_count].getValue('name'),
                                'Child_Practice_Id': a_subPracticeInvolved[i_sub_prac_count].getId(),
                                'Total_Unallocated_Count': 0
                            });
                        }
                    }

                    var i_unallocated_resource_count = 0;
                    var a_resouce_unallocated_details = new Array();
                    var filter_unallocated_emp = [
                        ['isinactive', 'is', 'F'], 'and',
                       // ['internalid', 'noneof', employee_allocated_list], 'and', //commented the code to avoid the SSS_SEARCH_TIMEOUT error
                        ['custentity_employee_inactive', 'is', 'F'], 'and',
                        ['custentity_implementationteam', 'is', 'F'], 'and',
                        [
                            ['department.custrecord_parent_practice', 'anyof', practice_internal_id], 'or',
                            ['department', 'anyof', practice_internal_id]
                        ]
                    ];

                    var column_unallocated_emp = new Array();
                    column_unallocated_emp[0] = new nlobjSearchColumn('entityid');
                    column_unallocated_emp[1] = new nlobjSearchColumn('department');
                    column_unallocated_emp[2] = new nlobjSearchColumn('subsidiary');
                    column_unallocated_emp[3] = new nlobjSearchColumn('location');
                    column_unallocated_emp[4] = new nlobjSearchColumn('title');

                    var a_unallocated_resources = searchRecord('employee', null, filter_unallocated_emp, column_unallocated_emp);
                    if (a_unallocated_resources) {
                       // i_unallocated_resource_count = a_unallocated_resources.length;
						for (var i_emp_index = 0; i_emp_index < a_unallocated_resources.length; i_emp_index++) {
                            if(employee_allocated_list.indexOf(a_unallocated_resources[i_emp_index].getId())==-1)//Added the code to prevent the SSS_SEARCH_TIMEOUT error
							{
								i_unallocated_resource_count=i_unallocated_resource_count++;
								if (a_child_practice_not_allocated_count.indexOf(a_unallocated_resources[i_emp_index].getValue('department')) >= 0) {
									var i_already_considered_prac_index = a_child_practice_not_allocated_count.indexOf(a_unallocated_resources[i_emp_index].getValue('department'));
									var i_total_un_alllocated_count_existing = a_child_practice_not_allocated_count_details[i_already_considered_prac_index].Total_Unallocated_Count;
									i_total_un_alllocated_count_existing = parseInt(i_total_un_alllocated_count_existing) + parseInt(1);
									a_child_practice_not_allocated_count_details[i_already_considered_prac_index] = {
										'Child_Practice_Name': a_unallocated_resources[i_emp_index].getText('department'),
										'Child_Practice_Id': a_unallocated_resources[i_emp_index].getValue('department'),
										'Total_Unallocated_Count': i_total_un_alllocated_count_existing
									}

								} else {
									a_child_practice_not_allocated_count_details.push({
										'Child_Practice_Name': a_unallocated_resources[i_emp_index].getText('department'),
										'Child_Practice_Id': a_unallocated_resources[i_emp_index].getValue('department'),
										'Total_Unallocated_Count': 1
									});

									a_child_practice_not_allocated_count.push(a_unallocated_resources[i_emp_index].getValue('department'));
								}

								a_resouce_unallocated_details.push({
									's_emp_name': a_unallocated_resources[i_emp_index].getValue('entityid'),
									's_emp_practice': a_unallocated_resources[i_emp_index].getText('department'),
									's_emp_subsidiary': a_unallocated_resources[i_emp_index].getText('subsidiary'),
									's_emp_location': a_unallocated_resources[i_emp_index].getText('location'),
									's_emp_designation': a_unallocated_resources[i_emp_index].getValue('title')
								});
							}
						}
                    }

                    //nlapiLogExecution('audit', 'proj detail len:-- ', proj_data.length);
                    send_mail(proj_data, billing_from_date, practice_arr_list, practice_detail, excel_file_obj, employee_allocated_list, i_unallocated_resource_count, a_resouce_unallocated_details, a_child_practice_not_allocated_count_details, a_child_practice_not_allocated_count);
                    yieldScript(context);
                }
                //}
				}
				catch(e)
				{
					 nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', e);
					var context=nlapiGetContext();

					var mailTemplate = "";
					mailTemplate += "<p> This is to inform that Daily Ticker for Practice Head script has been failed for practice i.e."+parctice_srch[i_indx].getValue('name')+".And It is having Error - <b>["+e.code+" : "+e.message +"]</b> and therefore, system is not able to the send the notification to Practice head</p>";

					mailTemplate += "<p> Please check the issue and do fix.</p>";
					mailTemplate += "<p><b> Script Id:- "+context.getScriptId() +"</b></p>";
					mailTemplate += "<p><b> Script Deployment Id:- "+context.getDeploymentId() +"</b></p>";
					mailTemplate += "<br/>"
					mailTemplate += "<p>Regards, <br/> Information Systems</p>";
					nlapiSendEmail(442,'sitaram.upadhya@brillio.com', 'Failure Notification on '+parctice_srch[i_indx].getValue('name')+' Billed Count (Daily Ticker For Practice Head) ',mailTemplate, null, null, null, null);
				
				}
            }
        }
        //nlapiLogExecution('audit', 'practice len:- ', practice_arr_list.length);
        return;

        if (_logValidation(project_allocation_result)) {


        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
    }
}

function send_mail(proj_data, billing_from_date, practice_arr_list, practice_detail, excel_file_obj, employee_allocated_list, i_unallocated_resource_count, a_resouce_unallocated_details, a_child_practice_not_allocated_count_details, a_child_practice_not_allocated_count) {
  
    if (_logValidation(proj_data)) {
       // nlapiYieldScript(); //commented the to optimize the script
       
	   //nlapiLogExecution('debug', 'Practice Listings ====', JSON.stringify(practice_arr_list));
	   var mailList = getPracticeMailingList();

        var today = new Date();
        var date_yesterday = nlapiAddDays(today, -1);
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var hours = today.getHours();
        var minutes = today.getMinutes();

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;

        var total_billable_head = 0;
        var total_unbillable_head = 0;
        var Total_billable_head_onsite = 0;
        var Total_billabel_head_offsite = 0;
        var Total_unbillable_head_onsite = 0;
        var Total_unbillable_head_offsite = 0;
        var total_billable_head_allo = 0;
        var total_billed_head_diff_report = 0;
        var incrmnt = 1;

        // Total Row Practice Wise Variables
        var totalRow_billed_count_onsite = 0;
        var totalRow_billed_count_offsite = 0;
        var totalRow_unbilled_count_onsite = 0;
        var totalRow_unbilled_count_offsite = 0;
        var totalRow_billed_count = 0;
        var totalRow_unbilled_count = 0;

        // Total Row Bench,Management,Trainee,Investment
        var totalRow_bench_count = 0;
        var totalRow_trainee_count = 0;
        var totalRow_mngmnt_count = 0;
        var totalRow_invest_count = 0;
        var toatalColmn = 0;
        var total_bench = 0;
        var total_management = 0;
        var total_investment = 0;
        var total_trainee = 0;
        var total_unallocated_column = 0;

        var strVar = '';
        strVar += '<html>';
        strVar += '<body>';

        strVar += '<p>Daily Ticker dated ' + today + ' ' + hours + ':' + minutes + ' (24 Hour Format)</p>';

        strVar += '<table>';

        // Diff Report Table
        for (var parent_index = 0; parent_index < practice_arr_list.length; parent_index++) {
            var prent_practice_id = practice_arr_list[parent_index].internal_id;
            for (var child_index = 0; child_index < practice_detail.length; child_index++) {
                var child_parent_practice_id = practice_detail[child_index].practice_parent_id;
                var child_practice_id = practice_detail[child_index].practice_id;
                if (parseInt(prent_practice_id) == parseInt(child_parent_practice_id)) {
                    total_billed_head_diff_report = parseFloat(total_billed_head_diff_report) + parseFloat(practice_detail[child_index].billable_allocation_onsite);
                    total_billed_head_diff_report = parseFloat(total_billed_head_diff_report) + parseFloat(practice_detail[child_index].billable_allocation_offsite);
                }
            }
        }

        var billed_count_last_day = 0;
        var filters_emp_data = [
            ['custrecord_parent_practice_daily_ticker', 'anyof', parseInt(prent_practice_id)], 'and',
            ['custrecord_date_of_dump', 'on', date_yesterday]
        ];

        var emp_data_rcrd_srch = nlapiSearchRecord('customrecord_daily_emp_head_count_detail', 'customsearch1661', filters_emp_data);
        if (emp_data_rcrd_srch) {
            var columns = emp_data_rcrd_srch[0].getAllColumns();
            billed_count_last_day = emp_data_rcrd_srch[0].getValue(columns[0]);
            //nlapiLogExecution('audit', 'yesterday billed count:- ' + billed_count_last_day, total_billed_head_diff_report);
        }

        if (!billed_count_last_day)
            billed_count_last_day = 0;

        var total_diff = parseFloat(billed_count_last_day) - parseFloat(total_billed_head_diff_report);
        total_diff = Math.abs(total_diff);

        for (var parent_index = 0; parent_index < practice_arr_list.length; parent_index++) {

            // Table For Practice Wise BreakUp
            strVar += '	<tr>';
            strVar += ' <tr><td width="20%" font-size="11" align="center">Total Head Count Detail</td></tr>';
            strVar += ' <td width="100%">';
            strVar += '<table width="100%" border="1">';

            strVar += '	<tr>';
            strVar += ' <td width="20%" font-size="11" align="center">Practice Name</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">Sub-Practice Total</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center">Onsite Billed</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center">Offshore Billed</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">Total Billed</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center">Onsite Unbilled</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center">Offshore Unbilled</td>';
            strVar += ' <td width="7%" font-size="11" align="center">Bench</td>';
            strVar += ' <td width="7%" font-size="11" align="center">Trainee</td>';
            strVar += ' <td width="7%" font-size="11" align="center">Management</td>';
            strVar += ' <td width="7%" font-size="11" align="center">Investment</td>';
            strVar += ' <td width="7%" font-size="11" align="center">Un-Allocated Count</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">Total Unbilled</td>';
            strVar += '	</tr>';

            var prent_practice_id = practice_arr_list[parent_index].internal_id;
            var practice_mail = practice_arr_list[parent_index].mail_id;
            //var practice_mail = 'shamanth.k@brillio.com';
            var onsite_prac_head_mail = practice_arr_list[parent_index].onsite_prac_head_mail;
            for (var practice_grp = 0; practice_grp < practice_detail.length; practice_grp++) {
                var practice_id_child_parent = practice_detail[practice_grp].practice_parent_id;
                var child_practice_id = practice_detail[practice_grp].practice_child;

                if (parseInt(prent_practice_id) == parseInt(practice_id_child_parent)) {
                    var i_index_for_unallocated_count = a_child_practice_not_allocated_count.indexOf(child_practice_id);

                    totalRow_billed_count_onsite = parseFloat(totalRow_billed_count_onsite) + parseFloat(practice_detail[practice_grp].billable_allocation_onsite);
                    totalRow_billed_count_offsite = parseFloat(totalRow_billed_count_offsite) + parseFloat(practice_detail[practice_grp].billable_allocation_offsite);

                    totalRow_unbilled_count_onsite = parseFloat(totalRow_unbilled_count_onsite) + parseFloat(practice_detail[practice_grp].unbillabel_allo_onsite);
                    totalRow_unbilled_count_offsite = parseFloat(totalRow_unbilled_count_offsite) + parseFloat(practice_detail[practice_grp].unbillable_allo_offsite);

                    var total_bill = parseFloat(practice_detail[practice_grp].billable_allocation_onsite) + parseFloat(practice_detail[practice_grp].billable_allocation_offsite);
                    totalRow_billed_count = parseFloat(total_bill) + parseFloat(totalRow_billed_count);
                    total_bill = parseFloat(total_bill).toFixed(2);

                    var total_unbill = parseFloat(practice_detail[practice_grp].unbillabel_allo_onsite) + parseFloat(practice_detail[practice_grp].unbillable_allo_offsite) + parseFloat(practice_detail[practice_grp].bench_count) + parseFloat(practice_detail[practice_grp].trainee_count) + parseFloat(practice_detail[practice_grp].management_count) + parseFloat(practice_detail[practice_grp].investment_count);
                    totalRow_unbilled_count = parseFloat(total_unbill) + parseFloat(totalRow_unbilled_count);

                    var i_total_unallocated_resource_count = 0;
                    if (parseInt(i_index_for_unallocated_count) > 0)
                        i_total_unallocated_resource_count = a_child_practice_not_allocated_count_details[i_index_for_unallocated_count].Total_Unallocated_Count;

                    totalRow_unbilled_count = parseFloat(totalRow_unbilled_count) + parseFloat(i_total_unallocated_resource_count);
                    total_unbill = parseFloat(total_unbill) + parseFloat(i_total_unallocated_resource_count);
                    total_unbill = parseFloat(total_unbill).toFixed(2);

                    var total_colmn_level = parseFloat(total_bill) + parseFloat(total_unbill);
                    toatalColmn = parseFloat(total_colmn_level) + parseFloat(toatalColmn);
                    //toatalColmn = parseFloat(toatalColmn) + parseFloat(a_child_practice_not_allocated_count_details[i_index_for_unallocated_count].Total_Unallocated_Count); 

                    total_bench = parseFloat(total_bench) + parseFloat(practice_detail[practice_grp].bench_count);
                    total_management = parseFloat(total_management) + parseFloat(practice_detail[practice_grp].management_count);
                    total_investment = parseFloat(total_investment) + parseFloat(practice_detail[practice_grp].investment_count);
                    total_trainee = parseFloat(total_trainee) + parseFloat(practice_detail[practice_grp].trainee_count);
                    total_unallocated_column = parseFloat(total_unallocated_column) + parseFloat(i_total_unallocated_resource_count);

                    strVar += '	<tr>';
                    strVar += ' <td width="20%" font-size="11" align="center">' + practice_detail[practice_grp].practice_child_name + '</td>';
                    strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_colmn_level).toFixed(2) + '</td>';
                    strVar += ' <td width="7.5%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].billable_allocation_onsite).toFixed(2) + '</td>';
                    strVar += ' <td width="7.5%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].billable_allocation_offsite).toFixed(2) + '</td>';
                    strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_bill).toFixed(2) + '</td>';
                    strVar += ' <td width="7.5%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].unbillabel_allo_onsite).toFixed(2) + '</td>';
                    strVar += ' <td width="7.5%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].unbillable_allo_offsite).toFixed(2) + '</td>';
                    strVar += ' <td width="7%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].bench_count).toFixed(2) + '</td>';
                    strVar += ' <td width="7%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].trainee_count).toFixed(2) + '</td>';
                    strVar += ' <td width="7%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].management_count).toFixed(2) + '</td>';
                    strVar += ' <td width="7%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].investment_count).toFixed(2) + '</td>';
                    strVar += ' <td width="7%" font-size="11" align="center">' + parseFloat(i_total_unallocated_resource_count).toFixed(2) + '</td>';
                    strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_unbill).toFixed(2) + '</td>';
                    strVar += '	</tr>';
                }
            }

            strVar += '	<tr>';
            strVar += ' <td width="25%" font-size="11" align="center" bgcolor="#D3D3D3">Total</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(toatalColmn).toFixed(2) + '</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_billed_count_onsite).toFixed(2) + '</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_billed_count_offsite).toFixed(2) + '</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_billed_count).toFixed(2) + '</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_unbilled_count_onsite).toFixed(2) + '</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_unbilled_count_offsite).toFixed(2) + '</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_bench).toFixed(2) + '</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_trainee).toFixed(2) + '</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_management).toFixed(2) + '</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_investment).toFixed(2) + '</td>';
            strVar += ' <td width="7%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(total_unallocated_column).toFixed(2) + '</td>';
            strVar += ' <td width="7.5%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_unbilled_count).toFixed(2) + '</td>';
            strVar += '	</tr>';

            strVar += '</table>';
            strVar += ' </td>';
            strVar += '	</tr>';

            //}

            if (a_resouce_unallocated_details.length > 0) {
                strVar += '	<tr height="10px">';
                strVar += ' <td width="100%">';
                strVar += '	</tr>';

                strVar += ' <tr><td width="20%" font-size="11" align="center">Unallocated Resource Details</td></tr>';
                strVar += '	<tr>';
                strVar += ' <td width="100%">';
                strVar += '<table width="100%" border="1">';

                strVar += '	<tr>';
                strVar += ' <td width="35%" font-size="11" align="center">Resource Name</td>';
                strVar += ' <td width="35%" font-size="11" align="center">Practice</td>';
                strVar += ' <td width="30%" font-size="11" align="center">Subsidiary</td>';
                strVar += ' <td width="30%" font-size="11" align="center">Location</td>';
                strVar += ' <td width="30%" font-size="11" align="center">Designation</td>';
                strVar += '	</tr>';

                for (var i_emp_index = 0; i_emp_index < a_resouce_unallocated_details.length; i_emp_index++) {
                    strVar += '	<tr>';
                    strVar += ' <td width="35%" font-size="11" align="center">' + a_resouce_unallocated_details[i_emp_index].s_emp_name + '</td>';
                    strVar += ' <td width="35%" font-size="11" align="center">' + a_resouce_unallocated_details[i_emp_index].s_emp_practice + '</td>';
                    strVar += ' <td width="30%" font-size="11" align="center">' + a_resouce_unallocated_details[i_emp_index].s_emp_subsidiary + '</td>';
                    strVar += ' <td width="30%" font-size="11" align="center">' + a_resouce_unallocated_details[i_emp_index].s_emp_location + '</td>';
                    strVar += ' <td width="30%" font-size="11" align="center">' + a_resouce_unallocated_details[i_emp_index].s_emp_designation + '</td>';
                    strVar += '	</tr>';
                }

                strVar += '</table>';
                strVar += ' </td>';
                strVar += '	</tr>';
            }

            strVar += '	<tr height="10px">';
            strVar += ' <td width="100%">';
            strVar += '	</tr>';

            strVar += ' <tr><td width="20%" font-size="11" align="center">Diff Report</td></tr>';
            strVar += '	<tr>';
            strVar += ' <td width="100%">';
            strVar += '<table width="100%" border="1">';

            strVar += '	<tr>';
            strVar += ' <td width="35%" font-size="11" align="center">Billed Count Yesterday</td>';
            strVar += ' <td width="35%" font-size="11" align="center">Billed Count Today</td>';
            strVar += ' <td width="30%" font-size="11" align="center">Difference(+/-)</td>';
            strVar += '	</tr>';

            strVar += '	<tr>';
            strVar += ' <td width="35%" font-size="11" align="center">' + parseFloat(billed_count_last_day).toFixed(2) + '</td>';
            strVar += ' <td width="35%" font-size="11" align="center">' + parseFloat(total_billed_head_diff_report).toFixed(2) + '</td>';
            strVar += ' <td width="30%" font-size="11" align="center">' + parseFloat(total_diff).toFixed(2) + '</td>';
            strVar += '	</tr>';

            strVar += '</table>';
            strVar += ' </td>';
            strVar += '	</tr>';

            if (billed_count_last_day > total_billed_head_diff_report) {
                var filters_emp_diff_data = [
                    //['custrecord_emp_daily_ticker', 'noneof', employee_allocated_list], 'and',
                    ['custrecord_emp_daily_ticker.custentity_employee_inactive', 'is', 'F'], 'and',
                    ['custrecord_date_of_dump', 'on', date_yesterday], 'and',
                    ['custrecord_parent_practice_daily_ticker', 'anyof', parseInt(prent_practice_id)]
                ];
                var columns_emp_diff_data = new Array();
                columns_emp_diff_data[0] = new nlobjSearchColumn('custrecord_emp_daily_ticker');
                columns_emp_diff_data[1] = new nlobjSearchColumn('custrecord_emp_practice_daily_ticker');
                columns_emp_diff_data[2] = new nlobjSearchColumn('custrecord_project_daily_ticker');
                columns_emp_diff_data[3] = new nlobjSearchColumn('custrecord_customer_daily_ticker');
                columns_emp_diff_data[4] = new nlobjSearchColumn('custrecord_region_daily_ticker');
                //var emp_data_srch_yesterday = nlapiSearchRecord('customrecord_daily_emp_head_count_detail', null, filters_emp_diff_data, columns_emp_diff_data);
                var emp_data_srch_yesterday = searchRecord('customrecord_daily_emp_head_count_detail', null, filters_emp_diff_data, columns_emp_diff_data);
				
				var strVar2="";var falgs=false;
				if (emp_data_srch_yesterday) {
					
                    
                    
					var i_srch_yesterday=0;
                    for (var last_day_emp_index = 0; last_day_emp_index < emp_data_srch_yesterday.length; last_day_emp_index++) {
                        
						if(employee_allocated_list.indexOf(emp_data_srch_yesterday[last_day_emp_index].getValue('custrecord_emp_daily_ticker'))==-1)
						{
							i_srch_yesterday++;
							falgs=true;
							strVar2 += '	<tr>';
							strVar2 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_region_daily_ticker') + '</td>';
							strVar2 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_customer_daily_ticker') + '</td>';
							strVar2 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_project_daily_ticker') + '</td>';
							strVar2 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_emp_daily_ticker') + '</td>';
							strVar2 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_emp_practice_daily_ticker') + '</td>';
							strVar2 += '	</tr>';
						}
                    }
					
					if(falgs==true){
						nlapiLogExecution('audit','total diff count:- ',i_srch_yesterday);
					strVar += '	<tr height="10px">';
                    strVar += ' <td width="100%">';
                    strVar += '	</tr>';
                    strVar += ' <tr><td width="20%" font-size="11" align="center">Diff Report Resource Details</td></tr>';
                    strVar += '	<tr>';
                    strVar += ' <td width="100%">';
                    strVar += '<table width="100%" border="1">';
                    strVar += '	<tr>';
                    strVar += ' <td width="6%" font-size="11" align="center">Region</td>';
                    strVar += ' <td width="6%" font-size="11" align="center">Customer</td>';
                    strVar += ' <td width="6%" font-size="11" align="center">Project</td>';
                    strVar += ' <td width="6%" font-size="11" align="center">Employee</td>';
                    strVar += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
                    strVar += '	</tr>';
					strVar += strVar2;
                    strVar += '</table>';
                    strVar += ' </td>';
                    strVar += '	</tr>';
					}

                }
            } else if (billed_count_last_day < total_billed_head_diff_report) {
                var emp_yesterday_unique_list = new Array();
                var filters_emp_data = [
                    ['custrecord_parent_practice_daily_ticker', 'anyof', parseInt(prent_practice_id)], 'and',
                    ['custrecord_date_of_dump', 'on', date_yesterday], 'and',
                    ['custrecord_resource_billabl_daily_ticker', 'is', 'T']
                ];
                var columns_emp_data = new Array();
                columns_emp_data[0] = new nlobjSearchColumn('custrecord_emp_daily_ticker');
                var emp_data_srch_yesterday = nlapiSearchRecord('customrecord_daily_emp_head_count_detail', null, filters_emp_data, columns_emp_data);
                if (emp_data_srch_yesterday) {
                    for (var emp_index = 0; emp_index < emp_data_srch_yesterday.length; emp_index++)
                        emp_yesterday_unique_list.push(emp_data_srch_yesterday[emp_index].getValue('custrecord_emp_daily_ticker'));
                }

                if (emp_yesterday_unique_list.length > 0) {
                    var filters_emp_diff_data = [
                        //['resource', 'noneof', emp_yesterday_unique_list], 'and',
                        ['startdate', 'notafter', billing_from_date], 'and',
                        ['enddate', 'notbefore', billing_from_date], 'and',
                        ['employee.custentity_employee_inactive', 'is', 'F'], 'and',
                        [
                            ['custevent_practice', 'anyof', parseInt(prent_practice_id)], 'or',
                            ['custevent_practice.custrecord_parent_practice', 'anyof', parseInt(prent_practice_id)]
                        ], 'and',
                        ['custeventrbillable', 'is', 'T']
                    ];

                    var columns_emp_diff_data = new Array();
                    columns_emp_diff_data[0] = new nlobjSearchColumn('resource');
                    columns_emp_diff_data[1] = new nlobjSearchColumn('customer');
                    columns_emp_diff_data[2] = new nlobjSearchColumn('company');
                    columns_emp_diff_data[3] = new nlobjSearchColumn('custentity_region', 'customer');
                    columns_emp_diff_data[4] = new nlobjSearchColumn('custevent_practice');
                    //var emp_data_srch_yesterday = nlapiSearchRecord('resourceallocation', null, filters_emp_diff_data, columns_emp_diff_data);
                    var emp_data_srch_yesterday = searchRecord('resourceallocation', null, filters_emp_diff_data, columns_emp_diff_data);
					var strVar3="";var flag3=false;
					if (emp_data_srch_yesterday) {
                        //nlapiLogExecution('audit','total diff count:- ',emp_data_srch_yesterday.length);
                        

                        for (var last_day_emp_index = 0; last_day_emp_index < emp_data_srch_yesterday.length; last_day_emp_index++) {
                           
							if(emp_yesterday_unique_list.indexOf(emp_data_srch_yesterday[last_day_emp_index].getValue('resource'))==-1)
							{
								flag3=true;
						    strVar3 += '	<tr>';
                            strVar3 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custentity_region', 'customer') + '</td>';
                            strVar3 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('customer') + '</td>';
                            strVar3 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('company') + '</td>';
                            strVar3 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('resource') + '</td>';
                            strVar3 += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custevent_practice') + '</td>';
                            strVar3 += '	</tr>';
							}
                        }
						
						if(flag3==true){
						strVar += '	<tr height="10px">';
                        strVar += ' <td width="100%">';
                        strVar += '	</tr>';

                        strVar += ' <tr><td width="20%" font-size="11" align="center">Diff Report Resource Details</td></tr>';
                        strVar += '	<tr>';
                        strVar += ' <td width="100%">';
                        strVar += '<table width="100%" border="1">';

                        strVar += '	<tr>';
                        strVar += ' <td width="6%" font-size="11" align="center">Region</td>';
                        strVar += ' <td width="6%" font-size="11" align="center">Customer</td>';
                        strVar += ' <td width="6%" font-size="11" align="center">Project</td>';
                        strVar += ' <td width="6%" font-size="11" align="center">Employee</td>';
                        strVar += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
                        strVar += '	</tr>';
						strVar += strVar3;
                        strVar += '</table>';
                        strVar += ' </td>';
                        strVar += '	</tr>';
						}

                    }
                }
            }

            //strVar += '</table>';

            //for (var parent_index = 0; parent_index < practice_arr_list.length; parent_index++)
            //{
            var total_billable_head = 0;
            var send_mail_flag = 0;
            var prent_practice_id = practice_arr_list[parent_index].internal_id;

            /*var strVar = '';
            strVar += '<html>';
            strVar += '<body>';
            
            strVar += '<p>Daily Ticker dated '+today+' '+hours+':'+minutes+' (24 Hour Format)</p>';*/

            //strVar += '<table>';

            if (parseInt(prent_practice_id) != parseInt(457) && parseInt(prent_practice_id) != parseInt(407)) {
                // Table for Project Wise BreakUp
                strVar += '	<tr height="10px">';
                strVar += ' <td width="100%">';
                strVar += '	</tr>';

                strVar += ' <tr><td width="20%" font-size="11" align="center">Head Count Detail Project wise</td></tr>';
                strVar += '	<tr>';
                strVar += ' <td width="100%">';
                strVar += '<table width="100%" border="1">';

                strVar += '	<tr>';
                strVar += ' <td width="20%" font-size="11" align="center">Project Name</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Oniste Billed Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Offiste Billed Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Total Billed Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Onsite Unbilled Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Offsite Unbilled Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Total Unbilled Count</td>';
                strVar += ' <td width="7%" font-size="11" align="center">Project Manager</td>';
                strVar += ' <td width="7%" font-size="11" align="center">Delivery Manager</td>';
                strVar += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
                //strVar += ' <td width="5%" font-size="11" align="center">Investment Count</td>';
                strVar += '	</tr>';

                for (var child_index = 0; child_index < proj_data.length; child_index++) {
                    var child_parent_practice_id = proj_data[child_index].parent_practice_id;
                    var child_practice_id = proj_data[child_index].practice_id;

                    if (parseInt(prent_practice_id) == parseInt(child_parent_practice_id)) {
                        total_billable_head = parseInt(total_billable_head) + parseInt(proj_data[child_index].billable_count_onsite);
                        total_billable_head = parseInt(total_billable_head) + parseInt(proj_data[child_index].billable_count_offsite);

                        var total_bill_count = parseFloat(proj_data[child_index].billable_allocation_onsite) + parseFloat(proj_data[child_index].billable_allocation_offsite);
                        var total_unbill_count = parseFloat(proj_data[child_index].unbillabel_allo_onsite) + parseFloat(proj_data[child_index].unbillable_allo_offsite);

                        strVar += '	<tr>';
                        strVar += ' <td width="20%" font-size="11" align="left">' + proj_data[child_index].project_name + '</td>';
                        strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_data[child_index].billable_allocation_onsite).toFixed(2) + '</td>';
                        strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_data[child_index].billable_allocation_offsite).toFixed(2) + '</td>';
                        strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_bill_count).toFixed(2) + '</td>';
                        strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_data[child_index].unbillabel_allo_onsite).toFixed(2) + '</td>';
                        strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_data[child_index].unbillable_allo_offsite).toFixed(2) + '</td>';
                        strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_unbill_count).toFixed(2) + '</td>';
                        strVar += ' <td width="7%" font-size="11" align="center">' + proj_data[child_index].proj_manager + '</td>';
                        strVar += ' <td width="7%" font-size="11" align="center">' + proj_data[child_index].delivery_mana + '</td>';
                        strVar += ' <td width="6%" font-size="11" align="center">' + proj_data[child_index].execusting_practice + '</td>';
                        //strVar += ' <td width="5%" font-size="11" align="center">'+parseFloat(proj_data[child_index].investment_count).toFixed(2)+'</td>';
                        strVar += '	</tr>';

                        send_mail_flag = 1;
                    }
                }

                strVar += '</table>';
                strVar += ' </td>';
                strVar += '	</tr>';
            } else if (parseInt(prent_practice_id) == parseInt(457) || parseInt(prent_practice_id) == parseInt(407)) {
                send_mail_flag = 1;
            }
            // Table For Bench,Investment,Management,Trainee
            /*strVar += '	<tr height="10px">';
				strVar += ' <td width="100%">';
				strVar += '	</tr>';
				
				strVar += '	<tr>';
				strVar += ' <td width="100%">';
				strVar += '<table width="100%" border="1">';
			
					strVar += '	<tr>';
					strVar += ' <td width="20%" font-size="11" align="center">Practice Name</td>';
					strVar += ' <td width="20%" font-size="11" align="center">Bench Count</td>';
					strVar += ' <td width="20%" font-size="11" align="center">Trainee Count</td>';
					strVar += ' <td width="20%" font-size="11" align="center">Management Count</td>';
					strVar += ' <td width="20%" font-size="11" align="center">Investment Count</td>';
					strVar += '	</tr>';
				
				for(var practice_grp=0; practice_grp<practice_detail.length; practice_grp++)
				{
					var practice_id_child_parent = practice_detail[practice_grp].practice_parent_id;
					var child_practice_id = practice_detail[practice_grp].practice_child;
					
					if (parseInt(prent_practice_id) == parseInt(practice_id_child_parent))
					{
						totalRow_bench_count = parseFloat(totalRow_bench_count) + parseFloat(practice_detail[practice_grp].bench_count);
						totalRow_trainee_count = parseFloat(totalRow_trainee_count) + parseFloat(practice_detail[practice_grp].trainee_count);
						
						totalRow_mngmnt_count = parseFloat(totalRow_mngmnt_count) + parseFloat(practice_detail[practice_grp].management_count);
						totalRow_invest_count = parseFloat(totalRow_invest_count) + parseFloat(practice_detail[practice_grp].investment_count);
						
						strVar += '	<tr>';
						strVar += ' <td width="20%" font-size="11" align="center">' + practice_detail[practice_grp].practice_child_name + '</td>';
						strVar += ' <td width="20%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].bench_count).toFixed(2) + '</td>';
						strVar += ' <td width="20%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].trainee_count).toFixed(2) + '</td>';
						strVar += ' <td width="20%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].management_count).toFixed(2) + '</td>';
						strVar += ' <td width="20%" font-size="11" align="center">' + parseFloat(practice_detail[practice_grp].investment_count).toFixed(2) + '</td>';
						strVar += '	</tr>';
					}
				}
				
				strVar += '	<tr>';
				strVar += ' <td width="20%" font-size="11" align="center" bgcolor="#D3D3D3">Total</td>';
				strVar += ' <td width="20%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_bench_count).toFixed(2) + '</td>';
				strVar += ' <td width="20%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_trainee_count).toFixed(2) + '</td>';
				strVar += ' <td width="20%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_mngmnt_count).toFixed(2) + '</td>';
				strVar += ' <td width="20%" font-size="11" align="center" bgcolor="#D3D3D3">' + parseFloat(totalRow_invest_count).toFixed(2) + '</td>';
				strVar += '	</tr>';
				
				strVar += '</table>';
				strVar += ' </td>';	
				strVar += '	</tr>';*/

            strVar += '</table>';

            strVar += '<p>**For any discrepancy in data, please reach out to business.ops@brillio.com or chetan.barot@brillio.com </p>';

            strVar += '<p>Thanks & Regards,</p>';
            strVar += '<p>Team IS</p>';

            strVar += '</body>';
            strVar += '</html>';

            if (send_mail_flag == 1) { //practice_mail
                var bcc = new Array();
                var ccValue = new Array();
               // bcc[0] = 'siva123.b@brillio.com';
               // bcc[1] = 'Chetan.barot@brillio.com';
               // bcc[2] = 'shamanth.k@brillio.com';
               // bcc[3] = 'prabhat.gupta@brillio.com';
				var records = new Array();
            //    records['entity'] = 140512;
            records['entity'] = 442;  /// Record holder IS systems
              
              //nlapiLogExecution('DEBUG', 'Cognetik VALUES', mailList['']);
              
              
            

            if(_logValidation(mailList[prent_practice_id])){
					
                bcc = mailList[prent_practice_id].filter(function(each){
                return each.receipient=='bcc';
                }).map(function(each){
                return each.email;
               })
                   
               }
               
               if(bcc.length == 0){
                   bcc = null;
               }

               

                if(_logValidation(mailList[prent_practice_id])){
					
                    ccValue = mailList[prent_practice_id].filter(function(each){
                    return each.receipient=='cc';
                    }).map(function(each){
                    return each.email;
                   })

                  // if(parseInt(prent_practice_id) == parseInt(191) || parseInt(prent_practice_id) == parseInt(571) || parseInt(prent_practice_id) == parseInt(455) || parseInt(prent_practice_id) == parseInt(490) || parseInt(prent_practice_id) == parseInt(424)){
                      
                       
                //   }
               //    else{
                     //if(_logValidation(onsite_prac_head_mail)){
                     //  ccValue.push(onsite_prac_head_mail)
                    // }
                    
                  // }
                       
                   }

                   if(ccValue.length==0){
                       ccValue = null;

                   }
               nlapiLogExecution('DEBUG', 'PARENT PRACTICE  VALUES', prent_practice_id );
               nlapiLogExecution('DEBUG', 'practice_mail', practice_mail );
               //nlapiLogExecution('DEBUG', 'totalRow_billed_count', totalRow_billed_count );
               //nlapiLogExecution('DEBUG', 'practice_arr_list[parent_index].parent_practice_name', practice_arr_list[parent_index].parent_practice_name );
               //nlapiLogExecution('DEBUG', 'ccValue', ccValue );
               nlapiLogExecution('DEBUG', 'bcc', bcc );
               
if (parseInt(prent_practice_id) == parseInt(455) || parseInt(prent_practice_id) == parseInt(490) || parseInt(prent_practice_id) == parseInt(424)) {
	}else{
nlapiSendEmail(442, 'sitaram.upadhya@brillio.com', 'Billed Count for today: ' + parseFloat(totalRow_billed_count).toFixed(2) + ' (' + practice_arr_list[parent_index].parent_practice_name + ') dt ' + today, strVar, null, null, null, excel_file_obj);
}
                   
               
                 
            }
        }
    }
}

function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function find_proj_practice(parctice_id, project_id, proj_data) {
    for (var kl = 0; kl < proj_data.length; kl++) {
        var practice_id_proj = proj_data[kl].practice_id;
        var project_id_pro_data = proj_data[kl].project_id;

        if (parseInt(practice_id_proj) == parseInt(parctice_id)) {
            if (parseInt(project_id_pro_data) == parseInt(project_id)) {
                return kl;
            }
        }
    }

    return Number(0.0);
}

function yieldScript(currentContext) {

    if (currentContext.getRemainingUsage() <= 2000) {
        nlapiLogExecution('AUDIT', 'API Limit Exceeded');
        var state = nlapiYieldScript();

        if (state.status == "FAILURE") {
            nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
                state.reason + ' / Size : ' + state.size);
            return false;
        } else if (state.status == "RESUME") {
            nlapiLogExecution('AUDIT', 'Script Resumed');
        }
    }
}

function generate_excel(strVar_excel) {
    var strVar1 = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
        '<head>' +
        '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>' +
        '<meta name=ProgId content=Excel.Sheet/>' +
        '<meta name=Generator content="Microsoft Excel 11"/>' +
        '<!--[if gte mso 9]><xml>' +
        '<x:excelworkbook>' +
        '<x:excelworksheets>' +
        '<x:excelworksheet=sheet1>' +
        '<x:name>** ESTIMATE FILE**</x:name>' +
        '<x:worksheetoptions>' +
        '<x:selected></x:selected>' +
        '<x:freezepanes></x:freezepanes>' +
        '<x:frozennosplit></x:frozennosplit>' +
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>' +
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>' +
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>' +
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>' +
        '<x:activepane>0</x:activepane>' + // 0
        '<x:panes>' +
        '<x:pane>' +
        '<x:number>3</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>1</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>2</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>0</x:number>' + //1
        '</x:pane>' +
        '</x:panes>' +
        '<x:protectcontents>False</x:protectcontents>' +
        '<x:protectobjects>False</x:protectobjects>' +
        '<x:protectscenarios>False</x:protectscenarios>' +
        '</x:worksheetoptions>' +
        '</x:excelworksheet>' +
        '</x:excelworksheets>' +
        '<x:protectstructure>False</x:protectstructure>' +
        '<x:protectwindows>False</x:protectwindows>' +
        '</x:excelworkbook>' +

        // '<style>'+

        //-------------------------------------
        '</x:excelworkbook>' +
        '</xml><![endif]-->' +
        '<style>' +
        'p.MsoFooter, li.MsoFooter, div.MsoFooter' +
        '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}' +
        '<style>' +

        '<!-- /* Style Definitions */' +

        //'@page Section1'+
        //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

        'div.Section1' +
        '{ page:Section1;}' +

        'table#hrdftrtbl' +
        '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->' +

        '</style>' +


        '</head>' +


        '<body>' +

        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'

    var strVar2 = strVar_excel;
    strVar1 = strVar1 + strVar2;
    var file = nlapiCreateFile('Resource Allocation Data.xls', 'XMLDOC', strVar1);
    return file;
}

function find_practice(parctice_id, proj_data) {
    for (var kl = 0; kl < proj_data.length; kl++) {
        var practice_id_proj = proj_data[kl].practice_id;
        //var project_id_pro_data = proj_data[kl].project_id;

        if (parseInt(practice_id_proj) == parseInt(parctice_id)) {
            return kl;
        }
    }

    return Number(0.0);
}



function getPracticeMailingList(){
var mailList = {};

var practiceSearch = nlapiSearchRecord("customrecord_dailyticker_mailing_data",null,
[
   ["custrecord_type_value","is","Practice"]
], 
[
   new nlobjSearchColumn("id",null,"GROUP").setSort(false), 
   new nlobjSearchColumn("custrecord_email_value",null,"GROUP"), 
   new nlobjSearchColumn("custrecord_receipient_name",null,"GROUP"), 
   new nlobjSearchColumn("custrecord_type_value",null,"GROUP"), 
   new nlobjSearchColumn("custrecord_practice_value",null,"GROUP"), 
   new nlobjSearchColumn("custrecord_region_value",null,"GROUP")
]
);

if (practiceSearch) {
  
  //nlapiLogExecution('debug', 'practice_test',JSON.stringify(practiceSearch));
      
     for(var i=0;i<practiceSearch.length;i++){
       
       //nlapiLogExecution('debug', 'i length', i);
       var emailId = practiceSearch[i].getValue("custrecord_email_value",null,"GROUP");
	   var receipient = practiceSearch[i].getValue("custrecord_receipient_name",null,"GROUP");
	   var type = practiceSearch[i].getValue("custrecord_type_value",null,"GROUP");
	   var practice = practiceSearch[i].getValue("custrecord_practice_value",null,"GROUP");
	   var region = practiceSearch[i].getValue("custrecord_region_value",null,"GROUP");
	   
	   if(!mailList[practice]){
          mailList[practice] = new Array();
		   mailList[practice] = [{
			   'email': practiceSearch[i].getValue("custrecord_email_value",null,"GROUP"),
			   'receipient' : practiceSearch[i].getValue("custrecord_receipient_name",null,"GROUP"),
	            'type' :practiceSearch[i].getValue("custrecord_type_value",null,"GROUP"),
	           'practice' : practiceSearch[i].getValue("custrecord_practice_value",null,"GROUP"),
	            'region' : practiceSearch[i].getValue("custrecord_region_value",null,"GROUP")
		   }]
	   } else {
		   mailList[practice].push({
			   'email': practiceSearch[i].getValue("custrecord_email_value",null,"GROUP"),
			   'receipient' : practiceSearch[i].getValue("custrecord_receipient_name",null,"GROUP"),
	            'type' :practiceSearch[i].getValue("custrecord_type_value",null,"GROUP"),
	           'practice' : practiceSearch[i].getValue("custrecord_practice_value",null,"GROUP"),
	            'region' : practiceSearch[i].getValue("custrecord_region_value",null,"GROUP")
			   
		   })
	   }
	   
	   
     }
    
                         
}
  

return mailList




}

function dateString(dateStr) {
    if(!(_logValidation(dateStr))){
      return dateStr;
    }
    var newDate = new Date(dateStr);
    var dateStrValue =  newDate.getDate()+'-'+(newDate.getMonth()+1)+'-'+newDate.getFullYear();
    return dateStrValue;

}