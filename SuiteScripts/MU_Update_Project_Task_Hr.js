/**
 * Update Hrs for the project task for Move projects
 * 
 * Version Date Author Remarks 1.00 21 Mar 2016 Nitish Mishra
 * 
 */

// var search = nlapiSearchRecord(null, 1293);
// for (var i = 0; i < search.length; i++) {
// massUpdate(null, search[i].getId());
// nlapiLogExecution('debug', 'done ' + i);
// }
function massUpdate(recType, recId) {

	try {
		var projectTaskRec = nlapiLoadRecord('projecttask', recId);

		for (var i = 1; i <= projectTaskRec.getLineItemCount('assignee'); i++) {

			projectTaskRec.selectLineItem('assignee', i);

			var employeeId = projectTaskRec.getCurrentLineItemValue('assignee',
			        'resource');
			var employeeActive = false;
			try {
				var details = nlapiLookupField('employee', employeeId,
				        'isinactive');
				employeeActive = details == 'F';
			} catch (e) {
			}

			if (employeeActive) {
				projectTaskRec.setCurrentLineItemValue('assignee',
				        'estimatedwork', 0.5);
				projectTaskRec.commitLineItem('assignee');
			}
		}

		var id = nlapiSubmitRecord(projectTaskRec);
		nlapiLogExecution('DEBUG', 'Project Task Updated', id);
	} catch (err) {
		nlapiLogExecution('ERROR', recId, err);
	}
}

function aa() {
	var file = nlapiLoadFile('149830');
	var value = file.getValue();
	var row = value.split('\r\n');

	for (var i = 1; i < row.length; i++) {

		var columns = row[i].split(',');
		var emp = columns[0];
		var project = columns[1];
		var role = columns[2];

		try {

			var search = nlapiSearchRecord('resourceallocation', null,
			        [
			                new nlobjSearchFilter('startdate', null,
			                        'notafter', 'today'),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        'today'),
			                new nlobjSearchFilter('resource', null, 'anyof',
			                        emp),
			                new nlobjSearchFilter('project', null, 'anyof',
			                        project) ]);

			if (search) {
				nlapiLogExecution('debug', emp + " " + project + ' found : '
				        + search[0].getId() + " role " + role);

				var record = nlapiLoadRecord('resourceallocation', search[0]
				        .getId());
				record.setFieldValue('custevent_ra_move_billing_role', role);
				nlapiSubmitRecord(record);

				nlapiLogExecution('debug', 'updated');
			}
		} catch (err) {
			nlapiLogExecution('error', emp + " " + project, err);
		}
	}

}
