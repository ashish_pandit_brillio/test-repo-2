// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES_JV_Restriction.js
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
    if(type=='view'||type=='edit')
    {
		var recordId=nlapiGetRecordId()
 nlapiLogExecution('DEBUG', 'Debug',  'recordId->'+recordId)
 if(recordId!=null)
{
		var recordType=nlapiGetRecordType()
		var userId=nlapiGetUser();
		var context = nlapiGetContext();
		
		var jvRec=nlapiLoadRecord(recordType,recordId)
		nlapiLogExecution('DEBUG', 'Debug',  'jvRec->'+jvRec)
		var subsidiary=jvRec.getFieldValue('subsidiary')
		var salary_upload_process_je=jvRec.getFieldValue('custbody_salary_upload_process_je')
		
		if(salary_upload_process_je!=''&&salary_upload_process_je!=null && context.getExecutionContext == 'userinterface')
		{
			var errorMessge="You are not have permission to view/edit this Journal Entry"
			if(subsidiary==2)//BLLC
			{
				if(userId!==1538&&userId!==1610&&userId!==7981&&userId!==2641&&userId!==9109&&userId!==9060&&userId!==5706&&userId!==9185&&userId!==41571&&userId!==1615)
				{
					throw errorMessge
				}
			}
			else if(subsidiary==3)//BTPL
			{
				if(userId!==1538&&userId!==1610&&userId!==7981&&userId!==2641&&userId!==9109&&userId!==9060&&userId!==5706&&userId!==9185&&userId!==41571&&userId!==1615&&userId!==10720)
				{
					throw errorMessge
				}
			}
		}
}
    }

	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
