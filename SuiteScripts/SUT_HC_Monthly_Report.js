/**
 * CSV Download For Employee TIme Sheet
 * 
 * Version Date Author Remarks 1.00
 * 
 * 12 Aug 2016 Deepak MS
 * 
 */
var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
function suitelet(request, response) {
	try {
	
	
		if(request.getMethod() == 'GET')
		{
			//Create the form and add fields to it
			var form = nlapiCreateForm("HC Report");
			
			form.addFieldGroup('custpage_grp_0', 'Select Options');	
			var startDate = form.addField('custpage_startdate', 'date', 'Start Date', null,
	        'custpage_grp_0')
	        startDate.setBreakType('startcol');	
			startDate.setMandatory(true);
			
			var endDate = form.addField('custpage_enddate',
	        'date', 'End Date', null, 'custpage_grp_0').setBreakType('startcol');
			endDate.setMandatory(true);	
			 form.addSubmitButton('Submit');
			response.writePage(form);	
		}
		else{
			//Get Parameters
			var st_date = request.getParameter('custpage_startdate');
			var d_st_date = nlapiStringToDate(st_date);
			var end_date = request.getParameter('custpage_enddate');
			var d_end_date = nlapiStringToDate(end_date);
			var IMG_currentDate = nlapiDateToString(new Date());
			var resource_List  = getResourceAllocations(d_st_date, d_end_date);
	
	
		var csvText = generateTableCsv(resource_List);
		var fileName = 'HC_Report' +' '+IMG_currentDate
		        + '.csv';
		var file = nlapiCreateFile(fileName, 'CSV', csvText);
		response.setContentType('CSV', fileName);
		response.write(file.getValue());	
	}	
	}
	
	
catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}	
}

//Get Active Resource Allocations
function getResourceAllocations(d_start_date, d_end_date)
{

	nlapiLogExecution('AUDIT', 'getResourceAllocation Parameters', d_start_date
	        + ' - ' + d_end_date );

	

	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();

	// Get Resource allocations for this month
	var filters = new Array();
	//
	filters[0]  = new nlobjSearchFilter('formuladate', null, 'notafter',
			d_end_date);
	filters[0].setFormula('{startdate}');

	filters[1] = new nlobjSearchFilter('formuladate', null, 'notbefore',
			d_start_date);
	filters[1].setFormula('{enddate}');
	
//	filters[2] = new nlobjSearchFilter('resource', null, 'anyof',
//			41343);
	

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('percentoftime', null, 'avg');
	columns[1] = new nlobjSearchColumn('startdate', null, 'group');
	columns[2] = new nlobjSearchColumn('enddate', null, 'group');
	columns[3] = new nlobjSearchColumn('resource', null, 'group');
	columns[4] = new nlobjSearchColumn('project', null, 'group');
	columns[5] = new nlobjSearchColumn('custevent3', null, 'avg');
	columns[6] = new nlobjSearchColumn('custevent_monthly_rate', null, 'avg');
	columns[7] = new nlobjSearchColumn('custeventrbillable', null, 'group');
	columns[8] = new nlobjSearchColumn('departmentnohierarchy', 'employee',
    'group');
	columns[9] = new nlobjSearchColumn('subsidiary', 'employee', 'group');
	columns[10] = new nlobjSearchColumn('customer', 'job', 'group');
	columns[11] = new nlobjSearchColumn('custentity_project_currency', 'job',
    'group');
	columns[12] = new nlobjSearchColumn('custentity_vertical', 'job', 'group');
	columns[13] = new nlobjSearchColumn('custentity_t_and_m_monthly', 'job',
    'group');
	columns[14] = new nlobjSearchColumn('custevent_monthly_rate', null, 'group');
	
	columns[15] = new nlobjSearchColumn('formulatext', null, 'group');
	columns[15]
	        .setFormula('CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END');
	columns[16] = new nlobjSearchColumn('custentity_endcustomer', 'job',
	        'group');
	columns[17] = new nlobjSearchColumn('internalid', null, 'count');
	columns[18] = new nlobjSearchColumn('custentity_project_services', 'job',
    'group');
	columns[19] = new nlobjSearchColumn('jobbillingtype', 'job',
    'group');
	columns[20] = new nlobjSearchColumn('custentity_fp_rev_rec_type', 'job',
    'group');
	
	columns[21] = new nlobjSearchColumn('entityid', 'customer',
    'group');
	columns[22] = new nlobjSearchColumn('altname', 'customer',
    'group');
	columns[23] = new nlobjSearchColumn('custentity_fusion_empid', 'employee', 'group');
	columns[24] = new nlobjSearchColumn('custevent4', null, 'group');
	
	columns[25] = new nlobjSearchColumn('custentity_persontype', 'employee', 'group');
	columns[26] = new nlobjSearchColumn('type', 'employee', 'group');
	columns[27] = new nlobjSearchColumn('custentity_project_allocation_category', 'job', 'group');
	columns[28] = new nlobjSearchColumn('custentity_region', 'job', 'group');
	columns[29] = new nlobjSearchColumn('jobname', 'job', 'group');
	columns[30] = new nlobjSearchColumn('entityid', 'job', 'group');
	columns[31] = new nlobjSearchColumn('firstname', 'employee', 'group');
	columns[32] = new nlobjSearchColumn('middlename', 'employee', 'group');
	columns[33] = new nlobjSearchColumn('lastname', 'employee', 'group');
	try {
		var search_results = searchRecord('resourceallocation', null, filters,
		        columns);
	} catch (e) {
		nlapiLogExecution('ERROR', 'Error', e.message);
	}

	if (search_results != null && search_results.length > 0) {
		for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
			var emp_full_name ='';
			var i_project_id = search_results[i_search_indx]
			        .getValue(columns[30]);
			var s_project_name = search_results[i_search_indx]
			        .getValue(columns[29]);
			var is_resource_billable = search_results[i_search_indx]
			        .getValue(columns[7]);
			var stRate = search_results[i_search_indx].getValue(columns[5]);
			stRate = stRate ? parseFloat(stRate) : 0;
			var i_employee = search_results[i_search_indx].getValue(columns[23]);
			var s_employee_name = search_results[i_search_indx]
			        .getText(columns[3]);
					
					var emp_frst_name = search_results[i_search_indx]
			        .getValue(columns[31]);
					var emp_middl_name = search_results[i_search_indx]
			        .getValue(columns[32]);
					var emp_lst_name = search_results[i_search_indx]
			        .getValue(columns[33]);
					
					if(emp_frst_name && emp_frst_name!='- None -')
						emp_full_name = emp_frst_name;
											
					if(emp_middl_name && emp_middl_name!='- None -')
						emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
					if(emp_lst_name && emp_lst_name!='- None -')
						emp_full_name = emp_full_name + ' ' + emp_lst_name;
					
			var i_percent_of_time = search_results[i_search_indx]
			        .getValue(columns[0]);
			var i_department_id = search_results[i_search_indx]
			        .getValue(columns[8]);
			var s_department = search_results[i_search_indx]
			        .getValue(columns[8]);
			var i_subsidiary = search_results[i_search_indx]
			        .getValue(columns[9]);
			var s_personType = search_results[i_search_indx]
	        .getText(columns[25]);
			var s_employee_type = search_results[i_search_indx]
	        .getValue(columns[26]);
			
			var s_project_category = search_results[i_search_indx]
	        .getText(columns[27]);
			var s_pro_region = search_results[i_search_indx]
	        .getText(columns[28]);
			
		//	var i_working_days = search_results[i_search_indx].getValue(columns[8]);
			var allocationStartDate = search_results[i_search_indx].getValue(columns[1]);
			var allocationEndDate = search_results[i_search_indx].getValue(columns[2]);
			var s_project_services = search_results[i_search_indx]
	        .getText(columns[18]);
			var s_billingType = search_results[i_search_indx]
	        .getText(columns[19]);
			if(s_billingType != 'Time and Materials'){
				s_billingType = search_results[i_search_indx]
		        .getText(columns[20]);
			}
			
			//var i_working_days = parseInt(calcBusinessDays(d_start_date, d_end_date));;
			
			var i_customer_id = search_results[i_search_indx]
			        .getValue(columns[10]);
			var s_customer = search_results[i_search_indx].getText(columns[10]);
			var i_count = search_results[i_search_indx].getValue(columns[17]);
			var i_currency = search_results[i_search_indx].getText(columns[11]);
			var i_vertical_id = search_results[i_search_indx]
			        .getValue(columns[12]);
			var s_vertical = search_results[i_search_indx].getText(columns[12]);
			var is_T_and_M_monthly = search_results[i_search_indx]
			        .getValue(columns[13]);
			var f_monthly_rate = search_results[i_search_indx]
			        .getValue(columns[14]);
			f_monthly_rate = f_monthly_rate ? parseFloat(f_monthly_rate) : 0;
			var s_parent_department = search_results[i_search_indx]
			        .getValue(columns[15]);
			var s_end_customer = search_results[i_search_indx]
			        .getText(columns[16]);
			
			var s_cust_id = search_results[i_search_indx]
	        .getValue(columns[21]);
			var s_cust_name = search_results[i_search_indx]
	        .getValue(columns[22]);
			
			var s_offsite_onsite = search_results[i_search_indx]
	        .getText(columns[24]);
			
			var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
			var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
			var d_provisionStartDate = d_start_date;
			var d_provisionEndDate = d_end_date;
			var provisionStartDate = nlapiDateToString(d_start_date);
			var provisionEndDate = nlapiDateToString(d_end_date);

			var provisionMonthStartDate = nlapiStringToDate((d_provisionStartDate
			        .getMonth() + 1)
			        + '/1/' + d_provisionStartDate.getFullYear());
			var nextProvisionMonthStartDate = nlapiAddMonths(
			        provisionMonthStartDate, 1);
			var provisionMonthEndDate = nlapiAddDays(
			        nextProvisionMonthStartDate, -1);
			// nlapiLogExecution('debug', 'provisionMonthStartDate',
			// provisionMonthStartDate);
			// nlapiLogExecution('debug', 'provisionMonthEndDate',
			// provisionMonthEndDate);

			var startDate = d_allocationStartDate > d_provisionStartDate ? allocationStartDate
			        : provisionStartDate;
			var endDate = d_allocationEndDate < d_provisionEndDate ? allocationEndDate
			        : provisionEndDate;
			
			
			// days calculation
		//	var workingDays = parseFloat(getWorkingDays(
		//			startDate, endDate));
			//var holidayDetailsInMonth = get_holidays(startDate,
			//		endDate, i_employee, i_project_id, i_customer_id);
			//var holidayCountInMonth = holidayDetailsInMonth.length;

			//Calculate the percent allocation
			
			var timeDiff_Allocation = Math.abs((nlapiStringToDate(endDate).getTime() - nlapiStringToDate(startDate).getTime()) + 1);
			var diffDays_Allocation = Math.ceil(timeDiff_Allocation / (1000 * 3600 * 24)); 
			
			var timeDiff_provision = Math.abs((d_end_date.getTime() - d_start_date.getTime()) + 1);
			var diffDays_provision = Math.ceil(timeDiff_provision / (1000 * 3600 * 24)); 
			
			var percent_allocation = parseFloat(diffDays_Allocation) / parseFloat(diffDays_provision);
			var allocation_percent = ((parseFloat(percent_allocation) * parseFloat(i_percent_of_time))/100).toFixed(5);
			
			a_resource_allocations[i_search_indx] = {
			    project_id : i_project_id,
			    'project_name' : s_project_name,
			    is_billable : is_resource_billable,
			    st_rate : stRate,
			    'employee' : i_employee,
			    'employee_name' : emp_full_name,
			    'percentoftime' : allocation_percent,
			    'department_id' : i_department_id,
			    'department' : s_department,
			  //  'holidays' : holidayCountInMonth,
			  //  'workingdays' : workingDays,
			    'customer_id' : s_cust_id,
			    'customer' : s_cust_name,
			    'count' : i_count,
			    'currency' : i_currency,
			    'vertical_id' : i_vertical_id,
			    'vertical' : s_vertical,
			    'monthly_billing' : is_T_and_M_monthly,
			    'monthly_rate' : f_monthly_rate,
			    'parent_department' : s_parent_department,
			    'billingType': s_billingType,
			    'project_service': s_project_services, //s_offsite_onsite
			   'onsite_offsite':s_offsite_onsite,
			   's_personType':s_personType,
			   's_employee_type':s_employee_type,
			   's_project_category':s_project_category,
			   's_pro_region': s_pro_region
			   
			};
		}
	}  else {
		a_resource_allocations = null;
	}

	return a_resource_allocations;
}

function generateTableCsv(res_list){
	try{
var html = "";
		
		
		html += "Financial Row,";
		html += "Type,";
		html += "Date,";
		html += "Transaction Number,";
		html += "Name,";
		html += "Allocation Percent,";
		html += "Billing From,";
		html += "Billing To,";
		html += "Customer ID,";
		html += "Cust Name,";
		html += "Project ID,";
		html += "Proj Name,";
		html += "Employee ID,";
		html += "Emp Name,";
		html += "Practice,";
		html += "Practice: Parent Practice,";
		html += "Region Setup,";
		html += "Location,";
		html += "Memo,";
		html += "Memo Line,";
		html += "Description,";
		html += "Hour,";
		html += "Bill Rate,";  //monthly_rate
		html += "Employee type,";
		html += "Person type,";
		html += "Onsite/Offsite,";
		html += "Billing type,";
		html += "Proj Category,";
		html += "Project Services,";
		html += "\r\n";
		
		
		// table header
		for(var j=0;j<res_list.length;j++){
			var allocation_Data = res_list[j];
					html += ' '; //Financial Row
					html += ",";
					html += ' '; //Type
					html += ",";
					html += ''; //Date
					html += ",";
					html += ' '; //Transaction Number
					html += ",";
					html += removeCommas(allocation_Data.customer_id) + '-'+ removeCommas(allocation_Data.customer); //Customer Name
					html += ",";
					html += removeCommas(allocation_Data.percentoftime); //Amount
					html += ",";
					html += " "; //Billing From
					html += ",";
					html += " "; //Billing To
					html += ",";
					html += removeCommas(allocation_Data.customer_id); //Customer ID
					html += ",";
					html += removeCommas(allocation_Data.customer); //Cust Name
					html += ",";
					html += removeCommas(allocation_Data.project_id); //Project ID
					html += ",";
					html += removeCommas(allocation_Data.project_name); //Proj Name
					html += ",";
					html += removeCommas(allocation_Data.employee); //Employee ID
					html += ",";
					html += removeCommas(allocation_Data.employee_name); //Emp Name
					html += ",";
					html += removeCommas(allocation_Data.department); //Practice
					html += ",";
					html += removeCommas(allocation_Data.parent_department); //Practice: Parent Practice
					html += ",";
					html += removeCommas(allocation_Data.s_pro_region); //Region Setup
					html += ",";
					html += " "; //Location
					html += ",";
					html += " "; //Memo
					html += ",";
					html += " "; //Memo in Line
					html += ",";
					html += " "; //Description
					html += ",";
					html += " "; //Hour
					html += ",";
					html += allocation_Data.st_rate; //Billable Rate
					html += ",";
					html += allocation_Data.s_employee_type; //Employee type
					html += ",";
					html += allocation_Data.s_personType; //Person type
					html += ",";
					html += allocation_Data.onsite_offsite; //Onsite/Offsitep
					html += ",";
					html += allocation_Data.billingType; //Billing type
					html += ",";
					html += allocation_Data.s_project_category; //Proj Category
					html += ",";
					html += allocation_Data.project_service; //Project Services
					
					html += "\r\n";
			}
		
		
		
		return html;
		
	}
	catch(e){
		nlapiLogExecution('DEBUG','Process Error',e);	
	}
	
}
function removeCommas(str) {
	if(_logValidation(str)){
    while (str.search(",") >= 0) {
        str = (str + "").replace(',', '');
    }
    return str;
	}
};

function _logValidation(value) 
{
 if(value != null && value != '- None -' && value.toString() != "- None -" && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function searchProjectObj(proID){
	try{
		var project_Concat;
		var filter = Array();
		filter.push(new nlobjSearchFilter('internalid', null, 'is', proID));
		
		var cols = Array();
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('companyname','customer'));
		cols.push(new nlobjSearchColumn('companyname'));
		
		var searchRes = nlapiSearchRecord('job',null,filter,cols);
		
		if(searchRes){
			var projectID = searchRes[0].getValue('entityid');
			var customer_Name = searchRes[0].getValue('companyname','customer');
			var projectName = searchRes[0].getValue('companyname');
			
			 project_Concat = projectID+' '+projectName;
		}
		return project_Concat;
	}
	catch(e){
		nlapiLogExecution('debug','Project Search Error',e);
	}
}

function searchProjects(proj_ID,d_StartDate){
try{
var d_st_date = nlapiStringToDate(d_StartDate);
var filters = Array();
filters.push(new nlobjSearchFilter('internalid', null, 'is', proj_ID));
filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
filters.push(new nlobjSearchFilter('startdate','resourceallocation', 'onorbefore',d_st_date));
filters.push(new nlobjSearchFilter('enddate','resourceallocation', 'onorafter',d_st_date));

var cols = Array();
cols.push(new nlobjSearchColumn('resource','resourceallocation')); //parent

var projectSearchObj = nlapiSearchRecord('job',null,filters,cols);
var dataRows =[];
var resourceList = {};
if(projectSearchObj){
for(var i=0;i<projectSearchObj.length;i++){
resourceList = {
Resource: projectSearchObj[i].getValue('resource','resourceallocation')
};
dataRows.push(resourceList);
}
}

return dataRows;
}
catch(e){
nlapiLogExecution('DEBUG',' Project Search Error',e);
}

}


