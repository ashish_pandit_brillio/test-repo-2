/**
 * Check if the employee has entered any timesheet within the allocation period.
 * Show a message for the same.
 * 
 * Version Date Author Remarks 1.00 12 May 2016 Nitish Mishra
 
	Date		Requested By 		Done By 		Description
	06/10/2016	Jai					Jayesh			This change was done to avoid saving of resource allocation if timesheets already exist for employee
 *  11/03/2020	Deepak				Praveena Madem	Search record type id is changed due to migration
 *
 */

function clientPageInitCheckTimesheet() {
	try {
		var main_div = document.getElementsByClassName("uir-page-title")[0];
		var newDiv = document.createElement("div");
		newDiv.innerHTML = "<span id='spanUserMsg' style='color:red;font-size:15px;position:absolute;right:0;'></span>";
		main_div.appendChild(newDiv);

		var startDate = nlapiGetFieldValue('startdate');
		var endDate = nlapiGetFieldValue('enddate');
		var employee = nlapiGetFieldValue('allocationresource');

		if (employee && startDate && endDate) {

			if (checkIfTimesheetExists(employee, startDate, endDate)) {
				document.getElementById('spanUserMsg').innerHTML = "Employee has already filled timesheets for this period";
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'clientPageInitCheckTimesheet', err);
		alert(err.message);
	}
}

function clientFieldChangeCheckTimesheet(type, name, linenum) {
	try {
		if (name == 'startdate' || name == 'enddate'
		        || name == 'allocationresource') {
			var startDate = nlapiGetFieldValue('startdate');
			var endDate = nlapiGetFieldValue('enddate');
			var employee = nlapiGetFieldValue('allocationresource');

			if (employee && startDate && endDate) {

				if (checkIfTimesheetExists(employee, startDate, endDate)) {
					document.getElementById('spanUserMsg').innerHTML = "Employee has already filled timesheets for this period";
				} else {
					document.getElementById('spanUserMsg').innerHTML = "";
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'clientFieldChangeCheckTimesheet', err);
		//alert(err.message);
	}
}

function clientSaveRecordCheckTimesheet() {
	try {
		var startDate = nlapiGetFieldValue('startdate');
		var endDate = nlapiGetFieldValue('enddate');
		var employee = nlapiGetFieldValue('allocationresource');
		var project = nlapiGetFieldValue('project');
		var employee_Lookup = nlapiLookupField('employee',employee,['hiredate']);
		var emp_hire_date = employee_Lookup.hiredate;
		emp_hire_date = nlapiStringToDate(emp_hire_date);
		var resAlloc_stDate = nlapiStringToDate(startDate);

		if (employee && startDate && endDate) {

			if (checkIfTimesheetExists_stop_resource_edit(employee, startDate, endDate,project)) {
				
				
				
				alert("Employee has already filled timesheets for this period");
				return false; // CR dated 06/10/2016 as per requested by Jai in meeting with all finance guys. 
			}
		}
		if(employee && resAlloc_stDate && emp_hire_date){
			if(resAlloc_stDate < emp_hire_date){
				alert('You cannot create allocation prior to hire date');
				return false; //CR Dated 24/10/2016 , restric user creating allocation prior to employee hire date
			}
		}
		return true;
	} catch (err) {
		nlapiLogExecution('ERROR', 'clientSaveRecordCheckTimesheet', err);
		alert(err.message);
	}
}

function checkIfTimesheetExists_stop_resource_edit(employee, startDate, endDate,project) {
	try {
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [//search record changed after migration. logic added by praveena on 11-03-2020  
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'after', endDate), 
				//new nlobjSearchFilter('internalid','customerproject','anyof',project)]);//Commented by praveena
				new nlobjSearchFilter('internalid','customer','anyof',project)]);//added by praveena

		return timeEntrySearch != null;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkIfTimesheetExists', err);
		throw err;
	}
}

function checkIfTimesheetExists(employee, startDate, endDate) {
	try {
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [//search record changed after migration. logic added by praveena on 11-03-2020  
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', startDate,
						endDate ) ]);

		return timeEntrySearch != null;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkIfTimesheetExists', err);
		throw err;
	}
}