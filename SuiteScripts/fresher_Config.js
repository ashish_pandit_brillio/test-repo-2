var fresherConfig  = {
  "fresherCode": {
    "FRE": "Fresher-Premiere-Special",
    "FRE1": "Fresher-Premiere-Regular",
    "FC" : "Fresher – Campus",
    "FOC": "Fresher – Off Campus",
    "CAM": "MBA Tier2 Campus",    
    "cmps":	"Campus",
    "EMP_CON":	"Employment conversion",
    "FRE2":	"Fresher",
    "LAT":	"Lateral",
    "M_A":	"M&A",
    "26":	"MA - Cognetik",
    "25":	"MA - Comity",
    "MA- ISL":	"MA- ISL",
    "MA- Marketelligent":	"MA- Marketelligent",
    "27":	"Rehire",
    "RE":	"Retainer",
	"AH" : "Adjacent Hire",
	"FRE234" : "Fresher",
	"FRE2" : "Fresher-Special",
	"NA" : "NA"
  }

}



var visaConfig = {
  
  "visaCode":{
    "WP_IN": "21",
    "C" : "21"
  }
  
}

var countryConfig = {
  
  "countryCode":{
    "US": "1",
    "UK": "2",
    "CN": "3",
    "IN": "4"
  }
  
}