/**
 * Search All the under allocated and unallocated resources and generate a csv
 * file and send as an email
 * 
 * Version Date Author Remarks 1.00 Mar 25 2015 Nitish Mishra
 * 
 */

function suitelet(request, response) {

	try {
		nlapiLogExecution('debug', 'Started');

		// get under allocated allocations
		var underAllocations = getUnderAllocatedEmployeeDetails();
		nlapiLogExecution('debug', 'Under Allocation Net Count', underAllocations.length);

		// get unallocated allocations
		var unallocated = [];// getUnallocatedEmployee();
		nlapiLogExecution('debug', 'Unallocation Net Count', unallocated.length);

		// merge the two results
		var resultantList = _.union(underAllocations, unallocated);
		nlapiLogExecution('debug', 'Resultant Net Count', resultantList.length);

		// generate excel files
		var file = generateReport(resultantList);
		response.setContentType('CSV', 'sample.csv');
		response.write(file.getValue());

		// send emails
		if (isNotEmpty(file)) {
			nlapiSendEmail(442, 'nitish.mishra@brillio.com', 'Resource Under Allocation Search',
					'Please find the underallocated resources in the attachment', null, null, file);

			nlapiLogExecution('debug', 'File send');
		}
		nlapiLogExecution('debug', 'Finished');
	}
	catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		// throw err;
	}
}

/**
 * @param {String}
 *        type Context Types: scheduled, ondemand, userinterface, aborted,
 *        skipped
 * @returns {Void}
 */
function scheduled(type) {

	try {
		nlapiLogExecution('debug', 'Started');

		// call the suitelet to generate and send the file
		var url =
				nlapiResolveURL('SUITELET', 'customscript_sut_underallocation_report',
						'customdeploy_sut_underallocation_report');
		nlapiRequestURL('https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=505&deploy=1');

		nlapiLogExecution('debug', 'Finished');
	}
	catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
		// throw err;
	}
}

function generateReport(resultantList) {

	try {

		nlapiLogExecution('debug', 'resultantList', JSON.stringify(resultantList));

		if (isArrayNotEmpty(resultantList)) {
			var csvText = 'Resource,Total Allocation %,Project,Customer,Percent Allocation\n';
			var index = 0;

			resultantList.forEach(function(employee) {

				index = 0;
				csvText += employee.Resource + ",";
				csvText += employee.TotalAllocation + ",";

				employee.Allocations.forEach(function(allocation) {

					// same employee, add some blank cells
					if (index > 0) {
						csvText += ',,';
					}

					index++;
					csvText += allocation.Project + ",";
					csvText += allocation.Customer + ",";
					csvText += allocation.Percent + "\n";
				});
			});

			var Datetime = new Date();
			var CSVName = 'Under Allocation Report' + " - " + Datetime + '.csv';
			var file = nlapiCreateFile(CSVName, 'CSV', csvText);
			return file;
		}
		else {
			return null;
		}
	}
	catch (err) {
		nlapiLogExecution('error', 'generateReport', err);
		throw err;
	}
}

/*
 * Get under allocated employee details
 */
function getUnderAllocatedEmployeeDetails() {

	try {
		var percent = null;
		var underAllocatedEmployeeList = [];
		var allocationDetails = [];

		// get all under allocated employees
		var underAllocationSearch =
				searchRecord('resourceallocation', null, [
					new nlobjSearchFilter('custentity_implementationteam', 'employee', 'is', 'F'),
					new nlobjSearchFilter('custentity_employee_inactive', 'employee', 'is', 'F'),
					new nlobjSearchFilter('enddate', null, 'onorafter', 'today'),
					new nlobjSearchFilter('startdate', null, 'onorbefore', 'today'),
					new nlobjSearchFilter('percentoftime', null, 'lessthan', 100)
				], [
					new nlobjSearchColumn('resource', null, 'group'),
					new nlobjSearchColumn('percentoftime', null, 'sum')
				]);

		if (isArrayNotEmpty(underAllocationSearch)) {
			nlapiLogExecution('debug', 'Under Allocation Search', underAllocationSearch.length);

			underAllocationSearch
					.forEach(function(allocation) {

						percent = allocation.getValue('percentoftime', null, 'sum').split('%')[0];
						percent = Math.floor(percent);

						// if the percent allocation is less than 100 , add the
						// employee to
						// the list
						if (percent < 100) {
							underAllocatedEmployeeList.push(allocation.getValue('resource', null,
									'group'));
							allocationDetails
									.push({
										Resource : allocation.getText('resource', null, 'group'),
										TotalAllocation : allocation.getValue('percentoftime',
												null, 'sum'),
										Allocations : []
									});
						}
					});
		}

		// get under allocated employees details


		if (isArrayNotEmpty(underAllocatedEmployeeList)) {
			underAllocationSearch =
					searchRecord('resourceallocation', null,
							[
								new nlobjSearchFilter('custentity_implementationteam', 'employee',
										'is', 'F'),
								new nlobjSearchFilter('custentity_employee_inactive', 'employee',
										'is', 'F'),
								new nlobjSearchFilter('enddate', null, 'onorafter', 'today'),
								new nlobjSearchFilter('startdate', null, 'onorbefore', 'today'),
								new nlobjSearchFilter('percentoftime', null, 'lessthan', 100),
								new nlobjSearchFilter('resource', null, 'anyof',
										underAllocatedEmployeeList),
							], [
								new nlobjSearchColumn('resource', null, 'group').setSort(),
								new nlobjSearchColumn('company', null, 'group'),
								new nlobjSearchColumn('customer', null, 'group'),
								new nlobjSearchColumn('percentoftime', null, 'sum')
							]);

			if (isArrayNotEmpty(underAllocationSearch)) {

				var previousResourceId = null;
				var index = -1;
				underAllocationSearch.forEach(function(allocation) {

					var resourceId = allocation.getValue('resource', null, 'group');

					// check if the employee has changed
					if (previousResourceId != resourceId) {
						previousResourceId = resourceId;

						// get the index of the resource
						index = -1;
						for (var i = 0; i < underAllocatedEmployeeList.length; i++) {

							if (underAllocatedEmployeeList[i] == resourceId) {
								index = i;
								break;
							}
						}
					}

					if (index != -1) {
						allocationDetails[index].Allocations.push({
							Customer : allocation.getText('customer', null, 'group'),
							Project : allocation.getText('company', null, 'group'),
							Percent : allocation.getValue('percentoftime', null, 'sum')
						});
					}
					else {
						throw "New employee in list";
					}
				});
			}
		}

		return allocationDetails;
	}
	catch (err) {
		nlapiLogExecution('error', 'getUnderAllocatedEmployeeDetails', err);
		throw err;
	}
}

function getAllAllocatedEmployees() {

	try {
		var employeeList = [];

		searchRecord(
				'resourceallocation',
				null,
				[
					new nlobjSearchFilter('custentity_implementationteam', 'employee', 'is', 'F'),
					new nlobjSearchFilter('custentity_employee_inactive', 'employee', 'is', 'F'),
					new nlobjSearchFilter('enddate', null, 'onorafter', 'today'),
					new nlobjSearchFilter('startdate', null, 'onorbefore', 'today')
				], [
					new nlobjSearchColumn('resource', null, 'group')
				]).forEach(function(allocation) {

			employeeList.push(allocation.getValue('resource', null, 'group'));
		});

		return employeeList;
	}
	catch (err) {
		nlapiLogExecution('error', 'getAllAllocatedEmployees', err);
		throw err;
	}
}

function getUnallocatedEmployee() {

	try {
		var allocatedEmployeeList = getAllAllocatedEmployees();
		nlapiLogExecution('debug', 'Allocated Employees Count', allocatedEmployeeList.length);

		var allocationDetails = [];

		searchRecord(
				'employee',
				null,
				[
					new nlobjSearchFilter('custentity_implementationteam', null, 'is', 'F'),
					new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
					new nlobjSearchFilter('internalid', null, 'noneof', allocatedEmployeeList)
				], [
					new nlobjSearchColumn('entityid')
				]).forEach(function(employee) {

			allocationDetails.push({
				Resource : employee.getValue('entityid'),
				Customer : '',
				Project : 'Not Allocated',
				Percent : 0
			});
		});

		return allocationDetails;
	}
	catch (err) {
		nlapiLogExecution('error', 'getUnallocatedEmployee', err);
		throw err;
	}
}