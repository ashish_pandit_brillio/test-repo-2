							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_CancelFRFonOppClosed.js
	Author      : 
	Date        : 16 Aug 2018
    Description : User Event to Cancel FRF if Opportunity Closed Lost/ Closed Shelved and remove softlock if any.    


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSendEmailCancellation(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var recType = nlapiGetRecordType();
			/**************** Cancel FRF if Opportunity Stage Closed Lost/ Closede Shelved **************************/
			if(recType == 'customrecord_sfdc_opportunity_record')
			{
				var oldRecord = nlapiGetOldRecord();
				
				var i_OldStage = oldRecord.getFieldValue('custrecord_stage_sfdc');
				nlapiLogExecution('Debug','i_OldStage ',i_OldStage);
				
				var recId = nlapiGetRecordId();
				
				var recordObject = nlapiLoadRecord(recType,recId);
				var i_NewStage = recordObject.getFieldValue('custrecord_stage_sfdc');
				nlapiLogExecution('Debug','i_NewStage ',i_NewStage);
				
				var i_project = recordObject.getFieldValue('custrecord_project_internal_id_sfdc');
				var i_account = recordObject.getFieldText('custrecord_customer_internal_id_sfdc');
				var s_project = recordObject.getFieldText('custrecord_project_internal_id_sfdc');
				var i_opp = recordObject.getFieldValue('custrecord_opportunity_name_sfdc');
				
				if((i_OldStage != 8 || i_OldStage != 9) && (i_NewStage == 8 || i_NewStage == 9)) // Closed Lost - 8 Closed Shelved - 9
				{
					 var params = new Array();
					 params['custscript_opportunity'] = recId;
					 var status = nlapiScheduleScript('customscript_sch_cancelfrfforclosedopp', null, params);
					 nlapiLogExecution('debug','status ',status);
				}
			}
			/**************** Cancel FRF if Project Closed **************************/
			if(recType == 'job')
			{
				var oldRecord = nlapiGetOldRecord();
				
				var i_OldStatus = oldRecord.getFieldValue('entitystatus');
				nlapiLogExecution('Debug','i_OldStage ',i_OldStatus);
				
				var recId = nlapiGetRecordId();
				var recType = nlapiGetRecordType();
				var recordObject = nlapiLoadRecord(recType,recId);
				
				var i_NewStatus = recordObject.getFieldValue('entitystatus');
				nlapiLogExecution('Debug','i_NewStatus ',i_NewStatus);
				if(i_OldStatus != 1 && i_NewStatus ==1) // Closed Lost - 8 Closed Shelved - 9
				{
					 var params = new Array();
					 params['custscript_projectid'] = recId;
					 var status = nlapiScheduleScript('customscript_sch_cancelfrfforclosedopp', null, params);
				}
			}
			/**************** Send Notification for FRF Cancellation  **************************/
			if(recType == 'customrecord_frf_details')
			{
				var oldRecord = nlapiGetOldRecord();
				
				var b_OldCancelFRF = oldRecord.getFieldValue('custrecord_frf_details_status');
				
				var recId = nlapiGetRecordId();
				var recType = nlapiGetRecordType();
				var recObj = nlapiLoadRecord(recType,recId);
				var b_NewCancelFRF = recObj.getFieldValue('custrecord_frf_details_status');
				nlapiLogExecution('Debug','b_OldCancelFRF ',b_OldCancelFRF);
				nlapiLogExecution('Debug','b_NewCancelFRF ',b_NewCancelFRF);
				
				var s_oppName = "";
				var s_cancelledById = "";
				var finalProjectOpp = "";
				if (b_OldCancelFRF == 'F' && b_NewCancelFRF == 'T') 
				{
					var n_oppId = recObj.getFieldValue("custrecord_frf_details_opp_id");
					nlapiLogExecution("DEBUG", "n_oppId : ", n_oppId);
					if (n_oppId) 
					{
						s_oppName = nlapiLookupField("customrecord_sfdc_opportunity_record", n_oppId, "custrecord_opportunity_name_sfdc");
					}
					var recipient = recObj.getFieldValue("custrecord_frf_details_created_by");
					nlapiLogExecution("DEBUG", "recipient : ", recipient);
					var s_recipient = nlapiLookupField("employee", recipient, "firstname");
					var frfNumber = recObj.getFieldValue("custrecord_frf_details_frf_number");
					var subject = frfNumber + ": FUEL Notification: FRF Cancelled";
					var s_project = recObj.getFieldText("custrecord_frf_details_project");
					var s_account = recObj.getFieldText("custrecord_frf_details_account");
					var s_reason = recObj.getFieldText("custrecord_frf_details_cancellation_reas");
					var cancelledById = recObj.getFieldValue("custrecord_frf_details_rrf_cancelled_by");
					nlapiLogExecution("DEBUG", "cancelledById : ", cancelledById);
					if(s_oppName){
						finalProjectOpp = s_oppName; 
					}else{
						finalProjectOpp = s_project;
					}
					if (cancelledById) {
						s_cancelledById = nlapiLookupField("employee", cancelledById, "firstname");
					}
					var body = "Dear " + s_recipient + " \n" + s_cancelledById + " has cancelled the FRF #" + frfNumber + " created for Opportunity/Project " + finalProjectOpp + "\n and Account " + s_account + "\n Cancellation Reason: "+s_reason+"\n\n\n"
					body += "Brillio-FUEL \n"
					body += "This email was sent from a notification only address.\n"
					body += "If you need further assistance, please write to fuel.support@brillio.com \n";
					if (recipient) 
					{
						nlapiSendEmail(154256, recipient, subject, body);
					}
				}
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

function sendEmailNotification(frfId)
{
	try
	{
		if(frfId)
		{
			var recObj = nlapiLoadRecord('customrecord_frf_details',frfId);
			var n_oppId = recObj.getFieldValue("custrecord_frf_details_opp_id");
			nlapiLogExecution("DEBUG", "n_oppId : ", n_oppId);
			if (n_oppId) 
			{
				s_oppName = nlapiLookupField("customrecord_sfdc_opportunity_record", n_oppId, "custrecord_opportunity_name_sfdc");
			}
			var recipient = recObj.getFieldValue("custrecord_frf_details_created_by");
			nlapiLogExecution("DEBUG", "recipient : ", recipient);
			var s_recipient = nlapiLookupField("employee", recipient, "firstname");
			var frfNumber = recObj.getFieldValue("custrecord_frf_details_frf_number");
			var subject = frfNumber + ": FUEL Notification: FRF Cancelled";
			var s_project = recObj.getFieldText("custrecord_frf_details_project");
			var s_account = recObj.getFieldText("custrecord_frf_details_account");
			var s_reason = recObj.getFieldText("custrecord_frf_details_cancellation_reas");
			var cancelledById = recObj.getFieldValue("custrecord_frf_details_rrf_cancelled_by");
			nlapiLogExecution("DEBUG", "cancelledById : ", cancelledById);
			if(s_oppName){
				finalProjectOpp = s_oppName; 
			}else{
				finalProjectOpp = s_project;
			}
			if (cancelledById) {
				s_cancelledById = nlapiLookupField("employee", cancelledById, "firstname");
			}
			var body = "Dear " + s_recipient + " \n" + s_cancelledById + " has cancelled the FRF #" + frfNumber + " created for Opportunity/Project " + finalProjectOpp + "\n and Account " + s_account + "\n Cancellation Reason: "+ s_reason +"\n\n\n"
			body += "Brillio-FUEL \n"
			body += "This email was sent from a notification only address.\n"
			body += "If you need further assistance, please write to fuel.support@brillio.com \n";
			if (recipient) 
			{
				nlapiSendEmail(154256, recipient, subject, body);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('error','Error in Sending Noti')
	}
}
// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================




