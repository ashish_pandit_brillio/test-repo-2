/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime', 'N/format'],
 function (obj_Record, obj_Search, obj_Runtime, obj_Format) {
        function Send_Emp_data(obj_Request) {
            try {
                log.debug('requestBody ==', JSON.stringify(obj_Request));
                var obj_Usage_Context = obj_Runtime.getCurrentScript();
                var s_Request_Type = obj_Request.RequestType;
                log.debug('s_Request_Type ==', s_Request_Type);
                var s_Request_Data = obj_Request.RequestData;
                log.debug('s_Request_Data ==', s_Request_Data);
                var d_start_Date = obj_Request.start_date;
                d_start_Date = obj_Format.parse({ value: d_start_Date, type: obj_Format.Type.DATE });
                d_start_Date = obj_Format.format({ value: d_start_Date, type: obj_Format.Type.DATE });
                log.debug('d_start_Date ==', d_start_Date);
                var d_End_Date = obj_Request.end_date;
                d_End_Date = obj_Format.parse({ value: d_End_Date, type: obj_Format.Type.DATE });
                d_End_Date = obj_Format.format({ value: d_End_Date, type: obj_Format.Type.DATE });
                log.debug('d_End_Date ==', d_End_Date);
                var obj_Response_Return = {};
                if (s_Request_Data == 'RESOURCEALLOCATION') {
                    obj_Response_Return = get_Monthly_Data(obj_Search,d_start_Date,d_End_Date);
                }
            }
            catch (s_Exception) {
                log.debug('s_Exception==', s_Exception);
            } //// 
            log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
            log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
            return obj_Response_Return;
        } ///// Function Send_Emp_data


        return {
            post: Send_Emp_data
        };

    }); //// End of Main Function
/****************** Supporting Functions ***********************************/

function get_Monthly_Data(obj_Search,d_start_Date,d_End_Date) {
    try {
        var resourceallocationSearchObj = obj_Search.create({
            type: "resourceallocation",
            filters:
                [
                    ["customer", "anyof", "@ALL@"],
                    "AND",
                    ["job.type", "anyof", "@ALL@"],
                    "AND",
                    ["job.jobbillingtype", "anyof", "@ALL@"],
                    "AND",
                    ["enddate", "notbefore", d_End_Date],
                    "AND",
                    ["employee.custentity_implementationteam", "is", "F"],
                    "AND",
                    ["employee.custentity_employee_inactive", "is", "F"],
                    "AND",
                    ["startdate", "notafter", d_start_Date]
                ],
            columns:
                [
                    obj_Search.createColumn({ name: "internalid", label: "Internal ID" }),
                    obj_Search.createColumn({
                        name: "id",
                        sort: obj_Search.Sort.ASC,
                        label: "ID"
                    }),
                    obj_Search.createColumn({
                        name: "internalid",
                        join: "employee",
                        label: "Resource"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_fusion_empid",
                        join: "resource",
                        label: "Employee ID"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "CASE WHEN {employee.middlename} is null THEN {employee.firstname} || ' '  || {employee.lastname} ELSE {employee.firstname} || ' '  || {employee.middlename} || ' ' || {employee.lastname}  END",
                        label: "Employee Name"
                    }),
                    obj_Search.createColumn({
                        name: "isinactive",
                        join: "resource",
                        label: "Employee Inactive"
                    }),
                    obj_Search.createColumn({
                        name: "department",
                        join: "employee",
                        label: "Employee Practice"
                    }),
                    obj_Search.createColumn({
                        name: "departmentnohierarchy",
                        join: "employee",
                        label: "Employee Sub Practice"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "CASE WHEN INSTR({employee.department} , ' : ', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , ' : ', 1) - 1) ELSE {employee.department} END",
                        label: "Employee Parent Practice"
                    }),
                    obj_Search.createColumn({
                        name: "employeetype",
                        join: "employee",
                        label: "Salaried/Hourly"
                    }),
                    obj_Search.createColumn({
                        name: "entityid",
                        join: "job",
                        label: "Project ID"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "({job.jobname})",
                        label: "Project Name"
                    }),
                    obj_Search.createColumn({
                        name: "internalid",
                        join: "job",
                        label: "Project Internal Id"
                    }),
                    obj_Search.createColumn({
                        name: "entityid",
                        join: "customer",
                        label: "Customer ID"
                    }),
                    obj_Search.createColumn({
                        name: "altname",
                        join: "customer",
                        label: "Customer Name"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "concat(CONCAT({customer.entityid}, ' '), {customer.altname})",
                        label: "Customer "
                    }),
                    obj_Search.createColumn({ name: "custeventrbillable", label: "Resource Billable" }),
                    obj_Search.createColumn({ name: "startdate", label: "Start Date" }),
                    obj_Search.createColumn({ name: "enddate", label: "End Date" }),
                    obj_Search.createColumn({ name: "numberhours", label: "Number of Hours" }),
                    obj_Search.createColumn({ name: "percentoftime", label: "Percentage of Time" }),
                    obj_Search.createColumn({ name: "custeventbstartdate", label: "Billing Start Date" }),
                    obj_Search.createColumn({ name: "custeventbenddate", label: "Billing End Date" }),
                    obj_Search.createColumn({ name: "custevent3", label: "Bill Rate" }),
                    obj_Search.createColumn({ name: "custevent_otrate", label: "OT Rate" }),
                    obj_Search.createColumn({ name: "custeventwlocation", label: "Work Location" }),
                    obj_Search.createColumn({ name: "custevent4", label: "Onsite/Offsite" }),
                    obj_Search.createColumn({
                        name: "custentity_vertical",
                        join: "customer",
                        label: "Customer Vertical"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_practice",
                        join: "job",
                        label: "Executing Practice"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_project_services",
                        join: "job",
                        label: "Project Services"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_projectmanager",
                        join: "job",
                        label: "Project Manager"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_deliverymanager",
                        join: "job",
                        label: "Delivery Manager"
                    }),
                    obj_Search.createColumn({
                        name: "supervisor",
                        join: "employee",
                        label: "Reporting Manager"
                    }),
                    obj_Search.createColumn({
                        name: "timeapprover",
                        join: "employee",
                        label: "Time Approver"
                    }),
                    obj_Search.createColumn({
                        name: "email",
                        join: "employee",
                        label: "Email"
                    }),
                    obj_Search.createColumn({
                        name: "altemail",
                        join: "employee",
                        label: "Alt. Email"
                    }),
                    obj_Search.createColumn({
                        name: "jobbillingtype",
                        join: "job",
                        label: "Billing Type"
                    }),
                    obj_Search.createColumn({
                        name: "jobtype",
                        join: "job",
                        label: "Project Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_stpayrate",
                        join: "employee",
                        label: "ST Pay Rate"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_otpayrate",
                        join: "employee",
                        label: "OT Pay Rate"
                    }),
                    obj_Search.createColumn({
                        name: "subsidiarynohierarchy",
                        join: "employee",
                        label: "Subsidiary"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_persontype",
                        join: "employee",
                        label: "Person Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_employeetype",
                        join: "employee",
                        label: "Employee Type"
                    }),
                    obj_Search.createColumn({
                        name: "employeestatus",
                        join: "employee",
                        label: "Employee Status"
                    }),
                    obj_Search.createColumn({
                        name: "title",
                        join: "employee",
                        label: "Job Title"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_actual_hire_date",
                        join: "employee",
                        label: "Actual Hire Date"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_emp_function",
                        join: "employee",
                        label: "Function"
                    }),
                    obj_Search.createColumn({
                        name: "territory",
                        join: "customer",
                        label: "Territory"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_project_allocation_category",
                        join: "job",
                        label: "Project Allocation Category"
                    }),
                    obj_Search.createColumn({
                        name: "location",
                        join: "employee",
                        label: "Employee Location"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_lwd",
                        join: "employee",
                        label: "Last Working Date"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "{employee.custentity_assignmentcategory} || '-' || {employee.custentity_flsacode} || '-' || {employee.employeetype}",
                        label: "Employment Category"
                    }),
                    obj_Search.createColumn({
                        name: "startdate",
                        join: "job",
                        label: "Project start date"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "{enddate}",
                        label: "Project End date"
                    })
                ]
        });
        var searchResultCount = resourceallocationSearchObj.runPaged().count;

        log.debug("resourceallocationSearchObj result count", searchResultCount);
        var myResults = getAllResults(resourceallocationSearchObj);
        var arr_Emp_json = [];
        var st_pay_rate_temp;
        var ot_pay_rate_temp;
        var ot_rate_temp;
        var cols = resourceallocationSearchObj.columns
        myResults.forEach(function (result) {
            st_pay_rate_temp =  result.getValue({
                name: "custentity_stpayrate",
                join: "employee",
                label: "ST Pay Rate"
            })
            ot_pay_rate_temp =  result.getValue({
                name: "custentity_otpayrate",
                join: "employee",
                label: "OT Pay Rate"
            })
            ot_rate_temp =result.getValue({ name: "custevent_otrate", label: "OT Rate" })
            if(st_pay_rate_temp>0){
               log.debug("rate_temp inside if");
               st_pay_rate_temp = st_pay_rate_temp
            }
            else{
               log.debug("rate_temp inside else");
               st_pay_rate_temp = "0.00"
            }
            if(ot_pay_rate_temp>0){
                log.debug("rate_temp inside if");
                ot_pay_rate_temp = ot_pay_rate_temp
             }
             else{
                log.debug("rate_temp inside else");
                ot_pay_rate_temp = "0.00"
             }
             if(ot_rate_temp>0){
                log.debug("rate_temp inside if");
                ot_rate_temp = ot_rate_temp
             }
             else{
                log.debug("rate_temp inside else");
                ot_rate_temp = "0.00"
             }
            var obj_json_Container = {}
            obj_json_Container = {
                'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
                'Resource': result.getValue({
                    name: "internalid",
                    join: "employee",
                }),
                'Employee_Id': result.getValue({
                    name: "internalid",
                    join: "employee",
                }),
                'Employee_Name': result.getValue(cols[4]),
                'Employee_Inactive': result.getValue({ name: "isinactive", join: "resource" }),
                'Employee_Practice': result.getText({ name: "department", join: "employee" }),
                'Employee_Subpractice': result.getText({ name: "departmentnohierarchy", join: "employee" }),
                'Employee_Parent_Practice': result.getValue(cols[8]),
                'Salaried/Hourly': result.getText({ name: "employeetype", join: "employee" }),
                'Project_ID': result.getValue({ name: "entityid", join: "job" }),
                'Project_Name': result.getValue(cols[11]),
                'Poject_Internal_ID': result.getValue({
                    name: "internalid",
                    join: "job",
                    label: "Project Internal Id"
                }),
                'Customer_ID': result.getValue({
                    name: "entityid",
                    join: "customer",
                    label: "Customer ID"
                }),
                'Customer_Name': result.getValue({
                    name: "altname",
                    join: "customer",
                    label: "Customer Name"
                }),
                'Customer': result.getValue(cols[15]),
                'Resource Billable': result.getValue({ name: "custeventrbillable", label: "Resource Billable" }),
                'Start_Date': result.getValue({ name: "startdate", label: "Start Date" }),
                'End_Date': result.getValue({ name: "enddate", label: "End Date" }),
                'Number_Of_Hours': result.getValue({ name: "numberhours", label: "Number of Hours" }),
                'Percentage_of_Time': result.getValue({ name: "percentoftime", label: "Percentage of Time" }),
                'Billing_Start_Date': result.getValue({ name: "custeventbstartdate", label: "Billing Start Date" }),
                'Billing_End_Date': result.getValue({ name: "custeventbenddate", label: "Billing End Date" }),
                'Bill_Rate': result.getValue({ name: "custevent3", label: "Bill Rate" }),
                'OT_Rate': ot_rate_temp,
                'Work Location': result.getText({ name: "custeventwlocation", label: "Work Location" }),
                'Onsite/Offsite': result.getText({ name: "custevent4", label: "Onsite/Offsite" }),
                'Customer_Vertical': result.getText({
                    name: "custentity_vertical",
                    join: "customer",
                    label: "Customer Vertical"
                }),
                'Executing_Practice': result.getText({
                    name: "custentity_practice",
                    join: "job",
                    label: "Executing Practice"
                }),
                'Project_Services': result.getValue({
                    name: "custentity_project_services",
                    join: "job",
                    label: "Project Services"
                }),
                'Project_Manager': result.getText({
                    name: "custentity_projectmanager",
                    join: "job",
                    label: "Project Manager"
                }),
                'Delivery_Manager': result.getText({
                    name: "custentity_deliverymanager",
                    join: "job",
                    label: "Delivery Manager"
                }),
                'Reporting Manager': result.getValue({
                    name: "supervisor",
                    join: "employee",
                    label: "Reporting Manager"
                }),
                'Time_Approver': result.getText({
                    name: "timeapprover",
                    join: "employee",
                    label: "Time Approver"
                }),
                'Email': result.getValue({
                    name: "email",
                    join: "employee",
                    label: "Email"
                }),
                'Alt. Email': result.getValue({
                    name: "altemail",
                    join: "employee",
                    label: "Alt. Email"
                }),
                'Billing_Type': result.getText({
                    name: "jobbillingtype",
                    join: "job",
                    label: "Billing Type"
                }),
                'Project_Type': result.getText({
                    name: "jobtype",
                    join: "job",
                    label: "Project Type"
                }),
                'ST_Pay_Rate':st_pay_rate_temp,              
                'OT_Pay_Rate':ot_pay_rate_temp,
                'Subsidiary': result.getText({
                    name: "subsidiarynohierarchy",
                    join: "employee",
                    label: "Subsidiary"
                }),
                'Person_Type': result.getText({
                    name: "custentity_persontype",
                    join: "employee",
                    label: "Person Type"
                }),
                'Employee_Type': result.getText({
                    name: "custentity_employeetype",
                    join: "employee",
                    label: "Employee Type"
                }),
                'Employee_Status': result.getText({
                    name: "employeestatus",
                    join: "employee",
                    label: "Employee Status"
                }),
                'Job_Title': result.getValue({
                    name: "title",
                    join: "employee",
                    label: "Job Title"
                }),
                'Actual_Hire_Date': result.getValue({
                    name: "custentity_actual_hire_date",
                    join: "employee",
                    label: "Actual Hire Date"
                }),
                'Function': result.getText({
                    name: "custentity_emp_function",
                    join: "employee",
                    label: "Function"
                }),
                'Territory': result.getText({
                    name: "territory",
                    join: "customer",
                    label: "Territory"
                }),
                'Project_Allocation_Category': result.getText({
                    name: "custentity_project_allocation_category",
                    join: "job",
                    label: "Project Allocation Category"
                }),
                'Employee_Location': result.getText({
                    name: "location",
                    join: "employee",
                    label: "Employee Location"
                }),
                'Last_Working_Date': result.getValue({
                    name: "custentity_lwd",
                    join: "employee",
                    label: "Last Working Date"
                }),
                'Employment_Category': result.getValue(cols[51]),
                'Project_Start_Date': result.getValue({
                    name: "startdate",
                    join: "job",
                    label: "Project start date"
                }),
                'Project_End_Date': result.getValue(cols[53]),
            };
            arr_Emp_json.push(obj_json_Container);
            return true;
        });
        return arr_Emp_json;
    } //// End of try
    catch (s_Exception) {
        log.debug('get_Employee_Data s_Exception== ', s_Exception);
    } //// End of catch 
}
function getAllResults(s) {
    var result = s.run();
    var searchResults = [];
    var searchid = 0;
    do {
        var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
        resultslice.forEach(function (slice) {
            searchResults.push(slice);
            searchid++;
        }
        );
    } while (resultslice.length >= 1000);
    return searchResults;
}