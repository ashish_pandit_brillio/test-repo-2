var f_current_month_actual_revenue_total_proj_level=0;
var a_recognized_revenue_total_proj_level = new Array();
function sch_fp_rev_project_report()
{
	try
	{
		var context=nlapiGetContext();
		var excel_data_res=[];
		var a_practice_list=new Array();
		var date_proj=new Date();
		var sr_num=0;
		
		var user=nlapiGetUser();
		if(user<0)
		{
			user=7905;
		}
		var email=nlapiLookupField('employee', user, 'email');
		if(user==30484||user== 9109)
		{
			user=7905;
		}
       //if(user == 7905)
            //user =2301;
		//var email=nlapiLookupField('employee', user, 'email');
		nlapiLogExecution('DEBUG','USER',email);
		nlapiLogExecution('DEBUG','USER',user);
		var pract_fil=new Array();
		pract_fil[0]=new nlobjSearchFilter('custrecord_practicehead',null,'anyof',user);
		pract_fil[1]=new nlobjSearchFilter('isinactive',null,'is','F');
		var pract_col=new Array();
		pract_col[0]=new nlobjSearchColumn('internalid');
		var pract_search=nlapiSearchRecord('department',null,pract_fil,pract_col);
		if(pract_search)
		{
			for(var i_prac=0;i_prac<pract_search.length;i_prac++)
			{
				a_practice_list.push(pract_search[i_prac].getValue('internalid'));
			}
		}
		var a_get_logged_in_user_exsiting_revenue_cap = '';
		var a_project_search_results='';
		var a_revenue_cap_filter = [[['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', user], 'or',
								['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', user], 'or',
								['custrecord_revenue_share_project.custentity_clientpartner', 'anyof', user], 'or',
								['custrecord_revenue_share_project.custentity_practice','anyof',a_practice_list],'or',
								['custrecord_revenue_share_cust.custentity_clientpartner', 'anyof', user]],
								'and',
								['custrecord_revenue_share_approval_status','anyof',3]];
			
		var a_columns_existing_cap_srch = new Array();
		a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_project');		
		a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
		a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
		a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
		if(parseInt(user) == parseInt(41571) || parseInt(user) == parseInt(7905)|| parseInt(user) == parseInt(35819) || 		parseInt(user) == parseInt(90037) || parseInt(user) == parseInt(72055) || parseInt(user) == parseInt(2301))
			{
				//var a_project_cap_filter =[ ['custrecord_revenue_share_approval_status','anyof',3],'and',
              //                            ['internalid','anyof',75]];
              var a_project_cap_filter =[[ ['custrecord_revenue_share_approval_status','anyof',3],
                                       'or', ['custrecord_sow_value_change_status','is','Saved']],
								'and',
								['custrecord_is_mnth_end_effort_created','is','T'],
                                        ];
														
				a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_project_cap_filter, a_columns_existing_cap_srch);
			}
		else
			 a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
		if(a_get_logged_in_user_exsiting_revenue_cap)
			{
			for(var i=0;i< a_get_logged_in_user_exsiting_revenue_cap.length;i++)
				//for(var i=0;i< 1;i++)
				{
					var pid=a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_revenue_share_project');
					
					if(pid)
					{
						var a_project_filter = [[['custentity_projectmanager', 'anyof', user], 'or',
								['custentity_deliverymanager', 'anyof', user], 'or',
								['custentity_clientpartner', 'anyof', user], 'or',
								['custentity_practice.custrecord_practicehead','anyof', user],'or',
								['customer.custentity_clientpartner', 'anyof', user]], 'and',
								['status', 'noneof', 1], 'and',
								['internalid', 'anyof',pid], 'and',
								['jobtype', 'anyof', 2], 'and',
                                ['enddate','onorafter',date_proj],'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];
								//['custentity_fp_rev_rec_type', 'noneof', '@NONE@']
						var a_columns_proj_srch = new Array();
						a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
						a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
						a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
						a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
						a_columns_proj_srch[4]= new nlobjSearchColumn('startdate');
						a_columns_proj_srch[5]= new nlobjSearchColumn('custentity_rev_rec_new_strt_date');
						a_columns_proj_srch[6]= new nlobjSearchColumn('enddate');
						a_columns_proj_srch[7]= new nlobjSearchColumn('custentity_project_currency');
						a_columns_proj_srch[8]=new nlobjSearchColumn('internalid');
						a_columns_proj_srch[9]=new nlobjSearchColumn('companyname');
						a_columns_proj_srch[10]=new nlobjSearchColumn('entityid');
						if(parseInt(user) == parseInt(41571)|| parseInt(user) == parseInt(7905)|| parseInt(user) == parseInt(35819) || parseInt(user) == parseInt(90037) || parseInt(user) == parseInt(72055) || parseInt(user) == parseInt(2301))
						{
							var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1],'and',
								['enddate','onorafter',date_proj],'and',
								['internalid','anyof',pid]];
								
							a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
						}
						else
						{
							a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
						}
						
						for(var pm=0;pm < a_project_search_results.length;pm++)
						{
							f_current_month_actual_revenue_total_proj_level = 0;
							a_recognized_revenue_total_proj_level = new Array();
							var excel_dat={
							'excel_data':[]
								};
							var data='';
							var internalid=a_project_search_results[pm].getValue('internalid');
							var proj_name=a_project_search_results[pm].getValue('companyname');
							var d_proj_start_date=a_project_search_results[pm].getValue('startdate');
							var d_proj_strt_date_old_proj = a_project_search_results[pm].getValue('custentity_rev_rec_new_strt_date');
							var d_proj_end_date = a_project_search_results[pm].getValue('enddate');
							var s_proj_currency = a_project_search_results[pm].getText('custentity_project_currency');
							var proj_id=a_project_search_results[pm].getValue('entityid');
							nlapiLogExecution('DEBUG','internalid',internalid);
							if(d_proj_strt_date_old_proj)
							{
								d_proj_start_date = d_proj_strt_date_old_proj;
							}
							
							var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
							var i_year_project = d_pro_strtDate.getFullYear();
							var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
							var i_year_project_end = s_pro_endDate.getFullYear();
							var i_project_mnth = d_pro_strtDate.getMonth();
							var i_prject_end_mnth = s_pro_endDate.getMonth();
							var s_currency_symbol_proj = s_proj_currency;
							var s_currency_symbol_usd = getCurrency_Symbol('USD');
							// get previous mnth effrt for true up and true down scenario
							
							data=getMargin(internalid,i_year_project,s_currency_symbol_proj,d_pro_strtDate,s_pro_endDate,i_project_mnth,s_currency_symbol_usd,i_prject_end_mnth,i_year_project_end);
							for(var dataln=0;dataln<data.length;dataln++)
							{

								excel_dat['excel_data'].push({
											'proj':proj_id,
											'project_id':proj_name,
											'project_name':data[dataln].project_id,
											'practice': data[dataln].project_name,
											'currency':s_currency_symbol_proj,
											'st_date':data[dataln].month,
											'actual_rev':data[dataln].actual_rev,
											'cumm_rev':data[dataln].cumm_rev,
											'monthlyrevenue':data[dataln].monthlyrevenue,
											'rev_rec_permnth':data[dataln].rev_rec_permnth,
											'act_rev_rec_mnth':data[dataln].act_rev_rec_mnth,
											'cummulative_mnth':data[dataln].cummulative_mnth
											/*'sold_margin': data[dataln].sold_margin,
											'expe': data[dataln].expe,
											'rev': data[dataln].rev,
											'actual_margin':data[dataln].actual_margin,
											'account_name': data[dataln].account_name  */                                 
										});
									//sr_num++;
								
							}
							excel_data_res.push(excel_dat);
							//sr_num++;
						}
						
					}
				}
				if(excel_data_res!=0)
				down_excel_function(excel_data_res,email); 
			}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','SCh_fp_rev_rec_report error','ERROR MESSAGE :- '+err);
		throw err;
	}
}
function getMargin(i_projectId,i_year_project,s_currency_symbol_proj,d_proj_start_date,d_proj_end_date,i_project_mnth,s_currency_symbol_usd,i_prject_end_mnth,i_year_project_end)
{
	var sr_no=0;
	var bill_data_arr = new Array();
	try
	{
	var a_revenue_cap_filters = [['custrecord_revenue_share_project', 'anyof', parseInt(i_projectId)]];
					
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
				a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filters, a_column);
				if (a_get_logged_in_user_exsiting_revenue_cap)
				{
					var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
					var i_revenue_share_stat = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
				}
	
			
		{
				var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)]];
			
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						var i_mnth_end_json_id = a_get_mnth_end_effrt_activity[0].getId();
					}			
					
				var a_recipient_mail_id = new Array();
				var a_practice_involved = new Array();
				var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_revenue_share_id)]];
				var a_columns_existing_cap_srch = new Array();
				a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
				a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
				var a_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
				if (a_exsiting_revenue_cap)
				{
					for(var i_revenue_index = 0; i_revenue_index < a_exsiting_revenue_cap.length; i_revenue_index++)
					{
						var i_parent_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue(	'custrecord_revenue_share_per_practice_pr');
						var i_sub_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_su');
					
						if(a_practice_involved.indexOf(i_parent_parctice_id) < 0)
						{
							a_practice_involved.push(i_parent_parctice_id);
						}
						if(a_practice_involved.indexOf(i_sub_parctice_id) < 0)
						{
							a_practice_involved.push(i_sub_parctice_id);
						}
					}
				}	
			
					var a_filters_search_practice = [['internalid','anyof',a_practice_involved]
									  ];						  
					var a_columns_practice = new Array();
					a_columns_practice[0] = new nlobjSearchColumn('email', 'custrecord_practicehead');
					var a_parctice_srch = nlapiSearchRecord('department', null, a_filters_search_practice, a_columns_practice);
					if (a_parctice_srch)
					{
						for(var i_practice_index = 0; i_practice_index < a_parctice_srch.length; i_practice_index++)
						{
						a_recipient_mail_id.push(a_parctice_srch[i_practice_index].getValue('email', 'custrecord_practicehead'));
						}
					}			
		}			
		var projectWiseRevenue = [];
		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', i_projectId]
													];
							
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
		
		var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
			nlapiLogExecution('audit','recognized revenue for project found');
			for(var i_revenue_index=0; i_revenue_index<a_get_ytd_revenue.length; i_revenue_index++)
			{
				//nlapiLogExecution('audit','recognized amnt:- ',a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'));
				a_revenue_recognized_for_project.push({
												'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
												'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
												'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
												'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
												'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
												'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
												'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
												});
			}
		}
		
		
	var d_today_date = new Date();
							var i_current_mnth = d_today_date.getMonth();
							var i_current_year = d_today_date.getFullYear();
							var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);
	
		
		var i_prev_month = parseFloat(i_current_mnth) - parseFloat(1);
		
		var a_filter_get_prev_mnth_effrt = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)], 'and',
											['custrecord_current_mnth_no', 'equalto', parseInt(i_prev_month)]];
													
		var a_columns_get_prev_mnth_effrt = new Array();
		a_columns_get_prev_mnth_effrt[0] = new nlobjSearchColumn('created').setSort(true);
			
		var a_get_prev_month_effrt = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_filter_get_prev_mnth_effrt, a_columns_get_prev_mnth_effrt);
		if (a_get_prev_month_effrt)
		{
			nlapiLogExecution('debug','prev mnth effrt json id:- '+a_get_prev_month_effrt[0].getId());
			
			var projectWiseRevenue_previous_effrt = [];
			
			var a_prev_subprac_searched_once = new Array();
			
			var o_mnth_end_effrt_prev_mnth = nlapiLoadRecord('customrecord_fp_rev_rec_mnth_end_json', a_get_prev_month_effrt[0].getId());
			
			var s_effrt_json_1_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt');
			if(s_effrt_json_1_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
				
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_1_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
							
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
			
			var s_effrt_json_2_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt2');
			if(s_effrt_json_2_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_2_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
			
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_2_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
								
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
			
			var s_effrt_json_3_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt3');
			if(s_effrt_json_3_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_3_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
			
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_3_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
							
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
			
			var s_effrt_json_4_prev_mnth = o_mnth_end_effrt_prev_mnth.getFieldValue('custrecord_json_mnth_end_effrt4');
			if(s_effrt_json_4_prev_mnth)
			{
				//generate_previous_effrt_revenue(s_effrt_json_4_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
			
				{
					var i_practice_previous = 0;
					var f_total_revenue_for_tenure = 0;
					//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
					var s_entire_json_clubed = JSON.parse(s_effrt_json_4_prev_mnth);
					for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
					{
						var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
						for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
						{
							var i_practice = a_row_json_data[i_row_json_index].prac;
							var s_practice = a_row_json_data[i_row_json_index].prac_text;
							var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
							var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
							var i_role = a_row_json_data[i_row_json_index].role;
							var s_role = a_row_json_data[i_row_json_index].role_text;
							var i_level = a_row_json_data[i_row_json_index].level;
							var s_level = a_row_json_data[i_row_json_index].level_text;
							var i_location = a_row_json_data[i_row_json_index].loc;
							var s_location = a_row_json_data[i_row_json_index].loc_text;
							var f_revenue = a_row_json_data[i_row_json_index].cost;
							if (!f_revenue) 
								f_revenue = 0;
							
							var f_revenue_share = a_row_json_data[i_row_json_index].share;
							if (!f_revenue_share) 
								f_revenue_share = 0;
								
							var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
							var s_mnth_strt_date = '1/31/2017';
							var s_mnth_end_date = '31/31/2017';
							var s_mnth = a_row_json_data[i_row_json_index].mnth;
							var s_year = a_row_json_data[i_row_json_index].year;
							
							var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
							
							if (!f_revenue_recognized) 
								f_revenue_recognized = 0;
							
							//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
							
							var s_month_name = s_mnth + '_' + s_year;
							
							var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
							total_revenue = parseFloat(total_revenue).toFixed(2);
							
							if (i_practice_previous == 0) {
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
							
							if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
							}
						
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
							f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
							
							var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
							
							if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
								};
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
							else {
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
								
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
								projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							}
						}
					}
				}
			}
		}
		
		if(i_mnth_end_json_id)
		{
			var a_total_effort_json = [];
			
			var o_mnth_end_effrt = nlapiLoadRecord('customrecord_fp_rev_rec_mnth_end_json',i_mnth_end_json_id);
			
			var s_effrt_json = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt');
			//s_effrt_json = JSON.stringify(s_effrt_json);
			var s_effrt_json_2 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt2');
			//s_effrt_json_2 = JSON.stringify(s_effrt_json_2);
			var s_effrt_json_3 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt3');
			var s_effrt_json_4 = o_mnth_end_effrt.getFieldValue('custrecord_json_mnth_end_effrt4');
			
			var a_duplicate_sub_prac_count = new Array();
			var a_unique_list_practice_sublist = new Array();
			
			var a_subprac_searched_once = new Array();
			var a_subprac_searched_once_month = new Array();
			var a_subprac_searched_once_year = new Array();
			
			if(s_effrt_json)
			{
				a_total_effort_json.push(s_effrt_json);
				
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
							
					
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_2)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_3)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
											
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			
			if(s_effrt_json_4)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
							
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
									
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
												
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
			var monthBreakUp = getMonthsBreakup(d_proj_start_date,d_proj_end_date);
			var excel_cost_data=[];		
			// Revenue share table
			var h_tableHtml_revenue_share = generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,5,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end);					
			// Effort View table
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
				//var f_prev_mnth_cost = generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				//nlapiLogExecution('audit','cost for prev mnth:- '+f_prev_mnth_cost,'sub prac:- '+a_duplicate_sub_prac_count[i_dupli].sub_prac);
				var h_tableHtml_effort_view = generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				excel_cost_data.push(h_tableHtml_effort_view);
			}
			// Total revenue table testing for now
			var h_tableHtml_total_view = generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,4,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
			//if(_logValidation(h_tableHtml_effort_view)&& )
			for(var excel_len=0;excel_len< excel_cost_data.length;excel_len++)
				{
						var rev_len=excel_cost_data[excel_len].month;
					//	for(var reven=0;reven<h_tableHtml_effort_view.length;reven++)
						if(excel_len!=excel_cost_data.length-1)
						{
							bill_data_arr[sr_no] = {
                                    'project_id': excel_cost_data[excel_len].practice,
                                    'project_name': excel_cost_data[excel_len].subpractice,
									'month' :excel_cost_data[excel_len].month,
									'actual_rev':excel_cost_data[excel_len].revenue,
									'cumm_rev' :excel_cost_data[excel_len].cummulative									
								};
						}
						else
						{
								bill_data_arr[sr_no] = {
                                    'project_id': excel_cost_data[excel_len].practice,
                                    'project_name': excel_cost_data[excel_len].subpractice,
									'month' :excel_cost_data[excel_len].month,
									'actual_rev':excel_cost_data[excel_len].revenue,
									'cumm_rev' :excel_cost_data[excel_len].cummulative,
									'monthlyrevenue':h_tableHtml_total_view.monthlyrevenue,
                                    'rev_rec_permnth': h_tableHtml_total_view.rev_rec_permnth,
                                    'act_rev_rec_mnth': h_tableHtml_total_view.act_rev_rec_mnth,
									'cummulative_mnth': h_tableHtml_total_view.cummulative_mnth
                                                             
                                   };
						}
							sr_no++;
							nlapiLogExecution('debug','Data',bill_data_arr);
				}
		}
		
		nlapiLogExecution('audit','remaining usage:- ',nlapiGetContext().getRemainingUsage());
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suitelet_month_end_confirmation','ERROR MESSAGE:- '+err);
		throw err;
	}
	return bill_data_arr;
}
function generate_excel(h_tableHtml_effort_view)
{
		var rev_len=h_tableHtml_effort_view.month;
		//	for(var reven=0;reven<h_tableHtml_effort_view.length;reven++)
		{
			bill_data_arr[sr_no] = {
                   'project_id': h_tableHtml_effort_view.practice,
                    'project_name': h_tableHtml_effort_view.subpractice,
					'month' :h_tableHtml_effort_view.month,
					'actual_rev':h_tableHtml_effort_view.revenue,
					'cumm_rev' :h_tableHtml_effort_view.cummulative,
					'monthlyrevenue':h_tableHtml_total_view.monthlyrevenue,
                    'rev_rec_permnth': h_tableHtml_total_view.rev_rec_permnth,
                    'act_rev_rec_mnth': h_tableHtml_total_view.act_rev_rec_mnth,
					'cummulative_mnth': h_tableHtml_total_view.cummulative_mnth
                                  /*  'client_partnr': client_partnr,
                                    'sold_margin': sold_margin,
									'expe': expe,
									'rev': rev,
                                    'actual_margin': margin,
                                     'account_name': s_pro_account  */                                  
                                   };
							sr_no++;
							nlapiLogExecution('debug','Data',bill_data_arr);
						}
					

}
function generate_cost_view_prev_mnth(projectWiseRevenue_previous_effrt,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue_previous_effrt) {
		
		for ( var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
			{
				
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							
							var total_revenue_format = parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue_previous_effrt[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue_previous_effrt[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue_previous_effrt[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		
	}
	
	
	// Percent row
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
	}
	
	//Total reveue recognized per month row
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt <= i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue_previous_effrt)
	{
		for (var project_internal_id in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue_previous_effrt[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue_previous_effrt[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	//Actual revenue to be recognized 
	
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
	}
	
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	//nlapiLogExecution('audit','i_current_month:- '+i_current_month);
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		
		
	}
	
	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)+ parseFloat(f_current_month_actual_revenue);
		
	
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_current_month]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		return f_revenue_amount;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	//Total revenue recognized for previous months
	
	
	for(var i_exist_index=0; i_exist_index<=a_recognized_revenue.length; i_exist_index++)
	{
		if(a_recognized_revenue_total_proj_level[i_exist_index])
		{
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		}
		else
		{
			var f_existing_cost_in_arr = 0;
		}
		
		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost) + parseFloat(f_existing_cost_in_arr);
		
		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	
	//return html;
}

function generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	var html_data=[];
	var html_val={};

	var html ={
		'month':[],
		'practice' :[],
		'subpractice' :[],
		'revenue':[],
		'actual_rec' :[],
		'cummulative' :[],
		'html_amount':[]
		};
		
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	//html['currency'].push(s_currency_symbol_proj);
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;	
		html['month'].push(s_month_name);
	}
		
	
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				
				
				if(i_diplay_frst_column == 0)
				{
				//	html += "<td class='label-name'>";
				//	html['revenue'].push('Effort View');
				//	html += "</td>";
				}
				else
				{
					//html += "<td class='label-name'>";
				//	html += '<b>';
					//html += "</td>";
				}
				
				
				projectWiseRevenue[emp_internal_id].practice_name;
				 projectWiseRevenue[emp_internal_id].sub_prac_name;	
				 projectWiseRevenue[emp_internal_id].role_name;
				 projectWiseRevenue[emp_internal_id].level_name;
				projectWiseRevenue[emp_internal_id].location_name;
				
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						
						parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			
			 parseFloat(f_total_allocated_row).toFixed(2);
					
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	//html += "<tr>";
	
//	html += '<b>Sub Total';	
//	html += '<b>'+s_practice_name;
//	html += '<b>'+s_sub_prac_name;
//	html += '';
//	html += '';
//	html += '';
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		//html += "<td class='monthly-amount'>";
		//html += '<b>'+parseFloat(total_prcnt_aloocated[i_index_total_allocated]).toFixed(2);
		//html += "</td>";
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	//html += "<td class='monthly-amount'>";
//	html['revenue'].push(parseFloat(f_total_row_allocation).toFixed(2));
	//html += "</td>";
		
	//html += "</tr>";
	
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{		
				 projectWiseRevenue[emp_internal_id].practice_name;
						
				 projectWiseRevenue[emp_internal_id].sub_prac_name;
				
				 projectWiseRevenue[emp_internal_id].role_name;
				 projectWiseRevenue[emp_internal_id].level_name;
				 projectWiseRevenue[emp_internal_id].location_name;	
				 var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								nlapiLogExecution('audit','month name:- '+month);
								nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						//html += "</td>";
					}
				}
				
				if (mode == 2)
				{
				 i_total_per_row = 0;
				}
				
				i_frst_row = 1;
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
		}
	}
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
	
	}
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		

	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		//html += "<td>";
	//	html += s_month_name;
	//	html += "</td>";
	}
		
//	html += "<td>";
//	html += "Total";
	//html += "</td>";
			
//	html += "</tr>";
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		//nlapiLogExecution('audit','prev mnth recog rev:- '+a_recognized_revenue[i_revenue_index]);
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	//Actual revenue to be recognized 
	//html += "<tr>";
	
	html['revenue'].push('Actual revenue recognized/Revenue forecast');
	html['practice'].push(s_practice_name);
	html['subpractice'].push(s_sub_prac_name);
	
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 + parseInt(i_current_month);
		var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
		i_current_month = parseFloat(f_total_prev_mnths) + parseFloat(1);
			
		for(var i_revenue_index=0; i_revenue_index<i_current_month; i_revenue_index++)
		{
			
			html['revenue'].push(format2(a_recognized_revenue[i_revenue_index]));
			
		}
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		nlapiLogExecution('audit','i_current_month:- '+i_current_month,a_recognized_revenue[i_current_month-1]);
		for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
		{
			html['revenue'].push(format2(a_recognized_revenue[i_revenue_index]));
		
		}
	}
	
	nlapiLogExecution('audit','f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month,'i_total_revenue_recognized_ytd:- '+i_total_revenue_recognized_ytd);
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	
	html['revenue'].push(format2(f_current_month_actual_revenue));
	
	var i_total_actual_revenue_recognized = parseFloat(i_total_revenue_recognized_ytd)+ parseFloat(f_current_month_actual_revenue);
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html['revenue'].push(format2(f_revenue_amount));
		
		
		i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	
	html['revenue'].push(format2(i_total_actual_revenue_recognized));

	
	
	//Total revenue recognized for previous months
	//html += "<tr>";
	
	//html += "<td class='label-name'>";
	html['cummulative'].push('Cumulative Revenue');
	//html += "</td>";
	
	//html += "<td class='label-name'>";
	//html += '<b>'+s_practice_name;
	//html += "</td>";
			
	//html += "<td class='label-name'>";
	//html += '<b>'+s_sub_prac_name;
	//html += "</td>";

	var f_cumulative_rev = 0;
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		var f_amount_rounded_off = parseFloat(a_recognized_revenue[i_revenue_index]).toFixed(1);
		
		f_cumulative_rev = parseFloat(f_cumulative_rev) + parseFloat(f_amount_rounded_off);
	//	html += "<td class='monthly-amount'>";
		html['cummulative'].push(format2(f_cumulative_rev));
		//html += "</td>";
	}
	
	f_amount_rounded_off = parseFloat(f_current_month_actual_revenue).toFixed(1);
	f_cumulative_rev = parseFloat(f_cumulative_rev) + parseFloat(f_amount_rounded_off);
	//html += "<td class='monthly-amount'>";
	html['cummulative'].push(format2(f_cumulative_rev));
	//html += "</td>";
	
	for(var i_future_index=i_current_month+1; i_future_index<a_amount.length; i_future_index++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_future_index]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		f_cumulative_rev = parseFloat(f_cumulative_rev) + parseFloat(f_revenue_amount);
	//	html += "<td class='monthly-amount'>";
		html['cummulative'].push(format2(f_cumulative_rev));
	//	html += "</td>";
	}
	
	//if(i_total_revenue_recognized_ytd == 0)
		i_total_revenue_recognized_ytd = f_revenue_share;
	
	//html += "<td class='monthly-amount'>";
	html['cummulative'].push(format2(i_total_revenue_recognized_ytd));
	//html += "</td>";
	
	//html += "</tr>";
	
	//html += "</table>";
	
	for(var i_exist_index=0; i_exist_index<=a_recognized_revenue.length; i_exist_index++)
	{
		if(a_recognized_revenue_total_proj_level[i_exist_index])
		{
			var f_existing_cost_in_arr = parseFloat(a_recognized_revenue_total_proj_level[i_exist_index]);
		}
		else
		{
			var f_existing_cost_in_arr = 0;
		}
		
		var f_existing_cost = a_recognized_revenue[i_exist_index];
		f_existing_cost = parseFloat(f_existing_cost) + parseFloat(f_existing_cost_in_arr);
		
		a_recognized_revenue_total_proj_level[i_exist_index] = f_existing_cost;
	}
	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	html_val={
		pract : s_practice_name,
		sub_pract : s_sub_prac_name,
		};
	html_data.push(html);
	nlapiLogExecution('DEBUG','HTML',JSON.stringify(html));	

	return html;
}

function generate_total_project_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	

	var html ={
		'month':[],
		'monthlyrevenue' :[],
		'rev_rec_permnth' :[],
		'act_rev_rec_mnth' :[],
		'cummulative_mnth':[],
		};

		
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html['month'].push(s_month_name);
		
	}
		

	//html += "Total";
		
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_prcnt_arr = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var i_total_row_revenue = 0;
	var f_total_prcnt_complt = 0;
	var a_sub_practice_arr = new Array();
	
	for ( var emp_internal_id in projectWiseRevenue)
	{
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_sub_practice_internal_id = 0;
			var i_amt = 0;
			
			if (a_sub_practice_arr.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue);
						if(!total_revenue_format)
							total_revenue_format = 0;
							
						var f_perctn_complete = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt);
						if(!f_perctn_complete)
							f_perctn_complete = 0;
							
						if (i_frst_row == 0)
						{
							a_amount.push(total_revenue_format);
							a_prcnt_arr.push(f_perctn_complete);
						}
						else
						{
							var amnt_mnth = a_amount[i_amt];
							
							var f_revenue_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue;
							if(!f_revenue_amount)
								f_revenue_amount = 0;
								
							amnt_mnth = parseFloat(amnt_mnth) + parseFloat(f_revenue_amount);
							a_amount[i_amt] = amnt_mnth;
							//i_amt++;
							
							var f_prcnt_present = a_prcnt_arr[i_amt];
							
							var f_prcnt_updated = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt;
							if(!f_prcnt_updated)
								f_prcnt_updated = 0;
								
							f_prcnt_present = parseFloat(f_prcnt_present) + parseFloat(f_prcnt_updated);
							a_prcnt_arr[i_amt] = f_prcnt_present;
							i_amt++;
						}
						
						a_sub_practice_arr.push(projectWiseRevenue[emp_internal_id].sub_practice);
						
					}
				}
			}
			i_frst_row = 1;
	
			var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;			
		}
	}
	
	//prcnt completed for project
	
	html['monthlyrevenue'].push('Monthly Percent Complete');

	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(a_amount[i_amt]);
	}
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_to_diaplay = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_to_diaplay = parseFloat(f_prcnt_to_diaplay) * 100;
		
		
		html['monthlyrevenue'].push(parseFloat(f_prcnt_to_diaplay).toFixed(1)+' %');
		
		
		f_total_prcnt_complt = parseFloat(f_total_prcnt_complt) + parseFloat(f_prcnt_to_diaplay);
		
	}
	
//	html += "<td class='projected-amount'>";
	html['monthlyrevenue'].push(parseFloat(f_total_prcnt_complt).toFixed(1)+' %');
	//html += "</td>";
	
	//html += "</tr>";
	
	// total revenue recognized project level

	html['rev_rec_permnth'].push('Revenue to be reconized/month'); //Total Project Revenue as per Plan
	
	
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		
		html['rev_rec_permnth'].push(format2(a_amount[i_amt]));
				
	}
	
	html['rev_rec_permnth'].push(format2(i_total_row_revenue));
		
	// actual revenue that will be recognized

	html['act_rev_rec_mnth'].push('Actual revenue recognized/Revenue forecast'); //Actual Revenue to Recognize
	
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	//nlapiLogExecution('audit','proj level len:- ',a_recognized_revenue_total_proj_level.length);
	
	var f_total_revenue_proj_level = 0;
	if(parseInt(i_year_project) != parseInt(i_current_year))
	{
		var i_total_project_tenure = 11 - parseInt(i_project_mnth);
		i_current_month = i_current_month + i_total_project_tenure + 1;
		
		for(var i_revenue_index=0; i_revenue_index<i_current_month; i_revenue_index++)
		{
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
			
			html['act_rev_rec_mnth'].push(format2(a_recognized_revenue_total_proj_level[i_revenue_index]));
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		}
	}
	else
	{
		i_current_month = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
		{
			//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
			
			
			html['act_rev_rec_mnth'].push(format2(a_recognized_revenue_total_proj_level[i_revenue_index]));
			
			f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		}
	}
	
	
	html['act_rev_rec_mnth'].push(format2(f_current_month_actual_revenue_total_proj_level));
	
	
	f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(f_current_month_actual_revenue_total_proj_level);
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		
		html['act_rev_rec_mnth'].push(format2(a_amount[i_amt]));
				
		f_total_revenue_proj_level = parseFloat(f_total_revenue_proj_level) + parseFloat(a_amount[i_amt]);
	}
	

	html['act_rev_rec_mnth'].push(format2(f_total_revenue_proj_level));
	
	// cumulative total project level

	html['cummulative_mnth'].push('Cumulative Revenue');
	
	var f_project_level_cumulative = 0;
	for(var i_revenue_index=0; i_revenue_index<=i_current_month-1; i_revenue_index++)
	{
		//i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(a_recognized_revenue_total_proj_level[i_revenue_index]);
		html['cummulative_mnth'].push(format2(f_project_level_cumulative));
		
	}
	
	f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(f_current_month_actual_revenue_total_proj_level);
	
	html['cummulative_mnth'].push(format2(f_project_level_cumulative));
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		f_project_level_cumulative = parseFloat(f_project_level_cumulative) + parseFloat(a_amount[i_amt]);
		html['cummulative_mnth'].push(format2(f_project_level_cumulative));
	}
	
	
	html['cummulative_mnth'].push(format2(f_total_revenue_proj_level));
	nlapiLogExecution('debug','Value',JSON.stringify(html));
	
	return html;
}

function generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	if(mode != 5)
	{	
		html += "<td>";
		html += "Role";
		html += "</td>";
	
		if(mode == 1 || mode == 2 || mode == 6)
		{
			html += "<td>";
			html += "Level";
			html += "</td>";
			
			if(mode == 6)
			{
				html += "<td>";
				html += "No. of resources";
				html += "</td>";
				
				html += "<td>";
				html += "Allocation start date";
				html += "</td>";
				
				html += "<td>";
				html += "Allocation end date";
				html += "</td>";
				
				html += "<td>";
				html += "Percent allocated";
				html += "</td>";
			}
			
			html += "<td>";
			html += "Location";
			html += "</td>";
		}
	}
	else
	{
		html += "<td>";
		html += "Revenue Share";
		html += "</td>";
	}
	
	if(mode != 5 && mode != 6)
	{
		var today = new Date();
		var currentMonthName = getMonthName(nlapiDateToString(today));
		
		for (var j = 0; j < monthBreakUp.length; j++)
		{
			var months = monthBreakUp[j];
			var s_month_name = getMonthName(months.Start); // get month name
			var s_year = nlapiStringToDate(months.Start)
			s_year = s_year.getFullYear();
			s_month_name = s_month_name+'_'+s_year;
					
			html += "<td>";
			html += s_month_name;
			html += "</td>";
		}
		
		if (mode == 2)
		{
			html += "<td>";
			html += "Total";
			html += "</td>";
		}
	}
		
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			var b_display_reveue_share = 'F';
			if(a_sub_prac_already_displayed.indexOf(projectWiseRevenue[emp_internal_id].sub_practice) < 0 && mode == 5)
			{
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
				
				b_display_reveue_share = 'T';
				a_sub_prac_already_displayed.push(projectWiseRevenue[emp_internal_id].sub_practice);
			}
			
			if(mode != 5)
			{
				html += "<tr>";
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			}
			
			if (mode != 5)
			{
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
				
				if (mode == 1 || mode == 2 || mode == 6)
				{
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].level_name;
					html += "</td>";
					
					if(mode == 6)
					{
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].no_of_resources;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_strt_date;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].allocation_end_date;
						html += "</td>";
						
						html += "<td class='label-name'>";
						html += projectWiseRevenue[emp_internal_id].total_allocation;
						html += "</td>";
					}
					
					html += "<td class='label-name'>";
					html += projectWiseRevenue[emp_internal_id].location_name;
					html += "</td>";
				}
			}
			else
			{
				if (b_display_reveue_share == 'T')
				{
					html += "<td class='label-name'>";
					html += ''+s_currency_symbol_proj+' '+format2(projectWiseRevenue[emp_internal_id].revenue_share);
					html += "</td>";
				}
			}
			
			if (mode != 5 && mode != 6)
			{
				var i_sub_practice_internal_id = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1)&& i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						
						if (mode == 2)
						{
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							html += format2(total_revenue_format);
						}
						else if (mode == 1)
						{
							html += "<td class='monthly-amount'>";
							html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(1);
							
							if(i_sub_practice_internal_id = 0)
							{
								i_sub_practice_internal_id = projectWiseRevenue[emp_internal_id].sub_practice;
							}
						}
						else if (mode == 3 || mode == 4)
						{
							var f_amount = projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount;
							var f_total_amount = projectWiseRevenue[emp_internal_id].total_revenue_for_tenure;
							
							var f_prcnt_cost = parseFloat(f_amount) / parseFloat(f_total_amount);
							var f_prcnt_cost_ = parseFloat(f_prcnt_cost) * parseFloat(100);
							
							if (mode == 3)
							{
								f_prcnt_cost_ = parseFloat(f_prcnt_cost_).toFixed(1);
								html += "<td class='monthly-amount'>";
								html += f_prcnt_cost_+' %';
							}
							else
							{
								html += "<td class='projected-amount'>";
								var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
								if(projectWiseRevenue[emp_internal_id].sub_practice == 316)
									nlapiLogExecution('audit','prcnt:- '+f_prcnt_cost,f_revenue_share);
									
								f_revenue_share = (parseFloat(f_prcnt_cost_) * parseFloat(f_revenue_share)) / 100;
								html += format2(f_revenue_share);
							}
						}
						
						html += "</td>";
					}
				}
			}
			
			if (mode == 2)
			{
				html += "<td class='monthly-amount'>";
				html += format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				html += "</td>";
			}
			
			html += "</tr>";			
		}
	}

	html += "</table>";
	
	return html;
}

function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function getCurrency_Symbol(s_proj_currency)
{
	var s_currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'GBP': '£', // British Pound Sterling
    'INR': '₹', // Indian Rupee
	};
	
	if(s_currency_symbols[s_proj_currency]!==undefined) {
	    return s_currency_symbols[s_proj_currency];
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth()+1, 1);
		
		var endDate = new Date(nxt_mnth_strt_date - 1);
				
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
		throw err;
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0,
			recognized_revenue : 0,
			actual_revenue : 0,
			mnth_strt : 0,
			mnth_end : 0,
			prcent_complt : 0
		};
	}
}

function generate_previous_effrt_revenue(s_effrt_json,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project)
{
	if(s_effrt_json)
	{
		var i_practice_previous = 0;
		var f_total_revenue_for_tenure = 0;
		//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
		var s_entire_json_clubed = JSON.parse(s_effrt_json);
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
			for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
			{
				var i_practice = a_row_json_data[i_row_json_index].prac;
				var s_practice = a_row_json_data[i_row_json_index].prac_text;
				var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
				var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
				var i_role = a_row_json_data[i_row_json_index].role;
				var s_role = a_row_json_data[i_row_json_index].role_text;
				var i_level = a_row_json_data[i_row_json_index].level;
				var s_level = a_row_json_data[i_row_json_index].level_text;
				var i_location = a_row_json_data[i_row_json_index].loc;
				var s_location = a_row_json_data[i_row_json_index].loc_text;
				var f_revenue = a_row_json_data[i_row_json_index].cost;
				if (!f_revenue) 
					f_revenue = 0;
				
				var f_revenue_share = a_row_json_data[i_row_json_index].share;
				if (!f_revenue_share) 
					f_revenue_share = 0;
					
				var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
				var s_mnth_strt_date = '1/31/2017';
				var s_mnth_end_date = '31/31/2017';
				var s_mnth = a_row_json_data[i_row_json_index].mnth;
				var s_year = a_row_json_data[i_row_json_index].year;
				
				var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
				
				if (!f_revenue_recognized) 
					f_revenue_recognized = 0;
				
				//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
				
				var s_month_name = s_mnth + '_' + s_year;
				
				var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
				total_revenue = parseFloat(total_revenue).toFixed(2);
				
				if (i_practice_previous == 0) {
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
				
				if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
					f_total_revenue_for_tenure = 0;
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
			
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
				
				var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
				
				if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
						practice_name: s_practice,
						sub_prac_name: s_sub_practice,
						sub_practice: i_sub_practice,
						role_name: s_role,
						level_name: s_level,
						location_name: s_location,
						total_revenue_for_tenure: f_total_revenue_for_tenure,
						revenue_share: f_revenue_share,
						RevenueData: {}
					};
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
				else {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
			}
		}
	}
	
	//return projectWiseRevenue_previous_effrt;
}

function find_recognized_revenue_prev(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_prev_subprac_searched_once)
{
	var f_recognized_amount = 0;
	
	if(a_prev_subprac_searched_once.indexOf(subpractice) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{
								if(year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									a_prev_subprac_searched_once.push(subpractice);
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function find_recognized_revenue(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year)
{
	var f_recognized_amount = 0;
	
	if(func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{				
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice ==  a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{	
								if(parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized))
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
									a_subprac_searched_once.push(
																{
																	'subpractice': subpractice,
																	'month': month,
																	'year': year
																});
									//a_subprac_searched_once_month.push(month);
									//a_subprac_searched_once_year.push(year);
									
									if(subpractice == 325)
									{
										nlapiLogExecution('audit','month:- '+month,'year:- '+year);
										nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}
function func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year)
{
	var i_return_var = -1;
	for(var i_loop=0; i_loop<a_subprac_searched_once.length; i_loop++)
	{
		if(a_subprac_searched_once[i_loop].subpractice == subpractice)
		{
			if(a_subprac_searched_once[i_loop].month == month)
			{
				if(a_subprac_searched_once[i_loop].year == year)
				{
					i_return_var = i_loop;
					break;
				}
			}
		}
	}
	
	return i_return_var;
}
				
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}
function down_excel_function(bill_data_arr,email)
{
                try
                {
                                var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
                                '<head>'+
                                '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
                                    '<x:activepane>0</x:activepane>'+// 0
                                    '<x:panes>'+
                                                '<x:pane>'+
                                                '<x:number>3</x:number>'+
                                                '</x:pane>'+
                                                '<x:pane>'+
                                    '<x:number>1</x:number>'+
                                    '</x:pane>'+
                                    '<x:pane>'+
                                                '<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
                                '</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
                                '</x:excelworkbook>'+
        
       
                                                
                                          
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
                                'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
                                '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
                                '<style>'+

                                '<!-- /* Style Definitions */'+

                               

                                'div.Section1'+
                                '{ page:Section1;}'+

                                'table#hrdftrtbl'+
                                '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

                                '</style>'+
                                
                                
                                '</head>'+
               

                                '<body>'+
                                
                                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
                                
                                var strVar2 = '';    
                                strVar2 += "<table width=\"100%\">";
								strVar2+= '<table width="100%" border="1">';
								var current_date=new Date();
								
								var i_year = current_date.getFullYear();
								var excel_mnths=MonthNames(i_year);
								if (bill_data_arr)
                                {
									
										strVar2 += "<tr>";
										strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Project ID</td>";
										strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Project</td>";
										strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999'\>Currency</td>";
										strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Practice</td>";
										strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Sub-Practice</td>";
										strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>   </td>";
                                        nlapiLogExecution('audit','down_excel_function transcation length:- ',bill_data_arr.length);
										
                                         for(mnth_ind=0;mnth_ind<excel_mnths.length;mnth_ind++)
											{
												strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>"+excel_mnths[mnth_ind]+"</td>";
											}
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" style='background-color:#999999' \>Total</td>";
												   strVar2 += "<\/tr>";
												   	strVar2 += "<tr>";
										for(var bill_data_arr_len=0;bill_data_arr_len < bill_data_arr.length;bill_data_arr_len++)
										{	     
											var bill_arr=bill_data_arr[bill_data_arr_len].excel_data;
												for (var jk = 0; jk < bill_arr.length; jk++)
                                                {
													var act=bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev;
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].proj+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_id+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].currency+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_name+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].practice+ "</td>";
													for(var act_len=0; act_len< act.length;act_len++)
													{
														if(act_len==0)
														{
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]+ "</td>";
														}
														var mn_len=act_len-1;
														var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
														var mon=excel_mnths.indexOf(mn);
														if(mon>=0)
														{
															if((mon>0)&&(act_len==1) )
															{
																for(var prev_mn=0;prev_mn<mon;prev_mn++)
																{
																	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																}
															}
															if(mn==excel_mnths[mon])
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]+ "</td>";
																
														}
														if(act_len==act.length-1)
														{
															var mn_len=act_len-2;
															var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
															var mon=excel_mnths.indexOf(mn);
															for(var mnth_indx=1;mnth_indx<excel_mnths.length-mon;mnth_indx++)
															{
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
															}
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].actual_rev[act_len]+ "</td>";
														}
														
                                                     }														
													strVar2 += "<\/tr>";
													strVar2+="<tr>";
													var cum=bill_data_arr[bill_data_arr_len].excel_data[jk].cumm_rev;
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].proj+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_id+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].currency+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_name+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].practice+ "</td>";
													for(var cum_len=0; cum_len< cum.length;cum_len++)
													{	
														if(cum_len==0)
														{
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].cumm_rev[cum_len]+ "</td>";
														}
														var mn_len=cum_len-1;
														var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
														var mon=excel_mnths.indexOf(mn);
														if(mon>=0)
														{
															if((mon>0)&&(cum_len==1))
															{
																for(var prev_mn=0;prev_mn<mon;prev_mn++)
																{
																	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																}
															}
															if(mn==excel_mnths[mon])
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].cumm_rev[cum_len]+ "</td>";
														}
														if(cum_len==cum.length-1)
														{
															var mn_len=cum_len-2;
															var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
															var mon=excel_mnths.indexOf(mn);
															for(var mnth_indx=1;mnth_indx<excel_mnths.length-mon;mnth_indx++)
															{
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
															}
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].cumm_rev[cum_len]+ "</td>";
														}
													}															
                                                    strVar2 += "<\/tr>";
													if(_logValidation(bill_data_arr[bill_data_arr_len].excel_data[jk].monthlyrevenue))
													{
														strVar2+="<tr>";
														var mnthly=bill_data_arr[bill_data_arr_len].excel_data[jk].monthlyrevenue;
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].proj+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_id+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].currency+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
														for(var mnthly_len=0; mnthly_len< mnthly.length;mnthly_len++)
														{
															if(mnthly_len==0)
															{
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].monthlyrevenue[mnthly_len]+ "</td>";
															}
															var mn_len=mnthly_len-1;
															var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
															var mon=excel_mnths.indexOf(mn);
															if(mon>=0)
															{
																if((mon>0)&& (mnthly_len==1))
																{
																	for(var prev_mn=0;prev_mn<mon;prev_mn++)
																	{
																		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																	}
																}
																if(mn==excel_mnths[mon])
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].monthlyrevenue[mnthly_len]+ "</td>";
															}
															if(mnthly_len==mnthly.length-1)
															{
																var mn_len=mnthly_len-2;
																var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
																var mon=excel_mnths.indexOf(mn);
																for(var mnth_indx=1;mnth_indx<excel_mnths.length-mon;mnth_indx++)
																{
																	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																}
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].monthlyrevenue[mnthly_len]+ "</td>";
															}
														}															
														strVar2 += "<\/tr>";
														strVar2+="<tr>";
														var rev_mnthly=bill_data_arr[bill_data_arr_len].excel_data[jk].rev_rec_permnth;
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].proj+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_id+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].currency+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
														for(var rev_mnthly_len=0; rev_mnthly_len< rev_mnthly.length;rev_mnthly_len++)
														{
															if(rev_mnthly_len==0)
															{
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].rev_rec_permnth[rev_mnthly_len]+ "</td>";
															}
															var mn_len=rev_mnthly_len-1;
															var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
															var mon=excel_mnths.indexOf(mn);
															if(mon>=0)
															{
																if((mon>0)&& (rev_mnthly_len==1))
																{
																	for(var prev_mn=0;prev_mn<mon;prev_mn++)
																	{
																		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																	}
																}
																if(mn==excel_mnths[mon])
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].rev_rec_permnth[rev_mnthly_len]+ "</td>";
															}
															if(rev_mnthly_len==rev_mnthly.length-1)
															{
																var mn_len=rev_mnthly_len-2;
																var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
																var mon=excel_mnths.indexOf(mn);
																for(var mnth_indx=1;mnth_indx<excel_mnths.length-mon;mnth_indx++)
																{
																	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																}
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].rev_rec_permnth[rev_mnthly_len]+ "</td>";
															}
														}															
                                                    strVar2 += "<\/tr>";
													strVar2+="<tr>";
													var act_mnthly=bill_data_arr[bill_data_arr_len].excel_data[jk].act_rev_rec_mnth;
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].proj+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_id+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].currency+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
													for(var act_mnthly_len=0; act_mnthly_len< act_mnthly.length;act_mnthly_len++)
													{
														if(act_mnthly_len==0)
														{
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].act_rev_rec_mnth[act_mnthly_len]+ "</td>";
														}
														var mn_len=act_mnthly_len-1;
														var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
														var mon=excel_mnths.indexOf(mn);
														if(mon>=0)
														{
															if((mon>0) && (act_mnthly_len==1))
															{
																for(var prev_mn=0;prev_mn<mon;prev_mn++)
																{
																	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																}
															}
															if(mn==excel_mnths[mon])
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].act_rev_rec_mnth[act_mnthly_len]+ "</td>";
														}
														if(act_mnthly_len==act_mnthly.length-1)
														{
															var mn_len=act_mnthly_len-2;
															var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
															var mon=excel_mnths.indexOf(mn);
															for(var mnth_indx=1;mnth_indx<excel_mnths.length-mon;mnth_indx++)
															{
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
															}
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].act_rev_rec_mnth[act_mnthly_len]+ "</td>";
														}
													}															
                                                    strVar2 += "<\/tr>";
													strVar2+="<tr>";
													var cum_mnthly=bill_data_arr[bill_data_arr_len].excel_data[jk].cummulative_mnth;
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].proj+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].project_id+ "</td>";
														strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].currency+ "</td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>  </td>";
													strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \> </td>";
													for(var cum_mnthly_len=0; cum_mnthly_len< cum_mnthly.length;cum_mnthly_len++)
													{
														if(cum_mnthly_len==0)
														{
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].cummulative_mnth[cum_mnthly_len]+ "</td>";
														}
														var mn_len=cum_mnthly_len-1;
														var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
														var mon=excel_mnths.indexOf(mn);
														if(mon>=0)
														{
															if((mon>0)&& (cum_mnthly_len==1))
															{
																for(var prev_mn=0;prev_mn<mon;prev_mn++)
																{
																	strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
																}
															}
															if(mn==excel_mnths[mon])
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].cummulative_mnth[cum_mnthly_len]+ "</td>";
														}
														if(cum_mnthly_len==cum_mnthly.length-1)
														{
															var mn_len=cum_mnthly_len-2;
															var mn=bill_data_arr[bill_data_arr_len].excel_data[jk].st_date[mn_len];
															var mon=excel_mnths.indexOf(mn);
															for(var mnth_indx=1;mnth_indx<excel_mnths.length-mon;mnth_indx++)
															{
																strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \></td>";
															}
															strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[bill_data_arr_len].excel_data[jk].cummulative_mnth[cum_mnthly_len]+ "</td>";
														}
													}															
                                                    strVar2 += "<\/tr>";
													}
                                                }
												//strVar2+="<tr></tr>";
										}
                                }
                                else
                                {
                                                
                                }
                                                                                
                                strVar2 += "<\/table>";                                 
                                strVar1 = strVar1 + strVar2;
                                var file = nlapiCreateFile('Project Revenue Report FP Milestone.xls', 'XMLDOC', strVar1);
                                nlapiLogExecution('debug','file:- ',file);
                                var rec=email;
								//var email-id ='sai.vannamareddy@brillio.com';
                                var a_emp_attachment = new Array();
                                a_emp_attachment['entity'] = 62082;
                                nlapiSendEmail(7905,email , 'Project Revenue Recognized Report', 'Please find the attachement of project rev rec report',null, 'sai.vannamareddy@brillio.com',a_emp_attachment, file, true, null, null);
                                
                }
                catch(err)
                {
                                nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
                }
}