//Nihal Mulani

function afterSubmitCheckFields(type)
{
	nlapiLogExecution('Debug','type ',type);
	try
	{
		if( type == 'edit' || type == 'xedit')
		{
			var oldRecord = nlapiGetOldRecord();

			var oldRecord_opportunity_name = oldRecord.getFieldValue('custrecord_opportunity_name_sfdc');
			var oldRecord_opportunity_id = oldRecord.getFieldValue('custrecord_opportunity_id_sfdc');
			var oldRecord_customer_internal_id = oldRecord.getFieldValue('custrecord_customer_internal_id_sfdc');
			var oldRecord_sales_act_sfd = oldRecord.getFieldText('custrecord_projection_status_sfdc');

			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var opportunity_name = recordObject.getFieldValue('custrecord_opportunity_name_sfdc');
			var opportunity_id = recordObject.getFieldValue('custrecord_opportunity_id_sfdc');
			var customer_internal_id = recordObject.getFieldValue('custrecord_customer_internal_id_sfdc');
			var sales_act_sfd = recordObject.getFieldText('custrecord_projection_status_sfdc');
			var sales_act_sfd = recordObject.getFieldValue('custrecord_projection_status_sfdc');
			var practice_internal_id_sfdc = recordObject.getFieldValue('custrecord_practice_internal_id_sfdc');
			nlapiLogExecution('Debug','practice_internal_id_sfdc',practice_internal_id_sfdc);
			var practiceId='';
			//var practiceName = '';
			var parentPractice = nlapiLookupField('department',practice_internal_id_sfdc,'custrecord_parent_practice');
			nlapiLogExecution('Debug','parentPractice',parentPractice);
			if(parentPractice)
			{
				practiceId = parentPractice;
				//practiceName = nlapiLookupField('department',practice_internal_id_sfdc,'custrecord_parent_practice',true);
			}
			else
			{
				practiceId = practice_internal_id_sfdc;
				//practiceName = recordObject.getFieldText('custrecord_practice_internal_id_sfdc');
			}
			if(oldRecord_opportunity_name == opportunity_name && oldRecord_opportunity_id == opportunity_id && oldRecord_customer_internal_id == customer_internal_id && oldRecord_sales_act_sfd == sales_act_sfd)
			{
				nlapiLogExecution('Debug','Message','Record 4 fields are not updated ');
			}
			else
			{

				var body = {};
				var method;
				method = "POST";
				body.internalId = Number(nlapiGetRecordId());
				body.name = opportunity_name;
				body.opportunityId = opportunity_id;
				body.accountInternalId = Number(customer_internal_id);
				body.revenueStatus = sales_act_sfd;
				//body.practiceName = practiceName;
				body.practiceInternalId = Number(practiceId);
				
				nlapiLogExecution("DEBUG", "body edit Mode: ", JSON.stringify(body));
				var url = "https://fuelnode1.azurewebsites.net/opportunity";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				nlapiLogExecution("DEBUG", "response edit Mode:body ", body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response edit Mode: ", JSON.stringify(response));
			}
		}
		else if(type == 'create')
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var opportunity_name = recordObject.getFieldValue('custrecord_opportunity_name_sfdc');
			var opportunity_id = recordObject.getFieldValue('custrecord_opportunity_id_sfdc');
			var sales_act_sfd = recordObject.getFieldText('custrecord_projection_status_sfdc');
			var s_customer = recordObject.getFieldValue('custrecord_customer_sfdc');
			var practice_internal_id_sfdc = recordObject.getFieldValue('custrecord_practice_internal_id_sfdc');

			var practiceInternalId='';
			var parentPractice = nlapiLookupField('department',practice_internal_id_sfdc,'custrecord_parent_practice');
			nlapiLogExecution('Debug','parentPractice cretae',parentPractice);
			if(parentPractice)
			{
				practiceInternalId = parentPractice;
			}
			else
			{
				practiceInternalId = practice_internal_id_sfdc;
			}
			nlapiLogExecution('Debug','s_customer ',s_customer);
			var customer_internal_id = "";
			if(s_customer)
			{
				var customerSearch = nlapiSearchRecord("customer",null,
						[
							["entityid","is",regExReplacer(s_customer)]
							// ["custentity_sfdc_account_id","is",s_customer]
							], 
							[
								new nlobjSearchColumn("internalid")
								]
				);
				if(customerSearch)
				{
					nlapiLogExecution('Debug','In customer Search');
					customer_internal_id = customerSearch[0].getValue('internalid');
				}
				else
				{
					//recObj.setFieldValue('custrecord_account_name_sfdc',s_customer);
					recordObject.setFieldValue('custrecord_account_type_sfdc','Prospect');
				}
			}

			var body = {};
			var method;
			method = "POST";
			body.internalId = Number(nlapiGetRecordId());
			body.name = opportunity_name;
			body.opportunityId = opportunity_id;
			body.accountInternalId = Number(customer_internal_id);
			body.revenueStatus = sales_act_sfd;
			//body.practiceName = practiceName;
			body.practiceInternalId = Number(practiceInternalId)
			nlapiLogExecution("DEBUG", "body create Mode: ", JSON.stringify(body));
			var url = "https://fuelnode1.azurewebsites.net/opportunity";
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			body = JSON.stringify(body);
			nlapiLogExecution("DEBUG", "response Create Mode:body ", body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}
function beforeSubmitCheckFields(type)
{
	if(type == 'delete')
	{
		try
		{
			var i_recordId = nlapiGetRecordId();
			var i_user = nlapiGetFieldValues('custrecord_fuel_anchor_employee');
			nlapiLogExecution('debug','i_user ',i_user);
			var body = {};
			var method = "DELETE";
			var url = "https://fuelnode1.azurewebsites.net/opportunity/"+i_recordId;
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','Delete Mode: url  ',url);
			nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}

//END BEFORE LOAD ====================================================


function _logValidation(value) 
{
	if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}
//END FUNCTION =====================================================

/************************************************/
function getEmpIds(s_employee)
{
	var resultArray = new Array();
	if(_logValidation(s_employee))
	{
		nlapiLogExecution('Debug','s_employee in function',s_employee);
		//var temp = s_employee.split(',');
		for(var i=0; i<s_employee.length;i++)
		{
			resultArray.push(s_employee[i]);
		}
	}
	nlapiLogExecution('debug','resultArray ',resultArray);
	return resultArray;
}
/************* Lookup Customer *********************/
function regExReplacer(str)
{
	return str.replace(/\n|\r/g, "");
}