/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Dec 2014     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function muSendEmail(recType, recId) {
	var emailId = nlapiLookupField(recType, recId, 'email');
	
	//var strText = nlapiMergeRecord(6, recType, recId);
	
	var template = nlapiLoadRecord('emailtemplate', 9);

	if (template.getFieldValue('mediaitem') != null) {
			var media = template.getFieldValue('mediaitem');
			// do something with the media...
	} else {
			var strText = template.getFieldValue('content');
			// do something with the content...
			var rec = nlapiLoadRecord(recType, recId);
			
			var subject = template.getFieldValue('subject');
			
			var renderer = nlapiCreateTemplateRenderer();
			renderer.setTemplate(strText);
			renderer.addRecord('employee', rec);
			
			var body = renderer.renderToString();
			try
			{
				nlapiSendEmail(442, emailId, subject, body);
			}
			catch(e)
			{
				nlapiLogExecution('ERROR','Error',e.message);
			}
			
			nlapiLogExecution('AUDIT', 'Email Sent ' + emailId, body);
	}
}