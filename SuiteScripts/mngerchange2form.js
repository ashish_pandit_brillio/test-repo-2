/**
 * Form to change TA / EA for resource allocated in a project
 * 
 * Version Date Author Remarks 1.00 29 Sep 2015 nitish.mishra
 * 
 */

function suitelet(request, response) {

	try {
		var method = request.getMethod();
		nlapiLogExecution('debug', 'method', method);

		if (method == "GET") {

			var projectId = request.getParameter('project');

			if (projectId) {
				renderProjectDetailsPage(projectId);
			} else {
				renderProjectSelectionPage();
			}
		} else {
			method = request.getParameter('custpage_next_step');
			nlapiLogExecution('debug', 'method', method);
			
			if(method == 'GoBack')
			{
				nlapiSetRedirectURL('SUITELET', '661', 'customdeploy_sut_project_wise_ta_ea', null, null); 	
			}

			if (method == "DisplayProjectDetails") {
				renderProjectDetailsPage(request);
			} else if (method == "SubmitProjectDetails") {
				renderResultPage(request);
			} else {
				renderProjectSelectionPage();
			}
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function renderProjectSelectionPage() {
	try {
		var getForm = nlapiCreateForm('Manager Change Form', false);
		getForm.addFieldGroup('custpage_1', 'Select a project');
		getForm.addField('custpage_project', 'select', 'Project', 'job',
		        'custpage_1').setMandatory(true);
		getForm.addField('custpage_next_step', 'text', 'Next Step')
		        .setDisplayType('hidden').setDefaultValue(
		                'DisplayProjectDetails');

		// getForm.setScript('customscript_cs_ta_ea_project');
		// getForm.addButton('custpage_btn_1', 'Get Project Details',
		// 'openProjectDetails');
		//getForm.addSubmitButton('Get Project Resources');
		getForm.setScript('customscript1534 ');
		getForm.addButton('custpage_btn_time_sbmt',
		        'Get Project Resources','redirectToEmpForm' );
		response.writePage(getForm);
	} catch (err) {
		nlapiLogExecution('ERROR', 'renderProjectSelectionPage', err);
		throw err;
	}
}

function renderProjectDetailsPage(request) {
	try {
		//var projectId = request.getParameter('custpage_project');
		var projectId = request;
		if (projectId) {
			var getForm = nlapiCreateForm('Manager Change Form', false);

			// Project
			getForm.addFieldGroup('custpage_1', 'Selected Project');
			getForm.addField('custpage_project', 'select', 'Project', 'job',
			        'custpage_1').setDisplayType('inline').setDefaultValue(
			        projectId);

			var projectDetails = nlapiLookupField('job', projectId, [
			        'custentity_projectmanager', 'custentity_deliverymanager',
			        'custentity_clientpartner' ]);

			// Project Manager
			getForm.addFieldGroup('custpage_2', 'Project Manager');
			getForm.addField('custpage_pm_old', 'select', 'Old', 'employee',
			        'custpage_2').setDisplayType('inline').setDefaultValue(
			        projectDetails.custentity_projectmanager);
			getForm.addField('custpage_pm_new', 'select', 'New', 'employee',
			        'custpage_2');

			// Delivery Manager
			getForm.addFieldGroup('custpage_3', 'Delivery Manager');
			getForm.addField('custpage_dm_old', 'select', 'Old', 'employee',
			        'custpage_3').setDisplayType('inline').setDefaultValue(
			        projectDetails.custentity_deliverymanager);
			getForm.addField('custpage_dm_new', 'select', 'New', 'employee',
			        'custpage_3');

			// Client Partner
			getForm.addFieldGroup('custpage_4', 'Client Partner');
			getForm.addField('custpage_cp_old', 'select', 'Old', 'employee',
			        'custpage_4').setDisplayType('inline').setDefaultValue(
			        projectDetails.custentity_clientpartner);
			getForm.addField('custpage_cp_new', 'select', 'New', 'employee',
			        'custpage_4');

			// get all allocated resources
			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('project', null, 'anyof',
			                        projectId),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        'today'),
			                new nlobjSearchFilter(
			                        'custentity_implementationteam',
			                        'employee', 'is', 'F'),
			                new nlobjSearchFilter(
			                        'custentity_employee_inactive', 'employee',
			                        'is', 'F') ], [
			                new nlobjSearchColumn('resource'),
			                new nlobjSearchColumn('timeapprover', 'employee'),
			                new nlobjSearchColumn('approver', 'employee') ]);

			var allocationData = [];

			if (allocationSearch) {
				allocationSearch.forEach(function(allocation) {
					allocationData.push({
					    employee : allocation.getValue('resource'),
					    ta_old : allocation
					            .getValue('timeapprover', 'employee'),
					    ea_old : allocation.getValue('approver', 'employee')
					});
				});
			}

			// add resource sublist
			getForm.addTab('custpage_employee', 'Employees');
			getForm.addSubTab('custpage_expense_approver', 'Set Approver', 'custpage_employee');
			var subList = getForm.addSubList('custpage_resource', 'list',
			        'Resources','custpage_expense_approver');
			
			getForm.addField('custpage_all_ex', 'select', 'New EA Approver',
			        'employee', 'custpage_expense_approver');
			getForm.addField('custpage_all_ta', 'select', 'New TA Approver',
			        'employee', 'custpage_expense_approver'); 
			
			subList.addButton('custpage_btn_expense',
			        'Update All EA', 'updateExpenseApprover');
			nlapiLogExecution('Debug', 'Update All EA button', '');
			
			subList.addButton('custpage_btn_expense_2',
			        'Update Selected EA', 'updateSelectedExpenseApprover');
			
            getForm.setScript('customscript_sut_project_wise_ta_ea');
			
			subList.addButton('custpage_btn_time',
			        'Update All TA', 'updateTimeApprover');
			nlapiLogExecution('Debug', 'Update all EA button', '');
			subList.addButton('custpage_btn_time_2',
			        'Update Selected TA', 'updationofSelectedTimeApprover');
			nlapiLogExecution('Debug', 'Update selectd TA button', '');
			
			
			subList.addField('employee', 'select', 'Employee', 'employee')
			        .setDisplayType('inline');
			subList.addField('ta_old', 'select', 'TA Old', 'employee')
			        .setDisplayType('inline');
			subList.addField('ea_old', 'select', 'EA Old', 'employee')
			        .setDisplayType('inline');
			subList.addField('ta_new', 'select', 'TA New', 'employee');
			subList.addField('ea_new', 'select', 'EA New', 'employee');
			subList.addField('select', 'checkbox', 'Select ');
			subList.setLineItemValues(allocationData);

			// add buttons and hidden fields
			getForm.addField('custpage_next_step', 'text', 'Next Step')
			        .setDisplayType('hidden').setDefaultValue(
			                'SubmitProjectDetails');

			getForm.addSubmitButton('Submit Changes');
			response.writePage(getForm);
		} else {
			renderProjectSelectionPage();
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'renderProjectDetailsPage', err);
		throw err;
	}
}

function renderResultPage(request) {
	try {
		var projectId = request.getParameter('custpage_project');

		var pm_new = request.getParameter('custpage_pm_new');
		var dm_new = request.getParameter('custpage_dm_new');
		var cp_new = request.getParameter('custpage_cp_new');

		var jobFields = [], jobValues = [];
		var mailContent = "";

		if (pm_new) {
			jobFields.push('custentity_projectmanager');
			jobValues.push(pm_new);
			mailContent += "Project Manager : "
			        + request.getParameter('custpage_pm_new_name');
		}

		if (dm_new) {
			jobFields.push('custentity_deliverymanager');
			jobValues.push(dm_new);
			mailContent += "Delivery Manager : "
			        + request.getParameter('custpage_dm_new_name');
		}

		if (cp_new) {
			jobFields.push('custentity_clientpartner');
			jobValues.push(cp_new);
			mailContent += "Client Partner : "
			        + request.getParameter('custpage_cp_new_name');
		}

		if (jobFields.length > 0) {
			try {
				nlapiSubmitField('job', projectId, jobFields, jobValues);
				nlapiLogExecution('debug', 'project details updated', projectId);
			} catch (ex) {
				nlapiLogExecution('error', 'project update failed : '
				        + projectId, ex);
			}
		}

		var resourceCount = request.getLineItemCount('custpage_resource');

		for (var linenum = 1; linenum <= resourceCount; linenum++) {
			var employeeId = request.getLineItemValue('custpage_resource',
			        'employee', linenum);
			try {
				var ta_new = request.getLineItemValue('custpage_resource',
				        'ta_new', linenum), ea_new = request.getLineItemValue(
				        'custpage_resource', 'ea_new', linenum), employeeFields = null, employeeValues = null;

				if (ta_new && ea_new) {
					employeeFields = [ 'timeapprover', 'approver' ];
					employeeValues = [ ta_new, ea_new ];
				} else if (ta_new) {
					employeeFields = [ 'timeapprover' ];
					employeeValues = [ ta_new ];
				} else if (ea_new) {
					employeeFields = [ 'approver' ];
					employeeValues = [ ea_new ];
				}

				if (employeeFields) {
					nlapiSubmitField('employee', employeeId, employeeFields,
					        employeeValues);
					nlapiLogExecution('debug', 'employee details updated',
					        employeeId);
				}
			} catch (ex) {
				nlapiLogExecution('error', 'employee updation failed : '
				        + employeeId, ex);
			}
		}

		// add buttons and hidden fields
		var getForm = nlapiCreateForm('Manager Change Form', false);
		getForm.addField('custpage_next_step', 'text', 'Next Step')
		        .setDisplayType('hidden').setDefaultValue('GoBack');
		getForm.addSubmitButton('Finish');
		response.writePage(getForm);
	} catch (err) {
		nlapiLogExecution('ERROR', 'renderResultPage', err);
		throw err;
	}
}

// --------------Client Script

function openProjectDetails() {
	var project = nlapiGetFieldValue('custpage_project');

	if (project) {
		var url = nlapiResolveURL('SUITELET',
		        'customscript_sut_project_wise_ta_ea',
		        'customdeploy_sut_project_wise_ta_ea')
		        + "&project=" + project;

		window.location = url;
	} else {
		alert("No Project Selected");
	}
}
function updateSelectedExpenseApprover() {
	
	nlapiLogExecution('Debug', 'updateSelectedExpenseApprover fun called', '');
	var value = nlapiGetFieldValue('custpage_all_ex');
	nlapiLogExecution('Debug', 'value=', value);
	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_resource',
		        '_ea_new', value);
		clearSelection('custpage_resource');
	} else {
		alert("Please select an employee");;
	}
}
function updateExpenseApprover() {//
	nlapiLogExecution('Debug', 'updateExpenseApprover fun called', '');
	var value = nlapiGetFieldValue('custpage_all_ex');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_resource', '_ea_new',
		        value);
	} else {
		alert("Please select an employee");
	}
}
function updateTimeApprover() {//
	nlapiLogExecution('Debug', 'updateTimeApprover fun called', '');
	var value = nlapiGetFieldValue('custpage_all_ta');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_resource', '_ta_new',
		        value);
	} else {
		alert("Please select an employee");
	}
}
function updationofSelectedTimeApprover() {
	nlapiLogExecution('Debug', 'updateSelectedTimeApprover fun called', '');
	var value = nlapiGetFieldValue('custpage_all_ta');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_resource',
		        '_ta_new', value);
		clearSelection('custpage_resource');
	} else {
		alert("Please select an employee");
	}
}
function updateSelectedInSection(sublistName, fieldName, value) {
	
	
	var i_count = nlapiGetLineItemCount(sublistName);
	nlapiLogExecution('Debug', 'sublistName=', sublistName);
	nlapiLogExecution('Debug', 'fieldName=', fieldName);
	nlapiLogExecution('Debug', 'value=', value);
	for (var linenum = 1; linenum <= i_count; linenum++) {
		nlapiSelectLineItem(sublistName, linenum);

		if (isTrue(nlapiGetCurrentLineItemValue(sublistName, 'select'))) {
			try{
			nlapiSetCurrentLineItemValue(sublistName, fieldName, value);
			nlapiCommitLineItem(sublistName);
			}catch (e) {
				nlapiLogExecution('Debug', 'error', e)			}
		}
	}
	
}

function updateAllInSection(sublistName, fieldName, value) {

	var i_count = nlapiGetLineItemCount(sublistName);
	// alert(i_count);

	for (var linenum = 1; linenum <= i_count; linenum++) {
		// alert(value);
		nlapiSelectLineItem(sublistName, linenum)
		// alert(nlapiGetLineItemValue(sublistName, fieldName, linenum));
		nlapiSetCurrentLineItemValue(sublistName, fieldName, value);
		nlapiCommitLineItem(sublistName);
		// alert(nlapiGetLineItemValue(sublistName, fieldName, linenum));

	}
}

function redirectToEmpForm() {
	try {
		
		var project = nlapiGetFieldValue('custpage_project');

		if (isNotEmpty(project)) {
			var url = nlapiResolveURL('SUITELET',
			        'customscript_sut_project_wise_ta_ea',
	        'customdeploy_sut_project_wise_ta_ea')
	        + "&project=" + project;
			window.location = url;
		} else {
			alert(project);
			alert('Please select a project');
		}
	} catch (err) {
		alert(err.message);
	}
}

function clearSelection(sublistName) {
	var i_count = nlapiGetLineItemCount(sublistName);

	for (var linenum = 1; linenum <= i_count; linenum++) {

		nlapiSelectLineItem(sublistName, linenum);

		if (isTrue(nlapiGetCurrentLineItemValue(sublistName, 'select'))) {
			nlapiSetCurrentLineItemValue(sublistName, 'select', 'F');
			nlapiCommitLineItem(sublistName);
		}
	}
}

