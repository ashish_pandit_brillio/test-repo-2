/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Region_data(obj_Request) {
         try {
            var obj_Usage_Context = obj_Runtime.getCurrentUser();
            log.debug('obj_Usage_Context', obj_Usage_Context);
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var obj_Response_Return = {};
             if (s_Request_Data == 'REGION') {
                 obj_Response_Return = get_Region_Data(obj_Search);
             }
         } 
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Region_data


     return {
         post: Send_Region_data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Region_Data(obj_Search) {
 try {
    var customrecord_regionSearchObj = obj_Search.create({
        type: "customrecord_region",
        filters:
        [
        ],
        columns:
        [
            obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
            obj_Search.createColumn({
              name: "name",
              sort: obj_Search.Sort.ASC,
              label: "Name"
           }),
           obj_Search.createColumn({
            name: "custentity_fusion_empid",
            join: "CUSTRECORD_REGION_HEAD",
            label: "Fusion Employee Id"
         })
        ]
     });
     var searchResultCount = customrecord_regionSearchObj.runPaged().count;
     log.debug("customrecord_regionSearchObj result count",searchResultCount);
     var myResults = getAllResults(customrecord_regionSearchObj);
     var arr_Region_json = [];
     myResults.forEach(function (result) {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
             'Name': result.getValue({ name: "name", sort: obj_Search.Sort.ASC }),
             'Region_Head_Employee_Id':result.getValue({name: "custentity_fusion_empid",join: "CUSTRECORD_REGION_HEAD"}),
         };
         arr_Region_json.push(obj_json_Container);
         return true;
     });
     return arr_Region_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('get_Employee_Data s_Exception== ', s_Exception);
 } //// End of catch 
} ///// End of function get_Employee_Data()


function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}