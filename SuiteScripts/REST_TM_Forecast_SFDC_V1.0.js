/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response objectt
 * @returns {Void} Any output is written via response object
 */
//
var _Project_details={};
 var pro_list_ = search_tm_projects();
 var praticeData = practiceSearch();

function scheduled(dataIn) {

    nlapiLogExecution('AUDIT', 'Report Run', '');

    try {


        {
            var context = nlapiGetContext();
var d_today = new Date();
            
            //	var d_today = '1/1/2018';
            //d_today = nlapiStringToDate(d_today);

            var error = false;
            var dataRow = [];
            //Conversion Rates table
            //Conversion rate search
		var o_PL_conversion_table=PL_curencyexchange_rate("T");
		
		//IF project id passed
		
          var project_id = dataIn.ProjectId;
		//var i_pro_internal_id=''
		if(_logValidation(project_id)){
		var i_pro_internal_id = getProject_internal_ID(project_id);
		nlapiLogExecution('DEBUG','Passed Project ID',i_pro_internal_id);
		}
		//Check for error
		if(_logValidation(project_id) && !_logValidation(i_pro_internal_id)){
		return 'Invalid Project ID';
		}
	
	
            //Search For TM Module Data
			if(!_logValidation(i_pro_internal_id))
            search_Active_TM_Projects = getTagged_Active_TM(o_PL_conversion_table);
		
            var dataRows = []; var temp_arr = [];
            for (var i = 0; i < 24; i++) {
                var d_day = nlapiAddMonths(d_today, i);
               // yieldScript(context);
                var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
                if(d_month_start.getDate()==2)/// @AUG 12 2021 : Logic is added sometimes Month end date is calcuating as 11/02/2021 instead of 11/01/2021 ADDED BY PRAVEENA
                    {
                      d_month_start=nlapiAddDays(d_month_start,-1);
                    }
              
                var s_month_start = d_month_start.getMonth() + 1 + '/' +
                    d_month_start.getDate() + '/' + d_month_start.getFullYear();
                var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
              if(d_month_end.getDate()==1)/// @April 03 20202 : Logic is added sometimes Month end date is calcauting as 01/11/2020 instead of 31/10/2020ADDED BY PRAVEENA
                {
                    d_month_end=nlapiAddDays(d_month_end,-1);
                }
              
                var s_month_end = d_month_end.getMonth() + 1 + '/' +
                    d_month_end.getDate() + '/' + d_month_end.getFullYear();
              
                try {
                    var JSON_Obj = {};
                    var o_total_array = [];
                    var a_data = get_data(i_pro_internal_id,s_month_start, s_month_end,
                        d_month_start, d_month_end, o_PL_conversion_table);

                    for (var indx = 0; indx < a_data.length; indx++) {
                        var s_local = a_data[indx];


                        JSON_Obj = {
                    //        'Employee': s_local.custpage_employee,	
                            'Vertical': s_local.custpage_vertical,
                            'Department': s_local.custpage_employee_department,
                            'ParentDepartment': s_local.custpage_employee_parent_department,
                            'ProjectID': s_local.custpage_project_id,
                            'ProjectName': s_local.custpage_project,
                            // 'CustomerID': s_local.custpage_customer_id,
                            'CustomerName': s_local.custpage_customer,
                            'Amount': s_local.custpage_total_amount,
                            'ProjectCurrency': s_local.custpage_currency
                        };
                        o_total_array.push(JSON_Obj);
                    }
                    /*exportToXml(a_data, 'T', d_month_start.getMonth() + 1, d_month_end
                            .getFullYear());*/
                    dataRow.push({
                        'MonthStart': s_month_start,
                        'MonthEnd': s_month_end,
                        'Data': o_total_array
                    });
                } catch (e) {
                    nlapiLogExecution('ERROR', 'Error: ', e.message);
					//Added the failure Notification
					_addFailurenotification(e);
                    error = true;
                }
            }
            dataRows.push(dataRow);
			//if(!_logValidation(i_pro_internal_id))
			if(_logValidation(search_Active_TM_Projects))
            dataRows.push(search_Active_TM_Projects);
            //dataRow.push(search_Active_TM_Projects);
        }
       
        nlapiLogExecution('debug', 'response', JSON.stringify(dataRows));
        nlapiLogExecution('debug', 'remaingUsage',context.getRemainingUsage());
        return dataRows;

    } catch (err) {
        nlapiLogExecution('ERROR', 'main', err);
		//Added the failure Notification
		_addFailurenotification(err);
        throw err;
    }
}

function get_data(i_pro_internal_id,s_month_start, s_month_end,
    d_month_start, d_month_end, o_PL_conversion_table) {
    try {

       // var pro_list_mapped_tm = search_tm_projects();
        // Store resource allocations for project and employee
        var a_resource_allocations = new Array();
        var filter = [];
      //commented the code with date filters and and added the filters to  pass the date as string instead of date format on 30-10-2020
       // filter.push(new nlobjSearchFilter('custrecord_forecast_month_startdate', null, 'on', d_month_start));
       // filter.push(new nlobjSearchFilter('custrecord_forecast_month_enddate', null, 'on', d_month_end));
      filter.push(new nlobjSearchFilter('custrecord_forecast_month_startdate', null, 'on', s_month_start));
        filter.push(new nlobjSearchFilter('custrecord_forecast_month_enddate', null, 'on', s_month_end));
		if(_logValidation(pro_list_)){
     //   filter.push(new nlobjSearchFilter('custrecord_forecast_project_name', null, 'noneof', pro_list_));
		}
		if(_logValidation(i_pro_internal_id)){
		      filter.push(new nlobjSearchFilter('custrecord_forecast_project_name', null, 'anyof', parseInt(i_pro_internal_id)));
		}
        var cols = [];
    	cols.push(new nlobjSearchColumn('custrecord_forecast_month_startdate',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_month_enddate',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_vertical',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_department',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_parent_department',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_project_id',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_project_name',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_customer',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_amount',null,'sum'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_project_currency',null,'group'));
    	cols.push(new nlobjSearchColumn('custrecord_forecast_employee',null,'group'));
    	cols.push(new nlobjSearchColumn("custrecord_is_delivery_practice","CUSTRECORD_FORECAST_DEPARTMENT","GROUP")); 
    	cols.push(new nlobjSearchColumn("custentity_practice","CUSTRECORD_FORECAST_PROJECT_NAME","GROUP"));
		

        var search_rec = searchRecord('customrecord_forecast_master_data', null, filter, cols);
        if (search_rec) {
            for (var indx = 0; indx < search_rec.length; indx++) {
            	//Added the logic for non-delivery practices
            	var project = search_rec[indx].getValue('custrecord_forecast_project_name',null,'group');
            	if(parseInt(project) == parseInt(102351)){
            		nlapiLogExecution('DEBUG','is_delivery:'+ search_rec[indx].getText('custrecord_forecast_project_name',null,'group'),is_delivery);
            	}
            	var is_delivery = search_rec[indx].getValue("custrecord_is_delivery_practice","CUSTRECORD_FORECAST_DEPARTMENT","GROUP");
            	if(is_delivery == 'F' || is_delivery == 'No'){
            		
            		var index_ =  search_rec[indx].getText("custentity_practice","CUSTRECORD_FORECAST_PROJECT_NAME","GROUP").indexOf(':');
            		if(parseInt(index_) > parseInt(0)){
            		var s_subPractice = search_rec[indx].getText("custentity_practice","CUSTRECORD_FORECAST_PROJECT_NAME","GROUP");
            		var s_practice = 	search_rec[indx].getText("custentity_practice","CUSTRECORD_FORECAST_PROJECT_NAME","GROUP").split(':')[0].trim();
            		}
            		else{
            			var s_subPractice = search_rec[indx].getText("custentity_practice","CUSTRECORD_FORECAST_PROJECT_NAME","GROUP");
                    	var s_practice = 	search_rec[indx].getText("custentity_practice","CUSTRECORD_FORECAST_PROJECT_NAME","GROUP");
            		}
            	}
            	else{
            		var s_subPractice = search_rec[indx].getText('custrecord_forecast_department',null,'group');
                	var s_practice = search_rec[indx].getValue('custrecord_forecast_parent_department',null,'group');	
            	}
            	
                a_resource_allocations[indx] = {

                		'endDate':search_rec[indx].getValue('custrecord_forecast_month_startdate',null,'group'),
                        'startDate':search_rec[indx].getValue('custrecord_forecast_month_enddate',null,'group'),

    				    'custpage_employee' : search_rec[indx].getValue('custrecord_forecast_employee',null,'group'),
    				    'custpage_vertical' : search_rec[indx].getText('custrecord_forecast_vertical',null,'group'),
    				    'custpage_employee_department' : s_subPractice,
    				    'custpage_employee_parent_department' : s_practice,
    				    'custpage_project_id' : search_rec[indx].getValue('custrecord_forecast_project_id',null,'group'),
    				    'custpage_project' : search_rec[indx].getText('custrecord_forecast_project_name',null,'group'),
    				    'custpage_customer' : search_rec[indx].getText('custrecord_forecast_customer',null,'group'),
    				    'custpage_total_amount' : search_rec[indx].getValue('custrecord_forecast_amount',null,'sum'),
    				    'custpage_currency' : search_rec[indx].getText('custrecord_forecast_project_currency',null,'group')


                };
            }

        }

    } catch (e) {
        nlapiLogExecution('DEBUG', 'Data Search Error', e);
        throw e;
    }
    return a_resource_allocations;
}


function yieldScript(currentContext) {

    if (currentContext.getRemainingUsage() <= 100) {
        nlapiLogExecution('AUDIT', 'API Limit Exceeded');
        var state = nlapiYieldScript();

        if (state.status == "FAILURE") {
            nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
                state.reason + ' / Size : ' + state.size);
            return false;
        } else if (state.status == "RESUME") {
            nlapiLogExecution('AUDIT', 'Script Resumed');
        }
    }
}

function search_tm_projects() {
    try {
    	//filtr.push(new nlobjSearchFilter('customer', 'custrecord_tm_data_project', 'noneof', 128288));
		var present_date= new Date();
		var month_num = present_date.getMonth();
        var customrecord_tm_month_total_jsonSearch = searchRecord("customrecord_tm_month_total_json", null, [["custrecord_project_json.enddate", "onorafter", "startofthismonth"]
        ,"AND", 
		[["created","within","thismonth"],"OR",["created","within","lastmonth"]]
		,"AND",
        ["custrecord_is_confirm","is","T"]
		,"AND", 
        ["custrecord_project_json","noneof","128288"]
        ], [
            //new nlobjSearchColumn("custrecord_project_json", null, "GROUP").setSort(true)
            new nlobjSearchColumn("custrecord_project_json", null, "GROUP").setSort(true),
			new nlobjSearchColumn("entityid","custrecord_project_json", "GROUP"),
			new nlobjSearchColumn("customer","custrecord_project_json",  "GROUP"),
			new nlobjSearchColumn("custentity_vertical", "custrecord_project_json", "GROUP"),
			new nlobjSearchColumn("companyname","custrecord_project_json",  "GROUP"),
			new nlobjSearchColumn( "custentity_project_currency","custrecord_project_json","GROUP")
        ]);
		//

		//
		
        //Search to identify the projects in module
        var pro_list = new Array();
        if (_logValidation(customrecord_tm_month_total_jsonSearch)) {
            for (var i = 0; i < customrecord_tm_month_total_jsonSearch.length; i++) {
                pro_list.push(customrecord_tm_month_total_jsonSearch[i].getValue('custrecord_project_json', null, 'GROUP'));
              
              //added LOGIC FOR OPTIMIZATION
				_Project_details[customrecord_tm_month_total_jsonSearch[i].getValue('custrecord_project_json', null, 'GROUP')]={
				'custentity_project_currency':customrecord_tm_month_total_jsonSearch[i].getText("custentity_project_currency","custrecord_project_json", "GROUP"),
				'custentity_vertical':customrecord_tm_month_total_jsonSearch[i].getText("custentity_vertical","custrecord_project_json", "GROUP"),
				'customer':customrecord_tm_month_total_jsonSearch[i].getText("customer","custrecord_project_json", "GROUP"),
				'entityid':customrecord_tm_month_total_jsonSearch[i].getValue("entityid","custrecord_project_json", "GROUP"),
				'companyname':customrecord_tm_month_total_jsonSearch[i].getText('custrecord_project_json', null, 'GROUP')
				}
              
            }
        }
      nlapiLogExecution('DEBUG', '_Project_details', JSON.stringify(_Project_details));
        return pro_list;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Finding Project inside module error', e);
    }
}
//Function to fetch project and recent id
function search_tm_projects_rec_id() {
    try {

        var customrecord_tm_month_total_jsonSearch = searchRecord("customrecord_tm_month_total_json", null, [
            ["formuladate: {custrecord_project_json.enddate}", "onorafter", "startofthismonth"]
        ], [
            new nlobjSearchColumn("internalid", null, "GROUP").setSort(true),
            new nlobjSearchColumn("custrecord_project_json", null, "GROUP")
        ]);
        //Search to identify the projects in module
        var pro_list = new Array();
        var unquie_proj = [];

        if (_logValidation(customrecord_tm_month_total_jsonSearch)) {
            for (var i = 0; i < customrecord_tm_month_total_jsonSearch.length; i++) {

                if (unquie_proj.indexOf(customrecord_tm_month_total_jsonSearch[i].getValue('custrecord_project_json', null, 'GROUP')) < 0) {

                    unquie_proj.push(customrecord_tm_month_total_jsonSearch[i].getValue('custrecord_project_json', null, 'GROUP'));
                    pro_list.push({
                        'Project': customrecord_tm_month_total_jsonSearch[i].getValue('custrecord_project_json', null, 'GROUP'),
                        'ID': customrecord_tm_month_total_jsonSearch[i].getValue('internalid', null, 'GROUP')

                    });
                }

            }
        }
        return pro_list;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Finding Project inside module error', e);
    }
}

function getTagged_Active_TM(o_PL_conversion_table) {
    try {
        //  var searchRes = searchRecord('resourceallocation', 'customsearch1521', null, null);
        //var pro_list_ = search_tm_projects();
        if (_logValidation(pro_list_)) {


            //search tm data
            var filtr = [];
            filtr.push(new nlobjSearchFilter('custrecord_tm_data_project', null, 'anyof', pro_list_));
			filtr.push(new nlobjSearchFilter('custrecord_rampup_type', null, 'isnot', 'Total'));
            filtr.push(new nlobjSearchFilter('custrecord_is_confirm', 'custrecord_tm_json_parent', 'is', 'T'));//send the JSON for only confimed Numbers
            var col = [];
            col.push(new nlobjSearchColumn('internalid').setSort(true));
            col.push(new nlobjSearchColumn('custrecord_tm_month'));
            col.push(new nlobjSearchColumn('custrecord_tm_year'));
            col.push(new nlobjSearchColumn('custrecord_tm_amount'));
            col.push(new nlobjSearchColumn('custrecord_rampup_type'));
            col.push(new nlobjSearchColumn('custrecord_tm_sub_practice'));
            col.push(new nlobjSearchColumn('custrecord_total_share'));
            col.push(new nlobjSearchColumn('custrecord_tm_json_parent'));
            col.push(new nlobjSearchColumn('custrecord_tm_data_project'));
            col.push(new nlobjSearchColumn('custentity_practice','custrecord_tm_data_project',null));

            var customrecord_tm_month_total_Search = searchRecord('customrecord_forecast_revenue_tm', null, filtr, col);
            //Loop to fetch project TM data
            var pro_duplicate = [];
            var json_parent = '';
            var dataRow_filtered = [];
            var s_subPrac = [];
           // var dataRow = [];
            if (_logValidation(customrecord_tm_month_total_Search)) {
                for (var j = 0; j < customrecord_tm_month_total_Search.length; j++) {
                	 var dataRow = [];
                	 var JSON_O ={};
                	 var i_subPractice_value = '';
                	 var subPrac_parse = '';
                	 
                    if (pro_duplicate.indexOf(customrecord_tm_month_total_Search[j].getValue('custrecord_tm_data_project')) < 0 ||
                        parseInt(json_parent) == parseInt(customrecord_tm_month_total_Search[j].getValue('custrecord_tm_json_parent'))) {

                        //Logic to fetch the data based on subpractice
                    	
                    	 json_parent = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_json_parent');
                        var i_project = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_data_project');
                        var i_ID = customrecord_tm_month_total_Search[j].getValue('internalid');
                        var context = nlapiGetContext();
                        i_subPractice_value = customrecord_tm_month_total_Search[j].getValue('custrecord_total_share');
                        var s_month = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_month');
                        var s_year = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_year');
                        var f_amount = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_amount');
                        var s_rampUp_type = customrecord_tm_month_total_Search[j].getValue('custrecord_rampup_type');
                        var i_subPractice = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_sub_practice');
                        var s_subPractice = customrecord_tm_month_total_Search[j].getText('custrecord_tm_sub_practice');

                        
                        if(s_rampUp_type != 'Total' && _logValidation(i_subPractice_value)){
                        subPrac_parse = JSON.parse(i_subPractice_value);
                        if (_logValidation(subPrac_parse)) {
                            for (var k = 0; k < subPrac_parse.length; k++) {
                                var s_temp = subPrac_parse[k];
                                var s_subpractice = s_temp.practice;
                                var f_share = s_temp.share;
                            }
                        }
                        }
                        // var a_pract = remove_dulpi(unique_practice);
                        var job_lookUp =_Project_details[i_project];// nlapiLookupField('job', parseInt(i_project), ['custentity_project_currency', 'custentity_vertical', 'customer','entityid','companyname'], true);
                     // nlapiLogExecution('debug','_Project_details[i_project]',JSON.stringify(_Project_details[i_project]));
                        var s_proj_currency = job_lookUp.custentity_project_currency;
//nlapiLogExecution('debug','job_lookUp',JSON.stringify(job_lookUp));
                        var i_master_currency = 1;
                        // get previous mnth effrt for true up and true down scenario
                   /*     if (s_proj_currency == 'USD')
                            i_master_currency = 1;
                        if (s_proj_currency == 'INR')
                            i_master_currency = inr_to_usd;
                        if (s_proj_currency == 'EUR')
                            i_master_currency = eur_to_usd;
                        if (s_proj_currency == 'GBP')
                            i_master_currency = gbp_to_usd;
						if (s_proj_currency == 'AUD')
                            i_master_currency = aud_to_usd; */
                        //a_recognized_revenue_total_proj_level = a_recognized_revenue;
                        //var data_month_All =s_entire_json_clubed.mnth;
                        //var commulative_rev_month = html.cummulative; 

                       
/*var i_master_currency=_Currency_exchangeRate(s_proj_currency, o_PL_conversion_table) 
						
								i_master_currency=i_master_currency?i_master_currency : 1;
*/
                      
                     

                        //Logic to restrict JSON only for future months
                        var d_today = nlapiDateToString(new Date());
                        // d_today = nlapiAddMonths(nlapiStringToDate(d_today), -1);
                        //var d_today = '1/1/2018';
                        d_today = nlapiStringToDate(d_today);

                        var getCurrentMonth = d_today.getMonth() + 1;
                        var getCurrentYear = d_today.getFullYear();

                        //Report Months Capture

                        var job_lookUp_Obj = {'entityid':job_lookUp.entityid,'companyname':job_lookUp.companyname};//nlapiLookupField('job', parseInt(i_project), ['entityid', 'companyname']);
                        //var data_month_ind = data_month_All[indxx].split('_');

                        //var data_m_ind = data_month_ind[0];
                        var d_month = getMonthCompleteIndex(s_month);
                        var data_year = s_year.split('.')[0];
                        //Get Start & End Date
                        var date_obj = get_month_start_end_date(s_month, data_year);
                        //	d_month = parseInt(d_month);
                       var projectPractice = customrecord_tm_month_total_Search[j].getText('custentity_practice','custrecord_tm_data_project');
                       
                        if (_logValidation(subPrac_parse)) {

                            for (var k = 0; k < subPrac_parse.length; k++) {
                                var s_temp = subPrac_parse[k];
                                var s_subpractice = s_temp.practice;
                                //Add the logic to check whether delivery practice or not
                                
                                for(var jj = 0; jj < praticeData.length; jj++){
                                	if(praticeData[jj].name.trim() == s_subpractice.trim()){
                                		
                                		var is_delivery = praticeData[jj].IsDelivery;
                                		if(is_delivery == 'F'){
                                			s_subpractice = projectPractice;
                                		}
                                	}
                                }
                                
                                var f_share = s_temp.share;

                                if (parseInt(d_month) >= parseInt(getCurrentMonth) && parseInt(data_year) >= parseInt(getCurrentYear)) {

                                	JSON_O = {
                                			'Vertical': job_lookUp.custentity_vertical,
                                            'Department': s_subpractice,
                                            'ParentDepartment': s_subpractice.split(':')[0].trim(),
                                            'ProjectID': job_lookUp_Obj.entityid,
                                            'ProjectName': job_lookUp_Obj.companyname,
                                            'CustomerName': job_lookUp.customer,
                                            'Amount': (i_master_currency * parseFloat(f_share)).toFixed(2),
                                            'ProjectCurrency': s_proj_currency
                                          //  'Type': s_rampUp_type
                                        };
                                	dataRow.push(JSON_O);
                                  /*var d_day = nlapiAddMonths(d_today, -1);
                                  nlapiLogExecution('debug', 'd_day',d_day);
                                  var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
                                  var s_month_start = d_month_start.getMonth() + 1 + '/'
                                          + d_month_start.getDate() + '/' + d_month_start.getFullYear();
                                  var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
                                  var s_month_end = d_month_end.getMonth() + 1 + '/'
                                          + d_month_end.getDate() + '/' + d_month_end.getFullYear();
                                  */
                              } else if (parseInt(data_year) > parseInt(getCurrentYear)) {

                              	JSON_O = {
                              			'Vertical': job_lookUp.custentity_vertical,
                                        'Department': s_subpractice,
                                        'ParentDepartment': s_subpractice.split(':')[0].trim(),
                                        'ProjectID': job_lookUp_Obj.entityid,
                                        'ProjectName': job_lookUp_Obj.companyname,
                                        'CustomerName': job_lookUp.customer,
                                        'Amount': (i_master_currency * parseFloat(f_share)).toFixed(2),
                                        'ProjectCurrency': s_proj_currency
                                       // 'Type': s_rampUp_type
                                        };
                              	dataRow.push(JSON_O);
                                    /*var d_day = nlapiAddMonths(d_today, -1);
                                    nlapiLogExecution('debug', 'd_day',d_day);
                                    var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
                                    var s_month_start = d_month_start.getMonth() + 1 + '/'
                                            + d_month_start.getDate() + '/' + d_month_start.getFullYear();
                                    var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
                                    var s_month_end = d_month_end.getMonth() + 1 + '/'
                                            + d_month_end.getDate() + '/' + d_month_end.getFullYear();
                                    */
                               

                                }
                                
                            }
                           
                           
                            
                        }
                        //IF Not Total type
                        if ( s_rampUp_type != 'Total' && (parseInt(d_month) >= parseInt(getCurrentMonth) && parseInt(data_year) >= parseInt(getCurrentYear))) {

                        	JSON_O = {
                        			'Vertical': job_lookUp.custentity_vertical,
                                    'Department': s_subPractice,
                                    'ParentDepartment': s_subPractice.split(':')[0].trim(),
                                    'ProjectID': job_lookUp_Obj.entityid,
                                    'ProjectName': job_lookUp_Obj.companyname,
                                    'CustomerName': job_lookUp.customer,
                                    'Amount': (i_master_currency * f_amount).toFixed(2),
                                    'ProjectCurrency': s_proj_currency
                                   // 'Type': s_rampUp_type
                                };
                        	dataRow.push(JSON_O);
                          /*var d_day = nlapiAddMonths(d_today, -1);
                          nlapiLogExecution('debug', 'd_day',d_day);
                          var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
                          var s_month_start = d_month_start.getMonth() + 1 + '/'
                                  + d_month_start.getDate() + '/' + d_month_start.getFullYear();
                          var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
                          var s_month_end = d_month_end.getMonth() + 1 + '/'
                                  + d_month_end.getDate() + '/' + d_month_end.getFullYear();
                          */
                      } else if (s_rampUp_type != 'Total' &&  (parseInt(data_year) > parseInt(getCurrentYear))) {

                      	JSON_O = {
                      			'Vertical': job_lookUp.custentity_vertical,
                                'Department': s_subPractice,
                                'ParentDepartment': s_subPractice.split(':')[0].trim(),
                                'ProjectID': job_lookUp_Obj.entityid,
                                'ProjectName': job_lookUp_Obj.companyname,
                                'CustomerName': job_lookUp.customer,
                                'Amount': (i_master_currency * f_amount).toFixed(2),
                                'ProjectCurrency': s_proj_currency
                              //  'Type': s_rampUp_type
                                };
                      	dataRow.push(JSON_O);
                            /*var d_day = nlapiAddMonths(d_today, -1);
                            nlapiLogExecution('debug', 'd_day',d_day);
                            var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
                            var s_month_start = d_month_start.getMonth() + 1 + '/'
                                    + d_month_start.getDate() + '/' + d_month_start.getFullYear();
                            var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
                            var s_month_end = d_month_end.getMonth() + 1 + '/'
                                    + d_month_end.getDate() + '/' + d_month_end.getFullYear();
                            */
                       

                        }
                        
                        pro_duplicate.push(customrecord_tm_month_total_Search[j].getValue('custrecord_tm_data_project'));

                    }
                    if(_logValidation(dataRow)){
                    dataRow_filtered.push({
                    	'MonthStart': date_obj.split('#')[0],
                        'MonthEnd': date_obj.split('#')[1],
                    	'Data': dataRow
                    	
                    });
                    }

                }
            }


        }
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Data Search Error', e);
        throw e;
    }
    return dataRow_filtered;
}

function getMonthCompleteIndex(month) {
    var s_mont_indx = '';
    if (month == 'JAN')
        s_mont_indx = 1;
    if (month == 'FEB')
        s_mont_indx = 2;
    if (month == 'MAR')
        s_mont_indx = 3;
    if (month == 'APR')
        s_mont_indx = 4;
    if (month == 'MAY')
        s_mont_indx = 5;
    if (month == 'JUN')
        s_mont_indx = 6;
    if (month == 'JUL')
        s_mont_indx = 7;
    if (month == 'AUG')
        s_mont_indx = 8;
    if (month == 'SEP')
        s_mont_indx = 9;
    if (month == 'OCT')
        s_mont_indx = 10;
    if (month == 'NOV')
        s_mont_indx = 11;
    if (month == 'DEC')
        s_mont_indx = 12;

    return s_mont_indx;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function remove_dulpi(practices_involved) {
    var unique_array = new Array();
    for (var uni_prac = 0; uni_prac < practices_involved.length; uni_prac++) {
        if (unique_array.indexOf(practices_involved[uni_prac]) < 0) {
            unique_array.push(practices_involved[uni_prac]);
        }
    }
    return unique_array;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function get_month_start_end_date(i_month, i_year) {
    var d_start_date = '';
    var d_end_date = '';

    var date_format = checkDateFormat();

    if (_logValidation(i_month) && _logValidation(i_year)) {
        if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
               d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
            if ((i_year % 4) == 0) {
                i_day = 29;
            } else {
                i_day = 28;
            }

            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
            }

        } else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
               d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
            }


        } else if (i_month == 'May' || i_month == 'MAY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
               d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
            }
        } else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
            }

        }
    } //Month & Year
    return d_start_date + '#' + d_end_date;
}

function getProject_internal_ID(pro_id){
	try{
		var id = '';
		var a_project_filtr = [['entityid', 'is', pro_id]];
		var cols = [];
			cols.push(new nlobjSearchColumn('internalid'));
			cols.push(new nlobjSearchColumn('companyname'));
		var a_project_search = searchRecord('job', null, a_project_filtr, cols);
		if(_logValidation(a_project_search)){
			id = a_project_search[0].getValue('internalid');
		}
		return id;
	}
	catch(e){
		nlapiLogExecution('ERROR','Get Project Internal ID Function',e);
	}
}

//search for practice master
function practiceSearch(){
	try{
		var departmentSearch = nlapiSearchRecord("department",null,
				[
				   ["isinactive","is","F"]
				], 
				[
				   new nlobjSearchColumn("name").setSort(false), 
				   new nlobjSearchColumn("custrecord_parent_practice"), 
				   new nlobjSearchColumn("custrecord_is_delivery_practice")
				]
				);
		var datarow = [];
		
		if(departmentSearch){
			for(var k=0;k<departmentSearch.length;k++){
				
				var json = {
						'name': departmentSearch[k].getValue('name'),
						'parentPractice': departmentSearch[k].getValue('custrecord_parent_practice'),
						'IsDelivery': departmentSearch[k].getValue('custrecord_is_delivery_practice')
						
				}
				datarow.push(json);
			}
		}
		return datarow;
		
	}
	catch(e){
		nlapiLogExecution('ERROR','Get Project Internal ID Function',e);
	}
}

function _addFailurenotification(err){
var context=nlapiGetContext();

var mailTemplate = "";
mailTemplate += "<p> This is to inform that NS To SFDC T&M Forecast sync script has been failed.And It is having Error - <b>["+err.code+" : "+err.message +"]</b> and therefore, system is not able to the send the T&M Forecast numbers to SFDC</p>";

mailTemplate += "<p> Kindly have a look on to the Error and Please do needfull.</p>";
mailTemplate += "<p><b> Script Id:- "+context.getScriptId() +"</b></p>";
mailTemplate += "<p><b> Script Deployment Id:- "+context.getDeploymentId() +"</b></p>";
mailTemplate += "<br/>"
mailTemplate += "<p>Regards, <br/> Information Systems</p>";
nlapiSendEmail(442,['information.systems@brillio.com','netsuite.support@brillio.com',"BrillioSFDCSupport@brillio.com"], 'Failure Notification on T&M Forecast Sync',mailTemplate, null, null, null, null);
}