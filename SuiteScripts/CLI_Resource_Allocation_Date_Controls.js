/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Resource_Allocation_Date_Controls.js
	Author      : Shweta Chopde
	Date        : 20 May 2014
	Description : Controls on Resource Allocation


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_resource()
{
	//Check for Bill Start Date and End Date
	if(nlapiGetFieldValue('custeventrbillable') == 'T')
	{
		if(nlapiGetFieldValue('custeventbstartdate') != null && nlapiGetFieldValue('custeventbstartdate').length <= 0)
		{
			alert('Please enter Billing Start Date');
			return false;
		}

		if(nlapiGetFieldValue('custeventbenddate') != null && nlapiGetFieldValue('custeventbenddate').length <= 0)
		{
			alert('Please enter Billing End Date');
			return false;
		}
	}
	
	//Check for OT Rate
	if((nlapiGetFieldValue('custevent_otbillable') != null && nlapiGetFieldValue('custevent_otbillable') == 'T') ||  (nlapiGetFieldValue('custevent_otpayable') != null && nlapiGetFieldValue('custevent_otpayable') == 'T'))
	{
		if(nlapiGetFieldValue('custevent_otserviceitem') != null && nlapiGetFieldValue('custevent_otserviceitem').length <= 0)
		{
			alert('Please select OT Service Item');
			return false;
		}
	
		if(nlapiGetFieldValue('custevent_otrate') != null && nlapiGetFieldValue('custevent_otrate') <= 0)
		{
			alert('Please enter the OT rate.');
			return false;
		}
	}
	
	var a_service_item_array = new Array();
	var i_service_item_f = nlapiGetFieldValue('custevent1')
	
	var i_project = nlapiGetFieldValue('project')
	
	nlapiLogExecution('DEBUG', 'get_project_task', ' i_service_item_f -->' + i_service_item_f);
			
    nlapiLogExecution('DEBUG', 'get_project_task', ' i_project-->' + i_project);
					
	 if(_logValidation(i_project))
	  {
	  	var filter = new Array();
	    filter[0] = new nlobjSearchFilter('custrecord_proj',null,'is',i_project);
		
		var columns = new Array();
	    columns[0] = new nlobjSearchColumn('custrecord_item1');
		
		 var a_search_results = nlapiSearchRecord('customrecord_customraterecord',null,filter,columns);
		 
		 if(_logValidation(a_search_results))
		 {
		 	  nlapiLogExecution('DEBUG', 'get_project_task', ' Search Results Length -->' + a_search_results.length);
			
			  for(var t=0;t<a_search_results.length;t++)
			  {
			  	var i_service_item = a_search_results[t].getValue('custrecord_item1')
				
				var i_service_item_text = a_search_results[t].getText('custrecord_item1')
				
				nlapiLogExecution('DEBUG', 'get_project_task', ' i_service_item -->' + i_service_item);
				nlapiLogExecution('DEBUG', 'get_project_task', ' i_service_item_text -->' + i_service_item_text);
				
				a_service_item_array.push(i_service_item_text)
				
			  }//Loop	
			  				 	
		 }//Search Results
		 
		 nlapiLogExecution('DEBUG', 'get_project_task', ' Service Item Array Length -->' + a_service_item_array.length);
		 nlapiLogExecution('DEBUG', 'get_project_task', ' Service Item Array -->' + a_service_item_array);
		 nlapiLogExecution('DEBUG', 'get_project_task', ' SSSSSSS -->' + a_service_item_array.indexOf(i_service_item_f));
		
		
		 if(!_logValidation(a_service_item_array)&&_logValidation(i_service_item_f))
		 {		 	
			alert(' Service Item is missing on Rate Card Tab .')
			return false;
		 }
		
		
		//if(_logValidation(a_service_item_array)&&_logValidation(i_service_item_f))

	     if(_logValidation(a_service_item_array)&&_logValidation(i_service_item_f)&& a_service_item_array.length!=0)
		 {
		 	nlapiLogExecution('DEBUG', 'get_project_task', ' In Block ....');
			
		 	 if(a_service_item_array.indexOf(i_service_item_f)== -1)
			 {
			 	nlapiLogExecution('DEBUG', 'get_project_task', ' i_service_item -->' + i_service_item);
				
			 	alert(' Service Item is missing on Rate Card Tab .')
			
				return false;
			 }	
			
		 }
 
			
	  }//Employee
	
    


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================


var i_flag_t = 0;


// BEGIN FIELD CHANGED ==============================================

function fieldChanged_allocation_ctrls(type, name, linenum)
{
   if(name =='startdate')
   {
     	var i_project = nlapiGetFieldValue('project')
		
		if (_logValidation(i_project)) 
		{
			var o_projectOBJ = nlapiLoadRecord('job', i_project);
			
			if (_logValidation(o_projectOBJ)) 
			{
				var d_project_start_date = o_projectOBJ.getFieldValue('startdate')
				
				var d_project_end_date = o_projectOBJ.getFieldValue('enddate')
				
				var d_start_date = nlapiGetFieldValue('startdate');
				
				var d_end_date = nlapiGetFieldValue('enddate');
					
				d_start_date = nlapiStringToDate(d_start_date);
				d_end_date = nlapiStringToDate(d_end_date);
				d_project_end_date = nlapiStringToDate(d_project_end_date);
				d_project_start_date = nlapiStringToDate(d_project_start_date);
			
							 
			    if(_logValidation(d_start_date)&&_logValidation(d_project_start_date))
				{
				    if(d_start_date<d_project_start_date)
					{
						alert(' Start Date of the project should be greater than / equal to Project Start Date');
						nlapiSetFieldValue('startdate','')
						return false;
					}
			  
				}
				 if(_logValidation(d_start_date)&&_logValidation(d_project_end_date))
				{					
				    if(d_start_date>d_project_end_date && d_start_date>d_project_start_date)
					{
						alert(' Start Date of the project should be less than / equal to Project End Date');
						nlapiSetFieldValue('startdate','')
						return false;
					}
			  
				}
			    if(_logValidation(d_end_date)&&_logValidation(d_project_end_date))
				{
				    if(d_end_date>d_project_end_date)
					{
						alert(' End Date of the project should be less than /equal to Project End Date .');
						nlapiSetFieldValue('enddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_end_date)&&_logValidation(d_project_start_date)&&_logValidation(d_project_end_date))
				{
				    if(d_end_date<d_project_start_date && d_end_date<d_project_end_date)
					{
						alert(' End Date of the project should be greater than /equal to Project Start Date .');
						nlapiSetFieldValue('enddate','')
						return false;
					}
			  
				}
				
				  if(_logValidation(d_end_date)&&_logValidation(d_start_date))
				{
				    if(d_end_date<d_start_date)
					{
						alert(' Start Date should be less than End Date . ');
						nlapiSetFieldValue('startdate','')
						return false;
					}
			  
				}
				
				
			}
		}
   	
   }//Start Date
   if(name =='enddate')
   {
   	var i_project = nlapiGetFieldValue('project')
		
		if (_logValidation(i_project)) 
		{
			var o_projectOBJ = nlapiLoadRecord('job', i_project);
			
			if (_logValidation(o_projectOBJ)) 
			{
				var d_project_start_date = o_projectOBJ.getFieldValue('startdate')
				
				var d_project_end_date = o_projectOBJ.getFieldValue('enddate')
				
				var d_start_date = nlapiGetFieldValue('startdate');
				
				var d_end_date = nlapiGetFieldValue('enddate');
				
				d_start_date = nlapiStringToDate(d_start_date);
				d_end_date = nlapiStringToDate(d_end_date);
				d_project_end_date = nlapiStringToDate(d_project_end_date);
				d_project_start_date = nlapiStringToDate(d_project_start_date);
		
							 
			    if(_logValidation(d_start_date)&&_logValidation(d_project_start_date))
				{
				    if(d_start_date<d_project_start_date)
					{
						alert(' Start Date of the project should be greater than / equal to Project Start Date');
						nlapiSetFieldValue('startdate','')
						return false;
					}
			  
				}
				 if(_logValidation(d_start_date)&&_logValidation(d_project_end_date))
				{					
				    if(d_start_date>d_project_end_date && d_start_date>d_project_start_date)
					{
						alert(' Start Date of the project should be less than / equal to Project End Date');
						nlapiSetFieldValue('startdate','')
						return false;
					}
			  
				}
			    if(_logValidation(d_end_date)&&_logValidation(d_project_end_date)&&_logValidation(d_project_start_date))
				{
					
				    if(d_end_date>d_project_start_date && d_end_date>d_project_end_date)
					{
						alert(' End Date of the project should be less than /equal to Project End Date .');
						nlapiSetFieldValue('enddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_end_date)&&_logValidation(d_project_start_date)&&_logValidation(d_project_end_date))
				{
				    if(d_end_date<d_project_start_date && d_end_date<d_project_end_date)
					{
						alert(' End Date of the project should be greater than /equal to Project Start Date .');
						nlapiSetFieldValue('enddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_end_date)&&_logValidation(d_start_date))
				{
				    if(d_end_date<d_start_date)
					{
						alert(' Start Date should be less than End Date . ');
						nlapiSetFieldValue('enddate','')
						return false;
					}
			  
				}
			}
		}
   	
   }//End Date
   
   if(name == 'custeventbstartdate')
   {   	 
		var d_start_date = nlapiGetFieldValue('startdate')
		
		var d_end_date = nlapiGetFieldValue('enddate')
		
		var d_billing_start_date = nlapiGetFieldValue('custeventbstartdate');
		
		var d_billing_end_date = nlapiGetFieldValue('custeventbenddate');
		
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);
		d_billing_start_date = nlapiStringToDate(d_billing_start_date);
		d_billing_end_date = nlapiStringToDate(d_billing_end_date);
		
			 
			    if(_logValidation(d_start_date)&&_logValidation(d_billing_start_date))
				{
				    if(d_billing_start_date<d_start_date)
					{
						alert(' Billing Start Date of the project should be greater than / equal to Start Date');
						nlapiSetFieldValue('custeventbstartdate','')
						return false;
					}
			  
				}
				 if(_logValidation(d_start_date)&&_logValidation(d_billing_end_date))
				{					
				    if(d_billing_start_date>d_end_date && d_billing_start_date>d_start_date)
					{
						alert(' Billing Start Date of the project should be less than / equal to End Date');
						nlapiSetFieldValue('custeventbstartdate','')
						return false;
					}
			  
				}
			    if(_logValidation(d_end_date)&&_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
				{
					
				    if(d_billing_end_date>d_start_date && d_billing_end_date>d_end_date)
					{
						alert(' Billing End Date of the project should be less than /equal to End Date .');
						nlapiSetFieldValue('custeventbenddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_end_date)&&_logValidation(d_billing_start_date)&&_logValidation(d_billing_end_date))
				{
				    if(d_billing_end_date<d_start_date && d_billing_end_date<d_end_date)
					{
						alert(' Billing End Date of the project should be greater than /equal to Start Date .');
						nlapiSetFieldValue('custeventbenddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
				{
				    if(d_billing_end_date<d_billing_start_date)
					{
						alert(' Billing Start Date should be less than Billing End Date . ');
						nlapiSetFieldValue('custeventbenddate','')
						return false;
					}
			  
				}

		
			
/*
		if(_logValidation(d_start_date)&&_logValidation(d_billing_start_date))
		{
		    if(d_start_date>d_billing_start_date)
			{
				alert(' Billing Start Date should be greater than / equal to Start Date');
				nlapiSetFieldValue('custeventbstartdate','')
				return false;
			}
	  
		}

	    if(_logValidation(d_end_date)&&_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
		{
			
		    if(d_end_date>d_billing_start_date && d_end_date<d_billing_end_date)
			{
				alert(' Billing End Date should be less than /equal to End Date .');
				nlapiSetFieldValue('custeventbenddate','')
				return false;
			}
	  
		}
		  if(_logValidation(d_end_date)&&_logValidation(d_billing_start_date)&&_logValidation(d_billing_end_date))
		{
		    if(d_end_date>d_billing_start_date && d_end_date<d_billing_end_date)
			{
				alert(' Billing End Date should be greater than /equal to Start Date .');
				nlapiSetFieldValue('custeventbenddate','')
				return false;
			}
	  
		}
		 if(_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
		{
		    if(d_billing_end_date<d_billing_start_date)
			{
				alert(' Billing Start Date should be less than Billing End Date .');
				nlapiSetFieldValue('custeventbstartdate','')
				return false;
			}
	  
		}
*/
		
		
   }//Billing Start Date
   
   if(name == 'custeventbenddate')
   {
   		var d_start_date = nlapiGetFieldValue('startdate')
		
		var d_end_date = nlapiGetFieldValue('enddate')
		
		var d_billing_start_date = nlapiGetFieldValue('custeventbstartdate');
		
		var d_billing_end_date = nlapiGetFieldValue('custeventbenddate');
		
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);
		d_billing_start_date = nlapiStringToDate(d_billing_start_date);
		d_billing_end_date = nlapiStringToDate(d_billing_end_date);
		
		/*
if(_logValidation(d_start_date)&&_logValidation(d_billing_start_date))
		{
		    if(d_start_date>d_billing_start_date)
			{
				alert(' Billing Start Date should be greater than / equal to Start Date');
				nlapiSetFieldValue('custeventbstartdate','')
				return false;
			}
	  
		}
		
	    if(_logValidation(d_end_date)&&_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
		{
			
		    if(d_end_date>d_billing_start_date && d_end_date<d_billing_end_date)
			{
				alert(' Billing End Date should be less than /equal to End Date .');
				nlapiSetFieldValue('custeventbenddate','')
				return false;
			}
	  
		}
		  if(_logValidation(d_end_date)&&_logValidation(d_billing_start_date)&&_logValidation(d_billing_end_date))
		{
		    if(d_end_date>d_billing_start_date && d_end_date<d_billing_end_date)
			{
				alert(' Billing End Date should be greater than /equal to Start Date .');
				nlapiSetFieldValue('custeventbenddate','')
				return false;
			}
	  
		}
		  if(_logValidation(d_billing_start_date)&&_logValidation(d_billing_end_date))
		{
		    if(d_billing_end_date<d_billing_start_date)
			{
				alert(' Billing Start Date should be less than Billing End Date . ');
				nlapiSetFieldValue('custeventbenddate','')
				return false;
			}
	  
		}
*/
		 
			    if(_logValidation(d_start_date)&&_logValidation(d_billing_start_date))
				{
				    if(d_billing_start_date<d_start_date)
					{
						alert(' Billing Start Date of the project should be greater than / equal to Start Date');
						nlapiSetFieldValue('custeventbstartdate','')
						return false;
					}
			  
				}
				 if(_logValidation(d_start_date)&&_logValidation(d_billing_end_date))
				{					
				    if(d_billing_start_date>d_end_date && d_billing_start_date>d_start_date)
					{
						alert(' Billing Start Date of the project should be less than / equal to End Date');
						nlapiSetFieldValue('custeventbstartdate','')
						return false;
					}
			  
				}
			    if(_logValidation(d_end_date)&&_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
				{
					
				    if(d_billing_end_date>d_start_date && d_billing_end_date>d_end_date)
					{
						alert(' Billing End Date of the project should be less than /equal to End Date .');
						nlapiSetFieldValue('custeventbenddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_end_date)&&_logValidation(d_billing_start_date)&&_logValidation(d_billing_end_date))
				{
				    if(d_billing_end_date<d_start_date && d_billing_end_date<d_end_date)
					{
						alert(' Billing End Date of the project should be greater than /equal to Start Date .');
						nlapiSetFieldValue('custeventbenddate','')
						return false;
					}
			  
				}
				  if(_logValidation(d_billing_end_date)&&_logValidation(d_billing_start_date))
				{
				    if(d_billing_end_date<d_billing_start_date)
					{
						alert(' Billing Start Date should be less than Billing End Date . ');
						nlapiSetFieldValue('custeventbenddate','')
						return false;
					}
			  
				}

	
   }//Billing End Date
      
   if(name == 'custevent1')
   {
  	// if(i_flag_t!=1)
	 {
	 	var i_project = nlapiGetFieldValue('project');
		var i_resource = nlapiGetFieldValue('allocationresource');		
		
		var temp = '/app/site/hosting/scriptlet.nl?script=169&deploy=1&custscript_project='+i_project+'&custscript_resource='+i_resource
	    var objWind = window.open(temp, "_blank", "toolbar=no,menubar=0,status=0,copyhistory=0,scrollbars=yes,resizable=1,location=0,Width=500,Height=450");
		
		//i_flag_t = 1		
	 }
   		
   }//Service Item
   
   if(name =='allocationresource')
   {
   	 var i_resource = nlapiGetFieldValue('allocationresource');
	 
	 var i_service_item = nlapiGetFieldValue('assignee','custevent1');
	 
	 var i_project = nlapiGetFieldValue('project');
	 
	 var i_currency = get_project_currency(i_project);
		 
	 var i_unit_cost  = get_employee_labour_cost(i_resource);
	 
	 nlapiSetFieldValue('custevent2',i_unit_cost)
	 
	 var i_bill_rate = get_project_bill_rate(i_project,i_currency,i_service_item)
	 nlapiSetFieldValue('custevent3',i_bill_rate)
	
   }//Resource
   
   //Added by Vinod
   if(name == 'custeventrbillable')
   {
		if(nlapiGetFieldValue('project') != null && nlapiGetFieldValue('project').length > 0)
		{
			var ProjType = nlapiLookupField('job', nlapiGetFieldValue('project'), 'jobtype');
			if(ProjType == 1)
			{
				nlapiDisableField('custevent_otbillable', true);
				nlapiDisableField('custevent_otpayable', true);
				nlapiDisableField('custevent_otrate', true);
			}
			else
			{
				if(nlapiGetFieldValue('custeventrbillable') == 'T')
				{
					nlapiDisableField('custevent_otbillable', false);
					nlapiDisableField('custevent_otpayable', false);
					nlapiDisableField('custevent_otserviceitem', false);
					nlapiDisableField('custevent_otrate', false);
				}
				else
				{
					nlapiSetFieldValue('custevent_otbillable', false);
					nlapiSetFieldValue('custevent_otpayable', false);
					nlapiSetFieldValue('custevent_otserviceitem', '');
					nlapiSetFieldValue('custevent_otrate', 0.00);
					
					nlapiDisableField('custevent_otbillable', true);
					nlapiDisableField('custevent_otpayable', true);
					nlapiDisableField('custevent_otserviceitem', true);
					nlapiDisableField('custevent_otrate', true);
				}
			}
		}
	}
}

// END FIELD CHANGED ================================================

function add_item(arr)
{
  if(_logValidation(arr))
  {
	var split_array = new Array();
	split_array = arr.split('##');
	var i_item =  split_array[0];
	var i_project = split_array[1];
	var i_resource = split_array[2];		
			
			
  //	nlapiSetFieldValue('custevent1',i_item)
		
    var i_unit_cost  = get_employee_labour_cost(i_resource);
	 
	nlapiSetFieldValue('custevent2',i_unit_cost)		
		
	var i_currency = get_project_currency(i_project);
		
	 var i_bill_rate = get_project_bill_rate(i_project,i_currency,i_item)
	 
	 nlapiSetFieldValue('custevent1',i_item,false)
	 nlapiSetFieldValue('custevent3',i_bill_rate,false)
	
  }//Item	
	
}




// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================


function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function get_project_bill_rate(i_project,i_currency,i_service_item)
{
	var i_bill_rate = 0;
	if(_logValidation(i_project)&&_logValidation(i_currency)&&_logValidation(i_service_item))
	{
	var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_proj',null,'is',i_project);
	filter[1] = new nlobjSearchFilter('custrecord_currency1',null,'is',i_currency);
	filter[2] = new nlobjSearchFilter('custrecord_item1',null,'is',i_service_item);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_rate');
	
	 var a_search_results = nlapiSearchRecord('customrecord_customraterecord',null,filter,columns);
	 
	 if (_logValidation(a_search_results)) 
	 {	 	
	 	for (var t = 0; t < a_search_results.length; t++) 
		{
	 	  i_bill_rate = a_search_results[t].getValue('custrecord_rate');		  
		  
		 if(!_logValidation(i_bill_rate))
		 {
		 	i_bill_rate = 0
		 }
		
	 		
	  	}
	 }
	}//Project
	
	return i_bill_rate;
}

function get_project_currency(i_project)
{
  var i_currency;
  if(_logValidation(i_project))
  {
  	var o_projectOBJ = nlapiLoadRecord('job',i_project);
	
	if(_logValidation(o_projectOBJ))
	{
		i_currency = o_projectOBJ.getFieldValue('currency')
		
	}//Project OBJ	
  }//Project
  
  return i_currency;	
}//Project Currency


function get_employee_labour_cost(i_resource)
{
  var i_labour_cost = 0;
  if(_logValidation(i_resource))
  {
  	var o_employeeOBJ = nlapiLoadRecord('employee',i_resource);
	
	if(_logValidation(o_employeeOBJ))
	{
		i_labour_cost = o_employeeOBJ.getFieldValue('laborcost')
		
		 if(!_logValidation(i_labour_cost))
		 {
		 	i_labour_cost = 0
		 }
		
	}//Employee OBJ	
  }//Resource	
  return i_labour_cost;
}//Employee Labour Cost
// END FUNCTION =====================================================







