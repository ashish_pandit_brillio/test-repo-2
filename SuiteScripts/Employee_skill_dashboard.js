function skill_dash_board_suitelet(request, response){
	try{
	nlapiLogExecution('DEBUG','method',request.getMethod());
	if (request.getMethod() == 'GET' )
	{
		
		var User_ID = nlapiGetUser();
      if(User_ID == 41571 ||User_ID== 62082)
        {
          User_ID =442 ;
        }
		var s_request_type = request.getParameter('share_type');
		var employee = nlapiLookupField('employee', User_ID,['entityid','department','custentity_reportingmanager','custentity_skill_updated']);
		var employee_name = employee.entityid;
		var employee_dep = employee.department;
		var reporting_manager = employee.custentity_reportingmanager;
		var skill_updated = employee.custentity_skill_updated;
		if(skill_updated == 'T')
		{
			var form = nlapiCreateForm('You have already submitted the Skills. Thank you !!!" ');
			
		}
		else
		{
		var email = nlapiLookupField('employee',reporting_manager,'email');
		
		var col = new Array();
		col[0] = new nlobjSearchColumn("name");
		var practice_search = nlapiSearchRecord('department',null,['internalid','anyof',employee_dep],col);
		var emp_dep=practice_search[0].getValue('name');
		
		var form = nlapiCreateForm('Employee Skill Update');
		form.setScript('customscript_employee_skill_field_change');
		var reporting_id=form.addField('reprtng_id','select','Employee','employee').setDisplayType('hidden');
		reporting_id.setDefaultValue(reporting_manager);
		var rm=form.addField('reprtng_mangr','text','Approver').setDisplayType('hidden');
		rm.setDefaultValue(email);
		var group = form.addFieldGroup( 'employeegroup', 'Employee Information');
		var skill_group = form.addFieldGroup( 'skillgroup', 'Skill Update');
		var certicate_group = form.addFieldGroup( 'certificategroup', 'Certification');
		var user_name = form.addField('custpage_employee','text','Employee',null,'employeegroup');
		user_name.setDefaultValue(employee_name);
		user_name.setDisplayType('inline');
		var employee_view = form.addField('share_type', 'text','Request Type').setDisplayType('hidden');
		employee_view.setDefaultValue('EmployeeView');
		var user_name = form.addField('custpage_department','text','Department',null,'employeegroup').setDisplayType('disabled');;
		user_name.setDefaultValue(emp_dep);
		var practice_list = getTaggedFamilies(employee_dep);
        var family = form.addField('custpage_family','multiselect','Family','null','skillgroup').setMandatory(true);
          family.addSelectOption('','');
		for(var i=0;i<practice_list.length;++i){
        var n = practice_list[i].getValue('internalid');
	         family.addSelectOption(n,practice_list[i].getValue('name'));
		}
			//var family_search = searchRecord('customrecord542',null,['inactive','is','F'],['name']);customrecord_employee_skills
		var primary= form.addField('custpage_primary_skill','multiselect','Primary Skill(Multi select. Max allowed 3)',null,'skillgroup')
		//primary.setHelpText('Can select multiple from dropdown list.Max 3 are allowed','T');
		form.addField('custpage_secondary_skill','text','Secondary Skill',null,'skillgroup');
		//
		var certificate_tab = form.addSubTab('certificate_tab', 'Certification Details');
		var certificate_sublist = form.addSubList('certificate_sublist', 'inlineeditor', 'Certification', 'certificate_tab');
		var fld_practice = certificate_sublist.addField('custpage_certificate', 'text', 'Certification');
		var fld_sub_practice = certificate_sublist.addField('custpage_cstart_date', 'date', 'Start Date');
		var fld_emp_level = certificate_sublist.addField('custpage_cend_date', 'date', 'End Date'); //customlist_fp_rev_rec_level
		
		form.addSubmitButton('Submit');
		}
		response.writePage(form);
	}
	else
	{	
			var s_request_type = request.getParameter('share_type');
    		  var emp_id = nlapiGetUser();
			if(s_request_type == 'EmployeeView')
			{
				var post_form = nlapiCreateForm('Skills Updated Successfully!!!');
				//form.setScript('customscript_employee_skill_field_change');
			}
			//else
			{
				//if(s_request_type == 'approverview')
				{
				//var record = request.getParameter('record_id');
				//var post_form = nlapiCreateForm('Skills Approved Successfully!!!');
			//	var rec = nlapiLoadRecord('customrecord_employee_master_skill_data',record);
				//var emp_id = rec.getFieldValue('custrecord_employee_skill_updated');
				//rec.setFieldValue('custrecord_skill_status','2');
				//nlapiSubmitRecord(rec,true,true);
				//Update Employee Record
				nlapiSubmitField('employee',parseInt(emp_id), 'custentity_skill_updated', 'T');
				}
		
			}
			
		//	nlapiSendEmail(442, 'sai.vannamaredddy@brillio.com', 'FP Project Monthly effort confirmed: ', strVar,a_participating_mail_id,null,a_emp_attachment);
			response.writePage(post_form);
	}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}
}
function getTaggedFamilies(emp_dep)
{
try{
  var parent_practice = nlapiLookupField('department',emp_dep,'custrecord_parent_practice');
var practice_filter = [
		        
		           [ [ 'custrecord_skill_emp_dep', 'anyOf', emp_dep ],'or',[ 'custrecord_skill_emp_dep', 'anyOf', parent_practice]],
		                'and',
		                [ 'isinactive',
		                        'anyOf', 'F' ]
		         ];

		var practice_search_results = searchRecord('customrecord_employee_skill_family_list', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		               	new nlobjSearchColumn("internalid")]);


	return practice_search_results;
}catch(err){
	throw err;
	}
}

		
		
			 