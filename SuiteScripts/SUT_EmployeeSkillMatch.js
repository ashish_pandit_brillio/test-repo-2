/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Jan 2019     Aazamali Khan
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function getEmployees(request, response){
	try
	{
		var body = request.getBody();
		var searchResultAllocation = null;
		var firstIndex , secondIndex , endDate ,skillFamily ,skillsArray,frfstartDate,mode;
		if (_logValidation(body)) {
			var bodyObj = JSON.parse(body);
			if(bodyObj.mandatorySkills)
				skillsArray = bodyObj.mandatorySkills.id;
			if(bodyObj.skillFamily)
				skillFamily = bodyObj.skillFamily.id;
			if(bodyObj.endDate)
				endDate = bodyObj.startDate.name;
			if(bodyObj.mode)
				mode = bodyObj.mode.name;
		}
      	nlapiLogExecution('AUDIT','body',body);
		nlapiLogExecution('AUDIT','endDate',endDate);
		nlapiLogExecution('AUDIT','skillFamily',skillFamily);
		nlapiLogExecution('AUDIT','skillsArray',skillsArray);
		if(_logValidation(endDate)){
			frfstartDate = endDate;//endDate;
		}
		var searchResult = getEmployeeSkillPriority(skillsArray,skillFamily);
		nlapiLogExecution('AUDIT','searchResult',JSON.stringify(searchResult));
		if (_logValidation(mode)) {
			firstIndex = parseInt(mode) * 20;
			secondIndex = firstIndex + 20
		}else{
			firstIndex = 0;
			secondIndex = firstIndex + 20;
		}
		var dataOut = [];
		if(searchResult){
			if(searchResult.length< secondIndex){
				secondIndex = searchResult.length;
			}
			for (var int = firstIndex; int < secondIndex; int++) {
				var obj ={};
				var empId = searchResult[int].getValue("custrecord_employee_skill_updated");
				var empName = searchResult[int].getText("custrecord_employee_skill_updated");
				var availableSkills = searchResult[int].getValue("formulanumeric");
				var visaSearch =  GetVisa(empId);
				if (visaSearch) {
					obj.visa = {"name" :visaSearch[0].getText("custrecord_visa")};
				}else{
					obj.visa = {"name" :"NA"};
				}
				obj.employee = {"name" :GetManagerName(empName) ,"id" :empId};
				obj.practice = {"name":nlapiLookupField("employee", empId, "department", true),"id":nlapiLookupField("employee", empId, "department", false)};
				if (_logValidation(frfstartDate)) {
					searchResultAllocation = getAllocationStatus(empId,frfstartDate);
				}
				if(searchResultAllocation){
					var workLocation = searchResultAllocation[0].getText("custeventwlocation");
					var projectCategory = searchResultAllocation[0].getText("custentity_project_allocation_category","job",null);
					nlapiLogExecution('DEBUG','Project Category : ',projectCategory);
					obj.role = {"name":searchResultAllocation[0].getValue("title","employee",null)};
					obj.location = {"name":workLocation,"id":searchResultAllocation[0].getValue("custeventwlocation")};
					obj.customer = {"name":searchResultAllocation[0].getText("customer"),"id":searchResultAllocation[0].getValue("customer")};

					if(projectCategory == "Bench"){
						s_status = "Bench";
						obj.availablefrom = {"name":"Available" + " : "+s_status};
						//tempArr = [resultArray.length,resultArray,empId,employeeName,s_status,workLocation];
					}else{
						s_status = searchResultAllocation[0].getValue("enddate");
						obj.availablefrom = {"name":"Available From" + " : "+s_status};
						//tempArr = [resultArray.length,resultArray,empId,employeeName,s_status,workLocation];
					}
				}else{
					var workLocation = nlapiLookupField("employee", empId, "location", true);
					var locationId = nlapiLookupField("employee", empId, "location");
					var role = nlapiLookupField("employee", empId, "title");
					obj.role = {"name":role};
					obj.location = {"name":workLocation,"id":locationId};
					obj.availablefrom = {"name":"Available"}
					//tempArr = [resultArray.length,resultArray,empId,employeeName,s_status,nlapiLookupField("employee",empId,"location",true)];
				}
				getMatchRating(skillsArray.length,availableSkills,obj);
				dataOut.push(obj);
			}
		}
		response.write(JSON.stringify(dataOut));
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}

}
function getAllocationStatus(empId,startDate){
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["enddate","onorafter",startDate], 
			 "AND", 
			 ["resource","anyof",empId]
			 ], 
			 [
			  new nlobjSearchColumn("id").setSort(false), 
			  new nlobjSearchColumn("resource"), 
			  new nlobjSearchColumn("customer"), 
			  new nlobjSearchColumn("company"), 
			  new nlobjSearchColumn("projecttask"), 
			  new nlobjSearchColumn("numberhours"), 
			  new nlobjSearchColumn("startdate"), 
			  new nlobjSearchColumn("percentoftime"), 
			  new nlobjSearchColumn("allocationunit"), 
			  new nlobjSearchColumn("notes"), 
			  new nlobjSearchColumn("enddate"), 
			  new nlobjSearchColumn("allocationtype"), 
			  new nlobjSearchColumn("custeventbstartdate"), 
			  new nlobjSearchColumn("custeventbenddate"), 
			  new nlobjSearchColumn("custeventrbillable"), 
			  new nlobjSearchColumn("custeventwlocation"), 
			  new nlobjSearchColumn("custevent_practice"), 
			  new nlobjSearchColumn("custevent1"), 
			  new nlobjSearchColumn("custevent2"), 
			  new nlobjSearchColumn("custevent3"), 
			  new nlobjSearchColumn("custevent4"), 
			  new nlobjSearchColumn("custevent_otbillable"), 
			  new nlobjSearchColumn("custevent_otpayable"), 
			  new nlobjSearchColumn("custevent_otrate"), 
			  new nlobjSearchColumn("custevent_tenuredisctapplicable"), 
			  new nlobjSearchColumn("custevent_workcityra"), 
			  new nlobjSearchColumn("custevent_workstatera"), 
			  new nlobjSearchColumn("custevent_holiday_service_item"), 
			  new nlobjSearchColumn("custevent_leave_service_item"), 
			  new nlobjSearchColumn("custevent_ra_is_shadow"), 
			  new nlobjSearchColumn("custevent_30_days_prior_notification"),
			  new nlobjSearchColumn("custentity_project_allocation_category","job",null),
			  new nlobjSearchColumn("title","employee",null)
			  ]
	);
	return resourceallocationSearch;
}
function createString(skillArray){
	var temp="";
	for (var int = 0; int < skillArray.length; int++) {
		temp += "CASE WHEN INSTR({custrecord_primary_updated.id},"+skillArray[int]+",1)>0 THEN 1 ELSE 0 END";
		if((skillArray.length - int)>1){
			temp += " +";
		}
	}
	return temp;
} 
function getEmployeeSkillPriority(arrSkills,familyId) {
	nlapiLogExecution("ERROR",'arrSkills',arrSkills);
	var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data",null,
			[
			 ["custrecord_primary_updated","anyof",arrSkills]
			 , 
			 "AND", 
			 ["custrecord_family_selected","anyof",familyId],"AND", 
			 ["custrecord_employee_skill_updated.custentity_employee_inactive","is","F"]
			 ], 
			 [
			  new nlobjSearchColumn("custrecord_employee_skill_updated"), 
			  new nlobjSearchColumn("custrecord_primary_updated"),
			  new nlobjSearchColumn("custrecord_family_selected"),	
			  new nlobjSearchColumn("formulatext").setFormula("{custrecord_primary_updated.id}"), 
			  new nlobjSearchColumn("formulanumeric").setFormula(createString(arrSkills)).setSort(true)
			  ]
	);
	return customrecord_employee_master_skill_dataSearch;
}
function getMatchRating(requireSkills,avaliableSkills,obj) {
	var result = (parseInt(avaliableSkills)/parseInt(requireSkills))*100;
	//nlapiLogExecution('AUDIT', 'result : ', result);
	if (Math.round(result) > 60) {
		obj.mandatorySkills = {"name":"Good Match"+" : "+avaliableSkills + "/" + requireSkills}
	}else if (Math.round(result) <= 0 ){
		obj.mandatorySkills = {"name":"No Match"+" : "+avaliableSkills + "/" + requireSkills}
	}else{
		obj.mandatorySkills = {"name":"Partial Match"+" : "+avaliableSkills + "/" + requireSkills}
	}
}
function GetManagerName(tempString) {
	var s_manager = "";
	//var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
	temp = tempString.indexOf("-");
	if(temp>0)
	{
		var s_manager = tempString.split("-")[1];
	}
	else{
		var s_manager = tempString;
	}
	return s_manager;
}
function GetVisa(empId) {
	var customrecord_empvisadetailsSearch = nlapiSearchRecord("customrecord_empvisadetails",null,
			[
			 ["custrecord_visaid","anyof",empId], 
			 "AND", 
			 ["custrecord_validtill","notbefore","today"]
			 ], 
			 [
			  new nlobjSearchColumn("scriptid").setSort(false), 
			  new nlobjSearchColumn("custrecord_visa"), 
			  new nlobjSearchColumn("custrecord27"), 
			  new nlobjSearchColumn("custrecord_validtill"), 
			  new nlobjSearchColumn("custrecord28")
			  ]
	);
	return customrecord_empvisadetailsSearch;
}
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}