/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2016     shruthi.l
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try{
		nlapiLogExecution('Debug', 'context', nlapiGetContext().getExecutionContext());
		if(nlapiGetContext().getExecutionContext()!='userinterface'){
		nlapiInitiateWorkflow(nlapiGetRecordType(), nlapiGetRecordId(), 'customworkflow71');
		}
	} catch (err) {
		nlapiLogExecution('ERROR',
		        'TS Quick Approval Delete : deleteQuickPoolRecord', err);
		throw err;
	}
}