/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Jul 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function afterSubmitDocumentReceivedMailTrigger(type) {

	if (type == 'edit' || type == 'xedit') {
		var old_record = nlapiGetOldRecord();
		var old_doc_received = old_record
				.getFieldValue('custbody_exp_doc_received');
		var new_doc_received = nlapiGetFieldValue('custbody_exp_doc_received');
		var remarks = nlapiGetFieldValue('custbody_exp_doc_remark');

		if (old_doc_received != new_doc_received && new_doc_received == 'T') {
			sendDocumentReceivedMail(nlapiGetRecordId(), remarks.trim());
		}
	}
}

function sendDocumentReceivedMail(expenseId, remarks) {
	try {
		var expenseDetails = getExpenseDetails(expenseId);
		nlapiLogExecution('debug', 'nlapiGetRecordId', expenseId);
		nlapiLogExecution('debug', 'remarks', remarks);

		var employeeMailContent = documentReceivedMailTemplate(expenseDetails,
				remarks);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
				expenseDetails.Employee.email, employeeMailContent.Subject,
				employeeMailContent.Body,
				expenseDetails.SecondLevelApprover.email, null, {
					entity : expenseDetails.Employee.internalid,
					transaction : expenseId
				});

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendDocumentReminderMail', err);
	}
}

function documentReceivedMailTemplate(expenseDetails, remarks) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';

	htmltext += "<p>This is to inform you that the supporting documents for the expense "
			+ expenseDetails.Expense.Reference
			+ " have been received by the finance team.</p>";

	htmltext += "<p>The bills are subject to verification.</p>";

	if (isNotEmpty(remarks)) {
		htmltext += "<p>The following additional comments have been added : <br/>";
		htmltext += "<i>" + remarks + "</i></p>";
	}

	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
		Subject : 'Expense : ' + expenseDetails.Expense.Reference
				+ ' - Document Received',
		Body : addMailTemplate(htmltext)
	};
}
