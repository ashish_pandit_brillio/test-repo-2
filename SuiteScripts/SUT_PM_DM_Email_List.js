/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Aug 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var context = nlapiGetContext();
		var email_list = getEmailIdList();
		var fileName = "";
		var content = "";

		if (context.getDeploymentId() == 'customdeploy_sut_pm_email_list') {
			fileName = "PM Email List.txt";
			content = email_list.PM;
		} else if (context.getDeploymentId() == 'customdeploy_sut_dm_email_list') {
			fileName = "DM Email List.txt";
			content = email_list.DM;
		}

		nlapiLogExecution('debug', 'content', content);

		// show the file as response
		var file = nlapiCreateFile(fileName, 'PLAINTEXT', content);
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		throw err;
	}
}

function getEmailIdList() {
	try {
		var pm_list = [];
		var dm_list = [];

		// load the saved search and push all the email ids into the respective
		// arrays
		searchRecord(null, 1037).forEach(
				function(allocation) {

					if (allocation.getValue('email',
							'custentity_projectmanager')) {
						pm_list.push(allocation.getValue('email',
								'custentity_projectmanager'));
					}

					if (allocation.getValue('email',
							'custentity_deliverymanager')) {
						dm_list.push(allocation.getValue('email',
								'custentity_deliverymanager'));
					}
				});

		nlapiLogExecution('debug', 'pm_list', pm_list.length);
		nlapiLogExecution('debug', 'dm_list', dm_list.length);

		// remove the duplicate email ids
		pm_list = _.uniq(pm_list);
		dm_list = _.uniq(dm_list);

		nlapiLogExecution('debug', 'pm_list', pm_list.length);
		nlapiLogExecution('debug', 'dm_list', dm_list.length);

		// create a string by concatenating emails with comma
		var pm_email_list = '';
		pm_list.forEach(function(emp) {
			pm_email_list += emp;
			pm_email_list += ";";
		});

		var dm_email_list = '';
		dm_list.forEach(function(emp) {
			dm_email_list += emp;
			dm_email_list += ";";
		});

		return {
			PM : pm_email_list,
			DM : dm_email_list
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmailIdList', err);
		throw err;
	}
}
