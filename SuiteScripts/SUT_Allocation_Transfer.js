/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Jun 2015     Nitish Mishra
 *
 * Script Modification Log:
   -- Date --			-- Modified By --				--Requested By--				-- Description --
   11 FEB 2020			Praveena Madem						Deepak						Updated logic to display and working functionality of go back button function
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var mode = request.getMethod();

		if (mode == 'POST') {
			postCreateAllocations(request);
	} else {
			generateAllocationSelectionScreen(request);
			}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function generateAllocationSelectionScreen(request) {
	try {
		var form = nlapiCreateForm('Allocation Transfer');
		form.setScript('customscript_cs_sut_alloc_transfer');

		var from_project_id = request.getParameter('fmp');
		var to_project_id = request.getParameter('tp');

		var from_project = form.addField('custpage_from_project', 'select',
				'From Project', 'job').setMandatory(true);

		if (from_project_id) {
			from_project.setDefaultValue(from_project_id);
			from_project.setDisplayType('inline');

			form.addField('custpage_from_project_type', 'inlinehtml', 'Type')
					.setDefaultValue(
							nlapiLookupField('job', from_project_id,
									'jobbillingtype', true));
		}

		var to_project = form.addField('custpage_to_project', 'select',
				'To Project', 'job').setMandatory(true);

		if (to_project_id) {
			to_project.setDefaultValue(to_project_id);
			to_project.setDisplayType('inline');

			form.addField('custpage_to_project_type', 'inlinehtml', 'Type')
					.setDefaultValue(
							nlapiLookupField('job', to_project_id,
									'jobbillingtype', true));
		}

		var allocationList = form.addSubList('custpage_allocation_list',
				'list', 'Allocations');

		allocationList.addField('id', 'select', 'allocation',
				'resourceallocation').setDisplayType('hidden');

		allocationList.addField('select', 'checkbox', 'Select');

		allocationList.addField('resource', 'select', 'Resource', 'employee')
				.setDisplayType('inline');

		allocationList.addField('startdate', 'date', 'Start Date');
		allocationList.addField('enddate', 'date', 'End Date');
		allocationList.addField('billrate', 'currency', 'Bill Rate');
		allocationList.addField('otrate', 'currency', 'OT Rate');
		allocationList.addField('site', 'select', 'Site',
				'customlistonsiteoffsitelist').setDisplayType('inline');
		allocationList.addField('isbillable', 'checkbox', 'Billable?')
				.setDisplayType('inline');
		allocationList.addField('percent', 'percent', '%age Allocation');
		allocationList.addField('location', 'text', 'Location', '');
		allocationList.addField('newlocation', 'text', 'Location')
				.setDisplayType('hidden');

		allocationList.addField('newstartdate', 'date', 'Start Date')
				.setDisplayType('entry');
		allocationList.addField('newenddate', 'date', 'End Date')
				.setDisplayType('entry');
		allocationList.addField('newbillrate', 'currency', 'Bill Rate')
				.setDisplayType('entry');
		allocationList.addField('newotrate', 'currency', 'OT Rate')
				.setDisplayType('entry');
		allocationList.addField('newisbillable', 'checkbox', 'Is Billable')
				.setDisplayType('entry');
		allocationList.addField('newpercent', 'percent', '%age Allocation')
				.setDisplayType('entry');
		allocationList.addField('newsite', 'select', 'Site',
				'customlistonsiteoffsitelist').setDisplayType('entry');
		// allocationList.addField('newlocation', 'select', 'Work Location',
		// 'location').setDisplayType('entry');

		if (from_project_id) {
			allocationList
					.setLineItemValues(getAllocationForSelectedProject(from_project_id));

			form.addButton('custpage_btn_go_back', 'Go Back', 'goBack');
			form.addSubmitButton('Submit');
		} else {
			form.addButton('custpage_btn_get_allocation', 'Get Allocations',
					'getProjectDetails');
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateAllocationSelectionScreen', err);
		throw err;
	}
}

function getAllocationForSelectedProject(projectId) {
	try {
		var allocationSublistDetails = [];

		searchRecord(
				'resourceallocation',
				null,
				[
						new nlobjSearchFilter('project', null, 'anyof',
								projectId),
						new nlobjSearchFilter('custentity_employee_inactive',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('custentity_implementationteam',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('isjobresource', 'employee',
								'is', 'T') ],
				[ new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custevent3'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_otrate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('enddate').setSort(true),
						new nlobjSearchColumn('resource') ]).forEach(
				function(allocation) {
					allocationSublistDetails.push({
						id : allocation.getId(),
						resource : allocation.getValue('resource'),
						startdate : allocation.getValue('startdate'),
						enddate : allocation.getValue('enddate'),
						percent : allocation.getValue('percentoftime'),
						billrate : allocation.getValue('custevent3'),
						isbillable : allocation.getValue('custeventrbillable'),
						site : allocation.getValue('custevent4'),
						otrate : allocation.getValue('custevent_otrate'),
						location : allocation.getText('custeventwlocation'),
						newlocation : allocation.getValue('custeventwlocation')
					});
				});
		return allocationSublistDetails;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveAllocationForSelectedProject', err);
		throw err;
	}
}

function postCreateAllocations(request) {
	try {
		var form = nlapiCreateForm('Allocation Transfer - Result');
		form.setScript('customscript_cs_sut_alloc_transfer');
        
		var project = request.getParameter('custpage_to_project');
var i_fromproject=request.getParameter('custpage_from_project');

		var billing_type = nlapiLookupField('job', project, 'jobbillingtype');
		var serviceItem = null;

		if (billing_type == 'TM') {
			serviceItem = 'ST';
		} else {
			serviceItem = 'FP';
		}

		var allocationCount = request
				.getLineItemCount('custpage_allocation_list');

		var failedAllocations = [];
		var createdAllocations = [];
		var a_get_transfer_allocation_details=[];
		var i_process_bulk=0;
		for (var linenum = 1; linenum <= allocationCount; linenum++) {
		var doCreate = request.getLineItemValue('custpage_allocation_list','select', linenum);
		if (isTrue(doCreate)) {
			var obj_data={'resource':request.getLineItemValue('custpage_allocation_list','resource', linenum),
					     'doCreate':request.getLineItemValue('custpage_allocation_list','select', linenum),
						 'newstartdate':request.getLineItemValue('custpage_allocation_list','newstartdate', linenum),
			             'newenddate':request.getLineItemValue('custpage_allocation_list','newenddate', linenum),
		                 'isBillable' : request.getLineItemValue('custpage_allocation_list', 'newisbillable',linenum),
						 'newbillrate':request.getLineItemValue('custpage_allocation_list', 'newbillrate',linenum),
						 'newotrate':request.getLineItemValue('custpage_allocation_list','newotrate', linenum),
						 'newsite':request.getLineItemValue('custpage_allocation_list','newsite', linenum),
						 'newlocation':request.getLineItemValue('custpage_allocation_list', 'newlocation',linenum),
						 'newpercent':request.getLineItemValue('custpage_allocation_list','newpercent', linenum)
			};
			a_get_transfer_allocation_details.push(obj_data);
			i_process_bulk++;
		}
		}//END for (var linenum = 1; linenum <= allocationCount; linenum++) {
			if(i_process_bulk>6)//Call SCH script when data lines having more than 6 lines
			{
				    var param=new Array();
					param['custscript_transfer_allocation_details']=JSON.stringify(a_get_transfer_allocation_details);
					param['custscript_project_']=project;
                    param['custscript_from_project_']=i_fromproject;
					param['custscript_billing_type']=billing_type;
                    param['custscript_useremail_']=nlapiGetContext().getEmail();
				    nlapiScheduleScript ('customscript_sch_ra_transfer',null,param);
					nlapiLogExecution('DEBUG','a_get_transfer_allocation_details',JSON.stringify(a_get_transfer_allocation_details));
			        //form.addField('bulkshowmessage', 'text', 'Message').setDisplayType('inline').setDefaultValue('Bulk Process is trigger once it is done email will triggered to you');
		          form.addField('bulkshowmessage', 'text', 'Message').setDisplayType('inline').setDefaultValue('Bulk Process has been triggered. Once the process is done email will be sent to you');
		
			}
			else{
				a_get_transfer_allocation_details=[];
				for (var linenum = 1; linenum <= allocationCount; linenum++) {
					var resource = request.getLineItemValue('custpage_allocation_list',
							'resource', linenum);

					var doCreate = request.getLineItemValue('custpage_allocation_list',
							'select', linenum);

					if (isTrue(doCreate)) {
						try {
						   nlapiLogExecution('debug', 'percent', request.getLineItemValue(
							'custpage_allocation_list', 'newpercent', linenum));
							var allocationRecord = nlapiCreateRecord('resourceallocation');
							allocationRecord.setFieldValue('allocationresource',
									resource);
							allocationRecord.setFieldValue('project', project);

							allocationRecord.setFieldValue('startdate', request
									.getLineItemValue('custpage_allocation_list',
											'newstartdate', linenum));

							allocationRecord.setFieldValue('enddate', request
									.getLineItemValue('custpage_allocation_list',
											'newenddate', linenum));

							var isBillable = request.getLineItemValue(
									'custpage_allocation_list', 'newisbillable',
									linenum);

							allocationRecord.setFieldValue('custeventrbillable',
									request.getLineItemValue(
											'custpage_allocation_list',
											'newisbillable', linenum));

							if (isTrue(isBillable)) {
								allocationRecord.setFieldValue('custeventbstartdate',
										request.getLineItemValue(
												'custpage_allocation_list',
												'newstartdate', linenum));

								allocationRecord.setFieldValue('custeventbenddate',
										request.getLineItemValue(
												'custpage_allocation_list',
												'newenddate', linenum));

								allocationRecord.setFieldValue(
										'custevent_allocation_status', '1');
							} else {
								allocationRecord.setFieldValue(
										'custevent_allocation_status', '2');
							}

							if (request.getLineItemValue('custpage_allocation_list',
									'newbillrate', linenum)) {
								allocationRecord.setFieldValue('custevent3', request
										.getLineItemValue('custpage_allocation_list',
												'newbillrate', linenum));
							} else {
								allocationRecord.setFieldValue('custevent3', 0);
							}

							if (request.getLineItemValue('custpage_allocation_list',
									'newotrate', linenum)) {
								allocationRecord.setFieldValue('custevent_otrate',
										request.getLineItemValue(
												'custpage_allocation_list',
												'newotrate', linenum));
							}

							// unit cost
							allocationRecord.setFieldValue('custevent2', '0.0');

							allocationRecord.setFieldValue('custevent4', request
									.getLineItemValue('custpage_allocation_list',
											'newsite', linenum));

							allocationRecord.setFieldValue('custevent1', serviceItem);

							allocationRecord.setFieldValue('custeventwlocation',
									request.getLineItemValue(
											'custpage_allocation_list', 'newlocation',
											linenum));
		nlapiLogExecution('DEBUG','newpercent '+linenum,request.getLineItemValue('custpage_allocation_list','newpercent', linenum));
							allocationRecord.setFieldValue('allocationamount', (request
									.getLineItemValue('custpage_allocation_list',
											'newpercent', linenum)).split('%')[0]
									.trim());

							allocationRecord.setFieldValue('allocationunit', 'P');

							var id = nlapiSubmitRecord(allocationRecord, true, true);
							nlapiLogExecution('debug', 'Allocation Created', id);
							createdAllocations.push({
								id : id
							});
						} catch (e) {
							nlapiLogExecution('ERROR', 'Allocation Creation', e);
							failedAllocations.push({
								'resource' : resource,
								'error' : e
							});
						}
					}
				}//END for (var linenum = 1; linenum <= allocationCount; linenum++) {
}
		var form = nlapiCreateForm('Allocation Transfer - Result');
		form.setScript('customscript_cs_sut_alloc_transfer');
        form.addField('bulkshowmessage', 'text', 'Message').setDisplayType('inline').setDefaultValue('Bulk Process is trigger once it is done email will triggered to you');
		var success_list = form.addSubList('custpage_list_success',
				'staticlist', 'Allocations Created');
		success_list.addField('id', 'select', 'Allocations',
				'resourceallocation').setDisplayType('inline');
		success_list.setLineItemValues(createdAllocations);
       
		var failed_list = form.addSubList('custpage_list_error', 'staticlist',
				'Allocations Failed');
		failed_list.addField('resource', 'select', 'Resource', 'employee')
				.setDisplayType('inline');
		failed_list.addField('error', 'text', 'Error').setDisplayType('inline');
		failed_list.setLineItemValues(failedAllocations);

		//form.addButton('custpage_go_back', 'Go Back', 'goback');//Commented by praveena on 11-12-2020
		form.addButton('custpage_go_back', 'Go Back', 'window.history.back()');//Added by praveena on 11-12-2020

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'postCreateAllocations', err);
		throw err;
	}
}

// --Client Script
function getProjectDetails() {
	try {
		var from_project = nlapiGetFieldValue('custpage_from_project');
		var to_project = nlapiGetFieldValue('custpage_to_project');

		if (from_project && to_project) {
			var url = nlapiResolveURL('SUITELET',
					'customscript_sut_allocation_transfer',
					'customdeploy_sut_allocation_transfer');
			url += "&fmp=" + from_project + "&tp=" + to_project;
			window.location = url;
		} else {
			alert('Both From and To Project are mandatory');
		}
	} catch (err) {
		alert(err.message);
	}
}

function goBack() {
	try {
		var url = nlapiResolveURL('SUITELET',
				'customscript_sut_allocation_transfer',
				'customdeploy_sut_allocation_transfer');
		window.location = url;
	} catch (err) {
		alert(err.message);
	}
}