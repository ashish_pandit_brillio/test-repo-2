/**
 * @author Jayesh
 */

function suiteletFunction_ExportCSV(request, response)
{
	try
	{
		var disocunt_module_rcrd_id = request.getParameter('custscript_record_id');
		
		var discount_module_rcrd_obj = nlapiLoadRecord('customrecord_discount_module_for_cust',disocunt_module_rcrd_id);
		var customer = discount_module_rcrd_obj.getFieldValue('custrecord_customer_to_apply_discount');
		var projects_to_exclude = discount_module_rcrd_obj.getFieldValues('custrecord_projects_to_exclude_discount');
		var discount_amount = discount_module_rcrd_obj.getFieldValue('custrecord_discount_amount_to_apply');
		var onsite_to_offshore_conversion_rate = discount_module_rcrd_obj.getFieldValue('custrecord_onsite_to_offshore');
		var discount_module_month = discount_module_rcrd_obj.getFieldText('custrecord_month_to_apply_discount');
		var discount_module_year = discount_module_rcrd_obj.getFieldText('custrecord_year_to_apply_discount');
		var i_account_debit_discount_module = discount_module_rcrd_obj.getFieldValue('custrecord_debit_account_to_apply');
		var i_account_credit_discount_module = discount_module_rcrd_obj.getFieldValue('custrecord_credit_account_to_apply');
		var cust_subsidiary = nlapiLookupField('customer',discount_module_rcrd_obj.getFieldValue('custrecord_customer_to_apply_discount'),'subsidiary');
		
		var exclude_tm = discount_module_rcrd_obj.getFieldValue('custrecord_exclude_tm_projects');
		var exclude_fp = discount_module_rcrd_obj.getFieldValue('custrecord_exclude_fp_projects');
		
		var i_month = discount_module_month;
		var i_year = discount_module_year;
		
		var d_start_date = get_current_month_start_date(i_month, i_year);
		var d_end_date = get_current_month_end_date(i_month, i_year);
		
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);
		
		var total_days_in_month = daysInMonth(discount_module_month,discount_module_year);
	
		var emp_detail_arr = new Array();
		var total_onsite = 0;
		var total_offshore = 0;
		
		var filters_search_allocation = new Array();
		filters_search_allocation[0] = new nlobjSearchFilter('internalid', 'customer', 'anyof', parseInt(customer));
		filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date);
		filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date);
		filters_search_allocation[3] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
		
		var filter_counter = 4;
		if(projects_to_exclude)
		{
			filters_search_allocation[filter_counter] = new nlobjSearchFilter('project', null, 'noneof', projects_to_exclude);
			filter_counter++;
		}
		
		if(exclude_tm == 'T')
		{
			filters_search_allocation[filter_counter] = new nlobjSearchFilter('jobbillingtype', 'job', 'noneof', 'TM');
			filter_counter++;
		}
		
		if(exclude_fp == 'T')
		{
			filters_search_allocation[filter_counter] = new nlobjSearchFilter('jobbillingtype', 'job', 'noneof', ['FBM','FBI']);
			filter_counter++;
		}
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('customer');
		columns[1] = new nlobjSearchColumn('company');
		columns[2] = new nlobjSearchColumn('startdate');
		columns[3] = new nlobjSearchColumn('enddate');
		columns[4] = new nlobjSearchColumn('percentoftime');
		columns[5] = new nlobjSearchColumn('resource');
		columns[6] = new nlobjSearchColumn('subsidiary','employee');
		columns[7] = new nlobjSearchColumn('custevent_practice');
		columns[8] = new nlobjSearchColumn('custentity_vertical','job');
		columns[9] = new nlobjSearchColumn('territory','customer');
		columns[10] = new nlobjSearchColumn('subsidiary','customer');
		
		var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
		if (_logValidation(project_allocation_result))
		{
			nlapiLogExecution('audit','len:- ',project_allocation_result.length);
			
			var sr_no = 0;
			for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++)
			{
				var onsite_flag = 0;
				var onsite_offshore_text = '';
				
				var customer = project_allocation_result[i_search_indx].getValue('customer');
				var cust_name = project_allocation_result[i_search_indx].getText('customer');
				var cust_territory = project_allocation_result[i_search_indx].getText('territory', 'customer');
				var cust_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'customer');
				
				var project = project_allocation_result[i_search_indx].getValue('company');
				var project_name = project_allocation_result[i_search_indx].getText('company');
				var proj_vertical = project_allocation_result[i_search_indx].getText('custentity_vertical', 'job');
				
				var resource = project_allocation_result[i_search_indx].getValue('resource');
				var resource_name = project_allocation_result[i_search_indx].getText('resource');
				var resource_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
				var emp_practice = project_allocation_result[i_search_indx].getText('custevent_practice');
				
				var startdate = project_allocation_result[i_search_indx].getValue('startdate');
				var enddate = project_allocation_result[i_search_indx].getValue('enddate');
				var percent_of_time = project_allocation_result[i_search_indx].getValue('percentoftime');
				percent_of_time = percent_of_time.toString();
				percent_of_time = percent_of_time.split('.');
				percent_of_time = percent_of_time[0].toString().split('%');
				
				var startdate_to_compare = nlapiStringToDate(startdate);
				startdate_to_compare = d_start_date < startdate_to_compare ? startdate_to_compare : d_start_date;
				
				var enddate_to_compare = nlapiStringToDate(enddate);
				enddate_to_compare = d_end_date > enddate_to_compare ? enddate_to_compare : d_end_date;
				
				var days_worked_in_month = getDatediffIndays(startdate_to_compare, enddate_to_compare);
				
				var final_prcnt_allocation = ((parseFloat(days_worked_in_month) / parseFloat(total_days_in_month)) * parseFloat(percent_of_time)) / parseFloat(100);
				
				if (parseInt(resource_subsidiary) == 3) {
					total_offshore = parseFloat(total_offshore) + parseFloat(final_prcnt_allocation);
					onsite_offshore_text = 'Offshore';
				}
				else {
					total_onsite = parseFloat(total_onsite) + parseFloat(final_prcnt_allocation);
					onsite_flag = 1;
					onsite_offshore_text = 'Onsite';
				}
				
				emp_detail_arr[sr_no] = {
					'resource': resource,
					'resource_name': resource_name,
					'resource_subsi': resource_subsidiary,
					'emp_practice': emp_practice,
					'startdate': startdate,
					'enddate': enddate,
					'revised_strt_date':nlapiDateToString(startdate_to_compare),
					'revised_end_date':nlapiDateToString(enddate_to_compare),
					'days_worked_in_month':days_worked_in_month,
					'customer': customer,
					'cust_name': cust_name,
					'cust_territory': cust_territory,
					'cust_subsidiary': cust_subsidiary,
					'project': project,
					'project_name':project_name,
					'proj_vertical': proj_vertical,
					'prcnt_of_tym': final_prcnt_allocation,
					'onsite_flag': onsite_flag,
					'onsite_offshore_text':onsite_offshore_text
				};
				sr_no++;
				
			}
			
			total_onsite = parseFloat(total_onsite) * parseFloat(onsite_to_offshore_conversion_rate);
		
			var total_resources = parseFloat(total_onsite) + parseFloat(total_offshore);
			nlapiLogExecution('audit','total_resources:- '+total_resources);
			
			var average_discount_amnt = parseFloat(discount_amount) / parseFloat(total_resources);
			
			down_excel_function(emp_detail_arr,average_discount_amnt,onsite_to_offshore_conversion_rate,total_days_in_month);
		}
		else
		{
			var f_form = nlapiCreateForm("Customer Discount Module");
				
			var result_no_rslt_found = f_form.addField('custpage_resource_nt_found', 'inlinehtml', 'No Record Found');
			result_no_rslt_found.setDefaultValue('No Record Found');
			
			response.writePage(f_form);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function down_excel_function(emp_detail_arr,average_discount_amnt,onsite_to_offshore_conversion_rate,total_days_in_month)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"15%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Employee</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Customer</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Practice</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Vertical</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Territory</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Onsite/Offshore</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Start Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>End Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Revised Start Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Revised End Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Days Worked</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Days in Month</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Percent Allocated</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Average Cost Per Resource</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Total Cost For Employee</td>";
		
		strVar2 += "<\/tr>";
		
		if (_logValidation(emp_detail_arr))
		{
			for (var jk = 0; jk < emp_detail_arr.length; jk++)
			{
				var emp_amount_to_be_tagged = parseFloat(average_discount_amnt) * parseFloat(emp_detail_arr[jk].prcnt_of_tym);
				if (emp_detail_arr[jk].onsite_flag == 1) {
					emp_amount_to_be_tagged = parseFloat(emp_amount_to_be_tagged) * parseFloat(onsite_to_offshore_conversion_rate);
				}
				
				emp_amount_to_be_tagged = emp_amount_to_be_tagged.toString();
				if (emp_amount_to_be_tagged.indexOf(".") > 0) {
					var temp_index = emp_amount_to_be_tagged.indexOf(".") + 3;
					emp_amount_to_be_tagged = emp_amount_to_be_tagged.substr(0, temp_index);
				}
				emp_amount_to_be_tagged = parseFloat(emp_amount_to_be_tagged);
				
				
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"15%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].resource_name + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].project_name + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].cust_name + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].emp_practice + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].proj_vertical + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].cust_territory + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].onsite_offshore_text + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].startdate + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].enddate + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].revised_strt_date + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].revised_end_date + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].days_worked_in_month + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + total_days_in_month + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_detail_arr[jk].prcnt_of_tym + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + average_discount_amnt + "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" + emp_amount_to_be_tagged + "</td>";
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Customer Discount Module Data.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		response.setContentType('XMLDOC','Customer Discount Module Data.xls');
		response.write( file.getValue() );	 
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date = Math.round((toDate - fromDate) / one_day);
	return (date + 1);
}

function daysInMonth(i_month,year) {
	i_month = i_month.trim();
	
	if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	{	  	
		i_month = 1;		
	}	
	else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	{
		i_month = 2;	
	}		
	else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	{	  
		i_month = 3;
	}	
	else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	{
		i_month = 4;
	}	
	else if(i_month == 'May' || i_month == 'MAY')
	{	  
		i_month = 5;
	}	  
	else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	{	 
		i_month = 6;
	}	
	else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	{	  
		i_month = 7;
	}	
	else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	{	  
		i_month = 8;
	}  
	else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	{  	
		i_month = 9;
	}	
	else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	{	 	
		i_month = 10;
	}	
	else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	{	  
		i_month = 11;
	}	
	else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	{	 
		i_month = 12;
	}
	
	year = Number(year).toFixed(0);
    return new Date(year, i_month, 0).getDate();
}
