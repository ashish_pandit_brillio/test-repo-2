// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Salary_Upload_Validate_Process.js
	Author      : Shweta Chopde
	Date        : 30 July 2014
    Description : Suitlet Call for validate process


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try
	{
		var i_CSV_File_ID =  request.getParameter('custscript_csv_file_su');
		var i_monthly_employee =  request.getParameter('custscript_monthly_employee_su');
		var i_hourly_employee =  request.getParameter('custscript_hourly_employee_su');
		var i_account_debit =  request.getParameter('custscript_account_debit_su');
		var i_account_credit =  request.getParameter('custscript_account_credit_su');
		var i_recordID =  request.getParameter('custscript_record_id_su');
		
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Record ID -->' + i_recordID);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' CSV File ID -->' + i_CSV_File_ID);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Monthly Employee -->' + i_monthly_employee);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Hourly Employee -->' + i_hourly_employee);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Account Credit -->' + i_account_debit);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Account Debit -->' + i_account_credit);
		
		// Section Added by Vikrant for 3 new cases
		var i_Salaried_OT =  request.getParameter('custscript_sal_emp_ot_hrs');
		var i_Salaried_OT_Diff =  request.getParameter('custscript_salaried_ot_diff');
		var i_Hourly_Diff =  request.getParameter('custscript_hourly_emp_diff_hrs');
		var i_hourly_FP = request.getParameter('custscript_hourly_emp_fp');
		var i_hourly_FP_DIFF = request.getParameter('custscript_hourly_emp_fp_diff');
		
		var i_hourly_FP_Internal = request.getParameter('custscript_hourly_employee_internal');		
		var i_hourly_FP_Internal_Diff = request.getParameter('custscripthourly_employee_internal_diff');
		
		nlapiLogExecution('DEBUG', 'suiteletFunction',' i_Salaried_OT -->' + i_Salaried_OT);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' i_Salaried_OT_Diff -->' + i_Salaried_OT_Diff);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' i_Hourly_Diff -->' + i_Hourly_Diff);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' i_hourly_FP -->' + i_hourly_FP);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' i_hourly_FP_DIFF -->' + i_hourly_FP_DIFF);
		
		nlapiLogExecution('DEBUG', 'suiteletFunction',' i_hourly_FP_Internal -->' + i_hourly_FP_Internal);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' i_hourly_FP_Internal_Diff -->' + i_hourly_FP_Internal_Diff);
		// End of Section Added by Vikrant for 3 new cases
		
		// ================= Call Schedule Script ================
				
		 var params=new Array();
	     params['custscript_csv_file_id_s_u'] = i_CSV_File_ID
		 params['custscript_monthly_employee_s_u'] = i_monthly_employee
		 params['custscript_hourly_employee_s_u'] = i_hourly_employee
		 params['custscript_account_debit_s_u'] = i_account_debit
		 params['custscript_account_credit_s_u'] = i_account_credit
		 params['custscript_record_id_s_u'] = i_recordID
		 
		 // Section Added by Vikrant for 3 new cases
		 params['custscript_sal_emp_ot_hrs_sch'] = i_Salaried_OT
		 params['custscript_sal_emp_ot_hrs_diff_sch'] = i_Salaried_OT_Diff
		 params['custscript_hourly_emp_diff_hrs_sch'] = i_Hourly_Diff
		 
		 params['custscript_hourly_emp_fp_sch'] = i_hourly_FP
		 params['custscript_hourly_emp_fp_diff_sch'] = i_hourly_FP_DIFF
		 
		 params['custscript_hourly_emp_internal_hrs_sch'] = i_hourly_FP_Internal
		 params['custscript_hourly_emp_internal_diff_sch'] = i_hourly_FP_Internal_Diff
		 // End of Section Added by Vikrant for 3 new cases
				 
		 var status=nlapiScheduleScript('customscript_sch_salary_upload_validate_',null,params);
		 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
				 
		 if(status == 'QUEUED')
		 {
		 	if(_logValidation(i_recordID)) 
			{
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID)
				
				if(_logValidation(o_recordOBJ)) 
				{
					o_recordOBJ.setFieldValue('custrecord_file_status',3)
					o_recordOBJ.setFieldValue('custrecord_button_click_type','VALIDATE')
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
					nlapiLogExecution('DEBUG', 'suiteletFunction', ' Submit ID -->' + i_submitID);
					
				}//Record OBJ				
			}//Record ID			
		 }//QUEUED
		 
		nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process', i_recordID, null,null);
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ......... ' );	
	    		
		 
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value.toString()!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
