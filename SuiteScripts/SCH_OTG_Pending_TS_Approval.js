/**
 * Module Description
 * 
 * Version   Date                          Author                          Remarks  
 * 1.00         26th July 2017          Deepak         
 */

function Main(){
try{
var context = nlapiGetContext();
var slno = '';
var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');
columns[2]=new nlobjSearchColumn('email','approver');
columns[3]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');

// search for employee with 1st year of service as of today

var emp_Oneyr_search = searchRecord('timesheet', 'customsearch1992', null,null);

nlapiLogExecution('debug', 'emp_Oneyr_search.length',emp_Oneyr_search.length);

if(isNotEmpty(emp_Oneyr_search)){
	
	for(var i=0;i<emp_Oneyr_search.length;i++){
		
		var result = emp_Oneyr_search[i];
		var columns = result.getAllColumns();
		
		if(result){
			var employee = result.getValue(columns[0]);
			var timeSheetCount = result.getValue(columns[1]);
			
			var emp_lookUP = nlapiLookupField('employee',parseInt(employee),['firstname','email','custentity_otg_app']);
			var emp_name = emp_lookUP.firstname;
			var emp_email = emp_lookUP.email;
			var otg_download = emp_lookUP.custentity_otg_app;
		/*// get the HR BP email id
		var hr_bp_id = emp_Oneyr_search[i].getValue('custrecord_hrbusinesspartner','department');
		var hr_bp_email = null;
		
		if(isNotEmpty(hr_bp_id)){
			hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
		}*/
		//Trigger email if &&& if app not downloaded	
		if(otg_download == 'F'){
		slno++
		sendServiceMails(emp_name,emp_email,employee,timeSheetCount);
		yieldScript(context);
		}
	}
}
	nlapiLogExecution('debug', 'slno',slno);
}
}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, emp_id,timeSheetCount){
	
	try{
		var bcc =  new Array();
		bcc[0] = 'deepak.srinivas@brillio.com';
		var mailTemplate = serviceTemplate(firstName,timeSheetCount);		
		
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = emp_id;
		if(email)
		nlapiSendEmail('83134', email, mailTemplate.MailSubject, mailTemplate.MailBody,null,bcc,a_emp_attachment);
		nlapiLogExecution('debug', 'chekpoint',email);
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName,pendingTS) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    htmltext += '<p>Dear ' + firstName + ',</p>';  
   // htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
htmltext += '<p>Timesheet approvals have never been easier!</p>';
//htmltext += &nbsp;
//htmltext += '<p>Wishing you a day filled with happiness and a life filled with joy</p>';
//htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=496695&c=3883006&h=f9b97829ba49061e5bb0" alt="Right click to download image." /></p>';
//htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=496695&c=3883006&h=f9b97829ba49061e5bb0" alt="Right click to download image." /></p>';
  //  htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=500778&c=3883006&h=30489326b0e9a0a675d7" alt="Right click to download image." /></p>';
htmltext += '<p>Download OnTheGo today!</br>';
htmltext += 'Click on the link: https://onthegoprod.blob.core.windows.net/onthego/index.html</p>';

htmltext += '<p>NOTE: Timesheets are considered from July month</p>';
htmltext += '<p>With Best Wishes,</br>';
htmltext += 'Information Systems</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "You have "+pendingTS+" timesheet/s to be approved, use OntheGo!"      
    };
}

function yieldScript(currentContext) {

	nlapiLogExecution('AUDIT', 'API Limit Exceeded');
	var state = nlapiYieldScript();

	if (state.status == "FAILURE") {
		nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
		        + state.reason + ' / Size : ' + state.size);
		return false;
	} else if (state.status == "RESUME") {
		nlapiLogExecution('AUDIT', 'Script Resumed');
	}
}
