/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2015     Nitish Mishra
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled (type) {

	try {

		var projectSearch = searchRecord('job', null,
				[ new nlobjSearchFilter('enddate', null, 'after', '12/31/2014') ], [
						new nlobjSearchColumn('custentity_vertical'),
						new nlobjSearchColumn('custentity_vertical', 'customer') ]);

		nlapiLogExecution('debug', 'no. of records : ', projectSearch.length);

		return;

		var context = nlapiGetContext();

		for (var i = 0; i < 1; i++) {

			try {

				if (projectSearch[i].getValue('custentity_vertical', 'customer') != projectSearch[i]
						.getValue('custentity_vertical')) {

					yieldScript(context);
					var project = nlapiLoadRecord('job', projectSearch[i].getId());
					project.setFieldValue('custentity_vertical', projectSearch[i].getValue('custentity_vertical',
							'customer'));
					nlapiSubmitRecord(project);
				}
			}
			catch (err) {
				nlapiLogExecution('debug', 'error in record : ' + projectSearch[i].getId(), err);
			}
		}
	}
	catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
	}
}

function Main () {

	var projects = [ {
		'id' : 6076,
		'vertical' : 'Consumer Industry'
	}, {
		'id' : 5529,
		'vertical' : 'Independent Software Vendor'
	}, {
		'id' : 5718,
		'vertical' : 'Consumer Industry'
	}, {
		'id' : 5534,
		'vertical' : 'Independent Software Vendor'
	}, {
		'id' : 7161,
		'vertical' : 'Independent Software Vendor'
	}, {
		'id' : 5536,
		'vertical' : 'Consumer Industry'
	}, {
		'id' : 5537,
		'vertical' : 'Consumer Industry'
	}, {
		'id' : 5538,
		'vertical' : 'Consumer Industry'
	}, {
		'id' : 5719,
		'vertical' : 'Consumer Industry'
	}, {
		'id' : 5539,
		'vertical' : 'Independent Software Vendor'
	} ];

	projects.forEach(function (project) {

		var rec = nlapiLoadRecord('job', project.id);
		rec.setFieldText('custentity_vertical', project.vertical);
		nlapiSubmitRecord(rec, false, true);
	});
};

function customerProjectVerticalHeadCorrection () {

	try {
		var currentContext = nlapiGetContext();

		// customer - CI, ISV
		var customerSearch = nlapiSearchRecord('customer', null, new nlobjSearchFilter('custentity_vertical', null,
				'anyof', [ '18', '17' ]));

		nlapiLogExecution('debug', 'customer count', customerSearch.length);

		if (false) {// isArrayNotEmpty(customerSearch)) {

			for (var i = 0; i < customerSearch.length; i++) {
				yieldScript(currentContext);
				try {
					nlapiSubmitField('customer', customerSearch[i].getId(), 'custentity_verticalhead', '1535');
					// nlapiLogExecution('debug', 'customer : ' +
					// customerSearch[i].getId(), 'done');
				}
				catch (er) {
					nlapiLogExecution('error', 'customer : ' + customerSearch[i].getId(), er);
				}
				/*
				 * var rec = nlapiLoadRecord('customer', '7180', { recordmode :
				 * 'dynamic' }); rec.setFieldValue('custentity_verticalhead',
				 * '1535'); nlapiSubmitRecord(rec, true, true);
				 */
			}
		}

		nlapiLogExecution('debug', 'customer', 'done');

		// project - CI , ISV
		var jobSearch = nlapiSearchRecord('job', null, new nlobjSearchFilter('custentity_vertical', null, 'anyof',
				'anyof', [ '18', '17' ]));

		nlapiLogExecution('debug', 'project count', customerSearch.length);

		if (isArrayNotEmpty(jobSearch)) {

			for (var i = 0; i < jobSearch.length; i++) {
				yieldScript(currentContext);
				try {
					nlapiSubmitField('job', jobSearch[i].getId(), 'custentity_verticalhead', '1535');
					// nlapiLogExecution('debug', 'job : ' +
					// jobSearch[i].getId(), 'done');
				}
				catch (er) {
					nlapiLogExecution('error', 'job : ' + jobSearch[i].getId(), er);
				}
				/*
				 * var rec = nlapiLoadRecord('job', jobSearch[i].getId(), {
				 * recordmode : 'dynamic' });
				 * rec.setFieldValue('custentity_verticalhead', '1535');
				 * nlapiSubmitRecord(rec, true, true); return;
				 */
			}
		}

		nlapiLogExecution('debug', 'job', 'done');
	}
	catch (err) {
		nlapiLogExecution('error', 'customerProjectVerticalHeadCorrection', err);
	}
}

function yieldScript (currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == constants.K_YIELD_FAILURE) {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason + ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == constants.K_YIELD_RESUME) {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}