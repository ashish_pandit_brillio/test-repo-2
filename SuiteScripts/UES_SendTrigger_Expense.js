/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Dec 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try{
			var currentContext = nlapiGetContext();
			var t = currentContext.getExecutionContext();
			//Execute the logic only when creating a sales order with the browser UI
			if(type == 'create'  && currentContext.getExecutionContext() == 'userinterface')
			{
			var user = nlapiGetUser(); // entity id of the current user
			if(user){
			
			var emp_lookUP = nlapiLookupField('employee',parseInt(user),['firstname','email']);
			var emp_name = emp_lookUP.firstname;
			var emp_email = emp_lookUP.email;
		
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Dear '+ emp_name +',</p>';
			//strVar += '<br/>';
			strVar += '<p>We are very excited to announce that the Expense Entry feature is now live on the Brillio On The Go app !';
			strVar += ' You can enter your expense within seconds, scan your bills live and submit at the end of your trip.  It is as simple as that! </p>';
			//strVar += '<br/>';
			strVar += '<p> Its available in Andriod, IOS,  url: onthego.brillio.com</p>';
	
			//strVar += '<br/>';
			//strVar += '<a href="https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&proj_id='+nlapiGetRecordId()+'>Project Setup</a>';
			
			strVar += '<p>Thanks & Regards,</p>';
			strVar += '<p>Team IS</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = user;
			//nlapiSendEmail(author, recipient, subject, body, cc, bcc, records, attachments)
			nlapiLogExecution('DEBUG','emp_email',emp_email);
			nlapiLogExecution('DEBUG','emp_user',user);
			nlapiSendEmail(442, emp_email , 'Brillio On The Go - Expense Entry', strVar,null,'deepak.srinivas@brillio.com',a_emp_attachment);
			nlapiLogExecution('audit','mail sent');
		}
	}
	
}
	catch(e){
		nlapiLogExecution('ERROR','Process Error',e);
	}
  
}
