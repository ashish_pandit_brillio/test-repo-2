// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI Invoice Or CreditMemo DiscountCal 
	Author: Swati Kurariya
	Company: Aashna CloudTech
	Date: 04-07-2014
    Description: "Customer Discount" functionality ,Validation Perform on Customer.check from and to slab value perform validation.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord_Custdesc_Desccal()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================
//=========================================================================================
function pageInit_Disable_Discount_Btn(type) //
{//fun start
	//var test_btn=document.getElementById("newrecrecmachcustrecord_fpcustparent");
	//alert(test_btn);
	if (type == 'create') //
	{
	
	}
	else //
	{
		//document.getElementById('newrec5').style.display='none';
		//document.getElementById('newrec151').style.display='none';
		//document.getElementById('newrec153').style.display='none';
	}
	
	//document.getElementById('newrecrecmachcustrecord_customer_parent').style.display = 'none';
	//document.getElementById('newrecrecmachcustrecord_tdcustparent').style.display = 'none';
	
}//fun close
//=========================================================================================
function fieldChanged_Cust_Validation(type, name, linenum)
{//fun start
 
	//alert('name'+name);
	//alert('type'+type);
	////////////////////////////////////////////////////////////////////////////////////
	if(type == 'recmachcustrecord_customer_parent')
		{
			var Volumn_name='custrecord_vdslabfrom';
			var Volumn_Slab_Form='custrecord_vdslabfrom';
			var Volumn_Slab_To='custrecord_vdslabto';
			var Volumn_Rec='recmachcustrecord_customer_parent';
			var Volumn_Remittence_Method='custrecord_vdremittancemethod';
			var Volumn_Discount_Validity='custrecord_vddiscountvalidity';
			var Volumn_Base_Amount='custrecord_vdbasedamount';
			var Volumn_Discount_Rate='custrecord_vddiscountrate';
			Volumn_Desc(name,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Discount_Rate,Volumn_Base_Amount);
		}
	//==========================================================================================================
	if(type == 'recmachcustrecord_tdcustparent')
	{
		var Volumn_name='custrecord_tdslabfrom';
		var Volumn_Slab_Form='custrecord_tdslabfrom';
		var Volumn_Slab_To='custrecord_tdslabto';
		var Volumn_Rec='recmachcustrecord_tdcustparent';
		var Volumn_Remittence_Method='custrecord_tdremittancemethod';
		var Volumn_Discount_Validity='custrecord_tddiscountvalidity';
		var Volumn_Base_Amount='custrecord_tdbasedamount';
		var Volumn_Discount_Rate='custrecord_tddiscountrate';
		Volumn_Desc(name,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Discount_Rate,Volumn_Base_Amount);
	}
	//========================================================================================================
	if(type == 'recmachcustrecord_fpcustparent')
	{
		var Volumn_name='custrecord_fpslabfrom';
		var Volumn_Slab_Form='custrecord_fpslabfrom';
		var Volumn_Slab_To='custrecord_fpslabto';
		var Volumn_Rec='recmachcustrecord_fpcustparent';
		var Volumn_Remittence_Method='custrecord_fpremittancemethod';
		var Volumn_Discount_Validity='custrecord_fddiscountvalidity';
		var Volumn_Base_Amount='custrecord_fdbasedamount';
		var Volumn_Discount_Item='custrecord_fpdiscountitem';
		var Volumn_Apply_Method='custrecord_applymethod123';
		var Volumn_Discount_Rate='custrecord_fpdiscountrate';
		Fixed_Desc(name,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Discount_Item,Volumn_Base_Amount,Volumn_Apply_Method,Volumn_Discount_Rate);
	}
}//fun close

function validateLine_Cust_Validation(type)
{//fun start
	
	/*  On validate line:
      Validate Remittance method,discount validity and base amount
    - EXPLAIN THE PURPOSE OF THIS FUNCTION


    FIELDS USED:

    --Field Name--				--ID--


*/
	if(type == 'recmachcustrecord_customer_parent')
	{
		
		var Volumn_name='custrecord_vdslabfrom';
		var Volumn_Slab_Form='custrecord_vdslabfrom';
		var Volumn_Slab_To='custrecord_vdslabto';
		var Volumn_Rec='recmachcustrecord_customer_parent';
		var Volumn_Remittence_Method='custrecord_vdremittancemethod';
		var Volumn_Discount_Validity='custrecord_vddiscountvalidity';
		//var Volumn_Apply_Method='custrecord_applymethod123';
		var Volumn_Discount_Item='custrecord_vddiscountitem';
		var Volumn_Base_Amount='custrecord_vdbasedamount';
		line_level(type,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Base_Amount,Volumn_Discount_Item);

	}
	//==========================================================================================================
	if(type == 'recmachcustrecord_tdcustparent')
	{
		var Volumn_name='custrecord_tdslabfrom';
		var Volumn_Slab_Form='custrecord_tdslabfrom';
		var Volumn_Slab_To='custrecord_tdslabto';
		var Volumn_Rec='recmachcustrecord_tdcustparent';
		var Volumn_Remittence_Method='custrecord_tdremittancemethod';
		var Volumn_Discount_Validity='custrecord_tddiscountvalidity';
		//var Volumn_Apply_Method='custrecord_applymethod123';
		var Volumn_Base_Amount='custrecord_tdbasedamount';
		var Volumn_Discount_Item='custrecord_tddiscountitem';
		line_level(type,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Base_Amount,Volumn_Discount_Item);
	}
	//=========================================================================================================
	if(type == 'recmachcustrecord_fpcustparent')
	{
		var Volumn_name='custrecord_fpslabfrom';
		var Volumn_Slab_Form='custrecord_fpslabfrom';
		var Volumn_Slab_To='custrecord_fpslabto';
		var Volumn_Rec='recmachcustrecord_fpcustparent';
		var Volumn_Remittence_Method='custrecord_fpremittancemethod';
		var Volumn_Discount_Validity='custrecord_fddiscountvalidity';
		var Volumn_Base_Amount='custrecord_fdbasedamount';
		var Volumn_Discount_Item='custrecord_fpdiscountitem';
		var Volumn_Apply_Method='custrecord_applymethod123';
		var Volumn_Discount_Rate='custrecord_fpdiscountrate';
		line_level(type,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Base_Amount,Volumn_Discount_Item,Volumn_Apply_Method);
	}
	//  LOCAL VARIABLES
	
	return true;
}//fun close
//////////////////////////////////////////////////////////////////////////////
function line_level(type,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Base_Amount,Volumn_Discount_Item,Volumn_Apply_Method)
{
	var i_Volumn_Count=nlapiGetCurrentLineItemIndex(Volumn_Rec);
	//---------------------------first validation------------------------------------------------------
	if(i_Volumn_Count == 1)
		{
			//var Volumne_Remiitance=nlapiSetLineItemValue('recmachcustrecord_vdcustparent', 'custrecord_vdremittancemethod',1);
			//var Volumne_Discount_Validity=nlapiSetLineItemValue('recmachcustrecord_vdcustparent', 'custrecord_vddiscountvalidity',1);
			//var Volumne_Base_Amount=nlapiSetLineItemValue('recmachcustrecord_vdcustparent', 'custrecord_vdbasedamount',1);
			
			nlapiDisableLineItemField(Volumn_Rec, Volumn_Remittence_Method,false);
			nlapiDisableLineItemField(Volumn_Rec, Volumn_Discount_Validity,false);
			nlapiDisableLineItemField(Volumn_Rec,Volumn_Base_Amount,false);
			
			 if(type == 'recmachcustrecord_fpcustparent')
				{
				 	nlapiDisableLineItemField(Volumn_Rec,Volumn_Apply_Method,false);
				 	//nlapiDisableLineItemField(Volumn_Rec,Volumn_Discount_Item,false);
				}
		}
	else{
			var Volumne_Remiitance=nlapiGetLineItemValue(Volumn_Rec, Volumn_Remittence_Method,1);
			//alert('Volumne_Remiitance'+Volumne_Remiitance);
			if(Volumne_Remiitance == null)
				{
				Volumne_Remiitance='';
				}
			var Discount_Validity=nlapiGetLineItemValue(Volumn_Rec, Volumn_Discount_Validity,1);
			//alert('Discount_Validity'+Discount_Validity);
			if(Discount_Validity == null)
			{
				Discount_Validity='';
			}
			
			var Base_Amount=nlapiGetLineItemValue(Volumn_Rec, Volumn_Base_Amount,1);
			//alert('Volumne_Base_Amount'+Volumne_Base_Amount);
			if(Base_Amount == null)
			{
				Base_Amount='';
			}
			
			
			
			var Apply_Method=nlapiGetLineItemValue(Volumn_Rec, Volumn_Apply_Method,1);
			//alert('Volumn_Apply_Method'+Volumn_Apply_Method);
			if(Apply_Method == null)
			{
				Apply_Method='';
			}
			
			nlapiDisableLineItemField(Volumn_Rec, Volumn_Remittence_Method,true);
			nlapiDisableLineItemField(Volumn_Rec, Volumn_Discount_Validity,true);
			nlapiDisableLineItemField(Volumn_Rec, Volumn_Base_Amount,true);
			
			
			
		//	nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Remittence_Method, Volumne_Remiitance);
		    //nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Validity, Discount_Validity);
		 //   nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Base_Amount, Base_Amount);
			
			
		   /* if(type == 'recmachcustrecord_fpcustparent')
			{
		    	var Discount_Item=nlapiGetLineItemValue(Volumn_Rec, Volumn_Discount_Item,1);
				//alert('Discount_Item'+Discount_Item);
				if(Discount_Item == null)
				{
					Discount_Item='';
				}
				
		    	nlapiDisableLineItemField(Volumn_Rec, Volumn_Apply_Method,true);
		    	nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Apply_Method, Apply_Method);
		    	//nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Item, Discount_Item);
			}*/
		}
	
}
///////////////////////////////////////////////////////////////////////////////
function Volumn_Desc(name,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Discount_Rate,Volumn_Base_Amount)
{//fun start
	//alert('working');
	//alert('Volumn_Discount_Rate'+Volumn_Discount_Rate);
	//alert('Volumn_Slab_Form'+Volumn_Slab_Form);
	//alert('Volumn_Rec'+Volumn_Rec);
	
	var b_first_counter=false;
	var b_second_counter=false;
	var b_third_counter=false;
	
	if(name == Volumn_Slab_Form)//'custrecord_vdslabfrom'
	{
		
		var i_Volumn_Count=nlapiGetCurrentLineItemIndex(Volumn_Rec);//'recmachcustrecord_vdcustparent'
		//alert('i_Volumn_Count'+i_Volumn_Count);
		//---------------------------first validation------------------------------------------------------
		if(i_Volumn_Count == 1)
			{
				
			}
		else{
			//-----------------------------------------------------------------------------------------
			var Volumne_Remiitance=nlapiGetLineItemValue(Volumn_Rec, Volumn_Remittence_Method,1);
			//alert('Volumne_Remiitance'+Volumne_Remiitance);
			if(Volumne_Remiitance == null)
				{
				Volumne_Remiitance='';
				}
			var Discount_Validity=nlapiGetLineItemValue(Volumn_Rec, Volumn_Discount_Validity,1);
			//alert('Discount_Validity'+Discount_Validity);
			if(Discount_Validity == null)
			{
				Discount_Validity='';
			}
			
			var Base_Amount=nlapiGetLineItemValue(Volumn_Rec, Volumn_Base_Amount,1);
			//alert('Volumne_Base_Amount'+Volumne_Base_Amount);
			if(Base_Amount == null)
			{
				Base_Amount='';
			}
			
			
			//-----------------------------------------------------------------------------------------
			nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Remittence_Method, Volumne_Remiitance);
		    nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Validity, Discount_Validity);
		    nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Base_Amount, Base_Amount);
		    
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Remittence_Method,true);
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Discount_Validity,true);
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Base_Amount,true);
			 //nlapiSetCurrentLineItemValue('recmachcustrecord_vdcustparent', 'custrecord_vdremittancemethod', Volumne_Remiitance);
			}
		//---------------------------second validation----------------------------------------------------------
		var i_Previous_Slab_From=parseFloat(nlapiGetLineItemValue(Volumn_Rec,Volumn_Slab_Form,i_Volumn_Count-1));
		//alert('i_Previous_Slab_From'+i_Previous_Slab_From);
				
		var i_Previous_Slab_To=parseFloat(nlapiGetLineItemValue(Volumn_Rec,Volumn_Slab_To,i_Volumn_Count-1));
		//alert('i_Previous_Slab_To'+i_Previous_Slab_To);
		//------------------------------------------------------------------------------------
		var i_Slab_From_Val=parseFloat(nlapiGetCurrentLineItemValue(Volumn_Rec,Volumn_Slab_Form));
		//alert('i_Slab_From_Val'+i_Slab_From_Val);
			
		var i_Slab_To_Val=parseFloat(nlapiGetCurrentLineItemValue(Volumn_Rec,Volumn_Slab_To));
		//alert('i_Slab_To_Val'+i_Slab_To_Val);
		///-------------------------------------------------------------------------------
		if(i_Slab_From_Val <0 || i_Slab_To_Val<0)
			{
				alert('Value Cant Not Be negative ');
				b_third_counter=false;
			}
		else
			{
				b_third_counter=true;
			}
		//---------------------------------------------------------------------------------
			if(isNaN(i_Slab_To_Val))
				{
				b_first_counter=true;
				}
			else
				{
					if(i_Slab_To_Val > i_Slab_From_Val)
						{
							b_first_counter=true;
						}
					else{
							alert('Slab From Value Can Not Be Greater Than Slab To Value');
							//return false;
							b_first_counter=false;
						}
				}
				//----------------------------------------------------------------------------------
				if((i_Slab_From_Val >= i_Previous_Slab_From) && (i_Slab_From_Val<=i_Previous_Slab_To))
				{
					alert('Slab From Value Can Not Between Above Slab');
					//return false;
					b_second_counter=false;
					
				}
			else{
				b_second_counter=true;
				}
				
				if(b_first_counter==true && b_second_counter == true && b_third_counter == true)
					{
					//return true;
					}
				else
					{
					//return false;
					nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Slab_Form,'',false,false);
					}
	}
	if(name == Volumn_Slab_To)
	{
		var i_Volumn_Count=nlapiGetCurrentLineItemIndex(Volumn_Rec);
		//alert('i_Volumn_Count'+i_Volumn_Count);
			//-------------------------------------------------------------------------------------
		var i_Previous_Slab_From=parseFloat(nlapiGetLineItemValue(Volumn_Rec,Volumn_Slab_Form,i_Volumn_Count-1));
		//alert('i_Previous_Slab_From'+i_Previous_Slab_From);
			
		var i_Previous_Slab_To=parseFloat(nlapiGetLineItemValue(Volumn_Rec,Volumn_Slab_To,i_Volumn_Count-1));
		//alert('i_Previous_Slab_To'+i_Previous_Slab_To);
			//------------------------------------------------------------------------------------
		var i_Slab_From_Val=parseFloat(nlapiGetCurrentLineItemValue(Volumn_Rec,Volumn_Slab_Form));
		//alert('i_Slab_From_Val'+i_Slab_From_Val);
			
		var i_Slab_To_Val=parseFloat(nlapiGetCurrentLineItemValue(Volumn_Rec,Volumn_Slab_To));
		//alert('i_Slab_To_Val'+i_Slab_To_Val);//Volumn_Discount_Rate
			//-------------------------------------------------------------------------------
			
		if(i_Slab_To_Val > i_Slab_From_Val)
			{
				b_first_counter=true;
			}
		else{
				alert('Slab From Value Can Not Be Greater Than Slab To Value');
				//return false;
				b_first_counter=false;
			}
			//----------------------------------------------------------------------------------
			if((i_Slab_To_Val >= i_Previous_Slab_From) && (i_Slab_To_Val<=i_Previous_Slab_To))
			{
				alert('Slab From Value Can Not Between Above Slab');
				//return false;
				b_second_counter=false;
				
			}
		else{
				b_second_counter=true;
			}
			
		if(b_first_counter==true && b_second_counter == true)
			{
				//return true;
			}
		else
			{
				//return false;
				nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Slab_To,'',false,false);
			}
	}
	//alert('name'+name);
	//alert('Volumn_Remittence_Method'+Volumn_Remittence_Method);
	if(name == Volumn_Remittence_Method)
		{
			//alert('Volumn_Remittence_Method'+Volumn_Remittence_Method);
			
			var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
			var Remittence_Method=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Remittence_Method);
			//alert('Remittence_Method'+Remittence_Method);
			for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
				{
				nlapiSetLineItemValue(Volumn_Rec, Volumn_Remittence_Method, ks, Remittence_Method,false,false);
				}
		
		}
	if(name == Volumn_Discount_Rate)
		{
		    var Discount_Rate=parseFloat(nlapiGetCurrentLineItemValue(Volumn_Rec,Volumn_Discount_Rate));
		    if(Discount_Rate <=0)
		    	{
		    		alert('Discount Rate Cant Not Be Negative Or Zero');
		    		nlapiSetCurrentLineItemValue(Volumn_Rec,Volumn_Discount_Rate,'',false,false);
		    	}
		
		}
	
	if(name == Volumn_Discount_Validity)
	{
		var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
		var Discount_Validity=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Validity);
		for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
			{
			nlapiSetLineItemValue(Volumn_Rec, Volumn_Discount_Validity, ks, Discount_Validity,false,false);
			}
	
	}
	
	if(name == Volumn_Base_Amount)
	{
		var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
		var Base_Amount=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Base_Amount);
		for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
			{
			nlapiSetLineItemValue(Volumn_Rec, Volumn_Base_Amount, ks, Base_Amount,false,false);
			}
	
	}
}//fun close
////////////////////////////////////////////////////////////////////////////////////////////////
function Fixed_Desc(name,Volumn_name,Volumn_Slab_Form,Volumn_Slab_To,Volumn_Rec,Volumn_Remittence_Method,Volumn_Discount_Validity,Volumn_Discount_Item,Volumn_Base_Amount,Volumn_Apply_Method,Volumn_Discount_Rate)
{//fun start
	//alert('working');
	//alert('name'+name);
	var b_first_counter=false;
	var b_second_counter=false;
	
//	if(name == Volumn_Slab_Form)//'custrecord_vdslabfrom'
	//{
		var i_Volumn_Count=nlapiGetCurrentLineItemIndex(Volumn_Rec);//'recmachcustrecord_vdcustparent'
		//alert('i_Volumn_Count'+i_Volumn_Count);
		//---------------------------first validation------------------------------------------------------
		if(i_Volumn_Count == 1)
			{
				
			}
		else{
			
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Remittence_Method,true);
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Discount_Validity,true);
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Base_Amount,true);
			// nlapiDisableLineItemField(Volumn_Rec,Volumn_Discount_Item,true);
			 nlapiDisableLineItemField(Volumn_Rec,Volumn_Apply_Method,true);
			// nlapiDisableLineItemField(Volumn_Rec,Volumn_Apply_Method,true);
			 //nlapiSetCurrentLineItemValue('recmachcustrecord_vdcustparent', 'custrecord_vdremittancemethod', Volumne_Remiitance);
			}
		
//	}
	
	if(name == Volumn_Remittence_Method)
		{
			var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
			var Remittence_Method=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Remittence_Method);
			for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
				{
				nlapiSetLineItemValue(Volumn_Rec, Volumn_Remittence_Method, ks, Remittence_Method,false,false);
				}
		
		}
	
	if(name == Volumn_Discount_Validity)
	{
		var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
		var Discount_Validity=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Validity);
		for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
			{
			nlapiSetLineItemValue(Volumn_Rec, Volumn_Discount_Validity, ks, Discount_Validity,false,false);
			}
	
	}
	
	if(name == Volumn_Base_Amount)
	{
		var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
		var Base_Amount=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Base_Amount);
		for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
			{
			nlapiSetLineItemValue(Volumn_Rec, Volumn_Base_Amount, ks, Base_Amount,false,false);
			}
	
	}
	if(name == Volumn_Discount_Item)
	{
		
		
		var i_Total_Count_Of_Line=parseFloat(nlapiGetLineItemCount(Volumn_Rec));
		//alert('i_Total_Count_Of_Line'+i_Total_Count_Of_Line);
		/////////////////////////////////////////////////////////////////////////////
		if(i_Total_Count_Of_Line == '0')
		{
			
		}
	else{
				//------------------------------------------------------------------------------------------
				var Volumne_Remiitance=nlapiGetLineItemValue(Volumn_Rec, Volumn_Remittence_Method,1);
				//alert('Volumne_Remiitance'+Volumne_Remiitance);
				if(Volumne_Remiitance == null)
					{
					Volumne_Remiitance='';
					}
				var Discount_Validity=nlapiGetLineItemValue(Volumn_Rec, Volumn_Discount_Validity,1);
				//alert('Discount_Validity'+Discount_Validity);
				if(Discount_Validity == null)
				{
					Discount_Validity='';
				}
				
				var Base_Amount=nlapiGetLineItemValue(Volumn_Rec, Volumn_Base_Amount,1);
				//alert('Volumne_Base_Amount'+Volumne_Base_Amount);
				if(Base_Amount == null)
				{
					Base_Amount='';
				}
				
				
				
				var Apply_Method=nlapiGetLineItemValue(Volumn_Rec, Volumn_Apply_Method,1);
				//alert('Volumn_Apply_Method'+Volumn_Apply_Method);
				if(Apply_Method == null)
				{
					Apply_Method='';
				}
				//------------------------------------------------------------------------------------------
				    nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Apply_Method, Apply_Method,false,false);
					nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Remittence_Method, Volumne_Remiitance,false,false);
					nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Validity, Discount_Validity,false,false);
			        nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Base_Amount, Base_Amount,false,false);
					
		
		}
		/////////////////////////////////////////////////////////////////////////////
		
		//var index=parseFloat(nlapiGetCurrentLineItemIndex(Volumn_Rec));
		//var Discount_Item=nlapiGetLineItemValue(Volumn_Rec, Volumn_Discount_Item,i_Total_Count_Of_Line-1);
		var Current_Discount_Item=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Item);
		//alert('Current_Discount_Item'+Current_Discount_Item);
		for(var ks=1 ; ks<=i_Total_Count_Of_Line ; ks++)
		{
			
			var Discount_Item=nlapiGetLineItemValue(Volumn_Rec, Volumn_Discount_Item,ks);
			//alert('Discount_Item'+Discount_Item);
			
			if(Current_Discount_Item == Discount_Item)
				{
			
					alert('This Item has been already selected');
					nlapiSetCurrentLineItemValue(Volumn_Rec, Volumn_Discount_Item,'',false,false);
				}
		}	
		
	}
	if(name == Volumn_Apply_Method)
	{
		var i_Total_Count_Of_Line=nlapiGetLineItemCount(Volumn_Rec);
		var Apply_Method=nlapiGetCurrentLineItemValue(Volumn_Rec, Volumn_Apply_Method);
		for(var ks=2 ; ks<=i_Total_Count_Of_Line ; ks++)
			{
			nlapiSetLineItemValue(Volumn_Rec,Volumn_Apply_Method, ks, Apply_Method,false,false);
			}	
	}
	if(name == Volumn_Discount_Rate)
	{
	    var Discount_Rate=parseFloat(nlapiGetCurrentLineItemValue(Volumn_Rec,Volumn_Discount_Rate));
	    if(Discount_Rate <=0)
	    	{
	    		alert('Discount Rate Cant Not Be Negative Or Zero');
	    		nlapiSetCurrentLineItemValue(Volumn_Rec,Volumn_Discount_Rate,'',false,false);
	    	}
	
	}
}//fun close