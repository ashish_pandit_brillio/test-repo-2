function DIDEMailTrigger(s_proj_name,pm_email,dm_email,i_pro_id,type_value,res_projName){
  
  nlapiLogExecution('DEBUG','Inside triger',s_proj_name+', '+pm_email+', '+dm_email)
  
  if(type_value=='job'){
    
    var strVar = '';
					strVar += '<html>';
					strVar += '<body>';
					
					strVar += '<p>Hi DI DE Team,</p>';
					strVar += '<p>This is to notify that a new project '+ s_proj_name +' with project ID '+ i_pro_id +' has been created with PM as '+ pm_email +' and DM as '+ dm_email +'.</p>';
					strVar += '<p>Please contact PM/DM and initiate downstream activities like CODEX/CLIP integrations, Teams folder structure creation, Green onboarding initiation etc as needed</p>';
					
					strVar += '<p>Regards,</p>';
					strVar += '<p>Information Systems</p>';
						
					strVar += '</body>';
					strVar += '</html>';
					
					//var a_emp_attachment = new Array();
					//a_emp_attachment['entity'] = '39108';
					//var a_group_mail_id = '';
					
					var a_RECIPIENT = 'brilliodide@brillio.com'
					nlapiSendEmail(442, a_RECIPIENT, 'ALERT!!! Brillio Digital Infrastructure - New project creation '+i_pro_id +' ' + s_proj_name +'<Action required by DE team>', strVar);
					nlapiLogExecution('debug','mail sent for project creation');
    
    
  }
  
  if(type_value=='resourceallocation'){
    
    var strVar = '';
	strVar += '<html>';
	strVar += '<body>';

	strVar += '<p>Hi DI DE Team,</p>';
	strVar += '<p>This is to notify that a new DI resource ' + s_proj_name + ' has been allocated to a non DI project -'+ i_pro_id +' ' + res_projName + '  with PM as ' + pm_email + ' and DM as ' + dm_email + '.</p>';
	strVar += '<p>Please contact PM/DM and initiate downstream activities like CODEX/CLIP integrations, Teams folder structure creation, Green onboarding initiation etc as needed</p>';

	strVar += '<p>Regards,</p>';
	strVar += '<p>Information Systems</p>';

	strVar += '</body>';
	strVar += '</html>';


	var a_RECIPIENT = 'brilliodide@brillio.com'
	nlapiSendEmail(442, a_RECIPIENT, 'ALERT!!! Brillio Digital Infrastructure – New DI resource allocation to non DI project- '+ i_pro_id +' ' + res_projName +' <Action required by DE team>', strVar);
	nlapiLogExecution('debug', 'mail sent for resource creation');
    
    
  }
	
	
	
}
