/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Account_Data(obj_Request) {
         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var obj_Response_Return = {};
             if (s_Request_Data == 'ACCOUNT') {
                 obj_Response_Return = get_Account_Data(obj_Search);
             }
         } 
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Account_Data


     return {
         post: Send_Account_Data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Account_Data(obj_Search) {
 try {
     log.debug({title: 'get_Account_Data'})
     var accountSearchObj = obj_Search.create({
        type: "account",
        filters:
        [
        ],
        columns:
        [
           obj_Search.createColumn({
              name: "name",
              sort: obj_Search.Sort.ASC,
              label: "Name"
           }),
           obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
		   obj_Search.createColumn({name: "type", label: "Account Type"})
        ]
     });
     var searchResultCount = accountSearchObj.runPaged().count;
     log.debug("accountSearchObj result count",searchResultCount);
     var i_Search_Count_Employee = accountSearchObj.runPaged().count;
     log.debug("i_Search_Count_Employee==", i_Search_Count_Employee);
     var myResults = getAllResults(accountSearchObj);
     var arr_Emp_json = [];
     myResults.forEach(function (result) 
	 {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
             'Name': result.getValue({ name: "name", sort: obj_Search.Sort.ASC }),
			 'Account_type' : result.getText({name: "type", label: "Account Type"})
         };
         arr_Emp_json.push(obj_json_Container);
         return true;
     }); //// myResults.forEach(function (result) 
     return arr_Emp_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('get_Account_Data s_Exception== ', s_Exception);
 } //// End of catch 
} ///// End of function get_Account_Data()

/////// Supporting Functions ////////
function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}/////// Supporting Functions ////////