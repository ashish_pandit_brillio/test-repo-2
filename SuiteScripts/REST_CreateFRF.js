// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name : REST_CreateFRF.js
    	Author      : ASHISH PANDIT
    	Date        : 04 April 2018
        Description : RESTlet to get data for Create FRF form 


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
     


    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         SCHEDULED FUNCTION
    		- scheduledFunction(type)


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

/*Suitlet Main function*/
function REST_createFRF(dataIn) {
    try {
        var values = {};
        var useremail = dataIn.user;
        var user = getUserUsingEmailId(useremail);
        var admin = GetAdmin(user);
        var region = GetRegion(user);
        nlapiLogExecution('debug', 'Admin ', admin);
        var practice = GetPracticeSPOC(user);
        var a_subPractices = getSubPractices(practice);
        var deliveryAnchore = GetDeliveryAnchore(user);
        var i_resourceId = dataIn.empName;
        var i_accSelected = dataIn.account;
        var i_project = dataIn.project;
        var s_reqType = dataIn.request;
        var i_opportunity = dataIn.opportunity;
        var i_account;
        var s_projectName;
        var s_accountDisplay;
        var tileData;
        //values.rolefilter = getFilteredRoleNew(user);
        values.rolefilter = getFilteredRole(user, a_subPractices, admin, region, deliveryAnchore);
        //values.account_project = getAccountProjectList(user,a_subPractices);
        //values.project = getProjectList(user,a_subPractices,admin);
        values.project = getProjectListNew(user, a_subPractices, admin, deliveryAnchore);
		
		values.nonDeliveryProject = getNonDeliveryProjectList(user, a_subPractices, admin, deliveryAnchore);
		
        values.opportunity = getOpportunityList(user, a_subPractices, admin, deliveryAnchore);
        values.account = getAccountList(user, a_subPractices, admin, deliveryAnchore);
        values.frflist = getFrfList(user, a_subPractices, admin, region, deliveryAnchore);


        /*Get resource details*/
        var searchResult;
        if (i_resourceId) {
            //searchResult = getResourceDetails(i_resourceId,i_project);
            //values.rotation = searchResult;
            //nlapiLogExecution('Debug','JSON.stringify(a_dataArray) ',JSON.stringify(searchResult));
            values.accountSelected = i_accSelected;
            values.projectSelected = i_project;
            values.type = s_reqType;
            values.resourceSelected = i_resourceId;
        } else if (i_project) /*Get Project / Opportunity Name*/ {
            i_account = nlapiLookupField('job', i_project, 'customer');
            s_projectName = nlapiLookupField('job', i_project, 'companyname');
            //d_projectStartDate = nlapiLookupField('job',i_project,'startdate');
            //d_projectEndDate = nlapiLookupField('job',i_project,'enddate');
            tileData = getTileData(i_account, i_project, i_opportunity);
            //values.project_role_options = getProjectRoleValues(null,select,'Select Project Role',null,i_project);
            nlapiLogExecution('debug', 'tileData ', tileData);
            //values.projectStartDate = d_projectStartDate;
            //values.projectEndDate = d_projectEndDate;
            values.data = tileData;
        } else if (i_opportunity) {
            nlapiLogExecution('Debug', 'i_opportunity ', i_opportunity);
            //d_oppStartDate = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opportunity,'custrecord_start_date_sfdc');
            //d_oppEndDate = nlapiLookupField('customrecord_sfdc_opportunity_record',i_opportunity,'custrecord_end_date_sfdc');
            tileData = getTileData(i_account, null, i_opportunity);
            //values.projectStartDate = d_oppStartDate;
            //values.projectEndDate = d_oppEndDate;
            values.data = tileData;
        }


        var select = "";

        //values.role_list = getRole();
        //values.emp_level_list = getEmpLevelValues('customsearch2490',select,'Select Employee Level');
        values.employee_search = getSearchValues('customsearch_employee_search', select, 'Select Employee');
        values.skill = getSkills();
        values.sla = getFuelSkills();
        values.practice_list = GetPracticeList();
        values.positiontype = getListValuesPositionType('customlist_employeetypenew', select, 'Select Position Type');
        values.request_type = getListValues('customlist_fulfillment_frf_types', select, 'Select Request Type');

        values.fitment_type = getListValues('customlist_fitment_type', select, 'Select Fitment Type'); // prabhat gupta 09/09/2020 NIS-1723

     //   values.level_list = getEmployeeLevel();//getListValues('customlist_level', select, 'Select Level'); // prabhat gupta 09/09/2020 NIS-1755

        //values.location_list = getListValues('location', select, 'Select Location');
        values.location_list = getLocationList();
        values.employee_status = getListValues('customlist_fuel_employee_status', select, 'Select Employee Status');
        values.required_edu = getListValues('customlist_fuel_req_edu_level', select, 'Select Req. Education Level');
        values.education_program = getListValues('customlist_fuel_education_program', select, 'Select Education Program');
        var userName = '';
        if (user)
            userName = nlapiLookupField('employee', user, 'entityid');
        //values.createdby = userName;
        values.createdby = user;
        values.softlocked_resources = getSoftLockedEmployees();
        return values;
    } catch (error) {
        nlapiLogExecution('Debug', 'error ', error);
		throw error;
    }
}

function getLocationList() {
    var customrecord_taleo_location_masterSearch = nlapiSearchRecord("customrecord_taleo_location_master", null,
        [
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("name").setSort(false)
        ]
    );
    if (customrecord_taleo_location_masterSearch) {
        var dataArray = new Array();
        for (var i = 0; i < customrecord_taleo_location_masterSearch.length; i++) {
            var data = {};
            data.location = {
                "name": customrecord_taleo_location_masterSearch[i].getValue('name'),
                "id": customrecord_taleo_location_masterSearch[i].getId()
            };
            dataArray.push(data);
        }
        return dataArray;
    }
}


/**************************************************************/
function getOpportunityList(user, practice, isAdmin, deliveryAnchore) {
    var filters = new Array();
    var columns = new Array();
    if (isAdmin) {
        filters = [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfillment_dash_excluded", "is", "F"]
        ]
    } else if (practice) {
        filters = ["custrecord_fulfill_dashboard_practice", "anyof", practice],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfillment_dash_excluded", "is", "F"]
    } else if (deliveryAnchore.length > 0) {
        filters = ["custrecord_fulfill_dashboard_account", "anyof", deliveryAnchore],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfillment_dash_excluded", "is", "F"]
    } else if (user) {
        filters = [
            ["custrecord_fulfill_dashboard_project.custentity_deliverymanager", "anyof", user],
            "OR",
            [
                ["custrecord_fulfill_dashboard_project.custentity_projectmanager", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_project.custentity_clientpartner", "anyof", user]
            ],
            "OR",
            [
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner", "anyof", user]
            ], //Need to check for filters
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfillment_dash_excluded", "is", "F"]
        ]
    }

    columns = [
        new nlobjSearchColumn("internalid", "CUSTRECORD_FULFILL_DASHBOARD_PROJECT", null),
        new nlobjSearchColumn("altname", "CUSTRECORD_FULFILL_DASHBOARD_PROJECT", null),
        new nlobjSearchColumn("internalid", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
        new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
        new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null),
        new nlobjSearchColumn("customer", "custrecord_fulfill_dashboard_project", null),
        new nlobjSearchColumn("custrecord_start_date_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
        new nlobjSearchColumn("custrecord_end_date_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
        new nlobjSearchColumn("custrecord_sfdc_opp_rec_rev_startdate", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
        new nlobjSearchColumn("custrecord_sfdc_opp_rec_rev_enddate", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null)
    ]


    var customrecord_fulfillment_dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data", null, filters, columns);
    if (customrecord_fulfillment_dashboard_dataSearch) {
        nlapiLogExecution('Debug', 'customrecord_fulfillment_dashboard_dataSearch.length  ', customrecord_fulfillment_dashboard_dataSearch.length);
        var dataArray = new Array();
        for (var i = 0; i < customrecord_fulfillment_dashboard_dataSearch.length; i++) {
            var data = {};
            var i_opp = customrecord_fulfillment_dashboard_dataSearch[i].getValue("internalid", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
            var projectStartDate = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_start_date_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
            var projectEndDate = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_end_date_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
            var projectRevisedStartDate = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_sfdc_opp_rec_rev_startdate", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
            var projectRevisedEndDate = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_sfdc_opp_rec_rev_enddate", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);

            var projectStartDate = '';
            var projectEndDate = '';

            if (projectRevisedStartDate)
                projectStartDate = projectRevisedStartDate;
            else
                projectStartDate = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_start_date_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);
            if (projectRevisedEndDate)
                projectEndDate = projectRevisedEndDate;
            else
                projectEndDate = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_end_date_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null);


            if (i_opp) {
                var account = customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null);
            }

            //var i_proj = customrecord_fulfillment_dashboard_dataSearch[i].getValue("internalid","CUSTRECORD_FULFILL_DASHBOARD_PROJECT",null);
            if (i_opp) {
                data.data = {
                    "name": customrecord_fulfillment_dashboard_dataSearch[i].getValue("custrecord_opportunity_name_sfdc", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
                    "id": customrecord_fulfillment_dashboard_dataSearch[i].getValue("internalid", "CUSTRECORD_FULFILL_DASHBOARD_OPP_ID", null),
                    "type": "opportunity",
                    "account_id": account,
                    "projectStartDate": projectStartDate,
                    "projectEndDate": projectEndDate
                }
                dataArray.push(data);
            }
        }
        return dataArray;
    }
}


/***************Get Practice And Account**********************/
function GetPracticeList() {
    var departmentSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"],
           "AND",
        /*    ["custrecord_is_delivery_practice", "is", "T"],
            "AND",
			*/
            ["custrecord_parent_practice", "noneof", "@NONE@"]
        ],
        [
            new nlobjSearchColumn("custrecord_parent_practice", null, "GROUP")
        ]
    );
    if (departmentSearch) {
        var dataArray = new Array();
        for (var i = 0; i < departmentSearch.length; i++) {
            var data = {};
            data.practice = {
                "name": departmentSearch[i].getText("custrecord_parent_practice", null, "GROUP"),
                "id": departmentSearch[i].getValue("custrecord_parent_practice", null, "GROUP")
            };
            dataArray.push(data);
        }
        return dataArray;
    }
}

/***********Get Skills Function**************/
function getSkills() {
    var customrecord_employee_skillsSearch = nlapiSearchRecord("customrecord_skills", null,
        [
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("name").setSort(false)
        ]
    );
    if (customrecord_employee_skillsSearch) {
        var dataArray = new Array();
        for (var i = 0; i < customrecord_employee_skillsSearch.length; i++) {
            var data = {};
            data.skill = {
                "name": customrecord_employee_skillsSearch[i].getValue('name'),
                "id": customrecord_employee_skillsSearch[i].getId()
            };
            dataArray.push(data);
        }
        return dataArray;
    }
}

function getFuelSkills() {
    var customrecord_fuel_skill_ageSearch = nlapiSearchRecord("customrecord_fuel_skill_age", null,
        [
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_skillage_skill", null, "GROUP"),
            new nlobjSearchColumn("custrecord_fuel_frfage_hire_count", null, "MAX"),
            new nlobjSearchColumn("custrecord_fuel_frfage_location", null, "GROUP")
        ]
    );
    if (customrecord_fuel_skill_ageSearch) {
        var data = [];
        var dataArray = new Array();
        for (var i = 0; i < customrecord_fuel_skill_ageSearch.length; i++) {
            data.push({
                "skill": customrecord_fuel_skill_ageSearch[i].getText("custrecord_fuel_skillage_skill", null, "GROUP"),
                "skillid": customrecord_fuel_skill_ageSearch[i].getValue("custrecord_fuel_skillage_skill", null, "GROUP"),
                "headcount": customrecord_fuel_skill_ageSearch[i].getValue("custrecord_fuel_frfage_hire_count", null, "MAX"),
                "location": customrecord_fuel_skill_ageSearch[i].getText("custrecord_fuel_frfage_location", null, "GROUP")
            });
        }
        return data;
    }
}
/***********Filtered Role*************/
function getFilteredRole(user, practice, admin, region, deliveryAnchore) {
    var filter = new Array();
    var column = [new nlobjSearchColumn("custrecord_frf_details_role", null, "GROUP")];
    //nlapiLogExecution('Debug','In Function ');
    if (admin) {
        filter = ["custrecord_frf_details_status", "is", "F"];
    } else if (practice) {
        filter = [
            ["custrecord_frf_details_project.custentity_practice", "anyof", practice],
            "OR",
            ["custrecord_frf_details_opp_id.custrecord_practice_internal_id_sfdc", "anyof", practice],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];
    } else if (region) {
        filter = [
            ["custrecord_frf_details_account.custentity_region", "anyof", region],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];
    } else if (deliveryAnchore.length > 0) {
        filter = [
            ["custrecord_frf_details_account", "anyof", deliveryAnchore],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];
    } else if (user) {
        filter = [
            ["custrecord_frf_details_project.custentity_deliverymanager", "anyof", user],
            "OR",
            ["custrecord_frf_details_project.custentity_projectmanager", "anyof", user],
            "OR",
            ["custrecord_frf_details_project.custentity_clientpartner", "anyof", user],
            "OR",
            ["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
            "OR",
            ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
            "OR",
            ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner", "anyof", user],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ]
    }
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null, filter, column);
    var dataArray = new Array();
    if (customrecord_frf_detailsSearch) {
        for (var i = 1; i < customrecord_frf_detailsSearch.length; i++) {
            var data = {};
            data.filteredrole = {
                "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_role", null, "GROUP"),
                "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_role", null, "GROUP")
            };
            dataArray.push(data);
        }
    }
    return dataArray;
}

/***********Filtered Role*************/
function getFilteredRoleNew(user) {
    nlapiLogExecution('debug', 'user&&&&&**** ', user)
    var i_practice = nlapiLookupField('employee', user, 'department');
    var i_parent_practice = nlapiLookupField('department', i_practice, 'custrecord_parent_practice');
    nlapiLogExecution('debug', 'i_parent_practice&&&&&**** ' + i_parent_practice, 'i_practice 7&& ' + i_practice);
    if (i_parent_practice) {
        var customrecord_fuel_role_mapping_skillSearch = nlapiSearchRecord("customrecord_fuel_role_mapping_skill", null,
            [
                ["custrecord_fuel_role_mapping_practice", "anyof", i_parent_practice]
            ],
            [
                new nlobjSearchColumn("custrecord_fuel_role_mapping_name")
            ]
        );
        if (customrecord_fuel_role_mapping_skillSearch) {
            var dataArray = new Array();
            for (var i = 1; i < customrecord_fuel_role_mapping_skillSearch.length; i++) {
                var data = {};
                data.filteredrole = {
                    "name": customrecord_fuel_role_mapping_skillSearch[i].getId(),
                    "id": customrecord_fuel_role_mapping_skillSearch[i].getValue("custrecord_fuel_role_mapping_name")
                };
                dataArray.push(data);
            }
            return dataArray;
        }
    }

}

/****Function to get FRF List*****/
function getFrfList(user, practice, admin, region, deliveryAnchore) {
    var filter = new Array();
    var column = new Array();
    if (admin) {
        //filter = ["custrecord_frf_details_status","is","F"];
        filter.push(new nlobjSearchFilter('custrecord_frf_details_status', null, 'is', "F")); // Added by nihal
        column.push(new nlobjSearchColumn('custrecord_frf_details_frf_number'));
        //	column.push(new nlobjSearchColumn('custrecord_frf_desired_start_date'));
        column.push(new nlobjSearchColumn('internalid').setSort());
    } else if (practice) {
        filter = [
            ["custrecord_frf_details_project.custentity_practice", "anyof", practice],
            "OR",
            ["custrecord_frf_details_opp_id.custrecord_practice_internal_id_sfdc", "anyof", practice],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];
        column.push(new nlobjSearchColumn("custrecord_frf_details_frf_number"));
        column.push(new nlobjSearchColumn('internalid').setSort(true)); //prabhat gupta NIS-1373 11/6/2020 
    } else if (region) {
        filter = [
            ["custrecord_frf_details_account.custentity_region", "anyof", region],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];

        column.push(new nlobjSearchColumn("custrecord_frf_details_frf_number"));
        column.push(new nlobjSearchColumn('internalid').setSort(true)); //prabhat gupta NIS-1373 11/6/2020
    } else if (deliveryAnchore.length > 0) {
        filter = [
            ["custrecord_frf_details_account", "anyof", deliveryAnchore],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];
        column.push(new nlobjSearchColumn("custrecord_frf_details_frf_number"));
        column.push(new nlobjSearchColumn('internalid').setSort(true)); //prabhat gupta NIS-1373 11/6/2020
    } else if (user) {
        filter = [
            [
                ["custrecord_frf_details_project.custentity_deliverymanager", "anyof", user],
                "OR",
                ["custrecord_frf_details_project.custentity_projectmanager", "anyof", user],
                "OR",
                ["custrecord_frf_details_project.custentity_clientpartner", "anyof", user],
                "OR",
                ["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                "OR",
                ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                "OR",
                ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner", "anyof", user]
            ],
            "AND",
            ["custrecord_frf_details_created_by", "anyof", user],
            "AND",
            ["custrecord_frf_details_status", "is", "F"]
        ];
        column.push(new nlobjSearchColumn("custrecord_frf_details_frf_number"));
        column.push(new nlobjSearchColumn('internalid').setSort(true)); //prabhat gupta NIS-1373 11/6/2020
    }

    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null, filter, column);
    //customrecord_frf_detailsSearch = ResultMorethanThousand(customrecord_frf_detailsSearch,filter,column,"customrecord_frf_details");
    var completeResultSet = customrecord_frf_detailsSearch;
    if (admin) {
        while (customrecord_frf_detailsSearch.length == 1000) {
            var lastId = customrecord_frf_detailsSearch[999].getId();
            filter.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastId));
            customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null, filter, column);
            completeResultSet = completeResultSet.concat(customrecord_frf_detailsSearch);
        }
        customrecord_frf_detailsSearch = completeResultSet;
        nlapiLogExecution('debug', 'ResultMorethanThousand for the is', completeResultSet.length);
    }
    if (customrecord_frf_detailsSearch) {
        var dataArray = new Array();
        for (var i = 0; i < customrecord_frf_detailsSearch.length; i++) {
            var data = {};
            data.frfnumber = {
                "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_frf_number")
            };
            //	data.desired_start_date = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_desired_start_date");
            dataArray.push(data);
        }
        return dataArray;
    }
}

/** Function to get resource details**/

function getResourceDetails(i_resourceId, i_project) {
    nlapiLogExecution('Debug', 'i_resourceId ', i_resourceId);
    nlapiLogExecution('Debug', 'i_project ', i_project);
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["resource", "anyof", i_resourceId],
            "AND",
            ["project", "anyof", i_project]
            /*,
            	   "AND",
            	   ["enddate","onorafter","today"]*/
        ],
        [
            new nlobjSearchColumn("custevent_practice"),
            new nlobjSearchColumn("level", "employee", null),
            new nlobjSearchColumn("location", "employee", null),
            new nlobjSearchColumn("custentity_lwd", "employee", null),
            new nlobjSearchColumn("title", "employee", null),
            new nlobjSearchColumn("internalid", "employee", null),
            new nlobjSearchColumn("employeestatus", "employee", null),
            new nlobjSearchColumn("custevent3"),
            new nlobjSearchColumn("company"),
        ]
    );
    if (resourceallocationSearch) {
        nlapiLogExecution('Debug', 'resourceallocationSearch.length ', resourceallocationSearch.length);
        var i_project = resourceallocationSearch[0].getValue("company");
        var i_resource = resourceallocationSearch[0].getValue("internalid", "employee", null);
        var i_practice = resourceallocationSearch[0].getValue("custevent_practice");
        var d_lwd = resourceallocationSearch[0].getValue("custentity_lwd", "employee", null);
        var s_frf_source;
        if (d_lwd)
            s_frf_source = "Attrition";
        else
            s_frf_source = "Rotation";
        var i_frf = '1';
        //var s_skillFamily = getTaggedFamilies(i_practice);


        //var dataOut = {};
        dataOut.project = {
            "name": resourceallocationSearch[0].getText("company"),
            "id": resourceallocationSearch[0].getValue("company")
        };
        dataOut.account = {
            "name": nlapiLookupField('job', i_project, 'customer', true),
            "id": nlapiLookupField('job', i_project, 'customer')
        };
        //dataOut.resource ={"name" : resourceallocationSearch[0].getText("internalid","employee",null),"id" : resourceallocationSearch[0].getValue("internalid","employee",null)} ;
        dataOut.practice = {
            "name": resourceallocationSearch[0].getText("custevent_practice"),
            "id": resourceallocationSearch[0].getValue("custevent_practice")
        };
        dataOut.emplevel = {
            "name": resourceallocationSearch[0].getText("employeestatus", "employee", null),
            "id": resourceallocationSearch[0].getValue("employeestatus", "employee", null)
        };
        dataOut.frfsource = {
            "name": s_frf_source
        };
        dataOut.location = {
            "name": resourceallocationSearch[0].getText("location", "employee", null),
            "id": resourceallocationSearch[0].getValue("location", "employee", null)
        };
        getEmployeeSkills(i_resource);
        //dataOut.family = s_skillFamily;
        //dataOut.primaryskills =s_skills;
        //dataOut.otherskills = {"name" : searchResult[int].getText('custrecord_frf_plan_details_other_skills'),"id" : searchResult[int].getText('custrecord_frf_plan_details_other_skills')};

        a_dataArray.push(dataOut);
    }
    return dataOut;
}

/************************************Function to get Employee Skills******************************************************/
function getEmployeeSkills(emp_id) {
    try {
        //nlapiLogExecution('Debug','Skill ',emp_id);
        var a_resultArray = new Array();
        var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data", null,
            [
                ["custrecord_employee_skill_updated", "anyof", emp_id]
            ],
            [
                new nlobjSearchColumn("custrecord_employee_skill_updated"),
                new nlobjSearchColumn("custrecord_primary_updated"),
                new nlobjSearchColumn("custrecord_secondry_updated"),
                new nlobjSearchColumn("custrecord_family_selected")
            ]
        );
        if (customrecord_employee_master_skill_dataSearch) {
            dataOut.family = {
                "name": customrecord_employee_master_skill_dataSearch[0].getText("custrecord_family_selected"),
                "id": customrecord_employee_master_skill_dataSearch[0].getValue('custrecord_family_selected')
            };
            dataOut.primaryskills = {
                "name": customrecord_employee_master_skill_dataSearch[0].getText("custrecord_primary_updated"),
                "id": customrecord_employee_master_skill_dataSearch[0].getValue('custrecord_primary_updated')
            };
            dataOut.otherskills = {
                "name": customrecord_employee_master_skill_dataSearch[0].getText("custrecord_secondry_updated"),
                "id": customrecord_employee_master_skill_dataSearch[0].getValue('custrecord_secondry_updated')
            };
        } else {
            dataOut.family = {
                "name": "",
                "id": ""
            };
            dataOut.primaryskills = {
                "name": "",
                "id": ""
            };
            dataOut.otherskills = {
                "name": "",
                "id": ""
            };
        }
    } catch (error) {
        nlapiLogExecution('Debug', 'Error at getEmployeeSkills ', error);
    }

}
/******************************Function to get skill Families***********************************************/
function getTaggedFamilies(i_emp_dep) {
    try {
        var a_resultArray = new Array();
        var parent_practice = nlapiLookupField('department', i_emp_dep, 'custrecord_parent_practice');
        var practice_filter = [

            [
                ['custrecord_skill_emp_dep', 'anyOf', i_emp_dep],
                'or',
                ['custrecord_skill_emp_dep', 'anyOf', parent_practice]
            ],
            'and',
            ['isinactive', 'anyOf', 'F']
        ];

        var practice_search_results = searchRecord('customrecord_employee_skill_family_list', null, practice_filter,
            [new nlobjSearchColumn("name"),
                new nlobjSearchColumn("internalid")
            ]);
        if (practice_search_results) {
            for (var i = 0; i < practice_search_results.length; i++) {
                var dataOut2 = {};
                dataOut2 = {
                    "name": practice_search_results[i].getValue("name"),
                    "id": practice_search_results[i].getId()
                };
                a_resultArray.push(dataOut2);
            }
        }
        return a_resultArray;
    } catch (err) {
        throw err;
    }
}
/*********************************************Function to get List Values**********************************************/
function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption) {
    var columns = new Array();
    var filters = new Array();
    columns[0] = new nlobjSearchColumn('name');
    filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
    var a_search_results = searchRecord(s_list_name, null, filters, columns);

    var list_values = new Array();
    var dataArray = new Array();
    //nlapiLogExecution('Debug','a_search_results ',a_search_results.length);
    for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
        var data = {};
        data.list = {
            "name": a_search_results[i].getValue('name'),
            "id": a_search_results[i].id
        };
        list_values.push(data); //({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
    }
    return list_values;
}
/*********************************************Function to Project Role Values**********************************************/
function getProjectRoleValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption, i_project) {
    var a_search_results = nlapiSearchRecord("customrecord_frf_plan_details", null,
        [
            ["custrecord_frf_plan_details_ns_project", "anyof", i_project],
            "OR",
            ["custrecord_frf_plan_details_opp_id", "anyof", i_project]
        ],
        [
            new nlobjSearchColumn("custrecord_frf_plan_details_project_role", null, "GROUP")
        ]
    );
    var list_values = new Array();
    if (a_search_results) {
        for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
            list_values.push({
                'display': a_search_results[i].getValue("custrecord_frf_plan_details_project_role", null, "GROUP"),
                'value': a_search_results[i].getValue("custrecord_frf_plan_details_project_role", null, "GROUP")
            });
        }
    }
    return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Values**********************************************/
function getSearchValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption) {
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('entityid');

    var a_search_results = searchRecord(null, s_list_name, null, columns);

    var list_values = new Array();

    for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
        list_values.push({
            'name': a_search_results[i].getValue('entityid'),
            'id': a_search_results[i].id
        });
    }
    //nlapiLogExecution('Debug','list_values ',JSON.stringify(list_values));
    return list_values; //getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Employee Level Values**********************************************/
function getEmpLevelValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption) {
    var columns = new Array();
    columns[0] = new nlobjSearchColumn("employeestatus", null, "GROUP");

    var a_search_results = searchRecord(null, s_list_name, null, columns);

    var list_values = new Array();

    for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
        var data = {};
        data.emplevel = {
            "name": a_search_results[i].getText("employeestatus", null, "GROUP"),
            "id": a_search_results[i].getText("employeestatus", null, "GROUP")
        };
        //list_values.push({'display':a_search_results[i].getText("employeestatus",null,"GROUP"), 'value':a_search_results[i].getText("employeestatus",null,"GROUP")});
        list_values.push(data);
    }
    return list_values;
    //return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

/*********************************************Function for adding selected options**********************************************/
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions) {
    var strOptions = '';

    if (removeBlankOptions == true) {

    } else {
        strOptions = '<option value = "">' + (s_placeholder == undefined ? '' : s_placeholder) + '</option>';
    }

    var strSelected = '';

    for (var i = 0; i < a_data.length; i++) {
        if (i_selected_value == a_data[i].value) {
            strSelected = 'selected';
        } else {
            strSelected = '';
        }
        strOptions += '<option value = "' + a_data[i].value + '" ' + strSelected + ' >' + a_data[i].display + '</option>';
    }
    //nlapiLogExecution('Debug', 'stringsss', strOptions);
    return strOptions;
}
/***********************************************Function to get tile data***********************************/
function getTileData(i_account, s_projectName, i_opportunity) {
    nlapiLogExecution('Debug', 'i_account ', i_account);
    nlapiLogExecution('Debug', 's_projectName ', s_projectName);
    nlapiLogExecution('Debug', 'i_opportunity ', i_opportunity);
    var filters = [];


    if (s_projectName) {
        filters = [
            ['custrecord_frf_plan_details_ns_project', 'anyOf', s_projectName],
            'and',
            ['custrecord_frf_plan_details_confirmed', 'is', 'F']
        ];
    }
    if (i_opportunity) {
        filters = [
            ['custrecord_frf_plan_details_opp_id', 'anyOf', i_opportunity],
            'and',
            ['custrecord_frf_plan_details_confirmed', 'is', 'F']
        ];
    }

    /*if(i_account)
    	filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_ns_account',null,'anyof',i_account));
    if(s_projectName)
    	filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_ns_project',null,'anyof',s_projectName));
    if(i_opportunity)
    	filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_opp_id',null,'anyof',i_opportunity));
    filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_confirmed',null,'is','F'));
    */
    nlapiLogExecution('Debug', 'filters ', filters);
    var customrecord_frf_plan_detailsSearch = nlapiSearchRecord("customrecord_frf_plan_details", null, filters,
        [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_frf_plan_details_project_role"),
            new nlobjSearchColumn("custrecord_frf_plan_details_startdate"),
            new nlobjSearchColumn("custrecord_frf_plan_details_positions"),
            new nlobjSearchColumn("custrecord_frf_plan_request_type"),
            new nlobjSearchColumn("custrecord_frf_plan_details_ns_account"),
            new nlobjSearchColumn("custrecord_frf_plan_details_allocation"),
            new nlobjSearchColumn("custrecord_frf_plan_details_bill_rate"),
            new nlobjSearchColumn("custrecord_frf_plan_details_bill_role"),
            new nlobjSearchColumn("custrecord_frf_plan_details_criticalrole"),
            new nlobjSearchColumn("custrecord_frf_plan_details_emp_lvl"),
            new nlobjSearchColumn("custrecord_frf_plan_details_position_typ"),
            new nlobjSearchColumn("custrecord_frf_plan_details_enddate"),
            new nlobjSearchColumn("custrecord_frf_plan_details_externalhire"),
            new nlobjSearchColumn("custrecord_frf_plan_details_location"),
            new nlobjSearchColumn("custrecord_frf_plan_details_opp_id"),
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null),
            new nlobjSearchColumn("custrecord_frf_plan_details_primary"),
            new nlobjSearchColumn("custrecord_frf_plan_details_practice"),
            new nlobjSearchColumn("custrecord_frf_plan_details_ns_project"),
            new nlobjSearchColumn("custrecord_frf_plan_details_special_req"),
            new nlobjSearchColumn("custrecord_frf_plan_details_suggest_res"),
            new nlobjSearchColumn("custrecord_frf_plan_details_other_skills"),
            new nlobjSearchColumn("custrecord_frf_plan_first_inte_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_sec_inte_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_reason_rrf"),
            new nlobjSearchColumn("custrecord_frf_pla_det_backup_require"),
            new nlobjSearchColumn("custrecord_frf_plan_desc_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_job_title_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_edu_lvl_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_edu_pro_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_emp_status_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_cus_inter_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_cust_inter_email_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_instruction_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_res_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_replacement_emp"), //prabhat gupta
            new nlobjSearchColumn("custrecord_frf_plan_fitment_type"), //prabhat gupta  NIS-1723 04/9/2020
            new nlobjSearchColumn("custrecord_frf_plan_level") //prabhat gupta  NIS-1755 06/10/2020
        ]
    );

    if (customrecord_frf_plan_detailsSearch) {
        var tileDataArray = new Array();
        nlapiLogExecution('Debug', 'customrecord_frf_plan_detailsSearch ', customrecord_frf_plan_detailsSearch.length);
        for (var ii = 0; ii < customrecord_frf_plan_detailsSearch.length; ii++) {
            var b_billRoll = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_bill_role");
            var b_criticalRoll = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_criticalrole");
            var b_externalHire = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_externalhire");
            var tileData = {};
            tileData.recordid = {
                "name": customrecord_frf_plan_detailsSearch[ii].getId()
            };
            tileData.role = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_project_role"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_project_role")
            };
            tileData.positionType = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_position_typ"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_position_typ")
            };
            tileData.startDate = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_startdate")
            };
            tileData.endDate = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_enddate")
            };
            tileData.positions = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_positions")
            };
            tileData.Allocation = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_allocation")
            };
            tileData.billrate = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_bill_rate")
            };
            tileData.category = {
                "name": b_billRoll
            };
            tileData.splrequirment = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_special_req")
            };
            tileData.externalhire = {
                "name": b_externalHire
            };
            tileData.criticalrole = {
                "name": b_criticalRoll
            };
            tileData.account = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_account"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_ns_account")
            };
            tileData.emplevel = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_emp_lvl"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_emp_lvl")
            };
            tileData.location = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_location"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_location")
            };
            //tileData.primaryskills = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_primary"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_primary")};
            tileData.requestType = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_request_type"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_request_type")
            };

            //--------------------------------------------------------------------------------------------------------

            //prabhat gupta NIS-1723 04/09/2020

            tileData.fitmentType = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_fitment_type"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_fitment_type")
            };


            //---------------------------------------------------------------------------------------------------------

            //prabhat gupta NIS-1755 06/10/2020

            tileData.level = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_level"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_level")
            };


            //---------------------------------------------------------------------------------------------------------


            //	-------------------------------------------------------------------------------------------------------------
            //prabhat gupta


            var replacementCandidatesData = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_replacement_emp"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_replacement_emp")
            };
            var replacementCandidatesArray = [];
            for (var j = 0; j < replacementCandidatesData.name.split(",").length; j++) {

                replacementCandidatesArray.push({
                    name: replacementCandidatesData.name.split(",")[j],
                    id: replacementCandidatesData.id.split(",")[j]
                })
            }

            tileData.replacingCandidates = replacementCandidatesArray;
            //----------------------------------------------------------------------------------------------------------------

            tileData.practice = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_practice"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_practice")
            };
            if (customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_project")) {
                tileData.project = {
                    "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_project"),
                    "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_ns_project")
                };
            } else if (customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null)) {
                tileData.project = {
                    "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null),
                    "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null)
                }
            }


            var suggestedPeopleData = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")
            };
            var suggestedPeopleArray = [];
            for (var j = 0; j < suggestedPeopleData.name.split(",").length; j++) {

                suggestedPeopleArray.push({
                    name: suggestedPeopleData.name.split(",")[j],
                    id: suggestedPeopleData.id.split(",")[j]
                })
            }

            var primaryskills = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_primary"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_primary")
            };
            var primaryskillsArray = [];
            for (var j = 0; j < primaryskills.name.split(",").length; j++) {

                primaryskillsArray.push({
                    name: primaryskills.name.split(",")[j],
                    id: primaryskills.id.split(",")[j]
                })
            }

            var secondaryskills = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_other_skills"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_other_skills")
            };
            var secondaryskillsArray = [];
            for (var j = 0; j < secondaryskills.name.split(",").length; j++) {

                secondaryskillsArray.push({
                    name: secondaryskills.name.split(",")[j],
                    id: secondaryskills.id.split(",")[j]
                })
            }
            tileData.primaryskills = primaryskillsArray; //{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
            tileData.otherskills = secondaryskillsArray; //{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
            tileData.suggestedpeople = suggestedPeopleArray; //{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
            //tileData.otherskills = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_other_skills"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_other_skills")};
            tileData.frfsource = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_request_type"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_request_type")
            };
            tileData.backuprequired = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_pla_det_backup_require")
            };
            tileData.jobtitle = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_job_title_rrf")
            };
            tileData.jobdescription = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_desc_rrf")
            };
            tileData.instruction = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_instruction_rrf")
            };
            tileData.educationlevel = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_edu_lvl_rrf"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_edu_lvl_rrf")
            };
            tileData.educationprogram = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_edu_pro_rrf"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_edu_pro_rrf")
            };
            tileData.employeestatus = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_emp_status_rrf"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_emp_status_rrf")
            };
            tileData.firstinterviewer = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_first_inte_rrf")
            };
            tileData.secondinterviewer = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_sec_inte_rrf")
            };
            tileData.custinterview = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_cus_inter_rrf")
            };
            tileData.custintervieweremail = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_cust_inter_email_rrf")
            };
            tileData.externalhirereason = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_reason_rrf")
            };
            //tileDataArray.push(tileData);
            tileDataArray.push(tileData);
        }
        nlapiLogExecution('Debug', 'tileDataArray$$$$$ ', JSON.stringify(tileDataArray));
        return tileDataArray;
    }
}

////////////////////////////////////////////////////
/*****Function To Get Account List******/
function getAccountProjectList(user, practice) {
    var filters = new Array();
    var columns = new Array();

    /*filters = ["stage","anyof","CUSTOMER"]
    columns = [
       new nlobjSearchColumn("internalid"), 
       new nlobjSearchColumn("altname")
    ]
    var customerSearch = searchRecord("customer",null,filters,columns,null);
    */
    if (practice) {
        filters = ["custentity_practice", "anyof", practice];
    } else if (user) {
        filters = [
            ["custentity_deliverymanager", "anyof", user], "OR", ["custentity_projectmanager", "anyof", user]
        ];
    }

    columns = [
        new nlobjSearchColumn("customer"),
        new nlobjSearchColumn("altname"),
        new nlobjSearchColumn("internalid")
    ]

    var customerSearch = nlapiSearchRecord("job", null, filters, columns, null);

    if (customerSearch) {
        var dataArray = new Array();
        for (var i = 0; i < customerSearch.length; i++) {
            var data = {};
            data.account = {
                "name": customerSearch[i].getValue("altname"),
                "id": customerSearch[i].getValue('internalid')
            };
            data.project = {
                "name": customerSearch[i].getText("customer"),
                "id": customerSearch[i].getValue('customer')
            }
            dataArray.push(data);
        }
        return dataArray;
    }
}


/*****Function TO Get Project List******/
function getProjectList(user, practice, isAdmin) {
    var filters = new Array();
    var columns = new Array();
    if (isAdmin) {
        filters = ["isinactive", "is", "F"]
    } else if (practice) {
        filters = ["custrecord_fulfill_dashboard_practice", "anyof", practice],
            "AND",
            ["isinactive", "is", "F"]
    } else if (user) {
        filters = [
            ["custrecord_fulfill_dashboard_project.custentity_deliverymanager", "anyof", user],
            "OR",
            [
                ["custrecord_fulfill_dashboard_project.custentity_projectmanager", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_project.custentity_clientpartner", "anyof", user]
            ],
            "OR",
            [
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner", "anyof", user]
            ], //Need to check for filters
            "AND",
            ["isinactive", "is", "F"]
        ]
    }

    columns = [
        new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
        new nlobjSearchColumn("custrecord_fulfill_dashboard_account")
    ]
    var jobSearch = searchRecord("customrecord_fulfillment_dashboard_data", null, filters, columns, null);
    if (jobSearch) {
        var dataArray = new Array();
        for (var i = 0; i < jobSearch.length; i++) {
            var data = {};
            data.project = {
                "name": jobSearch[i].getText('custrecord_fulfill_dashboard_project'),
                "id": jobSearch[i].getValue('custrecord_fulfill_dashboard_project')
            };
            //data.account = {"name":jobSearch[i].getValue('custrecord_fulfill_dashboard_account')};
            dataArray.push(data);
        }
        return dataArray;
    }
}

/*****Function TO Get Account List******/
function getAccountList(user, practice, isAdmin, deliveryAnchore) {
    var filters = new Array();
    var columns = new Array();
    if (isAdmin) {
        filters = ["isinactive", "is", "F"]
    } else if (practice) {
        filters = [
            [
                ["custrecord_fulfill_dashboard_opp_id.custrecord_practice_internal_id_sfdc", "anyof", practice],
                "OR",
                ["custrecord_fulfill_dashboard_project.custentity_practice", "anyof", practice]
            ],
            "AND",
            ["isinactive", "is", "F"]
        ]
    } else if (deliveryAnchore.length > 0) {
        filters = [
            [
                ["custrecord_fulfill_dashboard_opp_id.custrecord_customer_internal_id_sfdc", "anyof", deliveryAnchore],
                "OR",
                ["custrecord_fulfill_dashboard_project.parent", "anyof", deliveryAnchore]
            ],
            "AND",
            ["isinactive", "is", "F"]
        ]
    } else if (user) {
        filters = [
            ["custrecord_fulfill_dashboard_project.custentity_deliverymanager", "anyof", user],
            "OR",
            [
                ["custrecord_fulfill_dashboard_project.custentity_projectmanager", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_project.custentity_clientpartner", "anyof", user]
            ],
            "OR",
            [
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner", "anyof", user]
            ], //Need to check for filters
            "AND",
            ["isinactive", "is", "F"]
        ]
    }

    columns = [
        new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
        new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
        new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null),
        new nlobjSearchColumn("customer", "custrecord_fulfill_dashboard_project", null)
    ]
    var jobSearch = searchRecord("customrecord_fulfillment_dashboard_data", null, filters, columns, null);
    if (jobSearch) {
        var dataArray = new Array();
        for (var i = 0; i < jobSearch.length; i++) {
            var data = {};
            //data.project = {"name":jobSearch[i].getText('custrecord_fulfill_dashboard_project'),"id":jobSearch[i].getValue('custrecord_fulfill_dashboard_project')};
            if (jobSearch[i].getText("customer", "custrecord_fulfill_dashboard_project", null))
                data.account = {
                    "name": jobSearch[i].getText("customer", "custrecord_fulfill_dashboard_project", null),
                    "id": jobSearch[i].getValue("customer", "custrecord_fulfill_dashboard_project", null)
                };
            else
                data.account = {
                    "name": jobSearch[i].getText("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null),
                    "id": jobSearch[i].getValue("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null)
                };
            dataArray.push(data);
        }
        nlapiLogExecution('Debug', 'dataArray 1130', JSON.stringify(dataArray));
        return dataArray;
    }
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

/**********Function to get Role List***********/
function getRole() {
    var customrecord_fuel_role_mapping_skillSearch = nlapiSearchRecord("customrecord_fuel_role_mapping_skill", null,
        [],
        [
            new nlobjSearchColumn("custrecord_fuel_role_mapping_name", null, "GROUP")
        ]
    );
    if (customrecord_fuel_role_mapping_skillSearch) {
        var dataArray = new Array();
        for (var i = 0; i < customrecord_fuel_role_mapping_skillSearch.length; i++) {
            var data = {};
            data.role = {
                "name": customrecord_fuel_role_mapping_skillSearch[i].getValue("custrecord_fuel_role_mapping_name", null, "GROUP")
            };
            dataArray.push(data);
            nlapiLogExecution('debug', 'dataArray 1232', dataArray);

        }
        return dataArray;
    }
}
/**************Get Admin User***********************/
function GetAdmin(user) {
    var adminSearch = nlapiSearchRecord("customrecord_fuel_leadership", null,
        [
            ["custrecord_fuel_leadership_employee", "anyof", user],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_leadership_employee")
        ]
    );
    return adminSearch;
}

function GetPracticeSPOC(user) {
    var spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null,
        [
            ["custrecord_spoc", "anyof", user],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_practice")
        ]
    );
    if (spocSearch) {
        var practice = spocSearch[0].getValue("custrecord_practice");
    }
    return practice;
}

function GetProject() {
    var customrecord_fulfillment_dashboard_dataSearch = nlapiSearchRecord("customrecord_fulfillment_dashboard_data", null,
        [
            [
                ["custrecord_fulfill_dashboard_project.custentity_deliverymanager", "anyof", "94862"], "OR", ["custrecord_fulfill_dashboard_project.custentity_projectmanager", "anyof", "94862"], "OR", ["custrecord_fulfill_dashboard_project.custentity_clientpartner", "anyof", "94862"]
            ],
            "AND",
            ["custrecord_fulfill_dashboard_practice", "anyof", "322"]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_reve_confid"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_rev_sta_pro"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_manager"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_practice"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_pro_team"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_rampup"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_exiting"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_frf"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_rrf"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_last_update"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_end_date"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_risk"),
            new nlobjSearchColumn("custrecord_fulfill_dashboard_overdue")
        ]
    );
}

function GetRegion(user) {
    try {
        nlapiLogExecution('AUDIT', 'user', user);
        var regionSearch = nlapiSearchRecord("customrecord_region", null,
            [
                [
                    ["custrecord_region_head", "anyof", user], "OR", ["custrecord_region_head_2", "anyof", user]
                ],
                "AND",
                ["isinactive", "is", "F"]
            ],
            [
                new nlobjSearchColumn("name")
            ]
        );
        if (regionSearch) {
            var region = regionSearch[0].getValue('name');
            return region;
        } else {
            return '';
        }

    } catch (e) {
        return e;
    }

}


function getProjectListNew(user, practice, isAdmin, deliveryAnchore) {
    var filters = new Array();
    var columns = new Array();
    if (isAdmin) {
        filters = [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfill_dashboard_opp_id", "anyof", "@NONE@"], "AND", ["custrecord_fulfill_dashboard_project.status", "noneof", "1"]
        ]
    } else if (practice) {
        filters = [
            ["custrecord_fulfill_dashboard_practice", "anyof", practice],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfill_dashboard_opp_id", "anyof", "@NONE@"], "AND", ["custrecord_fulfill_dashboard_project.status", "noneof", "1"]
        ]
    } else if (deliveryAnchore.length > 0) {
        filters = [
            ["custrecord_fulfill_dashboard_account", "anyof", deliveryAnchore],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfill_dashboard_opp_id", "anyof", "@NONE@"], "AND", ["custrecord_fulfill_dashboard_project.status", "noneof", "1"]
        ]
    } else if (user) {
        filters = [
            ["custrecord_fulfill_dashboard_project.custentity_deliverymanager", "anyof", user],
            "OR",
            [
                ["custrecord_fulfill_dashboard_project.custentity_projectmanager", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_project.custentity_clientpartner", "anyof", user]
            ],
            "OR",
            [
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                "OR",
                ["custrecord_fulfill_dashboard_opp_id.custrecord_sfdc_client_partner", "anyof", user]
            ], //Need to check for filters
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_fulfill_dashboard_opp_id", "anyof", "@NONE@"], "AND", ["custrecord_fulfill_dashboard_project.status", "noneof", "1"]
        ]
    }
    nlapiLogExecution('Debug', 'Filters = ', filters);
    columns = [
        new nlobjSearchColumn("custrecord_fulfill_dashboard_project"),
        new nlobjSearchColumn("custrecord_fulfill_dashboard_account"),
        new nlobjSearchColumn("customer", "custrecord_fulfill_dashboard_project", null),
        new nlobjSearchColumn("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null),
        new nlobjSearchColumn("startdate", "custrecord_fulfill_dashboard_project", null),
        new nlobjSearchColumn("enddate", "custrecord_fulfill_dashboard_project", null)
    ]
    var jobSearch = searchRecord("customrecord_fulfillment_dashboard_data", null, filters, columns, null);
    if (jobSearch) {
        var dataArray = new Array();
        for (var i = 0; i < jobSearch.length; i++) {
            if (jobSearch[i].getValue("customer", "custrecord_fulfill_dashboard_project", null)) {
                var account = jobSearch[i].getValue("customer", "custrecord_fulfill_dashboard_project", null);
            } else {
                var account = jobSearch[i].getValue("custrecord_customer_internal_id_sfdc", "custrecord_fulfill_dashboard_opp_id", null);
            }
            var projectStartDate = jobSearch[i].getValue("startdate", "custrecord_fulfill_dashboard_project", null);
            var projectEndDate = jobSearch[i].getValue("enddate", "custrecord_fulfill_dashboard_project", null);
            var data = {};
            data.data = {
                "name": jobSearch[i].getText('custrecord_fulfill_dashboard_project'),
                "id": jobSearch[i].getValue('custrecord_fulfill_dashboard_project'),
                "type": "project",
                "account_id": account,
                "projectStartDate": projectStartDate,
                "projectEndDate": projectEndDate
            };
            //data.account = {"name":jobSearch[i].getValue('custrecord_fulfill_dashboard_account')};
            dataArray.push(data);
        }
        return dataArray;
    }
}

//function to get the subpractice tagged to parent 
function getSubPractices(i_practice) {
    var temp = [];
    if (i_practice) {
        var departmentSearch = nlapiSearchRecord("department", null,
            [
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_parent_practice", "anyof", i_practice]
            ],
            []
        );
        if (departmentSearch) {
            for (var i = 0; i < departmentSearch.length; i++) {
                temp.push(departmentSearch[i].getId());
            }
            return temp;
        } else {
            return i_practice
        }
    }
}

function GetDeliveryAnchore(user) {
    var temp = [];
    var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor", null,
        [
            ["custrecord_fuel_anchor_employee", "anyof", user],
			"AND",
			["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_anchor_customer")
        ]
    );
    if (customrecord_fuel_delivery_anchorSearch) {
        for (var i = 0; i < customrecord_fuel_delivery_anchorSearch.length; i++) {
            temp.push(customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_customer'));
        }
        return temp;
    } else {
        return false;
    }
}

function getSoftLockedEmployees() {
    var empArray = new Array();
    var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_frf_details_selected_emp", "noneof", "@NONE@"],
            "AND",
            ["custrecord_frf_details_status", "is", "F"],
            "AND",
            [
                ["custrecord_frf_details_open_close_status", "noneof", 2],
                "OR",
                ["custrecord_frf_details_status_flag", "noneof", 2]
            ]
        ],
        [
            new nlobjSearchColumn("custrecord_frf_details_softlock_date"),
            new nlobjSearchColumn("custrecord_frf_details_selected_emp"),
            new nlobjSearchColumn("custrecord_frf_details_frf_number"),
            new nlobjSearchColumn("custrecord_frf_details_start_date"),
            new nlobjSearchColumn("custrecord_frf_details_end_date"),
            new nlobjSearchColumn("custrecord_frf_details_allocation")
        ]
    );
    if (customrecord_frf_detailsSearch) {
        for (var ii = 0; ii < customrecord_frf_detailsSearch.length; ii++) {
            var obj = {};
            obj.name = customrecord_frf_detailsSearch[ii].getText("custrecord_frf_details_selected_emp");
            obj.id = customrecord_frf_detailsSearch[ii].getValue("custrecord_frf_details_selected_emp");
            obj.frfNumber = customrecord_frf_detailsSearch[ii].getValue("custrecord_frf_details_frf_number");
            obj.startDate = customrecord_frf_detailsSearch[ii].getValue("custrecord_frf_details_start_date");
            obj.endDate = customrecord_frf_detailsSearch[ii].getValue("custrecord_frf_details_end_date");
            obj.allocation = parseInt(customrecord_frf_detailsSearch[ii].getValue("custrecord_frf_details_allocation"));
            empArray.push(obj);
        }
        //nlapiLogExecution('audit','empArray length ',empArray.length);
    }
    return empArray;
}

/*********************************************Function to get List Values**********************************************/
function getListValuesPositionType(s_list_name, strSelectedValue, s_placeholder, removeBlankOption) {
    var columns = new Array();
    var filters = new Array();
    columns[0] = new nlobjSearchColumn('name');
    filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
    filters.push(new nlobjSearchFilter('name', null, 'isnot', 'Campus'));
    filters.push(new nlobjSearchFilter('name', null, 'isnot', 'Direct Contract'));
    filters.push(new nlobjSearchFilter('name', null, 'isnot', 'Third Party Contract'));
    var a_search_results = searchRecord(s_list_name, null, filters, columns);

    var list_values = new Array();
    var dataArray = new Array();
    //nlapiLogExecution('Debug','a_search_results ',a_search_results.length);
    for (var i = 0; a_search_results != null && i < a_search_results.length; i++) {
        var data = {};
        data.list = {
            "name": a_search_results[i].getValue('name'),
            "id": a_search_results[i].id
        };
        list_values.push(data); //({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
    }
    return list_values;
}

function getNonDeliveryProjectList(user, practice, isAdmin, deliveryAnchore) {
    var filters = new Array();
    var columns = new Array();
    if (isAdmin) {
        filters = [
            ["isinactive", "is", "F"],
			"AND",
            ["status", "noneof", "1"]
        ]

    } else if (practice) {
        filters = [
            ["custentity_practice.custrecord_parent_practice", "anyof", practice],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["status", "noneof", "1"]
        ]
    } else if (deliveryAnchore.length > 0) {
        filters = [
            ["customer", "anyof", deliveryAnchore],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["status", "noneof", "1"]
        ]
    } else if (user) {
        filters = [
            ["custentity_deliverymanager", "anyof", user],
            "OR",
            [
                ["custentity_projectmanager", "anyof", user],
                "OR",
                ["custentity_clientpartner", "anyof", user]
            ],
            "AND",
            ["isinactive", "is", "F"],
            "AND",
            ["status", "noneof", "1"]
        ]
    }
    nlapiLogExecution('Debug', 'Filters = ', filters);
    columns = [
        new nlobjSearchColumn("formulatext").setFormula("concat(CONCAT({entityid}, ' '), {jobname})"),
        new nlobjSearchColumn("internalid"),
        new nlobjSearchColumn("customer"),
        new nlobjSearchColumn("internalid", "customer", null),
        new nlobjSearchColumn("startdate"),
        new nlobjSearchColumn("enddate")
    ]
    var jobSearch = searchRecord("job", null, filters, columns, null);



    if (jobSearch) {
        var dataArray = new Array();
        for (var i = 0; i < jobSearch.length; i++) {
            if (jobSearch[i].getValue("customer")) {
                var account = jobSearch[i].getValue("customer");
                var account_name = jobSearch[i].getText("customer");
            }

            var projectStartDate = jobSearch[i].getValue("startdate");
            var projectEndDate = jobSearch[i].getValue("enddate");
            var data = {};
            data.data = {
                "name": jobSearch[i].getValue('formulatext'),
                "id": jobSearch[i].getId(),
                "type": "project",
                "account_name": account_name,
                "account_id": account,
                "projectStartDate": projectStartDate,
                "projectEndDate": projectEndDate
            };

            dataArray.push(data);
        }
        return dataArray;
    }
}


/*
function getEmployeeLevel(){
	var list_values = [];
	var employeestatusSearch = nlapiSearchRecord("employeestatus",null,
									[
									   ["category","anyof","@NONE@"]
									], 
									[
									   new nlobjSearchColumn("name").setSort(false), 
									   new nlobjSearchColumn("category")
									]
									);
									
			for (var i = 0; employeestatusSearch != null && i < employeestatusSearch.length; i++) {
				var data = {};
				data.list = {
					"name": employeestatusSearch[i].getValue('name'),
					"id": employeestatusSearch[i].id
				};
				list_values.push(data); //({'display':employeestatusSearch[i].getValue('name'), 'value':employeestatusSearch[i].id});
    }
    return list_values;					
	
}
*/