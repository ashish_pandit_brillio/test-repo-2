/**
 * CSV Download For Employee TIme Sheet
 * 
 * Version Date Author Remarks 1.00
 * 
 * 12 Aug 2016 Deepak MS
 * 
 */
var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
function suitelet(request, response) {
	try {
	
	
	if(request.getMethod() == 'GET')
	{
		
	var current_date = nlapiDateToString(new Date());
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	//Create the form and add fields to it
	var form = nlapiCreateForm("Timesheet CSV Download");
	
	form.addFieldGroup('custpage_grp_0', 'Select Options');
	var customerField = form.addField('custpage_project',
		        'select', 'Project', 'job', 'custpage_grp_0').setBreakType('startcol');
				
		customerField.setMandatory(true); 		
				
	var startDate = form.addField('custpage_startdate', 'date', 'Start Date', null,
		        'custpage_grp_0')
		startDate.setBreakType('startcol');	
startDate.setMandatory(true); 	
	/*var endDate = form.addField('custpage_enddate',
		        'date', 'End Date', null, 'custpage_grp_0').setBreakType('startcol');
		endDate.setMandatory(true);	*/

		

	form.setScript('customscript_cs_ts_date_validate');
	form.addSubmitButton('Load Project Data');
	
		response.writePage(form);		
	}
	else
	{
	var form = nlapiCreateForm("Timesheet CSV Download");
	
	form.addFieldGroup('custpage_grp_01', 'Select Options');
	form.addFieldGroup('custpage_grp_1', 'Export Data');
	
	var projectID = request.getParameter('custpage_project');
	
	
	
	var projectName = searchProjectObj(projectID);
	//var project_Name  = nlapiLookupField('job',projectID,['entityid']);
	var customerField1 = form.addField('custpage_customer_post',
		        'text', 'Project', null, 'custpage_grp_01').setDisplayType('inline');
	//customerField1.setBreakType('startcol');
	customerField1.setDefaultValue(projectName);	
	
	
	var st_date = request.getParameter('custpage_startdate');
	var mode = request.getParameter('mode')	
	var startDate1 = form.addField('custpage_startdate_post', 'date', 'Start Date', null,
		        'custpage_grp_01').setDisplayType('inline');
	//startDate1.setBreakType('startcol');
	startDate1.setDefaultValue(request.getParameter('custpage_startdate'));		
	
	/*var h_data =  form.addField('custpage_actual_count_data', 'text', 'Note',
	        null, 'custpage_grp_01');
	h_data.setBreakType('startcol');
	h_data.setDefaultValue("Please manually update Task and Service Item if any Leaves/Holidays falls in the week");
	h_data.setDisplayType('inline');*/
	//var endDate1 = form.addField('custpage_enddate_post',
		       // 'date', 'End Date', null, 'custpage_grp_01').setDefaultValue(request.getParameter('custpage_enddate'));
	//endDate1.setBreakType('startcol');
	//endDate1.setDisplayType('inline');
				
				
	
	var resource_List  = searchProjects(projectID,st_date);
	var actualCount = searchTotalResource(projectID);
	
	var actual_Count = actualCount.length;
	actual_Count = actual_Count.toFixed(0);
	
	
	var csvLink = "<a href='/app/site/hosting/scriptlet.nl?script=1026&deploy=1"
        + "&custpage_project="
        + projectID
        + "&custpage_startdate="
        + st_date
        + "&mode=CSV' target='_blank'>"
        + "<img style='height: 40px; margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460'></a>"; //https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460
//https://debugger.sandbox.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460
	var csvLinkField = form.addField('custpage_download', 'inlinehtml', '',
        null, 'custpage_grp_1');
	//csvLinkField.setBreakType('startcol');
	csvLinkField.setDefaultValue(csvLink);
	
	var count = 0;
	for(var k=0;k<resource_List.length;k++){
		var datarow = resource_List[k];
	    var emp = datarow.Resource;
	    count = parseInt(k+1);
	    count = count.toFixed(0);
	}
	//form.setScript('');

	
	var header_field =  form.addField('custpage_actual_count', 'text', 'Actual Count',
	        null, 'custpage_grp_1');
		header_field.setBreakType('startcol');
		header_field.setDefaultValue(actual_Count);
		header_field.setDisplayType('inline');
		
		var header_field_1 =  form.addField('custpage_header', 'text', 'Active Resource Count',
		        null, 'custpage_grp_1');
		header_field_1.setBreakType('startcol');
		header_field_1.setDefaultValue(count);
		header_field_1.setDisplayType('inline');
			
	
			form.addFieldGroup('custpage_grp_2', 'Projects');
			form.setScript('customscript_cs_ts_sut_header');
	//TIme Sheet Search
	//var timeSheet_res_list = searchTimeSheet(resource_List,st_date);
	
	//var F_resource = resourceCompare(resource_List,timeSheet_res_list);
	
		var html  = createTableList(resource_List,st_date,projectName);
		var tableField = form.addField('custpage_html', 'inlinehtml', null,
			        null, 'custpage_grp_2');
		
			tableField.setDefaultValue(html);
			tableField.setBreakType('startcol');
			response.writePage(form);
	}
}	
catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}	
}
//Count of total resources
function searchTotalResource(proJ_ID) {
	try{
		var dataRow_R = [];
		var filters = Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', proJ_ID));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		
		var cols = Array();
		cols.push(new nlobjSearchColumn('resource','resourceallocation')); //parent

		var projectSearchObj = nlapiSearchRecord('job',null,filters,cols);
		if(projectSearchObj){
			for(var i=0;i<projectSearchObj.length;i++){
				dataRow_R.push(projectSearchObj[i].getValue('resource','resourceallocation'));
			}
		}
		return dataRow_R;
	}
	catch(e){
		
	}
	
}
//Compare Resource Against TimeSheet Submitted Resource
function resourceCompare(res_alloc_list,time_Sheet_res_list){
	try{
		var dataRow_F = [];
		var dataRow_S =[];
		var json_F = {};
		var flag =false;
		for(var i=0;i<res_alloc_list.length;i++){
			var datarow_resAlloc = res_alloc_list[i];
			for(var j=0;j<time_Sheet_res_list.length;j++){
				flag =false;
				var timeStRow = time_Sheet_res_list[j];
				if(timeStRow.Resource == datarow_resAlloc.Resource){
					dataRow_S.push(timeStRow.Resource);
					flag=true;
					break;
				}
				
				
			}
			if(flag==false){
				json_F ={
						Resource: datarow_resAlloc.Resource
					};
				dataRow_F.push(json_F);
			flag=false;
			}
		}
		return dataRow_F;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Compare Process Error',e);
	}
	
}
function searchTimeSheet(resList,d_stDate){
	try{
		var json_T = {};
		var dataRows = [];
		var T_stDate =  nlapiStringToDate(d_stDate);
		for(var m=0;m<resList.length;m++){
			var datarow = resList[m];
			var emp_id = datarow.Resource;
			
			var filter = Array();
			filter.push(new nlobjSearchFilter('employee', null, 'anyof', emp_id ));
			//filter.push(new nlobjSearchFilter('startdate', null, 'noton', T_stDate));
			filter.push(new nlobjSearchFilter('formuladate', null , 'on',
					T_stDate).setFormula('{startdate}'));
			
			var cols = Array();
			cols.push(new nlobjSearchColumn('employee'));
			//cols.push(new nlobjSearchColumn('internalid'));
			
			
			var searchRes_T = nlapiSearchRecord('timesheet',null,filter,cols);
			
			if(searchRes_T){
				for(var p=0;p<searchRes_T.length;p++){
					json_T ={
							Resource: searchRes_T[p].getValue('employee')
					};
					
				}
				dataRows.push(json_T);
			}
		}
		return dataRows;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Time Sheet Process Error',e);
	}
	
}
function generateTableCsv(res_list,d_stDate,projectVal){
	try{
var html = "";
		

		html += "Project,";
		html += "Employee,";
		html += "Task,";
		html += "Service Item,";
		html += "Days Of Week,";
		html += "Start Date,";
		html += "Duration,";
		html += "Approval Status,";
		html += "\r\n";

		// table header
		for(var j=0;j<res_list.length;j++){
			var projectData = res_list[j];
			
			var employee = nlapiLookupField('employee',projectData.Resource,['entityid']);
			
			for(var index=0;index<=6;index++){
			var day = days[index];
			
		
					html += projectVal;
					html += ",";
					html += employee.entityid;
					html += ",";
					html += "Project Activites (Project Task)";
					html += ",";
					html += "ST";
					html += ",";
					html += day;
					html += ",";
					html += d_stDate;
					html += ",";
					html += "";
					html += ",";
					html += "";
					html += "\r\n";
			}
		}
		/*for(var v=0;v<timeEntryList_1.length;v++){
			var employeeData = timeEntryList_1[v];
			var Amt = employeeData.Bill_rate * employeeData.TotalHrs;
			
			
			html += employeeData.Project;
			html += ",";
			html += employeeData.Employee;
			html += ",";
			html += employeeData.PracticeR;
			html += ",";
			html += employeeData.VericalR;
			html += ",";
			html += employeeData.Bill_rate;
			html += ",";
			html += employeeData.TotalHrs;
			html += ",";
			html += Amt;
			html += "\r\n";
		}*/
		
		return html;
		
	}
	catch(e){
		nlapiLogExecution('DEBUG','Process Error',e);	
	}
	
}
function searchProjectObj(proID){
	try{
		var project_Concat;
		var filter = Array();
		filter.push(new nlobjSearchFilter('internalid', null, 'is', proID));
		
		var cols = Array();
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('companyname','customer'));
		cols.push(new nlobjSearchColumn('companyname'));
		
		var searchRes = nlapiSearchRecord('job',null,filter,cols);
		
		if(searchRes){
			var projectID = searchRes[0].getValue('entityid');
			var customer_Name = searchRes[0].getValue('companyname','customer');
			var projectName = searchRes[0].getValue('companyname');
			
			// project_Concat = projectID+' '+customer_Name+' '+':'+' '+projectName;
			 project_Concat = projectID+' '+' '+projectName;
		}
		return project_Concat;
	}
	catch(e){
		nlapiLogExecution('debug','Project Search Error',e);
	}
}

function searchProjects(proj_ID,d_StartDate){
try{
var d_st_date = nlapiStringToDate(d_StartDate);
var filters = Array();
filters.push(new nlobjSearchFilter('internalid', null, 'is', proj_ID));
filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
filters.push(new nlobjSearchFilter('startdate','resourceallocation', 'onorbefore',d_st_date));
filters.push(new nlobjSearchFilter('enddate','resourceallocation', 'onorafter',d_st_date));

var cols = Array();
cols.push(new nlobjSearchColumn('resource','resourceallocation')); //parent

var projectSearchObj = nlapiSearchRecord('job',null,filters,cols);
var dataRows =[];
var resourceList = {};
if(projectSearchObj){
for(var i=0;i<projectSearchObj.length;i++){
resourceList = {
Resource: projectSearchObj[i].getValue('resource','resourceallocation')
};
dataRows.push(resourceList);
}
}

return dataRows;
}
catch(e){
nlapiLogExecution('DEBUG',' Project Search Error',e);
}

}

function date_split(date){
	
	var dateArray = date.split('/');
	var date_minus =  parseInt(dateArray[2]) - 2000;
	
	var len_month  = dateArray[0].length;
	var len_date = dateArray[1].length;
	
	if(len_month ==1 || parseInt(dateArray[1]) < 12) {
		dateArray[0] = parseInt(dateArray[0])+ Math.floor(1+(Math.random())* 0x56) ;
	}
	
	var date_minus = dateArray[2] - 2000;
	var total = parseInt(dateArray[0]) + parseInt(dateArray[1]) + parseInt(date_minus);
	return total;
}

function createTableList(res_list,d_stDate,projectVal){
try{

var date_1 = date_split(d_stDate);


	var html = "";
	// table css
	html += "<style>";
	html += ".ops-metrics-table td {border:1px solid black;}";
	html += ".ops-metrics-table tr:nth-child(even) {background: #ffffe6}";
	html += ".ops-metrics-table tr:nth-child(odd) {background: #e6ffe6}";
	html += ".ops-metrics-table tr:nth-child(even):hover {background: #ffff99}";
	html += ".ops-metrics-table tr:nth-child(odd):hover {background: #80ff80}";
	html += ".ops-metrics-table .header td {background-color: #001a33 !important; color : white !important;}";
	html += "</style>";

	// table
	html += "<table class='ops-metrics-table' width='250%' >";

	html += "<tr class='header'>";

	html += "<td  font-size='20'>";
	html += "Project";
	html += "</td>";

	html += "<td  font-size='20'>";
	html += "Employee";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Task";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Service Item";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Day of Week";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Start Date";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Duration";
	html += "</td>";
	
	html += "<td  font-size='20'>";
	html += "Approval Status";
	html += "</td>";
	

	
	html += "</tr>";	
	// table header
		for(var j=0;j<res_list.length;j++){
		var projectData = res_list[j];
		
		var employee = nlapiLookupField('employee',projectData.Resource,['entityid']);
		
		for(var index=0;index<=6;index++){
		var day = days[index];
		
		//if(projectData.Project){
		html += "<tr>";
		
		html += "<td font-size='14'>";
				html += projectVal;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += employee.entityid;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += "Project Activities (Project Task)";
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += "ST";
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += day;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += d_stDate;
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += "";
				html += "</td>";
				
				html += "<td font-size='14'>";
				html += "";
				html += "</td>";
		}

				/*var linkURL = nlapiResolveURL('SUITELET', 'customscript_sut_invoice_employee_report','customdeploy_sut_invoice_employee_report', null)
				+ '&custpage_project_id=' + projectData.Id + '&custpage_start_date=' + stDate + '&custpage_end_date=' + end_date ;
				
				 var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript_sut_invoice_employee_report', 'customdeploy_sut_invoice_employee_report', false);

				var param = '&custpage_project_id=' + projectData.Id + '&custpage_start_date=' + stDate + '&custpage_end_date=' + end_date;

				//var url = 'https://debugger.sandbox.netsuite.com'+suiteletUrl+param;
				var url = 'https://system.na1.netsuite.com'+suiteletUrl+param;
				var finalUrl = url + suiteletUrl + param;
				nlapiLogExecution('debug','url',url);
				nlapiLogExecution('debug','finalUrl',finalUrl);
				//var request  = nlapiRequestURL();
				
				html += "<td  font-size='14' >";
				html += "<a href="+url+">View</a>";
			   
				html += "</td>";*/
				//html += "<td width='100%' font-size='14'>";
				//Define the parameters of the Suitelet that will be executed.
			//html +="<p><a href="http://www.w3schools.com/html/">Visit our HTML tutorial</a></p>";
			
			//Create a link to launch the Suitelet.
			//createNewReqLink.setDefaultValue('<B>Click <A HREF="' + linkURL +'">here</A> to
			//create a new document signature request record.</B>');
				
			//	html +="<a href="http://www.w3schools.com">View</a>";
			//	html += "<p>View<a href  ="https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=681&deploy=1&compid=3883006&h=889d72db68c85c6ca6a0" ></p>";

				//html += "</td>";
		html += "</tr>";
		}
	//}
	html += "</table>";

	return html;

}
catch(e){
nlapiLogExecution('DEBUG',' Project Search Error',e);
}

}