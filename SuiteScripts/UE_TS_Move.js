/**
 * Add a shortcut for navigating to the project task suitelet
 * 
 * Version Date Author Remarks 1.00 22 Feb 2016 Nitish Mishra
 * 
 */

function userEventBeforeLoad(type, form, request) {
	form
	        .addPageLink(
	                'crosslink',
	                'Task',
	                'https://3883006.app.netsuite.com/app/site/hosting/scriptlet.nl?script=785&deploy=1');

	form
	        .addPageLink(
	                'crosslink',
	                'Sub Task',
	                "https://3883006.app.netsuite.com/app/common/custom/custrecordentrylist.nl?rectype=316");
}
