/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Sep 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	try{
		if(type == 'view'){
			
			form.setScript('customscript_cs_ofc_calling_suitelt');
			var i_recordID = nlapiGetRecordId();
			var customerList = nlapiGetFieldValues('custrecord_customer_ofc');
			var projectList = nlapiGetFieldValues('custrecord_project_ofc');
			var month = nlapiGetFieldText('custrecord_month_ofc');
			var year = nlapiGetFieldText('custrecord_year_ofc');
			var status_OFC = nlapiGetFieldValue('custrecord_ofc_cost_status');
			nlapiLogExecution('DEBUG','Values ','customerList:'+customerList+'projectList:'+projectList+'month:'+month+'year:'+year);
			
			if((nlapiGetFieldValue('custrecord_journal_entry_ofc') == '' || nlapiGetFieldValue('custrecord_journal_entry_ofc') == null) && (nlapiGetFieldValue('custrecord_ofc_cost_status') != 'Processing' && nlapiGetFieldValue('custrecord_ofc_cost_status') != 'Pending'))
			{
				form.addButton('custpage_create_ofc', 'Create JE', 'Create_JE(\'  ' + i_recordID + '  \');');
			}
			if(nlapiGetFieldValue('custrecord_ofc_cost_status') == 'Failed'){
				return;
			}
			if (nlapiGetFieldValue('custrecord_ofc_cost_status') != 'Complete' && (nlapiGetFieldValue('custrecord_ofc_cost_status') != '' && nlapiGetFieldValue('custrecord_ofc_cost_status') != null))
			{
				var filters = new Array();
				//filters.push(new nlobjSearchFilter('internalid', 'script', 'is', '1017')); sandbox
				filters.push(new nlobjSearchFilter('internalid', 'script', 'is', '1054'));
				
				var column = new Array();
				column.push(new nlobjSearchColumn('name', 'script'));
				column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
				column.push(new nlobjSearchColumn('datecreated'));
				column.push(new nlobjSearchColumn('status'));
				column.push(new nlobjSearchColumn('startdate'));
				column.push(new nlobjSearchColumn('enddate'));
				column.push(new nlobjSearchColumn('queue'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				column.push(new nlobjSearchColumn('queueposition'));
				column.push(new nlobjSearchColumn('percentcomplete'));
				
				var a_search_results = nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
				if (a_search_results) {
					for (var i = 0; i < a_search_results.length; i++) {
						var s_script = a_search_results[i].getValue('name', 'script');
						
						var d_date_created = a_search_results[i].getValue('datecreated');
						
						var i_percent_complete = a_search_results[i].getValue('percentcomplete');
						
						var s_script_status = a_search_results[i].getValue('status');
						
					}
				}
				
				if (i_percent_complete == '' || i_percent_complete == null || i_percent_complete == undefined) {
					s_script_status = 'Not Started';
					i_percent_complete = '0%';
				}
				
				if (s_script_status == 'Complete') {
					i_percent_complete = '100%';
					s_script_status = 'Complete';
				}
				
				var message = '<html>';
				message += '<head>';
				message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
				message += '<meta charset="utf-8" />';
				message += '</head>';
				message += '<body>';
				var i_counter = '0%'
				message += "<div id=\"my-progressbar-container\">";
				message += "            ";
				message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
				message += "<img src='https://debugger.sandbox.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:30%;width:30%;align=left;'/>";
				message += "        <\/div>";
				message += '</body>';
				message += '</html>';
				
				nlapiSetFieldValue('custrecord_ofc_cost_progress_bar', message);
				
				nlapiSubmitField('customrecord_ofc_cost_against_customer', nlapiGetRecordId(), 'custrecord_ofc_cost_status', s_script_status);
			}
		}
		
	}
    catch(e){
    	nlapiLogExecution('DEBUG','User Event Error',e);
    }
 
}
