/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

var PAGE_SIZE = 1000;
var SEARCH_ID = 'customsearch3284'//'customsearch3249'; SB
var CLIENT_SCRIPT_FILE_ID = 2346293;

define(['N/ui/serverWidget', 'N/search', 'N/redirect', 'N/file', 'N/encode','N/task'],
		function (serverWidget, search, redirect, file, encode,task) {
	function onRequest(context) {
		if (context.request.method == 'GET') {
			var form = serverWidget.createForm({
				title: 'Unallocated Report',
				hideNavBar: false
			});

		//	form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;

			// Get parameters
			var scriptId = context.request.parameters.script;
			var deploymentId = context.request.parameters.deploy;
			
			form.addSubmitButton({
				id: 'exportexcelreport',
				label: 'Generate Report'
			});
			var stDate = form.addField({
				id: 'custpage_st_date',
				type: serverWidget.FieldType.DATE,
				label: 'Start Date'
			});
			if(startDate)
			{
				stDate.defaultValue = startDate;
			}
			var edDate =  form.addField({
				id: 'custpage_end_date',
				type: serverWidget.FieldType.DATE,
				label: 'End Date'
			});
			if(endDate)
			{
				edDate.defaultValue = endDate;
			}
			
			context.response.writePage(form);
		}
		else 
		{
			var startDate = context.request.parameters.custpage_st_date;
			var endDate = context.request.parameters.custpage_end_date;
			log.debug('startDate',startDate);
			log.debug('endDate',endDate);
			
			var schTask = task.create({
				taskType: task.TaskType.SCHEDULED_SCRIPT
			});
			schTask.scriptId = 'customscript_unallocated_back_report';
			schTask.deploymentId = 'customdeploy_unallocated_back_report';
			schTask.params = {
					'custscript_start_date_un' : startDate,
					"custscript_end_date_un":endDate
			};
			var taskId = schTask.submit();
			context.response.write("Your request is processing. The report will sent you on email.");
			/*	var json = "";
			if(startDate && endDate)
			{
				json = runSearch(SEARCH_ID, startDate, endDate);
			}
			var Gsd = new Date(startDate);
			var Ged = new Date(endDate);
			log.debug('allocation_Data',JSON.stringify(json));
	
			var glob_array = [];
			for (var key in json) {
				if(json[key].length>1)
				{
					for (var r=0;r<json[key].length;r++)
					{  
						  // var endDate = json[key][r].enddate;
							   // if(r>0)
									// var endDate = json[key][r-1].enddate;	
						var endDate =  (r>0) ? json[key][r-1].enddate : json[key][r].enddate;
						var stDate = json[key][r].startdate;
						var hire_date = new Date(json[key][r].actual_hire_date);
						
						if( hire_date>=Gsd && hire_date<=Ged && r==0) 
						{
							var NstDate = stDate;
							if(hire_date< new Date(NstDate) || hire_date> new Date(NstDate))
							{
								glob_array.push(json[key][r]);
							}
						}
						if(endDate && stDate && r>0)
						{
						   var diff = daysDiff(endDate , stDate);
						   if(diff>1)
						   {
							   glob_array.push(json[key][r]);
						   }
						}
					}
				}
				log.debug("Final Array : ",JSON.stringify(glob_array));
			}

			var delimiter1 = /\u0001/;
			var delimiter = /\u0002/;
			var SO_ID = [];
			var sublistData = context.request.parameters.sublistdata.split(delimiter);
			log.debug('length', sublistData.length);
			
			var hardHeaders = ["ALLOCATION INTERNAL ID ", "RESOURCE","SUBSIDIARY","DEPARTMENT","LOCATION","REPORTING MANAGER","HIRE DATE"];
			var CSVData = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
			CSVData += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
			CSVData += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
			CSVData += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
			CSVData += 'xmlns:html="http://www.w3.org/TR/REC-html40">';

			CSVData += '<Styles>';

			CSVData += '<Style ss:ID="s1">';
			CSVData += '<Font ss:Bold="1" ss:Underline="Single"/>';
			CSVData += '<Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>';
			CSVData += ' <Borders>';
			CSVData += ' <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>';
			CSVData += '</Borders>';
			CSVData += '</Style>';

			CSVData += '</Styles>';

			CSVData += '<Worksheet ss:Name="Report">';
			CSVData += '<Table><Row>';
			for (var k = 0; k < hardHeaders.length; k++) {
				CSVData += '<Cell ss:StyleID="s1"><Data ss:Type="String">' + hardHeaders[k] + '</Data></Cell>';
			}
			CSVData += '</Row>';
			for (var i = 0; i < glob_array.length; i++) {
				
				CSVData += '<Row>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].id + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].resource_text + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].subsidiary + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].department + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].location + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].reportingmanager + '</Data></Cell>';
				CSVData += '<Cell><Data ss:Type="String">' + glob_array[i].actual_hire_date + '</Data></Cell>';
				
				CSVData += '</Row>';
			}
			
			CSVData += '</Table></Worksheet></Workbook>';
			var strXmlEncoded = encode.convert({
				string: CSVData,
				inputEncoding: encode.Encoding.UTF_8,
				outputEncoding: encode.Encoding.BASE_64
			});

			var objXlsFile = file.create({
				name: 'Un-Allocated Employee Report.xls',
				fileType: file.Type.EXCEL,
				contents: strXmlEncoded
			});
			// Optional: you can choose to save it to file cabinet
			// objXlsFile.folder = -14;
			// var intFileId = objXlsFile.save();

			context.response.writeFile({
				file: objXlsFile
			});
*/
		}
	}

	return {
		onRequest: onRequest
	};
	function daysDiff(a,b){
		var date1 = new Date(a);
		var date2 = new Date(b);
		var diffTime = Math.abs(date2 - date1);
		var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
		return diffDays;
		} 
	function runSearch(searchId, startDate, endDate) {
		var searchObj = search.load({
			id: searchId
		});
		if (startDate && endDate)
		{
			searchObj.filters.push(search.createFilter({
				name : 'startdate',
				operator : search.Operator.ONORAFTER,
				values : startDate
			}));
			searchObj.filters.push(search.createFilter({
				name : 'enddate',
				operator : search.Operator.ONORBEFORE,
				values : endDate
			}));
		}

		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = searchObj.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		log.debug('resultsresultsresults', results.length);

		var resultsofData = new Array();
		for (var res = 0; res < results.length; res++) {

			var internalId = results[res].getValue({name: "id", label: "ID"});
			var resource = results[res].getValue({
				name: "resource",
				sort: search.Sort.ASC,
				label: "Resource"
			});
			var resource_text = results[res].getText({
				name: "resource",
				sort: search.Sort.ASC,
				label: "Resource"
			});
			var project = results[res].getValue({name: "company", label: "Project"});

			var startdate = results[res].getValue({
				name: "startdate",
				sort: search.Sort.ASC,
				label: "Start Date"
			});
			var enddate = results[res].getValue({name: "enddate", label: "End Date"});
			var percentoftime = results[res].getValue({name: "percentoftime", label: "Percentage of Time"});
			var actual_hire_date = results[res].getValue({
				name: "hiredate",
				join: "employee",
				label: "Actual Hire Date"
			});
			var subsidiary = results[res].getText({
				  name: "subsidiary",
			         join: "employee",
			         label: "Subsidiary"
			});
			var department = results[res].getText({
				name: "department",
		         join: "employee",
		         label: "Practice"
			});
			var location = results[res].getText({
				 name: "location",
		         join: "employee",
		         label: "Location"
			});
			var reportingmanager = results[res].getText({
				  name: "custentity_reportingmanager",
			         join: "employee",
			         label: "Reporting Manager"
			});
			
			resultsofData.push({
				"id": internalId,
				"resource": resource,
				"resource_text":resource_text,
				"project": project,
				"startdate": startdate,
				"enddate": enddate,
				"percentoftime": percentoftime,
				"actual_hire_date": actual_hire_date,
				"subsidiary":subsidiary,
				"department":department,
				"location":location,
				"reportingmanager":reportingmanager
			});
		}
		log.debug('All resultsofData is :', resultsofData.length);
		var groupResult_resource = resultsofData.reduce(function (r, a) {
			r[a.resource] = r[a.resource] || [];
			r[a.resource].push(a);
			return r;
		}, Object.create(null));

		return groupResult_resource;

	}
});