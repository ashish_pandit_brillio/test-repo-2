// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:	CLI_ST_VALIDATE_40
	Author:			Vikrant
	Company:		Aashna	
	Date:			02-09-2014
    Description:	Validation for timesheet, should contain on 40 hrs for ST service items for a week


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_val_st_hrs() //
{
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SAVE RECORD CODE BODY
	try //
	{
		var a = new Array();
        a['User-Agent-x'] = 'SuiteScript-Call';
		var resposeObject='';
		
		var weekly = nlapiGetFieldValue('weekly');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'weekly : ' + weekly);
		
		if (weekly == 'T') // checking if it is weekly timesheet form then do not execute this script
		{
			//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Execution stops as it is a weekly timesheet');
			return true;
		}
		
		var internal_ID = nlapiGetFieldValue('id');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'internal_ID : ' + internal_ID);
		
		var time_flag = true;
		
		if (_validate(internal_ID)) // if ID is present then this is earlier time else it is new time
		{
			time_flag = true;
		}
		else //
		{
			time_flag = false;
		}
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'time_flag : ' + time_flag);
		
		var trandate = nlapiGetFieldValue('trandate');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'trandate : ' + trandate);
		
		var employee = nlapiGetFieldValue('employee');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'employee : ' + employee);
		
		var hours = nlapiGetFieldValue('hours');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'hours : ' + hours);
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'g_hrs : ' + g_hrs);
		if(g_hrs == 0) //
		{
			hours = 0;
			return false;
		}
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'hours : ' + hours);
		
		hours = _correct_time(hours);
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'after correction hours : ' + hours);
		
		var emp_ID = nlapiGetFieldValue('employee');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'emp_ID : ' + emp_ID);
		
		var project_ID = nlapiGetFieldValue('customer');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'project_ID : ' + project_ID);
		
		var casetaskevent_ID = nlapiGetFieldValue('casetaskevent');
		//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'casetaskevent_ID : ' + casetaskevent_ID);
		
		//return false;
		
		var daily_limit = null;
		var week_limit = null;
		var st_task_id = '';
		var leave_task_id = '';
		var holiday_task_id = '';
		var st_task_id = '';
		var ot_task_id = '';
		var FH_task_id = '';
		var service_item = '';
		
		if (_validate(project_ID)) // 
		{
			var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
			+ '&custscript_req_type=project', null, a);
    		var status = resposeObject.getBody();
			var project_rec = status; //nlapiLoadRecord('job', project_ID);
			
			if (_validate(project_rec)) //
			{
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Project found...');
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=custentity_hoursperday', null, a);
    			var status = resposeObject.getBody();
			
				var temp = status; //project_rec.getFieldValue('custentity_hoursperday');
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'daily_limit temp: ' + temp);
				
				if (_validate(temp)) //
				{
					daily_limit = temp;
				}
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'daily_limit : ' + daily_limit);
				
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=custentity_hoursperweek', null, a);
    			var status = resposeObject.getBody();
				
				temp = status; //project_rec.getFieldValue('custentity_hoursperweek');
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'week_limit temp: ' + temp);
				
				if (_validate(temp)) //
				{
					week_limit = temp;
				}
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'week_limit : ' + week_limit);
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=st_task', null, a);
    			var status = resposeObject.getBody();
				
				st_task_id = status; //get_ST_Task_ID(project_ID);
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'st_task_id : ' + st_task_id);
				
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=lv_task', null, a);
    			var status = resposeObject.getBody();
				
				leave_task_id = status; //get_Leave_Task_ID(project_ID);
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'leave_task_id : ' + leave_task_id);
				
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=hd_task', null, a);
    			var status = resposeObject.getBody();
				
				holiday_task_id = status; //get_Holiday_Task_ID(project_ID);
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'holiday_task_id : ' + holiday_task_id);
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=ot_task', null, a);
    			var status = resposeObject.getBody();
				
				ot_task_id = status; //get_OT_Task_ID(project_ID)
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'ot_task_id : ' + ot_task_id);
				
				
				var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
				+ '&custscript_req_type=fh_task', null, a);
    			var status = resposeObject.getBody();
				
				FH_task_id = status; //get_FH_Task_ID(project_ID);
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'FH_task_id : ' + FH_task_id);
			}
			else //
			{
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Project record not found');
			}
		}
		
		if (_validate(emp_ID)) //
		{
			//if (casetaskevent_ID == st_task_id) // if it is not a ST task
			{
				if (daily_limit != null) //  
				{
					trandate = nlapiStringToDate(trandate);
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'nlapiStringToDate trandate : ' + trandate);
					trandate = nlapiDateToString(trandate);
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'nlapiDateToString trandate : ' + trandate);
					
					var filters = new Array();
					filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', emp_ID);
					filters[filters.length] = new nlobjSearchFilter('date', null, 'on', trandate);
					filters[filters.length] = new nlobjSearchFilter('internalid', 'projecttask', 'anyof', st_task_id);
					filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', project_ID);
					filters[filters.length] = new nlobjSearchFilter('type', null, 'anyof', 'A'); // Allow only Actual time...
					
					var columns = new Array();
					columns[columns.length] = new nlobjSearchColumn('durationdecimal', null, 'sum');
					
					//var search_result = nlapiSearchRecord('timebill', 'customsearch_st_40_validation', filters, null);
					var search_result = nlapiSearchRecord('timebill', null, filters, columns);
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_result : ' + search_result);
					
					var search_total_hrs = 0;
					
					if (_validate(search_result)) //
					{
						var count = search_result.length;
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'count : ' + count);
						
						var result = search_result[0];
						var column = result.getAllColumns();
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'count 2 : ' + count);
						
						for (var i = 0; i < count; i++) //
						{
							search_total_hrs = result.getValue(column[0]);
							//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'into search search_total_hrs : ' + search_total_hrs);
							
							if (_validate(search_total_hrs)) //
							{
								//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'into search search_total_hrs : ' + search_total_hrs);
							}
							else //
							{
								search_total_hrs = 0;
								//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'into search search_total_hrs : ' + search_total_hrs);
							}
						}
					}
					else //
					{
						nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_result is invalid : ' + search_result);
					}
				}
				else //
				{
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Daily limit is not defined on Project !');
				}
			}
			//else //
			{
				//nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'It is not a ST task !!!');
			}
			
			// Leave validation
			if (daily_limit != null) //
			{
				if (casetaskevent_ID == leave_task_id) // if it is leave task then check for remaining hrs of day
				{
                    /*
                     var remaining_hrs = parseFloat(daily_limit) - parseFloat(search_total_hrs);
                     
                     if (remaining_hrs > hours) // if remaining hrs are greater than entered hrs
                     {
                     alert('You can only enter "' + remaining_hrs + '" for leave.');
                     nlapiSetFieldValue('hours', '', false, false);
                     return false;
                     }
                     */
					if (hours > 8)//
					{
						alert('You can only enter more 8 hours for leave !');
						nlapiSetFieldValue('hours', '', false, false);
						g_hrs = 0;
						return false;
					}
				}
			}
			else //
			{
				nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Day limit not defined !');
			}
			
			
			// Holiday validation
			if (search_total_hrs > 0) // If ST hrs are present then restrict from entering Holiday.
			{
				if (casetaskevent_ID == holiday_task_id) //
				{
					alert('You cannot enter Holiday where Standard Time is entered.');
					nlapiSetFieldValue('hours', '', false, false);
					g_hrs = 0;
					return false;
				}
			}
			
			
			if (time_flag) // if true subtract current hrs from, true means its edit in earlier time.
			{
				var orighours = nlapiGetFieldValue('orighours');
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'orighours : ' + orighours);
				
				if (_validate(orighours)) //
				{
					orighours = _correct_time(orighours); // correct hrs and mins
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', '_correct_time orighours : ' + orighours);
					
					search_total_hrs = parseFloat(search_total_hrs) - parseFloat(orighours);
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'substract search_total_hrs : ' + search_total_hrs);
					
					search_total_hrs = parseFloat(search_total_hrs) + parseFloat(hours);
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'addition search_total_hrs : ' + search_total_hrs);
				}
				else //
				{
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_total_hrs : ' + search_total_hrs);
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'hours : ' + hours);
					
					search_total_hrs = parseFloat(search_total_hrs) + parseFloat(hours);
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_total_hrs : ' + search_total_hrs);
				}
			}
			else //
			{
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_total_hrs : ' + search_total_hrs);
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'hours : ' + hours);
				
				search_total_hrs = parseFloat(search_total_hrs) + parseFloat(hours);
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_total_hrs : ' + search_total_hrs);
			}
			
			if (daily_limit != null && casetaskevent_ID == st_task_id) //
			{
				if (search_total_hrs > daily_limit) //
				{
					alert('ST hours cannot be more than day limit "' + daily_limit + '", Please check it before submitting !');
					nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'Alert is working');
					nlapiSetFieldValue('hours', '', false, false);
					g_hrs = 0;
					return false;
				}
				else {
					nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'Time is fine working');
				}
			}
			else //
			{
				nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Day limit not defined !');
			}
			
			
			//  Validating for weekly time sheet entry
			var total_week_hrs = 0;
			if (week_limit != null) //
			{
				//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'week_limit : ' + week_limit);
				
				//if (casetaskevent_ID == st_task_id) //
				{
					trandate = nlapiStringToDate(trandate);
					trandate = new Date(trandate);
					var day = trandate.getDay(); // sunday is 0 for this
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'day : ' + day);
					
					if (day == 0)//
					{
						day == -7;
					}
					else //
					{
						day = day - 1;
						day = 0 - day;
					}
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'day : ' + day);
					
					trandate = nlapiAddDays(trandate, day);
					//trandate = nlapiDateToString(trandate);
					
					var end_date = nlapiAddDays(new Date(trandate), 6);
					end_date = nlapiDateToString(end_date);
					trandate = nlapiDateToString(trandate);
					
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'trandate : ' + trandate);
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'end_date : ' + end_date);
					
					var filters = new Array();
					filters[filters.length] = new nlobjSearchFilter('employee', null, 'anyof', emp_ID);
					filters[filters.length] = new nlobjSearchFilter('date', null, 'within', trandate, end_date);
					filters[filters.length] = new nlobjSearchFilter('internalid', 'projecttask', 'anyof', st_task_id);
					filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', project_ID);
					filters[filters.length] = new nlobjSearchFilter('type', null, 'anyof', 'A'); // Allow only Actual time...
					
					var columns = new Array();
					columns[columns.length] = new nlobjSearchColumn('durationdecimal', null, 'sum');
					
					//var search_result = nlapiSearchRecord('timebill', 'customsearch_st_40_validation', filters, null);
					var search_result = nlapiSearchRecord('timebill', null, filters, columns);
					//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_result : ' + search_result);
					
					if (_validate(search_result)) //
					{
						var count = search_result.length;
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'count : ' + count);
						
						var result = search_result[0];
						var column = result.getAllColumns();
						//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'count 2 : ' + count);
						
						for (var i = 0; i < count; i++) //
						{
							total_week_hrs = result.getValue(column[0]);
							nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', '1 total_week_hrs : ' + total_week_hrs);
							
							if (_validate(total_week_hrs)) //
							{
							
							}
							else //
							{
								total_week_hrs = 0;
								//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'total_week_hrs : ' + total_week_hrs);
							}
						}
					}
					else //
					{
						nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'search_result is invalid : ' + search_result);
					}
					
					if (time_flag) // means it is editing earlier time
					{
						if (total_week_hrs > 0) //
						{
							var orighours = nlapiGetFieldValue('orighours');
							//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'orighours : ' + orighours);
							
							if (_validate(orighours)) //
							{
								orighours = _correct_time(orighours); // correct hrs and mins
								nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', '_correct_time orighours : ' + orighours);
								
								//total_week_hrs = parseFloat(total_week_hrs) - parseFloat(orighours);
								//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'substract total_week_hrs : ' + total_week_hrs);
								
								if (casetaskevent_ID == st_task_id) ///
								{
									nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'total_week_hrs : ' + total_week_hrs);
									total_week_hrs = parseFloat(total_week_hrs) - parseFloat(orighours);
									nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'substract total_week_hrs : ' + total_week_hrs);
									
									
									total_week_hrs = parseFloat(total_week_hrs) + parseFloat(hours);
									nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'addition total_week_hrs : ' + total_week_hrs);
								}
							}
							else //
							{
								if (casetaskevent_ID == st_task_id) ///
								{
									total_week_hrs = parseFloat(total_week_hrs) + parseFloat(hours);
									nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'total_week_hrs : ' + total_week_hrs);
								}
							}
						}
					}
					else //
					{
						if (casetaskevent_ID == st_task_id) ///
						{
							total_week_hrs = parseFloat(total_week_hrs) + parseFloat(hours);
							nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'total_week_hrs : ' + total_week_hrs);
						}
					}
					
					if (casetaskevent_ID == st_task_id) //
					{
						if (total_week_hrs > week_limit) //
						{
                            nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'total_week_hrs : ' + total_week_hrs + ', week_limit : ' + week_limit);
							alert('You cannot enter more than weekly limit of "' + week_limit + '" hours');
							nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'Weekly Alert is working');
							nlapiSetFieldValue('hours', '', false, false);
							g_hrs = 0;
							return false;
						}
					}
				}
			}
			else //
			{
				nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Weekly time limit is not defined !');
			}
			
			if (casetaskevent_ID == ot_task_id) // if it a OT task
			{
				//nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'total_week_hrs : ' + total_week_hrs);
				if (total_week_hrs < week_limit) //
				{
					alert('You cannot enter OT hours as your ST hours for this week are below the limit of "' + week_limit + '" hours !');
					nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Weekly Alert is working');
					nlapiSetFieldValue('hours', '', false, false);
					g_hrs = 0;
					return false;
				}
			}
		}
		else //
		{
			nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', 'Employee ID is invalid : ' + emp_ID);
		}
		
		return true;
	} 
	catch (ex)//
	{
		nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'ex : ' + ex);
		nlapiLogExecution('ERROR', 'saveRecord_val_st_hrs', 'ex.message : ' + ex.message);
		//return false; 
	}
}

// END SAVE RECORD ==================================================








// BEGIN VALIDATE FIELD =============================================

function fieldChanged(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================




var g_hrs = 0;
// BEGIN FIELD CHANGED ==============================================

function weekly_fld_validate(type, name, linenum)//
{
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	try //
	{
		var a = new Array();
        a['User-Agent-x'] = 'SuiteScript-Call';
		var resposeObject='';
		
		if(name == 'hours' || name == 'casetaskevent')	 //
		{
			//nlapiLogExecution('DEBUG', 'saveRecord_val_st_hrs', '********* Execution Started ********');
			
			var hours = nlapiGetFieldValue('hours');
            //nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'hours : ' + hours);
			
			if (_validate(hours)) //
			{
				hours = _correct_time(hours);
				//nlapiLogExecution('DEBUG', 'weekly_fld_validate', '_correct_time hours : ' + hours);
				
				var project_ID = nlapiGetFieldValue('customer');
				//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'project_ID : ' + project_ID);
				
				var casetaskevent_ID = nlapiGetFieldValue('casetaskevent');
				//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'casetaskevent_ID : ' + casetaskevent_ID);
				
				var trandate = nlapiGetFieldValue('trandate');
				//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'trandate : ' + trandate);
				
				var daily_limit = null;
				var week_limit = null;
				var st_task_id = '';
				var leave_task_id = '';
				var holiday_task_id = '';
				var ot_task_id = '';
				var FH_task_id = '';
				var service_item = '';
				var custentity_otapplicable = 'F';
				
				service_item = nlapiGetFieldValue('item');
				//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'service_item : ' + service_item);
				
				if (_validate(project_ID)) // 
				{
					var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
					+ '&custscript_req_type=project', null, a);
            		var status = resposeObject.getBody();
					
					var project_rec = status; //nlapiLoadRecord('job', project_ID);
					
					if (_validate(project_rec)) //
					{
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=custentity_hoursperday', null, a);
	            		var status = resposeObject.getBody();
						var temp = status; // project_rec.getFieldValue('custentity_hoursperday');
						
						if (_validate(temp)) {
							daily_limit = temp;
						}
						
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=custentity_hoursperweek', null, a);
	            		var status = resposeObject.getBody();
						temp = status; //project_rec.getFieldValue('custentity_hoursperweek');
						
						if (_validate(temp)) {
							week_limit = temp;
						}
						
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=custentity_otapplicable', null, a);
	            		var status = resposeObject.getBody();
						custentity_otapplicable = status; //project_rec.getFieldValue('custentity_otapplicable');
						//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'custentity_otapplicable : ' + custentity_otapplicable);
						
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=st_task', null, a);
	            		var status = resposeObject.getBody();
						
						st_task_id = status; //get_ST_Task_ID(project_ID);
						//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'st_task_id : ' + st_task_id);
						
						
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=lv_task', null, a);
	            		var status = resposeObject.getBody();
						
						leave_task_id = status; //get_Leave_Task_ID(project_ID);
						//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'leave_task_id : ' + leave_task_id);
						
						
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=hd_task', null, a);
	            		var status = resposeObject.getBody();
						
						holiday_task_id = status; //get_Holiday_Task_ID(project_ID);
						//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'holiday_task_id : ' + holiday_task_id);
						
						
						var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + project_ID 
						+ '&custscript_req_type=fh_task', null, a);
	            		var status = resposeObject.getBody();
						
						FH_task_id = status; //get_FH_Task_ID(project_ID);
						//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'FH_task_id : ' + FH_task_id);
						
					}
					else //
					{
						//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'Project record not found');
					}
					
					if (service_item == '2425') //  if service item is OT
					{
						if (custentity_otapplicable == 'F') //
						{
							alert('OT hours are not allowed for this Project !!!');
							nlapiSetFieldValue('hours', '', false, false);
							g_hrs = 0;
							return false;
						}
					}
					
					if (casetaskevent_ID == st_task_id) // if task is ST
					{
						if (daily_limit != null) //
						{
							if (hours > daily_limit) //
							{
								alert('Time entered by you exceeds day limit of "' + daily_limit + '" hours !');
								nlapiSetFieldValue('hours', '', false, false);
                                g_hrs = 0;
								return false;
							}
						}
					}
					
					if (casetaskevent_ID == leave_task_id) // if task is Leave
					{
						if (hours > 8) //
						{
							alert('You cannot enter more than 8 hours in Leave !');
							nlapiSetFieldValue('hours', '', false, false);
							g_hrs = 0;
							return false;
						}
					}
					
					if (casetaskevent_ID == holiday_task_id) // if task is Holiday
					{
						if (hours != 8) //
						{
							alert('You can enter 8 hours only in Floating Holiday !');
							nlapiSetFieldValue('hours', '', false, false);
							g_hrs = 0;
							return false;
						}
					}
					
					
				}
				else //
				{
					//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'project_ID invalid : ' + project_ID);
				}
			}
			
		}
	} 
	catch (ex) //
	{
		nlapiLogExecution('ERROR', 'weekly_fld_chnged', 'ex : ' + ex);
		nlapiLogExecution('ERROR', 'weekly_fld_chnged', 'ex : ' + ex.message);
	}
	
	g_hrs = nlapiGetFieldValue('hours');
	//nlapiLogExecution('DEBUG', 'weekly_fld_validate', 'g_hrs : ' + g_hrs);
	return true;
	
	
	//  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function get_ST_Task_ID(projectID) // search for ST task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_st_task"
	// And confirm that Task names of "OT", "Leave" and "Holiday" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_st_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var st_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Task_ID', 'st_task_ID : ' + st_task_ID);
			
			return st_task_ID;
		}
	}
	return null;
}

function get_Holiday_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_holiday_task"
	// And confirm that Task names of "Holiday" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_holiday_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var hday_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Holiday_Task_ID', 'hday_task_ID : ' + hday_task_ID);
			
			return hday_task_ID;
		}
	}
	return null;
}

function get_FH_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_fh_task"
	// And confirm that Task names of "Floating Holiday" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_fh_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var fh_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_FH_Task_ID', 'fh_task_ID : ' + fh_task_ID);
			
			return fh_task_ID;
		}
	}
	return null;
}

function get_Leave_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_leave_task"
	// And confirm that Task names of "Leave" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_leave_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var leave_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
			
			return leave_task_ID;
		}
	}
	return null;
}

function get_OT_Task_ID(projectID) // search for Holiday task in project task list
{
	var filters = new Array();
	filters[filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', projectID);
	
	// In case of any issue, please take a look at saved search "customsearch_task_search_for_ot_task"
	// And confirm that Task names of "Leave" has not been change in NetSuite 
	// You can check this by analyzing result if the saved search
	
	var search_result = nlapiSearchRecord('projecttask', 'customsearch_task_search_for_ot_task', filters, null);
	
	if (_validate(search_result)) //
	{
		//nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'search_result.length : ' + search_result.length);
		
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			var leave_task_ID = result.getValue(all_columns[0]);
            //nlapiLogExecution('DEBUG', 'get_Leave_Task_ID', 'leave_task_ID : ' + leave_task_ID);
			
			return leave_task_ID;
		}
	}
	return null;
}


function _validate(obj) //
{
	if(obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}

function _correct_time(t_time) //
{
	// function is used to correct the time 
	
	if(_validate(t_time))
	{
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
		
		var hrs = t_time.split(':')[0];	 	//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		var mins = t_time.split(':')[1];	//nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
		
		mins = parseFloat(mins) / parseFloat('60');  //nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
		
		hrs = parseFloat(hrs) + parseFloat(mins);	//nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);	
		
		return hrs;
	}
	else
	{
        //nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
	}
	
}


// END FUNCTION =====================================================
