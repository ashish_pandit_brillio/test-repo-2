/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Apr 2019     Aazamali Khan    Update the FRF status based on 
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
  
	var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	var allocatedEmp = recObj.getFieldValue("custrecord_frf_details_allocated_emp");
	var softlockedEmp = recObj.getFieldValue("custrecord_frf_details_selected_emp"); 
	if(_logValidation(softlockedEmp)){
		recObj.setFieldValue("custrecord_frf_details_status_flag", 3);
	}
  	if(_logValidation(allocatedEmp)){
		recObj.setFieldValue("custrecord_frf_details_status_flag", 2);
	}
    nlapiSubmitRecord(recObj);
}
function _logValidation(value) {
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	} else {
		return false;
	}
}