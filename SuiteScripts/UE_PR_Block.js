/**
 * Block record access for employee center
 * 
 * Version Date Author Remarks 1.00 18 Feb 2016 Nitish Mishra
 * 
 */

function userEventBeforeLoad(type, form, request) {
	var context = nlapiGetContext();
	var employee = nlapiGetUser();

	if (employee != 9673 && employee != 5803) {
		if (context.getExecutionContext() == 'userinterface'
		        && context.getRoleCenter() == 'EMPLOYEE') {
			throw "System down for maintainance. Please check after some time.";
		}
	}
}
