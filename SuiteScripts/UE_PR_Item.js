/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Dec 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	try {

		// get the parent practice head and set it
		var practice = nlapiGetFieldValue('custrecord_prpractices');
		var parentPractice = nlapiLookupField('department', practice,
		        'custrecord_parent_practice');

		if (parentPractice) {
			var parentPracticeHead = nlapiLookupField('department',
			        parentPractice, 'custrecord_practicehead');

			if (parentPracticeHead) {
				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(),
				        'custrecord15', parentPracticeHead);
				nlapiLogExecution('debug',
				        'parent practice head value submitted');
			}
		} else {

		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}
