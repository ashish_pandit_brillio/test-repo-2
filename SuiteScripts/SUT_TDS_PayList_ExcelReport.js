
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_TDS_PayList_ExcelReport
	Author: Sunita Raskar
	Company: Aashna Cloudtech Pvt.Ltd
	Date: 4/4/2016
	Description: This script creates excel file to add tds list data of tds payment list section wise form


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY






}

// END SUITELET ====================================================


// Begin : Display_ExcelReport_TDSPay_SectionWise()
function Display_ExcelReport_TDSPay_SectionWise(requset, response)
{
	try
	{
	var SearchxmlString = '';
	nlapiLogExecution('DEBUG','test ', "test"+SearchxmlString);

	var postingperiod = request.getParameter('custscriptpaylist_tds_postingperiod');
	nlapiLogExecution('DEBUG','postingperiod ', "postingperiod"+postingperiod);
	var fromdate = request.getParameter('custscriptpaylist_fromdate')
	var todate = request.getParameter('custscriptpaylist_todate')
	var subsidiary = request.getParameter('custscriptpaylist_subsidiary')
	var tds_section = request.getParameter('custscriptpaylist_tds_section')
	var tds_assesssee = request.getParameter('custscriptpaylist_tds_assesse_type')

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
	if(_logValidation(postingperiod))
    {
    	filters.push( new nlobjSearchFilter('custrecord_iit_tds_posting_period', null, 'is', postingperiod));
    }
    else
    {
    	filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	    filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
    }
	/*filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));*/
	filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	filters.push(new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
	filters.push(new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
	//filters.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','check'));
	filters.push(new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is', 'F'))
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is', 'T'))
	filters.push(new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is', 'check'))
	
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_billvendorrel');
	column[1] = new nlobjSearchColumn('custrecord_billbillno');
	column[2] = new nlobjSearchColumn('custrecord_billbillamount');
	column[3] = new nlobjSearchColumn('custrecord_billtdsamount');
	column[4] = new nlobjSearchColumn('custrecord_billdate');
	column[5] = new nlobjSearchColumn('custrecord_billtdstype');
	column[6] = new nlobjSearchColumn('custrecord_billtdssection');
	column[7] = new nlobjSearchColumn('custrecord_billtdsaccount');
	column[8] = new nlobjSearchColumn('internalid');
	column[9] = new nlobjSearchColumn('custrecord_billtdspayable');
	column[10] = new nlobjSearchColumn('custrecord_bill_tds_section');
	column[11] = new nlobjSearchColumn('custrecord_billassessee_code');
	
	column[12] = new nlobjSearchColumn('custrecord_tdsperrate');
	column[13] = new nlobjSearchColumn('custentity_vendor_panno', 'custrecord_billvendorrel', null);
	column[14] = new nlobjSearchColumn('custentity_ao_certificate_number', 'custrecord_billvendorrel', null);
	column[15] = new nlobjSearchColumn('custentity_ao_cert_from_date', 'custrecord_billvendorrel', null);
	column[16] = new nlobjSearchColumn('custentity_ao_cert_to_date', 'custrecord_billvendorrel', null);
	column[17] = new nlobjSearchColumn('transactionnumber', 'CUSTRECORD_BILLBILLNO', null);
	column[18] = new nlobjSearchColumn('custrecord_tdsperrate');
	column[19] = new nlobjSearchColumn('custrecord_tds_transaction_type');
	
	var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, filters, column);
	nlapiLogExecution('DEBUG','results ', "results"+results);
	if (results != null) {
		//return results;
	}
	
	var filter1 = new Array();
	filter1.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
	if(_logValidation(postingperiod))
    {
		filter1.push( new nlobjSearchFilter('custrecord_iit_tds_posting_period', null, 'is', postingperiod));
    }
    else
    {
    	filter1.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
    	filter1.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
    }
	/*filter1.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	filter1.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));*/
	filter1.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	filter1.push(new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
	filter1.push(new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
	filter1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter1.push(new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
	filter1.push(new nlobjSearchFilter('approvalstatus', 'custrecord_billbillno', 'is', 2))
	filter1.push(new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is', 'T'))
	//filter1.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','bill'));
	filter1.push(new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is', 'F'))
	filter1.push(new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is', 'vendorbill'))
	
	
	var res = nlapiSearchRecord('customrecord_tdsbillrelation', null, filter1, column);
	if (res != null) {
		//return results;
		if (results != null) {
			var results = res.concat(results);
			//return results;
		}
		else {
			var results = res;
		}
	}
	var filter2 = new Array();
	filter2.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
	if(_logValidation(postingperiod))
    {
		filter2.push( new nlobjSearchFilter('custrecord_iit_tds_posting_period', null, 'is', postingperiod));
    }
    else
    {
    	filter2.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
    	filter2.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
    }
	/*filter2.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	filter2.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));*/
	filter2.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	filter2.push(new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
	filter2.push(new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
	filter2.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filter2.push(new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
	// filter2.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','journal'));
	filter2.push(new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is', 'F'))
	filter2.push(new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is', 'journalentry'))
	//filter2.push( new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is','T'))
	var res2 = nlapiSearchRecord('customrecord_tdsbillrelation', null, filter2, column);
	if (res2 != null) {
		var res3 = new Array()
		var i_new_bill = 0;
		var i_new_internalid = 0;
		for (var k = 0; k < res2.length; k++) {
			//var bill = res2[k].getValue('custrecord_billbillno')
			//nlapiLogExecution('DEBUG','Bill ', "bill 3"+bill);
			
			var internal_id = res2[k].getValue('internalid')
			//nlapiLogExecution('DEBUG','Bill ', "internal_id 3"+internal_id);
			
			if (internal_id != i_new_internalid) {
				var i_array_push = res2[k].getValue('internalid')
				
				res3.push(i_array_push)
				//nlapiLogExecution('DEBUG', 'Bill ', "res 3" + res3);
				
				//var i_new_bill = res2[k].getValue('custrecord_billbillno')
				//nlapiLogExecution('DEBUG','Bill ', "bill 3"+i_new_bill);
				
				var i_new_internalid = res2[k].getValue('internalid')
				//nlapiLogExecution('DEBUG','Bill ', "internal_id 3"+i_new_internalid);
			}
			
		}
		var filter3 = new Array();
		filter3.push(new nlobjSearchFilter('internalid', null, 'anyOf', res3));
		var res2 = nlapiSearchRecord('customrecord_tdsbillrelation', null, filter3, column);
		if (results != null) {
			var results = res2.concat(results);
		}
		else {
			var results = res2
		}
		
	}
	
	//return results;
	
	if (results != null && results != '' && results != undefined) 
	{
		for (var i = 0; i < results.length; i++) 
		{
			var searchResult = results[i];
			
			var billdate = searchResult.getValue('custrecord_billdate');
			nlapiLogExecution('DEBUG', 'tdslist', 'billdate : ' + billdate);
			
			var billno = searchResult.getText('custrecord_billbillno');
			nlapiLogExecution('DEBUG', 'tdslist', 'billno : ' + billno);
			
			var vendorrel = searchResult.getText('custrecord_billvendorrel');
			nlapiLogExecution('DEBUG', 'tdslist', 'vendorrel : ' + vendorrel);
			
			var tdstype = searchResult.getText('custrecord_billtdstype');
			nlapiLogExecution('DEBUG', 'tdslist', 'tdstype : ' + tdstype);
			
			var tdsrate = searchResult.getValue('custrecord_tdsperrate');
			nlapiLogExecution('DEBUG', 'tdslist', 'tdsrate : ' + tdsrate);
			
			var tdssection = searchResult.getValue('custrecord_billtdssection');
			nlapiLogExecution('DEBUG', 'tdslist', 'tdssection : ' + tdssection);
			
			var tds_assessee_type = searchResult.getText('custrecord_billassessee_code');
			nlapiLogExecution('DEBUG', 'tdslist', 'tds_assessee_type : ' + tds_assessee_type);
			
			var tdsaccount = searchResult.getText('custrecord_billtdsaccount');
			nlapiLogExecution('DEBUG', 'tdslist', 'tdsaccount : ' + tdsaccount);
			
			var billamount = searchResult.getValue('custrecord_billbillamount');
			nlapiLogExecution('DEBUG', 'tdslist', 'billamount : ' + billamount);
			
			var tdsamount = searchResult.getValue('custrecord_billtdsamount');
			nlapiLogExecution('DEBUG', 'tdslist', 'tdsamount : ' + tdsamount);
			
			var payableamount = searchResult.getValue('custrecord_billtdspayable');
			nlapiLogExecution('DEBUG', 'tdslist', 'payableamount : ' + payableamount);
			
			var pan_no = results[i].getValue(column[13]);
			nlapiLogExecution('DEBUG', 'tdslist', 'pan_no : ' + pan_no);
			
			var ao_certificate_no = results[i].getValue(column[14]);
			nlapiLogExecution('DEBUG', 'tdslist', 'ao_certificate_no : ' + ao_certificate_no);		
				
			var ao_certificate_from_date = results[i].getValue(column[15]);
			nlapiLogExecution('DEBUG', 'tdslist', 'pan_no : ' + ao_certificate_from_date);
			
			var ao_certificate_to_date = results[i].getValue(column[16]);
			nlapiLogExecution('DEBUG', 'tdslist', 'ao_certificate_to_date : ' + ao_certificate_to_date);
			
			var transaction_number = results[i].getValue(column[17]);
			nlapiLogExecution('DEBUG', 'tdslist', 'transaction_number : ' + transaction_number);
			
			var tds_rate = results[i].getValue(column[18]);
			nlapiLogExecution('DEBUG', 'tdslist', 'tds_rate : ' + tds_rate);
			
			var tds_transaction_type = results[i].getValue(column[19]);
			nlapiLogExecution('DEBUG', 'tdslist', 'tds_transaction_type : ' + tds_transaction_type);
			
			
			SearchxmlString += '<Row>' +
			'<Cell><Data ss:Type="String">' +
			billdate +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			billno +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			vendorrel +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tdstype +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tdsrate +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tdssection +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tds_assessee_type +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tdsaccount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			billamount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			tdsamount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="Number">' +
			payableamount +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			pan_no +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			ao_certificate_no +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			ao_certificate_from_date +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			ao_certificate_to_date +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			transaction_number +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tds_rate +
			'</Data></Cell>' +
			'<Cell><Data ss:Type="String">' +
			tds_transaction_type +
			'</Data></Cell>' +
			'</Row>';
		}
	}
	generate_excel_file(SearchxmlString, request, response);
	}
	catch(e)
	{
		if (e instanceof nlobjError)
			nlapiLogExecution('DEBUG', 'system error', e.getCode() + '\n'+ e.getDetails());
		else
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}
}
// End : Display_ExcelReport_TDSPay_SectionWise()

// Begin : generate_excel_file()

function generate_excel_file(SearchxmlString,request, response)
{
	nlapiLogExecution('DEBUG', 'generate vendor payable report', "Generate file");
	var xmlString = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
	xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
	xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
	xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
	xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
	xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">';
	
	xmlString += '<Worksheet ss:Name="Sheet1">';
	xmlString += '<Table>' +
	'<Row>' +
	'<Cell><Data ss:Type="String"> Date </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Bill No </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Vendor Name </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Type </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Rate </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Section </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Assessee Type </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Account Name </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Bill Amount </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Total TDS Amount </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Payable Amount </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Pan No </Data></Cell>' +
	'<Cell><Data ss:Type="String"> AO Certificate Number </Data></Cell>' +
	'<Cell><Data ss:Type="String"> AO Certificate From Date </Data></Cell>' +
	'<Cell><Data ss:Type="String"> AO Certificate To Date </Data></Cell>' +
	'<Cell><Data ss:Type="String"> Transaction Number </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Rate </Data></Cell>' +
	'<Cell><Data ss:Type="String"> TDS Transaction Type </Data></Cell>' +
	'</Row>';
	
	xmlString += SearchxmlString;
	
	xmlString += '</Table></Worksheet></Workbook>';
	
	//Create file
	var xlsFile = nlapiCreateFile('TDS_Payment_SectionWise_List.xls', 'EXCEL', nlapiEncrypt(xmlString, 'base64'));
	
	var folderID = get_folderID();
	xlsFile.setFolder(folderID);
	xlsFile.setIsOnline(true);
	
	//Save file 
	var fileID = nlapiSubmitFile(xlsFile);
	var fileobj = nlapiLoadFile(fileID);
	nlapiLogExecution('DEBUG', 'Script Scheduled ', 'fileobj -->' + fileobj);
			
	var file_url = fileobj.getURL();
	nlapiLogExecution('DEBUG', 'Script Scheduled ', 'file_url -->' + file_url);

    response.write(file_url);
}
// End : generate_excel_file()

// Begin : get_folderID()
function get_folderID()
{
			var folder_filters = new Array();
			folder_filters.push(new nlobjSearchFilter('name', null, 'is', 'Vendor_TDS_SectionWise_ExcelReport'));
	
			var folder_column = new Array();
			folder_column.push( new nlobjSearchColumn('internalid'));    
	
			var folder_results = nlapiSearchRecord('folder', null, folder_filters, folder_column);
			if (folder_results != null && folder_results != '' && folder_results != undefined) 
			{
				var folderId = folder_results[0].getId();
				
			}
			else
			{
				var o_folder_obj = nlapiCreateRecord('folder')
				o_folder_obj.setFieldValue('name','Vendor_TDS_SectionWise_ExcelReport')
				var folderId = nlapiSubmitRecord(o_folder_obj)
				
			}
			return folderId;
}
// End : get_folderID()


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
	function _logValidation(value) 
	{
		 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined'&& value != 'NaN' && value != NaN) 
		 {
		  return true;
		 }
		 else 
		 { 
		  return false;
		 }
	}


}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
