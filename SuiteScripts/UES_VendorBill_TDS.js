	/*	Script Name:    UES Vendor Bill  
	 Author:         Sachin B. Khairnar
	 Company:		Aashna Cloudtech Pvt. Ltd.
	 Date:
	 Version:
	 
	 Description:	This script Create a new Bill entry if Deduct TDS is applicable.
	 after creating Bill credit entry will update that Bill credit and apply particular bill to that entry
	 * Script Modification Log:
	 * 
	 -- Date --		-- Modified By --			--Requested By--				-- Description --
	 22 sep 2014         Supriya                    Kalpana                   Normal Account Validation
	 26 Feb 2014         Nikhil                     kalpana/Sachin K           Ait enhancement point
	 30 July 2015        Nikhil                     sachin k                   add a reneweal process
	 21 oct 2015         Nikhil j                   Sachin k                     currency other than Inr (convert to INR)
	 Below is a summary of the process controls enforced by this script file.  The control logic is described
	 more fully, below, in the appropriate function headers and code blocks.
	 
	 BEFORE LOAD
	 - beforeLoadRecord(type)
	 Not Used
	 
	 BEFORE SUBMIT
	 - beforeSubmitRecord(type)
	 Not Used
	 
	 AFTER SUBMIT
	 - afterSubmitRecord(type)
	 afterSubmitRecord(type)
	 
	 SUB-FUNCTIONS
	 - The following sub-functions are called by the above core functions in order to maintain code
	 modularization:
	 SearchGlobalParameter();
	 */
	function beforeLoadRecord(type, form)	
	{
	
	    /*  On before load:
	     
	     - EXPLAIN THE PURPOSE OF THIS FUNCTION
	     
	     -
	     
	     
	     FIELDS USED:
	     
	     --Field Name--				--ID--
	     
	     
	     */
	    //  LOCAL VARIABLES
	    
	    //  BEFORE LOAD CODE BODY
	    
	    
	    
	    return true;
	    
	    
	    
	}
	
	// END BEFORE LOAD ====================================================
	
	
	
	
	
	// BEGIN BEFORE SUBMIT ================================================
	
	function beforeSubmitRecord_ait_adjustTDS_Amt(type){
		/*  On before submit:
	 
	 - PURPOSE
	 -
	 
	 FIELDS USED:
	 
	 --Field Name--				--ID--
	 
	 
	 */
		//  LOCAL VARIABLES
		
		
		//  BEFORE SUBMIT CODE BODY
		
		
		
		if (type != 'delete') 
		{
			var s_recordType = nlapiGetRecordType();
			
			if (s_recordType == 'vendorbill') 
			{
			
				var a_subisidiary = new Array();
				var s_pass_code = '';
				var tdsRoundMethod = '';
				
				
				var i_AitGlobalRecId = SearchGlobalParameter();
				nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
				
				if (i_AitGlobalRecId != 0) {
					var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
					
					a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
					nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
					
					var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
					nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
					
					var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
					nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
					
					s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
					nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
					
					tdsRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_tds_roundoff');
					nlapiLogExecution('DEBUG', 'Bill ', " client tdsRoundMethod->" + tdsRoundMethod);
					
				} // END   if (i_AitGlobalRecId != 0)
				var subsidiary = nlapiGetFieldValue('subsidiary');
				
				var Flag = 0;
				
				Flag = isindia_subsidiary(a_subisidiary, s_pass_code, subsidiary)
				var currentContext = nlapiGetContext();
				if (Flag == 1 && (currentContext.getExecutionContext() != 'csvimport' || currentContext.getExecutionContext() != 'webservices')) 	
				{
				
					nlapiLogExecution('DEBUG', 'new bill credit created', "Inside Bill")
					var tdssublistFlag;
					
					var b_create_Billcredit = parseInt(0);
					
					var b_tdsApply = 0;
					
					var b_tdsAdjusted = parseInt(0);
					
					var b_adjust_Tds = nlapiGetFieldValue('custbody_adjust_tds_amt')
					
					if (b_adjust_Tds == 'T') {
						var account = nlapiGetLineItemValue('expense', 'account', 1);
						
						if (account != null && account != '' && account != undefined) {
							var totaltds = nlapiGetLineItemValue('expense', 'custcol_tdsamount', 1);
							var i_tdstype = nlapiGetLineItemValue('expense', 'custcol_tdstype', 1);
							var i_department = nlapiGetLineItemValue('expense', 'department', 1);
							var i_class = nlapiGetLineItemValue('expense', 'class', 1);
							var i_location = nlapiGetLineItemValue('expense', 'location', 1);
							
							var i_billBaseAmount = nlapiGetLineItemValue('expense', 'amount', 2);
							
							var i_RemainingTDS_Amt = (parseFloat(totaltds) + parseFloat(i_billBaseAmount))
							
							tdssublistFlag = parseInt(1);
							
							b_create_Billcredit = parseInt(1);
							
							b_tdsAdjusted = parseInt(1);
						}
						
						if (parseInt(b_tdsAdjusted) != parseInt(1)) {
							var item = nlapiGetLineItemValue('item', 'item', 1);
							
							if (item != null && item != '' && item != undefined) {
								var totaltds = nlapiGetLineItemValue('item', 'custcol_tdsamount', 1);
								var i_tdstype = nlapiGetLineItemValue('item', 'custcol_tdstype', 1);
								var i_department = nlapiGetLineItemValue('item', 'department', 1);
								var i_class = nlapiGetLineItemValue('item', 'class', 1);
								var i_location = nlapiGetLineItemValue('item', 'location', 1);
								
								var i_RemainingTDS_Amt = (parseFloat(totaltds) + parseFloat(i_billBaseAmount))
								//alert('i_RemainingTDS_Amt' + i_RemainingTDS_Amt)
								
								tdssublistFlag = parseInt(0);
								
								b_create_Billcredit = parseInt(1);
							}
						}
						
						if (parseInt(b_create_Billcredit) == parseInt(1)) {
						
							var tdsFields = ['custrecord_tdsrecievable_account', 'custrecord_tdsrecievable_item']
							
							var tdsmasterResults = nlapiLookupField('customrecord_tdsmaster', i_tdstype, tdsFields)
							
							var i_vendor = nlapiGetFieldValue('entity');
							
							var subsidiary = nlapiGetFieldValue('subsidiary');
							
							var currency = nlapiGetFieldValue('currency');
							
							var o_billcredit_obj = nlapiCreateRecord('vendorcredit')
							nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "bill credit object" + o_billcredit_obj)
							
							if (o_billcredit_obj != null && o_billcredit_obj != '' && o_billcredit_obj != undefined) {
								o_billcredit_obj.setFieldValue('entity', i_vendor)
								o_billcredit_obj.setFieldValue('subsidiary', subsidiary)
								//o_billcredit_obj.setFieldValue('memo', memo)
								o_billcredit_obj.setFieldValue('currency', currency)
								o_billcredit_obj.setFieldValue('department', i_department)
								o_billcredit_obj.setFieldValue('location', i_location)
								o_billcredit_obj.setFieldValue('class', i_class)
								
								if (parseInt(tdssublistFlag) == parseInt(1)) {
									var account = tdsmasterResults.custrecord_tdsrecievable_account;
									
									o_billcredit_obj.selectNewLineItem('expense')
									//o_billcredit_obj.setCurrentLineItemValue('expense', 'category', category)
									o_billcredit_obj.setCurrentLineItemValue('expense', 'account', account)
									o_billcredit_obj.setCurrentLineItemValue('expense', 'amount', i_RemainingTDS_Amt)
									//o_billcredit_obj.setCurrentLineItemValue('expense', 'taxcode', taxcode)
									o_billcredit_obj.setCurrentLineItemValue('expense', 'department', i_department)
									o_billcredit_obj.setCurrentLineItemValue('expense', 'location', i_location)
									o_billcredit_obj.setCurrentLineItemValue('expense', 'class', i_class)
									o_billcredit_obj.commitLineItem('expense')
								}
								if (parseInt(tdssublistFlag) == parseInt(0)) {
									var item = tdsmasterResults.custrecord_tdsrecievable_item;
									
									o_billcredit_obj.selectNewLineItem('item')
									o_billcredit_obj.setCurrentLineItemValue('item', 'item', item)
									o_billcredit_obj.setCurrentLineItemValue('item', 'quantity', '1')
									o_billcredit_obj.setCurrentLineItemValue('item', 'rate', i_RemainingTDS_Amt)
									o_billcredit_obj.setCurrentLineItemValue('item', 'amount', i_RemainingTDS_Amt)
									//o_billcredit_obj.setCurrentLineItemValue('item', 'taxcode', taxcode)
									o_billcredit_obj.setCurrentLineItemValue('item', 'department', i_department)
									o_billcredit_obj.setCurrentLineItemValue('item', 'location', i_location)
									o_billcredit_obj.setCurrentLineItemValue('item', 'class', i_class)
									o_billcredit_obj.commitLineItem('item')
								}
								var billcredit_id = nlapiSubmitRecord(o_billcredit_obj, false, false)
								nlapiLogExecution('DEBUG', 'new bill credit created', "billcredit id" + billcredit_id)
							}
						}
						nlapiSetFieldValue('custbody_adjust_tds_amt', 'F');
					}
					
				}
			}
		}
		return true;
	}
	
	// END BEFORE SUBMIT ==============================================
	
	// BEGIN AFTER SUBMIT =============================================
	function afterSubmitRecord(type)	
	{
	    
		if(type != 'delete')
		{
			//Begin :Variable Declaration
	    var billId = nlapiGetRecordId();
	    var recType = nlapiGetRecordType();
	    var billRec = nlapiLoadRecord(recType, billId);
	    var newAmount = billRec.getFieldValue('usertotal');
	    var userTotal = billRec.getFieldValue('usertotal');
	    var tdsAmount = billRec.getFieldValue('custbody_tdsamount');
	    var vendorcode = billRec.getFieldValue('entity');
	    var period = billRec.getFieldValue('postingperiod');
	    var currency = billRec.getFieldValue('currencyname');
	    var tranDate = billRec.getFieldValue('trandate');
	    var exchangeRate = billRec.getFieldValue('exchangerate');
	    var account = billRec.getLineItemValue('expense', 'account', 1);
	    var tdsDeduct = billRec.getFieldValue('custbody_deducttds');
	    var customform = billRec.getFieldValue('customform');
	    var subsidiary = billRec.getFieldValue('subsidiary');
	    var postingPeriod = billRec.getFieldValue('postingperiod');
		var tdsRoundMethod;
		
		//==========================Get indian Currency internalD and Exchange rate =====================//
		var currency = billRec.getFieldValue('currency')
		var INRcurrencyID = getAITIndianCurrency()
		var exchangerate = nlapiExchangeRate(currency, INRcurrencyID)
		
	    var tdsaccountArray = new Array();
	    var tdsamountArray = new Array();
	    var tdssectionArray = new Array();
	    var tdsnameArray = new Array();
	    var taxcodeArray = new Array();
	    var tdsrateArray = new Array();
	    var baseamtArray = new Array();
		var a_subisidiary = new Array();
		var tdssection_listArray = new Array();
		var tdsasseesseecode = new Array();
	    var tdsamt;
	    var itamt;
	    var scamt;
	    var itscamt;
	    var ecamt;
	    var tdstype
	    var tdsapply
	    var taxcode;
	    var baseamt;
	    var s_pass_code;
	    //End:Variable Declaration
	    
	    // === CODE TO GET THE GLOBAL SUBSIDIARY PARAMETER ===
	    var i_AitGlobalRecId = SearchGlobalParameter();
	    nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
	    
	    if (i_AitGlobalRecId != 0)	
		 {
	        var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
	        
	        a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
	        nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
	        var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
	        nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
	        
	        var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
	        nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
			nlapiLogExecution('DEBUG','Bill ', "s_pass_code->"+s_pass_code);
			
			tdsRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_tds_roundoff');
			nlapiLogExecution('DEBUG','Bill ', "tdsRoundMethod->"+tdsRoundMethod);
	        
	    } // END   if (i_AitGlobalRecId != 0)	
	    
		
		var Flag = 0;
		Flag = isindia_subsidiary(a_subisidiary,s_pass_code,subsidiary)
		var currentContext = nlapiGetContext();
	    if (Flag == 1 && (currentContext.getExecutionContext() != 'csvimport' || currentContext.getExecutionContext() != 'webservices')) 	
		{
			var pan_number = nlapiLookupField('vendor',vendorcode,'custentity_vendor_panno')
	    
	        //**********************************************Begin:If expense item is selected******************************************************************* 
	        var rowcount = billRec.getLineItemCount('expense');
	        var p = 1;
	        if (rowcount >= 1) 	
			{
	            for (var k = 1; k <= rowcount; k++) 	
				{
	                tdstype = billRec.getLineItemValue('expense', 'custcol_tdstype', k);
	                tdsapply = billRec.getLineItemValue('expense', 'custcol_tdsapply', k);
	                
	                if (tdsapply == 'T') 
					{
	                
	                    taxcode = billRec.getLineItemValue('expense', 'taxcode', k);
						
						if (INRcurrencyID != currency) 
						{
							var Billtdsamount = billRec.getLineItemValue('expense', 'custcol_tdsamount', k);
							var i_INR_amt = (parseFloat(Billtdsamount) * parseFloat(exchangerate));
							nlapiLogExecution('DEBUG', 'Bill ', " i_INR_amt ->  " + i_INR_amt)
							
							i_INR_amt = applyTdsRoundMethod(tdsRoundMethod,i_INR_amt)
							tdsamt = i_INR_amt;
							
							var Billamount = billRec.getLineItemValue('expense', 'custcol_baseamount', k);
							var i_inr_bill_amt = (parseFloat(Billamount) * parseFloat(exchangerate));
							nlapiLogExecution('DEBUG', 'Bill ', " i_inr_bill_amt ->  " + i_inr_bill_amt)
							
							baseamt = i_inr_bill_amt;
						}
						else 
						{
							tdsamt = billRec.getLineItemValue('expense', 'custcol_tdsamount', k);
							baseamt = billRec.getLineItemValue('expense', 'custcol_baseamount', k);
						}
	                    
	                    itamt = billRec.getLineItemValue('expense', 'custcol_it', k);
	                    scamt = billRec.getLineItemValue('expense', 'custcol_sc', k);
	                    itscamt = billRec.getLineItemValue('expense', 'custcol_itsc', k);
	                    ecamt = billRec.getLineItemValue('expense', 'custcol_ecess', k);
	                   
	                    
	                    var filters = new Array();
	                    var column = new Array();
	                    
	                    filters.push(new nlobjSearchFilter('internalid', null, 'is', tdstype));
	                    
	                    column.push(new nlobjSearchColumn('custrecord_tdsaccount'));
	                    column.push(new nlobjSearchColumn('name'));
	                    column.push(new nlobjSearchColumn('custrecord_section'));
						column.push(new nlobjSearchColumn('custrecord_tds_section'));
						column.push(new nlobjSearchColumn('custrecord_assessecode'));
	                    column.push(new nlobjSearchColumn('custrecord_netper'));
						column.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
	                    
	                    var results = nlapiSearchRecord('customrecord_tdsmaster', null, filters, column);
	                    for (var i = 0; results != null && i < results.length; i++) 	
						{
	                    
	                        tdsaccountArray[p] = results[i].getValue('custrecord_tdsaccount');
	                        tdsnameArray[p] = tdstype;
	                        tdssectionArray[p] = results[i].getValue('custrecord_section');
							tdssection_listArray[p] = results[i].getValue('custrecord_tds_section');
							
							tdsasseesseecode[p] = results[i].getValue('custrecord_assessecode');
							
	                     	if (pan_number != '' && pan_number != undefined && pan_number != null) 
							{
								tdsrateArray[p] = results[i].getValue('custrecord_netper');
							}
							else
							{
								tdsrateArray[p] = results[i].getValue('custrecord_empty_pan_tdsper');
							}
	                        taxcodeArray[p] = taxcode;
	                        baseamtArray[p] = baseamt;
	                        tdsamountArray[p++] = tdsamt;
	                    }// END  for (var i = 0; results != null && i < results.length; i++)
						 
	                }// END if (tdsapply == 'T') 
					
	            }// END  for (var k = 1; k <= rowcount; k++) 
	            
	        }// END  if (rowcount >= 1) 	
	        
	        //****************************************End:If Expense item is selected********************************************************************   
	        
	        //****************************************Begin:If item is selected***************************************************************************     
	        var rowcount = billRec.getLineItemCount('item');
	        var tds_itemacc = new Array();
	        var tds_itemamt = new Array();
	        var m = 1
	        if (rowcount >= 1)	
			 {
	            for (var k = 1; k <= rowcount; k++) 	
				{
	                tdstype = billRec.getLineItemValue('item', 'custcol_tdstype', k);
	                tdsapply = billRec.getLineItemValue('item', 'custcol_tdsapply', k);
	                
	                if (tdsapply == 'T') 	
					{
	                    taxcode = billRec.getLineItemValue('item', 'taxcode', k);
	                  
	                    itamt = billRec.getLineItemValue('item', 'custcol_it', k);
	                    scamt = billRec.getLineItemValue('item', 'custcol_sc', k);
	                    itscamt = billRec.getLineItemValue('item', 'custcol_itsc', k);
	                    ecamt = billRec.getLineItemValue('item', 'custcol_ecess', k);
	                  
						
						if (INRcurrencyID != currency) 
						{
							var Billtdsamount = billRec.getLineItemValue('item', 'custcol_tdsamount', k);
							var i_INR_amt = (parseFloat(Billtdsamount) * parseFloat(exchangerate));
							nlapiLogExecution('DEBUG', 'Bill ', " i_INR_amt ->  " + i_INR_amt)
							
							i_INR_amt = applyTdsRoundMethod(tdsRoundMethod,i_INR_amt)
							tdsamt = i_INR_amt;
							
							var Billamount = billRec.getLineItemValue('item', 'custcol_baseamount', k);
							var i_inr_bill_amt = (parseFloat(Billamount) * parseFloat(exchangerate));
							nlapiLogExecution('DEBUG', 'Bill ', " i_inr_bill_amt ->  " + i_inr_bill_amt)
							
							baseamt = i_inr_bill_amt;
						}
						else 
						{
							tdsamt = billRec.getLineItemValue('item', 'custcol_tdsamount', k);
							baseamt = billRec.getLineItemValue('item', 'custcol_baseamount', k);
						}
	                    
	                    var filters1 = new Array();
	                    var column1 = new Array();
	                    
	                    filters1.push(new nlobjSearchFilter('internalid', null, 'is', tdstype));
	                    
	                    column1.push(new nlobjSearchColumn('custrecord_tdsaccount'));
	                    column1.push(new nlobjSearchColumn('name'));
	                    column1.push(new nlobjSearchColumn('custrecord_section'));
						column1.push(new nlobjSearchColumn('custrecord_tds_section'));
						column1.push(new nlobjSearchColumn('custrecord_assessecode'));
	                    column1.push(new nlobjSearchColumn('custrecord_netper'));
	                    column1.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
						
	                    var results = nlapiSearchRecord('customrecord_tdsmaster', null, filters1, column1);
	                    
	                    nlapiLogExecution('DEBUG', 'Bill ', " length " + results.length)
	                    for (var i = 0; results != null && i < results.length; i++) 	
						{
	                        tdsaccountArray[p] = results[i].getValue('custrecord_tdsaccount');
	                        tdsnameArray[p] = tdstype;
	                        tdssectionArray[p] = results[i].getValue('custrecord_section');
							
							tdssection_listArray[p] = results[i].getValue('custrecord_tds_section');
							
							tdsasseesseecode[p] = results[i].getValue('custrecord_assessecode');
							
	                     	if (pan_number != '' && pan_number != undefined && pan_number != null) 
							{
								tdsrateArray[p] = results[i].getValue('custrecord_netper');
							}
							else
							{
								tdsrateArray[p] = results[i].getValue('custrecord_empty_pan_tdsper');
							}
							
	                        taxcodeArray[p] = taxcode;
	                        baseamtArray[p] = baseamt;
	                        tdsamountArray[p++] = tdsamt;
	                    }// END  for (var i = 0; results != null && i < results.length; i++) 
							
	                }// END  if (tdsapply == 'T') 	
					
	            }//  END for (var k = 1; k <= rowcount; k++) 
	            
	        
	        }// END if (rowcount >= 1)	
	        
	        
	        //****************************************End:If item is selected***************************************************************************
	        
	        //****************************************Begin:Code to Add New TDS Bill Relation record***************************************************
	        
	        if (type == 'create') 	
			{
	        
	            for (var m = 1; m < p; m++)	
				 {
	            
	                var tdsbillRelationrecord = nlapiCreateRecord('customrecord_tdsbillrelation');
	                tdsbillRelationrecord.setFieldValue('custrecord_billbillamount', baseamtArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billvendorrel', vendorcode);
	                tdsbillRelationrecord.setFieldValue('custrecord_billbillno', billId);
	                tdsbillRelationrecord.setFieldValue('custrecord_iit_tds_posting_period', postingPeriod);
	               if (recType == 'vendorcredit') 
					{
						tdsbillRelationrecord.setFieldValue('custrecord_billtdspayable',(-(tdsamountArray[m])));
						tdsbillRelationrecord.setFieldValue('custrecord_billtdsamount', (-(tdsamountArray[m])));
					}
					else
					{
						tdsbillRelationrecord.setFieldValue('custrecord_billtdspayable',tdsamountArray[m]);
						tdsbillRelationrecord.setFieldValue('custrecord_billtdsamount',tdsamountArray[m]);
					}
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdsdeduct', tdsDeduct);
	                tdsbillRelationrecord.setFieldValue('custrecord_billdate', tranDate);
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdstype', tdsnameArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdssection', tdssectionArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdsaccount', tdsaccountArray[m]);
	               
	                tdsbillRelationrecord.setFieldValue('custrecord_billstatus', 'Open');
	                tdsbillRelationrecord.setFieldValue('custrecord_tdsperrate', tdsrateArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billsubsidiary', subsidiary);
					
					tdsbillRelationrecord.setFieldValue('custrecord_tds_transaction_type',recType);
	                tdsbillRelationrecord.setFieldValue('custrecord_bill_tds_section',tdssection_listArray[m]);
					tdsbillRelationrecord.setFieldValue('custrecord_billassessee_code',tdsasseesseecode[m]);
	                
	                tdsbillRelationrecordId = nlapiSubmitRecord(tdsbillRelationrecord, true);
	                nlapiLogExecution('DEBUG', 'Bill ', "Created TDS Bill Relation Record" + tdsbillRelationrecordId);
	            }// END  for (var m = 1; m < p; m++)	
	            
	        }// END  if (type == 'create') 	
	        
	        //******************************************End:Code to Add New TDS Bill Relation record*****************************************************
	        
	        //******************************************Begin: code for if record is updated*************************************************************
	        
	        if (type == 'edit') 	
			{
	        
	            nlapiLogExecution('DEBUG', 'Bill ', " type " + type);
	            
	            //End:Get the current Bill Id & its Related Credit Id then update that vendor credit
	            
	            //Begin:Code to check related Bill Relation record
	            var billarray = new Array()
	            var filters3 = new Array();
	            var column3 = new Array();
	            filters3.push(new nlobjSearchFilter('custrecord_billbillno', null, 'is', billId));
	            column3.push(new nlobjSearchColumn('internalid'))
	            var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, filters3, column3);
	            if(results)
				{
				nlapiLogExecution('DEBUG', 'Bill ', "results  " + results.length);
	            }
				for (var i = 0; results != null && i < results.length; i++) 	
				{
	                var relationno = results[i].getValue('internalid')
	                
	                var record = nlapiLoadRecord('customrecord_tdsbillrelation', relationno);
	                record.setFieldValue('isinactive', 'T')
	                record.setFieldValue('custrecord_bill_status', 'Inactive')
	                var updatedId = nlapiSubmitRecord(record, true);
	                nlapiLogExecution('DEBUG', 'Bill ', "Inactivated Bill Relation Record ID  " + updatedId);
	            }// END  for (var i = 0; results != null && i < results.length; i++) 
	            //End:Code to check related Bill Relation record
	            
	            
	            //Begin:Code For Create New Bill relation Record
	            for (var m = 1; m < p; m++) 	
				{
	            
	                var tdsbillRelationrecord = nlapiCreateRecord('customrecord_tdsbillrelation');
	                tdsbillRelationrecord.setFieldValue('custrecord_billbillamount', baseamtArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billvendorrel', vendorcode);
	                tdsbillRelationrecord.setFieldValue('custrecord_billbillno', billId);
	                tdsbillRelationrecord.setFieldValue('custrecord_iit_tds_posting_period', postingPeriod);
					if (recType == 'vendorcredit') 
					{
						tdsbillRelationrecord.setFieldValue('custrecord_billtdspayable',(-(tdsamountArray[m])));
						tdsbillRelationrecord.setFieldValue('custrecord_billtdsamount', (-(tdsamountArray[m])));
					}
					else
					{
						tdsbillRelationrecord.setFieldValue('custrecord_billtdspayable', tdsamountArray[m]);
						tdsbillRelationrecord.setFieldValue('custrecord_billtdsamount',tdsamountArray[m]);
					}
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdsdeduct', tdsDeduct);
	                tdsbillRelationrecord.setFieldValue('custrecord_billdate', tranDate);
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdstype', tdsnameArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdssection', tdssectionArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billtdsaccount', tdsaccountArray[m]);
	               
	                tdsbillRelationrecord.setFieldValue('custrecord_billstatus', 'Open');
	                tdsbillRelationrecord.setFieldValue('custrecord_tdsperrate', tdsrateArray[m]);
	                tdsbillRelationrecord.setFieldValue('custrecord_billsubsidiary', subsidiary);
					tdsbillRelationrecord.setFieldValue('custrecord_tds_transaction_type',recType);
	                tdsbillRelationrecord.setFieldValue('custrecord_bill_tds_section',tdssection_listArray[m]);
					tdsbillRelationrecord.setFieldValue('custrecord_billassessee_code',tdsasseesseecode[m]);
	                
	                var tdsbillRelationrecordId = nlapiSubmitRecord(tdsbillRelationrecord, true);
	                nlapiLogExecution('DEBUG', 'Bill ', "Udated TDS Bill Relation Record" + tdsbillRelationrecordId);
	                
	            }// END  for (var m = 1; m < p; m++) 	
	            
	        }// END  if (type == 'edit') 	
	        
	        //******************************************END: code for if record is updated********************************************
	    
	    }//END if (a_subisidiary[y] == i_subsidiary) 
		}
		
		
	}// afterSubmitRecord(type)	
	
	
	
	// == FUNCTION FOR SEARCHING SUBSIDIARY GOLBAL PARAMETER ==
	function SearchGlobalParameter()			
	{
	
	    var a_filters = new Array();
	    var a_column = new Array();
	    var i_globalRecId = 0;
	    
	    a_column.push(new nlobjSearchColumn('internalid'));
	    
	    var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
	    
	    if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 	
		{
	        for (var i = 0; i < s_serchResult.length; i++) 	
			{
	            i_globalRecId = s_serchResult[0].getValue('internalid');
	            nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
	            
	        }
	        
	    }
	    
	    
	    return i_globalRecId;
	}
	function applyTdsRoundMethod(tdsRoundMethod,tdsAmount)
{
	var roundedtdsAmount = tdsAmount;
	
	if(tdsRoundMethod == 2)
	{
		roundedtdsAmount = Math.round(tdsAmount)
	}
	if(tdsRoundMethod == 3)
	{
		roundedtdsAmount = Math.round(tdsAmount/10)*10;
	}
	if(tdsRoundMethod == 4)
	{
		roundedtdsAmount = Math.round(tdsAmount/100)*100;
	}
	
	return roundedtdsAmount;
}
