function getPmMailTemplate(provisionRec) {
	var htmltext = '';
	htmltext += '<p>Dear All,</p>';

	htmltext += '<p>The monthly accounts closing activities for the P&L reporting of <b>February 16</b> '
	        + 'is planned on <b>29th Feb</b> 16 by the finance team. <b>Timesheet compliance by all resources,'
	        + ' in NetSuite</b>, is the key factor for the revenue recognition and cost allocation in the P&L.'
	        + ' Hence all  Project Managers  and Timesheet Approvers are requested to take responsibility'
	        + ' for 100% timesheet compliance of the project teams and <b>approve the timecards till'
	        + ' 26th February 16 in NetSuite, by 7.00 AM (IST) of 29th February 16.</b></p>';

	htmltext += '<p>Since the month-end falls in mid-week, the system will estimate the'
	        + ' revenue for the last 1 day of Feb 16 as per the project allocation.</p>';

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Nikhil Motiani</p>';

	return {
	    MailBody : htmltext,
	    MailSubject : "P&L Report February 16 - Timesheet Compliance"
	};
}

function getFinanceNotificationTemplate(provisionRec) {
	var htmltext = '';
	htmltext += '<p>Dear All,</p>';

	htmltext += '<p>This is to inform you that because of the month end activity,'
	        + ' a mail is scheduled to be delivered to all the employees on.</p>';

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    MailBody : htmltext,
	    MailSubject : "Monthly End Activity Mail Scheduled"
	};
}

function getTimesheetBlockMailTemplate(employeeFirstName) {
	var htmltext = '';
	htmltext += '<p>Dear ' + employeeFirstName + ',</p>';

	htmltext += '<p>This is to inform you that the timesheets have been blocked for few'
	        + ' hours because of month end activity. Once the process is completed'
	        + ' will communicate the same to you. </p>';
	htmltext += '<p>Inconvenience regretted !!</p>';

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    MailBody : htmltext,
	    MailSubject : "Timesheet Blocked For Month End Activities"
	};
}

function getTimesheetUnblockedMailTemplate(employeeFirstName) {
	var htmltext = '';
	htmltext += '<p>Dear ' + employeeFirstName + ',</p>';

	htmltext += '<p>This is to inform you that the month end activities have been completed ';
	htmltext += 'and the timesheets are available now.</p>';
	htmltext += '<p>Thanks for your patience and co-operation</p>';

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    MailBody : htmltext,
	    MailSubject : "Timesheet Unblocked For Month End Activities"
	};
}