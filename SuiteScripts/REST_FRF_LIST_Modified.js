/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Sep 2019     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {

    return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
var rrfArr = [];

function postRESTlet(dataIn) {
    try {
        var response = new Response();
        var currentDateAndTime = sysDate();
        var receivedDate = dataIn.lastUpdatedTimestamp;
        var endDate = dataIn.endTimeStamp;
        nlapiLogExecution('DEBUG', 'End Date as received', 'receivedDate...' + endDate);
        nlapiLogExecution('DEBUG', 'Current Date as received', 'receivedDate...' + receivedDate);
        nlapiLogExecution('DEBUG', 'Current Date as setting', 'currentDateAndTime...' + currentDateAndTime);
        if (endDate && receivedDate) {
            var customrecord_taleo_external_hireSearch = searchRecord("customrecord_taleo_external_hire", null,
                [
                    ["systemnotes.date", "within", receivedDate, endDate]
                ],
                [
                    new nlobjSearchColumn("internalid")
                ]
            );
            nlapiLogExecution('Debug', 'customrecord_taleo_external_hireSearch length ', customrecord_taleo_external_hireSearch.length);
            if (customrecord_taleo_external_hireSearch) {

                for (var m = 0; m < customrecord_taleo_external_hireSearch.length; m++) {
                    var rrfID = customrecord_taleo_external_hireSearch[m].getId()
                    rrfArr.push(rrfID)
                }
            }
            var filters = new Array();
            if (rrfArr.length > 0) {
                filters = [
                    ["isinactive", "is", "F"],
                    "AND",
                    [
                        ["lastmodified", "within", receivedDate, endDate],
                        "OR",
                        ["custrecord_frf_details_rrf_number.internalid", "anyof", rrfArr]
                    ]
                ];
            } else {
                filters = [
                    ["isinactive", "is", "F"],
                    "AND",
                    [
                        ["lastmodified", "within", receivedDate, endDate]
                    ]
                ];
            }
            nlapiLogExecution('debug', 'filters ', filters);
        } else if ((endDate == '' || endDate == null) && receivedDate) {
            var customrecord_taleo_external_hireSearch = searchRecord("customrecord_taleo_external_hire", null,
                [
                    ["systemnotes.date", "within", receivedDate, currentDateAndTime]
                ],
                [
                    new nlobjSearchColumn("internalid")
                ]
            );
            nlapiLogExecution('Debug', 'customrecord_taleo_external_hireSearch length ', customrecord_taleo_external_hireSearch.length);
            if (customrecord_taleo_external_hireSearch) {

                for (var m = 0; m < customrecord_taleo_external_hireSearch.length; m++) {
                    var rrfID = customrecord_taleo_external_hireSearch[m].getId()
                    rrfArr.push(rrfID)
                }
            }
            var filters = new Array();
            if (rrfArr.length > 0) {
                filters = [
                    ["isinactive", "is", "F"],
                    "AND",
                    [
                        ["lastmodified", "within", receivedDate, currentDateAndTime],
                        "OR",
                        ["custrecord_frf_details_rrf_number.internalid", "anyof", rrfArr]
                    ]
                ];
            } else {
                filters = [
                    ["isinactive", "is", "F"],
                    "AND",
                    [
                        ["lastmodified", "within", receivedDate, currentDateAndTime]
                    ]
                ];
            }
            nlapiLogExecution('debug', 'filters ', filters);
        } else if ((receivedDate == '' || receivedDate == null) && endDate) {
            var customrecord_taleo_external_hireSearch = searchRecord("customrecord_taleo_external_hire", null,
                [
                    ["systemnotes.date", "onorbefore", endDate]
                ],
                [
                    new nlobjSearchColumn("internalid")
                ]
            );
            nlapiLogExecution('Debug', 'customrecord_taleo_external_hireSearch length ', customrecord_taleo_external_hireSearch.length);
            if (customrecord_taleo_external_hireSearch) {

                for (var m = 0; m < customrecord_taleo_external_hireSearch.length; m++) {
                    var rrfID = customrecord_taleo_external_hireSearch[m].getId()
                    rrfArr.push(rrfID)
                }
            }
            var filters = new Array();
            if (rrfArr.length > 0) {
                filters = [
                    ["isinactive", "is", "F"],
                    "AND",
                    [
                        ["lastmodified", "onorbefore", endDate],
                        "OR",
                        ["custrecord_frf_details_rrf_number.internalid", "anyof", rrfArr]
                    ]
                ];
            } else {
                filters = [
                    ["isinactive", "is", "F"],
                    "AND",
                    [
                        ["lastmodified", "onorbefore", endDate]
                    ]
                ];
            }
            nlapiLogExecution('debug', 'filters ', filters);
        } else {
            var filters = new Array();
            filters = [
                ["isinactive", "is", "F"]
            ];
        }
        var onHold = GetOnholdStatusOfAllRRF();
        var reduceOnHoldID = meargeDate(onHold);
        var searchResults = searchRecord("customrecord_frf_details", null, filters,
            [
                new nlobjSearchColumn("internalid"),
                new nlobjSearchColumn("custrecord_frf_details_frf_number"),
                new nlobjSearchColumn("custrecord_frf_details_status"),
                new nlobjSearchColumn("custrecord_frf_details_project"),
                new nlobjSearchColumn("custrecord_frf_details_opp_id"),
                new nlobjSearchColumn("custrecord_frf_details_status_flag"),
                new nlobjSearchColumn("custrecord_frf_details_ns_rrf_number"),
                new nlobjSearchColumn("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("internalid", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_frf_details_external_hire"),
                new nlobjSearchColumn("custrecord_frf_details_billiable"),
                new nlobjSearchColumn("custrecord_frf_details_role"), //custrecord_rrf_taleo
                new nlobjSearchColumn("formulatext").setFormula("CASE WHEN {custrecord_frf_details_opp_id}  IS NOT NULL THEN {custrecord_frf_details_opp_id.custrecord_projection_status_sfdc}  ELSE 'Booked' END"),
                new nlobjSearchColumn("internalid", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                new nlobjSearchColumn("custrecord_frf_details_res_location"),
                new nlobjSearchColumn("custrecord_frf_details_res_practice"),
                new nlobjSearchColumn("internalid", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null),
                new nlobjSearchColumn("internalid", "CUSTRECORD_FRF_DETAILS_PROJECT", null),
                new nlobjSearchColumn("custrecord_frf_details_primary_skills"),
                new nlobjSearchColumn("custrecord_frf_details_start_date"),
                new nlobjSearchColumn("custrecord_frf_details_position_type"),
                new nlobjSearchColumn("custrecord_frf_details_selected_emp"),
                new nlobjSearchColumn("custrecord_rrf_taleo"), //custrecord_frf_details_availabledate custrecord_frf_type
                new nlobjSearchColumn("custrecord_frf_details_availabledate"),
                new nlobjSearchColumn("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_frf_details_bill_rate"), //created
                new nlobjSearchColumn("custrecord_frf_details_created_date"),
                new nlobjSearchColumn("custrecord_frf_details_created_by"),
                new nlobjSearchColumn("custrecord_frf_type"),
                new nlobjSearchColumn("CUSTRECORD_FRF_DETAILS_RRF_NUMBER"), // Nihal
                new nlobjSearchColumn("custrecord_frf_details_job_descriptions"),
                new nlobjSearchColumn("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_interview_stage_second_lvl_c", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_taleo_interview_conducted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_taleo_external_selected_can", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_taleo_emp_joining_date", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                new nlobjSearchColumn("custrecord_frf_desired_start_date"),

                new nlobjSearchColumn("custrecord_frf_details_allocated_date"), //prabhat gupta NIS-1297 6/3/2020

                new nlobjSearchColumn("custrecord_frf_details_allocation"),
                new nlobjSearchColumn("custrecord_frf_details_end_date"),
                new nlobjSearchColumn("custrecord_opportunity_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                new nlobjSearchColumn("custrecord_stage_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                new nlobjSearchColumn("custrecord_frf_details_softlock_date"),
                new nlobjSearchColumn("custrecord_frf_details_desoftlock_date"), //prabhat gupta 07/07/2020
                new nlobjSearchColumn("custrecord_frf_details_closed_date"),
                new nlobjSearchColumn("custrecord_rrf_filled_date", "custrecord_frf_details_rrf_number", null),
                new nlobjSearchColumn("custrecord_rrf_approve_date", "custrecord_frf_details_rrf_number", null), //prabhat gupta NIS-1317 13/05/2020
                new nlobjSearchColumn("custrecord_frf_details_replacement_emp"), //prabhat gupta
                new nlobjSearchColumn("custrecord_frf_details_fitment_type"), //prabhat gupta 04/09/2020 NIS-1723
                new nlobjSearchColumn("custrecord_frf_details_level"), //prabhat gupta 07/10/2020 NIS-1755

                new nlobjSearchColumn("custrecord_frf_details_backup_require"),
                new nlobjSearchColumn("custrecord_frf_details_critical_role"),
                new nlobjSearchColumn("custrecord_frf_details_cust_interview"),
                new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"),
                new nlobjSearchColumn("custrecord_frf_details_edu_lvl"),
                new nlobjSearchColumn("custrecord_frf_details_edu_program"),
                new nlobjSearchColumn("custrecord_frf_details_first_interviewer"),
                new nlobjSearchColumn("custrecord_frf_details_instruction_team"),
                new nlobjSearchColumn("custrecord_frf_details_job_title"),
                new nlobjSearchColumn("custrecord_frf_details_reason_external"),
                new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"),
                new nlobjSearchColumn("custrecord_frf_details_red_date"),
                new nlobjSearchColumn("custrecord_taleo_ext_offer_released", "custrecord_frf_details_rrf_number", null),
                new nlobjSearchColumn("custrecord_frf_details_special_req"),
                new nlobjSearchColumn("custrecord_frf_details_last_updatedby"),
                new nlobjSearchColumn("custrecord_frf_details_bo_comments"),
                new nlobjSearchColumn("custrecord_frf_details_talep_created"),
                new nlobjSearchColumn("custrecord_frf_details_ori_start_date"),
                new nlobjSearchColumn("custrecord_frf_details_ori_end_date"),
                new nlobjSearchColumn("custrecord_frf_details_skill_class"),
                new nlobjSearchColumn("custrecord_frf_details_hire_type"),
                new nlobjSearchColumn("custrecord_frf_details_cancelled_date"),
                new nlobjSearchColumn("custrecord_frf_details_cancellation_reas"),
                new nlobjSearchColumn("custrecord_frf_details_rrf_cancelled_by"),
				new nlobjSearchColumn("custrecord_offered_emp_email", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            ]
        );
        var JSON = {};
        var dataRow = [];

        if (searchResults) {

            nlapiLogExecution('Debug', 'searchResults length ', searchResults.length);
            for (var i = 0; i < searchResults.length; i++) { //searchResults.length
                var data = {};
                var a_columns = searchResults[i].getAllColumns();
                var s_rrfNumber = '';

                if (searchResults[i].getValue("custrecord_rrf_taleo"))
                    s_rrfNumber = searchResults[i].getValue("custrecord_rrf_taleo");
                else
                    s_rrfNumber = searchResults[i].getValue("custrecord_frf_details_ns_rrf_number");

                data.internalId = {
                    "name": searchResults[i].getValue("internalid")
                };
                data.frfNumber = {
                    "name": searchResults[i].getValue("custrecord_frf_details_frf_number")
                };
                data.roleName = {
                    "name": searchResults[i].getText("custrecord_frf_details_role"),
                    "id": searchResults[i].getValue("custrecord_frf_details_role")
                };
                data.billable = {
                    "name": searchResults[i].getValue("custrecord_frf_details_billiable")
                };
                data.revenueStatus = {
                    "name": searchResults[i].getValue(a_columns[12])
                }; // Revenue Status we need to check by get all columns
                data.status = {
                    "name": searchResults[i].getText("custrecord_frf_details_status_flag"),
                    "id": searchResults[i].getValue("custrecord_frf_details_status_flag")
                };
                data.rrfNumber = {
                    "name": s_rrfNumber,
                    "internalId": searchResults[i].getValue("internalid", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                    "status": searchResults[i].getValue("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
                    "offerAccepted": searchResults[i].getValue("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.external = {
                    "name": searchResults[i].getValue("custrecord_frf_details_external_hire")
                };
                data.opportunityInternalId = {
                    "name": searchResults[i].getText("custrecord_frf_details_opp_id")
                };
                data.resourceLocation = {
                    "name": searchResults[i].getText("custrecord_frf_details_res_location"),
                    "id": searchResults[i].getValue("custrecord_frf_details_res_location")
                };
                data.practiceName = {
                    "name": searchResults[i].getText("custrecord_frf_details_res_practice"),
                    "id": searchResults[i].getValue("custrecord_frf_details_res_practice")
                };
                data.accountInternalId = {
                    "name": searchResults[i].getValue("internalid", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null)
                };
                data.projectInternalId = {
                    "name": searchResults[i].getValue("internalid", "CUSTRECORD_FRF_DETAILS_PROJECT", null)
                };
                data.skill = {
                    "name": searchResults[i].getText("custrecord_frf_details_primary_skills")
                };
                data.hireType = {
                    "name": searchResults[i].getText("custrecord_frf_details_hire_type"),
                    "id": searchResults[i].getValue("custrecord_frf_details_hire_type")
                };
                data.skillCategory = {
                    "name": searchResults[i].getValue("custrecord_frf_details_skill_class")
                };
                data.startDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_start_date")
                };
                data.endDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_end_date")
                };
                data.allocationPercentage = {
                    "name": searchResults[i].getValue("custrecord_frf_details_allocation")
                }; // //prabhat gupta NIS-1297 6/3/2020
                data.allocationDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_allocated_date") || ''
                };
                data.positionType = {
                    "name": searchResults[i].getText("custrecord_frf_details_position_type")
                };
                data.softlockedEmployeeName = {
                    "name": searchResults[i].getText("custrecord_frf_details_selected_emp"),
                    "id": searchResults[i].getValue("custrecord_frf_details_selected_emp")
                };
                data.availableDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_availabledate")
                };
                data.billRate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_bill_rate")
                };
                data.createdDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_created_date")
                };
                data.requestType = {
                    "name": searchResults[i].getText("custrecord_frf_type"),
                    "id": searchResults[i].getValue("custrecord_frf_type")
                };
                data.createdBy = {
                    "name": searchResults[i].getText("custrecord_frf_details_created_by"),
                    "id": searchResults[i].getValue("custrecord_frf_details_created_by")
                };

                //--------------------------------------------------------------------------------------------------------

                //prabhat gupta 04/09/2020 NIS-1723

                data.fitmentType = {
                    "name": searchResults[i].getText("custrecord_frf_details_fitment_type"),
                    "id": searchResults[i].getValue("custrecord_frf_details_fitment_type")
                };
				
				data.candidateEmail = searchResults[i].getValue("custrecord_offered_emp_email", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");


                //---------------------------------------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------

                //prabhat gupta 07/10/2020 NIS-1755

                data.level = {
                    "name": searchResults[i].getText("custrecord_frf_details_level"),
                    "id": searchResults[i].getValue("custrecord_frf_details_level")
                };

                var redDate = searchResults[i].getValue("custrecord_frf_details_red_date") || "";
                data.redDate = {
                    "name": redDate

                };


                //---------------------------------------------------------------------------------------------------------
                //------------------------------------------------------------------------------------------------------------------			//prabhat gupta
                data.replacingCandidate = {
                    "name": searchResults[i].getText("custrecord_frf_details_replacement_emp") || '',
                    "id": searchResults[i].getValue("custrecord_frf_details_replacement_emp") || ''
                };

                //--------------------------------------------------------------------------------------------------------------------	
                data.offerReleased = {
                    "name": searchResults[i].getValue("custrecord_taleo_ext_offer_released", "custrecord_frf_details_rrf_number", null)
                };

                data.backupofferd = {
                    "name": searchResults[i].getValue('custrecord_frf_details_backup_require')
                };

                data.criticalrole = {
                    "name": searchResults[i].getValue('custrecord_frf_details_critical_role')
                };

                data.custinterviewercheck = {
                    "name": searchResults[i].getValue('custrecord_frf_details_cust_interview')
                };

                data.custintervieweremail = {
                    "name": searchResults[i].getValue('custrecord_frf_details_cust_inter_email')
                };

                data.educationlevel = {
                    "name": searchResults[i].getText('custrecord_frf_details_edu_lvl'),
                    "id": searchResults[i].getValue('custrecord_frf_details_edu_lvl')
                };

                data.educationprogram = {
                    "name": searchResults[i].getText('custrecord_frf_details_edu_program'),
                    "id": searchResults[i].getValue('custrecord_frf_details_edu_program')
                };

                data.firstinterviewer = {
                    "name": searchResults[i].getValue('custrecord_frf_details_first_interviewer')
                };

                data.instruction = {
                    "name": searchResults[i].getValue('custrecord_frf_details_instruction_team')
                };


                data.jobtitle = {
                    "name": searchResults[i].getValue('custrecord_frf_details_job_title')
                };

                data.reasonforexthire = {
                    "name": searchResults[i].getValue('custrecord_frf_details_reason_external')
                };

                data.secondinterviewer = {
                    "name": searchResults[i].getValue('custrecord_frf_details_sec_interviewer')
                };
                data.splRequirement = {
                    "name": searchResults[i].getValue("custrecord_frf_details_special_req")
                };
                data.lastUpdatedBy = {
                    "name": searchResults[i].getText("custrecord_frf_details_last_updatedby"),
                    "id": searchResults[i].getValue("custrecord_frf_details_last_updatedby")
                };
                data.boComments = {
                    "name": searchResults[i].getText("custrecord_frf_details_bo_comments")
                };
                data.isTaleoRecordCreated = {
                    "name": searchResults[i].getValue("custrecord_frf_details_talep_created")
                };
                data.originalStartDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_ori_start_date")
                };
                data.originalendDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_ori_end_date")
                };
                data.jobDescription = searchResults[i].getValue("custrecord_frf_details_job_descriptions")
				
				
                var OFFER_ACCEPTED_TOTAL = searchResults[i].getValue("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                var STATUS = searchResults[i].getValue("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                if (OFFER_ACCEPTED_TOTAL > 0 || STATUS == "Filled") {
                    data.selectedCandidate = searchResults[i].getValue("custrecord_taleo_external_selected_can", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                    data.employeeJoiningDate = searchResults[i].getValue("custrecord_taleo_emp_joining_date", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                }
                if (searchResults[i].getValue("CUSTRECORD_FRF_DETAILS_RRF_NUMBER")) {
                    var rrfNumber = searchResults[i].getValue("CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                    data.OnHoldDetails = reduceOnHoldID[rrfNumber] ? reduceOnHoldID[rrfNumber] : "";
                } else {
                    data.OnHoldDetails = "";
                }
                data.secondLevelInterview = searchResults[i].getValue("custrecord_interview_stage_second_lvl_c", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                data.customerInterviewConducted = searchResults[i].getValue("custrecord_taleo_interview_conducted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER");
                data.desiredStartDate = searchResults[i].getValue("custrecord_frf_desired_start_date");
                data.OppID = searchResults[i].getValue("custrecord_opportunity_id_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null);
                data.OpportunityStage = {
                    "name": searchResults[i].getText("custrecord_stage_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null),
                    "id": searchResults[i].getValue("custrecord_stage_sfdc", "CUSTRECORD_FRF_DETAILS_OPP_ID", null)
                };
                data.frfSoftLockDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_softlock_date")
                };

                data.frfDeSoftLockDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_desoftlock_date")
                }; //prabhat gupta 07/07/2020

                data.frfClosedDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_closed_date")
                };
                data.rrfFilledDate = {
                    "name": searchResults[i].getValue("custrecord_rrf_filled_date", "custrecord_frf_details_rrf_number", null)
                };
                data.lastApprovedDate = {
                    "name": searchResults[i].getValue("custrecord_rrf_approve_date", "custrecord_frf_details_rrf_number", null)
                }; // prabhat gupta NIS-1317 13/05/2020
                data.frfCancellationDate = {
                    "name": searchResults[i].getValue("custrecord_frf_details_cancelled_date")
                };
                data.frfCancellationBy = {
                    "id": searchResults[i].getValue("custrecord_frf_details_rrf_cancelled_by"),
                    "name": searchResults[i].getText("custrecord_frf_details_rrf_cancelled_by")
                };
                data.frfCancellationReason = {
                    "id": searchResults[i].getValue("custrecord_frf_details_cancellation_reas"),
                    "name": searchResults[i].getText("custrecord_frf_details_cancellation_reas")
                };
                dataRow.push(data);
            }
            //dataRow.push(JSON);
        }

        response.timeStamp = currentDateAndTime;
        response.data = dataRow;
        response.status = true;
        //	nlapiLogExecution('debug', 'response',JSON.stringify(response));
    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.timeStamp = '';
        response.data = err;
        response.status = false;
    }
    return response;

}
//validate blank entries
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
//Get current date


//Returns timestamp
function timestamp() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    //str += hours + ":" + minutes + ":" + seconds + " ";
    str += hours + ":" + minutes + " ";
    return str + meridian;
}

function Response() {
    this.status = "";
    this.timeStamp = "";
    this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (savedSearch) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
    if (dateObj) {
        var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
        return nsFormatDate;
    }
    return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
    var str = num.toString();
    while (str.length < len) {
        str = '0' + str;
    }
    return str;
}

function GetOnholdStatusOfAllRRF() {
    var filter = [];
    var column = [];
    /*	filter.push(new nlobjSearchFilter('systemnotes.newvalue',null,'is',"On hold")); // Added by nihal
    	filter.push(new nlobjSearchFilter('systemnotes.oldvalue',null,'is',"On hold")); // Added by nihal
    	filter.push(new nlobjSearchFilter('isinactive',null,'is',"F")); // Added by nihal
    	column.push(new nlobjSearchColumn("oldvalue","systemNotes",null));
    	column.push(new nlobjSearchColumn("newvalue","systemNotes",null));
    	column.push(new nlobjSearchColumn("date","systemNotes",null));
    	column.push(new nlobjSearchColumn("date","systemNotes",null));
    	var customrecord_taleo_external_hireSearch = nlapiSearchRecord("customrecord_taleo_external_hire",null,filter,column);*/
    /*filter = [
       [["systemnotes.newvalue","is","On hold"],"OR",["systemnotes.oldvalue","is","On hold"]], 
       "AND", 
       ["isinactive","is","F"]
    ];*/
    filter = [
        ["systemnotes.newvalue", "contains", "On hold"],
        "OR",
        ["systemnotes.oldvalue", "contains", "On hold"],
        "AND",
        ["isinactive", "is", "F"]
    ];
    column = [
        new nlobjSearchColumn("oldvalue", "systemNotes", null),
        new nlobjSearchColumn("newvalue", "systemNotes", null),
        new nlobjSearchColumn("date", "systemNotes", null),
        new nlobjSearchColumn("internalid").setSort(false)
    ]
    /*var customrecord_taleo_external_hireSearch = nlapiSearchRecord("customrecord_taleo_external_hire",null,filter,column);
var completeResultSet = customrecord_taleo_external_hireSearch;
	
		while(customrecord_taleo_external_hireSearch.length == 1000)
		{
			var lastId = customrecord_taleo_external_hireSearch[999].getId();
			filter.push(new nlobjSearchFilter('internalid',null,'greaterthan',lastId));
			customrecord_taleo_external_hireSearch = nlapiSearchRecord("customrecord_taleo_external_hire",null,filter,column);
			completeResultSet = completeResultSet.concat(customrecord_taleo_external_hireSearch); 
		} 
		customrecord_taleo_external_hireSearch = completeResultSet;*/
    var customrecord_taleo_external_hireSearch = nlapiCreateSearch('customrecord_taleo_external_hire', filter, column) // nlapiSearchRecord("customrecord_taleo_external_hire",null,filter,column);
    resultSet = customrecord_taleo_external_hireSearch.runSearch();

    // iterate through the search and get all data 1000 at a time
    var searchResultCount = 0;
    var resultSlice = null;
    var searchResult = [];

    do {
        resultSlice = resultSet.getResults(searchResultCount,
            searchResultCount + 1000);

        if (resultSlice) {

            resultSlice.forEach(function(result) {

                searchResult.push(result);
                searchResultCount++;
            });
        }
    } while ((resultSlice) && resultSlice.length >= 1000);

    customrecord_taleo_external_hireSearch = searchResult;
    var returnData = [];
    if (customrecord_taleo_external_hireSearch) {
        for (var t = 0; t < customrecord_taleo_external_hireSearch.length; t++) {
            returnData.push({
                "internalId": customrecord_taleo_external_hireSearch[t].getId(),
                "oldStatus": customrecord_taleo_external_hireSearch[t].getValue("oldvalue", "systemNotes", null),
                "newStatus": customrecord_taleo_external_hireSearch[t].getValue("newvalue", "systemNotes", null),
                "Date": customrecord_taleo_external_hireSearch[t].getValue("date", "systemNotes", null)
            });
        }
    }
    return returnData;

}

function meargeDate(all_data) {
    return groupResult = all_data.reduce(function(r, a) {
        r[a.internalId] = r[a.internalId] || [];
        r[a.internalId].push(a);
        return r;
    }, Object.create(null));
}