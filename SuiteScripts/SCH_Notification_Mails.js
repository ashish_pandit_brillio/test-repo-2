/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 09 Feb 2015 nitish.mishra
 * 
 */

// to send only to project managers
function getProjectManager() {
	var project_manager = [];

	var projectManagerSearch = nlapiSearchRecord('job', '609');

	projectManagerSearch.forEach(function(manager) {
		project_manager.push(manager.getValue('custentity_projectmanager',
		        null, 'group'));
	});

	nlapiLogExecution('debug', 'project manager count', project_manager.length);
	return project_manager;
}

// to send only to project managers
function getReportUsers() {
	var project_manager = [];

	var projectManagerSearch = nlapiSearchRecord(null, '782');

	projectManagerSearch.forEach(function(manager) {
		project_manager.push(manager.getValue('custrecord_rpl_employee'));
	});

	nlapiLogExecution('debug', 'project manager count', project_manager.length);
	return project_manager;
}

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {

	try {
		var context = nlapiGetContext();

		// var filtered_employees = getReportUsers();
		// filtered_employees.push('9673');

		// get all employees
		var employeeSearch = searchRecord('employee', null,
		        [
		                // Check 1
		               //new nlobjSearchFilter('internalid', null, 'anyof',
		                  //      [ '8871' ]),
		                // new nlobjSearchFilter('internalid', null, 'noneof',
		                // filtered_employees),
		                new nlobjSearchFilter('custentity_employee_inactive',
		                        null, 'is', 'F') ], [
		                new nlobjSearchColumn('firstname'),
		                new nlobjSearchColumn('email') ]);

		nlapiLogExecution('debug', 'total employee count ',
		        employeeSearch.length);

		// Check 2
		//return;

		if (isArrayNotEmpty(employeeSearch)) {
			nlapiLogExecution('debug', 'total emp', employeeSearch.length);
			var mailData = null, firstName = null, emailId = null, employeeId = null;

			for (var index = 0; index < employeeSearch.length; index++) {
				firstName = employeeSearch[index].getValue('firstname');
				emailId = employeeSearch[index].getValue('email');
				employeeId = employeeSearch[index].getId();
				yieldScript(context);

				if (isNotEmpty(firstName) && isNotEmpty(emailId)) {
					mailData = timeSheetNotificationMailTemplate(firstName);
					// nlapiLogExecution('debug', emailId);

					nlapiSendEmail('442', emailId, mailData.MailSubject,
					        mailData.MailBody, null, null, {
						        entity : employeeId
					        }, nlapiLoadFile(178115));

				} else {
					nlapiLogExecution('debug', 'failed sending',
					        employeeSearch[index].getId());
				}
			}

			nlapiLogExecution('debug', 'send emp', index);
		}

	} catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
	}
}

function timeSheetNotificationMailTemplate(firstName) {
	var htmltext = '';

	htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + firstName + ',</p>';	
	
	// -----------
	
	//htmltext += '<br />';
	htmltext += 'This email is with reference to new feature in Mobile app &ldquo;AD password reset&rdquo; which allows you to perform the following';
	htmltext += '<ol>';
	htmltext += '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Change your existing domain password</li>';
	htmltext += '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Reset your domain password</li>';
	htmltext += '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Forgot password</li>';
	htmltext += '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unlock your domain password</li>';
	htmltext += '</ol>';
//	htmltext += '&nbsp;<br />';
	htmltext += 'This feature has been configured to send OTP password to your personal email ID/mobile number.&nbsp; We have found that you have not updated either personal email ID/Mobile number or both in Fusion.<br />';
	htmltext += '&nbsp;<br />';
	htmltext += 'We are Launching the mobile app on 23<sup>rd</sup> May, kindly request you to update the above mentioned details in Fusion ASAP. &nbsp;<br />';
	htmltext += '<br />';
	htmltext += '<b>Login to Fusion->Click on About me->Personal information->Click on “edit” button. Attached file for your reference</b>';
	
	//------------------

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
	    MailBody : htmltext,
	    MailSubject : "Update mobile number and personal email id in Fusion"
	};
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}