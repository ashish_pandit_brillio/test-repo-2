/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var id = request.getParameter('id');
		var param = request.getParameter('param');
		if (id == 'Export') {

			var to_Date = request.getParameter('param');
			var fromdate;
			var lastDay = new Date(nlapiStringToDate(to_Date).getFullYear(), nlapiStringToDate(to_Date).getMonth() + 1, 0);
			var last_day = nlapiDateToString(lastDay,'mm/dd/yyyy');
			if(to_Date == last_day)
			{
				fromDate = nlapiStringToDate(to_Date).getDate();
				if(fromDate == 31)
				{
					fromDate = nlapiAddDays(nlapiStringToDate(to_Date) , -30);
				}
				else
				{
					fromDate = nlapiAddDays(nlapiStringToDate(to_Date), -29);	
				}
				fromDate = nlapiDateToString(fromDate,'mm/dd/yyyy');
				var arraytoCSV = _SearchData(fromDate, to_Date);
				var creat_csv_text = create_CSV(arraytoCSV);

				var fileName = 'Comity ' + lastDay + '.csv';
				var file = nlapiCreateFile(fileName, 'CSV', creat_csv_text);
				response.setContentType('CSV', fileName);
				response.write(file.getValue());
			}
			else
			{	
				var firstDay = new Date(nlapiStringToDate(param).getFullYear(),
						nlapiStringToDate(param).getMonth(), 1);
				var Last_Day = request.getParameter('param');
				firstDay = nlapiDateToString(firstDay, 'mm/dd/yyyy');
				//Last_Day = nlapiDateToString(Last_Day, 'mm/dd/yyyy');
				var arraytoCSV = _SearchData(firstDay, Last_Day);
				var creat_csv_text = create_CSV(arraytoCSV);

				var fileName = 'Comity ' + Last_Day + '.csv';
				var file = nlapiCreateFile(fileName, 'CSV', creat_csv_text);
				response.setContentType('CSV', fileName);
				response.write(file.getValue());

			}
		}	
		if (request.getMethod() == 'GET' && id != 'Export') //
		{

			var form = nlapiCreateForm('Salaried Rpt-From TS-Comity');
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :');
			var sublist1 = form.addSubList('record', 'list',
					'Comity-Report');
			form.setScript('customscript1451');

			sublist1.addField('custevent_employee', 'text', 'Employee');
			sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
			sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
			sublist1.addField('custevent_wkholy1', 'text', 'WK1 HOLIDAY');
			sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');

			sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
			sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
			sublist1.addField('custevent_wkholy2', 'text', 'WK2 HOLIDAY');
			sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');

			sublist1.addField('custevent_wkst3', 'text', 'WK3 ST');
			sublist1.addField('custevent_wkot3', 'text', 'WK3 OT');
			sublist1.addField('custevent_wkholy3', 'text', 'WK3 HOLIDAY');
			sublist1.addField('custevent_wk1timeoff3', 'text', 'WK3 TIMEOFF');

			sublist1.addField('custevent_wkst4', 'text', 'WK4 ST');
			sublist1.addField('custevent_wkot4', 'text', 'WK4 OT');
			sublist1.addField('custevent_wkholy4', 'text', 'WK4 HOLIDAY');
			sublist1.addField('custevent_wk1timeoff4', 'text', 'WK4 TIMEOFF');

			sublist1.addField('custevent_wkst5', 'text', 'WK5 ST');
			sublist1.addField('custevent_wkot5', 'text', 'WK5 OT');
			sublist1.addField('custevent_wkholy5', 'text', 'WK5 HOLIDAY');
			sublist1.addField('custevent_wk1timeoff5', 'text', 'WK5 TIMEOFF');

			sublist1.addField('custevent_tst', 'text', 'Total ST');

			sublist1.addField('custevent_tot', 'text', 'Total OT');

			sublist1.addField('custevent_ttmoff', 'text', 'Total Timeoff');

			sublist1.addField('custevent_tth', 'text', 'Total Holiday');

			sublist1.addField('custevent_tun', 'text', 'User Notes');

			sublist1.addField('custevent_aprvr', 'text', 'Timesheet Approver');
			
			sublist1.addField('custevent_proj','text','Project');
			
			sublist1.addField('custevent_projtype','text','Project Type');

			sublist1.addField('custevent_dptmt', 'text', 'Department');

			sublist1.addField('custevent_clnt', 'text', 'Customer');

			sublist1.addField('custevent_pm', 'text', 'Project Manager');

			form.addSubmitButton('Submit');
			response.writePage(form);

		} else {
			if (id != 'Export') {
				var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS-Comity');
				//form.setScript('customscript1451');// change
				var select = form.addField('custpage_startdate', 'Date',
						'Period End Date :').setDefaultValue(
						request.getParameter('custpage_startdate'));
				var sublist1 = form.addSubList('record', 'list',
						'Comity-Report');
				form.setScript('customscript1451');

				sublist1.addField('custevent_employee', 'text', 'Employee');
				sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
				sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
				sublist1.addField('custevent_wkholy1', 'text', 'WK1 HOLIDAY');
				sublist1.addField('custevent_wk1timeoff1', 'text',
						'WK1 TIMEOFF');

				sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
				sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
				sublist1.addField('custevent_wkholy2', 'text', 'WK2 HOLIDAY');
				sublist1.addField('custevent_wk1timeoff2', 'text',
						'WK2 TIMEOFF');

				sublist1.addField('custevent_wkst3', 'text', 'WK3 ST');
				sublist1.addField('custevent_wkot3', 'text', 'WK3 OT');
				sublist1.addField('custevent_wkholy3', 'text', 'WK3 HOLIDAY');
				sublist1.addField('custevent_wk1timeoff3', 'text',
						'WK3 TIMEOFF');

				sublist1.addField('custevent_wkst4', 'text', 'WK4 ST');
				sublist1.addField('custevent_wkot4', 'text', 'WK4 OT');
				sublist1.addField('custevent_wkholy4', 'text', 'WK4 HOLIDAY');
				sublist1.addField('custevent_wk1timeoff4', 'text',
						'WK4 TIMEOFF');

				sublist1.addField('custevent_wkst5', 'text', 'WK5 ST');
				sublist1.addField('custevent_wkot5', 'text', 'WK5 OT');
				sublist1.addField('custevent_wkholy5', 'text', 'WK5 HOLIDAY');
				sublist1.addField('custevent_wk1timeoff5', 'text',
						'WK5 TIMEOFF');

				sublist1.addField('custevent_tst', 'text', 'Total ST');

				sublist1.addField('custevent_tot', 'text', 'Total OT');

				sublist1.addField('custevent_ttmoff', 'text', 'Total Timeoff');

				sublist1.addField('custevent_tth', 'text', 'Total Holiday');

				sublist1.addField('custevent_tun', 'text', 'User Notes');

				sublist1.addField('custevent_aprvr', 'text',
						'Timesheet Approver');
						
				sublist1.addField('custevent_proj','text','Project');

				sublist1.addField('custevent_projtype','text','Project Type');				

				sublist1.addField('custevent_dptmt', 'text', 'Department');

				sublist1.addField('custevent_clnt', 'text', 'Customer');

				sublist1.addField('custevent_pm', 'text', 'Project Manager');

				var to_Date = request.getParameter('custpage_startdate');
				var s_afterdate = request.getParameter('custpage_fromdate');
				nlapiLogExecution('debug', 'date value', to_Date);

				var firstDay = new Date(
						nlapiStringToDate(to_Date).getFullYear(),
						nlapiStringToDate(to_Date).getMonth(), 1);
				//var lastDay = new Date(nlapiStringToDate(to_Date).getFullYear(),
						//nlapiStringToDate(to_Date).getMonth() + 1, 0);
				firstDay = nlapiDateToString(firstDay, 'mm/dd/yyyy');
				//lastDay = nlapiDateToString(lastDay, 'mm/dd/yyyy');
				var array = _SearchData(firstDay, to_Date);
				sublist1.setLineItemValues(array);
				form.addSubmitButton('Submit');
				form.addButton('custombutton', 'Export As CSV',
						'fxn_generatePDF();');
				response.writePage(form);
			}
		}
	} 
	
	catch (e) 
	{
		nlapiLogExecution('DEBUG', 'Search results exception ', +e)
	}
}

function _SearchData(fromDate, toDate) {
	try {
		// var tem_te_id = new Array();
		var fromdate;
		var lastDay = new Date(nlapiStringToDate(toDate).getFullYear(), nlapiStringToDate(toDate).getMonth() + 1, 0);
		var last_day = nlapiDateToString(lastDay,'mm/dd/yyyy');
		if(toDate == last_day)
		{
			fromDate = nlapiStringToDate(toDate).getDate();
			if(fromDate == 31)
			{
				fromDate = nlapiAddDays(nlapiStringToDate(toDate) , -30);
			}
			else
			{
				fromDate = nlapiAddDays(nlapiStringToDate(toDate), -29);	
			}
			fromDate = nlapiDateToString(fromDate,'mm/dd/yyyy');
		}
		var startdayofweek2 = nlapiStringToDate(fromDate).getDay();
		if (startdayofweek2 == 0) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 7);
		}
		if (startdayofweek2 == 1) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 6);
		}
		if (startdayofweek2 == 2) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 5);

		}
		if (startdayofweek2 == 3) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 4);
		}
		if (startdayofweek2 == 4) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 3);
		}
		if (startdayofweek2 == 5) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 2);
		}
		if (startdayofweek2 == 6) {
			startdayofweek2 = nlapiAddDays(nlapiStringToDate(fromDate), 1);
		}
		
		startdayofweek2 = nlapiDateToString(startdayofweek2, 'mm/dd/yyyy');

		var startdayofweek3 = nlapiAddDays(nlapiStringToDate(startdayofweek2),
				7);
		startdayofweek3 = nlapiDateToString(startdayofweek3, 'mm/dd/yyyy');

		var startdayofweek4 = nlapiAddDays(nlapiStringToDate(startdayofweek3),
				7);
		startdayofweek4 = nlapiDateToString(startdayofweek4, 'mm/dd/yyyy');

		var startdayofweek5 = nlapiAddDays(nlapiStringToDate(startdayofweek4),
				7);
		startdayofweek5 = nlapiDateToString(startdayofweek5, 'mm/dd/yyyy');

		var startdayofweek6 = nlapiAddDays(nlapiStringToDate(startdayofweek5),
				7);
		startdayofweek6 = nlapiDateToString(startdayofweek6, 'mm/dd/yyyy');

		var emp = new Array();
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('date', null, 'within', [
				fromDate, toDate ]);
		a_filters[1] = new nlobjSearchFilter("employee",null,"anyof",["employee","anyof","1605","3169","1614","1542","3212","2120","3216",
		"38446","1709","1634","64930","1728","1641","1957","15585","1587","37281","9742",
		"15693","29344","32605","32642","33724","37078","46224","164933","58279","62815",
		"63980","71222","80271","108662","108663","116033","116133","118471","144323","120850",
		"121280","121576","121903","125935","130013","130010","132614","157331","134763","135470",
		"136511","137234","137842","138361","147847","151154","151764","152034","152028","154890",
		"154887","154698","154888","154580","35856","57789","97261","92425","1704","62389","128082",
		"138154","91290","98280","154695","154687","154731","104289","120101","137238","23469"]);
		

		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('employee');
		a_columns[1] = new nlobjSearchColumn('item');
		a_columns[2] = new nlobjSearchColumn('date');
		a_columns[3] = new nlobjSearchColumn('hours');
		a_columns[4] = new nlobjSearchColumn('internalid');
		a_columns[5] = new nlobjSearchColumn('timesheet');
		a_columns[6] = new nlobjSearchColumn('startdate', 'timesheet');
		a_columns[7] = new nlobjSearchColumn(
				'custrecordcustcol_temp_customer_ts');
		a_columns[8] = new nlobjSearchColumn('department');
		a_columns[9] = new nlobjSearchColumn('custrecord_ts_time_approver',
				'timesheet');
		a_columns[10] = new nlobjSearchColumn('custrecord_projectmanager_ts');
		a_columns[11] = new nlobjSearchColumn('memo');
		a_columns[12] = new nlobjSearchColumn('custrecord_timestamp');
		a_columns[13] = new nlobjSearchColumn('customer');
		a_columns[14] = new nlobjSearchColumn('jobtype','job');

		var searchresult_T = searchRecord('timeentry',
				'customsearch_salnonstdrptte_4', a_filters, a_columns);
		
		//var searchresult_T = searchRecord('timeentry',
				//'customsearch2970', a_filters, a_columns);

		var st = JSON.stringify(searchresult_T);
		searchresult_T = JSON.parse(st);
		var arywk = [];
		var st_fp = 0;
		var mins = 0; 
		var ot = 0;
		var dt = 0;
		var timeoff = 0;
		var p = 0;
		q = 0;
		var holiday = 0;
		var m = 0;
		var itemArray = new Array();
		var emp_array = new Array();
		var week1;
		var week2;
		var week3;
		var week4;
		var week5;

		var stratdate = fromDate;

		for ( var i in searchresult_T) {

			var JSON_Temp = {};
			var curr_col = searchresult_T[i].columns;

			if (i != 0) {
				var temp_col = searchresult_T[i - 1].columns;
				if (timesheet != curr_col.timesheet) {

					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					holiday = 0;

				}
			}
			if (i != 0) {
				if (temp_col.employee.name != curr_col.employee.name) {
					week1 = '';
					week2 = '';
					week3 = '';
					week4 = '';
					stratdate = fromDate;
					for (var kl = 0; kl < 20; ++kl) {
						var d = arywk[kl];
						if (arywk[kl] == undefined) {
							arywk[kl] = 0;
						}

					}
					var stTotal = Number(arywk[0]) + Number(arywk[4])
							+ Number(arywk[8]) + Number(arywk[12])
							+ Number(arywk[16]);
					var otTotal = Number(arywk[1]) + Number(arywk[5])
							+ Number(arywk[9]) + Number(arywk[13])
							+ Number(arywk[17]);
					var tTimeof = Number(arywk[2]) + Number(arywk[6])
							+ Number(arywk[10]) + Number(arywk[14])
							+ Number(arywk[18]);
					var tholidy = Number(arywk[3]) + Number(arywk[7])
							+ Number(arywk[11]) + Number(arywk[15])
							+ Number(arywk[19]);

					itemArray[m] = {
						'custevent_employee' : temp_col.employee.name,
						'custevent_wkst1' : arywk[0],
						'custevent_wkot1' : arywk[1],
						'custevent_wk1timeoff1' : arywk[2],
						'custevent_wkholy1' : arywk[3],
						'custevent_wkst2' : arywk[4],
						'custevent_wkot2' : arywk[5],
						'custevent_wk1timeoff2' : arywk[6],
						'custevent_wkholy2' : arywk[7],
						'custevent_wkst3' : arywk[8],
						'custevent_wkot3' : arywk[9],
						'custevent_wk1timeoff3' : arywk[10],
						'custevent_wkholy3' : arywk[11],
						'custevent_wkst4' : arywk[12],
						'custevent_wkot4' : arywk[13],
						'custevent_wk1timeoff4' : arywk[14],
						'custevent_wkholy4' : arywk[15],
						'custevent_wkst5' : arywk[16],
						'custevent_wkot5' : arywk[17],
						'custevent_wk1timeoff5' : arywk[18],
						'custevent_wkholy5' : arywk[19],
						'custevent_tst' : stTotal,
						'custevent_tot' : otTotal,
						'custevent_ttmoff' : tTimeof,
						'custevent_tth' : tholidy,
						'custevent_aprvr' : temp_col.custrecord_ts_time_approver.name,
						'custevent_proj' : temp_col.customer.name,
						'custevent_projtype': temp_col.jobtype.name,
						'custevent_dptmt' : temp_col.department.name,
						'custevent_clnt' : temp_col.custrecordcustcol_temp_customer_ts,
						'custevent_pm' : temp_col.custrecord_projectmanager_ts.name,
						'custevent_tun' : temp_col.memo

					};
					m = m + 1;
					p = 0;
					arywk = [];
				}
			}

			var employee = curr_col.employee;
			var hours = [];
			//var mins = 0;
			hours = curr_col.hours.split(':');
			
			var hour = parseInt(hours[0]);
			var min = parseInt(hours[1]);
			var date = curr_col.date;
			var item = curr_col.item;
			var timesheet = curr_col.timesheet;
			var timesheetStartdate = curr_col.startdate;
			nlapiStringToDate(timesheetStartdate)
			if (nlapiStringToDate(timesheetStartdate) <= nlapiStringToDate(fromDate)) {

				if (item == 'FP' || item == 'ST' || item == 'Internal Projects') {
					mins = mins + min;
					if(mins != 0)
					{
						if(mins >=60)
						{
							hour = parseInt(hours[0]) + 1;
							mins = mins - 60;
						}	
						else
						{
							var hr = mins/60;
							var h = parseFloat(hr.toFixed(2));
							hour = hour + h;							
							mins = 0;
						}
					}
					st_fp = st_fp + hour;

				}
				if (item == 'OT') {
					ot = ot + hour;

				}
				if (item == 'Leave') {
					timeoff = timeoff + hour;

				}
				if (item == 'Holiday') {
					holiday = holiday + hour;

				}
				week1 = 'filled';
				arywk[p] = st_fp;
				arywk[p + 1] = ot;
				arywk[p + 2] = timeoff;
				arywk[p + 3] = holiday;

			} else if (timesheetStartdate == startdayofweek2) {
				if (nlapiStringToDate(fromDate).getDay() != 6) {
					if (week1 != 'filled') {
						arywk[p] = 0;
						arywk[p + 1] = 0;
						arywk[p + 2] = 0;
						arywk[p + 3] = holiday;
						p = p + 4;
						st_fp = 0;
						ot = 0;
						timeoff = 0;
						week1 = 'filled';
					}
				}

				if (item == 'FP' || item == 'ST' || item == 'Internal Projects') {
					mins = mins + min;
					if(mins != 0)
					{
						if(mins >=60)
						{
							hour = parseInt(hours[0]) + 1;
							mins = mins - 60;
						}
						else
						{
							var hr = mins/60;
							var h = parseFloat(hr.toFixed(2));
							hour = hour + h;							
							mins = 0;
						}						
					}
					st_fp = st_fp + hour;

				}
				if (item == 'OT') {
					ot = ot + hour;

				}
				if (item == 'Leave') {
					timeoff = timeoff + hour;

				}
				if (item == 'Holiday') {
					holiday = holiday + hour;

				}
				week2 = 'filled';
				arywk[p] = st_fp;
				arywk[p + 1] = ot;
				arywk[p + 2] = timeoff;
				arywk[p + 3] = holiday;
			} else if (timesheetStartdate == startdayofweek3) {
				if (nlapiStringToDate(fromDate).getDay() != 6) {
					if (week1 != 'filled') {
						arywk[p] = 0;
						arywk[p + 1] = 0;
						arywk[p + 2] = 0;
						arywk[p + 3] = holiday;
						p = p + 4;
						st_fp = 0;
						ot = 0;
						timeoff = 0;
						week1 = 'filled';
					}
				}
				if (week2 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week2 = 'filled';
				}

				if (item == 'FP' || item == 'ST' || item == 'Internal Projects') {
					mins = mins + min;
					if(mins != 0)
					{
						if(mins >=60)
						{
							hour = parseInt(hours[0]) + 1;
							mins = mins - 60;
						}	
						else
						{
							var hr = mins/60;
							var h = parseFloat(hr.toFixed(2));
							hour = hour + h;							
							mins = 0;
						}
					}
					st_fp = st_fp + hour;

				}
				if (item == 'OT') {
					ot = ot + hour;

				}
				if (item == 'Leave') {
					timeoff = timeoff + hour;

				}
				if (item == 'Holiday') {
					holiday = holiday + hour;

				}
				week3 = 'filled';
				arywk[p] = st_fp;
				arywk[p + 1] = ot;
				arywk[p + 2] = timeoff;
				arywk[p + 3] = holiday;

			} else if (timesheetStartdate == startdayofweek4) {
				if (nlapiStringToDate(fromDate).getDay() != 6) {
					if (week1 != 'filled') {
						arywk[p] = 0;
						arywk[p + 1] = 0;
						arywk[p + 2] = 0;
						arywk[p + 3] = holiday;
						p = p + 4;
						st_fp = 0;
						ot = 0;
						timeoff = 0;
						week1 = 'filled';
					}
				}
				if (week2 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week2 = 'filled';
				}
				if (week3 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week3 = 'filled';
				}

				if (item == 'FP' || item == 'ST' || item == 'Internal Projects') {
					mins = mins + min;
					if(mins != 0)
					{
						if(mins >=60)
						{
							hour = parseInt(hours[0]) + 1;
							mins = mins - 60;
						}
						else
						{
							var hr = mins/60;
							var h = parseFloat(hr.toFixed(2));
							hour = hour + h;							
							mins = 0;
						}						
					}
					st_fp = st_fp + hour;

				}
				if (item == 'OT') {
					ot = ot + hour;

				}
				if (item == 'Leave') {
					timeoff = timeoff + hour;

				}
				if (item == 'Holiday') {
					holiday = holiday + hour;

				}
				week4 = 'filled';
				arywk[p] = st_fp;
				arywk[p + 1] = ot;
				arywk[p + 2] = timeoff;
				arywk[p + 3] = holiday;

			} else if (timesheetStartdate == startdayofweek5) {
				if (nlapiStringToDate(fromDate).getDay() != 6) {
					if (week1 != 'filled') {
						arywk[p] = 0;
						arywk[p + 1] = 0;
						arywk[p + 2] = 0;
						arywk[p + 3] = holiday;
						p = p + 4;
						st_fp = 0;
						ot = 0;
						timeoff = 0;
						week1 = 'filled';
					}
				}
				if (week2 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week2 = 'filled';
				}
				if (week3 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week3 = 'filled';
				}

				if (week4 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week4 = 'filled';
				}
				if (item == 'FP' || item == 'ST' || item == 'Internal Projects') {
					mins = mins + min;
					if(mins != 0)
					{
						if(mins >=60)
						{
							hour = parseInt(hours[0]) + 1;
							mins = mins - 60;
						}	
						else
						{
							var hr = mins/60;
							var h = parseFloat(hr.toFixed(2));
							hour = hour + h;							
							mins = 0;
						}
					}
					st_fp = st_fp + hour;

				}
				if (item == 'OT') {
					ot = ot + hour;

				}
				if (item == 'Leave') {
					timeoff = timeoff + hour;

				}
				if (item == 'Holiday') {
					holiday = holiday + hour;

				}
				week5 = 'filled';
				arywk[p] = st_fp;
				arywk[p + 1] = ot;
				arywk[p + 2] = timeoff;
				arywk[p + 3] = holiday;
			} else if (timesheetStartdate == startdayofweek6) {
				if (nlapiStringToDate(fromDate).getDay() != 6) {
					if (week1 != 'filled') {
						arywk[p] = 0;
						arywk[p + 1] = 0;
						arywk[p + 2] = 0;
						arywk[p + 3] = holiday;
						p = p + 4;
						st_fp = 0;
						ot = 0;
						timeoff = 0;
						week1 = 'filled';
					}
				}
				if (week2 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week2 = 'filled';
				}
				if (week3 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week3 = 'filled';
				}

				if (week4 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week4 = 'filled';
				}
				if (week5 != 'filled') {
					arywk[p] = 0;
					arywk[p + 1] = 0;
					arywk[p + 2] = 0;
					arywk[p + 3] = holiday;
					p = p + 4;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					week5 = 'filled';
				}
				if (item == 'FP' || item == 'ST' || item == 'Internal Projects') {
					mins = mins + min;
					if(mins != 0)
					{
						if(mins >=60)
						{
							hour = parseInt(hours[0]) + 1;
							mins = mins - 60;
						}
						else
						{
							var hr = mins/60;
							var h = parseFloat(hr.toFixed(2));
							hour = hour + h;
							mins = 0;
						}						
					}
					st_fp = st_fp + hour;

				}	
				if (item == 'OT') {
					ot = ot + hour;

				}
				if (item == 'Leave') {
					timeoff = timeoff + hour;

				}
				if (item == 'Holiday') {
					holiday = holiday + hour;

				}
				week6 = 'filled';
				arywk[p] = st_fp;
				arywk[p + 1] = ot;
				arywk[p + 2] = timeoff;
				arywk[p + 3] = holiday;
			}

			else {
				st_fp = 0;
				ot = 0;
				timeoff = 0;
				holiday = 0
			}

		}

		return itemArray;

	}

	catch (e) {
		nlapiLogExecution('DEBUG', 'Search results exception ', e);
		throw e;
	}
}

function create_CSV(lists) {

	var html = "";

	html += "Employee,";
	html += "WK1 ST,";
	html += "WK1 OT,";
	html += "WK1 TIMEOFF,";
	html += "WK1 HOLIDAY,";

	html += "WK2 ST,";
	html += "WK2 OT,";
	html += "WK2 TIMEOFF,";
	html += "WK2 HOLIDAY,";

	html += "WK3 ST,";
	html += "WK3 OT,";
	html += "WK3 TIMEOFF,";
	html += "WK3 HOLIDAY,";

	html += "WK4 ST,";
	html += "WK4 OT,";
	html += "WK4 TIMEOFF,";
	html += "WK4 HOLIDAY,";

	html += "WK5 ST,";
	html += "WK5 OT,";
	html += "WK5 TIMEOFF,";
	html += "WK5 HOLIDAY,";

	html += "Total ST,";
	html += "Total OT,";
	html += "Total TIMEOFF,";
	html += "Total HOLIDAY,";

	html += "Timesheet Approver,";
	html += "Department,";
	html += "Customer,";
	html += "Project Manager,";
	html += "User Notes,";

	html += "\r\n";
	try {
		for (var l = 0; l < lists.length; l++) {
			var list = lists[l];
			html += "\r\n";
			for ( var i in list) {
				if (list[i] == 'undefined' || list[i] == null) {
					list[i] = 0;
				}

				html += list[i];// e
				html += ",";

			}
		}

		return html;
	} catch (e) {
		// TODO: handle exception
	}
}
