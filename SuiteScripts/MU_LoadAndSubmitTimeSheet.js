/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Jan 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	try
	{
		var rec = nlapiLoadRecord(recType, recId);
		
		nlapiSubmitRecord = nlapiSubmitRecord(rec);
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'TimeSheet ID: ' + recId, e.message);
	}
	
	nlapiLogExecution('AUDIT', 'TimeSheet ID: ' + recId, 'Record Submitted.');
}
