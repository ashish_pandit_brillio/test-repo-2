function getAllAllocations() {
	try {
		var allocationSearch = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('formuladate', null, 'onorafter',
		                '1/1/2016').setFormula('{enddate}'),
		        new nlobjSearchFilter('custentity_implementationteam',
		                'employee', 'is', 'F') ], [
		        new nlobjSearchColumn('resource'),
		        new nlobjSearchColumn('company'),
		        new nlobjSearchColumn('percentoftime'),
		        new nlobjSearchColumn('startdate').setSort(),
		        new nlobjSearchColumn('enddate') ]);

		var allocationList = {};
		var employeeList = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				employeeList.push(allocation.getValue('resource'));

				if (!allocationList[allocation.getValue('resource')]) {
					allocationList[allocation.getValue('resource')] = {
					    Name : allocation.getText('resource'),
					    Allocation : []
					};
				}

				allocationList[allocation.getValue('resource')].Allocation
				        .push({
				            projectId : allocation.getValue('company'),
				            projectText : allocation.getText('company'),
				            percent : allocation.getValue('percentoftime'),
				            startdate : allocation.getValue('startdate'),
				            enddate : allocation.getValue('enddate'),
				            d_stardate : nlapiStringToDate(allocation
				                    .getValue('startdate')),
				            d_enddate : nlapiStringToDate(allocation
				                    .getValue('enddate'))
				        });
			});
		}

		return {
		    Allocation : allocationList,
		    Employee : employeeList
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllAllocations', err);
		throw err;
	}
}

function suitelet(request, response) {
	try {
		var allocationData = getAllAllocations();

		nlapiLogExecution('debug', 'allocationData.Employee', _
		        .uniq(allocationData.Employee).length);

		var practiceObject = {};
		var practiceSearch = searchRecord(null, 1400, [ new nlobjSearchFilter(
		        'internalid', null, 'anyof', allocationData.Employee) ]);

		var empProcessed = [];

		for (var i = 0; i < practiceSearch.length;) {
			var employeeId = practiceSearch[i].getId();
			empProcessed.push(employeeId);
			var employeeName = practiceSearch[i].getValue('entityid');
			var empFunction = practiceSearch[i]
			        .getText('custentity_emp_function');
			var search = [];

			for (var j = i; j < practiceSearch.length; j++, i++) {

				if (practiceSearch[j].getId() != employeeId) {
					break;
				}

				search.push(practiceSearch[j]);
			}

			if (search.length > 0) {
				practiceObject[employeeId] = {};
				practiceObject[employeeId].Name = employeeName;
				practiceObject[employeeId].Function = empFunction;
				practiceObject[employeeId].Practice = getEmployeePracticeHistory(
				        employeeId, search);
			}
		}

		nlapiLogExecution('debug', 'empProcessed', _.uniq(empProcessed).length);

		// search for remaining employees
		var employeeSearch = searchRecord('employee', null, [
		        new nlobjSearchFilter('internalid', null, 'noneof',
		                empProcessed),
		        new nlobjSearchFilter('internalid', null, 'anyof',
		                allocationData.Employee),
		        new nlobjSearchFilter('custentity_implementationteam', null,
		                'is', 'F') ], [ new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('department'),
		        new nlobjSearchColumn('hiredate'),
		        new nlobjSearchColumn('custentity_lwd'),
		        new nlobjSearchColumn('custentity_emp_function') ]);

		if (employeeSearch) {
			nlapiLogExecution('debug', 'remaining count', employeeSearch.length);

			employeeSearch.forEach(function(emp) {
				practiceObject[emp.getId()] = {};
				practiceObject[emp.getId()].Name = emp.getValue('entityid');

				var entry = {
				    Name : emp.getText('department'),
				    Function : emp.getText('custentity_emp_function'),
				    StartDate : emp.getValue('hiredate'),
				    EndDate : '1/1/2099'
				};

				if (emp.getValue('custentity_lwd')) {
					entry.EndDate = emp.getValue('custentity_lwd');
				}

				practiceObject[emp.getId()].Practice = [ entry ];
			});
		}

		var mainObject = [];
		var newYear = nlapiStringToDate('1/1/2016');

		for ( var employee in allocationData.Allocation) {

			if (!practiceObject[employee]) {
				nlapiLogExecution('debug', 'employee practice not found',
				        allocationData.Allocation[employee].Name);
				continue;
			}

			var practiceList = practiceObject[employee].Practice;
			var allocationList = allocationData.Allocation[employee].Allocation;

			allocationList
			        .forEach(function(currentAllocation) {
				        var allocationStartDate = currentAllocation.d_startdate;
				        var allocationEndDate = currentAllocation.d_enddate;

				        practiceList
				                .forEach(function(currentPractice) {
					                var practiceStartDate = nlapiStringToDate(currentPractice.StartDate);
					                var practiceEndDate = nlapiStringToDate(currentPractice.EndDate);

					                if (practiceEndDate < newYear)
						                return;

					                if (allocationStartDate > practiceEndDate)
						                return;

					                if (practiceStartDate > allocationEndDate)
						                return;

					                if (practiceStartDate < newYear)
						                practiceStartDate = newYear;

					                var newStartDate = allocationStartDate > practiceStartDate ? allocationStartDate
					                        : practiceStartDate;

					                var newEndDate = allocationEndDate < practiceEndDate ? allocationEndDate
					                        : practiceEndDate;

					                if (newStartDate >= newEndDate)
						                return;

					                mainObject
					                        .push({
					                            // Function :
												// practiceObject[employee].Function,
					                            EmployeeName : allocationData.Allocation[employee].Name,
					                            ProjectName : currentAllocation.projectText,
					                            PracticeName : currentPractice.Name,
					                            AllocationStart : currentAllocation.startdate,
					                            AllocationEnd : currentAllocation.enddate,
					                            Percent : currentAllocation.percent,
					                            PracticeStart : currentPractice.StartDate,
					                            PracticeEnd : currentPractice.EndDate,
					                            StartDate : nlapiDateToString(
					                                    newStartDate, "date"),
					                            EndDate : nlapiDateToString(
					                                    newEndDate, "date"),
					                        });
				                });
			        });
		}

		var table = "<table>";

		table += "<tr>";
		// table += "<td style='border:1px solid black;'>";
		// table += "Function";
		// table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Employee Name";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Project Name";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Percent";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Practice Name";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Start Date";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "End Date";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Allocation Start Date";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Allocation End Date";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Practice Start Date";
		table += "</td>";
		table += "<td style='border:1px solid black;'>";
		table += "Practice End Date";
		table += "</td>";
		table += "</tr>";

		mainObject.forEach(function(row) {
			table += "<tr>";

			// table += "<td style='border:1px solid black;'>";
			// table += row.Function;
			// table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.EmployeeName;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.ProjectName;
			table += "</td>";
			
			table += "<td style='border:1px solid black;'>";
			table += row.Percent;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.PracticeName;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.StartDate;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.EndDate;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.AllocationStart;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.AllocationEnd;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.PracticeStart;
			table += "</td>";

			table += "<td style='border:1px solid black;'>";
			table += row.PracticeEnd;
			table += "</td>";

			table += "</tr>";
		});

		// for ( var emp in practiceObject) {
		//
		// practiceObject[emp].Practice.forEach(function(row) {
		// table += "<tr>";
		//
		// table += "<td style='border:1px solid black;'>";
		// table += practiceObject[emp].Name;
		// table += "</td>";
		//
		// table += "<td style='border:1px solid black;'>";
		// table += row.Name;
		// table += "</td>";
		//
		// table += "<td style='border:1px solid black;'>";
		// table += row.StartDate;
		// table += "</td>";
		//
		// table += "<td style='border:1px solid black;'>";
		// table += row.EndDate;
		// table += "</td>";
		//
		// table += "</tr>";
		// });
		// }

		table += "</table>";

		response.write(table);
		nlapiLogExecution('debug', 'done');
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
	}
}

function getEmployeePracticeHistory(employeeId, search) {
	try {

		var practiceHistory = [];

		if (search && search.length > 0) {

			// Practice Name
			practiceHistory.push({
				Name : search[0].getValue('oldvalue', 'systemnotes')
			});

			for (var i = 0; i < search.length; i++) {
				practiceHistory.push({
					Name : search[i].getValue('newvalue', 'systemnotes')
				});
			}

			// End Date
			for (var i = 0; i < search.length; i++) {
				practiceHistory[i].EndDate = search[i].getValue('formuladate');
			}

			var lwd = search[0].getValue('custentity_lwd');
			var hiredate = search[0].getValue('hiredate');

			if (lwd) {
				practiceHistory[search.length].EndDate = lwd;
			} else {
				practiceHistory[search.length].EndDate = "1/1/2099";
			}

			// Start Date
			practiceHistory[0].StartDate = hiredate;

			for (var i = 1; i < practiceHistory.length; i++) {
				var s_endDate = practiceHistory[i - 1].EndDate;
				var d_endDate = nlapiStringToDate(s_endDate);
				var d_startDate = nlapiAddDays(d_endDate, 1);
				var s_startDate = nlapiDateToString(d_startDate, 'date');
				practiceHistory[i].StartDate = s_startDate;
			}
		}

		return practiceHistory;
	} catch (err) {
		nlapiLogExecution('ERROR', employeeId, err);
		nlapiLogExecution('ERROR', employeeId, JSON.stringify(search));
		throw err;
	}
}
