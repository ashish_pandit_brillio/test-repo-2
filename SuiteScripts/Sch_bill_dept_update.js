/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Nov 2018     Sai Saranya
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function bill_update(type) {
	
	try{
		// 
			var search = nlapiSearchRecord(
			        'vendorbill','customsearch2551',null,[new nlobjSearchColumn('internalid')]);
				if(search)
				{
					for(var i=0;i<search.length;i++)
					{
						var bilid=search[i].getValue('internalid');
						var rec=nlapiLoadRecord('vendorbill',bilid);
						var i_expense_count= rec.getLineItemCount('expense');
						for (var i_exp_index = 1; i_exp_index <= i_expense_count; i_exp_index++)
						{
							rec.setLineItemValue('expense', 'department', i_exp_index, '');
							
						}
						rec.setFieldValue('custbody_is_je_updated_for_emp_type','F');
						nlapiSubmitRecord(rec);
						nlapiLogExecution('debug', 'bilid', bilid);
					
					}
					}
				}
				catch(e)
				{
				nlapiLogExecution('error', 'Main', e);
				}			

}
