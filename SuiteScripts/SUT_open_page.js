/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Mar 2018     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	var method = request.getMethod();
	if (method == 'GET') {
		postFormAppoved(request);
	}
}
function postFormAppoved(request) {
	try {
		
	
		var user = nlapiGetUser();
		if(user == 7905)
		user = 3169;
		var status = '';
		if(parseInt(user)==parseInt(30484) || parseInt(user)==parseInt(3165) || parseInt(user)==parseInt(41571) ||parseInt(user)==parseInt(62082) ||parseInt(user)==parseInt(7905) ){
          	var form = nlapiCreateForm('Admin Practise Forecast report generation will take few moments, please continue with other tasks as you will receive this report in your mail box soon....!');
				 status=nlapiScheduleScript('customscript1559', 'customdeploy1');//  
		}					
		else
		{
			var a_practice_list_names = new Array();
			var pract_col = new Array();
			var pract_fil = new Array();
			var pract_search = '';
			//
			try{
				pract_fil = [[ ['custrecord_practicehead','anyof', user ],
		            	'or',
						['custrecord_practice_head_2','anyof', user ],
		            	'and',
		            	['isinactive','is', 'F' ]]];
				pract_col[0] = new nlobjSearchColumn('internalid');
				pract_col[1] = new nlobjSearchColumn('custrecord_practicehead');
				pract_col[2] = new nlobjSearchColumn('custrecord_parent_practice');
				pract_search = nlapiSearchRecord('department', null, pract_fil,pract_col);
				if (pract_search) {
					form = nlapiCreateForm('Participating Practise Forecast report generation will take few moments, please continue with other tasks as you will receive this report in your mail box soon....!');
					 status=nlapiScheduleScript('customscript1566', 'customdeploy1');// 
				}
				 else
				 {
					form = nlapiCreateForm('Forecast report can be accessable only by Practice Heads!!');
				 }
			}
			catch(err)
			{
				nlapiLogExecution('error','practiceSearch',err);
			}
		}
			nlapiLogExecution('DEBUG','STATUS',status);
			response.writePage(form);
			nlapiLogExecution('error', 'USER', user);
		
		
	}     catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		
	}
	
}