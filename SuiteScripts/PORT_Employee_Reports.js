/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Feb 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjPortlet}
 *            portlet Current portlet object
 * @param {Number}
 *            column Column position index: 1 = left, 2 = middle, 3 = right
 * @returns {Void}
 */
function main(portlet, column) {
	try {
		portlet.setTitle('Reports');
		var content = getReportAccess();
		nlapiLogExecution('debug', 'done', content);
		portlet.setHtml(content);
	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
		portlet.setHtml('Some Error Occured. Please contact the administrator.');
	}
}

function getReportAccess() {
	var currentEmployeeId = nlapiGetUser();
	var permissionSearch = nlapiSearchRecord(
			'customrecord_report_permission_list',
			null,
			[ new nlobjSearchFilter('custrecord_rpl_employee', null, 'anyof', [ currentEmployeeId ]) ],
			[ new nlobjSearchColumn('custrecord_rpl_employee'),
				new nlobjSearchColumn('custrecord_rpl_vertical'),
				new nlobjSearchColumn('custrecord_rpl_department') ]);
	nlapiLogExecution('debug', 'done', 'a');
	var content = '';

	if (isArrayNotEmpty(permissionSearch)) {
		var url = null, value = null, link = null;

		getParams().forEach(
				function(param) {
					url = null, link = null, value = null;
					nlapiLogExecution('debug', 'done', JSON.stringify(param));

					for (var i = 0; i < permissionSearch.length; i++) {

						value = permissionSearch[i].getValue(param.SearchColumn);

						if (isNotEmpty(value)) {
							nlapiLogExecution('debug', 'done', 'd ' + param.Script + ' '
									+ param.Deployment);
							url = nlapiResolveURL('SUITELET', param.Script, param.Deployment);
							nlapiLogExecution('debug', 'url', url);
							link = "<a href='" + url + "'>" + param.Text + "</a><br/>";
							content += link;
							break;
						}
					}
					nlapiLogExecution('debug', 'done', 'c');

				});

	}

	return content;
}

function getParams() {
	nlapiLogExecution('debug', 'done', 'b');
	return [ {
		Script : 'customscript_sut_revenue_report_prj_wise',
		Deployment : 'customdeploy1',
		Text : 'Revenue Report - Vertical Wise',
		SearchColumn : 'custrecord_rpl_vertical'
	}, {
		Script : 'customscript_sut_revenue_report_prj_wise',
		Deployment : 'customdeploy_sut_revenue_rep_department',
		Text : 'Revenue Report - Department Wise',
		SearchColumn : 'custrecord_rpl_department'
	} ];
}