/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
    Script Name: CLI_PR_Validations.js
	Author     : Shweta Chopde
	Company    : Aashna Cloudtech
	Date       : 10 April 2014
	Description: Validations on PR


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...
}
// END GLOBAL VARIABLE BLOCK  =======================================


var s_type = '';


// BEGIN PAGE INIT ==================================================
/**
 * 
 * @param {Object} type
 * 
 * Description --> Disable the line item fields o PR ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Item , Quotation Analysis & few of the body fields
 * 
 */
function pageInit_PR(type)
{	
    s_type = type;

  	if(type == 'create')//@20-01-2021_Show pop up message when the pr page opens in creation mode
	{
		alert('Raising PR is Now on finger tips, You can raise PR with On The Go App now !!');
	} /// if(type == 'create')

     nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prno1', true)	
	 
	 /*
var i_approval_status = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus')
	 
	 
	 if((i_approval_status == 5) ||(i_approval_status == 6)||(i_approval_status == 7)||(i_approval_status == 8)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
	 {	 	
		   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	 }
	 
	  if(i_approval_status != 23)
	  {
	  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
	  }
	 
	 
	 if(i_approval_status == 6 || i_approval_status == 5)
	 {
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', true)	
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)				
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	 	
	 }
	 
	 
	 
	 if(i_approval_status == 7)
	 {	
	      nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
		
	 }
*/
   
if(type == 'recmachcustrecord_prquote')
   {
   	
	 var i_approval_status_1 = ''
   	 i_approval_status_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_approvalstatusforquotation')
	 
	 if(i_approval_status_1 == 3)
	 {	 					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_prquote', true)		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_description', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_qty', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_uom', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_currency', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor1name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate1', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discount', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymentterms', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv1', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor2name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_disocuntven2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor3name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discountv3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv3', true)					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_verticalheadforquotation', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_financehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendorforpo', true)		
	 }
	 
		
	  nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)	 
	  nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', true)	 
   }

   nlapiDisableField('custrecord_prno', true)  
   nlapiDisableField('custrecord_requisitionname', true)  
   nlapiDisableField('custrecord_requisitionerid', true)
  /*
 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)  
   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', true)   
   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', true)   
   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', true)      
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true) 
*/  
  
   var i_employee = nlapiGetFieldValue('custrecord_requisitionname')	 
 	 
	 if(_logValidation(i_employee))
	 {	 	
		 var a = new Array();
          a['User-Agent-x'] = 'SuiteScript-Call';
		  
		  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=130&deploy=1&custscript_employee=' + i_employee, null, a);
      		                    
		  var i_employeeID = resposeObject.getBody();	
	 
	         if(_logValidation(i_employeeID))
			 {
			 	 nlapiSetFieldValue('custrecord_requisitionerid',i_employeeID,true,true)
			 }//Employee ID	
			
	 }//Employee ID	

}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_PR()
{
  var i_line_count = nlapiGetLineItemCount('recmachcustrecord_purchaserequest');
  
   if(!_logValidation(i_line_count))
   {
   	alert(' Please enter atleast a single PR - Item .')
	return false;	
   }


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================
/**
 * 
 * @param {Object} type
 * @param {Object} name
 * @param {Object} linenum
 * 
 * Description --> Disable the line item fields o PR ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Item , Quotation Analysis & few of the body fields ,  
 *                 throw alert on selection of duplicate vendors for fields Vendor 1 , Vendor 2 & Vendor 3 &
 *                 on selection of larger Quantity than the Quantity present
 */
function fieldChanged_PR_validations(type, name, linenum)
{
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)	
	
	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
    if(name == 'custrecord_prmatgrpcategory')
	{		
		var i_current_index = nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')
		nlapiLogExecution('DEBUG', 'afterSubmit_auto_numbering', ' Current Index --> ' + i_current_index);  
		nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prlineitemno',i_current_index,true,true)
					
	}//Added SR NO

   if(name =='custrecord_vendorforpo')
   {
   	 var i_vendor_PO = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendorforpo')
	 
	 var i_vendor_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name')
	
	 var i_vendor_2 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name')
	 
	 var i_vendor_3 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name')
	 
	 if(_logValidation(i_vendor_PO) && _logValidation(i_vendor_1) &&_logValidation(i_vendor_2) && _logValidation(i_vendor_3))
	 {
	 	 if((parseInt(i_vendor_PO) != parseInt(i_vendor_1))&&(parseInt(i_vendor_PO) != parseInt(i_vendor_2))&&(parseInt(i_vendor_PO) != parseInt(i_vendor_3)))
		 {		 
		 	alert(' You cannot enter this vendor \n Please enter amongst the entered vendors.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendorforpo','',true,true)
					
		 }
		
	 }	 
	 if(_logValidation(i_vendor_PO) && _logValidation(i_vendor_1)&&_logValidation(i_vendor_2)&&!_logValidation(i_vendor_3))
	 {
	 	 if((parseInt(i_vendor_PO) != parseInt(i_vendor_2))&&(parseInt(i_vendor_PO) != parseInt(i_vendor_1)))
		 {
		 	 nlapiLogExecution('DEBUG', 'fieldChanged_PR_validations',' Vendor 2 .............');
	
		 	alert(' You cannot enter this vendor \n Please enter amongst the entered vendors.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendorforpo','',true,true)
					
		 }
		
	 }	 
	 
	 if(_logValidation(i_vendor_PO) && _logValidation(i_vendor_1)&&!_logValidation(i_vendor_2)&&!_logValidation(i_vendor_3))
	 {
	 	 if(parseInt(i_vendor_PO) != parseInt(i_vendor_1))
		 {
		 	 nlapiLogExecution('DEBUG', 'fieldChanged_PR_validations',' Vendor 3 .............');
	
		 	alert(' You cannot enter this vendor \n Please enter amongst the entered vendors.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendorforpo','',true,true)
					
		 }
		
	 }	 
   }//Vendor 1
   
   if(name =='custrecord_vendor1name')
   {
   	 var i_vendor_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name')
	 
	 var i_vendor_2 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name')
	 
	 var i_vendor_3 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name')
	 
	 if(_logValidation(i_vendor_2))
	 {
	 	 if(i_vendor_1 == i_vendor_2)
		 {
		 	alert(' You cannot enter duplicate vendor.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name','',true,true)
			
		 }
		
	 }
	  if(_logValidation(i_vendor_3))
	 {
	 	 if(i_vendor_1 == i_vendor_3)
		 {
		 	alert(' You cannot enter duplicate vendor.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name','',true,true)
					
		 }
		
	 }	 
	
   }//Vendor 1
      
   
   if(name =='custrecord_vendor2name')
   {
   	 var i_vendor_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name')
	 
	 var i_vendor_2 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name')
	 
	 var i_vendor_3 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name')
	 
	   if(!_logValidation(i_vendor_1))
	  {
	  	alert(' Please enter Vendor 1 . ')
		nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name','',false,false)
		
	  }	 
	 if(_logValidation(i_vendor_1))
	 {
	 	 if(i_vendor_2 == i_vendor_1)
		 {
		 	alert(' You cannot enter duplicate vendor.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name','',true,true)
			
		 }
		
	 }
	  if(_logValidation(i_vendor_3))
	 {
	 	 if(i_vendor_2 == i_vendor_3)
		 {
		 	alert(' You cannot enter duplicate vendor.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name','',true,true)
					
		 }
		
	 }	 
	
   }//Vendor 2
   
    if(name =='custrecord_vendor3name')
   {
   	 var i_vendor_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor1name')
	 
	 var i_vendor_2 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor2name')
	
	 var i_vendor_3 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name')
	 
	  if(!_logValidation(i_vendor_1))
	  {
	  	alert(' Please enter Vendor 1 . ')
		nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name','',false,false)
		
	  }
	   if(!_logValidation(i_vendor_2))
	  {
	  	alert(' Please enter Vendor 2 . ')
		nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name','',false,false)
		
	  }
	 if(_logValidation(i_vendor_2))
	 {
	 	 if(i_vendor_3 == i_vendor_2)
		 {
		 	alert(' You cannot enter duplicate vendor.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name','',true,true)
			
		 }
		
	 }
	 if(_logValidation(i_vendor_1))
	 {
	 	 if(i_vendor_3 == i_vendor_1)
		 {
		 	alert(' You cannot enter duplicate vendor.')
			nlapiSetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_vendor3name','',true,true)
					
		 }
		
	 }	 
	
   }//Vendor 2
   
   if(name == 'custrecord_valueperunit')
   {
   	  var i_quantity = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity')
	  var i_value_per_unit = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_valueperunit')
	
	  if(!_logValidation(i_quantity))
	  {
	  	i_quantity = 0
		
	  } 
	   if(!_logValidation(i_value_per_unit))
	  {
	  	i_value_per_unit = 0
		
	  } 
	  
	  var i_total_value = parseFloat(i_quantity)* parseFloat(i_value_per_unit)
	  nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_totalvalue',i_total_value,true,true)
		
   }//Value Per Unit
    if(name == 'custrecord_quantity')
   {
   /*
	alert(' Quantity .....')
	
*/
   	  var i_index = nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')	
   	 /*
 var i_quantity = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',i_index)
	  var i_quantity_remain = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_qtyremain',i_index)
	  var i_value_per_unit = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_valueperunit',i_index)
	
*/	

       var i_quantity = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity')
	   var i_quantity_remain = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_qtyremain')
	   var i_value_per_unit = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_valueperunit')

	 	/*
alert(' Quantity .....'+i_quantity)
		alert(' i_index .....'+i_index)
	
*/
	 /*
	alert(' Quantity Remaining.....'+i_quantity_remain)
		
*/
		
	  if(!_logValidation(i_quantity_remain))
	  {
	   nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_qtyremain' , i_quantity,true)
	  } 
	
	
	
	   if(!_logValidation(i_value_per_unit))
	  {
	  	i_value_per_unit = 0		
	  } 
	  
	  var i_total_value = parseFloat(i_quantity)* parseFloat(i_value_per_unit)	  
	  nlapiSetLineItemValue('recmachcustrecord_purchaserequest','custrecord_totalvalue',i_index ,i_total_value,true,true)
		
	   if(i_quantity_remain!=0 && (i_quantity!=null && i_quantity!='' && i_quantity!=undefined) &&(i_quantity_remain!=null && i_quantity_remain!=undefined && i_quantity_remain!=''))
		{			
			if(parseInt(i_quantity) > parseInt(i_quantity_remain))
			{
				nlapiSelectLineItem('recmachcustrecord_purchaserequest',i_index)
				nlapiSetLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',i_index ,'')			
				alert(' Entered quanity is exceeding the quantity in stock \n Please enter quantity within remaining quantity limit .')
			    
			}
		}	
		
   }//Quantity

    if(name == 'custrecord_select_create_po')
	{
	var i_PR_recordID = nlapiGetRecordId()	
		
	var s_status ;	
	
	var i_user = nlapiGetUser()
	
	var f_select_create_PO = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_select_create_po')
		
	var i_vendor = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_vendor_pr_item')
	
	var i_item = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prmatgrpcategory')
			
	var i_created_PO = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_created_po_id')
	
	if(f_select_create_PO == 'T')
	{
	  var i_index = nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')	
   	  var i_quantity = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity')
	  var i_quantity_remain = nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_qtyremain')	
	
	   if(i_quantity_remain!=0 && (i_quantity!=null && i_quantity!='' && i_quantity!=undefined) &&(i_quantity_remain!=null && i_quantity_remain!=undefined && i_quantity_remain!=''))
		{			
			if(parseInt(i_quantity) > parseInt(i_quantity_remain))
			{			
				nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity','')	
				nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_select_create_po','F')		
				alert(' Entered quanity is exceeding the quantity in stock \n Please enter quantity within remaining quantity limit .')
			}
		}		
		
	}
			
	}
   
     nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)

   if(type == 'recmachcustrecord_purchaserequest')
   {   	
	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
    nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
   
   	  var i_index = nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')	
	
   	  var i_approval_status = ''
   	  i_approval_status = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',i_index)
	  var i_quantity = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',i_index)
	 
	    if((i_approval_status == 22)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
	   {	  
		   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	       
		  if((i_approval_status != 23))
		  {
		  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		  }
		  
		   if(i_quantity == 0)
			{
				nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			} 
       }
	   
	  if((i_approval_status == 1) ||(i_approval_status == 2) ||(i_approval_status == 3) ||(i_approval_status == 4) )
	  {	  	 
	  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
	  }
	   
	  var i_approval_status = ''
   	  i_approval_status = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',i_index)
	 
	 if(i_approval_status == 6 || i_approval_status == 5)
	 {	 
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', true)	
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)				
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	 	 
		if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		
	 }
	 else
	 {
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', false)
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', false)	
	 	//nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)				
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', false)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', false)
		 //nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', false)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', false)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', false)
	
	       if((i_approval_status == 1) ||(i_approval_status == 2) ||(i_approval_status == 3) ||(i_approval_status == 4) )
		  {	  	 
		  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		  }
	       if((i_approval_status == 22)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
		   {	  
			   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		       
			  if((i_approval_status != 23))
			  {
			  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			  } 
	       }
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)	
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		  
		  
		  
	 }

	 var i_approval_status = ''
   	  i_approval_status = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',i_index)
	  
	 if(i_approval_status == 7)
	 {	  
	    nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)  
	     nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	
	     if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
	 }
	 else
	 {
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', false)				
		//nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', false)
		// nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', false)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', false)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', false)
		
		 if((i_approval_status == 1) ||(i_approval_status == 2) ||(i_approval_status == 3) ||(i_approval_status == 4) )
		  {	  	 
		  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		  }
	       if((i_approval_status == 22)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
		   {	  
			   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		       
			  if((i_approval_status != 23))
			  {
			  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			  } 
	       }
		 if(i_approval_status == 6 || i_approval_status == 5)
	 {	 
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', true)	
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)				
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	 	
		
	 }
  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)	
  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
    if(i_quantity == 0)
	{
		nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
	}
	else
	{
		nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
	}
		
	 }
	 
	 
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
	   
	 
	 
	 
	 
	
	 return true;
   }

   if(type == 'recmachcustrecord_prquote')
   {
   	 var i_approval_status_1 = ''
   	 i_approval_status_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_approvalstatusforquotation')
	 
	 if(i_approval_status_1 == 3)
	 {	 					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_prquote', true)		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_description', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_qty', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_uom', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_currency', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor1name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate1', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discount', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymentterms', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv1', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor2name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_disocuntven2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor3name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discountv3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv3', true)					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_verticalheadforquotation', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_financehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendorforpo', true)
	
	 }
	 else
	 {
	     nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_prquote', false)		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_item', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_description', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_qty', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_uom', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_currency', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor1name', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate1', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discount', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymentterms', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv1', false)				  		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor2name', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_disocuntven2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor3name', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discountv3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv3', false)					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_verticalheadforquotation', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_financehead', false)
	//	 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', false)
	//	 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendorforpo', false)	
		 
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', true)
   
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', true)
		   
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', true)
		   
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', true)
      
	 }
	 
	  nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)	 
	  nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', true)	 
	 
   }
   return true; 
   
}//Field Change

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================
/**
 * 
 * @param {Object} type
 * 
 * Description --> Disable the line item fields o PR ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Item , Quotation Analysis 
 */
function lineInit_PR(type)
{	
 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)	
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)

   if(type == 'recmachcustrecord_purchaserequest')
   {
   	
	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
    nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
   
   	  var i_index = nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')	
	
   	  var i_approval_status = ''
   	  i_approval_status = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',i_index)
	 
	  var i_quantity = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_quantity',i_index)
	 	 
	    if((i_approval_status == 22)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
	   {
	   	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}	  
		   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	       
		  if((i_approval_status != 23))
		  {
		  	if(i_quantity == 0)
			{
				nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			}
			else
			{
				nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
			}
		  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		  } 
       }
	   
	  if((i_approval_status == 1) ||(i_approval_status == 2) ||(i_approval_status == 3) ||(i_approval_status == 4) )
	  {	
	   if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}  	 
	  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
	  }
	   
	  var i_approval_status = ''
   	  i_approval_status = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',i_index)
	 
	 if(i_approval_status == 6 || i_approval_status == 5)
	 {
	 	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}	 
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', true)	
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)				
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	 	
	 }
	 else
	 {
	 	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', false)
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', false)	
	 	//nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)				
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', false)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', false)
		 //nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', false)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', false)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', false)
	
	       if((i_approval_status == 1) ||(i_approval_status == 2) ||(i_approval_status == 3) ||(i_approval_status == 4) )
		  {	 
		    
	 	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		} 	 
		  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		  }
	       if((i_approval_status == 22)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
		   {	
		   
	 	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}  
			   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		       
			  if((i_approval_status != 23))
			  {
			  	
	 	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}
			  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			  } 
	       }
		   	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
             nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
			 
			 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
    nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)	
	
	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)

	 }

	 var i_approval_status = ''
   	  i_approval_status = nlapiGetLineItemValue('recmachcustrecord_purchaserequest','custrecord_prastatus',i_index)
	  
	 if(i_approval_status == 7)
	 {
	 	if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}
		nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)	    
	     nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	
	 }
	 else
	 {	 	
	 	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', false)				
		//nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', false)
		// nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', false)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', false)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', false)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', false)
		
		 if((i_approval_status == 1) ||(i_approval_status == 2) ||(i_approval_status == 3) ||(i_approval_status == 4) )
		  {	
		  
		    if(i_quantity == 0)
			{
				nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			}
			else
			{
				nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
			}  	 
		  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		    
		  
		  }
	       if((i_approval_status == 22)||(i_approval_status == 23)||(i_approval_status == 24)||(i_approval_status == 25))
		   {	  
			   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
		       
			  if((i_approval_status != 23))
			  {
			  	if(i_quantity == 0)
				{
					nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
				}
				else
				{
					nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
				}
				
			  	nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
			  } 
	       }
		 if(i_approval_status == 6 || i_approval_status == 5)
	 {	 
	 
	    
		if(i_quantity == 0)
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
		}
		else
		{
			nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', false)
		}
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_cancel_pr', true)
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_quantity', true)	
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)				
	 	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)				
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_employee', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatgrpcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prmatldescription', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itemcategory', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_currencyforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_unit', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_valueperunit', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_project', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prpractices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_practices', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_deliverylocation', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_billabletocustomer', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticeapproval', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)					
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord11', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadforpr', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticalheadremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteamremarks', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_pr_item_quote', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_updated_by_consolidated', true)
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_remaining_quantity_consolidat', true)
	 	
	 }
		nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)
	
    nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subsidiaryforpr', true)	
	
	 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_totalvalue', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)
   nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_qtyremain', true)
    nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_created_po_id', true)
	
	/*
    if(i_quantity == 0)
	{
		nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_select_create_po', true)
	}
	
   */
  
	 }
	 
	 
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_customerforpritem', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_verticals', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_subpracticehead', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_itforpurchaserequest', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_procurementteam', true)
	  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_vendor_pr_item', true)
	   
	 return true;
   }

   if(type == 'recmachcustrecord_prquote')
   {
   	 var i_approval_status_1 = ''
   	 i_approval_status_1 = nlapiGetCurrentLineItemValue('recmachcustrecord_prquote','custrecord_approvalstatusforquotation')
	 
	 if(i_approval_status_1 == 3)
	 {	 					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_prquote', true)		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_item', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_description', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_qty', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_uom', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_currency', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor1name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate1', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discount', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymentterms', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv1', true)				  		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor2name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_disocuntven2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor3name', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discountv3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv3', true)					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_verticalheadforquotation', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_financehead', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', true)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendorforpo', true)
	
	 }
	 else
	 {
	     nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_prquote', false)		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_item', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_description', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_qty', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_uom', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_currency', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor1name', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate1', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discount', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymentterms', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv1', false)				  		
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor2name', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_disocuntven2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendor3name', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_rate3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_discountv3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_paymenttermsv3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_otherchargesv3', false)					
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_verticalheadforquotation', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_financehead', false)
	//	 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_approvalstatusforquotation', false)
		// nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_preferred_vendor', false)
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_vendorforpo', false)	
		 
		 nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount1', true)
   
		   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount2', true)
		   
		   nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_amount3', true)
		   
		    nlapiDisableLineItemField('recmachcustrecord_prquote', 'custrecord_srno', true)
      
	 }
	 
   }

}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================
/**
 * 
 * @param {Object} type
 * 
 * Description -->  Disable the line item fields o PR ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Item , Quotation Analysis 
 * 
 */
var a_status_array = new Array();
function validateLine_disable(type)
{
		 nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prastatus', true)	
  nlapiDisableLineItemField('recmachcustrecord_purchaserequest', 'custrecord_prlineitemno', true)

  if(s_type == 'edit')
  {  	
   if (type == 'recmachcustrecord_purchaserequest') 
   {
   	  //=================================== Do not allow to add line  ====================================
	  var i_current_index = nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest');
	  
	  var i_line_no = nlapiGetLineItemCount('recmachcustrecord_purchaserequest')
		
	  if (_logValidation(i_current_index)&&_logValidation(i_line_no)) 
	  {
	  	if(i_current_index>i_line_no)
		{			
		alert(' You are not allowed to do modifications on the line .');
		return false;
		}//New Line		
	  }//Current Index
	}//Purchase Request  		
  }//Edit Type
  return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
