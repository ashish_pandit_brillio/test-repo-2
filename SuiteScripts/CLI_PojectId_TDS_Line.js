function onFieldChange(type,name)
{
	try
	{	//custcol_tdsamount
		if(type == 'expense' && name == 'custcol_tdsapply')
		{
			var index = nlapiGetCurrentLineItemIndex('expense');
			var project_id = nlapiGetLineItemValue('expense','custcol_project_entity_id',index - 1);
			nlapiSetCurrentLineItemValue('expense','custcol_project_entity_id',project_id,true,true);
			nlapiCommitLineItem('expense');
		}
		if(type == 'item' && name == 'custcol_tdsapply')
		{
			var index = nlapiGetCurrentLineItemIndex('item');
			var project_id = nlapiGetLineItemValue('item','custcol_project_entity_id',index - 1);
			nlapiSetCurrentLineItemValue('item','custcol_project_entity_id',project_id,true,true);
			nlapiCommitLineItem('item');
		}
		if(name == 'billaddress')
		{		
		var s_billaddress = nlapiGetFieldValue('billaddress'); //billzip
		var i_vendor = nlapiGetFieldValue('entity');
		var i_billzip = nlapiGetFieldValue('billzip');
		var billaddresslist = nlapiGetFieldValue('billaddresslist');
		var id_gstin_tagged = nlapiGetFieldValue('custbody_vendor_gstin');
		
		if(i_vendor){
		var vendorSearch = nlapiSearchRecord("vendor",null,
				[
		   ["internalid","anyof",i_vendor]		   
		], 
		[
		   new nlobjSearchColumn("entityid").setSort(false), 
		   new nlobjSearchColumn("custrecord_iit_address_gstn_uid","Address",null), 
		   new nlobjSearchColumn("addressinternalid","Address",null), 
		   new nlobjSearchColumn("zipcode","Address",null) //addressinternalid
		]
		);
		if(vendorSearch){	
		for(var i=0;i<vendorSearch.length;i++){
		var line_zipCode = vendorSearch[i].getValue("zipcode","Address");	
		var address_id = vendorSearch[i].getValue("addressinternalid","Address");			
		if(parseInt(address_id) == parseInt(billaddresslist))
        {
		var gstin_num = vendorSearch[i].getValue("custrecord_iit_address_gstn_uid","Address");
	
		nlapiSetFieldValue('custbody_vendor_gstin',gstin_num);
		}
		}
		}
		}
		
		return true;			
		}
		
	}
	catch(err)
	{
		nlapiLogExecution('debug','Error in the process',err);
		return err;
	}
}

function _logValidation(value) {
		if (value != null && value.toString() != null && value != ''
				&& value != undefined && value.toString() != undefined
				&& value != 'undefined' && value.toString() != 'undefined'
				&& value.toString() != 'NaN' && value != NaN) {
			return true;
		} else {
			return false;
		}
	}