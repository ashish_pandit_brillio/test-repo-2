// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_TP_Report_Display.js
	Author:     Sachin K
	Company: Aashna
	Date:  25-Aug-2014
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function genaratePivotreport(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
	        //var fromDate=request.getParameter('fromdate');
			//var toDate=request.getParameter('todate');
			//var fromsubsidiary=request.getParameter('fromsubsidiary');
			//var tosubsidiary=request.getParameter('tosubsidiary');

			var fromDate=request.getParameter('fromdate')
			var toDate=request.getParameter('todate')
			var fromsubsidiary=request.getParameter('fromsubsidiary')
			var tosubsidiary=request.getParameter('tosubsidiary')
			 var fromsubsidiaryName=nlapiLookupField('subsidiary',fromsubsidiary,'name')
			  var tosubsidiaryName=nlapiLookupField('subsidiary',tosubsidiary,'name')

			nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDate);
			nlapiLogExecution('DEBUG','In Post','toDate=='+toDate);
			nlapiLogExecution('DEBUG','In Post','fromsubsidiary=='+fromsubsidiary);
			nlapiLogExecution('DEBUG','In Post','tosubsidiary=='+tosubsidiary);

			var report_Filters= new Array();
	        var report_Column  = new Array();

			report_Filters[0]=new nlobjSearchFilter('custrecord_tp_fromdate','custrecord_tp_main','on',fromDate)
			report_Filters[1]=new nlobjSearchFilter('custrecord_tp_todate','custrecord_tp_main','on',toDate)
			report_Filters[2]=new nlobjSearchFilter('custrecord_tp_fromsubsidiary','custrecord_tp_main','is',fromsubsidiary)
			report_Filters[3]=new nlobjSearchFilter('custrecord_tp_tosubsidary','custrecord_tp_main','is',tosubsidiary)
			//nlapiLogExecution('DEBUG','In report','after filter');

     		report_Column[0]= new nlobjSearchColumn('internalid');
     		report_Column[1]= new nlobjSearchColumn('custrecord_tp_category');
     		report_Column[2]= new nlobjSearchColumn('custrecord_tp_subcategory');
     		report_Column[3]= new nlobjSearchColumn('custrecord_tp_particulars');
     		report_Column[4]= new nlobjSearchColumn('custrecord_tp_project');
     		report_Column[5]= new nlobjSearchColumn('custrecord_tp_amount');
     		report_Column[6]= new nlobjSearchColumn('custrecord_tp_location');
     		//report_Column[0]= new nlobjSearchColumn('custrecord_tp_project');


			//var reportResult = nlapiSearchRecord('customrecord_transferprice_data', 'customsearch_transferpricedatareport',report_Filters,report_Column);

			//nlapiLogExecution('DEBUG','In report','search=='+reportResult);



		//-----------------------------------------------Report Defination-----------------------------------------------------------------------------
		 var reportDefinition = nlapiCreateReportDefinition();
		   //Define the rows hierarchy and the actual column data
   		 var paymonth = reportDefinition.addRowHierarchy('custrecord_tp_category', 'Category', 'TEXT');
		 var paygroup = reportDefinition.addRowHierarchy('custrecord_tp_subcategory', 'Sub Category', 'TEXT');
	  	 var employee= reportDefinition.addRowHierarchy('custrecord_tp_particulars',  'Particulars', 'TEXT');

		 var Department= reportDefinition.addColumn('custrecord_tp_project', false, 'Project', null, 'TEXT', null);

		//Define the Column hierarchy and the actual column data
	  	var locationName = reportDefinition.addColumnHierarchy('custrecord_tp_location', 'Location', null, 'TEXT');
	  	var total = reportDefinition.addColumn('custrecord_tp_amount', true, 'Amt',locationName, 'FLOAT', null);//Value

		//-----------------------------------------------Report Defination-----------------------------------------------------------------------------
		reportDefinition.addSearchDataSource('customrecord_transferprice_data', 'customsearch_transferpricedatareport', report_Filters, report_Column,
	    {'internalID':report_Column[0], 'custrecord_tp_category':report_Column[1], 'custrecord_tp_subcategory':report_Column[2], 'custrecord_tp_particulars':report_Column[3],
	     'custrecord_tp_project':report_Column[4],'custrecord_tp_amount':report_Column[5],'custrecord_tp_location':report_Column[6]});

	    reportDefinition.setTitle('TP Report '+fromDate +' To '+toDate+': '+fromsubsidiaryName+'- '+ tosubsidiaryName)
	    //Create a form to build the report on
	    var form = nlapiCreateReportForm('TP Report '+fromDate +' To'+toDate+': '+fromsubsidiaryName+'- '+ tosubsidiaryName);

	    //Build the form from the report definition
	    var pvtTable = reportDefinition.executeReport(form);

	    //Write the form to the browser
	    response.writePage(form);

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
