/**
 * @author Jayesh
 */

function suiteletFunction_FP_ApprovalDashboard(request,response)
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		
		// design form which will be displayed to user
		var o_form_obj = nlapiCreateForm("FP Projects Pending for Approval");
		
		var a_project_filter = [['custentity_practice.custrecord_practicehead', 'anyof', i_user_logegdIn_id], 'and',
								['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];
		
		var a_columns_proj_srch = new Array();
		a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
		a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
		a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
			
		var a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		
		if (a_project_search_results)
		{
			var a_existing_proj_cap_details = new Array();
			
			//nlapiLogExecution('audit','Fp proj len:- '+a_project_search_results.length);
			for (var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var linkUrl	=	nlapiResolveURL('SUITELET', '1144', 'customdeploy_sut_fp_revrec_effrtcst_view');
				
				var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', a_project_search_results[i_pro_index].getId()]
											];
				
				var a_columns_existing_cap_srch = new Array();
				a_columns_existing_cap_srch[0] = new nlobjSearchColumn('created').setSort(true);
				a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_project');
				a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
				a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_total');
				a_columns_existing_cap_srch[4] = new nlobjSearchColumn('customer','custrecord_revenue_share_project');
				a_columns_existing_cap_srch[5] = new nlobjSearchColumn('custentity_fp_rev_rec_type','custrecord_revenue_share_project');
				
				var a_get_revenue_share = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
				if(a_get_revenue_share)
				{
					var i_existing_rcrd_status = a_get_revenue_share[0].getValue('custrecord_revenue_share_approval_status');
					
					if(i_existing_rcrd_status == 2)
					{
						nlapiLogExecution('audit','proj id:- '+a_project_search_results[i_pro_index].getId(),i_pro_index);
						var i_proj_internal_id = a_project_search_results[i_pro_index].getId();
						linkUrl = linkUrl+'&proj_id='+i_proj_internal_id+'&mode=PendingApproval&existing_rcrd_id='+a_get_revenue_share[0].getId()+'&revenue_rcrd_status='+a_get_revenue_share[0].getValue('custrecord_revenue_share_approval_status');
						
						a_existing_proj_cap_details.push({
															'i_proj_intenal_id': a_get_revenue_share[0].getValue('custrecord_revenue_share_project'),
															'i_proj_cust': a_get_revenue_share[0].getValue('customer','custrecord_revenue_share_project'),
															'f_pro_value': a_get_revenue_share[0].getValue('custrecord_revenue_share_total'),
															'i_revenue_share_rcrd_id': a_get_revenue_share[0].getId(),
															'i_revenue_share_rcrd_status': a_get_revenue_share[0].getValue('custrecord_revenue_share_approval_status'),
															's_existing_rcrd_status': a_get_revenue_share[0].getText('custrecord_revenue_share_approval_status'),
															'i_proj_rev_rec_type': a_get_revenue_share[0].getValue('custentity_fp_rev_rec_type','custrecord_revenue_share_project'),
															'suitelet_url': linkUrl
														});
					}
				}
			}
			
			//create sublist for project
			var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
			f_form_sublist.addField('i_proj_cust','select','Customer','customer').setDisplayType('inline');
			f_form_sublist.addField('i_proj_intenal_id','select','Project','job').setDisplayType('inline');
			f_form_sublist.addField('f_pro_value','currency','Project Value');
			f_form_sublist.addField('s_existing_rcrd_status','text','Project Setup Status');
			f_form_sublist.addField('i_proj_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
			f_form_sublist.addField('suitelet_url', 'url','').setLinkText('Approval Page');
			f_form_sublist.setLineItemValues(a_existing_proj_cap_details);
			
		}
		else
		{
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
		}
		
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suiteletFunction_FP_ApprovalDashboard','ERROR MESSAGE :- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}