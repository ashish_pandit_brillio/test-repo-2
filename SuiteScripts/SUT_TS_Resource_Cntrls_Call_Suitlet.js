/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_TS_Resource_Cntrls_Call_Suitlet.js
	Author      : Shweta Chopde
	Date        : 11 July 2014
	Description : Search Resource Allocation records & return the value to the Time Sheet Validation Script


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{	
	var i_cnt = 0;
	var i_resource_1;
	var i_hours;
	var d_start_date;	
	var d_end_date;
	var i_work_location;
	var d_billing_start_date;
	var d_billing_end_date;
	var is_billable;
	var a_resource_array =  new Array();
	var i_project;	
	var result;
	var d_date_start = '';	
	var d_date_end= '';	

	try
	{
		if(request.getMethod()=='GET')
		{	 	
			var i_resource=request.getParameter('custscript_resource_ts_r');
			nlapiLogExecution('DEBUG', ' suiteletFunction',' Resource -->' + i_resource);

			var i_project=request.getParameter('custscript_project_ts_r');
			nlapiLogExecution('DEBUG', ' suiteletFunction',' Project -->' + i_project);	   

			var i_hours0 = request.getParameter('custscript_hours0');

			var i_hours1 = request.getParameter('custscript_hours1');

			var i_hours2 = request.getParameter('custscript_hours2');

			var i_hours3 = request.getParameter('custscript_hours3');

			var i_hours4 = request.getParameter('custscript_hours4');

			var i_hours5 = request.getParameter('custscript_hours5');

			var i_hours6 = request.getParameter('custscript_hours6');

			var i_tran_date = request.getParameter('custscript_tran_date_ts_r');

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours0 -->' + i_hours0);	   

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours1 -->' + i_hours1);	   

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours2 -->' + i_hours2);	   

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours3 -->' + i_hours3);	   

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours4 -->' + i_hours4);	   

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours5 -->' + i_hours5);

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours6 -->' + i_hours6);	   

			nlapiLogExecution('DEBUG', ' suiteletFunction',' i_tran_date -->' + i_tran_date);	   

			var d_start_week = get_start_of_week(i_tran_date) 
			var d_end_week =  get_end_of_week(i_tran_date)
			
			nlapiLogExecution('DEBUG', ' suiteletFunction',' ---------------------- d_start_week -->' + d_start_week);
			nlapiLogExecution('DEBUG', ' suiteletFunction',' ---------------------- d_end_week -->' + d_end_week);

			d_start_week = nlapiStringToDate(d_start_week)
			d_end_week = nlapiStringToDate(d_end_week)	  

			var i_hours0_date_SOW = nlapiAddDays(d_start_week,1)

			var i_hours1_date_SOW = nlapiAddDays(d_start_week,2)

			var i_hours2_date_SOW = nlapiAddDays(d_start_week,3)

			var i_hours3_date_SOW = nlapiAddDays(d_start_week,4)

			var i_hours4_date_SOW = nlapiAddDays(d_start_week,5)

			var i_hours5_date_SOW = nlapiAddDays(d_start_week,6)

			var i_hours6_date_SOW = nlapiAddDays(d_start_week,7)

            var d_start_week_date = nlapiDateToString(d_start_week) ;			
            var d_end_week_date = nlapiDateToString(d_end_week) ;
			
			nlapiLogExecution('DEBUG', ' suiteletFunction',' ************** Start Week Date -->' + d_start_week_date);
			nlapiLogExecution('DEBUG', ' suiteletFunction',' ************* End Week Date  -->' + d_end_week_date);


			if(_logValidation(i_project))
			{		
			   var filter = new Array();
			   
			   filter[0] = new nlobjSearchFilter('resource', null, 'is',i_resource)	               
               filter[1] = new nlobjSearchFilter('project', null, 'is', i_project);
			   filter[2] = new nlobjSearchFilter('startdate',null,'onorbefore',d_start_week_date)
			   filter[3] = new nlobjSearchFilter('enddate',null,'within',d_start_week_date,d_end_week_date)
	       									
			   var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);

              nlapiLogExecution('DEBUG', ' suiteletFunction',' Search Results  1 -->' + i_search_results);

                if (!_logValidation(i_search_results)) 
				{
				  var filter = new Array();
				   filter[0] = new nlobjSearchFilter('resource', null, 'is',i_resource)	            
	               filter[1] = new nlobjSearchFilter('project', null, 'is', i_project);
				   filter[2] = new nlobjSearchFilter('startdate',null,'onorbefore',d_start_week_date)
				   filter[3] = new nlobjSearchFilter('enddate',null,'onorafter',d_end_week_date)
												
				  var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
				
				}
                  nlapiLogExecution('DEBUG', ' suiteletFunction',' Search Results  2 -->' + i_search_results);


                if (!_logValidation(i_search_results)) 
				{
				   var filter = new Array();
				   filter[0] = new nlobjSearchFilter('resource', null, 'is',i_resource)	            
	               filter[1] = new nlobjSearchFilter('project', null, 'is', i_project);
				   filter[2] = new nlobjSearchFilter('startdate',null,'within',d_start_week_date,d_end_week_date)
	       		   filter[3] = new nlobjSearchFilter('enddate',null,'onorafter',d_end_week_date)
												
				   var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
				
				}
				  nlapiLogExecution('DEBUG', ' suiteletFunction',' Search Results  3 -->' + i_search_results);

                if (!_logValidation(i_search_results)) 
				{
				   var filter = new Array();
				   filter[0] = new nlobjSearchFilter('resource', null, 'is',i_resource)	            
	               filter[1] = new nlobjSearchFilter('project', null, 'is', i_project);
				   filter[2] = new nlobjSearchFilter('startdate',null,'within',d_start_week_date,d_end_week_date)
	       		   filter[3] = new nlobjSearchFilter('enddate',null,'within',d_start_week_date,d_end_week_date)
												
				   var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
				
				}
              nlapiLogExecution('DEBUG', ' suiteletFunction',' Search Results  4 -->' + i_search_results);


				if (_logValidation(i_search_results)) 
				{
				
	            nlapiLogExecution('DEBUG', ' suiteletFunction',' Search Results  -->' + i_search_results.length);
				nlapiLogExecution('DEBUG', 'Count records', i_search_results.length);
				
					for (var c = 0; c < i_search_results.length; c++) 
					{
						var a_search_transaction_result = i_search_results[c];
						var columns = a_search_transaction_result.getAllColumns();
						var columnLen = columns.length;

						for (var hg = 0; hg < columnLen; hg++) 
						{
							var column = columns[hg];
							var label = column.getLabel();
							var value = a_search_transaction_result.getValue(column)
							var text = a_search_transaction_result.getText(column)				

							if(label =='project')
							{
								i_project = value;
							}				

							if(label =='Resource')
							{
								i_resource_1 = value;
							}
							if(label =='Hours')
							{
								i_hours = value;
							}
							if(label =='Start Date')
							{
								d_start_date = value;
							}
							if(label =='End Date')
							{
								d_end_date = value;
							}
							if(label =='Work Loctaion')
							{
								i_work_location = value;
							}	
							if(label =='Billing Start Date')
							{
								d_billing_start_date = value;
							}
							if(label =='Billing End Date')
							{
								d_billing_end_date = value;
							}	
							if(label =='Billable')
							{
								is_billable = value;
							}				
						}	
				nlapiLogExecution('DEBUG', 'ccccccccccccccccc','i_resource', i_resource);
			
				nlapiLogExecution('DEBUG', 'ccccccccccccccccc','i_resource_1', i_resource_1);
					
				//		if(i_resource == i_resource_1)
						{
							a_resource_array[i_cnt++] = d_start_date+'&&&'+d_end_date+'&&&'+is_billable;				
						}	
					}
				}
			}//Project
	
			nlapiLogExecution('DEBUG', 'Resource Array', a_resource_array);
			var result;
			var a_resource_allocated = a_resource_array; 
			nlapiLogExecution('DEBUG', ' suiteletFunction',' a_resource_allocated -->' + a_resource_allocated);	   

			var a_split_array =  new Array();
			
			if (_logValidation(a_resource_allocated)) 
			{
			      for (var b = 0; b <=1; b++) 
				  {
				  	a_split_array = a_resource_allocated[b].split('&&&');
				  	
				  	var d_start_date_1 = a_split_array[0];
				  	
				  	var d_end_date_1 = a_split_array[1];
				  	
				   	d_start_date_1 = nlapiStringToDate(d_start_date_1);
				  	
				  	d_end_date_1 = nlapiStringToDate(d_end_date_1);
					
					d_date_start = d_start_date_1;
					
					d_date_end = d_end_date_1;
					break;
				  					  
				  }		
				  
				 // nlapiLogExecution('DEBUG', ' suiteletFunction','d_date_start ........'+d_date_start);	 
					
				  var d_start_date_1_add = nlapiAddDays(d_date_start,1)
				//  nlapiLogExecution('DEBUG', ' suiteletFunction',' Month Add start........'+d_start_date_1_add);	 
						
				 //  nlapiLogExecution('DEBUG', ' suiteletFunction','d_date_end ........'+d_date_end);	 
					
				  var d_date_end_1_add = nlapiAddDays(d_date_end,1)
			//	  nlapiLogExecution('DEBUG', ' suiteletFunction',' Month Add End........'+d_date_end_1_add);	 
					
				/*
                 d_start_date_1_add = nlapiDateToString(d_start_date_1_add)
				 d_start_date_1_add = nlapiStringToDate(d_start_date_1_add);	
					
				 d_date_end_1_add = nlapiDateToString(d_date_end_1_add)
				 d_date_end_1_add = nlapiStringToDate(d_date_end_1_add);	
                 */
										
				  for (var b = 0; b <a_resource_allocated.length; b++) 
				  {
				  	a_split_array = a_resource_allocated[b].split('&&&');
				  	
				  	var d_start_date_1 = a_split_array[0];
				  	
				  	var d_end_date_1 = a_split_array[1];
				  	
				   	d_start_date_1 = nlapiStringToDate(d_start_date_1);
				  	
				  	d_end_date_1 = nlapiStringToDate(d_end_date_1);
										
					nlapiLogExecution('DEBUG', ' suiteletFunction',' d_start_date_1 ........'+d_start_date_1);	 
					nlapiLogExecution('DEBUG', ' suiteletFunction',' d_end_date_1 ........'+d_end_date_1);	 
					nlapiLogExecution('DEBUG', ' suiteletFunction',' Month Add End........'+d_date_end_1_add);	 
				    nlapiLogExecution('DEBUG', ' suiteletFunction',' Month Add start........'+d_start_date_1_add);	 
						
				/*
	                var d_start_date_1_up = nlapiDateToString(d_start_date_1);
					var d_end_date_1_up = nlapiDateToString(d_end_date_1);
					d_date_end_1_add = nlapiDateToString(d_date_end_1_add);
					d_start_date_1_add = nlapiDateToString(d_start_date_1_add);
						
               */
					nlapiLogExecution('DEBUG', ' suiteletFunction',' d_start_date_1 ........'+d_start_date_1);	 
					nlapiLogExecution('DEBUG', ' suiteletFunction',' d_end_date_1 ........'+d_end_date_1);	 
					nlapiLogExecution('DEBUG', ' suiteletFunction',' Month Add End........'+d_date_end_1_add);	 
				    nlapiLogExecution('DEBUG', ' suiteletFunction',' Month Add start........'+d_start_date_1_add);	 
							
										
					if(d_date_end_1_add == d_start_date_1)
					{
						nlapiLogExecution('DEBUG', ' suiteletFunction',' Month In Continuation Start ........');	   
					}
					if(d_date_end_1_add == d_end_date_1)
					{
						nlapiLogExecution('DEBUG', ' suiteletFunction',' Month In Continuation End ........');	   
					}									  					  
				  }					
			}       
			if (_logValidation(a_resource_allocated)) 
			{
				nlapiLogExecution('DEBUG', '1', '1');
				for (var b = 0; b < a_resource_allocated.length; b++) 
				{
					a_split_array = a_resource_allocated[b].split('&&&');

					var d_start_date_1 = a_split_array[0];

					var d_end_date_1 = a_split_array[1];

					var is_billable_1 = a_split_array[2];			

					d_start_date_1 = nlapiStringToDate(d_start_date_1);

					d_end_date_1 = nlapiStringToDate(d_end_date_1);

					nlapiLogExecution('DEBUG', ' suiteletFunction',' d_start_date_1 -->' + d_start_date_1);	   
					nlapiLogExecution('DEBUG', ' suiteletFunction',' d_end_date_1 -->' + d_end_date_1);	   
					nlapiLogExecution('DEBUG', ' suiteletFunction',' i_hours0_date_SOW -->' + i_hours0_date_SOW);	   
					
					if (_logValidation(i_hours0))
					{				   
						if((i_hours0_date_SOW <= d_end_date_1 && i_hours0_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 0......');	
						} 
						else
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Else Hours 0......'+!(i_hours0_date_SOW >= d_end_date_1 && i_hours0_date_SOW >= d_start_date_1));
						   if(!(i_hours0_date_SOW >= d_end_date_1 && i_hours0_date_SOW >= d_start_date_1))
						   {
						   	nlapiLogExecution('DEBUG', '1', 'Hours 0');
							result = 'Resource is not allocated for this period .';						   	
						   }							
						}
					}//i_hours0

					if (_logValidation(i_hours1))
					{	   	
						if((i_hours1_date_SOW <= d_end_date_1 && i_hours1_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 1 ......');	
						} 
						else
						{
							 if(!(i_hours1_date_SOW >= d_end_date_1 && i_hours1_date_SOW >= d_start_date_1))
							 {
							 	nlapiLogExecution('DEBUG', '1', '4');
						    	result = 'Resource is not allocated for this period .';
							 }							
						}	
					}//i_hours1

					if (_logValidation(i_hours2))
					{	   	
						if((i_hours2_date_SOW <= d_end_date_1 && i_hours2_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 2......');	
						} 
						else
						{
							if(!(i_hours2_date_SOW >= d_end_date_1 && i_hours2_date_SOW >= d_start_date_1))
							{
							nlapiLogExecution('DEBUG', '1', '5');
							result = 'Resource is not allocated for this period .';	
							}							
						}	
					}//i_hours2
				
					if (_logValidation(i_hours3))
					{	   	
						if((i_hours3_date_SOW <= d_end_date_1 && i_hours3_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 3......');	
						} 
						else
						{
							if(!(i_hours3_date_SOW >= d_end_date_1 && i_hours3_date_SOW >= d_start_date_1))
							{
							nlapiLogExecution('DEBUG', '1', '6');
							result = 'Resource is not allocated for this period .';
							}							
						}	
					}//i_hours3

					if (_logValidation(i_hours4))
					{	   	
						if((i_hours4_date_SOW <= d_end_date_1 && i_hours4_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 4......');	
						} 
						else
						{
							if(!(i_hours4_date_SOW >= d_end_date_1 && i_hours4_date_SOW >= d_start_date_1))
							{
							nlapiLogExecution('DEBUG', '1', '7');
							result = 'Resource is not allocated for this period .';
							}							
						}	
					}//i_hours4

					if (_logValidation(i_hours5))
					{	   	
						if((i_hours5_date_SOW <= d_end_date_1 && i_hours5_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 5......');	
						} 
						else
						{
							if(!(i_hours5_date_SOW >= d_end_date_1 && i_hours5_date_SOW >= d_start_date_1))
							{
							nlapiLogExecution('DEBUG', '1', '8');
							result = 'Resource is not allocated for this period .';	
							}
							
						}	
					}//i_hours5

					if (_logValidation(i_hours6))
					{	   	
						if((i_hours6_date_SOW <= d_end_date_1 && i_hours6_date_SOW >= d_start_date_1))
						{
							nlapiLogExecution('DEBUG', ' suiteletFunction',' Date is present Hours 6 ......');	
						} 
						else
						{
							if(!(i_hours6_date_SOW >= d_end_date_1 && i_hours6_date_SOW >= d_start_date_1))
							{
							nlapiLogExecution('DEBUG', '1', '9');
							result = 'Resource is not allocated for this period .';
							}
							
						}	
					}//i_hours5
				}
			}
			else{
				result = 'Resource is not allocated for this period .';
			}
			
		}
	}
    catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
	nlapiLogExecution('DEBUG', ' suiteletFunction',' result -->' +result);
	response.write(result);
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function _logValidation(value) 
{
	if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}


// return an array of date objects for start (monday)
// and end (sunday) of week based on supplied 
// date object or current date
function get_start_of_week(date) 
{
  // If no date object supplied, use current date
  // Copy date so don't modify supplied date
 var now  = nlapiStringToDate(date)

  // set time to some convenient value
  now.setHours(0,0,0,0);

  // Get the previous Monday
  var monday = new Date(now);
  monday.setDate(monday.getDate() - monday.getDay());
  monday = nlapiDateToString(monday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> ' + monday);
  
  // Get next Sunday
  var sunday = new Date(now);
  sunday.setDate(sunday.getDate() - sunday.getDay() +7);
  sunday = nlapiDateToString(sunday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> ' + sunday);
  // Return array of date objects
  return monday;
}

function get_end_of_week(date) 
{
  // If no date object supplied, use current date
  // Copy date so don't modify supplied date
 var now  = nlapiStringToDate(date)

  // set time to some convenient value
  now.setHours(0,0,0,0);

  // Get the previous Monday
  var monday = new Date(now);
  monday.setDate(monday.getDate() - monday.getDay());
  monday = nlapiDateToString(monday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> ' + monday);
  
  // Get next Sunday
  var sunday = new Date(now);
  sunday.setDate(sunday.getDate() - sunday.getDay() +7);
  sunday = nlapiDateToString(sunday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> ' + sunday);
  // Return array of date objects
  return sunday;
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
