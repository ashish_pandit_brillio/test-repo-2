function scheduled()
{
	try
	{
		var date = new Date();
		var enddate = nlapiAddDays(date,15);
		enddate = nlapiStringToDate(nlapiDateToString(enddate));
		
		//Search Project Ending After 15 days
		var jobSearch = nlapiSearchRecord("job",null,
			[
				["status","noneof","1"], 
				"AND", 
				["enddate","on",enddate],
				   "AND", 
				["type","anyof","2"]
			], 
			[
				new nlobjSearchColumn("internalid"), 
				new nlobjSearchColumn("customer"),
				new nlobjSearchColumn("entityid").setSort(false), 
				new nlobjSearchColumn("companyname"),  
				new nlobjSearchColumn("custentity_clientpartner"),
				new nlobjSearchColumn("email","CUSTENTITY_CLIENTPARTNER",null),
				new nlobjSearchColumn("firstname","CUSTENTITY_CLIENTPARTNER",null),
				new nlobjSearchColumn("email","CUSTENTITY_DELIVERYMANAGER",null),
				new nlobjSearchColumn("email","CUSTENTITY_PROJECTMANAGER",null),
				new nlobjSearchColumn("enddate"),
				new nlobjSearchColumn("custentity_region")
			]
			);
			
			
			if(_logValidation(jobSearch))
			{
					for(var i = 0; i < jobSearch.length; i++)
					{
						var id = jobSearch[i].getValue('internalid');
						var Client_partner = jobSearch[i].getValue('email','CUSTENTITY_CLIENTPARTNER',null);
						var Client_partner_name = jobSearch[i].getValue('firstname','CUSTENTITY_CLIENTPARTNER',null);
					    var delivery_manager_email = jobSearch[i].getValue('email','CUSTENTITY_DELIVERYMANAGER',null);
						var project_name = jobSearch[i].getValue('companyname');
						var name = project_name.split(":");
						projectname = name[1];
						var project_end_date = jobSearch[i].getValue('enddate');
						var project_id = jobSearch[i].getValue('entityid');
						var project_region = jobSearch[i].getText('custentity_region');
						var customer_name = jobSearch[i].getText('customer');
						var project_manager_email = jobSearch[i].getValue('email','CUSTENTITY_PROJECTMANAGER',null);
						
						var strVar = '';
							strVar += '<html>';
							strVar += '<body>';
							strVar += '<p>Dear '+ Client_partner_name + ',</p>';
							strVar += '<p>As per the system the project ' + '<b>'+ projectname + '</b>' + ' and the allocation for the listed resources are ending as on ' +  '<b>' +project_end_date + '</b>' +' in Netsuite.<p/>';
							strVar += '<p>Request you to kindly share the Extension document i.e. SOW/PO or Updated CR with Business Operations team at the earliest , if the project is getting extend beyond the current end date. Thanks!.</p>';
							
							strVar += '<table>';
		
							strVar += '	<tr>';
							strVar += ' <td width="100%">';
							strVar += '<table width="100%" border="1">';
							
							strVar += '	<tr>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Project Region</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Customer</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Project ID</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Project Name</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Resource Name</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>% Allocation</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Location</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Allocation Start Date</b></td>';
							strVar += ' <td width="6%" font-size="11" align="center"><b>Allocation End Date</b></td>';
						
						//Search for allocation under the current project
						var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
						[
							["job.internalid","anyof",id], 
								"AND", 
							["enddate","notbefore",date]
						], 
						[
							new nlobjSearchColumn("resource"), 
							new nlobjSearchColumn("startdate"), 
							new nlobjSearchColumn("enddate"), 
							new nlobjSearchColumn("custeventwlocation"),
							new nlobjSearchColumn("percentoftime")
						]
						);
						
						if(_logValidation(resourceallocationSearch))
						{
							for(var j = 0 ; j < resourceallocationSearch.length ; j++)
							{
								var resource = resourceallocationSearch[j].getText('resource');
								var allocation_start_date = resourceallocationSearch[j].getValue('startdate');
								var allocation_end_date = resourceallocationSearch[j].getValue('enddate');
								var location = resourceallocationSearch[j].getText('custeventwlocation');
								var percent_of_time = resourceallocationSearch[j].getValue('percentoftime');
							
								strVar += '	<tr>';
								strVar += ' <td width="6%" font-size="11" align="center">' +project_region+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +customer_name+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +project_id+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +projectname+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +resource+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +percent_of_time+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +location+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +allocation_start_date+ '</td>';
								strVar += ' <td width="6%" font-size="11" align="center">' +allocation_end_date+ '</td>';
								strVar += '	</tr>';

							}	
							
						}	
						strVar += '</table>'
						strVar += '<p>Thanks & Regards,</p>';
						strVar += '<p>Business Operations</p>';
						strVar += '</body>';
						strVar += '</html>';
						var cc = []
						cc[0] = delivery_manager_email;
						cc[1] = project_manager_email;
						cc[2] = 'business.ops@brillio.com';
						//var bcc = 'shamanth.k@brillio.com';
						nlapiSendEmail(15279,Client_partner,'Project Ending Reminder',strVar,cc,null,null,null);
				}	
			}	
	}
	
	catch (err)
	{
		nlapiLogExecution('Error','Error in the main block',err);
	}
}


function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}