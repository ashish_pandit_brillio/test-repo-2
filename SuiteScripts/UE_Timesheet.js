/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 May 2015     nitish.mishra
 * 2.00		  23 Mar 2020 	  Praveena 		  Changed logic to fetch values from timeitem sublist instead of getting data from timeentries
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	try {
		var old_record = nlapiGetOldRecord();
		var old_approval_status = old_record.getFieldValue('approvalstatus');
        nlapiLogExecution('debug',old_approval_status);
		var new_approval_status = nlapiGetFieldValue('approvalstatus');
        nlapiLogExecution('debug',new_approval_status);

		// if (old_approval_status != new_approval_status) {

		switch (new_approval_status) {

			case "4": // rejected
				sendTimesheetRejectedMail();
			break;
		}
		// }
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function sendTimesheetRejectedMail() {
	try {

		// loop through all the timeentry and create a list of the rejected ones
		var rejected_entry_list = [];
//commented by praveena on 24-03-2020
	/*	var line_item_count = nlapiGetLineItemCount('timegrid');
		var days_list = [ 'sunday', 'monday', 'tuesday', 'wednesday',
		        'thursday', 'friday', 'saturday' ];

		for (var linenum = 1; linenum <= line_item_count; linenum++) {

			for (var i = 0; i < 7; i++) {
				var day = days_list[i];
				var subrecord_view = nlapiViewLineItemSubrecord('timegrid',
				        day, linenum);

				if (subrecord_view) {
					var subrecord_status = subrecord_view
					        .getFieldValue('approvalstatus');

					if (subrecord_status == "4") {
						rejected_entry_list.push({
						    Date : subrecord_view.getFieldValue('day'),
						    Project : subrecord_view
						            .getFieldValue('custrecordprj_name_ts')
						});
					}
				}
			}
		}
*/

//Changed sublist and structire of code by praveena on 24-03-2020
nlapiLogExecution('debug',"entered");
var line_item_count = nlapiGetLineItemCount('timeitem');
		var days_list=['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];//replaced timebill array added by praveena
		var s_trandate=nlapiGetFieldValue('startdate');
		var d_trandate=s_trandate ? nlapiStringToDate(s_trandate) : "";
		for (var linenum = 1; linenum <= line_item_count; linenum++) {

			for (var i = 0; i < 7; i++) {
				var day = days_list[i];
				var subrecord_view = nlapiGetLineItemValue('timeitem',day, linenum);

				if (subrecord_view) {
					var subrecord_status = nlapiGetLineItemValue('timeitem','statushours'+i, linenum);
					

					if (subrecord_status == "4") {
						
						rejected_entry_list.push({
						    Date : d_trandate ? nlapiDateToString(nlapiAddDays(d_trandate,i)) : "",//Get the Time bill date
						    Project : nlapiGetLineItemValue('timeitem','custcolprj_name', linenum)
						   
						});
					}
				}
			}
		}






		var start_date = nlapiGetFieldValue('startdate');
		var end_date = nlapiGetFieldValue('enddate');
		var employee_id = nlapiGetFieldValue('employee');
		var emp_details = nlapiLookupField('employee', employee_id, [
		        'firstname', 'email', 'timeapprover' ]);

		var timeApproverEmail = nlapiLookupField('employee',
		        emp_details.timeapprover, 'email');

		// get the email template
		var mailTemplate = getTimesheetRejectionTemplate(emp_details.firstname,
		        start_date, end_date, rejected_entry_list);

		nlapiSendEmail('442', emp_details.email, mailTemplate.MailSubject,
		        mailTemplate.MailBody, [ constant.EmailId.TimeSheet,
		                timeApproverEmail ], null, {
			        entity : employee_id
		        });

		nlapiLogExecution('debug', 'email send', emp_details.email);
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendTimesheetRejectedMail', err);
		throw err;
	}
}

function getTimesheetRejectionTemplate(firstName, weekStartDate, weekEndDate,
        rejectedTimeEntryDetails)
{
	var htmltext = '';

	htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + firstName + ',</p>';

	htmltext += '<p>Your timesheet for the week <b>' + weekStartDate
	        + '</b> - <b>' + weekEndDate
	        + '</b> has been rejected for the following days : </p>';

	htmltext += "<table>";
	rejectedTimeEntryDetails.forEach(function(day) {
		htmltext += "<tr><td>" + day.Date + "</td></tr>";
	});
	htmltext += "</table>";

	htmltext += "<p>Please contact the timesheet team to get your timesheet updated.</p>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
	    MailBody : htmltext,
	    MailSubject : "Timesheet Rejected :" + weekStartDate + " - "
	            + weekEndDate
	};
}

function actionScript() {
	nlapiLogExecution('debug', 'working', new Date());
	sendTimesheetRejectedMail();
}