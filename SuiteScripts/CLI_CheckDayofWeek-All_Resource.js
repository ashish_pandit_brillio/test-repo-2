// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:CLI_CheckDayofWeek.js
     Author:Rujuta K
     Company:
     Date:
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================


// END PAGE INIT ====================================================

function pageInit(type){

}

function fxn_generatePDF(param){
    //alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL = nlapiResolveURL("SUITELET", 'customscriptsut_report_salnonstdrptfromt', 'customdeploy1', false);
    
    //pass the internal id of the current record
    createPDFURL += '&id=' + 'Export' + "&param=" + param;
    // alert(createPDFURL);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL);
}

function fxn_generatePDF_Dec(param){
    //alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL = nlapiResolveURL("SUITELET", 'customscript_diff_report_all', 'customdeploy1', false);
    
    //pass the internal id of the current record
    createPDFURL += '&id=' + 'Export' + "&param=" + param;
    // alert(createPDFURL);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL);
}
function fxn_generateHourPDF(param){
    alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscriptsut_report_hourlyreportfromt', 'customdeploysut_report_hourlyreportfromt', false);
    
    //pass the internal id of the current record
    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
     alert(createPDFURL1);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL1);
}

function Callsuitelet(param){


    var count = nlapiGetLineItemCount('record');
    //alert(count);
    
    var globalArray = new Array();
    var arr = new Array();
    var month1 = nlapiGetFieldText('custpage_startdate');
    
    var curUser = nlapiGetUser()
    // alert(month1)
    for (var i = 1; i <= count; i++) {
        var cocode = nlapiGetLineItemValue('record', 'custevent_cocode', i);
        //alert(custevent_pruchesdetalis);
        var batchid = nlapiGetLineItemValue('record', 'custevent_batchid', i);
        //alert(custevent_devicesold);
        var file = nlapiGetLineItemValue('record', 'custevent_file', i);
        //alert('custevent_month0' + custevent_month0);
        var firstname = nlapiGetLineItemValue('record', 'custevent_firstname', i);
        //alert('custevent_month1' + custevent_month1);
        var lastname = nlapiGetLineItemValue('record', 'custevent_lastname', i);
        //alert('custevent_month2' + custevent_month2);
        var wkst1 = nlapiGetLineItemValue('record', 'custevent_wkst1', i);
        //alert('custevent_month3' + custevent_month3);
        var wkot1 = nlapiGetLineItemValue('record', 'custevent_wkot1', i);
        //alert('custevent_month4' + custevent_month4);
        var wkdt1 = nlapiGetLineItemValue('record', 'custevent_wkdt1', i);
        //alert('custevent_month5' + custevent_month5);
        var wk1timeoff1 = nlapiGetLineItemValue('record', 'custevent_wk1timeoff1', i);
        //alert('custevent_month6' + custevent_month6);
        var wkst2 = nlapiGetLineItemValue('record', 'custevent_wkst2', i);
        //alert('custevent_month7' + custevent_month7);
        var wkot2 = nlapiGetLineItemValue('record', 'custevent_wkot2', i);
        //alert('custevent_month8' + custevent_month8);
        var wkdt2 = nlapiGetLineItemValue('record', 'custevent_wkdt2', i);
        //alert('custevent_month9' + custevent_month9);
        var wk1timeoff2 = nlapiGetLineItemValue('record', 'custevent_wk1timeoff2', i);
        //alert('custevent_month10' + custevent_month10);
        var totaltimeoff = nlapiGetLineItemValue('record', 'custevent_totaltimeoff', i);
        var floatingh = nlapiGetLineItemValue('record', 'custevent_floatingh', i);
        var projectcity = nlapiGetLineItemValue('record', 'custevent_projectcity', i);
        var employeetype = nlapiGetLineItemValue('record', 'custevent_employeetype', i);
        var usernotes = nlapiGetLineItemValue('record', 'custevent_usernotes', i);
        usernotes = usernotes.toString().replace(/[|]/g, " ")
        usernotes = usernotes.toString().replace(/[,]/g, " ")
        //usernotes = nlapiEscapeXML(usernotes)
        var employeeid = nlapiGetLineItemValue('record', 'custevent_employeeid', i);
        var department = nlapiGetLineItemValue('record', 'custevent_department', i);
        var projectloc = nlapiGetLineItemValue('record', 'custevent_projectloc', i);
        if (i == 1) {
            arr[i] = "Co Code,Batch ID,File #,Employee ID,First Name,Last Name,WK1 ST,WK1 OT,WK1 DT,WK1 TIMEOFF,WK2 ST,WK2 OT,WK2 TIMEOFF,OT Hours,Total Timeoff,Float H(if in either wk),Project Location,Project_City,Employee_Type,Department,User Notes\n";
        }
        else {
            arr[i] = cocode + "," + batchid + "," + file + "," + employeeid + "," + firstname + "," + lastname + "," + wkst1 + "," + wkot1 + "," + wkdt1 + "," + wk1timeoff1 + "," + wkst2 + "," + wkot2 + "," + wkdt2 + "," + wk1timeoff2 + "," + totaltimeoff + ',' + floatingh + ',' + projectloc + "," + projectcity + ',' + employeetype + ',' + department + "," + usernotes + "\n";
            
        }
        globalArray[i] = (arr[i]);
    }
    ///alert(globalArray)
    
    
    var param = new Array();
    param.push(globalArray.toString())
    ///alert(globalArray)
    ////param['custscriptcustscript_csvdata1'] = globalArray.toString()
    
    ////param['custscriptcustscript_csvdata1']='export';
    
    // var url = nlapiResolveURL('SUITELET', 'customscriptsut_salnonstdrptfromts', 'customdeploy1', null)
    ///nlapiRequestURL('https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=260&deploy=1&compid=3883006&h=d007f67ff1133ebb4404', param, null, 'POST', null)
    //alert('suitlet called')globalArray
    // window.open(url + "&custscript_csvdata" + param)
    window.open('https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=264&deploy=1&compid=3883006&h=97e904225d423bb2487d');
    //window.open('https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=260&deploy=1&compid=3883006&h=d007f67ff1133ebb4404')

    ///nlapiSetRedirectURL('SUITELET', 'customscriptsut_report_salnonstdrptfromt', 'customdeploy1', false, params);
}

function fieldChanged(type, name, linenum){


    if (name == 'custpage_startdate') {
        var s_period = nlapiGetFieldValue('custpage_startdate')
        //alert("S_period=======" + s_period)
        if (s_period != null && s_period != '' && s_period != 'undefined') {
            var date = nlapiStringToDate(s_period)
            //alert("date=======" + date)
            if (date != null && date != '' && date != 'undefined') {
                var day = date.getDay()
                //alert("day=======" + day)
                if (day != 6) {
                    alert("Please select enddate as Saturday.")
                    nlapiSetFieldValue('custpage_startdate', '')
                }
            }
        }
        
    }
 
}








// BEGIN SAVE RECORD ================================================
// END FIELD CHANGED ================================================

