/**
 * Send Notification Mails to PM / DM for any allocation that is ending in 30 /
 * 15 / 3 days.
 * 
 * Version Date Author Remarks 1.00 22 Dec 2015 nitish.mishra
 * 
 */

function scheduled(type) {
	try {
		processAllocationExtension(30);
		processAllocationExtension(15);
		processAllocationExtension(3);

		// allocationEndingIn30Days();
		// allocationEndingIn15Days();
		// allocationEndingIn3Days();
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

// ---------- 30 days allocation mail - start

function allocationEndingIn30Days() {
	try {
		var allocationSearch = searchRecord('resourceallocation', null,
		        [ new nlobjSearchFilter('enddate', null, 'on',
		                'thirtydaysfromnow') ], [
		                new nlobjSearchColumn('company').setSort(),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime') ]);

		if (allocationSearch && allocationSearch.length > 0) {
			nlapiLogExecution('debug', '30 days', allocationSearch.length
			        + ' allocations found');

			var mailText = "";

			for (var i = 0; i < allocationSearch.length; i++) {
				mailText += "<tr>";
				mailText += "<td>" + allocationSearch[i].getText('resource')
				        + "</td>";
				mailText += "<td>" + allocationSearch[i].getValue('startdate')
				        + "</td>";
				mailText += "<td>" + allocationSearch[i].getValue('enddate')
				        + "</td>";
				mailText += "<td>"
				        + (allocationSearch[i].getValue('custeventrbillable') == 'T' ? 'Yes'
				                : 'No') + "</td>";
				mailText += "</tr>";

				var currentProject = allocationSearch[i].getValue('company');
				var nextProject = "";

				if (i + 1 < allocationSearch.length) {
					nextProject = allocationSearch[i + 1].getValue('company');
				} else {
					nextProject = "last project";
				}

				nlapiLogExecution('debug', 'current Project', currentProject);
				nlapiLogExecution('debug', 'next project', nextProject);

				if (nextProject == currentProject) {
					nlapiLogExecution('debug', '30 days',
					        'next project is same');
				} else {
					nlapiLogExecution('debug', '30 days',
					        'next project is different, so send the mail');

					// send the current mail text to the PM
					send30DaysMail(mailText, currentProject);

					// reset the mail text
					mailText = "";
				}
			}
		} else {
			nlapiLogExecution('debug', '30 days', 'no allocation found');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'allocationEndingIn30Days', err);
		throw err;
	}
}

function send30DaysMail(mailText, projectId) {
	var allocationDetailsTable = "<table class='mailText mailTable'>";
	allocationDetailsTable += "<tr>";
	allocationDetailsTable += "<td><b>Resource</b></td>";
	allocationDetailsTable += "<td><b>Start Date</b></td>";
	allocationDetailsTable += "<td><b>End Date</b></td>";
	allocationDetailsTable += "<td><b>Billable</b></td>";
	allocationDetailsTable += "</tr>";
	allocationDetailsTable += mailText;
	allocationDetailsTable += "</table>";

	nlapiLogExecution('debug', 'allocation table', allocationDetailsTable)

	var projectDetails = nlapiLookupField('job', projectId, [ 'entityid',
	        'altname', 'custentity_projectmanager.email',
	        'custentity_projectmanager.firstname',
	        'custentity_deliverymanager.email',
	        'custentity_clientpartner.email', 'enddate' ]);

	var projectsDetailTable = "<p class='mailText'>";
	projectsDetailTable += "<b>Project : </b>";
	projectsDetailTable += projectDetails.entityid + " "
	        + projectDetails.altname + "<br/>";
	projectsDetailTable += "<b>Project End Date : </b>";
	projectsDetailTable += projectDetails.enddate + "<br/>";
	projectsDetailTable += "</p>";

	var link = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=619&deploy=1&customer=&project="
	        + projectId + "&startdate=&enddate=&status=2";

	var mailBody = "<style> .mailText { font-size : 15px } .mailTable tr td { border : 1px solid black; }</style>";
	mailBody += "<p class='mailText'>Hi "
	        + projectDetails['custentity_projectmanager.firstname'] + ", </p>";

	mailBody += "<p class='mailText'>Following resources are ending the allocation as per the dates listed below.</p>";
	mailBody += "<p class='mailText'>Please share the extended SOW / Amendment with Business Ops team"
	        + " to have them extended in the systems to avoid any timesheet issues and last minute rush."
	        + " Please create allocation tickets for any changes.</p>";

	mailBody += projectsDetailTable;
	mailBody += allocationDetailsTable;

	mailBody += "<p class='mailText'>Regards, <br/>Information Systems</p>";

	mailBody = addMailTemplate(mailBody);

	nlapiSendEmail('442', 'nitish.mishra@brillio.com', // projectDetails['custentity_projectmanager.email'],
	'Allocations Ending After 30 days', mailBody);

	nlapiLogExecution('debug', '30 days', 'mail send');
}

// ---------- 15 days allocation mail - start

function allocationEndingIn15Days() {
	try {
		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        [ new nlobjSearchFilter('enddate', null, 'on', 'daysfromnow15') ],
		        [ new nlobjSearchColumn('company').setSort(),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime') ]);

		nlapiLogExecution('debug', '15 days', allocationSearch);

		if (allocationSearch && allocationSearch.length > 0) {
			nlapiLogExecution('debug', '15 days', allocationSearch.length
			        + ' allocations found');

			var mailText = "";

			for (var i = 0; i < allocationSearch.length; i++) {
				var resource = allocationSearch[i].getValue('resource');
				var allocationEndDate = allocationSearch[i].getValue('enddate');

				mailText += "<tr>";
				mailText += "<td>" + allocationSearch[i].getText('resource')
				        + "</td>";
				mailText += "<td>" + allocationSearch[i].getValue('startdate')
				        + "</td>";
				mailText += "<td>" + allocationSearch[i].getValue('enddate')
				        + "</td>";
				mailText += "<td>"
				        + (allocationSearch[i].getValue('custeventrbillable') == 'T' ? 'Yes'
				                : 'No') + "</td>";
				mailText += "</tr>";

				var currentProject = allocationSearch[i].getValue('company');
				var nextProject = "";

				if (i + 1 < allocationSearch.length) {
					nextProject = allocationSearch[i + 1].getValue('company');
				} else {
					nextProject = "last project";
				}

				nlapiLogExecution('debug', 'current Project', currentProject);
				nlapiLogExecution('debug', 'next project', nextProject);

				if (nextProject == currentProject) {
					nlapiLogExecution('debug', '15 days',
					        'next project is same');
				} else {
					nlapiLogExecution('debug', '15 days',
					        'next project is different, so send the mail');

					// send the current mail text to the PM
					sendExtensionRequestMail(mailText, currentProject,
					        '15 days Allocation Ending', 15);

					// reset the mail text
					mailText = "";
				}
			}
		} else {
			nlapiLogExecution('debug', '15 days', 'no allocation found');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'allocationEndingIn15Days', err);
		throw err;
	}
}

function send15DaysMail(mailText, projectId) {
	var allocationDetailsTable = "<table class='mailText mailTable'>";
	allocationDetailsTable += "<tr>";
	allocationDetailsTable += "<td><b>Resource</b></td>";
	allocationDetailsTable += "<td><b>Start Date</b></td>";
	allocationDetailsTable += "<td><b>End Date</b></td>";
	allocationDetailsTable += "<td><b>Billable</b></td>";
	allocationDetailsTable += "</tr>";
	allocationDetailsTable += mailText;
	allocationDetailsTable += "</table>";

	nlapiLogExecution('debug', 'allocation table', allocationDetailsTable)

	var projectDetails = nlapiLookupField('job', projectId, [ 'entityid',
	        'altname', 'custentity_projectmanager.email',
	        'custentity_projectmanager.firstname',
	        'custentity_deliverymanager.email',
	        'custentity_clientpartner.email', 'enddate' ]);

	var projectsDetailTable = "<p class='mailText'>";
	projectsDetailTable += "<b>Project : </b>";
	projectsDetailTable += projectDetails.entityid + " "
	        + projectDetails.altname + "<br/>";
	projectsDetailTable += "<b>Project End Date : </b>";
	projectsDetailTable += projectDetails.enddate + "<br/>";
	projectsDetailTable += "</p>";

	var link = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=619&deploy=1&customer=&project="
	        + projectId + "&startdate=&enddate=&status=2";

	var mailBody = "<style> .mailText { font-size : 15px } .mailTable tr td { border : 1px solid black; }</style>";
	mailBody += "<p class='mailText'>Hi "
	        + projectDetails['custentity_projectmanager.firstname'] + ", </p>";

	mailBody += "<p class='mailText'>Following resources are ending the allocation as per the dates listed below.</p>";
	mailBody += "<p class='mailText'>Please share the extended SOW / Amendment with Business Ops team"
	        + " to have them extended in the systems to avoid any timesheet issues and last minute rush."
	        + " Please create allocation tickets for any changes.</p>";

	mailBody += projectsDetailTable;
	mailBody += allocationDetailsTable;

	mailBody += "<p class='mailText'>Regards, <br/>Information Systems</p>";

	mailBody = addMailTemplate(mailBody);

	nlapiSendEmail('442', 'nitish.mishra@brillio.com', // projectDetails['custentity_projectmanager.email'],
	'Allocations Ending After 15 days', mailBody);

	nlapiLogExecution('debug', '15 days', 'mail send');
}

// ---------- 3 days allocation mail - start

function allocationEndingIn3Days() {
	try {
		var allocationSearch = searchRecord(
		        'resourceallocation',
		        null,
		        [ new nlobjSearchFilter('enddate', null, 'on', 'daysfromnow3') ],
		        [ new nlobjSearchColumn('company').setSort(),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime') ]);

		if (allocationSearch && allocationSearch.length > 0) {
			nlapiLogExecution('debug', '3 days', allocationSearch.length
			        + ' allocations found');

			var mailText = "";

			for (var i = 0; i < allocationSearch.length; i++) {
				mailText += "<tr>";
				mailText += "<td>" + allocationSearch[i].getText('resource')
				        + "</td>";
				mailText += "<td>" + allocationSearch[i].getValue('startdate')
				        + "</td>";
				mailText += "<td>" + allocationSearch[i].getValue('enddate')
				        + "</td>";
				mailText += "<td>"
				        + (allocationSearch[i].getValue('custeventrbillable') == 'T' ? 'Yes'
				                : 'No') + "</td>";
				mailText += "</tr>";

				var currentProject = allocationSearch[i].getValue('company');
				var nextProject = "";

				if (i + 1 < allocationSearch.length) {
					nextProject = allocationSearch[i + 1].getValue('company');
				} else {
					nextProject = "last project";
				}

				nlapiLogExecution('debug', 'current Project', currentProject);
				nlapiLogExecution('debug', 'next project', nextProject);

				if (nextProject == currentProject) {
					nlapiLogExecution('debug', '3 days', 'next project is same');
				} else {
					nlapiLogExecution('debug', '3 days',
					        'next project is different, so send the mail');

					// send the current mail text to the PM
					send3DaysMail(mailText, currentProject);

					// reset the mail text
					mailText = "";
				}
			}
		} else {
			nlapiLogExecution('debug', '3 days', 'no allocation found');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'allocationEndingIn3Days', err);
		throw err;
	}
}

function send3DaysMail(mailText, projectId) {
	var allocationDetailsTable = "<table class='mailText mailTable'>";
	allocationDetailsTable += "<tr>";
	allocationDetailsTable += "<td><b>Resource</b></td>";
	allocationDetailsTable += "<td><b>Start Date</b></td>";
	allocationDetailsTable += "<td><b>End Date</b></td>";
	allocationDetailsTable += "<td><b>Billable</b></td>";
	allocationDetailsTable += "</tr>";
	allocationDetailsTable += mailText;
	allocationDetailsTable += "</table>";

	nlapiLogExecution('debug', 'allocation table', allocationDetailsTable)

	var projectDetails = nlapiLookupField('job', projectId, [ 'entityid',
	        'altname', 'custentity_projectmanager.email',
	        'custentity_projectmanager.firstname',
	        'custentity_deliverymanager.email',
	        'custentity_clientpartner.email', 'enddate' ]);

	var projectsDetailTable = "<p class='mailText'>";
	projectsDetailTable += "<b>Project : </b>";
	projectsDetailTable += projectDetails.entityid + " "
	        + projectDetails.altname + "<br/>";
	projectsDetailTable += "<b>Project End Date : </b>";
	projectsDetailTable += projectDetails.enddate + "<br/>";
	projectsDetailTable += "</p>";

	var link = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=619&deploy=1&customer=&project="
	        + projectId + "&startdate=&enddate=&status=2";

	var mailBody = "<style> .mailText { font-size : 15px } .mailTable tr td { border : 1px solid black; }</style>";
	mailBody += "<p class='mailText'>Hi "
	        + projectDetails['custentity_projectmanager.firstname'] + ", </p>";

	mailBody += "<p class='mailText'>Following resources are ending the allocation as per the dates listed below.</p>";
	mailBody += "<p class='mailText'>Please share the extended SOW / Amendment with Business Ops team"
	        + " to have them extended in the systems to avoid any timesheet issues and last minute rush."
	        + " Please create allocation tickets for any changes.</p>";

	mailBody += projectsDetailTable;
	mailBody += allocationDetailsTable;

	// mailBody += "<p class='mailText'>Please contact the Brillio Operations
	// team for any extension.</p>";

	// mailBody += "<p class='mailText'>Check the <a href='" + link
	// + "'>allocation report</a> in NetSuite for more details.</p>";

	mailBody += "<p class='mailText'>Regards, <br/>Information Systems</p>";

	mailBody = addMailTemplate(mailBody);

	nlapiSendEmail('442', 'nitish.mishra@brillio.com', // projectDetails['custentity_projectmanager.email'],
	'Allocations Ending After 3 days', mailBody);

	nlapiLogExecution('debug', '3 days', 'mail send');
}

function resourceHasNextAllocation(resource, enddate) {
	try {
		var nextDate = nlapiDateToString(nlapiAddDays(
		        nlapiStringToDate(enddate), 1), 'date');

		var allocationSearch = searchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('resource', null, 'anyof', resource),
		        new nlobjSearchFilter('startdate', null, 'on', nextDate) ]);

		return allocationSearch && allocationSearch.length > 0;
	} catch (err) {
		nlapiLogExecution('error', 'resourceHasNextAllocation', err);
		throw err;
	}
}

function processAllocationExtension(nDays) {
	try {
		var allocationSearch = searchRecord('resourceallocation', null,
		        [ new nlobjSearchFilter('enddate', null, 'on', 'daysfromnow'
		                + nDays) ], [
		                new nlobjSearchColumn('company').setSort(),
		                new nlobjSearchColumn('resource'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('custeventrbillable'),
		                new nlobjSearchColumn('percentoftime') ]);

		nlapiLogExecution('debug', nDays + ' days', allocationSearch);

		if (allocationSearch && allocationSearch.length > 0) {
			nlapiLogExecution('debug', nDays + ' days', allocationSearch.length
			        + ' allocations found');

			var mailText = "";

			for (var i = 0; i < allocationSearch.length; i++) {
				var currentProject = allocationSearch[i].getValue('company');
				var nextProject = "";
				var resource = allocationSearch[i].getValue('resource');
				var allocationEndDate = allocationSearch[i].getValue('enddate');
				nlapiLogExecution('debug', 'current Project', currentProject);
				nlapiLogExecution('debug', 'next project', nextProject);

				if (i + 1 < allocationSearch.length) {
					nextProject = allocationSearch[i + 1].getValue('company');
				} else {
					nextProject = "last project";
				}

				if (!resourceHasNextAllocation(resource, allocationEndDate)) {
					nlapiLogExecution('debug', nDays + ' days',
					        'resource has no next allocation '
					                + allocationSearch[i].getText('resource'));
					mailText += "<tr>";
					mailText += "<td>"
					        + allocationSearch[i].getText('resource') + "</td>";
					mailText += "<td>"
					        + allocationSearch[i].getValue('startdate')
					        + "</td>";
					mailText += "<td>"
					        + allocationSearch[i].getValue('enddate') + "</td>";
					mailText += "<td>"
					        + (allocationSearch[i]
					                .getValue('custeventrbillable') == 'T' ? 'Yes'
					                : 'No') + "</td>";
					mailText += "</tr>";
				} else {
					nlapiLogExecution('debug', nDays + ' days',
					        'resource has next allocation '
					                + allocationSearch[i].getText('resource'));
				}

				if (nextProject == currentProject) {
					nlapiLogExecution('debug', nDays + ' days',
					        'next project is same');
				} else {
					nlapiLogExecution('debug', nDays + ' days',
					        'next project is different, so send the mail');

					if (mailText) {
						// send the current mail text to the PM
						sendExtensionRequestMail(mailText, currentProject,
						        nDays + ' days Allocation Ending', nDays);
					} else {
						nlapiLogExecution('debug', nDays + ' days',
						        'no resource for extension, so not sending mail');
					}

					// reset the mail text
					mailText = "";
				}
			}
		} else {
			nlapiLogExecution('debug', nDays + ' days', 'no allocation found');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', nDays + ' days, processAllocationExtension',
		        err);
		throw err;
	}
}

function sendExtensionRequestMail(mailText, projectId, mailSubject, nDays) {
	try {
		var allocationDetailsTable = "<table class='mailText mailTable'>";
		allocationDetailsTable += "<tr>";
		allocationDetailsTable += "<td><b>Resource</b></td>";
		allocationDetailsTable += "<td><b>Start Date</b></td>";
		allocationDetailsTable += "<td><b>End Date</b></td>";
		allocationDetailsTable += "<td><b>Billable</b></td>";
		allocationDetailsTable += "</tr>";
		allocationDetailsTable += mailText;
		allocationDetailsTable += "</table>";

		var projectDetails = nlapiLookupField('job', projectId, [ 'entityid',
		        'altname', 'custentity_projectmanager.email',
		        'custentity_projectmanager.firstname',
		        'custentity_deliverymanager.email',
		        'custentity_clientpartner.email', 'enddate' ]);

		var projectsDetailTable = "<p class='mailText'>";
		projectsDetailTable += "<b>Project : </b>";
		projectsDetailTable += projectDetails.entityid + " "
		        + projectDetails.altname + "<br/>";
		projectsDetailTable += "<b>Project End Date : </b>";
		projectsDetailTable += projectDetails.enddate + "<br/>";
		projectsDetailTable += "</p>";

		var link = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=619&deploy=1&customer=&project="
		        + projectId + "&startdate=&enddate=&status=2";

		var mailBody = "<style> .mailText { font-size : 15px } .mailTable tr td { border : 1px solid black; }</style>";
		mailBody += "<p class='mailText'>Hi "
		        + projectDetails['custentity_projectmanager.firstname']
		        + ", </p>";

		mailBody += "<p class='mailText'>Following resources are ending the allocation as per the dates listed below.</p>";
		mailBody += "<p class='mailText'>Please share the extended SOW / Amendment with Business Ops team"
		        + " to have them extended in the systems to avoid any timesheet issues and last minute rush."
		        + " Please create allocation tickets for any changes.</p>";

		mailBody += projectsDetailTable;
		mailBody += allocationDetailsTable;

		mailBody += "<p class='mailText'>Regards, <br/>Information Systems</p>";

		mailBody = addMailTemplate(mailBody);

		nlapiSendEmail('442', 'nitish.mishra@brillio.com', // projectDetails['custentity_projectmanager.email'],
		mailSubject, mailBody);

		nlapiLogExecution('debug', nDays + ' days', 'mail send');
	} catch (err) {
		nlapiLogExecution('error', nDays + ' days, sendExtensionRequestMail',
		        err);
		throw err;
	}
}
