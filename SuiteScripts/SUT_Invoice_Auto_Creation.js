// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Invoice_Auto_Creation.js
	Author      : Jayesh Dinde
	Date        : 25 July 2016
	Description : Create Invoice based on CSV uploaded for billable time entries.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --													  
                                                                                         																						  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

// BEGIN SUITELET FUNCTION==================================================

function suitelet_invoice_creation(request,response)
{
	try
	{
		if (request.getMethod() == 'GET')
		{
			var form_obj = nlapiCreateForm("Invoice Auto Creation");
			
			var project_fld = form_obj.addField('custpage_project', 'select', 'Project','job');
			
			var billing_from = form_obj.addField('custpage_billfrom', 'date', 'Billing From');
			
			var billing_to = form_obj.addField('custpage_billto', 'date', 'Billing To');
			
			var csv_file = form_obj.addField('custpage_csv', 'file', 'CSV File');
			
			//form_obj.addButton('custpage_validate_csv','Validate CSV',validate_csv(request));
			form_obj.addSubmitButton('Validate CSV');
			response.writePage(form_obj);
		}
		else
		{
			var validation_complete_flag = 0;
			var sublist_index = 1;
			var proj_selected = request.getParameter('custpage_project');
			var bill_from = request.getParameter('custpage_billfrom');
			var bill_to = request.getParameter('custpage_billto');
			//var csv_file_slected = request.getParameter('custpage_csv');
			
			var form_obj = nlapiCreateForm("Invoice Auto Creation");
			
			var project_fld = form_obj.addField('custpage_project', 'select', 'Project','job').setDisplayType('inline');
			project_fld.setDefaultValue(proj_selected);
			
			var billing_from = form_obj.addField('custpage_billfrom', 'date', 'Billing From').setDisplayType('inline');
			billing_from.setDefaultValue(bill_from);
			bill_from = nlapiStringToDate(bill_from);
			
			var billing_to = form_obj.addField('custpage_billto', 'date', 'Billing To').setDisplayType('inline');
			billing_to.setDefaultValue(bill_to);
			bill_to = nlapiStringToDate(bill_to);
			
			var csv_file_obj = request.getFile('custpage_csv');
			csv_file_obj.setFolder(-15);
			var csv_file_id = nlapiSubmitFile(csv_file_obj);
			
			var file_data = getFileContents(csv_file_id);
			
			if(_logValidation(file_data))
			{
				var emp_list = new Array();
				var employee_list = new Array();
				
				/*var filters_search_timentry = new Array();
				filters_search_timentry[0] = 	new nlobjSearchFilter('customer', null, 'anyof', parseInt(23694));
				filters_search_timentry[1] =	new nlobjSearchFilter('date', null, 'within',bill_from,bill_to);
				filters_search_timentry[2] =	new nlobjSearchFilter('type', null, 'anyof', 'A');
				filters_search_timentry[3] =	new nlobjSearchFilter('approvalstatus', null, 'anyof', 3);
				filters_search_timentry[4] =	new nlobjSearchFilter('billingstatus', null, 'is', 'T');
				filters_search_timentry[5] =	new nlobjSearchFilter('billable', null, 'is', 'T');
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('date');
				columns[1] = new nlobjSearchColumn('rate');
				columns[2] = new nlobjSearchColumn('employee');
				columns[3] = new nlobjSearchColumn('durationdecimal');
				columns[4] = new nlobjSearchColumn('startdate','timesheet');
					
				var project_time_entry = nlapiSearchRecord('timeentry', null, filters_search_timentry, columns);*/
				
				var filters_search_timentry = new Array();
				filters_search_timentry[0] = 	new nlobjSearchFilter('customer', 'timebill', 'anyof', parseInt(23694));
				filters_search_timentry[1] =	new nlobjSearchFilter('date', 'timebill', 'within',bill_from,bill_to);
				filters_search_timentry[2] =	new nlobjSearchFilter('type', 'timebill', 'anyof', 'A');
				filters_search_timentry[3] =	new nlobjSearchFilter('approvalstatus', 'timebill', 'anyof', 3);
				filters_search_timentry[4] =	new nlobjSearchFilter('status', 'timebill', 'is', 'T');
				filters_search_timentry[5] =	new nlobjSearchFilter('billable', 'timebill', 'is', 'T');
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('startdate');
				columns[1] = new nlobjSearchColumn('employee');
				columns[2] = new nlobjSearchColumn('totalhours');
				columns[3] = new nlobjSearchColumn('rate','timebill');
					
				var project_time_entry = nlapiSearchRecord('timesheet', null, filters_search_timentry, columns);
				if (_logValidation(project_time_entry))
				{
					nlapiLogExecution('audit','time entry length:- ',project_time_entry.length);
					for (var i_search_indx = 0; i_search_indx < project_time_entry.length; i_search_indx++)
					{
						var i_employee_id =	project_time_entry[i_search_indx].getValue('employee');
						var emp_name = project_time_entry[i_search_indx].getText('employee');
						var time_entry_date	=	project_time_entry[i_search_indx].getValue('startdate');
						var rate =	project_time_entry[i_search_indx].getValue('rate','timebill');
						var duration =	project_time_entry[i_search_indx].getValue('totalhours');
						var timesheet_date = project_time_entry[i_search_indx].getValue('startdate');
						
						var emp_already_exist_index = get_index(employee_list,i_employee_id,timesheet_date);
						if(emp_already_exist_index >= 0)
						{
							nlapiLogExecution('audit','emp_already_exist_index:-- ',emp_already_exist_index);
							//var emp_already_exist_index = emp_list.indexOf(i_employee_id);
							
							if(timesheet_date == employee_list[emp_already_exist_index].timesheet_date)
							{
								var current_duration = employee_list[emp_already_exist_index].total_hours;
								var total_duration = parseFloat(current_duration) + parseFloat(duration);
								
								employee_list[emp_already_exist_index] = {
													'emp_id': i_employee_id,
													'emp_name':emp_name,
													'time_entry_date':time_entry_date,
													'rate':rate,
													'duration':duration,
													'timesheet_date':timesheet_date,
													'total_hours':total_duration
												};
							}
							/*else
							{
								employee_list[i_search_indx] = {
													'emp_id': i_employee_id,
													'emp_name':emp_name,
													'time_entry_date':time_entry_date,
													'rate':rate,
													'duration':duration,
													'timesheet_date':timesheet_date,
													'total_hours':duration
												};
						
								//emp_list.push(i_employee_id);
							}*/
							
						}
						else
						{
							//nlapiLogExecution('audit','index new entry:-- ',i_search_indx);
							employee_list[i_search_indx] = {
													'emp_id': i_employee_id,
													'emp_name':emp_name,
													'time_entry_date':time_entry_date,
													'rate':rate,
													'duration':duration,
													'timesheet_date':timesheet_date,
													'total_hours':duration
												};
												
							var emp_name_in_list = emp_name;
							emp_name_in_list = emp_name_in_list.toString();
							emp_name_in_list = emp_name_in_list.split('-');
							var emp_to_comp = emp_name_in_list[0];
							nlapiLogExecution('audit','amp srch',emp_to_comp);
							emp_list.push(emp_to_comp);
						}
					}
					
					nlapiLogExecution('audit','emplist len:-- ',employee_list.length);
					var invoice_emp_list = form_obj.addSubList('custpage_emp_list', 'list', 'Employee Entries', 'custpage_emp_tab');
					
					var i_S_No= invoice_emp_list.addField('custpage_s_no', 'text', 'Sr. No');				
					var i_employee = invoice_emp_list.addField('custpage_employee_name', 'text', 'Contractor','employee').setDisplayType('inline');
					var i_rate = invoice_emp_list.addField('custpage_emp_rate', 'text', 'Bill Rate').setDisplayType('inline');
					var i_duration = invoice_emp_list.addField('custpage_duration', 'text', 'Billable Hours').setDisplayType('inline');
					var d_timeentry_date = invoice_emp_list.addField('custpage_timeentry_date', 'text', 'Time Sheet Start Date').setDisplayType('inline');
					var d_timesheet_date = invoice_emp_list.addField('custpage_timesheet_date', 'text', 'Time Sheet End Date').setDisplayType('inline');
					var i_total_hrs = invoice_emp_list.addField('custpage_total_hrs', 'text', 'Total hours worked').setDisplayType('inline');
					
					var sr_no = 1;
					for(var csv_index = 1; csv_index < file_data.length-1; csv_index++)
					{
						for (var emp_counter = 0; emp_counter < employee_list.length; emp_counter++) //employee_list.length
						{
							var row_data = file_data[csv_index].toString().split(',');
						
							var employee_name_csv = row_data[0];
							employee_name_csv = employee_name_csv.split('-');
							employee_name_csv = employee_name_csv[0];
							var bill_rate_csv = row_data[1];
							var timesheet_strt_date_csv = row_data[2];
							var timesheet_end_date_csv = row_data[3];
							var billable_hours_csv = row_data[4];
							
							var emp_name_system = employee_list[emp_counter].emp_name;
							emp_name_system = emp_name_system.split('-');
							emp_name_system = emp_name_system[0];
							
							if(employee_name_csv == emp_name_system)
							{
								timesheet_strt_date_csv = new Date(timesheet_strt_date_csv);
								timesheet_strt_date_csv = nlapiDateToString(timesheet_strt_date_csv);
								
								//nlapiLogExecution('audit','timesheet_strt_date_csv:-- '+timesheet_strt_date_csv,employee_list[emp_counter].timesheet_date);
								if(timesheet_strt_date_csv == employee_list[emp_counter].timesheet_date)
								{
									var tot_hrs_emp = employee_list[emp_counter].total_hours;
									tot_hrs_emp = tot_hrs_emp.toString();
									tot_hrs_emp = tot_hrs_emp.split(':');
									var tot_hrs_for_emp = tot_hrs_emp[0];
									
									//if(parseFloat(billable_hours_csv) != parseFloat(tot_hrs_for_emp) || parseFloat(bill_rate_csv) != parseFloat(employee_list[emp_counter].rate))
									if(parseFloat(billable_hours_csv) != parseFloat(tot_hrs_for_emp))
									{
										//nlapiLogExecution('audit','employee_name_csv:-- '+employee_name_csv,billable_hours_csv);
										//nlapiLogExecution('audit','billable_hours_csv:-- '+billable_hours_csv,employee_list[emp_counter].total_hours);
									
										validation_complete_flag = 1;
										
										invoice_emp_list.setLineItemValue('custpage_s_no',sublist_index,sr_no);
										sr_no++;		
										invoice_emp_list.setLineItemValue('custpage_employee_name',sublist_index,employee_list[emp_counter].emp_name);
										invoice_emp_list.setLineItemValue('custpage_emp_rate',sublist_index,employee_list[emp_counter].rate);
										invoice_emp_list.setLineItemValue('custpage_duration',sublist_index,billable_hours_csv);
										invoice_emp_list.setLineItemValue('custpage_timeentry_date',sublist_index,employee_list[emp_counter].time_entry_date);
										invoice_emp_list.setLineItemValue('custpage_timesheet_date',sublist_index,timesheet_end_date_csv);
										invoice_emp_list.setLineItemValue('custpage_total_hrs',sublist_index,employee_list[emp_counter].total_hours);
										sublist_index = parseFloat(sublist_index) + parseFloat(1);
										
									}
								}
							}
						}
					}
					
					/*(for (var csv_index = 1; csv_index < file_data.length - 1; csv_index++)
					{
						var row_data = file_data[csv_index].toString().split(',');
						
						var employee_name_csv = row_data[0];
						var bill_rate_csv = row_data[1];
						var timesheet_strt_date_csv = row_data[2];
						var timesheet_end_date_csv = row_data[3];
						var billable_hours_csv = row_data[4];
						
						var emp_name_in_csv = employee_name_csv.toString();
						emp_name_in_csv = emp_name_in_csv.split('-');
						var emp_name_to_check = emp_name_in_csv[0];
						var does_emp_exist = 0;
						does_emp_exist = get_nt_submittd_emp(emp_list,emp_name_to_check,timesheet_strt_date_csv);
						
						if(does_emp_exist == 1)
						{
							validation_complete_flag = 1;
							
							invoice_emp_list.setLineItemValue('custpage_s_no',sublist_index,sr_no);
							sr_no++;		
							invoice_emp_list.setLineItemValue('custpage_employee_name',sublist_index,employee_name_csv);
							invoice_emp_list.setLineItemValue('custpage_emp_rate',sublist_index,bill_rate_csv);
							invoice_emp_list.setLineItemValue('custpage_duration',sublist_index,billable_hours_csv);
							invoice_emp_list.setLineItemValue('custpage_timeentry_date',sublist_index,timesheet_strt_date_csv);
							invoice_emp_list.setLineItemValue('custpage_timesheet_date',sublist_index,timesheet_end_date_csv);
							invoice_emp_list.setLineItemValue('custpage_total_hrs',sublist_index,0);
							sublist_index = parseFloat(sublist_index) + parseFloat(1);
						}
					}*/
				}
				
				if(validation_complete_flag == 0)
				{
					//form_obj.setScript('customscript_cli_invoice_suto_creation');
					//form_obj.addButton('custpage_create_invoice', 'Create Invoice', create_auto_invoice(proj_selected,bill_from,bill_to,csv_file_id));
				}
			}
			
			
			response.writePage(form_obj);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

// END SUITELET FUNCTION==================================================

function getFileContents(csv_file_id)
{
	var csvFile = nlapiLoadFile(csv_file_id);
    var csvContent = csvFile.getValue();
	var rows_data = csvContent.split('\r\n');
	nlapiLogExecution('debug', 'rows.data', rows_data.length);
	return rows_data;
}

function create_auto_invoice(proj_selected,bill_from,bill_to,csv_file_id)
{
	nlapiLogExecution('debug', 'csv_file_id in sut', csv_file_id);
	var params=new Array();
	params['custscript_proj_selected'] = proj_selected;
	params['custscript_bill_from'] = bill_from;
	params['custscript_bill_to'] = bill_to;
 	params['custscript_csv_file_id'] = csv_file_id;
	
	var status = nlapiScheduleScript('customscript_sch_invoice_auto_creation',null,params);
	
}

function get_index(employee_list,i_employee_id,timesheet_date)
{
	for(var index_check = 0; index_check<employee_list.length; index_check++)
	{
		if(employee_list[index_check].emp_id == i_employee_id && employee_list[index_check].timesheet_date == timesheet_date)
		{
			return index_check;
		}
		else
		{
			return -1;
		}
	}
}

function get_nt_submittd_emp(emp_list,emp_name_to_check,timesheet_strt_date_csv)
{
	for(var index_check_emp = 0; index_check_emp<emp_list.length; index_check_emp++)
	{
		/*var emp_name_in_list = emp_list[index_check_emp];
		emp_name_in_list = emp_name_in_list.toString();
		emp_name_in_list = emp_name_in_list.split('-');
		var emp_to_comp = emp_name_in_list[0];
		nlapiLogExecution('audit','emp_name_to_check:- '+emp_name_to_check,'emp_to_comp:- '+emp_to_comp+':: '+emp_list.length);
		//var emp_list_strt_date = employee_list[index_check_emp].time_entry_date;
		
		if(emp_name_to_check == emp_to_comp)
		{
			return 0;
		}
		else
		{
			return 1;
		}*/
		
		if(emp_list.indexOf(emp_name_to_check) >=0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}


function decode_base64(s)
{
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [47, 48], [43, 44]];
    
    for (z in n) 
	{
        for (i = n[z][0]; i < n[z][1]; i++) 
		{
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++) 
	{
        e[v[i]] = i;
    }
    
    for (i = 0; i < s.length; i += 72) 
	{
        var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
        for (x = 0; x < o.length; x++) {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) 
			{
                r += w((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
    
}

function GetFileContents(csv_file_id)
{
    if (_logValidation(csv_file_id)) 
	{
		var FileObj = nlapiLoadFile(csv_file_id);
        var filedecodedrows = '';
        
        var fileContents = FileObj.getValue();
        nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet:- '+FileObj, 'fileContents=',fileContents);
        
        // Begin Code : To parse The data.
        
        if (FileObj.getType() == 'EXCEL') 
		{
            //nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ excel file');
            filedecodedrows = decode_base64(fileContents);
            //nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After filedecodedrows=' + filedecodedrows);
        }
        else 
		{
            //nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ other than excel file');
            filedecodedrows = fileContents;
            //nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After fileContents=' + fileContents);
        }
        filedecodedrows = fileContents;
        // End Code : To parse The data.
        
        var arr = filedecodedrows.split(/\n/g);
        //nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'base64Ecodedstring=' + arr.length);
        
        var Length = parseInt(arr.length) - parseInt(1);
        
        var FullContents = '';
        
        for (var i = 1; i < Length; i++) 
		{
            //Begin Code : to get the data from the CSV Row wise
            
            arr[i] = parseCSV(arr[i]);
            
            if (i != (Length - 1)) 
			{
                FullContents += arr[i] + '#$';
            }
            else 
			{
                FullContents += arr[i];
            }
            
        }
        return FullContents.toString();
    }
}

function _logValidation(value)
{
    if (value != null && value != '' && value != undefined) 
	{
        return true;
    }
    else 
	{
        return false;
    }
}

function parseCSV(csvString)
{
    var fieldEndMarker = /([,\015\012] *)/g; /* Comma is assumed as field separator */
    var qFieldEndMarker = /("")*"([,\015\012] *)/g; /* Double quotes are assumed as the quote character */
    var startIndex = 0;
    var records = [], currentRecord = [];
    do 
	{
        // If the to-be-matched substring starts with a double-quote, use the qFieldMarker regex, otherwise use fieldMarker.
        var endMarkerRE = (csvString.charAt(startIndex) == '"') ? qFieldEndMarker : fieldEndMarker;
        endMarkerRE.lastIndex = startIndex;
        var matchArray = endMarkerRE.exec(csvString);
        if (!matchArray || !matchArray.length) 
		{
            break;
        }
        var endIndex = endMarkerRE.lastIndex - matchArray[matchArray.length - 1].length;
        var match = csvString.substring(startIndex, endIndex);
        if (match.charAt(0) == '"') // The matching field starts with a quoting character, so remove the quotes
        {
            match = match.substring(1, match.length - 1).replace(/""/g, '"');
        }
        currentRecord.push(match);
        var marker = matchArray[0];
        if (marker.indexOf(',') < 0) // Field ends with newline, not comma
        {
            records.push(currentRecord);
            currentRecord = [];
        }
        startIndex = endMarkerRE.lastIndex;
    }
    while (true);
    if (startIndex < csvString.length) 
	{ // Maybe something left over?
        var remaining = csvString.substring(startIndex).trim();
        if (remaining) 
            currentRecord.push(remaining);
    }
    if (currentRecord.length > 0) 
	{ // Account for the last record
        records.push(currentRecord);
    }
    ////nlapiLogExecution('DEBUG','parseCSV','records :'+records);
    return records;
    
}


function dump()
{
	var FileObj = nlapiLoadFile(csv_file_id);
    
    		var Contents = FileObj.getValue();
			nlapiLogExecution('audit','contents:- ',Contents);
				
			if (FileObj.getType() == 'EXCEL') 
			{
			nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ excel file');
				filedecodedrows = decode_base64(Contents);
			//nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After filedecodedrows=' + filedecodedrows);
			}
			else 
			{
			//nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', '@@ other than excel file');
				filedecodedrows = Contents;
			//nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'After fileContents=' + fileContents);
			}
       	 //filedecodedrows = Contents;
        // End Code : To parse The data.
        
        	var arr = filedecodedrows.split(/\n/g);
        //nlapiLogExecution('DEBUG', 'ReadfileFrmCabinet', 'base64Ecodedstring=' + arr.length);
        
        var Length = parseInt(arr.length) - parseInt(1);
        
        var FullContents = '';
        
	        for (var i = 1; i < Length; i++) 
			{
	            //Begin Code : to get the data from the CSV Row wise
	            
	            arr[i] = parseCSV(arr[i]);
	            
	            if (i != (Length - 1)) 
				{
	                FullContents += arr[i] + '#$';
	            }
	            else 
				{
	                FullContents += arr[i];
	            }
	            
	        }
				
				var records = FullContents.toString();
				
				
				//var records = GetFileContents(csv_file_id);
				
	            var splitarr = records.toString().split('#$');
	            nlapiLogExecution('DEBUG', 'My array:',splitarr.length);
				for (var j = 0; j < splitarr.length; j++)
				{
					var splitArray = splitarr[j].toString().split(',');
					
					var part_no = splitArray[0];
					var item_desc = splitArray[1];
					var item_strtdate = splitArray[2];
					var item_enddate = splitArray[3];
					var item_qty = splitArray[4];
					var item_unitprice = splitArray[5];
					var item_totalprice = splitArray[6];
					
				nlapiLogExecution('DEBUG', 'My array:: ', 'array==== :' + part_no + '@' + item_desc + '@' + item_strtdate + '@' + item_enddate + '@' + item_qty + '@' + item_unitprice + '@' + item_totalprice);
				}
}