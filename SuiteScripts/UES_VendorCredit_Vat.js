// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: UES Vendor Credit VAT
     Author:      Supriya Jadhav
     Company:     Aashna Cloudtech Pvt Ltd.
     Date:
     Description:
     
     Script Modification Log:
     
     -- Date --			-- Modified By --				--Requested By--				-- Description --
      22 sep 2014         Supriya                         Kalpana                       Normal Account Validation
      21 oct 2015         Nikhil j                        Sachin k                      currency other than Inr (convert to INR)    
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     
     BEFORE LOAD
     - beforeLoadRecord(type)
     
     NOT USED
     BEFORE SUBMIT
     - beforeSubmitRecord(type)
     
     NOT USED
     AFTER SUBMIT
     - afterSubmitRecord(type)
     
     afterSubmitRecord(type)
     SUB-FUNCTIONS
     
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)	
{

    /*  On before load:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    //  BEFORE LOAD CODE BODY
    
    
    return true;
    
    
    
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)	
{
    /*  On before submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  BEFORE SUBMIT CODE BODY
	//try
	{
	var i_Flagcheck = 0;
	var Flag = 0;
	var a_subisidiary = new Array()
	var s_pass_code;
	var stRoundMethod;
	
	nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " i_Flagcheck " + i_Flagcheck)
	nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit', " type " + type)
    if (type == 'delete') 	
		{
				    
					var billcreditid = nlapiGetRecordId();
					nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " billcreditid " + billcreditid)
					
					var recordType = nlapiGetRecordType()
					nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit', " recordType " + recordType)
					
					var o_billCreditobj = nlapiLoadRecord(recordType,billcreditid)
					
					var i_AitGlobalRecId = SearchGlobalParameter();
				    nlapiLogExecution('DEBUG', 'Bill Credit', "i_AitGlobalRecId" + i_AitGlobalRecId);
				    
				    if (i_AitGlobalRecId != 0) 	
					{
				        var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
				        
				      
				        a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				        nlapiLogExecution('DEBUG', 'Bill Credit ', "a_subisidiary->" + a_subisidiary);
				        
				        var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
				        nlapiLogExecution('DEBUG', 'Bill Credit ', "i_vatCode->" + i_vatCode);
				        
				        var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
				        nlapiLogExecution('DEBUG', 'Bill Credit', "i_taxcode->" + i_taxcode);
				        
						var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
						nlapiLogExecution('DEBUG','Bill ', "s_pass_code->"+s_pass_code);
						
						stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');

				        
				    }// end if(i_AitGlobalRecId != 0 )
					//var a_subsidiary1 = a_subisidiary.toString();
					var i_subsidiary = o_billCreditobj.getFieldValue('subsidiary')
				    Flag = isindia_subsidiary(a_subisidiary, s_pass_code,i_subsidiary)  
				        
				        /*
if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 	
						{
				        
				            for (var y = 0; y < a_subisidiary.length; y++) 	
							{
				                var i_subsidiary = o_billCreditobj.getFieldValue('subsidiary')
				                
				                //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
				                if (a_subisidiary[y] == i_subsidiary) 	
								{
				                    Flag = 1;
				                    break;
				                }// END if (a_subisidiary[y] == i_subsidiary)
								
				            }// END for (var y = 0; y < a_subisidiary.length; y++)
							
				        }// END  if (a_subsidiary1.indexOf(',') > -1)
				        else 	
						{
				            // === IF COMMA NOT FOUND ===
				            var i_subsidiary = o_billCreditobj.getFieldValue('subsidiary')
				            
				            //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
				            if (a_subisidiary[0] == i_subsidiary) 	
							{
				                Flag = 1;
				            }// END  if (a_subsidiary1 == i_subsidiary)
							
				        }// END  if (a_subsidiary1 == i_subsidiary)
					
					//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
					var context = nlapiGetContext();
				    var i_subcontext = context.getFeature('SUBSIDIARIES');
					if(i_subcontext == false)
					{
						 Flag = 1;
					}
					
*/
					
					if(Flag == parseInt(1))
					{
						var column = new Array();
	                    var filters = new Array();
	                    filters.push(new nlobjSearchFilter('custrecord_vatbill_billno', null, 'is', billcreditid));
	                    column.push(new nlobjSearchColumn('internalid'))
	                    column.push(new nlobjSearchColumn('custrecord_vatbill_status'))
	                    var results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
	                    
	                    if (results != null) 	
						{
	                    
	                        for (var i = 0; results != null && i < results.length; i++) 
							{
	                            var vatbilldetailId = results[i].getValue('internalid')
	                            var vatbillStatus = results[i].getValue('custrecord_vatbill_status')
	                            nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " Vat Bill Credit Detail ID " + vatbilldetailId)
	                            nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " Vat Bill Credit Status " + vatbillStatus)
	                            if (vatbilldetailId != null && vatbilldetailId != undefined && vatbilldetailId != '') 
								{
	                                if (vatbillStatus == 'Close') 
									{
	                                    i_Flagcheck = 1;
										break;
	                                }// vat bill status end
									
	                            }// vat bill deatail id check
	                            
	                        }// end of for loop.
							
							 nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit', " i_Flagcheck " + i_Flagcheck)
							if(i_Flagcheck == parseInt(1))
							{
								throw("You can not delete this record as payment has been applied for this record..")
							}
							else
							{
								for (var i = 0; results != null && i < results.length; i++) 
								{
		                            var vatbilldetailId = results[i].getValue('internalid')
		                            var vatbillStatus = results[i].getValue('custrecord_vatbill_status')
		                            nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " Vat Bill Credit Detail ID " + vatbilldetailId)
		                            nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " Vat Bill Credit Status " + vatbillStatus)
		                            if (vatbilldetailId != null && vatbilldetailId != undefined && vatbilldetailId != '') 
									{
											nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill Credit ', " if(vatbillStatus == 'Open')")
		                                    nlapiDeleteRecord('customrecord_vatbilldetail', vatbilldetailId);
										
		                            }// vat bill deatail id check
		                            
		                        }// end of for loop.
							}
							
	                    }// result end.
					}// End Flag Check
					
				}//End Delete Record
	}
	//catch(EX)
	//{
	//	 nlapiLogExecution('DEBUG', 'Vendor Credit beforeSubmitRecord ', "Exception Thrown" + EX);
	//}
	
    
    return true;
    
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)	
{
    /*  On after submit:
     - PURPOSE
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  AFTER SUBMIT CODE BODY
    
    // === CODE TO GET THE GLOBAL SUBSIDIARY PARAMETER ===
	
	if (type != 'delete')
	{
		try
	{
    var i_AitGlobalRecId = SearchGlobalParameter();
    nlapiLogExecution('DEBUG', 'Bill Credit ', "i_AitGlobalRecId" + i_AitGlobalRecId);
     var a_subisidiary = new Array()
	 var s_pass_code;
	 var stRoundMethod;
	 
    if (i_AitGlobalRecId != 0) 
	{
        var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
        
       
        a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
        nlapiLogExecution('DEBUG', 'Bill Credit ', "a_subisidiary->" + a_subisidiary);
        
        var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
        nlapiLogExecution('DEBUG', 'Bill Credit ', "i_vatCode->" + i_vatCode);
        
        var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
        nlapiLogExecution('DEBUG', 'Bill Credit ', "i_taxcode->" + i_taxcode);
        
		var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
		nlapiLogExecution('DEBUG','Bill ', "s_pass_code->"+s_pass_code);
		
		stRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_st_roundoff');
        
    }// end if(i_AitGlobalRecId != 0 )
	
    //===VAT CODE ===========
    // ==== if VAT Credit BILL form is selected then run the following code ===
    // ==== Begin :Variable Declaration ===
    
    var formid = nlapiGetFieldValue('customform')
    var billcreditid;
    var currentRectype;
    var vendor
    var date
    var account = new Array();
    var taxcode = new Array();
    var taxcodename = new Array();
    var locationArray = new Array();
    var creditamount = new Array();
    var debitamount = new Array();
    var posting = new Array();
    var memo = new Array();
    var entity = new Array();
    var subsidiary = new Array();
    var department = new Array();
    var class1 = new Array();
    var fxamount = new Array();
    var rectype = new Array();
    var rowcount;
    var usertotal;
    
    // === End:Variable Declaration =====
	
	
    // === CODE BODY ======
    billcreditid = nlapiGetRecordId()
    currentRectype = nlapiGetRecordType();
    
    if (billcreditid != null && billcreditid != undefined && billcreditid != '') 	
	{
    
        var o_billCreditobj = nlapiLoadRecord(currentRectype, billcreditid);
        
        if (o_billCreditobj != null && o_billCreditobj != undefined && o_billCreditobj != '') 	
		{
            vendor = o_billCreditobj.getFieldValue('entity')
            date = o_billCreditobj.getFieldValue('trandate')
            usertotal = o_billCreditobj.getFieldValue('usertotal')
            var postingperiod = nlapiGetFieldValue('postingperiod')
            
           // var a_subsidiary1 = a_subisidiary.toString();
            var Flag = 0;
			var i_subsidiary = o_billCreditobj.getFieldValue('subsidiary')
             Flag = isindia_subsidiary(a_subisidiary, s_pass_code,i_subsidiary) 
			 
			 //==========================Get indian Currency internalD and Exchange rate =====================//
			
			var currency = o_billCreditobj.getFieldValue('currency')
			
			var INRcurrencyID = getAITIndianCurrency()
			
			var exchangerate = nlapiExchangeRate(currency, INRcurrencyID,date)
			
			if(currency != INRcurrencyID)
			{
				usertotal = (parseFloat(usertotal) * parseFloat(exchangerate)) 
				usertotal = applySTRoundMethod(stRoundMethod,usertotal)
				//taxtotal = (parseFloat(taxtotal) * parseFloat(exchangerate)) 
			}
			
            /*
if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 	
			{
            
                for (var y = 0; y < a_subisidiary.length; y++) 	
				{
                    var i_subsidiary = o_billCreditobj.getFieldValue('subsidiary')
                    
                    //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
                    if (a_subisidiary[y] == i_subsidiary) 	
					{
                        Flag = 1;
                        break;
                    }// END if (a_subisidiary[y] == i_subsidiary)
					
                }// END for (var y = 0; y < a_subisidiary.length; y++)
				
            }// END  if (a_subsidiary1.indexOf(',') > -1)
            else 
			{
                // === IF COMMA NOT FOUND ===
                var i_subsidiary = o_billCreditobj.getFieldValue('subsidiary')
                
                //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
                if (a_subisidiary[0] == i_subsidiary) 
				{
                    Flag = 1;
                }// END  if (a_subsidiary1 == i_subsidiary)
				
            }// END  if (a_subsidiary1 == i_subsidiary)
			
           
			//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
			var context = nlapiGetContext();
		    var i_subcontext = context.getFeature('SUBSIDIARIES');
			if(i_subcontext == false)
			{
				 Flag = 1;
			}
*/
			 nlapiLogExecution('DEBUG', 'Bill Credit', ' Flag -->' + Flag);
            if (Flag == 1) 
			{
                //  CODE TO GET THE DATA ONE BY ONE FROM THE SAVED SEARCH
                var filter = new Array();
                filter.push(new nlobjSearchFilter('internalid', null, 'is', billcreditid))
                filter.push(new nlobjSearchFilter('type', null, 'is', 'VendCred'))
                
                
                var columns = new Array();
                columns.push(new nlobjSearchColumn('internalid'));
                var search_results = nlapiSearchRecord('transaction', 'customsearch_tax_bill_details_info', filter, null);
                
                if (search_results != null && search_results != '' && search_results != 'undefined') 
				{
                    for (k = 0; k < search_results.length; k++) 
					{
                        var grpresult = search_results[k];
                        var grpcolumns = grpresult.getAllColumns();
                        var grpcolumnLen = grpcolumns.length;
                        
                        for (var jcnt = 0; jcnt < grpcolumnLen; jcnt++) 
						{
                        
                            var grpcolumn = grpcolumns[jcnt];
                            var grpfieldName = grpcolumn.getName();
                            var grpvalue = grpresult.getValue(grpcolumn);
                            var Txtvalue = grpresult.getText(grpcolumn);
                            if (grpfieldName == 'account') 
							{
                                account[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " account ->  " + account[k])
                            }
                            
                            if (grpfieldName == 'creditamount') 
							{
                                creditamount[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " creditamount ->  " + creditamount[k])
                            }
                            if (grpfieldName == 'debitamount') 
							{
                                debitamount[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " debitamount ->  " + debitamount[k])
                            }
                            if (grpfieldName == 'postingperiod') 
							{
                                posting[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " posting ->  " + posting[k])
                            }
                            if (grpfieldName == 'memo') 
							{
                                memo[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " memo ->  " + memo[k])
                            }
                            if (grpfieldName == 'entity') 
							{
                                entity[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " entity ->  " + entity[k])
                            }
                            if (grpfieldName == 'subsidiary') 
							{
                                subsidiary[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " subsidiary ->  " + subsidiary[k])
                            }
                            if (grpfieldName == 'department') 
							{
                                department[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " department ->  " + department[k])
                            }
                            if (grpfieldName == 'class') 
							{
                                class1[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " class1 ->  " + class1[k])
                                
                            }
                            if (grpfieldName == 'location') 
							{
                                locationArray[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " locationArray ->  " + locationArray[k])
                            }
                            if (grpfieldName == 'fxamount') 
							{	
								if (INRcurrencyID != currency) 
								{
									var i_INR_amt = (parseFloat(grpvalue) * parseFloat(exchangerate));
									nlapiLogExecution('DEBUG', 'Bill ', " i_INR_amt ->  " + i_INR_amt)
									
									i_INR_amt = applySTRoundMethod(stRoundMethod,i_INR_amt)
										
									fxamount[k] = i_INR_amt;
									
								}
								else 
								{
									fxamount[k] = parseFloat(grpvalue);
									nlapiLogExecution('DEBUG', 'Bill Credit ', " fxamount ->  " + fxamount[k])
								}
                            }
                            if (grpfieldName == 'taxcode') 
							{
                                taxcode[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " taxcode ->  " + taxcode[k])
                            }
                            if (grpfieldName == 'type') 
							{
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " grpfieldName ->  " + grpfieldName)
                                if (grpvalue == 'VendCred') 
								{
                                    grpvalue = 'Bill Credit'
                                }
                                rectype[k] = grpvalue;
                                nlapiLogExecution('DEBUG', 'Bill Credit ', " rectype ->  " + rectype[k])
								
                            }// type check end
                            
                            
                        }// end of for loop coloumn number 
                        
                    }// end of search result for loop
                    
                }// search result not null check
                
                
                // ======== Code  when New Vat bill is created we create on custom record.of the type VAT Bill Detail with all the detail  ====
                
                if (type == 'create') 	
				{
                    if(search_results != null)
					{
						for (var k = 0; k < search_results.length && search_results != null; k++) 	
						{
	                        nlapiLogExecution('DEBUG', 'Bill Credit ', " fxamount[k] ->  " + fxamount[k])
	                        record = nlapiCreateRecord('customrecord_vatbilldetail');
	                        
	                        record.setFieldValue('custrecord_vatbill_billno', billcreditid)
							record.setFieldValue('custrecord_vatbill_tranno', billcreditid)
	                        record.setFieldValue('custrecord_vatbill_account', account[k])
	                        record.setFieldValue('custrecord_vatbill_vendor', vendor);
	                        record.setFieldValue('custrecord_vatbill_billdate', date);
	                        record.setFieldValue('custrecord_vatbill_grossamount', usertotal);
	                        record.setFieldValue('custrecord_vatbill_taxamount', fxamount[k]);
	                        record.setFieldValue('custrecord_vatbill_status', 'Open');
	                        record.setFieldValue('custrecord_vatbill_type', rectype[k]);
	                        record.setFieldValue('custrecord_vatbill_taxid', taxcode[k]);
	                        record.setFieldValue('custrecord_vatbill_postingperiod', posting[k]);
	                        record.setFieldValue('custrecord_vatbill_location', locationArray[k]);
	                        var id = nlapiSubmitRecord(record, true);
	                        nlapiLogExecution('DEBUG', 'Bill Credit ', " New Vat Bill Credit Detail Record ID " + id)
	                        
	                    } // END for(var k=1;k<=rowcount;k++)
	                    
					}// end of search result check end
					
                    
                } // END if(type=='create')
                
                // ====  End :Code for create New VAT Bill  Detail custom Record ======
				
				
                if (type == 'edit') 	
				{
                    var column = new Array();
                    var filters = new Array();
                    filters.push(new nlobjSearchFilter('custrecord_vatbill_billno', null, 'is', billcreditid));
                    column.push(new nlobjSearchColumn('internalid'))
                    column.push(new nlobjSearchColumn('custrecord_vatbill_status'))
                    var results = nlapiSearchRecord('customrecord_vatbilldetail', null, filters, column);
                    
                    if (results != null) 	
					{
                    
                        for (var i = 0; results != null && i < results.length; i++) 
						{
                            var vatbilldetailId = results[i].getValue('internalid')
                            var vatbillStatus = results[i].getValue('custrecord_vatbill_status')
                            nlapiLogExecution('DEBUG', 'Bill Credit ', " Vat Bill Credit Detail ID " + vatbilldetailId)
                            nlapiLogExecution('DEBUG', 'Bill Credit ', " Vat Bill Credit Status " + vatbillStatus)
                            if (vatbilldetailId != null && vatbilldetailId != undefined && vatbilldetailId != '') 
							{
                                if (vatbillStatus == 'Open') 
								{
                                    nlapiLogExecution('DEBUG', 'Bill Credit ', " if(vatbillStatus == 'Open')")
                                    nlapiDeleteRecord('customrecord_vatbilldetail', vatbilldetailId);
                                }// vat bill status end
								
                            }// vat bill deatail id check
                            
                        }// end of for loop.
						
                    }// result end.
                    
                    if (search_results != null) 	
					{
                        for (var k = 0; k < search_results.length && search_results != null; k++) 	
						{
                        
                            record = nlapiCreateRecord('customrecord_vatbilldetail');
                            record.setFieldValue('custrecord_vatbill_billno', billcreditid)
							record.setFieldValue('custrecord_vatbill_tranno', billcreditid)
                            record.setFieldValue('custrecord_vatbill_account', account[k])
                            record.setFieldValue('custrecord_vatbill_vendor', vendor);
                            record.setFieldValue('custrecord_vatbill_billdate', date);
                            record.setFieldValue('custrecord_vatbill_grossamount', usertotal);
                            record.setFieldValue('custrecord_vatbill_taxamount', fxamount[k]);
                            record.setFieldValue('custrecord_vatbill_status', 'Open');
                            record.setFieldValue('custrecord_vatbill_type', rectype[k]);
                            record.setFieldValue('custrecord_vatbill_taxid', taxcode[k]);
                            record.setFieldValue('custrecord_vatbill_postingperiod', posting[k]);
                            record.setFieldValue('custrecord_vatbill_location', locationArray[k]);
                            var id = nlapiSubmitRecord(record, true);
                            nlapiLogExecution('DEBUG', 'Bill Credit ', " New Vat Bill Credit Detail Record ID " + id)
                            
                        } // END for(var k=1;k<=rowcount;k++)
                        
                    }// search result check null
                    
                }// type edit change
				
				
                
            }// end if (a_subisidiary[y] == i_subsidiary)
			
        }// bill credit object not null check
		
    }// bill credit id check.
	
	}// try block end
	catch(EX)
	{
		 nlapiLogExecution('DEBUG', 'Bill Credit ', "Exception thrown" + EX);
	}// catch block end
	} 	
	
	
    return true;
    
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
    // == FUNCTION FOR SEARCHING SUBSIDIARY GOLBAL PARAMETER ==
    function SearchGlobalParameter(){
    
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') {
            for (var i = 0; i < s_serchResult.length; i++) {
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill Credit ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
	
	 
    
}

function applySTRoundMethod(stRoundMethod,taxamount)
{
	var roundedSTAmount = taxamount;
	
	if(stRoundMethod == 2)
	{
		roundedSTAmount = Math.round(taxamount)
	}
	if(stRoundMethod == 3)
	{
		roundedSTAmount = Math.round(taxamount/10)*10;
	}
	if(stRoundMethod == 4)
	{
		roundedSTAmount = Math.round(taxamount/100)*100;
	}
	
	return roundedSTAmount;
}
// END FUNCTION =====================================================
