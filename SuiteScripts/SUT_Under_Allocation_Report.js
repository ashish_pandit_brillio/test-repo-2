/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 May 2015     Internship
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {

		var form = nlapiCreateForm('Under Allocation Report', false);

		// add the "From Date" and "To Date" fields
		form.addField("custpage_from_date", "date", "From Date").setMandatory(
				true);
		form.addField("custpage_to_date", "date", "To Date").setMandatory(true);

		form.addSubmitButton("Show Report");

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}
