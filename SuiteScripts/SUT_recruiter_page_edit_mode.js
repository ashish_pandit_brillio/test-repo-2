// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name: SUT_recruiter_page_edit_mode.js
    	Author:		 Rakesh K 
    	Company:	 Brillio
    	Date:		 03-02-2017
    	Version:	 1.0
    	Description: Script is uses for create user interface for recruiters in edit mode 


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --



    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         SUITELET
    		- suiteletFunction(request, response)


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================



//var postRecordID;
// BEGIN SUITELET ==================================================
//suiteletFunction_edit_recruiters(request, response)();
function suiteletFunction_edit_recruiters(request, response) {
	

    //try {
        if (request.getMethod() == 'GET') {

            // Get logged-in user
            var objUser = nlapiGetContext();
            var o_values = new Object();
            var values = new Object();
            var o_values_create = new Object();
            var i_employee_id = objUser.getUser();
            //nlapiLogExecution('debug', 'i_employee_id', i_employee_id);
            var s_role = getRoleName(i_employee_id, objUser.getRole());
            //nlapiLogExecution('debug', 'Role', objUser.getRole());
            var i_recID = request.getParameter('pid');
            var recmode = request.getParameter('edit');
            nlapiLogExecution('debug', 'i_recID', i_recID);
			nlapiLogExecution('debug', 'recmode', recmode);
            if (_logValidation(i_recID)) {
                var recid = nlapiDecrypt(i_recID, 'base64');
                //nlapiLogExecution('debug', 'recid', recid);
                //postRecordID = recid;
            }
            //nlapiLogExecution('debug', 'postRecordID', postRecordID);
            if (recmode == '1') {

                var o_recuriter = nlapiLoadRecord('customrecord_recruiters_details', recid);
                //nlapiLogExecution('debug', 'o_recuriter', o_recuriter);

                var vendor_name = o_recuriter.getFieldValue('custrecord_vendor_name');
                //nlapiLogExecution('debug', 'vendor_name', vendor_name);

                if (vendor_name != null && vendor_name != '') {
                   // nlapiLogExecution('Debug', 'vendor_name1', vendor_name);
                    values['vendor_name'] = "'" + vendor_name + "'";
                } else {
                    values['vendor_name'] = '';
                }
                if(recid){
                	
                	 values['record-internal-id'] = recid;
                } else{
                	
               	 values['record-internal-id'] = '';
               }
                values['edit-tr'] = '1'; // record in edit mode for display values at form 
                values['internal_id'] = recid;

                var federal_id = o_recuriter.getFieldValue('custrecord_federal_id');
                if (_logValidation(federal_id)) {
                    values['federal_id'] = federal_id;
                } else {
                    values['federal_id'] = '';
                }
                var contact_person = o_recuriter.getFieldValue('custrecord_contact_person');
                if (_logValidation(contact_person)) {
                    values['contact_person'] = "'" + contact_person + "'";
                } else {
                    values['contact_person'] = '';
                }
                var signing_authority = o_recuriter.getFieldValue('custrecord_signing_authority');
                if (_logValidation(signing_authority)) {
                    values['signing_authority'] = "'" + signing_authority + "'";
                } else {
                    values['signing_authority'] = '';
                }
                var email_for_administration = o_recuriter.getFieldValue('custrecord_email_for_administration');
                if (_logValidation(email_for_administration)) {
                    values['email_for_administration'] = email_for_administration;
                } else {
                    values['email_for_administration'] = '';
                }
                var i_customer = o_recuriter.getFieldValues('custrecord_vms_customer');
               // nlapiLogExecution('debug', 'i_customer', i_customer);
               
                var i_customer_text = o_recuriter.getFieldValues('custrecord_vms_customer');
               // nlapiLogExecution('debug', 'i_customer_text', i_customer_text);
                var str_customer = '';
                if(i_customer_text){
                	for(var i_cust = 0; i_cust<i_customer_text.length; i_cust++){
                		var i_customer = i_customer_text[i_cust];
                		//nlapiLogExecution('debug', 'i_customer', i_customer);
                		var c_name = nlapiLookupField('customer', i_customer, 'companyname');
                		//nlapiLogExecution('debug', 'c_name', c_name);
                		if(str_customer == ''){
                			//str_customer = c_name;
							str_customer += '<option value = "' + i_customer + '">' + c_name + '</option>';
                		}else{
                			//str_customer = str_customer +';'+c_name;
							str_customer += '<option value = "' + i_customer + '">' + c_name + '</option>';
                		}
                		
                	}
                	//nlapiLogExecution('debug', 'str_customer', str_customer);
                	values['i_customer_text'] = "'" + str_customer + "'";
                	//values['i_customer_text'] = str_customer;
                }
                
                        if (_logValidation(i_customer)) 
                        {

                        	var s_name = nlapiLookupField('customer', i_customer, 'companyname');
                        	// nlapiLogExecution('debug', 's_name', s_name);
                        	//values['c_selected'] = 'selected';
                        	values['customer_options'] = s_name;
                } else {

                    values['customer_options'] = '';
                }

                var email_for_requirements = o_recuriter.getFieldValue('custrecord_email_for_requirements');
                if (_logValidation(email_for_requirements)) {
                    values['email_for_requirements'] = email_for_requirements;
                } else {
                    values['email_for_requirements'] = '';
                }

                var s_phone_number = o_recuriter.getFieldValue('custrecord_phone_number');
               // nlapiLogExecution('debug', 'phone_number', s_phone_number);
                
                if (_logValidation(s_phone_number)) {
                    values['phone_number'] ="'" + s_phone_number + "'";
                } else{
                    values['phone_number'] = '';
                }
                var fax_number = o_recuriter.getFieldValue('custrecord_fax_number');
                if (_logValidation(fax_number)) {
                    values['fax_number'] = fax_number;
                } else {
                    values['fax_number'] = '';
                }
                var contract_type = o_recuriter.getFieldValue('custrecord_contract_type');
                if (_logValidation(contract_type)) {
                    values['contract_type'] = contract_type;
                } else {
                    values['contract_type'] = '';
                }
				values['contarcttypeid'] = "'" + contract_type + "'";
				
                var address_line = o_recuriter.getFieldValue('custrecord_address_line');
                //encodeURIComponent(address_line);
                //address_line.trim();
                if (_logValidation(address_line)) {
                    values['address_line'] = "'" + address_line + "'";
                } else {
                    values['address_line'] = '';
                }

                var address_2 = o_recuriter.getFieldValue('custrecord_address_2');
                //nlapiLogExecution('debug', 'address_2', address_2);

                if (_logValidation(address_2)) {
                    values['address_2'] = "'" + address_2 + "'";
                } else {
                    values['address_2'] = '';
                }

                var state_name_text = o_recuriter.getFieldText('custrecord_state_name');
                nlapiLogExecution('debug', 'state_name_text', state_name_text);
				
				var i_state_id = o_recuriter.getFieldValue('custrecord_state_name');
                
                if (_logValidation(state_name_text)) {
                    values['state_name_text'] = "'" + state_name_text + "'";
                    
                   // values['state_name_selected'] = 'selected'; 
                } else {
                    values['state_name_text'] = '';
                }
				
				values['stateid'] = "'" + i_state_id + "'";
				
                var _city = o_recuriter.getFieldValue('custrecord_city');
                if (_logValidation(_city)) {
                    values['_city'] = "'" + _city + "'";
                } else {
                    values['_city'] = '';
                }
                var zip_code = o_recuriter.getFieldValue('custrecord_zip_code');
                if (_logValidation(zip_code)) {
                    values['zip_code'] = zip_code;
                } else {
                    values['zip_code'] = '';
                }
                var need_follow_up_by_contracts = o_recuriter.getFieldValue('custrecord_need_follow_up_by_contracts');

                if (need_follow_up_by_contracts == 'T') {
                    var checked = 'checked';
                    values['need_follow_up_by_contracts'] = checked;
                } else {
                    values['need_follow_up_by_contracts'] = '';
                }
                //nlapiLogExecution('debug', 'need_follow_up_by_contracts', need_follow_up_by_contracts);

                var vendor_usa = o_recuriter.getFieldValue('custrecord_vendor_usa');
                //nlapiLogExecution('debug', 'vendor_usa', vendor_usa);

                if (vendor_usa == 'T') {
                    var checked = 'checked';
                    values['vendor_usa'] = checked;
                } else {
                    values['vendor_usa'] = '';
                }
                var netsuite_role = o_recuriter.getFieldValue('custrecord_netsuite_role');

                if (_logValidation(netsuite_role)) {
                    values['netsuite_role'] = netsuite_role;
                } else {
                    values['netsuite_role'] = '';
                }
                var status_recruiters = o_recuriter.getFieldValue('custrecord_approval_status_recruiters');

                if (_logValidation(status_recruiters)) {
                    values['status_recruiters'] = status_recruiters;
                } else {
                    values['status_recruiters'] = '';
                }

                var initiator_recruiter_name = o_recuriter.getFieldValue('custrecord_initiator_recruiter_name');
                nlapiLogExecution('debug', 'initiator_recruiter_name', initiator_recruiter_name);
                if (_logValidation(initiator_recruiter_name)) {
                    values['initiator_recruiter_name'] = "'" + initiator_recruiter_name + "'";;
                } else {
                    values['initiator_recruiter_name'] = '';
                }

                var date = o_recuriter.getFieldValue('custrecord_vms_date');
               // nlapiLogExecution('debug', 'date', date);
                if (_logValidation(date)) {
                    values['d_date'] = date;
                } else {
                    values['d_date'] = '';
                }
                var i_contracttype = o_recuriter.getFieldValue('custrecord_contract_type');
                nlapiLogExecution('debug', 'i_contracttype', i_contracttype);
                /*if (i_contracttype == 1) {
                    values['contracttype'] = 'One Person Corp';
                }
                if (i_contracttype == 2) {
                    values['contracttype'] = 'Single Client';
                }
                if (i_contracttype == 3) {
                    values['contracttype'] = 'Multiple Client Contract';
                }
                if (i_contracttype == 4) {
                    values['contracttype'] = 'Referral Deal';
                } else if (i_contracttype == '') {
                    values['contracttype'] = '';
                }*/
				
				var contract_type_str = o_recuriter.getFieldText('custrecord_contract_type');
				values['contracttype'] = "'" + contract_type_str + "'";;
                //nlapiLogExecution('audit', 'i_contracttype', contract_type_str);


            } else {
                values['federal_id'] = '';
                values['contact_person'] = '';
                values['signing_authority'] = '';
                values['phone_number'] = '';
                values['fax_number'] = '';
                values['contract_type'] = '';
                values['vendor_name'] = '';
                values['status_recruiters'] = '';
                values['netsuite_role'] = '';
                values['vendor_usa'] = '';
                values['need_follow_up_by_contracts'] = '';
                values['zip_code'] = '';
                values['_city'] = '';
                values['state_name'] = '';
                values['address_2'] = '';
                values['address_line'] = '';
                values['email_for_requirements'] = '';
                values['email_for_administration'] = '';
                values['initiator_recruiter_name'] = '';
                values['d_date'] = '';
                values['customer_options'] = '';

            }

            var o_employee_data = nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
            var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			var s_new_home_url	=	nlapiResolveURL('SUITELET', 'customscript_vendor_contract_screen', 'customdeploy1');
			//var s_my_requests_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
			var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
			var s_edit_mode_URL = nlapiResolveURL('SUITELET', 'customscript_sut_va_screen_in_edit_mode', 'customdeploy1');
			values['s_new_home_url']	=	s_new_home_url;
		 	values['new_request_url']	=	s_new_request_url;
		 	//values['my_requests_url']	=	s_my_requests_url;
		 	values['s_search_URL'] = 	s_search_URL;
		 	var o_json_state = getStateList();
			var strStateOptions= getStateDropdown(o_json_state);
			values['state_name'] = strStateOptions;
            var view_mode = false;
            values['username'] = o_employee_data.firstname + ' ' + o_employee_data.lastname;
            values['role'] = s_role;
           
            values['script-id-cancel'] = 1194;
            values['button_name'] = o_values.button_name;

            var o_json = getCustomerProjectJSON();
            //nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
            var strCustomerOptions = getCustomerDropdown(o_json);
            o_values.button_name = 'save_tr';

            values['customer-project-json'] = JSON.stringify(o_json);
            values['customer_options'] = strCustomerOptions;


            var file = nlapiLoadFile(2046236);
            var contents = file.getValue(); //get the contents

            //nlapiLogExecution('debug', 'if contents', contents);
            contents = replaceValues(contents, values);
            response.write(contents);
            //form.setScript('customscript_cli_validate_rec_field');


        }

        if (request.getMethod() == 'POST') {
        	var context = nlapiGetContext();
            var environment = context.getEnvironment();
            nlapiLogExecution('debug', 'POST');

            var feedback = new Object();

            feedback['Response1'] = request.getParameter('vendorname'); // vendor name 
            feedback['Response2'] = request.getParameter('federalid'); //federalid
            feedback['Response3'] = request.getParameter('contactperson'); //contactperson
            feedback['Response4'] = request.getParameter('signingauthority'); //signingauthority
            feedback['Response5'] = request.getParameter('emailforadmin'); //emailforadmin
            feedback['Response6'] = request.getParameter('emailforreq'); //emailforreq
            feedback['Response7'] = request.getParameter('phonenumber'); //phonenumber
            feedback['Response8'] = request.getParameter('faxnumber'); //faxnumber
            feedback['Response9'] = request.getParameter('contracttype'); //contracttype
            feedback['Response10'] = request.getParameter('address1'); //address1
            feedback['Response11'] = request.getParameter('address2'); //address2
            feedback['Response12'] = request.getParameter('statename'); //state
            feedback['Response13'] = request.getParameter('city'); //city
            feedback['Response14'] = request.getParameter('zipcode'); //zipcode
            // feedback['Response15'] = request.getParameter('field15');
            feedback['Response16'] = request.getParameter('field16');
            feedback['Response17'] = request.getParameter('field17');
            feedback['Response18'] = request.getParameter('initiator'); //initiator
            feedback['Response19'] = request.getParameter('clientname_contact');
            feedback['Response20'] = request.getParameter('internalidrecord');
            feedback['Response21'] = request.getParameter('approvalstatus');
           
            feedback['Response22'] = request.getParameter('stateid'); // state name with text 
            feedback['Response23'] = request.getParameter('contract_type_text'); // contract type with number
            feedback['Response24'] = request.getParameter('clientnametext'); // client name 
            // var status = saveRequest(feedback);
            var recordID = request.getParameter('internalidrecord');
            var o_status = saveTravelRequestDetails(request);
            nlapiLogExecution('debug', 'o_status:- '+o_status,'rcrd id:- '+recordID);
            if(o_status){
            	
            	var param = new Array();
            	param['recordid'] = recordID;
            	nlapiSetRedirectURL('RECORD', 'customrecord_recruiters_details', recordID,null,null);
            	  //response.sendRedirect('SUITELET', 'customscript_sut_upload_file_in_netsuite', 'customdeploy1', false);
                 // nlapiRequestURL('https://' + request.getHeader('Host'));
            	
            }else{
            	nlapiSetRedirectURL('SUITELET', 'customscript_sut_va_screen_in_edit_mode', 'customdeploy1');
            	 // response.sendRedirect('SUITELET', 'customscript_sut_va_screen_in_edit_mode', 'customdeploy1', false);
                  //nlapiRequestURL('https://' + request.getHeader('Host'));
            	
            }
          
            // response.write(thanks_contents);

        }

        //response.writePage(form_obj);


    //} catch (err) {
     //   nlapiLogExecution('ERROR', 'ERROR MESSAGE :- ', err);

    //}


}

function saveTravelRequestDetails(request) {

    //nlapiLogExecution('debug', 'saveTravelRequestDetails', request);
    var feedback = new Object();

    feedback['Response1'] = request.getParameter('vendorname'); // vendor name 
    feedback['Response2'] = request.getParameter('federalid'); //federalid
    feedback['Response3'] = request.getParameter('contactperson'); //contactperson
    feedback['Response4'] = request.getParameter('signingauthority'); //signingauthority
    feedback['Response5'] = request.getParameter('emailforadmin'); //emailforadmin
    feedback['Response6'] = request.getParameter('emailforreq'); //emailforreq
    feedback['Response7'] = request.getParameter('phonenumber'); //phonenumber
    feedback['Response8'] = request.getParameter('faxnumber'); //faxnumber
    feedback['Response9'] = request.getParameter('contracttype'); //contracttype
    feedback['Response10'] = request.getParameter('address1'); //address1
    feedback['Response11'] = request.getParameter('address2'); //address2
    feedback['Response12'] = request.getParameter('statename'); //state
    feedback['Response13'] = request.getParameter('city'); //city
    feedback['Response14'] = request.getParameter('zipcode'); //zipcode
    // feedback['Response15'] = request.getParameter('field15');
    feedback['Response16'] = request.getParameter('field16');
    feedback['Response17'] = request.getParameter('field17');
    feedback['Response18'] = request.getParameter('initiator'); //initiator
    feedback['Response19'] = request.getParameter('clientname_contact');
    feedback['Response20'] = request.getParameter('internalidrecord');
    feedback['Response21'] = request.getParameter('approvalstatus');
    feedback['Response22'] = request.getParameter('statename'); // state name with text 
    feedback['Response23'] = request.getParameter('contracttype'); // contract type with number
    feedback['Response24'] = request.getParameter('clientnametext'); // client name 
    var o_status = saveReqdetail(request, feedback);
    return o_status;

}



// Edit RECORD FOR REQ SCREEN 

function saveReqdetail(request, feedback) {

    try {
       
    	//nlapiLogExecution('debug', 'saveReqdetail', request);
        // Get logged-in user
        var objUser = nlapiGetContext();
        var i_employee_id = objUser.getUser();
        //nlapiLogExecution('debug', 'postRecordID', postRecordID);

        var feedback_form = nlapiLoadRecord('customrecord_recruiters_details',  feedback.Response20);
        //nlapiLogExecution('debug', 'feedback_form', feedback_form);
        
        var s_vendo = feedback_form.getFieldValue('custrecord_vendor_name');
        feedback_form.setFieldValue('custrecord_vendor_name', s_vendo); // vendor name 
        
        var s_federal_id = feedback_form.getFieldValue('custrecord_federal_id');
        feedback_form.setFieldValue('custrecord_federal_id', s_federal_id); // federal id 
        
        var d_date = feedback_form.getFieldValue('custrecord_vms_date');
        feedback_form.setFieldValue('custrecord_vms_date', d_date); // Date
        
        
      
        //feedback_form.setFieldValue('custrecord_federal_id', feedback.Response2);
        feedback_form.setFieldValue('custrecord_contact_person', feedback.Response3);
        feedback_form.setFieldValue('custrecord_signing_authority', feedback.Response4);
        feedback_form.setFieldValue('custrecord_email_for_administration', feedback.Response5);
        feedback_form.setFieldValue('custrecord_email_for_requirements', feedback.Response6);
        //nlapiLogExecution('debug', 'phone number', feedback.Response7);
        feedback_form.setFieldValue('custrecord_phone_number', feedback.Response7);
        feedback_form.setFieldValue('custrecord_fax_number', feedback.Response8);
        feedback_form.setFieldValue('custrecord_netsuite_role', i_employee_id);
        //nlapiLogExecution('debug', 'State name', feedback.Response12);
       
        /*if (feedback.Response9 == 'onepersoncorp') {
            feedback_form.setFieldValue('custrecord_contract_type', 1);
        }
        if (feedback.Response9 == 'singleclient') {
            feedback_form.setFieldValue('custrecord_contract_type', 2);
        }
        if (feedback.Response9 == 'multipleclientcontract') {
            feedback_form.setFieldValue('custrecord_contract_type', 3);
        }
        if (feedback.Response9 == 'referraldeal') {
            feedback_form.setFieldValue('custrecord_contract_type', 4);
        }*/

        feedback_form.setFieldValue('custrecord_address_line', feedback.Response10);
        feedback_form.setFieldValue('custrecord_address_2', feedback.Response11);
        
        //feedback_form.setFieldValue('custrecord_state_name', feedback.Response12);
        // code added 23-03-2017 for changes 
        feedback_form.setFieldValue('custrecord_state_name', feedback.Response22);
       // nlapiLogExecution('debug', 'feedback.Response23 ', feedback.Response23);
        //nlapiLogExecution('debug', 'feedback.Response22 ', feedback.Response22);
        if (feedback.Response23 == '1') {
            feedback_form.setFieldValue('custrecord_contract_type', 1);
        }
        if (feedback.Response23 == '2') {
            feedback_form.setFieldValue('custrecord_contract_type', 2);
        }
        if (feedback.Response23 == '3') {
            feedback_form.setFieldValue('custrecord_contract_type', 3);
        }
        if (feedback.Response23 == '4') {
            feedback_form.setFieldValue('custrecord_contract_type', 4);
        }
        
        feedback_form.setFieldValue('custrecord_city', feedback.Response13);
        feedback_form.setFieldValue('custrecord_zip_code', feedback.Response14);
        
        
        if (feedback.Response16 == 'on') {
            feedback_form.setFieldValue('custrecord_need_follow_up_by_contracts', 'T');
        }
        if (feedback.Response17 == 'on') {
            feedback_form.setFieldValue('custrecord_vendor_usa', 'T');

        }
        feedback_form.setFieldValue('custrecord_initiator_recruiter_name', feedback.Response18);
        //var str = feedback.Response19;
        var str = feedback.Response24;
		var a_client = new Array();
		nlapiLogExecution('debug', 'str means client name', str);
		/*if(feedback.Response19){
			
			for(var j=0 ; j<feedback.Response19.length; j++){
				  
					var res = str.split(",");
					  if(res[j] != null && res[j] != '' && res[j] !='null'){
						  a_client.push(res[j]);
					  }
				
			  }
	        feedback_form.setFieldValues('custrecord_vms_customer', a_client);
		} */
		if(feedback.Response24){
			
			for(var j=0; j<feedback.Response24.length; j++){
				  
					var res = str.split(",");
					  if(res[j] != null && res[j] != '' && res[j] !='null'){
						  a_client.push(res[j]);
					  }
				
			  }
	        feedback_form.setFieldValues('custrecord_vms_customer', a_client);
		} 
		nlapiLogExecution('debug', 'a_client', a_client);
		
		var returnstatus;
        if(feedback.Response21 == 'Approve'){
        	feedback_form.setFieldValue('custrecord_approval_status_recruiters', 2);
        	feedback_form.setFieldValue('custrecord_docusign_process', "T");
        	
        	returnstatus = 1;
        } 
        if(feedback.Response21 == 'Reject'){
        	
        	feedback_form.setFieldValue('custrecord_approval_status_recruiters', 3);
        	//feedback_form.setFieldValue('custrecord_docusign_process', "F");
        	returnstatus = 3;
        }
        
        //feedback_form.setFieldValue('custrecord_contract_type', feedback.Response9);

        var rec_id = nlapiSubmitRecord(feedback_form, true, true);
        nlapiLogExecution('debug', 'Record Saved', rec_id);
      

        return returnstatus;
    } catch (e) {
        return ({
            'status': -1,
            'message': 'Error: ' + e.message,
            'tr_id': null
        });
    }
}

// get VIEW Mode 

function getViewMode(i_status, i_logged_in_user, i_tr_employee_id) {
    if (i_status == 1 && i_logged_in_user == i_tr_employee_id) {
        return false;
    }

    return true;
}


// function for get list of customer form NetSuite Database
function getCustomerList() {
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('isinactive', null, 'is', "F");
    var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
    columns[1] = new nlobjSearchColumn('altname');

    var searchResults = searchRecord('customer', null, filters, columns);

    var a_data = new Array();

    for (var i = 0; i < searchResults.length; i++) {

        var s_customer_id = searchResults[i].getValue(columns[0]);
        var s_customer_name = searchResults[i].getValue(columns[1]);

        a_data.push({
            'customer_id': s_customer_id,
            'customer_name': s_customer_name
        });
    }

    return a_data;
}
//
function searchRecord(recordType, savedSearch, arrFilters, arrColumns, filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}


// END SUITELET ====================================================
// create customer JSON 
function getCustomerProjectJSON() {
    var a_data = getCustomerList();
    //nlapiLogExecution('debug', 'getCustomerProjectJSON', a_data);
    var o_json = new Object();

    for (var i = 0; i < a_data.length; i++) {

        var o_customer = {
            'id': a_data[i].s_customer_id,
            'value': a_data[i].s_customer_name
        };
        //nlapiLogExecution('Debug', 'o_customer', JSON.stringify(o_customer));
        if (o_json[a_data[i].customer_id] != undefined) {
            o_json[a_data[i].customer_id].list.push(o_customer);
        } else {
            o_json[a_data[i].customer_id] = {
                'name': a_data[i].customer_name
            };
        }

    }
    //nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
    //nlapiLogExecution('debug', 'getCustomerProjectJSON', a_data);
    return a_data;

}


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
    // function is used for replace the string code which is created for different-2 purpose to display content on user interface  
    function replaceValues(content, oValues) {
        for (param in oValues) {
            // Replace null values with blank
            var s_value = (oValues[param] == null) ? '' : oValues[param];

            // Replace content
            content = content.replace(new RegExp('{{' + param + '}}', 'gi'), s_value);
        }

        return content;
    }



}


// END OBJECT CALLED/INVOKING FUNCTION =====================================================
// get ROLE name 

function getRoleName(i_user_id, i_role_id) {
    var a_search_results = nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));

    if (a_search_results != null && a_search_results.length == 1) {
        return a_search_results[0].getText('role');
    } else {
        return '';
    }
}

function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions) {
    var strOptions = '';

    if (removeBlankOptions == true) {

    } else {
        strOptions = '<option value = "">' + (s_placeholder == undefined ? '' : s_placeholder) + '</option>';
    }

    var strSelected = '';

    for (var i = 0; i < a_data.length; i++) {
        if (i_selected_value == a_data[i].value) {
            strSelected = 'selected';
        } else {
            strSelected = '';
        }
        strOptions += '<option value = "' + a_data[i].value + '" ' + strSelected + ' >' + a_data[i].display + '</option>';
    }
    //nlapiLogExecution('Debug', 'stringsss', strOptions);
    return strOptions;
}


function getCustomerDropdown(o_json) {
    var list = new Array();
    var strOptions = '';
    var i_selected_value = '';

    for (var x in o_json) {

        strOptions += '<option value = "' + o_json[x].customer_id + '">' + o_json[x].customer_name + '</option>';
        /*list.push({'display': o_json[x].s_customer_name, 'value': x});
			
        for(var i = 0; i < o_json[x].list.length; i++)
        	{
        		//if(o_json[x].list[i].id	==	i_project_id)
        			{
        				i_selected_value	=	x;
        			}
        	}*/

    }
    return strOptions;
    //return getOptions(list, i_selected_value);
}

// get status list 
function getStateList() {
	
	var o_vendor_rec = nlapiCreateRecord('vendor');
	
	var fld_state = o_vendor_rec.getField('custentity_vendor_state');
		
	var state_options = fld_state.getSelectOptions();
	var a_data = new Array(); 
	for(var i=0; i<state_options.length; i++){
		
		var s_stateID = state_options [i].getId();
		var s_state_name = state_options [i].getText() ;
		a_data.push({ 'state_id': s_stateID, 'state_name': s_state_name});
		//nlapiLogExecution('DEBUG', state_options [i].getId() + ',' + state_options [i].getText() ); 
	}

	//nlapiLogExecution('debug','state_options',state_options);
	//var i=0;
	return a_data;
}

//get state drop down list

function getStateDropdown(o_json){
	var list	=	new Array();
	var strOptions	=	'';
	var i_selected_value	=	'';
	
	for(var x in o_json){
		
		strOptions += '<option value = "'+o_json[x].state_id+'">' + o_json[x].state_name + '</option>';
			/*list.push({'display': o_json[x].s_customer_name, 'value': x});
			
			for(var i = 0; i < o_json[x].list.length; i++)
				{
					//if(o_json[x].list[i].id	==	i_project_id)
						{
							i_selected_value	=	x;
						}
				}*/
			
		}
	return strOptions;
	//return getOptions(list, i_selected_value);
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN') {
        return true;
    } else {
        return false;
    }
}