/**
 * @author Nikhil Jain
 */
function getAITIndianCurrency()
{
	var currency_id;
	
	var curFilters = new Array();
	var curColumns = new Array();
	
	curFilters.push(new nlobjSearchFilter('symbol', null, 'is', 'INR'));
	curColumns.push(new nlobjSearchColumn('internalid'));
	
	var curSearchResults = nlapiSearchRecord('currency', null, curFilters, curColumns);
	{
		currency_id = curSearchResults[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'currency = ' + currency_id);
	}
	
	return currency_id;
}