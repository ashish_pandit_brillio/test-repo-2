// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name : REST_ExternalHire.js
    	Author      : ASHISH PANDIT
    	Date        : 03 APRIL 2019
        Description : RESTlet to show External Hire data 


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
     


    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         SCHEDULED FUNCTION
    		- scheduledFunction(type)


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
//============================================================================================================


/*RESTlet Main function*/
function REST_externalHire(dataIn) {
    try {
        var useremail = dataIn.user;
        var user = getUserUsingEmailId(useremail);
        var values = {};
        nlapiLogExecution('Debug', 'Script Parameter user ', user);
        if (user) {
            var spocSearch = GetPracticeSPOC(user);
            var deliveryAnchore = GetDeliveryAnchore(user);
            nlapiLogExecution('Debug', ' spocSearch  ', spocSearch);
            nlapiLogExecution('Debug', ' deliveryAnchore  ', deliveryAnchore);
            var customrecord_fuel_adminSearch = nlapiSearchRecord("customrecord_fuel_leadership", null,
                [
                    ["custrecord_fuel_leadership_employee", "anyof", user],
                    "AND",
                    ['isinactive', 'is', 'F']
                ],
                [
                    new nlobjSearchColumn("custrecord_fuel_leadership_employee")
                ]
            );
            if (customrecord_fuel_adminSearch) {
                values.rrfdata = getRrfData(null, null, null);
            } else if (spocSearch.length > 0) {
                var practice = spocSearch; //[0].getValue('custrecord_practice');
                values.rrfdata = getRrfData(null, practice, null);
            } else if (deliveryAnchore) {
                values.rrfdata = getRrfData(null, null, deliveryAnchore);
            } else {
                values.rrfdata = getRrfData(user, null, null);
            }
        }
        return values;
    } catch (error) {
        nlapiLogExecution('Debug', 'Error ', error);
        return {
            "code": "error",
            "message": error
        }
    }
}


/****Function to get Training List ****/
function getRrfData(user, practice, deliveryAnchore) {
    try {
        var filters = new Array();
        var columns = new Array();
        if (user) {
            filters = [
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_external_hire", "is", "T"],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
                "AND",
                [
                    ["custrecord_frf_details_project.custentity_projectmanager", "anyof", user],
                    "OR",
                    ["custrecord_frf_details_project.custentity_deliverymanager", "anyof", user],
                    "OR",
                    ["custrecord_frf_details_project.custentity_clientpartner", "anyof", user]
                ],
                "OR",
                [
                    ["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm", "anyof", user],
                    "OR",
                    ["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm", "anyof", user],
                    "OR",
                    ["custrecord_frf_details_opp_id.custrecord_sfdc_client_partner", "anyof", user]
                ], "AND", ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Deleted"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Canceled"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Rejected"]
            ]
        } else if (practice) {
            filters = [
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_res_practice.custrecord_parent_practice", "anyof", practice],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["custrecord_frf_details_external_hire", "is", "T"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], "AND", ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Deleted"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Canceled"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Rejected"]
            ]
        } else if (deliveryAnchore) {
            filters = [
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_account", "anyof", deliveryAnchore],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["custrecord_frf_details_external_hire", "is", "T"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"], "AND", ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Deleted"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Canceled"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Rejected"]
            ]
        } else {
            filters = [
                ["isinactive", "is", "F"],
                "AND",
                ["custrecord_frf_details_external_hire", "is", "T"],
                "AND",
                ["custrecord_frf_details_status", "is", "F"],
                "AND",
                ["custrecord_frf_details_opp_id.custrecord_stage_sfdc", "noneof", "9", "8"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Deleted"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Canceled"],
                "AND",
                ["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status", "isnot", "Rejected"]
            ]
        }

        columns = [
            new nlobjSearchColumn("custrecord_frf_details_opp_id"),
            new nlobjSearchColumn("custrecord_frf_details_account"),
            new nlobjSearchColumn("custrecord_frf_details_frf_number"),
            new nlobjSearchColumn("custrecord_frf_details_res_location"),
            new nlobjSearchColumn("custrecord_frf_details_res_practice"),
            new nlobjSearchColumn("custrecord_frf_details_project"),
            new nlobjSearchColumn("custrecord_frf_details_role"),
            new nlobjSearchColumn("custrecord_frf_details_emp_level"),
            new nlobjSearchColumn("custrecord_frf_details_external_hire"),
            new nlobjSearchColumn("custrecord_frf_details_bill_rate"),
            new nlobjSearchColumn("custrecord_frf_details_critical_role"),
            new nlobjSearchColumn("custrecord_frf_details_skill_family"),
            new nlobjSearchColumn("custrecord_frf_details_start_date"),
            new nlobjSearchColumn("custrecord_frf_details_end_date"),
            new nlobjSearchColumn("custrecord_frf_details_selected_emp"),
            new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"),
            new nlobjSearchColumn("custrecord_frf_details_taleo_emp_select"),
            new nlobjSearchColumn("custrecord_frf_details_emp_joining_date"),
            new nlobjSearchColumn("custrecord_frf_details_primary_skills"),
            new nlobjSearchColumn("custrecord_frf_type"),
            new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"),
            new nlobjSearchColumn("custrecord_frf_details_billiable"),
            new nlobjSearchColumn("custrecord_frf_details_source"),
            new nlobjSearchColumn("custrecord_frf_details_created_by"),
            new nlobjSearchColumn("custrecord_frf_details_created_date"),
            new nlobjSearchColumn("custrecord_frf_details_parent"),
            new nlobjSearchColumn("custrecord_frf_details_status_flag"),
            new nlobjSearchColumn("custrecord_frf_details_availabledate"),
            new nlobjSearchColumn("custrecord_frf_details_allocated_date"),
            new nlobjSearchColumn("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null),
            new nlobjSearchColumn("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null),
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "custrecord_frf_details_opp_id", null),
            //===============================================================================================================
            new nlobjSearchColumn("custrecord_taleo_screening_stage", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_inter_stg_first_level_c", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_interview_stage_second_lvl_c", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_interview_conducted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_extenal_hr_round", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_offer_made_conducted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_external_offer_rejected", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),


            new nlobjSearchColumn("custrecord_taleo_ext_back_check", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("created", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("externalid", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_ext_hire_frf_details", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("isinactive", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("internalid", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_external_joining_date", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("lastmodified", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("lastmodifiedby", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_external_rrf_number", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_external_selected_can", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
            new nlobjSearchColumn("entityid", "custrecord_frf_details_account", null),
            new nlobjSearchColumn("custrecord_frf_details_fitment_type"), //prabhat gupta 07/9/2020 NIS-1723
            new nlobjSearchColumn("custrecord_frf_details_level"), //prabhat gupta 06/10/2020 NIS-1755
            //new nlobjSearchColumn("custrecord_taleo_external_selected_can","custrecord_frf_details_rrf_number",null)
            new nlobjSearchColumn("custrecord_frf_details_red_date"),
           new nlobjSearchColumn("custrecord_frf_desired_start_date"),
           new nlobjSearchColumn("custrecord_parent_practice","CUSTRECORD_FRF_DETAILS_RES_PRACTICE",null),
           new nlobjSearchColumn("custrecord_frf_details_position_type"),
           new nlobjSearchColumn("custrecord_frf_details_hire_type"),
           	   //===================================================================================================================
        ]
        var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns, null);
        if (customrecord_frf_detailsSearch) {
            nlapiLogExecution('Debug', 'customrecord_frf_detailsSearch length ', customrecord_frf_detailsSearch.length);
            var dataArray = new Array();
            for (var i = 0; i < customrecord_frf_detailsSearch.length; i++) {
                var data = {};
                data.frfnumber = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_frf_number")
                };
                if (customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project")) {
                    data.project = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project"),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project")
                    };
                } else if (customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_opp_id")) {
                    data.project = {
                        "name": customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc", "custrecord_frf_details_opp_id", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_opp_id")
                    };
                }

                 var parentPractice = customrecord_frf_detailsSearch[i].getText("custrecord_parent_practice","CUSTRECORD_FRF_DETAILS_RES_PRACTICE",null) || "";
					var parentPracticeId = customrecord_frf_detailsSearch[i].getValue("custrecord_parent_practice","CUSTRECORD_FRF_DETAILS_RES_PRACTICE",null) || "";
					
					data.practice = {
	                    "name": parentPractice,
	                    "id": parentPracticeId
	                };

                 var subPractice =  customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_practice");
				 var subPracticeId =  customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_practice");
				  
				  if(subPracticeId == parentPracticeId){// Fuel Team is using this key as a unique value to show in a subpractice dropdown that's why we are excluding parent practice if FRF raised against parent practice implications already discuss with Ganapati
					  subPractice = "";
					  subPracticeId = "";
				  }
				  
                data.subPractice = {
                    "name": subPractice,
                    "id": subPracticeId
                };
              
         
					
					
              
                if (customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_role"))
                    data.role = {
                        "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_role")
                    };
                else
                    data.role = {
                        "name": ''
                    };
                data.location = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_location"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_location")
                };
                data.emplevel = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_emp_level"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_emp_level")
                };
                data.exthire = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire")
                };
                data.billrate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_bill_rate")
                };
                data.account = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account")
                };
                data.criticalrole = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_critical_role")
                };
                data.skillfamily = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_skill_family"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_skill_family")
                };
                data.startdate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date")
                };
                data.enddate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_end_date")
                };
                data.selectedemp = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_selected_emp"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp")
                };
                data.internalfulfill = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_internal_fulfill")
                };
                data.primaryskill = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_primary_skills"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_primary_skills")
                };
                data.frftype = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_type"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_type")
                };

                //---------------------------------------------------------------------------------------------------
                //prabhat gupta 07/09/2020 NIS-1723

                data.fitmentType = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_fitment_type"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_fitment_type")
                };
                //---------------------------------------------------------------------------------------------------

                //---------------------------------------------------------------------------------------------------
                //prabhat gupta 06/10/2020 NIS-1755

                data.level = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_level"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_level")
                };
                //---------------------------------------------------------------------------------------------------
                data.redDate = {
					"name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_red_date")
				}
				
				data.desiredStartDate = {
					"name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_desired_start_date")
				}


                data.createdDate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_created_date")
                };

                data.billable = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_billiable")
                };
                data.availabledate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_availabledate")
                };
                data.allocateddate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_allocated_date")
                };
                data.source = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_source"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_source")
                };
                data.frfstatus = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_status_flag"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_status_flag")
                };
                data.candidatejoined = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_taleo_emp_select"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_taleo_emp_select")
                };
                data.joiningdate = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_emp_joining_date")
                };
                //==========================================================================================================
                data.screening = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_screening_stage", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.interview1 = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_inter_stg_first_level_c", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.interview2 = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_interview_stage_second_lvl_c", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.clientinterview = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_interview_conducted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.hr_round = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_extenal_hr_round", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.offerd = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_offer_made_conducted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.rejected = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_external_offer_rejected", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.rrfnumber = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_external_rrf_number", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                data.status = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                //data.accountid = {"name" : customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account","entityid",null)};
                data.accountid = {
                    "name": customrecord_frf_detailsSearch[i].getValue("entityid", "custrecord_frf_details_account", null)
                };
                data.position = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_position_type"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_position_type")
                };
                data.hireType = {
                    "name": customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_hire_type"),
                    "id": customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_hire_type")
                };
                data.candidatename = {
                    "name": customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_external_selected_can", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
                };
                //==========================================================================================================
                if (customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null)) {
                    data.region = {
                        "name": customrecord_frf_detailsSearch[i].getText("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_ACCOUNT", null)
                    };
                } else if (customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null)) {
                    data.region = {
                        "name": customrecord_frf_detailsSearch[i].getText("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null),
                        "id": customrecord_frf_detailsSearch[i].getValue("custentity_region", "CUSTRECORD_FRF_DETAILS_PROJECT", null)
                    };
                } else {
                    data.region = {
                        "name": 'other'
                    };
                }
                dataArray.push(data);
            }
            return dataArray;
        }
    } catch (error) {
        nlapiLogExecution('Error', 'Exception ', error);
    }
}


/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

function GetPracticeSPOC(user) {
    var practiceArray = new Array();
    nlapiLogExecution('AUDIT', 'user', user);
    var spocSearch = nlapiSearchRecord("customrecord_practice_spoc", null,
        [
            ["custrecord_spoc", "anyof", user],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_practice")
        ]
    );
    if (spocSearch) {
        for (var i = 0; i < spocSearch.length; i++) {
            practiceArray.push(spocSearch[i].getValue('custrecord_practice'));
        }
    }
    return practiceArray;
}

function GetDeliveryAnchore(user) {
    var temp = new Array();
    var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor", null,
        [
            ["custrecord_fuel_anchor_employee", "anyof", user]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_anchor_customer")
        ]
    );
    if (customrecord_fuel_delivery_anchorSearch) {
        for (var i = 0; i < customrecord_fuel_delivery_anchorSearch.length; i++) {
            temp.push(customrecord_fuel_delivery_anchorSearch[i].getValue('custrecord_fuel_anchor_customer'));
        }
    }
    return temp;
}