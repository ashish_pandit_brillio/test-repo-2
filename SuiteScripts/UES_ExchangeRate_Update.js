/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Oct 2018     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
  try{
	  var rec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	  rec.getFieldValue('custrecord_pl_currency_exchange_revnue_r');
	  
	  var filtr = [];
	  filtr.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	  
	  var cols = [];
	  cols.push(new nlobjSearchColumn('internalid'));
	  cols.push(new nlobjSearchColumn('lastmodified').setSort(true));
	  cols.push(new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r'));
	  cols.push(new nlobjSearchColumn('custrecord_pl_currency_exchange_month'));
	  cols.push(new nlobjSearchColumn('custrecord_pl_currency_exchange_year'));
	  
	  var searcRes = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,filtr,cols);
	  
	  if(searcRes){
	  var filtr_ = [];
	  filtr_.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	  
	  var cols_ = [];
	  cols_.push(new nlobjSearchColumn('internalid').setSort(true));
	  
	  var searcRes_ = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,filtr_,cols_);
	  if(searcRes_){
	  var loadRec_SFDC = nlapiLoadRecord('customrecord_conversion_rate_sfdc',searcRes_[0].getValue('internalid'));
	  loadRec_SFDC.setFieldValue('custrecord_rate_inr_to_usd',searcRes[0].getValue('custrecord_pl_currency_exchange_revnue_r'));
	  nlapiSubmitRecord(loadRec_SFDC);
	  nlapiLogExecution('DEBUG','update done',loadRec_SFDC);
	  }
	  }
  }
  catch(e){
	  nlapiLogExecution('DEBUG','update error',e);
  }
}
