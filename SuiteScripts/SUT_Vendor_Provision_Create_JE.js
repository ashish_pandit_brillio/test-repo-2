// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Vendor_Provision_Create_JE.js
	Author      : Jayesh Dinde
	Date        : 21 April 2016
	Description : Create JE based on vendor provision salary upload record entries.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --													  
                                                                                         																						  

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

// BEGIN SUITELET FUNCTION==================================================

function create_JE_suitelet_func(request, response)
{
	try
	{
		var rcrd_id = request.getParameter('rcrd_id');
		rcrd_id = rcrd_id.trim();
		var vendor_provision_month = request.getParameter('vendor_provision_month');
		var vendor_provision_year = request.getParameter('vendor_provision_year');
		var vendor_provision_debit_acnt = request.getParameter('vendor_provision_debit_acnt');
		var vendor_provision_credit_acnt = request.getParameter('vendor_provision_credit_acnt');
		var vendor_provision_subsidiary = request.getParameter('vendor_provision_subsidiary');
		var rate_type = nlapiLookupField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_rate_type');
		nlapiLogExecution('audit','rate:- ',rate_type);
		var array_fld = new Array();
		array_fld[0] = 'custrecord_status_btpl';
		array_fld[1] = 'custrecord_completion_status';
		array_fld[2] = 'custrecord_notes_btpl';
		array_fld[3] = 'custrecord_prcnt_complete';
		array_fld[4] = 'custrecord_which_bttn_clicked';
		
		if(rate_type == 2)
		{
			var array_fld_values = new Array();
			array_fld_values[0] = 6;
			array_fld_values[1] = 'Not Started';
			array_fld_values[2] = '';
			array_fld_values[3] = '';
			array_fld_values[4] = 'CREATE_JE';
		
			var params=new Array();
			params['custscript_rcrd_id'] = rcrd_id;
			params['custscript_vendor_provision_month'] = vendor_provision_month;
			params['custscript_vendor_provision_year'] = vendor_provision_year;
			params['custscript_debit_account'] = vendor_provision_debit_acnt;
			params['custscript_credit_account'] = vendor_provision_credit_acnt;
			params['custscript_ven_pro_subsidiary_id'] = vendor_provision_subsidiary;
		 
			var status = nlapiScheduleScript('customscript_sch_vendor_pro_create_je',null,params);
		}
		else
		{
			var array_fld_values = new Array();
			array_fld_values[0] = 6;
			array_fld_values[1] = 'Not Started';
			array_fld_values[2] = '';
			array_fld_values[3] = '';
			array_fld_values[4] = 'CREATE_JE_HOURLY';
		
			var params=new Array();
			params['custscript_vendor_pro_record_id'] = rcrd_id;
			params['custscript_month_processed'] = vendor_provision_month;
			params['custscript_year_processed'] = vendor_provision_year;
			params['custscript_debit_account_used'] = vendor_provision_debit_acnt;
			params['custscript_credit_account_used'] = vendor_provision_credit_acnt;
			params['custscript_subsidiary_internal_id'] = vendor_provision_subsidiary;
			
			var status=nlapiScheduleScript('customscript_vendor_pro_create_je_hourly',null,params);
		}
		nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,array_fld,array_fld_values);
		 
		nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process_btpl', rcrd_id, false);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}

// END SUITELET FUNCTION==================================================
