function postRESTlet(dataIn)
{
	try
	{
			nlapiLogExecution('Debug','dataIn',JSON.stringify(dataIn));
			var request_Type = 'GET';
			var response = new Response();
			switch(request_Type)
			{
				case M_Constants.Request.Get:
				if(!dataIn instanceof Array)
				{
                    response.Data = "Some error with the data sent";
                    response.Status = false;
				}
			    else
				{
					response.Data = getProjectDetails(dataIn);
                    response.Status = true;
                }
			break;
			}
          return response;
	}

	catch(error)
	{
		nlapiLogExecution('debug','postRESTlet',error);
		response.Data = error;
		response.status = false;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));

}
var month_array = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
function getProjectDetails(dataIn)
{
	try{
		var excel_data=[];
		var sr_num=0;
		//Added code
		var a_project_list = new Array();
		var requestData = dataIn.project;
		//var requestData = [138764,84486,88180,142968,65557,121898,126336];
		//var requestData = [138764];
		//var internal_id_search = '';
		//internal_id_search = getSelectedProjects(requestData)
		if(requestData)
		{
			for(var id_lent=0;id_lent<requestData.length;id_lent++)
			{
				var int_project_id = requestData[id_lent];	
				var data=getMargin(int_project_id);
				for(var dataln=0;dataln<data.length;dataln++)
				{
					excel_data[sr_num] = {
                                    'project_id':data[dataln].project_id,
                                    'project_name':data[dataln].project_name,
                                    'sold_margin': data[dataln].sold_margin,
                                    'billing_type': data[dataln].billing_type,
									'people_cost': (data[dataln].people_cost).toFixed(2) ,
									'facility_cost': (data[dataln].facilitycost).toFixed(2) ,
									//'rev': (data[dataln].rev).toFixed(2) ,
									'ytd_exp': (data[dataln].ytd_exp).toFixed(2) ,
									'ytd_rev': (data[dataln].ytd_rev).toFixed(2) ,
                                    'actual_margin':data[dataln].actual_margin,
                                                                
                                   };
						sr_num++;
		
				}
			}
		} 
	return excel_data;
	}
catch (err)
{
	nlapiLogExecution('DEBUG','Error in getProjectDetails',err);
}	

}
function getMargin(int_project_id)
{
	var sr_no = 0;
	var bill_data_arr = new Array();
	var a_project_list = new Array();
	var a_selected_project_list = '';
	var i_user_id = nlapiGetUser();
	var search = nlapiLoadSearch('transaction', 1897);
	var filters = search.getFilters();
	//Search for exchange Rate
	//Search for exchange Rate
		var f_rev_curr = 0;
		var f_cost_curr = 0;
		var filters = [];
	
		//var filters = [];
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
			var a_dataVal = {};
			var dataRows = [];

			var column = new Array();
			column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
			column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
			column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
			column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
			//column[4] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');
			column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
			column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
			column[6] = new nlobjSearchColumn('custrecord_eur_usd_conv_rate');
			column[7] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');
			column[8] = new nlobjSearchColumn('custrecord_aud_to_usd');

			var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
			if (currencySearch) {
				for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {

					a_dataVal = {
						s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
						i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
						rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
						cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
						gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_gbp_converstion_rate'),
						crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
						nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate'),
						 eur_conv_rate : currencySearch[i_indx].getValue('custrecord_eur_usd_conv_rate'),
						 aud_conv_rate : currencySearch[i_indx].getValue('custrecord_aud_to_usd')


					};
					if (_logValidation(a_dataVal))
						dataRows.push(a_dataVal);


				}
			}
	
	var project_search_results = '';
	project_search_results = getSelectedProjects(int_project_id);
	
	var s_project_options = '';
	for (var i = 0; project_search_results != null&& i < project_search_results.length; i++) {
		var i_project_id = project_search_results[i].getValue('internalid');
		var s_project_number = project_search_results[i]
			        .getValue('entityid');
		var s_project_name = project_search_results[i].getValue('companyname');
		var sold_margin = project_search_results[i].getValue('custentity_sold_margin_percent');
		var billing_type = project_search_results[i].getText('jobbillingtype');
		var st_date=project_search_results[i].getValue('startdate');
		//st_date=nlapiDateToString(st_date);
		var end_date=project_search_results[i].getValue('enddate');
		//end_date=nlapiDateToString(end_date);
		s_from = st_date;
		var project_name = project_search_results[i].getValue('companyname');
		var pro_internal_id = project_search_results[i].getId();
		

		if(_logValidation(s_project_number)){
				a_project_list.push(s_project_number);
		}
		var s_selected = '';
		if ((a_selected_project_list != null && a_selected_project_list
			        .indexOf(s_project_number) != -1)) {
			s_selected = ' selected="selected" ';
		if (s_selected_project_name != '') {
					s_selected_project_name += ', ';
		}
		s_selected_project_name += s_project_number + ' '
				        + s_project_name;
		}
		s_project_options += '<option value="' + s_project_number + '" '
			        + s_selected + '>' + s_project_number + ' '
			        + s_project_name + '</option>';
	
	}
	if (a_selected_project_list == null
		        || a_selected_project_list.length == 0) {
		if (a_project_list.length > 0) {
			a_selected_project_list = a_project_list;

				
		} else {
				a_selected_project_list = new Array();
		}
	}
	
		if (a_selected_project_list != null
		        && a_selected_project_list.length != 0) {
			var s_formula = '';

			for (var i = 0; i < a_selected_project_list.length; i++) {
				if (i != 0) {
					s_formula += " OR";
				}

				s_formula += " SUBSTR({custcolprj_name}, 0,9) = '"
				        + a_selected_project_list[i] + "' ";
			}

			var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("CASE WHEN " + s_formula
			        + " THEN 1 ELSE 0 END");

			filters = filters.concat([ projectFilter ]);
		} else {
			var projectFilter = new nlobjSearchFilter('formulanumeric', null,
			        'equalto', 1);

			projectFilter.setFormula("0");

			filters = filters.concat([ projectFilter ]);
		}
		
			var current_date = new Date();
			
	//	var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
	//	var d_firstDay_mnth = new Date(new Date().getFullYear(), 0, 1,0,0,0);
	//	nlapiLogExecution('DEBUG','s_from',d_firstDay_mnth);
	//	s_from = nlapiDateToString(d_firstDay_mnth);
		
		var columns = search.getColumns();

		columns[0].setSort(false);
		columns[3].setSort(true);
		columns[9].setSort(false);
		filters.push(new nlobjSearchFilter('type',null,'noneof','SalesOrd'));
		filters.push(new nlobjSearchFilter('transactionnumbernumber',null,'isnotempty'));
        filters.push(new nlobjSearchFilter('type',null,'noneof','PurchOrd'));
	//  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));
	  	
	   filters.push(new nlobjSearchFilter('status',null,'noneof',
			['ExpRept:A','ExpRept:B','ExpRept:C','ExpRept:D','ExpRept:E','ExpRept:H','Journal:A','VendBill:C',
			'VendBill:D','VendBill:E',,'CustInvc:E','CustInvc:D']));
	  
		var search_results = searchRecord('transaction', null, filters, [
		        columns[2], columns[1], columns[0], columns[3], columns[4],
		        columns[5], columns[8], columns[9] ,columns[10] ,columns[11],columns[12],columns[13]]);
			var o_json = new Object();
		
		// Get the facility cost

		var s_facility_cost_filter = '';

		for (var i = 0; i < a_selected_project_list.length; i++) {
			s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";

			if (i != a_selected_project_list.length - 1) {
				s_facility_cost_filter += ",";
			}
		}

		var facility_cost_filters = new Array();
		facility_cost_filters[0] = new nlobjSearchFilter('formulanumeric',
		        null, 'equalto', 1);
		if (s_facility_cost_filter != '') {
			facility_cost_filters[0]
			        .setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN ('
			                + s_facility_cost_filter + ') THEN 1 ELSE 0 END');
		} else {
			facility_cost_filters[0].setFormula('0');
		}

		var facility_cost_columns = new Array();
		facility_cost_columns[0] = new nlobjSearchColumn(
		        'custrecord_arpm_period');
		facility_cost_columns[1] = new nlobjSearchColumn(
		        'custrecord_arpm_num_allocated_resources');
		facility_cost_columns[2] = new nlobjSearchColumn(
		        'custrecord_arpm_facility_cost_per_person');
		facility_cost_columns[3] = new nlobjSearchColumn(
		        'custrecord_arpm_location');

		var facility_cost_search_results = searchRecord(
		        'customrecord_allocated_resources_per_mon', null,
		        facility_cost_filters, facility_cost_columns);
		var o_data = {
		    'Revenue' : [],
		    'Discount' : [],
		    'People Cost' : [],
		    'Facility Cost' : [],
			'Other Cost - Travel':[],
			'Other Cost - Immigration': [],
			'Other Cost - Professional Fees' : [],
			'Other Cost - Others' : []
			
		};
		var new_object = null;

		var s_period = '';

		var a_period_list = [];

		var a_category_list = [];
		a_category_list[0]='';
		var a_group = [];

		var a_income_group = [];
		var j_other_list = {};
		var other_list =[];
				var col = new Array();
		col[0] = new nlobjSearchColumn('custrecord_account_name');
		col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
      var oth_fil=new Array();
      oth_fil[0]=new nlobjSearchFilter('isinactive',null,'is','F');
		var other_Search = nlapiSearchRecord('customrecord_pl_other_costs',null,oth_fil,col);
		if(other_Search){
			for(var i_ind=0;i_ind < other_Search.length;i_ind++){
				var acctname=other_Search[i_ind].getValue('custrecord_account_name');
				
				a_category_list.push(acctname);
				
				
				j_other_list = {
						o_acct : other_Search[i_ind].getValue('custrecord_account_name'),
						o_type : other_Search[i_ind].getText('custrecord_type_of_cost')					
				};
				
				other_list.push(j_other_list);
				
				
			}
		}
		for (var i = 0; search_results != null && i < search_results.length; i++) {
			var period = search_results[i].getText(columns[0]);

			//Code updated by Deepak, Dated - 21 Mar 17
			var s_month_year = period.split(' ');
			var s_mont = s_month_year[0];
			s_mont = getMonthCompleteName(s_mont);
			var s_year_ = s_month_year[1];
			var f_revRate = 66.0;
			var f_costRate = 66.0;	
			 var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate,f_eur_conv_rate,f_aud_conv_rate;
            //	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
            //	nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
            //nlapiLogExecution('audit','accnt name:- '+rate);

             //Fetch matching cost and rev rate convertion rate
			for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
				if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
					f_revRate = dataRows[data_indx].rev_rate;
					f_costRate = dataRows[data_indx].cost_rate;
					f_gbp_rev_rate = dataRows[data_indx].gbp_rev_rate;
					f_crc_cost_rate = dataRows[data_indx].crc_cost;
					f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
					f_eur_conv_rate = dataRows[data_indx].eur_conv_rate;
					f_aud_conv_rate = dataRows[data_indx].aud_conv_rate;
				}
			}
		/*	if(f_gbp_rev_rate==0)
			{
			f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
			}*/
			
			var transaction_type = search_results[i].getText(columns[8]);

			var transaction_date = search_results[i].getValue(columns[9]);
			
			var i_subsidiary = search_results[i].getValue(columns[10]);

			var amount = parseFloat(search_results[i].getValue(columns[1]));

			var category = search_results[i].getValue(columns[3]);
			
			var s_account_name = search_results[i].getValue(columns[11]);
			
			var currency=search_results[i].getValue(columns[12]);
			
			var exhangeRate=search_results[i].getValue(columns[13]);
		//	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);
			
			nlapiLogExecution('audit','accnt name:- '+s_account_name);
			
			var flag = false;
			if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				flag = true;
				
			}
			var eur_to_inr = 77.25;
			
			//AMount Convertion Logic
			if((parseInt(i_subsidiary) != parseInt(2))&&(parseInt(i_subsidiary) != parseInt(7)) &&(currency!= parseInt(1))){
			if (category != 'Revenue' && category != 'Other Income'
				&& category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
					if(currency== parseInt(9))
					{
						amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
					}
					else if(currency == parseInt(8)){
					amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
					}
					else if(currency == parseInt(10))
					{
                      	amount= parseFloat(amount)* parseFloat(exhangeRate);
						amount = parseFloat(amount)/parseFloat(f_aud_conv_rate);
					}
					else{
			
				amount= parseFloat(amount)* parseFloat(exhangeRate);		
				amount = parseFloat(amount) /parseFloat(f_costRate);	
				}
				
			}
              else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_revRate);
				
			//f_total_revenue += o_data[s_category][j].amount;
			}
            }
			else if((currency != parseInt(2)) && (parseInt(i_subsidiary) == parseInt(7)))
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_gbp_rev_rate);
			}
          else if((currency == parseInt(2)) && (parseInt(i_subsidiary) == parseInt(7)))
			{
				amount= parseFloat(amount)* parseFloat(exhangeRate);
				amount = parseFloat(amount) /parseFloat(f_gbp_rev_rate);
			}
			

			
			var isWithinDateRange = true;

			var d_transaction_date = nlapiStringToDate(transaction_date,
			        'datetimetz');

			if (s_from != '') {
				var d_from = nlapiStringToDate(s_from, 'datetimetz');

				if (d_transaction_date < d_from) {
					isWithinDateRange = false;
				}
			}

			/*if (s_to != '') {
				var d_to = nlapiStringToDate(s_to, 'datetimetz');

				if (d_transaction_date > d_to) {
					isWithinDateRange = false;
				}
			}*/

			if (isWithinDateRange == true) {
				var i_index = a_period_list.indexOf(period);

				if (i_index == -1) {
					if(_logValidation(period)){
					a_period_list.push(period);
					}
					else{
					//nlapiLogExecution('debug', 'error', '388');
					}
			
				}

				i_index = a_period_list.indexOf(period);
			}
			if(category != 'People Cost' && category != 'Discount' && category != 'Facility Cost'&& category != 'Revenue')
			{
			var o_index=a_category_list.indexOf(s_account_name);
			if(o_index!=-1)
			{
			o_index=o_index-1;
			var acct=other_list[o_index].o_acct;
			var typeofact=other_list[o_index].o_type;
			
			
			if((_logValidation(period))&&(_logValidation(amount))&& (_logValidation(search_results[i].getValue(columns[4])))
			   && (_logValidation(search_results[i].getValue(columns[5]))) &&  (_logValidation(transaction_type))
			   &&(_logValidation(transaction_date))&&(_logValidation(isWithinDateRange))&& (_logValidation(s_account_name)))
			   {
			   }
			   else{
			 //  nlapiLogExecution('debug','error','408');
			   }
			if(typeofact == 'Other Direct Cost - Travel'){
			o_data['Other Cost - Travel'].push({
			    'period' : period,
			    'amount' : amount,
			   'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			else if(typeofact == 'Other Direct Cost - Immigration'){
			o_data['Other Cost - Immigration'].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			if(typeofact == 'Other Direct Cost - Professional Fees'){
			o_data['Other Cost - Professional Fees'].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			if(typeofact == 'Other Direct Cost - Others'){
			o_data['Other Cost - Others'].push({
			    'period' : period,
			    'amount' : amount,
			   'num' : search_results[i].getValue(columns[4]),
			   'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			
			}
			
			
		}
		else
			{
			
			//avoiding discounts from account						  
			   if(category == 'Revenue' && (i_subsidiary == parseInt(2) || i_subsidiary == parseInt(13) || i_subsidiary == parseInt(14)))
			   {
					if(s_account_name != parseInt(732))
					{
					o_data[category].push({
					'period' : period,
					'amount' : amount,
					'num' : search_results[i].getValue(columns[4]),
					'memo' : search_results[i].getValue(columns[5]),
					'type' : transaction_type,
					'dt' : transaction_date,
					'include' : isWithinDateRange,
					's_account_name' : s_account_name
					});
                      nlapiLogExecution('audit','subs:- '+i_subsidiary);
					//nlapiLogExecution('audit','account Revenue:- '+s_account_name);
                    //  nlapiLogExecution('audit','trnsactnnum:'+search_results[i].getValue(columns[4]));
                      
					}
					//nlapiLogExecution('audit','subs:- '+i_subsidiary);
					//nlapiLogExecution('audit','account Revenue:- '+s_account_name);
			   }
			   else{
			o_data[category].push({
			    'period' : period,
			    'amount' : amount,
			    'num' : search_results[i].getValue(columns[4]),
			    'memo' : search_results[i].getValue(columns[5]),
			    'type' : transaction_type,
			    'dt' : transaction_date,
			    'include' : isWithinDateRange,
				's_account_name' : s_account_name
			});
			}
			
				//nlapiLogExecution('audit','cat:- '+category);
			}
		}
		
		// Add facility cost
		//o_data['Facility Cost'] = new Array();

		for (var i = 0; facility_cost_search_results != null
		        && i < facility_cost_search_results.length; i++) {
			var s_period = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[0]);
			var f_allocated_resources = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[1]);
			var f_cost_per_resource = facility_cost_search_results[i]
			        .getValue(facility_cost_columns[2]);
			var s_location = facility_cost_search_results[i]
			        .getText(facility_cost_columns[3]);

			if (f_cost_per_resource == '') {
				f_cost_per_resource = 0;
			}
			if(_logValidation(f_allocated_resources))
			{
			}
			else{
			f_allocated_resources=0;
			}
						   
			   
			//if(s_exclude == 'true'){
				o_data['Facility Cost'].push({
					'period' : s_period,
					'amount' : 0,
					'num' : '',
					'memo' : 'Facility cost for ' + f_allocated_resources
					+ ' resources at ' + s_location + '.',
					'type' : 'Facility Cost',
					'dt' : '',
					'include' : a_period_list.indexOf(s_period) != -1,
					's_account_name' : ''
				});
		
		}
		var o_total = new Array();
		var o_period_total = new Array();

		var f_total_revenue = 0.0;
		var f_total_expense = 0.0;
		var f_profit_percentage = 0.0;
		var a_chart = new Array();
		
		//
		// a_chart.push(['x'].concat(a_period_list));

		for ( var s_category in o_data) {
			var a_temp = [];

			o_total[s_category] = 0.0;
			o_period_total[s_category] = 0.0;

			for (var i = 0; i < a_period_list.length; i++) {
				a_temp.push(0.0);
			}

			for (var j = 0; j < o_data[s_category].length; j++) {
				var i_index = a_period_list
				        .indexOf(o_data[s_category][j].period);

				a_temp[i_index] += o_data[s_category][j].amount;

				if (o_data[s_category][j].include == true) {
					o_period_total[s_category] += o_data[s_category][j].amount;
				}

				o_total[s_category] += o_data[s_category][j].amount;

				if (s_category != 'Revenue' && s_category != 'Other Income'
				        && s_category != 'Discount') {
					f_total_expense += o_data[s_category][j].amount;
				} else {
					f_total_revenue += o_data[s_category][j].amount;
				}
			}

			if (s_category != 'Revenue' && s_category != 'Other Income'
			        && s_category != 'Discount') {
					if(_logValidation(s_category)){
				a_group.push(s_category);
				}
				else{
				//nlapiLogExecution('debug', 'error', '580');
				}
				
			} else {
			if(_logValidation(s_category))
			{
				a_income_group.push(s_category);
				}
				else{
				//nlapiLogExecution('debug', 'error', '589');
				}
			}
	if(_logValidation(s_category))
			a_chart.push([ s_category ].concat(a_temp));
			else{
			
		//	nlapiLogExecution('debug', 'error', '596');
			}
		}

		var a_profit = [ 'Net Margin(%)', 0.0 ];

		var a_expense = [ '', 0.0 ];

		var a_revenue = [ '', 0.0 ];
		var a_peoplecost = ['',0.0];
		var a_facilitycost = ['',0.0];
		// Calculate Profit
		for (var i = 0; i < a_chart.length; i++) {

			if (i == 0) {
				for (j = 1; j < a_chart[i].length; j++) {
					a_profit.push(0.0);

					a_expense.push(0.0);
					
					a_peoplecost.push(0.0);
					
					a_facilitycost.push(0.0);
					
					a_revenue.push(0.0);
				}
			}

			for (var j = 1; j < a_chart[i].length; j++) {

				if (a_chart[i][0] == 'Revenue'
				        || a_chart[i][0] == 'Other Income'
				        || a_chart[i][0] == 'Discount') {
					a_profit[j] = a_profit[j] + a_chart[i][j];

					a_revenue[j] = a_revenue[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] += a_chart[i][j];
					a_revenue[a_revenue.length - 1] += a_chart[i][j];
				} 
				else if(a_chart[i][0] == 'People Cost'){
					a_profit[j] = a_profit[j] - a_chart[i][j];

					a_peoplecost[j] = a_peoplecost[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] -= a_chart[i][j];
					a_peoplecost[a_peoplecost.length - 1] += a_chart[i][j];
				}
				else if(a_chart[i][0] == 'Facility Cost'){
					a_profit[j] = a_profit[j] - a_chart[i][j];

					a_facilitycost[j] = a_facilitycost[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] -= a_chart[i][j];
					a_facilitycost[a_facilitycost.length - 1] += a_chart[i][j];
				}else {
					a_profit[j] = a_profit[j] - Math.abs(a_chart[i][j]);

					a_expense[j] = a_expense[j] + a_chart[i][j];

					a_profit[a_profit.length - 1] -= a_chart[i][j];
					a_expense[a_expense.length - 1] += a_chart[i][j];
				}
			}
		}
		for (var i = 1; i < a_profit.length; i++) {
			if (a_revenue[i] != 0.0) {
				a_profit[i] = (a_profit[i] / a_revenue[i] * 100).toFixed(2)
				        + '%';
			} else {
				a_profit[i] = ' - ';
			}
		}
		//var expe=accounting.formatNumber(f_total_expense,2);		
		var expe=f_total_expense;
		nlapiLogExecution('DEBUG','EXpense',expe);
		//var rev=accounting.formatNumber(f_total_revenue,2);		
		var rev=f_total_revenue;
		nlapiLogExecution('DEBUG','EXpense',rev);
		if(rev!=0){
		var margin = (((rev - expe) / rev) * 100).toFixed(2)  + '%';
		}
		else{
			var margin= -expe;
		}
		nlapiLogExecution('DEBUG','Margin',margin);
		var rev_lenth = a_revenue.length;
		var exp_lenth = a_expense.length;
		//	bil_unique_list.push(a_project_list);
		//	for(var len=0;len< project_search_results.length;len++)
		//	{
             bill_data_arr[sr_no] = {
                        'project_id': s_project_number,
                        'project_name': project_name,
                        'sold_margin': sold_margin,
                        'billing_type': billing_type,
						'people_cost': a_peoplecost[a_peoplecost.length-1],    //expe,
						'facilitycost': a_facilitycost[a_facilitycost.length-1],    //expe,
						//'rev':  a_revenue[rev_lenth-2], //rev,
						'ytd_rev' : a_revenue[rev_lenth-1],
						'ytd_exp':a_expense[exp_lenth-1] ,
                        'actual_margin': a_profit[a_profit.length - 1],
                                                        
                   };
						sr_no++;
				return bill_data_arr;		
}
function getSelectedProjects(requestData)
{
	
	
	if(_logValidation(requestData))
	{
		nlapiLogExecution('debug', 'projectlist',requestData);
		var project_filter = [
		[	             
		 'internalid','anyOf', requestData]
		];

		var project_search_results = searchRecord('job', null, project_filter,
		[ 	new nlobjSearchColumn("entityid"),
		    new nlobjSearchColumn("altname"),
			new nlobjSearchColumn("companyname"),
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("startdate"),
			new nlobjSearchColumn("custentity_projectvalue"),
			new nlobjSearchColumn("custentity_project_currency"),
			new nlobjSearchColumn("custentity_sold_margin_percent"),
			new nlobjSearchColumn("jobbillingtype")
			
		]);

		nlapiLogExecution('debug', 'project count',project_search_results.length);
				
		return project_search_results;
	}
}

function createPeriodList(startDate, endDate) {
	var d_startDate = nlapiStringToDate(startDate);
	var d_endDate = nlapiStringToDate(endDate);

	var arrPeriod = [];

	for (var i = 0;; i++) {
		var currentDate = nlapiAddMonths(d_startDate, i);
		if((_logValidation(getMonthName(currentDate)))&& (_logValidation(getMonthStartDate(currentDate)))&&(_logValidation(getMonthEndDate(currentDate))))
		{
		}
		else{
		nlapiLogExecution('debug', 'error', '909');
		}
		arrPeriod.push({
		    Name : getMonthName(currentDate),
		    StartDate : getMonthStartDate(currentDate),
		    EndDate : getMonthEndDate(currentDate)
		});

		if (getMonthEndDate(currentDate) >= d_endDate) {
			break;
		}
	}

	var today = new Date();

	// remove the current month
	if (d_endDate.getMonth() >= today.getMonth()) {
		arrPeriod.pop();
	}

	return arrPeriod;
}

function getMonthEndDate(currentDate) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getMonthName(currentDate) {
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}

function getExchangerates(){
//var mnt_lat_date=getLastDate(s_mont,s_year_);
var amount=3910;
var rate=nlapiExchangeRate('USD', 'INR');
var con_amou=parseFloat(amount)*parseFloat(rate);
nlapiLogExecution('Debug','ExchangeValue'+rate,'amount'+con_amou);
var rate2=nlapiExchangeRate('INR', 'USD');
con_amou=parseFloat(con_amou)*parseFloat(rate2);
nlapiLogExecution('Debug','ExchangeValue'+rate2,'amount'+con_amou);
var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
		column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
		column[2] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
		column[3]=new nlobjSearchColumn('internalid');	
		//var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,null,column);
	//	if(currencySearch){
		//	for(var i_indx=0;i_indx < currencySearch.length;i_indx++){
		//	var id=currencySearch[i_indx].getValue('internalid');	
			//var rec=nlapiLoadRecord('customrecord_pl_currency_exchange_rates',id);	
			//	rec.setFieldValue('custrecord_pl_gbp_revenue',rate);
			//	nlapiSubmitRecord(rec);
			nlapiLogExecution('Debug','ExchangeValue',rate);
		//}
	//}					
	//return rate;
}

function getLastDate(month,year)
{
	var date='';
	if(month == 'January')
		date = "1/31/"+year;
	if(month == 'February')
		date = "2/28/"+year;
	if(month == 'March')
		date = "3/31/"+year;
	if(month == 'April')
		date = "4/30/"+year;
	if(month == 'May')
		date = "5/31/"+year;
	if(month == 'June')
		date = "6/30/"+year;
	if(month == 'July')
		date = "7/31/"+year;
	if(month == 'August')
		date = "8/31/"+year;
	if(month == 'September')
		date = "9/30/"+year;
	if(month == 'October')
		date = "10/31/"+year;
	if(month == 'November')
		date = "11/30/"+year;
	if(month == 'December')
		date = "12/31/"+year;
	
	return date;
}

//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != '- None -' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function getResourceCount(proj_list)
{
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
	[
		["project","anyof","138764","88180","142968","65557","121898","126336"]], 
		[new nlobjSearchColumn("id").setSort(false), 
		new nlobjSearchColumn("resource"), 
		new nlobjSearchColumn("company"), 
		new nlobjSearchColumn("startdate"), 
		new nlobjSearchColumn("percentoftime"), 
		new nlobjSearchColumn("enddate"), 
		new nlobjSearchColumn("employeestatus","employee",null), 
		new nlobjSearchColumn("subsidiary","employee",null)]);
		if(_logValidation(resourceallocationSearch))
		{
			return resourceallocationSearch;
		}
		else
		{
			
			throw "No Active Allocations";
		}
}