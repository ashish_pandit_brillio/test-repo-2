/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Sep 2019     Aazamali Khan    Updated Allocation for FRFs on the basis of selected candidate
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	try {
		if (type == "edit" || type == "xedit") {
			var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var frfId = recObj.getFieldValue("custrecord_taleo_ext_hire_frf_details");
			var selectedEmployeeEmail = recObj.getFieldValue("custrecord_offered_emp_email");
            nlapiLogExecution("DEBUG","Email : ",selectedEmployeeEmail);
			// write a search 
			if(isNotEmpty(selectedEmployeeEmail)) {
				var searchResult = getEmployeeRec(selectedEmployeeEmail);
				nlapiLogExecution("DEBUG","searchResult : ",JSON.stringify(searchResult));
                 if(searchResult) {
					var empId = searchResult[0].getId();
                    nlapiLogExecution("DEBUG","empId : ",empId);
					// Get the resource allocation 
					var allocationSearch = getAllocationSearchForExternal(empId);
					if(allocationSearch) {
						var resourceId = allocationSearch[0].getValue("resource");
						var startDate = allocationSearch[0].getValue("startdate");
						var frfrecObj = nlapiLoadRecord("customrecord_frf_details", frfId);
						frfrecObj.setFieldValue("custrecord_frf_details_allocated_emp", resourceId);
						frfrecObj.setFieldValue("custrecord_frf_details_allocated_date", startDate);
						frfrecObj.setFieldValue("custrecord_frf_details_open_close_status", "2");
						frfrecObj.setFieldValue("custrecord_frf_details_status_flag", "2");
//-------------------------------------------------------------------------------------------------------------------------------------						
						// capturing close date  prabhat gupta NIS-1307
                      var currentDate = sysDate();
						currentDate = nlapiStringToDate(currentDate);
						frfrecObj.setFieldValue("custrecord_frf_details_closed_date", currentDate);
						
//--------------------------------------------------------------------------------------------------------------------------
						nlapiSubmitRecord(frfrecObj);
					}
				}
			}
		}
} catch (e) {
		// TODO: handle exception
		nlapiLogExecution("DEBUG", "error in script : ", e);
}
}
function getAllocationSearchForExternal(empId) {
		var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
			[
				["startdate", "notafter", "today"], "AND",
				["enddate", "notbefore", "today"], "AND",
				["resource", "anyof", empId]
			],
			[
				new nlobjSearchColumn("resource"),
				new nlobjSearchColumn("startdate"),
				new nlobjSearchColumn("company")
			]);
		return resourceallocationSearch;
}
function getEmployeeRec(personalEmail) {
		var employeeSearch = nlapiSearchRecord("employee", null,
			[
				["email", "is", personalEmail],"OR",["custentity_personalemailid", "is", personalEmail]
			],
			[]);
		return employeeSearch;
}

//--------------------------------------------------------------------------------------------
// capturing close date  prabhat gupta NIS-1307
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
}