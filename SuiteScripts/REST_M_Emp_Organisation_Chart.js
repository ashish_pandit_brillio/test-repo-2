/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Feb 2016     Nitish Mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('debug', 'employeeId', employeeId);
		var personId = dataIn.Data.Person;
		nlapiLogExecution('debug', 'personId', personId);
		var requestType = dataIn.RequestType;

		var allowCall = false;

		if (personId && (personId != employeeId)) {
			allowCall = true;
		}

		if (!personId) {
			personId = employeeId;
		}

		switch (requestType) {

			case M_Constants.Request.GetAll:
				response.Data = getEmployeeHierarchy(personId, allowCall);
				response.Status = true;
			break;

			case M_Constants.Request.Get:
				response.Data = getMyReportee(personId);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getEmployeeHierarchy(employeeId, allowCall) {
	try {
		if (employeeId) {
			nlapiLogExecution('debug', 'employeeId', employeeId);

			// get the reporting manager of the employee
			var myDetails = nlapiLookupField('employee', employeeId, [
			        'entityid', 'title', 'custentity_reportingmanager',
			        'mobilephone', 'phone', 'gender' ]);

			if (myDetails) {
				// get details of the reporting manager
				var reportingManagerDetails = nlapiLookupField('employee',
				        myDetails.custentity_reportingmanager, [ 'entityid',
				                'title', 'gender' ]);

				// get a list of all the co-workers
				var coworkersList = getReporteeDetails(myDetails.custentity_reportingmanager);

				// get a list
				var reporteeList = getReporteeDetails(employeeId);

				return {
				    ReportingManager : {
				        Name : reportingManagerDetails.entityid,
				        Designation : reportingManagerDetails.title,
				        RecordId : myDetails.custentity_reportingmanager,
				        Gender : reportingManagerDetails.gender
				    },
				    Me : {
				        Name : myDetails.entityid,
				        Designation : myDetails.title,
				        RecordId : employeeId,
				        AllowCall : allowCall,
				        PhoneNumber : myDetails['mobilephone'] ? myDetails['mobilephone']
				                : myDetails['phone'],
				        Gender : myDetails.gender,
				    },
				    Coworkers : coworkersList,
				    Reportees : reporteeList
				};
			} else {
				throw "Reporting Manager Empty";
			}
		} else {
			throw "Employee Id Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getEmployeeHierarchy', err);
		throw err;
	}
}

function getMyReportee(employeeId) {
	try {
		if (employeeId) {
			return getReporteeDetails(employeeId);
		} else {
			throw "Employee Id Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getMyReportee', err);
		throw err;
	}
}

function getReporteeDetails(employeeId) {
	try {
		if (employeeId) {
			var employeeSearch = nlapiSearchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_implementationteam',
			                null, 'is', 'F'),
			        new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is', 'F'),
			        new nlobjSearchFilter('custentity_reportingmanager', null,
			                'anyof', employeeId) ], [
			        new nlobjSearchColumn('entityid'),
			        new nlobjSearchColumn('title'),
			        new nlobjSearchColumn('gender') ]);

			var employeeList = [];

			if (employeeSearch) {
				employeeSearch.forEach(function(employee) {
					employeeList.push({
					    RecordId : employee.getId(),
					    Name : employee.getValue('entityid'),
					    Designation : employee.getValue('title'),
					    Gender : employee.getValue('gender')
					});
				});
			}

			return employeeList;
		} else {
			throw "Employee Id Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getReporteeDetails', err);
		throw err;
	}
}
