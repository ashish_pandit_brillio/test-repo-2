/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 30 Jun 2015 nitish.mishra
 * 
 */

function main() {
	var rec = nlapiSearchRecord(null, 954);

	for (var i = 0; i < rec.length; i++) {
		try {
			moveTimesheet(rec[i].getId());
			// break;
		} catch (err) {
			nlapiLogExecution('error', rec[i].getId(), err);
		}

	}
}

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function moveTimesheet(recId) {

	var ab = [ recId ];

	for (var j = 0; j < ab.length; j++) {
		nlapiLogExecution('debug', 'ID: ', ab[j]);
		var a_days = [ 'timebill0', 'timebill1', 'timebill2', 'timebil3', 'timebill4',
				'timebill5', 'timebill6' ];

		var a;
		try {
			a = nlapiLoadRecord('timesheet', ab[j]);
		} catch (e) {
			nlapiLogExecution('ERROR', 'e', e.message);
			if (e.message == 'That record does not exist.') {
				continue;
			}
			break;
		}

		for (var i = 1; i <= a.getLineItemCount('timeitem'); i++) {
			a.selectLineItem('timeitem', i);
			var flagcheck=false;
			for (var i_day_indx = 0; i_day_indx < 7; i_day_indx++) {
					var o_sub_record_view = a.getCurrentLineItemValue('timeitem', 'timebill'+i_day_indx);
					
					if (o_sub_record_view) {
						flagcheck=true;
				}
			}
			if(flagcheck==true){
			a.setCurrentLineItemValue('timeitem', 'customer',11181);
			a.setCurrentLineItemValue('timeitem', 'casetaskevent',17737);
			}
			a.commitLineItem('timeitem');
		}
		nlapiSubmitRecord(a);
	}
}