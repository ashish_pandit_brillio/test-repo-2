/**
 * Whenever a TimeSheet is submitted move it to the TimeSheet quick approval
 * pool
 * 
 * Version Date Author Remarks 1.00 31 Aug 2015 Nitish Mishra
 * 								2.0 5-Aug-2020 Shravan Kulkarni Changes on the Send Email added new function and removed 
								3.0 shravan modified the send mail on 6-aug-2020
 */

// if a TS is submitted, create a new custom record in the approval pool
function createQuickPoolRecord(type) {

	try {
		nlapiLogExecution('debug', 'type', type);

		// if a TS is submitted, create a new custom record in the approval pool
		if (type == 'markcomplete' || type == 'edit') {

			// check if the quick approval pool record exists for this timesheet
			var quickPoolSearch = nlapiSearchRecord(
			        'customrecord_ts_quick_approval_pool', null,
			        new nlobjSearchFilter('custrecord_qap_timesheet', null,
			                'anyof', nlapiGetRecordId()));

			var quickApprovalRecord = null;

			if (quickPoolSearch) {
				quickApprovalRecord = nlapiLoadRecord(
				        'customrecord_ts_quick_approval_pool',
				        quickPoolSearch[0].getId());
			} else {
				quickApprovalRecord = nlapiCreateRecord('customrecord_ts_quick_approval_pool');
			}

			quickApprovalRecord.setFieldValue('custrecord_qap_timesheet',
			        nlapiGetRecordId());
			quickApprovalRecord.setFieldValue('custrecord_qap_employee',
			        nlapiGetFieldValue('employee'));
			quickApprovalRecord.setFieldValue('custrecord_qap_week_start_date',
			        nlapiGetFieldValue('startdate'));
			quickApprovalRecord.setFieldValue('custrecord_qap_week_end_date',
			        nlapiGetFieldValue('enddate'));

			quickApprovalRecord.setFieldValue('custrecord_qap_st_hours', '');
			quickApprovalRecord.setFieldValue('custrecord_qap_ot_hours', '');
			quickApprovalRecord.setFieldValue('custrecord_qap_leave_hours', '');
			quickApprovalRecord.setFieldValue('custrecord_qap_holiday_hours',
			        '');

			quickApprovalRecord.setFieldValue('custrecord_qap_total_hours',
			        nlapiGetFieldValue('totalhours'));

			quickApprovalRecord.setFieldValue('custrecord_qap_approved', 'F');
			quickApprovalRecord.setFieldValue('custrecord_qap_rejected', 'F');

			var timesheetStatus = nlapiGetFieldValue('approvalstatus');

			if (timesheetStatus == 3 || timesheetStatus == 4) {
				quickApprovalRecord.setFieldValue('custrecord_qap_ts_updated',
				        'T');
			} else {
				quickApprovalRecord.setFieldValue('custrecord_qap_ts_updated',
				        'F');
			}

			var recordId = nlapiSubmitRecord(quickApprovalRecord);

			nlapiInitiateWorkflowAsync('customrecord_ts_quick_approval_pool',
			        recordId, 'customworkflow_quick_approval_pool');

			nlapiLogExecution('debug',
			        'Quick Approval Pool TS Created / Updated', recordId);
		} 
		else if (type == 'approve' || type == 'reject') {
			var quickPoolSearch = nlapiSearchRecord(
			        'customrecord_ts_quick_approval_pool', null,
			        new nlobjSearchFilter('custrecord_qap_timesheet', null,
			                'anyof', nlapiGetRecordId()));

			if (quickPoolSearch) {
				quickApprovalRecord = nlapiSubmitField(
				        'customrecord_ts_quick_approval_pool',
				        quickPoolSearch[0].getId(),
				        'custrecord_qap_ts_updated', 'T');

				nlapiLogExecution('debug', 'Quick Approval Pool TS Updated',
				        quickPoolSearch[0].getId());
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR',
		        'TS Quick Approval Create : createQuickPoolRecord', err);
	}
}


function BeforeSubmit_sendEmial_Notification(type)
{
	try
	{
		nlapiLogExecution('DEBUG','Type==',type);
		if(type == 'delete')
		{
			var i_Id = nlapiGetRecordId();
			nlapiLogExecution('DEBUG','i_Id==',i_Id);
			var s_Recipient = 'onthego@brillio.com';
			var s_Subject = 'Time Sheet Deleted :'+ '' + i_Id;
			var obj_Timesheet = nlapiLoadRecord('TimeSheet',i_Id);
			var s_Status = obj_Timesheet.getFieldText('approvalstatus');
			var s_Status_Value = obj_Timesheet.getFieldValue('approvalstatus');
			nlapiLogExecution('DEBUG','s_Status==',s_Status);
			nlapiLogExecution('DEBUG','s_Status_Value==',s_Status_Value);
			
			/*var s_Employee = nlapiGetFieldValue('employee');
			nlapiLogExecution('DEBUG','s_Employee==',s_Employee);
			var i_User = nlapiGetUser();
			var s_User = nlapiLookupField('Employee',i_User,'firstname');
			nlapiLogExecution('DEBUG','s_User==',s_User);*/

			/*var s_Body = "<p>Hi, </p>";
			s_Body += "<p> This is to inform you that a timesheet has been deleted. </p>";
			s_Body += "<b>Employee : </b>"
			        + nlapiGetFieldText('employee') + "<br/>";
			s_Body += "<b>Week : </b>" + nlapiGetFieldValue('startdate')
			        + " - " + nlapiGetFieldValue('enddate') + "<br/>";
			s_Body += "<b>Status : </b>"
			        + nlapiGetFieldText('approvalstatus') + "<br/>";
			s_Body += "<b>Deleted By : </b>"
			        + nlapiGetContext().getName() + "<br/>";
			s_Body += "<b>Deleted On : </b>"
			        + nlapiDateToString(new Date(), 'datetimetz') + "<br/>";
			s_Body += "<b>Reason for Deletion : </b>"  + "<br/>";
			s_Body += "<p>Regards, <br/> Information Systems</p>";*/
			
				var s_Body = "<p>Hi, </p>";
			s_Body += "<p> This is to inform you that a timesheet has been deleted. </p>";
			s_Body += "<b>Employee : </b>"
			        + obj_Timesheet.getFieldText('employee') + "<br/>";
			s_Body += "<b>Week : </b>" + obj_Timesheet.getFieldValue('startdate')
			        + " - " + obj_Timesheet.getFieldValue('enddate') + "<br/>";
			s_Body += "<b>Status : </b>"
			        + obj_Timesheet.getFieldText('approvalstatus') + "<br/>";
			s_Body += "<b>Deleted By : </b>"
			        + nlapiGetContext().getName() + "<br/>";
			s_Body += "<b>Deleted On : </b>"
			        + nlapiDateToString(new Date(), 'datetimetz') + "<br/>";
			s_Body += "<b>Reason for Deletion : </b>"  + "<br/>";
			s_Body += "<p>Regards, <br/> Information Systems</p>";
			
			 nlapiSendEmail( 442, s_Recipient, s_Subject ,
   s_Body , null, null, null );
			
				
		}//if(type == 'delete')
	}/// end of try
	catch(e)
	{
		nlapiLogExecution('ERROR','BeforeSubmit_sendEmial_Notification', e);
	}/// end of catch
	
}//function BeforeSubmit_sendEmial_Notification
/*
// delete the custom quick approval record when a TS is deleted
function deleteQuickPoolRecord(type) {

	try {
		nlapiLogExecution('debug', 'type', type);

		// delete the custom quick approval record when a TS is deleted
		if (type == 'delete') {

		//	var remarks = nlapiGetFieldValue('custrecord_ts_remark');
			
			
			
			var record_obj=nlapiGetNewRecord();
    
			var remarks=record_obj.getFieldValue('custrecord_ts_remark');
			nlapiLogExecution('debug', ' normal remarks==', remarks);
			
			var context = nlapiGetContext();
			if (context.getExecutionContext() != "userinterface" && context.getExecutionContext() != "userevent" ) 
			{
				remarks = context.getExecutionContext();
				nlapiLogExecution('debug', 'inside if remarks==', remarks);
			} ///if Remarks
			else if (isEmpty(remarks)) {
				nlapiLogExecution('debug', '(isEmpty(remarks)==',isEmpty(remarks) );
				throw "Please enter the reason for deletion in the remarks field";
			}

			//var context = nlapiGetContext();
			var i_Id = nlapiGetRecordId();
			nlapiLogExecution('DEBUG','i_Id==',i_Id);
			var obj_Timesheet = nlapiLoadRecord('TimeSheet',i_Id);
			//obj_Timesheet.getField
			
			var mailTemplate = "<p>Hi, </p>";
			mailTemplate += "<p> This is to inform you that a timesheet has been deleted. </p>";

			mailTemplate += "<b>Employee : </b>"
			        + obj_Timesheet.getFieldText('employee') + "<br/>";
			mailTemplate += "<b>Week : </b>" + obj_Timesheet.getFieldValue('startdate')
			        + " - " + obj_Timesheet.getFieldValue('enddate') + "<br/>";
			mailTemplate += "<b>Status : </b>"
			        + obj_Timesheet.getFieldText('approvalstatus') + "<br/>";
			mailTemplate += "<b>Deleted By : </b>"
			        + nlapiGetContext().getName() + "<br/>";
			mailTemplate += "<b>Deleted On : </b>"
			        + nlapiDateToString(new Date(), 'datetimetz') + "<br/>";
			mailTemplate += "<b>Reason for Deletion : </b>" + remarks + "<br/>";

			mailTemplate += "<p>Regards, <br/> Information Systems</p>";
			
			
			

			// check the subsidiary and generate the recipient list as per that
			var recipient = "";

			var subsidiary = nlapiGetFieldValue('subsidiary');
			if (subsidiary == '2') {
				recipient = 'onthego@brillio.com,shivam.pandya@brillio.com,lisa.joshy@brillio.com,information.systems@brillio.com,vertica@brillio.com,himanshu.negi@brillio.com,kaushal.thakar@brillio.com';
			
			} else {
				recipient = 'onthego@brillio.com,shivam.pandya@brillio.com,lisa.joshy@brillio.com,information.systems@brillio.com,himanshu.negi@brillio.com,kaushal.thakar@brillio.com';
			}
			nlapiLogExecution('debug', 'context.getExecutionContext()==', context.getExecutionContext());
			if (context.getExecutionContext() == "userinterface" || context.getExecutionContext() == "userevent") 
			{
				nlapiLogExecution('debug', 'context.getExecutionContext()==', context.getExecutionContext());
				//constant.Mail_Author.InformationSystems
				nlapiSendEmail(constant.Mail_Author.InformationSystems, recipient,
			        'A Timesheet has been deleted', mailTemplate, null,
			        "shravan.k@brillio.com");
			}
			var approvalPoolSearch = nlapiSearchRecord(
			        'customrecord_ts_quick_approval_pool', null,
			        [ new nlobjSearchFilter('custrecord_qap_timesheet', null,
			                'anyof', nlapiGetRecordId()) ]);

			if (approvalPoolSearch) {
				nlapiDeleteRecord('customrecord_ts_quick_approval_pool',
				        approvalPoolSearch[0].getId());
				nlapiLogExecution('debug', 'Quick Approval Pool TS Deleted',
				        approvalPoolSearch[0].getId());
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR',
		        'TS Quick Approval Delete : deleteQuickPoolRecord', err);
		throw err;
	}
}
*/



// delete the custom quick approval record when a TS is deleted
function deleteQuickPoolRecord(type) {

	try {
		nlapiLogExecution('debug', 'type', type);
      var remarks = nlapiGetFieldValue('custrecord_ts_remark');
			
nlapiLogExecution('debug', 'remarks', remarks);

		// delete the custom quick approval record when a TS is deleted
		if (type == 'delete') {

			var remarks = nlapiGetFieldValue('custrecord_ts_remark');
			//var remarks = nlapiGetFieldValue('custrecord_ts_delete_remarks');
nlapiLogExecution('debug', 'remarks', remarks);
			var context = nlapiGetContext();
			if (context.getExecutionContext() != "userinterface" && context.getExecutionContext() != "userevent") {
				remarks = context.getExecutionContext();
			} else if (isEmpty(remarks)) {
		//throw "Please enter the reason for deletion in the remarks field";
			}

			
			var mailTemplate = "<p>Hi, </p>";
			mailTemplate += "<p> This is to inform you that a timesheet has been deleted. </p>";

			mailTemplate += "<b>Employee : </b>"
			        + nlapiGetFieldText('employee') + "<br/>";
			mailTemplate += "<b>Week : </b>" + nlapiGetFieldValue('startdate')
			        + " - " + nlapiGetFieldValue('enddate') + "<br/>";
			mailTemplate += "<b>Status : </b>"
			        + nlapiGetFieldText('approvalstatus') + "<br/>";
			mailTemplate += "<b>Deleted By : </b>"
			        + nlapiGetContext().getName() + "<br/>";
			mailTemplate += "<b>Deleted On : </b>"
			        + nlapiDateToString(new Date(), 'datetimetz') + "<br/>";
			mailTemplate += "<b>Reason for Deletion : </b>" + remarks + "<br/>";

			mailTemplate += "<p>Regards, <br/> Information Systems</p>";

			// check the subsidiary and generate the recipient list as per that
			var recipient = "";

			var subsidiary = nlapiGetFieldValue('subsidiary');
			if (subsidiary == '2') {
				recipient = 'onthego@brillio.com,information.systems@brillio.com,vertica@brillio.com';
			} else {
				recipient = 'onthego@brillio.com,information.systems@brillio.com';
			}
			
			if (context.getExecutionContext() == "userinterface" || context.getExecutionContext() == "userevent") 
			{
				nlapiSendEmail(constant.Mail_Author.InformationSystems, recipient,
			        'A Timesheet has been deleted', mailTemplate, null,
			        "deepak.srinivas@brillio.com");
			}
			var approvalPoolSearch = nlapiSearchRecord(
			        'customrecord_ts_quick_approval_pool', null,
			        [ new nlobjSearchFilter('custrecord_qap_timesheet', null,
			                'anyof', nlapiGetRecordId()) ]);

			if (approvalPoolSearch) {
				nlapiDeleteRecord('customrecord_ts_quick_approval_pool',
				        approvalPoolSearch[0].getId());
				nlapiLogExecution('debug', 'Quick Approval Pool TS Deleted',
				        approvalPoolSearch[0].getId());
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR',
		        'TS Quick Approval Delete : deleteQuickPoolRecord', err);
		throw err;
	}
}