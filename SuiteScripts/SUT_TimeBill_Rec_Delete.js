// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_TimeBill_Rec_Delete.js
	Author:Swati Kurariya
	Company:Aashna CloudTech Pvt. Ltd.
	Date:16-02-2015
	Description:Call Schedulen script to delete monthly_provision_allocation custom record. 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_delete_time_rec(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-call schedule script to delete monthly_provision_allocation custom record.


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
	
	
	var d_From_Date = request.getParameter('d_from_date');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' d_From_Date -->' + d_From_Date);
	
	var d_To_Date = request.getParameter('d_to_date');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' d_To_Date -->' + d_To_Date);
	
	var i_Criteria = request.getParameter('i_criteria');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Criteria -->' + i_Criteria);
	
	var i_Subsidiary = request.getParameter('i_subsidiary');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Subsidiary -->' + i_Subsidiary);
	
	var i_Month = request.getParameter('i_month');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Month -->' + i_Month);
	
	var i_Year = request.getParameter('i_year');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Year -->' + i_Year);
	
	var i_Unbilled_Type = request.getParameter('i_unbilled_type');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Unbilled_Type -->' + i_Unbilled_Type);
	
	
	var i_Unbilled_Receivable_GL = request.getParameter('i_ubilled_receivable_GL');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Unbilled_Receivable_GL -->' + i_Unbilled_Receivable_GL);
	
	var i_Unbilled_Revenue_GL = request.getParameter('i_unbilled_revenue_GL');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Unbilled_Revenue_GL -->' + i_Unbilled_Revenue_GL);
	
	var i_Reverse_Date = request.getParameter('i_reverse_date');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Reverse_Date -->' + i_Reverse_Date);
	
	var i_Search_Count_f = request.getParameter('i_search_count_f');
	nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_Search_Count_f -->' + i_Search_Count_f);

	 var params1=new Array();
     params1['custscript_from_date_val'] = d_From_Date;
	 params1['custscript_to_date_val'] = d_To_Date;
	 params1['custscript_user_subsidiary_val'] = i_Subsidiary;
	 params1['custscript_criteria_val'] = i_Criteria;
	 params1['custscript_month_val'] = i_Month;
	 params1['custscript_year_val'] = i_Year;
			 
	 var status=nlapiScheduleScript('customscript_sch_timebill_rec_delete',null,params1);
	 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
	 
	 
	 var params=new Array();
     params['custpage_from_date'] = d_From_Date
	 params['custpage_to_date'] = d_To_Date
	 params['custpage_unbilled_type'] = i_Unbilled_Type
	 params['custpage_unbilled_receivable'] = i_Unbilled_Receivable_GL
	 params['custpage_unbilled_revenue'] = i_Unbilled_Revenue_GL
	 params['custpage_reverse_date'] = i_Reverse_Date
	 params['custpage_criteria'] = i_Criteria
	 params['custpage_user_subsidiary'] = i_Subsidiary
	 params['custpage_search_count'] = i_Search_Count_f
	 params['custpage_month'] = i_Month
	 params['custpage_year'] = i_Year
	 params['custscript_status'] = 'T';
	 
     nlapiSetRedirectURL('SUITELET','customscript_unbilled_timeexpense_report','customdeploy1', null,params);
	




}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
