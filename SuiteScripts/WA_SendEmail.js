/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Dec 2014     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	nlapiLogExecution('AUDIT', 'Test', nlapiGetRecordId());
	var rec = nlapiLoadRecord('employee', nlapiGetRecordId());
	
	var i_time_approver = rec.getFieldValue('timeapprover');
	var s_email = nlapiLookupField('employee', i_time_approver, 'email');
	
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_recipient_email', null, 'is', s_email);
	filters[0] = new nlobjSearchFilter('custrecord_send_email_status', null, 'is', 'PENDING');
	filters[1] = new nlobjSearchFilter('custrecord_recipient_employee', null, 'anyof', i_time_approver);
	filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	var search_result = nlapiSearchRecord('customrecord_send_email_queue', null, filters, null);
	//nlapiLogExecution('AUDIT', 'Number of Lines' + search_result.length, 'Email: ' + s_email);
	if(search_result == null)
		{
			try
			{
				var newRecord = nlapiCreateRecord('customrecord_send_email_queue');
				
				newRecord.setFieldValue('custrecord_recipient_email', s_email);
				newRecord.setFieldValue('custrecord_send_email_status', 'PENDING');
				newRecord.setFieldValue('custrecord_recipient_employee', i_time_approver);
				nlapiSubmitRecord(newRecord);
			}
			catch(e)
			{
				nlapiLogExecution('EMERGENCY', nlapiGetRecordId(), e.message);
			}
			
		}
	nlapiLogExecution('AUDIT', 'Employee Id: ' + nlapiGetRecordId(), rec.getFieldValue('name'));
}
