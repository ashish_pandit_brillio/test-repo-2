// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Salary_Upload_Update.js
	Author      : Shweta Chopde
	Date        : 6 Aug 2014
    Description : Update the JEs created


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	try
	{
		var i_CSV_File_ID =  request.getParameter('custscript_csv_file_id_update_sut');
		var i_monthly_employee =  request.getParameter('custscript_monthly_employee_update_sut');
		var i_hourly_employee =  request.getParameter('custscript_hourly_employee_update_sut');
		var i_account_debit =  request.getParameter('custscript_account_debit_update_sut');
		var i_account_credit =  request.getParameter('custscript_account_credit_update_sut');
		var i_recordID =  request.getParameter('custscript_record_id_update_sut');
		var a_JE_array =  request.getParameter('custscript_je_array_update_sut');
		
					
		nlapiLogExecution('DEBUG', 'suiteletFunction',' Record ID -->' + i_recordID);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' CSV File ID -->' + i_CSV_File_ID);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Monthly Employee -->' + i_monthly_employee);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Hourly Employee -->' + i_hourly_employee);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Account Credit -->' + i_account_debit);
	    nlapiLogExecution('DEBUG', 'suiteletFunction',' Account Debit -->' + i_account_credit);
		nlapiLogExecution('DEBUG', 'suiteletFunction',' JE Array -->' + a_JE_array);
		
		
		// ================= Call Schedule Script ================
				
		 var params=new Array();
	     params['custscript_csv_file_id_update'] = i_CSV_File_ID
		 params['custscript_monthly_employee_update'] = i_monthly_employee
		 params['custscript_hourly_employee_update'] = i_hourly_employee
		 params['custscript_account_debit_update'] = i_account_debit
		 params['custscript_account_credit_update'] = i_account_credit
		 params['custscript_record_id_update'] = i_recordID
		 params['custscript_je_array_update'] = a_JE_array
				 
		 var status=nlapiScheduleScript('customscript_sch_salary_upload_update',null,params);
		 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
			 
		nlapiSetRedirectURL('RECORD', 'customrecord_salary_upload_process', i_recordID, null,null);
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ......... ' );	
	    		
		 
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
/**
 * @author Shweta
 */
