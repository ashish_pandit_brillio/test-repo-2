/**
 * Send a mail trigger to IS team if any new allocation is done for the Move
 * customer
 * 
 * Version Date Author Remarks 1.00 22 Feb 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {

		if (type == 'create' || type == 'edit') {
			var jobId = nlapiGetFieldValue('project');

			// check if the project belongs to customer Move
			var customer = nlapiLookupField('job', jobId, 'customer');

			if (customer == 8381) {
				var mailDetails = moveAllocationMailTemplate();
				nlapiSendEmail(constant.Mail_Author.InformationSystem,
				        constant.Mail_Author.InformationSystem,
				        mailDetails.Subject, mailDetails.Body);
				nlapiLogExecution('debug', 'mail send to information systems');
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}

function moveAllocationMailTemplate() {
	var htmltext = '<p>Hi ,</p>';

	htmltext += "<p>This is to inform you that a new resource allocation has been created for the customer Move."
	        + "Please make sure <b>"
	        + nlapiGetFieldText('allocationresource')
	        + "</b> is assigned the correct role.</p>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'New Move Allocation',
	    Body : addMailTemplate(htmltext)
	};
}
