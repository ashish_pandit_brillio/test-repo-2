/**
 * Module Description
 * 
 * Version    Date               Author           Remarks
 * 1.00       06 Nov 2020     shravan.k    Sets the Employee name for PR Request
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function UE_BS_PR_Item_Employee(type)
{
  try
  {
	 nlapiLogExecution('DEBUG', 'type==', type)
	  if((type== 'create') )
		  {
		  var i_Requestor_Id = nlapiGetFieldValue('custrecord_requisitionname');
		//  nlapiLogExecution('DEBUG', 'i_Requestor_Id==', i_Requestor_Id);
		  var i_Item_Line_Count = nlapiGetLineItemCount('recmachcustrecord_purchaserequest');
		//  nlapiLogExecution('DEBUG', 'i_Item_Line_Count==', i_Item_Line_Count);
		  for (var i_Line_index = 1; i_Line_index <= i_Item_Line_Count ; i_Line_index++ )
			  {
			//  nlapiSetLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_employee', i_Line_index, i_Requestor_Id);
			//  nlapiLogExecution('DEBUG', 'i_Line_index==', i_Line_index);
			  nlapiSelectLineItem('recmachcustrecord_purchaserequest', i_Line_index)
			  nlapiSetCurrentLineItemValue('recmachcustrecord_purchaserequest', 'custrecord_employee',i_Requestor_Id );
			  nlapiCommitLineItem('recmachcustrecord_purchaserequest');
			  }// for (var i_Line_index = 0; i_Line_index < i_Item_Line_Count ; i_Line_index++ )
		  } /// End of  if(type== 'create')
  } /// End of try
  catch(e)
  {
	  nlapiLogExecution('DEBUG', 'userEventAfterSubmit_PR_Item_Employee Exception===', e)
  }/// End of catch
}/// End of function userEventAfterSubmit
