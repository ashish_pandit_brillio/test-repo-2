/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Item_data(obj_Request) {
         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var obj_Response_Return = {};
             if (s_Request_Data == 'ITEM') {
                 obj_Response_Return = get_Item_Data(obj_Search);
             }
         } 
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Item_data


     return {
         post: Send_Item_data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Item_Data(obj_Search) {
 try {
    var itemSearchObj = obj_Search.create({
        type: "item",
        filters:
        [
        ],
        columns:
        [
            obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
            obj_Search.createColumn({
              name: "itemid",
              sort: obj_Search.Sort.ASC,
              label: "Name"
           })
        ]
     });
     var searchResultCount = itemSearchObj.runPaged().count;
     log.debug("itemSearchObj result count",searchResultCount);
     var myResults = getAllResults(itemSearchObj);
     var arr_Item_json = [];
     myResults.forEach(function (result) {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.getValue({ name: "internalid"}),
             'Name': result.getValue({ name: "itemid"}),
         };
         arr_Item_json.push(obj_json_Container);
         return true;
     });
     return arr_Item_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('get_Item_Data s_Exception== ', s_Exception);
 } //// End of catch 
} ///// End of function get_Item_Data()


function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}