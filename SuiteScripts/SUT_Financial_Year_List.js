// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT Financial Year
	Author:Nikhil jain
	Company:Aashnacloudtech Pvt Ltd.
	Date:4/7/2015
	Description:Create and display a financila year list


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_financial_year(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-

		FIELDS USED:

          --Field Name--				--ID--


	*/


	//  LOCAL VARIABLES


    //  SUITELET CODE BODY
	// == SC : CREATE FORM ===
	
	if (request.getMethod() == 'GET') 
	{
	
		var form = nlapiCreateForm('AIT Financial Year');
		
		var start_year = form.addField('fy_start_date', 'date', 'Financial Year Start Date')
		start_year.setMandatory(true);
		
		var end_year = form.addField('fy_end_date', 'date', 'Financial Year End Date')
		end_year.setMandatory(true);
			
		var attResult = searchfinancial_year(request, response);
		
		var sublist1 = form.addSubList('year','list','Financial Year', 'tab1');
		
		setSubList(sublist1,form,1, attResult);
		
		form.addSubmitButton('Setup New Financial Year');
		
		response.writePage( form );
	}
	else if(request.getMethod() == 'POST')
   		{
   		// === SC:Calling function for Add new Record of TDS Master It is call after Sbmitting the form ===
   		addRecord(request,response);
   		nlapiSetRedirectURL('SUITELET','customscript_sut_ait_financial_year', '1', false);
	
   		}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
function setSubList(sublist, form,isForm,results,request,response)
{

              // === Add fields to subList === 
			  sublist.addField('name','text','Name')
			  
	          sublist.addField('custrecord_fy_start_date','date','Start Date')
			 
			  sublist.addField('custrecord_fy_end_date','date','End Date')
			  
			  sublist.addField('custrecord_fy_code','text','FY Code')
				
			 sublist.addField('custrecord_assessment_code','text','Assessment Year')
	  
	         // === set the sublist ===
	         sublist.setLineItemValues(results);
	return sublist;
}
function searchfinancial_year(request, response)
{
	var filters = new Array();
    var column= new Array();

	//=== Begin:Select the Fields from TDS Master Record. ===
	column.push( new nlobjSearchColumn('name'));
	column.push( new nlobjSearchColumn('custrecord_fy_start_date'));
	column.push( new nlobjSearchColumn('custrecord_fy_end_date'));
	column.push( new nlobjSearchColumn('custrecord_fy_code'));
	column.push( new nlobjSearchColumn('custrecord_assessment_code'));
	
	var results = nlapiSearchRecord('customrecord_financial_year', null, null, column);
	if(results != null)
	{
		return results;
	}
}
function addRecord(request,response)
{
	var start_date = request.getParameter('fy_start_date')
	
	var end_date = request.getParameter('fy_end_date')
	
	var o_start_date = nlapiStringToDate(start_date)
	
	var o_end_date = nlapiStringToDate(end_date)
	
	var start_year = o_start_date.getFullYear();
	
	var end_year = o_end_date.getFullYear();
	
	var new_end_year = end_year.toString();
	
	var last_year = new_end_year.substring(2);
	
	var fy_year = start_year +''+last_year
	
	var asseement_year = end_year +''+(parseInt(last_year)+parseInt(1))
	
	var name = 'FY ' + start_year +'-'+last_year;
	
 	var o_fy_record = nlapiCreateRecord('customrecord_financial_year')
	
	o_fy_record.setFieldValue('name',name)
	
	o_fy_record.setFieldValue('custrecord_fy_code',fy_year)
	
	o_fy_record.setFieldValue('custrecord_assessment_code',asseement_year)
	
	o_fy_record.setFieldValue('custrecord_fy_start_date',start_date)
	
	o_fy_record.setFieldValue('custrecord_fy_end_date',end_date)
	
	nlapiSubmitRecord(o_fy_record)
}



// END OBJECT CALLED/INVOKING FUNCTION =====================================================
