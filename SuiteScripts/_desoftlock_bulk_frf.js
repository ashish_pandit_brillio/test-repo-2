/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */
define(['N/record', 'N/search', 'N/format'], function (record, search, format) {
    function _post(datain) {
        try {
            log.debug({
                title: 'datain.length',
                details: datain.length
            });
            var data = [];
            var d = new Date();
            log.debug({
                title: 'd',
                details: d
            })
            var formatDate = format.format({
                value: d,
                type: format.Type.DATE
            })
            log.debug({
                title: 'formatDate',
                details: formatDate
            })
            for (var i = 0; i < datain.length; i++) {
                var datainObj = datain[i];
                log.debug('datainObj Creation', datainObj);
                frfId = datainObj.frfId;
                empId = datainObj.empId;
                user = datainObj.user;
                if (frfId) {
                    var employeeSearch = getEmployeeRec(user);
                    record.submitFields({
                        type: "customrecord_frf_details",
                        id: frfId,
                        values: {
                            'custrecord_frf_details_selected_emp': "",
                            "custrecord_frf_details_status_flag": "1",
                            "custrecord_frf_details_open_close_status": "1",
                            "custrecord_frf_details_desoftlock_date": formatDate,
                            "custrecord_frf_details_softlock_date": "",
                            "custrecordfrf_details_created_time": "",
                            "custrecord_frf_details_softlocked_by": employeeSearch
                        }
                    })
                    data.push(frfId);
                }
                else{
                    return {
                        "code": "frf doesnot exist",
                        "message": "unsuccessful"
                    }
                }
            }
            return {
                "code": "frf has been processed",
                "message": data
            }
        } catch (error) {
            log.error({
                title: error.name,
                details: error.message
            })
        }
    }
    function getEmployeeRec(user) {
        var employeeSearchObj = search.create({
            type: "employee",
            filters:
                [
                    ["email", "is", user]
                ],
            columns:
                [

                ]
        });
        var searchResultCount = employeeSearchObj.runPaged().count;
        var empId;
        log.debug("employeeSearchObj result count", searchResultCount);
        employeeSearchObj.run().each(function (result) {
            empId = result.id;
            return true;
        });
        return empId
    }
    return {
        post: _post
    }
});
