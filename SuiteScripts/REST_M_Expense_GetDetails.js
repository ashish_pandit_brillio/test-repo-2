/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Aug 2016     shruthi.l
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('debug', 'dataIn employeeId', employeeId);
		
		var transactionDate = dataIn.Data.Date;
		nlapiLogExecution('debug', 'dataIn transactionDate', transactionDate);
		var T_date = dateSplit(transactionDate);
		nlapiLogExecution('debug', 'T_date transactionDate', T_date);
		
		//Testign Data
		/*var employeeId = 30237;
		var transactionDate = '22/8/2016';
		var T_date = dateSplit(transactionDate);*/
		
		//var requestType = dataIn.RequestType;
		//var expenseId = dataIn.Data.ExpenseId;
	//	var project_ID = dataIn.ProjectName;
	//	switch (requestType) {

	//	case M_Constants.Request.Get:

			if (employeeId) {
				response.Data = getEmployeeDetail(employeeId, T_date);
				response.Status = true;
			} 
			
		
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	return response;
}

function dateSplit(tranDate){
	
	try{
		var date = tranDate.split('/');
		var day = date[0];
		var month = date[1];
		var year = date[2];
		var tran_Date = month+'/'+day+'/'+year;
		return tran_Date;
	}
	catch(e){
		nlapiLogExecution('ERROR', 'Date Splitting Error', err);
		throw e;
	}
}

function getEmployeeDetail(employeeId , T_date) {
	try {
		//var employeeRec = nlapiLoadRecord('employee', employeeId);
		var employeeDetails = {};
		var exchangeRateTable ={};
		var json = {};
		
		var C_date = nlapiStringToDate(T_date);
		
		//Get Subsidiary
		var employeeLookup = nlapiLookupField('employee',employeeId,['subsidiary'],true);
		var subsidiary_E = employeeLookup.subsidiary;
		
		var employeeLookup_S  = nlapiLookupField('employee',employeeId,['subsidiary']); 
		var subsidiary_V = employeeLookup_S.subsidiary;
		//Get Approver
		if(employeeId){
			var emp_lookUp = nlapiLookupField('employee',employeeId,['approver'],true);
			var first_Approver = emp_lookUp.approver;
			nlapiLogExecution('debug', 'first_Approver', first_Approver);
			}
		
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('internalid', 'employee', 'anyof', employeeId);
		var current_date = nlapiStringToDate(nlapiDateToString(new Date()));
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', 'current_date...' + current_date)
		filters[filters.length] = new nlobjSearchFilter('enddate', null, 'onorafter', current_date);
		filters[filters.length] = new nlobjSearchFilter('startdate', null, 'onorbefore', current_date);

		var returncols = new Array();
		returncols[0] = new nlobjSearchColumn('company');
	
		
		nlapiLogExecution('DEBUG', 'getEmployeeDetail', '2 Before Search...');
		var employeeAllocatedProjects = nlapiSearchRecord('resourceallocation', null, filters, returncols);
		
		employeeDetails.projects = employeeAllocatedProjects;
		
		//Get Expense Categories
		var expCatFilter =  new Array();;
		expCatFilter[0]= new nlobjSearchFilter('isinactive', null, 'is', 'F');
		expCatFilter[1]= new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary_V);
		expCatFilter[2]= new nlobjSearchFilter('custrecord_exp_category_applicable_emp', null, 'anyof', 1);
		var expCatColumns = new Array();
		expCatColumns[0] = new nlobjSearchColumn('name');
		var expCategories = nlapiSearchRecord('expensecategory', null, expCatFilter, expCatColumns);
		employeeDetails.expenseCategories = expCategories;
		//employeeDetails.firstLevelApprover = first_Approver;
		if(subsidiary_V == 3){ //BTPL
		var inr = nlapiExchangeRate('USD', 'INR', T_date);
		 exchangeRateTable = {
			   // INR : nlapiExchangeRate('INR', 'USD', C_date),
			    USD : nlapiExchangeRate('USD', 'INR', T_date),
			    GBP : nlapiExchangeRate('GBP', 'INR', T_date),
			    EUR : nlapiExchangeRate('EUR', 'INR', T_date),
			    SGD : nlapiExchangeRate('SGD', 'INR', T_date),
			    INR: nlapiExchangeRate('INR', 'INR', T_date),
			    Peso : nlapiExchangeRate('PHP', 'INR', T_date)
			};
		}
		
		if(subsidiary_V == 2){   //BLLC
			var inr = nlapiExchangeRate('INR', 'USD', T_date);
			 exchangeRateTable = {
				   // INR : nlapiExchangeRate('INR', 'USD', C_date),
				    USD : nlapiExchangeRate('USD', 'USD', T_date),
				    GBP : nlapiExchangeRate('GBP', 'USD', T_date),
				    EUR : nlapiExchangeRate('EUR', 'USD', T_date),
				    SGD : nlapiExchangeRate('SGD', 'USD', T_date),
				    INR: nlapiExchangeRate('INR', 'USD', T_date),
				    Peso : nlapiExchangeRate('PHP', 'USD', T_date)
				};
		}
			json ={
					Details:employeeDetails,
					FirstLevelApprover: first_Approver,
					Subsidiary: subsidiary_E,
					Subsidiary_ID: subsidiary_V,
					ExchangeRate: exchangeRateTable
					
			};
			
		nlapiLogExecution('debug','Response',JSON.stringify(json));
		return json;
		// check access
/*		if (expenseReportRec.getFieldValue('custbody1stlevelapprover') == firstLevelApprover) {
			var expenseDetails = {};

			expenseDetails.Employee = expenseReportRec.getFieldText('entity');
			expenseDetails.FirstLevelApprover = expenseReportRec
			        .getFieldText('custbody1stlevelapprover');
			expenseDetails.SecondLevelApprover = expenseReportRec
			        .getFieldText('custbody_expenseapprover');

			expenseDetails.Expense = {
			    Reference : expenseReportRec.getFieldValue('tranid'),
			    Purpose : isEmpty(expenseReportRec.getFieldValue('memo')) ? ''
			            : expenseReportRec.getFieldValue('memo'),
			    Status : expenseReportRec.getFieldValue('status'),
			    InternalId : expenseReportRec.getId(),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    VAT : expenseReportRec.getFieldValue('tax1amt'),
			    PostingDate : expenseReportRec.getFieldValue('trandate'),
			    TransactionDate : expenseReportRec
			            .getFieldValue('custbody_transactiondate'),
			    AccountName : expenseReportRec.getFieldText('account'),
			    Account : expenseReportRec.getFieldValue('account'),
			    // Reason : '',
			    Currency : expenseReportRec.getFieldText('currency'),
			    DocumentReceived : expenseReportRec
			            .getFieldValue('custbody_exp_doc_received'),
			    DocumentReceivedDate : '',
			    ItemList : []
			// ApproverRemark : expenseReportRec.getFieldValue(''),
			};

			expenseDetails.Expense.Summary = {
			    Expenses : expenseReportRec.getFieldValue('total'),
			    NonReimbursableExpenses : expenseReportRec
			            .getFieldValue('nonreimbursable'),
			    ReimbursableExpenses : expenseReportRec
			            .getFieldValue('reimbursable'),
			    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
			    TotalReimbursableAmount : expenseReportRec
			            .getFieldValue('amount')
			};

			// get all line items
			var lineItemCount = expenseReportRec.getLineItemCount('expense');
			var expenseList = [];
			var isReimbursable = null;
			var projectId = null;
			var projectIdList = [];

			for (var line = 1; line <= lineItemCount; line++) {
				isReimbursable = expenseReportRec.getLineItemValue('expense',
				        'isnonreimbursable', line);
				projectId = expenseReportRec.getLineItemValue('expense',
				        'customer', line);
				projectIdList.push(projectId);

				// if (isFalse(isReimbursable)) {

				if (!expenseDetails.ProjectText) {
					expenseDetails.ProjectText = expenseReportRec
					        .getLineItemValue('expense', 'custcolprj_name',
					                line);
				}
				expenseList.push({
				    ProjectText : expenseReportRec.getLineItemValue('expense',
				            'custcolprj_name', line),
				    ProjectId : projectId,
				    Vertical : expenseReportRec.getLineItemValue('expense',
				            'class_display', line),
				    CategoryText : expenseReportRec.getLineItemValue('expense',
				            'category_display', line),
				    Amount : expenseReportRec.getLineItemValue('expense',
				            'foreignamount', line),
				    ForeignAmount : expenseReportRec.getLineItemValue(
				            'expense', 'amount', line),
				    Currency : expenseReportRec.getLineItemText('expense',
				            'currency', line),
				    IsReimbursable : isReimbursable,
				    DeliveryManager : null
				});
				// }
			}

			expenseDetails.Expense.ItemList = expenseList;
			return expenseDetails;
		} else {
			throw "You are not authorised to access this record";
		}*/
	} catch (err) {
		nlapiLogExecution('error', 'getExpenseDetail', err);
		throw err;
	}
}