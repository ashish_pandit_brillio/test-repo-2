/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_SOW_Project_Sourcing.js
	Author     : Shweta Chopde
	Date       :
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================


if(type =='item' && name == 'job')
	{
		var job =nlapiGetCurrentLineItemValue('item','job');
		if (job != null && job != undefined && job != '') {
			nlapiSetCurrentLineItemValue('item','custcol_sow_project', job);
		}
	}


// BEGIN AFTER SUBMIT =============================================

function afterSubmit_JOB(type)
{
   try
   {
   	var i_recordID = nlapiGetRecordId()
	var s_record_type = nlapiGetRecordType()
	
	if (_logValidation(i_recordID) && _logValidation(s_record_type)) 
	{
		var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID)
		nlapiLogExecution('DEBUG', 'beforeLoad_create_button', ' Record Object -->' + o_recordOBJ);
		
		if (_logValidation(o_recordOBJ)) 
		{
			var i_customer = o_recordOBJ.getFieldText('entity')
			nlapiLogExecution('DEBUG', 'beforeLoad_create_button', ' Customer -->' + i_customer);
			
			if(i_customer!='COIN-01 Collabera Inc')
			{					
				var i_line_count_QA = o_recordOBJ.getLineItemCount('item')
				nlapiLogExecution('DEBUG', 'beforeLoad_create_button', ' Line Count QA -->' + i_line_count_QA);
				
				if (_logValidation(i_line_count_QA)) 
				{
					for (var a = 1; a <= i_line_count_QA; a++) 
					{
						var i_approval_status = o_recordOBJ.getLineItemValue('item', 'job', a)
						nlapiLogExecution('DEBUG', 'beforeLoad_create_button', ' Approval Status -->' + i_approval_status);
						
						
						o_recordOBJ.setLineItemValue('item','custcol_sow_project', a,i_approval_status);
					}
				}
				
			}//Customer	
			
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
			nlapiLogExecution('DEBUG', 'beforeLoad_create_button', ' i_submitID-->' + i_submitID);
					
		}
	}
   	
   }
    catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}//CATCH
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END FUNCTION =====================================================
