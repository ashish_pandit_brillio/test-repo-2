function postRESTlet(dataIn) {

	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));

		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		nlapiLogExecution('debug', 'dataIn employeeId', employeeId);

		var expense_ID = dataIn.Data ? dataIn.Data.ExpenseID : '';
		if(expense_ID){
			var i_expense_Id = getExpenseInternalID(expense_ID,employeeId);
		}

		var requestType = dataIn.RequestType;

		switch (requestType) {

		case M_Constants.Request.Get:
			if (employeeId) {
				response.Data = getEmployeeExpenseDetail(employeeId);
				response.Status = true;
			}
			else {
				response.Data = 'Please provide the employee email !!';
				response.Status = false;
			}
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
	}
	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function dateSplit(tranDate){

	try{
		var date = tranDate.split('/');
		var day = date[0];
		var month = date[1];
		var year = date[2];
		var tran_Date = month+'/'+day+'/'+year;
		return tran_Date;
	}
	catch(e){
		nlapiLogExecution('ERROR', 'Date Splitting Error', err);
		throw e;
	}
}

function getEmployeeExpenseDetail(employeeId) {
	try {

		var InProgress = ['ExpRept:D' , 'ExpRept:E','ExpRept:A'];
		var overall = [];
		var filters = [];
		var dataRow = [];
		var InProgressList ={};
		var filters = [];

		filters.push(new nlobjSearchFilter('status', null, 'anyof', InProgress));
		filters.push(new nlobjSearchFilter('employee', null, 'anyof', employeeId));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		var columns = Array();
		columns.push(new nlobjSearchColumn('tranid'));
		columns.push(new nlobjSearchColumn('memo'));
		columns.push(new nlobjSearchColumn('status'));
		columns.push(new nlobjSearchColumn('fxamount'));
		columns.push(new nlobjSearchColumn('trandate'));
		columns.push(new nlobjSearchColumn('lastmodifieddate').setSort(true));
		columns.push(new nlobjSearchColumn('subsidiary')); //

		var expenseSearch = nlapiSearchRecord('expensereport',null,filters,columns);
		var len = 0;

		var employeeLookup_1 = nlapiLookupField('employee',parseInt(employeeId),['entityid']);
		var employee_id = employeeLookup_1.entityid;
		if(expenseSearch){
			nlapiLogExecution('debug', 'expenseSearch InProgress', expenseSearch.length);
			if(expenseSearch.length > 10)
				len = 10;
				else
				len = expenseSearch.length;	
			//len = 1;
			for(var i=0;i<len;i++){

			var expenseID = expenseSearch[i].getId();
			var expenseLoad = nlapiLoadRecord('expensereport',expenseID);
			var lineCount = expenseLoad.getLineItemCount('expense');


			for(var j = 1;j <= 1;j++){

				var project =  expenseLoad.getLineItemValue('expense','customer_display',j);

				}

			var subsidiary = expenseSearch[i].getValue('subsidiary');
		    var lookupSUbsidairy_ = nlapiLookupField('subsidiary',subsidiary,['currency'],true);
			var currency_1 = lookupSUbsidairy_.currency;
			var amount_f = expenseSearch[i].getValue('fxamount');
			amount_f = parseFloat(amount_f).toFixed(1);

		InProgressList ={
		SlNo: i+1,
		Employee: employee_id,
		ExpenseID : expenseSearch[i].getValue('tranid'),
		Date : expenseSearch[i].getValue('trandate'),
		Purpose : expenseSearch[i].getValue('memo'),
		Status : expenseSearch[i].getValue('status'),
		Amount : amount_f,
		Project: project,
		Subsidiary: expenseSearch[i].getText('subsidiary'),
		Currency: currency_1
		};
		dataRow.push(InProgressList);
		}
		}
		overall.push(dataRow);

		//Rejected Expenses
	/*	var filters_R = [];
		var Rejected = ['ExpRept:D' , 'ExpRept:E','ExpRept:A'];
		var len_R = 0;
		var RejectedList = {};
		var dataRows = [];

		filters_R.push(new nlobjSearchFilter('status', null, 'anyof', Rejected));
		filters_R.push(new nlobjSearchFilter('employee', null, 'anyof', employeeId));
		filters_R.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var columns_R = Array();
		columns_R.push(new nlobjSearchColumn('tranid'));
		columns_R.push(new nlobjSearchColumn('memo'));
		columns_R.push(new nlobjSearchColumn('status'));
		columns_R.push(new nlobjSearchColumn('fxamount'));
		columns_R.push(new nlobjSearchColumn('trandate').setSort(true));
		columns_R.push(new nlobjSearchColumn('subsidiary')); //

		var expenseSearch_R = nlapiSearchRecord('expensereport',null,filters_R,columns_R);

		var employeeLookup_1_R = nlapiLookupField('employee',parseInt(employeeId),['entityid']);
		var employee_id_R = employeeLookup_1_R.entityid;

		if(expenseSearch_R){

			nlapiLogExecution('debug', 'expenseSearch RejectedList', expenseSearch_R.length);
			if(expenseSearch_R.length > 10)
				len_R = 10;
				else
				len_R = expenseSearch_R.length;	
			//len_R = 1;
			for(var i = 0; i < len_R; i++){
				var expenseID = '';
			expenseID = expenseSearch_R[i].getId();
			var expenseLoad = nlapiLoadRecord('expensereport',expenseID);
			var lineCount = expenseLoad.getLineItemCount('expense');

			if(lineCount >= 1){
			for(var j = 1;j <= 1;j++){

				var project =  expenseLoad.getLineItemValue('expense','customer_display',j);

				}
			}


			var subsidiary_ = expenseSearch_R[i].getValue('subsidiary');
			var lookupSUbsidairy = nlapiLookupField('subsidiary',subsidiary_,['currency'],true);
			var currency_2 = lookupSUbsidairy.currency;
			var amount_fll = expenseSearch_R[i].getValue('fxamount');
			amount_fll = parseFloat(amount_fll).toFixed(1);



		RejectedList ={
		SlNo: i+1,
		Employee: employee_id,
		ExpenseID : expenseSearch_R[i].getValue('tranid'),
		Date : expenseSearch_R[i].getValue('trandate'),
		Purpose : expenseSearch_R[i].getValue('memo'),
		Status : expenseSearch_R[i].getValue('status'),
		Amount : amount_fll,
		Project: project,
		Subsidiary: expenseSearch_R[i].getText('subsidiary'),
		Currency: currency_2
		};
		dataRows.push(RejectedList);
		}
		} */
		overall = {
			InProgressList : dataRow
			//RejectedList   : dataRows
		};
		nlapiLogExecution('debug','Response',JSON.stringify(overall));
		return overall;

	} catch (err) {
		nlapiLogExecution('error', 'getRejectedList', err);
		throw err;
	}
}

function getExpenseInternalID(expense_id,emp) {
	try{
	var id = '';
	var filters = Array();
	filters.push(new nlobjSearchFilter('tranid', null, 'startswith', expense_id));
	filters.push(new nlobjSearchFilter('employee', null, 'anyof', emp));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	var cols = Array();
	cols.push(new nlobjSearchColumn('internalid'));

	var searchObj = nlapiSearchRecord('expensereport',null,filters,cols);
	if(searchObj)
	id = searchObj[0].getValue('internalid');

	return id;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Erron in searching expense',e);
		throw e.details;
	}
}
/*function getExpensesDetails(emp){
	try{
		var flag = true;
		var currency_1;
		var json ={},JsonCategory ={};
		var dataRow =[],dataRows=[];
		var main ={};
		nlapiLogExecution('DEBUG','Expense ID ON GETONE',expenseid);
		var record_Obj = nlapiLoadRecord('expensereport',expenseid);
		var emp_name = record_Obj.getFieldValue('entityname');
		var first_approver = record_Obj.getFieldText('custbody1stlevelapprover');
		var T_purpose = record_Obj.getFieldValue('memo'); //subsidiary
		var subsidiary_ = record_Obj.getFieldValue('subsidiary');
		var subsidiary_text = record_Obj.getFieldText('subsidiary');
		var totalAmt = record_Obj.getFieldValue('total');
		totalAmt = parseFloat(totalAmt).toFixed(1);
		var T_Status = record_Obj.getFieldValue('statusRef');
		var trandate = record_Obj.getFieldValue('trandate');
		var lineCount = record_Obj.getLineItemCount('expense');

		for(var jk=1;jk<= 1;jk++){

			var project =  record_Obj.getLineItemValue('expense','customer_display',jk);

			}
		if(subsidiary_ == 3){
			currency_1 = 'INR';
			}
			else{
			currency_1 = 'USD';
			}
		json ={
			Employee: emp_name,
			Currency: currency_1,
			Date: trandate,
			Approver: first_approver,
			Subsidiary: subsidiary_text,
			Project: project,
			Status: T_Status,
			Purpose: T_purpose,
			TotalAmt: totalAmt

		};
		dataRow.push(json);

		for(var i=1;i<= lineCount;i++){
			var img ='';
			var data = '';
			var file = '';
			var amount_line = record_Obj.getLineItemValue('expense','foreignamount',i);
			amount_line = parseFloat(amount_line).toFixed(1);
			 img = record_Obj.getLineItemValue('expense','expmediaitem',i);
			if(img){
			 file  = nlapiLoadFile(img);
			 data = file.getValue();
			 nlapiLogExecution('debug','Img data','line:'+i+','+'data:'+data);
		}
			JsonCategory ={
			 RefNo: record_Obj.getLineItemValue('expense','refnumber',i),		
			 Category: record_Obj.getLineItemValue('expense','category_display',i),
			 Currency: record_Obj.getLineItemText('expense','currency',i),
			 Memo: record_Obj.getLineItemValue('expense','memo',i),
			 Date : record_Obj.getLineItemValue('expense','expensedate',i),
			 Amount: amount_line, //customer_display
			 Project: record_Obj.getLineItemValue('expense','customer_display',i),
			 imgData: data

			}
			dataRows.push(JsonCategory);
		}
		main = {
			Body: dataRow,
			Line:dataRows

		};
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(main));
		return main;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Erron in getting the expense details',e);
		throw e;

	}

}*/
function _logValidation(value)
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN)
 {
  return true;
 }
 else
 {
  return false;
 }
}