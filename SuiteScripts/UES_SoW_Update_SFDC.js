/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2017     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	
	try{
		if(type == 'edit'){
		//Conversion rate search
			var inr_to_usd = 0;
			var gbp_to_usd = 0;
			var eur_to_usd = 0;
			var a_conversion_rate_table = nlapiSearchRecord('customrecord_conversion_rate_sfdc',null,null,[new nlobjSearchColumn('custrecord_rate_inr_to_usd'),
			                                                              new nlobjSearchColumn('custrecord_rate_gbp_to_usd'),
			                                                              new nlobjSearchColumn('custrecord_rate_eur_to_usd'),
			                                                              new nlobjSearchColumn('internalid').setSort(true)]);
			if(a_conversion_rate_table){
				inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_rate_inr_to_usd'));
				gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_gbp_to_usd');
				eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_rate_eur_to_usd');
				}
				//
		var soW_Obj = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var o_job_old_rcrd = nlapiGetOldRecord();
		var old_status = o_job_old_rcrd.getFieldText('orderstatus');
		var new_status = soW_Obj.getFieldText('orderstatus');
		var soW_val=soW_Obj.getFieldValue('subtotal');
        var projctID = soW_Obj.getFieldValue('job');
        var Booked_status = '';
          var project_bckup = '';
		var s_currency = soW_Obj.getFieldText('currency');
		var JSON_O  = {};
		var dataRow = [];
		//Conversion factor
		var f_currency_conversion = 1;
		if(s_currency =='USD')
			f_currency_conversion = 1;
		if(s_currency =='INR')
			f_currency_conversion = inr_to_usd;
		if(s_currency =='EUR')
			f_currency_conversion = 1/eur_to_usd;
		if(s_currency =='GBP')
			f_currency_conversion = 1/gbp_to_usd;
		
		if(!_logValidation(soW_val)){
			soW_val = 0;
		}
         //Added for SOW Booked Trigger
         if(_logValidation(projctID))
         {
           project_bckup = nlapiLookupField('job',projctID,'entityid');
            var resourceSearch= nlapiSearchRecord('resourceallocation',null,[["project","anyof",projctID],"AND", ["job.jobbillingtype","anyof","TM"]],[new nlobjSearchColumn("internalid")]);
              if(_logValidation(resourceSearch))
                {
                  Booked_status  = 'Booked';
                }
              else
                Booked_status = '';
              
           }
          //
		 nlapiLogExecution('DEBUG','conversion_rate',f_currency_conversion);
		//if(soW_Obj.getFieldText('custbody_projecttype') == 'External' )//&& old_status != new_status
			JSON_O = {
				'PONumber': soW_Obj.getFieldValue('otherrefnum'),
				'SOWID': soW_Obj.getFieldValue('tranid'),
				'Customer': soW_Obj.getFieldText('entity'),
				'location': soW_Obj.getFieldText('location'),
				'SowStatus': soW_Obj.getFieldText('custbody_sow'),
				'Subsidiary': soW_Obj.getFieldText('subsidiary'),
				'BillingStatus': soW_Obj.getFieldText('orderstatus'), //custbody_projectmodel
              'ProjectionStatus':Booked_status, //Added for child opp
              'ProjectId':project_bckup ,
				//'SubPractice': soW_Obj.getFieldText('custentity_practice'),
				'SowValue':	(parseFloat(f_currency_conversion) * parseFloat(soW_val)).toFixed(1),
				'BillingType': soW_Obj.getFieldText('custbody_billingtype'),
				'DeliveryModel': soW_Obj.getFieldText('custbody_projectmodel')
		};
		dataRow.push(JSON_O);
		
		/*var header = new Object();
	  	var clientId = '3MVG9snqYUvtJB1PmnEp.Fve_rGABQCLfVhDFXQmZZkzMHssYKilXlSWrsQxlGOljsYGbobCWtwgLMptX73vn';
        var clientSecret = '3792420350206543236';
        var username= 'anirudh.bandi@brillio.com.brillio';
        var password='manhunt!1234'+'cryMDjFlS63Im0uTzEEyaLCYx';
        
        var param_header = 'grant_type=password&client_id='+clientId+'&client_secret='+clientSecret+'&username='+username+'&password='+password;
        
        var URL = 'https://test.salesforce.com/services/oauth2/token?'+param_header;*/
		var accessURL = getAccessToken();
        var method = 'POST';
		
        var response = nlapiRequestURL(accessURL,null,null,method);
        var temp = response.body;
        nlapiLogExecution('DEBUG','JSON',temp);
        var data = JSON.parse(response.body);
        var access_token_SFDC = data.access_token;
        var instance_URL_Sfdc = data.instance_url;
        nlapiLogExecution('DEBUG','JSON',data);
	        
	        var dynamic_URL = instance_URL_Sfdc+'/services/apexrest/SowBackUpdate/v1.0/';
	        var auth = 'Bearer'+' '+access_token_SFDC;
	        
	        //Test
	      //Setting up Headers 
	        /*var headers = {"User-Agent-x": "SuiteScript-Call",
	                       "Authorization": "Bearer "+access_token_SFDC,
	                       "Content-Type": "application/json"};*/
	        
	        var headers = {"Authorization": "Bearer "+access_token_SFDC,
	        		 "Content-Type": "application/json",
	        		 "accept": "application/json"};
	        //
	        var method_ = 'POST';
	        var response_ = nlapiRequestURL(dynamic_URL,JSON.stringify(dataRow),headers,method_);
	        var data = JSON.parse(response_.body);
	        var message = data[0].message;
	        var details_ = data.details;
	        nlapiLogExecution('DEBUG','JSON',message); 
	        nlapiLogExecution('DEBUG','details_',details_); 
	        nlapiLogExecution('DEBUG','JSON',JSON.stringify(dataRow)); 
	        
	       // var JSON = JSON.stringify(response);
		}  
	}
	catch(e){
		
		nlapiLogExecution('DEBUG','Process Error',e);
		throw e;
	}
  
}

function replacer(key, value){
    if (typeof value == "number" && !isFinite(value)){
        return String(value);
    }
    return value;
}
//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}