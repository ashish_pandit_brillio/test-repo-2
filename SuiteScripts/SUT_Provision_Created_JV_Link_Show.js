// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_Provision_Created_JV_Link_Show.js
	Author:Swati Kurariya
	Company:Aashna CloudTech Pvt. Ltd.
	Date:26-02-2015
	Description:Show Created Jv Link To Navigation Purpose.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_Show_JV_Link(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		  -After processing of monthly provision ,created jv link show. 


		FIELDS USED:

          --Field Name--				--ID--


	*/


//  LOCAL VARIABLES
    var d_From_Date;
	  var d_To_Date;
	  var o_Subsidairy;
	  var i_Unbilled_Type;
	  var i_Criteria;
	  var i_Customer;
	  var i;
	  var i_Month;
	  var i_Year;
	  var s_str='';


  //  SUITELET CODE BODY
   //---------------------get papameter value---------------
	  var f_form=nlapiCreateForm('Journal Entry Reference');
	  f_form.setScript('customscript_provision_time_expense_v');
	//----------------------------------------------------  
     d_From_Date=request.getParameter('d_from_date');
	 d_To_Date=request.getParameter('d_to_date');
	 o_Subsidairy=request.getParameter('o_subsidairy');
	 i_Unbilled_Type=request.getParameter('i_unbilled_type');
	 i_Criteria=request.getParameter('i_criteria');
	 i_Customer=request.getParameter('i_customer');
	 i_Month=request.getParameter('i_month');
	 i_Year=request.getParameter('i_year');
	 
	 nlapiLogExecution('DEBUG','In Get','d_From_Date=='+d_From_Date);
	 nlapiLogExecution('DEBUG','In Get','d_To_Date=='+d_To_Date);
	 nlapiLogExecution('DEBUG','In Get','o_Subsidairy=='+o_Subsidairy);
	 nlapiLogExecution('DEBUG','In Get','i_Unbilled_Type=='+i_Unbilled_Type);
	 nlapiLogExecution('DEBUG','In Get','i_Criteria=='+i_Criteria);
	 nlapiLogExecution('DEBUG','In Get','i_Customer=='+i_Customer);
	 nlapiLogExecution('DEBUG','In Get','i_Month=='+i_Month);
	 nlapiLogExecution('DEBUG','In Get','i_Year=='+i_Year);
	 
	  var i_time_provision_f = f_form.addField('custpage_time_provision', 'textarea', 'List Of Created Journal Entry.').setDisplayType('inline');	
	  var link=f_form.addField('custpagelink', 'inlinehtml','journal entry').setLayoutType('midrow','startrow')
	  
	if(i_Unbilled_Type == 2)
		{
	 
	    var filter = new Array();
		var i=4;
        filter[0] = new nlobjSearchFilter('custrecord_month_val',null,'is',i_Month);  
	    filter[1] = new nlobjSearchFilter('custrecord_year_val',null,'is',i_Year);
     	filter[2] = new nlobjSearchFilter('custrecord_subsidiary',null,'is', parseInt(o_Subsidairy));
  	    filter[3] = new nlobjSearchFilter('custrecord_criteria',null,'is', parseInt(i_Criteria));
  	
	
	if(_logValidation(i_Customer))
		{
		filter[i] = new nlobjSearchFilter('custrecord_customer',null,'is',parseInt(i_Customer));
		i++;
		}
		
		
  /*	if(i_criteria == 3)
  	{
  		
	        nlapiLogExecution('DEBUG', 'suiteletFunction',' i_criteria -->' + i_criteria);
  		filter[i] = new nlobjSearchFilter('custrecord_billable',null,'is','T');
  		i++;
  		filter[i] = new nlobjSearchFilter('custrecord_sum_of_not_submitted',null,'greaterthan',0);
  		i++;
  	}*/

	var custom_time_search_results = nlapiSearchRecord('customrecord_time_bill_rec','customsearch_journal_entry_list',filter,null);
	 //nlapiLogExecution('DEBUG', 'i_criteria','i_criteria' + i_criteria);
	if(_logValidation(custom_time_search_results))
		{
		     nlapiLogExecution('DEBUG', 'custom_time_search_results','custom_time_search_results' + custom_time_search_results.length);
		    for(var kk=0 ; kk<custom_time_search_results.length ; kk++)
		    	{
				    
		    		
	    	    
		    	    var a_search_transaction_result = custom_time_search_results[kk];
					nlapiLogExecution('DEBUG', 'a_search_transaction_result',a_search_transaction_result);
						
					var columns=a_search_transaction_result.getAllColumns();
					nlapiLogExecution('DEBUG', 'columns',columns);
					
					var columnLen = columns.length;
					var journal_entry_id=a_search_transaction_result.getValue(columns[0]);
					nlapiLogExecution('DEBUG', 'journal_entry_id',journal_entry_id);
					
					
					if(_logValidation(journal_entry_id))
					{
						var load_jv=nlapiLoadRecord('journalentry',journal_entry_id)//
						
						
						
						var tran_id=load_jv.getFieldValue('tranid');
						
						var url='https://system.na1.netsuite.com'+nlapiResolveURL('RECORD','journalentry',journal_entry_id);
						var linkText='<a target="_blank" href="'+url+'"><b>'+tran_id+'<b></a><br>';
						
						 if(kk==0)
							{
								s_str=linkText;
							}
						else
							{
								s_str=s_str+linkText;
							}
					}
		    	}
		    
		    link.setDefaultValue(s_str);
			
		
		}
	//f_form.addButton('custpage_go_home_page','Go to Home Page','redirect_to_home_page()');
	//f_form.addSubmitButton('SUBMIT');
		}//close unbilled type 2
    
     
     response.writePage(f_form);

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

	function _logValidation(value) 
	{
	 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
	 {
	  return true;
	 }
	 else 
	 { 
	  return false;
	 }
	}

}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
