/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Expense_Report_Buttons.js
	Author      : Shweta Chopde
	Date        : 22 Aug 2014
	Description : Create Approve Button  on Expense Report


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_expense_report_buttons(type)
{
  try
  {
  	var i_expense_report_approver = nlapiGetFieldValue('custbody_expenseapprover');
	
	var i_status = nlapiGetFieldValue('status');
	
	var i_user = nlapiGetUser();
	nlapiLogExecution('DEBUG','beforeLoad_expense_report_buttons','User -->'+i_user)
	
	var i_user_role = nlapiGetRole();
	nlapiLogExecution('DEBUG','beforeLoad_expense_report_buttons','User Role -->'+i_user_role)
	
	nlapiLogExecution('DEBUG','beforeLoad_expense_report_buttons','Expense Report Approver -->'+i_expense_report_approver)
	nlapiLogExecution('DEBUG','beforeLoad_expense_report_buttons','Status -->'+i_status)
	
	if( i_status == 'Pending Accounting Approval'&&_logValidation(i_expense_report_approver))
	{
		if((i_user == i_expense_report_approver) || (i_user == 123052) ||(i_user_role == 3))
		{
			form.setScript('customscript_cli_expense_report_approval');
	        form.addButton('custpage_expense_print_button','Approve','approval_expense_report()');				
		}
		
	} 	
  	
  }
  catch(exception)
  {
  	nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Exception -->' + exception);		
  }//Exception

  return true;
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END FUNCTION =====================================================
