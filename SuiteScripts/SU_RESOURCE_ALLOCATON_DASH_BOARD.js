/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Oct 2017     Poobalan K
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY)

}


function workday_count(start,end) {
	  var first = start.clone().endOf('week'); // end of first week
	  var last = end.clone().startOf('week'); // start of last week
	  var days = last.diff(first,'days') * 5 / 7; // this will always multiply of 7
	  var wfirst = first.day() - start.day(); // check first week
	  if(start.day() == 0) --wfirst; // -1 if start with sunday 
	  var wlast = end.day() - last.day(); // check last week
	  if(end.day() == 6) --wlast; // -1 if end with saturday
	  return wfirst + days + wlast; // get the total
	}
function getBusinessDatesCount(startDate, endDate) {
    var count = 0;
    var curDate = startDate;
    while (curDate <= endDate) {
        var dayOfWeek = curDate.getDay();
        if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
           count++;
        curDate.setDate(curDate.getDate() + 1);
    }
    //alert(count)
    return count;
}

function Resourcea_allocation_dash_board_suitelet(request, response){
	try{
	if (request.getMethod() == 'GET' )
		{
		var User_ID = nlapiGetUser();
		var User_Email = nlapiLookupField('employee',User_ID, 'email',false);
		nlapiLogExecution('DEBUG','UserEmail', User_Email);
		var form = nlapiCreateForm('Resource Allocation');
		form.addField('custpage_customer','select','CUSTOMER','customer').setMandatory(true);
		form.addField('custpage_project_id','multiselect','Project ID','job');
		form.addField('custpage_email','email','Email').setDisplayType('hidden').setDefaultValue(User_Email);

		form.setScript('customscript_cli_resource_allocaton_dash');
		form.addField('custpage_proj_billing_type','select','Billing Type','customlist_billing_type_list');
		form.addSubmitButton('Export');
		response.writePage(form);
		}
	else
		{
		var customer = request.getParameter('custpage_customer');
		var billing_type = request.getParameter('custpage_proj_billing_type');
		var proj_id = request.getParameter('custpage_project_id');
		var User_Email = request.getParameter('custpage_email');
		
		if(billing_type == 1){var billing_type_value = 'FBI'}
		if(billing_type == 2){var billing_type_value = 'FBM'}
		if(billing_type == 3){var billing_type_value = 'TM'}
		
      /*nlapiLogExecution('DEBUG','UserEmail', User_Email);
		nlapiLogExecution('DEBUG', 'proj_id', proj_id);*/
		
		if(User_Email == 'poobalan@inspirria.com' || User_Email == 'Ananda.Hirekerur@brillio.com'|| User_Email == 'chetan.vasuki@brillio.com' || User_Email == 'Devi.Swamy@brillio.com' ||User_Email == 'Smitha.Thumbikkat@brillio.com' ||User_Email == 'sai.vannamareddy@brillio.com'||User_Email == 'netsuitehelpdesk@brillio.com')
		  {  
			  var intArr = [];
			  if(proj_id)
				  {
				  var PROJECT_ID = proj_id.toString();
				  var strArr = PROJECT_ID.split('');
				  for(i=0; i < strArr.length; i++)
				     intArr.push(parseInt(strArr[i]));
				  
				  nlapiLogExecution('DEBUG', 'intArr',JSON.stringify(intArr));
				  }
			 
			
			
		   var col = [],filt = [],Searchresults = [];
		   
		  
		  if(customer && proj_id && billing_type)// && billing_type
		   {
	     	filt[0] = new nlobjSearchFilter("parent",null,"is",customer);
	    	filt[1] = new nlobjSearchFilter("jobbillingtype", null,"is",billing_type_value);
	    	filt[2] = new nlobjSearchFilter("internalid", null,"is",intArr);
	   	    }        
	    else if(customer  && proj_id && (!billing_type)) 
	    	{
	    	filt[0] = new nlobjSearchFilter("parent",null,"is",customer);
	    	filt[1] = new nlobjSearchFilter("internalid", null,"is",intArr);
		    }       
	    else if(customer && (!proj_id) && billing_type)  // && proj_id
		   {
	    	filt[0] = new nlobjSearchFilter("parent",null,"is",customer);
	    	filt[1] = new nlobjSearchFilter("jobbillingtype", null,"is",billing_type_value);
		   } 
		  Searchresults = nlapiSearchRecord(null,2171,filt,null);
		  
		  
		 // var a_project_search_results = searchRecord('null', 2171, a_project_filter, a_columns_proj_srch);
	   
		  nlapiLogExecution('DEBUG', 'Searchresults',JSON.stringify(Searchresults));
		  
	    var strName="<head>";
		    strName+="<style>";
		    strName+="th{background-color: #3c8dbc; color:white;}";
		    strName+=".BorderTopLeft{border-top:solid;border-left:solid}";
		    strName+=".BorderBottomLeft{border-bottom:solid;border-left:solid}";
		    strName+=".BorderTopRight{border-top:solid;border-right:solid}";
		    strName+=".BorderBottomRight{border-bottom:solid;border-right:solid}";
		    strName+=".BorderTop{border-top:solid;}";
		    strName+=".BorderLeft{border-left:solid}";
		    strName+=".BoderAll{border-bottom:solid;border-right:solid;border-Top:solid;border-left:solid}";
		    strName+=".BorderRight{border-right:solid;}";
		    strName+=".BorderBottom{border-bottom:solid;}";
		    strName+=".BorderTopLeftRight{border-Top:solid;border-Right:solid;border-Left:solid;}"; 
		    strName+="body{font-family:sans-serif;font-size:8px;margin-top:0px;}";
		    strName+="</style>";
		    strName+="<macrolist>";
		    strName+="<macro id='myfooter'>";
		    strName+="<table  style='width: 100%;'><tr>";
		    strName+="<td align='right' ><pagenumber/> of <totalpages/></td>";
		    strName+="</tr></table>";
		    strName+="</macro>";
		    strName+="<macro id='myHead'>";
		    strName+="</macro>";
		    strName+="</macrolist>";
		    strName+="</head>";
		    
		    
		    strName += "<table width='1430px' border='1'>";
		    strName += "<tr>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Customer</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Project Id</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Project Description</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Billing Type</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Start Date</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>End Date</b></td>";
		    
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Resoure Name</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Allocation Start Date</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Allocation End Date</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>% of Allocation</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Efforts used<br/> till date (Hrs)</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Efforts remaining<br/>till allocation<br/> end date (Hrs)</b></td>";
		    strName += "</tr>";
		    

		    for(var i=0; Searchresults!=null && i<Searchresults.length;i++)
			{
		    	var columns	= Searchresults[i].getAllColumns();
		    	var Proj_ID	= Searchresults[i].getValue(columns[0]);
		    	var Proj_Description= Searchresults[i].getValue(columns[1]);
		    	var Customer= Searchresults[i].getText(columns[2]);
		    	var Start_Date= Searchresults[i].getValue(columns[3]);
		    	var End_Date	= Searchresults[i].getValue(columns[4]);
		    	var Resource_Name= Searchresults[i].getText(columns[5]);
		    	var Allocation_start_Date= Searchresults[i].getValue(columns[6]);
		    	var Allocation_End_Date	= Searchresults[i].getValue(columns[7]);
		    	var Allow_percent= Searchresults[i].getValue(columns[9]);
		    	var Bill_Type= Searchresults[i].getText(columns[12]);
		    	var Resource_s=Searchresults[i].getValue(columns[5]);
		    	
		    	var Allo_StartDate = nlapiStringToDate(Allocation_start_Date);
		    	var Allo_EndDate = nlapiStringToDate(Allocation_End_Date);
		    	var TodayDate = new Date();
				var employee_subsi=nlapiLookupField('employee',Resource_s,'subsidiary');
		    	var holidays=getHolidays(Allo_StartDate,Allo_EndDate,employee_subsi);
								
				if(Allo_EndDate<TodayDate)
                {
                  var Total_days_Spent = getBusinessDatesCount(Allo_StartDate,Allo_EndDate);
                }
              else
                {
                  var Total_days_Spent = getBusinessDatesCount(Allo_StartDate,TodayDate);
                }
				Total_days_Spent=parseInt(Total_days_Spent)-parseInt(holidays);
		    	nlapiLogExecution('DEBUG','TodayDate -- Allo_StartDate---Allo_EndDate',TodayDate+'===='+Allo_StartDate+'==='+Allo_EndDate);
		    	
		    	
             
		    	var Total_remaining_days = getBusinessDatesCount(TodayDate,Allo_EndDate);
		    	
		    	nlapiLogExecution('DEBUG','Total_days_Spent --- Total_remaining_days',Total_days_Spent+'===='+Total_remaining_days);
		    	
		    	var Total_Hours_Spent = parseFloat(Total_days_Spent)*8;
		    	var Total_Hours_Remain = parseFloat(Total_remaining_days)*8;
		    	
		    	nlapiLogExecution('DEBUG', 'Total_Hours_Spent -- Total_Hours_Remain', Total_Hours_Spent+'==='+Total_Hours_Remain);
		    	
		    	strName += "<tr>";
		    	strName += "<td width='30px' style='align:left;' font-size='10px'>"+Customer+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Proj_ID+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Proj_Description+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Bill_Type+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Start_Date+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+End_Date+"</td>";
			    
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Resource_Name+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Allocation_start_Date+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Allocation_End_Date+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+nlapiEscapeXML(Allow_percent)+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Total_Hours_Spent+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+Total_Hours_Remain+"</td>";
			    strName += "</tr>";
		      }
		    strName += "</table>";

	//======================================DISPLAY IN PRINT============================		
			strName += "</body>";
	      

			var xlsFile = nlapiCreateFile('TEST.xls', 'EXCEL', nlapiEncrypt(strName, 'base64'));
			//var id = nlapiSubmitFile(xlsFile);
		   //nlapiSendEmail('poobalan@inspirria.com','poobalan@inspirria.com','Test', 'Test', null, null,null, id, null, null, null);
			response.setContentType(xlsFile.getType(),'Resource Allocation Details.xls');
			response.write(xlsFile.getValue());
		  }	
		else
			{
			response.write("You don't have the Privilege to Access this Record.......!!!! ");
			}

	  
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','Resourcea_allocation_dash_board_suitelet','ERROR MESSAGE :- '+err);
	}
		
}



/////// client script

function SuiteletformFieldChanged(type, name, linenum)
{
	try
	{
	  if(name == 'custpage_customer' || name == 'custpage_proj_billing_type')
		{
		var CustomerID= nlapiGetFieldValue('custpage_customer');
		var billing_type= nlapiGetFieldValue('custpage_proj_billing_type');
		
		 /* Calling Restlet for*/
	    function getAccount () { return '3883006'; }
		
		function getRESTletURL()
		{
			return 'https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=1502&deploy=1&c='+getAccount(); 
		}

		function credentials()
		{
			this.email='Poobalan@inspirria.com';
			this.account=getAccount();
			this.role='3';
			this.password="Inspirria@2017";//netsuiteAccessPassword;
		}
		var url = getRESTletURL();
		var cred = new credentials();
		
		var headers = new Array();
		    headers['User-Agent-x'] = 'SuiteScript-Call';
		    headers['Authorization'] = 'NLAuth nlauth_account='+cred.account+', nlauth_email='+cred.email+', nlauth_signature='+cred.password+', nlauth_role='+cred.role;
		    headers['Content-Type'] = 'application/json';	
		var  jsonObj = new Object();
	         jsonObj.CustomerID      = CustomerID;
	         jsonObj.billing_type        = billing_type;
	        
	         
	    var myJson = JSON.stringify(jsonObj);
	    alert("MY JASON"+myJson);
	    var ItemRecArr = nlapiRequestURL( url, myJson, headers,null, "POST" );
		  var obj = (ItemRecArr.getBody());//getting item data from the Restlet Response...
		   
		    var json = JSON.parse(obj);
		  alert(JSON.stringify(json));
		 var MyArray =[];
		  for(var k in json)
	    	{
			  var Proj_ID = json[k].Proj_ID;
			  MyArray.push(Proj_ID);
			  
	    	}
		  alert("MyArray===="+JSON.stringify(MyArray));

		   nlapiSetFieldValues('custpage_project_id',MyArray,false,false);
		
		    /* Calling Restlet for*/
		
		//alert(CustomerID+"=="+billing_type);
		
/*	*/
		}
	}
	catch (error)
	{
		nlapiLogExecution('ERROR', 'ERROR',error);
	}
	
}

//// LIBRARY FUCTION TO LOAD MORE THAN 1000 RESULTS////////

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

 try {
  var search = null;

  // if a saved search is provided, load it and add the filters and
  // columns
  if (isNotEmpty(savedSearch)) {
   search = nlapiLoadSearch(recordType, savedSearch);

   if (isArrayNotEmpty(arrFilters)) {
    search.addFilters(arrFilters);
   }

   if (isArrayNotEmpty(arrColumns)) {
    search.addColumns(arrColumns);
   }

   if (isArrayNotEmpty(filterExpression)) {
    search.setFilterExpression(filterExpression);
   }
  }
  // create a new search
  else {
   search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
  }

  // run search
  var resultSet = search.runSearch();

  // iterate through the search and get all data 1000 at a time
  var searchResultCount = 0;
  var resultSlice = null;
  var searchResult = [];

  do {
   resultSlice = resultSet.getResults(searchResultCount,
           searchResultCount + 1000);

   if (resultSlice) {

    resultSlice.forEach(function(result) {

     searchResult.push(result);
     searchResultCount++;
    });
   }
  } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

  return searchResult;
 } catch (err) {
  nlapiLogExecution('ERROR', 'searchRecord', err);
  throw err;
 }
}

function isEmpty(value) {

 return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

 return !isEmpty(value);
}

function isArrayEmpty(argArray) {

 return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

 return (isNotEmpty(argArray) && argArray.length > 0);
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }
 
 function getHolidays(d_start_date, d_end_date,subsi) {

      var filters = new Array();
      filters[0] = new nlobjSearchFilter('custrecord_date', null, 'within',
              d_start_date, d_end_date);
		filters[1] = new nlobjSearchFilter('custrecordsubsidiary', null, 'is',
              subsi);	 

      var columns = new Array();
      columns[0] = new nlobjSearchColumn('custrecord_date',null,'group');
      columns[1] = new nlobjSearchColumn('custrecordsubsidiary',null,'group');

      var search_holidays = nlapiSearchRecord('customrecord_holiday', null,
              filters, columns);

      var a_holidays = new Object();
	var count=0;
      for (var i = 0; search_holidays != null && i < search_holidays.length; i++) {
            var i_subsidiary = search_holidays[i].getValue('custrecordsubsidiary',null,'group');
            var o_holiday = {
                  'date' : nlapiStringToDate(search_holidays[i]
                          .getValue('custrecord_date',null,'group'))
            };
            if (a_holidays[i_subsidiary] == undefined) {
                  a_holidays[i_subsidiary] = [ o_holiday ];
				  count++;
            } else {
                  a_holidays[i_subsidiary].push(o_holiday);
				  count++;
            }
      }

      return count;
}
