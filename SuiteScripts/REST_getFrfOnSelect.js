// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name : REST_getFrfOnSelect.js
    	Author      : ASHISH PANDIT
    	Date        : 09 APRIL 2018
        Description : RESTlet to get FRF details on drop down selection 


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
     


    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         SCHEDULED FUNCTION
    		- scheduledFunction(type)


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function REST_getFrfOnSelect(dataIn) {
    try {
        var role = dataIn.roleSelected;
        var frfnumber = dataIn.frfnumber;
        var practice = dataIn.practice;
        var project = dataIn.project;
        var type = dataIn.type;
        /*var useremail = dataIn.user;
        var user = getUserUsingEmailId(useremail);
        var spoc = GetPracticeSPOC(user);
        var admin = GetAdmin(user);
        var projects = GetProjects(user);
        
        
        nlapiLogExecution('debug','projects ',projects[0].getId());
        nlapiLogExecution('Debug','Admin ',admin);
        nlapiLogExecution('Debug','spoc ',spoc);
        */
        nlapiLogExecution('Debug', 'dataIn ', JSON.stringify(dataIn));
        var values = {};
        var arrSkills = [];
        if (role) {
            values.roleselected = getRoleDetails(role, null);
        } else if (frfnumber) {
            values.frf = getRoleDetails(null, frfnumber);
        } else if (practice) {
            values.role = getRoles(practice);
        } else if (project) {
            values.projectTile = getTileData(project, type);
        }

        return values;
    } catch (error) {
        nlapiLogExecution('Debug', 'error ', error);
    }
}
/*******************************SEARCH TO GET ROLES ON SELCTION OF PRACTICE****************************************/
function getRoles(practice) {

    nlapiLogExecution('Debug', 'In function', practice);
    var i_parent_practice = nlapiLookupField('department', practice, 'custrecord_parent_practice');
    var customrecord_fuel_role_mapping_skillSearch = nlapiSearchRecord("customrecord_fuel_role_mapping_skill", null,
        [
            ["custrecord_fuel_role_mapping_practice", "anyof", i_parent_practice],
            "AND",
            ["custrecord_exclude", "is", "F"],
            "AND",
            ["isinactive", "is", "F"]
        ],
        [
            new nlobjSearchColumn("custrecord_fuel_role_mapping_name"),
            new nlobjSearchColumn("custrecord_fuel_role_mapping_practice")
        ]
    );
    if (customrecord_fuel_role_mapping_skillSearch) {
        nlapiLogExecution('Debug', 'customrecord_fuel_role_mapping_skillSearch ', customrecord_fuel_role_mapping_skillSearch.length);
        var dataArray = new Array();
        for (var i = 0; i < customrecord_fuel_role_mapping_skillSearch.length; i++) {
            var data = {};
            data.role = {
                "name": customrecord_fuel_role_mapping_skillSearch[i].getValue("custrecord_fuel_role_mapping_name"),
                "id": customrecord_fuel_role_mapping_skillSearch[i].getId()
            };
            dataArray.push(data);
        }
        return dataArray;
    }
}

/*******************************SEARCH TO GET DATA FOR SELCTED FRF/ROLE******************************************/
function getRoleDetails(role, frfnumber) {
    var i_role = role;
    nlapiLogExecution('debug', 'i_role ', i_role);
    var i_frfnumber = frfnumber;
    var filters = new Array();
    var columns = new Array();
    columns = [
        new nlobjSearchColumn("scriptid").setSort(false),
        new nlobjSearchColumn("custrecord_frf_details_opp_id"),
        new nlobjSearchColumn("custrecord_frf_details_res_location"),
        new nlobjSearchColumn("custrecord_frf_details_res_practice"),
        new nlobjSearchColumn("custrecord_frf_details_emp_level"),
        new nlobjSearchColumn("custrecord_frf_details_external_hire"),
        new nlobjSearchColumn("custrecord_frf_details_role"),
        new nlobjSearchColumn("custrecord_frf_details_bill_rate"),
        new nlobjSearchColumn("custrecord_frf_details_critical_role"),
        new nlobjSearchColumn("custrecord_frf_details_skill_family"),
        new nlobjSearchColumn("custrecord_frf_details_start_date"),
        new nlobjSearchColumn("custrecord_frf_details_end_date"),
        new nlobjSearchColumn("custrecord_frf_details_selected_emp"), //Need to check
        new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"),
        new nlobjSearchColumn("custrecord_frf_details_primary_skills"),
        new nlobjSearchColumn("custrecord_frf_details_frf_number"),
        new nlobjSearchColumn("custrecord_frf_details_project"),
        new nlobjSearchColumn("custrecord_frf_details_suggestion"),
        new nlobjSearchColumn("custrecord_frf_type"),
        new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"),
        new nlobjSearchColumn("custrecord_frf_details_billiable"),
        new nlobjSearchColumn("custrecord_frf_details_account"),
        new nlobjSearchColumn("custrecord_frf_details_source"),
        new nlobjSearchColumn("custrecord_frf_details_secondary_skills"),
        new nlobjSearchColumn("custrecord_frf_details_allocation"),
        new nlobjSearchColumn("custrecord_frf_details_dollar_loss"),
        new nlobjSearchColumn("custrecord_frf_details_special_req"),
        new nlobjSearchColumn("custrecord_frf_details_personal_email"),
        new nlobjSearchColumn("custrecord_frf_details_created_by"),
        new nlobjSearchColumn("custrecord_frf_details_parent"),
        new nlobjSearchColumn("custrecord_frf_details_status_flag"),
        new nlobjSearchColumn("custrecord_frf_details_availabledate"),
        new nlobjSearchColumn("custrecord_frf_details_sales_activity"),
        new nlobjSearchColumn("custrecord_frf_details_region"),
        new nlobjSearchColumn("custrecord_frf_details_backup_require"),
        new nlobjSearchColumn("custrecord_frf_details_job_title"),
        new nlobjSearchColumn("custrecord_frf_details_edu_lvl"),
        new nlobjSearchColumn("custrecord_frf_details_edu_program"),
        new nlobjSearchColumn("custrecord_frf_details_emp_status"),
        new nlobjSearchColumn("custrecord_frf_details_first_interviewer"),
        new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"),
        new nlobjSearchColumn("custrecord_frf_details_cust_interview"),
        new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"),
        new nlobjSearchColumn("custrecord_frf_details_ta_manager"),
        new nlobjSearchColumn("custrecord_frf_details_req_type"),
        new nlobjSearchColumn("custrecord_frf_details_reason_external"),
        new nlobjSearchColumn("custrecord_frf_details_allocated_emp"),
        new nlobjSearchColumn("custrecord_frf_details_allocated_date"),
        new nlobjSearchColumn("custrecord_frf_details_rrf_number"),
        new nlobjSearchColumn("custrecord_frf_details_instruction_team"),
        new nlobjSearchColumn("custrecord_frf_details_position_type"),
        new nlobjSearchColumn("custrecord_frf_details_job_descriptions"),
        new nlobjSearchColumn("custrecord_frf_desired_start_date"),
        new nlobjSearchColumn("custrecord_frf_details_replacement_emp"), // prabhat gupta 28/05/2020
        new nlobjSearchColumn("custrecord_frf_details_fitment_type"), //prabhat gupta 04/09/2020 NIS-1723
        new nlobjSearchColumn("custrecord_frf_details_level") //prabhat gupta 07/10/2020 NIS-1755
    ]
    if (i_role) {
        filters = [
            ["custrecord_frf_details_role", "anyof", i_role],
            "AND",
            ["isinactive", "is", "F"]
        ]; //["custrecord_frf_details_role","is",i_role];

    }
    if (i_frfnumber) {
        filters = ["custrecord_frf_details_frf_number", "is", i_frfnumber]
    }
    var customrecord_frf_detailsSearch = searchRecord("customrecord_frf_details", null, filters, columns);
    if (customrecord_frf_detailsSearch) {
        nlapiLogExecution('Debug', 'customrecord_frf_detailsSearch length ', JSON.stringify(customrecord_frf_detailsSearch));
        nlapiLogExecution('Debug', 'customrecord_frf_detailsSearch 243234 ', customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_project'));
        nlapiLogExecution('Debug', 'FRF Number ', customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_frf_number'));
        var data = {};
        data.role = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_role'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_role')
        };
        data.account = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_account'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_account')
        };
        data.project = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_project'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_project')
        };
        data.practice = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_res_practice'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_res_practice')
        };
        data.location = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_res_location'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_res_location')
        };
        data.allocation = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_allocation')
        };
        data.category = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_billiable')
        };
        data.startdate = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_start_date')
        };
        data.enddate = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_end_date')
        };
        data.criticalrole = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_critical_role')
        };
        data.exthire = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_external_hire')
        };
        data.billrate = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_bill_rate')
        };
        var primarySkillsText = customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_primary_skills');
        var primarySkillsIds = customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_primary_skills');
        var secSkillsText = customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_secondary_skills');
        var secSkillsIds = customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_secondary_skills');
        data.primaryskills = {
            "name": getSkillIds(primarySkillsText),
            "id": getSkillIds(primarySkillsIds)
        };
        data.secondaryskills = {
            "name": getSkillIds(secSkillsText),
            "id": getSkillIds(secSkillsIds)
        };
        data.requesttype = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_type'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_type')
        };

        //--------------------------------------------------------------------------------------------------------

        //prabhat gupta 04/09/2020 NIS-1723

        data.fitmentType = {
            "name": customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_fitment_type"),
            "id": customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_fitment_type")
        };


        //---------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------

        //prabhat gupta 06/10/2020 NIS-1755

        data.level = {
            "name": customrecord_frf_detailsSearch[0].getText("custrecord_frf_details_level"),
            "id": customrecord_frf_detailsSearch[0].getValue("custrecord_frf_details_level")
        };


        //---------------------------------------------------------------------------------------------------------



        //----------------------------------------------------------------------------------------------------------
        // prabhat gupta 28/05/2020

        data.replacingCandidate = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_replacement_emp'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_replacement_emp')
        };

        //-------------------------------------------------------------------------------------------------------------
        data.positionType = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_position_type'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_position_type')
        };
        data.reasonforexthire = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_reason_external')
        };
        data.jobtitle = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_job_title')
        };
        data.backupofferd = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_backup_require')
        };
        data.employeestatus = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_emp_status'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_emp_status')
        };
        data.educationlevel = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_edu_lvl'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_edu_lvl')
        };
        data.educationprogram = {
            "name": customrecord_frf_detailsSearch[0].getText('custrecord_frf_details_edu_program'),
            "id": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_edu_program')
        };
        data.firstinterviewer = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_first_interviewer')
        };
        data.secondinterviewer = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_sec_interviewer')
        };
        data.custinterviewercheck = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_cust_interview')
        };
        data.custintervieweremail = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_cust_inter_email')
        };
        data.jobdescription = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_job_descriptions')
        };
        data.instruction = {
            "name": customrecord_frf_detailsSearch[0].getValue('custrecord_frf_details_instruction_team')
        };
        data.desired_start_date = customrecord_frf_detailsSearch[0].getValue('custrecord_frf_desired_start_date');
        nlapiLogExecution('Debug', 'Data Array ', JSON.stringify(data));
        return data;
    }
}
/*******************************SEARCH TO TILE DATA******************************************/
function getTileData(i_projectName, type) {
    nlapiLogExecution('Debug', 'i_projectName ' + i_projectName, 'type ' + type);
    var filters = [];
    if (i_projectName) {
        if (type == 'project') {
            
            filters = [
                ['custrecord_frf_plan_details_ns_project', 'anyOf', i_projectName],
                'and',
                ['custrecord_frf_plan_details_confirmed', 'is', 'F']
            ];
        } else if (type == 'opportunity') {
            var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
                [
                    ["custrecord_opportunity_id_sfdc", "is", i_projectName]
                ],
                [
                    new nlobjSearchColumn("internalid")
                ]
            );
            for(var i = 0; i < customrecord_sfdc_opportunity_recordSearch.length; i++){
                var searchObj = customrecord_sfdc_opportunity_recordSearch[i];

                var i_opp  = searchObj.getValue('internalid');
            }
            filters = [
                ['custrecord_frf_plan_details_opp_id', 'anyOf', i_opp],
                'and',
                ['custrecord_frf_plan_details_confirmed', 'is', 'F']
            ];
        }
    }
    nlapiLogExecution('Debug', 'filters ', filters);
    var customrecord_frf_plan_detailsSearch = searchRecord("customrecord_frf_plan_details", null, filters,
        [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_frf_plan_details_project_role"),
            new nlobjSearchColumn("custrecord_frf_plan_details_startdate"),
            new nlobjSearchColumn("custrecord_frf_plan_details_positions"),
            new nlobjSearchColumn("custrecord_frf_plan_request_type"),
            new nlobjSearchColumn("custrecord_frf_plan_details_ns_account"),
            new nlobjSearchColumn("custrecord_frf_plan_details_allocation"),
            new nlobjSearchColumn("custrecord_frf_plan_details_bill_rate"),
            new nlobjSearchColumn("custrecord_frf_plan_details_bill_role"),
            new nlobjSearchColumn("custrecord_frf_plan_details_criticalrole"),
            new nlobjSearchColumn("custrecord_frf_plan_details_emp_lvl"),
            new nlobjSearchColumn("custrecord_frf_plan_details_enddate"),
            new nlobjSearchColumn("custrecord_frf_plan_details_externalhire"),
            new nlobjSearchColumn("custrecord_frf_plan_details_location"),
            new nlobjSearchColumn("custrecord_frf_plan_details_opp_id"),
            new nlobjSearchColumn("custrecord_frf_plan_details_primary"),
            new nlobjSearchColumn("custrecord_frf_plan_details_practice"),
            new nlobjSearchColumn("custrecord_frf_plan_details_ns_project"),
            new nlobjSearchColumn("custrecord_frf_plan_details_special_req"),
            new nlobjSearchColumn("custrecord_frf_plan_details_suggest_res"),
            new nlobjSearchColumn("custrecord_frf_plan_details_other_skills"),
            new nlobjSearchColumn("custrecord_frf_plan_first_inte_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_sec_inte_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_reason_rrf"),
            new nlobjSearchColumn("custrecord_frf_pla_det_backup_require"),
            new nlobjSearchColumn("custrecord_frf_plan_desc_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_job_title_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_edu_lvl_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_edu_pro_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_emp_status_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_cus_inter_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_cust_inter_email_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_instruction_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_res_rrf"),
            new nlobjSearchColumn("custrecord_frf_plan_details_position_typ"),
            new nlobjSearchColumn("custrecord_desired_start_date"),
            new nlobjSearchColumn("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null),
            new nlobjSearchColumn("custrecord_frf_plan_replacement_emp"), //prabhat gupta
            new nlobjSearchColumn("custrecord_frf_plan_fitment_type"), //prabhat gupta 04/9/2020 NIS-1723
            new nlobjSearchColumn("custrecord_frf_plan_level") //prabhat gupta 07/10/2020 NIS-1755
        ]
    );

    if (customrecord_frf_plan_detailsSearch) {
        var tileDataArray = new Array();
        nlapiLogExecution('Debug', 'customrecord_frf_plan_detailsSearch ', customrecord_frf_plan_detailsSearch.length);
        for (var ii = 0; ii < customrecord_frf_plan_detailsSearch.length; ii++) {
            var b_billRoll = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_bill_role");
            var b_criticalRoll = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_criticalrole");
            var b_externalHire = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_externalhire");
            var tileData = {};
            tileData.recordid = {
                "name": customrecord_frf_plan_detailsSearch[ii].getId()
            };
            tileData.role = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_project_role"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_project_role")
            };
            tileData.startDate = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_startdate")
            };
            tileData.endDate = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_enddate")
            };
            tileData.positions = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_positions")
            };
            tileData.Allocation = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_allocation")
            };
            tileData.billrate = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_bill_rate")
            };
            tileData.category = {
                "name": b_billRoll
            };
            tileData.splrequirment = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_special_req")
            };
            tileData.externalhire = {
                "name": b_externalHire
            };
            tileData.criticalrole = {
                "name": b_criticalRoll
            };
            tileData.account = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_account"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_ns_account")
            };
            tileData.emplevel = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_emp_lvl"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_emp_lvl")
            };
            tileData.location = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_location"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_location")
            };
            //tileData.primaryskills = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_primary"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_primary")};
            tileData.requestType = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_request_type"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_request_type")
            };
            tileData.practice = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_practice"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_practice")
            };
            tileData.desired_start_date = customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_desired_start_date");
            if (customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_project")) {
                tileData.project = {
                    "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_project"),
                    "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_ns_project")
                };
            } else if (customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null)) {
                tileData.project = {
                    "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_opportunity_name_sfdc", "custrecord_frf_plan_details_opp_id", null),
                    "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_opp_id")
                }
            }
            //tileData.project = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_ns_project"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_ns_project")};
            tileData.positionType = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_position_typ"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_position_typ")
            };

            //--------------------------------------------------------------------------------------------------------
            //prabhat gupta
            var replacementCandidatesData = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_replacement_emp"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_replacement_emp")
            };
            var replacementCandidatesArray = [];
            for (var j = 0; j < replacementCandidatesData.name.split(",").length; j++) {

                replacementCandidatesArray.push({
                    name: replacementCandidatesData.name.split(",")[j],
                    id: replacementCandidatesData.id.split(",")[j]
                })
            }

            tileData.replacingCandidates = replacementCandidatesArray;
            //--------------------------------------------------------------------------------------------------------


            var suggestedPeopleData = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")
            };
            var suggestedPeopleArray = [];
            for (var j = 0; j < suggestedPeopleData.name.split(",").length; j++) {

                suggestedPeopleArray.push({
                    name: suggestedPeopleData.name.split(",")[j],
                    id: suggestedPeopleData.id.split(",")[j]
                })
            }

            var primaryskills = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_primary"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_primary")
            };
            var primaryskillsArray = [];
            for (var j = 0; j < primaryskills.name.split(",").length; j++) {

                primaryskillsArray.push({
                    name: primaryskills.name.split(",")[j],
                    id: primaryskills.id.split(",")[j]
                })
            }

            var secondaryskills = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_other_skills"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_other_skills")
            };
            var secondaryskillsArray = [];
            for (var j = 0; j < secondaryskills.name.split(",").length; j++) {

                secondaryskillsArray.push({
                    name: secondaryskills.name.split(",")[j],
                    id: secondaryskills.id.split(",")[j]
                })
            }
            tileData.primaryskills = primaryskillsArray; //{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
            tileData.otherskills = secondaryskillsArray; //{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
            tileData.suggestedpeople = suggestedPeopleArray; //{"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_suggest_res"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_suggest_res")};
            //tileData.otherskills = {"name":customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_details_other_skills"),"id":customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_details_other_skills")};
            tileData.frfsource = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_request_type"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_request_type")
            };

            //---------------------------------------------------------------------------------------------------
            //prabhat gupta 04/09/2020 NIS-1723

            tileData.fitmentType = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_fitment_type"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_fitment_type")
            };
            //---------------------------------------------------------------------------------------------------

            //---------------------------------------------------------------------------------------------------
            //prabhat gupta 07/10/2020 NIS-1755

            tileData.level = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_level"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_level")
            };
            //---------------------------------------------------------------------------------------------------

            tileData.backuprequired = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_pla_det_backup_require")
            };
            tileData.jobtitle = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_job_title_rrf")
            };
            tileData.jobdescription = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_desc_rrf")
            };
            tileData.instruction = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_instruction_rrf")
            };
            tileData.educationlevel = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_edu_lvl_rrf"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_edu_lvl_rrf")
            };
            tileData.educationprogram = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_edu_pro_rrf"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_edu_pro_rrf")
            };
            tileData.employeestatus = {
                "name": customrecord_frf_plan_detailsSearch[ii].getText("custrecord_frf_plan_emp_status_rrf"),
                "id": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_emp_status_rrf")
            };
            tileData.firstinterviewer = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_first_inte_rrf")
            };
            tileData.secondinterviewer = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_sec_inte_rrf")
            };
            tileData.custinterview = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_cus_inter_rrf")
            };
            tileData.custintervieweremail = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_cust_inter_email_rrf")
            };
            tileData.externalhirereason = {
                "name": customrecord_frf_plan_detailsSearch[ii].getValue("custrecord_frf_plan_reason_rrf")
            };
            //tileDataArray.push(tileData);
            tileDataArray.push(tileData);
        }
        nlapiLogExecution('Debug', 'tileDataArray$$$$$ ', JSON.stringify(tileDataArray));
        return tileDataArray;
    }
}

/*******************************SEARCH RECORD CODE**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns, filterExpression) {
    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}
/*********************************************************************************************/

function getSkillIds(s_skills) {
    var resultArray = new Array();
    if (_logValidation(s_skills)) {
        nlapiLogExecution('Debug', 's_skills in function', s_skills);
        var temp = s_skills.split(',');
        for (var i = 0; i < temp.length; i++) {
            resultArray.push(temp[i]);
        }
    }
    return resultArray;
}

function _logValidation(value) {
    if (value != null && value != 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}