// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_UpdateExternalHireResource.js
	Author      : ASHISH PANDIT
	Date        : 05 July 2019
    Description : Suitelet to set External Hire Resource on FRF Details Record 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function suitelet(request, response) {
	try
	{
		var i_frfDetails = request.getParameter('custpage_frfnumber');
		var i_selectedEmp = request.getParameter('custpage_employee');
		var d_joiningDate = request.getParameter('custpage_joindate');
		nlapiLogExecution('Debug','i_frfDetails ',i_frfDetails);
		//var i_frfDetails = 90029;
		//var i_selectedEmp = 62082;
		if(i_frfDetails && i_selectedEmp)
		{
			var frf_details = nlapiLookupField('customrecord_frf_details',i_frfDetails,['custrecord_frf_details_allocated_emp','custrecord_frf_details_status_flag']);
			var allocated_emp = frf_details.custrecord_frf_details_allocated_emp;
			var frf_status = frf_details.custrecord_frf_details_status_flag;
			if(!allocated_emp && frf_status == 1)
			{
				var o_frfDetails = nlapiLoadRecord('customrecord_frf_details',i_frfDetails);
				o_frfDetails.setFieldValue('custrecord_frf_details_taleo_emp_select',i_selectedEmp);
				o_frfDetails.setFieldValue('custrecord_frf_details_emp_joining_date',d_joiningDate);
				var recObj = nlapiSubmitRecord(o_frfDetails);
				nlapiLogExecution('Debug','recObj ',recObj);
			}
          else
              	nlapiLogExecution('Debug','Employee Already Allocated ',allocated_emp);
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Exception ',e);
	}
}
