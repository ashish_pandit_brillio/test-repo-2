//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
         Script Name	: SCH_CreateFrfDetails.js
         Author			: Ashish Pandit
         Company		: Inspirria CloudTech Pvt Ltd
         Date			: 17 JAN 2019
         Details        : Scheduled Script to create FRF Details custom records 

    	 Script Modification Log:

    	 -- Date --               -- Modified By --                  --Requested By--                   -- Description --


    	 Below is a summary of the process controls enforced by this script file.  The control logic is described
         more fully, below, in the appropriate function headers and code blocks.
         SCHEDULED FUNCTION
         - RFQbody_SCH_main()
         SUB-FUNCTIONS
         - The following sub-functions are called by the above core functions in order to maintain code
         modularization:
         - NOT USED
     */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================

function scheduled_createFRFDetails() {
    try {
        nlapiLogExecution("DEBUG", "Accoutn ID : ", 'schedule script called');
        //i_oppid = 2816 testing
        var context = nlapiGetContext();
        var i_account = context.getSetting('SCRIPT', 'custscript_account');
        nlapiLogExecution("DEBUG", "Accoutn ID : ", i_account);
        var i_project = context.getSetting('SCRIPT', 'custscript_project_id_create_frf');
        nlapiLogExecution("DEBUG", "i_project ID : ", i_project);
        var i_oppid = context.getSetting('SCRIPT', 'custscript_opp_id');
        nlapiLogExecution("DEBUG", "opp ID : ", i_oppid);
        var i_user = context.getSetting('SCRIPT', 'custscript_user_id');
        nlapiLogExecution("DEBUG", "i_user : ", i_user);
		
		var frf_plan_id = context.getSetting('SCRIPT', 'custscript_frf_plan_id');
        nlapiLogExecution("DEBUG", "frf_plan_id : ", frf_plan_id);
		
        var filters = [];
        if (i_account)
            filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_ns_account', null, 'anyof', parseInt(i_account)));
        if (i_project) {
            filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_ns_project', null, 'anyof', parseInt(i_project)));
        } else if (i_oppid) {
            filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_opp_id', null, 'anyof', parseInt(i_oppid)));
        }
		
		if(_logValidation(frf_plan_id)){
		 filters.push(new nlobjSearchFilter('internalid', null, 'is', parseInt(frf_plan_id)));
		}

        filters.push(new nlobjSearchFilter('custrecord_frf_plan_details_confirmed', null, 'is', 'F'));
        var customrecord_frf_plan_detailsSearch = nlapiSearchRecord("customrecord_frf_plan_details", null, filters,
            [
                new nlobjSearchColumn("custrecord_frf_plan_details_opp_id")
            ]
        );
        if (customrecord_frf_plan_detailsSearch) {
            nlapiLogExecution('Debug', 'customrecord_frf_plan_detailsSearch Length ', customrecord_frf_plan_detailsSearch.length);
            for (var i_counter = 0; i_counter < customrecord_frf_plan_detailsSearch.length; i_counter++) {
                var i_recId = customrecord_frf_plan_detailsSearch[i_counter].getId();
                var recType = 'customrecord_frf_plan_details';
                nlapiLogExecution('Debug', 'i_recId ', i_recId);
                nlapiLogExecution('Debug', 'recType  ', recType);

                var o_RecObj = nlapiLoadRecord(recType, i_recId);
                var i_account = o_RecObj.getFieldValue('custrecord_frf_plan_details_ns_account');
                var accountName = o_RecObj.getFieldText('custrecord_frf_plan_details_ns_account');
                var i_project = o_RecObj.getFieldValue('custrecord_frf_plan_details_ns_project');
                var s_project = o_RecObj.getFieldText('custrecord_frf_plan_details_ns_project');
                var i_practice = o_RecObj.getFieldValue('custrecord_frf_plan_details_practice');
                var practiceName = o_RecObj.getFieldText('custrecord_frf_plan_details_practice');
                var i_emp_level = o_RecObj.getFieldValue('custrecord_frf_plan_details_emp_lvl');
                var i_project_role = o_RecObj.getFieldValue('custrecord_frf_plan_details_project_role');
                var b_bill_role = o_RecObj.getFieldValue('custrecord_frf_plan_details_bill_role');
                var i_requestType = o_RecObj.getFieldValue('custrecord_frf_plan_request_type');

                //------------------------------------------------------------------------------------------------------
                //prabhat gupta NIS-1723 04/09/2020 setting fitment type

                var i_fitmentType = o_RecObj.getFieldValue('custrecord_frf_plan_fitment_type');

                //-----------------------------------------------------------------------------------------------------

                //------------------------------------------------------------------------------------------------------
                //prabhat gupta NIS-1755 07/10/2020 setting level

                var i_level = o_RecObj.getFieldValue('custrecord_frf_plan_level');

                //-----------------------------------------------------------------------------------------------------

                //-----------------------------------------------------------------------------------------------
                //prabhat gupta

                var i_replacementEmp = o_RecObj.getFieldValues('custrecord_frf_plan_replacement_emp');
                //----------------------------------------------------------------------------------------------

                var i_positions = o_RecObj.getFieldValue('custrecord_frf_plan_details_positions');
                var i_location = o_RecObj.getFieldValue('custrecord_frf_plan_details_location');
                var i_family = o_RecObj.getFieldValue('custrecord_frf_plan_details_skill_family');
                var i_otherSkill = o_RecObj.getFieldValue('custrecord_frf_plan_details_other_skills');
                var d_startDate = o_RecObj.getFieldValue('custrecord_frf_plan_details_startdate');
                var d_endDate = o_RecObj.getFieldValue('custrecord_frf_plan_details_enddate');
                var i_allocation = o_RecObj.getFieldValue('custrecord_frf_plan_details_allocation');
                var i_dollar_loss = o_RecObj.getFieldValue('custrecord_frf_plan_dollar_loss');
                var b_critical_role = o_RecObj.getFieldValue('custrecord_frf_plan_details_criticalrole');
                var b_ext_hire = o_RecObj.getFieldValue('custrecord_frf_plan_details_externalhire');
                var i_bill_rate = o_RecObj.getFieldValue('custrecord_frf_plan_details_bill_rate');
                var i_suggested_resources = o_RecObj.getFieldValue('custrecord_frf_plan_details_suggest_res');
                var s_spl_requirment = o_RecObj.getFieldValue('custrecord_frf_plan_details_special_req');
                var i_opp_id = o_RecObj.getFieldValue('custrecord_frf_plan_details_opp_id');
                var i_opportunity = o_RecObj.getFieldValue("custrecord_frf_plan_details_opp_id");
                var i_skills = o_RecObj.getFieldValue('custrecord_frf_plan_details_primary');
                var i_resource_rotate = o_RecObj.getFieldValue("custrecord_frf_plan_details_emp_notice_r");
                var i_positionType = o_RecObj.getFieldValue('custrecord_frf_plan_details_position_typ');
                var b_backuprequired = o_RecObj.getFieldValue("custrecord_frf_pla_det_backup_require");
                var s_jobTitle = o_RecObj.getFieldValue("custrecord_frf_plan_job_title_rrf");
                var i_educationLevel = o_RecObj.getFieldValue("custrecord_frf_plan_edu_lvl_rrf");
                var i_educationProgram = o_RecObj.getFieldValue("custrecord_frf_plan_edu_pro_rrf");
                var i_empStatus = o_RecObj.getFieldValue("custrecord_frf_plan_emp_status_rrf");
                var s_firstInt = o_RecObj.getFieldValue("custrecord_frf_plan_first_inte_rrf");
                var s_secondInt = o_RecObj.getFieldValue("custrecord_frf_plan_sec_inte_rrf");
                var b_custInterviewr = o_RecObj.getFieldValue("custrecord_frf_plan_cus_inter_rrf");
                var s_intEmailId = o_RecObj.getFieldValue("custrecord_frf_plan_cust_inter_email_rrf");
                var s_instructions = o_RecObj.getFieldValue("custrecord_frf_plan_instruction_rrf");
                var s_jobDescription = o_RecObj.getFieldValue("custrecord_frf_plan_desc_rrf");
                var s_responsiblity = o_RecObj.getFieldValue("custrecord_frf_plan_res_rrf");
                var s_reason = o_RecObj.getFieldValue("custrecord_frf_plan_reason_rrf");
                var desired_start_date = o_RecObj.getFieldValue("custrecord_desired_start_date");
                if (s_project)
                    var projectName = s_project;
                else if (i_opportunity) {
                    var projectName = nlapiLookupField('customrecord_sfdc_opportunity_record', i_opportunity, 'custrecord_opportunity_name_sfdc');
                }

                if (_logValidation(i_skills)) {
                    i_skills = i_skills.toString();
                    nlapiLogExecution('debug', 'i_skills ', i_skills);
                    var arrSkills = getSkillIds(i_skills);
                    //arrSkills = arrSkills.toString();
                }
                if (_logValidation(i_otherSkill)) {
                    i_otherSkill = i_otherSkill.toString();
                    nlapiLogExecution('debug', 'i_otherSkill ', i_otherSkill);
                    var arrOtherSkills = getSkillIds(i_otherSkill);
                    //arrOtherSkills = arrOtherSkills.toString(); 
                }
				
				var account_wise_mailId = [];
				if(_logValidation(i_account)){
					var account_mail_search = getFuelAccountMapping(i_account);
					
                  if(_logValidation(account_mail_search)){
					for(var i=0; i<account_mail_search.length; i++){
						
						var mail = account_mail_search[i].getValue("email","custrecord_fuel_employee",null);
						
						if(_logValidation(mail)){
							account_wise_mailId.push(mail);
						}
						
					}
                }
					
				}
				

                var i_createdBy = o_RecObj.getFieldValue('custrecord_frf_plan_details_created_by');
                var d_date_created = o_RecObj.getFieldValue('custrecord_frf_plan_details_created_date');
				var s_skillCategory = o_RecObj.getFieldValue('custrecord_frf_plan_details_skill_class');
				var s_hireType= o_RecObj.getFieldValue('custrecord_frf_plan_details_hire_type');
                if (i_positions) {
                    i_positions = parseInt(i_positions)
                    nlapiLogExecution('Debug', 'i_positions  ', i_positions);
                    nlapiLogExecution('Debug', 'i_positions type  ', typeof(i_positions));
                         var redDate = nlapiDateToString(new Date());
					nlapiLogExecution('Debug', 'Red Date', redDate);	 
                    for (var xx = 0; xx < i_positions; xx++) {
                        nlapiLogExecution('Debug', 'recType  ', recType);
                        var o_frfRec = nlapiCreateRecord('customrecord_frf_details');
                        if (i_opp_id)
                            o_frfRec.setFieldValue('custrecord_frf_details_opp_id', i_opp_id);
                        if (i_recId)
                            o_frfRec.setFieldValue('custrecord_frf_details_parent', i_recId);
                        if (i_location)
                            o_frfRec.setFieldValue('custrecord_frf_details_res_location', i_location);
                        if (i_practice)
                            o_frfRec.setFieldValue('custrecord_frf_details_res_practice', i_practice);
                        if (i_emp_level)
                            o_frfRec.setFieldValue('custrecord_frf_details_emp_level', i_emp_level);
                        if (i_project_role)
                            o_frfRec.setFieldValue('custrecord_frf_details_role', i_project_role);
                        if (i_bill_rate)
                            o_frfRec.setFieldValue('custrecord_frf_details_bill_rate', i_bill_rate);
                        if (b_critical_role)
                            o_frfRec.setFieldValue('custrecord_frf_details_critical_role', b_critical_role);
                        if (i_family)
                            o_frfRec.setFieldValue('custrecord_frf_details_skill_family', i_family);
                        if (d_startDate)
                            o_frfRec.setFieldValue('custrecord_frf_details_start_date', d_startDate);
                        if (d_endDate)
                            o_frfRec.setFieldValue('custrecord_frf_details_end_date', d_endDate);
                        if (arrSkills)
                            o_frfRec.setFieldValues('custrecord_frf_details_primary_skills', arrSkills);
                        if (i_project)
                            o_frfRec.setFieldValue('custrecord_frf_details_project', i_project);
                        if (i_suggested_resources)
                            o_frfRec.setFieldValue('custrecord_frf_details_suggestion', i_suggested_resources);
                        if (i_resource_rotate)
                            o_frfRec.setFieldValue('custrecord_frf_emp_notice_rotation', i_resource_rotate);
                        if (b_bill_role)
                            o_frfRec.setFieldValue('custrecord_frf_details_billiable', b_bill_role);
                        if (i_account)
                            o_frfRec.setFieldValue('custrecord_frf_details_account', i_account);
                        if (i_requestType)
                            o_frfRec.setFieldValue('custrecord_frf_type', i_requestType);

                         if (redDate)
                            o_frfRec.setFieldValue('custrecord_frf_details_red_date', redDate);
                        //-------------------------------------------------------------------------------------
                        //prabhat gupta NIS-1723 04/09/2020 setting fitment type

                        if (i_fitmentType)
                            o_frfRec.setFieldValue('custrecord_frf_details_fitment_type', i_fitmentType);

                        //--------------------------------------------------------------------------------------

                        //-------------------------------------------------------------------------------------
                        //prabhat gupta NIS-1723 04/09/2020 setting fitment type

                        if (i_level)
                            o_frfRec.setFieldValue('custrecord_frf_details_level', i_level);

                        //------------------------------------------------------------------------------------
                        //prabhat gupta
                        if (i_replacementEmp) {
                            if (i_replacementEmp.length != 0) {

                                o_frfRec.setFieldValue('custrecord_frf_details_replacement_emp', i_replacementEmp[xx]);
                                nlapiLogExecution('Debug', 'replacing candidate ', i_replacementEmp[xx]);
                            }
                        }
                        //----------------------------------------------------------------------------------------


                        if (arrOtherSkills)
                            o_frfRec.setFieldValues('custrecord_frf_details_secondary_skills', arrOtherSkills);
                        if (i_allocation)
                            o_frfRec.setFieldValue('custrecord_frf_details_allocation', i_allocation);
                        if (i_dollar_loss)
                            o_frfRec.setFieldValue('custrecord_frf_details_dollar_loss', i_dollar_loss);
                        //o_frfRec.setFieldValue('custrecord_frf_details_suggest_pref_mat', '');//Need to check
                        if (s_spl_requirment)
                            o_frfRec.setFieldValue('custrecord_frf_details_special_req', s_spl_requirment);
                        if (i_createdBy)
                            o_frfRec.setFieldValue('custrecord_frf_details_created_by', i_createdBy);
                        if (b_ext_hire)
                            o_frfRec.setFieldValue('custrecord_frf_details_external_hire', b_ext_hire);
                        if (d_date_created)
                            o_frfRec.setFieldValue('custrecord_frf_details_created_date', d_date_created);
                        if (b_backuprequired)
                            o_frfRec.setFieldValue('custrecord_frf_details_backup_require', b_backuprequired);
                        if (s_jobTitle)
                            o_frfRec.setFieldValue('custrecord_frf_details_job_title', s_jobTitle);
                        if (i_educationLevel)
                            o_frfRec.setFieldValue('custrecord_frf_details_edu_lvl', i_educationLevel);
                        if (i_educationProgram)
                            o_frfRec.setFieldValue('custrecord_frf_details_edu_program', i_educationProgram);
                        if (i_empStatus)
                            o_frfRec.setFieldValue('custrecord_frf_details_emp_status', i_empStatus);
                        if (s_firstInt)
                            o_frfRec.setFieldValue('custrecord_frf_details_first_interviewer', s_firstInt);
                        if (s_secondInt)
                            o_frfRec.setFieldValue('custrecord_frf_details_sec_interviewer', s_secondInt);
                        if (b_custInterviewr)
                            o_frfRec.setFieldValue('custrecord_frf_details_cust_interview', b_custInterviewr);
                        if (s_intEmailId)
                            o_frfRec.setFieldValue('custrecord_frf_details_cust_inter_email', s_intEmailId);
                        if (s_instructions)
                            o_frfRec.setFieldValue('custrecord_frf_details_instruction_team', s_instructions);
                        if (s_jobDescription)
                            o_frfRec.setFieldValue('custrecord_frf_details_job_descriptions', s_jobDescription);
                        if (s_responsiblity)
                            o_frfRec.setFieldValue('custrecord_frf_details_responsible', s_responsiblity);
                        if (s_reason)
                            o_frfRec.setFieldValue('custrecord_frf_details_reason_external', s_reason);
                        if (i_positionType)
                            o_frfRec.setFieldValue('custrecord_frf_details_position_type', i_positionType);
                        if (desired_start_date)
                            o_frfRec.setFieldValue('custrecord_frf_desired_start_date', desired_start_date);
                        if (s_skillCategory)   
                            o_frfRec.setFieldValue('custrecord_frf_details_skill_class', s_skillCategory);  
							if (s_hireType)   
                            o_frfRec.setFieldValue('custrecord_frf_details_hire_type', s_hireType);
							var i_osfRec = nlapiSubmitRecord(o_frfRec, true, true);
                        nlapiLogExecution("AUDIT", "record created : ", i_osfRec);
                        if (i_osfRec) {
                            nlapiSubmitField("customrecord_frf_details", i_osfRec, "custrecord_frf_details_frf_number", getFRFNumber(i_osfRec));
                            nlapiSubmitField(recType, i_recId, "custrecord_frf_plan_details_confirmed", "T");
                            //var frfPlanId = nlapiSubmitRecord(o_RecObj);
                            var frfNumber = nlapiLookupField('customrecord_frf_details', i_osfRec, 'custrecord_frf_details_frf_number');
                            var is_delivery_practice = nlapiLookupField('department', i_practice, 'custrecord_is_delivery_practice');
                            //nlapiLogExecution("DEBUG", "is_delivery_practice : ", is_delivery_practice);
                            if (i_user) {
                                var records = new Array();
                                var userName = nlapiLookupField('employee', i_user, 'firstname');
                                var emailSubject = frfNumber + ': FUEL Notification: FRF created'
								
								var note = "";
								
								if(b_ext_hire == "T"){
									note = "<b>Please note, the Start Date as per Lead Time required by TA team to fulfill this position is </b>" + d_startDate;
								}else{
									note = "<b>Please note, the Start Date as per Lead Time required by RMG team to fulfill this position is </b>" + d_startDate;
								}
								
                                var emailBody = 'Dear ' + userName + ',' +
                                '<br> Below fulfillment request has been created in FUEL for Opportunity/Project: ' + projectName + ' and Account: ' + accountName + ' to be fulfilled by Practice: ' + practiceName + ' FRF Number: ' + frfNumber +
                                '<br> ' + note +
                                '<br>' +
                                '<br> Brillio-FUEL' +
                                '<br>This email was sent from a notification only address.' +
                                '<br>If you need further assistance, please write to fuel.support@brillio.com'
                                records['entity'] = ['94862', '41571'];
                                if (is_delivery_practice == "T" && b_bill_role == "F") {
	//---------------------------------------------------------------------------------------------------------
//prabhat gupta 01/12/2020	NIS-1864	
									var mail_id = ["sandeep.sayal@BRILLIO.COM"];
									
									if(_logValidation(account_wise_mailId)){
										
										account_wise_mailId.push("sandeep.sayal@BRILLIO.COM")
										mail_id = account_wise_mailId;
										
									}
									
	//-----------------------------------------------------------------------------------------------------------	
	
									
                                    nlapiSendEmail(154256, i_user, emailSubject, emailBody, mail_id , null, records);
                                } else {
                                    nlapiSendEmail(154256, i_user, emailSubject, emailBody, null, null, records);
                                }
                            }
                        }
                        if (b_ext_hire == "T") {
                            var taleoId = createTaleoExternalHireRecord(i_osfRec);
                            nlapiSubmitField("customrecord_frf_details", i_osfRec, "custrecord_frf_details_rrf_number", taleoId);
                        }
                    }
                }

            }
        }
    } catch (e) {
        nlapiLogExecution('Debug', 'Exception ', e);
        var subject = "Error while creating FRFs for opportuntiy##" + nlapiLookupField("customrecord_sfdc_opportunity_record", i_opp_id, "custrecord_opportunity_name_sfdc") + "/Project##" + o_RecObj.getFieldText('custrecord_frf_plan_details_ns_project');
        var s_body = "Dear Stakeholders" + "\n" +
            "Error occurs while creating FRFs for opportunity# " + nlapiLookupField("customrecord_sfdc_opportunity_record", i_opp_id, "custrecord_opportunity_name_sfdc") + "\n" + e + "\n" + "please contact." + "\n" + "\n" + "NetSuite HelpDesk";
        sendEmail(subject, s_body, i_createdBy);
    }
}

//========================================================================
function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function getSkillIds(s_skills) {
    var resultArray = new Array();
    if (_logValidation(s_skills)) {
        nlapiLogExecution('Debug', 's_skills in function', s_skills);
        var temp = s_skills.split('');
        for (var i = 0; i < temp.length; i++) {
            resultArray.push(temp[i]);
        }
    }
    nlapiLogExecution('Debug', 'resultArray ', resultArray);
    return resultArray;
}

function getFRFNumber(recId) {
    var str = "" + recId;
    var pad = "00000000";
    var s_number = pad.substring(0, pad.length - str.length) + str;
    return "F" + s_number;
}

function sendEmail(subject, body, createdBy) {
    var a_ccArray = new Array();
    a_ccArray.push('information.systems@brillio.com')
    var recordarray = new Array();
    recordarray['entity'] = nlapiGetUser();
    var author = 442;
    var recipient = createdBy;
    nlapiLogExecution('AUDIT', 'Recipient : ', recipient);
    nlapiSendEmail(author, recipient, subject, body, a_ccArray, null, recordarray);
}

function createTaleoExternalHireRecord(i_osfRec) {
    var taleoExternalHireRec = nlapiCreateRecord("customrecord_taleo_external_hire");
    taleoExternalHireRec.setFieldValue("custrecord_taleo_ext_hire_frf_details", i_osfRec);
    taleoExternalHireRec.setFieldValue("custrecord_taleo_ext_hire_status", "To Be Approved");
    return nlapiSubmitRecord(taleoExternalHireRec);
}
//-----------------------------------------------------------------------------------------------------------------
//prabhat gupta 01/12/2020	NIS-1864
function getFuelAccountMapping(i_account) {
	
	var customrecord_fuel_recruiterSearch = nlapiSearchRecord("customrecord_fuel_customer_wise_notify", null,
		[
			["custrecord_fuel_employee.custentity_employee_inactive", "is", "F"], "AND",
			["custrecord_fuel_customer", "anyof", i_account]
		],
		[
			new nlobjSearchColumn("internalid").setSort(true),
			new nlobjSearchColumn("email","custrecord_fuel_employee",null)
			
		]);
	return customrecord_fuel_recruiterSearch;
}
//-------------------------------------------------------------------------------------------------------------------