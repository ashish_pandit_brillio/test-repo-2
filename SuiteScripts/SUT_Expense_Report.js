/**
 * Expense Report
 * 
 * Version Date Author Remarks 1.00 Mar 26 2015 Nitish Mishra
 * 
 */

/**
 * @param {nlobjRequest}
 *        request Request object
 * @param {nlobjResponse}
 *        response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {

	try {

		if (request.getParameter("mode") == "Export") {
			exportExpenseReport(request);
		}
		else {
			createExpenseForm(request);
		}
	}
	catch (err) {
		displayErrorForm(err);
	}
}

function exportExpenseReport(request) {

	try {
		// get the values from the request
		var vertical = request.getParameter('custpage_vertical');
		var practice = request.getParameter('custpage_practice');
		var customer = request.getParameter('custpage_customer');
		var project = request.getParameter('custpage_project');
		var fromDate = request.getParameter('custpage_from_date');
		var toDate = request.getParameter('custpage_to_date');

		if (isEmpty(fromDate) || isEmpty(toDate)) {
			nlapiLogExecution('ERROR', 'Fields Missing');
			return;
		}

		var expenseDetails =
				getExpenseDetails(vertical, practice, customer, project, fromDate, toDate);

		var csvString =
				"Employee,Customer,End Customer,Project,Expense,Amount,Currency,Status,Practice,Vertical\n";

		expenseDetails.forEach(function(expense) {

			csvString +=
					expense.employee + "," + expense.customer + "," + expense.endcustomer + ","
							+ expense.project + "," + expense.tranid + "," + expense.amount + ","
							+ expense.currency + "," + expense.status + "," + expense.practice
							+ "," + expense.vertical + "\n";
		});


		nlapiLogExecution('debug', 'file created');

		var filename = 'Expense Details ' + new Date() + ".csv";
		var file = nlapiCreateFile(filename, 'CSV', csvString);
		response.setContentType('CSV', filename);
		response.write(file.getValue());
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'exportExpenseReport', err);
		throw err;
	}
}

function createExpenseForm(request) {

	try {
		var form = nlapiCreateForm('Expense Report', false);
		var report_access = getUserReportAccessList();

		// check deployment
		var context = nlapiGetContext();
		var deploymentId = context.getDeploymentId();
		form.addField('custpage_deployment', 'text', 'Deployement').setDisplayType('hidden')
				.setDefaultValue(deploymentId);

		// add from date
		var field_from_date = form.addField('custpage_from_date', 'date', 'From Date : ');
		field_from_date.setMandatory(true);

		// add to date
		var field_to_date = form.addField('custpage_to_date', 'date', 'To Date : ');
		field_to_date.setMandatory(true);

		var field_practice = null;
		var field_vertical = null;
		var field_customer = null;
		var field_project = null;

		switch (deploymentId) {

			case 'customdeploy_sut_expense_rep_vertical':
				field_practice =
						form.addField('custpage_practice', 'select', 'Practice', 'department');

				field_vertical = form.addField('custpage_vertical', 'select', 'Vertical');
				createDropdown(report_access.VerticalList.Values, report_access.VerticalList.Texts)
						.forEach(
								function(vertical) {

									field_vertical.addSelectOption(vertical.Value, vertical.Text,
											vertical.IsSelected);
								});

				field_customer =
						form.addField('custpage_customer', 'select', 'Customer', 'customer');
			break;

			case 'customdeploy_sut_expense_rep_practice':
				field_practice = form.addField('custpage_practice', 'select', 'Practice');
				createDropdown(report_access.PracticeList.Values, report_access.PracticeList.Texts)
						.forEach(
								function(department) {

									field_practice.addSelectOption(department.Value,
											department.Text, department.IsSelected);
								});

				field_vertical =
						form.addField('custpage_vertical', 'select', 'Vertical', 'classification');

				field_customer =
						form.addField('custpage_customer', 'select', 'Customer', 'customer');
			break;

			case 'customdeploy_sut_expense_rep_customer':
				if (isArrayEmpty(report_access.CustomerList)) {
					throw "No Customer Specified";
				}

				// add the filtered practice list
				field_practice = form.addField('custpage_practice', 'select', 'Practice');

				var practiceList = [];
				report_access.CustomerList.forEach(function(cust) {

					cust.Practice.forEach(function(prac) {

						practiceList.push(prac);
					});
				});

				nlapiSearchRecord('department', null,
						new nlobjSearchFilter('internalid', null, 'anyof', practiceList),
						new nlobjSearchColumn('name')).forEach(function(dept) {

					field_practice.addSelectOption(dept.getId(), dept.getValue('name'), false);
				});

				field_practice.addSelectOption('', '', true);
				field_practice.setMandatory(true);

				// add the vertical list
				field_vertical =
						form.addField('custpage_vertical', 'select', 'Vertical', 'classification');

				// add the customers list
				field_customer = form.addField('custpage_customer', 'select', 'Customer');

				report_access.CustomerList.forEach(function(cust) {

					field_customer.addSelectOption(cust.CustomerValue, cust.CustomerText, false);
				});

				field_customer.addSelectOption('', '', true);
				field_customer.setMandatory(true);
			break;
		}

		// add the project field
		field_project = form.addField('custpage_project', 'select', 'Project', 'job');

		// get the values from the request
		var vertical = request.getParameter('custpage_vertical');
		var practice = request.getParameter('custpage_practice');
		var customer = request.getParameter('custpage_customer');
		var project = request.getParameter('custpage_project');
		var fromDate = request.getParameter('custpage_from_date');
		var toDate = request.getParameter('custpage_to_date');

		// set the default value in the drop down
		if (isNotEmpty(vertical)) {
			field_vertical.setDefaultValue(vertical);
		}

		if (isNotEmpty(practice)) {
			field_practice.setDefaultValue(practice);
		}

		if (isNotEmpty(customer)) {
			field_customer.setDefaultValue(customer);
		}

		if (isNotEmpty(project)) {
			field_project.setDefaultValue(project);
		}

		if (isNotEmpty(fromDate)) {
			field_from_date.setDefaultValue(fromDate);
		}

		if (isNotEmpty(toDate)) {
			field_to_date.setDefaultValue(toDate);
		}

		if (request.getMethod() == "POST") {
			var expenseDetails =
					getExpenseDetails(vertical, practice, customer, project, fromDate, toDate);
			var expense_sublist = form.addSubList('custpage_expenses', 'list', 'Expenses');
			expense_sublist.addField('employee', 'text', 'Employee');
			expense_sublist.addField('customer', 'text', 'Customer');
			expense_sublist.addField('endcustomer', 'text', 'End Customer');
			expense_sublist.addField('project', 'text', 'Project');
			expense_sublist.addField('tranid', 'text', 'Expense');
			expense_sublist.addField('amount', 'text', 'Amount');
			expense_sublist.addField('currency', 'text', 'Currency');
			expense_sublist.addField('status', 'text', 'Status');
			expense_sublist.addField('practice', 'text', 'Practice');
			expense_sublist.addField('vertical', 'text', 'Vertical');
			expense_sublist.setLineItemValues(expenseDetails);

			// add the export to CSV button
			form.addButton('custpage_export_csv', 'Export To CSV', 'exportToCsv');
		}

		form.addSubmitButton('Get Expenses');
		form.setScript('customscript_cs_sut_expense_report');
		response.writePage(form);
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'createExpenseForm', err);
		throw err;
	}
}

function getExpenseDetails(vertical, practice, customer, project, fromDate, toDate) {

	try {
		var customerName = null;
		var filters =
				[
					new nlobjSearchFilter('mainline', null, 'is', 'F'),
					new nlobjSearchFilter('type', null, 'anyof', 'ExpRept'),
					new nlobjSearchFilter('status', null, 'anyof', [
						'ExpRept:C', 'ExpRept:B', 'ExpRept:F', 'ExpRept:I', 'ExpRept:G'
					]),
					new nlobjSearchFilter('custbody_transactiondate', null, 'within', fromDate,
							toDate)
				];

		var deploymentId = nlapiGetContext().getDeploymentId();
		var report_access = getUserReportAccessList();

		switch (deploymentId) {

			case "customdeploy_sut_expense_rep_vertical":
				filters.push(new nlobjSearchFilter('class', null, 'anyof',
						report_access.VerticalList.Values));
			break;

			case "customdeploy_sut_expense_rep_practice":
				filters.push(new nlobjSearchFilter('department', null, 'anyof',
						report_access.PracticeList.Values));
			break;

			case "customdeploy_sut_expense_rep_customer":

				// add a check to see if the employee is
				// allowed to access the specified customer
				// with the specified practice
				var customerList = report_access.CustomerList;
				var allowAccess = false;

				for (var i = 0; i < customerList.length; i++) {

					if (customerList[i].CustomerValue == customer) {
						customerName = customerList[i].CustomerText;
						var practiceList = customerList[i].Practice;

						for (var j = 0; j < practiceList.length; j++) {

							if (practiceList[j] == practice) {
								allowAccess = true;
								break;
							}
						}
						break;
					}
				}

				if (!allowAccess) {
					throw "Access not allowed";
				}
			break;
		}


		// department filtering
		if (isNotEmpty(practice)) {
			filters.push(new nlobjSearchFilter('department', null, 'anyof',
					searchSubDepartments(practice)));
		}

		// project filtering
		// if (isNotEmpty(i_project_filter)) {
		// filters[filters.length] =
		// new nlobjSearchFilter('project', null, 'anyof', i_project_filter);
		// }

		// customer filtering
		if (isNotEmpty(customer)) {

			if (isEmpty(customerName)) {
				// get the customer name using search
				var customerDetails = nlapiLookupField('customer', customer, [
					'entityid', 'companyname'
				]);

				customerName = customerDetails.entityid + ' ' + customerDetails.companyname;
			}

			// add customer filter
			var customer_filter_2 = "case when {custcolcustcol_temp_customer} = any(";
			customer_filter_2 += "'" + customerName + "'";
			customer_filter_2 += ") then 'T' else 'F' end";
			nlapiLogExecution('debug', 'customer_filter_2', customer_filter_2);

			filters.push(new nlobjSearchFilter('formulatext', null, 'is', 'T')
					.setFormula(customer_filter_2));
		}

		// vertical filtering
		if (isNotEmpty(vertical)) {
			filters.push(new nlobjSearchFilter('class', null, 'anyof', vertical));
		}

		// search the expense record
		var expenses = [];
		var expenseDetails = [];

		searchRecord(
				'expensereport',
				null,
				filters,
				[
					new nlobjSearchColumn('fxamount', null, 'sum'),
					new nlobjSearchColumn('name', null, 'group'),
					new nlobjSearchColumn('class', null, 'group'),
					new nlobjSearchColumn('department', null, 'group'),
					new nlobjSearchColumn('currency', null, 'group'),
					new nlobjSearchColumn('mainname', null, 'group'),
					new nlobjSearchColumn('transactionnumber', null, 'group'),
					new nlobjSearchColumn('statusref', null, 'group'),
					new nlobjSearchColumn('custcolcustcol_temp_customer', null, 'group'),
					new nlobjSearchColumn('custentity_endcustomer', 'job', 'group'),
					new nlobjSearchColumn('internalid', null, 'group').setSort()
				]).forEach(function(expense) {

			expenses.push(expense.getValue('internalid', null, 'group'));
			expenseDetails.push({
				employee : expense.getText('mainname', null, 'group'),
				project : expense.getText('name', null, 'group'),
				tranid : expense.getValue('transactionnumber', null, 'group'),
				internalid : expense.getValue('internalid', null, 'group'),
				status : expense.getText('statusref', null, 'group'),
				amount : expense.getValue('fxamount', null, 'sum'),
				currency : expense.getText('currency', null, 'group'),
				vertical : expense.getText('class', null, 'group'),
				practice : expense.getText('department', null, 'group'),
				customer : expense.getValue('custcolcustcol_temp_customer', null, 'group'),
				endcustomer : expense.getText('custentity_endcustomer', 'job', 'group')
			});
		});

		nlapiLogExecution('debug', 'expense length A', expenseDetails.length);
		return expenseDetails;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseDetails', err);
		throw err;
	}
}

function searchSubDepartments(i_department_id) {

	try {
		var s_department_name = nlapiLookupField('department', i_department_id, 'name');

		var filters = new Array();
		if (s_department_name.indexOf(' : ') == -1) {
			filters[0] = new nlobjSearchFilter('formulatext', null, 'is', s_department_name);
			filters[0]
					.setFormula('TRIM(CASE WHEN INSTR({name} , \' : \', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , \' : \', 1)) ELSE {name} END)');
		}
		else {
			filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', i_department_id);
		}
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');

		var search_sub_departments = nlapiSearchRecord('department', null, filters, columns);

		var a_sub_departments = new Array();

		for (var i = 0; i < search_sub_departments.length; i++) {
			a_sub_departments.push(search_sub_departments[i].getValue('internalid'));
		}

		return a_sub_departments;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'searchSubDepartments', err);
		throw err;
	}
}

function exportToCsv() {

	try {
		// read the selections
		var vertical = nlapiGetFieldValue('custpage_vertical');
		var practice = nlapiGetFieldValue('custpage_practice');
		var project = nlapiGetFieldValue('custpage_project');
		var customer = nlapiGetFieldValue('custpage_customer');
		var from_date = nlapiGetFieldValue('custpage_from_date');
		var to_date = nlapiGetFieldValue('custpage_to_date');
		var deployment = nlapiGetFieldValue('custpage_deployment');

		var csvExportUrl =
				nlapiResolveURL('SUITELET', 'customscript_sut_expense_report', deployment);
		var link =
				csvExportUrl + "&custpage_vertical=" + vertical + "&custpage_practice=" + practice
						+ "&custpage_project=" + project + "&custpage_customer=" + customer
						+ "&custpage_from_date=" + from_date + "&custpage_to_date=" + to_date
						+ "&mode=Export";

		var win = window.open(link);
		win.focus();
	}
	catch (err) {
		alert(JSON.stringify(err));
	}
}