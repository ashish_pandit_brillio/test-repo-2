////////////original script for produnction 
  /*
   * Script Name: VendorBill_UES.js
   
       
     Script Modification Log:
     
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     1 Sep 2014          Shweta Chopde                     Vaibhav  
   * 
   */
   
function rounding_off_on_save(type){
if(type == 'create' || type == 'edit'){
var amt = nlapiGetFieldValue('usertotal');
var amount_round_off = Math.round(amt);
						nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
nlapiSetFieldValue('usertotal',amount_round_off);	
}  
} 
function VendorBill_ValidateLine(type)
{
	if(type == 'item')
	{
		var Temp_Project = nlapiGetCurrentLineItemValue('item', 'custpage_temp_expproject');
		if(Temp_Project != null && Temp_Project.length > 0)
		{
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=P&projid=' + Temp_Project, null, null );
			var splitvalues = response.getBody().split(":");
			//nlapiSetCurrentLineItemValue('item', 'department', splitvalues[1]);
			nlapiSetCurrentLineItemValue('item', 'class', splitvalues[2]);
			nlapiSetCurrentLineItemValue('item', 'custcol_temp_project', Temp_Project); 
		}
	}

	if(type == 'expense')
	{
		var Temp_Project = nlapiGetCurrentLineItemValue('expense', 'custpage_temp_expproject');
		if(Temp_Project != null && Temp_Project.length > 0)
		{
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=P&projid=' + Temp_Project, null, null );
			var splitvalues = response.getBody().split(":");
			//nlapiSetCurrentLineItemValue('expense', 'department', splitvalues[1]);
			nlapiSetCurrentLineItemValue('expense', 'class', splitvalues[2]);
			nlapiSetCurrentLineItemValue('expense', 'custcol_temp_project', Temp_Project); 
		}
	}
	
	if(type == 'line')
	{
		var Temp_Project = nlapiGetCurrentLineItemValue('line', 'custpage_temp_expproject');
		if(Temp_Project != null && Temp_Project.length > 0)
		{
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fetchvalue_timesheet', 'customdeploy_fetchvalue_timesheet') + '&type=P&projid=' + Temp_Project, null, null );
			var splitvalues = response.getBody().split(":");
			//nlapiSetCurrentLineItemValue('line', 'department', splitvalues[1]);
			nlapiSetCurrentLineItemValue('line', 'class', splitvalues[2]);
			nlapiSetCurrentLineItemValue('line', 'custcol_temp_project', Temp_Project); 
		}
	}
	
	return true;
}


function page_init_disable(type)
{

try{
var  userID = nlapiGetUser();
if(userID == 39108){
return true;
}
var recType = nlapiGetRecordType();
if( recType == 'journalentry'){
if (type == 'copy'){
var newId = nlapiGetRecordId();
var newType = nlapiGetRecordType();
var lineCount = nlapiGetLineItemCount('line');
nlapiLogExecution('DEBUG','lineCount',lineCount);

for(var t=1;t<=lineCount;t++){

nlapiSelectLineItem('line',t);

nlapiSetCurrentLineItemValue('line', 'department', '');
nlapiSetCurrentLineItemValue('line', 'department_display', '');

nlapiSetCurrentLineItemValue('line', 'custcol_employee_type', '');
nlapiSetCurrentLineItemValue('line', 'custcol_onsite_offsite', '');
nlapiSetCurrentLineItemValue('line', 'custcol_employeenamecolumn', '');
nlapiSetCurrentLineItemValue('line', 'custcol_emp_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('line', 'custcol_person_type', '');

nlapiSetCurrentLineItemValue('line', 'custcol_territory', '');


nlapiSetCurrentLineItemValue('line', 'custcolprj_name', '');
nlapiSetCurrentLineItemValue('line', 'custcol_customer_entityid', '');

nlapiSetCurrentLineItemValue('line', 'custcol_region_master_setup', '');

nlapiSetCurrentLineItemValue('line', 'custcol_billing_type', '');
nlapiSetCurrentLineItemValue('line', 'custcol_cust_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('line', 'custcol_proj_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('line', 'custcol_proj_category_on_a_click', '');
nlapiSetCurrentLineItemValue('line', 'custcol_territory', '');
nlapiSetCurrentLineItemValue('line', 'custcol_je_region', '');
nlapiSetCurrentLineItemValue('line', 'custcol_territory', '');  
nlapiSetCurrentLineItemValue('line', 'custcolcustcol_temp_customer', ''); 

// commit the line to the database

nlapiCommitLineItem('line');

}
var setFildValue = nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');
nlapiLogExecution('DEBUG','<Before Load Script> type:'+type+', RecordType: '+newType+', Id:'+newId);
}
}
//Credit memo
if( recType == 'creditmemo'){
if (type == 'copy' || type =='create' ||type == 'edit'){
var newId = nlapiGetRecordId();
var newType = nlapiGetRecordType();
var lineCount = nlapiGetLineItemCount('item');
nlapiLogExecution('DEBUG','lineCount',lineCount);

for(var t=1;t<=lineCount;t++){

nlapiSelectLineItem('item',t);

nlapiSetCurrentLineItemValue('item', 'department', '');
nlapiSetCurrentLineItemValue('item', 'department_display', '');

nlapiSetCurrentLineItemValue('item', 'custcol_employee_type', '');
nlapiSetCurrentLineItemValue('item', 'custcol_onsite_offsite', '');
nlapiSetCurrentLineItemValue('item', 'custcol_employeenamecolumn', '');
nlapiSetCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('item', 'custcol_person_type', '');

nlapiSetCurrentLineItemValue('item', 'custcol_territory', '');


nlapiSetCurrentLineItemValue('item', 'custcolprj_name', '');
nlapiSetCurrentLineItemValue('item', 'custcol_customer_entityid', '');

nlapiSetCurrentLineItemValue('item', 'custcol_region_master_setup', '');

nlapiSetCurrentLineItemValue('item', 'custcol_billing_type', '');
nlapiSetCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', '');
nlapiSetCurrentLineItemValue('item', 'custcol_territory', '');
nlapiSetCurrentLineItemValue('item', 'custcol_je_region', '');
nlapiSetCurrentLineItemValue('item', 'custcol_territory', '');  
nlapiSetCurrentLineItemValue('item', 'custcolcustcol_temp_customer', ''); 

// commit the line to the database

nlapiCommitLineItem('item');

}

nlapiLogExecution('DEBUG','<Before Load Script> type:'+type+', RecordType: '+newType+', Id:'+newId);
}

}
if(recType == 'vendorbill' || recType == 'check' || recType == 'vendorcredit'){
if (type == 'copy' || type =='create' ||type == 'edit'){
var newId = nlapiGetRecordId();
var newType = nlapiGetRecordType();
var lineCount = nlapiGetLineItemCount('expense');
nlapiLogExecution('DEBUG','lineCount',lineCount);
var lineCount_i = nlapiGetLineItemCount('item');
nlapiLogExecution('DEBUG','lineCount_i',lineCount_i);
for(var t=1;t<=lineCount;t++){

nlapiSelectLineItem('expense',t);

nlapiSetCurrentLineItemValue('expense', 'department', '');
nlapiSetCurrentLineItemValue('expense', 'department_display', '');

nlapiSetCurrentLineItemValue('expense', 'custcol_employee_type', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_onsite_offsite', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_employeenamecolumn', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_emp_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_person_type', '');

nlapiSetCurrentLineItemValue('expense', 'custcol_territory', '');


nlapiSetCurrentLineItemValue('expense', 'custcolprj_name', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_customer_entityid', '');

nlapiSetCurrentLineItemValue('expense', 'custcol_region_master_setup', '');

nlapiSetCurrentLineItemValue('expense', 'custcol_billing_type', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_cust_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_proj_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_proj_category_on_a_click', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_territory', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_je_region', '');
nlapiSetCurrentLineItemValue('expense', 'custcol_territory', '');  
nlapiSetCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', ''); 

// commit the line to the database

nlapiCommitLineItem('expense');

}
for(var q=1;q<=lineCount_i;q++){

nlapiSelectLineItem('item',q);

nlapiSetCurrentLineItemValue('item', 'department', '');
nlapiSetCurrentLineItemValue('item', 'department_display', '');

nlapiSetCurrentLineItemValue('item', 'custcol_employee_type', '');
nlapiSetCurrentLineItemValue('item', 'custcol_onsite_offsite', '');
nlapiSetCurrentLineItemValue('item', 'custcol_employeenamecolumn', '');
nlapiSetCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('item', 'custcol_person_type', '');

nlapiSetCurrentLineItemValue('item', 'custcol_territory', '');


nlapiSetCurrentLineItemValue('item', 'custcolprj_name', '');
nlapiSetCurrentLineItemValue('item', 'custcol_customer_entityid', '');

nlapiSetCurrentLineItemValue('item', 'custcol_region_master_setup', '');

nlapiSetCurrentLineItemValue('item', 'custcol_billing_type', '');
nlapiSetCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', '');
nlapiSetCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', '');
nlapiSetCurrentLineItemValue('item', 'custcol_territory', '');
nlapiSetCurrentLineItemValue('item', 'custcol_je_region', '');
nlapiSetCurrentLineItemValue('item', 'custcol_territory', '');  
nlapiSetCurrentLineItemValue('item', 'custcolcustcol_temp_customer', ''); 
// commit the line to the database

nlapiCommitLineItem('item');

}
  var setFildValue = nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');
nlapiLogExecution('DEBUG','<Before Load Script> type:'+type+', RecordType: '+newType+', Id:'+newId);
}
}
}
catch(e){
nlapiLogExecution('debug','Error in Before load',e);
}
	//nlapiDisableLineItemField('line','custcolprj_name',true)
	//nlapiDisableLineItemField('line','custcolcustcol_temp_customer',true)
		
	//nlapiDisableLineItemField('expense','custcolprj_name',true)
	//nlapiDisableLineItemField('expense','custcolcustcol_temp_customer',true)
		
	//nlapiDisableLineItemField('item','custcolprj_name',true)
	//nlapiDisableLineItemField('item','custcolcustcol_temp_customer',true)

		
}
function field_change_project_source(type, name, linenum)
{	
  if(name == 'custpage_temp_expproject' || name == 'custpage_temp_expemp' )
  {
  	//var i_projectID = nlapiGetCurrentLineItemValue(type,'custpage_temp_expproject');
	var i_projectID = (nlapiGetCurrentLineItemValue(type,'custpage_temp_expproject') == null) ? "" : nlapiGetCurrentLineItemValue(type,'custpage_temp_expproject');
	var i_index = nlapiGetCurrentLineItemIndex(type)
    var i_employeeID = (nlapiGetCurrentLineItemValue(type,'custpage_temp_expemp') == null) ? "" : nlapiGetCurrentLineItemValue(type,'custpage_temp_expemp');


//----------------------added by swati------------------------------------------------------------------------------
	if(name == 'custpage_temp_expemp')
	{
	  var i_employeeID_name = (nlapiGetCurrentLineItemText(type,'custpage_temp_expemp') == null) ? "" : nlapiGetCurrentLineItemText(type,'custpage_temp_expemp');
	  //alert('i_employeeID_name -->'+i_employeeID_name)
	  nlapiSetCurrentLineItemValue(type,'custcol_employeenamecolumn',i_employeeID_name);
	}
	//------------------------------------------------------------------------------------------------------------------
	
   if(_logValidation(i_projectID) || _logValidation(i_employeeID))
   {   	
	 var a = new Array();
     a['User-Agent-x'] = 'SuiteScript-Call';
	  
	 var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=277&deploy=1&custscript_project_id_s=' + i_projectID +'&custscript_employee_id='+i_employeeID, null, a);
  	                    
	 var a_return_array = resposeObject.getBody();
	 
	 var a_TR_array_values = new Array()	
	 var i_data_TR = new Array()
	 i_data_TR =  a_return_array;
 
      for(var dt=0;dt<i_data_TR.length;dt++)
	  {
		 	a_TR_array_values = i_data_TR.split(',')
		    break;				
	  }	
	    var a_split_array = new Array();
		if(_logValidation(a_TR_array_values))
		{
			a_split_array = a_TR_array_values[0].split('&&%%%**')
						
			var s_project_description = a_split_array[0];
			var i_customer_name = a_split_array[1];
			var i_practiceID = a_split_array[2];
			var i_vertical_head = a_split_array[3];
			var i_territory = a_split_array[4];
			
			//alert(i_practiceID);
			if (_logValidation(i_customer_name)) 
			{
				nlapiSetCurrentLineItemValue(type,'custcolcustcol_temp_customer',i_customer_name,true,true);
			}

			if(i_territory != null && i_territory != 'null' && i_territory != '')
			{
				nlapiSetCurrentLineItemValue(type,'custcol_territory',i_territory,true,true);
			}
			
			
			if (i_practiceID == null || i_practiceID == 'null' || i_practiceID == '') 
			{
				alert('For that employee,practice is not define.');
			}
			else
			{
				nlapiSetCurrentLineItemValue(type,'department',i_practiceID,true,true)
			}
						
			nlapiSetCurrentLineItemValue(type,'custcolprj_name',s_project_description,true,true)
			//nlapiSetCurrentLineItemValue(type,'custcolcustcol_temp_customer',i_customer_name,true,true)
			
			nlapiSetCurrentLineItemValue(type,'class',i_vertical_head,true,true)
		}
	}//Project ID	
  }//Project	
}//Field Change

function VendorBill_BeforeLoad(type, form)
{
 
	form.setScript('customscript_vendorbill_sourcing');
		//Add field for Item Tab
		var ItemTab = form.getSubList('item');		
		
		if(form.getSubList('item') != null)
		{  //Add field for Expense Tab
			var ExpenseTab = form.getSubList('item');
			var ExpenseProjectField = ExpenseTab.addField('custpage_temp_expproject', 'select', 'Project V', 'job');
			ExpenseProjectField.setDisplayType('hidden');
			//-------------added by swati------------------------------------------------------------------------------
			var ExpenseProjectField1 = ExpenseTab.addField('custpage_temp_expemp', 'select', 'Employee', 'employee');
			ExpenseProjectField1.setDisplayType('hidden');
		}
		if(form.getSubList('expense') != null)
		{
			//Add field for Expense Tab
			var ExpenseTab = form.getSubList('expense');
			var ExpenseProjectField = ExpenseTab.addField('custpage_temp_expproject', 'select', 'Project V', 'job');
			ExpenseProjectField.setDisplayType('hidden');
			//-------------added by swati------------------------------------------------------------------------------
			var ExpenseProjectField1 = ExpenseTab.addField('custpage_temp_expemp', 'select', 'Employee', 'employee');
			ExpenseProjectField1.setDisplayType('hidden');	
		}
		if(form.getSubList('line') != null)
		{
			//Add field for Expense Tab
			var ExpenseTab = form.getSubList('line');
			var ExpenseProjectField = ExpenseTab.addField('custpage_temp_expproject', 'select', 'Project V', 'job');
			ExpenseProjectField.setDisplayType('hidden');
			//-------------added by swati------------------------------------------------------------------------------
			var ExpenseProjectField1 = ExpenseTab.addField('custpage_temp_expemp', 'select', 'Employee', 'employee');
			ExpenseProjectField1.setDisplayType('hidden');
		}
}

/*

function after_submit_project_sourcing(type)
{	
   try
   {   		
	var i_customer = '';
	var s_project_description = '';
   	
	var i_recordID = nlapiGetRecordId();	
	var s_record_type = nlapiGetRecordType();
	
	nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Record ID -->' + i_recordID);
	nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Record Type -->' + s_record_type);
				
//	if(s_record_type == 'expensereport' || s_record_type == 'salesorder' || s_record_type == 'invoice')
	{
		if(_logValidation(i_recordID)&&_logValidation(s_record_type))
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);
			
			if(_logValidation(o_recordOBJ))
			{				
			    var s_line = 'expense';			
					
				var i_line_count = o_recordOBJ.getLineItemCount(s_line)
				 nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Line_count -->' + i_line_count);
						
				if(_logValidation(i_line_count))
				{
					for(var i=1;i<=i_line_count;i++)
					{						
				//	if(_logValidation(i_projectID))
					{						
						var i_projectID = o_recordOBJ.getLineItemValue(s_line,'custpage_temp_expproject',i);
					    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project ID -->' + i_projectID);
											
						var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
						
						if(_logValidation(o_projectOBJ))
						{
							var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);
								
							var i_project_name =  o_projectOBJ.getFieldValue('companyname');
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);
							
							s_project_description = i_project_name_ID+' '+i_project_name;
							nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);
							
							i_customer =  o_projectOBJ.getFieldText('parent');
						    nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);
						
							
						}//Project OBJ				
					}//Project ID	
												
						o_recordOBJ.setLineItemValue(s_line,'custcolcustcol_temp_customer',i,i_customer);
						o_recordOBJ.setLineItemValue(s_line,'custcolprj_name',i,s_project_description);
						
					}//Loop				
				}//Line Count
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' ***************** Submit ID ****************-->' + i_submitID);
						
							
			}//Record OBJ		
		}//Record ID & Record Type 
	
	}//Record Type - Sales Order / Invoice / Expense Report			
				
	if(s_record_type == 'timebill')
	{
		if (_logValidation(i_recordID) && _logValidation(s_record_type)) 
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID);
			
			if(_logValidation(o_recordOBJ))
			{
			  	var i_projectID = o_recordOBJ.getFieldValue('customer');
				nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project ID -->' + i_projectID);
				
			    var o_projectOBJ = nlapiLoadRecord('job',i_projectID);
				
				if(_logValidation(o_projectOBJ))
				{
					var i_project_name_ID =  o_projectOBJ.getFieldValue('entityid');
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name ID -->' + i_project_name_ID);
						
					var i_project_name =  o_projectOBJ.getFieldValue('companyname');
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Name -->' + i_project_name);
					
					var s_project_description = i_project_name_ID+' '+i_project_name;
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Project Description -->' + s_project_description);
					
					var i_customer =  o_projectOBJ.getFieldText('parent');
					nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' Customer-->' + i_customer);
					
					
					o_recordOBJ.setFieldValue('custcolprj_name',s_project_description);
					o_recordOBJ.setFieldValue('custcolcustcol_temp_customer',i_customer);
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				   nlapiLogExecution('DEBUG', 'afterSubmit_cross_project', ' ***************** Submit ID ****************-->' + i_submitID);
				
					
				}//Project OBJ				
				
			}//Record OBJ			
		}//Record ID & Record Type		
	}//Timesheet			  	
   }
   catch(exception)
   {
	  nlapiLogExecution('DEBUG','ERROR','  Exception Caught -->'+exception);	
   }
	return true;		
}
*/
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}