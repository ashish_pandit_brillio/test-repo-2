/**
 * @author Shweta
 */

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Approve_Expense.js
	Author      : Shweta Chopde
	Date        : 28 May 2014
	Description : Create a Expense Entry Report


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var internal_ID;
	var d_date;
	var i_employee;
	var i_employee_txt;
	var i_customer;
	var i_item;
	var i_note;
	var i_duration;
	var i_type;
	var i_approver = '';
	var i_cnt = 0;
	var i_ent = 0;
	var i_ant = 0;
	var i_ref_no;
	var i_amount;
	var i_billable_s;
	var i_status_s;
	var i_item;
	var i_currency_s;
	var i_billable_s;	
	var i_currency_s;
	var i_employee_txt;
	var i_memo_s;
	
	var a_employee_array = new Array();
	try
	{
		if (request.getMethod() == 'GET') 
		{
			var i_context = nlapiGetContext();
		
			var i_usage_begin = i_context.getRemainingUsage();
		    nlapiLogExecution('DEBUG', 'suiteletFunction','Usage Begin -->' + i_usage_begin);
			
			var d_as_on_date_prm =  request.getParameter('custscript_as_on_date_e');
	        var i_employee_prm =  request.getParameter('custscript_employee_expense');
		
		    nlapiLogExecution('DEBUG', 'suiteletFunction',' As On Date -->' + d_as_on_date_prm);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Employee -->' + i_employee_prm);	
			
			var i_current_user =  nlapiGetUser();
		
			var f_form = nlapiCreateForm(" EXPENSE REPORT PENDING FOR APPROVAL ");
				
			f_form.setScript('customscript_cli_approve_expense');
				
			var i_data_f =  f_form.addField('custpage_data', 'textarea', 'Data Array').setDisplayType('hidden');
		   
		    var i_current_user_f =  f_form.addField('custpage_current_user', 'text', 'User').setDisplayType('hidden');
		    i_current_user_f.setDefaultValue(i_current_user)
								
			var f_approve_fld =  f_form.addField('custpage_select_approve_chk', 'text', 'Approve').setDisplayType('hidden');
			
			var f_reject_fld =  f_form.addField('custpage_select_reject_chk', 'text', 'Reject').setDisplayType('hidden');
						
			var i_as_on_date = f_form.addField('custpage_as_on_date', 'date', 'As on Date')
			i_as_on_date.setMandatory(true)
					
			var i_employee_f_1 = f_form.addField('custpage_employee','select', ' Employee');
			i_employee_f_1.addSelectOption('-All-','-All-');
		 	i_employee_f_1.setMandatory(true)
			i_employee_f_1.setLayoutType('normal','startrow')
			
			
			if(_logValidation(d_as_on_date_prm))
			{
				i_as_on_date.setDefaultValue(d_as_on_date_prm)
			}
			else
			{
				i_as_on_date.setDefaultValue(get_todays_date())
			}

	         if(_logValidation(i_employee_prm))
			 {
			 	i_employee_f_1.setDefaultValue(i_employee_prm)
			 }
			 else
			 {
				i_employee_f_1.setDefaultValue('-All-')
			 }		
			
			
						
			var i_current_user = nlapiGetUser();
			
			var i_time_expense_entries_tab = f_form.addSubList('custpage_time_expense_sublist', 'list', 'Time / Expense Entries', 'custpage_time_entries_tab');
	
			
			var i_select_approve_f = i_time_expense_entries_tab.addField('custpage_select_approve', 'checkbox', 'Approve');
			var i_select_reject_f = i_time_expense_entries_tab.addField('custpage_select_reject', 'checkbox', 'Reject');		
			var i_S_No_f= i_time_expense_entries_tab.addField('custpage_s_no', 'text', 'S No');	
			
			var i_internalID_f = i_time_expense_entries_tab.addField('custpage_internal_id', 'select', 'ID','expensereport').setDisplayType('inline');			
			var i_employee_f = i_time_expense_entries_tab.addField('custpage_employee', 'select', 'Employee','employee').setDisplayType('inline');		
			var i_customer_f = i_time_expense_entries_tab.addField('custpage_customer', 'select', 'Project','customer').setDisplayType('inline');	
			var i_amount_f = i_time_expense_entries_tab.addField('custpage_amount', 'text', 'Amount')//.setDisplayType('inline');	
			var i_date_f = i_time_expense_entries_tab.addField('custpage_date', 'date', 'Date').setDisplayType('inline');	
			var i_ref_no_fld = i_time_expense_entries_tab.addField('custpage_ref_no', 'text', 'Ref No')//.setDisplayType('hidden');	
					
			var i_billable_f = i_time_expense_entries_tab.addField('custpage_billable', 'checkbox', 'Billable').setDisplayType('inline');	
		    var i_status_f = i_time_expense_entries_tab.addField('custpage_status', 'select', 'Status','customlist124').setDisplayType('inline');	
			var i_currency_f = i_time_expense_entries_tab.addField('custpage_currency', 'select','Currency', 'currency').setDisplayType('inline');	
			var i_memo_f = i_time_expense_entries_tab.addField('custpage_memo', 'textarea','Memo');
				
			
			 var i_usage_0 = i_context.getRemainingUsage();
		  //   nlapiLogExecution('DEBUG', 'suiteletFunction','Usage 0 -->' + i_usage_0);
			
						
		//	if(_logValidation(i_from_date)&&_logValidation(i_to_date))
		    {			
		  // ================== Accounting - User =====================
				
			var i_expense_status;
			var i_expense_approver;
			var i_employeeID;	
			
			if(_logValidation(d_as_on_date_prm)&&_logValidation(i_employee_prm))
			{	
			    nlapiLogExecution('DEBUG', 'suiteletFunction',' IF ');		
		        
				var filter = new Array();
				
                filter[0] = new nlobjSearchFilter('employee', null, 'is', i_employee_prm);
		        filter[1] = new nlobjSearchFilter('trandate', null, 'onorbefore', d_as_on_date_prm);
		    	filter[2] = new nlobjSearchFilter('custbody_expenseapprover', null, 'is',i_current_user);
				
						
			    var i_search_results = nlapiSearchRecord('expensereport','customsearch_expensereportpendingforrm_6',filter,null);
									
			}
			else
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction',' ELSE ');
				
				var filter = new Array();
	            filter[0] = new nlobjSearchFilter('custbody_expenseapprover', null, 'is',i_current_user);
					
			    var i_search_results = nlapiSearchRecord('expensereport','customsearch_expensereportpendingforrm_6',filter,null);
							
			}
		
	        if (_logValidation(i_search_results)) 
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Expense Report Search Results Length Accounting -->' + i_search_results.length);
				
				for (var c = 0; c < i_search_results.length; c++) 
			//	for (var c = 0; c < 50; c++) 
				{
					var a_search_transaction_result = i_search_results[c];
					if(!_logValidation(a_search_transaction_result))
					{
						break;
					}
					var columns = a_search_transaction_result.getAllColumns();
					var columnLen = columns.length;
					
					for (var hg = 0; hg < columnLen; hg++) 
					{
						var column = columns[hg];
						var label = column.getLabel();
						var value = a_search_transaction_result.getValue(column)
						var text = a_search_transaction_result.getText(column)
						
						if (label == 'Internal ID') 
						{
							internal_ID = value;
						}
						if (label == 'Date') 
						{
							d_date = value;
						}
						if (label == 'Employee') 
						{
							i_employee = value;
						}
						if (label == 'Project ID') 
						{
							i_customer = value;
						}						
						if (label == 'Amount')
						{
							i_amount = value;
						}
						if (label == 'Ref No') 
						{
							i_ref_no = value;
						}
						if (label == 'Status') 
						{
							i_status_s = value;
						}
						if (label == 'Currency') 
						{
							i_currency_s = value;
						}
						if (label == 'Billable') 
						{
							i_billable_s = value;
						}
			            if (label == 'Memo') 
						{
							i_memo_s = value;
						}
						if (label == 'Employee ID') 
						{
							i_employeeID = value;
						}
						if (label == 'Expense Status') 
						{
							i_expense_status = value;
						}
						if (label == 'Expense Approver') 
						{
							i_expense_approver = value;
						}
					
					}//Time Column Loop
								
					if (!_logValidation(i_amount)) 
					{
						i_amount = ''
					}
					if (!_logValidation(i_status_s))
					{
						i_status_s = ''
					}
					if (!_logValidation(i_customer)) 
					{
						i_customer = ''
					}
					if (!_logValidation(i_employee)) 
					{
						i_employee = ''
					}
					if (!_logValidation(d_date)) 
					{
						d_date = ''
					}
					if (!_logValidation(internal_ID)) 
					{
						internal_ID = ''
					}
					if (!_logValidation(i_memo_s))
					{
						i_memo_s = ''
					}
					if (!_logValidation(i_billable_s)) 
					{
						i_billable_s = ''
					}
					if (!_logValidation(i_currency_s))
					{
						i_currency_s = ''
					}
					if (!_logValidation(i_ref_no))
					{
						i_ref_no = ''
					}
								
					
					if (_logValidation(internal_ID)) 
					{				
						if (_logValidation(i_ref_no)) 
						{	
						
							if (_logValidation(i_expense_status)) 
							{
								if (i_expense_status == 'pendingAcctApproval')
								{
									nlapiLogExecution('DEBUG', 'suiteletFunction', ' ******************** Accounting Block *********************');
									
									if (_logValidation(i_current_user) && _logValidation(i_expense_approver)) 
									{
										if (i_current_user == i_expense_approver) 
										{
											i_ant++;
					
											// ========================= Set values ==========================
											
											if(_logValidation(i_employeeID)&&_logValidation(i_employee))
											{
												a_employee_array.push(i_employeeID+'&&&&'+i_employee);  											
											}
											 
											i_time_expense_entries_tab.setLineItemValue('custpage_internal_id', i_ant, internal_ID);
											i_time_expense_entries_tab.setLineItemValue('custpage_s_no', i_ant, (parseInt(i_ant)).toFixed(0));
											i_time_expense_entries_tab.setLineItemValue('custpage_employee', i_ant, i_employeeID);
											i_time_expense_entries_tab.setLineItemValue('custpage_customer', i_ant, i_customer);
											i_time_expense_entries_tab.setLineItemValue('custpage_amount', i_ant, i_amount);
											i_time_expense_entries_tab.setLineItemValue('custpage_date', i_ant, d_date);
											i_time_expense_entries_tab.setLineItemValue('custpage_ref_no', i_ant, i_ref_no);
											i_time_expense_entries_tab.setLineItemValue('custpage_status',i_ant, i_status_s);	
											i_time_expense_entries_tab.setLineItemValue('custpage_currency',i_ant, i_currency_s);	
											i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_ant,i_billable_s);
											i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_ant,i_memo_s);
										
										     var i_usage_end = i_context.getRemainingUsage();
	                                         nlapiLogExecution('DEBUG', 'suiteletFunction','Usage End Accounting Manager -->' + i_usage_end);

                                             if(i_usage_end<=50)
											 {
											 	break;
											 }
										
										}//Approver 
									}
									
								}//Time Loop				
							}//Status Validation
						}//Expense OBJ
					}//Internal ID
				}//Search Loop
			}
				
		
		   var i_usage_0_1 = i_context.getRemainingUsage();
		//   nlapiLogExecution('DEBUG', 'suiteletFunction','Usage 0 1-->' + i_usage_0_1);
				
				// ================== Delivery Manager - User =====================
			
			
			var i_expense_status;
			var i_expense_approver;
			var i_employeeID;	
			
			if(_logValidation(d_as_on_date_prm)&&_logValidation(i_employee_prm))
			{	
			   nlapiLogExecution('DEBUG', 'suiteletFunction',' IF ');		
		        
				var filter = new Array();
			   
                filter[0] = new nlobjSearchFilter('employee', null, 'is', i_employee_prm);
		        filter[1] = new nlobjSearchFilter('trandate', null, 'onorbefore', d_as_on_date_prm);
		   	    filter[2] = new nlobjSearchFilter('custentity_deliverymanager', 'job', 'is',i_current_user);
				
					
			   var i_search_results = nlapiSearchRecord('expensereport','customsearch_expensereportpendingforrm_3',filter,null);
			
			}
			else
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction',' ELSE ');
				
				var filter = new Array();		   	
			
			    filter[0] = new nlobjSearchFilter('custentity_deliverymanager', 'job', 'is',i_current_user);
						
			   var i_search_results = nlapiSearchRecord('expensereport','customsearch_expensereportpendingforrm_3',filter,null);
										
			}
			
			if (_logValidation(i_search_results)) 
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Expense Report Search Results Length Delivery manager -->' + i_search_results.length);
				for (var c = 0; c < i_search_results.length; c++) 
				//for (var c = 0; c < 25; c++) 
				{
					var a_search_transaction_result = i_search_results[c];
					if(!_logValidation(a_search_transaction_result))
					{
						break;
					}
					var columns = a_search_transaction_result.getAllColumns();
					var columnLen = columns.length;
					
					for (var hg = 0; hg < columnLen; hg++) 
					{
						var column = columns[hg];
						var label = column.getLabel();
						var value = a_search_transaction_result.getValue(column)
						var text = a_search_transaction_result.getText(column)
						
						if (label == 'Internal ID') 
						{
							internal_ID = value;
						}
						if (label == 'Date') 
						{
							d_date = value;
						}
						if (label == 'Employee') 
						{
							i_employee = value;
						}
						if (label == 'Project ID') 
						{
							i_customer = value;
						}
						
						if (label == 'Amount') 
						{
							i_amount = value;
						}
						if (label == 'Ref No') 
						{
							i_ref_no = value;
						}
						if (label == 'Status') 
						{
							i_status_s = value;
						}
						if (label == 'Currency') 
						{
							i_currency_s = value;
						}
						if (label == 'Billable') 
					   {
						 i_billable_s = value;
					   }
					    if (label == 'Memo') 
						{
							i_memo_s = value;
						}
						if (label == 'Employee ID') 
					{
						i_employeeID = value;
					}
					if (label == 'Expense Status') 
					{
						i_expense_status = value;
					}
					if (label == 'Expense Approver') 
					{
						i_expense_approver = value;
					}
					}//Time Column Loop
					
					if (!_logValidation(i_amount))
					{
						i_amount = ''
					}
					if (!_logValidation(i_status_s))
					{
						i_status_s = ''
					}
					if (!_logValidation(i_customer)) 
					{
						i_customer = ''
					}
					if (!_logValidation(i_employee)) 
					{
						i_employee = ''
					}
					if (!_logValidation(d_date)) 
					{
						d_date = ''
					}
					if (!_logValidation(internal_ID)) 
					{
						internal_ID = ''
					}
					if (!_logValidation(i_memo_s))
					{
						i_memo_s = ''
					}
					if (!_logValidation(i_billable_s)) 
					{
						i_billable_s = ''
					}
					if (!_logValidation(i_currency_s))
					{
						i_currency_s = ''
					}
					if (!_logValidation(i_ref_no))
					{
						i_ref_no = ''
					}
											
						if (_logValidation(internal_ID)&& _logValidation(i_ref_no)) 
						{
							
								nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_expense_status -->' + i_expense_status);
								nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_expense_approver -->' + i_expense_approver);
								
								if (_logValidation(i_expense_status)) 
								{
									if (i_expense_status == 'pendingSupApproval') 
									{
										nlapiLogExecution('DEBUG', 'suiteletFunction', ' ******************** Supervisor Block 1 *********************');
										
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' internal_ID -->' + internal_ID);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_employeeID -->' + i_employeeID);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_customer -->' + i_customer);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_amount -->' + i_amount);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' d_date -->' + d_date);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_ref_no -->' + i_ref_no);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_status_s -->' + i_status_s);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_currency_s -->' + i_currency_s);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_billable_s -->' + i_billable_s);
										nlapiLogExecution('DEBUG', 'POST suiteletFunction', ' i_memo_s -->' + i_memo_s);
										nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_ent -->' + i_ent);
							
										//if (_logValidation(i_current_user) && _logValidation(i_approver)) 
										{
										//	if (i_current_user == i_approver) 
											{
											//	i_ent = i_ant ;
												i_ant++;
												// ========================= Set values ==========================
												
												if(_logValidation(i_employeeID)&&_logValidation(i_employee))
												{
												a_employee_array.push(i_employeeID+'&&&&'+i_employee);  													
												}
												
												i_time_expense_entries_tab.setLineItemValue('custpage_internal_id', i_ant, internal_ID);
												i_time_expense_entries_tab.setLineItemValue('custpage_s_no',i_ant,(parseInt(i_ant)).toFixed(0));
												
                                                i_time_expense_entries_tab.setLineItemValue('custpage_employee', i_ant, i_employeeID);
												i_time_expense_entries_tab.setLineItemValue('custpage_customer', i_ant, i_customer);
												i_time_expense_entries_tab.setLineItemValue('custpage_amount', i_ant, i_amount);
												i_time_expense_entries_tab.setLineItemValue('custpage_date', i_ant,d_date);
												i_time_expense_entries_tab.setLineItemValue('custpage_ref_no', i_ant, i_ref_no);
												i_time_expense_entries_tab.setLineItemValue('custpage_status',i_ant, i_status_s);
												i_time_expense_entries_tab.setLineItemValue('custpage_currency',i_ant, i_currency_s);
											    i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_ant,i_billable_s);
												i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_ant,nlapiEscapeXML(i_memo_s));


                                                 var i_usage_end = i_context.getRemainingUsage();
		                                         nlapiLogExecution('DEBUG', 'suiteletFunction','Usage End Delivery Manager -->' + i_usage_end);

                                                 if(i_usage_end<=50)
												 {
												 	break;
												 }

											}//Approver 
										}//Current User						
									}//Expense Status									
								}							
						}									
				}//Search Loop
			}//Search Length					  	
	       // ============================= Supervisor ==========================			
				
				
			var i_expense_status;
			var i_expense_approver;
			var i_employeeID;	
				
			if(_logValidation(d_as_on_date_prm)&&_logValidation(i_employee_prm))
			{	
			   nlapiLogExecution('DEBUG', 'suiteletFunction',' IF ');			   
			  
                var filter = new Array();
				
                filter[0] = new nlobjSearchFilter('employee', null, 'is', i_employee_prm);
		        filter[1] = new nlobjSearchFilter('trandate', null, 'onorbefore', d_as_on_date_prm);
			    filter[2] = new nlobjSearchFilter('supervisor','employee', 'is',i_current_user);
		

			   var a_search_results = nlapiSearchRecord('expensereport','customsearch_expensereportpendingforrm_4',filter,null);
			
			}
			else
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction',' ELSE ');
				
                var filter = new Array();				
		        filter[0] = new nlobjSearchFilter('supervisor','employee', 'is',i_current_user);
		   		
			    var a_search_results = nlapiSearchRecord('expensereport','customsearch_expensereportpendingforrm_4',filter,null);
					
			}
			
			if(_logValidation(a_search_results))
			{
			  nlapiLogExecution('DEBUG', 'suiteletFunction',' Expense Report Search Supervisor Results Length  -->' + a_search_results.length);	
			
			  for (var cd = 0; cd < a_search_results.length; cd++) 
			  //for (var cd = 0; cd < 25; cd++) 
			  {
			  	  nlapiLogExecution('DEBUG', 'suiteletFunction',' cd  -->' + cd);	
			
			  	var a_search_transaction_result = a_search_results[cd];
			
                if(!_logValidation(a_search_transaction_result))
				{
					break;
				}

			  	var columns = a_search_transaction_result.getAllColumns();
			  	var columnLen = columns.length;
			  
			  	for (var hgd = 0; hgd < columnLen; hgd++) 
				{
			  		var column = columns[hgd];
			  		var label = column.getLabel();
			  		var value = a_search_transaction_result.getValue(column)
			  		var text = a_search_transaction_result.getText(column)
			  		
			  		if (label == 'Internal ID') 
					{
			  			internal_ID = value;
			  		}
			  		if (label == 'Date') 
					{
			  			d_date = value;
			  		}
			  		if (label == 'Employee') 
					{
			  			i_employee = value;						
			  		}
			  		if (label == 'Customer')
					{
			  			i_customer = value;
			  		}
			  		if (label == 'Project ID') 
					{
			  			i_customer = value;
			  		}
			  		
			  		if (label == 'Amount') 
					{
			  			i_amount = value;
			  		}
			  		if (label == 'Ref No') 
					{
			  			i_ref_no = value;
			  		}
			  		if (label == 'Status') 
					{
						i_status_s = value;
					}
					if (label == 'Currency') 
					{
						i_currency_s = value;
					}
					if (label == 'Billable') 
					{
						i_billable_s = value;
					}
					if (label == 'Memo') 
					{
						i_memo_s = value;
					}
					
					if (label == 'Employee ID') 
					{
						i_employeeID = value;
					}
					if (label == 'Expense Status') 
					{
						i_expense_status = value;
					}
					if (label == 'Expense Approver') 
					{
						i_expense_approver = value;
					}
					
			  	}//Time Column Loop
					
							
						if (!_logValidation(i_duration)) 
						{
							i_duration = ''
						}
						if (!_logValidation(i_note)) 
						{
							i_note = ''
						}
						if (!_logValidation(i_item)) 
						{
							i_item = ''
						}
						if (!_logValidation(i_customer)) 
						{
							i_customer = ''
						}
						if (!_logValidation(i_employee)) 
						{
							i_employee = ''
						}
						if (!_logValidation(d_date)) 
						{
							d_date = ''
						}
						if (!_logValidation(i_status_s))
						{
							i_status_s = ''
						}
						if (!_logValidation(i_memo_s))
						{
							i_memo_s = ''
						}
						if (!_logValidation(i_billable_s)) 
						{
							i_billable_s = ''
						}
						if (!_logValidation(i_currency_s))
						{
							i_currency_s = ''
						}
						if (!_logValidation(i_ref_no))
						{
							i_ref_no = ''
						}
	
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_expense_status -->' + i_expense_status);
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_expense_approver -->' + i_expense_approver);
			nlapiLogExecution('DEBUG', 'suiteletFunction',' internal_ID  -->' + internal_ID);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' d_date  -->' + d_date);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employee  -->' + i_employee);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_amount  -->' + i_amount);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_customer  -->' + i_customer);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_ref_no  -->' + i_ref_no);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_employeeID  -->' + i_employeeID);	
					
						
	                        if (_logValidation(internal_ID)) 
							{
							
								if (_logValidation(i_ref_no)) 
								{								
									if (_logValidation(i_expense_status)) 
									{
										if (i_expense_status == 'pendingSupApproval') 
										{
											nlapiLogExecution('DEBUG', 'suiteletFunction',' ******************** Supervisor Block 2 *********************');	
																		
											//if (i_current_user == i_approver) 
											{
												i_ant++;
												// ========================= Set values ==========================
												
												if(_logValidation(i_employeeID)&&_logValidation(i_employee))
												{													
												 a_employee_array.push(i_employeeID+'&&&&'+i_employee);  												
												}
												
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' internal_ID -->' + internal_ID);
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_employeeID -->' + i_employeeID);
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_customer -->' + i_customer);
												
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_amount -->' + i_amount);
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' d_date -->' + d_date);
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_ref_no -->' + i_ref_no);
													
															
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_status_s -->' + i_status_s);
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_currency_s -->' + i_currency_s);
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_billable_s -->' + i_billable_s);
															
												nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_memo_s -->' + i_memo_s);
															
												
											    i_time_expense_entries_tab.setLineItemValue('custpage_internal_id', i_ant, internal_ID);
												
												i_time_expense_entries_tab.setLineItemValue('custpage_s_no', i_ant, parseInt(i_ant).toFixed(0));
												
                                                i_time_expense_entries_tab.setLineItemValue('custpage_employee', i_ant, i_employeeID);
												i_time_expense_entries_tab.setLineItemValue('custpage_customer', i_ant, i_customer);
												i_time_expense_entries_tab.setLineItemValue('custpage_amount', i_ant, i_amount);
												i_time_expense_entries_tab.setLineItemValue('custpage_date', i_ant, d_date);
												i_time_expense_entries_tab.setLineItemValue('custpage_ref_no', i_ant, i_ref_no);
												i_time_expense_entries_tab.setLineItemValue('custpage_status',i_ant, i_status_s);
												i_time_expense_entries_tab.setLineItemValue('custpage_currency',i_ant, i_currency_s);
											    i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_ant,i_billable_s);
												i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_ant,i_memo_s);

                                                 var i_usage_end = i_context.getRemainingUsage();
		                                         nlapiLogExecution('DEBUG', 'suiteletFunction','Usage End Supervisor -->' + i_usage_end);

                                                 if(i_usage_end<=50)
												 {
												 	break;
												 }
											}//Approver 
										}//Status
									}
								}
							}
						
					}//Loop				   
			  }// Search Length			              
		}//Time 
		
		// ========================== Employee List Sourcing - Start =======================================
			
		a_employee_array = removearrayduplicate(a_employee_array)
			
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Employee Array Length  -->' + a_employee_array.length);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Employee Array -->' + a_employee_array);
			var a_employee_split_array = new Array();
			
			if(_logValidation(a_employee_array))
			{
				for(var et=0;et<a_employee_array.length;et++)
				{
				 a_employee_split_array = a_employee_array[et].split('&&&&')
				 i_employee_f_1.addSelectOption(a_employee_split_array[0],a_employee_split_array[1])				
				}
			}
			
			
			
		// ========================== Employee List Sourcing - End =======================================
		
			f_form.addSubmitButton('APPROVE/REJECT');
			f_form.addButton('custpage_approve_markall','APPROVE MARK ALL','approve_mark_all()');
			f_form.addButton('custpage_approve_unmarkall','APPROVE UNMARK ALL','approve_unmark_all()');
			f_form.addButton('custpage_reject_markall','REJECT MARK ALL','reject_mark_all()');
			f_form.addButton('custpage_reject_unmarkall','REJECT UNMARK ALL','reject_unmark_all()');
		
						
		    response.writePage(f_form);	
			
			 var i_usage_end = i_context.getRemainingUsage();
		     nlapiLogExecution('DEBUG', 'suiteletFunction','Usage End -->' + i_usage_end);
		
		}//GET
		else if (request.getMethod() == 'POST') 	
		{
			 var d_as_on_date =  request.getParameter('custpage_as_on_date');
			 var i_employee =  request.getParameter('custpage_employee');
			 var a_data_array =   request.getParameter('custpage_data');
		     var i_selected_approve =   request.getParameter('custpage_select_approve_chk');
		     var i_selected_reject =   request.getParameter('custpage_select_reject_chk');
		     var i_current_user =   request.getParameter('custpage_current_user');
 
             nlapiLogExecution('DEBUG', 'POST suiteletFunction',' As On Date -->' + d_as_on_date);		
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Emplyee -->' + i_employee);		
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Array -->' + a_data_array);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Selected Approve -->' + i_selected_approve);		
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Selected Reject -->' + i_selected_reject);	
	 
			 var params=new Array();
		     params['custscript_date_till_now_e'] = d_as_on_date
			 params['custscript_employee_e'] = i_employee 			
			 params['custscript_data_array_approve_e'] = a_data_array
			 params['custscript_approve_e'] = i_selected_approve
			 params['custscript_reject_e'] = i_selected_reject
			 params['custscript_current_user_e'] = i_current_user
					
			 var status=nlapiScheduleScript('customscript_sch_approve_expense','customdeploy1',params);
			 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
										
		//	 nlapiSetRedirectURL('suitelet', 'customscript_sut_approve_expense', 'customdeploy1', null, null);
		//	 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ........... ' ); 
		
		     
			 if(_logValidation(i_selected_approve))
			{
				 response.write('<p><br/><br/><br/> Selected Expense Reports has been Approved.<\/p>')
			
			}	
			if(_logValidation(i_selected_reject))
			{
				 response.write('<p><br/><br/><br/> Selected Expense Reports has been Rejected<\/p>')
			
			} 	
						
			
						 	
		}//POST
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);		
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!= 'null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function get_project_manager(i_project)
{
	var i_project_manager;
	if(_logValidation(i_project)) 
	{
	  var o_projectOBJ = nlapiLoadRecord('job',i_project);
	  
	  if(_logValidation(o_projectOBJ)) 
	  {
	  	i_project_manager = o_projectOBJ.getFieldValue('custentity_projectmanager');
		nlapiLogExecution('DEBUG', 'get_project_manager', ' i_project_manager -->' + i_project_manager);	  	
	  }//Project OBJ		
	}//Project	
	return i_project_manager;
}//Project Manager

function get_supervisor(i_employee)
{
	var i_supervisor;
	if(_logValidation(i_employee)) 
	{
	  var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
	  
	  if(_logValidation(o_employeeOBJ)) 
	  {
	  	i_supervisor = o_employeeOBJ.getFieldValue('supervisor');
		nlapiLogExecution('DEBUG', 'get_project_manager', ' i_supervisor -->' + i_supervisor);
		  	
	  }//Project OBJ		
	}//Project	
	return i_supervisor;
}//Project Manager

function get_employee_details(i_employee)
{
  var i_employeeID;	
  var i_supervisor;
  var a_return_array = new Array();
  
  if(_logValidation(i_employee))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('entityid', null, 'is',i_employee);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('supervisor');
	
	 var a_search_results = nlapiSearchRecord('employee',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	 i_employeeID = a_search_results[0].getValue('internalid');
		 i_supervisor = a_search_results[0].getValue('supervisor');
			
	 }//Search Results	
  }//Employee
  a_return_array[0] = i_employeeID+'%%%'+i_supervisor;
  return a_return_array;
}
/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}
function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
      nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
     var today = nlapiDateToString(date1);
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'today ==' +today);
	 

	 
	 
/*
	 
	  if (date_format == 'YYYY/MM/DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 var s_month='';
	 var s_MONTH = '';
	 
	 if(month == 1)
	 {
	 	s_month = 'Jan'
		s_MONTH = 'January'
		
	 }
	 else if(month == 2)
	 {
	 		s_month = 'Feb'
			s_MONTH = 'February'
		
	 }
	 else if(month == 3)
	 {	 	
		s_month = 'Mar'
	    s_MONTH = 'March'
	 }
	 else if(month == 4)
	 {
	 		s_month = 'Apr'
			s_MONTH = 'April'		
	 }
	 else if(month == 5)
	 {
 		s_month = 'May'
		s_MONTH = 'May'	
	 }
	 else if(month == 6)
	 {	 	
		s_month = 'Jun'
		s_MONTH = 'June'
	 }
	 else if(month ==7)
	 {
	 	s_month = 'Jul'
		s_MONTH = 'July'
	 }
	 else if(month == 8)
	 {
	 	s_month = 'Aug'
		s_MONTH = 'August'
	 }
	 else if(month == 9)
	 {
	 	s_month = 'Sep'
		s_MONTH = 'September'
	 }
	 else if(month == 10)
	 {
 		s_month = 'Oct'
		s_MONTH = 'October'		
	 }
	 else if(month == 11)
	 {	 	
		s_month = 'Nov'
	    s_MONTH = 'November'
	 }
	 else if(month == 12)
	 {	 	
		s_month = 'Dec'
		s_MONTH = 'December'
	 }
	
	 
	  if (date_format == 'DD-Mon-YYYY')
	 {
	  	today = day + '-' + s_month + '-' + year;
	 }
	 if (date_format == 'DD-MONTH-YYYY')
	 {
	  	today = day + '-' + s_MONTH + '-' + year;
	 }
	  if (date_format == 'DD MONTH, YYYY')
	 {
	  	today = day+' '+ s_MONTH+', '+ year;
	 }
*/
	
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}


// END OBJECT CALLED/INVOKING FUNCTION =====================================================
