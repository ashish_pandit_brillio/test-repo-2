/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 08 Mar 2016 Nitish Mishra
 * 
 */

function scheduledaaa(type) {
	var search = nlapiSearchRecord(null, 1267);

	for (var i = 0; i < search.length; i++) {
		var empId = search[i].getValue('resource', null, 'group');
		var empName = search[i].getValue('firstname', 'employee', 'group');
		var mailTemplate = mailTemplateForMove(empName);
		try {
			nlapiSendEmail(442, empId, mailTemplate.Subject, mailTemplate.Body,
			        null, null, null, nlapiLoadFile('136960'));
		} catch (err) {
			nlapiLogExecution('error', i, err);
		}
	}
}

function mailTemplateForMove(firstName) {
	var htmltext = '<p>Hi ' + firstName + ',</p>';
	htmltext += '<p>This is to inform you that the new NetSuite timesheet has been rolled'
	        + ' out for Move team. Please check the attached document for the steps to be followed.</p>';

	htmltext += '<p>For further information, please contact ';
	htmltext += '<a href="mailto:Kiran.Susarla@brillio.com">Kiran Kumar Susarla</a> or ';
	htmltext += '<a href="mailto:Ananda.Hirekerur@brillio.com">Ananda Kumar H R</a>';

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'Move NetSuite Timesheet Rollout',
	    Body : addMailTemplate(htmltext)
	};
}

function scheduledA(type) {

	var file = nlapiLoadFile('63529');
	var fileValue = file.getValue();
	var rows = fileValue.split('\r\n');
	var currentContext = nlapiGetContext();

	for (var i = 1; i < rows.length; i++) {
		var row = rows[i];
		var columns = row.split(',');
		var projectId = columns[0];
		var taskName = columns[1];
		var taskTitle = columns[2];

		try {
			createProjectTask(projectId, taskName, taskTitle);
		} catch (err) {
			nlapiLogExecution('ERROR', 'line ' + i, err);
		}

		yieldScript(currentContext)
	}
}

function createProjectTask(projectId, taskJiraId, taskTitle) {
	var projectTaskRec = nlapiCreateRecord('projecttask');

	projectTaskRec.setFieldValue('company', projectId);
	projectTaskRec.setFieldValue('title', taskJiraId);
	projectTaskRec.setFieldValue('custevent_move_task_title', taskTitle);
	projectTaskRec.setFieldValue('estimatedwork', 100);
	projectTaskRec.setFieldValue('ismilestone', 'F');

	// assignee all active allocated resource to this task
	// search on allocation to get bill rate
	var costSearch = nlapiSearchRecord('resourceallocation', null, [
	        new nlobjSearchFilter('project', null, 'anyof', projectId),
	        new nlobjSearchFilter('enddate', null, 'notbefore', 'today'),
	        new nlobjSearchFilter('startdate', null, 'notafter', 'today') ], [
	        new nlobjSearchColumn('custevent3'),
	        new nlobjSearchColumn('resource') ]);

	if (costSearch) {

		for (var i = 0; i < costSearch.length; i++) {
			projectTaskRec.selectNewLineItem('assignee');
			projectTaskRec.setCurrentLineItemValue('assignee', 'resource',
			        costSearch[i].getValue('resource'));
			projectTaskRec.setCurrentLineItemValue('assignee', 'units',
			        '100.0%');
			projectTaskRec.setCurrentLineItemValue('assignee', 'estimatedwork',
			        100);
			projectTaskRec.setCurrentLineItemValue('assignee', 'serviceitem',
			        '2222');
			projectTaskRec.setCurrentLineItemValue('assignee', 'unitcost', 0);
			projectTaskRec.setCurrentLineItemValue('assignee', 'unitprice',
			        costSearch[i].getValue('custevent3'));
			projectTaskRec.commitLineItem('assignee');
		}
	}

	// var taskId = nlapiSubmitRecord(projectTaskRec);
	// nlapiLogExecution('AUDIT', 'New Project Task Created', taskId);
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}