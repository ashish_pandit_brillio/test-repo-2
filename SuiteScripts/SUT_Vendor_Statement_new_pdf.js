// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT Vendor Statement  
	Author:      Sachin K.
	Company:     Aashna Cloudtech Pvt Ltd
	Date:
	Version:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	7-8-2014			   NIKHIL							    kishor							change the location text
    22 sep 2014         Supriya                         Kalpana                       Normal Account Validation


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
            prepareVendorStatementCriteria()
            prepareVendorStatementPDF(request, response)
            HTMLDesignPopUpPDF(request, response) 
			GetHeadFmt()
			XMLConversion(html)
			prepareVendorStatementXL(request, response)
			HTMLDesignPopUpXL(request, response)
			getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate)
			GetCurrentTimeStamp()
			replaceCommaWithSemiColon(data)
			afterReplaceCR(custAddr)
			afterReplaceAddComma(custAddr)
			getLocationName(locationname)
			getCurrency(custCurrency)
               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY






}

// END SUITELET ====================================================

function prepareVendorStatementCriteria()
{
	nlapiLogExecution('DEBUG','prepareVendorStatementCriteria', "Request Method = " +request.getMethod());

	if(request.getMethod() == 'GET')
	{
		//============== CREATE FORM ============
		var form = nlapiCreateForm("Print Vendor Statement");
		var vendorName = form.addField('vendorname', 'select', 'Vendor', 'vendor');
		vendorName.setMandatory(true);
		
		//=======validate the reneweal=====
		 isindia_subsidiary()

		// ======= ADD FIELDS ========
		var locationField = form.addField('locationname', 'select', 'Location', 'location');
		
		var startDateField = form.addField('startdate', 'date', 'Start Date');
		startDateField.setMandatory(true);
		startDateField.setLayoutType('normal','startcol')
		
		var endDateField = form.addField('enddate', 'date', 'End Date');
		endDateField.setMandatory(true);

		// ==== CALL A CLIENT SCRIPT ====
		form.setScript('customscript_cli_vendorstatement_new');
       
	    // ==== ADD A BUTTON =====
	   	form.addButton('custombutton', 'Print in PDF Format', 'clientScriptPDF()');
      	response.writePage(form);

      	// ==== CALL A CLIENT SCRIPT ====
		form.setScript('customscript_cli_vendorstatement_new');
		
      	// ==== ADD A BUTTON =====
		form.addButton('custombutton', 'Email in CSV Format', 'clientScriptXL()');
      	response.writePage(form);

	}// if(request.getMethod() == 'GET')
	else if(request.getMethod() == 'POST')
	{
		nlapiLogExecution('DEBUG','createPrintCriteriaForm','posting request');

		var params = new Array();
		params['vendorname'] = request.getParameter('vendorname');
		params['locationname'] = request.getParameter('locationname');
		params['startdate'] = request.getParameter('startdate');
		params['enddate'] = request.getParameter('enddate');
		
		//==== REDIRECT TO NEXT URL ======
		nlapiSetRedirectURL('SUITELET','customscript_vendor_stmt', '1', false, params);
	} // else if(request.getMethod() == 'POST')
	else
	{
	    //===WRITE A RESPONSE ======
		var pageNotFound = '<html><body>404-Page Not Found</body></html>';
	    response.write(pageNotFound);
	} // END else
}// END prepareVendorStatementCriteria()

function prepareVendorStatementPDF(request, response)
{
	//==== CALL A HTMLDesignPopUpPDF(request, response) FUNCTION
	nlapiLogExecution('DEBUG', 'prepareVendorStatementPDF',  'Request Method = ' + request.getMethod());
	HTMLDesignPopUpPDF(request, response);
} // END prepareVendorStatementPDF(request, response)

function HTMLDesignPopUpPDF(request, response)
{
	//===== FUNCTION FOR DESIGNING THE PDF ======
	nlapiLogExecution('DEBUG', 'HTMLDesignPopUp', 'Request Method = ' + request.getMethod());
    if (request.getMethod() == 'GET')
	{
		var htmlMsg = "";
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Searching Vendor Transactions');

		var vendor = request.getParameter('vendorname');
		var locationname = request.getParameter('locationname');
		var start = request.getParameter('startdate');
		var end = request.getParameter('enddate');
		var startdate = nlapiStringToDate(start);
		var enddate = nlapiStringToDate(end);
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Vendor Name = '+vendor);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Location Name = '+locationname);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Start date = '+startdate);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'End date = '+enddate);

		var custFilters = new Array();
		var custColumns = new Array();
		
		custFilters.push( new nlobjSearchFilter('internalid', null, 'is', vendor));
		custColumns.push( new nlobjSearchColumn('datecreated'));
		custColumns.push( new nlobjSearchColumn('entityid'));
		custColumns.push( new nlobjSearchColumn('address'));
		custColumns.push( new nlobjSearchColumn('currency'));

		var custSearchResults = nlapiSearchRecord('vendor', null, custFilters, custColumns);
		
		if(custSearchResults != null)
		{
		var custCreatedDate = custSearchResults[0].getValue('datecreated');
		custCreatedDate = nlapiStringToDate(custCreatedDate);
		var custName = custSearchResults[0].getValue('entityid');
		var custAddr = custSearchResults[0].getValue('address');
		var custCurrency = custSearchResults[0].getValue('currency');
		
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCreatedDate = '+custCreatedDate);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custName = '+custName);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custAddr = '+custAddr);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = '+custCurrency);

		custAddr = nlapiEscapeXML(custAddr);
		custAddr = afterReplaceCR(custAddr);
		var locationnametext;
		if(locationname != '' && locationname != null && locationname!= 'undefined' && locationname!= undefined)
		{
			locationnametext = getLocationName(locationname);
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'locationnametext = '+locationnametext);
		}
		else
		{
			locationnametext="All"		
		}
		locationnametext = nlapiEscapeXML(locationnametext);
		custCurrency = getCurrency(custCurrency);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = '+custCurrency);
		
		var opBalanceEndDatems = new Date(startdate.getTime());
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = '+opBalanceEndDatems);
		
		opBalanceEndDatems = opBalanceEndDatems - 86400000;
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = '+opBalanceEndDatems);
		
		var opBalanceEndDate = new Date(opBalanceEndDatems);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDate = '+opBalanceEndDate);

		var openingBalanceDate = start;
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalanceDate = '+openingBalanceDate);

		//var openingBalance = getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate);
		var openingBalance = getOpeningBal(vendor, locationname, custCreatedDate, opBalanceEndDate);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalance = '+openingBalance);

		var logourl = nlapiEscapeXML("https://system.na1.netsuite.com/core/media/media.nl?id=48&c=TSTDRV990220&h=de8aeaec54240560eaa9");
        nlapiLogExecution('DEBUG', 'HTMLDesignPopUp', 'logourl = ' +logourl);

		htmlMsg += GetHeadFmt() + "<body>";

		//htmlMsg = htmlMsg + "<table>";
		//htmlMsg = htmlMsg + "<tr>";
		//htmlMsg = htmlMsg + "<td><img src=\"" +logourl+ "\" style=\"margin-top:0px  \" align=\"center\" /><BR/></td>";//height:42px; width:306px;
		//htmlMsg = htmlMsg + "</tr>";
		//htmlMsg = htmlMsg + "</table>";

		htmlMsg = htmlMsg + "<table align=\"center\">";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textVeryLargeBold\">Vendor Statement<BR/><BR/></td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "</table>";

		htmlMsg = htmlMsg + "<table width=\"100%\">";
		htmlMsg = htmlMsg + "<tr>";

		htmlMsg = htmlMsg + "<td width=\"50%\">";
		htmlMsg = htmlMsg + "<table>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textLargeBold\" align=\"left\">Vendor : "+nlapiEscapeXML(custName)+"</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textLargeBold\" align=\"left\">"+custAddr+"</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "</table>";
		htmlMsg = htmlMsg + "</td>";

		htmlMsg = htmlMsg + "<td width=\"15%\">";
		htmlMsg = htmlMsg + "<table>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td>";
		htmlMsg = htmlMsg + "&nbsp;";
		htmlMsg = htmlMsg + "</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "</table>";
		htmlMsg = htmlMsg + "</td>";

		htmlMsg = htmlMsg + "<td width=\"35%\">";
		htmlMsg = htmlMsg + "<table width=\"100%\">";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"35%\">From Date : </td>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"65%\">"+nlapiEscapeXML(start)+"</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"30%\">To Date : </td>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"70%\">"+nlapiEscapeXML(end)+"</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"30%\">Location : </td>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"70%\">"+nlapiEscapeXML(locationnametext)+"</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" align =\"left\" width=\"30%\">Currency : </td>";
		htmlMsg = htmlMsg + "<td class=\"textGoodBold\" width=\"70%\">"+nlapiEscapeXML(custCurrency)+"</td>";
		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "</table>";
		htmlMsg = htmlMsg + "</td>";

		htmlMsg = htmlMsg + "</tr>";
		htmlMsg = htmlMsg + "</table>";

		htmlMsg = htmlMsg + "<table id=\"detailsTable\" align=\"center\" table-layout=\"fixed\" width=\"100%\">";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Date</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Transaction</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Transaction No.</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Vendor Invoice No.</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Memo</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Debit</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Credit</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"center\" colspan=\"1\" class=\"textGoodBold\">Balance<BR/></td>";
		htmlMsg = htmlMsg + "</tr>";

		var blank = '-';
		var description = 'Opening Balance';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(openingBalanceDate)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(description)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(openingBalance)+"</td>";
		htmlMsg = htmlMsg + "</tr>";

		var balance = 0;
		var lastInternalID = 0;
		var totalAmount = 0;
		totalAmount = parseFloat(totalAmount);
		var debitTotal = 0;
		debitTotal = parseFloat(debitTotal);
		var creditTotal = 0;
		creditTotal = parseFloat(creditTotal);
		var recordCount = 0;

		
			var filters = new Array();
			
			filters.push( new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
			filters.push( new nlobjSearchFilter('name', null, 'is', vendor));
			//filters.push( new nlobjSearchFilter('trandate', null, 'within', startdate, enddate));
			filters.push( new nlobjSearchFilter('trandate', null, 'within', startdate, enddate));
			if(locationname != '' && locationname != null)
			{
				filters.push( new nlobjSearchFilter('location', null, 'is', locationname));
			} // END if(locationname != '' && locationname != null)
			var searchResults = nlapiSearchRecord('transaction','customsearch_vendor_statement', filters, null);

			var breakFlag = 0;
			if(searchResults != null && searchResults != '')
			{
				var length = searchResults.length;
				nlapiLogExecution('DEBUG', 'ScheduleTest', 'search result length = ' +length);

				for (var counter = 0; counter < searchResults.length; counter++)
				{
					recordCount = recordCount + 1;

					var result = searchResults[counter];
					var columns = result.getAllColumns();
					var columnLen = columns.length;

					var internalID = '';
					var amount = '';
					var type = '';
					var date = '';
					var number = '';
					var typenumber = '';
					var memo = '';
					var previousInternalID = '';
					var transaction_number = '';
					for (var i = 0; i < columnLen; i++)
					{
						var column = columns[i];
						var fieldName = column.getName();
						var value = result.getValue(column);

						if(fieldName == 'internalid')
						{
							previousInternalID = lastInternalID;
							//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'previousInternalID = ' +previousInternalID);
							internalID = value;
							//nlapiLogExecution('DEBUG', 'ScheduleTest Internal ID', 'internalID = ' +internalID);
						} // END if(fieldName == 'internalid')

						if(fieldName == 'trandate')
						{
							date = value;
							//nlapiLogExecution('DEBUG', 'ScheduleTest Date', 'date = ' +date);
						} // END if(fieldName == 'trandate')

						if(fieldName == 'transactionnumber')
						{
							transaction_number = value;
							transaction_number = nlapiEscapeXML(transaction_number);
							//nlapiLogExecution('DEBUG', 'ScheduleTest transaction_number', 'transaction_number = ' +transaction_number);
						} 
						
						if(fieldName == 'memomain')
						{
							memo = value;
							memo = nlapiEscapeXML(memo);
							//nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'memo = ' +memo);
						} // END if(fieldName == 'memomain')

						if(fieldName == 'type')
						{
							type = value;
							//nlapiLogExecution('DEBUG', 'ScheduleTest Type', 'type = ' +type);
						} // END if(fieldName == 'type')

						if(fieldName == 'tranid')
						{
							number = value;
							//nlapiLogExecution('DEBUG', 'ScheduleTest Number', 'number = ' +number);
							typenumber = type + ' ' + number;
							//nlapiLogExecution('DEBUG', 'ScheduleTest TypeNumber', 'typenumber = ' +typenumber);
						} // END if(fieldName == 'tranid')

						if(fieldName == 'amount')
						{
							amount = value;
							amount = parseFloat(amount);
							amount = Math.round(amount*100)/100;
							//nlapiLogExecution('DEBUG', 'ScheduleTest Amount', 'amount = ' +amount);
						} // END if(fieldName == 'amount')
						if(counter >= 900)
						{
							if(previousInternalID != internalID)
							{
								breakFlag = 1;
							} // END if(previousInternalID != internalID)
							
						} // END if(counter >= 900)
						
					} // END for (var i = 0; i < columnLen; i++)
					if(breakFlag == 1)
					{
						break;
					} // END if(breakFlag == 1)

					totalAmount = parseFloat(totalAmount) - parseFloat(amount);
					totalAmount = Math.round(totalAmount*100)/100;
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'current totalAmount = ' +totalAmount);

					var blank = '-';
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(date)+"</td>";
					htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(type)+"</td>";
					htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(transaction_number)+"</td>";
					
					if(number == '' || number == null)
					{
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
					} // END if(number == '' || number == null)
					else
					{
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(number)+"</td>";
					} // END else
					
					if(memo == '' || memo == null)
					{
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
					} // END if(memo == '' || memo == null)
					else
					{
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(memo)+"</td>";
					} // END else
					if((type == 'VendPymt'  || type == 'VendCred')|| (type == 'Journal' && parseFloat(amount) <= parseFloat(0) ))
					{
						nlapiLogExecution('Debug',' type VP VC',type);
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(-(amount))+"</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
						debitTotal = parseFloat(debitTotal) - parseFloat(amount);
						debitTotal = Math.round(debitTotal*100)/100;
						nlapiLogExecution('Debug','debitTotal ',creditTotal);
					} // END if(type == 'VendBill')
					if((type == 'VendBill')|| (type == 'Journal' && parseFloat(amount) > parseFloat(0)))
					{
						nlapiLogExecution('Debug',' type VB J'+type,'amount '+amount);
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
						htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(amount)+"</td>";
						creditTotal = parseFloat(creditTotal) + parseFloat(amount);
						creditTotal = Math.round(creditTotal*100)/100;
						nlapiLogExecution('Debug','creditTotal ',creditTotal);
					} // END if(type == 'VendPymt' || type == 'Journal' || type == 'VendCred')

					if(counter == 0 && lastInternalID == 0)
					{
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'first time');
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'openingBalance = ' +openingBalance);
						balance = parseFloat(openingBalance) - parseFloat(amount);
						balance = Math.round(balance*100)/100;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' +balance);
					} // END if(counter == 0 && lastInternalID == 0)
					else
					{
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'not first time');
						balance = parseFloat(balance) - parseFloat(amount);
						balance = Math.round(balance*100)/100;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' +balance);
					} // END else
					
					htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(balance)+"</td>";
					htmlMsg = htmlMsg + "</tr>";

					lastInternalID = internalID;
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'test lastInternalID = ' +lastInternalID);
				}// for (var counter = 0; counter < searchResults.length; counter++)
			} // if(searchResults != null && searchResults != '')

		totalAmount = parseFloat(totalAmount);
		totalAmount = Math.round(totalAmount*100)/100;
		nlapiLogExecution('DEBUG', 'ScheduleTest', 'totalAmount = ' +totalAmount);
		nlapiLogExecution('DEBUG', 'ScheduleTest', 'recordCount = ' +recordCount);

		var currentBalance = parseFloat(openingBalance) + parseFloat(totalAmount);
		currentBalance = Math.round(currentBalance*100)/100;
		nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' +currentBalance);

		var blank = '-';
		var endDescription = 'Debit Total';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(debitTotal)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		htmlMsg = htmlMsg + "</tr>";

		var blank = '-';
		var endDescription = 'Credit Total';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML((creditTotal))+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "</tr>";

		if(currentBalance < 0)
		{
			var blank = '-';
			var endDescription = 'Closing Balance';
			var newbalance = ((currentBalance));
			htmlMsg = htmlMsg + "<tr>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(newbalance)+"</td>";
			htmlMsg = htmlMsg + "</tr>";
		} // END if(currentBalance < 0)
		else if(currentBalance > 0)
		{
			var blank = '-';
			var endDescription = 'Closing Balance';
			htmlMsg = htmlMsg + "<tr>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(currentBalance)+"</td>";
			htmlMsg = htmlMsg + "</tr>";
		} // END else if(currentBalance > 0)
		else
		{
			var blank = '-';
			var endDescription = 'Closing Balance';
			htmlMsg = htmlMsg + "<tr>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"11%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
			htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(currentBalance)+"</td>";
			htmlMsg = htmlMsg + "</tr>";
		} // END else

		htmlMsg = htmlMsg + "</table>";

		htmlMsg = htmlMsg + "</body>";
		htmlMsg = htmlMsg + "";

		XMLConversion(htmlMsg);
			
		}// End if(custSearchResults != null)

		
	}// END if (request.getMethod() == 'GET')
	
}// END HTMLDesignPopUpPDF(request, response)

function GetHeadFmt()
{
    
	//===== CODE TO GET THE HEADER FORMAT ========
	var format = "<head>" +
    "<style>" +
    ".textreg { font-family:Arial, Helvetica; font-size: 8pt; }" +
    ".linetextreg { font-family:Arial, Helvetica; font-size: 8pt; border-bottom: 1px dotted;}" +
    ".textregTopBorder { font-family:Arial, Helvetica; font-size: 9.5pt; border-top:solid; }" +
    ".textregBoldTopBorder { font-family:Arial, Helvetica; font-size: 9.5pt; font-weight: bold; border-top:solid; }" +
    ".textregbold { font-family: Arial,Helvetica; font-size: 8pt; font-weight: bold;}" +
    ".tabletextregbold { font-family: Arial,Helvetica; font-size: 8pt; font-weight: bold;background-color: #FFCCCC}" +
    ".textHeaderBold { font-family:Arial, Helvetica; font-size: 12pt; font-weight: bold;}" +
    ".textLargeBold { font-family:Arial, Helvetica; font-size: 10.5pt; font-weight: bold; text-align:center;}" +
    ".textGoodBold { font-family:Arial, Helvetica; font-size: 9.5pt; font-weight: bold;}" +
    ".textGoodThin { font-family:Arial, Helvetica; font-size: 9pt;}" +
    ".textVeryLargeBold { font-family:Arial, Helvetica; font-size: 18pt; font-weight: bold;}" +
    ".textregSmall { font-family:Arial, Helvetica; font-size: 8pt; }" +
    ".texttitle { font-family: Arial, Helvetica; font-size: 16pt ; font-weight: bold; font-style:italic; }" +
    ".textprojtitle { font-family: Arial, Helvetica; font-size: 11pt; font-weight: bold; }" +
    ".textregAddr { font-family:Arial, Helvetica; font-size: 7pt; }" +
    ".footer, .push {height: 6em;}" +
    ".P { margin-top: 10pt; margin-bottom:10pt; margin-left:0pt; margin-right:0pt; }" +
    "</style>" +
    "</head>";

    return format;
} // END GetHeadFmt()

function XMLConversion(html)
{
    //==== CODE FOR HTML TO XML CONVERSION =====
    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    xml += "<pdf lang=\"ru-RU\" xml:lang=\"ru-RU\">\n";
    xml += html;
    xml += "</pdf>";
        var file = nlapiXMLToPDF(xml);

        // set content type, file name, and content-disposition (inline means display in browser)
        response.setContentType('PDF', 'Generatedprintpreview.pdf', 'inline');
        //response.setContentType('HTMLDOC', 'Generated Invoice.pdf', 'inline');

        // write response to the client
        response.write(file.getValue());
        //response.write(html);
} // END XMLConversion(html)

function prepareVendorStatementXL(request, response)
{
	//===== CODE TO CALL HTMLDesignPopUpXL(request, response) FUNCTION
	nlapiLogExecution('DEBUG', 'prepareVendorStatementXL',  'Request Method = ' + request.getMethod());
	HTMLDesignPopUpXL(request, response);
} // END prepareVendorStatementXL(request, response)

function HTMLDesignPopUpXL(request, response)
{
	//==== CODE FOR DESGNING POP UP XL ======
	var userId = nlapiGetUser();
	var empFilters = new Array();
	var empColumns = new Array();
	empFilters.push( new nlobjSearchFilter('internalid', null, 'is', userId));
	empColumns.push( new nlobjSearchColumn('email'));
	var empSearchResults = nlapiSearchRecord('employee', null, empFilters, empColumns);
	if(empSearchResults != null)
	{
		var empEmail = empSearchResults[0].getValue('email');
		
		
		
		//var empEmail ='sachink@aashnacloudtech.com'
		//nlapiLogExecution('DEBUG', 'HTMLDesignPopUp', 'empEmail = ' +empEmail);

	//CSV
	var csvCount = 0;
	var lineItemDataArray = new Array();
	nlapiLogExecution('DEBUG', 'HTMLDesignPopUp', 'Request Method = ' + request.getMethod());
	var htmlMsg = "<HTML>";
	//var htmlMsg = "";
    if (request.getMethod() == 'GET')
	{
		var htmlMsg = "";
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Searching Vendor Transactions');
		var vendor = request.getParameter('vendorname');
		var locationname = request.getParameter('locationname');
		var start = request.getParameter('startdate');
		var end = request.getParameter('enddate');

		var startdate = nlapiStringToDate(start);
		var enddate = nlapiStringToDate(end);

		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Vendor Name = '+vendor);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Location Name = '+locationname);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'Start date = '+startdate);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'End date = '+enddate);

		var custFilters = new Array();
		var custColumns = new Array();
		custFilters.push( new nlobjSearchFilter('internalid', null, 'is', vendor));
		custColumns.push( new nlobjSearchColumn('datecreated'));
		custColumns.push( new nlobjSearchColumn('entityid'));
		custColumns.push( new nlobjSearchColumn('address'));
		custColumns.push( new nlobjSearchColumn('currency'));
		var custSearchResults = nlapiSearchRecord('vendor', null, custFilters, custColumns);
		
		if(custSearchResults != null && custSearchResults != undefined && custSearchResults != '')
		{
		var custCreatedDate = custSearchResults[0].getValue('datecreated');
		custCreatedDate = nlapiStringToDate(custCreatedDate);
		var custName = custSearchResults[0].getValue('entityid');
		var custAddr = custSearchResults[0].getValue('address');
		var custCurrency = custSearchResults[0].getValue('currency');
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCreatedDate = '+custCreatedDate);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custName = '+custName);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custAddr = '+custAddr);
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = '+custCurrency);

		custAddr = '"'+afterReplaceAddComma(custAddr)+'"';
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custAddr after replace = '+custAddr);
		var locationnametext;
		if(locationname != '' && locationname != null && locationname!= 'undefined' && locationname!= undefined)
		{
			locationnametext = getLocationName(locationname);
			nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'locationnametext = '+locationnametext);
		}
		else
		{
			locationnametext="All"		
		}
		custCurrency = getCurrency(custCurrency);
		//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'custCurrency = '+custCurrency);

		var opBalanceEndDatems = new Date(startdate.getTime());
		//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = '+opBalanceEndDatems);
		opBalanceEndDatems = opBalanceEndDatems - 86400000;
		//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDatems = '+opBalanceEndDatems);
		var opBalanceEndDate = new Date(opBalanceEndDatems);
		//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'opBalanceEndDate = '+opBalanceEndDate);

		var openingBalanceDate = start;
		//nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalanceDate = '+openingBalanceDate);
		//var openingBalance = getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate);
		var openingBalance = getOpeningBal(vendor, locationname, custCreatedDate, opBalanceEndDate);
		openingBalance = openingBalance;
		nlapiLogExecution('DEBUG', 'searchVendorTransactions', 'openingBalance = '+openingBalance);

		var empty = " ";

		//------------CSV IMPORT START------------------------------------
		lineItemDataArray[csvCount] = "Vendor :-"+","+custName+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;
		lineItemDataArray[csvCount] = "Address :-"+","+custAddr+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;
		lineItemDataArray[csvCount] = "From Date :-"+","+start+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;
		lineItemDataArray[csvCount] = "To Date :-"+","+end+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;
		lineItemDataArray[csvCount] = "Location :-"+","+locationnametext+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;
		lineItemDataArray[csvCount] = "Currency :-"+","+custCurrency+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;
		lineItemDataArray[csvCount] = empty+","+empty+","+empty+","+empty+","+empty+","+empty+","+empty;
		csvCount = csvCount + 1;


        htmlMsg = htmlMsg + "<body>";
		htmlMsg = htmlMsg + "<CENTER>";


		htmlMsg = htmlMsg + "<table id=detailsTable border=1>";
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td align=center width=10%>Date</td>";
		htmlMsg = htmlMsg + "<td align=center width=15%>Transaction</td>";
		htmlMsg = htmlMsg + "<td align=center width=15%>Reference No.</td>";
		htmlMsg = htmlMsg + "<td align=center width=20%>Memo</td>";
		htmlMsg = htmlMsg + "<td align=center width=13%>Debit</td>";
		htmlMsg = htmlMsg + "<td align=center width=13%>Credit</td>";
		htmlMsg = htmlMsg + "<td align=center width=14%>Balance<BR/></td>";
		
		htmlMsg = htmlMsg + "</tr>";

		//CSV
		lineItemDataArray[csvCount] = "Date"+","+"Transaction"+","+"Reference No."+","+"Memo"+","+"Debit"+","+"Credit"+","+"Balance";
		csvCount = csvCount + 1;

		var blank = '-';

		var description = 'Opening Balance';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td align=left width=10%>"+nlapiEscapeXML(openingBalanceDate)+"</td>";
		htmlMsg = htmlMsg + "<td align=justified width=15%>"+nlapiEscapeXML(description)+"</td>";
		htmlMsg = htmlMsg + "<td align=justified width=15%>"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td align=justified width=15%>"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=20%>"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=13%>"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=13%>"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td align=right width=14%>"+nlapiEscapeXML(openingBalance)+"</td>";
		htmlMsg = htmlMsg + "</tr>";

		//CSV
		lineItemDataArray[csvCount] = openingBalanceDate+","+description+","+blank+","+blank+","+blank+","+blank+","+openingBalance;
		csvCount = csvCount + 1;

		var balance = 0;
		var lastInternalID = 0;
		var totalAmount = 0;
		totalAmount = parseFloat(totalAmount);
		var debitTotal = 0;
		debitTotal = parseFloat(debitTotal);
		var creditTotal = 0;
		creditTotal = parseFloat(creditTotal);
		var recordCount = 0;

		
			var filters = new Array();
			filters.push( new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
			filters.push( new nlobjSearchFilter('name', null, 'is', vendor));
			filters.push( new nlobjSearchFilter('trandate', null, 'within', startdate, enddate));
			
			if(locationname != '' && locationname != null)
			{
				filters.push( new nlobjSearchFilter('location', null, 'is', locationname));
			}
			
			var searchResults = nlapiSearchRecord('transaction','customsearch_vendor_statement', filters, null);

			var breakFlag = 0;
			if(searchResults != null && searchResults != '')
			{
				var length = searchResults.length;
				nlapiLogExecution('DEBUG', 'ScheduleTest', 'search result length = ' +length);

				for (var counter = 0; counter < searchResults.length; counter++)
				{
					recordCount = recordCount + 1;

					var result = searchResults[counter];
					var columns = result.getAllColumns();
					var columnLen = columns.length;

					var internalID = '';
					var amount = '';
					var type = '';
					var date = '';
					var number = '';
					var typenumber = '';
					var memo = '';
					var previousInternalID = '';

					for (var i = 0; i < columnLen; i++)
					{
						var column = columns[i];
						var fieldName = column.getName();
						var value = result.getValue(column);

						if(fieldName == 'internalid')
						{
							previousInternalID = lastInternalID;
							nlapiLogExecution('DEBUG', 'getOpeningBalance', 'previousInternalID = ' +previousInternalID);
							internalID = value;
							nlapiLogExecution('DEBUG', 'ScheduleTest Internal ID', 'internalID = ' +internalID);
						}

						if(fieldName == 'trandate')
						{
							date = value;
							nlapiLogExecution('DEBUG', 'ScheduleTest Date', 'date = ' +date);
						}

						if(fieldName == 'memomain')
						{
							memo = value;
							memo = replaceCommaWithSemiColon(memo);
							nlapiLogExecution('DEBUG', 'ScheduleTest Memo', 'memo = ' +memo);
						}

						if(fieldName == 'type')
						{
							type = value;
							nlapiLogExecution('DEBUG', 'ScheduleTest Type', 'type = ' +type);
						}

						if(fieldName == 'tranid')
						{
							number = value;
							number = replaceCommaWithSemiColon(number);
							nlapiLogExecution('DEBUG', 'ScheduleTest Number', 'number = ' +number);
							typenumber = type + ' ' + number;
							typenumber = replaceCommaWithSemiColon(typenumber);
							nlapiLogExecution('DEBUG', 'ScheduleTest TypeNumber', 'typenumber = ' +typenumber);
						}

						if(fieldName == 'amount')
						{
							amount = value;
							amount = parseFloat(amount);
							amount = Math.round(amount*100)/100;
							nlapiLogExecution('DEBUG', 'ScheduleTest Amount', 'amount = ' +amount);
						}
						if(counter >= 900)
						{
							if(previousInternalID != internalID)
							{
								breakFlag = 1;
							}
						}
					}
					if(breakFlag == 1)
					{
						break;
					}

					totalAmount = parseFloat(totalAmount) - parseFloat(amount);
					totalAmount = Math.round(totalAmount*100)/100;
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'current totalAmount = ' +totalAmount);

					var blank = '-';
					htmlMsg = htmlMsg + "<tr>";
					htmlMsg = htmlMsg + "<td align=left width=10%>"+date+"</td>";
					htmlMsg = htmlMsg + "<td align=justified width=15%>"+type+"</td>";
					//CSV
					lineItemDataArray[csvCount] = date+","+type;
					if(number == '' || number == null)
					{
						htmlMsg = htmlMsg + "<td align=center width=15%>"+nlapiEscapeXML(blank)+"</td>";
						//CSV
						lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+blank;
					}
					else
					{
						htmlMsg = htmlMsg + "<td align=justified width=15%>"+nlapiEscapeXML(number)+"</td>";
						//CSV
						lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+number;
					}
					if(memo == '' || memo == null)
					{
						htmlMsg = htmlMsg + "<td align=center width=20%>"+nlapiEscapeXML(blank)+"</td>";
						//CSV
						lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+blank;
					}
					else
					{
						htmlMsg = htmlMsg + "<td align=justified width=20%>"+nlapiEscapeXML(memo)+"</td>";
						//CSV
						lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+memo;
					}
					if((type == 'VendPymt' || type == 'VendCred')|| (type == 'Journal' && parseFloat(amount) <= parseFloat(0) ))
					{
						htmlMsg = htmlMsg + "<td align=right width=13%>"+nlapiEscapeXML(amount)+"</td>";
						htmlMsg = htmlMsg + "<td align=center width=13%>"+nlapiEscapeXML(blank)+"</td>";
						//CSV
						lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+nlapiEscapeXML(-(amount))+","+blank;
						debitTotal = parseFloat(debitTotal) - parseFloat(amount);
						debitTotal = Math.round(debitTotal*100)/100;
					}
					if((type == 'VendBill')|| (type == 'Journal' && parseFloat(amount) > parseFloat(0) ))
					{
						htmlMsg = htmlMsg + "<td align=center width=13%>"+blank+"</td>";
						htmlMsg = htmlMsg + "<td align=right width=13%>"+nlapiEscapeXML(amount)+"</td>";
						//CSV
						lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+nlapiEscapeXML(blank)+","+(amount);
						creditTotal = parseFloat(creditTotal) + parseFloat(amount);
						creditTotal = Math.round(creditTotal*100)/100;
					}

					if(counter == 0 && lastInternalID == 0)
					{
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'first time');
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'openingBalance = ' +openingBalance);
						balance = parseFloat(openingBalance) - parseFloat(amount);
						balance = Math.round(balance*100)/100;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' +balance);
					}
					else
					{
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'not first time');
						balance = parseFloat(balance) - parseFloat(amount);
						balance = Math.round(balance*100)/100;
						nlapiLogExecution('DEBUG', 'ScheduleTest', 'balance = ' +balance);
					}
					htmlMsg = htmlMsg + "<td align=right width=14%>"+nlapiEscapeXML(balance)+"</td>";
					//CSV
					lineItemDataArray[csvCount] = lineItemDataArray[csvCount]+","+balance;
					csvCount = csvCount + 1;
					htmlMsg = htmlMsg + "</tr>";

					lastInternalID = internalID;
					nlapiLogExecution('DEBUG', 'ScheduleTest', 'test lastInternalID = ' +lastInternalID);
				}
			}
		//}while(searchResults != null)

		totalAmount = parseFloat(totalAmount);
		totalAmount = Math.round(totalAmount*100)/100;
		nlapiLogExecution('DEBUG', 'ScheduleTest', 'totalAmount = ' +totalAmount);
		nlapiLogExecution('DEBUG', 'ScheduleTest', 'recordCount = ' +recordCount);

		var currentBalance = parseFloat(openingBalance) + parseFloat(totalAmount);
		currentBalance = Math.round(currentBalance*100)/100;
		nlapiLogExecution('DEBUG', 'ScheduleTest', 'currentBalance = ' +currentBalance);

		var blank = '-';
		var endDescription = 'Debit Total';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(debitTotal)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"114%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		//CSV
		lineItemDataArray[csvCount] = end+","+endDescription+","+blank+","+blank+","+debitTotal+","+blank+","+blank;
		csvCount = csvCount + 1;
		htmlMsg = htmlMsg + "</tr>";

		var blank = '-';
		var endDescription = 'Credit Total';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"10%\" align=\"left\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(end)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"justified\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(endDescription)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"15%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"20%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML(blank)+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"13%\" align=\"right\" colspan=\"1\" class=\"textGoodThin\">"+nlapiEscapeXML((-(creditTotal)))+"</td>";
		htmlMsg = htmlMsg + "<td border-width=\"0.1\" width=\"14%\" align=\"center\" colspan=\"1\" class=\"textGoodThin\">"+blank+"</td>";
		//CSV
		lineItemDataArray[csvCount] = end+","+endDescription+","+blank+","+blank+","+blank+","+(creditTotal)+","+blank;
		csvCount = csvCount + 1;
		htmlMsg = htmlMsg + "</tr>";

		var blank = '-';
		var endDescription = 'Closing Balance';
		htmlMsg = htmlMsg + "<tr>";
		htmlMsg = htmlMsg + "<td align=left width=10%>"+end+"</td>";
		htmlMsg = htmlMsg + "<td align=justified width=15%>"+nlapiEscapeXML(endDescription)+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=15%>"+blank+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=20%>"+blank+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=13%>"+blank+"</td>";
		htmlMsg = htmlMsg + "<td align=center width=13%>"+blank+"</td>";
		htmlMsg = htmlMsg + "<td align=right width=14%>"+nlapiEscapeXML(currentBalance)+"</td>";
		nlapiLogExecution('Debug','currentBalance %%%% ',currentBalance);
		if(currentBalance < 0)
		{
			//CSV
			var newBalance = (-(currentBalance));
			lineItemDataArray[csvCount] = end+","+endDescription+","+blank+","+blank+","+newBalance+","+blank+","+blank;
			csvCount = csvCount + 1;
		}
		else if(currentBalance > 0)
		{
			//CSV
			lineItemDataArray[csvCount] = end+","+endDescription+","+blank+","+blank+","+blank+","+currentBalance+","+blank;
			csvCount = csvCount + 1;
		}
		else
		{
			//CSV
			lineItemDataArray[csvCount] = end+","+endDescription+","+blank+","+blank+","+blank+","+blank+","+currentBalance;
			csvCount = csvCount + 1;
		}

		htmlMsg = htmlMsg + "</tr>";

		htmlMsg = htmlMsg + "</table>";
		htmlMsg = htmlMsg + "</CENTER>";

		htmlMsg = htmlMsg + "</body>";
		htmlMsg = htmlMsg + "";
		htmlMsg = htmlMsg + "</HTML>";
		//nlapiLogExecution('DEBUG', 'ScheduleTest', 'htmlMsg = ' +htmlMsg);
		//response.write(htmlMsg);

		//CSV
		var MultiArray = '';
		for (var icnt = 0; lineItemDataArray != null && icnt < lineItemDataArray.length; icnt++)
		{
			nlapiLogExecution('DEBUG', 'PrintCSV', 'icnt='+icnt);
			if(icnt == 0)
			{
				MultiArray = lineItemDataArray[icnt];
			} // END if(icnt == 0)
			else
			{
			  	MultiArray = MultiArray+"\n"+lineItemDataArray[icnt];
			} // END else
		} // END for (var icnt = 0; lineItemDataArray != null && icnt < lineItemDataArray.length; icnt++)
		var UnixTimeStamp = GetCurrentTimeStamp();
		nlapiLogExecution('DEBUG', 'UnixTimeStamp', 'UnixTimeStamp = '+ UnixTimeStamp);
		var fileObj = nlapiCreateFile('VendorStatement'+'_'+UnixTimeStamp+'.csv', 'CSV', MultiArray);//strName
		nlapiLogExecution('DEBUG', 'CSV', 'CSV fileObj = '+fileObj);
		
		var emailBody = 'PFA';
		nlapiSendEmail(userId, empEmail, 'Vendor Statement', emailBody , null, null, null, fileObj);
		var pageNotFound = '<html><body> Vendor Statement sent in CSV format to email - '+empEmail+'</body></html>';
	    response.write(pageNotFound);
			
		} // result null
		
		
		
	} // END if (request.getMethod() == 'GET')
	}
	
    
	
} // END HTMLDesignPopUpXL(request, response)

function getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate)
{
	
	//====== CODE FOR GETTIMNG OPENING BALANCE ======
	var lastInternalID = 0;
	var totalAmount = 0;
	totalAmount = parseFloat(totalAmount);

		var filters = new Array();
		
		filters.push( new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastInternalID));
		filters.push( new nlobjSearchFilter('name', null, 'is', vendor));
		//filters.push( new nlobjSearchFilter('trandate', null, 'within', custCreatedDate, opBalanceEndDate));
		filters.push( new nlobjSearchFilter('trandate', null, 'before', opBalanceEndDate));
		if(locationname != '' && locationname != null)
		{
			filters.push( new nlobjSearchFilter('location', null, 'is', locationname));
		} // END if(locationname != '' && locationname != null)
		var searchResults = nlapiSearchRecord('transaction', 'customsearch_vendor_statement', filters, null);

		var breakFlag = 0;
		if(searchResults != null && searchResults != '')
		{
			var length = searchResults.length;
			nlapiLogExecution('DEBUG', 'getOpeningBalance', 'search result length = ' +length);

			for (var counter = 0; counter < searchResults.length; counter++)
			{
				//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'counter for begin= ' +counter);
				var result = searchResults[counter];
				var columns = result.getAllColumns();
				var columnLen = columns.length;

				var internalID = '';
				var amount = '';
				var type = '';
				var previousInternalID = '';

				for (var i = 0; i < columnLen; i++)
				{
					var column = columns[i];
					var fieldName = column.getName();
					var value = result.getValue(column);

					if(fieldName == 'internalid')
					{
						previousInternalID = lastInternalID;
						//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'previousInternalID = ' +previousInternalID);
						internalID = value;
						//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'internalID = ' +internalID);
					}

					if(fieldName == 'amount')
					{
						amount = value;
						amount = parseFloat(amount);
						amount = Math.round(amount*100)/100;
					}

					if(fieldName == 'type')
					{
						type = value;
					}
					if(counter >= 900)
					{
						if(previousInternalID != internalID)
						{
							breakFlag = 1;
						}
					}
				}
				if(breakFlag == 1)
				{
					break;
				}
				lastInternalID = internalID;

				totalAmount = parseFloat(totalAmount) - parseFloat(amount);
				totalAmount = Math.round(totalAmount*100)/100;
				
			}// END for (var counter = 0; counter < searchResults.length; counter++)
			
		} // END if(searchResults != null && searchResults != '')
	totalAmount = parseFloat(totalAmount);
	totalAmount = Math.round(totalAmount*100)/100;
	var getOpeningBalancefunction = parseFloat(totalAmount);
	//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'getOpeningBalancefunction = ' +getOpeningBalancefunction);

	return totalAmount;
} // END getOpeningBalance(vendor, locationname, custCreatedDate, opBalanceEndDate)

function GetCurrentTimeStamp()
{
	//====CODE TO GET CURRENT TIME STAMP ===========
	return (new Date().getTime());
} // END GetCurrentTimeStamp()

function replaceCommaWithSemiColon(data)
{
	//==== CODE FOR REPLACING COMMA WITH SEMICOLON ======
	if(data != null && data != '')
	{
		var numberOfCommas = 0;
		var lengthData = data.length;
		for(var n = 0; n < lengthData; n++)
		{
			var currentChar = data.charAt(n);
			if(currentChar == ",")
			numberOfCommas = numberOfCommas + 1;
		}
		var trimmedData = data;
	    for(var m = 0; m < numberOfCommas; m++)
	    {
	    	var trim = trimmedData.replace(",",";");
	    	trimmedData = trim;
	    }
	}
    return trimmedData;
} // END replaceCommaWithSemiColon(data)

function afterReplaceCR(custAddr)
{
    //======= CODE TO REPLACE SPECIAL SYMBOL ===
	var custAddrString = custAddr.toString();

    custAddrString = custAddrString.replace(/\r/g, '<BR/>');/// /g

    return custAddrString;

} // END afterReplaceCR(custAddr)

function afterReplaceAddComma(custAddr)
{
    
	/// ====== CODE TO ADD COMMA AFTER REPLACE ====
	var custAddrString = custAddr.toString();

    custAddrString = custAddrString.replace(/\r/g, '-');/// /g
    custAddrString = custAddrString.replace(/\n/g, '--');/// /g

    return custAddrString;
} // END afterReplaceAddComma(custAddr)

function getLocationName(locationname)
{
	//======= CODE TO GET LOCATION NAME  ==========
	var Filters = new Array();
	var Columns = new Array();
	
	Filters.push( new nlobjSearchFilter('internalid', null, 'is', locationname));
	Columns.push( new nlobjSearchColumn('internalid'));
	Columns.push( new nlobjSearchColumn('name'));

	var SearchResults = nlapiSearchRecord('location', null, Filters, Columns);
	
	if(SearchResults != null)
	{
		var InternalID = SearchResults[0].getValue('internalid');
		var LocName = SearchResults[0].getValue('name');
		//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'InternalID = ' +InternalID);
		//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'LocName = ' +LocName);
		return LocName;
	}

	

	
} // END getLocationName(locationname)

function getCurrency(custCurrency)
{
	// ====CODE TO GET THE CURRENCY ======
	var Filters = new Array();
	var Columns = new Array();
	Filters.push( new nlobjSearchFilter('internalid', null, 'is', custCurrency));
	Columns.push( new nlobjSearchColumn('internalid'));
	Columns.push( new nlobjSearchColumn('name'));

	var SearchResults = nlapiSearchRecord('currency', null, Filters, Columns);
    if(SearchResults != null)
	{
		var InternalID = SearchResults[0].getValue('internalid');
		var Currency = SearchResults[0].getValue('name');
		//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'InternalID = ' +InternalID);
		//nlapiLogExecution('DEBUG', 'getOpeningBalance', 'Currency = ' +Currency);
	
		return Currency;
		
	}
	
	
}// END getCurrency(custCurrency)

function SearchGlobalParameter()	
	{
    
        // == GET TDS GLOBAL PARAMETER ====
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '')	
		 {
            for (var i = 0; i < s_serchResult.length; i++) 	
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Invoice ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }

function getOpeningBal(vendor, locationname, custCreatedDate, opBalanceEndDate)
{
	nlapiLogExecution('Debug','In function opBalanceEndDate ',opBalanceEndDate);
	var Filters = new Array();
	var Columns = new Array();
	
	Filters.push( new nlobjSearchFilter('trandate', null, 'onorbefore', opBalanceEndDate));
	Filters.push( new nlobjSearchFilter('name', null, 'anyof', vendor));
	if(locationname)
		Filters.push( new nlobjSearchFilter('location', null, 'anyof', locationname));
	Columns.push( new nlobjSearchColumn("entity",null,"GROUP"));
	Columns.push( new nlobjSearchColumn("amount",null,"SUM"));

	var SearchResults = nlapiSearchRecord('transaction', 'customsearch_vendor_statement_2', Filters, Columns);

	if(SearchResults)
	{
		var openingBal = SearchResults[0].getValue("amount",null,"SUM");
		nlapiLogExecution('Debug','Opening Balence in function',openingBal)
		return openingBal;
	}
}