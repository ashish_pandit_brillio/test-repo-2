/**
 * @author Jayesh
 */

function userEventBeforeSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {
			
			var projectId = nlapiGetFieldValue('custrecord_project_name_tm_transaction');

			if (projectId) {
				
				var emp_intrnal_id = 0;
				var emp_name = nlapiGetFieldValue('custrecord_resource_tm_transaction');
				var s_employee_name_id = emp_name.split('-');
				var s_employee_name_split = s_employee_name_id[0];
							
				var filters_emp = new Array();
				filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
				
				var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, null);
				if(a_results_emp)
				{
					emp_intrnal_id = a_results_emp[0].getId();
				}
				
				nlapiSetFieldValue('custrecord_resource_id_tm', emp_intrnal_id);
				var sub_id = nlapiLookupField('employee',emp_intrnal_id,'subsidiary');
				nlapiSetFieldValue('custrecord_emp_subsidiary_tm_transaction', sub_id);
				var onsite_offsite = '';
				if(sub_id == 3)
				{
					onsite_offsite = 'Offsite';
				}
				else
				{
					onsite_offsite = 'Onsite';
				}
				nlapiSetFieldValue('custrecord_emp_onsite_tm_transaction', onsite_offsite);
				
				var startDate = nlapiGetFieldValue('custrecord_strt_date_tm_transaction');
				var d_startDate = nlapiStringToDate(startDate);
				var d_endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
				var endDate = nlapiDateToString(d_endDate, 'date');
				var today = new Date();
				
				//var emp_practice = nlapiGetFieldValue('custrecord_emp_practice_tm_transaction');
				//var emp_practice_id = nlapiGetFieldValue('custrecord_practice_id_tm_transaction');
				var emp_id = nlapiGetFieldValue('custrecord_resource_tm_transaction');
				var employee_name = nlapiGetFieldText('custrecord_resource_tm_transaction');
				employee_name = emp_id.split('-');
				employee_name = employee_name[0];
				nlapiSetFieldValue('custrecord_end_date_tm_transaction', endDate);
				nlapiSetFieldValue('custrecord_month_tm_transaction',getMonthName(startDate));
				nlapiSetFieldValue('custrecord_year_tm_transaction', getYear(d_startDate));

				var cust_territory = nlapiLookupField('customer', nlapiGetFieldValue('custrecord_customer_tm_transaction'),'territory');
				nlapiSetFieldValue('custrecord_region_tm_transaction',cust_territory);
				
				var projectData = nlapiLookupField('job', projectId, [
				        'jobbillingtype', 'custentity_t_and_m_monthly',
				        'customer', 'entityid', 'custentity_project_currency', 'subsidiary' ]);

				nlapiSetFieldValue('custrecord_currency_tm_transaction',projectData.custentity_project_currency);

				var proj_is_monthly = projectData.custentity_t_and_m_monthly;
				nlapiSetFieldValue('custrecord_tm_monthly_transaction',projectData.custentity_t_and_m_monthly);
				
				var projectCurrencyValue = nlapiGetFieldValue('custrecord_currency_tm_transaction');
				var projectCurrencyText = nlapiGetFieldText('custrecord_currency_tm_transaction');

				var usd_inr_rate = nlapiGetFieldValue('custrecord_usd_inr_rate_conversion');
				var usd_gbp_rate = nlapiGetFieldValue('custrecord_usd_gbp_conversion');
				
				//if (projectData.jobbillingtype == 'TM'
				//        && projectData.custentity_t_and_m_monthly == 'F') {

					// if its a past month, do revenue recognition
					if (d_endDate < today) {
						nlapiSetFieldValue('custrecord_recognised', 'T');
						revenueRecognition(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue,employee_name,emp_id,usd_inr_rate,usd_gbp_rate);
					} else { // else projection
						revenueProjection(projectData, projectId, startDate,
						        endDate, d_startDate, d_endDate,
						        projectCurrencyText, projectCurrencyValue,employee_name,emp_intrnal_id,proj_is_monthly,usd_inr_rate,usd_gbp_rate);
					}
				//}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeSubmit', err);
	}
}

function revenueRecognition(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue,employee_name,emp_id,usd_inr_rate,usd_gbp_rate)
{
	try {
		var recognizedAmount = 0;
		var projectName = projectData.entityid;

		
		nlapiLogExecution('debug', 'employee_name', employee_name);
		
		var filters_search_transaction = new Array();
		filters_search_transaction[0] = new nlobjSearchFilter('custcolprj_name', null, 'startswith', projectName);
		
		if (employee_name)
		{
			filters_search_transaction[1] = new nlobjSearchFilter('custcol_employeenamecolumn', null, 'startswith', employee_name);
		}
		else
		{
			filters_search_transaction[1] = new nlobjSearchFilter('custcol_employeenamecolumn', null, 'isempty');
		}
		//filters_search_transaction[3] = new nlobjSearchFilter('department', null, 'anyof', emp_practice_id);//"@NONE@"
		
		if (projectData.subsidiary == 2) {
			filters_search_transaction.push(new nlobjSearchFilter('account', null, 'anyof', [ '647' ]));
		} else { // BTPL
			filters_search_transaction.push(new nlobjSearchFilter('account', null, 'anyof', ['644', '647']));
		}
		
		filters_search_transaction.push(new nlobjSearchFilter('formuladate', null, 'within', startDate,endDate).setFormula('{trandate}'));
		
		filters_search_transaction.push(new nlobjSearchFilter('memorized', null, 'is', 'F'));
			
		var jeSearch = searchRecord('transaction', null, filters_search_transaction, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group'),
				new nlobjSearchColumn('custcol_employeenamecolumn', null, 'group'),
				new nlobjSearchColumn('department', null, 'group') ]);
		
		nlapiLogExecution('debug', 'search data', JSON.stringify(jeSearch));
		
		var practiceWiseBreakup = {};
		//var inactivePracticeMapping = getInactivePracticeMapping();

		if (jeSearch) {
			
			usd_inr_rate = 1/parseFloat(usd_inr_rate);
			usd_gbp_rate = 1/parseFloat(usd_gbp_rate);
			
			for(var srch_indx=0; srch_indx<jeSearch.length; srch_indx++)
			{
				var currency = jeSearch[srch_indx].getText('currency', null, 'group');
				var amount = jeSearch[srch_indx].getValue('fxamount', null, 'sum');
				var practice = jeSearch[srch_indx].getText('department', null, 'group');
				var practice_id = jeSearch[srch_indx].getValue('department', null, 'group');
				var emp_name = jeSearch[srch_indx].getText('custcol_employeenamecolumn', null, 'group');
				var emp_id = jeSearch[srch_indx].getValue('custcol_employeenamecolumn', null, 'group');
				nlapiLogExecution('audit','srch currency:- ',currency);
				if(currency == 'GBP')
				{
					amount = parseFloat(amount) * parseFloat(usd_gbp_rate); 
				}
				else if(currency == 'USD')
				{
					amount = parseFloat(amount) * parseFloat(1);
				}
				else if(currency == 'INR')
				{
					amount = parseFloat(amount) * parseFloat(usd_inr_rate);
				}
				
				recognizedAmount = parseFloat(recognizedAmount) + parseFloat(amount);
				
			}
			nlapiLogExecution('debug', 'search data recognizedAmount',recognizedAmount);
			var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',endDate);
			var convertedAmount = usdExchangeRate * recognizedAmount;
			nlapiSetFieldValue('custrecord_usd_revenue', recognizedAmount);
		
		}
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueRecognition', err);
		throw err;
	}
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function revenueProjection(projectData, projectId, startDate, endDate,
        d_startDate, d_endDate, projectCurrencyText, projectCurrencyValue,employee_name,emp_id,proj_is_monthly,usd_inr_rate,usd_gbp_rate)
{
	try {
		
		usd_inr_rate = 1/parseFloat(usd_inr_rate);
		usd_gbp_rate = 1/parseFloat(usd_gbp_rate);
		
		// get project holiday list (only working days holiday)
		var holidayList = getWorkingDayHolidays(startDate, endDate, projectId,
		        projectData.customer);

		// get practice wise revenue projection
		
		if(proj_is_monthly == 'T')
		{
			nlapiLogExecution('audit','inside monthly');
			var practiceWiseBreakUp = getDepartmentWiseBreakup_Monthly(projectId, startDate, endDate, holidayList,
       			d_startDate, d_endDate,emp_id);
		}
		else
		{
			var practiceWiseBreakUp = getDepartmentWiseBreakup_Hourly(projectId,
		        startDate, endDate, holidayList, d_startDate, d_endDate,emp_id);
		}
		

		var usdExchangeRate = nlapiExchangeRate(projectCurrencyText, 'USD',
		        endDate);

		// add new lines
		var convertedAmount = 0;
		for ( var practice in practiceWiseBreakUp) {
			if (practiceWiseBreakUp[practice] < 0) {
				practiceWiseBreakUp[practice] = 0;
			}
			//convertedAmount += usdExchangeRate * practiceWiseBreakUp[practice];
			convertedAmount += practiceWiseBreakUp[practice];
		}
		nlapiLogExecution('audit','projected amount:- ',convertedAmount);
		if(projectCurrencyText == 'GBP')
		{
			convertedAmount = parseFloat(convertedAmount) * parseFloat(usd_gbp_rate);
		}
		else if(projectCurrencyText == 'USD')
		{
			convertedAmount = parseFloat(convertedAmount) * parseFloat(1);
		}
		else if(projectCurrencyText == 'INR')
		{
			convertedAmount = parseFloat(convertedAmount) * parseFloat(usd_inr_rate);
		}
		
		nlapiLogExecution('audit','projected amount after convert:- ',convertedAmount);
		nlapiSetFieldValue('custrecord_usd_revenue', convertedAmount);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'revenueProjection', err);
		throw err;
	}
}

function getDepartmentWiseBreakup_Hourly(project, startDate, endDate, holidayList,
        d_startDate, d_endDate,emp_id)
{
	try {
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('project', null, 'anyof', project),
		                new nlobjSearchFilter('custeventrbillable', null, 'is',
		                        'T'),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        endDate),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        startDate),
						new nlobjSearchFilter('resource', null, 'anyof',
		                        emp_id) ],
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('department', 'employee'),
		                new nlobjSearchColumn('subsidiary', 'employee'),
		                new nlobjSearchColumn('custevent3'),
		                new nlobjSearchColumn('custentity_hoursperday', 'job') ]);
		
		nlapiLogExecution('debug', 'allocationSearch search data', JSON.stringify(allocationSearch));
		
		var departmentMainObject = {};
		var inactivePracticeMapping = getInactivePracticeMapping();
		
		if (allocationSearch) {

			allocationSearch
			        .forEach(function(allocation) {
				        var department = allocation.getValue('department',
				                'employee');

				        if (inactivePracticeMapping[department]) {
					        department = inactivePracticeMapping[department];
				        }

				        if (!departmentMainObject[department]) {
					        departmentMainObject[department] = 0;
				        }

				        var allocationStartDate = nlapiStringToDate(allocation
				                .getValue('startdate'));
				        var allocationEndDate = nlapiStringToDate(allocation
				                .getValue('enddate'));

				        var newStartDate = allocationStartDate > d_startDate ? allocationStartDate
				                : d_startDate;
				        var newEndDate = allocationEndDate < d_endDate ? allocationEndDate
				                : d_endDate;

				        departmentMainObject[department] += getEmployeeRevenueBetweenDates_Hourly(
				                newStartDate, newEndDate, parseFloat(allocation
				                        .getValue('custevent3')), allocation
				                        .getValue('custentity_hoursperday',
				                                'job'), allocation
				                        .getValue('percentoftime'),
				                holidayList, allocation.getValue('subsidiary',
				                        'employee'));
			        });
		}

		//nlapiLogExecution('debug', 'departmentMainObject', JSON
		//        .stringify(departmentMainObject));
		return departmentMainObject;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentWiseBreakup', err);
		throw err;
	}
}

function getDepartmentWiseBreakup_Monthly(project, startDate, endDate, holidayList,
        d_startDate, d_endDate,emp_id)
{
	try {
		nlapiLogExecution('audit','project'+project,emp_id);
		var allocationSearch = nlapiSearchRecord(
		        'resourceallocation',
		        null,
		        [
		                new nlobjSearchFilter('project', null, 'anyof', project),
		                new nlobjSearchFilter('custeventrbillable', null, 'is',
		                        'T'),
		                new nlobjSearchFilter('startdate', null, 'notafter',
		                        endDate),
		                new nlobjSearchFilter('enddate', null, 'notbefore',
		                        startDate),
						new nlobjSearchFilter('resource', null, 'anyof',
		                        emp_id) ],
		        [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('percentoftime'),
		                new nlobjSearchColumn('department', 'employee'),
		                new nlobjSearchColumn('subsidiary', 'employee'),
		                new nlobjSearchColumn('custevent_monthly_rate') ]);

		var departmentMainObject = {};
		var inactivePracticeMapping = getInactivePracticeMapping();

		if (allocationSearch) {
		
			var noOfDaysInMonth = getWorkingDays(d_startDate, d_endDate);
			var inactivePracticeMapping = getInactivePracticeMapping();
			
			allocationSearch
			        .forEach(function(allocation) {
				        var department = allocation.getValue('department',
				                'employee');

				        if (inactivePracticeMapping[department]) {
					        department = inactivePracticeMapping[department];
				        }

				        if (!departmentMainObject[department]) {
					        departmentMainObject[department] = 0;
				        }

				        var allocationStartDate = nlapiStringToDate(allocation
				                .getValue('startdate'));
				        var allocationEndDate = nlapiStringToDate(allocation
				                .getValue('enddate'));

				        var newStartDate = allocationStartDate > d_startDate ? allocationStartDate
				                : d_startDate;
				        var newEndDate = allocationEndDate < d_endDate ? allocationEndDate
				                : d_endDate;

				        departmentMainObject[department] += getEmployeeRevenueBetweenDates_Monthly(
				                newStartDate, newEndDate, parseFloat(allocation
				                        .getValue('custevent_monthly_rate')),noOfDaysInMonth, allocation
				                        .getValue('percentoftime'),
				                holidayList, allocation.getValue('subsidiary',
				                        'employee'));
			        });
		}

		//nlapiLogExecution('debug', 'departmentMainObject', JSON
		//        .stringify(departmentMainObject));
		return departmentMainObject;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentWiseBreakup', err);
		throw err;
	}
}

function getEmployeeRevenueBetweenDates_Hourly(d_startDate, d_endDate, hourlyRate,
        hoursPerDay, percentAllocation, holidayList, subsidiary)
{
	try {
		//nlapiLogExecution('debug', 'hourlyRate', hourlyRate);
		//nlapiLogExecution('debug', 'hoursPerDay', hoursPerDay);

		if (!hoursPerDay) {
			hoursPerDay = 8;
		}

		var noOfWorkingsDays = getWorkingDays(d_startDate, d_endDate);
		//nlapiLogExecution('debug', 'noOfWorkingsDays', noOfWorkingsDays);

		// holidays between allocation dates
		var holidayCount = 0;

		holidayList.forEach(function(holiday) {

			if (d_startDate <= holiday.Date && holiday.Date <= d_endDate
			        && subsidiary == holiday.Subsidiary) {
				holidayCount += 1;
			}
		});

		// final working days
		noOfWorkingsDays -= holidayCount;
		//nlapiLogExecution('debug', 'holidayCount', holidayCount);

		percentAllocation = parseFloat((percentAllocation.split('%')[0].trim())) / 100;
		//nlapiLogExecution('debug', 'percentAllocation', percentAllocation);

		var noOfWorkingHours = noOfWorkingsDays * percentAllocation
		        * hoursPerDay;
		nlapiLogExecution('debug', 'noOfWorkingHours', noOfWorkingHours);

		var revenue = noOfWorkingHours * hourlyRate;
		//nlapiLogExecution('debug', 'revenue', revenue);
		return revenue;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeRevenueBetweenDates', err);
		throw err;
	}
}

function getEmployeeRevenueBetweenDates_Monthly(d_startDate, d_endDate, monthlyRate,
        noOfDaysInMonth, percentAllocation, holidayList, subsidiary)
{
	try {
		
		var dailyRate = monthlyRate / noOfDaysInMonth;
		var noOfWorkingsDays = getWorkingDays(d_startDate, d_endDate);
		percentAllocation = parseFloat((percentAllocation.split('%')[0].trim())) / 100;
		//nlapiLogExecution('debug', 'noOfWorkingsDays', noOfWorkingsDays);
		
		var noOfWorkingDays = noOfWorkingsDays * percentAllocation;
		var revenue = noOfWorkingDays * dailyRate;
		
		return revenue;
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeRevenueBetweenDates', err);
		throw err;
	}
}

function getWorkingDays(d_startDate, d_endDate) {
	try {
		var numberOfWorkingDays = 0;

		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);

			if (currentDate > d_endDate) {
				break;
			}

			if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
				numberOfWorkingDays += 1;
			}
		}

		return numberOfWorkingDays;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getWorkingDays', err);
		throw err;
	}
}

function getWorkingDayHolidays(start_date, end_date, project, customer) {
	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date);
	} else {
		return get_customer_holidays(start_date, end_date, customer);
	}
}

function get_company_holidays(start_date, end_date) {
	try {
		var holiday_list = [];

		var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
		        'customsearch_company_holiday_search',
		        [ new nlobjSearchFilter('custrecord_date', null, 'within',
		                start_date, end_date) ], [
		                new nlobjSearchColumn('custrecord_date'),
		                new nlobjSearchColumn('custrecordsubsidiary') ]);

		if (search_company_holiday) {

			for (var i = 0; i < search_company_holiday.length; i++) {
				var holidayDate = nlapiStringToDate(search_company_holiday[i]
				        .getValue('custrecord_date'));

				if (holidayDate.getDay() != 0 && holidayDate.getDay() != 6) {
					holiday_list.push({
					    Subsidiary : search_company_holiday[i]
					            .getValue('custrecordsubsidiary'),
					    Date : holidayDate
					});
				}
			}
		}

		return holiday_list;
	} catch (err) {
		nlapiLogExecution('ERROR', 'get_company_holidays', err);
		throw err;
	}
}

function get_customer_holidays(start_date, end_date, customer) {
	var holiday_list = [];

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [
	                new nlobjSearchColumn('custrecordholidaydate', null,
	                        'group'),
	                new nlobjSearchColumn('custrecordcustomersubsidiary', null,
	                        'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			var holidayDate = nlapiStringToDate(search_customer_holiday[i]
			        .getValue('custrecordholidaydate', null, 'group'));

			if (holidayDate.getDay() != 0 && holidayDate.getDay() != 6) {
				holiday_list.push({
				    Subsidiary : search_customer_holiday[i].getValue(
				            'custrecordcustomersubsidiary', null, 'group'),
				    Date : holidayDate
				});
			}
		}
	}

	return holiday_list;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getYear(currentDate) {
	return Math.round(currentDate.getFullYear()).toString();
}

function getInactivePracticeMapping() {
	return {
	    282 : 324,
	    292 : 325,
	    307 : 325,
	    452 : 451,
	    305 : 320,
	    432 : 328,
	    339 : 320,
	    306 : 325,
	    308 : 316,
	    345 : 326,
	    314 : 458,
	    439 : 322,
	    341 : 322,
	    333 : 326,
	    302 : 326,
	    404 : 316,
	    289 : 316,
	    402 : 326,
	    293 : 316,
	    431 : 325,
	    408 : 407,
	    337 : 318,
	    285 : 326,
	    428 : 316,
	    294 : 316,
	    309 : 316,
	    429 : 316,
	    335 : 316,
	    310 : 316,
	    300 : 326,
	    303 : 326,
	    301 : 326,
	    286 : 326,
	    287 : 326,
	    304 : 316,
	    344 : 324,
	    440 : 327
	};
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
