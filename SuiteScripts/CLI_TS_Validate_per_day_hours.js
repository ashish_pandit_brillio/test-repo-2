/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Oct 2016     shruthi.l
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
	// Error array
	var a_errors = new Array();
	
	// Date
	var str_start_date = nlapiGetFieldValue('startdate');
	var d_start_date = new Date(str_start_date);
	
	var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday' ];
	var lineitemcount=nlapiGetLineItemCount('timegrid');
	
	var totalHours = nlapiGetFieldValue('totalhours').substring(':');
	totalHours = totalHours.split(':');
	totalHours = totalHours[0];
	
	var timeArray;
	var timeEntered ;
	//var flag=0;
	var a_project = new Array();
	// Contains the timesheet data projectwise and daywise
	var a_data = new Array();
	
	for(var i_day = 0; i_day < 7; i_day++)
	{
		var o_day_details = new Object();
		o_day_details.ST_Hours = 0;
		o_day_details.OT_Hours = 0;
		o_day_details.Holiday_Hours = 0;
		o_day_details.Leave_Hours = 0;
		o_day_details.Floating_Holiday_Hours = 0;
		o_day_details.OT_Hours_For_Project = new Object();
		
		a_data[i_day] = o_day_details;
	}
	
	for(var i=1;i<=lineitemcount;i++){
		var i_line_Project = nlapiGetLineItemValue('timegrid', 'customer', i);
		if(getProjectIndx(a_project, i_line_Project) == -1)
		{
			var o_project_details = getProjectDetails(i_line_Project);
			o_project_details.project_name = nlapiGetLineItemValue('timegrid', 'customer_display', i);
			a_project.push(o_project_details);
		}
		nlapiSelectLineItem('timegrid', i);
		
		var i_line_Item = nlapiGetLineItemValue('timegrid', 'item', i);
		var i_item_type = getItemType(i_line_Item);
		
		for (var j = 0; j < 7; j++) 
		{
			
			var i_hours = nlapiGetLineItemValue('timegrid', 'timeentry_hours_' + j, i);
			
			if(a_data[j].OT_Hours_For_Project[i_line_Project.toString()] == undefined)
			{
				a_data[j].OT_Hours_For_Project[i_line_Project.toString()] = 0.0;
			}
			
			if(_is_Valid(i_hours))
				{
					
					i_hours	=	_correct_time(i_hours);
					
					switch(i_item_type)
					{
						case ST_ITEM:
							a_data[j].ST_Hours += i_hours;
							break;
						case OT_ITEM:
							a_data[j].OT_Hours += i_hours;
							a_data[j].OT_Hours_For_Project[i_line_Project] += i_hours;
							break;
						case LEAVE_ITEM:
							a_data[j].Leave_Hours += i_hours;
							break;
						case HOLIDAY_ITEM:
							a_data[j].Holiday_Hours += i_hours;
							break;
						case FLOATING_HOLIDAY_ITEM:
							a_data[j].Floating_Holiday_Hours += i_hours;
							break;
					}
				}
		}
	}
	
	

	var i_weekly_total = 0.0;
	var i_weekly_OT_total = 0.0;
	var i_weekly_holiday_total = 0.0;
	var i_weekly_leave_total = 0.0;
	var i_week_limit = 0.0;

	var i_week_limit_project = null;
	
	// Validate Data
	for(var i_project_indx = 0; i_project_indx < a_project.length; i_project_indx++)
		{
			var i_day_limit = a_project[i_project_indx].i_day_limit;
			if(i_project_indx == 0)
				{
					i_week_limit = a_project[i_project_indx].i_week_limit;
					i_week_limit_project = i_project_indx;
				}
			else
				{
					i_week_limit = a_project[i_project_indx].i_week_limit > i_week_limit?a_project[i_project_indx].i_week_limit:i_week_limit;
					i_week_limit_project = i_project_indx;
				}
			
			var i_OT_Check = a_project[i_project_indx].i_OT_Check;
			
			for(i_day = 0; i_day < 7; i_day++)
				{
					var i_ST_Hours = a_data[i_day].ST_Hours;
					var i_OT_Hours = a_data[i_day].OT_Hours;
					var i_OT_Hours_For_Project = a_data[i_day].OT_Hours_For_Project[i_project_indx];
					var i_Leave_Hours = a_data[i_day].Leave_Hours;
					var i_Holiday_Hours = a_data[i_day].Holiday_Hours;
					var i_Floating_Holiday_Hours = a_data[i_day].Floating_Holiday_Hours;
				
					var isWeekday = true;
					if(i_day == 0 || i_day == 6)
						{
							isWeekday = false;
						}
					if(i_project_indx == 0)
						{
							i_weekly_total += a_data[i_day].ST_Hours;
							
							if(isWeekday == true)
							{
								i_weekly_OT_total += a_data[i_day].OT_Hours;
							}
							
							i_weekly_holiday_total += a_data[i_day].Holiday_Hours;
							i_weekly_holiday_total += a_data[i_day].Floating_Holiday_Hours;
							i_weekly_leave_total += a_data[i_day].Leave_Hours;
						}
					
					// ST Hours greater than day limit
					if(i_day_limit != null && a_data[i_day].ST_Hours > i_day_limit)
						{
							var o_error = new Object();
							o_error.day = i_day;
							o_error.project_indx = i_project_indx;
							o_error.type = 'ST_HOURS_EXCEEDED_DAILY_LIMIT';
							
							a_errors.push(o_error);
						}
					
					// ST Hours on Holiday
					if(i_Holiday_Hours > 0 && i_ST_Hours > 0)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'ST_HOURS_ON_HOLIDAY'});
						}
					
					// ST Hours plus Leave Hours
					if(i_day_limit != null && i_ST_Hours + i_Leave_Hours > i_day_limit && i_Leave_Hours > 0)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'ST_PLUS_LEAVE_HOURS_EXCEED_DAY_LIMITS'});
						}
					else if(i_day_limit == null && i_ST_Hours + i_Leave_Hours > 8 && i_Leave_Hours > 0)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'ST_PLUS_LEAVE_HOURS_EXCEED_8'});
						}
					
					// Check if OT enabled for this project
					if(i_OT_Check == 'T')
						{
							// OT Validations for Weekdays
							if(isWeekday == true)
								{
									// OT Hours not allowed if daily limit is not reached
									if(i_day_limit != null && i_OT_Hours_For_Project > 0 && i_ST_Hours < 8 && i_Holiday_Hours == 0)
										{
											a_errors.push({day:i_day, project_indx:i_project_indx, type: 'DAILY_LIMIT_NOT_REACHED_FOR_OT_HOURS'});
										}
								}
							
						}
					else if(i_OT_Hours_For_Project > 0)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'OT_HOURS_NOT_ALLOWED_FOR_THIS_PROJECT'});
						}
					
					// Check Leave Hours
					if(i_day_limit != null && i_Leave_Hours > i_day_limit)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'LEAVE_HOURS_EXCEED_DAY_LIMIT'});
						}
					
					// Check Holiday Hours
					if(i_Holiday_Hours > 8)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'HOLIDAY_HOURS_GREATER_THAN_8'});
						}
					
					if(i_ST_Hours > 0 && i_Holiday_Hours > 0)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'HOLIDAY_HOURS_NOT_ALLOWED_WITH_ST_HOURS'});
						}
					
					// Check Floating Holiday Hours
					if(i_Floating_Holiday_Hours > 0 && (i_ST_Hours > 0 || i_OT_Hours > 0 || i_Holiday_Hours > 0 || i_Leave_Hours > 0))
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'FLOATING_HOLIDAY_HOURS_NOT_ALLOWED_WITH_ST_HOURS'});
						}
					
					// Check Floating Holiday Hours Greater Than 8
					if(i_Floating_Holiday_Hours > 8)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'FLOATING_HOLIDAY_HOURS_GREATER_THAN_8'});
						}
					
					// Check if Floating Holiday Availed
					if(i_Floating_Holiday_Hours > 0 && checkFloatingHoliday() == false)
						{
							a_errors.push({day:i_day, project_indx:i_project_indx, type: 'FLOATING_HOLIDAY_AVAILED'});
						}
				}
		}
	
	// Check ST Weekly Hours
	if(i_week_limit != 0.0 && i_weekly_total > i_week_limit)
		{
			a_errors.push({day:null, project_indx:i_week_limit_project, type: 'ST_HOURS_EXCEEDED_WEEKLY_LIMIT'});
		}
	
	// OT Hours not allowed as weekly limit is not exceeded
	if(i_weekly_OT_total > 0.0 && i_weekly_total < (40.0 - i_weekly_holiday_total - i_weekly_leave_total))
		{
			a_errors.push({day:null, project_indx:i_week_limit_project, type: 'WEEKLY_LIMIT_NOT_REACHED_FOR_OT_HOURS'});
		}
	
	// Display Alert
	if(a_errors.length > 0)
		{
		
			var a_days = ['Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
			var a_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			
			var i_project_indx = -1;
			var strDisplay = '';
			for(var i = 0; i < a_errors.length; i++)
				{
					if(a_errors[i].project_indx != i_project_indx)
						{
							i_project_indx = a_errors[i].project_indx;
							strDisplay += '\nProject: ' + a_project[i_project_indx].project_name;
							strDisplay += '\n------------------------------------------------------------\n'; 
						}
					
					if(a_errors[i].day != null)
					{
						var d_date = nlapiAddDays(d_start_date, a_errors[i].day);
						strDisplay += a_days[d_date.getDay()] + ', ' + a_months[d_date.getMonth()] + ' ' + d_date.getDate() + ': ';
					}
				
					strDisplay += getMessage(a_errors[i].type, a_project[i_project_indx]) + '\n';
				}
			
			alert(strDisplay);
			
			return false;
		}
return true;
	
	if(timeArray.custentity_otapplicable=='T'){
		 
		 return true;
	 }
 
		if(timeArray.custentity_hoursperweek){
			if(parseFloat(totalHours) > parseFloat(timeArray.custentity_hoursperweek)){
				alert("Please enter hours per week Lesser than or equal to : " + timeArray.custentity_hoursperweek);
				return false;
			}
		}
		
    return true;
}

function _is_Valid(obj) //
{
	if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}


function getProjectDetails(i_project_id) {
	
	// Get Project Details
	var a = new Array();
	a['User-Agent-x'] = 'SuiteScript-Call';
	var resposeObject = '';
	
	resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=289&deploy=1&custscript_pro_id=' + i_project_id +
	'&custscript_req_type=project_details', null, a);
	var status = resposeObject.getBody();
			
	//var r_project_rec = nlapiLoadRecord('job', i_project_ID);
	
	if (_is_Valid(status)) //
	{
		var o_new_project = new Object();
		
		o_new_project.i_day_limit = status.split('$')[1]; 
		if(_is_Valid(o_new_project.i_day_limit) == false || o_new_project.i_day_limit == 'null')
			{
				o_new_project.i_day_limit = null;
			}
		
		o_new_project.i_week_limit = status.split('$')[2]; 
		if(_is_Valid(o_new_project.i_week_limit) == false || o_new_project.i_week_limit == 'null')
			{
				o_new_project.i_week_limit = 0.0;
			}
		else
			{
				o_new_project.i_week_limit = parseFloat(o_new_project.i_week_limit);
			}
		
		o_new_project.i_OT_Check = status.split('$')[3]; 
		o_new_project.i_project_id = i_project_id;
		
		return o_new_project;
		//a_project.push(o_new_project);
		//a_Project_Details.push(i_project_ID + '$' + i_day_limit + '$' + i_week_limit + '$' + i_OT_Check)
	}
	
	return null;
}


function getItemType(i_item_id) {
	switch(i_item_id)
	{
		case '2222':
		case '2221':
			return ST_ITEM;
		case '2479':
			return LEAVE_ITEM;
		case '2425':
			return OT_ITEM;
		case '2480':
			return HOLIDAY_ITEM;
		case '2481':
			return FLOATING_HOLIDAY_ITEM;
	}
}

function getMessage(message_id, o_project)
{
	switch(message_id)
	{
		case 'ST_HOURS_EXCEEDED_DAILY_LIMIT':
			return 'Hours entered are more than maximum limit ' + o_project.i_day_limit;
		case 'ST_HOURS_ON_HOLIDAY':
			return 'ST hours are not allowed on Holiday. Please enter the hours in OT task.';
		case 'ST_PLUS_LEAVE_HOURS_EXCEED_DAY_LIMITS':
			return 'The sum of hours in ST task and Leave task exceeds the day limit ' + o_project.i_day_limit;
		case 'ST_PLUS_LEAVE_HOURS_EXCEED_8':
			return 'The sum of hours in ST task and Leave task exceeds 8';
		case 'WEEKLY_LIMIT_NOT_REACHED_FOR_OT_HOURS':
			return 'You cannot enter OT time as your Standard Time limit (' + 40 + ') per week is not reached!';
		case 'DAILY_LIMIT_NOT_REACHED_FOR_OT_HOURS':
			return 'You cannot enter OT time as your Standard Time limit (' + 8 + ') for a day is not reached';
		case 'OT_HOURS_NOT_ALLOWED_FOR_THIS_PROJECT':
			return 'OT Hours are not allowed for this project!!!';
		case 'LEAVE_HOURS_EXCEED_DAY_LIMIT':
			return 'You cannot enter more than ' + o_project.i_day_limit + ' hours in Leave task!';
		case 'HOLIDAY_HOURS_GREATER_THAN_8':
			return 'You cannot enter more than 8 hours in Holiday task!';
		case 'HOLIDAY_HOURS_NOT_ALLOWED_WITH_ST_HOURS':
			return 'Holiday hours are not allowed on working day. Please enter the hours in OT task';
		case 'FLOATING_HOLIDAY_HOURS_NOT_ALLOWED_WITH_ST_HOURS':
			return 'No other entries are allowed on a floating holiday';
		case 'FLOATING_HOLIDAY_HOURS_GREATER_THAN_8':
			return 'You cannot enter more than 8 hours in Floating Holiday task';
		case 'FLOATING_HOLIDAY_AVAILED':
			return 'You have already availed Floating Holiday for this year.';
		case 'ST_HOURS_EXCEEDED_WEEKLY_LIMIT':
			return 'Weekly hour limit "' + o_project.i_week_limit + '" exceeded.';
	}
}

function getProjectIndx(a_project, i_project_id)
{
	var i_project_count = a_project.length;
	
	for(var i = 0; i < i_project_count; i++)
		{
			if(a_project[i].i_project_id == i_project_id)
				{
					return i;
				}
		}
	
	return -1;
}


function checkFloatingHoliday()
{
	var filters = new Array();
	var emp_ID = nlapiGetFieldValue('employee');
	//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'emp_ID : ' + emp_ID);
	
	//emp_ID = 3243;
	//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'emp_ID : ' + emp_ID);
	
	filters[filters.length] = new nlobjSearchFilter('custrecord_empid', null, 'anyof', emp_ID);
	filters[filters.length] = new nlobjSearchFilter('custrecord_fh_date_availed', null, 'within', 'thisyear');
	
	var columns = new Array();
	columns[columns.length] = new nlobjSearchColumn('custrecord_empid');
	columns[columns.length] = new nlobjSearchColumn('created');
	
	var search_result = nlapiSearchRecord('customrecord_floating_holiday', null, filters, columns);
	
	if (_is_Valid(search_result)) //
	{
		if (search_result.length > 0) //
		{
			return false;
		}
		else
		{
			return true;//nlapiLogExecution('DEBUG', 'fld_changed_8hrs_val', 'search_result.length is ' + search_result.length);
		}
	}
	
	return true;
}


function _correct_time(t_time) //
{
	// function is used to correct the time 
	
	if (_is_Valid(t_time)) //
	{
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
		
		var hrs = t_time.split(':')[0]; 
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		
		if (t_time.indexOf(':') > -1) //
		{
			var mins = t_time.split(':')[1]; 
			//nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
			
			if (_is_Valid(mins)) //
			{
				mins = parseFloat(mins) / parseFloat('60'); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
				
				hrs = parseFloat(hrs) + parseFloat(mins); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);
			}
		}
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		return hrs;
	}
	else //
	{
		return 0;
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
	}
}
