/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Apr 2020     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

    try {
        nlapiLogExecution('debug', 'type', type);
        var currentContext = nlapiGetContext();
        nlapiLogExecution('debug', 'Context', currentContext.getExecutionContext());
		var obj_dataRow = {};   	//added on 02/04/2021
		var arr_dataRow =[];
		 if (type == 'delete') 
		 {
           // dataRow.push(nlapiGetRecordId());
		   //Added logic to send the timesheetid and email and weekstart date on 02/04/2021
		   var i_Timesheet_Internal_Id = nlapiGetRecordId();	
			var obj_Old_record = nlapiGetOldRecord()	
			var i_Employee_Id = obj_Old_record.getFieldValue('employee');	
			var d_Start_Date = obj_Old_record.getFieldValue('startdate');	
			var s_Employee_Email = nlapiLookupField('employee',i_Employee_Id,'email');	
			nlapiLogExecution('debug', 's_Employee_Email==', s_Employee_Email);	
			nlapiLogExecution("DEBUG", "d_Start_Date==== i_Employee_Id ",d_Start_Date+'====='+i_Employee_Id);	
			
			obj_dataRow = {	
				'timesheetId':i_Timesheet_Internal_Id,	
				'workEmail':s_Employee_Email,	
				'weekStartDate' : d_Start_Date	
			};	
			nlapiLogExecution("DEBUG", "obj_dataRow==",JSON.stringify(obj_dataRow));	
			arr_dataRow.push(obj_dataRow);
            //************************Token Generation steps*****************************//
            var commonHeader = {
            		"Content-Type": "application/json",
           		    "accept": "application/json"
            };
            
            var tokenURL = 'https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/authenticate';
            var tokenMethod = 'POST';
            var tokenBody = {
            		 "username":"ankit",
            		 "password":"ankit" 

            };
            var tokenResponse = nlapiRequestURL(tokenURL, JSON.stringify(tokenBody), commonHeader, null, tokenMethod);
            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
            var tken = JSON.parse(tokenResponse.body);
          //  nlapiLogExecution("DEBUG", "tokenResponse token :", (tken.token)); //JSON.stringify
            //********************END***********************************//
            
            var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/timesheet?apitype=1003";
            var method = "POST";
            var headers = {
                	"Authorization": "Bearer "+tken.token,
                    "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
                    "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
                    "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
                    "x.api.user": "113056",
                    "content-type": "application/json",
                    "Accept": "application/json"

                };
			var JSONBody = JSON.stringify(arr_dataRow, replacer);
		 
            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
            nlapiLogExecution("DEBUG", "Response Body :", (responseObj.body)); //JSON.stringify

            if (!(parseInt(JSON.parse(responseObj.getCode())) == 200)) {
            	var a_emp_attachment = new Array();
        		a_emp_attachment['entity'] = 83134;
                // send Email to fuel
                // send Email to fuel
                var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                var emailSub = "Error in TS Deletion #" + nlapiGetRecordId(); //onthego
                var emailBoody = "Team," + "\r\n" + "Error while updating the TS details into OTG server Timesheet #" + nlapiGetRecordId() + "\r\n";
                nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment, file);
            }
        }
    }catch (err) {
    	var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var emailSub = "Error in TS - NS Script Error #" + nlapiGetRecordId(); //onthego
		 var emailBoody = "Team," + "\r\n" + "Error while sending the TS details into OTG server while deletion #" + nlapiGetRecordId() + "\r\n" + "Please find below NS error." + err + "\r\n";
       // nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'TS Notification Error - OTG', err);
    }
}

function userEventAfterSubmit(type) 
{

    try {
    	var user = nlapiGetUser();
    	var load_TS = '';
   nlapiLogExecution('debug', 'After Submit type', type);
        var currentContext = nlapiGetContext();
        nlapiLogExecution('debug', 'After Submit Context', currentContext.getExecutionContext());
        var oldRecord	=	nlapiGetOldRecord();
		var newRecord	=	nlapiGetNewRecord();
	//********** Need to update the user if change the tokens in feature **********//
	
		if(parseInt(user) != parseInt(32162))
		{ //Hard Coded for Mobile Admin User 
			
        if ((currentContext.getExecutionContext() == 'userinterface') || (currentContext.getExecutionContext() == 'suitelet' && type == 'create') ||
            (currentContext.getExecutionContext() == 'userevent' && (type == 'approve' || type == 'reject' || type == 'savesubmit'|| type=='edit'))) {
            //Load timesheet record
            var dataRow = [];
            var body = {};
            var approver_email = '';
            var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
            var temp = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
           // if(!_logValidation(oldRecord.getFieldValue('approvalstatus')))
             load_TS = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
            //else
             //load_TS = 	nlapiGetNewRecord();
            var emp = load_TS.getFieldText('employee');
            var emp_id = load_TS.getFieldValue('employee');
            var emp_lookUP = nlapiLookupField('employee', emp_id, ['email']);
            var emp_email = emp_lookUP.email;
            var emp_TS_approver = load_TS.getFieldText('custrecord_ts_time_approver');
            var i_emp_TS_approver = load_TS.getFieldValue('custrecord_ts_time_approver');
            if (i_emp_TS_approver)
                approver_email = nlapiLookupField('employee', i_emp_TS_approver, 'email');
            var weekStart = load_TS.getFieldValue('startdate');
            var weekEnd = load_TS.getFieldValue('enddate');
            var totalHrs = load_TS.getFieldValue('totalhours');
            var approvalstatus = load_TS.getFieldText('approvalstatus');
			nlapiLogExecution('DEBUG','approvalstatus',approvalstatus);
            var iscomplete = load_TS.getFieldValue('iscomplete');
            var iscomplete = newRecord.getFieldValue('iscomplete');
            var isSubmitted = newRecord.getFieldValue('submitted'); 
			nlapiLogExecution('DEBUG','isSubmitted',isSubmitted);
           /* if(isSubmitted == 'T' && approvalstatus == 'Open')
            	approvalstatus = 'Pending Approval';
			
            if(oldRecord.getFieldText('approvalstatus') == 'Rejected' && isSubmitted == 'T')
            	approvalstatus = 'Pending Approval';*/
            	
            body.timeSheetID = nlapiGetRecordId();
            body.employee = emp;
            body.employee_internalID = emp_id;
            body.email = emp_email;
            body.weekStartDate = weekStart;
            body.weekEndDate = weekEnd;
            body.totalHours = totalHrs;
            body.status = approvalstatus;
            body.tsApprover = emp_TS_approver;
            body.approver_email = approver_email;
            body.TimeSheetBlock = false;
            body.DayLimit = 8;
            body.WeekLimit = 40;
            body.otlimit = 24;
            
            var JSON_Body = {
            taskHoliday: 'Holiday (Project Task)',
            taskLeave: 'Leave (Project Task)',
            taskOt: 'OT (Project Task)',
            taskHolidayId: '2480',
            taskLeaveId: '2479',
            taskOtid: '2425',
            };
            body.taskItems = JSON_Body;

            //Line Data
        var lineCount = load_TS.getLineItemCount('timeitem');
           
            var dataRow_Items = [];
            for (var inx = 1; inx <= lineCount; inx++) {
            	var dataRow_TS_Lines = [];
                load_TS.selectLineItem('timeitem', inx);
						var project = load_TS.getCurrentLineItemText('timeitem','customer');
                        var i_project = load_TS.getCurrentLineItemValue('timeitem','customer');
                        var task = load_TS.getCurrentLineItemText('timeitem','casetaskevent');
                        var service_item = load_TS.getCurrentLineItemValue('timeitem','item');
						var hour_day = 8;
                        var hours_week = 40;
						if (i_project) {
						var proj_lookUp = nlapiLookupField('job', i_project, ['custentity_hoursperday', 'custentity_hoursperweek']);
						hour_day = proj_lookUp.custentity_hoursperday;
						hours_week = proj_lookUp.custentity_hoursperweek;
						}
                //day wise data
                for (var n = 0; n < 7; n++) {

                    var o_sub_record_view = ''
                    var sub_day = days[n];
					
                    var o_sub_record_view = load_TS.getCurrentLineItemValue('timeitem', 'timebill'+n);
                    if (o_sub_record_view) {
                       
                        
                        var hrs_sub = load_TS.getCurrentLineItemValue('timeitem','hours'+n);
                        var day_Date = nlapiDateToString(nlapiAddDays(nlapiStringToDate(load_TS.getFieldValue('startdate'),'d'),n));//GET DAY

                        //Service Item
                        var JSON_TS_Lines = {

                            Day: sub_day,
                            Date: day_Date,
                            Project: project,
                            Task: task,
                            ServiceItem: service_item,
                            Hrs: hrs_sub,
                            DayLimit: hour_day,
                            WeekLimit: hours_week

                        };
                        dataRow_Items.push(JSON_TS_Lines);
                        //projectList.push( o_sub_record_view.getFieldValue('customer'));
                    }
                }
            // dataRow_Items.push(dataRow_TS_Lines);
            }
            
            dataRow.push(body);
            dataRow.push(dataRow_Items);
            nlapiLogExecution('DEBUG', 'Response', JSON.stringify(dataRow));
            //Calling ONTHEGO 
            var JSONBody = JSON.stringify(dataRow, replacer);
            
            //**************Authentication steps******************//
            var commonHeader = {
            		"Content-Type": "application/json",
           		    "accept": "application/json"
            };
            /*var registerURL = 'https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/register';
            var registerMethod = 'POST';
            var registerBody = {
            		 "userName":"ankit", 
            		 "email":"ankit@brillio.com", 
            		 "password":"ankit" 
            };
            var registerResponse = nlapiRequestURL(registerURL, JSON.stringify(registerBody), commonHeader, null, registerMethod);
            nlapiLogExecution("DEBUG", "registerResponse Body :", (registerResponse.body)); //JSON.stringify
*/            
            //************************Token Generation steps*****************************//
            var tokenURL = 'https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/authenticate';
            var tokenMethod = 'POST';
            var tokenBody = {
            		 "username":"ankit",
            		 "password":"ankit" 

            };
            var tokenResponse = nlapiRequestURL(tokenURL, JSON.stringify(tokenBody), commonHeader, null, tokenMethod);
            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
            var tken = JSON.parse(tokenResponse.body);
          //  nlapiLogExecution("DEBUG", "tokenResponse token :", (tken.token)); //JSON.stringify
            //********************END***********************************//
            
           //If old record is blank, then url should be 10001 
           if(!_logValidation(oldRecord.getFieldValue('approvalstatus')))
           // if(isObjectEmpty(oldRecord))
            {
            var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/timesheet?apitype=1001";
        }
        else{
        	var url = "https://onthegoactivitylog.azurewebsites.net/onthego-v2-payrollmanagement/api/timesheet?apitype=1002";
        }
            var method = "POST";
            var headers = {
            	"Authorization": "Bearer "+tken.token,
                "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
                "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
                "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
                "x.api.user": "113056",
                "content-type": "application/json",
                "Accept": "application/json"

            };
            var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
            nlapiLogExecution("DEBUG", "Response Body :", (responseObj.body)); //JSON.stringify
            

            if (!(parseInt(JSON.parse(responseObj.getCode())) == 200)) {
            	var a_emp_attachment = new Array();
        		a_emp_attachment['entity'] = 83134;
                // send Email to fuel
                // send Email to fuel
                var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
                var emailSub = "Error in TS Updation #" + nlapiGetRecordId(); //onthego
                var emailBoody = "Team," + "\r\n" + "Error while updating the TS details into OTG server #" + nlapiGetRecordId() + "\r\n" + "\r\n" +  'Employee :'+emp + "\r\n" +'and the week start date: '+ weekStart + "\r\n" + responseObj.body + "\r\n" + "\r\n" + "Please find attached payLoad." + "\r\n";
                nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment, file);
            }

        }
    }
    } catch (err) {
    	var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var emailSub = "Error in TS - NS Script Error #" + nlapiGetRecordId(); //onthego
		 var emailBoody = "Team," + "\r\n" + "Error while sending the TS details into OTG server #" + nlapiGetRecordId() + "\r\n" + "\r\n" +  'Employee :'+emp +'and the week start date: '+ weekStart + "\r\n" + "\r\n" + "Please find below NS error." + err + "\r\n";
        nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'TS Notification Error - OTG', err);
    }
}

function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function isEmptyObj(object) {
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            return false;
        }
    }
}