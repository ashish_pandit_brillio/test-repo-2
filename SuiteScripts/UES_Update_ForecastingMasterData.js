var SEARCHMODULE;
var RECORD;
var TASK;
/**
 *@NApiVersion 2.x
 *@NModuleScope Public
 *@NScriptType UserEventScript
 */
define(['N/search', 'N/task'], runUserEvent);
function runUserEvent(search, task) {
	SEARCHMODULE = search;
 	TASK=task;
	var returnObj = {};
	//returnObj.beforeLoad = beforeLoad;
	returnObj.afterSubmit = afterSubmit;
	//returnObj.beforeSubmit = beforeSubmit;
	return returnObj;
}
 

function afterSubmit(context) {
	
	var Currentdate=new Date(); //Get Currentdate
	var o_Record=context.newRecord;
	var o_request_data=new Object();
	var flag=false;
	var i_date;
	var i_subsidiary;
	var s_recordtype;
	var i_customer;
	if(o_Record.type=='customrecord_holiday'){
	/*o_request_data.i_date=o_Record.getValue({"fieldId": "custrecord_date"});//get the Holiday
	o_request_data.i_subsidiary=o_Record.getValue({"fieldId": "custrecordsubsidiary"});
    o_request_data.s_recordtype=o_Record.type;
	*/
	i_date=o_Record.getValue({"fieldId": "custrecord_date"});//get the Holiday
	i_subsidiary=o_Record.getValue({"fieldId": "custrecordsubsidiary"});
    s_recordtype=o_Record.type;
	log.debug('o_Record',JSON.stringify(o_Record));
	flag=true;
	}
	else if(o_Record.type=='customrecordcustomerholiday'){
		i_date=o_Record.getValue({"fieldId": "custrecordholidaydate"});//get the Holiday
		i_customer=o_Record.getValue({"fieldId": "custrecord13"});
		i_subsidiary=o_Record.getValue({"fieldId": "custrecordcustomersubsidiary"});
		s_recordtype=o_Record.type;
			flag=true;
	}
	else{
		return ;
	}
	
var data_parameters=
{'custscript_i_customer':i_customer,
'custscript_holiday_date':i_date,
'custscript_i_subsidiary':i_subsidiary,
'custscript_project_holiday_type':s_recordtype
};
log.debug('data_parameters',data_parameters);
var scriptTask = TASK.create({taskType: TASK.TaskType.SCHEDULED_SCRIPT});
scriptTask.scriptId = 'customscript_sch_update_forecastingmaste';//2105;
scriptTask.params = data_parameters;
var scriptTaskId = scriptTask.submit();

}