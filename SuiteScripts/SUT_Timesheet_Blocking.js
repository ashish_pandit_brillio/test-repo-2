/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Jul 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {

		if (request.getMethod() == 'GET') {
			getForm(request);
		} else {
			postForm(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function getForm(request) {
	try {

		// get the current script deployment status
		var scriptDeployment = nlapiLoadRecord('scriptdeployment', '886');
		var deploymentStatus = scriptDeployment.getFieldValue('isdeployed');

		var form = nlapiCreateForm('Block / Unblock Timesheet');

		var currentStatusField = form.addField('custpage_current_status',
		        'text', 'Current Timesheet Status').setDisplayType('inline');

		var nextStatusField = form.addField('custpage_new_status', 'checkbox',
		        'New Timesheet Status').setDisplayType('hidden');

		var taPmEmailLink = form.addField('custpage_url', 'url', '')
		        .setDisplayType('inline').setLinkText("Get TA PM Email List")
		        .setDefaultValue(
		                nlapiResolveURL('SUITELET',
		                        'customscript_sut_ext_ta_pm_email_list',
		                        'customdeploy_sut_ext_ta_pm_email_list'));

		if (deploymentStatus == 'T') {
			currentStatusField.setDefaultValue('Disabled');
			nextStatusField.setDefaultValue('F');
			form.addSubmitButton('Enable Timesheet');
		} else {
			currentStatusField.setDefaultValue('Enabled');
			nextStatusField.setDefaultValue('T');
			form.addSubmitButton('Disable Timesheet');
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'getForm', err);
		throw err;
	}
}

function postForm(request) {
	try {
		var nextTimesheetStatus = request.getParameter('custpage_new_status');
		var scriptDeployment = nlapiLoadRecord('scriptdeployment', '886');
		scriptDeployment.setFieldValue('isdeployed', nextTimesheetStatus);
		nlapiSubmitRecord(scriptDeployment);
		nlapiLogExecution('debug', 'Timesheet Status Changed',
		        nextTimesheetStatus == 'T' ? 'Blocked' : 'Unblocked');

		try {
			// call the scheduled script to trigger unblock /block mail
			if (nextTimesheetStatus != 'T') {
				nlapiScheduleScript('customscript_sch_mp_employee_notify',
				        'customdeploy_sch_mp_notify_unblock');
			} else {
				nlapiScheduleScript('customscript_sch_mp_employee_notify',
				        'customdeploy_sch_mp_notify_block');
			}
		} catch (ex) {
			nlapiLogExecution('ERROR',
			        'Failed to schedule the email trigger script', ex);
		}

		var context = nlapiGetContext();
		response.sendRedirect('SUITELET', context.getScriptId(), context
		        .getDeploymentId());
	} catch (err) {
		nlapiLogExecution('ERROR', 'postForm', err);
		throw err;
	}
}
