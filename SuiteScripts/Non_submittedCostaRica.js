/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var id = request.getParameter('id');
		var param = request.getParameter('param');
		if (id == 'Export') {

			/*var firstDay = new Date(nlapiStringToDate(param).getFullYear(),
					nlapiStringToDate(param).getMonth(), 1);
			var lastDay = new Date(nlapiStringToDate(param).getFullYear(),
					nlapiStringToDate(param).getMonth() + 1, 0);
			firstDay = nlapiDateToString(firstDay, 'mm/dd/yyyy');
			lastDay = nlapiDateToString(lastDay, 'mm/dd/yyyy');*/
			var arraytoCSV = _SearchData(firstDay, lastDay);
			var creat_csv_text = create_CSV(arraytoCSV);

			var fileName = 'CostaRica '+lastDay + '.csv';
			var file = nlapiCreateFile(fileName, 'CSV', creat_csv_text);
			response.setContentType('CSV', fileName);
			response.write(file.getValue());

		}
		if (request.getMethod() == 'GET' && id !='Export') //
		{

			var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS-Costa-Rica');
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :');
			var sublist1 = form.addSubList('record', 'list',
					'Costa-Rica-Report');
			form.setScript('customscript1390');

			form.addSubmitButton('Submit');
			response.writePage(form);

		} else {
			if(id !='Export'){
			var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS-Costa-Rica');
			form.setScript('customscript1390');//change
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :').setDefaultValue(
					request.getParameter('custpage_startdate'));
			var sublist1 = form.addSubList('record', 'list',
					'Costa-Rica-Report');
			form.setScript('customscript1390');
			var toDate = request.getParameter('custpage_startdate');
			var date1 = nlapiStringToDate(date);
            var fromDate = nlapiAddDays(date1, -55);
			
			
			sublist1.addField('custevent_employee', 'text', 'Employee');
			
			sublist1.addField('custevent_wk1', 'text', 'WK1 ST').setDefaultValue(fromDate);

			sublist1.addField('custevent_wk2', 'text', 'WK2 ST').setDefaultValue(nlapiAddDays(fromDate, 7));
			
			sublist1.addField('custevent_wk3', 'text', 'WK3 ST').setDefaultValue(nlapiAddDays(fromDate, 14));
			
			sublist1.addField('custevent_wk4', 'text', 'WK4 ST')setDefaultValue(nlapiAddDays(fromDate, 21));
			
			sublist1.addField('custevent_wk5', 'text', 'WK5 ST')setDefaultValue(nlapiAddDays(fromDate, 28));
			
			sublist1.addField('custevent_wk6', 'text', 'WK6 ST')setDefaultValue(nlapiAddDays(fromDate, 35));
			
			sublist1.addField('custevent_wk7', 'text', 'WK7 ST')setDefaultValue(nlapiAddDays(fromDate, 42));
			
			sublist1.addField('custevent_wk8', 'text', 'WK8 ST')setDefaultValue(toDate);
			
			var array = _SearchData(fromDate, toDate);
			
			sublist1.setLineItemValues(array);
			
			form.addSubmitButton('Submit');
			
			form.addButton('custombutton', 'Export As CSV',
					'fxn_generatePDF();');
			
			response.writePage(form);
		}
		}
	} catch (e) {
		nlapiLogExecution('DEBUG', 'Search results exception ', +e)
	}
}

function _SearchData(fromDate, toDate) {

		var week2 = nlapiAddDays(fromDate, 7);
		var week2 = nlapiAddDays(fromDate, 14);
		var week2 = nlapiAddDays(fromDate, 21);
		var week2 = nlapiAddDays(fromDate, 28);
		var week2 = nlapiAddDays(fromDate, 35);
		var week2 = nlapiAddDays(fromDate, 42);
		
		var emp = new Array();
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('date', null, 'within', [
				fromDate, toDate ]);

		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('employee');
		a_columns[1] = new nlobjSearchColumn('item');
		a_columns[5] = new nlobjSearchColumn('timesheet');
		a_columns[6] = new nlobjSearchColumn('startdate', 'timesheet');

		var searchresult_T = searchRecord('timeentry',
				'customsearch_salnonstdrptte_4', a_filters, a_columns);

		var st = JSON.stringify(searchresult_T);
		searchresult_T = JSON.parse(st);
		var arywk = [];
		var j = 0;
		var status ;
		var itemArray = new Array();
		

		for ( var i in searchresult_T) {
			
		try{

			var curr_col = searchresult_T[i].columns;

			if (i != 0) {
				var temp_col = searchresult_T[i - 1].columns;
				
				if (timesheet != curr_col.timesheet) {
					
				}
					fromDate = nlapiAddDays(fromDate, 7);
					
				}
			
			if (i != 0) {
				if (temp_col.employee.name != curr_col.employee.name) {
					
					itemArray[m] = {
						'custevent_employee' : temp_col.employee.name,
						
						'custevent_wk1' : arywk[0],
						
						'custevent_wk2' : arywk[1],
						
						'custevent_wk3' : arywk[2],
						
						'custevent_wk4' : arywk[3],
						
						'custevent_wk5' : arywk[4],
						
						'custevent_wk6' : arywk[5],
						
						'custevent_wk7' : arywk[6],
						
						'custevent_wk8' : arywk[7]
						
					};
					
				}
			}

			var employee = curr_col.employee;
			var timesheetStartdate = curr_col.startdate;
			
			if (timesheetStartdate == fromDate && status != 'checked') {

				arywk[j] = 'filled';
				j =j+1;
				status = 'checked';
			}
			else if( status != 'checked'){
				arywk[j] = 'Not filled';
				j =j+1;
				status = 'checked';
			}
			return itemArray;
		
		}catch (e) {
			nlapiLogExecution('DEBUG', 'Search loop exception ', +e)
		}
}
}


function create_CSV(lists) {
	

		var html = "";

		html += "Employee,";
		html += "WK1 ST,";
		html += "WK1 OT,";
		html += "WK1 DT,";
		html += "WK1 TIMEOFF,";
		html += "WK1 HOLIDAY,";

		html += "WK2 ST,";
		html += "WK2 OT,";
		html += "WK2 DT,";
		html += "WK2 TIMEOFF,";
		html += "WK2 HOLIDAY,";

		html += "WK3 ST,";
		html += "WK3 OT,";
		html += "WK3 DT,";
		html += "WK3 TIMEOFF,";
		html += "WK3 HOLIDAY,";

		html += "WK4 ST,";
		html += "WK4 OT,";
		html += "WK4 DT,";
		html += "WK4 TIMEOFF,";
		html += "WK4 HOLIDAY,";

		html += "WK5 ST,";
		html += "WK5 OT,";
		html += "WK5 DT,";
		html += "WK5 TIMEOFF,";
		html += "WK5 HOLIDAY,";

		html += "\r\n";
	
		for (var l = 0; l < lists.length; l++) {
			var list = lists[l];
			html += "\r\n";
			for ( var i in list) {
				if(list[i] == 'undefined'|| list[i] == null){
					list[i] = 0;
				}
				

				html += list[i];//e
				html += ",";
			    
			}
		}
		
			return html;
		}


			
	

