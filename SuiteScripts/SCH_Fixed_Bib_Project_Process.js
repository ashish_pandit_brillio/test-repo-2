// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH_Fixed_bid_Project_Process.js
	Author:Swati Kurariya
	Company:Brillio
	Date:21-08-2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduled(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

//----------------------------------------------------------------------------------------------
function scheduled(type) 
{//main fun start
	
	 /*  On scheduled function:

    - Generate journel entry based on selected project.
	-

FIELDS USED:

    --Field Name--				--ID--


*/
	try
	{
		/*
		var vendorList = {
		2: '209190',
		3: '209299',
		5: '209360',
		6: '209361',
		7: '209300',
		8: '209358',
		9: '209359',
		10: '209301',
		21: '209302',
		23: '209303',
	}*/
      
      var vendorList = { };

var default_vendor_mappingSearch = nlapiSearchRecord("customrecord_default_vendor_mapping",null,
[
],
[
new nlobjSearchColumn("custrecord_subsidiary_je"),
new nlobjSearchColumn("custrecord_default_vendor")
]
);

for (var i_index = 0; i_index < default_vendor_mappingSearch.length; i_index++)
{
    var subsidiary_je = default_vendor_mappingSearch[i_index].getValue('custrecord_subsidiary_je');
    var default_vendor_je = default_vendor_mappingSearch[i_index].getValue('custrecord_default_vendor');
    
    vendorList[subsidiary_je] = default_vendor_je;
                
}
		var acc='';
		var d_last_date; 
	
	 var project_id_arr = nlapiGetContext().getSetting('SCRIPT','custscript_project_arr');
	 nlapiLogExecution('DEBUG','project_id_arr',project_id_arr);
	 
	 var d_month = nlapiGetContext().getSetting('SCRIPT','custscript_month');
	 nlapiLogExecution('DEBUG','d_month',d_month);
	 
	 var d_year = nlapiGetContext().getSetting('SCRIPT','custscript_year');
	 nlapiLogExecution('DEBUG','d_year',d_year);
	 
	 var d_date=nlapiGetContext().getSetting('SCRIPT','custscript_date');
	 nlapiLogExecution('DEBUG','d_date',d_date);//custscript_arrcount
	 
	// var i_arr_count=nlapiGetContext().getSetting('SCRIPT','custscript_arrcount');
	// nlapiLogExecution('DEBUG','i_arr_count',i_arr_count);//
	 
	 var res = project_id_arr.split(",");
	 //-----------------------------------------------------------------------------------------------
	
		for(var ab=0 ;ab<res.length ; ab++)
		{//for start
		 
		 if(res[ab] == '' || res[ab] == null || res[ab] == 'undefined')
			 {
			 
			 }
		 else
			{//else start
					var project_id=res[ab];
					 nlapiLogExecution('DEBUG','project_id',project_id);
					 
					var project_load=nlapiLoadRecord('job', project_id);
					 
					 var dr_acc=project_load.getFieldValue('custentity_fpr_draccount');
					// nlapiLogExecution('DEBUG','dr_acc',dr_acc);
					 
					 var cr_acc=project_load.getFieldValue('custentity_fpr_craccount');
					// nlapiLogExecution('DEBUG','cr_acc',cr_acc);
					 
					 var subsi=project_load.getFieldValue('subsidiary');
					// nlapiLogExecution('DEBUG','subsi',subsi);
					 
					 var class_name=project_load.getFieldValue('custentity_vertical');
					 //nlapiLogExecution('DEBUG','class_name',class_name);
					
					 var customer_name=project_load.getFieldText('parent');
					// nlapiLogExecution('DEBUG','customer_name',customer_name);
					 
					 var project_name=project_load.getFieldValue('entityid');
					// nlapiLogExecution('DEBUG','project_name',project_name);
					 
					 var company_name=project_load.getFieldValue('companyname');
					// nlapiLogExecution('DEBUG','company_name',company_name);
					 
					 
					 var project_description=project_name+'-'+company_name;
					// nlapiLogExecution('DEBUG','project_description',project_description);
					 //-----------------------------------------------------------------------------------------------
					 
					
					 
					 //----------------------------input revenue record search----------------------------------------
					 var rev_rec_filter=new Array();
					 rev_rec_filter[0]=new nlobjSearchFilter('custrecord_fpr_parent', null, 'is', project_id);
					 rev_rec_filter[1]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is', d_month);
					 rev_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_year', null, 'is', d_year);
					 rev_rec_filter[3]=new nlobjSearchFilter('custrecord_select', null, 'is', 'F');
					 
					 var rev_rec_column=new Array();
					 rev_rec_column[0]=new nlobjSearchColumn('custrecord_fpr_per_completed');
					 rev_rec_column[1]=new nlobjSearchColumn('custrecord_fpr_ano_milestone');
					 
					 var rev_rec_search=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', null, rev_rec_filter,rev_rec_column);
					 if(rev_rec_search != null)
					{
						 var sum_effort_expend=0;
						for(var ks=0 ; ks<rev_rec_search.length ; ks++)
						{
								var rev_rec_id=rev_rec_search[ks].getId();
								nlapiLogExecution('DEBUG','rev_rec_id------------>',rev_rec_id);
								nlapiLogExecution('DEBUG','rev_rec_search.length', rev_rec_search.length);
								var per_completed=parseFloat(rev_rec_search[ks].getValue('custrecord_fpr_per_completed'));
								nlapiLogExecution('DEBUG', 'per_completed', per_completed);
								
								var milestone=parseInt(rev_rec_search[ks].getValue('custrecord_fpr_ano_milestone'));
								nlapiLogExecution('DEBUG', 'milestone', milestone);
								
								//-----------------------------------------------------------------------------
								if(per_completed == '100')
									{
									//-------------------------------------------------------
									var revenue_rec_filter=new Array();
									revenue_rec_filter[0]=new nlobjSearchFilter('custrecord_fpr_parent', null, 'is',project_id);
									revenue_rec_filter[1]=new nlobjSearchFilter('custrecord_fpr_ano_milestone', null, 'is',milestone);
									//revenue_rec_filter[2]=new nlobjSearchFilter('internalid', null,'isnot','28');
									//revenue_rec_filter[2]=new nlobjSearchFilter('custrecord_fpr_month', null, 'is',month);
								
									//var revenue_rec_column=new Array();
									//revenue_rec_column[0]=new nlobjSearchColumn('custrecord_fpr_effort_expended');
								
									var search_Revenue_Rec=nlapiSearchRecord('customrecord_fpr_input_revenue_rec','customsearch_input_rev_rec_effort_expend', revenue_rec_filter, null);//
									nlapiLogExecution('DEBUG', 'search_Revenue_Rec.length------------->', search_Revenue_Rec.length);
									
									if(search_Revenue_Rec!=null)
										{
										for(var ss=0 ; ss<search_Revenue_Rec.length ; ss++)
											{
												var new_rev_rec_id=search_Revenue_Rec[ss].getId();
												nlapiLogExecution('DEBUG', 'new_rev_rec_id------------->',new_rev_rec_id);
												if(rev_rec_id == new_rev_rec_id)
													{
													nlapiLogExecution('DEBUG', 'rev_rec_id after match------------->',rev_rec_id);
													nlapiLogExecution('DEBUG', 'new_rev_rec_id after match------------->',new_rev_rec_id);
													}
												else
													{
													nlapiLogExecution('DEBUG', 'search_Revenue_Rec.length----------->',search_Revenue_Rec.length);
													var rev_rec_search=search_Revenue_Rec[ss];
													var rev_rec_col=rev_rec_search.getAllColumns();
													var effort_expand=rev_rec_search.getValue(rev_rec_col[0]);
													sum_effort_expend=parseFloat(sum_effort_expend)+parseFloat(effort_expand);
													nlapiLogExecution('DEBUG', 'sum_effort_expend----------->', sum_effort_expend);
													}
											}
										}
									per_completed=100-parseInt(sum_effort_expend);
									nlapiLogExecution('DEBUG', 'per_completed after change logic----------->', per_completed);
									//------------------------------------------------------
									}
								//----------------------------------------------------------------------------
								
								
						 
							//-----------------------------------------------------------------------------------------------
							//---------------------------milestone record search---------------------------------------------
							var milestone_filter=new Array();
							milestone_filter[0]=new nlobjSearchFilter('custrecordfpr_projparent', null, 'is', project_id);
							milestone_filter[1]=new nlobjSearchFilter('custrecord_fpr_milestone', null, 'is', milestone);
						 
							var milestone_column=new Array();
							milestone_column[0]=new nlobjSearchColumn('custrecord_fpr_amount');
						 
							var milestone_search=nlapiSearchRecord('customrecord_fpr_milestone_wi', null, milestone_filter,milestone_column);
							if(milestone_search != null)
							{//milestone if start
						 
								for(var ll=0 ; ll<milestone_search.length ; ll++)
								{//for start
									var per_after_amt_sum=0.00;
									nlapiLogExecution('DEBUG','milestone_search.length', milestone_search.length);
									var milestone_id=milestone_search[0].getId();
									//nlapiLogExecution('DEBUG','milestone_id', milestone_id);
									
									var milestone_amt=parseFloat(milestone_search[0].getValue('custrecord_fpr_amount'));
									nlapiLogExecution('DEBUG','milestone_amt', milestone_amt);
									
									var after_per_cal_amt=((milestone_amt)*per_completed/100);
									
									var after_per_cal_amt_after_deci=parseFloat(after_per_cal_amt).toFixed(2);
									nlapiLogExecution('DEBUG','after_per_cal_amt', after_per_cal_amt);
									nlapiLogExecution('DEBUG','after_per_cal_amt_after_deci', after_per_cal_amt_after_deci);
									//---------------------------practice percentage search-----------------------------------
									 var pra_per_filter=new Array();
									 pra_per_filter[0]=new nlobjSearchFilter('custrecord_fpr_prac_project_par', null, 'is', milestone_id);
									
									 var pra_per_column=new Array();
									 pra_per_column[0]=new nlobjSearchColumn('custrecord_fpr_percentage');
									 pra_per_column[1]=new nlobjSearchColumn('custrecord_fpr_practice');
									 
									 var practice_per_search=nlapiSearchRecord('customrecord_fpr_prac_wise_re', null, pra_per_filter, pra_per_column);
									if(practice_per_search != null)
									{//if start
										
										//-------------------------------------------
										//var d_date=new Date();
										//var month=d_date.getMonth()+1;
										//var 
										if(d_month == '1' || d_month == '3' || d_month == '5'|| d_month == '7'|| d_month == '8' || d_month == '12')
											{
											last_date='31';
											}
										else if(d_month == '2')
											{
											last_date='28';
											}
										else{
											last_date='30';
											
											}
										if(d_year == '1')
											{
											var d_year_new='2014';
											}
										if(d_year == '2')
										{
											var d_year_new ='2015';
										}
										//--------------------------------------------
										nlapiLogExecution('DEBUG','practice_per_search.length',practice_per_search.length);
										
										//----------------------------create JE------------------------------------------
										var create_je=nlapiCreateRecord('journalentry');
											create_je.setFieldValue('subsidiary',subsi);
											create_je.setFieldValue('custbody_financehead','1610');
											create_je.setFieldValue('custbody_revenue_record_ref',rev_rec_id);
											create_je.setFieldValue('custbody_approvalstatusonje',2);//approved
											create_je.setFieldValue('approved','T');//approved
											create_je.setFieldValue('trandate',d_date);
											create_je.setFieldValue('custbody_milestone',milestone);//added by swati 27-03-2013	for reverse process									
										for(var ss=0 ; ss<practice_per_search.length ; ss++)
										{//for start
										  
											var i_percentage=parseFloat(practice_per_search[ss].getValue('custrecord_fpr_percentage'));
											nlapiLogExecution('DEBUG','i_percentage',i_percentage);
											
											var i_practice=parseInt(practice_per_search[ss].getValue('custrecord_fpr_practice'));
											nlapiLogExecution('DEBUG','i_practice',i_practice);
											
											var per_after_amt=parseFloat(after_per_cal_amt_after_deci)*parseFloat(i_percentage/100);
											nlapiLogExecution('DEBUG', 'per_after_amt',per_after_amt);
											
											
											var parsing_per_after_amt=parseFloat(per_after_amt).toFixed(2);
											nlapiLogExecution('DEBUG', 'parsing_per_after_amt',parsing_per_after_amt);
											
											per_after_amt_sum=parseFloat(per_after_amt_sum)+parseFloat(parsing_per_after_amt);
											
											create_je.selectNewLineItem('line');
											//create_je.setCurrentLineItemValue('line', 'account', cr_acc);
											create_je.setCurrentLineItemValue('line', 'account', cr_acc);
											create_je.setCurrentLineItemValue('line','credit', parseFloat(per_after_amt).toFixed(2));//
											
											create_je.setCurrentLineItemValue('line','department', i_practice);//pratice
											create_je.setCurrentLineItemValue('line','class', class_name);//custcolcustcol_temp_customer
											create_je.setCurrentLineItemValue('line','custcolcustcol_temp_customer', customer_name);//
											create_je.setCurrentLineItemValue('line','custcolprj_name', project_description);//
											//Added by Sitaram
									        if(_logValidation(vendorList[subsi])){
									        create_je.setCurrentLineItemValue('line', 'entity', vendorList[subsi]);												
									        }
											
											create_je.commitLineItem('line');
											
										}//for close
										
										nlapiLogExecution('DEBUG', 'per_after_amt_sum',parseFloat(per_after_amt_sum).toFixed(3));
										nlapiLogExecution('DEBUG', 'cr_acc',cr_acc);
										
										create_je.selectNewLineItem('line');
										create_je.setCurrentLineItemValue('line','account',dr_acc);
										create_je.setCurrentLineItemValue('line','debit',parseFloat(per_after_amt_sum).toFixed(2));
										//Added by Sitaram
										if(_logValidation(vendorList[subsi])){
										create_je.setCurrentLineItemValue('line', 'entity', vendorList[subsi]);												
										}
										create_je.commitLineItem('line');
										var je_id=nlapiSubmitRecord(create_je);
										nlapiLogExecution('DEBUG','je_id',je_id);
										
										//------------------------------------------------------------------------------
										var JE_Record_In_Project=nlapiCreateRecord('customrecord_journal_entry');
										JE_Record_In_Project.setFieldValue('custrecord_fpr_je',je_id);
										JE_Record_In_Project.setFieldValue('custrecord_revenue_record',rev_rec_id);
										JE_Record_In_Project.setFieldValue('custrecord_month',d_month);
										JE_Record_In_Project.setFieldValue('custrecord_year',d_year);
										JE_Record_In_Project.setFieldValue('custrecord_parent',project_id);
										var JE_Record_In_Project_id=nlapiSubmitRecord(JE_Record_In_Project);
										nlapiLogExecution('DEBUG','JE_Record_In_Project_id',JE_Record_In_Project_id);
										//----------------------------------------------------------------------------
										
										 var rev_rec_search=nlapiSearchRecord('customrecord_fpr_input_revenue_rec', null, rev_rec_filter,rev_rec_column);
										nlapiSubmitField('customrecord_fpr_input_revenue_rec', rev_rec_id, 'custrecord_select', 'T');
										
									}//if close
									//----------------------------------------------------------------------------------------
								}//for close
							}//milestone if close
							//-----------------------------------------------------------------------------------------------
						}
							
					}
			}//else close
		}//for close
	}
	catch(exception)
	{
		 nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
		   var status_rec_create=nlapiLoadRecord('customrecord_schedule_script_status','1');
		   status_rec_create.setFieldValue('custrecord_status','Fail');
		   status_rec_create.setFieldValue('custrecord_error',exception);
		   nlapiSubmitRecord(status_rec_create);
		 
		  
	}
}//main fun close

//Added by Sitaram
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
