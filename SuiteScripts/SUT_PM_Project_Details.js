/**
 * Suitelet for PM/DM to check the project details
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Aug 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
    try {
        var projectId = request.getParameter('pi');
        nlapiLogExecution('debug', 'projectId', projectId);
        var loggedUser = nlapiGetUser();
        var role = nlapiGetRole();
        var context = nlapiGetContext();

        if (loggedUser == 9673) {
            loggedUser = 1759;
        }

        if (projectId) {
            var projectRecord = nlapiLoadRecord('job', projectId);
            nlapiLogExecution('debug', 'inside - if', '');

            // get the customer client partner
            var customerClientPartner = nlapiLookupField('customer',
                projectRecord.getFieldValue('parent'),
                'custentity_clientpartner');

            var executingPracticeHead = nlapiLookupField('department',
                projectRecord.getFieldValue('custentity_practice'),
                'custrecord_practicehead');
            var i_customer_ado = nlapiLookupField('customer',
                projectRecord.getFieldValue('parent'),
                'custentity_account_delivery_owner');
            var flag_check = false;
            if (context.getDeploymentId() == "customdeploy_project_details_under_ado" && loggedUser == i_customer_ado) {
                flag_check = true;
                nlapiLogExecution('debug', 'customdeploy_project_details_under_ado', flag_check);
            }
            // check if the user is authorised to access the project information
            else if ((loggedUser == projectRecord
                    .getFieldValue('custentity_projectmanager') ||
                    loggedUser == projectRecord
                    .getFieldValue('custentity_deliverymanager') ||
                    loggedUser == projectRecord
                    .getFieldValue('custentity_clientpartner') ||
                    loggedUser == customerClientPartner ||
                    loggedUser == executingPracticeHead || role == 3) && context.getDeploymentId() != "customdeploy_project_details_under_ado") {
                nlapiLogExecution('debug', 'Not customdeploy_project_details_under_ado', flag_check);
                flag_check = true;
            } else {}

            if (flag_check == true) {
                var form = nlapiCreateForm(projectRecord
                    .getFieldValue('entityid') +
                    " " + projectRecord.getFieldValue('altname'));

                // Primary Information
                form.addFieldGroup('custpage_primary_info',
                    'Primary Information');

                form.addField('custpage_name', 'text', 'Name', null,
                        'custpage_primary_info').setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord.getFieldValue('entityid') +
                        " " +
                        projectRecord
                        .getFieldValue('altname'));

                form.addField('custpage_customer', 'text', 'Customer', null,
                        'custpage_primary_info').setDisplayType('inline')
                    .setDefaultValue(projectRecord.getFieldText('parent'));

                form.addField('custpage_status', 'text', 'Status', null,
                        'custpage_primary_info').setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord.getFieldText('entitystatus'));

                form.addField('custpage_type', 'text', 'Type', null,
                        'custpage_primary_info').setDisplayType('inline')
                    .setDefaultValue(projectRecord.getFieldText('jobtype'));

                var billingType = projectRecord.getFieldText('jobbillingtype');
                var isMonthly = projectRecord
                    .getFieldValue('custentity_t_and_m_monthly') == 'T' ? " Monthly" :
                    "";

                form.addField('custpage_billing_type', 'text', 'Billing Type',
                        null, 'custpage_primary_info').setDisplayType('inline')
                    .setDefaultValue(billingType + isMonthly);

                form
                    .addField('custpage_project_value', 'currency',
                        'Project Value', null, 'custpage_primary_info')
                    .setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord
                        .getFieldValue('custentity_projectvalue'));

                form.addField('custpage_vertical', 'text', 'Vertical', null,
                        'custpage_primary_info').setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord
                        .getFieldText('custentity_vertical'));

                form.addField('custpage_practice', 'text',
                        'Executing Practice', null, 'custpage_primary_info')
                    .setDisplayType('inline').setDefaultValue(
                        projectRecord
                        .getFieldText('custentity_practice'));

                // Project Dates
                form.addFieldGroup('custpage_project_dates', 'Project Dates');

                form.addField('custpage_startdate', 'date', 'Start Date', null,
                        'custpage_project_dates').setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord.getFieldValue('startdate'));

                form
                    .addField('custpage_enddate', 'date', 'End Date', null,
                        'custpage_project_dates')
                    .setDisplayType('inline')
                    .setDefaultValue(projectRecord.getFieldValue('enddate'));

                // Project Team
                form.addFieldGroup('custpage_project_team', 'Project Team');

                form
                    .addField('custpage_pm', 'text', 'Project Manager',
                        null, 'custpage_project_team')
                    .setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord
                        .getFieldText('custentity_projectmanager'));

                form
                    .addField('custpage_dm', 'text', 'Delivery Manager',
                        null, 'custpage_project_team')
                    .setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord
                        .getFieldText('custentity_deliverymanager'));

                form
                    .addField('custpage_cp', 'text', 'Client Partner',
                        null, 'custpage_project_team')
                    .setDisplayType('inline')
                    .setDefaultValue(
                        projectRecord
                        .getFieldText('custentity_clientpartner'));

                // allocation details
                var allocationList = getProjectAllocationDetails(projectId);
                form.addTab('custpage_allocation', 'Allocation Detail');
                var activeAllocationList = form.addSubList(
                    'custpage_active_allocation', 'staticlist',
                    'Active Allocation', 'custpage_allocation');
                addSublistColumns(activeAllocationList);
                activeAllocationList
                    .setLineItemValues(allocationList.PresentAllocation);

                var pastAllocationList = form.addSubList(
                    'custpage_past_allocation', 'staticlist',
                    'Past Allocation', 'custpage_allocation');
                addSublistColumns(pastAllocationList);
                pastAllocationList
                    .setLineItemValues(allocationList.PastAllocation);

                response.writePage(form);
            } else {
                throw "You are not authorised to access this information. Please contact your system adminstrator.";
            }
        } else {
            nlapiLogExecution('debug', 'inside - else', '');
            nlapiLogExecution('debug', 'context.getDeploymentId()', context.getDeploymentId());
            if (role == 3) {
                //	loggedUser = "10432";
            }


            var form = nlapiCreateForm("Project List");


            var filters = [];
            if (context.getDeploymentId() == "customdeploy_project_details_under_ado" ||
                context.getDeploymentId() == "customdeploy_project_details_under_dm" ||
                context.getDeploymentId() == "customdeploy_project_details_under_pm"
            ) {
                var request_customer_id = request.getParameter('custpage_customer');
                var request_projectstatus_id = request.getParameter('custpage_project_status');
                var field_customer = form.addField('custpage_customer', 'select', 'Customer');
                field_customer.setMandatory(true);
                var s_current_usertype = '';

                if (context.getDeploymentId() == "customdeploy_project_details_under_ado") {
                    filters = [
                        ["customer.custentity_account_delivery_owner", "anyof", loggedUser]
                    ];
                    s_current_usertype = 'custentity_account_delivery_owner'
                } else if (context.getDeploymentId() == "customdeploy_project_details_under_dm") {
                    filters = [
                        ["custentity_deliverymanager", "anyof", loggedUser]
                    ];
                    s_current_usertype = 'job.custentity_deliverymanager'
                } else if (context.getDeploymentId() == "customdeploy_project_details_under_pm") {
                    filters = [
                        ["custentity_projectmanager", "anyof", loggedUser]
                    ];
                    s_current_usertype = 'job.custentity_projectmanager'
                }

                // get all projects he is a ADO
                var customerList = getRelatedCustomers(loggedUser, s_current_usertype);


                var field_project_statu = form.addField('custpage_project_status', 'select', 'Status');
                if (!request_projectstatus_id) {
                    field_project_statu.addSelectOption('', '', true);
                    field_project_statu.addSelectOption('All', 'All');
                } else {
                    field_project_statu.addSelectOption('', '');
                    field_project_statu.addSelectOption('All', 'All');
                }

                field_project_statu.addSelectOption(2, "Active");
                field_project_statu.addSelectOption(1, "Closed");
                field_project_statu.addSelectOption(17, "Hold");
                field_project_statu.addSelectOption(4, "Pending");
                if (request_projectstatus_id) {

                    field_project_statu.setDefaultValue(request_projectstatus_id);
                }


                if (!request_customer_id) {
                    field_customer.addSelectOption('', '', true);
                    field_customer.addSelectOption('All', 'All');
                } else {
                    field_customer.addSelectOption('', '');
                    field_customer.addSelectOption('All', 'All');
                }

                if (customerList) {

                    for (var i = 0; i < customerList.length; i++) {
                        field_customer.addSelectOption(customerList[i].getId(),
                            customerList[i].getValue('entityid') + " " +
                            customerList[i].getValue('altname'));
                    }
                }

                if (request_customer_id) {
                    field_customer.setDefaultValue(request_customer_id);
                }



                    if (request_customer_id && request_customer_id != 'ALL') {
                        filters.push("AND");
                        filters.push(["customer", "anyof", request_customer_id]);
                    }
                    if (request_projectstatus_id && request_projectstatus_id != 'ALL') {
                        filters.push("AND");
                        filters.push(["status", "anyof", request_projectstatus_id]);
                    }
					if(filters.length>0)
					{
						filters.push('AND');
					}
                    filters.push(["isinactive", "is", "F"]);



                    var o_current_deployment = form.addField('custpage_currentdeploymentid',
                        'text', 'Current deployment').setDisplayType('hidden');

                    o_current_deployment.setDefaultValue(context.getDeploymentId()); form.setScript('customscript_cli_ado_project_details');
                    //form.addSubmitButton("Refresh");
                    //form.addButton('custpage_refresh', 'Refresh', 'refreshProjects');

                }
                else {
                    filters = [
                        ['isinactive', 'is', 'F'],
                        'and',
                        [
                            ['custentity_projectmanager', 'anyof',
                                loggedUser
                            ],
                            'or',
                            ['custentity_deliverymanager', 'anyof',
                                loggedUser
                            ],
                            'or',
                            ['custentity_clientpartner', 'anyof',
                                loggedUser
                            ]
                        ]
                    ];
                }

                var search = nlapiSearchRecord(
                    'job',
                    null,
                    filters,
                    [new nlobjSearchColumn('entityid'),
                        new nlobjSearchColumn('altname'),
                        new nlobjSearchColumn('entitystatus').setSort(),
                        new nlobjSearchColumn('jobtype'),
                        new nlobjSearchColumn('customer'),
                        new nlobjSearchColumn('custentity_projectmanager'),
                        new nlobjSearchColumn('custentity_deliverymanager')
                    ]);

                var jobList = [];
                if (search) {
                    var context = nlapiGetContext();
                    var url = nlapiResolveURL('SUITELET', context.getScriptId(), context.getDeploymentId());

                    search.forEach(function(job) {
                        jobList.push({
                            project: "<a href='" + url + "&pi=" + job.getId() +
                                "'>" + job.getValue('entityid') + " : " +
                                job.getValue('altname') + "</a>",
                            customer: job.getText('customer'),
                            status: job.getText('entitystatus'),
                            type: job.getText('jobtype'),
                            pm: generateEmpDetailsUrl(job
                                .getValue('custentity_projectmanager'), job
                                .getText('custentity_projectmanager')),
                            dm: generateEmpDetailsUrl(job
                                .getValue('custentity_deliverymanager'), job
                                .getText('custentity_deliverymanager'))
                        });
                    });
                }
                var projectList = form.addSubList("custpage_project_list",
                    "staticlist", "Project List");
                projectList.addField('project', 'text', 'Project').setDisplayType(
                    'inline');
                projectList.addField('customer', 'text', 'Customer')
                    .setDisplayType('inline');
                projectList.addField('status', 'text', 'Status').setDisplayType(
                    'inline');
                projectList.addField('type', 'text', 'Type').setDisplayType(
                    'inline');
                projectList.addField('pm', 'text', 'Project Manager')
                    .setDisplayType('inline');
                projectList.addField('dm', 'text', 'Delivery Manager')
                    .setDisplayType('inline');

                if (context.getDeploymentId() == "customdeploy_project_details_under_ado" ||
                    context.getDeploymentId() == "customdeploy_project_details_under_dm" ||
                    context.getDeploymentId() == "customdeploy_project_details_under_pm"
                ) {
                    if (request_customer_id) {
                        projectList.setLineItemValues(jobList);
                    }
                } else {
                    projectList.setLineItemValues(jobList);
                }




                response.writePage(form);
            }
        } catch (err) {
            nlapiLogExecution('error', 'suitelet', err);
            displayErrorForm(err, "Project Details");
        }
    }

    function addSublistColumns(sublist) {
        sublist.addField('resource', 'select', 'Resource', 'employee')
            .setDisplayType('inline');
        sublist.addField('internalid', 'text', 'Resource Allocation')
            .setDisplayType('hidden');
        sublist.addField('resourcename', 'text', 'Resource Name').setDisplayType(
            'hidden');
        sublist.addField('practice_text', 'text', 'Practice').setDisplayType(
            'inline');
        sublist.addField('subpractice_text', 'text', 'Sub-Practice').setDisplayType(
            'inline');
        sublist.addField('startdate', 'date', 'Start Date')
            .setDisplayType('inline');
        sublist.addField('enddate', 'date', 'End Date').setDisplayType('inline');
        sublist.addField('isbillable', 'checkbox', 'Is Billable?').setDisplayType(
            'inline');
        sublist.addField('billrate', 'currency', 'Bill Rate').setDisplayType(
            'inline');
        sublist.addField('percent', 'percent', '% Allocation').setDisplayType(
            'inline');
        sublist.addField('billingstartdate', 'date', 'Billing Start Date')
            .setDisplayType('inline');
        sublist.addField('billingenddate', 'date', 'Billing End Date')
            .setDisplayType('inline');
        sublist.addField('timeapprover', 'select', 'Time Approver', 'employee')
            .setDisplayType('inline');
        sublist.addField('expenseapprover', 'select', 'Expense Approver',
            'employee').setDisplayType('inline');
        sublist.addField('site', 'select', 'Site', 'customlistonsiteoffsitelist')
            .setDisplayType('inline');
    }

    function getProjectAllocationDetails(projectId) {
        try {
            var filters = [new nlobjSearchFilter('project', null, 'anyof',
                projectId)];

            /*
             * if (startDate && endDate) { filters.push(new
             * nlobjSearchFilter('enddate', null, 'notbefore', startDate));
             * filters.push(new nlobjSearchFilter('startdate', null, 'notafter',
             * endDate)); } else if (startDate) { filters.push(new
             * nlobjSearchFilter('enddate', null, 'notbefore', startDate)); } else
             * if (endDate) { filters.push(new nlobjSearchFilter('startdate', null,
             * 'notafter', endDate)); } else { filters.push(new
             * nlobjSearchFilter('enddate', null, 'onorafter', 'today')); }
             */

            nlapiLogExecution('debug', 'filters count', filters.length);

            var allocationSearch = searchRecord(
                'resourceallocation',
                null,
                filters,
                [
                    new nlobjSearchColumn('resource'),
                    new nlobjSearchColumn('project'),
                    new nlobjSearchColumn('startdate'),
                    new nlobjSearchColumn('enddate'),
                    new nlobjSearchColumn('custeventrbillable'),
                    new nlobjSearchColumn('percentoftime'),
                    new nlobjSearchColumn('custeventbstartdate'),
                    new nlobjSearchColumn('custeventbenddate'),
                    new nlobjSearchColumn('custeventwlocation'),
                    new nlobjSearchColumn('custevent3'),
                    new nlobjSearchColumn('custevent_practice'),
                    new nlobjSearchColumn('custevent4'),
                    new nlobjSearchColumn('custevent_allocation_status'),
                    new nlobjSearchColumn('approver', 'employee'),
                    new nlobjSearchColumn('custevent_ra_last_modified_date')
                    .setSort(true),
                    new nlobjSearchColumn('timeapprover', 'employee'),
                    //new nlobjSearchColumn('department', 'employee'),
                    new nlobjSearchColumn("department", "employee", null)
                ]);

            nlapiLogExecution('debug', 'allocation count', allocationSearch.length);

            var presentAllocation = [];
            var pastAllocation = [];
            var futureAllocation = [];

            if (allocationSearch) {
                var currentDate = new Date();
                // var weekStartDate = getWeekStartDate();
                //var practiceObj = practiceSearch()

                allocationSearch
                    .forEach(function(allocation) {
                        var endDate = nlapiStringToDate(allocation
                            .getValue('enddate'));

                        var startDate = nlapiStringToDate(allocation
                            .getValue('startdate'));

                        // var emp_practice = allocation.getValue('department', 'employee);
                        //var emp_practice = nlapiLookupField('employee', allocation.getValue('resource'),'department');
                        var subpractice = '';
                        if (currentDate > endDate) {

                            subpractice = allocation.getText("custevent_practice");


                        } else {
                            subpractice = allocation.getText("department", "employee", null);
                        }

                        //var subpractice = allocation.getText("department","employee",null);
                        var subpractice_text = ''
                        if (subpractice.split(':').length == 1) {
                            subpractice_text = subpractice
                        } else {
                            subpractice_text = subpractice.split(':')[1]
                        }
                        var practice = subpractice.split(':') ? subpractice.split(':')[0] : subpractice


                        var currentAllocation = {
                            internalid: allocation.getId(),
                            resource: allocation.getValue('resource'),
                            resourcename: allocation.getText('resource'),
                            startdate: allocation.getValue('startdate'),
                            enddate: allocation.getValue('enddate'),
                            isbillable: allocation
                                .getValue('custeventrbillable'),
                            billrate: allocation.getValue('custevent3'),
                            percent: allocation.getValue('percentoftime'),
                            location: allocation
                                .getValue('custeventwlocation'),
                            billingstartdate: allocation
                                .getValue('custeventbstartdate'),
                            billingenddate: allocation
                                .getValue('custeventbenddate'),
                            practice: allocation
                                .getValue('custevent_practice'),
                            //practice_text : allocation
                            //       .getText('custevent_practice'),
                            practice_text: practice ? practice : subpractice,
                            subpractice_text: subpractice_text,
                            timeapprover: allocation.getValue('timeapprover',
                                'employee'),
                            timeapprover_text: allocation.getText(
                                'timeapprover', 'employee'),
                            expenseapprover: allocation.getValue('approver',
                                'employee'),
                            expenseapprover_text: allocation.getText(
                                'approver', 'employee'),
                            site: allocation.getValue('custevent4'),
                            site_text: allocation.getText('custevent4'),
                            location: allocation
                                .getValue('custeventwlocation'),
                            location_text: allocation
                                .getText('custeventwlocation'),
                            ismarked: 'F',
                            lastmodified: '',
                            change: ''
                        };

                        if (currentDate > endDate) {
                            pastAllocation.push(currentAllocation);
                        } else {
                            presentAllocation.push(currentAllocation);
                        }
                    });
            }

            return {
                PresentAllocation: presentAllocation,
                FutureAllocation: futureAllocation,
                PastAllocation: pastAllocation
            };
        } catch (err) {
            nlapiLogExecution('ERROR', 'getProjectAllocationDetails', err);
            throw err;
        }
    }

    function _logValidation(value) {
        if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
            return true;
        } else {
            return false;
        }
    }

    function practiceSearch() {
        var departmentSearch = nlapiSearchRecord("department", null,
            [
                ["isinactive", "is", "F"]
            ],
            [
                new nlobjSearchColumn("name")
            ]
        );

        if (departmentSearch) {
            var deptObj = {};
            for (var i = 0; i < departmentSearch.length; i++) {
                deptObj[departmentSearch[i].getId()] = departmentSearch[i].getValue('name')
            }
            return deptObj



        }
    }

    function getRelatedCustomers(currentUser, s_current_usertype) {
        var filters = [
            [s_current_usertype, "anyof", currentUser],
            //["custentity_account_delivery_owner","anyof",currentUser],
            //'and',
            //[ [ 'job.status', 'anyof', '2' ], 'or', [ 'job.status', 'anyof', '4' ] ] 
        ];

        var columns = [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname")
        ];

        var search = nlapiSearchRecord('customer', null, filters, columns);
        return search;
    }


    function refreshProjects() {
        var customer = nlapiGetFieldValue('custpage_customer');
        var status = nlapiGetFieldValue('custpage_project_status');
        var s_current_deployment = nlapiGetFieldValue('custpage_currentdeploymentid');

        window.location = nlapiResolveURL('SUITELET',
                'customscript_sut_pm_project_details',
                s_current_deployment) +
            "&custpage_customer=" +
            customer +
            "&custpage_project_status=" + status;
    }


    function fieldChangeEvent(type, name, linenum) {

        if (name == 'custpage_customer') {
            nlapiSetFieldValue('custpage_filter_project', '');
            refreshProjects();
        } else if (name == 'custpage_project_status') {
            refreshProjects();
        }
    }