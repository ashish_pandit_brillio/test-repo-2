// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

	/*
		Script Name		:	SUT_Work_book_For_PP_Report.js
		Author			: 	Anukaran
		Company			:	Inspirria Cloudtech Pvt Ltd
		Created Date	:	25 Aug 2020
		Description		:	This script will create UI For Customer with Date Filter and after submit one message will generate on the screen.
		SCRIPT Type		:   2.0
		
		
		Script Modification Log:
	
		-- Date --		-- Modified By --			--Requested By--				-- Description --
	

}
// END SCRIPT DESCRIPTION BLOCK  ====================================
*/

/**
 *@NApiVersion 2.0
 *@NScriptType Suitelet
 *@NModuleScope SameAccount
 */
define(['N/ui/serverWidget', 'N/task' , 'N/search' ,'N/redirect'],
    function(serverWidget,task, search, redirect) 
	{
        function onRequest(context) 
		{
            if (context.request.method === 'GET') 
			{
                var form = serverWidget.createForm({
                    title: 'Income Statement PP Report'
                });
				
				//---------------
				var month = form.addField({
                   id: 'custpage_month',
                   type: serverWidget.FieldType.SELECT,
                   label: 'Month:'
               });
            
               month.addSelectOption({
                   value: '01',
                   text: 'January'
               });
           
               month.addSelectOption({
                   value: '02',
                   text: 'February'
               });
 
               month.addSelectOption({
                   value: '03',
                   text: 'March'
               });

               month.addSelectOption({
                   value: '04',
                   text: 'April'
               });
           
               month.addSelectOption({
                   value: '05',
                   text: 'May'
               });
 
               month.addSelectOption({
                   value: '06',
                   text: 'June'
               });

               month.addSelectOption({
                   value: '07',
                   text: 'July'
               });
           
               month.addSelectOption({
                   value: '08',
                   text: 'August'
               });
 
               month.addSelectOption({
                   value: '09',
                   text: 'September'
               });

               month.addSelectOption({
                   value: '10',
                   text: 'October'
               });
           
               month.addSelectOption({
                   value: '11',
                   text: 'November'
               });
 
               month.addSelectOption({
                   value: '12',
                   text: 'December'
               });
				
			month.isMandatory = true;
			
			/// Added by Shravan on 11-Feb-2021
			var cb_Yearly = form.addField({
                   id: 'custpage_check_box',
                   type: serverWidget.FieldType.CHECKBOX,
                   label: 'Yearly'
               });
			
			///// End /////
			
				//Current year by default on suitelet
				var	date = new Date();  //Current date 
				var year = date.getFullYear(); //Get full year
				var split1= new Array();
				var split1 = year.toString().split('.');
				year = split1[0];
				log.debug('year',year);
							
				
				var year_current = form.addField({
						id: 'custpage_year',
						type: serverWidget.FieldType.TEXT, 
						label: 'Year'
						}).updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY}) // Editable field
                        
						//}).updateDisplayType({displayType: serverWidget.FieldDisplayType.DISABLED}); //Disbale the field

					//Set Default Curreeny Year
					year_current.defaultValue = year.toString();
							
							
					//year_current.isMandatory = true;
					
                form.addSubmitButton({
                    label: 'Submit'
                });
                form.clientScriptFileId = 3474069;
              context.response.writePage(form);

            } else 
			{
                // Section Four - Output - Used in all sections
                var current_month = context.request.parameters.custpage_month;
                var current_year = context.request.parameters.custpage_year;
                var cb_yearly = context.request.parameters.custpage_check_box
				log.debug('cb_yearly',cb_yearly); 
				//Send Parameter to schedule script
				
				var mrTask = task.create({
											taskType: task.TaskType.SCHEDULED_SCRIPT,
											scriptId: 'customscript_sch_work_book_pp',
											deploymentId: 'customdeploy_sch_work_book_pp',
											params: {custscript__month: current_month, custscript__year: current_year, custscript_checkbox_yearly:cb_yearly }  //custscript__month, custscript__year, parameter those created in schedule script after deployed on system.
										});
			
					var submitedRecord = mrTask.submit();
					log.debug('submitedRecordLog',submitedRecord); 
				
				context.response.write("<b>Kindly check your email for Work Book Income PP Report<b/>.");
				
            }
			 
        }
    return {
        onRequest: onRequest
    };
});