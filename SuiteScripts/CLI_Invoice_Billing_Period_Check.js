/**
 * @author Shweta
 */
/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Invoice_Billing_Period_Check.js
	Author      : Shweta Chopde
    Date        : 17 July 2014
    Description : Billing From & Billing To Validations


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    



	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================


// BEGIN SAVE RECORD ================================================

function saveRecord()
{
   
   
   return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_INV(type, name, linenum)
{
  if(name == 'apply')
  {
  	var i_billable_from = nlapiGetFieldValue('custbody_billfrom')
	i_billable_from = nlapiStringToDate(i_billable_from)
	//alert('Billable From -->'+i_billable_from)
	
	var i_billable_to = nlapiGetFieldValue('custbody_billto')
	i_billable_to = nlapiStringToDate(i_billable_to)
	//alert('Billable To -->'+i_billable_to)
	
	var i_index = nlapiGetCurrentLineItemIndex('time')
	//alert(' Index -->'+i_index)
	
	var i_apply = nlapiGetLineItemValue('time','apply',i_index)
	//alert('Apply -->'+i_apply)
	
	if(i_apply == 'T')
	{			
		var i_billable_date = nlapiGetLineItemValue('time','billeddate',i_index)
		i_billable_date = nlapiStringToDate(i_billable_date)
		//alert('Billable Date -->'+i_billable_date)
		
		var s_date_format = checkDateFormat();
		//alert('Date Format -->'+s_date_format)
		
		if(i_billable_date>=i_billable_from && i_billable_date<=i_billable_to)
		{
		//	alert(' Billable Time ....')
			return true;
		}
		else
		{
			alert(' Please select in between the Billable From & Billable To.')
			nlapiSetLineItemValue('time','apply',i_index,'F')
			return false;
		}
		
	}//Apply T
	
  }  
   	return true;
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================



function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}


// END FUNCTION =====================================================
