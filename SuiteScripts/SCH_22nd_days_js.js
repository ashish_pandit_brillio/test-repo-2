/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         29th Oct 2015             Manikandan v          Send 22nd days completion mailer to the Employees scheduled at 9 a.m.
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');

// search for employee with 22nd days of service as of today

var emp_search = nlapiSearchRecord('employee', 'customsearch_22_days_completion_mailer', null,columns);

nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

if(isNotEmpty(emp_search)){
	
	for(var i=0;i<emp_search.length;i++){
		
		
		sendServiceMails(
			emp_search[i].getValue('firstname'),
			emp_search[i].getValue('email'),
			emp_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, emp_id){
	
	try{
                //var AttachmentID=32680;
                var fileObj = nlapiLoadFile();
		var mailTemplate = serviceTemplate(firstName);		
		nlapiLogExecution('debug', 'chekpoint',email);
		nlapiSendEmail(10730, email, mailTemplate.MailSubject, mailTemplate.MailBody,null,null,null,fileObj, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
//htmltext += &nbsp;
htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=251674&c=3883006&h=82da52b9cc8ae6c4e6e0&whence=" alt="Right click to download image." /></p>';
htmltext += '<p>Please <a href="https://brillioonline.sharepoint.com/hr/Shared%20Documents/Forms/AllItems.aspx?RootFolder=/hr/Shared+Documents/New+Hire%27s+Space&FolderCTID=0x0120003F79B70BAD303D42917D5B3DDA78665D&View={32411BB2-1570-40BA-A821-926615987E1E}" >Click</a> here to access the <b>New Hire’s Space section</b></p>';

htmltext += '<p>Go to CONNECT/Sharepoint--->People--->Human Resources(IN)--->New Hire’s Space</p>';


htmltext += '<p>Thanks & Regards,</p>';
htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : " Galaxy of Brillian and Rewards & Recognition User Guide"      
    };
}

