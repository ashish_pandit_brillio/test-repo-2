/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Unbilled_Time_Expense_Validations.js
	Author      : Shweta Chopde
	Date        : 19 May 2014
    Description : Validation on Expense / Time Report


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   16-02-2015           Swati Kurariya                    valibhav/sachin k              Add Delete button ,to delete monthly_provision_allocation custom record
   16-03-2020			Praveena Madem						Deepak Ms						Changed Internalid of searched record type and internal id of searched id


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{

          
			nlapiDisableField('custpage_criteria',true);
			nlapiDisableField('custpage_unbilled_type',true);
			nlapiDisableField('custpage_unbilled_receivable',true);
			nlapiDisableField('custpage_unbilled_revenue',true);
			nlapiDisableField('custpage_reverse_date',true);//
			nlapiDisableField('custpage_criteria',true);//custpage_date_range
			nlapiDisableField('custpage_date_range',true);//
			nlapiDisableField('custpage_to_date',true);
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_unbilled_report()
{

   var i_type = nlapiGetFieldValue('custpage_unbilled_type');

   var i_criteria = nlapiGetFieldValue('custpage_criteria');

   if(i_type == 2)
   {

   	 if(i_criteria == '' || i_criteria == null || i_criteria == undefined )
	 {
	 	alert(' Please enter Criteria for Time .')
		return false;
	 }

   }//Time Type
   
   //----------------------added by swati for validation purpose-
   /*
   Check For the selected data ,monthly_provision_allocation record  avilable or not,if avilable than can not allow to click 'LOAD DATA' button. 
   */
   if(i_type == 2)
   {
        var i_user_subsidiary = nlapiGetFieldValue('custpage_subsidiary');
		var i_month = nlapiGetFieldValue('custpage_month');
		var i_year = nlapiGetFieldValue('custpage_year');
		
        var filter1 = new Array();
	    filter1[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
		filter1[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
		filter1[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
		filter1[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
		var search_time_bill_count=searchRecord('customrecord_time_bill_rec',null,filter1,null);
		//alert('search_time_bill_count'+search_time_bill_count);
		//if(search_time_bill_count != null)
		if(_logValidation(search_time_bill_count))
		{//if start
		
		  alert('Record already processed ,for processing again need to click delete button');
		  return false;
		
		}//if close
   }
   //-----------------------------------------------------------


   return true;


}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_provision(type, name, linenum)
{
    if(name == 'custpage_reverse_date')
	{
		var i_reverse_date = nlapiGetFieldValue('custpage_reverse_date');
		var d_to_date = nlapiGetFieldValue('custpage_to_date');
		//var d_todays_date = get_todays_date();

		i_reverse_date = nlapiStringToDate(i_reverse_date)
		d_to_date = nlapiStringToDate(d_to_date)

		if(i_reverse_date!=null && i_reverse_date!='' && i_reverse_date!=undefined)
		{
			if(d_to_date > i_reverse_date)
			{
				alert('Reverse Date should be greater than / equal to To Date  .')
				nlapiSetFieldValue('custpage_reverse_date','',false)
				return false;
			}
		}

	}//Reverse Date
	//-----------------------------------------------------------------
	if(name == 'custpage_from_date')
	{
		nlapiDisableField('custpage_to_date',false);
	}
	if(name == 'custpage_to_date' || name == 'custpage_from_date')
	{
	     //alert(name);
		var d_from_date = nlapiGetFieldValue('custpage_from_date');
		var d_to_date = nlapiGetFieldValue('custpage_to_date');
		
		nlapiSetFieldValue('custpage_status','Not Started');
		nlapiSetFieldValue('custpage_percent_complete','0%');
		//-----------------------------------------------------------
		if(name == 'custpage_to_date')
		{
			nlapiDisableField('custpage_criteria',false);
			nlapiDisableField('custpage_unbilled_type',false);
			nlapiDisableField('custpage_unbilled_receivable',false);
			nlapiDisableField('custpage_unbilled_revenue',false);
			nlapiDisableField('custpage_reverse_date',false);//
			nlapiDisableField('custpage_criteria',false);//custpage_date_range
			nlapiDisableField('custpage_date_range',true);//
		}
		//------------------------------------------------------------

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



		//----------------------------------------------------------------------------

		var d_current_date = nlapiGetFieldValue('custpage_current_date');
		//var d_current_date = d_current_date();

		d_from_date = nlapiStringToDate(d_from_date)
		d_to_date = nlapiStringToDate(d_to_date)
		d_current_date = nlapiStringToDate(d_current_date)

		if(d_from_date!=null && d_from_date!='' && d_from_date!=undefined)
		{
		 //alert('d_from_date===>'+d_from_date);
		  //alert('d_to_date=====>'+d_to_date);
		  if(name == 'custpage_to_date')
		    {

				if(d_from_date >= d_to_date)
				{
					 nlapiDisableField('custpage_criteria',true);
					nlapiDisableField('custpage_unbilled_type',true);
					nlapiDisableField('custpage_unbilled_receivable',true);
					nlapiDisableField('custpage_unbilled_revenue',true);
					nlapiDisableField('custpage_reverse_date',true);//
					nlapiDisableField('custpage_criteria',true);//custpage_date_range
					nlapiDisableField('custpage_date_range',true);//

					alert('To Date should be greater than / equal to From Date .')
					nlapiSetFieldValue('custpage_to_date','',false)
					return false;

				}
		    }
		/*	else if(d_to_date > d_current_date)
			{
			    alert('To Date Can Not be greater than Current Date.')
				nlapiSetFieldValue('custpage_to_date','',false)
				return false;
			}*/
		}
		//----------------------------------------------------------------------------------------------------
		var i_type = nlapiGetFieldValue('custpage_unbilled_type');
		if(i_type == null || i_type == '' || i_type == 'null')
		{
		}
		else{
		  if(i_type == 1)
			{
			}
			else
			{
				 var i_search_count ='';
				var i_criteria = nlapiGetFieldValue('custpage_criteria');
				//alert('i_criteria'+i_criteria);
				if(i_criteria == '' || i_criteria == null || i_criteria == 'null')
				{
				}
				else{
						var i_current_date = nlapiGetFieldValue('custpage_current_date')
						var i_from_date = nlapiGetFieldValue('custpage_from_date')
						var i_to_date = nlapiGetFieldValue('custpage_to_date')
						var i_user_subsidiary = nlapiGetFieldValue('custpage_subsidiary')
						var i_user_role = nlapiGetFieldValue('custpage_user_role')
						//var i_customer = nlapiGetFieldValue('custpage_customer')

						if(i_criteria == 1)
						{
							i_search_count = get_approved_unbilled(i_from_date,i_to_date,i_user_subsidiary,i_criteria)

						}//Approved - Unbilled
						if(i_criteria == 2)
						{
							i_search_count =  get_submitted_not_approved(i_from_date,i_to_date,i_user_subsidiary,i_criteria)

						}//Submitted but Not approved - Unbilled
						if(i_criteria == 3)
						{
                         
							i_search_count =  get_allocated_not_submitted(i_from_date,i_to_date,i_user_subsidiary,i_criteria);
                         

						}//Not submitted - Allocated
						if(i_criteria == 4)
						{
						var i_search_count_a = get_approved_unbilled(i_from_date,i_to_date,i_user_subsidiary)
						alert('i_search_count_a'+i_search_count_a);
						var i_search_count_b =  get_submitted_not_approved(i_from_date,i_to_date,i_user_subsidiary)
						alert('i_search_count_b'+i_search_count_b);
						var i_search_count_c =  get_allocated_not_submitted(i_from_date,i_to_date,i_user_subsidiary)
						alert('i_search_count_c'+i_search_count_c);

						i_search_count=parseInt(i_search_count_a)+parseInt(i_search_count_b)+parseInt(i_search_count_c);
						}
						
                  nlapiSetFieldValue('custpage_search_count',i_search_count)
				}

			}//Criteria
		}
		//----------------------------------------------------------------------------------------------------

	}//Reverse Date
	//-------------------------------------------------------------------


	if(name == 'custpage_unbilled_type')
	{
	 var i_search_count = ''
	  var i_type = nlapiGetFieldValue('custpage_unbilled_type');

	  if(i_type == 1)
	  {
	  	nlapiSetFieldValue('custpage_criteria','',false);
	  	nlapiDisableField('custpage_criteria',true);

		var i_current_date = nlapiGetFieldValue('custpage_current_date')
		var i_from_date = nlapiGetFieldValue('custpage_from_date')
		var i_to_date = nlapiGetFieldValue('custpage_to_date')
        var i_user_subsidiary = nlapiGetFieldValue('custpage_subsidiary')
		var i_user_role = nlapiGetFieldValue('custpage_user_role')
       // var i_customer = nlapiGetFieldValue('custpage_customer')
       // alert('i_customer'+i_customer);

		//i_search_count = get_expense_unbilled_count(i_current_date,i_user_subsidiary,i_user_role)
		i_search_count = get_expense_unbilled_count(i_from_date,i_to_date,i_user_subsidiary,i_user_role)
		//alert('i_search_count'+i_search_count);
		//alert('i_search_count--2'+i_search_count);
		nlapiSetFieldValue('custpage_search_count',i_search_count)

	  }
	  else
	  {
	  	//alert('i_search_count--3'+i_search_count);
	  	nlapiSetFieldValue('custpage_search_count',i_search_count)
	  	nlapiDisableField('custpage_criteria',false);

	  }

	}//Type
	/*if(name == 'custpage_date_range')
	{
	          var i_criteria = nlapiGetFieldValue('custpage_criteria');
				//alert('i_criteria'+i_criteria);

				var i_current_date = nlapiGetFieldValue('custpage_current_date')
				var i_from_date = nlapiGetFieldValue('custpage_from_date')
				var i_to_date = nlapiGetFieldValue('custpage_to_date')
				var i_user_subsidiary = nlapiGetFieldValue('custpage_subsidiary')
				if(i_criteria == 3)
				{
					nlapiDisableField('custpage_date_range',false);//
					i_search_count =  get_allocated_not_submitted(i_from_date,i_to_date,i_user_subsidiary)
					nlapiSetFieldValue('custpage_search_count',i_search_count);

				}//Not submitted - Allocated
	}*/

	if(name == 'custpage_criteria')
	{
	      var i_type = nlapiGetFieldValue('custpage_unbilled_type');
		  if(i_type == 1)
			{
			}
			else
			{
				 var i_search_count ='';
				var i_criteria = nlapiGetFieldValue('custpage_criteria');
				//alert('i_criteria'+i_criteria);

				var i_current_date = nlapiGetFieldValue('custpage_current_date')
				var i_from_date = nlapiGetFieldValue('custpage_from_date')
				var i_to_date = nlapiGetFieldValue('custpage_to_date')
				var i_user_subsidiary = nlapiGetFieldValue('custpage_subsidiary')
				var i_user_role = nlapiGetFieldValue('custpage_user_role')
                //var i_customer = nlapiGetFieldValue('custpage_customer')
				
				nlapiSetFieldValue('custpage_status','Not Started');
		        nlapiSetFieldValue('custpage_percent_complete','0%');

				if(i_criteria == 1)
				{
				  nlapiDisableField('custpage_date_range',true);
					i_search_count = get_approved_unbilled(i_from_date,i_to_date,i_user_subsidiary,i_criteria)

				}//Approved - Unbilled
				if(i_criteria == 2)
				{
				    nlapiDisableField('custpage_date_range',true);
					i_search_count =  get_submitted_not_approved(i_from_date,i_to_date,i_user_subsidiary,i_criteria)

				}//Submitted but Not approved - Unbilled
				if(i_criteria == 3)
				{
					nlapiDisableField('custpage_date_range',false);//
					i_search_count =  get_allocated_not_submitted(i_from_date,i_to_date,i_user_subsidiary,i_criteria)

				}//Not submitted - Allocated
				if(i_criteria == 4)
				{
				  nlapiDisableField('custpage_date_range',true);
				var i_search_count_a = get_approved_unbilled(i_from_date,i_to_date,i_user_subsidiary)
				//alert('i_search_count_a'+i_search_count_a);
				var i_search_count_b =  get_submitted_not_approved(i_from_date,i_to_date,i_user_subsidiary)
				//alert('i_search_count_b'+i_search_count_b);
				var i_search_count_c =  get_allocated_not_submitted(i_from_date,i_to_date,i_user_subsidiary)
				//alert('i_search_count_c'+i_search_count_c);

				i_search_count=parseInt(i_search_count_a)+parseInt(i_search_count_b)+parseInt(i_search_count_c);
				}

				//alert('i_search_count-1'+i_search_count)
				nlapiSetFieldValue('custpage_search_count',i_search_count)

			}//Criteria

			//custpage_search_count

	}
    return true;
}//Field Change Provision

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function get_todays_date()
{
	// ============================= Todays Date ==========================================

	  var date1 = new Date();
      nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);


    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);

     var day = istdate.getDate();
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);

     var month = istdate.getMonth()+1;
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);

	 var today= month + '/' + day + '/' + year;

	 return today;
}
function get_expense_unbilled_count(i_from_date,i_to_date,i_user_subsidiary,i_user_role)
{
   var i_search_count = 0;
  // alert('i_from_date'+i_from_date);
  // alert('i_to_date'+i_to_date);
  // alert('i_user_subsidiary'+i_user_subsidiary);

   if(parseInt(i_user_role) != 3 )
	{
	var filter = new Array();
   // filter[0] = new nlobjSearchFilter('trandate', null, 'onorbefore', i_current_date);
    filter[0] = new nlobjSearchFilter('trandate', null, 'within', i_from_date,i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', null, 'is', parseInt(i_user_subsidiary));
	}
	else
	{
	//alert('inside else');
	var filter = new Array();
  //  filter[0] = new nlobjSearchFilter('trandate', null, 'onorbefore', i_current_date);
   filter[0] = new nlobjSearchFilter('trandate', null, 'within', i_from_date,i_to_date);
   filter[1] = new nlobjSearchFilter('subsidiary','customer', 'is', parseInt(i_user_subsidiary));
   // filter[2] = new nlobjSearchFilter('customersubof', null, 'is', parseInt(i_customer));
	}


	var i_search_results = searchRecord('expensereport','customsearch_unbilled_expense_search_2_3',filter,null);



	if (_logValidation(i_search_results))
	{
		for(var c=0;c<i_search_results.length;c++)
		{
			var a_search_transaction_result = i_search_results[c];
			var columns = a_search_transaction_result.getAllColumns();

			if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) {
				break;
			}
			/*var columns = a_search_transaction_result.getAllColumns();

			var columnLen = columns.length;

			for (var hg = 0; hg < columnLen; hg++)
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)

				if (label == 'Search Count')
				{
					i_search_count = value;
				}

			}*/
			var i_search_count=a_search_transaction_result.getValue(columns[0]);
		}
	}
  
  return i_search_count;

}

function get_approved_unbilled(i_from_date,i_to_date,i_user_subsidiary,i_criteria)
{
	//if(i_criteria == 1)
	{
	nlapiLogExecution('DEBUG','suiteletFunction','criteria 1');
	var filter = new Array();
    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
	var i_search_results = searchRecord('timebill','customsearch_unbilled_approved_new_tb',filter,null);//Changed by praveena
	//nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);

	var filter1 = new Array();
    filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);
    filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
    filter1[2] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
    filter1[3] = new nlobjSearchFilter('custrecord_processed',null, 'is','T');
     filter1[4] = new nlobjSearchFilter('custrecord_criteria',null, 'is',i_criteria);
    var search_time_bill_count = searchRecord('customrecord_time_bill_rec',null,filter1,null);


	}

	if(i_search_results == null)
	{
	var final_count=0;
	}
else
	{
	var final_count=i_search_results.length;
	}
	//alert('i_search_results.length--'+i_search_results.length+'-time_rec_count-'+time_rec_count)
   //var final_count=parseInt(i_search_results.length)-parseInt(time_rec_count);
   //alert('i_search_results.length--'+i_search_results.length+'-time_rec_count-'+time_rec_count+'-final_count-'+final_count)
  return final_count;
}

function get_allocated_not_submitted(i_from_date,i_to_date,i_user_subsidiary,i_criteria)
{

	//nlapiLogExecution('DEBUG','suiteletFunction','criteria 3');
	 var filter = new Array();
	 filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);
	 filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
	
	var i_search_results = searchRecord('timebill','customsearch_notsubmitted_new_timesheet_',filter,null);
//	nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);


	var filter1 = new Array();
    filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);
    filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
    filter1[2] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
    filter1[3] = new nlobjSearchFilter('custrecord_processed',null, 'is','T');
    filter1[4] = new nlobjSearchFilter('custrecord_criteria',null, 'is',i_criteria);

    var search_time_bill_count = searchRecord('customrecord_time_bill_rec',null,filter1,null);//Changed by praveena

    if(i_search_results == null)
	{
	var final_count=0;
	}
else
	{
	var final_count=i_search_results.length;
     // alert('final_count====>'+final_count);
	}

   //var final_count=parseInt(i_search_results.length)-parseInt(time_rec_count);

	  return final_count;
}
function get_submitted_not_approved(i_from_date,i_to_date,i_user_subsidiary,i_criteria)
{
	//nlapiLogExecution('DEBUG','suiteletFunction','criteria 3');
	 var filter = new Array();
	 filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);
	 filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
	
var i_search_results = searchRecord('timebill','customsearch_submitted_not_appr_new_tb',filter,null);//Changed by praveena customsearch_submitted_not_appr_new_tb


//	nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);


	var filter1 = new Array();
   filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);
   filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
   filter1[2] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
   filter1[3] = new nlobjSearchFilter('custrecord_processed',null, 'is','T');
   filter1[4] = new nlobjSearchFilter('custrecord_criteria',null, 'is',i_criteria);
   var search_time_bill_count = searchRecord('customrecord_time_bill_rec',null,filter1,null);


   if(i_search_results == null || i_search_results == '')
	{
	var final_count=0;
	}
else
	{
	var final_count=i_search_results.length;
	}

 // var final_count=parseInt(i_search_results.length)-parseInt(time_rec_count);

	  return final_count;

}
function _logValidation(value)
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN)
 {
  return true;
 }
 else
 {
  return false;
 }
}
//-----------------------------------delete function call-------------------------------------
/*
Description:-Call Schedule Script ,to delete monthly_provision_allocation custom record
*/
function delete_time_cus_rec()
{//fun start

   var d_From_Date=nlapiGetFieldValue('custpage_from_date');
   var d_To_Date=nlapiGetFieldValue('custpage_to_date');
   var i_Criteria=nlapiGetFieldValue('custpage_criteria');
   var i_Subsidiary=nlapiGetFieldValue('custpage_subsidiary');
   var i_Month=nlapiGetFieldValue('custpage_month');
   var i_Year=nlapiGetFieldValue('custpage_year');
   var i_Search_Count_f=nlapiGetFieldValue('custpage_search_count');
    var i_Unbilled_Type=nlapiGetFieldValue('custpage_unbilled_type');
   var i_Unbilled_Receivable_GL=nlapiGetFieldValue('custpage_unbilled_receivable');
   var i_Unbilled_Revenue_GL=nlapiGetFieldValue('custpage_unbilled_revenue');
   var i_Reverse_Date=nlapiGetFieldValue('custpage_reverse_date');
    var Status='T';
   
   nlapiLogExecution('DEBUG','delete_time_cus_rec','d_From_Date'+d_From_Date);
   nlapiLogExecution('DEBUG','delete_time_cus_rec','d_To_Date'+d_To_Date);
   nlapiLogExecution('DEBUG','delete_time_cus_rec','i_Criteria'+i_Criteria);
   nlapiLogExecution('DEBUG','delete_time_cus_rec','i_Subsidiary'+i_Subsidiary);
   nlapiLogExecution('DEBUG','delete_time_cus_rec','i_Month'+i_Month);
   nlapiLogExecution('DEBUG','delete_time_cus_rec','i_Year'+i_Year);
   
   
   
	 var filter1 = new Array();
     filter1[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_Subsidiary));
	 filter1[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_Criteria);
	 filter1[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_Month);
	 filter1[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_Year);
//------------------------------------------------------------------------------------------------
	 var search_time_bill_count=searchRecord('customrecord_time_bill_rec',null,filter1,null);
	 if(search_time_bill_count == null)
		 {
		  alert('No Record Are Available For Deletion');
		  return false;
		 }
   
   /*alert('d_From_Date===>'+d_From_Date);
   alert('d_To_Date===>'+d_To_Date);
   alert('i_Criteria===>'+i_Criteria);
   alert('i_Subsidiary===>'+i_Subsidiary);
   alert('i_Month===>'+i_Month);
   alert('i_Year===>'+i_Year);*/
   
     var params=new Array();
     params['custpage_from_date'] = d_From_Date
	 params['custpage_to_date'] = d_To_Date
	 params['custpage_unbilled_type'] = i_Unbilled_Type
	 params['custpage_unbilled_receivable'] = i_Unbilled_Receivable_GL
	 params['custpage_unbilled_revenue'] = i_Unbilled_Revenue_GL
	 params['custpage_reverse_date'] = i_Reverse_Date
	 params['custpage_criteria'] = i_Criteria
	 params['custpage_subsidiary'] = i_Subsidiary
	 params['custpage_search_count'] = i_Search_Count_f
	 params['custpage_month'] = i_Month
	 params['custpage_year'] = i_Year
	 params['custscript_status'] = 'T'
	 
	  myWindow =window.open('/app/site/hosting/scriptlet.nl?script=462&deploy=1&d_from_date='+d_From_Date+'&d_to_date='+d_To_Date+'&i_criteria='+i_Criteria+'&i_subsidiary='+i_Subsidiary+'&i_month='+i_Month+'&i_year='+i_Year+'&i_unbilled_type='+i_Unbilled_Type+'&i_ubilled_receivable_GL='+i_Unbilled_Receivable_GL+'&i_unbilled_revenue_GL='+i_Unbilled_Revenue_GL+'&i_reverse_date='+i_Reverse_Date+'&i_search_count_f='+i_Search_Count_f+'&custscript_status='+Status,'_self',null)//_self
	 
	// myWindow =window.open('/app/site/hosting/scriptlet.nl?script=462&deploy=1&d_from_date='+d_From_Date+'&d_to_date='+d_To_Date+'&i_criteria='+i_Criteria+'&i_subsidiary='+i_Subsidiary+'&i_month='+i_Month+'&i_year='+i_Year,'_self',null,null)//_self
	 
	// nlapiSetRedirectURL('SUITELET','customscript_unbilled_timeexpense_report', null, null,params);
    // myWindow.close();    
}//fun close
//--------------------------------------------------------------------------------------------

function refresh()
{
   //location.reload();
   
   var from_date=nlapiGetFieldValue('custpage_from_date');
   //alert('from_date'+from_date);
   var to_date=nlapiGetFieldValue('custpage_to_date');
   var i_unbilled_type=nlapiGetFieldValue('custpage_unbilled_type');
   var i_unbilled_receivable_GL=nlapiGetFieldValue('custpage_unbilled_receivable');
   var i_unbilled_revenue_GL=nlapiGetFieldValue('custpage_unbilled_revenue');
   var i_reverse_date=nlapiGetFieldValue('custpage_reverse_date');
   var i_criteria=nlapiGetFieldValue('custpage_criteria');
  
   var i_subsidiary=nlapiGetFieldValue('custpage_subsidiary');
   var i_search_count_f=nlapiGetFieldValue('custpage_search_count');
   var i_month=nlapiGetFieldValue('custpage_month');
   
   var i_year=nlapiGetFieldValue('custpage_year');
   var Status='T';
   
   
   
 /*  nlapiSetFieldValue('custpage_from_date',from_date);
   nlapiSetFieldValue('custpage_to_date',to_date);
   nlapiSetFieldValue('custpage_unbilled_type',type);
   nlapiSetFieldValue('custpage_unbilled_receivable',ac_credit);
   nlapiSetFieldValue('custpage_unbilled_revenue',ac_debit);
   nlapiSetFieldValue('custpage_reverse_date',reverse_date);
   nlapiSetFieldValue('custpage_criteria',criteria);
   nlapiSetFieldValue('custpage_month',month);
   nlapiSetFieldValue('custpage_year',year);
   nlapiSetFieldValue('custpage_subsidiary',subsidiary);
   nlapiSetFieldValue('custpage_search_count',count);*/
   
             window.open('/app/site/hosting/scriptlet.nl?script=233&deploy=1&custpage_from_date='+from_date+'&custpage_to_date='+to_date+'&custpage_unbilled_type='+i_unbilled_type+'&custpage_unbilled_receivable='+i_unbilled_receivable_GL+'&custpage_unbilled_revenue='+i_unbilled_revenue_GL+'&custpage_reverse_date='+i_reverse_date+'&custpage_criteria='+i_criteria+'&custpage_user_subsidiary='+i_subsidiary+'&custpage_search_count='+i_search_count_f+'&custpage_month='+i_month+'&custpage_year='+i_year+'&custscript_status='+Status,'_self',null)			
	 
	     //nlapiSetRedirectURL('SUITELET','customscript_unbilled_timeexpense_report', null, null,params);
	
   
	
}

// END FUNCTION =====================================================
// 
// // END FUNCTION =====================================================
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}