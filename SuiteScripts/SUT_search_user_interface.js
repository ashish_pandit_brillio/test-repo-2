// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_search_user_interface.js
	Author:		 Rakesh K 
	Company:	 Brillio 
	Date:		 15-02-2017
	Version: 	 1.0
	Description: This script is uses to show all records are saved in NetSuite Data Base which is created by current user.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================




var strVar= '';
var s_tablestring = '';
var l_link_URL = '';
// BEGIN SUITELET ==================================================
var a_vendor_name = new Array();
function suiteletFunction_searchVendor(request, response){
	
	
	try{
		if (request.getMethod() == 'GET'){
			
			var objUser = nlapiGetContext();
			var o_values  = new Object();
			var i_employee_id = objUser.getUser();
			var i_roleID = objUser.getRole();
			var s_role	=	getRoleName(i_employee_id, objUser.getRole());
			var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
			
			o_values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
			o_values['role']		=	s_role;
			//lastnextrecordID
			var lastNextID = request.getParameter('lastnextrecordID');
			var mode = request.getParameter('mode');
			var counter = request.getParameter('counter');
			var isactive = request.getParameter('active');
			if(isactive == 'F'){
				nlapiLogExecution('debug', 'isactive', isactive);
				o_values['is_selected_active'] = 'selected';
			}else if(isactive == 'T'){
				nlapiLogExecution('debug', 'isactive', isactive);
				o_values['is_selected_inactive'] = 'selected';
			}else{
				nlapiLogExecution('debug', 'isactive', isactive);
				o_values['s_selected_view_all'] = 'selected';
				
			}
			
			
			if(counter =='' ||counter == null){
				counter = 0;
			}
			//var counter;
			if(mode == '' || mode == null){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
				
			}
			if(mode == 'next'){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' || counter =='' ||counter == null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' && counter !='' && counter != null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			//nlapiLogExecution('debug', 'mode', mode);
			//nlapiLogExecution('debug', 'lastNextID', lastNextID);
			var last_pre_rec_id = request.getParameter('last_prev_id');
			
			//var o_jsonvendor	=	getvendorJSON();
			 //nlapiLogExecution('Debug', 'o_jsonvendor', JSON.stringify(o_jsonvendor));
			
			// var strVendorOptions = getvendorDropdown(o_jsonvendor);
			// nlapiLogExecution('Debug', 'strVendorOptions', (strVendorOptions));
			 
			// o_values['vendor_names'] = strVendorOptions;
			 var v_name = request.getParameter('vendor_name');
			 var f_id =  request.getParameter('federal_id');
			 var a_counter =  request.getParameter('counter');
			 
			 if(lastNextID != null ){
				
				var o_json = getrequiterdata(lastNextID,last_pre_rec_id,'next',v_name,f_id,isactive);
			}
			else if(last_pre_rec_id){
				var o_json = getrequiterdata(lastNextID,last_pre_rec_id,'prev',v_name,f_id,isactive);
			}
			else{
				var o_json = getrequiterdata(lastNextID,last_pre_rec_id,'diff',v_name,f_id,isactive);
			}
			 var recordid = '';
			 var s_vendor_name = '';
			 var  s_federal_id= '';
			 var s_sr_no = '';
			 var s_vendor_contracttype = '';
			 var s_vendor_emailaddress = '';
			 var s_vendor_contactperson = '';
			 var s_vendor_status = '';
			
			 strVar += "<html>";
		   	 strVar += "<body>";
		   
			 for(var key in o_json){
				
				// if(o_json[key].vendor_name != null && o_json[key].vendor_name != '')
				 {
					 if(isactive == '' || isactive == null){
						 if(mode == 'prev')
						 {
						 var newid = o_json[0].v_record_id;
						 
						 }else{
							  var newid = o_json[key].v_record_id;
							 }
					 
					 var find_prev_id = o_json[0].v_record_id;
				 	 //nlapiLogExecution('debug', 'find_prev_id', find_prev_id);
					 strVar += '<tr  class="noExl">';
					//var newid = o_json[key].record_id;
				 	 strVar += '<td>'+o_json[key].sr_no+'</td>';
					 strVar += '<td>'+o_json[key].record_id+'</td>';
					 strVar += '<td>'+o_json[key].vendor_name+'</td>';
					 strVar += '<td>'+o_json[key].federal_id+'</td>';
					// strVar += '<td>'+o_json[key].vendor_contracttype+'</td>';
					 strVar += '<td>'+o_json[key].vendor_emailaddress+'</td>';
					 strVar += '<td>'+o_json[key].phone_number+'</td>';
					 strVar += '<td>'+o_json[key].s_status_val+'</td>';
					 strVar += '<td>'+o_json[key].i_contact_name+'</td>';
					 
					 strVar += '<th style="display:none">'+o_json[key].v_record_id+'</th>';
					 strVar+= '</tr>';
						 
					 }else{
						
						 if(key <= 24)
						 {
							 if(mode == 'prev')
								 {
								 var newid = o_json[0].v_record_id;
								 
								 }else{
									  var newid = o_json[key].v_record_id;
									 }
							 
							 var find_prev_id = o_json[0].v_record_id;
						 	 //nlapiLogExecution('debug', 'find_prev_id', find_prev_id);
							 strVar += '<tr  class="noExl">';
							//var newid = o_json[key].record_id;
						 	 strVar += '<td>'+o_json[key].sr_no+'</td>';
							 strVar += '<td>'+o_json[key].record_id+'</td>';
							 strVar += '<td>'+o_json[key].vendor_name+'</td>';
							 strVar += '<td>'+o_json[key].federal_id+'</td>';
							// strVar += '<td>'+o_json[key].vendor_contracttype+'</td>';
							 strVar += '<td>'+o_json[key].vendor_emailaddress+'</td>';
							 strVar += '<td>'+o_json[key].phone_number+'</td>';
							 strVar += '<td>'+o_json[key].s_status_val+'</td>';
							 strVar += '<th style="display:none">'+o_json[key].v_record_id+'</th>';
							 strVar+= '</tr>';
						 }
					 }
					 
					
				 }
				
			 } // for loop ending 
			// nlapiLogExecution('debug', 'o_json', o_json.length);
			 strVar += "<\/body>";
			 strVar += "<\/html>";
			 var firstpage = request.getParameter('firstpage');
			if(firstpage == 'T'){
				o_values['btn_disable_prev'] = 'disabled';
				o_values['s_href_prev'] = '#';
			}
			 if(o_json.length <=1 || o_json.length == null){
					
					o_values['btn_disable_next'] = 'disabled';
					o_values['btn_disable_prev'] = 'disabled';
					o_values['s_href_next'] = '#';
					o_values['s_href_prev'] = '#';
					
					//nlapiLogExecution('debug', 'o_json.length if ', o_json.length);
					
				}
				else{		//if(isactive == '' && isactive == null){
							//o_values['btn_disable_next'] = 'disabled';
							//o_values['s_href_next']= '#';
							//}
							//o_values['btn_disable_next'] = '';
						
						if(mode){
							//o_values['btn_disable_prev'] = 'disabled';
							//o_values['btn_disable_prev'] ='';
							o_values['s_href_prev'] = '#';
							o_values['btn_disable_next'] = '';						
						}else{
							o_values['btn_disable_prev'] = 'disabled';
							o_values['s_href_prev'] = '#';
							o_values['btn_disable_next'] = 'disabled';
							o_values['s_href_next']= '#';
							
						}
						//&& mode == 'next'
						if(newid && mode){
							//nlapiLogExecution('debug', 'newid', newid);
							o_values['s_href_next'] = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1192&deploy=1&lastnextrecordID="+newid+"&mode=next"+"&counter="+finalvalue+"&active="+isactive+"&firstpage=F";	
							
						}
						//&& mode == 'prev'
						// && mode
						if(find_prev_id  && mode && firstpage == 'F'){
							
							o_values['s_href_prev'] = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1192&deploy=1&last_prev_id="+find_prev_id+"&mode=prev"+"&counter="+finalvalue+"&active="+isactive+"&firstpage=F";
						}	
					
				}
			 //nlapiLogExecution('debug', 'l_link_URL', l_link_URL);
			
			 o_values['s_table_content'] = strVar; 
			 if(!mode){
				 o_values['s_selected_view'] = 'selected'; 
			 }
			
			 
			 var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			 var s_my_requests_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
			 var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
			 o_values['new_request_url']	=	s_new_request_url;
			 o_values['my_requests_url']	=	s_my_requests_url;
			 o_values['s_search_URL'] = s_search_URL;
			 o_values['table_content'] = s_tablestring;
			 o_values['record_found'] = o_json.length;
			 var file = nlapiLoadFile(2046238); 
			 var contents = file.getValue();    //get the contents
			 //nlapiLogExecution('debug', 's_tablestring', s_tablestring);
			 contents = replaceValues(contents, o_values);
			 // nlapiLogExecution('debug', 'contents', contents);
			  response.write(contents);
		
		}
		
	}catch(e){
		
		nlapiLogExecution('debug', 'ERROR'+e);
	}




}
// get role name 
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}
// END SUITELET ====================================================

function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

	
		

	// get custom record data for particular user 
	
	
function getrequiterdata(lastNextID,last_pre_rec_id,mode,v_name,f_id,isactive)	{
	 	
		var temp = 0;
		//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
	 	var filters =	new Array();
	 	var columns	=	new Array();
	 	//if(i_employee_id != null && i_employee_id != ''){
	 		filters[temp++]	=	new nlobjSearchFilter('category',null,'anyof',12);
	 	//}
	 		
 		if(_logValidation(lastNextID)){
	 		filters[temp++]	=	new nlobjSearchFilter('internalidnumber',null,'greaterthan',parseInt(lastNextID));
	 	}
	 	if(_logValidation(last_pre_rec_id)){
	 		filters[temp++]	=	new nlobjSearchFilter('internalidnumber',null,'lessthan',last_pre_rec_id);
	 	}
	 	if(_logValidation(v_name)){
	 		filters[temp++]	=	new nlobjSearchFilter('entityid',null,'is',v_name);
	 	}
	 	if(_logValidation(f_id)){
	 		filters[temp++]	=	new nlobjSearchFilter('taxidnum',null,'is',f_id);
	 	}
	 	if(isactive == 'T'){
	 		filters[temp++]	=	new nlobjSearchFilter('isinactive',null,'is','T');
	 	}
	 	if(isactive == 'F'){
	 		filters[temp++]	=	new nlobjSearchFilter('isinactive',null,'is','F');
	 	}
		
		columns[0]	=	new nlobjSearchColumn('entityid');
		columns[1]	=	new nlobjSearchColumn('altname');
		columns[2]	=	new nlobjSearchColumn('taxidnum'); // FD ID
		columns[3]	=	new nlobjSearchColumn('email'); // email for admin 
		columns[4]	=	new nlobjSearchColumn('phone'); // contract type
	
		//columns[5]	=	new nlobjSearchColumn('internalid'); // contract type
		
		
		if(mode == 'next' || mode == 'diff'){
			nlapiLogExecution('audit','inside if sort');
			columns[5]	=	new nlobjSearchColumn('internalid').setSort(false);
		}else if(mode == 'prev'){
			
			nlapiLogExecution('audit','inside else sort');
			columns[5]	=	new nlobjSearchColumn('internalid').setSort(true);
		}
		
		columns[6]	=	new nlobjSearchColumn('isinactive');
		//columns[4]	=	new nlobjSearchColumn('custrecord_contract_type'); // contract type
		//columns[5]	=	new nlobjSearchColumn('custrecord_contact_person'); // contact person 
		//columns[6]	=	new nlobjSearchColumn('custrecord_approval_status_recruiters'); // status
		columns[7]	=	new nlobjSearchColumn("entityid","contactPrimary",null);
		
		var searchResults	=	searchRecord('vendor', null, filters, columns);
		nlapiLogExecution('debug', 'searchResults', searchResults.length);
		var a_data	=	new Array();
		var s_status ;
		//va s_tablestring="";
		if(searchResults){
				
				var k =1;
				for(var i = 0; i < searchResults.length; i++){
				
				var s_record_id	=	searchResults[i].getValue(columns[0]);
				var s_vendor_name	=	searchResults[i].getValue(columns[1]);
				var s_federal_id	=	searchResults[i].getValue(columns[2]);
				var s_emailaddress = searchResults[i].getValue(columns[3]);
				var s_phonenumber = searchResults[i].getValue(columns[4]);
				var s_v_internalid = searchResults[i].getValue(columns[5]);
				var b_active = searchResults[i].getValue(columns[6]);
				var s_contact_name = searchResults[i].getValue(columns[7]);
				if(b_active == 'F'){
					
					s_status = 'Active';
				}else{
					
					s_status = 'Inactive';
				}
				//var s_contracttype = searchResults[i].getText(columns[4]);
				//var s_contactperson =searchResults[i].getValue(columns[5]);
				//var s_status = searchResults[i].getText(columns[6]);
				//nlapiLogExecution('debug', 's_v_internalid', s_v_internalid);	
				a_data.push({ 'record_id': s_record_id, 'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': k,'vendor_emailaddress':s_emailaddress,'phone_number':s_phonenumber,'v_record_id':s_v_internalid,'s_status_val':s_status,'i_contact_name':s_contact_name});
				k++;
			}
			

		}
		
		return a_data;
	}


}


// search code 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function _logValidation(value)  
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		return false;
	}
}


//vendor string compare from netsuite database  
function getvendorDropdown(o_json)
{
	//var list	=	new Array();
	var strOptions	=	'';
	
	//var i_selected_value	=	'';
	strOptions += '[';
	for(var x in o_json)
		{
			if(x < o_json.length-1)
			{
				a_vendor_name.push(o_json[x].vendor_name);
				//nlapiLogExecution('debug', 'o_json[x].vendor_name', o_json[x].vendor_name);
				strOptions += '"'+o_json[x].vendor_name+'",';
							
			}else{
				a_vendor_name.push(o_json[x].vendor_name);
				strOptions += '"'+o_json[x].vendor_name+'"';
			}
		
		}
	strOptions += ']';
	return strOptions;
	//return getOptions(list, i_selected_value);
}

function getvendorJSON()
{
	var a_data	=	getvendorList();
	//nlapiLogExecution('debug', 'getvendorJSON', a_data);
	var o_json	=	new Object();
	
	for(var i = 0; i < a_data.length; i++){
			
			var o_vendor	=	{'id':a_data[i].s_vendor_id, 'value':a_data[i].s_vendor_name};
			//nlapiLogExecution('Debug', 'o_customer', JSON.stringify(o_customer));
			if(o_json[a_data[i].vendor_id] != undefined)
				{
					o_json[a_data[i].vendor_id].list.push(o_vendor);
				}
			else
			{
				o_json[a_data[i].vendor_id]	=	{'name': a_data[i].vendor_name};
			}
			
		}
	//nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
	//nlapiLogExecution('debug', 'getvendorJSON', a_data);
	return a_data;
	
}
function getvendorList()
{
	var filters =	new Array();
	//filters[0]	=	new nlobjSearchFilter('isinactive',null,'is',"F");
	filters[0]	=	new nlobjSearchFilter('category',null,'anyof',12);
	
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('internalid');
	columns[1]	=	new nlobjSearchColumn('companyname');
	columns[2]	=	new nlobjSearchColumn('isinactive');
	
	var a_data	=	new Array();
	var searchResults	=	searchRecord('vendor', null, filters, columns);
	for(var i = 0; i < searchResults.length; i++)
	{
		
		var s_vendor_id	=	searchResults[i].getValue(columns[0]);
		var s_vendor_name	=	searchResults[i].getValue(columns[1]);
		var s_vendor_inactive	=	searchResults[i].getValue(columns[2]);
		//nlapiLogExecution('debug', 's_vendor_inactive', s_vendor_inactive);
		if(s_vendor_name != '' && s_vendor_name != null){
			
			a_data.push({ 'vendor_id': s_vendor_id, 'vendor_name': s_vendor_name,'isinactive':s_vendor_inactive});
		}
		
	}
	
	return a_data;
}



// END OBJECT CALLED/INVOKING FUNCTION =====================================================