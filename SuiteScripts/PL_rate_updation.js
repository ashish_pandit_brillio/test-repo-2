/**
 *@NApiVersion 2.x
  @NScriptType ScheduledScript
 */
 define(['N/record','N/error','N/log','N/search','N/currency','N/runtime'],function(record,error, log ,search,currency,runtime){

    function execute(context) {
        var date = new Date();
		var mon= date.getMonth();
		var month= getMonthCompleteName(mon);
	//	var cost_val = 70;
		var start_date = new Date(date.getFullYear(), date.getMonth(), 1);
		var scriptObj = runtime.getCurrentScript();
		var cost=scriptObj.getParameter('custscript_cost_value');
		log.debug('cost',cost);
		
        var usd_inr_rate = currency.exchangeRate({
                source: 'USD',
                target: 'INR',
                date: start_date
		});
       var usd_gbp_rate = currency.exchangeRate({
                source: 'USD',
                target: 'GBP',
                date: start_date
		});
	 var usd_eur_rate = currency.exchangeRate({
                source: 'USD',
                target: 'EUR',
                date: start_date
		});
      var usd_aud_rate = currency.exchangeRate({
                source: 'USD',
                target: 'AUD',
                date: start_date
		});
	var usd_ron_rate = currency.exchangeRate({
                source: 'USD',
                target: 'RON',
                date: start_date
		});
		var usd_cad_rate = currency.exchangeRate({
                source: 'USD',
                target: 'CAD',
                date: start_date
		});
		
       var exchangeRate_rec= record.create({
			type: 'customrecord_pl_currency_exchange_rates'
		});
      exchangeRate_rec.setValue({fieldId:'custrecord_pl_currency_exchange_revnue_r',value: usd_inr_rate});
      
      exchangeRate_rec.setValue({fieldId:'custrecord_gbp_converstion_rate',value: (1/usd_gbp_rate)});
      exchangeRate_rec.setValue({fieldId:'custrecord_eur_usd_conv_rate',value: (usd_eur_rate)});
      exchangeRate_rec.setValue({fieldId:'custrecord_aud_to_usd',value: (usd_aud_rate)});
		exchangeRate_rec.setText({fieldId:'custrecord_pl_currency_exchange_month',text: month});
		var fullyear = date.getFullYear();
		fullyear= fullyear.toString();
		exchangeRate_rec.setText({fieldId: 'custrecord_pl_currency_exchange_year',text: fullyear});
		exchangeRate_rec.setValue({fieldId: 'custrecord_pl_currency_exchange_cost',value:cost});
		
		exchangeRate_rec.setValue({fieldId:'custrecord_usd_ron_conversion_rate',value: usd_ron_rate});//Added on 7-08-2021
		exchangeRate_rec.setValue({fieldId:'custrecord_usd_cad_conversion_rate',value: usd_cad_rate});//Added on 7-08-2021
		
      var recordId = exchangeRate_rec.save({
			enableSourcing: true,
			ignoreMandatoryFields: true
			});
	}
	 return {
        execute: execute
     };
});
	
//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 0)
		s_mont_complt_name = 'January';
	if(month == 1)
		s_mont_complt_name = 'February';
	if(month == 2)
		s_mont_complt_name = 'March';
	if(month == 3)
		s_mont_complt_name = 'April';
	if(month == 4)
		s_mont_complt_name = 'May';
	if(month == 5)
		s_mont_complt_name = 'June';
	if(month == 6)
		s_mont_complt_name = 'July';
	if(month == 7)
		s_mont_complt_name = 'August';
	if(month == 8)
		s_mont_complt_name = 'September';
	if(month == 9)
		s_mont_complt_name = 'October';
	if(month == 10)
		s_mont_complt_name = 'November';
	if(month == 11)
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}