/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_PR_Cancellation.js
	Author      : Shweta Chopde
	Date        : 29 April 2014
    Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================

var a_cancel_array = new Array();



// BEGIN FIELD CHANGED ==============================================

function fieldChanged_cancelPR(type, name, linenum)
{
	var i_recordID = nlapiGetRecordId();
	
	if(name == 'custrecord_select_cancel_pr')
	{
	  var i_select_cancel_PR =  nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_select_cancel_pr')		
	 
      var i_item =  nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prmatgrpcategory')		
	  var i_SR_No =  nlapiGetCurrentLineItemValue('recmachcustrecord_purchaserequest','custrecord_prlineitemno')		
	  var i_index_of =  nlapiGetCurrentLineItemIndex('recmachcustrecord_purchaserequest')	
	
	/*
 alert(' Select Cancel PR -->'+i_item)

 alert(' SR No -->'+i_SR_No)

 alert(' Index Of-->'+i_index_of)

*/

	
		 if(i_select_cancel_PR == 'T')
		 {
		 	a_cancel_array.push(i_SR_No+'###'+i_index_of+'###'+i_item+'###'+i_recordID) 
			 
		 }//Select Cancel PR
			
	}//Cancel PR
	
}//Field Changed Cancel PR

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================



function cancel_PR(i_recordID)
{	
   //alert(' Cancel Array -->'+a_cancel_array)
	var a_split_array = new Array();
		
	if(_logValidation(a_cancel_array)) 
	{
		for(var fg=0;fg<a_cancel_array.length;fg++)
		{		
		a_split_array = a_cancel_array[fg].split('###');
			
	       var i_SR_No =  a_split_array[0]
	       var i_index_of = a_split_array[1]
	       var i_item = a_split_array[2]
	       var i_PR_Req_ID = a_split_array[3]
		
		 /*
 alert(' i_SR_No -->'+i_SR_No)
		  alert(' i_index_of -->'+i_index_of)
		  alert(' i_item -->'+i_item)
		  alert(' i_PR_Req_ID -->'+i_PR_Req_ID)
*/
		  
		   var a = new Array();
          a['User-Agent-x'] = 'SuiteScript-Call';
		  
		  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=221&deploy=1&custscript_pr_req_id_c=' + i_PR_Req_ID+'&custscript_s_no_cn=' + i_SR_No+'&custscript_index_of_c=' + i_index_of+'&custscript_item_c=' + i_item, null, a);
      	/*
	                    
		  var i_employeeID = resposeObject.getBody();	
	 
	         if(_logValidation(i_employeeID))
			 {
			 	 nlapiSetFieldValue('custrecord_requisitionerid',i_employeeID,true,true)
			 }//Employee ID	
        */
		  

		/*
   var i_PR_ID = search_PR_Item_recordID(i_PR_Req_ID,i_SR_No,i_item)
		   alert(' i_PR_ID -->'+i_PR_ID)
		
		    if(_logValidation(i_PR_ID))
			{					
			   var o_PR_OBJ = nlapiLoadRecord('customrecord_pritem',i_PR_ID)
				
				if(_logValidation(o_PR_OBJ))
				{
				  o_PR_OBJ.setFieldValue('custrecord_prastatus',22)			
					
				   var i_PR_submitID = nlapiSubmitRecord(o_PR_OBJ,true,true)				 
				  alert(' i_PR_submitID -->'+i_PR_submitID)				
				}//PR OBJ			
					
						
			}
*/		
			
		}//Cancel Array
		
	}//Cancel Array

	location.reload();
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

/*
function search_PR_Item_recordID(i_recordID)
{	
   var i_internal_id;
   
   var a_PR_ID_array = new Array();
     
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
  
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
			
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
	   for(var i=0;i<a_seq_searchresults.length;i++)
	   {
	    	i_internal_id = a_seq_searchresults[i].getValue('internalid');
		//    alert(' Internal ID -->' + i_internal_id);
			
			a_PR_ID_array.push(i_internal_id);
	   }  
		
	}	
	return a_PR_ID_array;	
}

*/


function search_PR_Item_recordID(i_recordID,i_SR_No_QA,i_item_QA)
{
    nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID ==' +i_recordID);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA ==' +i_SR_No_QA);
   nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA ==' +i_item_QA);
	   	
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',i_recordID);
   filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',parseInt(i_SR_No_QA));
   filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null, 'is',i_item_QA);
	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
   columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
   columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');
   
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem',null,filter,columns);
	
	if (a_seq_searchresults != null && a_seq_searchresults != '' && a_seq_searchresults != undefined)
	{	
	 nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'a_seq_searchresults ==' +a_seq_searchresults.length);
	 
	 for(var ty=0;ty<a_seq_searchresults.length;ty++)
	 {
	 	i_internal_id = a_seq_searchresults[ty].getValue('internalid');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' Internal ID -->' + i_internal_id);
		
		var i_PR = a_seq_searchresults[ty].getValue('custrecord_purchaserequest');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_PR ID -->' + i_PR);
		
		var i_line_no = a_seq_searchresults[ty].getValue('custrecord_prlineitemno');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_line_no -->' + i_line_no);
		
		var i_item = a_seq_searchresults[ty].getValue('custrecord_prmatgrpcategory');
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->' + i_item);
		
		if((i_PR == i_recordID)&&(i_line_no == i_SR_No_QA)&&(i_item == i_item_QA))
		{
			i_internal_id = i_internal_id
			break
		}
		
	 }//LOOP
	 
		
		
	}
	
	return i_internal_id;
	
}