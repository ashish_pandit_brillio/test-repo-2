/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Aug 2016     shruthi.l
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){

	var method=request.getMethod();
	var employee_id= request.getParameter('custparam_employee_id'); 
	var feedback_record_id = request.getParameter('feedback_record_id');
	// Get the employee details
  
	var filters = new Array();
	filters[0]  = new nlobjSearchFilter('internalid',null,'anyof',employee_id);
   
	var columns   = new Array();
	columns[0]    = new nlobjSearchColumn('firstname');
	columns[1]    = new nlobjSearchColumn('custentity_fusion_empid');
	columns[2]    = new nlobjSearchColumn('department');
	columns[3]    = new nlobjSearchColumn('title');
	columns[4]    = new nlobjSearchColumn('custentity_actual_hire_date');
	columns[5]    = new nlobjSearchColumn('custentity_probationenddate');
	columns[6]    = new nlobjSearchColumn('custentity_reportingmanager');
	columns[7]    = new nlobjSearchColumn('location');
	columns[8]    = new nlobjSearchColumn('middlename');
	columns[9]    = new nlobjSearchColumn('lastname');
	columns[10]	  = new nlobjSearchColumn('email', 'custentity_reportingmanager');
	//columns[11]   = new nlobjSearchColumn('custrecord_hrbusinesspartner','department');Commented by Shravan on HRBP integrn
	columns[11]  =  new nlobjSearchColumn('custentity_emp_hrbp'); /// Added by shravan for HRBP integration on 7-dec-2020	
	columns[12]   = new nlobjSearchColumn('custentity_extended_confirmation_action');
   var emp_search = nlapiSearchRecord('employee',null,filters,columns);
   
   //  var hr_bp_id = emp_search[0].getValue('custrecord_hrbusinesspartner','department'); Commented by Shravan on HRBP integrn
	var hr_bp_id = emp_search[0].getValue('custentity_emp_hrbp'); /// Added by shravan for HRBP integration on 7-dec-2020
   var hr_bp_email = null;
   
   if(isNotEmpty(hr_bp_id)){
			hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
			hr_bp_firstname = nlapiLookupField('employee',hr_bp_id, 'firstname');
			hr_bp_lastname = nlapiLookupField('employee',hr_bp_id, 'lastname');
		}
		else
		{
			hr_bp_email = 'hrbpteam@brillio.com' ; //'hrb@brillio.com'; // added on 25 May
			hr_bp_firstname = '';
			hr_bp_lastname = '';
		} /// Added by shravan 
  
	if (method == 'GET' )
	{
		if(isNotEmpty(emp_search))
		{
			if(emp_search[0].getValue('custentity_extended_confirmation_action')=='F'){
		
			var file = nlapiLoadFile(232269);   //load the HTML file
			var contents = file.getValue();    //get the contents
			var objReplaceValues=new Object();
			objReplaceValues['employee_name']=emp_search[0].getValue('firstname');
            objReplaceValues['employee_number']=emp_search[0].getValue('custentity_fusion_empid');
            objReplaceValues['employee_pratcice']=emp_search[0].getText('department');
			objReplaceValues['employee_designation']=emp_search[0].getValue('title');
            objReplaceValues['date_of_joining']=emp_search[0].getValue('custentity_actual_hire_date');
            objReplaceValues['review_period']=emp_search[0].getValue('custentity_probationenddate');
            objReplaceValues['employee_reporting_manager']=emp_search[0].getText('custentity_reportingmanager');
            objReplaceValues['employee_location']=emp_search[0].getText('location');
            
			contents = replaceValues(contents, objReplaceValues);
			response.write(contents);          //render it on  suitlet
		}
		
		else{
			throw "Your feedback is filled";
		}
			}
		
	}
	else
	{
		//var form = nlapiCreateForm("Suitelet - POST call");
		var feedback = new Object();
		feedback['employee_id'] =  request.getParameter('custparam_employee_id');
		feedback['feedback_record_id'] =request.getParameter('feedback_record_id');
		feedback['Response9'] = request.getParameter('OptionsGroup9');
		//feedback['Comments1'] = request.getParameter('txtComments1');
		feedback['Comments2'] = request.getParameter('txtComments2');
		feedback['actionperformed'] = request.getParameter('whatclicked');
		
		feedback['employeeNumber'] = emp_search[0].getValue('custentity_fusion_empid');
		feedback['employee_pratcice'] = emp_search[0].getText('department');
		feedback['actual_hire_date'] = emp_search[0].getValue('custentity_actual_hire_date');
		feedback['probation_end_date'] = emp_search[0].getValue('custentity_probationenddate');
		feedback['location'] = emp_search[0].getValue('location');
		feedback['reporting_manager'] = emp_search[0].getValue('custentity_reportingmanager');
		
	
		
		var status = saveRequest(feedback);
		
		if(isNotEmpty(emp_search))
		{
			var firstName = emp_search[0].getValue('firstname');
			var middleName = emp_search[0].getValue('middlename');
			var lastName = emp_search[0].getValue('lastname');
			var employee_pratcice = emp_search[0].getValue('department');
			
			var employeeNumber=emp_search[0].getValue('custentity_fusion_empid');
			var title=emp_search[0].getValue('title');
			var actual_hire_date=emp_search[0].getValue('custentity_actual_hire_date');
			var probation_end_date=emp_search[0].getValue('custentity_probationenddate');
			var overallRating = request.getParameter('OptionsGroup9');
			var reporting_manager_email = emp_search[0].getValue('email', 'custentity_reportingmanager');
			var employee_location=emp_search[0].getValue('location');
			
			if (overallRating == 'Short of Expectation')
			{
			sendEmail(firstName, employee_id,reporting_manager_email,overallRating,actual_hire_date,probation_end_date,hr_bp_email);
			}
			//generatePDF(firstName,middleName,lastName,title,employeeNumber,overallRating,actual_hire_date,probation_end_date);
		}
   
		var thanks_note = nlapiLoadFile(63444);   //load the HTML file
		var thanks_contents = thanks_note.getValue();    //get the contents
		response.write(thanks_contents);     //render it on  suitlet
	}

}


//Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
 {
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
     content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
 }
             
 return content;
}   

function saveRequest(feedback)
{
	try
 {
     
     var feedback_form = nlapiLoadRecord('customrecord_emp_confirmation_feedback',feedback.feedback_record_id );
	// feedback_form.setFieldValue('custrecord_njcp_employee_name', feedback.employee_id);
		
    // feedback_form.setFieldValue('custrecordcustrecord_new_question_test1',"Please provide the reason to extend");
	// feedback_form.setFieldValue('custrecord__new_question_test_2', "Probation extension months");
	 //feedback_form.setFieldValue('custrecord__new_question_test_3', "Please click the below button to terminate");
	 //feedback_form.setFieldValue('custrecord_new_question_test_4', "Termination Reason");
	 feedback_form.setFieldValue('custrecord_new_comments_test_2', feedback.Response9);
		
//	 feedback_form.setFieldValue('custrecord_employee_fusion_num', feedback.employeeNumber);
   //  feedback_form.setFieldText('custrecord_employee_department_conf', feedback.employee_pratcice);
                            
   //  feedback_form.setFieldValue('custrecord_date_of_joining_conf', feedback.actual_hire_date);
   //  feedback_form.setFieldValue('custrecord_review_period_conf', feedback.probation_end_date);
   //  feedback_form.setFieldValue('custrecord_reporting_manager', feedback.reporting_manager);
    // feedback_form.setFieldValue('custrecord_location_conf', feedback.location);
    
     if(feedback.actionperformed=="Confirm"){
    	 feedback_form.setFieldValue('custrecord_confirm', 'T');
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_extended_confirmation_action', 'T');
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_notconfirmed', 'F');
     }else if(feedback.actionperformed=="Terminate"){
    	 feedback_form.setFieldValue('custrecord_termination', 'T');
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_extended_confirmation_action', 'T');
    	 feedback_form.setFieldValue('custrecord_termination_comments', feedback.Comments2);
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_notconfirmed', 'T');
     }
     
	 feedback_form.setFieldValue('custrecord_njcp_employee_name_test1', feedback.employee_id);
	 
	 var feedback_record_id = nlapiSubmitRecord(feedback_form, true,true);                                    
    
     nlapiLogExecution('debug', 'Record Saved', feedback_record_id);
	}
	catch(err)
	{
		nlapiLogExecution('error', 'Record not saved', err);
	}
}


function sendEmail(firstName, emp_id,custentity_reportingmanager_email,overallRating,actual_hire_date,probation_end_date,hr_bp_email)
{
	try
	{	
		var fileObj = nlapiLoadFile(175408);
		var email_to = emp_id;
		if(overallRating == 'Short of Expectation')
		{
			email_to = 'japnit.sethi@brillio.com';
		}
		
		
		var mailTemplate =serviceTemplate(firstName,overallRating,actual_hire_date,probation_end_date);
		nlapiLogExecution('Debug', 'mailTemplate', mailTemplate.MailBody);
		nlapiSendEmail('10730', email_to, mailTemplate.MailSubject, mailTemplate.MailBody,[custentity_reportingmanager_email, 'japnit.sethi@brillio.com' ,hr_bp_email],null,null,{entity: emp_id});
		
	}
	catch(err)
	{
		nlapiLogExecution('error', 'sendEmail', err);
		throw err;
	}
}

function serviceTemplate(firstName,overallRating,custentity_actual_hire_date,probation_end_date)
{

var htmltext = '';
var subject ='';	
	
	//Search the rating value if rating is Outstanding,Exceed Expectations, Met Expectations Confirmation Form will be Generated

	 if (overallRating == 'Short of Expectation')
	{
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Hi  </p>';
		//htmltext += &nbsp;
		htmltext += '<p> This is inform you that '+ firstName +'\'s manager has wished to terminate his services in Brillio. Please take it forward with the further process..</p>';
					
					
		htmltext += '<p>Regards,</p>';
		htmltext += '<p>Team HR</p>';

		
	
		htmltext += '</td></tr>';
		htmltext += '</table>';
		subject +='Employee Termination';
		
	}

return {
     MailBody : htmltext,
     MailSubject : subject,    
 };

}

	
