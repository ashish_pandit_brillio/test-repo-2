/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Aug 2016     shruthi.l
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
 if(type!='create'){
	
	 nlapiGetField('custrecord_ts_upload_csv_file').setDisplayType('inline');
	
	 
	 if(nlapiGetFieldValue('custrecord_ts_upload_err_log')==null){
	var message = '<html>';
	message += '<head>';
	message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
	message += '<meta charset="utf-8" />';
	message += '</head>';
	message += '<body>';
	var i_counter = '0%'
	message += "<div id=\"my-progressbar-container\">";
	message += "            ";
	message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
	message += "<img src='https://system.na1.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:35px;width:35px;align=left;'/>";
	message += "        <\/div>";
	message += '</body>';
	message += '</html>';
	nlapiSetFieldValue('custrecord_ts_upload_processing',message);
 }
 }
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
  if(type=='create')
  {
	  var flag=0,flag2=0;
	  var csv_file_id =nlapiGetFieldValue('custrecord_ts_upload_csv_file');
	  
	  var file_data = getFileContents(csv_file_id);
		
		if(_logValidation(file_data))
			{
				create_auto_timesheet(csv_file_id,nlapiGetRecordId());
				flag2=1;
			}
  }
}

function _logValidation(value)
{
    if (value != null && value != '' && value != undefined) 
	{
        return true;
    }
    else 
	{
        return false;
    }
}

function create_auto_timesheet(csv_file_id,custom_rec_Id)
{
	
	nlapiLogExecution('debug', 'csv_file_id in sut', csv_file_id);
	
	
	var params=new Array();

 	params['custscript_csv_file_id_ts'] = csv_file_id;
 	params['custscript_custom_rec_id'] = custom_rec_Id;
	
	var status = nlapiScheduleScript('customscript_sch_ts_import',null,params);
	nlapiLogExecution('debug', 'custom_rec_Id', custom_rec_Id);
	
}

function getFileContents(csv_file_id)
{
	var csvFile = nlapiLoadFile(csv_file_id);
    var csvContent = csvFile.getValue();
	var rows_data = csvContent.split('\r\n');
	nlapiLogExecution('debug', 'rows.data', rows_data.length);
	return rows_data;
}

