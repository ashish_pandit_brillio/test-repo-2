// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : REST_GetEmployeeAndSkills.js
	Author      : ASHISH PANDIT
	Date        : 30 JAN 2019
    Description : RESTlet to get employee and skill list 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function getRESTletEmployeeSearch(dataIn) {
	nlapiLogExecution('DEBUG','Start dataIn ',JSON.stringify(dataIn));
	var mode = dataIn.searchby;
	nlapiLogExecution('DEBUG','mode',mode);
  	var select;
	if(mode == 1)
	{
		try
		{
			return getEmployeeList('customsearch_employee_search',select,'Select Employee');
		}
		catch(error)
		{
			nlapiLogExecution('Debug','error ',error);
		}
	}
	if(mode ==2)
	{
		try
		{
			var arrSkills = [];
			var searchResult = getSkillsList();
			if(searchResult){
				for (var int = 0; int < searchResult.length; int++) {
					var dataOut = {};
					dataOut.id = searchResult[int].getValue('custrecord_emp_primary_skill');
					dataOut.name = searchResult[int].getText('custrecord_emp_primary_skill');
					arrSkills.push(dataOut);
				}
			}
			//response.write(JSON.stringify(arrSkills));
			return arrSkills;
		}
		catch(error)
		{
			nlapiLogExecution('Debug','error ',error);
		}
	}
}
/******************************Search To Get Skill List************************************/	
function getSkillsList() {
	try {
			var skill_filter = 
		        [
		            //[ 'custrecord_employee_skil_family', 'is', i_skillFamily],               
		              //'and',
					[ 'isinactive', 'is', 'F' ] 
				];

		var skill_search_results = searchRecord('customrecord_employee_skills', null, skill_filter,
		        [ 
					new nlobjSearchColumn("custrecord_emp_primary_skill"),
					new nlobjSearchColumn("internalid")
				]);

		nlapiLogExecution('debug', 'Results count',skill_search_results.length);

		if (skill_search_results.length == 0) {
			throw "You don't have any Primary Skills under Family Selected.";
		}
	
		return skill_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

/*******************************SEARCH RECORD CODE**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

/*********************************************Function to get Search Values**********************************************/
function getEmployeeList(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('entityid');
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'name':a_search_results[i].getValue('entityid'), 'id':a_search_results[i].id});
		}
	nlapiLogExecution('Debug','list_values ',JSON.stringify(list_values));
	return list_values;//getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}