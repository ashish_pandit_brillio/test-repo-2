

/**
* Module Description
* 
 * Version    Date            Author           Remarks
* 1.00       6th March 2018  Manikandan V
*
*/
/**
* @param {Object} dataIn Parameter object
* @returns {Object} Output object
*/
var TermList = [];

function postRESTlet(dataIn) {
    nlapiLogExecution('debug', 'Inside in postRESTlet function');
    try {
        nlapiLogExecution('debug', 'Inside in postRESTlet function and in try block');
        var response = new Response();
        var current_date = nlapiDateToString(new Date());

        //Log for Current Date
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));

        var requestType = 'GET';
        switch (requestType) {
            case M_Constants.Request.Get:

                if (dataIn) {
                    response.Data = employeeTermination(dataIn);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
        }

    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;
        response.Status = false;
    }

    nlapiLogExecution('debug', 'response', JSON.stringify(response));
    return response;
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function dateFormat(date) {
    var date_return = '';
    if (date) {
        var dateObj = date.split('-');
        var month = dateObj[1];
        var year = dateObj[0];
        var date_d = dateObj[2].split('T');
        //var date_d = dateObj[2].split('T');
        nlapiLogExecution('DEBUG', 'dateObj[2]', dateObj[2]);
        var temp = new Array();
        temp = dateObj[2].substring(0, 2);
        //date_d = date_d.split(',')[0];


        date_return = month + '/' + temp + '/' + year;
    }
    return date_return;
}

function formatDate(fusionDate) {

    if (fusionDate) {
        // fusionDate = "2016-07-14T00:00:00.000Z";
        var datePart = fusionDate.split('T')[0];
        var dateSplit = datePart.split('-');
        // MM/DD/ YYYY
        var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
            dateSplit[0];
        return netsuiteDateString;
    }
    return "";
}

function FusionRecordSearch(fusionNumber, termDate) {
    if (fusionNumber != null) {
        var search_employee = nlapiSearchRecord('employee', null, [
            new nlobjSearchFilter('custentity_fusion_empid', null,
                'is', fusionNumber)
            //                ,
            //   new nlobjSearchFilter('custentity_employee_inactive',
            //   null, 'is', 'F')
        ], [new nlobjSearchColumn(
            'internalid')]);
        if (search_employee) {
            var id = search_employee[0].getValue('internalid');
            var employeeRecord = nlapiLoadRecord('employee', id);
            //Update Last working date in Resource Allocation
            //nlapiSearchRecord(type, id, filters, columns)
            var resourceAllocationSearch = nlapiSearchRecord('resourceallocation', null, [
                new nlobjSearchFilter('internalid', 'employee', 'anyof', id),
                new nlobjSearchFilter('enddate', null, 'onorafter', formatDate(termDate))
            ]);
            if (isArrayNotEmpty(resourceAllocationSearch)) {
                for (var index = 0; index < resourceAllocationSearch.length; index++) {
                    nlapiLogExecution('DEBUG', 'Allocation', resourceAllocationSearch.length);
                    var resAlloRec = nlapiLoadRecord('resourceallocation', resourceAllocationSearch[index].getId(), {
                        recordmode: 'dynamic'
                    });
                    resAlloRec.setFieldValue('enddate', formatDate(termDate)); // Allocation End date
                    resAlloRec.setFieldValue('custeventbenddate', formatDate(termDate)); // Billing End date
                    resAlloRec.setFieldValue('custevent_allocation_status', '22'); // Allocation status, Employee terminated 
                    resAlloRec.setFieldValue('notes', 'Employee terminated, hence allocation end date changed');
                    resAlloRec.setFieldValue('custevent_ra_last_modified_by', 442);
                    resAlloRec.setFieldValue('custevent_ra_last_modified_date', nlapiDateToString(new Date(), 'datetimetz'));
                    nlapiSubmitRecord(resAlloRec, false, true);
                    nlapiLogExecution('DEBUG', 'Allocation ended');
                    nlapiLogExecution('DEBUG', 'Allocation', resAlloRec);
                }
            }

            // Update the subtier records
            // Get all subtier records that end after the LWD
            try {
                // Update the subtier records
                // Get all subtier records that end after the LWD
                var subtierSearch = nlapiSearchRecord('customrecord_subtier_vendor_data', null, [new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', id), new nlobjSearchFilter('custrecord_stvd_end_date', null,
                    'onorafter', formatDate(termDate))]);

                // change the end date of all the found subtier record
                if (subtierSearch) {

                    for (var index = 0; index < subtierSearch.length; index++) {
                        var subTierRec = nlapiLoadRecord(
                            'customrecord_subtier_vendor_data',
                            subtierSearch[index].getId(), {
                                recordmode: 'dynamic'
                            });

                        // if start date is after the termination date,
                        // change
                        // the start date
                        if (nlapiStringToDate(subTierRec.getFieldValue('custrecord_stvd_start_date')) > formatDate(termDate)) {
                            subTierRec.setFieldValue('isinactive', 'T');
                            subTierRec.setFieldValue(
                                'custrecord_stvd_start_date', formatDate(termDate));
                        }

                        subTierRec.setFieldValue(
                            'custrecord_stvd_end_date', formatDate(termDate));

                        nlapiSubmitRecord(subTierRec, false, true);
                    }
                }
            } catch (erro) {
                nlapiLogExecution('ERROR', 'While Updating subtier record',
                    erro);
            }
            employeeRecord.setFieldValue('giveaccess', 'F'); //Removing access
            employeeRecord.setFieldValue('custentity_employee_inactive', 'T'); //System Inactice
            //employeeRecord.setFieldValue('isjobresource', 'F');       //Project Resource Remove
            employeeRecord.setFieldValue('custentity_lwd', formatDate(termDate)); //Update the termination date at employee master:
            var internalId = nlapiSubmitRecord(employeeRecord);
            TermList.push({
                'Employee ID': fusionNumber
            });
        }
    }
    return TermList;
}

function employeeTermination(dataIn) {
    try {

        var employeeTerm = dataIn.DATA_DS.G_1.G_2.FILE_FRAGMENT.EmployeeExtract.AssignmentData; //JSON
        nlapiLogExecution('DEBUG', 'Checking whether it is array or object');
        if (isArray(employeeTerm)) {
            nlapiLogExecution('DEBUG', 'Going through inside Array');
            for (var i = 0; i < employeeTerm.length; i++) {
                var emp_assignment = employeeTerm[i].Assignment;
                var fusionNumber = emp_assignment.FusionID;
               var termDate = emp_assignment.Termination_Date;
                nlapiLogExecution('DEBUG', 'fusionNumber', fusionNumber);
                nlapiLogExecution('DEBUG', 'Termination_date', termDate);
                //var d = termDate.slice(0, 10).split('-');
                // Termination_date = w[1] + '/' + w[2] + '/' + w[0];
                var result = FusionRecordSearch(fusionNumber, termDate);


            }
        } else {
            nlapiLogExecution('DEBUG', 'Going through inside object');
            var fusionNumber = employeeTerm.Assignment.FusionID;
            var termDate = employeeTerm.Assignment.Termination_Date;
            nlapiLogExecution('DEBUG', 'fusionNumber in object', fusionNumber);
            nlapiLogExecution('DEBUG', 'Termination_date in object', termDate);
            var result = FusionRecordSearch(fusionNumber, termDate);


        }
        nlapiLogExecution('DEBUG', 'Result', JSON.stringify(result));
    } catch (e) {
        nlapiLogExecution('ERROR', 'postRESTlet', e);
    }
    return TermList;

}
