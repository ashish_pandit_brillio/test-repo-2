/**
 * 
 * Version Date Author Remarks 1.00 26 Aug 2016 Deepak MS
 * 
 */

function PageInit() {
try{
			var main_div = document.getElementsByClassName("uir-page-title")[0];
			var newDiv = document.createElement("div");
			newDiv.innerHTML = "<span style='color: black;font-size: 14px;float: left;border: 1px solid;padding: 5px;background-color: beige;'><b>Note: </b>If any Leaves/Holidays fall in the week. Please update the following two columns respectively in the CSV file.<br/>" +
								"<b>Task : </b>Leave as 'Leave (Project Task)', Holiday as 'Holiday (Project Task)'<br/>" +
								"<b>Service Item : </b>For leave as 'Leave' and holiday as 'Holiday'</span>";
			main_div.appendChild(newDiv);
		}
catch(e){
	nlapiLogExecution('ERROR','Process Error',e);
}
}

function fieldChanged(type, name, linenum){


    if (name == 'custpage_startdate') {
        var s_period = nlapiGetFieldValue('custpage_startdate')
        //alert("S_period=======" + s_period)
        if (s_period != null && s_period != '' && s_period != 'undefined') {
            var date = nlapiStringToDate(s_period)
            //alert("date=======" + date)
            if (date != null && date != '' && date != 'undefined') {
                var day = date.getDay()
                //alert("day=======" + day)
                if (day != 0) {
                    alert("Please select start date as Sunday.")
                    nlapiSetFieldValue('custpage_startdate', '')
                }
            }
        }
        
    }
 
}
