/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Othername_data(obj_Request) {
         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var obj_Response_Return = {};
             if (s_Request_Data == 'OTHERNAME') {
                 obj_Response_Return = get_OtherName_Data(obj_Search);
             }
         } 
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Othername_data


     return {
         post: Send_Othername_data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_OtherName_Data(obj_Search) {
 try {
    var othernameSearchObj = obj_Search.create({
        type: "othername",
        filters:
        [
           ["type","anyof","OtherName"]
        ],
        columns:
        [
            obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
            obj_Search.createColumn({
              name: "entityid",
              sort: obj_Search.Sort.ASC,
              label: "ID"
           })
        ]
     });
     var searchResultCount = othernameSearchObj.runPaged().count;
     log.debug("othernameSearchObj result count",searchResultCount);
     var myResults = getAllResults(othernameSearchObj);
     var arr_Othname_json = [];
     myResults.forEach(function (result) {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.getValue({ name: "internalid"}),
             'Name': result.getValue({ name: "entityid"}),
         };
         arr_Othname_json.push(obj_json_Container);
         return true;
     });
     return arr_Othname_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('get_OtherName_Data s_Exception== ', s_Exception);
 } //// End of catch 
} ///// End of function get_OtherName_Data()


function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}