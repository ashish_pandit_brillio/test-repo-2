/**
 * @author Jayeshh
 */

function changeButtonColor()
{
	alert('inside change colour code');
	
	$('tr#people_plan_sublist_row_7').css({ 'color': 'blue'});
	
	$('tr#people_plan_sublist_row_7').css({ 'color': 'blue'});
}
/*function page_init()
{
	 nlapiSetFieldValue('custpage_testfield','Monthly Split will be updated if you enter Practice wise Revenue ');
}*/
/*function hide_remove_button()
{
	var obj_remove_bttn = document.getElementById('tbl_people_plan_sublist_remove');
	if(obj_remove_bttn)
		obj_remove_bttn.innerHTML = '';
}*/

//function to validate revenue share doesn't exceed project value
function saveRecord_FP_RevRec()
{
	try
	{
		var s_request_type = nlapiGetFieldValue('share_type');
		var s_project_region = nlapiGetFieldText('proj_region');
		var s_project_cust = nlapiGetFieldText('customer_selected');
		var s_project_name = nlapiGetFieldText('proj_selected');
		var s_project_strt = nlapiGetFieldValue('proj_strt_date');
		var s_project_end = nlapiGetFieldValue('proj_end_date');
		var s_project_prac = nlapiGetFieldText('proj_practice');
		var s_pro_cust=nlapiGetFieldValue('customer_selected');
		
		
		// Get revenue share record id for data entered by PM
		var i_projectId = nlapiGetFieldValue('proj_selected');
		var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', parseInt(i_projectId)]];
		
		var a_column = new Array();
		a_column[0] = new nlobjSearchColumn('created').setSort(true);
		a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_auto_numb');
		a_column[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
		
		var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
		if (a_get_logged_in_user_exsiting_revenue_cap)
		{
			var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
			var i_revenue_share_stat = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_other_approval_status');
		}
			
		if (s_request_type == 'View') // send mail to practice head
		{
			// Get mail id for practice head
			var s_recipient_mail_id = '';
			var a_recipient_mail_id = new Array();
			s_recipient_mail_id = find_is_sub_practice_appropriate(i_revenue_share_id,2);
			s_recipient_mail_id = s_recipient_mail_id.split(':');
			
			for(var i_recipient_index=0; i_recipient_index<s_recipient_mail_id.length; i_recipient_index++)
			{
				if(s_recipient_mail_id[i_recipient_index])
					a_recipient_mail_id.push(s_recipient_mail_id[i_recipient_index]);
			}
			
			var a_participating_mail_id = new Array();
			
			var s_proj_related_mail_id = find_is_sub_practice_appropriate(i_projectId,6);
			s_proj_related_mail_id = s_proj_related_mail_id.split(':');
					
			if(s_proj_related_mail_id[0])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[0]);
			}
			if(s_proj_related_mail_id[1])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[1]);
			}
			if(s_proj_related_mail_id[2])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[2]);
			}
			if(s_proj_related_mail_id[3])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[3]);
			}
			
			a_participating_mail_id.push('billing@brillio.com');
			//a_participating_mail_id.push('vikash.garodia@brillio.com');
			//a_participating_mail_id.push('bhavanishankar.t@brillio.com');
			a_participating_mail_id.push('team.fpa@brillio.com');
	
			// mail to BO team and participating practice head
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Hello All,</p>';
			strVar += '<p>The Fixed price project with following details has been setup in Netsuite. </p>';
			strVar += '<p>Please review the data immediately and work with the Executing Practice DM/PM offline in case of any change.</p>';
			
			strVar += '<p>Region:- '+s_project_region+'<br>';
			strVar += 'Customer:- '+s_project_cust+'<br>';
			strVar += 'Project:- '+s_project_name+'<br>';
			strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
			strVar += 'Executing Practice:- '+s_project_prac+'</p>';
			
			strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1517&deploy=1&proj_id='+i_projectId+'&mode=view&existing_rcrd_id='+i_revenue_share_id+'&revenue_rcrd_status=1>Link to Revenue share and Budget effort screen in Netsuite</a>';
			
			strVar += '<p>Regards,</p>';
			strVar += '<p>Finance Team</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '41571';
			
			nlapiSendEmail(442, a_recipient_mail_id, 'FP Project Budget Plan submitted for Review: '+nlapiGetFieldText('proj_selected'), strVar,a_participating_mail_id,null,a_emp_attachment);
						
		}
		else if (s_request_type == 'Submit')
		{
			// Get revenue share record id for data entered by PM
			var i_projectId = nlapiGetFieldValue('proj_selected');
			var i_project_executing_practice = nlapiGetFieldValue('proj_practice');
						
			// mail to Executing Practice Head
			var s_executing_practice_head_details = find_is_sub_practice_appropriate(i_project_executing_practice,3);
			s_executing_practice_head_details = s_executing_practice_head_details.split(':');
			if(s_executing_practice_head_details)
			{ 
				var i_executing_practice_head = s_executing_practice_head_details[0];
				var s_executing_practice_head = s_executing_practice_head_details[1];	
			}
			
			var s_executing_practice_head_mail = find_is_sub_practice_appropriate(i_executing_practice_head,4);
			
			var s_participating_mail_id = '';
			var a_participating_mail_id = new Array();
			s_participating_mail_id = find_is_sub_practice_appropriate(i_revenue_share_id,2);
			s_participating_mail_id = s_participating_mail_id.split(':');
			
			for(var i_recipient_index=0; i_recipient_index<s_participating_mail_id.length; i_recipient_index++)
			{
				if(s_participating_mail_id[i_recipient_index])
					a_participating_mail_id.push(s_participating_mail_id[i_recipient_index]);
			}
			
			var s_proj_related_mail_id = find_is_sub_practice_appropriate(i_projectId,6);
			s_proj_related_mail_id = s_proj_related_mail_id.split(':');
					
			if(s_proj_related_mail_id[0])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[0]);
			}
			if(s_proj_related_mail_id[1])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[1]);
			}
			if(s_proj_related_mail_id[2])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[2]);
			}
			if(s_proj_related_mail_id[3])
			{
				a_participating_mail_id.push(s_proj_related_mail_id[3]);
			}
			
			var i_pro_region_id = nlapiGetFieldValue('proj_region');
			var s_region_head_mail = find_is_sub_practice_appropriate(i_pro_region_id,7);
			if(s_region_head_mail)
			{
				a_participating_mail_id.push(s_region_head_mail);
			}
			a_participating_mail_id.push('billing@brillio.com');
		//	a_participating_mail_id.push('vikash.garodia@brillio.com');
		//	a_participating_mail_id.push('bhavanishankar.t@brillio.com');
			a_participating_mail_id.push('team.fpa@brillio.com');
	
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Hello '+s_executing_practice_head+',</p>';
			strVar += '<p>The Fixed price project with the following details has been setup in Netsuite.</p>';
			strVar += '<p>Request you to review the Revenue share and Budget effort plan at the link below and approve the Project setup</p>';
			
			strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1518&deploy=1&proj_id='+i_projectId+'&mode=PendingApproval&existing_rcrd_id='+i_revenue_share_id+'&revenue_rcrd_status=2>Link to Revenue share and Budget effort screen in Netsuite</a>';
			
			strVar += '<p>Region:- '+s_project_region+'<br>';
			strVar += 'Customer:- '+s_project_cust+'<br>';
			strVar += 'Project:- '+s_project_name+'<br>';
			strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
			strVar += 'Executing Practice:- '+s_project_prac+'</p>';
			
			strVar += '<p>Regards,</p>';
			strVar += '<p>Finance Team</p>';
			
			strVar += '<p>Note: Revenue recognition will start only after approval of Budget effort plan</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '41571';
			
			nlapiSendEmail(442, s_executing_practice_head_mail, 'FP Project Budget Plan submitted for Approval: '+nlapiGetFieldText('proj_selected'), strVar,a_participating_mail_id,null,a_emp_attachment);
		}
		else if(i_revenue_share_stat == 2)
		{
			var d_proj_start_date = nlapiGetFieldValue('proj_strt_date');
			var d_proj_end_date = nlapiGetFieldValue('proj_end_date');
			var sow_val=nlapiGetFieldValue('proj_sow_value');
			var f_total_share_mnth_revenue=0;
			var mnth_reven=nlapiGetLineItemCount('revenue_sublist_month_end');
			for (var i_line_mnt = 1; i_line_mnt <= mnth_reven; i_line_mnt++) // iterate through sublist
			{
				var monthBreak = getMonthsBreakup(
					        nlapiStringToDate(d_proj_start_date),
					        nlapiStringToDate(d_proj_end_date));
				for (var j = 0; j < monthBreak.length; j++)
				{
							
					var months = monthBreak[j];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					s_month_name = s_month_name +'_'+ i_year;
					//var amount=nlapiGetLineItemValue(
                  if(i_line_mnt==1)
					var f_share_amount_mnth=nlapiGetLineItemValue('revenue_sublist_month_end',''+s_month_name.toLowerCase(),1);
					//var f_share_amount_mnth_pract=nlapiGetLineItemValue('revenue_sublist_month_end',''+s_month_name.toLowerCase(),i_line_mnt);
					if(!f_share_amount_mnth)
					{
						if(i_line_mnt==1)
						{
						alert('Monthly Revenue cannot be blank.For No revenue, please enter Zero value');
						return false;
						}
					
					}
				//capture amount eneterd at each line level sublist
				if(i_line_mnt==1)
				f_total_share_mnth_revenue = parseFloat(f_total_share_mnth_revenue) + parseFloat(f_share_amount_mnth); // calculate total revenue share entered
			}
			}
			f_total_share_mnth_revenue = parseFloat(f_total_share_mnth_revenue);
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_sut_exempt', 'customdeploy1') + '&i_customer_interal_id='+s_pro_cust, null, null);
			var res_sow = response.getBody();
			//var sow_exempt=nlapiLookupField('customer',s_pro_cust,'custentity_sow_validation_exception');
			if(res_sow!='T')
			{
				if ((f_total_share_mnth_revenue) > parseFloat(nlapiGetFieldValue('proj_sow_value'))) // if revenue share is > project value don't allow to proceed
				{
					var rem=f_total_share_mnth_revenue-sow_val;
					if(parseFloat(rem)> parseFloat(5)){
					alert('Total Revenue Share exceeds Project SOW Value by ' +rem+'. It should be equal to Project SOW Value');
					return false;
					}
				}
				else 
					if ((f_total_share_mnth_revenue) < parseFloat(nlapiGetFieldValue('proj_sow_value'))) // if revenue share is < project value don't allow to proceed
					{
						var rem=sow_val-f_total_share_mnth_revenue;
						if(parseFloat(rem)> parseFloat(5)){
						alert('Total Revenue Share is lessthan Project SOW Value by '+rem+'. It should be equal to Project SOW Value');
						return false;
						}
					}
			}
			for (var j_in = 0; j_in < monthBreak.length; j_in++)
			{	
				var flag=0;
				var total_split=0;
			/*	for(var pract_line_cnt=2;pract_line_cnt<=mnth_reven;pract_line_cnt++)
				{
					var months = monthBreak[j_in];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					s_month_name = s_month_name +'_'+ i_year;
					//var amount=nlapiGetLineItemValue(
					var f_pract_amount = nlapiGetLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase(),pract_line_cnt);
					
					/*if(pract_line_cnt == mnth_reven)
					{
						var f_pract_amount=nlapiGetCurrentLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase());
					}*/
				/*	if(_logValidation(f_pract_amount))
					{
						total_split=parseFloat(total_split)+parseFloat(f_pract_amount);
						flag=1;
								//alert('Monthly Revenue cannot be blank.For No revenue, please enter Zero value');
								//return false;
					}
                  if(flag== parseInt(1) && (!f_pract_amount))
					{
				
					alert('Practice split Share cannot be Blank. No Revenue Enter Zero');
					return false;
					}
				}*/
				
				var monthly_pract_total=nlapiGetLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase(),1);
				 if(!_logValidation(monthly_pract_total)){
					 alert('Practice split Share cannot be Blank. No Revenue Enter Zero');
					return false;
				 }
				
			}
			if(mnth_reven)
			{
				var o_month_revenue = nlapiCreateRecord('customrecord_fp_other_monthly_rev');
				var data_json_mn=[];
				var data_array_mn=[];
				var monthBreakUp = getMonthsBreakup(
					       nlapiStringToDate(d_proj_start_date),
					        nlapiStringToDate(d_proj_end_date));
				for(var i_mnthly_rev_mn=1;i_mnthly_rev_mn<=mnth_reven;i_mnthly_rev_mn++)
				{
					
					for (var j = 0; j < monthBreakUp.length; j++)
					{
							
						var months = monthBreakUp[j];
						var s_month_name = getMonthName(months.Start); // get month name
						var month_strt = nlapiStringToDate(months.Start);
						var month_end = nlapiStringToDate(months.End);
						var i_month = month_strt.getMonth();
						var i_year = month_strt.getFullYear();
						s_month_name = s_month_name +'_'+ i_year;
						//var amount=nlapiGetLineItemValue(
						var amount=nlapiGetLineItemValue('revenue_sublist_month_end',''+s_month_name.toLowerCase(),i_mnthly_rev_mn);
						var s_practice = nlapiGetLineItemValue('revenue_sublist_month_end','monthly_split',i_mnthly_rev_mn);
						//if(!_logValidation(amount))
						//{
						//	amount=0;
					//	}
						data_json_mn.push({
							month:s_month_name,
							amount:amount,
							line_no:i_mnthly_rev_mn,
							practice:s_practice
						});					
					}
					
					
				}
				data_array_mn.push(data_json_mn);
					var data=JSON.stringify(data_array_mn);
					//alert(data);
					o_month_revenue.setFieldValue('custrecord_other_month', data);
					o_month_revenue.setFieldValue('custrecord_fp_rev_rec_other', i_revenue_share_id);
					o_month_revenue.setFieldValue('custrecord_created_split','T');
					var id_mnth_rev = nlapiSubmitRecord(o_month_revenue,true,true);
			}
			alert('It will take some time to generate project detail view kindly hold on for some time');
			//return false;
			
			
			var monthBreakUp = getMonthsBreakup(
				        nlapiStringToDate(d_proj_start_date),
				        nlapiStringToDate(d_proj_end_date));
			
			/*var o_mnth_end_parent = nlapiCreateRecord('customrecord_fp_revrec_month_end_process');
			o_mnth_end_parent.setFieldValue('custrecord_revenue_share_parent',i_revenue_share_id);
			var i_mnth_end_parent_rcrd_id = nlapiSubmitRecord(o_mnth_end_parent,true,true);*/
			//alert('Month End parent id:- '+i_mnth_end_parent_rcrd_id);
			
			var i_current_month = '';
			var a_total_row_clubbed_array = [];
			var i_count_month_end_activity = nlapiGetLineItemCount('people_plan_sublist');
			var line_cnt=nlapiGetLineItemCount('revenue_sublist_month_end');
			for(var i_index_count=1; i_index_count<=i_count_month_end_activity; i_index_count++)
			{
				var i_practice_mnth_end = nlapiGetLineItemValue('people_plan_sublist','parent_practice_month_end',i_index_count);
				var i_practice_mnth_end_text = nlapiGetLineItemText('people_plan_sublist','parent_practice_month_end',i_index_count);
				var i_sub_practice_mnth_end = nlapiGetLineItemValue('people_plan_sublist','sub_practice_month_end',i_index_count);
				var i_sub_practice_mnth_end_text = nlapiGetLineItemText('people_plan_sublist','sub_practice_month_end',i_index_count);
				var i_role_mnth_end = nlapiGetLineItemValue('people_plan_sublist','role_month_end',i_index_count);
				var i_role_mnth_end_text = nlapiGetLineItemText('people_plan_sublist','role_month_end',i_index_count);
				var i_level_mnth_end = nlapiGetLineItemValue('people_plan_sublist','level_month_end',i_index_count);
				var i_level_mnth_end_text = nlapiGetLineItemText('people_plan_sublist','level_month_end',i_index_count);
				var i_location_mnth_end = nlapiGetLineItemValue('people_plan_sublist','location_month_end',i_index_count);
				var i_location_mnth_end_text = nlapiGetLineItemText('people_plan_sublist','location_month_end',i_index_count);
				var i_cost_mnth_end = nlapiGetLineItemValue('people_plan_sublist','resource_cost_month_end',i_index_count);
				var i_revenue_share_mnth_end = nlapiGetLineItemValue('people_plan_sublist','revenue_share_mnth_end',i_index_count);
				
				//validtaion for location, role not blank
				if(!i_practice_mnth_end)
				{
					alert('Practice cannot be blank at line no:- '+i_index_count);
					return false;
				}
				
				if(!i_sub_practice_mnth_end)
				{
					alert('Sub-Practice cannot be blank at line no:- '+i_index_count);
					return false;
				}
				
				if(!i_role_mnth_end)
				{
					alert('Role cannot be blank at line no:- '+i_index_count);
					return false;
				}
				
				if(!i_level_mnth_end)
				{
					alert('Level cannot be blank at line no:- '+i_index_count);
					return false;
				}
				
				if(!i_location_mnth_end)
				{
					alert('Location cannot be blank at line no:- '+i_index_count);
					return false;
				}
				
				
				var a_per_row_array = new Array();
				
				for (var j = 0; j < monthBreakUp.length; j++)
				{
					var months = monthBreakUp[j];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					var fld_s_month_name = s_month_name +'_'+ i_year;
					var d_today_date = new Date();
					var mnth_share=0;
					if(i_month == d_today_date.getMonth())
					{
						i_current_month = i_month;
					}
					
					//if(month_strt > d_today_date)
					{
						var i_s_month_name_value = nlapiGetLineItemValue('people_plan_sublist',''+fld_s_month_name.toLowerCase(),i_index_count);
						
						if(!i_s_month_name_value)
							i_s_month_name_value = 0;
							
						//	var i_revenue_share_mnth_end=get_revenue_share(i_sub_practice_mnth_end_text,fld_s_month_name);
							var i_revenue_share_mnth_end=nlapiGetLineItemValue('revenue_sublist_month_end',''+fld_s_month_name.toLowerCase(),1);
						for(var pract_line=1;pract_line<=line_cnt;pract_line++)
						{
							var line_sub_pract=nlapiGetLineItemValue('revenue_sublist_month_end','monthly_split',pract_line);
							//alert(line_sub_pract);
							i_sub_practice_mnth_end_text = i_sub_practice_mnth_end_text.replace(/[^a-zA-Z&: ]/g, "");
                          line_sub_pract = line_sub_pract.replace(/[^a-zA-Z&: ]/g, "");
							if(line_sub_pract==i_sub_practice_mnth_end_text)
							{
								var i_revenue_split_mnth_end=nlapiGetLineItemValue('revenue_sublist_month_end',''+fld_s_month_name.toLowerCase(),pract_line);
								
							}
							
						}
						if(i_revenue_split_mnth_end)
						{
							mnth_share=1;
						}
                      if(!i_revenue_split_mnth_end)
                        {
                          i_revenue_split_mnth_end=0;
                        }
						/*var o_actual_effrt_mnth_end = nlapiCreateRecord('customrecord_fp_revrec_month_end_effort');
						o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_practice', i_practice_mnth_end);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_sub_practice', i_sub_practice_mnth_end);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_role', i_role_mnth_end);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_level', i_level_mnth_end);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_location_mnth_end', i_location_mnth_end);				
						o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_month_name', s_month_name);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_year_name', i_year);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_month_start', months.Start);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_end_date', months.End);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_percent_allocated', i_s_month_name_value);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_resource_cost', i_cost_mnth_end);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_project_plan', i_projectId);
						o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_parent', i_mnth_end_parent_rcrd_id);
						var i_month_end_activity = nlapiSubmitRecord(o_actual_effrt_mnth_end);*/
						
						
						a_per_row_array.push({
										'prac': i_practice_mnth_end,
										'prac_text': i_practice_mnth_end_text,
										'subprac': i_sub_practice_mnth_end,
										'subprac_text': i_sub_practice_mnth_end_text,
										'role': i_role_mnth_end,
										'role_text': i_role_mnth_end_text,
										'level': i_level_mnth_end,
										'level_text': i_level_mnth_end_text,
										'loc': i_location_mnth_end,
										'loc_text': i_location_mnth_end_text,
										'share': i_revenue_share_mnth_end,
										'pract_share':i_revenue_split_mnth_end,
										'mnth_flag':mnth_share,
										'mnth': s_month_name,
										'year': i_year,
										'allo': i_s_month_name_value,
										'cost': i_cost_mnth_end,
										'project': i_projectId
										});
					}
				}
				//alert(JSON.stringify(a_per_row_array));
				a_total_row_clubbed_array.push(a_per_row_array);
				
				if(i_index_count == 4)
				{
					//alert('frst:- '+i_index_count);
					var o_mnth_end_effrt_json = nlapiCreateRecord('customrecord_fp_others_mnth_end_json');
					o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fp_parent',i_revenue_share_id);
					o_mnth_end_effrt_json.setFieldValue('custrecord_other_current_mnth_no',parseInt(i_current_month));
					o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fld_json1',JSON.stringify(a_total_row_clubbed_array));
					
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if((i_index_count > 4 && i_index_count == 8) || (i_index_count == i_count_month_end_activity && i_index_count > 4 && i_index_count <= 8))
				{
					//alert('second line:- '+i_index_count);
					o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fld_json2',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if((i_index_count > 8 && i_index_count == 12) || (i_index_count == i_count_month_end_activity && i_index_count > 8 && i_index_count <= 12))
				{
					//alert('third line:- '+i_index_count);
					o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fld_json3',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if((i_index_count > 12 && i_index_count == 16) || (i_index_count == i_count_month_end_activity && i_index_count > 12 && i_index_count <=16))
				{
					o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fld_json4',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				else if(i_index_count > 16 && i_index_count == i_count_month_end_activity )
				{
					o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fld_json5',JSON.stringify(a_total_row_clubbed_array));
				
					var a_per_row_array = new Array();
					var a_total_row_clubbed_array = [];
				}
				
			}
			
			if(i_count_month_end_activity < 4)
			{
				//alert('inside line < 4');
				var o_mnth_end_effrt_json = nlapiCreateRecord('customrecord_fp_others_mnth_end_json');
				o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fld_json1',JSON.stringify(a_total_row_clubbed_array));
				o_mnth_end_effrt_json.setFieldValue('custrecord_fp_others_mnth_end_fp_parent',i_revenue_share_id);
				o_mnth_end_effrt_json.setFieldValue('custrecord_other_current_mnth_no',parseInt(i_current_month));
			}
			
			try{
				var i_mnth_end_effrt_json = nlapiSubmitRecord(o_mnth_end_effrt_json,true,true);
			}
			catch(err_rcrd)
			{
				alert('record creation error:- '+err_rcrd);
			}
			
			alert('Execution complete:- '+i_mnth_end_effrt_json);
			return true;
		}
		else 
		{
			
			var f_total_share_revenue = 0; //variable to capture total revenue share entered
			var s_project = nlapiGetFieldValue('proj_selected');
			var i_project_cust = nlapiGetFieldValue('customer_selected');
			var i_sowValue = parseFloat(nlapiGetFieldValue('proj_sow_value'));
			var d_project_strt_date = nlapiGetFieldValue('proj_strt_date');
			var d_project_end_date = nlapiGetFieldValue('proj_end_date');
			var i_project_executing_practice = nlapiGetFieldValue('proj_practice');
			var i_excuting_practice_present_flag = 0;
			
			var i_total_line_count_revenue_share = nlapiGetLineItemCount('practice_sublist'); //returns number of lines present as sublist
			var i_total_monthly_effort=nlapiGetLineItemCount('revenue_month_sublist');
			
			/*if (i_total_line_count_revenue_share < 1)
			{
				alert('Please enter Revenue Share and Budget Effort Plan for the project');
				return false;
			}*/
			
			for (var i_line_index = 1; i_line_index <= i_total_monthly_effort; i_line_index++) // iterate through sublist
			{
				var monthBreakUp = getMonthsBreakup(
					        nlapiStringToDate(d_project_strt_date),
					        nlapiStringToDate(d_project_end_date));
				for (var j = 0; j < monthBreakUp.length; j++)
				{
							
					var months = monthBreakUp[j];
					var s_month_name = getMonthName(months.Start); // get month name
					var month_strt = nlapiStringToDate(months.Start);
					var month_end = nlapiStringToDate(months.End);
					var i_month = month_strt.getMonth();
					var i_year = month_strt.getFullYear();
					s_month_name = s_month_name +'_'+ i_year;
					//var amount=nlapiGetLineItemValue(
					var f_share_amount=nlapiGetLineItemValue('revenue_month_sublist',''+s_month_name.toLowerCase(),i_line_index);
					if(!f_share_amount)
					{
						alert('Monthly Revenue cannot be blank.For No revenue, please enter Zero value');
						return false;
					}
					//capture amount eneterd at each line level sublist
				
					f_total_share_revenue = parseFloat(f_total_share_revenue) + parseFloat(f_share_amount); // calculate total revenue share entered
				}
			}
			f_total_share_revenue = parseFloat(f_total_share_revenue).toFixed(2);
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_sut_exempt', 'customdeploy1') + '&i_customer_interal_id='+s_pro_cust, null, null);
			var res_sow = response.getBody();
			//var sow_exempt=nlapiLookupField('customer',s_pro_cust,'custentity_sow_validation_exception');
			if(res_sow!='T')
			{
			if ((f_total_share_revenue) > parseFloat(nlapiGetFieldValue('proj_sow_value'))) // if revenue share is > project value don't allow to proceed
			{
				var rem=f_total_share_revenue-i_sowValue;
				alert('Total Revenue Share exceeds Project SOW Value by ' +rem+'. It should be equal to Project SOW Value');
				return false;
			}
			else 
				if ((f_total_share_revenue) < parseFloat(nlapiGetFieldValue('proj_sow_value'))) // if revenue share is < project value don't allow to proceed
				{
					var rem=i_sowValue-f_total_share_revenue;
					alert('Total Revenue Share is lessthan Project SOW Value by '+rem+'. It should be equal to Project SOW Value');
					return false;
				}
				
			}
			var i_total_line_count_effor_plan = nlapiGetLineItemCount('effort_sublist'); //returns number of lines present as sublist
			if (i_total_line_count_effor_plan < 1)
			{
				alert('Please update "Budget Effort Plan" for the project');
				return false;
			}
			
			// chech whether all resoruce share practice and subpractice are involved in effort plan	
			for(var i_practice_sublist_index = 1; i_practice_sublist_index <= i_total_line_count_revenue_share; i_practice_sublist_index++)
			{
				var i_parent_practice_found = 0;
				var i_sub_practice_found = 0;
			
				var i_parent_practice_involved = nlapiGetLineItemValue('practice_sublist','parent_practice',i_practice_sublist_index);
				var s_parent_practice_involved = nlapiGetLineItemText('practice_sublist','parent_practice',i_practice_sublist_index);
				var i_sub_practice_involved = nlapiGetLineItemValue('practice_sublist','sub_practice',i_practice_sublist_index);
				var s_sub_practice_involved = nlapiGetLineItemText('practice_sublist','sub_practice',i_practice_sublist_index);
			
				
				
				for(var i_effort_sublist_index = 1; i_effort_sublist_index <= i_total_line_count_effor_plan; i_effort_sublist_index++)
				{
					var i_parent_practice_effrt_plan = nlapiGetLineItemValue('effort_sublist','parent_practice_effort_plan',i_effort_sublist_index);
					var i_sub_practice_effrt_plan = nlapiGetLineItemValue('effort_sublist','sub_practice_effort_plan',i_effort_sublist_index);
				
					if (i_parent_practice_involved == i_parent_practice_effrt_plan)
					{
						i_parent_practice_found = 1;
					}
					
					if(i_sub_practice_involved == i_sub_practice_effrt_plan)
					{
						i_sub_practice_found = 1;
					}
				}
				
				if(i_parent_practice_found == 0)
				{
					alert('Project effort budget needs to be entered for ('+s_parent_practice_involved+') practice');
					return false;
				}
				if(i_sub_practice_found == 0)
				{
					alert('Project effort budget needs to be entered for ('+s_sub_practice_involved+') sub practice');
					return false;
				}
			}
			
			/*if(i_excuting_practice_present_flag == 0)
			{
				alert('Revenue share needs to be entered for Executing practice');
				return false;
			}*/
			
			//Creating Custom Record for Revenue Cap
			var i_current_version_number = 0;
			
			if (s_request_type == 'Update') {
				//Update the custom record values
				var a_proj_participating_practice = new Array();
				var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', parseInt(s_project)]];
				
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_auto_numb');
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
				if (a_get_logged_in_user_exsiting_revenue_cap) {
					i_current_version_number = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_fp_rev_rec_others_auto_numb');
					var i_existing_rcrd_internal_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
				}
				
				/*var is_change_found = nlapiGetFieldValue('change_triggered');
				if(is_change_found != 'T')
				{
					alert('inside redirect for approval');
					var a_url_parameters = new Array();
					a_url_parameters['proj_id'] = s_project;
					a_url_parameters['mode'] = 'Submit';
					a_url_parameters['existing_rcrd_id'] = i_existing_rcrd_internal_id;
					a_url_parameters['revenue_share_status'] = 1;
					
					nlapiSetRedirectURL('SUITELET', '1044', 'customdeploy_sut_fp_rev_rec_proj_details', null, a_url_parameters);
					
					//window.location.url = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&mode=Submit&existing_rcrd_id='+i_existing_rcrd_internal_id+'&proj_id='+s_project+'&revenue_share_status=1&whence=';
					
					//window.open('https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&mode=Submit&existing_rcrd_id='+i_existing_rcrd_internal_id+'&proj_id='+s_project+'&whence=');
					//window.close();
					return false;
				}*/
			}
			
			var is_change_found = nlapiGetFieldValue('change_triggered');
			if(s_request_type != 'Submit' && s_request_type != 'PendingApproval' && is_change_found == 'T')
			{
				
				i_current_version_number = parseInt(i_current_version_number) + 1;
			
				var createRevenueCAP = nlapiCreateRecord('customrecord_fp_rev_rec_others_parent');
				createRevenueCAP.setFieldValue('custrecord_fp_rev_rec_others_projec', s_project);
				createRevenueCAP.setFieldValue('custrecord_fp_rev_rec_others_customer', i_project_cust);
				createRevenueCAP.setFieldValue('custrecord_fp_rev_rec_others_total_rev', i_sowValue);
				createRevenueCAP.setFieldValue('custrecord_fp_rev_rec_pro_st_date', d_project_strt_date);
				createRevenueCAP.setFieldValue('custrecord_fp_rev_rec_proj_end_date', d_project_end_date);
				createRevenueCAP.setFieldValue('custrecord_fp_rev_rec_others_auto_numb', i_current_version_number);
				var rev_cap_id = nlapiSubmitRecord(createRevenueCAP);
				
				//Create Custom Entry for Revenue Share per practice
				/*for (var i_line_index_ = 1; i_line_index_ <= i_total_line_count_revenue_share; i_line_index_++) // iterate through sublist
				{
					var parent_practice = nlapiGetLineItemValue('practice_sublist', 'parent_practice', i_line_index_);
					var sub_practice = nlapiGetLineItemValue('practice_sublist', 'sub_practice', i_line_index_);
					var share_amount = nlapiGetLineItemValue('practice_sublist', 'share_amount', i_line_index_);
					
					var createRevenue_Share_Per_Project = nlapiCreateRecord('customrecord_fp_rev_rec_others_parent_per_practice');
					createRevenue_Share_Per_Project.setFieldValue('custrecord_revenue_share_per_practice_re', parseFloat(share_amount));
					createRevenue_Share_Per_Project.setFieldValue('custrecord_revenue_share_per_practice_pr', parent_practice);
					createRevenue_Share_Per_Project.setFieldValue('custrecord_revenue_share_per_practice_su', sub_practice);
					createRevenue_Share_Per_Project.setFieldValue('custrecord_revenue_share_per_practice_ca', rev_cap_id);
					var id_rev_share = nlapiSubmitRecord(createRevenue_Share_Per_Project);
					
				}*/
				for(var i_mnthly_rev=1;i_mnthly_rev<=i_total_monthly_effort;i_mnthly_rev++)
				{
					var data_json=[];
					var data_array=[];
					var o_month_revenue = nlapiCreateRecord('customrecord_fp_other_monthly_rev');
					var monthBreakUp = getMonthsBreakup(
					        nlapiStringToDate(d_project_strt_date),
					        nlapiStringToDate(d_project_end_date));
					for (var j = 0; j < monthBreakUp.length; j++)
					{
							
						var months = monthBreakUp[j];
						var s_month_name = getMonthName(months.Start); // get month name
						var month_strt = nlapiStringToDate(months.Start);
						var month_end = nlapiStringToDate(months.End);
						var i_month = month_strt.getMonth();
						var i_year = month_strt.getFullYear();
						s_month_name = s_month_name +'_'+ i_year;
						//var amount=nlapiGetLineItemValue(
						var amount=nlapiGetLineItemValue('revenue_month_sublist',''+s_month_name.toLowerCase(),i_mnthly_rev);
						data_json.push({
							month:s_month_name,
							amount:amount,
							project:s_project
						
						});
						
						
						
						
					}
					data_array.push(data_json);
					var data=JSON.stringify(data_array);
					//alert(data);
					o_month_revenue.setFieldValue('custrecord_other_month', data);
					o_month_revenue.setFieldValue('custrecord_fp_rev_rec_other', rev_cap_id);
					var id_mnth_rev = nlapiSubmitRecord(o_month_revenue,true,true);
				}
				try
				{
					//Create Custom Entry for effort plan
					for (var i_index_effort_plan = 1; i_index_effort_plan <= i_total_line_count_effor_plan; i_index_effort_plan++) // iterate through sublist
					{
						var i_parent_practice = nlapiGetLineItemValue('effort_sublist', 'parent_practice_effort_plan', i_index_effort_plan);
						var i_sub_practice = nlapiGetLineItemValue('effort_sublist', 'sub_practice_effort_plan', i_index_effort_plan);
						var i_role = nlapiGetLineItemValue('effort_sublist', 'role_effort_plan', i_index_effort_plan);
						var s_level = nlapiGetLineItemValue('effort_sublist', 'level_effort_plan', i_index_effort_plan);
						var i_no_of_resources = nlapiGetLineItemValue('effort_sublist', 'no_resources_effort_plan', i_index_effort_plan);
						var d_allocation_strt_date = nlapiGetLineItemValue('effort_sublist', 'allocation_strt_date_effort_plan', i_index_effort_plan);
						var d_allocation_end_date = nlapiGetLineItemValue('effort_sublist', 'allocation_end_date_effort_plan', i_index_effort_plan);
						var s_prcnt_allocated = nlapiGetLineItemValue('effort_sublist', 'prcnt_allocation_effort_plan', i_index_effort_plan);
						var i_location = nlapiGetLineItemValue('effort_sublist', 'location_effort_plan', i_index_effort_plan);
						var f_contractor_cost = nlapiGetLineItemValue('effort_sublist', 'cost_effort_plan', i_index_effort_plan);
						
						var o_effort_plan_rcrd_obj = nlapiCreateRecord('customrecord_fp_rev_rec_others_eff_plan');
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_practice', i_parent_practice);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_sub_practic', i_sub_practice);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_role', i_role);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_level', s_level);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_no_of_resou', i_no_of_resources);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_alloc_st_da', d_allocation_strt_date);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_alloc_end_d', d_allocation_end_date);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_percent_all', s_prcnt_allocated);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_location', i_location);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_cost', f_contractor_cost);
						o_effort_plan_rcrd_obj.setFieldValue('custrecord_fp_rev_rec_others_parent_link', rev_cap_id);
						var id_rev_share = nlapiSubmitRecord(o_effort_plan_rcrd_obj,true,true);
						
					}
				}
				catch(err)
				{
					alert('ERROR IN CREATING EFFORT PLAN:- '+err);
				}
			}
		}
		
		return true;
	}
	catch(err)
	{
		alert(err);
	}
}

// function to check amount is not entered against untagged practice line 
function validateLine_FP_RevRec(type)
{
	var d_project_strt_date = nlapiGetFieldValue('proj_strt_date');
	var d_project_end_date = nlapiGetFieldValue('proj_end_date');
	if (type == 'practice_sublist')
	{
		var f_revenue_share = nlapiGetCurrentLineItemValue('practice_sublist', 'share_amount');
		
		if (f_revenue_share)
		{
			if(f_revenue_share == 0)
			{
				alert('Revenue Share cannot be blank or zero for Practice / Sub-Practice');
				return false;
			}
			
			var i_parent_practice_available = nlapiGetCurrentLineItemValue('practice_sublist', 'parent_practice');
			if (!i_parent_practice_available)
			{
				alert('Enter Practice before entering the Revenue share');
				return false;
			}
			
			var i_sub_practice_available = nlapiGetCurrentLineItemValue('practice_sublist', 'sub_practice');
			if (!i_sub_practice_available)
			{
				alert('Enter Sub Practice before entering the Revenue share');
				return false;
			}
			
			var i_sub_prac_parent_id = find_is_sub_practice_appropriate(i_sub_practice_available,1);
			if(i_sub_prac_parent_id != i_parent_practice_available)
			{
				alert('Map Sub practice as per practice selected');
				return false;
			}
			
			var f_total_revenue_share_budgeted = 0;
			var i_revenue_share_lines = nlapiGetLineItemCount('practice_sublist');
			var i_current_line = nlapiGetCurrentLineItemIndex('practice_sublist');
			//alert('line count:- '+i_revenue_share_lines+':::f_revenue_share:- '+f_revenue_share);
			for(var i_revnue_share_line_index=1; i_revnue_share_line_index<=i_revenue_share_lines; i_revnue_share_line_index++)
			{
				if(i_current_line != i_revnue_share_line_index)
				{
					var f_revenue_share_per_line = nlapiGetLineItemValue('practice_sublist', 'share_amount', i_revnue_share_line_index);
					f_total_revenue_share_budgeted = parseFloat(f_total_revenue_share_budgeted) + parseFloat(f_revenue_share_per_line); 
					//alert('f_revenue_share_per_line:- '+f_revenue_share_per_line+'::::f_total_revenue_share_budgeted:- '+f_total_revenue_share_budgeted);
				}
			}
			//alert(f_revenue_share);
			f_total_revenue_share_budgeted = parseFloat(f_total_revenue_share_budgeted) + parseFloat(f_revenue_share);
			nlapiSetFieldValue('proj_budgeted_value',parseFloat(f_total_revenue_share_budgeted).toFixed(2));
			
			return true;
		}
		else
		{
			alert('Revenue Share cannot be blank or zero for Practice / Sub-Practice');
			return false;
		}
	}
	else if(type == 'effort_sublist')
	{
		var a_parent_practice_involved = new Array();
		var a_sub_practice_involved = new Array();
		
		var i_total_line_count_revenue_share = nlapiGetLineItemCount('practice_sublist');
		for(var i_practice_sublist_index = 1; i_practice_sublist_index <= i_total_line_count_revenue_share; i_practice_sublist_index++)
		{
			var i_parent_practice_involved = nlapiGetLineItemValue('practice_sublist','parent_practice',i_practice_sublist_index);
			var i_sub_practice_involved = nlapiGetLineItemValue('practice_sublist','sub_practice',i_practice_sublist_index);
		
			a_parent_practice_involved.push(i_parent_practice_involved);
			a_sub_practice_involved.push(i_sub_practice_involved);
		}
			
		var i_parent_practice_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'parent_practice_effort_plan');
		if(!i_parent_practice_effort_plan)
		{
			alert('Parent Practice cannot be blank');
			return false;
		}
		else
		{
			/*if(a_parent_practice_involved.indexOf(i_parent_practice_effort_plan) < 0)
			{
				alert('Budget Effort plan should be entered only for Practices with "Revenue Share"');
				return false;
			}*/
		}
		
		var i_sub_practice_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'sub_practice_effort_plan');
		if(!i_sub_practice_effort_plan)
		{
			alert('Sub Practice cannot be blank');
			return false;
		}
		else
		{
			/*if(a_sub_practice_involved.indexOf(i_sub_practice_effort_plan) < 0)
			{
				alert('Budget Effort plan should be entered only for Sub-Practices with "Revenue Share"');
				return false;
			}*/
		}
		
		var i_sub_prac_parent_id = find_is_sub_practice_appropriate(i_sub_practice_effort_plan,1);
		if(i_sub_prac_parent_id != i_parent_practice_effort_plan)
		{
			alert('Map Sub practice as per practice selected');
			return false;
		}
		
		var i_role_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'role_effort_plan');
		if(!i_role_effort_plan)
		{
			alert('Role cannot be blank');
			return false;
		}
		else
		{
			if(i_role_effort_plan == 109)
			{
				var f_contractor_cost = nlapiGetCurrentLineItemValue('effort_sublist', 'cost_effort_plan');
				if(!f_contractor_cost)
				{
					alert('Contractor cost cannot be blank');
					return false;
				}
			}
			else
			{
				
				var a_fld_location_filters = ['custrecord_revenue_sourcetype', 'anyof', 3];
				var o_cost_location = Search_revenue_Location_subsdidary(a_fld_location_filters);
				o_cost_location = o_cost_location["Cost Reference"];
				var i_practice = nlapiGetCurrentLineItemValue('effort_sublist', 'parent_practice_effort_plan');
				var i_sub_practice = nlapiGetCurrentLineItemValue('effort_sublist', 'sub_practice_effort_plan');
				//var i_role_selected = nlapiGetCurrentLineItemValue('effort_sublist', 'role_effort_plan');
				var i_level_selected = nlapiGetCurrentLineItemValue('effort_sublist', 'level_effort_plan');
				var i_location_selec = nlapiGetCurrentLineItemValue('effort_sublist', 'location_effort_plan');
				var a_cost_setup_filter = [['custrecord_fp_cost_setup_practice', 'anyof', i_practice], 'and',
									['custrecord_fp_cost_setup_sub_practice', 'anyof', i_sub_practice], 'and',
									['custrecord_fp_cost_setup_level', 'anyof', i_level_selected]];
				
				//['custrecord_fp_cost_setup_role', 'anyof', i_role_selected], 'and',
				
				var a_columns_cost_setup = new Array();
				for (var key in o_cost_location) {
					a_columns_cost_setup.push(new nlobjSearchColumn(o_cost_location[key]));
					}
				var a_get_cost_setup_role_level = nlapiSearchRecord('customrecord_fp_cost_setup', null, a_cost_setup_filter, a_columns_cost_setup);
				if (!a_get_cost_setup_role_level)
				{
					nlapiSetCurrentLineItemValue('effort_sublist', 'cost_effort_plan', '');
					alert('Cost not available for selected level');
					return false;
				}
				else{
					var i_cost_location_cost = o_cost_location[i_location_selec] ? o_cost_location[i_location_selec] : 'custrecord_fp_cost_setup_us_cost';
					nlapiSetCurrentLineItemValue('effort_sublist', 'cost_effort_plan', a_get_cost_setup_role_level[0].getValue(i_cost_location_cost));
					
				}
			}
		}
		
		var s_level_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'level_effort_plan');
		if(!s_level_effort_plan)
		{
			alert('Level cannot be blank');
			return false;
		}
		
		var i_no_of_resources_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'no_resources_effort_plan');
		if(!i_no_of_resources_effort_plan)
		{
			alert('No. of resources field cannnot be blank');
			return false;
		}
		
		var d_project_strt_date = nlapiGetFieldValue('proj_strt_date');
		d_project_strt_date = nlapiStringToDate(d_project_strt_date);
		var d_project_end_date = nlapiGetFieldValue('proj_end_date');
		d_project_end_date = nlapiStringToDate(d_project_end_date);
		
		var d_allocation_strt_date_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'allocation_strt_date_effort_plan');
		if(!d_allocation_strt_date_effort_plan)
		{
			alert('Allocation start date cannot be blank');
			return false;
		}
		else
		{
			d_allocation_strt_date_effort_plan = nlapiStringToDate(d_allocation_strt_date_effort_plan);
			if(d_allocation_strt_date_effort_plan < d_project_strt_date || d_allocation_strt_date_effort_plan > d_project_end_date)
			{
				alert('Allocation start date should be between Project start date and end date');
				return false;
			}
		}
		
		var d_allocation_end_date_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'allocation_end_date_effort_plan');
		if(!d_allocation_end_date_effort_plan)
		{
			alert('Allocation end date cannot be blank');
			return false;
		}
		else
		{
			d_allocation_end_date_effort_plan = nlapiStringToDate(d_allocation_end_date_effort_plan);
			if(d_allocation_end_date_effort_plan < d_project_strt_date || d_allocation_end_date_effort_plan > d_project_end_date)
			{
				alert('Allocation end date should be between Project start date and end date');
				return false;
			}
		}
		
		if(d_allocation_strt_date_effort_plan > d_allocation_end_date_effort_plan)
		{
			alert('Allocation end date cannot be less than Allocation start date');
			return false;
		}
		
		var i_percent_allocated_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'prcnt_allocation_effort_plan');
		if(!i_percent_allocated_effort_plan)
		{
			alert('% allocation field cannnot be blank');
			return false;
		}
		else
		{
			if(i_percent_allocated_effort_plan > 100)
			{
				alert('% allocation cannot be greater than 100%');
				return false;
			}
		}
		
		var i_location_effort_plan = nlapiGetCurrentLineItemValue('effort_sublist', 'location_effort_plan');
		if(!i_location_effort_plan)
		{
			alert('Location cannot be blank');
			return false;
		}
		var effort_cnt=nlapiGetLineItemCount('effort_sublist');
		if(effort_cnt>=1)
		{
			
			var st_allotn_date=nlapiGetCurrentLineItemValue('effort_sublist','allocation_strt_date_effort_plan');
			var end_allotn_date=nlapiGetCurrentLineItemValue('effort_sublist','allocation_end_date_effort_plan');
			for(var ind=1;ind<=effort_cnt;ind++)
			{
				var practice_list=nlapiGetLineItemValue('effort_sublist', 'sub_practice_effort_plan',ind);
				var level_list=nlapiGetLineItemValue('effort_sublist','level_effort_plan',ind);
				var location_list=nlapiGetLineItemValue('effort_sublist','location_effort_plan',ind);
				var allocatn_date_st=nlapiGetLineItemValue('effort_sublist','allocation_strt_date_effort_plan',ind);
				var allocatn_date_end=nlapiGetLineItemValue('effort_sublist','allocation_end_date_effort_plan',ind);
				var percent_alloca_new = nlapiGetLineItemValue('effort_sublist', 'prcnt_allocation_effort_plan',ind);
              //allocatn_date_st=nlapiStringToDate(allocatn_date_st);
				//allocatn_date_end=nlapiStringToDate(allocatn_date_end);
				var line_indx=nlapiGetCurrentLineItemIndex('effort_sublist');
				if((ind!=line_indx)&& (practice_list==i_sub_practice_effort_plan && level_list ==s_level_effort_plan  && location_list==i_location_effort_plan && st_allotn_date== allocatn_date_st && end_allotn_date==allocatn_date_end && percent_alloca_new == i_percent_allocated_effort_plan))
				{
					
					alert('Duplicate Row is allowed for same practice, role and location');
					return false;
				}
			}
		}
	}
	
	else if(type == 'people_plan_sublist')
	{
		var i_mnth_end_practice = nlapiGetCurrentLineItemValue('people_plan_sublist', 'parent_practice_month_end');
		if(!i_mnth_end_practice)
		{
			alert('Practice cannot be blank');
			return false;
		}
		
		var i_mnth_end_sub_practice = nlapiGetCurrentLineItemValue('people_plan_sublist', 'sub_practice_month_end');
		if(!i_mnth_end_sub_practice)
		{
			alert('Sub Practice cannot be blank');
			return false;
		}
		
		var i_mnth_end_role = nlapiGetCurrentLineItemValue('people_plan_sublist', 'role_month_end');
		if(!i_mnth_end_role)
		{
			alert('Role cannot be blank');
			return false;
		}
		else
		{
			if(i_mnth_end_role == 109)
			{
				var f_contractor_cost = nlapiGetCurrentLineItemValue('people_plan_sublist', 'resource_cost_month_end');
				if(!f_contractor_cost)
				{
					alert('Cost cannot be blank');
					return false;
				}
			}
			else
			{
				var i_practice = nlapiGetCurrentLineItemValue('people_plan_sublist', 'parent_practice_month_end');
				var i_sub_practice = nlapiGetCurrentLineItemValue('people_plan_sublist', 'sub_practice_month_end');
				var i_role_selected = nlapiGetCurrentLineItemValue('people_plan_sublist', 'role_month_end');
				var i_level_selected = nlapiGetCurrentLineItemValue('people_plan_sublist', 'level_month_end');
				
				var a_cost_setup_filter = [['custrecord_fp_cost_setup_practice', 'anyof', i_practice], 'and',
									['custrecord_fp_cost_setup_sub_practice', 'anyof', i_sub_practice], 'and',
									['custrecord_fp_cost_setup_level', 'anyof', i_level_selected]];
				
				var a_get_cost_setup_role_level = nlapiSearchRecord('customrecord_fp_cost_setup', null, a_cost_setup_filter, null);
				if (!a_get_cost_setup_role_level)
				{
					nlapiSetCurrentLineItemValue('people_plan_sublist', 'resource_cost_month_end', '');
					alert('Cost not available for selected level');
					return false;
				}
			}
		}
		
		var i_mnth_end_location = nlapiGetCurrentLineItemValue('people_plan_sublist', 'location_month_end');
		if(!i_mnth_end_location)
		{
			alert('Location cannot be blank');
			return false;
		}
			
		// get resource cost based on location
		var i_mnth_end_level = nlapiGetCurrentLineItemValue('people_plan_sublist', 'level_month_end');
		var i_location_selected = nlapiGetCurrentLineItemValue('people_plan_sublist', 'location_month_end');
		var f_resource_cost = nlapiGetCurrentLineItemValue('people_plan_sublist', 'resource_cost_month_end');
		
		if(!f_resource_cost)
		{
			var a_fld_location_filters = ['custrecord_revenue_sourcetype', 'anyof', 3];
			var o_cost_location = Search_revenue_Location_subsdidary(a_fld_location_filters);
			o_cost_location = o_cost_location["Cost Reference"];
			var a_cost_setup_filter = [['custrecord_fp_cost_setup_practice', 'anyof', i_mnth_end_practice], 'and',
										['custrecord_fp_cost_setup_sub_practice', 'anyof', i_mnth_end_sub_practice], 'and',
										['custrecord_fp_cost_setup_level', 'anyof', i_mnth_end_level]];
			var a_columns_cost_setup = new Array();
			for (var key in o_cost_location) {
					a_columns_cost_setup.push(new nlobjSearchColumn(o_cost_location[key]));
				}
				
			
			var a_get_cost_setup_role_level = nlapiSearchRecord('customrecord_fp_cost_setup', null, a_cost_setup_filter, a_columns_cost_setup);
			if (a_get_cost_setup_role_level)
			{
				var i_cost_location_cost = o_cost_location[i_location_selected] ? o_cost_location[i_location_selected] : 'custrecord_fp_cost_setup_us_cost';
				nlapiSetCurrentLineItemValue('people_plan_sublist', 'resource_cost_month_end', a_get_cost_setup_role_level[0].getValue(i_cost_location_cost));
			}
		}
	}
	else if(type== 'revenue_sublist_month_end')
	{	
		var revenue_share=0;
		var practices_cnt=nlapiGetLineItemCount('revenue_sublist_month_end');
		var i_current_line_prac = nlapiGetCurrentLineItemIndex('revenue_sublist_month_end');
		var monthBreakUp = getMonthsBreakup(
					        nlapiStringToDate(d_project_strt_date),
					        nlapiStringToDate(d_project_end_date));
		if(i_current_line_prac==1)
		{
			for (var j_ind = 0; j_ind < monthBreakUp.length; j_ind++)
			{
				var months = monthBreakUp[j_ind];
				var s_month_name = getMonthName(months.Start); // get month name
				var month_strt = nlapiStringToDate(months.Start);
				var month_end = nlapiStringToDate(months.End);
				var i_month = month_strt.getMonth();
				var i_year = month_strt.getFullYear();
				s_month_name = s_month_name +'_'+ i_year;
				var f_share=nlapiGetCurrentLineItemValue('revenue_sublist_month_end',''+s_month_name.toLowerCase());
				if(!f_share)
				{
					alert('Monthly Revenue cannot be blank.For No revenue, please enter Zero value');
					return false;
				}
				else{
				revenue_share = parseFloat(revenue_share) + parseFloat(f_share); 
				nlapiSetFieldValue('amt_captured',parseFloat(revenue_share).toFixed(2));
				}
			}
		}
		//if(i_current_line_prac==practices_cnt)
		{
			var val_cap=0;
			for (var j = 0; j < monthBreakUp.length; j++)
			{	
				var flag=0;
				var total_split=0;
				var months = monthBreakUp[j];
				var s_month_name = getMonthName(months.Start); // get month name
				var month_strt = nlapiStringToDate(months.Start);
				var month_end = nlapiStringToDate(months.End);
				var i_month = month_strt.getMonth();
				var i_year = month_strt.getFullYear();
				var d_today_date = new Date();
				s_month_name = s_month_name +'_'+ i_year;
				if(month_end < d_today_date)
				{
				}
				else
				{
					//var amount=nlapiGetLineItemValue(
					for(var pract_line_cnt=2;pract_line_cnt<=practices_cnt;pract_line_cnt++)
					{
						var f_pract_amount = nlapiGetLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase(),pract_line_cnt);
						var current_indx=nlapiGetCurrentLineItemIndex('revenue_sublist_month_end');
						if(( current_indx== pract_line_cnt))
						{
							var f_pract_amount=nlapiGetCurrentLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase());
						}
						if((pract_line_cnt==practices_cnt) &&( current_indx== practices_cnt))
							var f_pract_amount=nlapiGetCurrentLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase());
						
						if(_logValidation(f_pract_amount))
						{
						total_split=parseFloat(total_split)+parseFloat(f_pract_amount);
						flag=1;
								//alert('Monthly Revenue cannot be blank.For No revenue, please enter Zero value');
								//return false;
					}
                 	 if(flag==parseInt(1) && (!f_pract_amount))
					{
					//	alert('Practice share cannot be blank.For No revenue, please enter Zero value');
					//	return false;
					}
				}
				var monthly_pract_total=nlapiGetLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase(),1);
				if(flag==1)
				//if(parseFloat(total_split)!=parseFloat(monthly_pract_total))
				{
					nlapiSetLineItemValue('revenue_sublist_month_end', ''+s_month_name.toLowerCase(),1,total_split);
					//alert('Practice split should equal to Monthly Split');
					//return false;
				}
			}
		//var revenue_split=
		
			
		}
	}
	return true;
}
  return true;
}
function validateDelete(type)
{
	if(type == 'people_plan_sublist')
	{
		//if (parseFloat(nlapiGetFieldValue('mnth_end_effrt_lines')) >= nlapiGetCurrentLineItemIndex('people_plan_sublist'))
		{
			
			var monthBreakUp = getMonthsBreakup(
						        nlapiStringToDate(nlapiGetFieldValue('proj_strt_date')),
						        nlapiStringToDate(nlapiGetFieldValue('proj_end_date')));
			
			var o_rcrd_obj = nlapiLoadRecord('customrecord_fp_rev_rec_others_parent', nlapiGetFieldValue('revenue_share_id'));
			var d_today = o_rcrd_obj.getFieldValue('custrecord_server_date_other');
			d_today = nlapiStringToDate(d_today);
			
			for (var j = 0; j < monthBreakUp.length; j++)
			{
				var months = monthBreakUp[j];
				var s_month_name = getMonthName(months.Start); // get month name
				var month_strt = nlapiStringToDate(months.Start);
				var month_end = nlapiStringToDate(months.End);
				var i_month = month_strt.getMonth();
				var i_year = month_strt.getFullYear();
				s_month_name = s_month_name + '_' + i_year;
				if (month_strt <= d_today)
				{
					if(parseFloat(nlapiGetCurrentLineItemValue('people_plan_sublist','' + s_month_name.toLowerCase())) > parseFloat(0))
					{
						alert('You cannot delete current or prior month actual effort');
						return false;
					}
				}
			}			
		}
		return true;
	}
	else if(type == 'effort_sublist')
	{
		if (parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= nlapiGetCurrentLineItemIndex('effort_sublist') && nlapiGetFieldValue('b_proj_val_chngd') == 'T')
		{
			alert('You cannot delete previous budgeted Effort plan');
			return false;
		}
		return true;
	}
	else if(type == 'practice_sublist')
	{
		if (parseFloat(nlapiGetFieldValue('revenue_share_lines')) >= nlapiGetCurrentLineItemIndex('practice_sublist') && nlapiGetFieldValue('b_proj_val_chngd') == 'T')
		{
			alert('You cannot delete previous budgeted Revenue share');
			return false;
		}
		return true;
	}
	else if(type=='revenue_sublist_month_end')
	{
		alert('you cannot delete Practices Involved');
	//	return false;
	}
	
	return true;
}

function find_is_sub_practice_appropriate(i_sub_practice_available,mode,i_revenue_share_id)
{
	var i_parent_prac_id = 0;
	
	if(mode == 1)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=1&i_sub_practice_available='+i_sub_practice_available, null, null);
	}
	else if(mode == 2)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=2&i_revenue_share_id='+i_sub_practice_available, null, null);
	
	}	
	else if(mode == 3)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=3&i_project_executing_practice='+i_sub_practice_available, null, null);
	}
	else if(mode == 4)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=4&i_executing_practice_head='+i_sub_practice_available, null, null);
	}
	else if(mode == 5)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=5&i_proj_id='+i_sub_practice_available+'&i_revenue_share_id='+i_revenue_share_id, null, null);
	}
	else if(mode == 6)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=6&i_proj_id='+i_sub_practice_available, null, null);
		
	}
	else if(mode == 7)
	{
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_fp_revrec_oth_search_pract', 'customdeploy_fp_revrec_oth_search_pract') + '&mode=7&i_region_id='+i_sub_practice_available, null, null);
	}
	
	return response.getBody();
}

function custom_Approve(i_revenue_share_id)
{
	i_revenue_share_id = i_revenue_share_id.trim();
	var i_project_id = nlapiGetFieldValue('proj_selected');
	
	// this code is now moved to suitelet because of permission issue using employee center role
	/*var a_projectData = nlapiLookupField('job', i_project_id, [
				        'custentity_projectmanager', 'custentity_deliverymanager',
				        'customer', 'custentity_clientpartner']);
		
	var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
	var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
	var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
	var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
	var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');*/
	
	//var a_recipient_mail = new Array();
	
	var a_fld_array = new Array();
	var a_fld_value_array = new Array();
	
	a_fld_array[0] = 'custrecord_revenue_other_approval_status';
	a_fld_array[1] = 'custrecord_rejection_reason';
	
	a_fld_value_array[0] = 3;
	a_fld_value_array[1] = '';
	
	nlapiSubmitField('customrecord_fp_rev_rec_others_parent',parseInt(i_revenue_share_id),a_fld_array,a_fld_value_array);
	
	var s_recipient_mail_id = find_is_sub_practice_appropriate(i_project_id,5,i_revenue_share_id);
	/*s_recipient_mail_id = s_recipient_mail_id.split(':');
			
	if(s_recipient_mail_id[0])
	{
		a_recipient_mail.push(s_recipient_mail_id[0]);
	}
	if(s_recipient_mail_id[1])
	{
		a_recipient_mail.push(s_recipient_mail_id[1]);
	}
	if(s_recipient_mail_id[2])
	{
		a_recipient_mail.push(s_recipient_mail_id[2]);
	}
	if(s_recipient_mail_id[3])
	{
		a_recipient_mail.push(s_recipient_mail_id[3]);
	}
	
	var strVar = '';
	strVar += '<html>';
	strVar += '<body>';
	
	strVar += '<p>Hello All,</p>';
	strVar += '<p>Revenue Share for project :- '+nlapiGetFieldText('proj_selected')+', is approved by executing practice head.</p>';
	strVar += '<p>Please find below link to confirm Revenue share.</p>';
	
	strVar += '<a href=https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1138&deploy=1>Confirm Revenue Share</a>';
	
	strVar += '<p>Thanks & Regards,</p>';
	strVar += '<p>Team IS</p>';
		
	strVar += '</body>';
	strVar += '</html>';
	
	var a_emp_attachment = new Array();
	a_emp_attachment['entity'] = '41571';
	
	nlapiSendEmail(442, a_recipient_mail, 'Revenue Share for Project :- '+nlapiGetFieldText('proj_selected')+' is Approved', strVar,null,null,a_emp_attachment);
	*/
	
	window.close();
	
}

function custom_Reject(i_revenue_share_id)
{
	i_revenue_share_id = i_revenue_share_id.trim();
	var s_reject_reason = nlapiGetFieldValue('rejection_reason');
	if(!s_reject_reason)
	{
		alert('Please enter reason for rejecting Budget plan');
		return false;
	}
	
	var i_project_id = nlapiGetFieldValue('proj_selected');
	
	var a_recipient_mail = new Array();
	var a_participating_prac_mail_id = new Array();
	var s_recipient_mail_id = find_is_sub_practice_appropriate(i_project_id,6);
	s_recipient_mail_id = s_recipient_mail_id.split(':');
			
	if(s_recipient_mail_id[0])
	{
		a_recipient_mail.push(s_recipient_mail_id[0]);
	}
	if(s_recipient_mail_id[1])
	{
		a_recipient_mail.push(s_recipient_mail_id[1]);
	}
	if(s_recipient_mail_id[2])
	{
		a_recipient_mail.push(s_recipient_mail_id[2]);
	}
	if(s_recipient_mail_id[3])
	{
		a_recipient_mail.push(s_recipient_mail_id[3]);
	}
	
	var i_region = nlapiGetFieldValue('proj_region');
	var s_region_head_mail = find_is_sub_practice_appropriate(i_region,7);
	
	a_participating_prac_mail_id.push(s_region_head_mail);
	a_participating_prac_mail_id.push('billing@brillio.com');
//	a_participating_prac_mail_id.push('vikash.garodia@brillio.com');
//	a_participating_prac_mail_id.push('bhavanishankar.t@brillio.com');
	a_participating_prac_mail_id.push('team.fpa@brillio.com');
	
	var s_project_region = nlapiGetFieldText('proj_region');
	var s_project_cust = nlapiGetFieldText('customer_selected');
	var s_project_name = nlapiGetFieldText('proj_selected');
	var s_project_strt = nlapiGetFieldValue('proj_strt_date');
	var s_project_end = nlapiGetFieldValue('proj_end_date');
	var s_project_prac = nlapiGetFieldText('proj_practice');
	var s_approver = nlapiGetFieldText('budget_plan_approver');
	
	var strVar = '';
	strVar += '<html>';
	strVar += '<body>';
	
	strVar += '<p>Hello PM,</p>';
	strVar += '<p>The Fixed price project setup with the following details has been rejected by '+s_approver+'</p>';
	strVar += '<p>Rejection Reason: '+s_reject_reason+'</p>';
	strVar += '<p>Please resubmit the Revenue share and Budget effort for sub-practice review and then final approval by the Practice head</p>';
	
	strVar += '<p>Region:- '+s_project_region+'<br>';
	strVar += 'Customer:- '+s_project_cust+'<br>';
	strVar += 'Project:- '+s_project_name+'<br>';
	strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
	strVar += 'Executing Practice:- '+s_project_prac+'</p>';
			
	strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1139&deploy=1&compid=3883006&proj_id='+i_project_id+'&mode=Update&existing_rcrd_id='+i_revenue_share_id+'&revenue_share_status=4&proj_val_chngd=F>Link to Revenue share and Budget effort screen in Netsuite</a>';
	
	strVar += '<p>Regards,</p>';
	strVar += '<p>Finance Team</p>';
		
	strVar += '</body>';
	strVar += '</html>';
	
	var a_emp_attachment = new Array();
	a_emp_attachment['entity'] = '41571';
	
	nlapiSendEmail(442, a_recipient_mail, 'FP Project Budget Plan “Rejected’: '+nlapiGetFieldText('proj_selected'), strVar,a_participating_prac_mail_id,null,a_emp_attachment);
	var a_fld_array = new Array();
	var a_fld_value_array = new Array();
	
	a_fld_array[0] = 'custrecord_revenue_other_approval_status';
	a_fld_array[1] = 'custrecord_rejection_reason';
	
	a_fld_value_array[0] = 4;
	a_fld_value_array[1] = s_reject_reason;
	
	nlapiSubmitField('customrecord_fp_rev_rec_others_parent',parseInt(i_revenue_share_id),a_fld_array,a_fld_value_array);
	window.close();
}

function custom_confirm()
{
	window.close();
}

function fieldChange_Fp_Rev_Rec(type,name,linenum)
{
	if(name == 'parent_practice')
	{
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'sub_practice')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('revenue_share_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('practice_sublist')))
		{
			alert('You cannot edit previous budgeted sub practice but can only update the Revenue share');
			var i_sub_prac_old_val = nlapiGetLineItemValue('practice_sublist','sub_practice',nlapiGetCurrentLineItemIndex('practice_sublist'));
			nlapiSetCurrentLineItemValue('practice_sublist','sub_practice',i_sub_prac_old_val,false);
			return false;
		}
		
		var i_sub_practice_selected = nlapiGetCurrentLineItemValue('practice_sublist','sub_practice');
		if(i_sub_practice_selected == 493 || i_sub_practice_selected == 494 || i_sub_practice_selected == 495 || i_sub_practice_selected == 496)
		{
			alert('Resource Level practices are DEXA:DEXA, DEXA:Studio and DEXA:Labs. Kindly select any of these three.');
			nlapiSetCurrentLineItemValue('practice_sublist','sub_practice','',false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
		
		var a_existing_revenue_share_prac = new Array();
		var i_line_count_revenue_share = nlapiGetLineItemCount('practice_sublist');
		for(var i_revenue_index=1; i_revenue_index<i_line_count_revenue_share; i_revenue_index++)
		{
			var i_sub_prac = nlapiGetLineItemValue('practice_sublist','sub_practice',i_revenue_index);
			a_existing_revenue_share_prac.push(i_sub_prac);
		}
		
		if(a_existing_revenue_share_prac.indexOf(i_sub_practice_selected) >=0 )
		{
			alert('You can enter single row against sub practice');
			nlapiSetCurrentLineItemValue('practice_sublist','sub_practice','');
			return false;
		}
		
		var i_sub_prac_parent_id = find_is_sub_practice_appropriate(i_sub_practice_selected,1);
		nlapiSetCurrentLineItemValue('practice_sublist','parent_practice',i_sub_prac_parent_id);
	}
	else if(name == 'share_amount')
	{
		nlapiSetFieldValue('change_triggered','T');
		var f_revenue_share_amount = nlapiGetCurrentLineItemValue('practice_sublist','share_amount');
		
		if(f_revenue_share_amount < 0)
		{
			alert('Revenue Share cannot be Negative');
			return false;
		}
	}
	else if(name == 'parent_practice_effort_plan')
	{
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'sub_practice_effort_plan')
	{
		//alert('budget tot line:- '+nlapiGetFieldValue('budegt_effrt_lines')+'currnt line:- '+nlapiGetCurrentLineItemIndex('effort_sublist'));
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan sub-practice but can update only allocation end date');
			var i_prac_old_value = nlapiGetLineItemValue('effort_sublist','sub_practice_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','sub_practice_effort_plan',i_prac_old_value,false);
			return false;
		}
		
		var i_sub_practice_selected = nlapiGetCurrentLineItemValue('effort_sublist','sub_practice_effort_plan');
		if(i_sub_practice_selected == 493 || i_sub_practice_selected == 494 || i_sub_practice_selected == 495 || i_sub_practice_selected == 496)
		{
			alert('Resource Level practices are DEXA:DEXA, DEXA:Studio and DEXA:Labs. Kindly select any of these three.');
			nlapiSetCurrentLineItemValue('effort_sublist','sub_practice_effort_plan','',false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
		
		var i_sub_prac_parent_id = find_is_sub_practice_appropriate(i_sub_practice_selected,1);
		nlapiSetCurrentLineItemValue('effort_sublist','parent_practice_effort_plan',i_sub_prac_parent_id);	
	}
	else if(name == 'role_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan role but can update only allocation end date');
			var i_role_old_value = nlapiGetLineItemValue('effort_sublist','role_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','role_effort_plan',i_role_old_value,false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
		
		/*this requirement was later mentioned as regardless as user should be allowed to 
		enter multiple rows against same practice, role n level (DUE TO CHANGE IN PROJECT VALUE)*/
		/*var a_existing_revenue_share_prac = new Array();
		var i_line_count_revenue_share = nlapiGetLineItemCount('effort_sublist');
		for(var i_revenue_index=1; i_revenue_index<i_line_count_revenue_share; i_revenue_index++)
		{
			var i_sub_prac = nlapiGetLineItemValue('effort_sublist','sub_practice_effort_plan',i_revenue_index);
			var i_sub_prac_role = nlapiGetLineItemValue('effort_sublist','role_effort_plan',i_revenue_index);
			a_existing_revenue_share_prac.push({
										'sub_prac': i_sub_prac,
										'sub_prac_role': i_sub_prac_role
										});
			
		}
		
		var i_sub_practice_selected = nlapiGetCurrentLineItemValue('effort_sublist','sub_practice_effort_plan');
		var i_role_selected = nlapiGetCurrentLineItemValue('effort_sublist','role_effort_plan');
		for(var i_index=0; i_index<a_existing_revenue_share_prac.length; i_index++)
		{
			if(i_sub_practice_selected == a_existing_revenue_share_prac[i_index].sub_prac)
			{
				if(i_role_selected == a_existing_revenue_share_prac[i_index].sub_prac_role)
				{
					alert('You can enter single row against sub practice and role');
					nlapiSetCurrentLineItemValue('effort_sublist','role_effort_plan','');
					return false;
				}
			}
		}*/
		
		var i_role_selected = nlapiGetCurrentLineItemValue('effort_sublist','role_effort_plan');
		if(i_role_selected)
		{
			/*var i_level_already_selected = nlapiGetCurrentLineItemValue('effort_sublist','level_effort_plan');
			
			if(!i_level_already_selected)
			{
				var a_cost_setup_filter = [['custrecord_fp_cost_setup_role', 'anyof', parseInt(i_role_selected)]];
				
				var a_columns_cost_setup = new Array();
				a_columns_cost_setup[0] = new nlobjSearchColumn('custrecord_fp_cost_setup_level');
				
				var a_get_cost_setup_role_level = nlapiSearchRecord('customrecord_fp_cost_setup', null, a_cost_setup_filter, a_columns_cost_setup);
				if (a_get_cost_setup_role_level)
				{
					var i_level = a_get_cost_setup_role_level[0].getValue('custrecord_fp_cost_setup_level');
					
					nlapiSetCurrentLineItemValue('effort_sublist','level_effort_plan',i_level);
				}
			}*/
			
			if(i_role_selected == 109)
			{
				var i_current_line_index = nlapiGetCurrentLineItemIndex('effort_sublist');
			
				var s_contractor_cost_sut_url = '/app/site/hosting/scriptlet.nl?script=1145&deploy=1&i_line_index='+i_current_line_index+'&sublist=1';
			    var obj_window = window.open(s_contractor_cost_sut_url, "_blank", "toolbar=no,menubar=0,status=0,copyhistory=0,scrollbars=yes,resizable=1,location=0,Width=500,Height=450");
			}
		}
	}
	else if(name == 'level_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan level but can update only allocation end date');
			var i_level_old_value = nlapiGetLineItemValue('effort_sublist','level_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','level_effort_plan',i_level_old_value,false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'no_resources_effort_plan')
	{	
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan number of resources but can update only allocation end date');
			var i_no_resources_old_value = nlapiGetLineItemValue('effort_sublist','no_resources_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','no_resources_effort_plan',i_no_resources_old_value,false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'allocation_strt_date_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan allocation start date but can update only allocation end date');
			var i_allo_strt_old_value = nlapiGetLineItemValue('effort_sublist','allocation_strt_date_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','allocation_strt_date_effort_plan',i_allo_strt_old_value,false);
			return false;
		}
		else if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) < parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			var d_allocation_strt_date = nlapiGetCurrentLineItemValue('effort_sublist', 'allocation_strt_date_effort_plan');
			d_allocation_strt_date = nlapiStringToDate(d_allocation_strt_date);
			var i_selected_date_month = d_allocation_strt_date.getMonth();
			var i_selected_date_year = d_allocation_strt_date.getFullYear();
			
			var o_rcrd_obj = nlapiLoadRecord('customrecord_fp_rev_rec_others_parent', nlapiGetFieldValue('revenue_share_id'));
			var d_today = o_rcrd_obj.getFieldValue('custrecord_server_date_other');
			d_today = nlapiStringToDate(d_today);
			var i_today_date_month = d_today.getMonth();
			var i_today_date_year = d_today.getFullYear();
			
			if(i_selected_date_month < i_today_date_month || i_selected_date_year < i_today_date_year)
			{
				alert('Allocation start date cannot be in the prior month but can start in the current month');
				nlapiSetCurrentLineItemValue('effort_sublist','allocation_strt_date_effort_plan','',false);
				return false;
			}
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'allocation_end_date_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			var d_allocation_end_date = nlapiGetCurrentLineItemValue('effort_sublist', 'allocation_end_date_effort_plan');
			d_allocation_end_date = nlapiStringToDate(d_allocation_end_date);
			
			var o_rcrd_obj = nlapiLoadRecord('customrecord_fp_rev_rec_others_parent', nlapiGetFieldValue('revenue_share_id'));
			var d_today = o_rcrd_obj.getFieldValue('custrecord_server_date_other');
			d_today = nlapiStringToDate(d_today);
			var d_firstDay = new Date(d_today.getFullYear(), d_today.getMonth(), 1);
			if (d_allocation_end_date < d_firstDay)
			{
				alert('Budget effort plan allocation end date cannot be in the prior month');
				var i_allo_end_old_value = nlapiGetLineItemValue('effort_sublist','allocation_end_date_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
				nlapiSetCurrentLineItemValue('effort_sublist','allocation_end_date_effort_plan',i_allo_end_old_value,false);
				return false;
			}
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'prcnt_allocation_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan allocation % but can update only allocation end date');
			var i_prcnt_allo_old_value = nlapiGetLineItemValue('effort_sublist','prcnt_allocation_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','prcnt_allocation_effort_plan',i_prcnt_allo_old_value,false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'location_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan location but can update only allocation end date');
			var i_location_old_value = nlapiGetLineItemValue('effort_sublist','location_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','location_effort_plan',i_location_old_value,false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'cost_effort_plan')
	{
		if (nlapiGetFieldValue('b_proj_val_chngd') == 'T' && parseFloat(nlapiGetFieldValue('budegt_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('effort_sublist')))
		{
			alert('You cannot edit previous budget plan contractor cost butcan update only allocation end date');
			var i_cost_old_value = nlapiGetLineItemValue('effort_sublist','cost_effort_plan',nlapiGetCurrentLineItemIndex('effort_sublist'));
			nlapiSetCurrentLineItemValue('effort_sublist','cost_effort_plan',i_cost_old_value,false);
			return false;
		}
		
		nlapiSetFieldValue('change_triggered','T');
	}
	else if(name == 'sub_practice_month_end')
	{
		if (parseFloat(nlapiGetFieldValue('mnth_end_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('people_plan_sublist')))
		{
			alert('You cannot update this field in the existing row. You can add a new row to capture any change in Level, Location or Sub-practice effort');
			var i_sub_prac_mnth_end_old_value = nlapiGetLineItemValue('people_plan_sublist','sub_practice_month_end',nlapiGetCurrentLineItemIndex('people_plan_sublist'));
			nlapiSetCurrentLineItemValue('people_plan_sublist','sub_practice_month_end',i_sub_prac_mnth_end_old_value,false);
			return false;
		}
		
		var i_sub_practice_selected = nlapiGetCurrentLineItemValue('people_plan_sublist','sub_practice_month_end');
		if(i_sub_practice_selected == 493 || i_sub_practice_selected == 494 || i_sub_practice_selected == 495 || i_sub_practice_selected == 496)
		{
			alert('Resource Level practices are DEXA:DEXA, DEXA:Studio and DEXA:Labs. Kindly select any of these three.');
			nlapiSetCurrentLineItemValue('people_plan_sublist','sub_practice_month_end','',false);
			return false;
		}
		
		var i_sub_prac_parent_id = find_is_sub_practice_appropriate(i_sub_practice_selected,1);
		nlapiSetCurrentLineItemValue('people_plan_sublist','parent_practice_month_end',i_sub_prac_parent_id);	
	}
	else if(name == 'role_month_end')
	{
		/*var i_role_mnth_end_old_value = nlapiGetLineItemValue('people_plan_sublist','role_month_end',nlapiGetCurrentLineItemIndex('people_plan_sublist'));
		if(i_role_mnth_end_old_value)
		{
			if (parseFloat(nlapiGetFieldValue('mnth_end_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('people_plan_sublist')))
			{
				alert('You cannot update this field in the existing row. You can add a new row to capture any change in Level, Location or Sub-practice, Role effort');
				nlapiSetCurrentLineItemValue('people_plan_sublist','role_month_end',i_role_mnth_end_old_value,false);
				return false;
			}
		}*/
			
		var i_role_selected = nlapiGetCurrentLineItemValue('people_plan_sublist','role_month_end');
		var i_level_selected_mnth_end = nlapiGetCurrentLineItemValue('people_plan_sublist','level_month_end');
		//alert('i_level_selected_mnth_end '+i_level_selected_mnth_end +'  i_role_selected '+i_role_selected);
		if(i_role_selected)
		{
			
			if(i_role_selected == 109)
			{
				var i_current_line_index = nlapiGetCurrentLineItemIndex('people_plan_sublist');
			
				var s_contractor_cost_sut_url = '/app/site/hosting/scriptlet.nl?script=1145&deploy=1&i_line_index='+i_current_line_index+'&sublist=2';
			    var obj_window = window.open(s_contractor_cost_sut_url, "_blank", "toolbar=no,menubar=0,status=0,copyhistory=0,scrollbars=yes,resizable=1,location=0,Width=500,Height=450");
			}
			if(i_level_selected_mnth_end == 13 && i_role_selected !=109)
			{
				alert('You cannot select other than Contractor role for Level CW');
                nlapiSetCurrentLineItemValue('people_plan_sublist','role_month_end','');
				return false;
			}
		}
	}
	else if(name == 'level_month_end')
	{
		if (parseFloat(nlapiGetFieldValue('mnth_end_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('people_plan_sublist')))
		{
			alert('You cannot update this field in the existing row. You can add a new row to capture any change in Level, Location or Sub-practice, Role effort');
			var i_level_mnth_end_old_value = nlapiGetLineItemValue('people_plan_sublist','level_month_end',nlapiGetCurrentLineItemIndex('people_plan_sublist'));
			nlapiSetCurrentLineItemValue('people_plan_sublist','level_month_end',i_level_mnth_end_old_value,false);
			return false;
		}
	}
	else if(name == 'location_month_end')
	{
		if (parseFloat(nlapiGetFieldValue('mnth_end_effrt_lines')) >= parseFloat(nlapiGetCurrentLineItemIndex('people_plan_sublist')))
		{
			alert('You cannot update this field in the existing row. You can add a new row to capture any change in Level, Location or Sub-practice, Role effort');
			var i_location_mnth_end_old_value = nlapiGetLineItemValue('people_plan_sublist','location_month_end',nlapiGetCurrentLineItemIndex('people_plan_sublist'));
			nlapiSetCurrentLineItemValue('people_plan_sublist','location_month_end',i_location_mnth_end_old_value,false);
			return false;
		}
		
		var a_existing_mnth_end_effrt_prac = new Array();
		var i_line_count_mnth_end_effrt = nlapiGetLineItemCount('people_plan_sublist');
		for(var i_effrt_index=1; i_effrt_index<i_line_count_mnth_end_effrt; i_effrt_index++)
		{
			var i_sub_prac = nlapiGetLineItemValue('people_plan_sublist','sub_practice_month_end',i_effrt_index);
			var i_sub_prac_role = nlapiGetLineItemValue('people_plan_sublist','role_month_end',i_effrt_index);
			var i_location_mnth_end = nlapiGetLineItemValue('people_plan_sublist','location_month_end',i_effrt_index);
			var i_level_mnth_end = nlapiGetLineItemValue('people_plan_sublist','level_month_end',i_effrt_index);
			a_existing_mnth_end_effrt_prac.push({
										'sub_prac': i_sub_prac,
										'sub_prac_role': i_sub_prac_role,
										'location': i_location_mnth_end,
										'level': i_level_mnth_end
										});
			
		}
		
		var i_sub_practice_selected = nlapiGetCurrentLineItemValue('people_plan_sublist','sub_practice_month_end');
		var i_role_selected = nlapiGetCurrentLineItemValue('people_plan_sublist','role_month_end');
		var i_location_selected = nlapiGetCurrentLineItemValue('people_plan_sublist','location_month_end');
		var i_level_selected = nlapiGetCurrentLineItemValue('people_plan_sublist','level_month_end');
		for(var i_index=0; i_index<a_existing_mnth_end_effrt_prac.length; i_index++)
		{
			if(i_sub_practice_selected == a_existing_mnth_end_effrt_prac[i_index].sub_prac)
			{
				if(i_role_selected == a_existing_mnth_end_effrt_prac[i_index].sub_prac_role)
				{
					if(i_level_selected == a_existing_mnth_end_effrt_prac[i_index].level)
					{
						if(i_location_selected == a_existing_mnth_end_effrt_prac[i_index].location)
						{
							alert('You can use the existing row for this effort plan');
							nlapiSetCurrentLineItemValue('people_plan_sublist','location_month_end','');
							return false;
						}
					}
				}
			}
		}
	}
	else if(type == 'people_plan_sublist')
	{
		if(name != 'parent_practice_month_end' && name != 'sub_practice_month_end' && name != 'level_month_end' && name != 'role_month_end' && name != 'location_month_end' && name != 'resource_cost_month_end' && name != 'revenue_share_mnth_end')
		{
			var f_old_effrt = nlapiGetLineItemValue('people_plan_sublist',name,nlapiGetCurrentLineItemIndex('people_plan_sublist'));
			var f_updated_effrt = nlapiGetCurrentLineItemValue('people_plan_sublist',name)
			if(parseFloat(f_updated_effrt) < 0)
			{
				alert('You cannot decrease future effort as it is real time data based on allocation, you can only increase this effort!');
				nlapiSetCurrentLineItemValue('people_plan_sublist',name,f_old_effrt,false);
				return false;
			}
		}
	}
	else if(type=='revenue_sublist_month_end')
	{
		var d_project_strt_date = nlapiGetFieldValue('proj_strt_date');
		var d_project_end_date = nlapiGetFieldValue('proj_end_date');
		var practices_cnt=nlapiGetLineItemCount('revenue_sublist_month_end');
		var i_current_line_prac = nlapiGetCurrentLineItemIndex('revenue_sublist_month_end');
		var monthBreakUp = getMonthsBreakup(
					        nlapiStringToDate(d_project_strt_date),
					        nlapiStringToDate(d_project_end_date));

		//if(i_current_line_prac==1)
		{
			for (var j_ind = 0; j_ind < monthBreakUp.length; j_ind++)
			{
				var months = monthBreakUp[j_ind];
				var s_month_name = getMonthName(months.Start); // get month name
				var month_strt = nlapiStringToDate(months.Start);
				var month_end = nlapiStringToDate(months.End);
				var i_month = month_strt.getMonth();
				var i_year = month_strt.getFullYear();
				s_month_name = s_month_name +'_'+ i_year;
				var f_share=nlapiGetCurrentLineItemValue('revenue_sublist_month_end',''+s_month_name.toLowerCase());
				if(!f_share && (s_month_name.toLowerCase() == name))
				{
					alert('Monthly Revenue cannot be blank.For No revenue, please enter Zero value');
					return false;
				}
				else
				{
					if(s_month_name.toLowerCase() == name)
					{
					var num=Math.round(f_share);
					nlapiSetCurrentLineItemValue('revenue_sublist_month_end',name,num,false);
					}
					//nlapiSetLineItemValue('revenue_sublist_month_end',''+s_month_name.toLowerCase(),1,100);
				}
		
			}
		}
	}
	
}

function setContractorCost(s_costDetails)
{
	s_costDetails = s_costDetails.split('##');
	var i_current_line_index = s_costDetails[0];
	var f_contractor_cost = s_costDetails[1];
	var i_sublist = s_costDetails[2];
	
	if(i_sublist == 1)
	{
		nlapiSelectLineItem('effort_sublist',i_current_line_index);
		nlapiSetCurrentLineItemValue('effort_sublist','cost_effort_plan',parseFloat(f_contractor_cost));
		nlapiSetCurrentLineItemValue('effort_sublist','level_effort_plan',13);
	}
	else
	{
		nlapiSelectLineItem('people_plan_sublist',i_current_line_index);
		nlapiSetCurrentLineItemValue('people_plan_sublist','resource_cost_month_end',parseFloat(f_contractor_cost));
		//nlapiSetCurrentLineItemValue('people_plan_sublist','level_month_end',13);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value!= '- None -' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }


function lineInit(type)
{
   var lineCount = nlapiGetLineItemCount('people_plan_sublist');
   var currentLineIndex = nlapiGetCurrentLineItemIndex('people_plan_sublist');
   //alert('lineCount '+lineCount+'currentLineIndex '+currentLineIndex);
   /*var emp_level = nlapiGetCurrentLineItemValue('people_plan_sublist','level_month_end');
   //alert('Level '+emp_level);
   if(emp_level == 13)
	  nlapiDisableLineItemField('people_plan_sublist', 'role_month_end', true); 
  else
	  nlapiDisableLineItemField('people_plan_sublist', 'role_month_end', false);*/
   if(currentLineIndex<=lineCount)
   {
	  nlapiDisableLineItemField('people_plan_sublist', 'sub_practice_month_end', true);
	  nlapiDisableLineItemField('people_plan_sublist', 'level_month_end', true);
	  nlapiDisableLineItemField('people_plan_sublist', 'location_month_end', true);
   }
   else
   {
	  nlapiDisableLineItemField('people_plan_sublist', 'sub_practice_month_end', false);
	  nlapiDisableLineItemField('people_plan_sublist', 'level_month_end', false);
	  nlapiDisableLineItemField('people_plan_sublist', 'location_month_end', false);
   }
}



function Search_revenue_Location_subsdidary(array_filters) {
    var filters = [];
    filters.push(["isinactive", "is", "F"]);

    if (array_filters) {
        filters.push("And");
        filters.push(array_filters);
    }

    var obj_rev_loc_subSrch = nlapiSearchRecord("customrecord_revenue_fields_mapping", null,
        filters,
        [
            new nlobjSearchColumn("custrecord_revenue_sourcetype", null, "GROUP").setSort(false),
            new nlobjSearchColumn("custrecord_source_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_revenue_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_subsidairy", null, "GROUP"),
            new nlobjSearchColumn("custrecord_revenue_subsidiary", null, "GROUP"),
            new nlobjSearchColumn("custrecord_cost_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_cost_in_dollor", null, "GROUP"),
            new nlobjSearchColumn("custrecord_offsite_onsite", null, "GROUP"),
            new nlobjSearchColumn("custrecord_currency_symbol", null, "GROUP"),
            new nlobjSearchColumn("custrecord_plcurrency", null, "GROUP"),
            new nlobjSearchColumn("custrecord_pl_rate_mapping", null, "GROUP"),
            new nlobjSearchColumn("custrecord_pl_revenue_mapping", null, "GROUP")
        ]
    );

    var o_rev_loc_sub = {
        "Location": {},
        "Subsidairy": {},
        "Cost Location": {},
        "Cost Reference": {},
        "Onsite/Offsite": {},
        'CurrencySymbol': {},
        'forecastExchangerates': {},
        'plcost': {},
        'plrevenue': {},
        'PLfldids': {}
    };
	
	if(obj_rev_loc_subSrch){
		
    for (var i = 0; obj_rev_loc_subSrch.length > i; i++) {
        var i_sourcetype = obj_rev_loc_subSrch[i].getText("custrecord_revenue_sourcetype", null, "GROUP");
        switch (i_sourcetype) {
            case "Location":
                o_rev_loc_sub.Location[obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_location', null, "GROUP");
                break;

            case "Subsidairy":
                o_rev_loc_sub["Subsidairy"]
                    [obj_rev_loc_subSrch[i].getValue('custrecord_subsidairy', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_subsidiary', null, "GROUP");
                o_rev_loc_sub["Onsite/Offsite"]
                    [obj_rev_loc_subSrch[i].getValue('custrecord_subsidairy', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_offsite_onsite', null, "GROUP");
                break;

            case "Cost Location":
                o_rev_loc_sub["Cost Location"][obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_cost_location', null, "GROUP");
                o_rev_loc_sub["Cost Reference"][obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_cost_in_dollor', null, "GROUP");
                break;
            case "Currency":

                var i_map_subsidairy = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_subsidiary', null, "GROUP");
                var s_pl_revenue_map = obj_rev_loc_subSrch[i].getValue('custrecord_pl_revenue_mapping', null, "GROUP");
                var s_pl_cost_map = obj_rev_loc_subSrch[i].getValue('custrecord_pl_rate_mapping', null, "GROUP");
                var s_currencey = obj_rev_loc_subSrch[i].getText('custrecord_plcurrency', null, "GROUP");
                o_rev_loc_sub["forecastExchangerates"][s_currencey] = s_pl_cost_map;
                o_rev_loc_sub["CurrencySymbol"][s_currencey] = obj_rev_loc_subSrch[i].getValue('custrecord_currency_symbol', null, "GROUP");
                if (i_map_subsidairy) {
                    o_rev_loc_sub["plcost"][i_map_subsidairy] = s_pl_cost_map;
                }
                if (_logValidation(s_pl_revenue_map)) {
                    o_rev_loc_sub["plrevenue"][i_map_subsidairy] = s_pl_revenue_map;
                    o_rev_loc_sub["PLfldids"][s_pl_revenue_map] = s_pl_revenue_map;
                }

                if (!o_rev_loc_sub["PLfldids"][s_pl_cost_map] && _logValidation(s_pl_cost_map)) {
                    o_rev_loc_sub["PLfldids"][s_pl_cost_map] = s_pl_cost_map;
                }

                break;
        }
    }
	}
    return o_rev_loc_sub;
}



function _logValidation(value) {
    if (value != null && value!= '- None -' && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}