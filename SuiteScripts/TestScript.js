function scheduled(type){
	
	try{
		var context = nlapiGetContext();
		var je_list = new Array();
		//var inv_id = '450963';
		//var closingComments = 'Project status updated as closed as it has been more than 90 days past the end date of the project.';
		var searchObj = nlapiSearchRecord('customrecord_fp_revrec_month_end_effort','customsearch2018');
		if(searchObj){
			for(var i=0;i<searchObj.length;i++){
				var recID = searchObj[i].getId();
				if(je_list.indexOf(searchObj[i].getId())>=0)
				{
					
				}
				else{
				if(recID){
					var loadRec = nlapiLoadRecord('customrecord_fp_revrec_month_end_effort',recID);
					var practiceVal = loadRec.getFieldValue('custrecord_mnth_end_practice');
					/*if(parseInt(practiceVal) == parseInt(460) || parseInt(practiceVal) == parseInt(458) || parseInt(practiceVal) == parseInt(326) ||
							parseInt(practiceVal) == parseInt(324) || parseInt(practiceVal) == parseInt(479) || parseInt(practiceVal) == parseInt(323)	|| parseInt(practiceVal) == parseInt(437) 
							|| parseInt(practiceVal) == parseInt(457)){
						nlapiDeleteRecord('customrecord_fp_cost_setup',recID);
					}*/
					if(parseInt(practiceVal) == parseInt(481)){
						loadRec.setFieldValue('custrecord_mnth_end_practice',492);
					}
					/*var lineCount = loadRec.getLineItemCount('item');
					for(var j=1;j<=lineCount;j++){
						
						var practiceVal = loadRec.getLineItemValue('item','department',j);
						if(parseInt(practiceVal) == parseInt(460) || parseInt(practiceVal) == parseInt(458) || parseInt(practiceVal) == parseInt(326) ||
								parseInt(practiceVal) == parseInt(324) || parseInt(practiceVal) == parseInt(479) || parseInt(practiceVal) == parseInt(323)	|| parseInt(practiceVal) == parseInt(437) 
								|| parseInt(practiceVal) == parseInt(457)){
							loadRec.setLineItemValue('item','department',j,497);
						}
					}
					
					var lineCount_time = loadRec.getLineItemCount('expense');
					for(var k=1;k<=lineCount_time;k++){
						//var applyfld = loadRec.getLineItemValue('time','apply',k);
						//if(applyfld == 'T'){
						var practiceVal = loadRec.getLineItemValue('expense','department',k);
						if(parseInt(practiceVal) == parseInt(460) || parseInt(practiceVal) == parseInt(458) || parseInt(practiceVal) == parseInt(326) ||
								parseInt(practiceVal) == parseInt(324) || parseInt(practiceVal) == parseInt(479) || parseInt(practiceVal) == parseInt(323)	|| parseInt(practiceVal) == parseInt(437) 
								|| parseInt(practiceVal) == parseInt(457)){
							loadRec.setLineItemValue('expense','department',k,497);
						}
						//}
					}*/
					//update the fields
				//	var finalComments = closingComments + '\n' +comments;
					//loadRec.setFieldValue('comments',finalComments);
					//loadRec.setFieldValue('status',1);
					var id = nlapiSubmitRecord(loadRec);
					je_list.push(id);
					nlapiLogExecution('DEBUG','Processed id :---',id);
					yieldScript(context);
				}
				}
			}
		}
		/*var appro = ['5764','69145'];
		var loadInv = nlapiLoadRecord('vendorbill',parseInt(470688));
		loadInv.setFieldValue('custbody_financemanager',appro);
		var id = nlapiSubmitRecord(loadInv);
		var loadInv = nlapiLoadRecord('invoice',parseInt(inv_id))
		var billingFrom = loadInv.getFieldValue('custbody_billfrom');
		var billingTo = loadInv.getFieldValue('custbody_billto');
		billingFrom = nlapiStringToDate(billingFrom);
		billingTo = nlapiStringToDate(billingTo);
		
		var getLineCount = loadInv.getLineItemCount('time');
		for(var i=1;i<=getLineCount;i++){
			
			yieldScript(context);
			nlapiLogExecution('DEBUG','Processed Line :---',i);
			var applyfld = loadInv.getLineItemValue('time','apply',i);
			var billeddate = loadInv.getLineItemValue('time','billeddate',i);
			billeddate = nlapiStringToDate(billeddate);
			var rate = loadInv.getLineItemValue('time','rate',i);
			//var applyfld = loadInv.getLineItemValue('time','apply',i);
			
			if(billeddate >= billingFrom && billeddate <= billingTo){
				loadInv.setLineItemValue('time','apply',i,'T');
				loadInv.setLineItemValue('time','rate',i,0);
				//loadInv.setLineItemValue('time','apply',i,'T');
			}
		}*/
		//yieldScript(context);
		//var id = nlapiSubmitRecord(loadInv);
	}
	catch (err) {
		nlapiLogExecution('error', 'getZeroAllocationEmployee', err);
		throw err;
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}