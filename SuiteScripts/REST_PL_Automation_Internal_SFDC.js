/**
 * P/L report for managers
 * 
 * Version Date Author Remarks
 * 
 * 1.00 16 May 2018 Deepak MS
 * 
 */
function postRESTlet(dataIn) {
    try {
        var response = new Response();
        var current_date = nlapiDateToString(new Date());



        //var employeeId =  getUserUsingEmailId(dataIn.EmailId);
        //var requestType = dataIn.RequestType;  
        //var email = 'rahul.murali@brillio.com';
        //var employeeId =  getUserUsingEmailId(email);
        var requestType = 'GET';

        //Log for current date
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));


        switch (requestType) {

            case M_Constants.Request.Get:

                if (requestType) {
                    response.Data = actual_data_flow(dataIn);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
        }

    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;
        response.Status = false;
    }
    nlapiLogExecution('debug', 'response', 'Done');
    return response;

}

function actual_data_flow(dataIn) {
    try {
        //var str = request.getBody();

        var context = nlapiGetContext();

        //	var i_user_id = nlapiGetUser();

        var a_project_list = new Array();

        var s_selected_project_name = '';

       //var d_today = nlapiDateToString(new Date());
      //  var d_today = dataIn.Date;
        var d_today = '2/1/2018';
        d_today = nlapiStringToDate(d_today);


        var d_day = nlapiAddMonths(d_today, -1);
        nlapiLogExecution('debug', 'd_day', d_day);
        var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
        var s_month_start = d_month_start.getMonth() + 1 + '/' +
            d_month_start.getDate() + '/' + d_month_start.getFullYear();
        var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
        var s_month_end = d_month_end.getMonth() + 1 + '/' +
            d_month_end.getDate() + '/' + d_month_end.getFullYear();

        var showAll = false;

        var s_from = '';
        var s_to = '';




        var search = nlapiLoadSearch('transaction', 2480);
        var filters = search.getFilters();

        //Search for exchange Rate
        var f_rev_curr = 0;
        var f_cost_curr = 0;
        var filters = [];

        //var filters = [];
        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
        var a_dataVal = {};
        var dataRows = [];

        var column = new Array();
        column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
        column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
        column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
        column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
        //column[4] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
        column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
        column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
        //column[5] = new nlobjSearchColumn('custrecord_pl_gbp_cost_rate');

        var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
        if (currencySearch) {
            for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {

                a_dataVal = {
                    s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
                    i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
                    rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
                    cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
                    //gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_revenue'),
                    crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
                    nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate')
                    //gbp_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_cost_rate'),


                };
                if (_logValidation(a_dataVal))
                    dataRows.push(a_dataVal);


            }
        }




        var project_search_results = getTaggedProjectList();

        // create html for select using the project data
        var s_project_options = '';
        var a_selected_project_list = '';
        var s_project_number = '';
        var s_project_name = '';
        for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
            var i_project_id = project_search_results[i].getValue('internalid');
            s_project_number = project_search_results[i]
                .getValue('entityid');
            s_project_name = project_search_results[i].getValue('companyname');



            if (_logValidation(s_project_number)) {
                a_project_list.push(s_project_number);
            }


            /*var s_selected = '';
            if ((a_selected_project_list != null && a_selected_project_list
                    .indexOf(s_project_number) != -1)
                    || showAll == true) {
            	s_selected = ' selected="selected" ';

            	if (s_selected_project_name != '') {
            		s_selected_project_name += ', ';
            	}

            	s_selected_project_name += s_project_number + ' '
            	        + s_project_name;
            }

            s_project_options += '<option value="' + s_project_number + '" '
                    + s_selected + '>' + s_project_number + ' '
                    + s_project_name + '</option>';*/
        }

        if (a_selected_project_list == null ||
            a_selected_project_list.length == 0) {
            if (a_project_list.length > 0) {
                a_selected_project_list = a_project_list;

                showAll = true;
            } else {
                a_selected_project_list = new Array();
            }
        }

        if (a_selected_project_list != null &&
            a_selected_project_list.length != 0) {
            var s_formula = '';

            for (var i = 0; i < a_selected_project_list.length; i++) {
                if (i != 0) {
                    s_formula += " OR";
                }

                s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
                    a_selected_project_list[i] + "' ";
            }

            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("CASE WHEN " + s_formula +
                " THEN 1 ELSE 0 END");

            filters = filters.concat([projectFilter]);
        } else {
            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("0");

            filters = filters.concat([projectFilter]);
        }

        var s_from = s_month_start;
        var s_to_ = s_month_end;
        filters = filters.concat([new nlobjSearchFilter('trandate', null,
            'within', s_from, s_to_)]);

        var columns = search.getColumns();

        columns[0].setSort(false);
        columns[3].setSort(true);
        columns[9].setSort(false);
        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
        filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
        //  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));

        filters.push(new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
            'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
        ]));

        var search_results = searchRecord('transaction', null, filters, [
            columns[2], columns[1], columns[0], columns[3], columns[4],
            columns[5],columns[6],columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14],
            columns[15], columns[16], columns[17], columns[18], columns[19], columns[20], columns[21],columns[22]
        ]);


        var o_json = new Object();

        var o_data = {

            'Account_Management_Salary_Cost': [],
            'Account_Management_Other_Cost': [],
            'Regional_Management_Salary_Cost': [],
            'Regional_Management_Other_Cost': [],
            'Horizontal_Management_onsite_Salary_Cost': [],
            'Horizontal_Management_onsite_Other_Cost': [],
            'Horizontal_Management_offshore_Salary_Cost': [],
            'Horizontal_Management_offshore_Other_Cost': [],
            'Horizontal_Investments_Salary_Cost': [],
            'Horizontal_Investments_Other_Cost': [],
            'Horizontal_Dedicated_Resources_Salary_Cost': [],
            'Horizontal_Dedicated_Resources_Other_Cost': [],


        };

        var new_object = null;

        var s_period = '';

        var a_period_list = [];

        var a_category_list = [];
        a_category_list[0] = '';
        var a_group = [];

        var a_income_group = [];
        var j_other_list = {};
        var other_list = [];

        var col = new Array();
        col[0] = new nlobjSearchColumn('custrecord_account_name');
        col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
        var oth_fil = new Array();
        oth_fil[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
        var other_Search = nlapiSearchRecord('customrecord_pl_other_costs', null, oth_fil, col);
        if (other_Search) {
            for (var i_ind = 0; i_ind < other_Search.length; i_ind++) {
                var acctname = other_Search[i_ind].getValue('custrecord_account_name');

                a_category_list.push(acctname);


                j_other_list = {
                    o_acct: other_Search[i_ind].getValue('custrecord_account_name'),
                    o_type: other_Search[i_ind].getText('custrecord_type_of_cost')
                };

                other_list.push(j_other_list);


            }
        }

        for (var i = 0; search_results != null && i < search_results.length; i++) {
            var period = search_results[i].getText(columns[0]);
            var amount = '';
            //Code updated by Deepak, Dated - 21 Mar 17
            var s_month_year = period.split(' ');
            var s_mont = s_month_year[0];
            s_mont = getMonthCompleteName(s_mont);
            var s_year_ = s_month_year[1];
            var f_revRate = 63.65;
            var f_costRate = 66.0;
            var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate;
            //	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
            //	nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
            //nlapiLogExecution('audit','accnt name:- '+rate);

            //Fetch matching cost and rev rate convertion rate
            for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
                if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
                    f_revRate = dataRows[data_indx].rev_rate;
                    f_costRate = dataRows[data_indx].cost_rate;
                    //f_gbp_rev_rate=dataRows[data_indx].gbp_rev_rate;
                    f_crc_cost_rate = dataRows[data_indx].crc_cost;
                    f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
                }
            }
            /*	if(f_gbp_rev_rate==0)
            	{
            	f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
            	}*/

            var transaction_type = search_results[i].getText(columns[8]);

            var transaction_date = search_results[i].getValue(columns[9]);

            var i_subsidiary = search_results[i].getValue(columns[10]);

            var amount = parseFloat(search_results[i].getValue(columns[1]));

            var category = search_results[i].getValue(columns[3]);

            var s_account_name = search_results[i].getValue(columns[11]);

            var currency = search_results[i].getValue(columns[12]);

            var exhangeRate = search_results[i].getValue(columns[13]);
            //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

            var s_project_description = search_results[i].getValue(columns[16]);
            var s_customer = search_results[i].getValue(columns[17]);
            var s_Proj_category = search_results[i].getValue(columns[21]);

            var project_ID = '';
            var projectName = '';
            var s_subPractice = '';
            var s_parentPractice = '';
            var projectSearch = '';
            if (_logValidation(s_project_description)) {
                project_ID = s_project_description.split(' ')[0];
                projectName = s_project_description.split(' ')[1];
                s_project_number = project_ID;
                //s_project_name =  projectName;
            }
            var s_practice = search_results[i].getText(columns[14]);
            s_subPractice = s_practice;
            s_parentPractice = search_results[i].getText(columns[15]);
            if (_logValidation(s_practice))
                s_parentPractice = s_practice.split(':')[0];

            nlapiLogExecution('audit', 'accnt name:- ' + s_account_name);

            /*if ((currency == parseInt(2)) && (parseInt(i_subsidiary) != parseInt(2))) {
                amount = parseFloat(amount) * parseFloat(exhangeRate);

            }*/
            var flag = false;
            if ((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue') {
                amount = parseFloat(amount) * parseFloat(exhangeRate);
                amount = parseFloat(amount) / parseFloat(f_revRate);
                flag = true;

            }
            //AMount Convertion Logic
            if ((parseInt(i_subsidiary) != parseInt(2)) && (currency != parseInt(1))) {
                if (category != 'Revenue' && category != 'Other Income' &&
                    category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
                    if (currency == parseInt(9)) {
                        amount = parseFloat(amount) / parseFloat(f_crc_cost_rate);
                    } else if (currency == parseInt(8)) {
                        amount = parseFloat(amount) / parseFloat(f_nok_cost_rate);
                    } else {
                        //f_total_expense += o_data[s_category][j].amount;
                        amount = parseFloat(amount) / parseFloat(f_costRate);
                    }

                } else {

                    amount = parseFloat(amount) / parseFloat(f_revRate);

                    //f_total_revenue += o_data[s_category][j].amount;
                }
            } //END



            var isWithinDateRange = true;

            var d_transaction_date = nlapiStringToDate(transaction_date,
                'datetimetz');

            if (s_from != '') {
                var d_from = nlapiStringToDate(s_from, 'datetimetz');

                if (d_transaction_date < d_from) {
                    isWithinDateRange = false;
                }
            }

            if (s_to != '') {
                var d_to = nlapiStringToDate(s_to, 'datetimetz');

                if (d_transaction_date > d_to) {
                    isWithinDateRange = false;
                }
            }

            if (isWithinDateRange == true) {
                var i_index = a_period_list.indexOf(period);

                if (i_index == -1) {
                    if (_logValidation(period)) {
                        a_period_list.push(period);
                    } else {
                        //nlapiLogExecution('debug', 'error', '388');
                    }

                }

                i_index = a_period_list.indexOf(period);
            }

            //Account Management
            if (category == 'People Cost' && s_Proj_category == 'Account Management') {


                o_data['Account_Management_Salary_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Salary Cost'
                });
            } else if (s_Proj_category == 'Account Management') {


                o_data['Account_Management_Other_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Other Cost'
                });

            }
            //Regional Management
            if (category == 'People Cost' && s_Proj_category == 'Regional Management') {


                o_data['Regional_Management_Salary_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Salary Cost'
                });

            } else if (s_Proj_category == 'Regional Management') {

                //avoiding discounts from account						  


                o_data['Regional_Management_Other_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Other Cost'
                });

            }

            //Horizontal Management Onsite
            if (category == 'People Cost' && s_Proj_category == 'Horizontal Management Onsite') {


                o_data['Horizontal_Management_onsite_Salary_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Salary Cost'
                });




            } else if (s_Proj_category == 'Horizontal Management Onsite') {

                //avoiding discounts from account						  


                o_data['Horizontal_Management_onsite_Other_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Other Cost'
                });

            }
            //Horizontal Management Offshore
            if (category == 'People Cost' && s_Proj_category == 'Horizontal Management Offshore') {


                o_data['Horizontal_Management_offshore_Salary_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Salary Cost'
                });




            } else if (s_Proj_category == 'Horizontal Management Offshore') {

                //avoiding discounts from account						  


                o_data['Horizontal_Management_offshore_Other_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Other Cost'
                });

            }
           //Investment
            if (category == 'People Cost' && s_Proj_category == 'Investment') {


                o_data['Horizontal_Investments_Salary_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Salary Cost'
                });




            } else if (s_Proj_category == 'Investment') {

                //avoiding discounts from account						  


                o_data['Horizontal_Investments_Other_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Other Cost'
                });

            }

            //Horizontal Management Offshore
            if (category == 'People Cost' && s_Proj_category == 'Bench') {


                o_data['Horizontal_Dedicated_Resources_Salary_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Salary Cost'
                });




            } else if (s_Proj_category == 'Bench') {

                //avoiding discounts from account						  


                o_data['Horizontal_Dedicated_Resources_Other_Cost'].push({
                    'project_id': s_project_number,
                    //	's_subsidiary' : s_subsidiary,
                    'period': period,
                    'amount': amount.toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    //	's_customer' : s_customer,
                    'sub_practice': s_subPractice,
                    'practice': s_parentPractice,
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_customer_id': search_results[i].getValue(columns[22]),
                    's_region': search_results[i].getText(columns[20]),
                    's_account_name': s_Proj_category +' '+'Other Cost'
                });

            }
        }



        return o_data;

    } catch (err) {
        nlapiLogExecution('Debug', 'RestLet Main ', err);
    }
}



// Used to display the html, by replacing the placeholders
function replaceValues(content, oValues) {
    for (param in oValues) {
        // Replace null values with blank
        var s_value = (oValues[param] == null) ? '' : oValues[param];

        // Replace content
        content = content.replace(new RegExp('{{' + param + '}}', 'gi'),
            s_value);
    }

    return content;
}

function getTaggedProjectList() {
    try {


        // get the list of project the user has access to
        /*var project_filter = [
                [[ 'custbody_projecttype', 'anyOf', 2 ], 'and' ,
                ['startdate',  'notafter',enDate]), 'and',
                ['enddate',  'notbefore',stDate]), 'and',		       
                [ 'status', 'anyOf', 2 ] ];*/

        var project_search_results = searchRecord('job', 2422, null, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("companyname")
        ]);



        if (project_search_results.length == 0) {
            throw "You don't have any projects under you.";
        }
        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}



function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function getSelectedProjects(project) {
    try {



        // get the list of project the user has access to
        var project_filter = [
            [
                ['internalid',
                    'anyOf', project
                ]
            ]
        ];

        var project_search_results = searchRecord('job', null, project_filter, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("companyname"),
            new nlobjSearchColumn("internalid")
        ]);

        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        if (project_search_results.length == 0) {
            throw "You didn't selected any projects.";
        }

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}
//Get the complete month name
function getMonthCompleteName(month) {
    var s_mont_complt_name = '';
    if (month == 'Jan')
        s_mont_complt_name = 'January';
    if (month == 'Feb')
        s_mont_complt_name = 'February';
    if (month == 'Mar')
        s_mont_complt_name = 'March';
    if (month == 'Apr')
        s_mont_complt_name = 'April';
    if (month == 'May')
        s_mont_complt_name = 'May';
    if (month == 'Jun')
        s_mont_complt_name = 'June';
    if (month == 'Jul')
        s_mont_complt_name = 'July';
    if (month == 'Aug')
        s_mont_complt_name = 'August';
    if (month == 'Sep')
        s_mont_complt_name = 'September';
    if (month == 'Oct')
        s_mont_complt_name = 'October';
    if (month == 'Nov')
        s_mont_complt_name = 'November';
    if (month == 'Dec')
        s_mont_complt_name = 'December';

    return s_mont_complt_name;
}