/**
 * @author Nikhil
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_email_trigger(type)
{
	if (type == 'create') {
		var i_record = nlapiGetRecordId();
		var s_recordtype = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' Record ID -->' + i_record);
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' Record Type -->' + s_recordtype);
		var record_obj = nlapiLoadRecord(s_recordtype, i_record);
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' userid -->' + userId);
		var emailBody = 'the record has been edited';
		var delivery_mgr = record_obj.getFieldValue('custentity_deliverymanager');
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' deliver mgr -->' + delivery_mgr);
		if (delivery_mgr != null && delivery_mgr != '' && delivery_mgr != undefined) {
			var record_obj = nlapiLoadRecord('employee',delivery_mgr);
			var email_deliverymgr = record_obj.getFieldValue('email');
			var records = new Object();
            records['entity'] = userId
			var sendEmail = nlapiSendEmail(userId, email_deliverymgr, 'Notification',emailBody,null,null,records,null,null,null);
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' mail sent1 -->' + sendEmail);
			
		}
		var vertical_head = record_obj.getFieldValue('custentity_verticalhead');
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' vertical head -->' + vertical_head);
		if (vertical_head != null && vertical_head != '' && vertical_head != undefined) {
			var record_obj2 = nlapiLoadRecord('employee', vertical_head);
			var email_vertical_head = record_obj2.getFieldValue('email');
			
			var records = new Object();
            records['entity'] = userId
			var sendEmail2 = nlapiSendEmail(userId, email_vertical_head, 'Notification',emailBody,null,null,records,null,null,null);
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' mail sent2 -->' + sendEmail2);
			
		}
		var executing_practise = record_obj.getFieldValue('custentity_executingpractice');
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' executing-->' + executing_practise);
		if (executing_practise != null && executing_practise != '' && executing_practise != undefined) {
			var record_obj3 = nlapiLoadRecord('employee', executing_practise);
			var email_executing_practise = record_obj3.getFieldValue('email');
			var records = new Object();
            records['entity'] = userId
			var sendEmail3 = nlapiSendEmail(userId, email_executing_practise, 'Notification',emailBody,null,null,records,null,null,null);
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' mail sent3 -->' + sendEmail3);
			
		}
		
		nlapiSubmitRecord(record_obj,true,true)
		
	}
	if (type == 'edit') {
		var i_record = nlapiGetRecordId();
		var s_recordtype = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' Record ID -->' + i_record);
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' Record Type -->' + s_recordtype);
		var record_obj = nlapiLoadRecord(s_recordtype, i_record);
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' userid -->' + userId);
		var emailBody = 'the record has been edited';
		var delivery_mgr = record_obj.getFieldValue('custentity_deliverymanager');
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' deliver mgr -->' + delivery_mgr);
		if (delivery_mgr != null && delivery_mgr != '' && delivery_mgr != undefined) {
			var record_obj = nlapiLoadRecord('employee', delivery_mgr);
			var email_deliverymgr = record_obj.getFieldValue('email');
			var records = new Object();
            records['entity'] = userId
			var sendEmail = nlapiSendEmail(userId, email_deliverymgr, 'Notification',emailBody,null,null,records,null,null,null);
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' mail sent1 -->' + sendEmail);
			
		}
		var vertical_head = record_obj.getFieldValue('custentity_verticalhead');
		nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' vertical head -->' + vertical_head);
		if (vertical_head != null && vertical_head != '' && vertical_head != undefined) {
			var record_obj2 = nlapiLoadRecord('employee', vertical_head);
			var email_vertical_head = record_obj2.getFieldValue('email');
			var records = new Object();
            records['entity'] = userId
			var sendEmail2 = nlapiSendEmail(userId, email_vertical_head, 'Notification',emailBody,null,null,records,null,null,null);
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' mail sent2 -->' + sendEmail2);
			
		}
		var executing_practise = record_obj.getFieldValue('custentity_executingpractice');
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' executing-->' + executing_practise);
		if (executing_practise != null && executing_practise != '' && executing_practise != undefined) {
			var record_obj3 = nlapiLoadRecord('employee', executing_practise);
			var email_executing_practise = record_obj3.getFieldValue('email');
			var records = new Object();
            records['entity'] = userId
			var sendEmail3 = nlapiSendEmail(userId, email_executing_practise, 'Notification',emailBody,null,null,records,null,null,null);
			nlapiLogExecution('DEBUG', 'afterSubmit_send_mail', ' mail sent3 -->' + sendEmail3);
			
		}
		nlapiSubmitRecord(record_obj,true,true)
	}
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
