/**
 * hide line item columns
 * 
 * Version Date Author Remarks 1.00 18 Nov 2015 nitish.mishra
 * 
 */

// hide line item columns
function userEventBeforeLoad(type, form) {
	try {
		// nlapiLogExecution('debug', 'type', type);

		[ 'custrecord_employee', 'custrecord_prpractices',
		        'custrecord_totalvalue', // 'custrecord_subpracticehead'
		        'custrecord_subsidiaryforpr', 'custrecord_procurementteam',
		        'custrecord_itforpurchaserequest',
		        'custrecord_pri_delivery_manager',
		        'custrecord_pritem_job_type', 'custrecord_pr_customer',
		        'custrecord_prterritory' ].forEach(function(fieldId) {

			var field = nlapiGetLineItemField(
			        'recmachcustrecord_purchaserequest', fieldId, 1);
			if (field) {
				field.setDisplayType('hidden');
			}
		});

		[ 'custrecord_financehead' ].forEach(function(fieldId) {

			var field = nlapiGetLineItemField('recmachcustrecord_prquote',
			        fieldId, 1);
			if (field) {
				field.setDisplayType('hidden');
			}
		});
	} catch (err) {
		nlapiLogExecution('error', 'before load', err);
	}
}