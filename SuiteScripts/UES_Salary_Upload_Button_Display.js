/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Salary_Upload_Button_Display.js
	Author      : Shweta Chopde
	Date        : 29 July 2014
    Description : Validations on Salary Upload Process Records


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_salary_upload(type)
{
	
	form.setScript('customscript_cli_salary_upload_validatio');
	
	var i_recordID = nlapiGetRecordId();
	
	var i_status = nlapiGetFieldValue('custrecord_file_status');
	nlapiLogExecution('DEBUG', 'beforeLoad_salary_upload', 'i_status : '+i_status);
	
	var i_JE_array = nlapiGetFieldValues('custrecord_journal_entries_created')
	nlapiLogExecution('DEBUG', 'beforeLoad_salary_upload', 'i_JE_array : '+i_JE_array);
	
	var i_CSV_file = nlapiGetFieldValue('custrecord_csv_file_s_p');
	nlapiLogExecution('DEBUG', 'beforeLoad_salary_upload', 'i_CSV_file : '+i_CSV_file);
	
	if(i_status == 5 && _logValidation(i_JE_array)) // If status is complete and JE list is present
	{
		form.addButton('custpage_delete','Delete','delete_JE()');
		//form.addButton('custpage_update','Update','update_JE()');
							
	}//Complete
	else //
	{
		if(_logValidation(i_JE_array)) // if status is not "Complete" and JE is present
		{
			form.addButton('custpage_delete','Delete','delete_JE()');
			//form.addButton('custpage_update','Update','update_JE()');							
		}
	}
	
	if(i_status == 1)
	{
		form.addButton('custpage_create_je','Create Journal Entry','create_journal_entry()');	
	}//Success
	
	if((i_status == 4)||(i_status == 2))
	{
		if(_logValidation(i_CSV_file))
		{
		 form.addButton('custpage_validate_process','Validate / Process','validate_process()');
			
		}
		
	}//Success
	
	
	form.addButton('custpage_refresh','Refresh','refresh_salary_upload()');	
		
	form.getField('custrecord_csv_file_id_no').setDisplayType('hidden');
	
	
/*
	
	
	
	var i_click_status = nlapiGetFieldValue('custrecord_button_click_type');
	nlapiLogExecution('DEBUG', 'beforeLoad_salary_upload', ' Click Status -->' + i_click_status);
	if(_logValidation(i_recordID))
	{		
		var column = new Array();
		var filters = new Array();
	
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING']));
		
		if(i_click_status == 'VALIDATE')
		{
			filters.push(new nlobjSearchFilter('internalid', 'script','is','242'));
		}
		if(i_click_status == 'CREATE JE')
		{
			filters.push(new nlobjSearchFilter('internalid', 'script','is','244'));
		}
		if(i_click_status == 'DELETE')
		{
			filters.push(new nlobjSearchFilter('internalid', 'script','is','246'));
		}

		column.push(new nlobjSearchColumn('name', 'script'));
		column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
		column.push(new nlobjSearchColumn('datecreated'));
		column.push(new nlobjSearchColumn('status'));
		column.push(new nlobjSearchColumn('startdate'));
		column.push(new nlobjSearchColumn('enddate'));
		column.push(new nlobjSearchColumn('queue'));
		column.push(new nlobjSearchColumn('percentcomplete'));
		column.push(new nlobjSearchColumn('queueposition'));
		column.push(new nlobjSearchColumn('percentcomplete'));

		var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
	    
		if(_logValidation(a_search_results)) 
	    {
	    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
	    	
			for(var i=0;i<a_search_results.length;i++)
	    	{
	    		var s_script  = a_search_results[i].getValue('name', 'script');
				nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);

				var d_date_created  = a_search_results[i].getValue('datecreated');
				nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);

				var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
				nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);

				var s_script_status  = a_search_results[i].getValue('status');
				nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
			
			   i_percent_complete = i_percent_complete;
			
	    	}
	    }
	    else
	    {	    	
         i_percent_complete = '100%';            
	    }
		
	    if(_logValidation(i_recordID)) 
		{ 
			var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process',i_recordID)
			
			if(_logValidation(o_recordOBJ)) 
			{
				
				 var f_progress_bar =  form.addField('custpage_bar','inlinehtml','Progress Bar')
				 
				 var i_counter = parseInt(i_percent_complete);
	 	          
	     	    var strVar="";
				strVar += "<html>";
				strVar += "<body>";
				strVar += "";
				strVar += "";
				
				strVar += "<progress value=\""+i_counter+"\" max=\"100\">";
				strVar += "<\/progress>";
				
				strVar += "        <div id=\"my-progressbar-container\">";
				strVar += "            ";
				
				if(i_counter == 100)
				{
					strVar += "<div id=\"my-progressbar-text1\" class=\"progressbar-text top-left\">Complete...<\/div>";
				}	
				if(i_counter != 100)
				{
					strVar += "<div id=\"my-progressbar-text1\" class=\"progressbar-text top-left\">Loading...<\/div>";
				}			
				strVar += "            <div id=\"my-progressbar-text2\" class=\"progressbar-text top-right\">"+i_percent_complete+"<\/div>";
				strVar += "            ";
				strVar += "            <div id=\"my-progressbar\"><\/div>";
		
				strVar += "        <\/div>";
		
				strVar += "<\/body>";
				strVar += "<\/html>";
				strVar += "";

	   
	   f_progress_bar.setDefaultValue(strVar);				
				
				o_recordOBJ.setFieldValue('custrecord_percent_complete',i_percent_complete)
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Submit ID -->' + i_submitID);
	 
				
			}//Record OBJ				
		}//Record ID			
				
	}//Validate
*/
	
			
	return true;
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_salary_upload(type)
{ 
  nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Type  -->' + type);
			
  if(type == 'edit')
  {
	  try
	  {	  	
		var i_context = nlapiGetContext();
    
        var s_context_type = i_context.getExecutionContext();
        nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Context Type ' + s_context_type);
				
		if(s_context_type == 'userinterface')
		{		
	  	var i_recordID = nlapiGetRecordId()
		
		var s_record_type = nlapiGetRecordType();
		
		if(_logValidation(i_recordID)&& _logValidation(s_record_type))
		{
		  var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)	
		  
		  var o_old_recordOBJ = nlapiGetOldRecord();
		  
		  if(_logValidation(o_recordOBJ)&&_logValidation(o_old_recordOBJ))
		  {
		  	// ================ Current Record Values =======================
			
		  	var i_CSV_FileID = o_recordOBJ.getFieldValue('custrecord_csv_file_s_p');
			nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' CSV File ID -->' + i_CSV_FileID);
			 			
			// ================ Previous Record Values =======================
			
		  	var i_old_CSV_FileNo = o_old_recordOBJ.getFieldValue('custrecord_csv_file_id_no');
			nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Old CSV File No -->' + i_old_CSV_FileID);
			
			var i_old_CSV_FileID = o_old_recordOBJ.getFieldValue('custrecord_csv_file_s_p');
			nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Old CSV File ID -->' + i_old_CSV_FileID);
			
						
			if(!_logValidation(i_old_CSV_FileID)&&_logValidation(i_CSV_FileID))
			{
				o_recordOBJ.setFieldValue('custrecord_notes','');
				o_recordOBJ.setFieldValue('custrecord_file_status',4);
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
			    nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
		
				// ================= Call Schedule Script ================
				
				 var params=new Array();
			     params['custscript_record_id_err'] = i_submitID
				 params['custscript_csv_file_no_err'] = i_old_CSV_FileNo
					 
				 var status = nlapiScheduleScript('customscript_sch_salary_upload_delete_er',null,params);
				 nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Status -->' + status);
								
			}//New CSV File is Uploaded		
		  }//Record OBJ		
		}//Check
			
		}//Context Type
		  	
	  }//TRY
	  catch(exception)
	  {
		  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	  }//CATCH	
  }//CREATE
  return true;
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
