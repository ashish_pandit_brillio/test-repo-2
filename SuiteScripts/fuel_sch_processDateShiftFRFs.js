/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Sep 2019     Aazamali Khan
 *
 */
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function updateFRFsDate(type) {
//	try 
	{
		var context = nlapiGetContext();
		var opportunityId = context.getSetting("SCRIPT", "custscript_fuel_sch_oppid");
		nlapiLogExecution("DEBUG", "opportunityId:", opportunityId);
		var oppRec = nlapiLoadRecord("customrecord_sfdc_opportunity_record", opportunityId);
		
		var opp_name = oppRec.getFieldValue("custrecord_opportunity_name_sfdc");
		var opp_pm = oppRec.getFieldValue("custrecord_sfdc_opp_pm").split("");
		
		var opp_pm_id = [];
		var opp_pm_mail = [];
		
		
		var str = '';
		for(var i=0; i<opp_pm.length; i++){			
			// opp_pm_mail.push(nlapiLookupField('employee',opp_pm[i],'email'))
            			
			if((opp_pm[i].charCodeAt(0) >= 48 && opp_pm[i].charCodeAt(0) <= 57)){
				
				str+=opp_pm[i];
                     
                   if(i == (opp_pm.length-1)){
					   opp_pm_id.push(str);
					   str = '';
				   }					 
				
			}else{
				opp_pm_id.push(str);
				str = '';
			}				
			
		}
		
		opp_pm_id.forEach(function (emp_id){ opp_pm_mail.push(nlapiLookupField('employee',emp_id,'email'))})
		opp_pm_mail.push('business.ops@brillio.com')//Business Operations Team
		
		var ccMail = [];
		ccMail.push('fuel.support@brillio.com'); // fuel support team
					
		
		var reviseStartDate = oppRec.getFieldValue("custrecord_sfdc_opp_rec_rev_startdate");
		reviseStartDate = nlapiStringToDate(reviseStartDate); //nlapiDateToString(d, formattype)
		var reviseEndDate = oppRec.getFieldValue("custrecord_sfdc_opp_rec_rev_enddate");
		reviseEndDate = nlapiStringToDate(reviseEndDate);
		//nlapiLogExecution("ERROR", "reviseStartDate : ", reviseStartDate);
		//nlapiLogExecution("ERROR", "reviseEndDate : ", reviseEndDate);
		if (reviseStartDate || reviseEndDate) {
			var searchResult = getFRFs(opportunityId);
			//nlapiLogExecution("DEBUG", "search Result : ", JSON.stringify(searchResult));
			if (searchResult) {
				for (var i = 0; i < searchResult.length; i++) {
					var desired_date= searchResult[i].getValue("custrecord_frf_desired_start_date");
					var resourceStartDate = searchResult[i].getValue("custrecord_frf_details_start_date");
					resourceStartDate = nlapiStringToDate(resourceStartDate);
					var resourceEndDate = searchResult[i].getValue("custrecord_frf_details_end_date");
					resourceEndDate = nlapiStringToDate(resourceEndDate)
					//nlapiLogExecution("ERROR", "resourceStartDate : ", resourceStartDate);
					//nlapiLogExecution("ERROR", "resourceEndDate : ", resourceEndDate);
					var status = searchResult[i].getValue("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null);
					//nlapiLogExecution("ERROR", "status : ", status);
					var externalHire = searchResult[i].getValue("custrecord_frf_details_external_hire");
					//nlapiLogExecution("ERROR", "status : ", status);
					//nlapiLogExecution("ERROR", "externalHire : ", externalHire);
					var offerAccepted = searchResult[i].getValue("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null);
					offerAccepted = parseInt(offerAccepted);
					if (externalHire == "T" && status != "Filled" && offerAccepted == "0") {
						nlapiLogExecution("ERROR", "reviseStartDate",reviseStartDate);
						nlapiLogExecution("ERROR", "resourceStartDate: ",resourceStartDate);
						nlapiLogExecution("ERROR", "reviseEndDate: ",reviseEndDate);
						nlapiLogExecution("ERROR", "resourceEndDate: ",resourceEndDate);
						desired_date = nlapiStringToDate(desired_date);
						nlapiLogExecution("ERROR", "desired_date: ",desired_date);
						
						// when it is external hire then load record and submit record // Left shift
						if (reviseStartDate < resourceStartDate && reviseEndDate < resourceEndDate && resourceStartDate < resourceEndDate && reviseEndDate > reviseStartDate ) {
							//do something Update Done By Nihal
							nlapiLogExecution("ERROR", "Comes Inside First condition: ");
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							if(reviseStartDate >= resourceStartDate)
							{
								frfObj.setFieldValue("custrecord_frf_details_start_date", reviseStartDate);
								frfObj.setFieldValue("custrecord_frf_details_ori_start_date", resourceStartDate);
							}
							frfObj.setFieldValue("custrecord_frf_details_end_date", reviseEndDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
							if(reviseStartDate >= desired_date)
							{
								frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
								//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
							}
							nlapiSubmitRecord(frfObj);
						}
						// Only Desired Date Update
							if (reviseStartDate < resourceStartDate && reviseEndDate > resourceEndDate && resourceStartDate < resourceEndDate && reviseEndDate > reviseStartDate) {
							//do something Update Done By Nihal
							nlapiLogExecution("ERROR", "Comes Inside First condition: ");
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());

							if(reviseStartDate >= desired_date)
							{
								frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
								//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
							}
							nlapiSubmitRecord(frfObj);
						}
						if (reviseStartDate > resourceStartDate && reviseEndDate > resourceEndDate && resourceEndDate > reviseStartDate && reviseEndDate > reviseStartDate) {
							//do something  Update Done By Nihal
							nlapiLogExecution("ERROR", "Comes Inside Second condition: ");
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							frfObj.setFieldValue("custrecord_frf_details_start_date", reviseStartDate);
							if(reviseEndDate<=resourceEndDate){
								frfObj.setFieldValue("custrecord_frf_details_end_date", reviseEndDate);
								frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
							}
							frfObj.setFieldValue("custrecord_frf_details_ori_start_date", resourceStartDate);
							if(reviseStartDate >= desired_date)
							{
								frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
								//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
							}
							nlapiSubmitRecord(frfObj);
						}
						// left shift of start date : Update Done By Nihal
						if (reviseStartDate<resourceStartDate  && resourceEndDate > reviseStartDate && reviseEndDate < resourceEndDate && reviseEndDate > reviseStartDate) {
							nlapiLogExecution("ERROR", "Comes Inside First condition: Left shift");
							//update resource end date with revise end date 
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							frfObj.setFieldValue("custrecord_frf_details_ori_end_date", reviseEndDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
							/*frfObj.setFieldValue("custrecord_frf_details_open_close_status", "1");
							frfObj.setFieldValue("custrecord_frf_details_status_flag", "1");
							frfObj.setFieldValue("custrecord_frf_details_selected_emp", "");*/
							nlapiSubmitRecord(frfObj);
						}
					/*	// left shift 
						if (resourceStartDate > reviseStartDate && resourceEndDate > reviseEndDate && resourceStartDate < reviseEndDate && reviseEndDate > reviseStartDate) {
							nlapiLogExecution("ERROR", "Comes Inside second condition: Left shift");
							nlapiLogExecution("ERROR", "left shift : ", "updated the end date");
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							frfObj.setFieldValue("custrecord_frf_details_end_date", reviseEndDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
							/*frfObj.setFieldValue("custrecord_frf_details_open_close_status", "1");
							frfObj.setFieldValue("custrecord_frf_details_status_flag", "1");
							frfObj.setFieldValue("custrecord_frf_details_selected_emp", "");*/
					/*		nlapiSubmitRecord(frfObj);
						}*/
						// shrink
						if (resourceStartDate < reviseStartDate && reviseEndDate < resourceEndDate && resourceStartDate < reviseEndDate && resourceEndDate > reviseStartDate) {
							nlapiLogExecution("ERROR", "Comes Inside First condition: shrink");
							nlapiLogExecution("ERROR", "shrink : ", "start and end dates are at shrink to existing dates");
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							frfObj.setFieldValue("custrecord_frf_details_start_date", reviseStartDate);
							frfObj.setFieldValue("custrecord_frf_details_end_date", reviseEndDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_start_date", resourceStartDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
							frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
							//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
							nlapiSubmitRecord(frfObj);
						}
					}
					if (externalHire == "F") {
						desired_date = nlapiStringToDate(desired_date);
						nlapiLogExecution("ERROR", "desired_date: externalHire F",desired_date);
						
						
						var softlock_resource = searchResult[i].getValue("custrecord_frf_details_selected_emp");
						var softlock_resource_name = searchResult[i].getText("custrecord_frf_details_selected_emp");
						
						
						var frf_creater = searchResult[i].getText("CUSTRECORD_FRF_DETAILS_CREATED_BY"); 
						var frf_creater_mail = searchResult[i].getValue("email","CUSTRECORD_FRF_DETAILS_CREATED_BY",null); 
						//var frf_created_by = searchResult[i].getValue("custrecord_frf_details_created_by");
						//var frf_creater_mail = nlapiLookupField('employee',frf_created_by,'email')
						var receiver_mail = [];
						receiver_mail = opp_pm_mail;
						
						var softlock_by = searchResult[i].getValue("custrecord_frf_details_softlocked_by");
						
						
						if(softlock_by){
							var softlock_by_email = nlapiLookupField('employee',softlock_by,'email')
							receiver_mail.push(softlock_by_email);
						}
						
						if(frf_creater_mail){
							receiver_mail.push(frf_creater_mail);
						}
						
						if(softlock_resource){
							
							   var frf_no =  searchResult[i].getValue("custrecord_frf_details_frf_number");
								 var frf_location =  searchResult[i].getText("custrecord_frf_details_res_location");
								  var billable =  searchResult[i].getValue("custrecord_frf_details_billiable");
									var bill_rate = searchResult[i].getValue("custrecord_frf_details_bill_rate");
									var allocation_percentage =  searchResult[i].getValue("custrecord_frf_details_allocation");
									
									billable = billable == "T" ? "Billable" : "Non-Billable";
									
									
								 
						}
						
						if (reviseStartDate < resourceStartDate && reviseEndDate < resourceEndDate && resourceStartDate >reviseEndDate && reviseEndDate > reviseStartDate) {
							//do something
							nlapiLogExecution("ERROR", "far left : ", "start and end dates are at left to existing dates");
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_start_date", reviseStartDate);
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_end_date", reviseEndDate);
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_ori_start_date", resourceStartDate);
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_ori_end_date", resourceEndDate);
							
							var allocation_flag = false;
							if(softlock_resource){
							
							allocation_flag = check_availability(softlock_resource,desired_date,reviseEndDate,reviseStartDate,frf_no,context);
							
							}
							
							if(allocation_flag){
								nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_selected_emp", "");
								nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_status_flag", "1");
								nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_open_close_status", "1");
							}else if(softlock_resource){								
								
									  
									   revisedSoftlockEmail(frf_no,frf_creater,softlock_resource_name,opp_name,desired_date,resourceEndDate,frf_location,billable,bill_rate,allocation_percentage,receiver_mail,ccMail);  
								
							}
							var recObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							nlapiSubmitRecord(recObj);
						}
						if (reviseStartDate > resourceStartDate && reviseEndDate > resourceEndDate && resourceEndDate < reviseStartDate && reviseEndDate > reviseStartDate) {
							nlapiLogExecution("ERROR", "far right : ", "start and end dates are at right to existing dates");
							//do something 
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_start_date", reviseStartDate);
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_end_date", reviseEndDate);
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_ori_start_date", resourceStartDate);
							nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_ori_end_date", resourceEndDate);
							
							var allocation_flag = false;
							if(softlock_resource){
							
							 allocation_flag = check_availability(softlock_resource,desired_date,reviseEndDate,reviseStartDate,frf_no,context);
							
							}
							
							
							if(allocation_flag){
								nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_selected_emp", "");
								nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_status_flag", "1");
								nlapiSubmitField("customrecord_frf_details", searchResult[i].getId(), "custrecord_frf_details_open_close_status", "1");
							}else if(softlock_resource){								
								
									  
									   revisedSoftlockEmail(frf_no,frf_creater,softlock_resource_name,opp_name,desired_date,reviseEndDate,frf_location,billable,bill_rate,allocation_percentage,receiver_mail,ccMail);  
								
							}
							
							
							var recObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							nlapiSubmitRecord(recObj);
						}
						// Right shift of start date and desired date update.
						if (resourceStartDate < reviseStartDate && resourceEndDate > reviseStartDate && reviseEndDate >= resourceEndDate && reviseEndDate > reviseStartDate) {
							//update resource end date with revise end date 
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							var capture_desire_date = reviseStartDate;
							if(reviseStartDate > desired_date)
							{
								frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
								//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
							}else{
								capture_desire_date = desired_date;
								
							}							
							
							nlapiLogExecution("ERROR", "right shift : ", "updated the start date");
							frfObj.setFieldValue("custrecord_frf_details_start_date", reviseStartDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_start_date", resourceStartDate);
							
							
							var allocation_flag = false;
							if(softlock_resource){
							
							 allocation_flag = check_availability(softlock_resource,capture_desire_date,resourceEndDate,reviseStartDate,frf_no,context);
							
							}
							
							if(allocation_flag){
								frfObj.setFieldValue("custrecord_frf_details_selected_emp", "");
								frfObj.setFieldValue("custrecord_frf_details_status_flag", "1");
								frfObj.setFieldValue("custrecord_frf_details_open_close_status", "1");
							}else if(softlock_resource){								
								
									  
									   revisedSoftlockEmail(frf_no,frf_creater,softlock_resource_name,opp_name,capture_desire_date,resourceEndDate,frf_location,billable,bill_rate,allocation_percentage,receiver_mail,ccMail);  
								
							}
							
							nlapiSubmitRecord(frfObj);
						}
						// Right shift of desired date update.
						if (resourceStartDate > reviseStartDate && resourceEndDate > reviseStartDate && reviseEndDate >= resourceEndDate && reviseEndDate > reviseStartDate) {
							//update resource end date with revise end date 
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							var capture_desire_date = reviseStartDate;
							
							if(reviseStartDate > desired_date)
							{
								frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
								//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
							}else{
								capture_desire_date = desired_date;
								
							}
							
							var allocation_flag = false;
							if(softlock_resource){
							
							 allocation_flag = check_availability(softlock_resource,capture_desire_date,resourceEndDate,resourceStartDate,frf_no,context);
							
							}
							
							if(allocation_flag){
								frfObj.setFieldValue("custrecord_frf_details_selected_emp", "");
								frfObj.setFieldValue("custrecord_frf_details_status_flag", "1");
								frfObj.setFieldValue("custrecord_frf_details_open_close_status", "1");
							}else if((softlock_resource) && (reviseStartDate > desired_date)){								
								
									  
									   revisedSoftlockEmail(frf_no,frf_creater,softlock_resource_name,opp_name,capture_desire_date,resourceEndDate,frf_location,billable,bill_rate,allocation_percentage,receiver_mail,ccMail);  
								
							}
							
							nlapiSubmitRecord(frfObj);
						}
						// left shift 
						if (resourceStartDate >= reviseStartDate && resourceEndDate > reviseEndDate && resourceStartDate < reviseEndDate && reviseEndDate > reviseStartDate) {
							
								var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
								var capture_desire_date = reviseStartDate;
								if(reviseStartDate > desired_date)
								{
									frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
									//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
								}else{
								capture_desire_date = desired_date;
								
							    }
								frfObj.setFieldValue("custrecord_frf_details_end_date", reviseEndDate);
								frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
								
								var allocation_flag = false;
							if(softlock_resource){
							
							 allocation_flag = check_availability(softlock_resource,capture_desire_date,reviseEndDate,reviseStartDate,frf_no,context);
							
							}
							
                            if(allocation_flag){							
								frfObj.setFieldValue("custrecord_frf_details_selected_emp", "");
								frfObj.setFieldValue("custrecord_frf_details_status_flag", "1");
								frfObj.setFieldValue("custrecord_frf_details_open_close_status", "1");
							}else if(softlock_resource){								
								
									  
									   revisedSoftlockEmail(frf_no,frf_creater,softlock_resource_name,opp_name,capture_desire_date,reviseEndDate,frf_location,billable,bill_rate,allocation_percentage,receiver_mail,ccMail);  
								
							}
							
							nlapiSubmitRecord(frfObj);
						
						}
						// shrink 
						if (resourceStartDate < reviseStartDate && reviseEndDate < resourceEndDate && resourceStartDate < reviseEndDate && resourceEndDate > reviseStartDate) {
							
							var frfObj = nlapiLoadRecord("customrecord_frf_details", searchResult[i].getId());
							var capture_desire_date = reviseStartDate;
								if(reviseStartDate > desired_date)
								{
									frfObj.setFieldValue("custrecord_frf_desired_start_date", reviseStartDate);
									//frfObj.setFieldValue("custrecord_desire_old_date", desired_date);
								}else{
								capture_desire_date = desired_date;
								
							    }
							frfObj.setFieldValue("custrecord_frf_details_start_date", reviseStartDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_start_date", resourceStartDate);
							frfObj.setFieldValue("custrecord_frf_details_end_date", reviseEndDate);
							frfObj.setFieldValue("custrecord_frf_details_ori_end_date", resourceEndDate);
							
							var allocation_flag = false;
							if(softlock_resource){
							
							 allocation_flag = check_availability(softlock_resource,capture_desire_date,reviseEndDate,reviseStartDate,frf_no,context);
							
							}
							
							if(allocation_flag){
								frfObj.setFieldValue("custrecord_frf_details_selected_emp", "");
								frfObj.setFieldValue("custrecord_frf_details_status_flag", "1");
								frfObj.setFieldValue("custrecord_frf_details_open_close_status", "1");		
							}else if(softlock_resource){								
								
									  
									   revisedSoftlockEmail(frf_no,frf_creater,softlock_resource_name,opp_name,capture_desire_date,reviseEndDate,frf_location,billable,bill_rate,allocation_percentage,receiver_mail,ccMail);  
								
							}
							
							nlapiSubmitRecord(frfObj);
							
						}
					}
				}
			}
		}
	} 
/*	catch (e) {
		// TODO: handle exception
	}*/
}

 //NS-1243 
function getFRFs(oppId) {
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
		[
			["custrecord_frf_details_opp_id", "anyof", oppId], "AND", ["custrecord_frf_details_status", "is", "F"], "AND",
			["custrecord_frf_details_status_flag", "noneof", "2", "4","5"]                                                              
		],
		[
		     new nlobjSearchColumn("custrecord_frf_details_frf_number"),
			new nlobjSearchColumn("custrecord_frf_details_start_date"),
			new nlobjSearchColumn("custrecord_frf_details_end_date"),
			new nlobjSearchColumn("custrecord_frf_details_external_hire"),
			new nlobjSearchColumn("CUSTRECORD_FRF_DETAILS_CREATED_BY"),
			new nlobjSearchColumn("email","CUSTRECORD_FRF_DETAILS_CREATED_BY",null),
			new nlobjSearchColumn("custrecord_frf_details_res_location"), 
			new nlobjSearchColumn("custrecord_frf_details_billiable"), 
			new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
			new nlobjSearchColumn("custrecord_frf_details_allocation"), 
			new nlobjSearchColumn("custrecord_frf_details_project"),
			new nlobjSearchColumn("custrecord_frf_desired_start_date"),
			new nlobjSearchColumn("custrecord_frf_details_selected_emp"),
			new nlobjSearchColumn("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
			new nlobjSearchColumn("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
		]);
	return customrecord_frf_detailsSearch;
}

function getResourceFrf(emp_id,start_date,end_date,resource_start_date){
	
	
	
	if(start_date){
		var filters = new Array();
		
		filters = [
					["custrecord_frf_details_selected_emp", "anyof", emp_id], "AND", ["custrecord_frf_details_status", "is", "F"], "AND",
					["custrecord_frf_details_status_flag", "noneof", "2", "4","5"], "AND" , ["custrecord_frf_desired_start_date","notonorafter",start_date], "AND" , 
					["custrecord_frf_details_end_date","notonorbefore",end_date]                                                             
		          ]
		
	}else{
		var filters = new Array();
		filters = [
					["custrecord_frf_details_selected_emp", "anyof", emp_id], "AND", ["custrecord_frf_details_status", "is", "F"], "AND",
					["custrecord_frf_details_status_flag", "noneof", "2", "4","5"], "AND" , ["custrecord_frf_details_start_date","notonorafter",resource_start_date], "AND" , 
					["custrecord_frf_details_end_date","notonorbefore",end_date]                                                             
		          ]
		
	}
	
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details", null,
		filters,
		[
			new nlobjSearchColumn("custrecord_frf_details_start_date"),
			new nlobjSearchColumn("custrecord_frf_details_end_date"),
			new nlobjSearchColumn("custrecord_frf_details_external_hire"),
			new nlobjSearchColumn("custrecord_frf_desired_start_date"),
			new nlobjSearchColumn("custrecord_frf_details_frf_number"),
			new nlobjSearchColumn("custrecord_taleo_ext_hire_status", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null),
			new nlobjSearchColumn("custrecord_taleo_offer_accepted", "CUSTRECORD_FRF_DETAILS_RRF_NUMBER", null)
		]);
	return customrecord_frf_detailsSearch;
	
}

function gerResourceAllocation(emp_id,start_date,end_date,resource_start_date){
	
	if(start_date){
		var filters = new Array();
		
		filters = [
						 ["employee.custentity_employee_inactive","is","F"], 
						 "AND", 
						 ["employee.custentity_implementationteam","is","F"],				 
						  "AND", 
						   ["startdate","notonorafter",start_date], 
						   "AND", 
						   ["enddate","notonorbefore",end_date],
						   "AND", 
						   ["resource","anyof",emp_id],
						   "AND",
						   ["job.custentity_project_allocation_category","noneof","4"],
														   
			      ]
		
	}else{
		var filters = new Array();
		
		filters = [
						 ["employee.custentity_employee_inactive","is","F"], 
						 "AND", 
						 ["employee.custentity_implementationteam","is","F"],				 
						  "AND", 
						   ["startdate","notonorafter",resource_start_date], 
						   "AND", 
						   ["enddate","notonorbefore",end_date],
						   "AND", 
						   ["resource","anyof",emp_id],
						   "AND",
						   ["job.custentity_project_allocation_category","noneof","4"],
														   
			      ]
		
	}
	
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
														filters, 
														[
														   new nlobjSearchColumn("internalid","employee",null), 
														  
														   new nlobjSearchColumn("resource"), 
														   new nlobjSearchColumn("employeestatus","employee",null), 
														   new nlobjSearchColumn("entityid","job",null).setSort(false), 
														   new nlobjSearchColumn("custentity_project_allocation_category","job",null),											  
														   new nlobjSearchColumn("startdate"), 
														   new nlobjSearchColumn("enddate"), 														  
														   new nlobjSearchColumn("company"), 
														  
														]
													);
	
	return resourceallocationSearch;
	
}


function revisedSoftlockEmail(frf_no,frf_creater,emp_name,opp_name,start_date,end_date,frf_location,billable,bill_rate,allocation_percentage,email,ccEmail){


              var startDate = nlapiDateToString(start_date);
			  var endDate = nlapiDateToString(end_date);
			var subject ='FRF Number: '+ frf_no + ' FUEL Notification: FRF start/end date updated, soft locking extended ' ;	
							
			var body = 'Dear ' + frf_creater + ', Business Operations Team. <br />' + frf_no +' has been modified as the start/end date of ' + opp_name +'  which is raised against has changed.<br />The soft locking of the employee has also been extended. Please review the changes and do needful.<br /><br />Details for Business Operations Team:<br /><br />Resource Name: '+ emp_name + '<br />Opportunity Name: '+  opp_name + '<br />Start date: '+ startDate + '<br />End date: '+ endDate + '<br />Percentage of Time: '+ allocation_percentage + '<br />Billing start date: '+ startDate + '<br />Billing end date: '+ endDate + '<br />Location: '+ frf_location + '<br />Bill rates: ' + bill_rate + '<br />Billable Status: '+ billable + '<br /><br />Brillio-FUEL<br />This email was sent from a notification only address.<br />If you need further assistance, please write to fuel.support@brillio.com';						
							
							
						var records = new Array();
							records['entity'] = 140512;
							
							var emailString = email.toString();
							
							nlapiLogExecution('Debug','Email Sent to ->',emailString);
							 nlapiSendEmail(154256, emailString, subject,body, ccEmail, "prabhat.gupta@brillio.com", records);
					
							nlapiLogExecution('Debug','Email Sent to ->',email);
	
	
	
}

function check_availability(emp_id,start_date,end_date,resource_start_date,frf_no,context){
	
	var allocation_flag = false;
	 var resource_frf_search = getResourceFrf(emp_id,start_date,end_date,resource_start_date);					
								
		yieldScript(context);
		if(resource_frf_search){
			
			for(var i=0; i<resource_frf_search.length; i++){
				
				if(resource_frf_search[i].getValue('custrecord_frf_details_frf_number') == frf_no){
					
					return allocation_flag;
				}
			}
			allocation_flag = true;			
			
			
		}else{
			
			var resource_allocation_search = gerResourceAllocation(emp_id,start_date,end_date,resource_start_date);
			
			if(resource_allocation_search){
				
				allocation_flag = true;
				
			}
		}
		
		return allocation_flag;
}

function yieldScript(currentContext) {

		if (currentContext.getRemainingUsage() <= 100) {
			nlapiLogExecution('AUDIT', 'API Limit Exceeded');
			var state = nlapiYieldScript();

			if (state.status == "FAILURE") {
				nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
						+ state.reason + ' / Size : ' + state.size);
				return false;
			} else if (state.status == "RESUME") {
				nlapiLogExecution('AUDIT', 'Script Resumed');
			}
		}
	}