function myFieldChange(type, name)
{
	if(name == 'custpage_family'){
	//nlapiSetFieldValue('custpage_field1','');
	var listVal = nlapiGetFieldValue('custpage_family');
	if(listVal == ''){
	nlapiRemoveSelectOption('custpage_primary_skill');
	}
	var user=nlapiGetUser();
	
	//nlapiRemoveSelectOption('custpage_primary_skill');
	
	var primary_selected=nlapiGetFieldValues('custpage_family');
	var valu=nlapiGetFieldValues('custpage_primary_skill');	
	if(primary_selected){
	for(var k=0;k<primary_selected.length;k++){
	var project_list=getSkills(primary_selected[k]);
	for(var i=0; i< project_list.length;i++)
	{
		//nlapiSetFieldValue('custpage_field1',project_list[i].getValue('internalid'));
	nlapiInsertSelectOption('custpage_primary_skill', project_list[i].getValue('custrecord_emp_primary_skill'), project_list[i].getText('custrecord_emp_primary_skill'), false);
	}
	}
	}
	}
	if(name == 'custpage_primary_skill')
	{
		var valu=nlapiGetFieldValues('custpage_primary_skill');	
		if(valu.length >3){
		alert('Only 3 Primary skills allowed'); 
		return false;
		}
	}
	if(name == 'custpage_family')
	{
		var valu_=nlapiGetFieldValues('custpage_family');	
		if(valu_.length >2){
		alert('Only 2 Family allowed'); 
		nlapiSetFieldValue('custpage_family','');
		return false;
		}
	}
	/*if(name =='custpage_field1'){
		var selected=nlapiGetFieldValues('custpage_field1');
		var arr=[];
		
		nlapiSetFieldValue('custpage_projects',selected);
		
	}*/
}
function onSave()
{
	var s_request_type = nlapiGetFieldValue('share_type');
	if(s_request_type == 'EmployeeView')
	{
		var employee_name = nlapiGetFieldValue('custpage_employee');
		var emp_id= nlapiGetUser();
		//var emp_id = nlapiGetFieldValue('emp_id');
		var family = nlapiGetFieldValues('custpage_family');
		if(!family)
		{	
			alert('please select family');
			return false;
		}
		var primary_skill = nlapiGetFieldValues('custpage_primary_skill');
		if(primary_skill.length == 0)
		{	
			alert('please select Primary Skill');
			return false;
		}
		else if(primary_skill.length > 3)
		{
			alert('Only 3 Primary skills allowed'); 
			return false;
		}
		
		var secondary_skill = nlapiGetFieldValue('custpage_secondary_skill');
		//var certificate = nlapiGetFieldValue('custpage_certificate');
		//var cstart_date = nlapiGetFieldValue('custpage_cstart_date');
		var reporting_manager = nlapiGetFieldValue('reprtng_id');
		//if(reporting_manager){
		/*var emp_lookUP = nlapiLookupField('employee',parseInt(reporting_manager),['firstname','email']);
		var reporting_manager_name = emp_lookUP.firstname;
		}
		if(reporting_manager_name)
		var name = reporting_manager_name;
		else
		var name = 'RM';*/
		
		var approver_mail = nlapiGetFieldValue('reprtng_mangr');
		var skill_record = nlapiCreateRecord('customrecord_employee_master_skill_data');
		skill_record.setFieldValue('custrecord_employee_skill_updated',emp_id);
		skill_record.setFieldValues('custrecord_family_selected',family);
		skill_record.setFieldValue('custrecord_secondry_updated',secondary_skill);
		skill_record.setFieldValues('custrecord_primary_updated',primary_skill);
		skill_record.setFieldValue('custrecord_skill_status',1);
		skill_record.setFieldValue('custrecord_employee_approver',reporting_manager);
		var id_skill = nlapiSubmitRecord(skill_record,true,true);
		var certificate_lne = nlapiGetLineItemCount('certificate_sublist');
		for (var i_index_ = 1; i_index_ <= certificate_lne; i_index_++) // iterate through sublist
		{
			var certificate_parent = nlapiGetLineItemValue('certificate_sublist', 'custpage_certificate', i_index_);
			var c_startdate = nlapiGetLineItemValue('certificate_sublist', 'custpage_cstart_date', i_index_);
			var c_enddate = nlapiGetLineItemValue('certificate_sublist', 'custpage_cend_date', i_index_);
			var certificate_record = nlapiCreateRecord('customrecord_emp_certificate_detail');
			certificate_record.setFieldValue('custrecord_certifcate_det', (certificate_parent));
			certificate_record.setFieldValue('custrecord_certificate_st_date', c_startdate);
			certificate_record.setFieldValue('custrecord_certificate_enddate', c_enddate);
			certificate_record.setFieldValue('custrecord_emp_skill_parent', id_skill);
			var id_rev_share = nlapiSubmitRecord(certificate_record);
					
		}
		 //nlapiSubmitField('employee',emp_id, 'custentity_skill_updated', 'T');
		// mail to BO team and participating practice head
	/*	var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
			
		strVar += '<p>Dear '+name+',</p>';
		strVar += '<p>'+employee_name+' has updated the skill field on the tool. Request you to kindly validate the same via accessing below mentioned link.</p>';
		strVar += '<p>This activity will help in achieving following objectives:</p>';
			
		strVar += '<p>•	Determining Learning Plans for Brillians basis validated skills<br>';
		strVar += '   •	Redeployment of Brillian across Projects</p>';
		/*strVar += 'Customer:- '+s_project_cust+'<br>';
		strVar += 'Project:- '+s_project_name+'<br>';
		strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
		strVar += 'Executing Practice:- '+s_project_prac+'</p>';
			
		strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1664&deploy=1>Link to Approve Skill Record in Netsuite</a>';
			
		strVar += '<p>Regards,<br>';
		strVar += 'Fulfilment Team</p>';
				
		strVar += '</body>';
		strVar += '</html>';*/
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '7905';
		//	var file_obj = nlapiLoadFile('857977');
		//	nlapiSendEmail(129799, approver_mail, 'Employee Skill Updated: '+employee_name+'.', strVar,null,'deepak.srinivas@brillio.com',a_emp_attachment,file_obj);
			
	}

	return true;
}
 function getSkills(primary_selected) {
	try {
			var skill_filter = 
		        [
		            [ 'custrecord_employee_skil_family', 'anyof', primary_selected ],               
		              'and',
		        [ 'isinactive', 'is', 'F' ] ];

		var skill_search_results = searchRecord('customrecord_employee_skills', null, skill_filter,
		        [ new nlobjSearchColumn("custrecord_emp_primary_skill"),
		               		new nlobjSearchColumn("internalid")]);

		nlapiLogExecution('debug', 'Results count',
		        skill_search_results.length);

		if (skill_search_results.length == 0) {
			throw "You don't have any Primary Skills under Family Selected.";
		}
	
		return skill_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
		
	
}
function validate_line(type)
{
	if(type == 'certificate_sublist')
	{
	var c_strt_date = nlapiGetCurrentLineItemValue('certificate_sublist', 'custpage_cstart_date');
	c_strt_date = nlapiStringToDate(c_strt_date);
	var c_end_date = nlapiGetCurrentLineItemValue('certificate_sublist', 'custpage_cend_date');
	c_end_date = nlapiStringToDate(c_end_date);
	var certificate = nlapiGetCurrentLineItemValue('certificate_sublist','custpage_certificate');
	
	if(c_strt_date > c_end_date)
	{
		alert('Certification end date cannot be less than Certification start date');
		return false;
	}
	if(!certificate)
	{
		alert('Please select Certification');
		return false;
	}
	/*if(!c_strt_date)
	{
		alert('Please select Certification');
		return false;
	}
	if(!c_end_date)
	{
		alert('Please select Certification');
		return false;
	}*/
	
	}
	return true;
}
function approveInvoices(url)
{

	//
		  var linecount=nlapiGetLineItemCount('custpage_skill_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=nlapiGetLineItemValue('custpage_skill_list','select',i);
			var invoice=nlapiGetLineItemValue('custpage_skill_list','skillid',i);
			
			if(linevalue== 'T')
			{
				var recid = nlapiGetLineItemValue('custpage_skill_list','internalid', i);
				var rec=nlapiSubmitField('customrecord_employee_master_skill_data',recid,['custrecord_skill_status'],['2']);
		
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
			}
		}
  //
	//var response =nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_skill_approval', 'customdeploy1'));
 window.open(url,'_self');
}
function rejectInvoices(url)
{

	//
		  var linecount=nlapiGetLineItemCount('custpage_skill_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=nlapiGetLineItemValue('custpage_skill_list','select',i);
			var invoice=nlapiGetLineItemValue('custpage_skill_list','skillid',i);
			//var emp = nlapiGetLineItemValue('skillid');
			if(linevalue== 'T')
			{
				var recid = nlapiGetLineItemValue('custpage_skill_list','internalid', i);
              var emp=nlapiGetLineItemValue('custpage_skill_list','skillid',i);
				var rec=nlapiSubmitField('customrecord_employee_master_skill_data',recid,['custrecord_skill_status'],['3']);
            //  var rec=nlapiSubmitField('employee',emp,'custentity_skill_updated','F');
		
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
			}
		}
  //
window.open(url,'_self');
}