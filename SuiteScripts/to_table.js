/**
 * toTable.js
 * @NApiVersion 2.x
 */
 define(function(){

  function toTable (data) {

      
        var headers
    
        function getBody() {
            var tpl = "";
            if (Array.isArray(data)) {
                for (var i = 0; i < data.length; i++) {
                    try {
                        tpl += "<tr>" + generateTd(data[i]) + "</tr>";
                    } catch (e) {
                    }
                }
            } else {
                tpl += "<tr>" + generateTd(data) + "</tr>"
            }
            return tpl;
        }
    
        function getHeader() {
            if (Array.isArray(data)) {
                var objectkeys = data.map(function (res, index) {
                    return {
                        index: index,
                        keycount: Object.keys(res)
                    };
                });
    
                var sortedkeys = objectkeys.sort(function (a, b) {
                    return b.keycount - a.keycount;
                });
    
                var headerrow_idx = sortedkeys[0].index;
    
                headers = Object.keys(data[headerrow_idx])
    
                return "<tr>" + generateTh(headers) + "</tr>"
    
                //it is array
            } else {
                //it is object
                headers = Object.keys(data)
                return "<tr>" + generateTh(headers) + "</tr>"
            }
        }
    
        function generateTh() {
            return headers.map(function (c) {
                return "<th>" + c.toUpperCase() + "</th>"
            }).join("");
        }
    
        function generateTd(obj) {
            return Object.keys(obj).map(function (c) {
                return "<td>" + obj[c] + "</td>"
            }).join("");
        };

        if(data.length>0)
        {
            var tpl = "<thead>" + getHeader() + "</thead><tbody>" + getBody() + "</tbody>"

            return tpl;
        }

        return "";
    }

    function merge(tpl,data)
    {
        for(var key in data)
        {
            var replacer = "{{"+key+"}}";
            var reg = new RegExp(replacer,'g')

            tpl = tpl.replace(reg,data[key])            
        }
        return tpl;
    }
    function getLines(rec,sublist,cols)
    {
        var result = [];
        
        var lineCount = rec.getLineCount(sublist);
        
        log.debug("linecount",lineCount);

        if(lineCount>0)
        {

            for(var i=0;i<lineCount;i++)
            {
                var row = {};
                for(var j=0;j<cols.length;j++)
                {
                    row[cols[j]] = rec.getSublistValue({                   
                        fieldId:cols[j],
                        sublistId:sublist,
                        line:i
                    })||""
    
                    // var txt = cols[j] + "_txt";
    
                    // row[txt] = rec.getSublistText({                   
                    //     fieldId:cols[j],
                    //     sublistId:sublist,
                    //     line:i
                    // });
    
                }
    
                result.push(row)
            }
        }


        return result;
    }

    return {
        render: toTable,
        merge:merge,
        getLines:getLines
    }
});

