/**
 * RESTlet to get purchase request for approval
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var requestType = dataIn.RequestType;
		var purchaseId = dataIn.Data.PurchaseId;

		switch (requestType) {

			case M_Constants.Request.Get:

				if (purchaseId) {
					response.Data = getPRDetail(purchaseId, employeeId);
					response.Status = true;
				} else {
					response.Data = getPRPendingApproval(employeeId);
					response.Status = true;
				}
			break;

			case M_Constants.Request.Approve:
				response.Data = approvePR(purchaseId, employeeId);
				response.Status = true;
			break;

			case M_Constants.Request.Reject:
				response.Data = rejectPR(purchaseId, employeeId);
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));
	return response;
}

function getPRDetail(prId, approverId) {
	try {
		var prItemRecord = nlapiLoadRecord('customrecord_pritem', prId);
		var prStatus = prItemRecord.getFieldValue('custrecord_prastatus');

		/*
		 * var allowAccess = (prStatus == '2' && prItemRecord
		 * .getFieldValue('custrecord_subpracticehead') == approverId) ||
		 * (prStatus == '3' && prItemRecord
		 * .getFieldValue('custrecord_itforpurchaserequest') == approverId) ||
		 * (prStatus == '4' && prItemRecord
		 * .getFieldValue('custrecord_procurementteam') == approverId) ||
		 * (prStatus == '30' && prItemRecord
		 * .getFieldValue('custrecord_pri_delivery_manager') == approverId) ||
		 * (prStatus == '24' && prItemRecord
		 * .getFieldValue('custrecord_pri_finance_approver') == approverId);
		 */
		var allowAccess = (prStatus == '2' && prItemRecord
		        .getFieldValue('custrecord_subpracticehead') == approverId)
		        || (prStatus == '30' && prItemRecord
		                .getFieldValue('custrecord_pri_delivery_manager') == approverId);

		if (allowAccess) {
			var prItem = {};
			prItem.InternalId = prItemRecord.getId();
			prItem.EmployeeName = prItemRecord
			        .getFieldText('custrecord_employee');
			prItem.PrNo = prItemRecord
			        .getFieldText('custrecord_purchaserequest');
			prItem.StatusText = prItemRecord
			        .getFieldText('custrecord_prastatus');
			prItem.ItemName = prItemRecord
			        .getFieldText('custrecord_prmatgrpcategory');
			prItem.Description = prItemRecord
			        .getFieldValue('custrecord_prmatldescription');
			prItem.ItemCategoryText = prItemRecord
			        .getFieldText('custrecord_itemcategory');
			prItem.Quantity = prItemRecord.getFieldValue('custrecord_quantity');
			prItem.UnitName = prItemRecord.getFieldText('custrecord_unit');
			prItem.CurrencyText = prItemRecord
			        .getFieldText('custrecord_currencyforpr');
			prItem.UnitCost = prItemRecord
			        .getFieldValue('custrecord_valueperunit');
			prItem.TotalCost = prItemRecord
			        .getFieldValue('custrecord_totalvalue');
			prItem.ProjectName = prItemRecord
			        .getFieldText('custrecord_project');
			return prItem;
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('error', 'getPRDetail', err);
		throw err;
	}
}

function getPRPendingApproval(approverId) {
	try {
		var filters = [
		        [ [ 'custrecord_prastatus', 'anyof', '2' ], 'and',
		                [ 'custrecord_subpracticehead', 'anyof', approverId ] ],
		        'or',
		        [
		                [ 'custrecord_prastatus', 'anyof', '30' ],
		                'and',
		                [ 'custrecord_pri_delivery_manager', 'anyof',
		                        approverId ] ] ];

		var columns = [ new nlobjSearchColumn('custrecord_purchaserequest'),
		        new nlobjSearchColumn('custrecord_employee'),
		        new nlobjSearchColumn('custrecord_prmatgrpcategory'),
		        new nlobjSearchColumn('custrecord_currencyforpr'),
		        new nlobjSearchColumn('custrecord_totalvalue'),
		        new nlobjSearchColumn('custrecord_prastatus') ];

		var pendingPrSearch = nlapiSearchRecord('customrecord_pritem', null,
		        filters, columns);

		var prList = [];

		if (pendingPrSearch) {
			for (var i = 0; i < pendingPrSearch.length && i < 50; i++) {
				var prItem = new PuchaseRequestListItem();
				prItem.InternalId = pendingPrSearch[i].getId();
				prItem.PrNo = pendingPrSearch[i]
				        .getText('custrecord_purchaserequest');
				prItem.EmployeeName = pendingPrSearch[i]
				        .getText('custrecord_employee');
				prItem.ItemName = pendingPrSearch[i]
				        .getText('custrecord_prmatgrpcategory');
				prItem.CurrencyText = pendingPrSearch[i]
				        .getText('custrecord_currencyforpr');
				prItem.TotalCost = pendingPrSearch[i]
				        .getValue('custrecord_totalvalue');
				prItem.StatusText = pendingPrSearch[i]
				        .getText('custrecord_prastatus');

				prList.push(prItem);
			}
		}

		return prList;
	} catch (err) {
		nlapiLogExecution('error', 'getPRPendingApproval', err);
		throw err;
	}
}

function approvePR(prId, approverId) {
	try {

		var prItemRecord = nlapiLoadRecord('customrecord_pritem', prId);
		var prStatus = prItemRecord.getFieldValue('custrecord_prastatus');
		var allowAccess = (prStatus == '2' && prItemRecord
		        .getFieldValue('custrecord_subpracticehead') == approverId)
		        || (prStatus == '3' && prItemRecord
		                .getFieldValue('custrecord_itforpurchaserequest') == approverId)
		        || (prStatus == '4' && prItemRecord
		                .getFieldValue('custrecord_procurementteam') == approverId)
		        || (prStatus == '30' && prItemRecord
		                .getFieldValue('custrecord_pri_delivery_manager') == approverId)
		        || (prStatus == '24' && prItemRecord
		                .getFieldValue('custrecord_pri_finance_approver') == approverId);

		if (allowAccess) {
			nlapiTriggerWorkflow('customrecord_pritem', prId,
			        'customworkflow_prworkflow', 'workflowaction417');
			
			createLog('customrecord_pritem', prId, 'Update',
			        approverId, 'Approved');
			
			nlapiLogExecution('AUDIT', 'PR Approved', prId);
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'approvePR', err);
		throw err;
	}
}

function rejectPR(prId, approverId) {
	try {

		var prItemRecord = nlapiLoadRecord('customrecord_pritem', prId);
		var prStatus = prItemRecord.getFieldValue('custrecord_prastatus');
		var allowAccess = (prStatus == '2' && prItemRecord
		        .getFieldValue('custrecord_subpracticehead') == approverId)
		        || (prStatus == '3' && prItemRecord
		                .getFieldValue('custrecord_itforpurchaserequest') == approverId)
		        || (prStatus == '4' && prItemRecord
		                .getFieldValue('custrecord_procurementteam') == approverId)
		        || (prStatus == '30' && prItemRecord
		                .getFieldValue('custrecord_pri_delivery_manager') == approverId)
		        || (prStatus == '24' && prItemRecord
		                .getFieldValue('custrecord_pri_finance_approver') == approverId);

		if (allowAccess) {
			nlapiTriggerWorkflow('customrecord_pritem', prId,
			        'customworkflow_prworkflow', 'workflowaction418');
			
			createLog('customrecord_pritem', prId, 'Update',
			        approverId, 'Rejected');
			
			nlapiLogExecution('AUDIT', 'PR Rejected', prId);
		} else {
			throw "You are not authorised to access this record";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'rejectPR', err);
		throw err;
	}
}