/**
 * @author Shweta
 */

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Approve_Time.js
	Author      : Shweta Chopde
	Date        : 28 May 2014
	Description : Create a Time Sheet Entry Report


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{nlapiSetRedirectURL('SUITELET', 348, 1, null, null);
	var internal_ID;
	var d_date;
	var i_employee;
	var i_employee_txt;
	var i_customer;
	var i_item;
	var i_note;
	var i_duration;
	var i_type;
	var i_approver = '';
	var i_cnt = 0;
	var i_ent = 0;
	var i_ant = 0;
	var i_ref_no;
	var i_amount;
	var i_billable_s;
	var i_status_s;
	var i_item;
	var i_currency_s;
	var i_billable_s;	
	var i_currency_s;
	var i_task_w;
	var i_memo_w;
	var i_rate_w;
	var i_search_count =0 ;
	var a_employee_array = new Array();
	var s_script_status = '';
	var i_no_of_records = 0 ;
	try
	{
		if (request.getMethod() == 'GET') 
		{
			var i_context = nlapiGetContext();
		
			var i_usage_begin = i_context.getRemainingUsage();
		    nlapiLogExecution('DEBUG', 'suiteletFunction','Usage Begin -->' + i_usage_begin);
			
			var d_as_on_date_prm =  request.getParameter('custscript_as_on_date');
	        var i_employee_prm =  request.getParameter('custscript_employee_time');
			var a_data_array =  request.getParameter('custscript_data_array_1');
			var i_custom_id =  request.getParameter('custscript_custom_id');
			var i_search_count_SEARCH =  request.getParameter('custscript_search_count');
												
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Data Array -->' + a_data_array);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' As On Date -->' + d_as_on_date_prm);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Employee -->' + i_employee_prm);
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Search Count Search-->' + i_search_count_SEARCH);
						
		 var a_TDR_array_values = new Array()	
		 var i_data_TDR = new Array()
		 i_data_TDR =  a_data_array;
		 
		 if(_logValidation(i_data_TDR))
		 {
		 	  for(var dtr=0;dtr<i_data_TDR.length;dtr++)
			  {
				 	a_TDR_array_values = i_data_TDR.split(',')
				    break;				
			  }
		 	
		 }
	      	
	
	     nlapiLogExecution('DEBUG', 'schedulerFunction','TDR Array Values-->' +a_TDR_array_values);
	//	 nlapiLogExecution('DEBUG', 'schedulerFunction',' TDR Array Values Length-->' + a_TDR_array_values.length);
		
		
	
		 nlapiLogExecution('DEBUG', 'schedulerFunction','TR Array Values-->' +a_TR_array_values);
		// nlapiLogExecution('DEBUG', 'schedulerFunction',' TR Array Values Length-->' + a_TR_array_values.length);
		
			
				
						
			var i_current_user =  nlapiGetUser();		
			
			var i_data_s  = search_custom_recordID(i_current_user)
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Data S -->' + i_data_s);
			i_data_s = i_data_s.toString()	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Data S Length-->' + i_data_s.length);	
			
			
		 var a_TR_array_values = new Array()	
		 var i_data_TR = new Array()
		 i_data_TR =  i_data_s;
	 
	    	if(_logValidation(i_data_TR))
			{
				for(var dt=0;dt<i_data_TR.length;dt++)
			  {
				 	a_TR_array_values = i_data_TR.split(',')
				    break;				
			  }	
				
			}
	      
	
		 nlapiLogExecution('DEBUG', 'schedulerFunction','TR Array Values-->' +a_TR_array_values);
		// nlapiLogExecution('DEBUG', 'schedulerFunction',' TR Array Values Length-->' + a_TR_array_values.length);
			
			
		// ===========================================================================
		
			// --------------------- SCRIPT EXECUTION STATUS ---------------------
	//    if(_logValidation(d_created_script_date))
		{									
		var column = new Array();
		var filters = new Array();
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING']));
				
		filters.push(new nlobjSearchFilter('internalid', 'script','is','161'));
		
		column.push(new nlobjSearchColumn('name', 'script'));
		column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
		column.push(new nlobjSearchColumn('datecreated'));
		column.push(new nlobjSearchColumn('status'));
		column.push(new nlobjSearchColumn('startdate'));
		column.push(new nlobjSearchColumn('enddate'));
		column.push(new nlobjSearchColumn('queue'));
		column.push(new nlobjSearchColumn('percentcomplete'));
		column.push(new nlobjSearchColumn('queueposition'));
		column.push(new nlobjSearchColumn('percentcomplete'));

		var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
	    
		if(_logValidation(a_search_results)) 
	    {
	    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
	    	
			for(var i=0;i<a_search_results.length;i++)
	    	{
	    		var s_script  = a_search_results[i].getValue('name', 'script');
				nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);

				d_date_created  = a_search_results[i].getValue('datecreated');
				nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);
       
	           	var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
				nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);

				s_script_status  = a_search_results[i].getValue('status');
				nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
					              
			
	    	}
	    }
		
		}					
				
			var f_form = nlapiCreateForm(" TIME PENDING FOR APPROVAL ");
				
			f_form.setScript('customscript_cli_approve_time');
				
			var i_data_f =  f_form.addField('custpage_data', 'textarea', 'Data Array').setDisplayType('hidden');
		   
		    var i_current_user_f =  f_form.addField('custpage_current_user', 'text', 'User').setDisplayType('hidden');
		    i_current_user_f.setDefaultValue(i_current_user)
								
			var f_approve_fld =  f_form.addField('custpage_select_approve_chk', 'text', 'Approve').setDisplayType('hidden');
			
			var f_reject_fld =  f_form.addField('custpage_select_reject_chk', 'text', 'Reject').setDisplayType('hidden');
			
			var f_approve_reject_fld =  f_form.addField('custpage_select_approve_reject_chk', 'text', 'Approve/Reject').setDisplayType('hidden');
			
			var f_custom_rec_fld =  f_form.addField('custpage_custom_record_id', 'textarea', 'Custom ID').setDisplayType('hidden');
			
			
			var f_pending_records =  f_form.addField('custpage_pending_records', 'text', 'No. Of Records Pending ').setDisplayType('hidden');
						
	//		var f_pending_records_show =  f_form.addField('custpage_pending_records_show', 'text', 'No. Of Records Pending ').setDisplayType('inline');
			
			/*
             var f_pending_records_PM =  f_form.addField('custpage_pending_records_pm', 'text', 'No. Of Records Pending - Project Manager').setDisplayType('inline');
			
			var f_pending_records_SP =  f_form.addField('custpage_pending_records_sp', 'text', 'No. Of Records Pending - Supervisor').setDisplayType('inline');
			
*/			
			var i_as_on_date = f_form.addField('custpage_as_on_date', 'date', 'As on Date')
			i_as_on_date.setMandatory(true)		
			
			var i_employee_f = f_form.addField('custpage_employee','select', ' Employee');
			i_employee_f.addSelectOption('-All-','-All-');
		 	i_employee_f.setMandatory(true)			
			i_employee_f.setLayoutType('normal','startrow')		

   var strVar = "";
     strVar += "	<html>"
	 strVar += "	<body>" 
      strVar += "	<tr>";
	strVar += "		<td style=\"color:#00CC00;font-size:25\" font-size=\"25\" font-family=\"Helvetica\" width=\"100%\">Purchase Order<\/td>";
	strVar += "	<\/tr>";
strVar += "	</body>"
strVar += "	</html>"

            
            var t_default_lbl = f_form.addField('custpage_label','label','<html><body><h1><font color=blue>Please wait for 2 mns and refresh to load pending records</font> </h1></body></html>');
          //   var t_default_lbl = f_form.addField('custpage_label','label', '<b>Please wait for 2 mns and refresh to load pending records</b>');
			 t_default_lbl.setLayoutType('outsidebelow','startcol');
			
			
			 var t_default_lbl_msg_blank = f_form.addField('custpage_label_msg_blank','label','');
          	 t_default_lbl_msg_blank.setLayoutType('outsidebelow','startrow');
			
			
			
			 var t_default_lbl_msg = f_form.addField('custpage_label_msg','label','<html><body><h1><font color=blue>Maxmium 300 records will be displayed .</font> </h1></body></html>');
          	 t_default_lbl_msg.setLayoutType('outsidebelow','startrow');
			
			
			
			if(s_script_status == 'PROCESSING' || s_script_status == 'PENDING'||s_script_status == 'Processing' || s_script_status == 'Pending')
			{	
			
			
			
			 var t_script_status_label = f_form.addField('custpage_script_status_label','label', '<html><body><h1><font color=blue>Selected Entries are being processsed .<br> Please select next time Sheet Entries for processing .</font> </h1></body></html>');			
			 //var t_script_status_label = f_form.addField('custpage_script_status_label','label', '<b>Selected Entries are being processsed .<br> Please select next time Sheet Entries for processing .</b> ');
			 t_default_lbl.setLayoutType('outsidebelow','startcol');
							
			}
			
			if(_logValidation(d_as_on_date_prm))
			{
				i_as_on_date.setDefaultValue(d_as_on_date_prm)
			}
			else
			{
				i_as_on_date.setDefaultValue(get_todays_date())
			}

	         if(_logValidation(i_employee_prm)&& i_employee_prm!='-All-')
			 {
			 	i_employee_f.setDefaultValue(i_employee_prm)
			 }
			 else
			 {
				i_employee_f.setDefaultValue('-All-')
			 }		
						
			var i_current_user = nlapiGetUser();
			nlapiLogExecution('DEBUG', 'suiteletFunction',' ************ i_current_user -->' + i_current_user);
			
			var i_time_expense_entries_tab = f_form.addSubList('custpage_time_expense_sublist', 'list', 'Time / Expense Entries', 'custpage_time_entries_tab');
				//i_time_expense_entries_tab.addRefreshButton();
			
			var i_select_approve = i_time_expense_entries_tab.addField('custpage_select_approve', 'checkbox', 'Approve');
			var i_select_reject = i_time_expense_entries_tab.addField('custpage_select_reject', 'checkbox', 'Reject');		
			var i_S_No= i_time_expense_entries_tab.addField('custpage_s_no', 'text', 'S No');	
			var i_internalID = i_time_expense_entries_tab.addField('custpage_internal_id', 'select', 'ID','timebill').setDisplayType('inline');			
			var i_date = i_time_expense_entries_tab.addField('custpage_date', 'date', 'Date').setDisplayType('inline');	
			
			var i_employee = i_time_expense_entries_tab.addField('custpage_employee', 'select', 'Employee','employee').setDisplayType('inline');	
			var i_customer = i_time_expense_entries_tab.addField('custpage_customer', 'select', 'Project','customer').setDisplayType('inline');	
			var i_duration = i_time_expense_entries_tab.addField('custpage_duration', 'text', 'Hours')//.setDisplayType('inline');	
			
			var i_memo = i_time_expense_entries_tab.addField('custpage_memo', 'textarea', 'Memo')//.setDisplayType('inline');	
			var i_project_task = i_time_expense_entries_tab.addField('custpage_projecttask', 'textarea', 'Project Task').setDisplayType('inline');	
			var i_service_item = i_time_expense_entries_tab.addField('custpage_item', 'textarea', 'Item').setDisplayType('inline');	
		    var i_rate = i_time_expense_entries_tab.addField('custpage_rate', 'text', 'Rate').setDisplayType('inline');	
			var i_billable = i_time_expense_entries_tab.addField('custpage_billable', 'checkbox', 'Billable').setDisplayType('inline');	
		    var i_status = i_time_expense_entries_tab.addField('custpage_status', 'select', 'Status','customlist124').setDisplayType('inline');	
			
			
			 var i_usage_0 = i_context.getRemainingUsage();
		  //   nlapiLogExecution('DEBUG', 'suiteletFunction','Usage 0 -->' + i_usage_0);
						
		//	if(_logValidation(i_from_date)&&_logValidation(i_to_date))
		    {			
		  // ================== Project Manager - User =====================
			
			
			if(_logValidation(d_as_on_date_prm)&&_logValidation(i_employee_prm)&&i_employee_prm!='-All-')
			{			
			   nlapiLogExecution('DEBUG', 'suiteletFunction',' IF ');		
		        
				var filter = new Array();
				filter[0] = new nlobjSearchFilter('employee', null, 'is', i_employee_prm);
		        filter[1] = new nlobjSearchFilter('date', null, 'onorbefore', d_as_on_date_prm);
		       filter[2] = new nlobjSearchFilter('customer', null, 'isnotempty');
			    filter[3] = new nlobjSearchFilter('custcol_projectmanager', null, 'is',i_current_user);	
				
				if(_logValidation(a_TR_array_values))
				{
					filter[4] = new nlobjSearchFilter('internalid',null, 'noneof',a_TR_array_values);				
				}		
			     var i_search_results = nlapiSearchRecord('timebill','customsearch_time_pendingforapproval',filter,null);
				
				 var i_search_results_SC = nlapiSearchRecord('timebill','customsearch_time_pendingforapproval_4',filter,null);
								
			}
			else
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction',' ELSE ');
				
				var filter = new Array();
			filter[0] = new nlobjSearchFilter('customer', null, 'isnotempty');
			    filter[1] = new nlobjSearchFilter('custcol_projectmanager', null, 'is',i_current_user);
				if(_logValidation(a_TR_array_values))
				{
					filter[2] = new nlobjSearchFilter('internalid',null, 'noneof',a_TR_array_values);				
				}						
			     var i_search_results = nlapiSearchRecord('timebill','customsearch_time_pendingforapproval',filter,null);
			    
			     var i_search_results_SC = nlapiSearchRecord('timebill','customsearch_time_pendingforapproval_4',filter,null);
								
			}
			var i_search_count_2 = 0;
			if (_logValidation(i_search_results_SC)) 
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Time Search Results Length Project Manager Count -->' + i_search_results_SC);
				
				for (var q = 0; q < i_search_results_SC.length; q++) 
				{			
					
					var a_search_transaction_result = i_search_results_SC[q];
					if (!_logValidation(a_search_transaction_result)) 
					{
						break;
					}
					var columns = a_search_transaction_result.getAllColumns();
					var columnLen = columns.length;
					
					for (var hgq = 0; hgq < columnLen; hgq++) 
					{
						var column = columns[hgq];
						var label = column.getLabel();
						var value = a_search_transaction_result.getValue(column)
						var text = a_search_transaction_result.getText(column)
					
						if (label == 'Search Count') 
						{
							i_search_count_2 = value;
						}
					}
				}
				
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_search_count -->' + i_search_count);
				
			}			 			
			i_search_count+=parseInt(i_search_count_2);
			
		//	f_pending_records_PM.setDefaultValue(i_search_count)
		
			if(_logValidation(i_search_results))
			{
			  nlapiLogExecution('DEBUG', 'suiteletFunction',' Time Search Results Length Project manager -->' + i_search_results.length);	
			
		    // for(var q=0;q<=700;q++)
			
			  for(var q=0;q<i_search_results.length;q++)
			  {
			 
	             if(i_no_of_records==300)
				 {
				 	break;
				 }

				
			  	 var i_usage_1 = i_context.getRemainingUsage();
		     //    nlapiLogExecution('DEBUG', 'suiteletFunction','Usage 1 -->' + i_usage_1);
			 
			   
		  		var a_search_transaction_result = i_search_results[q];
				if(!_logValidation(a_search_transaction_result))
				{
					break;
				}
		        var columns = a_search_transaction_result.getAllColumns();		
		        var columnLen = columns.length;
			
				for(var hgq= 0 ;hgq<columnLen ; hgq++)
				{
				 var column = columns[hgq];
		         var label = column.getLabel();
				 var value = a_search_transaction_result.getValue(column)
				 var text = a_search_transaction_result.getText(column)
			
                 if(label =='Internal ID')
				 {
				 	internal_ID = value;					
				 }				 			
	             if(label =='Date')
				 {
				 	d_date = value;					
				 }				 
				 if(label =='Employee')
				 {
				 	i_employee = value;	
					i_employee_txt = text				
				 }				
				 if(label =='Customer')
				 {
				 	i_customer = value;					
				 }				  
				 if(label =='Duration')
				 {
				 	i_duration = value;					
				 }
				   if(label =='Status')
				 {
				 	i_status_s = value;					
				 }
				  
				 if(label =='Billable')
				 {
				 	i_billable_s = value;					
				 }
				 if(label =='Item')
				 {
				 	i_item = value;					
				 }
				 if(label =='Memo')
				 {
				 	i_memo_w = value;					
				 }
				  if(label =='Task')
				 {
				 	i_task_w = value;					
				 }
				 if(label =='Rate')
				 {
				 	i_rate_w = value;					
				 }
				 				 
				}//Time Column Loop
				if(!_logValidation(i_rate_w))
				 {
				 	i_rate_w = ''
				 }
				 if(!_logValidation(i_task_w))
				 {
				 	i_task_w = ''
				 }	
				 if(!_logValidation(i_type))
				 {
				 	i_type = ''
				 }		
				 if(!_logValidation(i_duration))
				 {
				 	i_duration = ''
				 }	
				  if(!_logValidation(i_note))
				 {
				 	i_note = ''
				 }	
				  if(!_logValidation(i_item))
				 {
				 	i_item = ''
				 }	
				  if(!_logValidation(i_customer))
				 {
				 	i_customer = ''
				 }	
				  if(!_logValidation(i_employee))
				 {
				 	i_employee = ''
				 }	
				  if(!_logValidation(d_date))
				 {
				 	d_date = ''
				 }
				  if(!_logValidation(internal_ID))
				 {
				 	internal_ID = ''
				 }	
				  if(!_logValidation(i_memo_w))
				 {
				 	i_memo_w = ''
				 }
				 				
				 /*
                 if(_logValidation(i_customer))
				 {
				 	 i_approver = get_project_manager(i_customer)
				 }
				 else
				 {
				 	 i_approver = get_supervisor(i_employee)
				 }
                */
				
			//    if(_logValidation(i_current_user)&&_logValidation(i_approver))
				{
			// 	 if(i_current_user == i_approver)
				 {
				 
				  if(_logValidation(a_TR_array_values)&&_logValidation(internal_ID))
				  {
				  	if(a_TR_array_values.indexOf(internal_ID) == -1)
					{
					i_cnt++;								
				   // ========================= Set values ==========================
				
				    a_employee_array.push(i_employee+'&&&&'+i_employee_txt);  
				
					i_time_expense_entries_tab.setLineItemValue('custpage_internal_id',i_cnt, internal_ID);		     
	                i_time_expense_entries_tab.setLineItemValue('custpage_s_no',i_cnt, (parseInt(i_cnt)).toFixed(0));					
	                i_time_expense_entries_tab.setLineItemValue('custpage_employee',i_cnt,i_employee);   
					i_time_expense_entries_tab.setLineItemValue('custpage_customer',i_cnt,i_customer);					
					i_time_expense_entries_tab.setLineItemValue('custpage_duration',i_cnt,i_duration);
					i_time_expense_entries_tab.setLineItemValue('custpage_date',i_cnt, d_date);	
					i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_cnt,i_billable_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_status',i_cnt, i_status_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_item',i_cnt, i_item);		
				  	i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_cnt, i_memo_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_projecttask',i_cnt, i_task_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_rate',i_cnt, i_rate_w);
					
					
					
					}					
				
				  }	
				  else
				  {
				  	 	i_cnt++;								
				// ========================= Set values ==========================
				
				   a_employee_array.push(i_employee+'&&&&'+i_employee_txt);  
				
					i_time_expense_entries_tab.setLineItemValue('custpage_internal_id',i_cnt, internal_ID);		     
	                i_time_expense_entries_tab.setLineItemValue('custpage_s_no',i_cnt, (parseInt(i_cnt)).toFixed(0));					
	                i_time_expense_entries_tab.setLineItemValue('custpage_employee',i_cnt,i_employee);   
					i_time_expense_entries_tab.setLineItemValue('custpage_customer',i_cnt,i_customer);					
					i_time_expense_entries_tab.setLineItemValue('custpage_duration',i_cnt,i_duration);
					i_time_expense_entries_tab.setLineItemValue('custpage_date',i_cnt, d_date);	
					i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_cnt,i_billable_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_status',i_cnt, i_status_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_item',i_cnt, i_item);		
				  	 	i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_cnt, i_memo_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_projecttask',i_cnt, i_task_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_rate',i_cnt, i_rate_w);
				  }
										 								  			
				

				 }//Approver 
			
				}
				 if(i_usage_1<100)
				{
					break;
				}
				i_no_of_records++;
						
			  }//Time Loop				
			}//Search Results
		
		   var i_usage_0_1 = i_context.getRemainingUsage();
		//   nlapiLogExecution('DEBUG', 'suiteletFunction','Usage 0 1-->' + i_usage_0_1);
				
					
	       // ============================= Supervisor ==========================			
				
			var filter = new Array();	
			if(_logValidation(d_as_on_date_prm)&&_logValidation(i_employee_prm)&&i_employee_prm!='-All-')
			{					
		        filter[0] = new nlobjSearchFilter('employee', null, 'is', i_employee_prm);
		        filter[1] = new nlobjSearchFilter('date', null, 'onorbefore', d_as_on_date_prm);
		        filter[2] = new nlobjSearchFilter('supervisor','employee', 'is',i_current_user);
				
				if(_logValidation(a_TR_array_values))
				{
					filter[3] = new nlobjSearchFilter('internalid',null, 'noneof',a_TR_array_values);				
				}			
								
			}
			else
			{			
			    filter[0] = new nlobjSearchFilter('supervisor','employee', 'is',i_current_user);
				
				if(_logValidation(a_TR_array_values))
				{
					filter[1] = new nlobjSearchFilter('internalid',null, 'noneof',a_TR_array_values);				
				}
			}				
			var i_search_results = nlapiSearchRecord('timebill','customsearch_time_pendingforapproval_2',filter,null);
			
			var i_search_results_SC = nlapiSearchRecord('timebill','customsearch_time_pendingforapproval_2_3',filter,null);
						
			var i_search_count_1 = 0 ;
			if (_logValidation(i_search_results_SC)) 
			{
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Time Search Results Length Supervisor Count -->' + i_search_results_SC.length);
				
				for (var q = 0; q < i_search_results_SC.length; q++) 
				{			
					
					var a_search_transaction_result = i_search_results_SC[q];
					if (!_logValidation(a_search_transaction_result)) 
					{
						break;
					}
					var columns = a_search_transaction_result.getAllColumns();
					var columnLen = columns.length;
					
					for (var hgq = 0; hgq < columnLen; hgq++) 
					{
						var column = columns[hgq];
						var label = column.getLabel();
						var value = a_search_transaction_result.getValue(column)
						var text = a_search_transaction_result.getText(column)
					
						if (label == 'Search Count') 
						{
							i_search_count_1 = value;
						}
					}
				}
				
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_search_count -->' + i_search_count);
				
			}			 			
			
			i_search_count+=parseInt(i_search_count_1);
		//	f_pending_records_SP.setDefaultValue(i_search_count)
			
			if(_logValidation(i_search_results))
			{
			  nlapiLogExecution('DEBUG', 'suiteletFunction',' Time Search Results Supervisor Length  -->' + i_search_results.length);	
		 // for(var cw=0;cw<=700;cw++)
			  for(var cw=0;cw<i_search_results.length;cw++)
			  {
			  	
				 
                 if(i_no_of_records==300)
				 {
				 	break;
				 }
               
				
			  	 var i_usage_2 = i_context.getRemainingUsage();
		      //   nlapiLogExecution('DEBUG', 'suiteletFunction','Usage 2 -->' + i_usage_2);
				
		  		var a_search_transaction_result = i_search_results[cw];
				if(!_logValidation(a_search_transaction_result))
				{
					break;
				}
		        var columns = a_search_transaction_result.getAllColumns();		
		        var columnLen = columns.length;
			
				for(var hgw= 0 ;hgw<columnLen ; hgw++)
				{
				 var column = columns[hgw];
		         var label = column.getLabel();
				 var value = a_search_transaction_result.getValue(column)
				 var text = a_search_transaction_result.getText(column)
											
				 if(label =='Internal ID')
				 {
				 	internal_ID = value;
				 }
				 if(label =='Date')
				 {
				 	d_date = value;
				 }
				  if(label =='Employee')
				 {
				 	i_employee = value;
					i_employee_txt = text
				 }
				  if(label =='Customer')
				 {
				 	i_customer = value;
				 }
				  if(label =='Item')
				 {
				 	i_item = value;
				 }
				  if(label =='Note')
				 {
				 	i_note = value;
				 }
				 if(label =='Duration')
				 {
				 	i_duration = value;
				 }		
				    if(label =='Status')
				 {
				 	i_status_s = value;					
				 }
				  
				 if(label =='Billable')
				 {
				 	i_billable_s = value;					
				 }	
				 if(label =='Item')
				 {
				 	i_item = value;					
				 }			
				 if(label =='Memo')
				 {
				 	i_memo_w = value;					
				 }
				  if(label =='Task')
				 {
				 	i_task_w = value;					
				 }
				 if(label =='Rate')
				 {
				 	i_rate_w = value;					
				 }
				}//Time Column Loop
				 if(!_logValidation(i_rate_w))
				 {
				 	i_rate_w = ''
				 }
				 if(!_logValidation(i_memo_w))
				 {
				 	i_memo_w = ''
				 }
				 if(!_logValidation(i_task_w))
				 {
				 	i_task_w = ''
				 }
				 if(!_logValidation(i_type))
				 {
				 	i_type = ''
				 }		
				 if(!_logValidation(i_duration))
				 {
				 	i_duration = ''
				 }	
				  if(!_logValidation(i_note))
				 {
				 	i_note = ''
				 }	
				  if(!_logValidation(i_item))
				 {
				 	i_item = ''
				 }	
				  if(!_logValidation(i_customer))
				 {
				 	i_customer = ''
				 }	
				  if(!_logValidation(i_employee))
				 {
				 	i_employee = ''
				 }	
				  if(!_logValidation(d_date))
				 {
				 	d_date = ''
				 }	
					
				// if(i_current_user == i_approver)
				 {
				   if(_logValidation(a_TR_array_values)&&_logValidation(internal_ID))
				  {
				  	if(a_TR_array_values.indexOf(internal_ID) == -1)
					{
					i_cnt++;								
				   // ========================= Set values ==========================
				
				    a_employee_array.push(i_employee+'&&&&'+i_employee_txt);  
				
					i_time_expense_entries_tab.setLineItemValue('custpage_internal_id',i_cnt, internal_ID);		     
	                i_time_expense_entries_tab.setLineItemValue('custpage_s_no',i_cnt, (parseInt(i_cnt)).toFixed(0));					
	                i_time_expense_entries_tab.setLineItemValue('custpage_employee',i_cnt,i_employee);   
					i_time_expense_entries_tab.setLineItemValue('custpage_customer',i_cnt,i_customer);					
					i_time_expense_entries_tab.setLineItemValue('custpage_duration',i_cnt,i_duration);
					i_time_expense_entries_tab.setLineItemValue('custpage_date',i_cnt, d_date);	
					i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_cnt,i_billable_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_status',i_cnt, i_status_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_item',i_cnt, i_item);		
				  	i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_cnt, i_memo_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_projecttask',i_cnt, i_task_w);	
					i_time_expense_entries_tab.setLineItemValue('custpage_rate',i_cnt, i_rate_w);	
				  		
				
					}					
				
				  }	
				  else
				  {
				  	 	i_cnt++;								
				// ========================= Set values ==========================
				
				   a_employee_array.push(i_employee+'&&&&'+i_employee_txt);  
				
					i_time_expense_entries_tab.setLineItemValue('custpage_internal_id',i_cnt, internal_ID);		     
	                i_time_expense_entries_tab.setLineItemValue('custpage_s_no',i_cnt, (parseInt(i_cnt)).toFixed(0));					
	                i_time_expense_entries_tab.setLineItemValue('custpage_employee',i_cnt,i_employee);   
					i_time_expense_entries_tab.setLineItemValue('custpage_customer',i_cnt,i_customer);					
					i_time_expense_entries_tab.setLineItemValue('custpage_duration',i_cnt,i_duration);
					i_time_expense_entries_tab.setLineItemValue('custpage_date',i_cnt, d_date);	
					i_time_expense_entries_tab.setLineItemValue('custpage_billable',i_cnt,i_billable_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_status',i_cnt, i_status_s);
					i_time_expense_entries_tab.setLineItemValue('custpage_item',i_cnt, i_item);		
					 	i_time_expense_entries_tab.setLineItemValue('custpage_memo',i_cnt, i_memo_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_projecttask',i_cnt, i_task_w);
					i_time_expense_entries_tab.setLineItemValue('custpage_rate',i_cnt, i_rate_w);
				  	
				  }				
							
				
				 }//Approver 
				 
				 if(i_usage_2<100)
				 {
				 	break;
				 }
					
				i_no_of_records++;
				
							
			  }//Time Loop				
			}//Search Results		
	
		}//Time
		nlapiLogExecution('DEBUG', 'suiteletFunction',' s_script_status ^^^^^^^^^^^^^^^^^8 -->' + s_script_status);
		
				
		if(s_script_status == 'PROCESSING' || s_script_status == 'PENDING'||s_script_status == 'Processing' || s_script_status == 'Pending')
		{
		 nlapiLogExecution('DEBUG', 'suiteletFunction',' s_script_status ^^^^^^^^^^^^^^^^^6 -->' + s_script_status);
		
		 if(_logValidation(a_TDR_array_values)&&_logValidation(i_search_count_SEARCH))
		 {
			i_search_count = parseInt(i_search_count_SEARCH)-parseInt(a_TDR_array_values.length)
			//f_pending_records.setDefaultValue(i_search_count.toFixed(0))
			
			var kt = i_search_count.toFixed(0)
			
			var f_pending_records_show = f_form.addField('custpage_pending_records_show','label','No. Of Records Pending<html><body><h1><font color=blue>'+kt+'</font> </h1></body></html>');
			
		//	f_pending_records_show.setDefaultValue('<html><body><b><font color=blue>'+kt+' <b></body></html>')
			
			f_pending_records.setDefaultValue(kt)
			
		    nlapiLogExecution('DEBUG', 'suiteletFunction',' i_search_count ^^^^^^^^^^^^^^^^^^ -->' + i_search_count);
		
		 } 
		}
		else 
		{
			i_search_count = parseInt(i_search_count);
			//f_pending_records.setDefaultValue(i_search_count.toFixed(0))
			var kt = i_search_count.toFixed(0)			
			
			var f_pending_records_show = f_form.addField('custpage_pending_records_show','label','No. Of Records Pending<html><body><h1><font color=blue> '+kt+'</font> </h1></body></html>');
			
			//f_pending_records_show.setDefaultValue('<html><body><b><font color=blue>'+kt+' </b></body></html>')
			f_pending_records.setDefaultValue(kt)
			nlapiLogExecution('DEBUG', 'suiteletFunction',' i_search_count ^^^^^^^^^^^^^^^^^6 -->' + i_search_count);
				
		}
		 
		 nlapiLogExecution('DEBUG', 'suiteletFunction',' a_TDR_array_values ***********  -->' + a_TDR_array_values);
		 nlapiLogExecution('DEBUG', 'suiteletFunction',' i_search_count_SEARCH *************** -->' + i_search_count_SEARCH);
		 nlapiLogExecution('DEBUG', 'suiteletFunction',' i_search_count *************** -->' + i_search_count);
			 
		
			
		
		// ========================== Employee List Sourcing - Start =======================================
			
			a_employee_array = removearrayduplicate(a_employee_array)
			
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Employee Array Length  -->' + a_employee_array.length);	
			nlapiLogExecution('DEBUG', 'suiteletFunction',' Employee Array -->' + a_employee_array);
			var a_employee_split_array = new Array();
			
			for(var et=0;et<a_employee_array.length;et++)
			{
				a_employee_split_array = a_employee_array[et].split('&&&&')
				i_employee_f.addSelectOption(a_employee_split_array[0],a_employee_split_array[1])				
			}
			
		// ========================== Employee List Sourcing - End =======================================
		
			f_form.addSubmitButton('APPROVE/REJECT');
			f_form.addButton('custpage_approve_markall','APPROVE MARK ALL','approve_mark_all()');
			f_form.addButton('custpage_approve_unmarkall','APPROVE UNMARK ALL','approve_unmark_all()');
			f_form.addButton('custpage_reject_markall','REJECT MARK ALL','reject_mark_all()');
			f_form.addButton('custpage_reject_unmarkall','REJECT UNMARK ALL','reject_unmark_all()');
			f_form.addButton('custpage_refresh','Refresh','refresh_page()');
						
		    response.writePage(f_form);	
			
			 var i_usage_end = i_context.getRemainingUsage();
		     nlapiLogExecution('DEBUG', 'suiteletFunction','Usage End -->' + i_usage_end);
		
		}//GET
		else if (request.getMethod() == 'POST') 	
		{
			 var d_as_on_date =  request.getParameter('custpage_as_on_date');
			 var i_employee =  request.getParameter('custpage_employee');
			 var a_data_array =   request.getParameter('custpage_data');
		     var i_selected_approve =   request.getParameter('custpage_select_approve_chk');
		     var i_selected_reject =   request.getParameter('custpage_select_reject_chk');
		     var i_current_user =   request.getParameter('custpage_current_user');
			 var i_selected_data_array =   request.getParameter('custpage_select_approve_reject_chk');
			 var i_custom_recordID =   request.getParameter('custpage_custom_record_id');
             var i_search_cnt =   request.getParameter('custpage_pending_records');
         
 
             nlapiLogExecution('DEBUG', 'POST suiteletFunction',' As On Date -->' + d_as_on_date);		
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Emplyee -->' + i_employee);		
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Data Array -->' + a_data_array);	
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Selected Approve -->' + i_selected_approve);		
			 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Selected Reject -->' + i_selected_reject);	
	 		 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Selected Data Array -->' + i_selected_data_array);	
	 		 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Custom Record ID -->' + i_custom_recordID);	
	 		 nlapiLogExecution('DEBUG', 'POST suiteletFunction',' Search Count -->' + i_search_cnt);	
	 		
								
			 var params=new Array();
		     params['custscript_date_till_now'] = d_as_on_date
			 params['custscript_s_employee'] = i_employee 			
			 params['custscript_data_array_approve'] = a_data_array
			 params['custscript_approve_time'] = i_selected_approve
			 params['custscript_reject_time'] = i_selected_reject
			 params['custscript_current_user_approve'] = i_current_user
			 params['custscript_custom_record_id_sch'] = i_custom_recordID
			 params['custscript_selected_data_array_sch'] = i_selected_data_array			 
			 
			 
			 
			// 	var d_as_on_date_prm =  request.getParameter('custscript_as_on_date');
	     //   var i_employee_prm =  request.getParameter('custscript_employee_time');
			 
			  var params_1=new Array();
		      params_1['custscript_data_array_1'] = i_selected_data_array
			  params_1['custscript_as_on_date'] = d_as_on_date
			  params_1['custscript_employee_time'] = i_employee
	          params_1['custscript_search_count'] = i_search_cnt
			 
			  
			 
			 var status=nlapiScheduleScript('customscript_sch_approve_time',null,params);
			 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Status -->' + status);
										
			 nlapiSetRedirectURL('suitelet', 'customscript_sut_approve_time', 'customdeploy1', null, params_1);
			 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Suitelet Called ........... ' ); 
				
			if(_logValidation(i_selected_approve))
			{
				response.write('<p><br/><br/><br/> Selected time sheets has been Approved.<\/p>')
			}	
			if(_logValidation(i_selected_reject))
			{
				response.write('<p><br/><br/><br/> Selected time sheets has been Rejected <\/p>')
			} 			
			 
						 	
		}//POST
	}
	catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);		
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!= 'null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function get_project_manager(i_project)
{
	var i_project_manager;
	if(_logValidation(i_project)) 
	{
	  var o_projectOBJ = nlapiLoadRecord('job',i_project);
	  
	  if(_logValidation(o_projectOBJ)) 
	  {
	  	i_project_manager = o_projectOBJ.getFieldValue('custentity_projectmanager');
	//	nlapiLogExecution('DEBUG', 'get_project_manager', ' i_project_manager -->' + i_project_manager);	  	
	  }//Project OBJ		
	}//Project	
	return i_project_manager;
}//Project Manager

function get_supervisor(i_employee)
{
	var i_supervisor;
	if(_logValidation(i_employee)) 
	{
	  var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
	  
	  if(_logValidation(o_employeeOBJ)) 
	  {
	  	i_supervisor = o_employeeOBJ.getFieldValue('supervisor');
		nlapiLogExecution('DEBUG', 'get_project_manager', ' i_supervisor -->' + i_supervisor);
		  	
	  }//Project OBJ		
	}//Project	
	return i_supervisor;
}//Project Manager

function get_employee_details(i_employee)
{
  var i_employeeID;	
  var i_supervisor;
  var a_return_array = new Array();
  
  if(_logValidation(i_employee))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('entityid', null, 'is',i_employee);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('supervisor');
	
	 var a_search_results = nlapiSearchRecord('employee',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	 i_employeeID = a_search_results[0].getValue('internalid');
		 i_supervisor = a_search_results[0].getValue('supervisor');
			
	 }//Search Results	
  }//Employee
  a_return_array[0] = i_employeeID+'%%%'+i_supervisor;
  return a_return_array;
}
/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}
function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
      nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
     nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	 nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 var s_month='';
	 
	 if(month == 1)
	 {
	 	s_month = 'Jan'
		
	 }
	 else if(month == 2)
	 {
	 		s_month = 'Feb'
		
	 }
	 else if(month == 3)
	 {
	 	
			s_month = 'Mar'
	 }
	 else if(month == 4)
	 {
	 		s_month = 'Apr'
		
	 }
	 else if(month == 5)
	 {
	 		s_month = 'May'
		
	 }
	 else if(month == 6)
	 {
	 	
			s_month = 'Jun'
	 }
	 else if(month ==7)
	 {
	 		s_month = 'Jul'
		
	 }
	 else if(month == 8)
	 {
	 		s_month = 'Aug'
		
	 }else if(month == 9)
	 {
	 		s_month = 'Sep'
		
	 }
	 else if(month == 10)
	 {
	 		s_month = 'Oct'
		
	 }
	 else if(month == 11)
	 {
	 	
			s_month = 'Nov'
	 }
	 else if(month == 12)
	 {
	 	
			s_month = 'Dec'
	 }
	 
	  if (date_format == 'DD-Mon-YYYY')
	 {
	  	today = day + '-' + s_month + '-' + year;
	 }
	 
	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}


function search_custom_recordID(i_employee)
{
	var a_rec_arr = new Array();
	
 	if(_logValidation(i_employee))
	{
	nlapiLogExecution('DEBUG', 'search_custom_recordID', 'i_employee ==' +i_employee);
	 	
		
	var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_emp_p', null,'is',i_employee);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_data_p');
	
	 var a_search_results = nlapiSearchRecord('customrecord_time_process_record',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	for(var i=0;i<a_search_results.length;i++)
		{
			i_custom_recID = a_search_results[i].getValue('internalid');
		    i_processed_records = a_search_results[i].getValue('custrecord_data_p');
			
			nlapiLogExecution('DEBUG', 'search_custom_recordID', 'Custom Rec ID ==' +i_custom_recID);
	 
	        nlapiLogExecution('DEBUG', 'search_custom_recordID', 'Processed Records ==' +i_processed_records);
	 	      
			a_rec_arr.push(i_processed_records);
		}
	 	 
	 }//Search Results	
	
    }
	return a_rec_arr;
}





// END OBJECT CALLED/INVOKING FUNCTION =====================================================
