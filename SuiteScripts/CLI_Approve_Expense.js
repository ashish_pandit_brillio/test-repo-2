/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Approve_Expense.js
	Author      : Shweta Chopde
	Date        : 28 May 2014
    Description : Validations on Time Approval


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_disable_line(type)
{
   nlapiDisableLineItemField('expense','custcol_srnumber',true)
}

// END PAGE INIT ====================================================


// BEGIN SAVE RECORD ================================================
var a_data_array = '';
var a_selected_approve = '';
var a_selected_reject = '';

function saveRecord_time_exp()
{
	var i_employee = nlapiGetFieldValue('custpage_employee')
	var i_as_on_date = nlapiGetFieldValue('custpage_as_on_date')
    var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist');
	
	if(_logValidation(i_line_count))
	{
	  for(var i=1;i<=i_line_count;i++)
	  {	  
			var i_select_approve = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select_approve',i)
			var i_select_reject = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select_reject',i)
			var i_employee = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_employee',i)
			var i_internal_id = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_internal_id',i)
			var i_customer = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_customer',i)
			var i_amount = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_amount',i)	
			var i_ref_no = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_ref_no',i)	
			
			
			if(i_select_approve == 'T')
			{			
               if(a_data_array =='')
			   {
			   	a_data_array = i_employee+'#####'+i_internal_id+'#####'+i_customer+'#####'+i_amount+'#####'+i_ref_no
			   }
			   else
			   {
			   	a_data_array=a_data_array+','+i_employee+'#####'+i_internal_id+'#####'+i_customer+'#####'+i_amount+'#####'+i_ref_no
			   }
            }//Select T	
			
			if(i_select_reject == 'T')
			{			
               if(a_data_array =='')
			   {
			   	a_data_array = i_employee+'#####'+i_internal_id+'#####'+i_customer+'#####'+i_amount+'#####'+i_ref_no
			   }
			   else
			   {
			   	a_data_array=a_data_array+','+i_employee+'#####'+i_internal_id+'#####'+i_customer+'#####'+i_amount+'#####'+i_ref_no
			   }
            }//Select F
			
			if(i_select_approve == 'T')
			{			
               if(a_selected_approve =='')
			   {
			   	a_selected_approve = i_select_approve
			   }
			   else
			   {
			   	a_selected_approve=a_selected_approve+','+i_select_approve
			   }
            }//Select T	
			
			if(i_select_reject == 'T')
			{			
               if(a_selected_reject =='')
			   {
			   	a_selected_reject = i_select_reject
			   }
			   else
			   {
			   	a_selected_reject=a_selected_reject+','+i_select_reject
			   }
            }//Select F			
	  }//Loop		
	}//Line Count
	
	nlapiSetFieldValue('custpage_as_on_date',i_as_on_date)
	nlapiSetFieldValue('custpage_employee',i_employee)
	nlapiSetFieldValue('custpage_select_approve_chk',a_selected_approve)
	nlapiSetFieldValue('custpage_select_reject_chk',a_selected_reject)
	nlapiSetFieldValue('custpage_data',a_data_array)

	if(!_logValidation(a_data_array))
	{
	  alert(' Please select atleast a single entry from the list .')
	  return false;	
	}
	if(_logValidation(a_data_array))
	{
	 if(_logValidation(a_selected_approve))
	 {
	 	alert('Selected Expenses has been approved .')
		window.open('/app/center/card.nl?sc=-46&t=eRNkuZKrm&whence=')
	 }//Approve 	
	 if(_logValidation(a_selected_reject))
	 {
	 	alert('Selected Expenses has been rejected .')
		window.open('/app/center/card.nl?sc=-46&t=eRNkuZKrm&whence=')
	 }//Reject	
	 	  
	}

	return true;

}

// END SAVE RECORD ==================================================

/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}



// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_time_exp(type, name, linenum)
{
	var a_selected_approve_arr = new Array();
	var a_selected_reject_arr = new Array();
	var i_current_user =  nlapiGetUser();
	
    if(name == 'custpage_select_approve')
	{
		/*
alert('Approve .....')	
*/
		var i_approve = nlapiGetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve');
		
		var i_reject = nlapiGetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject');
		
		if(i_approve == 'T'&& i_reject =='T')
		{
			alert(' Select one of them : Approve / Reject . \n At a time you cannot Approve & Reject .')
			nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject','F')
			nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve','F')
			return false;
			
		}//Select Approve
		
		var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist');
		
		if( _logValidation(i_line_count))
		{
			for(var k=1;k<=i_line_count;k++)
			{
			  var i_reject_sel = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select_reject',k)
			  
			  if(i_reject_sel == 'T')
			  {
			  	break;			
			  }
			  
			}
			
			if(i_reject_sel == 'T'&& i_approve == 'T')
			{			
				alert(' Select one of them : Approve / Reject . \n At a time you cannot Approve & Reject .');
				nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve','F')
				return false;
				
			}
			
		}//Line Count
		
	}//Approve
	
	if(name == 'custpage_select_reject')
	{
		/*
	alert('Reject .....')	
*/
		var i_approve = nlapiGetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve');
		
		var i_reject = nlapiGetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject');
		
		if(i_approve == 'T'&& i_reject =='T')
		{
			alert(' Select one of them : Approve / Reject . \n At a time you cannot Approve & Reject .')
			nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve','F')
			nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject','F')
			return false;
			
		}//Select Approve	
		
			var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist');
		
		if( _logValidation(i_line_count))
		{
			for(var k=1;k<=i_line_count;k++)
			{
			  var i_approve_sel = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select_approve',k)
			  
			  if(i_approve_sel == 'T')
			  {
			  	break;			
			  }
			  
			}
			
			if(i_approve_sel == 'T'&& i_reject == 'T')
			{			
				alert(' Select one of them : Approve / Reject . \n At a time you cannot Approve & Reject .');
				nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject','F')
				return false;
				
			}
			
		}//Line Count	
	//	alert('Reject .....')	
	}//Reject
	
	if(name == 'custpage_employee')
	{
	/*
	alert('Employee .....')	
*/
	  var i_employee =  nlapiGetFieldValue('custpage_employee');
	 
	  var d_as_on_date = nlapiGetFieldValue('custpage_as_on_date')
						
		if(_logValidation(i_employee)&&_logValidation(d_as_on_date))
	    {
		//	 window.open('/app/site/hosting/scriptlet.nl?script=163&deploy=1','_self',null)
			
	       window.open('/app/site/hosting/scriptlet.nl?script=163&deploy=1&custscript_as_on_date_e='+d_as_on_date+'&custscript_employee_expense='+i_employee,'_self',null)
		}//Time	
		
	}//Employee
	
	if(name == 'custpage_as_on_date')
	{
	  var i_employee =  nlapiGetFieldValue('custpage_employee');
	 
	  var d_as_on_date = nlapiGetFieldValue('custpage_as_on_date')
	  
	 /*
 alert('As On Date .....'+d_as_on_date)	
	  alert('Employee.....'+i_employee)	
*/
					
		if(_logValidation(i_employee)&&_logValidation(d_as_on_date))
	    {	
		
		
//		 window.open('/app/site/hosting/scriptlet.nl?script=163&deploy=1')
		
	     window.open('/app/site/hosting/scriptlet.nl?script=163&deploy=1&custscript_as_on_date_e='+d_as_on_date+'&custscript_employee_expense='+i_employee,'_self',null)
		
		//window.open('/app/site/hosting/scriptlet.nl?script=163&deploy=1&custscript_as_on_date_e='+d_as_on_date+'&custscript_employee_expense='+i_employee)
		}//Time	
		
	}//Employee
	return true;
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================


function approve_mark_all()
{
   var a_mark_array = new Array();
   		
   var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
		
	if(i_line_count!=null && i_line_count!='' && i_line_count!=0 && i_line_count!= undefined)
	{		
		for(var a=1;a<=i_line_count;a++)
		{
		  var i_select_mark = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select_reject',a)	
		 
		  
		  if(i_select_mark == 'T')
		  {
		  	a_mark_array.push(i_select_mark)
		  	break;
		  } 
		  
		}
	
		if(_logValidation(a_mark_array))
		{
		  if(a_mark_array.indexOf('T')>-1)
		  {
		  	alert(' Select one of them : Approve / Reject . \n At a time you cannot Approve & Reject .');
			return false;
		  }		
			
		}//Mark Array
		else
		{
			for(var a=1;a<=i_line_count;a++)
			{			 
	           nlapiSelectLineItem('custpage_time_expense_sublist',a)
			   nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve','T')
			   nlapiCommitLineItem('custpage_time_expense_sublist');
	          
			}
		  	
		}
				
	}

}
function approve_unmark_all()
{	
	var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
		
	if(i_line_count!=null && i_line_count!='' && i_line_count!=0 && i_line_count!= undefined)
	{		
		for(var a=1;a<=i_line_count;a++)
		{		  
		   nlapiSelectLineItem('custpage_time_expense_sublist',a)
		   nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_approve','F')
		   nlapiCommitLineItem('custpage_time_expense_sublist');
		}
				
	}
}
function reject_mark_all()
{ 
  var a_mark_array = new Array();
   		
   var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
		
	if(i_line_count!=null && i_line_count!='' && i_line_count!=0 && i_line_count!= undefined)
	{		
		for(var a=1;a<=i_line_count;a++)
		{
		  var i_select_mark = nlapiGetLineItemValue('custpage_time_expense_sublist','custpage_select_approve',a)	
		  
		  
		  if(i_select_mark == 'T')
		  {
		  	a_mark_array.push(i_select_mark)
		  	break;
		  } 
		  
		}
		
		if(_logValidation(a_mark_array))
		{
		  if(a_mark_array.indexOf('T')>-1)
		  {
		  	alert(' Select one of them : Approve / Reject . \n At a time you cannot Approve & Reject .');
			return false;
		  }		
			
		}//Mark Array
		else
		{
			for(var a=1;a<=i_line_count;a++)
			{		  
			    nlapiSelectLineItem('custpage_time_expense_sublist',a)
			   nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject','T')
			   nlapiCommitLineItem('custpage_time_expense_sublist');
			}
		  	
		}
				
	}
	
}
function reject_unmark_all()
{	
	var i_line_count = nlapiGetLineItemCount('custpage_time_expense_sublist')
		
	if(i_line_count!=null && i_line_count!='' && i_line_count!=0 && i_line_count!= undefined)
	{		
		for(var a=1;a<=i_line_count;a++)
		{		  
		   nlapiSelectLineItem('custpage_time_expense_sublist',a)
		   nlapiSetCurrentLineItemValue('custpage_time_expense_sublist','custpage_select_reject','F')
		   nlapiCommitLineItem('custpage_time_expense_sublist');
		}
				
	}
}
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!= 'null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function get_project_manager(i_project)
{
	var i_project_manager;
	if(_logValidation(i_project)) 
	{
	  var o_projectOBJ = nlapiLoadRecord('job',i_project);
	  
	  if(_logValidation(o_projectOBJ)) 
	  {
	  	i_project_manager = o_projectOBJ.getFieldValue('custentity_projectmanager');
		nlapiLogExecution('DEBUG', 'get_project_manager', ' i_project_manager -->' + i_project_manager);	  	
	  }//Project OBJ		
	}//Project	
	return i_project_manager;
}//Project Manager

function get_supervisor(i_employee)
{
	var i_supervisor;
	if(_logValidation(i_employee)) 
	{
	  var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
	  
	  if(_logValidation(o_employeeOBJ)) 
	  {
	  	i_supervisor = o_employeeOBJ.getFieldValue('supervisor');
		nlapiLogExecution('DEBUG', 'get_project_manager', ' i_supervisor -->' + i_supervisor);
		  	
	  }//Project OBJ		
	}//Project	
	return i_supervisor;
}//Project Manager

function get_employee_details(i_employee)
{
  var i_employeeID;	
  var i_supervisor;
  var a_return_array = new Array();
  
  if(_logValidation(i_employee))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('entityid', null, 'is',i_employee);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('supervisor');
	
	 var a_search_results = nlapiSearchRecord('employee',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	 i_employeeID = a_search_results[0].getValue('internalid');
		 i_supervisor = a_search_results[0].getValue('supervisor');
			
	 }//Search Results	
  }//Employee
  a_return_array[0] = i_employeeID+'%%%'+i_supervisor;
  return a_return_array;
}
/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}

// END FUNCTION =====================================================
