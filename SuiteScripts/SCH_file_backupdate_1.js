/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 July 2019     sai saranya
 * 1.01		  13 Sep 2019		Aazamali 	  	Check for already processed ids 
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
var timesheetId_Emp = '';
var currentEmp = '';
var timesheetId = 0;

function scheduled(type) {

    try {
        var i_context = nlapiGetContext();
        //	var csvFileId = nlapiLoadFile('FUEL Back update/Fuel Funnel Report_without filter-with hire details1.csv');
        var csvFile = nlapiLoadFile('FUEL Back update/Fuel NS Funnel Report.csv');
        var proccessedIds = [];
        var csvContent = csvFile.getValue();
        var rows_data = csvContent.split('\r\n');
        var newTimesheet;
        var status = 'status_';
        var count = 1;
        for (var line_indx = 1; line_indx <= rows_data.length - 1; line_indx++) {
            if (i_context.getRemainingUsage() <= 50) {
                nlapiYieldScript();
            }
            var col_data = rows_data[line_indx].split(',');
            var req_id = col_data[0];
            var rrf_number = '';
            var taleo_external_hireSearch = nlapiSearchRecord("customrecord_taleo_external_hire", null,
                [
                    ["custrecord_taleo_ext_hire_frf_details.custrecord_frf_details_ns_rrf_number", "is", req_id], "OR",
                    ["custrecord_taleo_ext_hire_frf_details.custrecord_rrf_taleo", "is", req_id]
                ],
                [new nlobjSearchColumn("internalid")]);
            if (_logValidation(taleo_external_hireSearch)) {
                var taleo_rec_id = taleo_external_hireSearch[0].getValue('internalid');
                if (proccessedIds.indexOf(taleo_rec_id) == -1) {
                    var record_id = nlapiLoadRecord('customrecord_taleo_external_hire', taleo_rec_id);
                    record_id.setFieldValue('custrecord_taleo_external_hire_conducted', col_data[4]);
                    record_id.setFieldValue('custrecord_taleo_screening_stage', col_data[14]);
                    record_id.setFieldValue('custrecord_taleo_external_stage_cleared', col_data[5]);
                    record_id.setFieldValue('custrecord_taleo_inter_stg_first_level_c', col_data[15]);
                    record_id.setFieldValue('custrecord_taleo_ext_hire_tech1_cond', col_data[6]);
                    record_id.setFieldValue('custrecord_taleo_inter_stg_first_lvl_cl', col_data[7]);
                    record_id.setFieldValue('custrecord_interview_stage_second_lvl_c', col_data[16]);
                    record_id.setFieldValue('custrecord_taleo_ext_tech2_conducted', col_data[8]);
                    record_id.setFieldValue('custrecord_interview_stage_second_lvl_cl', col_data[9]);
                    record_id.setFieldValue('custrecord_taleo_ext_hr_round_eligible', col_data[18]);
                    record_id.setFieldValue('custrecord_taleo_ext_hr_round_conducted', col_data[10]);
                    record_id.setFieldValue('custrecord_taleo_extenal_hr_round', col_data[11]);
                    record_id.setFieldValue('custrecord_taleo_ext_cust_interview_eli', col_data[17]);
                    record_id.setFieldValue('custrecord_taleo_interview_conducted', col_data[12]);
                    record_id.setFieldValue('custrecord_taleo_customer_inter_clear', col_data[13]);
                    record_id.setFieldValue('custrecord_taleo_offer_accepted', col_data[2]);
                    record_id.setFieldValue('custrecord_taleo_external_offer_rejected', col_data[1]);
                    record_id.setFieldValue('custrecord_taleo_ext_offer_released', col_data[3]);


                    if (col_data[20] == 'NA' && col_data[19] == 'NA') {
                        col_data[20] = '';
                        col_data[19] = '';
                        var candi_name = ''
                    } else if (col_data[20] == 'NA' && col_data[19] != 'NA') {
                        col_data[20] = '';
                        var candi_name = col_data[19]
                        nlapiLogExecution('Debug', 'candi_name ', candi_name);
                    } else if (col_data[20] != 'NA' && col_data[19] == 'NA') {
                        col_data[19] = '';
                        var candi_name = col_data[20]
                        nlapiLogExecution('Debug', 'candi_name ', candi_name);
                    } else {
                        var candi_name = col_data[19] + ' , ' + col_data[20];
                        nlapiLogExecution('Debug', 'candi_name ', candi_name);
                    }
                    if (col_data[21] == 'NA') {
                        col_data[21] = '';
                        nlapiLogExecution('Debug', 'col_data[21] ', col_data[21]);
                        record_id.setFieldValue('custrecord_offered_emp_email', col_data[21]);
                    } else {
                        record_id.setFieldValue('custrecord_offered_emp_email', col_data[21]);
                    }
                    if (col_data[22] == 'NA') {
                        col_data[22] = '';
                        nlapiLogExecution('Debug', 'col_data[22] ', col_data[22]);
                        record_id.setFieldValue('custrecord_candidate_id', col_data[22]);
                    } else {
                        record_id.setFieldValue('custrecord_candidate_id', col_data[22]);
                    }
                    record_id.setFieldValue('custrecord_taleo_external_selected_can', candi_name);


                    record_id.setFieldValue('custrecord_taleo_ext_hire_status', col_data[23]);

                    if (col_data[24] != 'NA') {
                        var TheDate = FormatDate(col_data[24]);
                        //	nlapiLogExecution('debug', 'TheDate is: ', TheDate);
                        record_id.setFieldValue('custrecord_taleo_emp_joining_date', TheDate);
                    } else {

                        var TheDate = '';
                        nlapiLogExecution('debug', 'TheDate is: ', TheDate);
                        record_id.setFieldValue('custrecord_taleo_emp_joining_date', TheDate);
                    }
                    if (col_data[25] != 'NA' && _logValidation(col_data[25])) {
                        var filledDate = FormatDate(col_data[25]);
                        nlapiLogExecution('debug', 'TheDate is: ', filledDate);
                        record_id.setFieldValue('custrecord_rrf_filled_date', filledDate);
                    } else {
                        var filledDate = '';
                        nlapiLogExecution('debug', 'TheDate is: ', filledDate);
                        record_id.setFieldValue('custrecord_rrf_filled_date', filledDate);
                    }
                    //-------------------------------------------------------------------------------------------------------------------------			//prabhat gupta	NIS-1309 12/05/2020	
                    if (col_data[26] != 'NA' && _logValidation(col_data[26])) {
                        var approveDate = FormatDate(col_data[26]);
                        nlapiLogExecution('debug', 'TheDate is: ', approveDate);
                        record_id.setFieldValue('custrecord_rrf_approve_date', approveDate);
                    } else {
                        var approveDate = '';
                        nlapiLogExecution('debug', 'TheDate is: ', approveDate);
                        record_id.setFieldValue('custrecord_rrf_approve_date', approveDate);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------			//prabhat gupta 05/06/2020	

                    if (col_data[27] != 'NA' && _logValidation(col_data[27])) {
                        var ctc = col_data[27];
                        nlapiLogExecution('debug', 'CTC: ', ctc);
                        record_id.setFieldValue('custrecord_rrf_ctc_offered', ctc);

                        var encrypred_ctc = encrypt(ctc);
                        record_id.setFieldValue('custrecord_rrf_encrypted_ctc', encrypred_ctc);
                    } else {
                        var ctc = '';
                        nlapiLogExecution('debug', 'CTC: ', ctc);
                        record_id.setFieldValue('custrecord_rrf_ctc_offered', ctc);

                        var encrypred_ctc = '';
                        record_id.setFieldValue('custrecord_rrf_encrypted_ctc', encrypred_ctc);
                    }
                    if (col_data[28] != 'NA' && _logValidation(col_data[28])) {
                        var currencyCode = col_data[28].match(/\(([^)]+)\)/).pop();
                        nlapiLogExecution('debug', 'Currency Code: ', currencyCode);
                        record_id.setFieldValue('custrecord_rrf_currency_code', currencyCode);
                    } else {
                        var currencyCode = '';
                        nlapiLogExecution('debug', 'Currency Code: ', currencyCode);
                        record_id.setFieldValue('custrecord_rrf_currency_code', currencyCode);
                    }
                    if (col_data[29] != 'NA' && _logValidation(col_data[29])) {
                        var salaryBasis = col_data[29];
                        nlapiLogExecution('debug', 'Salary Basis: ', salaryBasis);
                        record_id.setFieldValue('custrecord_rrf_salary_basis', salaryBasis);
                    } else {
                        var salaryBasis = '';
                        nlapiLogExecution('debug', 'Salary Basis: ', salaryBasis);
                        record_id.setFieldValue('custrecord_rrf_salary_basis', salaryBasis);
                    }




                    var reord_id = nlapiSubmitRecord(record_id);
                    proccessedIds.push(reord_id);
                }
            }
            nlapiLogExecution('debug', 'requisition ID', req_id);

        }

    } catch (err) {
        nlapiLogExecution('ERROR', 'scheduled taleo rec:' + taleo_rec_id, err);
    }

}

function FormatDate(dateis) {
    // Netsuite Format is MM/DD/YYYY
    // Taleo format is DD/MM/YYYY
    var r = dateis.split(" ");
    var sp = r[0].split("-");
    var timeArr = r[1].split(":")
    var date = new Date(sp[2], sp[1] - 1, sp[0], timeArr[0], timeArr[1], timeArr[2])
    nlapiLogExecution('Debug','date ',date);
    var time = date.setHours(date.getHours() - 9.5)
    nlapiLogExecution('Debug','time ',time);
    var formattedDate = new Date(time)
    var tdate = formattedDate.getDate();
    var month = formattedDate.getMonth() + 1; // jan = 0
    var year = formattedDate.getFullYear();
    if (month < 10) {
        month = "0" + month;
    }
    if (tdate < 10) {
        tdate = "0" + tdate;
    }
    var currentDate = month + '/' + tdate + '/' + year;
    return currentDate;
    //	return nlapiDateToString(new Date(sp[2],sp[0]-1,sp[1]));// YYYY/mm-1/dd
}
var month = [' ', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

function FormatDateWithMonth(dateis) {
    // Netsuite Format is MM/DD/YYYY
    // Taleo format is DD/MM/YYYY
    var r = dateis.split(" ");
    var sp = r[0].split("-");
    var month_ind = sp[1];
    return monthName(month_ind) + "/" + sp[0] + "/" + sp[2];
    //	return nlapiDateToString(new Date(sp[2],sp[0]-1,sp[1]));// YYYY/mm-1/dd
}

function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function monthName(name) {
    var indx = '';
    if (name == 'Jan')
        indx = 01;
    if (name == 'Feb')
        indx = 02;
    if (name == 'Mar')
        indx = 03;
    if (name == 'Apr')
        indx = 04;
    if (name == 'May')
        indx = 05;
    if (name == 'Jun')
        indx = 06;
    if (name == 'Jul')
        indx = 07;
    if (name == 'Aug')
        indx = 08;
    if (name == 'Sep')
        indx = 09;
    if (name == 'Oct')
        indx = 10;
    if (name == 'Nov')
        indx = 11;
    if (name == 'Dec')
        indx = 12;

    return indx;
}

function encrypt(data) {

    var encrypted_data = nlapiEncrypt(data, "aes", "48656C6C6F0B0B0B0B0B0B0B0B0B0B0B");
    nlapiLogExecution('debug', 'encrypted data: ', encrypted_data);
    return encrypted_data;

}