function postRESTlet(dataIn) 
{
	var response = new Response();
	try
	{
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		
		//var dataIn = [];
		//dataIn = {"RequestType":"GET","Data":{"EmpId":"115675"}};
		
		var requestType = dataIn.RequestType;
		
		var id = dataIn.Data.EmpId;
		
		switch (requestType) 
		{
			case M_Constants.Request.Get:

			response.Data = getEmployeeDetails(id);
			response.Status = true;
		}
	}
	catch (err)
	{
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(response));
	return response;
}

function getEmployeeDetails(id)
{
	try
	{
		var emp_id ='';
		var emp_type ='';
		var person_type ='';
		var emp_fusion_id ='';
		var emp_frst_name ='';
		var emp_middl_name ='';
		var emp_lst_name ='';
		var emp_full_name ='';
		
		//Manager
		var mgr_fusion_id ='';
		var mgr_frst_name ='';
		var mgr_middl_name ='';
		var mgr_lst_name ='';
		var mgr_full_name ='';
		var mgr_email ='';
		
		var cols = [];
		cols.push(new nlobjSearchColumn('firstname'));
		cols.push(new nlobjSearchColumn('middlename'));
		cols.push(new nlobjSearchColumn('lastname'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
		cols.push(new nlobjSearchColumn('department'));
		cols.push(new nlobjSearchColumn('title'));
		cols.push(new nlobjSearchColumn('custentity_employeetype'));
		cols.push(new nlobjSearchColumn('custentity_persontype'));
		cols.push(new nlobjSearchColumn('email')); //custentity_emp_attendance_tracking
		cols.push(new nlobjSearchColumn('custentity_employee_inactive'));
		cols.push(new nlobjSearchColumn('custentity_emp_attendance_tracking'));
		cols.push(new nlobjSearchColumn('gender'));
		cols.push(new nlobjSearchColumn('employeetype'));
		cols.push(new nlobjSearchColumn('custentity_employee_inactive'));
		cols.push(new nlobjSearchColumn('email','custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('custentity_fusion_empid','custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('firstname','custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('middlename','custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('lastname','custentity_reportingmanager'));
		cols.push(new nlobjSearchColumn('subsidiary'));
		cols.push(new nlobjSearchColumn('custentity_lwd'));
		cols.push(new nlobjSearchColumn('employeestatus'));
		cols.push(new nlobjSearchColumn('phone'));
		cols.push(new nlobjSearchColumn('hiredate'));
		//Absence details:
		cols.push(new nlobjSearchColumn('custentity_personid'));
		cols.push(new nlobjSearchColumn('custentity_employer_id'));
		cols.push(new nlobjSearchColumn('custentity_assignment_num'));
			
		//Brillio Location
		cols.push(new nlobjSearchColumn('custentity_list_brillio_location_e'));	

      //Legal Entity
       cols.push(new nlobjSearchColumn('custentity_legal_entity_fusion'));
      cols.push(new nlobjSearchColumn('custentity_fresher'));
		var employeeSearch = nlapiSearchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_fusion_empid', null,
			                'is',id),
					new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is','F')], cols);	
		
		var dataRow = [];
		var JSON = {};
		if (employeeSearch)
		{
			for(var i = 0; i < employeeSearch.length; i++)
			{
				emp_id = employeeSearch[i].getId();
				emp_type = employeeSearch[i].getText('custentity_employeetype');
				person_type = employeeSearch[i].getText('custentity_persontype');
				emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
				emp_frst_name = employeeSearch[i].getValue('firstname');
				emp_middl_name = employeeSearch[i].getValue('middlename');
				emp_lst_name = employeeSearch[i].getValue('lastname');
						
						
						
				if(emp_frst_name)
					emp_full_name = emp_frst_name;
							
				if(emp_middl_name)
					emp_full_name = emp_full_name + ' ' + emp_middl_name;
							
				if(emp_lst_name)
					emp_full_name = emp_full_name + ' ' + emp_lst_name;
						
				//manager
				mgr_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid','custentity_reportingmanager');
				mgr_frst_name = employeeSearch[i].getValue('firstname','custentity_reportingmanager');
				mgr_middl_name = employeeSearch[i].getValue('middlename','custentity_reportingmanager');
				mgr_lst_name = employeeSearch[i].getValue('lastname','custentity_reportingmanager');
						
				if(mgr_frst_name)
					mgr_full_name = mgr_frst_name;
							
				if(mgr_middl_name)
					mgr_full_name = mgr_full_name + ' ' + mgr_middl_name;
							
				if(mgr_lst_name)
					mgr_full_name = mgr_full_name + ' ' + mgr_lst_name;
						
						
						
				JSON = {
					ID: emp_id,
					EmployeeID:emp_fusion_id,
					Emp_Display_Name: emp_full_name,
					EmployeeType: emp_type,
					PersonType: person_type,
					Designation: employeeSearch[i].getValue('title'),
					FirstName: employeeSearch[i].getValue('firstname'),
					MiddleName: employeeSearch[i].getValue('middlename'),
					LastName: employeeSearch[i].getValue('lastname'),
					Department: employeeSearch[i].getText('department'),
					LegalEntity: employeeSearch[i].getText('subsidiary'),
					Employee_InActive: employeeSearch[i].getValue('custentity_employee_inactive'),
					WorkEmail: employeeSearch[i].getValue('email'),
					Emp_Level: employeeSearch[i].getText('employeestatus'),
					AttendanceTracking: employeeSearch[i].getText('custentity_emp_attendance_tracking'),
					Phone: employeeSearch[i].getValue('phone'),
					HireDate: employeeSearch[i].getValue('hiredate'),
					TerminationDate: employeeSearch[i].getValue('custentity_lwd'),
					Gender: employeeSearch[i].getValue('gender'),
					Hourly_Salaried: employeeSearch[i].getText('employeetype'),
					ManagerEmail: employeeSearch[i].getValue('email','custentity_reportingmanager'),
					ManagerPersonNumber: employeeSearch[i].getValue('custentity_fusion_empid','custentity_reportingmanager'),
					//JSON:
					PersonID: employeeSearch[i].getValue('custentity_personid'),
					EmployerId: employeeSearch[i].getValue('custentity_employer_id'),
					AssignmentNumber: employeeSearch[i].getValue('custentity_assignment_num'),
					ManagerDisplayName: mgr_full_name,
					//Location
					BrillioLocation: employeeSearch[i].getValue('custentity_list_brillio_location_e'),
					LegalEntityFusion : employeeSearch[i].getValue('custentity_legal_entity_fusion'),
                    EmployeeCategory :employeeSearch[i].getValue('custentity_fresher')
					};
				dataRow.push(JSON);
			}		
		}
		return dataRow;	
	}	

	catch (err)
	{
		nlapiLogExecution('error', 'getAllActiveEmployeeName', err);
		throw err;
	}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}