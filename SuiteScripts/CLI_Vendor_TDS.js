// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:	CLI Vendor TDS
	Author:			Sachin Khairnar
	Company:		Aashna Cloudtech Pvt Ltd
	Date:			
	Version:		
	Description:	This Script checks the client side Validation on Vendor form


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.
	
	Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
      22 sep 2014           Supriya                    Kalpana                      Normal Account Validation
	  26 FEB 2015           Nikhil                     kalpana/sachin k             AIT enhancment points
	  30 July 2015          Nikhil                     sachinK                     Add a reneweal process
     PAGE INIT
		- pageInit(type)
        pageInit_TDS(type)

     SAVE RECORD
		- saveRecord()
		  NOT USED

     VALIDATE FIELD
		- validateField(type, name, linenum)
		  NOT USED

     FIELD CHANGED
		- fieldChanged(type, name, linenum)
          fieldChanged_TDS(type, name, linenum)

     POST SOURCING
		- postSourcing(type, name)
		  NOT USED

	LINE INIT
		- lineInit(type)
		  NOT USED

     VALIDATE LINE
		- validateLine()
		  NOT USED

     RECALC
		- reCalc()
		  NOT USED

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
           printdoc()
		   toWords(s)
           printnum1(num1)
           printnum2(num2)




*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...

	var operationType;
	var a_subisidiary = new Array;
	var i_vatCode;
	var i_taxcode;
	var s_pass_code;
}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_TDS(type){
	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES
	operationType = type;
	
	var i_AitGlobalRecId = SearchGlobalParameter();
	nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
	
	//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
	if (i_AitGlobalRecId != 0) {
		var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
		
		a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
		nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
		
		i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
		nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
		
		i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
		nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
		
		s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
	} // END if(i_AitGlobalRecId != 0 )
	//  PAGE INIT CODE BODY
	
	
	var i_line_count = nlapiGetLineItemCount('recmachcustrecord_vedname');
	
	if (i_line_count > 0 && i_line_count != null && i_line_count != '' && i_line_count != undefined) {
		nlapiDisableField('custentity_tds_assessee_code', true)
	}
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_TDS()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	
	//*****************************************************check the ao cerficate detail if apply ao is checked**************************
	/*
var a_subsidiary1 = a_subisidiary.toString();
    var Flag = 0;
	
	if (a_subsidiary1.indexOf(',') > -1) 	
	{
	 	// === IF COMMA FOUND ===
		for (var y = 0; y < a_subisidiary.length; y++)	
		 {
			var i_subsidiary = nlapiGetFieldValue('subsidiary')
			
			//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
			if (a_subisidiary[y] == i_subsidiary) 	
			{
			 Flag = 1;
			 break;
			}// END if (a_subisidiary[y] == i_subsidiary) 
			
		}//for (var y = 0; y < a_subisidiary.length; y++) 
		
	 }//if (a_subsidiary1.indexOf(',') > -1)
	 else 	
	 {
	  // === IF COMMA NOT FOUND ===
	   
	   var i_subsidiary = nlapiGetFieldValue('subsidiary')
			
		if (a_subsidiary1 == i_subsidiary)	
		{
            Flag = 1;
        }// END  if (a_subsidiary1 == i_subsidiary)	

	 }// END
	 
	 //===== FOR SINGLE SUBSIDARY ACCOUNT  =======
		var context = nlapiGetContext();
	    var i_subcontext = context.getFeature('SUBSIDIARIES');
		nlapiLogExecution('DEBUG','Test ', 'i_subcontext --> '+ i_subcontext )
				
		if(i_subcontext == false)
		{
		Flag = 1;
		}
   
*/
			var Flag = 0;
			Flag = isindia_subsidiary(a_subisidiary,s_pass_code)
  	        //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
			if (Flag == 1) 
			{
			
				var apply_ao = nlapiGetFieldValue('custentity_apply_ao_certificate')
				
				if (apply_ao == 'T') 
				{
					
					var ao_cerficate_number = nlapiGetFieldValue('custentity_ao_certificate_number')
					
					if (ao_cerficate_number == '' || ao_cerficate_number == null || ao_cerficate_number == undefined) 
					{
						alert('Please Enter the value of AO certificate number');
						return false;
					}
					
					var ao_from_date = nlapiGetFieldValue('custentity_ao_cert_from_date')
					
					if (ao_from_date == '' || ao_from_date == null || ao_from_date == undefined) 
					{
						alert('Please enter the value in AO certificate From date ');
						return false;
					}
					
					var ao_to_date = nlapiGetFieldValue('custentity_ao_cert_to_date')
					
					if (ao_to_date == '' || ao_to_date == null || ao_to_date == undefined) 
					{
						alert('Please enter  the value in AO certificate To Date');
						return false;
					}
					
					
					var o_from_date  = nlapiStringToDate(ao_from_date)
					
					var o_to_date = nlapiStringToDate(ao_to_date)
					
					if(o_from_date > o_to_date)
					{
						alert('AO certificate from date cannot be greater than to date')
						return false;
					}
				}
				
				var i_line_count = nlapiGetLineItemCount('recmachcustrecord_vedname')
				
				for (var i = 1; i <= i_line_count; i++) 
				{
					var ved_assessee_type = nlapiGetFieldValue('custentity_tds_assessee_code')
					
					var s_assessee_code = nlapiGetLineItemValue('recmachcustrecord_vedname', 'custrecord_vedassesseecode', i)
					
					if (s_assessee_code != null && s_assessee_code != '' && s_assessee_code != undefined) 
					{
						if (s_assessee_code != ved_assessee_type) 
						{
							alert('The Assesse code of TDS types and Assessee code on vendor are Different')
							return false;
						}
						
						
					}
					
				}
				
				var b_GST_Liable = nlapiGetFieldValue('custentity_iit_gst_liable');
				//alert(b_GST_Liable);
				if(b_GST_Liable == 'T')
				{
					var isNRIVendorCheck = nlapiGetFieldValue('custentity_iit_is_nri');
					if(isNRIVendorCheck != 'T')
					{
						var o_Address_SubRecord = nlapiViewLineItemSubrecord('addressbook', 'addressbookaddress', 1);
						if(_logValidation(o_Address_SubRecord))
						{
							var i_GST_State = o_Address_SubRecord.getFieldValue('custrecord_iit_address_gst_state');
							if(!_logValidation(i_GST_State))
							{
								alert('User needs to enter address as GST is Liable');
								return false;
							}
						}
						else
						{
							alert('User needs to enter atleast One Address as GST is Liable');
							return false;
						}
					}	// end if(isNRIVendorCheck != 'T')				
				}// end if(b_GST_Liable == 'T')				
			}
	//******************************************check the ao cerficate detail if apply ao is checked**************************
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField_TDS(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY
	
	/*if (name == 'custentity_vendor_panno') 
	{
		var b_Flag = 0; 
		
		var pan_no = nlapiGetFieldValue('custentity_vendor_panno');
		
		var patt = new RegExp("^[A-Z]{5}[0-9]{4}[A-Z]{1}$");
		
		if (pan_no != null && pan_no != '' && pan_no != undefined) 
		{
			if (patt.test(pan_no)) 
			{
			
			}
			else 
			{
				var b_Flag = 1;
				
				alert('Please enter The valid pan No ,\n * PAN Card Number Length 10 Character \n * First Five Character from A to Z \n * Four Numeric 0 to 9 \n * Last One Character from A to Z');
			
			}
			
			var filters = new Array();
			var columns = new Array();
			
			filters.push(new nlobjSearchFilter('custentity_vendor_panno',null,'is',pan_no))
			
			var recordID = nlapiGetRecordId();
			if (recordID != null && recordID != '' && recordID != undefined) {
				filters.push(new nlobjSearchFilter('internalid', null, 'noneOf',recordID))
			}
			
			columns.push(new nlobjSearchColumn('internalid'))
			
			var searchresult = nlapiSearchRecord('vendor',null,filters,columns)
			
			if(searchresult != null && searchresult != '' && searchresult != undefined)
			{
				var b_Flag = 1;
				
				var internalid = searchresult[0].getValue('internalid');
				
				alert('Vendor already exists with this Pan Number');
				
				//return false;
			}
			
		}
		
		if(b_Flag == parseInt(1))
		{
			nlapiSetFieldValue('custentity_vendor_panno', '');
		}
	}*/
	
	//Code on 21-04-2017 to validate GSTIN number field 
	if(name == 'custentity_iit_gstn_uid')
	{
		var b_Flag = 0;
		
		var s_GSTIN_No = nlapiGetFieldValue('custentity_iit_gstn_uid');
		
		var b_Check = new RegExp("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[A-Z]{1}[0-9]{1}$");
		
		if (s_GSTIN_No != null && s_GSTIN_No != '' && s_GSTIN_No != undefined) 
		{
			if (b_Check.test(s_GSTIN_No)) 
			{
			
			}
			else 
			{
				b_Flag = 1;
				
				alert('Please enter The valid GSTIN No ,\n * GSTIN Number Length 15 Character \n * First Two Numeric 0 to 9 \n * Five Character from A to Z \n * Four Numeric 0 to 9 \n * One Character from A to Z \n * One Numeric 0 to 9 \n * One Character A to Z \n * One Numeric 0 to 9');
				//return false;
			}
		
			var filters = new Array();
			var columns = new Array();
			
			filters.push(new nlobjSearchFilter('custentity_iit_gstn_uid',null,'is',s_GSTIN_No));
			
			var recordID = nlapiGetRecordId();
			if (recordID != null && recordID != '' && recordID != undefined) 
			{
				filters.push(new nlobjSearchFilter('internalid', null, 'noneOf',recordID));
			}
			
			columns.push(new nlobjSearchColumn('internalid'));
			
			var searchresult = nlapiSearchRecord('vendor',null,filters,columns);
			
			if(searchresult != null && searchresult != '' && searchresult != undefined)
			{
				b_Flag = 1;
				
				var internalid = searchresult[0].getValue('internalid');
				
				alert('Vendor already exists with this GSTIN Number');
				
			}
		}
		
		if(b_Flag == parseInt(1))
		{
			nlapiSetFieldValue('custentity_iit_gstn_uid', '');
		}
		
	}

	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_TDS(type, name, linenum){
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	
	// ==== CODE FOR GETTING THE GLOBAL SUBSIDIARY VALUES ===========
	/*
	 var a_subsidiary1 = a_subisidiary.toString();
	 var Flag = 0;
	 
	 
	 if (a_subsidiary1.indexOf(',') > -1)
	 {
	 // === IF COMMA FOUND ===
	 for (var y = 0; y < a_subisidiary.length; y++)
	 {
	 var i_subsidiary = nlapiGetFieldValue('subsidiary')
	 
	 //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
	 if (a_subisidiary[y] == i_subsidiary)
	 {
	 Flag = 1;
	 break;
	 }// END if (a_subisidiary[y] == i_subsidiary)
	 
	 }//for (var y = 0; y < a_subisidiary.length; y++)
	 
	 }//if (a_subsidiary1.indexOf(',') > -1)
	 else
	 {
	 // === IF COMMA NOT FOUND ===
	 
	 var i_subsidiary = nlapiGetFieldValue('subsidiary')
	 
	 if (a_subsidiary1 == i_subsidiary)
	 {
	 Flag = 1;
	 }// END  if (a_subsidiary1 == i_subsidiary)
	 }// END
	 
	 //===== FOR SINGLE SUBSIDARY ACCOUNT  =======
	 var context = nlapiGetContext();
	 var i_subcontext = context.getFeature('SUBSIDIARIES');
	 nlapiLogExecution('DEBUG','Test ', 'i_subcontext --> '+ i_subcontext )
	 
	 if(i_subcontext == false)
	 {
	 Flag = 1;
	 }
	 */
	if (name == 'custrecord_vedtdsapply' || name == 'custrecord_vedtdstype') {
		var Flag = 0;
		Flag = isindia_subsidiary(a_subisidiary, s_pass_code)
		//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
		if (Flag == 1) {
			var tdsapply = nlapiGetCurrentLineItemValue(type, 'custrecord_vedtdsapply')
			
			
			if (name == 'custrecord_vedtdsapply') {
			
				var tdsapply = nlapiGetCurrentLineItemValue(type, 'custrecord_vedtdsapply')
				
				if (tdsapply == 'T') {
					nlapiDisableLineItemField(type, 'custrecord_vedtdstype', false)
				}
				else {
					nlapiDisableLineItemField(type, 'custrecord_vedtdstype', true)
					
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedtdstype', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedassesseecode', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedsection', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedpaymentcode', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedtdsthreshold', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedsurchargethreshold', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedtdsaccount', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vednetper', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedempty_pan_tdsper', '')
					nlapiSetCurrentLineItemValue(type, 'custrecord_vedconecssion', '')
					nlapiSetCurrentLineItemValue(type, 'name', '')
					
				}
			}
			//End:
			
			if (name == 'custrecord_vedtdstype') {
				var tdstype = nlapiGetCurrentLineItemValue(type, 'custrecord_vedtdstype')
				var filters = new Array();
				var column = new Array();
				
				if(_logValidation(tdstype))
				{
					filters.push(new nlobjSearchFilter('internalid', null, 'is', tdstype));
				
				
					//Begin:Select the Fileds from TDS Master Record.
					
					column.push(new nlobjSearchColumn('custrecord_tdstype'));
					column.push(new nlobjSearchColumn('custrecord_assessecode'));
					column.push(new nlobjSearchColumn('custrecord_section'));
					column.push(new nlobjSearchColumn('custrecord_paymentcode'));
					column.push(new nlobjSearchColumn('custrecord_tdsthreshold'));
					column.push(new nlobjSearchColumn('custrecord_cumulativethreshold'));
					column.push(new nlobjSearchColumn('custrecord_tdsaccount'));
					column.push(new nlobjSearchColumn('custrecord_netper'));
					column.push(new nlobjSearchColumn('custrecord_ao_concession'));
					column.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
					column.push(new nlobjSearchColumn('name'));
					
					
					
					var results = nlapiSearchRecord('customrecord_tdsmaster', null, filters, column);
					
					if (results != null) {
						var section = results[0].getValue('custrecord_section');
						var payment_code = results[0].getValue('custrecord_paymentcode');
						var tdsthamount = results[0].getValue('custrecord_tdsthreshold');
						var scthamount = results[0].getValue('custrecord_cumulativethreshold');
						var net_per = results[0].getValue('custrecord_netper');
						var tds_acc = results[0].getValue('custrecord_tdsaccount')
						var assessee_code = results[0].getValue('custrecord_assessecode')
						var ao_concession = results[0].getValue('custrecord_ao_concession')
						var empty_vedpan = results[0].getValue('custrecord_empty_pan_tdsper')
						var name = results[0].getValue('name')
						
						//End:Search the Tds Record get thr values of that record
						
						nlapiSetCurrentLineItemValue(type, 'name', name)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedassesseecode', assessee_code)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedsection', section)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedpaymentcode', payment_code)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedtdsthreshold', tdsthamount)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedsurchargethreshold', scthamount)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedtdsaccount', tds_acc)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vednetper', net_per)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedconecssion', ao_concession)
						nlapiSetCurrentLineItemValue(type, 'custrecord_vedempty_pan_tdsper', empty_vedpan)
						
						
					}// end if(results != null)
				}
			}// END if(name=='custrecord_vedtdstype'&& tdsapply=='T')
			
		}// end if (a_subisidiary[y] == i_subsidiary) 	
	} // End if (name == 'custrecord_vedtdsapply' || name == 'custrecord_vedtdstype' || name == 'custentity_ait_popup_tdslist')
	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing_TDS(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit_TDS(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_TDS(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY
	/*
var a_subsidiary1 = a_subisidiary.toString();
   var Flag = 0;
	 
	
	 if (a_subsidiary1.indexOf(',') > -1) 	
	 {
	 	// === IF COMMA FOUND ===
		for (var y = 0; y < a_subisidiary.length; y++)	
		 {
			var i_subsidiary = nlapiGetFieldValue('subsidiary')
			
			//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
			if (a_subisidiary[y] == i_subsidiary) 	
			{
			 Flag = 1;
			 break;
			}// END if (a_subisidiary[y] == i_subsidiary) 
			
		}//for (var y = 0; y < a_subisidiary.length; y++) 
		
	 }//if (a_subsidiary1.indexOf(',') > -1)
	 else 	
	 {
	  // === IF COMMA NOT FOUND ===
	   
	   var i_subsidiary = nlapiGetFieldValue('subsidiary')
			
		if (a_subsidiary1 == i_subsidiary)	
		{
            Flag = 1;
        }// END  if (a_subsidiary1 == i_subsidiary)	

	 }// END
	 
	 //===== FOR SINGLE SUBSIDARY ACCOUNT  =======
		var context = nlapiGetContext();
	    var i_subcontext = context.getFeature('SUBSIDIARIES');
		nlapiLogExecution('DEBUG','Test ', 'i_subcontext --> '+ i_subcontext )
				
		if(i_subcontext == false)
		{
		Flag = 1;
		}
   
*/
 			var Flag = 0;
			Flag = isindia_subsidiary(a_subisidiary,s_pass_code)
  	        //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
			
			//*********************************validate the vendor type**********************************************
			if (Flag == 1)
			 {
			 	
				if (type == 'recmachcustrecord_vedname') 
				{
					
					var ved_assessee_type = nlapiGetFieldValue('custentity_tds_assessee_code')
					
					var s_assessee_code = nlapiGetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedassesseecode')
					
					if (s_assessee_code != null && s_assessee_code != '' && s_assessee_code != undefined) {
						if (ved_assessee_type != null && ved_assessee_type != '' && ved_assessee_type != undefined) {
							if (s_assessee_code != ved_assessee_type) {
								alert('The Assesse code of selected TDS type and Assessee code on vendor are Different , Please select the TDS type whose Assessee code are same')
								return false;
							}
						}
						else {
							alert('Please select the Assessee code on Vendor')
							return false;
						}
					}
					
					nlapiDisableField('custentity_tds_assessee_code',true)
					
				}
			}//end of flag

	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc_TDS(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}
function validate_remove_asscesse_code(type)
{
	if (type == 'recmachcustrecord_vedname') 
	{
		var i_line_count = nlapiGetLineItemCount('recmachcustrecord_vedname');
		
		if (i_line_count > 1 && i_line_count != null && i_line_count != '' && i_line_count != undefined) 
		{
			//alert('line count'+i_line_count)

			nlapiDisableField('custentity_tds_assessee_code', true)
		}
		else
		{
			//alert('empty line')
			nlapiDisableField('custentity_tds_assessee_code', false)
		}
	}
	
	return true;
}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================
function printdoc()
{
	  //Begin:Get the Deductor Detail.
		var ded_filters = new Array();
    	var ded_column= new Array();

       
		ded_column.push( new nlobjSearchColumn('custrecordcustrecord_tdsinitialsetup_per'));
		ded_column.push( new nlobjSearchColumn('custrecordcustrecord_tdsinitialsetup_ded'));

   		 var ded_results = nlapiSearchRecord('customrecord_setupcompany', null, null, ded_column);
		 
		 if(ded_results != null)
		 {
		 	var ded_name=ded_results[0].getValue('custrecordcustrecord_tdsinitialsetup_per')
			var ded_type=ded_results[0].getValue('custrecordcustrecord_tdsiitialsetup_ded')
	       //End:Get the Deductor Detail.
	
	        //Begin:Get the Employee Detail
	      		  var emp_filters = new Array();
	    		  var emp_column= new Array();
				  
		    	  emp_filters.push( new nlobjSearchFilter('internalid', null, 'is',ded_name ));
	
	              emp_column.push( new nlobjSearchColumn('entityid'));
	
	   		      var ded_results = nlapiSearchRecord('employee', null, emp_filters, emp_column);
				  
				  if(ded_results != null)
				  {
				  	 var ded_name=ded_results[0].getValue('entityid')
				  }
	
	        //End:Get the Employee Detail
	
	        //End:Get the Deductor Detail.
		 	
		 }// end if(ded_results != null)
		 
		 

	 //Begin:Get the Company Detail.
		var com_filters = new Array();
    	var com_column= new Array();
          
		com_column.push(  new nlobjSearchColumn('custrecord_companyname'))
		com_column.push(  new nlobjSearchColumn('custrecord_shippingaddress'))
		com_column.push(  new nlobjSearchColumn('custrecord_tdscompany_panno'))
   		 var com_results = nlapiSearchRecord('customrecord_companyinfo', null, null, com_column);
		 
		 if(com_results != null)
		 {
		 	var com_name=com_results[0].getValue('custrecord_companyname')
			var com_add=com_results[0].getValue('custrecord_shippingaddress')
			var com_Panno=com_results[0].getValue('custrecord_tdscompany_panno')
		 	
		 }
		 
    //End:Get the Company Detail.

	 //Get The Vendor Detail
     var vid=nlapiGetRecordId()
     var vname = nlapiGetFieldValue('entityid')
	 var address=nlapiGetFieldValue('defaultaddress')
	 var ved_panno= nlapiGetLineItemValue('recmachcustrecord_vendortax','custrecord_tax_panno',1)
   //Begin: code for finding Bill Record
    var filters = new Array();
    var column= new Array();
     filters.push(  new nlobjSearchFilter('custrecord_bill_vendorrel', null, 'is',vid ));
     filters.push(  new nlobjSearchFilter('custrecord_bill_status', null, 'is','Close' ));
   
	//Begin:Select the Fileds from TDS Master Record.
	 column.push(  new nlobjSearchColumn('custrecord_bill_billno'))
	 column.push( new nlobjSearchColumn('custrecord_bill_billamount'))
	 column.push( new nlobjSearchColumn('custrecord_bill_tdsamount'))
	 column.push( new nlobjSearchColumn('custrecord_bill_date'))
	 column.push( new nlobjSearchColumn('custrecord_bill_tdssection'))
	 column.push( new nlobjSearchColumn('custrecord_bill_bankname'))
    var rowline='';
    var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, filters, column);
    var totaltdsamt=parseFloat(0.0)

    for(var i=0;i<results.length && results!= null ;i++)
    {
		var billno=results[i].getValue('custrecord_bill_billno')
		var billamt=results[i].getValue('custrecord_bill_billamount')
		var tdsamt=results[i].getValue('custrecord_bill_tdsamount')
		var billdate=results[i].getValue('custrecord_bill_date')
        var section=results[i].getValue('custrecord_bill_tdssection')
        var bankname=results[i].getValue('custrecord_bill_bankname')
		totaltdsamt=totaltdsamt+parseFloat(tdsamt)

	     rowline=rowline+'<tr>'
		+ '<td height="25" valign="top" width="10%">'+billdate+'</td>'
		+ '<td height="25" valign="top" width="10%">'+billamt+'</td>'
		+ '<td height="25" valign="top" width="10%">'+tdsamt+'</td>'
		+ '<td height="25" valign="top" width="10%">'+section+'</td>'
		+ '<td height="25" valign="top" width="10%">'+billno+'</td>'
		+ '<td height="25" valign="top" width="10%">'+bankname+'</td>'
		+ '</tr>'
    }
   //End:code for finding Bill Record

    //Begin:code to convert tds amount into word
        amount=totaltdsamt.toString()

     	var amtstr=toWords(amount)
		var p=0
     	p=amtstr.search('And')
		if(p!=-1)
		{
			amtstr=amtstr+'Paisaa '
		}
		amtstr=amtstr+'Only'
		var amtstring=amtstr


   //End:code to convert tds amount into word
	 myWindow=window.open('','')

	 myWindow.document.write('<html><head><title>FORM NO 16a</title></head><body><p align="center"><font size="4">FORM NO. 16A<br></font><font size="2">[see rule 31(1) (c)]</font> <br>'
+ '<font size="2"><b>Certificate of deduction of tax at source under section 203 of the Income-tax Act, 1961</b></font> </p>'
+ '<p><font size="2">[For interest on securities; dividends; interest other than &#8216;&#8216;interest on securities&#8217;&#8217;; winnings from lottery or crossward'
+ 'puzzle; winnings from horse race; payments to contractors and sub-contractors; insurance commission; payments to non-resident'
+ 'sportsman / sports associations; payments in respect of deposits under National Savings Scheme; payments on account of repurchase'
+ 'of units by Mutual Fund or Unit Trust of India; commission,remunerationor prize on sale of lottery tickets; [rent] [fees for'
+ 'professional or technical services; income in respect of units;]other sums under section 195; income of foreign companies'
+ 'referred to in section 196A(2); income from units referred to in section 196B;</font> <br> &nbsp; </p>'

+ '<table border="1" cellpadding="0" cellspacing="0" width="60%">'
+ '    <tbody><tr>'
+ '        <td valign="top" width="48%"><font size="2">TDS'
+ '        CERTIFICATE NO.</font></td>'
+ '        <td valign="top">****</td>'
+ '    </tr>'
+ '</tbody></table>'

+ '<p>&nbsp; </p>'

+ '<table border="1" cellpadding="0" cellspacing="0" width="100%">'
+ '    <tbody><tr>'
+ '        <td valign="top"><font size="2">Name and address of the'
+ '        persondeducting tax</font> </td>'
+ '        <td colspan="2" valign="top" width="33%"><font size="2">TDS'
+ '        circle where Annual Return under section 206 is to be'
+ '        delivered</font> </td>'
+ '        <td colspan="6" valign="top" width="33%"><font size="2">Name'
+ '        and address of the person to whom payment made or in'
+ '        whose account it is credited</font> </td>'
+ '    </tr>'
+ '    <tr>'
+ '        <td valign="top" width="34%"><b>'+com_name+'</b><br>'+ com_add+'</td>'
+ '        <td colspan="2" valign="top" width="33%">&nbsp;</td>'
+ '        <td colspan="6" valign="top" width="33%"><b>'+ vname +'</b><br>'+address+'</td>'
+ '    </tr>'
+ '    <tr>'
+ '        <td valign="top" width="34%">&nbsp;</td>'
+ '        <td colspan="2" valign="top" width="33%">&nbsp;</td>'
+ '        <td colspan="6" valign="top" width="33%">&nbsp;</td>'
+ '    </tr>'
+ '    <tr>'
+ '        <td colspan="2" height="7" valign="top" width="40%"><font size="2">TAX DEDUCTION A/C NO. OF THE DEDUCTOR</font> </td>'
+ '        <td rowspan="3" colspan="2" height="7" valign="top" width="30%"><font size="2">NATURE OF&nbsp;</font> <font size="2">PAYMENT</font> </td>'
+ '        <td colspan="5" height="7" valign="top" width="30%"><font size="2">PAN/ GIR NO. OF THE&nbsp;</font> <font size="2">PAYEE:<b>'+ved_panno+'</b></font>'
+ '        </td>'
+ '    </tr>'
+ '    <tr>'
+ '        <td rowspan="2" colspan="2" height="7" valign="top" width="40%"><font size="2">PAN/ GIR NO. OF THE DEDUCTOR :<b>'+com_Panno+'</b></font>'
+ '        </td>'
+ '        <td colspan="3" height="7" valign="top" width="21%"><font size="2">FOR THE PERIOD</font> </td>'
+ '        <td colspan="2" height="7" valign="top" width="10%">&nbsp;</td>'
+ '    </tr>'
+ '   <tr>'
+ '        <td height="7" valign="top" width="8%"><font size="2">19</font>'
+ '        </td>'
+ '        <td height="7" valign="top" width="6%">&nbsp;</td>'
+ '        <td colspan="2" height="7" valign="top" width="10%"><font size="2">TO 19</font> </td>'
+ '        <td height="7" valign="top" width="6%">&nbsp;</td>'
+ '    </tr>'
+ '</tbody></table>'

+ '<p align="center"><font size="2"><b>DETAILS OF PAYMENT, TAX'
+ 'DEDUCTION AND DEPOSIT OF TAX INTO</b></font> <font size="2"><b>CENTRAL'
+ 'GOVERNMENT ACCOUNT</b></font> &nbsp; </p>'

+ '<table border="1" cellpadding="0" cellspacing="0" width="100%">'
+ '    <tbody><tr>'
+ '       <td valign="top"><font size="2">Date of payment / credit</font></td>'
+ '        <td valign="top" width="17%"><font size="2">Amount paid/'
+ '        credited (Rs)</font></td>'
+ '        <td valign="top" width="17%"><font size="2">Amount of'
+ '        income-tax deducted (Rs)</font></td>'
+ '        <td valign="top" width="17%"><font size="2">Rate of which'
+ '        deducted</font></td>'
+ '        <td valign="top" width="17%"><font size="2">Date and'
+ '        Challan No. of deposit of tax into Central Government'
+ '        Account</font></td>'
+ '        <td valign="top" width="17%"><font size="2">Name of bank'
+ '        and branch where tax deposited</font></td>'
+ '    </tr>'
// '    <tr>'
//+ '        <td height="71" valign="top" width="17%">bill no</td>'
//+ '        <td height="71" valign="top" width="17%">amount</td>'
//+ '        <td height="71" valign="top" width="17%">tds</td>'
//+ '        <td height="71" valign="top" width="17%">rate</td>'
//+ '        <td valign="top">bill no11</td>'
//+ '        <td height="71" valign="top" width="17%">bank</td>'
//+ '    </tr>
+''+ rowline+''
+ '</tbody></table>'

+ '<p>&nbsp; <br>'
+ '&nbsp; </p>'

+ '<table border="0" cellpadding="0" cellspacing="0" width="100%">'
+ '    <tbody><tr>'
+ '        <td valign="top" width="40%"><font size="2">Certified'
+ '        that a sum of Rs.(in words) </font></td>'
+ '        <td valign="top" width="20%"><font size="2">'+amtstring+'</font></td>'
+ '        <td valign="top" width="20%"><font size="2"> </font></td>'
+ '    </tr>'
+ '</tbody></table>'

+ '<p><font size="2">Has been deducted at source and to the credit of the'
+ 'Central Government as per details given above.</font> <br>'
+ '<br>'
+ '</p>'

+ '<table border="0" cellpadding="7" cellspacing="0" width="199">'
+ '    <tbody><tr>'
+ '       <td valign="top" width="31%"><font size="2">Place</font></td>'
+ '        <td valign="top" width="69%"></td>'
+ '    </tr>'
+ '    <tr>'
+ '        <td valign="top" width="31%"><font size="2">Date</font></td>'
+ '        <td valign="top" width="69%"></td>'
+ '    </tr>'
+ '</tbody></table>'

+ '<table border="0" cellpadding="0" cellspacing="0" width="100%">'
+ '    <tbody><tr>'
+ '        <td align="right" valign="top">&nbsp;</td>'
+ '    </tr>'
+ '    <tr>'
+ '        <td valign="top"><font size="2">Signature of person'
+ '       responsible for deduction of tax</font> </td>'
+ '    </tr>'
+ '</tbody></table>'

+ '<p>&nbsp;</p>'

+ '<table border="0" cellpadding="0" cellspacing="0" width="100%">'
+ '    <tbody><tr>'
+ '        <td valign="top">&nbsp;</td>'
+ '        <td valign="top" width="78%"><font size="2">Full Name :<B>'+ded_name+'</B></font></td>'
+ '   </tr>'
+ '    <tr>'
+ '        <td align="right" valign="top" width="22%">&nbsp;</td>'
+ '        <td valign="top" width="78%"><font size="2">Designation :</font></td>'
+ '    </tr>'
+ '</tbody></table>'

+ '<p align="center">&nbsp;</p>'

+ '</body></html>'
	 )
	 myWindow.document.close()
}

//Begin : code for convert given no into word
function toWords(s)
{
    	var str='Rs.'
    	var th  = new Array ('Corer ','Lakhs ','Thousand ','Hundered ');

	 	var dg = new Array ('10000000','100000','1000','100');
		var dem=s.substr(s.lastIndexOf('.')+1)
		s=parseInt(s)
		var d
		var n1,n2
		while(s>=100)
		{

			for(var k=0;k<4;k++)
			{
				d=parseInt(s/dg[k])

				if(d>0)
				{

					if(d>=20)
					{
					n1=parseInt(d/10)
					n2=d%10
					printnum2(n1)
					printnum1(n2)
					}
			  	else
			  		printnum1(d)
		     		str=str+th[k]

		    	}
		    	s=s%dg[k]
			}
		}
 	if(s>=20)
 	{
 		n1=parseInt(s/10)
	 	n2=s%10
 	}
 	else
 	{
 	n1=0
 	n2=s
 	}
		printnum2(n1)
		printnum1(n2)
	if(dem>0)
	{
		decprint(dem)
	}
	return str

	function decprint(nm)
	{
		  if(nm>=20)
     		{
	 	    	n1=parseInt(nm/10)
		 	    n2=nm%10
     		}
  			else
     		{
 	  	 		n1=0
 	   			n2=parseInt(nm)
     		}
     	str=str+'And '
		printnum2(n1)
		printnum1(n2)
	}
function printnum1(num1)
{


	switch(num1)
	{
  case 1:str=str+'One '
         break;
  case 2:str=str+'Two '
         break;
  case 3:str=str+'Three '
        break;
  case 4:str=str+'Four '
         break;
  case 5:str=str+'Five '
         break;
  case 6:str=str+'Six '
         break;
  case 7:str=str+'Seven '
         break;
  case 8:str=str+'Eight '
         break;
  case 9:str=str+'Nine '
         break;
  case 10:str=str+'Ten '
         break;
  case 11:str=str+'Eleven '
        break;
  case 12:str=str+'Twelve '
         break;
  case 13:str=str+'Thirteen '
         break;
  case 14:str=str+'Fourteen '
         break;
  case 15:str=str+'Fifteen '
         break;
  case 16:str=str+'Sixteen '
         break;
  case 17:str=str+'Seventeen '
         break;

  case 18:str=str+'Eighteen '
         break;
  case 19:str=str+'Nineteen '
         break;

	}

}
function printnum2(num2)
{
	//alert('in print 2'+num2)

switch(num2)
	{

  case 2:str=str+'Twenty '
         break;
  case 3:str=str+'Thirty '
        break;
  case 4:str=str+'Forty '
         break;
  case 5:str=str+'Fifty '
         break;
  case 6:str=str+'Sixty '
         break;
  case 7:str=str+'Seventy '
         break;
  case 8:str=str+'Eighty '
         break;
  case 9:str=str+'Ninety '
         break;

	}
	//alert('str in loop2'+str)
}




}
//End : Code for Convert amount into word


function SearchGlobalParameter()
{
	
	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	
	
	return i_globalRecId;
}
function setTDSRelationLine(a_tds_id)
{
	if (a_tds_id != null && a_tds_id != '' && a_tds_id != undefined) 
	{
		var a_id = a_tds_id.split(',')
		
		for (i = 0; i < a_id.length; i++) 
		{
			/*
			 nlapiSelectNewLineItem('recmachcustrecord_vedname')
			 nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdsapply', 'T', true, true)
			 nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdstype', a_id[i], true, true)
			 nlapiCommitLineItem('recmachcustrecord_vedname')
			 */
			var filters = new Array();
			var column = new Array();
			
			filters.push(new nlobjSearchFilter('internalid', null, 'anyOf', a_id));
			
			//Begin:Select the Fileds from TDS Master Record.
			
			column.push(new nlobjSearchColumn('custrecord_tdstype'));
			column.push(new nlobjSearchColumn('custrecord_assessecode'));
			column.push(new nlobjSearchColumn('custrecord_section'));
			column.push(new nlobjSearchColumn('custrecord_paymentcode'));
			column.push(new nlobjSearchColumn('custrecord_tdsthreshold'));
			column.push(new nlobjSearchColumn('custrecord_cumulativethreshold'));
			column.push(new nlobjSearchColumn('custrecord_tdsaccount'));
			column.push(new nlobjSearchColumn('custrecord_netper'));
			column.push(new nlobjSearchColumn('custrecord_ao_concession'));
			column.push(new nlobjSearchColumn('custrecord_empty_pan_tdsper'));
			column.push(new nlobjSearchColumn('name'));
			column.push(new nlobjSearchColumn('internalid'));
			
			var results = nlapiSearchRecord('customrecord_tdsmaster', null, filters, column);
			
			if (results != null && results != '' && results != undefined) 
			
				for (var i = 0; i < results.length; i++) {
					var section = results[i].getValue('custrecord_section');
					var payment_code = results[i].getValue('custrecord_paymentcode');
					var tdsthamount = results[i].getValue('custrecord_tdsthreshold');
					var scthamount = results[i].getValue('custrecord_cumulativethreshold');
					var net_per = results[i].getValue('custrecord_netper');
					var tds_acc = results[i].getValue('custrecord_tdsaccount')
					var assessee_code = results[i].getValue('custrecord_assessecode')
					var ao_concession = results[i].getValue('custrecord_ao_concession')
					var empty_vedpan = results[i].getValue('custrecord_empty_pan_tdsper')
					var name = results[i].getValue('name')
					var internalid = results[i].getValue('internalid')
					
					//End:Search the Tds Record get thr values of that record
					
					nlapiSelectNewLineItem('recmachcustrecord_vedname')
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdsapply', 'T', false, false)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdstype', internalid, false, false)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'name', name)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedassesseecode', assessee_code)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedsection', section)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedpaymentcode', payment_code)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdsthreshold', tdsthamount)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedsurchargethreshold', scthamount)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdsaccount', tds_acc)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vednetper', net_per)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedconecssion', ao_concession)
					nlapiSetCurrentLineItemValue('recmachcustrecord_vedname', 'custrecord_vedempty_pan_tdsper', empty_vedpan)
					nlapiCommitLineItem('recmachcustrecord_vedname')
					
				}// end if(results != null)
		}
	}
}
function addmultipletdsline()
{
	var a_tds_master = new Array();
	
	var ved_assessee_type = nlapiGetFieldValue('custentity_tds_assessee_code')
	
	var i_line_count = nlapiGetLineItemCount('recmachcustrecord_vedname')
	
	for (var i = 1; i <= i_line_count; i++) {
		var i_tds_master = nlapiGetLineItemValue('recmachcustrecord_vedname', 'custrecord_vedtdstype', i)
		a_tds_master.push(i_tds_master);
	}
	
	if (ved_assessee_type != null && ved_assessee_type != '' && ved_assessee_type != undefined) {
		var url = nlapiResolveURL('SUITELET', 'customscript_ait_pop_up_tds_list', '1')
		
		var urlwithparam = url + '&ved_assessee_type=' + ved_assessee_type + '&a_tds_master=' + a_tds_master;
		
		window.open(urlwithparam, '_blank', " top=200, left=300,height = 400,width = 800");
	}
	
}

function _logValidation(value)
{
	if(value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN' && value != ' ')
	{
		return true;
	}
	else
	{
		return false;
	}
}
// END FUNCTION =====================================================