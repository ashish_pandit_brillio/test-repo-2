

function afterSubmit_UpdateProjDesc_sch(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--

    */

	//  LOCAL VARIABLES
	
	var submit_record_flag = 0;

	//  AFTER SUBMIT CODE BODY
	
	try
	{
		var a_results = nlapiSearchRecord(null, 'customsearch1441', null, null);
		for (var m = 0; m < a_results.length; m++) //
		{
			nlapiLogExecution('debug', 'a_results.length:-- ', a_results.length);
			var ait_taxation_rcrd = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', 1);
			
			var stRoundMethod = ait_taxation_rcrd.getFieldValue('custrecord_ait_st_roundoff');
			
			var vendor_bill_rcrd = nlapiLoadRecord('vendorbill', a_results[m].getId());
			
			var is_subtier_bill = vendor_bill_rcrd.getFieldValue('custbody_is_subtier_bill');
			nlapiLogExecution('debug', 'is_subtier_bill:-- ' + is_subtier_bill, a_results[m].getId());
			if (is_subtier_bill == 'T') {
				var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
				
				
				for (var j = 1; j <= expense_line_count; j++) {
					var amount = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
					var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
					taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
					
					for (var q = j; q <= expense_line_count; q++) {
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', q);
						if (is_swach_cess_reverse_true == 'T') {
							var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							
							var amount_sbc = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
							if (parseFloat(amount_sbc) == parseFloat(taxAmount)) {
								var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
								var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
								var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
								vendor_bill_rcrd.commitLineItem('expense');
								
								submit_record_flag = 1;
							}
							else {
								if (taxAmount > parseFloat(0)) 
									taxAmount = '-' + taxAmount;
								
								if (parseFloat(amount_sbc) == parseFloat(taxAmount)) {
									var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
									var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
									var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
									var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
									
									vendor_bill_rcrd.selectLineItem('expense', q);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
									vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
									
								}
								taxAmount = Math.abs(taxAmount);
							}
						}
						else {
							var is_tds_line_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_tdsline', q);
							if (is_tds_line_true == 'T') {
								var taxAmount = parseFloat(amount) * (parseFloat(10) / 100);
								taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
								taxAmount = '-' + taxAmount;
								var amount_tds = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
								
								if (parseFloat(amount_tds) == parseFloat(taxAmount)) {
									var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
									var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
									var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
									var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
									
									vendor_bill_rcrd.selectLineItem('expense', q);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
									vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
									
								}
							}
						}
					}
				}
			}
			else {
				var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
				
				var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
				
				
				for (var j = 1; j <= expense_line_count; j++)
				{
					var amount = vendor_bill_rcrd.getLineItemValue('expense', 'amount', j);
					/*var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
					taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);*/
					
					for (var q = j; q <= expense_line_count; q++)
					{
						var is_swach_cess_reverse_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', q);
						
						if (is_swach_cess_reverse_true == 'T')
						{
							var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
							taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
							nlapiLogExecution('audit','tax amnt:-- ',taxAmount);
							var amount_sbc = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
							nlapiLogExecution('audit','amount_sbc:-- ',amount_sbc);
							if (parseFloat(amount_sbc) == parseFloat(taxAmount))
							{
								var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
								var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
								var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
								var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
								
								vendor_bill_rcrd.selectLineItem('expense', q);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
								vendor_bill_rcrd.commitLineItem('expense');
								
								submit_record_flag = 1;
							}
							else
							{
								if (taxAmount > parseFloat(0)) 
									taxAmount = '-' + taxAmount;
								
								if (parseFloat(amount_sbc) == parseFloat(taxAmount))
								{
									var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
									var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
									var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
									var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
									
									vendor_bill_rcrd.selectLineItem('expense', q);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
									vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
									
								}
								taxAmount = Math.abs(taxAmount);
							}
						}
						else
						{
							var is_tds_line_true = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_tdsline', q);
							if (is_tds_line_true == 'T')
							{
								var taxAmount = parseFloat(amount) * (parseFloat(10) / 100);
								taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
								taxAmount = '-' + taxAmount;
								var amount_tds = vendor_bill_rcrd.getLineItemValue('expense', 'amount', q);
								
								if (parseFloat(amount_tds) == parseFloat(taxAmount)) {
									var cust_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcolcustcol_temp_customer', j);
									var proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
									var emp_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
									var territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory', j);
									
									vendor_bill_rcrd.selectLineItem('expense', q);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_desc);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', emp_name);
									vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
									vendor_bill_rcrd.commitLineItem('expense');
									
									submit_record_flag = 1;
									
								}
							}
						}
					}
				}
			}
			
			if (submit_record_flag == 1) {
				var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd, true, true);
				nlapiLogExecution('debug', 'submitted bill id:-- ', vendor_bill_submitted_id);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',e);
	}
	
	return true;

}

// END AFTER SUBMIT ===============================================



// BEGIN FUNCTION ===================================================
{

function applySTRoundMethod(stRoundMethod, taxamount)
{
	var roundedSTAmount = taxamount;
	
	if (stRoundMethod == 2) {
		roundedSTAmount = Math.round(taxamount)
	}
	if (stRoundMethod == 3) {
		roundedSTAmount = Math.round(taxamount / 10) * 10;
	}
	if (stRoundMethod == 4) {
		roundedSTAmount = Math.round(taxamount / 100) * 100;
	}
	
	return roundedSTAmount;
}

}
// END FUNCTION =====================================================
