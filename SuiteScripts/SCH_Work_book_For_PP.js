// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

/*
		Script Name		:	SCH_Work_book_For_PP.js
		Author			: 	Anukarann	
		Company			:	Inspirria Cloudtech Pvt Ltd
		Created Date	:	21 Aug 2020
		Description		:	Getting Values from suitelet and this script will generate csv file using workbook details and calculation as per given below.and changes also added
		
		If Cost head base= 'Revenue' and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / USD-INR-REVENUE-RATE
		If Cost head base IN ("Salary Cost","Other Cost") and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / USD-INR COST RATE
		IF  subsidiary = UK  THEN USD-TO-GBP / AMOUNT
		IF  subsidiary = Canada THEN USD-TO-CAD / AMOUNT
		IF  subsidiary = Cognetik Srl THEN USD-TO-RON / AMOUNT
		IF  subsidiary IN (Inc, BLLC,Cognetik Corp,Comity Inc.) THEN AMOUNT
		
		subsidiary    														 internal id
		Brillio Technologies Private Limited									3
		Brillio LLC																2
		Comity Designs, Inc.													8
		Cognetik Srl															23
		Cognetik Corp															21
		BRILLIO UK LIMITED														7
		Comity Designs Private Limited											9
		Brillio Nordics															5
		Brillio LLC - Elimination												4
		Brillio Costa Rica Sociedad De Responsabilidad Limitada					6
		Brillio Canada Inc.														10
		Brillio Inc																1
		
		
		currency                           internal id
		USD										1
		GBP										2
		CAD										3
		EUR									    4
		Peso									5
		INR										6
		SGD										7
		NOK										8
		CRC										9
		AUD										10
		RON										11
		
		
		
		
		
		
		Script Modification Log:
		
	
		-- Date --		-- Modified By --			--Requested By--				-- Description --
		
}
// END SCRIPT DESCRIPTION BLOCK  ====================================
*/

/**
 * @NApiVersion 2.0
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/runtime', 'N/record', 'N/format', 'N/encode', 'N/email', 'N/file', 'N/task', 'N/query', 'N/error', 'N/query'],
    /**
     * @param {search} search
     */
    function(search, runtime, record, format, encode, email, file, task, query, error, query) {

        /**
         * Definition of the Scheduled script trigger point.
         *
         * @param {Object} scriptContext
         * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
         * @Since 2015.2
         */
        function schedule_workbook(scriptContext) {
            try {
                var scriptObj = runtime.getCurrentScript();
                log.debug("Remaining governance units start: ", scriptObj.getRemainingUsage());

                log.debug('sheduled script getting data from Suitelet');

                var usd_inr_revenue_Rate = 0;
                var usd_inr_cost_Rate = 0;
                var crc_cost_Conversion = 0;
                var nok_cost_Rate = 0
                var usd_to_GBP = 0;
                var usd_to_EUR = 0;
                var usd_to_AUD = 0;
                var total_USD = 0;
				var usd_to_CAD = 0;
				var usd_to_RON = 0;
				
				    var usd_Amount_total = 0;
					var usd_inr_cost_Rate_Amount = 0;
					var usd_to_EUR_total = 0;
					var usd_to_AUD_total = 0;
					var usd_to_RON_total = 0;
					var usdinr_rate_total = 0;
					var usd_inr_total = 0 ;
					var usd_inr_cost_tot = 0;


                //var from_date = "07/1/2020";
                var month = runtime.getCurrentScript().getParameter({
                    name: 'custscript__month'
                });
                log.debug('workbook_Report_PP----month=>', month);
					
					/// Added by Shravan 
				var cb_Check_box = runtime.getCurrentScript().getParameter({
                    name: 'custscript_checkbox_yearly'
                });
                log.debug('workbook_Report_PP----cb_Check_box=>', cb_Check_box);
					///// End
					
                //var to_date = "7/30/2020";
                var year = runtime.getCurrentScript().getParameter({
                    name: 'custscript__year'
                });
                log.debug('workbook_Report_PP----year=>', year);
			
/************************* New Formula SQL Added on 01 March by Shravan ***************************************/
// Query definition 
var objQ = query.create({type: "transaction"}); 

// Conditions 
objQ.condition = objQ.and(
 objQ.createCondition({"operator": "IS","values": [true],"fieldId": "posting"}),
                    objQ.createCondition({"operator": "ANY_OF","values": ["COGS", "Expense", "Income", "OthExpense", "OthIncome"],"fieldId": "transactionlines.accountingimpact.account.accttype"}),
					objQ.createCondition({"operator":"EQUAL_NOT","values":[0],"fieldId":"transactionlines.foreignamount"}), //Added on 08/10/2020
					objQ.createCondition({"formula": "to_char({trandate},'MM')","type": "STRING","operator": "ANY_OF","values": [month]}),
                    objQ.createCondition({"formula": "TO_CHAR({trandate},'YYYY')","type": "STRING","operator": "IS","values": [year]})
  ); 

// Columns 
		objQ.columns = [
	objQ.createColumn({
		"alias": "Account",
		"fieldId": "transactionlines.accountingimpact.account"
	}),
	//objQ.createColumn({"alias":"Account2","formula":"{transactionlines.accountingimpact.account.acctname}","type":"STRING"}),
	objQ.createColumn({
		"alias": "Account3",
		"fieldId": "transactionlines.accountingimpact.account.accountsearchdisplayname"
	}),

	objQ.createColumn({
		"alias": "Account Type",
		"fieldId": "transactionlines.accountingimpact.account.accttype"
	}), //code added on 28/09/2020

	objQ.createColumn({
		"alias": "Type",
		"fieldId": "transactionlines.accountingimpact.account.accttype"
	}),
	objQ.createColumn({
		"alias": "Type1",
		"fieldId": "type"
	}),
	objQ.createColumn({
		"alias": "Date",
		"fieldId": "trandate"
	}),
	objQ.createColumn({
		"alias": "Document Number/ID",
		"fieldId": "tranid"
	}),


	objQ.createColumn({
		"alias": "Name",
		"formula": "{entity#display}",
		"type": "STRING"
	}), //Added on 30/09/2020


	objQ.createColumn({
		"alias": "Amount",
		"formula": "CASE WHEN {transactionlines.accountingimpact.account.id} =580 THEN{transactionlines.accountingimpact.amount}*-1 WHEN{transactionlines.accountingimpact.account.accttype#display}IN('Income') THEN {transactionlines.accountingimpact.amount}*-1 ELSE {transactionlines.accountingimpact.amount}END",
		"type": "FLOAT"
	}), //Removed the Other Income with reversal sign on 27/10/2020

	objQ.createColumn({
		"alias": "Amount (Transaction Currency)",
		"fieldId": "transactionlines.foreignamount"
	}),


	objQ.createColumn({
		"alias": " Amount-USD ",
		"formula": "1",
		"type": "FLOAT"
	}),

	// objQ.createColumn({"alias":"Currency1","fieldId":"currency"}),
	objQ.createColumn({
		"alias": "CurrencyId",
		"fieldId": "currency"
	}),
	objQ.createColumn({
		"alias": "Currency",
		"fieldId": "currency.name"
	}),
	objQ.createColumn({
		"alias": "Billing From",
		"fieldId": "custbody_billfrom"
	}),
	objQ.createColumn({
		"alias": "Billing To",
		"fieldId": "custbody_billto"
	}),
	objQ.createColumn({
		"alias": "Cust Name",
		"fieldId": "transactionlines.custcol_cust_name_on_a_click_report"
	}),
	objQ.createColumn({
		"alias": "Project ID",
		"fieldId": "transactionlines.custcol_project_entity_id"
	}),
	objQ.createColumn({
		"alias": "Proj Name",
		"fieldId": "transactionlines.custcol_proj_name_on_a_click_report"
	}),
	objQ.createColumn({
		"alias": "Project Type",
		"fieldId": "entity^job.jobtype"
	}),
	objQ.createColumn({
		"alias": "Project1 Type1",
		"fieldId": "transactionlines.custcol_projecttype"
	}),
	objQ.createColumn({
		"alias": "Employee ID",
		"fieldId": "transactionlines.custcol_employee_entity_id"
	}),


	objQ.createColumn({
		"alias": "Employee Name",
		"fieldId": "transactionlines.custcol_emp_name_on_a_click_report"
	}), //Added on 08/10/2020


	objQ.createColumn({
		"alias": "Parent Executing Practice",
		"fieldId": "transactionlines.custcol_parent_executing_practice"
	}),
	objQ.createColumn({
		"alias": "Sub Practice",
		"fieldId": "transactionlines.department.name"
	}), //Uncommented Commented on 28/09/2020

	objQ.createColumn({
		"alias": "Sub Practice Revised",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('KPMG Consulting','Verizon Collabera') THEN 'House' \nWHEN {transactionlines.accountingimpact.account.id} =581 THEN 'Corporate'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.department.custrecord_is_delivery_practice}='F' THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.department.custrecord_is_delivery_practice}='T' THEN {transactionlines.department.name}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Delivery Office' THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' THEN {transactionlines.department.name}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN( 'Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Trainee','Investment') AND {transactionlines.custcol_parent_executing_practice} IN ('DFO','Brillio Analytics','Digital Infrastructure','Product Engineering','Design & Experience Studio','Technology &Advisory Consulting','Technology Advisory &Consulting','House') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN( 'Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('DFO','Brillio Analytics','Digital Infrastructure','Product Engineering','Design & Experience Studio','Technology &Advisory Consulting','Technology Advisory &Consulting') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Alliances','Corporate Marketing','Field Marketing','SET','Regional Management','Regional Sales','Corporate Sales ','CRO Management','Customer Success','Account Management') THEN 'Regional' else 'Corporate' END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN (2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('KPMG Consulting','Guidant Health','Verizon Collabera') THEN 'House' \nWHEN {transactionlines.accountingimpact.account} =581 THEN 'Corporate'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.department.custrecord_is_delivery_practice}='F' THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.department.custrecord_is_delivery_practice}='T' THEN {transactionlines.department.name}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Delivery Office' THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' AND {transactionlines.custcol_project_entity_id}='BRTP04021' THEN 'Customer Engagement Solutions'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' AND {transactionlines.custcol_project_entity_id}='BRTP04020' THEN 'Cloud Transformation Solutions'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' AND {transactionlines.custcol_project_entity_id}='BRTP04022' THEN 'Intelligent Enterprise Solutions'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' AND {transactionlines.custcol_project_entity_id} IN('BRTP03636','BRTP03875') THEN 'Solution Consulting Group'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN( 'Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Trainee','Investment') AND {transactionlines.custcol_parent_executing_practice} IN ('CEP','Data Analytics & Engineering','Digital Infrastructure','PPE','Design Studio','PPE Prof. Services') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN( 'Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('CEP','Data Analytics & Engineering','Digital Infrastructure','PPE','Design Studio','PPE Prof. Services','Cloud Engineering Studio') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Alliances','Corporate Marketing','Field Marketing','SET','Regional Management','Regional Sales','Corporate Sales ','CRO Management','Customer Success','Account Management') THEN 'Regional' else 'Corporate' END","type":"STRING"}),
		//"type": "STRING"
	//}),// 01/03/2021
	//Change the label as  Sub Practice Revised instead of Sub Pracice formula field  and update the logic as when sub pratice revised value is Delivery Office the system should display its Parent Executive Practice  on 27-10-2020

	//objQ.createColumn({"alias":"Sub Practice","formula":"{transactionlines.department}","type":"STRING"}),
	//objQ.createColumn({"alias":"Parent Practice","fieldId":"transactionlines.department.custrecord_parent_practice"}),
	objQ.createColumn({
		"alias": "Parent Practice",
		"fieldId": "transactionlines.department.custrecord_parent_practice.name"
	}),
	objQ.createColumn({
		"alias": "Project Region",
		"fieldId": "transactionlines.custcol_project_region"
	}),
	//objQ.createColumn({"alias":"Region Setup","fieldId":"transactionlines.custcol_region_master_setup"}),
	objQ.createColumn({
		"alias": "Region Setup",
		"fieldId": "transactionlines.custcol_region_master_setup.name"
	}),
	objQ.createColumn({
		"alias": "Project2 Type2",
		"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.custcol_project_entity_id} LIKE 'BRTP%' THEN 'Internal' else 'External' END",
		"type": "STRING"
	}),


	objQ.createColumn({
		"alias": "Region_New",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('KPMG Consulting','Verizon Collabera') THEN 'House' \nWHEN {transactionlines.accountingimpact.account.id} =581 THEN 'Corporate'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Alliances','Corporate Marketing','Field Marketing','SET','Delivery Office','CRO Management','Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Practice Management Offshore','Practice Management Onsite','SCG Management','CRO Management','Corporate Sales ','Investment','Sales Management','Trainee') THEN 'Brillio' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Regional Management','Regional Sales','Customer Success','Revenue','Account Management') THEN {transactionlines.custcol_project_region} else 'Corporate' END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('KPMG Consulting','Guidant Health','Verizon Collabera') THEN 'House' \nWHEN {transactionlines.accountingimpact.account} =581 THEN 'Corporate'\nWHEN  {transactionlines.custcol_proj_category_on_a_click} IN ('Alliances','Corporate Marketing','Field Marketing','SET','Delivery Office','CRO Management','Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Practice Management Offshore','Practice Management Onsite','SCG Management','CRO Management','Corporate Sales ','Investment','Sales Management','Trainee') THEN 'Brillio' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Regional Management','Regional Sales','Customer Success','Revenue','Account Management') THEN {transactionlines.custcol_project_region} else 'Corporate' END","type":"STRING"}),
		//"type": "STRING"
	//}), //01/03/2021

	// objQ.createColumn({"alias":"Location","fieldId":"transactionlines.location"}),
	objQ.createColumn({
		"alias": "Location",
		"fieldId": "transactionlines.location.name"
	}),
	objQ.createColumn({
		"alias": "Memo",
		"fieldId": "transactionlines.memo"
	}),
	objQ.createColumn({
		"alias": "Description",
		"fieldId": "transactionlines.custcol_memo_description"
	}),


	objQ.createColumn({
		"alias": "Item",
		"formula": "{transactionlines.item#display}",
		"type": "STRING"
	}), //code added on 30/09/2020

	objQ.createColumn({
		"alias": "Hour",
		"fieldId": "transactionlines.custcol_hour"
	}),
	objQ.createColumn({
		"alias": "Employee Type",
		"fieldId": "transactionlines.custcol_employee_type"
	}),
	objQ.createColumn({
		"alias": "Person Type",
		"fieldId": "transactionlines.custcol_person_type"
	}),
	objQ.createColumn({
		"alias": "Onsite/Offsite",
		"fieldId": "transactionlines.custcol_onsite_offsite"
	}),
	objQ.createColumn({
		"alias": "Billing Type",
		////        "fieldId": "custbody_billingtype"  Changed to transactionlines.custcol_billing_type on 24 Feb 2021
		"fieldId": "transactionlines.custcol_billing_type"

	}),
	objQ.createColumn({
		"alias": "Proj Category",
		"fieldId": "transactionlines.custcol_proj_category_on_a_click"
	}),
	objQ.createColumn({
		"alias": "MIS Practice",
		"fieldId": "transactionlines.custcol_mis_practice"
	}),
	// objQ.createColumn({"alias":"Books","fieldId":"transactionlines.subsidiary"}), 
	objQ.createColumn({
		"alias": "BooksId",
		"fieldId": "transactionlines.subsidiary"
	}), //For ID used
	objQ.createColumn({
		"alias": "Books",
		"fieldId": "transactionlines.subsidiary.name"
	}), //for text used name
	objQ.createColumn({
		"alias": "Transaction",
		"fieldId": "trandisplayname"
	}),


	objQ.createColumn({
		"alias": "Type Revised",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03412','BRTP03411','BLLC01192','BRUL03816','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_proj_category_on_a_click} LIKE '%Revenue%' OR {transactionlines.accountingimpact.account.id} IN('1234','515') THEN 'Direct' ELSE 'In Direct' END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14)  THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'  \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN  {transactionlines.custcol_proj_category_on_a_click}  LIKE '%Revenue%'  OR {transactionlines.accountingimpact.account} IN(1234,515)  THEN 'Direct' ELSE 'In Direct' END","type":"STRING"}),
		//"type": "STRING"
	//}), // 01/03/2021


	objQ.createColumn({
		"alias": "Cost Head Details_New",
		"formula": "' '",
		"type": "STRING"
	}),
	
	objQ.createColumn({
		"alias": "Cost Head Base New",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore'\nWHEN {transactionlines.custcol_parent_executing_practice}='India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nelse {transactionlines.accountingimpact.account.custrecord_cost_head.name} end","type":"STRING"}),
		//"type": "STRING"
	//}),  /// 01/03/2021


	objQ.createColumn({
		"alias": "Global Customer Name",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore' \nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Advanced Health Media LLC..' THEN 'Advanced Health Media LLC'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'AVID Center..' THEN 'AVID Center'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Beachbody LLC..' THEN 'Beachbody LLC'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Callidus Software Inc..' THEN 'Callidus Software Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'CVENT Inc..' THEN 'CVENT Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'DataStax Inc..' THEN 'DataStax Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('Dell International Services India Private Ltd','Dell Corporation Limited') THEN 'Dell International services private ltd'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Finastra Technology, Inc' THEN 'Finastra International Limited'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Infinite Computer Solutions Inc..' THEN 'Infinite Computer Solutions Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'LINL-01 Lineage Logistics LLC' THEN 'Lineage Logistics LLC'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('Lowell Suomi Oy Limited','Metis Bidco Limited','SIMON BIDCO LIMITED') THEN 'Lowell Group'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('MasterCard International Incorporated','MasterCard International Incorporated.') THEN 'MasterCard UK'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Microsoft Azure','Microsoft Contractor Hub Program-AGS (Brillio)') THEN 'Microsoft Corporation'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'NCR Corporation..' THEN 'NCR Corporation'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Nlyte Software Americas Limited..' THEN 'Nlyte Software Americas Limited'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'SAGE PEOPLE LIMITED' THEN 'Sage Global Services Limited'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'salesforce.com, Inc..' THEN 'salesforce.com, Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Agile1 -  SCE' THEN 'Southern California Edison Company'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'San Diego Gas & Electric Company' THEN 'Southern California Gas Company'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Stratford Schools..' THEN 'Stratford Schools'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Verizon Data Services India Pvt Ltd','Verizon Communications India Private Limited','Verizon Corporate Services Group Inc (Telecom)')THEN 'Verizon' \nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Goodman Telecom Services, Inc','Goodman Networks, Inc.') THEN 'Goodman Networks, Inc.' ELSE {transactionlines.custcol_cust_name_on_a_click_report} END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}='India - Practice' THEN 'Ignore'\nWHEN {transactionlines.department.custrecord_parent_practice#Display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Advanced Health Media LLC..' THEN 'Advanced Health Media LLC'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'AVID Center..' THEN 'AVID Center'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Beachbody LLC..' THEN 'Beachbody LLC'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Callidus Software Inc..' THEN 'Callidus Software Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'CVENT Inc..' THEN 'CVENT Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'DataStax Inc..' THEN 'DataStax Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('Dell International Services India Private Ltd','Dell Corporation Limited') THEN 'Dell International services private ltd'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Finastra Technology, Inc' THEN 'Finastra International Limited'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Infinite Computer Solutions Inc..' THEN 'Infinite Computer Solutions Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'LINL-01 Lineage Logistics LLC' THEN 'Lineage Logistics LLC'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('Lowell Suomi Oy Limited','Metis Bidco Limited','SIMON BIDCO LIMITED') THEN 'Lowell Group'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('MasterCard International Incorporated','MasterCard International Incorporated.') THEN 'MasterCard UK'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Microsoft Azure','Microsoft Contractor Hub Program-AGS (Brillio)') THEN 'Microsoft Corporation'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'NCR Corporation..' THEN 'NCR Corporation'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Nlyte Software Americas Limited..' THEN 'Nlyte Software Americas Limited'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'SAGE PEOPLE LIMITED' THEN 'Sage Global Services Limited'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'salesforce.com, Inc..' THEN 'salesforce.com, Inc'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Agile1 -  SCE' THEN 'Southern California Edison Company'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'San Diego Gas & Electric Company' THEN 'Southern California Gas Company'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report}= 'Stratford Schools..' THEN 'Stratford Schools'\nWHEN {transactionlines.custcol_project_entity_id}='VERI03168' THEN 'Verizon India'\nWHEN {transactionlines.custcol_project_entity_id}='VEDS04248' THEN 'Verizon US'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Verizon Data Services India Pvt Ltd','Verizon Communications India Private Limited') AND {transactionlines.custcol_project_entity_id}!='VEDS04248' THEN 'Verizon India' \nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Verizon','Verizon Corporate Services Group Inc (Telecom)')AND {transactionlines.custcol_project_entity_id}!='VERI03168' THEN 'Verizon US' \nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN('Goodman Telecom Services, Inc','Goodman Networks, Inc.','Goodman Telecom') THEN 'Goodman Networks, Inc.' ELSE {transactionlines.custcol_cust_name_on_a_click_report} END","type":"STRING"}),
		//"type": "STRING"
	//}), // 01/03/2021




	objQ.createColumn({
		"alias": "Final Ledger",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}='India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account.id} =581 THEN 'Other Income/expense'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Corporate Marketing','Field Marketing','Regional Sales','SCG Management','Regional Management','Delivery Office','Customer Success','SET','Alliances','CRO Management','Executive Office','CSR Expenses','Corporate Development','Finance','HR (IN)','HR (US)','IT Ops & IS','Admin','Rent','CARES Act Benefit','Bonus Reduction','Management Fees','Other Income/expense','Interest','Director Fees','Depreciation','Goodwill Amortization','ESOP Expenses','Income Tax','Merger & Acquisition','PSC') THEN {transactionlines.custcol_proj_category_on_a_click}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Account Management' THEN 'Customer Success'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('DFO','Brillio Analytics','Digital Infrastructure','Product Engineering') THEN {transactionlines.custcol_proj_category_on_a_click}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('Design & Experience Studio','Global Design Studio','Advanced Technology Group','Technology &Advisory Consulting','Technology Advisory &Consulting') THEN 'SCG Management'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} ='Revenue' THEN 'Gross Revenue'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} ='Discount' THEN 'Discount'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} ='OFC' THEN 'Offshore Facility Cost'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head.name} = 'Salary Cost' THEN 'People cost' \nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head.name} = 'Other Cost' THEN 'Other Direct Cost' END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN  'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}='India - Practice' THEN 'Ignore'\nWHEN {transactionlines.department.custrecord_parent_practice#Display} ='India - Practice' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =581 THEN 'Other Income/expense'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Onsite','Practice Management Offshore','Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Corporate Marketing','Field Marketing','Regional Sales','SCG Management','Regional Management','Delivery Office','Customer Success','SET','Alliances','CRO Management','Executive Office','CSR Expenses','Corporate Development','Finance','Global HR','HR (IN)','HR (US)','IT Ops & IS','Admin','Rent','CARES Act Benefit','Bonus Reduction','Management Fees','Other Income/expense','Interest','Director Fees','Depreciation','Goodwill Amortization','ESOP Expenses','Income Tax','Merger & Acquisition','PSC') THEN  {transactionlines.custcol_proj_category_on_a_click}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Account Management' THEN 'Customer Success'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} ='Revenue' THEN 'Gross Revenue'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} ='Discount' THEN 'Discount'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} ='OFC' THEN 'Offshore Facility Cost'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head.name}  = 'Salary Cost' THEN 'People cost' \nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.accountingimpact.account.custrecord_cost_head.name}  = 'Other Cost' THEN 'Other Direct Cost' END","type":"STRING"}),
		//"type": "STRING"
	//}), // 01/03/2021



	objQ.createColumn({
		"alias": "Practice Revised",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('KPMG Consulting','Verizon Collabera') THEN 'House' \nWHEN {transactionlines.accountingimpact.account} =581 THEN 'Corporate'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.department.custrecord_parent_practice.custrecord_is_delivery_practice}='F' THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue' AND {transactionlines.department.custrecord_parent_practice.custrecord_is_delivery_practice}='T' THEN {transactionlines.department.custrecord_parent_practice.name}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Delivery Office' THEN 'Delivery Office'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' THEN 'Solutions Consulting Group'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('DFO','Brillio Analytics','Digital Infrastructure','Product Engineering') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('Design & Experience Studio','Global Design Studio','Advanced Technology Group','Technology &Advisory Consulting','Technology Advisory &Consulting') THEN 'Solutions Consulting Group'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Trainee','Investment') AND {transactionlines.custcol_parent_executing_practice} IN ('DFO','Brillio Analytics','Digital Infrastructure','Product Engineering','Design & Experience Studio','Technology &Advisory Consulting','Technology Advisory &Consulting','House') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Alliances','Corporate Marketing','Field Marketing','SET','Regional Management','Regional Sales','Corporate Sales ','CRO Management','Customer Success','Account Management') THEN 'Regional' else 'Corporate' END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.custcol_cust_name_on_a_click_report} IN ('KPMG Consulting','Guidant Health','Verizon Collabera') THEN 'House' \nWHEN {transactionlines.accountingimpact.account} =581 THEN 'Corporate'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue'  AND {transactionlines.department.custrecord_parent_practice.custrecord_is_delivery_practice}='F' THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Revenue'  AND {transactionlines.department.custrecord_parent_practice.custrecord_is_delivery_practice}='T' THEN {transactionlines.department.custrecord_parent_practice.name}\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='Delivery Office' THEN 'Delivery Office'\nWHEN {transactionlines.custcol_proj_category_on_a_click} ='SCG Management' THEN 'Solutions Consulting Group'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('CEP','Data Analytics & Engineering','Digital Infrastructure','PPE','Design Studio','Cloud Engineering Studio') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Management Offshore','Practice Management Onsite') AND {transactionlines.custcol_parent_executing_practice} IN ('PPE Prof. Services') THEN 'PPE'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Trainee','Investment') AND {transactionlines.custcol_parent_executing_practice} IN ('CEP','Data Analytics & Engineering','Digital Infrastructure','PPE','Design Studio') THEN {transactionlines.custcol_parent_executing_practice}\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('PPE Prof. Services') THEN 'PPE'\nWHEN {transactionlines.custcol_proj_category_on_a_click} IN('Alliances','Corporate Marketing','Field Marketing','SET','Regional Management','Regional Sales','Corporate Sales ','CRO Management','Customer Success','Account Management') THEN 'Regional' else 'Corporate' END","type":"STRING"}),
		//"type": "STRING"
	//}),// 01/03/2021


	objQ.createColumn({
		"alias": "TB Reco Final",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account.id} =581 THEN 'Considered for Below EBITDA'\nWHEN {transactionlines.custcol_proj_category_on_a_click} = 'Revenue' THEN 'Considered for DM' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Corporate Marketing','Field Marketing','Regional Management','Regional Sales','SET','Alliances','CRO Management','Customer Success','Account Management') THEN 'Considered For Market' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Delivery Office','Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Practice Management Offshore','Practice Management Onsite','SCG Management') THEN 'Considered or Practice SGA' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Management Fees','Other Income/expense','Misc income/expense','Interest','Director Fees','Depreciation','Goodwill Amortization','ESOP Expenses','Income Tax','Merger & Acquisition') THEN 'Considered for Below EBITDA'\nELSE 'considered for SGA' END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN(2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN 'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}= 'India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =581 THEN 'Considered for Below EBITDA'\nWHEN {transactionlines.custcol_proj_category_on_a_click} = 'Revenue'  THEN 'Considered for DM' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Corporate Marketing','Field Marketing','Regional Management','Regional Sales','SET','Alliances','CRO Management','Customer Success','Account Management') THEN 'Considered for Market' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Delivery Office','Practice Dedicated Resources Offshore','Practice Dedicated Resources Onsite','Practice Management Offshore','Practice Management Onsite','SCG Management') THEN 'Considered for Practice SGA' \nWHEN {transactionlines.custcol_proj_category_on_a_click} IN ('Management Fees','Other Income/expense','Misc income/expense','Interest','Director Fees','Depreciation','Goodwill Amortization','ESOP Expenses','Income Tax','Merger & Acquisition') THEN 'Considered for Below EBITDA'\nELSE  'Considered for SGA' END","type":"STRING"}),
		//"type": "STRING"
	//}), // 01/03/2021


	objQ.createColumn({
		"alias": "Month",
		"formula": "{postingperiod.periodname}",
		"type": "STRING"
	}),
	objQ.createColumn({
		"alias": "MONTH_NEW",
		"formula": "CONCAT('*',TO_NCHAR({trandate},'Mon YYYY'))",
		"type": "STRING"
	}),
	objQ.createColumn({
		"alias": "is revenew",
		"formula": "{transactionlines.department.custrecord_parent_practice.custrecord_is_delivery_practice}",
		"type": "STRING"
	}),
	objQ.createColumn({
		"alias": "Memo Line",
		"fieldId": "transactionlines.memo"
	}),


	objQ.createColumn({
		"alias": "Cost Head - Detailed",
		//"formula": "CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id}=2 THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN \n'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}='India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nELSE {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} END",
		"formula":"CASE WHEN {transactionlines.accountingimpact.account.custrecord_cost_head.name}='Ignore' THEN 'Ignore'\nWHEN {transactionlines.accountingimpact.account} =732 AND {transactionlines.subsidiary.id} IN (2,11,12,3,7,13,14) THEN 'Ignore' \nWHEN {transactionlines.custcol_project_entity_id} IN ('BRTP03411','BRTP03412','BRUL03816','BLLC01192','BRCI03820','COSR04314','BRIT04011') THEN \n'Ignore' \nWHEN {transactionlines.custcol_parent_executing_practice}='India - Practice' OR {transactionlines.department.custrecord_parent_practice#display} = 'India - Practice' THEN 'Ignore'\nELSE {transactionlines.accountingimpact.account.custrecord_cost_head_details.name} END","type":"STRING"}),
		//"type": "STRING"
		
	//}) // 01/03/2021

];
                //Search on Custom Record=> "P & L Currency Exchange Rates List" 
                //Saved Search Name in Sandbox: "P & L Currency Exchange Rates Search Testing"
			
/************************* New Formula SQL Added on 01 March by Shravan ***************************************/
                log.debug('Search_Log----Search_Log', 'Search_Log');
/*********************************************	*******************************************/
/// New Modification as on 11-Feb-2021	
				if(cb_Check_box == 'T') ///// This is a Yearly Calculations
				{
					log.debug('cb_Check_box----cb_Check_box True', 'Inside cb_Check_box==T');
					  var customrecord_pl_currency_exchange_ratesSearchObj = search.create({
                    type: "customrecord_ypl_currency_exchange_rates",
                    filters: [
                        ["formulatext: {custrecord_ypl_currency_exchange_year}", "is", year]
                    ],
                    columns: [
                        search.createColumn({
                            name: "scriptid",
                            sort: search.Sort.ASC,
                            label: "Script ID"
                        }),

                        search.createColumn({
                            name: "custrecord_ypl_currency_exchange_revnue",
                            label: "USD-INR Revenue Rate"
                        }),
                        // search.createColumn({name: "custrecord_pl_currency_exchange_year", label: "Year"}),
                        search.createColumn({
                            name: "custrecord_ypl_currency_exchange_cost",
                            label: "USD-INR Cost Rate"
                        }),
                        search.createColumn({
                            name: "custrecord_ypl_gbp_converstion_rate",
                            label: "USD to GBP"
                        }),
                        search.createColumn({
                            name: "custrecord_ypl_eur_usd_conv_rate",
                            label: "USD to EUR"
                        }),
                        search.createColumn({
                            name: "custrecord_ypl_aud_to_usd",
                            label: "USD to AUD"
                        }),
                        search.createColumn({
                            name: "custrecord_ypl_usd_cad_conversion_rate",
                            label: "USD to CAD"
                        }),
                        search.createColumn({
                            name: "custrecord_ypl_usd_ron_conversion_rate",
                            label: "USD to RON"
                        })
						]
					});

					var searchResultCount = customrecord_pl_currency_exchange_ratesSearchObj.runPaged().count;
                log.debug("customrecord_pl_currency_exchange_ratesSearchObj result count",searchResultCount);

					customrecord_pl_currency_exchange_ratesSearchObj.run().each(function(result) {

                    var searchresult = customrecord_pl_currency_exchange_ratesSearchObj.run().getRange({
                        start: 0,
                        end: 1000
                    });
					
					if(searchResultCount != 0)
					{
                    if (_logValidation(searchResultCount)) 
					{
                        usd_inr_revenue_Rate = searchresult[0].getValue({
                            name: "custrecord_ypl_currency_exchange_revnue",
                            label: "USD-INR Revenue Rate"
                        });
                       // log.debug('Search_Log', 'usd_inr_revenue_Rate:=>' + usd_inr_revenue_Rate);
                        usd_inr_cost_Rate = searchresult[0].getValue({
                            name: "custrecord_ypl_currency_exchange_cost",
                            label: "USD-INR Cost Rate"
                        });
                     /*   //log.debug('Search_Log', 'usd_inr_cost_Rate:=>' + usd_inr_cost_Rate);
                        crc_cost_Conversion = searchresult[0].getValue({
                            name: "custrecord_ypl_crc_cost_rate",
                            label: "CRC Cost Conversion"
                        });
                        //log.debug('Search_Log', 'crc_cost_Conversion:=>' + crc_cost_Conversion);
                        nok_cost_Rate = searchresult[0].getValue({
                            name: "custrecord_ypl_nok_cost_rate",
                            label: "NOK Cost Rate"
                        });*/
                        //log.debug('Search_Log', 'nok_cost_Rate:=>' + nok_cost_Rate);
                        var usd_to_GBPP = searchresult[0].getValue({
                            name: "custrecord_ypl_gbp_converstion_rate",
                            label: "USD to GBP"
                        });
                        var usd_to_GBPP = Number(usd_to_GBPP);
                        usd_to_GBP = usd_to_GBPP.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_GBP:=>' + usd_to_GBP);
                        var usd_to_EURR = searchresult[0].getValue({
                            name: "custrecord_ypl_eur_usd_conv_rate",
                            label: "USD to EUR"
                        });
                        var usd_to_EURR = Number(usd_to_EURR);
                        usd_to_EUR = usd_to_EURR.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_EUR:=>' + usd_to_EUR);
                        var usd_to_AUDD = searchresult[0].getValue({
                            name: "custrecord_ypl_aud_to_usd",
                            label: "USD to AUD"
                        });
                        var usd_to_AUDD = Number(usd_to_AUDD);
                        usd_to_AUD = usd_to_AUDD.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_AUD:=>' + usd_to_AUD);

                        var usd_to_CADD = searchresult[0].getValue({
                            name: "custrecord_ypl_usd_cad_conversion_rate",
                            label: "USD to CAD"
                        });
                        var usd_to_CADD = Number(usd_to_CADD);
                        usd_to_CAD = usd_to_CADD.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_CAD:=>' + usd_to_CAD);
                        var usd_to_RONN = searchresult[0].getValue({
                            name: "custrecord_ypl_usd_ron_conversion_rate",
                            label: "USD to RON"
                        });
                        var usd_to_RONN = Number(usd_to_RONN);
                        usd_to_RON = usd_to_RONN.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_RON:=>' + usd_to_RON);
                    }
					}	
						return true;
					});
					
				} ////// End of if(cb_Check_box == 'T')
				else //// This is a Monthly Calculation
				{
					  var customrecord_pl_currency_exchange_ratesSearchObj = search.create({
                    type: "customrecord_pl_currency_exchange_rates",
                    filters: [
                        ["custrecord_pl_currency_exchange_month", "anyof", month],
                        "AND",
                        // ["custrecord_pl_currency_exchange_year","anyof","7"]
                        ["formulatext: {custrecord_pl_currency_exchange_year}", "is", year]
                    ],
                    columns: [
                        search.createColumn({
                            name: "scriptid",
                            sort: search.Sort.ASC,
                            label: "Script ID"
                        }),

                        search.createColumn({
                            name: "custrecord_pl_currency_exchange_revnue_r",
                            label: "USD-INR Revenue Rate"
                        }),
                        // search.createColumn({name: "custrecord_pl_currency_exchange_year", label: "Year"}),
                        search.createColumn({
                            name: "custrecord_pl_currency_exchange_cost",
                            label: "USD-INR Cost Rate"
                        }),
                        search.createColumn({
                            name: "custrecord_pl_crc_cost_rate",
                            label: "CRC Cost Conversion"
                        }),
                        search.createColumn({
                            name: "custrecord_pl_nok_cost_rate",
                            label: "NOK Cost Rate"
                        }),
                        search.createColumn({
                            name: "custrecord_gbp_converstion_rate",
                            label: "USD to GBP"
                        }),
                        search.createColumn({
                            name: "custrecord_eur_usd_conv_rate",
                            label: "USD to EUR"
                        }),
                        search.createColumn({
                            name: "custrecord_aud_to_usd",
                            label: "USD to AUD"
                        }),
                        search.createColumn({
                            name: "custrecord_usd_cad_conversion_rate",
                            label: "USD to CAD"
                        }),
                        search.createColumn({
                            name: "custrecord_usd_ron_conversion_rate",
                            label: "USD to RON"
                        })
                    ]
                });

                var searchResultCount = customrecord_pl_currency_exchange_ratesSearchObj.runPaged().count;
                //log.debug("customrecord_pl_currency_exchange_ratesSearchObj result count",searchResultCount);

                customrecord_pl_currency_exchange_ratesSearchObj.run().each(function(result) {

                    var searchresult = customrecord_pl_currency_exchange_ratesSearchObj.run().getRange({
                        start: 0,
                        end: 1000
                    });
					
			   if(searchResultCount != 0)
				{
                    if (_logValidation(searchResultCount)) 
					{
                        usd_inr_revenue_Rate = searchresult[0].getValue({
                            name: "custrecord_pl_currency_exchange_revnue_r",
                            label: "USD-INR Revenue Rate"
                        });
                       // log.debug('Search_Log', 'usd_inr_revenue_Rate:=>' + usd_inr_revenue_Rate);
                        usd_inr_cost_Rate = searchresult[0].getValue({
                            name: "custrecord_pl_currency_exchange_cost",
                            label: "USD-INR Cost Rate"
                        });
                        //log.debug('Search_Log', 'usd_inr_cost_Rate:=>' + usd_inr_cost_Rate);
                        crc_cost_Conversion = searchresult[0].getValue({
                            name: "custrecord_pl_crc_cost_rate",
                            label: "CRC Cost Conversion"
                        });
                        //log.debug('Search_Log', 'crc_cost_Conversion:=>' + crc_cost_Conversion);
                        nok_cost_Rate = searchresult[0].getValue({
                            name: "custrecord_pl_nok_cost_rate",
                            label: "NOK Cost Rate"
                        });
                        //log.debug('Search_Log', 'nok_cost_Rate:=>' + nok_cost_Rate);
                        var usd_to_GBPP = searchresult[0].getValue({
                            name: "custrecord_gbp_converstion_rate",
                            label: "USD to GBP"
                        });
                        var usd_to_GBPP = Number(usd_to_GBPP);
                        usd_to_GBP = usd_to_GBPP.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_GBP:=>' + usd_to_GBP);
                        var usd_to_EURR = searchresult[0].getValue({
                            name: "custrecord_eur_usd_conv_rate",
                            label: "USD to EUR"
                        });
                        var usd_to_EURR = Number(usd_to_EURR);
                        usd_to_EUR = usd_to_EURR.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_EUR:=>' + usd_to_EUR);
                        var usd_to_AUDD = searchresult[0].getValue({
                            name: "custrecord_aud_to_usd",
                            label: "USD to AUD"
                        });
                        var usd_to_AUDD = Number(usd_to_AUDD);
                        usd_to_AUD = usd_to_AUDD.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_AUD:=>' + usd_to_AUD);

                        var usd_to_CADD = searchresult[0].getValue({
                            name: "custrecord_usd_cad_conversion_rate",
                            label: "USD to CAD"
                        });
                        var usd_to_CADD = Number(usd_to_CADD);
                        usd_to_CAD = usd_to_CADD.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_CAD:=>' + usd_to_CAD);
                        var usd_to_RONN = searchresult[0].getValue({
                            name: "custrecord_usd_ron_conversion_rate",
                            label: "USD to RON"
                        });
                        var usd_to_RONN = Number(usd_to_RONN);
                        usd_to_RON = usd_to_RONN.toFixed(3);
                        //log.debug('Search_Log', 'usd_to_RON:=>' + usd_to_RON);
                    }
				}	
                    return true;
                });
					
				} //// End of else of if(cb_Check_box == 'T')
					/********************************************* End of Modification	*******************************************/			
			if(searchResultCount == 0)
			{
				usd_inr_revenue_Rate = 0;
				usd_inr_cost_Rate = 0;
				crc_cost_Conversion = 0;
				usd_to_GBP = 0;
				usd_to_EUR = 0;
				usd_to_AUD = 0;
				usd_to_CAD = 0;
				nok_cost_Rate = 0;
				usd_to_RON = 0;
			}
			
			
			/********** Mofification as on 11 Feb 2021 BY shravan *********************/
			
                //var folder_name = 1953737;  //Sandbox Folder Name
				if(cb_Check_box == 'T') /// This is a Yearly folder
				{
					var folder_name = 2594887; // Production Folder name
				}///if(cb_Check_box == 'T')
				else //// Monthly folder alredy being used 
					{
						var folder_name = 2172754; // Production Folder name
					} /// End of else of if(cb_Check_box == 'T')
		/*  ********   End ****************************************/
                var csvFile = file.create({
                    name: 'Work_Book_For_PP' + month + year + '.csv',
                    //name:  'Work_Book_For_PP.csv',
                    //contents: 'Account, Account, Account: Type, Type, Date, Document Number, Name, Transaction Accounting Line: Amount,	Transaction Line: Amount (Transaction Currency),USD Amount,	Currency, Billing From , Billing To	,Cust Name,	Global Customer Name, Project ID ,Entity (Project): Project Type, Transaction Line: Project Type, Proj Name, Employee ID, Employee Name, Project Region, Region Setup, Project Type, Location, Memo, Description, Item, Hour, Employee Type, Person Type, Onsite/Offsite, Billing Type,	Transaction Line: MIS Practice,	Books, Transaction, Type Revised, Sub Practice,	Practice Revised, TB Reco Final, Cost Head Base New, Cost Head Details_New,	Final Ledger, Region_New, Parent Executing Practice, Sub Practice, Parent Practice, Proj Category,	is revenew,	MONTH_NEW,Month\n', 
                   // contents: 'Financial Raw, Type, Date, Transaction Number, Name, Amount-Base Currency, Amount-Transaction Currency, Currency, Billing From, Billing To, Cust Name, Project ID, Proj Name,	Employee ID, Employee Name,	Parent Executing Practice, Sub Practice, Parent Practice, Project Region, Region Setup, Location, Memo,	Description, Item, Hour, Employee Type,	Person Type, Onsite/Offsite, Billing Type, Proj Category, Books, Type Revised, Cost Head, Revised Client Name-A/C, Region, Sub Practice, Practice Revised, USD Amount, Final Ledger, Final Ledger - Detailed, TB Reco Final, MONTH\n',
                     contents: 'Financial Raw,Type,Date,Transaction Number,Name,Amount,Billing From,Billing To,Cust Name,Project ID,Proj Name,Employee ID,Emp Name,Parent Executing Practice,Sub Practice,Practice: Parent Practice,Project:Region,Region Setup,Location,Memo,Memo Line,Description,Item,Hour,Employee Type,Person Type,Onsite/Offsite,Billing Type,Proj Category,Books,Type-Revised,Cost Head,Revised Client Name-A/C,Region Revised,Sub Practice Revised,Practice Revised,Amount-USD,Final Ledger - Detailed,Final Ledger,Cost Head - Detailed,TB Reco Final,MONTH\n',
                    

                    fileType: 'CSV',
                    isOnline: true,
                    folder: folder_name //Work Book PP

                });

                // Paged execution 
                var objPagedData = objQ.runPaged({
                    pageSize: 1000
                });

                // Paging 
                var arrResults = [];
                objPagedData.pageRanges.forEach(function(pageRange) {
                    var objPage = objPagedData.fetch({
                        index: pageRange.index
                    }).data;

                    // Map results to columns 
                    arrResults.push.apply(arrResults, objPage.asMappedResults());
                    //log.debug('arrResults----arrResults=>',JSON.stringify(arrResults));

                    // log.debug('arrResults----arrResults=>',JSON.stringify(arrResults));
                    //	[{"Account":647,"Type":"Income","Date":"01/21/2020","Document Number/ID":"JE26239","Name":null,"Amount":108800,"Amount (Transaction Currency)":108800,"USD Amount":1,"Currency":"INR","Billing From":null,"Billing To":null,"Cust Name":"Medline Industries India Pvt Limited","Project ID":"MEIN03847","Proj Name":"Agile Consulting Services","Project Type":null,"Employee ID":"118761","Employee Name":"118761-Sri Phani Krishna Chinnapuvvula","Parent Executing Practice":"Product Engineering","Sub Practice":"Product Engineering","Parent Practice":"Product Engineering","Project Region":"India","Region Setup":"India","Region_New":"India","Location":null,"Memo":"Hourly - Approved","Description":null,"Item":null,"Hour":40,"Employee Type":"Salaried","Person Type":"Employee","Onsite/Offsite":"Offsite","Billing Type":null,"Proj Category":"Revenue","MIS Practice":"Product Engineering : Product Engineering","Books":"Brillio Technologies Private Limited","Transaction":"Journal #JE26239","Type Revised":"Direct","Cost Head Base New":"Revenue","Cost Head Details_New":"Revenue","Global Customer Name":"Medline Industries India Pvt Limited","Final Ledger":"Gross Revenue","Practice Revised":"Product Engineering","TB Reco Final":"Considered for DM","Month":"Jan 2020","MONTH_NEW":"*Jan 2020","is revenew":"T"},
                    //
                    var rec_count = arrResults.length;
                    log.debug('rec_count----rec_count=>' + rec_count);
                })
                var rec_count = arrResults.length;
                log.debug('rec_count----rec_count=>' + rec_count);
                for (i = 0; i < rec_count; i++) 
				{
                    var temp = arrResults[i];
                    var account = temp["Account"];
                    //log.debug({ title : 'Account Text',details: account});
                    if (account == null) {
                        account = ' ';
                    }

                    var acc_account = temp["Account3"];
                    //log.debug('Export_Log----acc_account=>', 'acc_account:=>' + acc_account);
                    if (acc_account == null) {
                        acc_account = ' ';
                    }
                    var type = temp["Type"];
                    //log.debug('Export_Log----type=>', 'type:=>' + type);
                    if (type == null) {
                        type = ' ';
                    }

                    var account_type = temp["Type1"];
                    //log.debug('Export_Log----account_type=>', 'account_type:=>' + account_type);
                    if (account_type == null) {
                        account_type = ' ';
                    }

                    var date = temp["Date"];
                    //log.debug('Export_Log----date=>', 'date:=>' + date);
                    if (date == null) {
                        date = ' ';
                    }
                    var documents = temp["Document Number/ID"];
                    log.debug('Export_Log----documents=>', 'documents:=>' + documents);
                    if (documents == null) {
                        documents = ' ';
                    }
					else{
							var i_Comma_Index_Document = documents.indexOf('"');
							if(parseInt(i_Comma_Index_Document) != 0)
							{
								documents = '"' + documents + '"';
							} //// if(parseInt(i_Comma_Index_Document) != 0)
						} /// if (documents == null)

                  
                  log.debug('after else----documents=>', 'documents:=>' + documents);
                    var name = temp["Name"];
                    //log.debug('Export_Log----name=>', 'name:=>' + name);
                    if (name == null) {
                        name = ' ';
                    }
					
					// Removing all the commas
					//var name = name.replace(/,/g, " ");  //Added on 30/09/2020
					
					var name_str = name;
					//Keeping commas in column of CSV
					var name = '"' + name_str + '"';

                    var trans_Accounting_line_Amount = temp["Amount"];
                    //log.debug('Export_Log----trans_Accounting_line_Amount=>', 'trans_Accounting_line_Amount:=>' + trans_Accounting_line_Amount);
                    if (trans_Accounting_line_Amount == null) {
                        trans_Accounting_line_Amount = ' ';
                    }
                    var trans_line_Amount_Trans_currency = temp["Amount (Transaction Currency)"];
                    //log.debug('Export_Log----trans_line_Amount_Trans_currency=>', 'trans_line_Amount_Trans_currency:=>' + trans_line_Amount_Trans_currency);
                    if (trans_line_Amount_Trans_currency == null) {
                        trans_line_Amount_Trans_currency = ' ';
                    }
                    var usd_Amount = temp["Amount-USD"];
                    //log.debug('Export_Log----usd_Amount=>', 'usd_Amount:=>' + usd_Amount);
                    if (usd_Amount == null) {
                        usd_Amount = ' ';
                    }

                    var currency_Id = temp["CurrencyId"];
                    //log.debug('Export_Log----currency_Id=>', 'currency_ID:=>' + currency_ID);

                    var currency = temp["Currency"];
                    //log.debug('Export_Log----currency=>', 'currency:=>' + currency);
                    if (currency == null) {
                        currency = ' ';
                    }

                    var billing_From = temp["Billing From"];
                    //log.debug('Export_Log----billing_From=>', 'billing_From:=>' + billing_From);
                    if (billing_From == null) {
                        billing_From = ' ';
                    }
                    var billing_To = temp["Billing To"];
                    //log.debug('Export_Log----billing_To=>', 'billing_To:=>' + billing_To);
                    if (billing_To == null) {
                        billing_To = ' ';
                    }
                    var cust_name = temp["Cust Name"];
                    //log.debug('Export_Log----cust_name=>', 'cust_name:=>' + cust_name);
                    if (cust_name == null) {
                        cust_name = ' ';
                    }

                    //Regular Expression to remove special character
                    var str = cust_name;
                    //log.debug('str=>',str);
                    //var cust_name = str.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					
					//Checking  "
					 var cust_first_pos = str.indexOf('"');
					 //log.debug('Export_Log----cust_first_pos=>', 'cust_first_pos:=>' + cust_first_pos);
					
					
					if(cust_first_pos == 0)
					{

							   var cust_name = str;

					}else
					{
						// For Keeping commas in column
						var cust_name = '"' + str + '"';
					}
					

                    var project_id = temp["Project ID"];
                    //log.debug('Export_Log----project_id=>', 'project_id:=>' + project_id);
                    if (project_id == null) {
                        project_id = ' ';
                    }
                    var project_Name = temp["Proj Name"];
                    //log.debug('Export_Log----project_Name=>', 'project_Name:=>' + project_Name);
                    if (project_Name == null) {
                        project_Name = ' ';
                    }

                    //Regular Expression to remove special character
                    var str7 = project_Name;
                    //log.debug('str7=>',str7);
                    //var project_Name = str7.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					//Checking  "
					 var project_name_pos = str7.indexOf('"');
					 //log.debug('Export_Log----project_name_pos=>', 'project_name_pos:=>' + project_name_pos);
					
					
					if(project_name_pos == 0)
					{

							   var project_Name = str7;

					}else
					{
						// For Keeping commas in column
						var project_Name = '"' + str7 + '"';
					}
					
					
					//Keeping commas in column of CSV
					//var project_Name = '"' + str7 + '"';

                    var entity_project_type = temp["Project Type"];
                    //log.debug('Export_Log----entity_project_type=>', 'entity_project_type:=>' + entity_project_type);
                    if (entity_project_type == null) {
                        entity_project_type = ' ';
                    }

                    //Regular Expression to remove special character
                    var str8 = entity_project_type;
                    //log.debug('str8=>',str8);
                    //var entity_project_type = str8.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var entity_project_type = '"' + str8 + '"';

                    var transaction_line_project_Type = temp["Project1 Type1"];
                    //log.debug('Export_Log----transaction_line_project_Type=>', 'transaction_line_project_Type:=>' + transaction_line_project_Type);
                    if (transaction_line_project_Type == null) {
                        transaction_line_project_Type = ' ';
                    }

                    //Regular Expression to remove special character
                    var str9 = transaction_line_project_Type;
                    //log.debug('str9=>',str9);
                    //var transaction_line_project_Type = str9.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var transaction_line_project_Type = '"' + str9 + '"';
					

                    var employee_id = temp["Employee ID"];
                    //log.debug('Export_Log----employee_id=>', 'employee_id:=>' + employee_id);
                    if (employee_id == null) {
                        employee_id = ' ';
                    }
                    var emaployee_Name = temp["Employee Name"];
                    //log.debug('Export_Log----emaployee_Name=>', 'emaployee_Name:=>' + emaployee_Name);
                    if (emaployee_Name == null) {
                        emaployee_Name = ' ';
                    }

                    //Regular Expression to remove special character
                    var str10 = emaployee_Name;
                    //log.debug('str10=>',str10);
                    //var emaployee_Name = str10.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var emaployee_Name = '"' + str10 + '"';

                    var parent_Executing_Practice = temp["Parent Executing Practice"];
                    //log.debug('Export_Log----parent_Executing_Practice=>', 'parent_Executing_Practice:=>' + parent_Executing_Practice);
                    if (parent_Executing_Practice == null) {
                        parent_Executing_Practice = ' ';
                    }

                    //Regular Expression to remove special character
                    var str11 = parent_Executing_Practice;
                    //log.debug('str11=>',str11);
                    //var parent_Executing_Practice = str11.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					
					// For Keeping commas in column
					var parent_Executing_Practice = '"' + str11 + '"';

                    var sub_practice = temp["Sub Practice"];
                    //log.debug('Export_Log----sub_practice=>', 'sub_practice:=>' + sub_practice);
                    if (sub_practice == null) {
                        sub_practice = ' ';
                    }

                    //Regular Expression to remove special character
                    var str12 = sub_practice;
                    //log.debug('str12=>',str12);
                    //var sub_practice = str12.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var sub_practice = '"' + str12 + '"';

				

                    var parent_Practice = temp["Parent Practice"];
                    //log.debug('Export_Log----parent_Practice=>', 'parent_Practice:=>' + parent_Practice);
                    if (parent_Practice == null) {
                        parent_Practice = ' ';
                    }

                    //Regular Expression to remove special character
                    var str13 = parent_Practice;
                    //log.debug('str13=>',str13);
                    //var parent_Practice = str13.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var parent_Practice = '"' + str13 + '"';

                    var project_Region = temp["Project Region"];
                    //log.debug('Export_Log----project_Region=>', 'project_Region:=>' + project_Region);
                    if (project_Region == null) {
                        project_Region = ' ';
                    }

                    //Regular Expression to remove special character
                    var str14 = project_Region;
                    //log.debug('str14=>',str14);
                    //var project_Region = str14.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var project_Region = '"' + str14 + '"';

                    var project_Setup = temp["Region Setup"];
                    //log.debug('Export_Log----project_Setup=>', 'project_Setup:=>' + project_Setup);
                    if (project_Setup == null) {
                        project_Setup = ' ';
                    }

                    //Regular Expression to remove special character
                    var str15 = project_Setup;
                    //log.debug('str15=>',str15);
                    //var project_Setup = str15.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var project_Setup = '"' + str15 + '"';

                    var project_Type = temp["Project2 Type2"];
                    //log.debug('Export_Log----project_Type=>', 'project_Type:=>' + project_Type);
                    if (project_Type == null) {
                        project_Type = ' ';
                    }

                    //Regular Expression to remove special character
                    var str16 = project_Type;
                    //log.debug('str16=>',str16);
                    //var project_Type = str16.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');

					// For Keeping commas in column
					var project_Type = '"' + str16 + '"';

                    var region_New = temp["Region_New"];
                    //log.debug('Export_Log----region_New=>', 'region_New:=>' + region_New);
                    if (region_New == null) {
                        region_New = ' ';
                    }

                    //Regular Expression to remove special character
                    var str17 = region_New;
                    //log.debug('str17=>',str17);
                    //var region_New = str17.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var region_New = '"' + str17 + '"';

                    var l_location = temp["Location"];
                    //log.debug('Export_Log----l_location=>', 'l_location:=>' + l_location);

                    if (l_location == null) {
                        l_location = ' ';
                    }

                    //Regular Expression to remove special character
                    var str24 = l_location;
                    //log.debug('str24=>',str24);
                    //var l_location = str24.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var l_location = '"' + str24 + '"';

                    var memo = temp["Memo"];
                   // log.debug('Export_Log----memo=>', 'memo:=>' + memo);
                    if (memo == null) {
                        memo = ' ';
                    }

                    //Regular Expression to remove special character
                    var str3 = memo;
                    //log.debug('str3=>',str3);
                    var memo = str3.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');

					// Removing all the commas
					   var memo = str3.replace(/,/g, "");
					
                    var str30 = memo;
                    //log.debug('str30=>',str30);
                      var memo = str30.replace(/\s/g, ' ');
					
					
                    var description = temp["Description"];
                    //log.debug('Export_Log----description=>', 'description:=>' + description);
                    if (description == null) {
                        description = ' ';
                    }

                    //Regular Expression to remove special character
                    var str18 = description;
                    //log.debug('str18=>',str18);
                    var description = str18.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					

                    var item = temp["Item"];
                    //log.debug('Export_Log----item=>', 'item:=>' + item);
                    if (item == null) {
                        item = ' ';
                    }

					// Removing all the commas
					//var item = item.replace(/,/g, " ");  //Added on 30/09/2020
					
					 var str60 = item;
					//Checking  "
					 var pos_first = str60.indexOf('"');
					 //log.debug('Export_Log----pos_first=>', 'pos_first:=>' + pos_first);
					
					
					if(pos_first == 0)
					{
						      var item = str60
						
					}else
					{
						// For Keeping commas in column
						var item = '"' + str60 + '"';
					}
					
                    //Regular Expression to remove special character
                    //var str19 = item;
                    //log.debug('str19=>',str19);
                    //var item = str19.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,' ');

                    var hour = temp["Hour"];
                    //log.debug('Export_Log----hour=>', 'hour:=>' + hour);
                    if (hour == null) {
                        hour = ' ';
                    }
                    var empplyee_Type = temp["Employee Type"];
                    //log.debug('Export_Log----empplyee_Type=>', 'empplyee_Type:=>' + empplyee_Type);
                    if (empplyee_Type == null) {
                        empplyee_Type = ' ';
                    }

                    //Regular Expression to remove special character
                    var str20 = empplyee_Type;
                    //log.debug('str20=>',str20);
                    //var empplyee_Type = str20.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var empplyee_Type = '"' + str20 + '"';

                    var person_Type = temp["Person Type"];
                    //log.debug('Export_Log----person_Type=>', 'person_Type:=>' + person_Type);
                    if (person_Type == null) {
                        person_Type = ' ';
                    }
                    //Regular Expression to remove special character
                    var str21 = person_Type;
                    //log.debug('str21=>',str21);
                    //var person_Type = str21.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var person_Type = '"' + str21 + '"';

                    var onsite_offsite = temp["Onsite/Offsite"];
                    //log.debug('Export_Log----onsite_offsite=>', 'onsite_offsite:=>' + onsite_offsite);
                    if (onsite_offsite == null) {
                        onsite_offsite = ' ';
                    }
                    var billing_type = temp["Billing Type"];
                    if (billing_type == null) {
                        billing_type = ' ';
                    }
					else  //// added by shravan on 01 march
					{
						var s_Billing_Type = billing_type.split(',');
						var i_Comma_Index = billing_type.indexOf(',')
						if(i_Comma_Index != parseInt(-1))
						{
							billing_type = s_Billing_Type[0] + s_Billing_Type[1]
						} //// if(s_Billing_Type)
					
					} /// Else of if (billing_type == null)
					  // log.debug('Export_Log----billing_type=>', 'billing_type:=>' + billing_type);
                    var proj_Category = temp["Proj Category"];
                    //log.debug('Export_Log----proj_Category=>', 'proj_Category:=>' + proj_Category);
                    if (proj_Category == null) {
                        proj_Category = ' ';
                    }
					
                    //Regular Expression to remove special character
                    var str22 = proj_Category;
                    //log.debug('str22=>',str22);
                    //var proj_Category = str22.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var proj_Category = '"' + str22 + '"';

                    var trns_line_mis_practice = temp["MIS Practice"];
                    //log.debug('Export_Log----trns_line_mis_practice=>', 'trns_line_mis_practice:=>' + trns_line_mis_practice);
                    if (trns_line_mis_practice == null) {
                        trns_line_mis_practice = ' ';
                    }

                    var books_id = temp["BooksId"];
                    //log.debug('Export_Log----books_id=>', 'books_id:=>' + books_id);

                    var books = temp["Books"];
                    //log.debug('Export_Log----books=>', 'books:=>' + books);
                    if (books == null) {
                        books = ' ';
                    }
					
					//Regular Expression to remove special character
                    var str43 = books;
                    //log.debug('str43=>',str43);
                    //var books = str43.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var books = '"' + str43 + '"';

                    var transaction = temp["Transaction"];
                    //log.debug('Export_Log----transaction=>', 'transaction:=>' + transaction);
                    if (transaction == null) {
                        transaction = ' ';
                    }
                    var type_revised = temp["Type Revised"];
                    //log.debug('Export_Log----type_revised=>', 'type_revised:=>' + type_revised);
                    if (type_revised == null) {
                        type_revised = ' ';
                    }

                    //Regular Expression to remove special character
                    var str23 = type_revised;
                    //log.debug('str23=>',str23);
                    //var type_revised = str23.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
					
					// For Keeping commas in column
					var type_revised = '"' + str23 + '"';

                    var cost_head_base_new = temp["Cost Head Base New"];
                    //log.debug('Export_Log----cost_head_base_new=>', 'cost_head_base_new:=>' + cost_head_base_new);
                    if (cost_head_base_new == null) {
                        cost_head_base_new = ' ';
                    }
					
					
					 var type_account = temp["Account Type"];
                    //log.debug('Export_Log----Account Type=>', 'Account Type:=>' + type_account);
                    if (type_account == null) {
                        type_account = ' ';
                    }
					
					
				//Calculation Start:
                    var total_USD = 0;
					var usd_Amount_total = 0;
					var usd_inr_cost_Rate_Amount = 0;
					var usd_to_EUR_total = 0;
					var usd_to_AUD_total = 0;
					var usd_to_RON_total = 0;
					var usdinr_rate_total = 0;
					var usd_inr_total = 0 ;
					var usd_inr_cost_tot = 0;
					
				 //If Cost head base= 'Revenue' and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / USD-INR-REVENUE-RATE
                    if ((cost_head_base_new == "Revenue" && (books_id == 3 || books_id ==13)) || (cost_head_base_new == "Revenue" && books_id == 9)) {
                        /*log.debug('Calculation_Log----usd_inr_revenue_Rate=>', 'usd_inr_revenue_Rate:=>' + usd_inr_revenue_Rate);
                        log.debug('Calculation_Log----trans_line_Amount_Trans_currency=>', 'trans_line_Amount_Trans_currency=>' + trans_line_Amount_Trans_currency);
                        log.debug('Calculation_Log----cost_head_base_new=>', 'cost_head_base_new:=>' + cost_head_base_new);
                        log.debug('Calculation_Log----books_id=>', 'books_id:=>' + books_id);
                        log.debug('Calculation_Log----currency=>', 'currency:=>' + currency);*/

                        var usd_Amount_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_inr_revenue_Rate);
                        var total_USD = usd_Amount_total.toFixed(3);
                        //log.debug('Calculation_Log----USD-INR REVENUE RATE=>', 'Calculated USD-INR REVENUE RATE Amount:=>' + total_USD);
                    }

                    //If Cost head base IN ("Salary Cost","Other Cost") and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / USD-INR COST RATE
                    if ( (cost_head_base_new == "Salary Cost" && (books_id == 3 || books_id ==13)) || (cost_head_base_new == "Salary Cost" && books_id == 9) || (cost_head_base_new == "Other Cost" && books_id == 9) || (cost_head_base_new == "Other Cost" && (books_id == 3 || books_id ==13))) {
                        var usd_inr_cost_Rate_Amount = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_inr_cost_Rate);
                        var total_USD = usd_inr_cost_Rate_Amount.toFixed(3);
                        //log.debug('Calculation_Log---- USD-INR COST RATE=>', 'Calculated USD-INR COST RATE :=>' + total_USD);
                    }

                    //IF  subsidiary = UK  THEN USD-TO-GBP / AMOUNT
                    if (books_id == 7) {
                        var usd_to_EUR_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_to_GBP);
                        var total_USD = usd_to_EUR_total.toFixed(3);
                        //log.debug('Calculation_Log---- USD-TO-GBP=>', 'Calculated USD-TO-GBP Amount:=>' + total_USD);
                    }

                    //IF  subsidiary = Canada THEN USD-TO-CAD / AMOUNT
                    if (books_id == 10) {
                        var usd_to_AUD_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_to_CAD);
                        var total_USD = usd_to_AUD_total.toFixed(3);
                        //	log.debug('Calculation_Log---- USD-TO-CAD=>', 'Calculated USD-TO-CAD Amount:=>' + total_USD);
                    }

                    //IF  subsidiary = Cognetik Srl THEN USD-TO-RON / AMOUNT
                    if (books_id == 12) {
                        var usd_to_RON_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_to_RON);
                        var total_USD = usd_to_RON_total.toFixed(3);
                        //log.debug('Calculation_Log---- USD-TO-RON Amount=>', 'USD-TO-RON Amount:=>' + total_USD);
                    }

                    //IF  subsidiary IN (Inc, BLLC,Cognetik Corp,Comity Inc.) THEN AMOUNT
                    if (books_id == 1 || books_id == 2 || books_id == 11 || books_id == 8 || books_id == 14) {
                        var total_USD = trans_Accounting_line_Amount;
                        //log.debug('Calculation_Log---- Inc, BLLC,Cognetik Corp,Comity Inc AMOUNT =>', 'TotalInc, BLLC,Cognetik Corp,Comity Inc AMOUNT:=>' + total_USD);

                    }	
					
				//Code added on 28/09/2020
				
	             //If Cost head base = 'Ignore' and Account Type IN ('Income','Other Income') and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / usd_inr_revenue_Rate
					
					//if ((cost_head_base_new == "Ignore" && Account_Type == "Income" || cost_head_base_new == "Ignore" && Account_Type == "Other Income") && ((books_id ==  3) ||  books_id ==  9)){
                        
					if ((cost_head_base_new == "Ignore") && ( type_account == "Income" || type_account == "Other Income"  || type_account == "OthIncome") && (books_id ==  3 ||  books_id ==  9 ||  books_id ==  13))
					{
						
						var usdinr_rate_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_inr_revenue_Rate);
                        var total_USD = usdinr_rate_total.toFixed(3);
                        //log.debug('Calculation_Log----USD-INR COST RATE=>', 'Calculated USD-INR COST RATE:=>' + total_USD);
                    }

			// If Cost head base = 'Ignore'  and Account.id = 580 and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / usd_inr_revenue_Rate
			    if ((cost_head_base_new == "Ignore" && account == 580 ) && (books_id ==  3 ||  books_id ==  9)){
                        var usd_inr_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_inr_revenue_Rate);
                        var total_USD = usd_inr_total.toFixed(3);
                        //log.debug('Calculation_Log----USD-INR COST RATE=>', 'Calculated USD-INR COST RATE:=>' + total_USD);
                    }
			
				
				//If Cost head base = 'Ignore' and Account Type !IN ('Income','Other Income') and subsidiary IN (BTPL,COMMITY India) THEN AMOUNT / usd_inr_cost_Rate
				//if ((cost_head_base_new == "Ignore" && Account_Type != "Income" || cost_head_base_new == "Ignore" && Account_Type != "Other Income") && ((books_id ==  3) ||  books_id ==  9)){
                //  if ((cost_head_base_new == "Ignore") && (type_account != "Income" || type_account != "Other Income") && ((books_id ==  3 )|| (books_id ==  9)) )
				  
			      //log.debug('Calculation_Log----type_account=>', 'type_account***=>' + type_account);
				  if ((cost_head_base_new == "Ignore") && ( type_account == "Expense" || type_account == "OthExpense" || type_account == "COGS" ) && (books_id ==  3 ||  books_id ==  9))
				  {
					    //log.debug('Calculation_Log----documents=>', 'documents***=>' + documents);
						//log.debug('Calculation_Log----Book=>', 'book***=>' + books_id);
						//log.debug('Calculation_Log----type_account=>', 'type_account***=>' + type_account);
						//log.debug('Calculation_Log----cost_head_base_new=>', 'cost_head_base_new***=>' + cost_head_base_new);
						//log.debug('Calculation_Log----trans_Accounting_line_Amount=>', 'trans_Accounting_line_Amount:=>' + trans_Accounting_line_Amount);
						//log.debug('Calculation_Log----usd_inr_cost_Rate=>', 'usd_inr_cost_Rate***=>' + usd_inr_cost_Rate);
					  
				  var usd_inr_cost_tot = parseFloat(trans_Accounting_line_Amount) / parseFloat(usd_inr_cost_Rate);
                        var total_USD = usd_inr_cost_tot.toFixed(3);
                        //log.debug('Calculation_Log----usd_inr_cost_Rate=>', 'Calculated usd_inr_cost_Rate:=>' + total_USD);
                    }
                    
					

				
				
				
			//End Code added on 28/09/2020
             
                //Calculation End

                    var cost_head_details_new = temp["Cost Head Details_New"];
                    //log.debug('Export_Log----cost_head_details_new=>', 'cost_head_details_new:=>' + cost_head_details_new);
                    if (cost_head_details_new == null) {
                        cost_head_details_new = ' ';
                    }
                 /*   var sub_practice_new = temp["Sub1 Practice1"];
                    //log.debug('Export_Log----sub_practice_new=>', 'sub_practice_new:=>' + sub_practice_new);
                    if (sub_practice_new == null) {
                        sub_practice_new = ' ';
                    }
					*/
                    var revised_Client_Name_account = temp["Global Customer Name"];  
                    //log.debug('Export_Log----revised_Client_Name_account=>', 'revised_Client_Name_account:=>' + revised_Client_Name_account);
                    if (revised_Client_Name_account == null) {
                        revised_Client_Name_account = ' ';
                    }

                    //Regular Expression to remove special character
                    var str1 = revised_Client_Name_account;
                    //log.debug('str1=>',str1);
                    //var global_customer_name = str1.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
				   
				    // Removing all the commas
					//var revised_Client_Name_account = str1.replace(/,/g, "");
					
					//Keeping commas in column of CSV
					var revised_Client_Name_account = '"' + str1 + '"';

                    var final_Ledger = temp["Final Ledger"];
                    //log.debug('Export_Log----final_Ledger=>', 'final_Ledger:=>' + final_Ledger);
                    if (final_Ledger == null) {
                        final_Ledger = ' ';
                    }
                    var practice_received = temp["Practice Revised"];
                    //log.debug('Export_Log----practice_received=>', 'practice_received:=>' + practice_received);
                    if (practice_received == null) {
                        practice_received = ' ';
                    }
                    var tb_reco_final = temp["TB Reco Final"];
                    //log.debug('Export_Log----tb_reco_final=>', 'tb_reco_final:=>' + tb_reco_final);
                    if (tb_reco_final == null) {
                        tb_reco_final = ' ';
                    }
                    var month = temp["Month"];
                    //log.debug('Export_Log----month=>', 'month:=>' + month);
                    if (month == null) {
                        month = ' ';
                    }
                    var month_new = temp["MONTH_NEW"];
                    //log.debug('Export_Log----month_new=>', 'month_new:=>' + month_new);
                    if (month_new == null) {
                        month_new = ' ';
                    }
                    var is_revenew = temp["is revenew"];
                    //log.debug('Export_Log----is_revenew_id=>', 'is_revenew_id:=>' + is_revenew_id); 
                    if (is_revenew == null) {
                        is_revenew = ' ';
                    }
					
					var memo_line = temp["Memo Line"];
                    //log.debug('Export_Log----memo_line=>', 'memo_line:=>' + memo_line); 
                    if (memo_line == null) {
                        memo_line = ' ';
                    }
					
					//Regular Expression to remove special character
                    var str40 = memo_line;
                    //log.debug('str40=>',str40);
                      var memo_line = str40.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');
				   
				    //Removing all the commas
					var memo_line = str40.replace(/,/g, "");
					
					//var str_40 = memo_line;
					//var xyz = str_40.replace(/\s+/g, ' ');
					//var memo_line = xyz;
                  
				  
				  //Remove extra space
                    var str_40 = memo_line;
				    var replaced = str_40.replace(/\s/g, ' ');
				    var memo_line = replaced;
				
				
				    
					var cost_Head_Dtls = temp["Cost Head - Detailed"];
                    //log.debug('Export_Log----cost_Head_Dtls=>', 'cost_Head_Dtls:=>' + cost_Head_Dtls);   
                    if (cost_Head_Dtls == null) {
                        cost_Head_Dtls = ' ';
                    }
					
					//Regular Expression to remove special character
                    var str41 = cost_Head_Dtls;
                    //log.debug('str41=>',str41);
                    var cost_Head_Dtls = str41.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ' ');



					//Change the Lable and considered the field as Sub Practice Revised 0n 27-10-2020
					var sub_practice_revised = temp["Sub Practice Revised"];
                    //log.debug('Export_Log----sub_practice=>', 'sub_practice:=>' + sub_practice);
                    if (sub_practice_revised == null) {
                        sub_practice_revised = ' ';
                    }

                    //Regular Expression to remove special character
                    var str42 = sub_practice_revised;
                    
					// For Keeping commas in column
					var sub_practice_revised = '"' + str42 + '"';




                    //var	f_string =  + account + ',' + acc_account + ',' + account_type + ',' + type + ',' + date + ',' + documents + ',' + name + ',' + trans_Accounting_line_Amount + ',' + trans_line_Amount_Trans_currency + ',' + total_USD + ',' + currency + ',' + billing_From + ',' + billing_To + ',' + cust_name + ',' + global_customer_name +',' + project_id + ',' + entity_project_type +',' + transaction_line_project_Type +',' + project_Name + ',' + employee_id + ',' + emaployee_Name + ',' + project_Region + ',' + project_Setup + ',' + project_Type + ',' + l_location + ',' + memo + ',' + description + ',' + item + ',' + hour + ',' + empplyee_Type + ',' + person_Type + ',' + onsite_offsite + ',' + billing_type + ',' + trns_line_mis_practice + ',' + books + ',' + transaction + ',' + type_revised + ',' + sub_practice+ ',' + practice_received + ',' + tb_reco_final + ',' + cost_head_base_new + ','+ cost_head_details_new + ','+ final_Ledger + ','+ region_New + ','+ parent_Executing_Practice + ','+sub_practice_new+ ','+ parent_Practice + ','+ proj_Category + ','+ is_revenew + ','+ month_new + ','+ month;
                  //  var f_string = acc_account + ',' + account_type + ',' + date + ',' + documents + ',' + name + ',' + trans_Accounting_line_Amount + ',' + billing_From + ',' + billing_To + ',' + cust_name + ',' + project_id + ',' + project_Name + ',' + employee_id + ',' + emaployee_Name + ',' + parent_Executing_Practice + ',' + sub_practice_new + ',' + parent_Practice + ',' + project_Region + ',' + project_Setup + ',' + l_location + ',' + memo +',' + memo_line + ',' + description + ',' + item + ',' + hour + ',' + empplyee_Type + ',' + person_Type + ',' + onsite_offsite + ',' + billing_type + ',' + proj_Category + ',' + books + ',' + type_revised + ',' + cost_head_base_new+ ',' + revised_Client_Name_account + ',' + region_New + ',' + sub_practice + ',' + practice_received + ',' + total_USD + ',' +cost_head_details_new  + ',' + final_Ledger +',' + cost_Head_Dtls + ',' + tb_reco_final + ',' + month_new;
					 var f_string = acc_account + ',' + account_type + ',' + date + ',' + documents + ',' + name + ',' + trans_Accounting_line_Amount + ',' + billing_From + ',' + billing_To + ',' + cust_name + ',' + project_id + ',' + project_Name + ',' + employee_id + ',' + emaployee_Name + ',' + parent_Executing_Practice + ',' + sub_practice + ',' + parent_Practice + ',' + project_Region + ',' + project_Setup + ',' + l_location + ',' + memo +',' + memo_line + ',' + description + ',' + item + ',' + hour + ',' + empplyee_Type + ',' + person_Type + ',' + onsite_offsite + ',' + billing_type + ',' + proj_Category + ',' + books + ',' + type_revised + ',' + cost_head_base_new+ ',' + revised_Client_Name_account + ',' + region_New + ',' + sub_practice_revised + ',' + practice_received + ',' + total_USD + ',' +cost_head_details_new  + ',' + final_Ledger +',' + cost_Head_Dtls + ',' + tb_reco_final + ',' + month_new;
					
					//log.debug("f_string ",f_string);


                    csvFile.appendLine({
                        value: f_string
                    });

                } //End of For

                //log.debug('queryResultSet.types', 'queryResultSet.types=' + queryResultSet.types);
                var remainingUsage = scriptObj.getRemainingUsage();
                log.debug("Remaining governance units: ", remainingUsage);
                //if(remainingUsage >1500)
                {
                    //log.debug("In units Usages: " , remainingUsage);
                    var csvFileId = csvFile.save();
                    var current_user = runtime.getCurrentUser();
                    var userRole = current_user.role;
                    var userName = current_user.name;
                    var userId = current_user.id;
                    var fileObj = file.load({
                        id: csvFileId
                    }); //file id
                    log.debug('Search Result----> fileObj=>', fileObj);

                    var filesize = fileObj.size; // Retuns in bytes
                    log.debug('Search Result----> filesize=>', filesize);
					
                    var file_url;
                    //Search for getting dynamic url of file: Search name in SB/Production -> "File Cabinate URL Search"
                    var fileSearchObj = search.create({
                        type: "file",
                        filters: [
                            ["internalid", "anyof", csvFileId]
                        ],
                        columns: [
                            search.createColumn({
                                name: "url",
                                label: "URL"
                            })
                        ]
                    });
                    var searchResultCount = fileSearchObj.runPaged().count;
                    log.debug("fileSearchObj result count", searchResultCount);
                    fileSearchObj.run().each(function(result) {

                        file_url = result.getValue({
                            name: "url",
                            label: "URL"
                        })
                        log.debug('file_url----> file_url=>', file_url);

                        return true;
                    });

                    //var url = "https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=2667200&c=3883006_SB1&h=c534ae3f48af52f692f6&_xt=.csv";

                   // var sandbox_url_prefix = "https://3883006-sb1.app.netsuite.com";

				    var production_url_prefix = "https://3883006.app.netsuite.com";
                  
				  //Concate URL
                  //var url = sandbox_url_prefix + file_url;
					var url = production_url_prefix + file_url;
                    log.debug('url----> url=>', url);

                    //log.debug('Search Result----> Current User=>',current_user);
                    //log.debug('Search Result----> Current Role=>', userRole);
                    //log.debug('Search Result----> Current User Name=>', userName);
                    //log.debug('Search Result----> Current User Id=>', userId);


                    email.send({
                        author: current_user.id,
                        recipients: current_user.id,
                        subject: 'Income Statement PP',
                        //body: 'Hi,<br/>Please find the <b> Income Statement PP Report</b> on ' + file_link + ' and ' + year + '. <br/><br/><br/>Thanks',
                        body: "<html><body><p>Hello " + userName + ", <br/></p><p>Please click on the link below and download the Income Statement for " + month + ".</p> </br> <p><a href=" + url + " > View Record </a> </p></br></br><p>Thanks, </p></br></body></html>",
                        relatedRecords: null
                    });

                    //log.debug('Email Sent', userName);
                    log.debug('Email Sent', 'Email Sent=>' + userName);
                }

            } catch (err) {
					log.debug('Exception', err);
					var current_user = runtime.getCurrentUser();

					var scriptObj = runtime.getCurrentScript();
					var mailTemplate = "";
					mailTemplate += "<p> This is to inform that System is not able to send the Income Statement PP file due to script has been failed.And It is having Error - <b>["+err.name+" : "+err.message +"]</b></p>";

					mailTemplate += "<p> Kindly have a look on to the Error and Please do needfull.</p>";
					mailTemplate += "<p><b> Script Id:- "+scriptObj.id +"</b></p>";
					mailTemplate += "<p><b> Script Deployment Id:- "+scriptObj.deploymentId +"</b></p>";
					mailTemplate += "<br/>"
					mailTemplate += "<p>Regards, <br/> Information Systems</p>";
					email.send({
							author: current_user.id,
							recipients: [current_user.id,'information.systems@brillio.com','netsuite.support@brillio.com'],
							subject: 'Failure Notification on Income Statement PP',
							body: mailTemplate,
							relatedRecords: null
						});
            }

            log.debug('exit', 'End of Script');
        } //End of Function


        return {
            execute: schedule_workbook
        };

    }); //End of main function   

function _logValidation(value) {
    if (value != null && value!= '- None -' && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
