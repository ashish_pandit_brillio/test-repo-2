/**
@NApiVersion 2.0
@NScriptType ClientScript
@NModuleScope Public
*/
define(['N/search'], function(search) {
    function showMessage(context) {

        var rec = context.currentRecord;
		if (context.sublistId == 'item'){
        //if (context.sublistId == 'item' && context.fieldId=='quantity') {
            var i_po_id = rec.getValue({
                fieldId: 'custbody3'
            }); //Fetch the PO Intenal Id
            var quant = context.currentRecord.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'quantity',
            });
            var i_item = context.currentRecord.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
            });

            if (i_po_id) //Check the validation if po exits
            {
                try {
                    var i_qty = 0;
                    var purchaseorderSearchObj = search.create({
                        type: "purchaseorder",
                        filters: [
                            ["type", "anyof", "PurchOrd"],
                            "AND",
                            ["formulatext: {tranid}", "is", i_po_id],
                            "AND",
                            ["item", "anyof", i_item]
                        ],
                        columns: [
                            search.createColumn({
                                name: "quantity",
                                summary: "SUM",
                                label: "Quantity"
                            }),

                        ]
                    });
                    var searchResultCount = purchaseorderSearchObj.runPaged().count;

                    var searchresult = purchaseorderSearchObj.run().getRange({
                        start: 0,
                        end: 100
                    });

                    if (searchResultCount != 0) {
                        var i_qty = searchresult[0].getValue({
                            name: "quantity",
                            summary: "SUM",
                        });

                        i_qty = parseFloat(Number(i_qty));
                    }

                    if (parseFloat(i_qty) != parseFloat(quant)) {
                        alert("Quantity should be lesser or equal to billable qty:" + quant);
                        return false;
                    } else {
                        return true;
                    }

                } catch (e) {
                    alert(e);

                }

            }
          return true;
        }


    }



    return {
        validateLine: showMessage,
		//fieldChanged:showMessage
    };
});