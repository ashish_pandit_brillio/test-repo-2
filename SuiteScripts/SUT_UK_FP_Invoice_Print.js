/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
       	Script Name : SUT_BLLC_FP_Invoice_Print.js
    	Author      : Shweta Chopde
    	Date        : 3 June 2014
    	Description : Create a Invoice Layout


    	Script Modification Log:

    	-- Date --			-- Modified By --				--Requested By--				-- Description --
       26 June 2014           Shweta Chopde                      Shekar                      Modified IDs as per the sandbox accounts
     


    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.


         SUITELET
    		- suiteletFunction(request, response)


         SUB-FUNCTIONS
    		- The following sub-functions are called by the above core functions in order to maintain code
                modularization:

                   - NOT USED

    */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================




// BEGIN SUITELET ==================================================

function suiteletFunction(request, response) {
    var i_vat = 0;
    var strVar = "";
    var i_total = 0;
    var i_project;
    var i_addr1 = '';
    var s_currency_symbol = '';
    var linenum = '17';
    var pagening = 4;
    var pagecount = 0;
    var s_remittance_details = "Accounts Receivable,  BRILLIO UK LIMITED;\nDevonshire House, 60 Goswell Road;\nLondon EC1M 7AD\n United Kingdom"

    var s_brillio_address = " Devonshire House, 60 Goswell Road;\nLondon EC1M 7AD\n United Kingdom"

    s_remittance_details = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_remittance_details));
    s_brillio_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_brillio_address));

    try {
        var i_recordID = request.getParameter('i_recordID');
        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Record ID -->' + i_recordID);

        if (_logValidation(i_recordID)) {
            var o_recordOBJ = nlapiLoadRecord('invoice', i_recordID);

            if (_logValidation(o_recordOBJ)) {
                var i_customer = o_recordOBJ.getFieldValue('entity');
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer -->' + i_customer);

                if (_logValidation(i_customer)) {
                    var o_customerOBJ = nlapiLoadRecord('customer', i_customer);

                    if (_logValidation(o_customerOBJ)) {
                        i_customer_name = o_customerOBJ.getFieldValue('altname')
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer Name -->' + i_customer_name);

                    } //Customer OBJ				
                } //Customer	

                var i_customer_txt = o_recordOBJ.getFieldText('entity');
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer Text -->' + i_customer_txt);

                var i_tran_date = o_recordOBJ.getFieldValue('trandate')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tran Date --> ' + i_tran_date);

                var i_tran_id = o_recordOBJ.getFieldValue('tranid')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Invoice Number --> ' + i_tran_id);

                if (!_logValidation(i_tran_id)) {
                    i_tran_id = ''
                }
                var i_bill_address = nlapiEscapeXML(o_recordOBJ.getFieldValue('billaddress'))
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill Address --> ' + i_bill_address);
                // i_bill_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_bill_address));

                if (!_logValidation(i_bill_address)) {
                    i_bill_address = ''
                }
                var i_PO_ID = o_recordOBJ.getFieldValue('otherrefnum')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO # --> ' + i_PO_ID);

                if (!_logValidation(i_PO_ID)) {
                    i_PO_ID = ''
                }

                var s_memo = o_recordOBJ.getFieldValue('memo')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Memo --> ' + s_memo);

                if (!_logValidation(s_memo)) {
                    s_memo = ''
                }
                var i_terms = o_recordOBJ.getFieldText('terms')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Terms --> ' + i_terms);

                if (!_logValidation(i_terms)) {
                    i_terms = ''
                }
                var i_bill_to = o_recordOBJ.getFieldValue('custbody_billto')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill To --> ' + i_bill_to);

                if (!_logValidation(i_bill_to)) {
                    i_bill_to = ''
                }
                var i_bill_from = o_recordOBJ.getFieldValue('custbody_billfrom')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill From --> ' + i_bill_from);

                if (!_logValidation(i_bill_from)) {
                    i_bill_from = ''
                }
                var i_billing_description = o_recordOBJ.getFieldValue('custbody_billingdescription')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill Description --> ' + i_billing_description);

                if (!_logValidation(i_billing_description)) {
                    i_billing_description = ''
                }

                var i_service_tax_category = o_recordOBJ.getFieldText('custbody_servicetaxcategory')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Service Tax Category --> ' + i_service_tax_category);

                if (!_logValidation(i_service_tax_category)) {
                    i_service_tax_category = ''
                }
                var i_ST_Classification = o_recordOBJ.getFieldText(' custbody_stclassificationus')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' ST Classification U/S --> ' + i_ST_Classification);

                if (!_logValidation(i_ST_Classification)) {
                    i_ST_Classification = ''
                }

                var i_phone_no = '';
                var i_customer_address = '';
                var i_attention = '';
                var i_service_tax_reg_no = '';
                var i_CIN_No = '';
                var i_phone_no = '';
                var i_addr1 = '';
                var i_addr2 = '';
                var i_city = '';
                var i_zip = '';
                var i_country = '';
                var i_state = '';


                var a_customer_array = get_customer_details(i_customer)
                var a_split_array = new Array();
                if (_logValidation(a_customer_array)) {
                    a_split_array = a_customer_array[0].split('######')

                    i_customer_address = a_split_array[0];
                    i_attention = a_split_array[1];
                    i_service_tax_reg_no = a_split_array[2];
                    i_CIN_No = a_split_array[3];
                    i_phone_no = a_split_array[4];
                    i_addr1 = a_split_array[5];
                    i_addr2 = a_split_array[6];
                    i_city = a_split_array[7];
                    i_zip = a_split_array[8];
                    i_country = a_split_array[9];
                    i_state = a_split_array[10];


                } //Customer Array

                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer Address--> ' + i_customer_address);
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Phone No. --> ' + i_phone_no);

                i_customer_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_customer_address));

                var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary')

                var i_address = get_address(i_subsidiary)
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address --> ' + i_address);
                i_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address));


                var i_address_rem = nlapiEscapeXML(get_address_rem(i_subsidiary))
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address --> ' + i_address_rem);

                if (i_customer_address.toString() == 'null' || i_customer_address.toString() == null || i_customer_address == null || i_customer_address == '' || i_customer_address == undefined) {
                    i_customer_address = '';
                } //Customer Address


                if (i_phone_no.toString() == 'null' || i_phone_no.toString() == null || i_phone_no == null || i_phone_no == '' || i_phone_no == undefined) {
                    i_phone_no = '';
                } //Phone No



                if (i_attention.toString() == 'null' || i_attention.toString() == null || i_attention == null || i_attention == '' || i_attention == undefined) {
                    i_attention = '';
                } //Customer Address

                if (i_service_tax_reg_no.toString() == 'null' || i_service_tax_reg_no.toString() == null || i_service_tax_reg_no == null || i_service_tax_reg_no == '' || i_service_tax_reg_no == undefined) {
                    i_service_tax_reg_no = '';
                } //Phone No


                if (i_CIN_No.toString() == 'null' || i_CIN_No.toString() == null || i_CIN_No == null || i_CIN_No == '' || i_CIN_No == undefined) {
                    i_CIN_No = '';
                } //Customer Address

                if (i_addr1.toString() == 'null' || i_addr1.toString() == null || i_addr1 == null || i_addr1 == '' || i_addr1 == undefined) {
                    i_addr1 = '';
                } //Phone No

                if (i_addr2.toString() == 'null' || i_addr2.toString() == null || i_addr2 == null || i_addr2 == '' || i_addr2 == undefined) {
                    i_addr2 = '';
                } //Phone No			 

                if (i_city.toString() == 'null' || i_city.toString() == null || i_city == null || i_city == '' || i_city == undefined) {
                    i_city = '';
                } //Customer Address

                if (i_zip.toString() == 'null' || i_zip.toString() == null || i_zip == null || i_zip == '' || i_zip == undefined) {
                    i_zip = '';
                } //Phone No

                if (i_country.toString() == 'null' || i_country.toString() == null || i_country == null || i_country == '' || i_country == undefined) {
                    i_country = '';
                } //Phone No


                if (i_state.toString() == 'null' || i_state.toString() == null || i_state == null || i_state == '' || i_state == undefined) {
                    i_state = '';
                } //Customer Address


                var i_address_u = i_addr1 + '\n' + i_addr2 + '\n' + i_city + ',' + i_state + '\n' + i_zip + '\n' + i_country;
                i_address_u = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address_u));

                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer Address--> ' + i_customer_address);
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Phone No. --> ' + i_phone_no);

                i_customer_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_customer_address));

                var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary')

                //var i_subsidiary_text = o_recordOBJ.getFieldText('subsidiary') // Updated on 11/16/21 NIS-2562
                var i_subsidiary_text = "BRILLIO UK Ltd";
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Subsidiary --> ' + i_subsidiary_text);

                /* var i_address = get_address(i_subsidiary)
		    nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address --> ' + i_address);
          
		    var s_header_address = header_adress(i_address,i_subsidiary_text)			
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Header Address --> ' + s_header_address); */

                if (nlapiStringToDate(i_tran_date) >= nlapiStringToDate('11/1/2017')) {
                    //var i_address_rem = "BRILLIO UK LIMITED" 
                    //i_address_rem = i_address_rem +"\nNo. 2043, Ww Moor Place Limited,\n1 Fore Street Avenue,\nLondon EC2Y 9DT\nUnited Kingdom";
                    //var i_adress_rem_ = "\nNo. 2043, Ww Moor Place Limited,\n1 Fore Street Avenue,\nLondon EC2Y 9DT\nUnited Kingdom";
                    var i_address_rem = "BRILLIO UK Ltd"
                    i_address_rem = i_address_rem + "\n03-116, North West House,\n119 Marylebone Road,\nLondon, NW1 5PU\nUnited Kingdom";
                    var i_adress_rem_ = "\n03-116, North West House,\n119 Marylebone Road,\nLondon, NW1 5PU\nUnited Kingdom";
                    i_address_rem = nlapiEscapeXML(i_address_rem);
                    var s_header_address = nlapiEscapeXML(i_adress_rem_);
                } else {
                    var i_address_rem = nlapiEscapeXML(get_address_rem(i_subsidiary))
                    // nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address -->
                    // ' + i_address_rem);
                    var i_address = get_address(i_subsidiary)
                    // nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address -->
                    // ' + i_address);
                    var s_header_address = header_adress(i_address,
                        i_subsidiary_text)
                }

                i_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_header_address));

                var i_currency = o_recordOBJ.getFieldText('currency')
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Currency --> ' + i_currency);


                var i_project_name;
                var i_project = o_recordOBJ.getFieldValue('job')

                if (_logValidation(i_project)) {
                    var o_projectOBJ = nlapiLoadRecord('job', i_project);

                    if (_logValidation(o_projectOBJ)) {
                        i_project_name = o_projectOBJ.getFieldValue('companyname')
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Project Name -->' + i_project_name);

                    } //Customer OBJ				
                } //Customer	
                if (!_logValidation(i_project_name)) {
                    i_project_name = '';
                }
                if (!_logValidation(i_currency)) {
                    i_currency = '';
                }

                if (i_currency == 'USD') {
                    s_currency_symbol = '$'
                }
                if (i_currency == 'INR') {
                    s_currency_symbol = 'Rs.'
                }
                if (i_currency == 'GBP') {
                    s_currency_symbol = '�'
                }
                if (i_currency == 'SGD') {
                    s_currency_symbol = 'S$'
                }
                if (i_currency == 'Peso') {
                    s_currency_symbol = 'P'
                }
                var i_item_count = o_recordOBJ.getLineItemCount('item');
                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item Count --> ' + i_item_count);
                /*

		   		   
		   if (_logValidation(i_item_count))
		   {
		   	 for(var kt=1;kt<=i_item_count;kt++)
			 { 
			 	i_project = o_recordOBJ.getLineItemValue('item','job_display',kt)
				
				 if (!_logValidation(i_project))
				 {
				 	i_project = '';
				 }//Project
				 break;			
			 }//Expense Billable Loop	   	
			
		   }//Expense Billable Count
*/

                // =================================== PDF Generation Code ==================================

                var image_URL = constant.Images.CompanyLogo;
                image_URL = nlapiEscapeXML(image_URL)

                /* strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
	

         strVar += "<tr>";
		 strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\" colspan=\"2\"><b>"+i_subsidiary_text+"<\/b><br/>"+i_address+"</td>";
		
		 if (image_URL != '' && image_URL != null && image_URL != '' && image_URL != 'undefined' && image_URL != ' ' && image_URL != '&nbsp') 
		 {
		  	strVar += " <td width=\"50%\" colspan=\"2\"><img width=\"146\" height=\"75\" align=\"right\" src=\"" + image_URL + "\"><\/img><\/td>";
		 }		 
		 strVar += "</tr>";	 	
			
		 strVar += "</table>";*/

                strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
                strVar += "<tr>";
                strVar += "<td border-bottom=\"0.3\"  font-family=\"Helvetica\" style=\"color:#000000;font-size:35\" font-size=\"25\" width=\"100%\" colspan=\"2\"><b>Invoice<\/b></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
                strVar += "</tr>";

                i_tran_date = nlapiStringToDate(i_tran_date);

                var d_inv_date = get_date(i_tran_date);

                d_inv_date = get_dateFormat(d_inv_date);

                strVar += "<tr>"
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Invoice No. : <\/b>&nbsp;" + i_tran_id + "</td>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Date : &nbsp;<\/b>" + d_inv_date + "</td>";
                strVar += "</tr>";

                strVar += "<tr>"
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
                strVar += "</tr>";

                /*
  strVar += "</table>";	 
	 
	     strVar += "<table border=\"0\"  align=\"center\" width=\"100%\">";
	
*/


                strVar += "<tr>"
                strVar += "<td height=\"70\" border-top=\"0.3\" border-left=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"1\">"

                strVar += "<table border=\"0\"  align=\"center\" width=\"100%\">";
                strVar += "<tr>";
                strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b>Bill To :&nbsp;<\/b></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><PRE font-family=\"Helvetica\" font-size=\"9\">" + i_bill_address + "</PRE></td>";
                strVar += "</tr>";

                var i_address_u = i_addr1 + '\n' + i_addr2 + '\n' + i_city + ',' + i_state + '\n' + i_zip + '\n' + i_country;
                i_address_u = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address_u));

                /*if (_logValidation(i_attention)) 
	{	
	  strVar += "<tr>";
		 strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(i_attention)+"</td>";
	 strVar += "</tr>";
	} 	
		 
	
	 
	if (_logValidation(i_customer_name)) 
	{	
	  strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\">"+nlapiEscapeXML(i_customer_name)+"</td>";
	 strVar += "</tr>";
	} 
		
    if (_logValidation(i_addr1)) 
	{
	 strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_addr1)+"</td>";
	 strVar += "</tr>";
	}
    if (_logValidation(i_addr2)) 
	{
	  strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_addr2)+"</td>";
	 strVar += "</tr>";
	}
     if (_logValidation(i_city)) 
	{
	 strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_city)+"</td>";
	 strVar += "</tr>";	 
	}
    if (_logValidation(i_state)) 
	{
	 strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_state)+"</td>";
	 strVar += "</tr>";	 
	}
    if (_logValidation(i_zip)) 
	{
	 strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_zip)+"</td>";
	 strVar += "</tr>";	 
	}
    if (_logValidation(i_country)) 
	{
	 strVar += "<tr>"
		 strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_country)+"</td>";
	 strVar += "</tr>";
	}
	if (_logValidation(i_phone_no)) 
	{	
	strVar += "<tr>";
		strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\">"+nlapiEscapeXML(i_phone_no)+"</td>";
	strVar += "</tr>";
	} */
                strVar += "</table>";



                strVar += "</td>";
                strVar += "<td height=\"70\" border-top=\"0.3\" border-left=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"1\">"


                strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";

                strVar += "<tr>";
                strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"><b>Remittance Details : <\/b></td>";
                strVar += "</tr>";
                /* if (_logValidation(s_remittance_details)) 
	{	
	strVar += "<tr>";
		strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">"+s_remittance_details+"</td>";
	strVar += "</tr>";
	}*/

                if (_logValidation(i_address_rem)) {
                    strVar += "<tr>";
                    strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><PRE font-family=\"Helvetica\" font-size=\"9\">" + i_address_rem + "</PRE></td>";
                    strVar += "</tr>";
                }

                strVar += "</table>";


                strVar += "</td>";
                strVar += "</tr>";


                strVar += "</table>";



                strVar += "<table border=\"0\"  align=\"center\" width=\"100%\">";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Project Name :<\/b>&nbsp;" + nlapiEscapeXML(i_project_name) + "</td>";
                strVar += "</tr>";

                strVar += "<tr>"
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>PO Number :<\/b>&nbsp;" + i_PO_ID + "</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Terms of Payment:<\/b>&nbsp;" + nlapiEscapeXML(i_terms) + "</td>";
                strVar += "</tr>";
                /*

	    strVar += "<tr>";
			strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Billing Description:&nbsp;<\/b>"+nlapiEscapeXML(i_billing_description)+"</td>";
		strVar += "</tr>";
		
*/

                i_bill_from = nlapiStringToDate(i_bill_from);

                i_bill_from = get_date(i_bill_from);

                i_bill_to = nlapiStringToDate(i_bill_to);

                i_bill_to = get_date(i_bill_to);


                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Billing Period:&nbsp;<\/b>&nbsp;Beginning&nbsp;" + i_bill_from + "&nbsp;thru&nbsp;" + i_bill_to + "</td>";
                strVar += "</tr>";


                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
                strVar += "</tr>";

                /*strVar += "<tr>";
                	strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Notes:<\/b>&nbsp;"+s_memo+"</td>";
                strVar += "</tr>";
                */
                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>"
                strVar += "</tr>";

                strVar += "</table>";

                strVar += " <table border=\"0\" width=\"100%\">";
                strVar += "<tr>";
                strVar += "<td border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b><\/b></td>";
                strVar += "<td border-top=\"0.3\" border-bottom=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b><\/b></td>";
                strVar += "</tr>";
                strVar += "<tr>";
                strVar += "<td border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b>Project Details / Services Details<\/b></td>";
                strVar += "<td border-top=\"0\" border-bottom=\"0.3\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b>Base Amount<\/b></td>";
                strVar += "</tr>";


                nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item Count  --> ' + i_item_count);
                if (_logValidation(i_item_count)) {
                    for (var kt = 1; kt <= i_item_count; kt++) {
                        var i_item = o_recordOBJ.getLineItemValue('item', 'item', kt)
                        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item  --> ' + i_item);

                        if (i_item != 2219) {
                            var i_description = o_recordOBJ.getLineItemValue('item', 'description', kt)
                            nlapiLogExecution('DEBUG', 'suiteletFunction', ' Description  --> ' + i_description);

                            var i_amount = o_recordOBJ.getLineItemValue('item', 'amount', kt)
                            nlapiLogExecution('DEBUG', 'suiteletFunction', ' Amount  --> ' + i_amount);

                            if (!_logValidation(i_description)) {
                                i_description = '';
                            } //Description
                            if (!_logValidation(i_amount)) {
                                i_amount = 0;
                            } //Amount	
                            if (i_currency == 'GBP') {
                                var i_vat = o_recordOBJ.getLineItemValue('item', 'tax1amt', kt)
                                nlapiLogExecution('DEBUG', 'suiteletFunction', ' VAT Amount  --> ' + i_vat);
                                //VAT Amount	


                                i_total = parseFloat(i_total) + parseFloat(i_amount) + parseFloat(i_vat);
                            } else
                                i_total = parseFloat(i_total) + parseFloat(i_amount);

                            nlapiLogExecution('DEBUG', 'suiteletFunction', ' Total--> ' + i_total);

                            strVar += "<tr>";
                            strVar += "<td  align=\"left\" border-left=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                            strVar += "<td  align=\"right\" border-left=\"0\"  border-right=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                            strVar += "</tr>";

                            strVar += "<tr>";
                            strVar += "<td  align=\"left\" border-left=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">" + nlapiEscapeXML(i_description) + "</td>";
                            strVar += "<td  align=\"right\" border-left=\"0\"  border-right=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">" + nlapiEscapeXML(i_amount) + "</td>";
                            strVar += "</tr>";

                            if (i_currency == 'GBP') {
                                strVar += "<tr>";



                                strVar += "</tr>";

                                /*strVar += "<tr>";
                                	strVar += "<td  align=\"left\" border-left=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                                	//strVar += "<td align=\"right\" border-left=\"0\"  border-right=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\"  font-size=\"10\"><b>VAT @20%<\/b></td>";
                                	strVar += "<td  border-left=\"0\"  border-right=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"10\">"+ "<b>VAT @20%<\/b>"+"<span padding-left=\"365px\" font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(i_vat)+"</span>"+"</td>";
                                strVar += "</tr>";*/
                                strVar += "<tr>";
                                strVar += "<td colspan=\"2\" border-right=\"0\"  border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>VAT&nbps;&nbsp;@20%:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/b>" + "&nbsp;" + nlapiEscapeXML(i_vat) + "</td>";
                                strVar += "</tr>";
                            }
                            strVar += "<tr>";
                            strVar += "<td  align=\"left\" border-left=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                            strVar += "<td  align=\"right\" border-left=\"0\"  border-right=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                            strVar += "</tr>";

                            strVar += "<tr>";
                            strVar += "<td  align=\"left\" border-left=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                            strVar += "<td  align=\"right\" border-left=\"0\"  border-right=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                            strVar += "</tr>";

                        }

                    } //Expense Billable Loop	   	

                } //Expense Billable Count
                strVar += "<tr>";
                strVar += "<td colspan=\"2\" border-right=\"0\"  border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>Total&nbps;&nbsp;Amount:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/b>" + "&nbsp;" + formatDollar(parseFloat(i_total).toFixed(2)) + "</td>";
                strVar += "</tr>";

                /*
	strVar += "<tr>";
				strVar += "<td  align=\"right\" border-left=\"0\"  border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total:&nbps;<\/b></td>";
				strVar += "<td  align=\"right\" border-left=\"0\" border-top=\"0.3\"  border-right=\"0\"  border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\">"+s_currency_symbol+"&nbsp;"+formatDollar(parseFloat(i_total).toFixed(2))+"</td>";
			strVar += "</tr>";
*/

                /*
          strVar += "<tr>";			
			      strVar += "<td colspan=\"2\" border-right=\"0\"  border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total:&nbps;"+s_currency_symbol+"&nbsp;"+formatDollar(parseFloat(i_total).toFixed(2))+"<\/b></td>";
		   strVar += "</tr>";	
         */

                /*
            strVar += "<tr>";
				strVar += "<td border-left=\"1\"  border-bottom=\"1\" font-family=\"Helvetica\" font-size=\"9\">&nbsp;</td>";
				strVar += "<td border-bottom=\"1\" border-right=\"1\" font-family=\"Helvetica\" font-size=\"9\"><b>Invoice Total :&nbsp;"+s_currency_symbol+"&nbsp;<\/b>"+formatDollar(parseFloat(i_total).toFixed(2))+"</td>";
			strVar += "</tr>";
         */

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\"  font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>" + i_currency + "</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Amount In Words&nbsp;:&nbsp;<\/b>" + toWordsFunc(parseFloat(i_total).toFixed(2), i_currency) + "</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td  align=\"left\" border-left=\"0\"  border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\" align=\"left\"><b>VAT Registration No:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;319 2933 90</b></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";
                //strVar += "<br></br>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Payment Instruction:		</b></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Bank Details:		</b></td>";
                strVar += "</tr>";
                strVar += "<tr>";
                strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Name of the Account:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSBC UK BANK PLC</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Currency:</b>&nbps;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;GBP</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Sort Code:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;40&nbsp;03&nbsp;04</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Account Number:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;72327252</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>IBAN Number:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GB76HBUK40030472327252</td>";
                strVar += "</tr>";

                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Swift ID / BIC:</b> &nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HBUKGB4B</td>";
                strVar += "</tr>";
                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Bank Name and Address</b>:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSBC UK BANK PLC, The Helicon 1</td>";
                strVar += "</tr>";
                strVar += "<tr>";
                strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbps;&nbps;&nbps;South Place, London, EC2M 2UP.</td>";
                strVar += "</tr>";
                strVar += "</table>";

            } //Record OBJ		
        } //Record ID


        var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
        xml += "<pdf>";
        xml += "<head>";
        xml += "  <macrolist>";
        xml += " <macro id=\"myfooter\">";
        //   xml += "<i font-size=\"7\">";
        xml += " <table width=\"100%\">"
        xml += "<tr>"
        xml += "<td border-top=\"0.3\" font-size=\"8\"  font-family=\"Helvetica\" width=\"100%\" align=\"center\"> Brillio UK LIMITED. certifies that this invoice is correct, that payment has not been received and that it is presented with the understanding that<\/td>"
        xml += "<\/tr> "
        xml += "<tr>"
        xml += "<td border-bottom=\"0.3\" font-size=\"8\"  font-family=\"Helvetica\" width=\"100%\" align=\"center\">the amount to be paid thereunder can be audited to verify.<\/td>"
        xml += "<\/tr> "

        xml += "<tr width=\"100%\">";
        xml += "<td font-size='10' font-family='Times' align=\"right\" width=\"100%\"> Page <pagenumber size='2'/> of <totalpages size='2'/></td>";
        xml += "</tr>";

        xml += "<\/table> "
        //  xml += "<\/i>";
        xml += "    <\/macro>";

        //--------------header repeat------added by swati---------------
        xml += " <macro id=\"myHead\">";
        xml += "<table border=\"0\" align=\"center\" width=\"100%\">";


        xml += "<tr>";
        xml += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\" colspan=\"2\"><b>" + i_subsidiary_text + "<\/b><br/>" + i_address + "</td>";

        if (image_URL != '' && image_URL != null && image_URL != '' && image_URL != 'undefined' && image_URL != ' ' && image_URL != '&nbsp') {
            //strVar += " <td width=\"50%\" colspan=\"2\"><img width=\"100\" height=\"40\" align=\"right\" src=\"" + image_URL + "\"><\/img><\/td>";//146,75
            xml += " <td width=\"50%\" colspan=\"2\"><img width=\"110\" height=\"45\" align=\"right\" src=\"" + image_URL + "\"><\/img><\/td>"; //146,75
        }

        //  xml+="<td font-family=\"Helvetica\" font-size=\"9\"> Page <pagenumber size=\"2\"/>";		

        xml += "</tr>";
        xml += "</table>";
        xml += "    <\/macro>";

        //--------------------------------------------------------------


        xml += "  <\/macrolist>";
        xml += "<\/head>";

        xml += "<body margin-top=\"0pt\"  header=\"myHead\" header-height=\"30mm\" footer=\"myfooter\" footer-height=\"2em\">";
        xml += strVar;
        xml += "</body>\n</pdf>";

        // run the BFO library to convert the xml document to a PDF
        var file = nlapiXMLToPDF(xml);
        //Create PDF File with TimeStamp in File Name

        var d_currentTime = new Date();
        var timestamp = d_currentTime.getTime();
        //Create PDF File with TimeStamp in File Name

        var fileObj = nlapiCreateFile('BLLC FP' + '.pdf', 'PDF', file.getValue());
        fileObj.setFolder(62); // Folder PDF File is to be stored
        var fileId = nlapiSubmitFile(fileObj);
        nlapiLogExecution('DEBUG', 'OT Calculations', '************* PDF ID *********** -->' + fileId);
        response.setContentType('PDF', 'BLLC FP.pdf', 'inline');
        response.write(file.getValue());

    } catch (exception) {
        nlapiLogExecution('DEBUG', 'ERROR', '  Exception Caught -->' + exception);
    }

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function get_customer_details(i_customer) {
    var i_address;
    var i_attention;
    var i_service_tax_reg_no;
    var i_CIN_No;
    var i_phone = '';
    var a_return_array = new Array();
    if (_logValidation(i_customer)) {
        var o_customerOBJ = nlapiLoadRecord('customer', i_customer)

        if (_logValidation(o_customerOBJ)) {
            i_address = o_customerOBJ.getFieldValue('defaultaddress')
            nlapiLogExecution('DEBUG', 'get_customer_address', ' Address -->' + i_address);

            i_service_tax_reg_no = o_customerOBJ.getFieldValue('vatregnumber')
            nlapiLogExecution('DEBUG', 'get_customer_address', ' Service Tax Reg No. -->' + i_service_tax_reg_no);

            i_CIN_No = o_customerOBJ.getFieldValue('custentity_cinnumber')
            nlapiLogExecution('DEBUG', 'get_customer_address', ' CIN No . -->' + i_CIN_No);

            i_phone = o_customerOBJ.getFieldValue('phone')
            nlapiLogExecution('DEBUG', 'get_customer_address', ' Phone No . -->' + i_phone);

            var i_addr_count = o_customerOBJ.getLineItemCount('addressbook')
            nlapiLogExecution('DEBUG', 'get_customer_address', ' Adress Count -->' + i_addr_count);

            if (_logValidation(i_addr_count)) {
                for (var r = 1; r <= i_addr_count; r++) {
                    var i_default_billing = o_customerOBJ.getLineItemValue('addressbook', 'defaultbilling', r)
                    nlapiLogExecution('DEBUG', 'get_customer_address', ' Adress Count -->' + i_addr_count);

                    if (i_default_billing == 'T') {
                        var i_attention = o_customerOBJ.getLineItemValue('addressbook', 'attention', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Attention -->' + i_attention);

                        var i_addr1 = o_customerOBJ.getLineItemValue('addressbook', 'addr1', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Addr1 -->' + i_addr1);

                        var i_addr2 = o_customerOBJ.getLineItemValue('addressbook', 'addr2', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Addr2 -->' + i_addr2);

                        var i_city = o_customerOBJ.getLineItemValue('addressbook', 'city', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' City -->' + i_city);

                        var i_zip = o_customerOBJ.getLineItemValue('addressbook', 'zip', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Zip -->' + i_zip);

                        var i_country = o_customerOBJ.getLineItemText('addressbook', 'country', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' Country -->' + i_country);

                        var i_state = o_customerOBJ.getLineItemValue('addressbook', 'displaystate', r);
                        nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' State -->' + i_state);




                        break;
                    } //Default Billing			
                } //Address Loop			
            } //Address Count		
            a_return_array[0] = i_address + '######' + i_attention + '######' + i_service_tax_reg_no + '######' + i_CIN_No + '######' + i_phone + '######' + i_addr1 + '######' + i_addr2 + '######' + i_city + '######' + i_zip + '######' + i_country + '######' + i_state

        }

    }

    return a_return_array;
}

function formatAndReplaceSpacesofMessage(messgaeToBeSendPara) {
    if (messgaeToBeSendPara != null && messgaeToBeSendPara != '' && messgaeToBeSendPara != undefined) {
        messgaeToBeSendPara = messgaeToBeSendPara.toString();
        messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g, "<br/>"); /// /g
        return messgaeToBeSendPara;
    }

}


function get_address(i_subsidiary) {
    var i_address;
    if (_logValidation(i_subsidiary)) {
        var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)

        if (_logValidation(o_subsidiaryOBJ)) {
            i_address = o_subsidiaryOBJ.getFieldValue('addrtext')

        }
    }

    return i_address;
}

function get_address_rem(i_subsidiary) {
    var i_address;
    if (_logValidation(i_subsidiary)) {
        var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)

        if (_logValidation(o_subsidiaryOBJ)) {
            i_address_rem = o_subsidiaryOBJ.getFieldValue('mainaddress_text')
        }
    }
    return i_address_rem;
}




//--------------------------------Function Begin -  Amount in words----------------------------------------------------------------//
function toWordsFunc(s, i_currency) {
    var str = '';
    if (i_currency == 'USD') {
        str = 'Dollar' + ' '
    }
    if (i_currency == 'INR') {
        str = 'Rs.' + ' '
    }
    if (i_currency == 'GBP') {
        str = '�' + ' '
    }
    if (i_currency == 'SGD') {
        str = 'S$' + ' '
    }
    if (i_currency == 'Peso') {
        str = 'P' + ' '
    }

    return str + toWords(s);
    var th = new Array('Billion ', 'Million ', 'Thousand ', 'Hundred ');
    var dg = new Array('10000000', '1000000', '1000', '100');
    var dem = s.substr(s.lastIndexOf('.') + 1)
    s = parseInt(s)
    var d
    var n1, n2
    while (s >= 100) {
        for (var k = 0; k < 4; k++) {
            d = parseInt(s / dg[k])
            if (d > 0) {
                if (d >= 20) {
                    n1 = parseInt(d / 10)
                    n2 = d % 10
                    printnum2(n1)
                    printnum1(n2)
                } else
                    printnum1(d)
                str = str + th[k]
            }
            s = s % dg[k]
        }
    }
    if (s >= 20) {
        n1 = parseInt(s / 10)
        n2 = s % 10
    } else {
        n1 = 0
        n2 = s
    }

    printnum2(n1)
    printnum1(n2)
    if (dem > 0) {
        decprint(dem)
    }
    return str

    function decprint(nm) {
        if (nm >= 20) {
            n1 = parseInt(nm / 10)
            n2 = nm % 10
        } else {
            n1 = 0
            n2 = parseInt(nm)
        }
        str = str + 'And '
        printnum2(n1)
        printnum1(n2)
    }

    function printnum1(num1) {
        switch (num1) {
            case 1:
                str = str + 'One '
                break;
            case 2:
                str = str + 'Two '
                break;
            case 3:
                str = str + 'Three '
                break;
            case 4:
                str = str + 'Four '
                break;
            case 5:
                str = str + 'Five '
                break;
            case 6:
                str = str + 'Six '
                break;
            case 7:
                str = str + 'Seven '
                break;
            case 8:
                str = str + 'Eight '
                break;
            case 9:
                str = str + 'Nine '
                break;
            case 10:
                str = str + 'Ten '
                break;
            case 11:
                str = str + 'Eleven '
                break;
            case 12:
                str = str + 'Twelve '
                break;
            case 13:
                str = str + 'Thirteen '
                break;
            case 14:
                str = str + 'Fourteen '
                break;
            case 15:
                str = str + 'Fifteen '
                break;
            case 16:
                str = str + 'Sixteen '
                break;
            case 17:
                str = str + 'Seventeen '
                break;
            case 18:
                str = str + 'Eighteen '
                break;
            case 19:
                str = str + 'Nineteen '
                break;
        }
    }

    function printnum2(num2) {
        switch (num2) {
            case 2:
                str = str + 'Twenty '
                break;
            case 3:
                str = str + 'Thirty '
                break;
            case 4:
                str = str + 'Forty '
                break;
            case 5:
                str = str + 'Fifty '
                break;
            case 6:
                str = str + 'Sixty '
                break;
            case 7:
                str = str + 'Seventy '
                break;
            case 8:
                str = str + 'Eighty '
                break;
            case 9:
                str = str + 'Ninety '
                break;
        }

    }
}

function toWords(s) {

    var th = ['', 'thousand', 'million', 'billion', 'trillion'];
    var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }

        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');
}
//--------------------------------------------Function End - Amount in Words----------------------------------//


function formatDollar(somenum) {
    var split_arr = new Array()
    var i_no_before_comma;
    var i_no_before_comma_length;
    if (somenum != null && somenum != '' && somenum != undefined) {
        split_arr = somenum.toString().split('.')
        i_no_before_comma = split_arr[0]
        i_no_before_comma = Math.abs(i_no_before_comma)

        if (i_no_before_comma.toString().length <= 3) {
            return somenum;
        } else {
            var p = somenum.toString().split(".");

            if (p[1] != null && p[1] != '' && p[1] != undefined) {
                p[1] = p[1]
            } else {
                p[1] = '00'
            }
            return p[0].split("").reverse().reduce(function(acc, somenum, i, orig) {
                return somenum + (i && !(i % 3) ? "," : "") + acc;
            }, "") + "." + p[1];
        }
    }
}

function checkDateFormat() {
    var context = nlapiGetContext();

    var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function get_date(date1) {
    var date_format = checkDateFormat();



    var today;
    // ============================= Todays Date ==========================================

    var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
    var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
    var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));



    var day = date1.getDate();

    var month = date1.getMonth() + 1;

    var year = date1.getFullYear();

    nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' day -->' + day);

    nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' month -->' + month);

    nlapiLogExecution('DEBUG', 'beforeSubmitRecord', ' year -->' + year);




    if (month == 1) {
        month = '01'
    } else if (month == 2) {
        month = '02'
    } else if (month == 3) {
        month = '03'
    } else if (month == 4) {
        month = '04'
    } else if (month == 5) {
        month = '05'
    } else if (month == 6) {
        month = '06'
    } else if (month == 7) {
        month = '07'
    } else if (month == 8) {
        month = '08'
    } else if (month == 9) {
        month = '09'
    }
    if (day == 1) {
        day = '01'
    } else if (day == 2) {
        day = '02'
    } else if (day == 3) {
        day = '03'
    } else if (day == 4) {
        day = '04'
    } else if (day == 5) {
        day = '05'
    } else if (day == 6) {
        day = '06'
    } else if (day == 7) {
        day = '07'
    } else if (day == 8) {
        day = '08'
    } else if (day == 9) {
        day = '09'
    }
    if (date_format == 'YYYY-MM-DD') {
        today = year + '-' + month + '-' + day;
    }
    if (date_format == 'DD/MM/YYYY') {
        today = day + '/' + month + '/' + year;
    }
    if (date_format == 'MM/DD/YYYY') {
        today = month + '/' + day + '/' + year;
    }
    /* if (date_format == 'M/D/YYYY')
    {
     	today = month + '/' + day + '/' + year;
    }*/

    // today = nlapiStringToDate(today) 

    //today = dateFormat(today,date_format)

    return today;
}

function header_adress(text, subsidiary) {
    var iChars = subsidiary;
    var text_new = ''

    if (text != null && text != '' && text != undefined) {
        for (var y = 0; y < text.toString().length; y++) {
            text_new += text[y]
            if (text_new == subsidiary) {
                text_new = '';
            } //Break			
        } //Loop				
    } //Text Validation

    nlapiLogExecution('DEBUG', 'header_adress', ' text_new -->' + text_new);
    return text_new;
} //Header Address

function escape_special_chars(text) {
    var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.1234567890";
    var text_new = ''

    if (text != null && text != '' && text != undefined) {
        for (var y = 0; y < text.toString().length; y++) {
            if (iChars.indexOf(text[y]) == -1) {
                text_new += text[y]
            }

        } //Loop				
    } //Text Validation	
    return text_new;
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================


function get_dateFormat(d_inv_date) {
    try {
        var dateSplit = d_inv_date.split('/');

        var newFormat = dateSplit[1] + "-" + dateSplit[0] + "-" + dateSplit[2];
        return newFormat;

    } catch (e) {
        nlapiLogExecution('DEBUG', 'Error in Date conversion function', e);
    }
}