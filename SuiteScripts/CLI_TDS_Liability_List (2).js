// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:    CLI TDS Liability List
	Author:			Sachin Khairnar
	Company:		Aashna Cloudtech Pvt Ltd
	Date:
	Version:		
	Description:	


	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.
	Script Modification Log:
	
     -- Date --			-- Modified By --				--Requested By--				-- Description --
	22 sep 2014           Supriya                    Kalpana                      Normal Account Validation
        23 march 2015      Nikhil                     sachin k/kalpana                solve the bugs

     PAGE INIT
		- pageInit(type)
		  NOT USED

     SAVE RECORD
		- saveRecord()
		  NOT USED

     VALIDATE FIELD
		- validateField(type, name, linenum)
		  NOT USED

     FIELD CHANGED
		- fieldChanged(type, name, linenum)
          fieldChanged(type,name, linenum)

     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)
		  NOT USED

     VALIDATE LINE
		- validateLine()
		  NOT USED

     RECALC
		- reCalc()
		  NOT USED

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
		  NOT USED




*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...
var totalamount=0

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES

    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_validateInterest(){
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SAVE RECORD CODE BODY
	
	var b_applyInterest = nlapiGetFieldValue('custpage_apply_interest');
	
	if (b_applyInterest == 'T') {
		var interestAmt = nlapiGetFieldValue('custpage_interest_amt')
		
		var interestAccount = nlapiGetFieldValue('custpage_interst_acc')
		
		if (interestAmt != null && interestAmt != '' && interestAmt != undefined) {
		
		}
		else {
			alert('Please enter the value for Interest amount')
			return false;
		}
		if (interestAccount != null && interestAccount != '' && interestAccount != undefined) {
		
		}
		else {
			alert('Please enter the value for Interest account')
			return false;
		}
	}
	return true;
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum){
	/*  On field changed:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  FIELD CHANGED CODE BODY
	
	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	
	// ===== Begin : Code For setting Account Balance on account field change ======
	if (name == 'account') {
		var bankacc = nlapiGetFieldValue('account')
		var acc_filchk1 = new Array();
		var acc_columnsCheck1 = new Array();
		
		acc_filchk1.push(new nlobjSearchFilter('internalid', null, 'is', bankacc));
		
		acc_columnsCheck1.push(new nlobjSearchColumn('balance'));
		acc_columnsCheck1.push(new nlobjSearchColumn('name'));
		
		var acc_searchResultsCheck1 = nlapiSearchRecord('account', null, acc_filchk1, acc_columnsCheck1);
		
		for (var j = 0; j < acc_searchResultsCheck1 != null && j < acc_searchResultsCheck1.length; j++) {
			var acc = acc_searchResultsCheck1[j].getValue('name')
			var accbal = acc_searchResultsCheck1[j].getValue('balance')
			
		}// END for(var j=0; j < acc_searchResultsCheck1 != null && j < acc_searchResultsCheck1.length; j++)
		nlapiSetFieldValue('bankbal', accbal);
		
	}//END if(name=='account')
	// ======= End : Code For setting Account Balance on account field change ========.
	
	// ======= Begin : Code For Amount Validation On Total Amount change =====
	if (name == 'totalamt') {
	
		var curretamt = nlapiGetLineItemValue('tdslist', 'totalamt', linenum)
		
		var amount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', linenum)
		
		if (curretamt > amount) {
			alert('Sorry! Invalid Amount')
			nlapiSetLineItemValue('tdslist', 'totalamt', linenum, '');
			
		}// END if(curretamt>amount)
	}// END if(name=='totalamt')
	// ======= End : Code For Amount Validation On Total Amount change =======
	
	// ======= Begin : Code For setting TDS Payable on apply field change ====
	if (name == 'apply') {
		var apply = nlapiGetLineItemValue('tdslist', 'apply', linenum)
		
		if (apply == 'T') {
			var totalamount = 0
			
			totalamount = nlapiGetFieldValue('totalamt')
			
			var currentamount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', linenum)
			
			if (totalamount != '' && totalamount != undefined && totalamount != null) {
				totalamount = parseFloat(totalamount) + parseFloat(currentamount)
			}
			else {
				totalamount = parseFloat(currentamount)
			}
			
			nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
			
		}// END  if(apply=='T')
		else {
			var totalamount = 0
			
			totalamount = nlapiGetFieldValue('totalamt')
			
			var currentamount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', linenum)
			
			totalamount = parseFloat(totalamount) - parseFloat(currentamount)
			
			nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
			
		}//END ELSE 
	}//END if(name=='apply')
	// ===== End : Code For setting TDS Payable on apply field change ======
	
	if (name == 'custpage_apply_interest') {
		var b_applyInterest = nlapiGetFieldValue('custpage_apply_interest');
		
		if (b_applyInterest == 'T') {
			nlapiSetFieldMandatory('custpage_interest_amt', true)
			nlapiSetFieldMandatory('custpage_interst_acc', true)
			
			var interestAmt = nlapiGetFieldValue('custpage_interest_amt')
			
			if (interestAmt != null && interestAmt != '' && interestAmt != undefined) {
				var totalamt = nlapiGetFieldValue('totalamt');
				var totalamount = parseFloat(interestAmt) + parseFloat(totalamt)
				nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
			}
			
			
		}
		else {
			nlapiSetFieldMandatory('custpage_interest_amt', false)
			nlapiSetFieldMandatory('custpage_interst_acc', false)
			
			nlapiSetFieldValue('custpage_interest_amt', '')
			nlapiSetFieldValue('custpage_interst_acc', '')
			
			var lincount = nlapiGetLineItemCount('tdslist');
			var totalamount = parseFloat(0);
			
			for (var i = 1; i <= lincount; i++) {
				var Apply = nlapiGetLineItemValue('tdslist', 'apply', i);
				
				if (Apply == 'T') {
					var currentamount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', i)
					
					totalamount = parseFloat(totalamount) + parseFloat(currentamount);
				}
			}
			nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
		}
	}
	
	if (name == 'custpage_interest_amt') {
		var b_applyInterest = nlapiGetFieldValue('custpage_apply_interest');
		
		if (b_applyInterest == 'T') {
			var interestAmt = nlapiGetFieldValue('custpage_interest_amt')
			
			if (interestAmt != null && interestAmt != '' && interestAmt != undefined) {
				var totalamt = nlapiGetFieldValue('totalamt');
				var totalamount = parseFloat(interestAmt) + parseFloat(totalamt)
				
				nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
			}
			else {
				var lincount = nlapiGetLineItemCount('tdslist');
				var totalamount = parseFloat(0);
				
				for (var i = 1; i <= lincount; i++) {
					var Apply = nlapiGetLineItemValue('tdslist', 'apply', i);
					
					if (Apply == 'T') {
						var currentamount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', i)
						
						totalamount = parseFloat(totalamount) + parseFloat(currentamount);
					}
				}
				nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
			}
		}
	}
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================
function MarkAll()
{
	var lincount = nlapiGetLineItemCount('tdslist');
	var totalamount = 0;
	
	for (var i = 1; i <= lincount; i++) 
	{
		nlapiSetLineItemValue('tdslist', 'apply', i, 'T', false, false);
		
		var currentamount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', i)
		
		totalamount = parseFloat(totalamount) + parseFloat(currentamount)
		
	}
	nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
	
	var b_applyInterest = nlapiGetFieldValue('custpage_apply_interest');
	
	if (b_applyInterest == 'T') 
	{
		nlapiSetFieldMandatory('custpage_interest_amt', true)
		nlapiSetFieldMandatory('custpage_interst_acc', true)
		
		var interestAmt = nlapiGetFieldValue('custpage_interest_amt')
		
		if (interestAmt != null && interestAmt != '' && interestAmt != undefined) 
		{
			var totalamt = nlapiGetFieldValue('totalamt');
			var totalamount = parseFloat(interestAmt) + parseFloat(totalamt)
		}
		nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
	}
	
}
function UnMarkAll()
{
	var lincount = nlapiGetLineItemCount('tdslist');
	var totalamount = parseFloat(0);
	
	totalamount = nlapiGetFieldValue('totalamt')
	
	for (var i = 1; i <= lincount; i++) 
	{
		var apply = nlapiGetLineItemValue('tdslist', 'apply', i)
		if (apply == 'T') 
		{
		
			nlapiSetLineItemValue('tdslist', 'apply', i, 'F');
			
			var currentamount = nlapiGetLineItemValue('tdslist', 'custrecord_billtdspayable', i)
			
			totalamount = parseFloat(totalamount) - parseFloat(currentamount)
			
			
		}// END  if(apply=='T')
	}
	nlapiSetFieldValue('totalamt', totalamount.toFixed(3), true)
	
}


// BEGIN FUNCTION ===================================================

// Begin : Function generateExcel()

function generateExcel()
{
	var fromdate = nlapiGetFieldValue('fromdate');
	var todate = nlapiGetFieldValue('todate');
	var subsidiary = nlapiGetFieldValue('subsidiary');
	var tds = nlapiGetFieldValue('tdstype');
	
	//alert(fromdate);
	//alert(todate);
	//alert(subsidiary);
	//alert(tds);

	var url = nlapiResolveURL('SUITELET', 'customscript_tds_liability_excelreport', 'customdeploysut_tds_liability_excelrepor');
    	url += '&custscriptfromdate=' +fromdate ;
		url += '&custscripttodate=' +todate ;
		url += '&custscriptsubsidiary=' +subsidiary ;
		url += '&custscripttds=' +tds ;
		
	var response = nlapiRequestURL(url);	

	window.open(response.getBody());

}

// End : Function generateExcel()



// Begin : generateExcel_TDS_Payment_List_SectionWise()

function generateExcel_TDS_Payment_List_SectioinWise()
{
	//alert('HI');
	var postingperiod = nlapiGetFieldValue('accountingperiod');
	var fromdate = nlapiGetFieldValue('fromdate');
	var todate = nlapiGetFieldValue('todate');
	var subsidiary = nlapiGetFieldValue('subsidiary');
	var tds_section = nlapiGetFieldValue('tds_section');
	var tds_assessee_type = nlapiGetFieldValue('tds_assessee_type');
	
/*	var user = nlapiGetUser();
	if(user == 92399)
	{
		alert(postingperiod);
		alert(fromdate);
		alert(todate);
		alert(subsidiary);
		alert(tds_section);
		alert(tds_assessee_type);
	}
*/	
	
	var url = nlapiResolveURL('SUITELET', 'customscript_sut_tds_paylist_excelreport', 'customdeploysut_tds_paylist_excelreport');
    	url += '&custscriptpaylist_fromdate=' +fromdate ;
		url += '&custscriptpaylist_todate=' +todate ;
		url += '&custscriptpaylist_subsidiary=' +subsidiary ;
		url += '&custscriptpaylist_tds_section=' +tds_section ;
		url += '&custscriptpaylist_tds_assesse_type=' +tds_assessee_type ;
		url += '&custscriptpaylist_tds_postingperiod=' +postingperiod;
	//alert(url);	
	var response = nlapiRequestURL(url);	
	
	window.open(response.getBody());
	
}
function create_tds_payableReport(i_recordID)
{
	var url = nlapiResolveURL('SUITELET', 'customscript_sut_ait_tds_payment_report',1)
	
	url = url +'&i_recordID='+i_recordID;
	
	var response = nlapiRequestURL(url);	
	
	window.open(response.getBody());
}

// End : generateExcel_TDS_Payment_List_SectionWise()

// END FUNCTION =====================================================
