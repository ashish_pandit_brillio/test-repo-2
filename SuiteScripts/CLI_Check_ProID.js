function onPageInit(type)
{
	var mode = type;
}

function onProjectIdFieldChanged(type,name)
{
	try
	{
		if(type == 'edit')
		{
			if(type == 'item' && name == 'custcol_project_entity_id')
			{
				nlapiSetCurrentLineItemValue('item','custcol_project_id_check','T');
			}
			else if(type == 'expense' && name == 'custcol_project_entity_id')
			{
				nlapiSetCurrentLineItemValue('expense','custcol_project_id_check','T');
			}
			else if(type == 'line' && name == 'custcol_project_entity_id')
			{
				nlapiSetCurrentLineItemValue('line','custcol_project_id_check','T');
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('debug','Error',err)
		return err;
	}
}