/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Jul 2019     Aazamali Khan
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	var xmlStr = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
	xmlStr += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
	xmlStr += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
	xmlStr += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
	xmlStr += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
	xmlStr += 'xmlns:html="http://www.w3.org/TR/REC-html40">';
	xmlStr += '<Styles>'
		+ '<Style ss:ID="s63">'
		+ '<Font x:CharSet="204" ss:Size="12" ss:Color="#000000" ss:Bold="1" ss:Underline="Single"/>'
		+ '</Style>' + '</Styles>';
	xmlStr += '<Worksheet ss:Name="Sheet1">';
	xmlStr += '<Table>'
	xmlStr += '<Row>'
	var date = new Date();
	var monthNumber = date.getMonth();
	for (var int = 0; int < monthNumber; int++) {
		var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];
		xmlStr += '<Cell><Data ss:Type="String"> '+month[int]+'</Data></Cell>'
	}
	xmlStr += '<Row>'
	xmlStr += '<Cell><Data ss:Type="String"> '+"1"+'</Data></Cell>'
	xmlStr += '</Row>'
	xmlStr += '</Table></Worksheet></Workbook>';
	nlapiLogExecution("DEBUG", "xmlStr : ", xmlStr);
	var excelFile = nlapiCreateFile('TEST_encrypted.xls', 'EXCEL', nlapiEncrypt(xmlStr, 'base64'));
	nlapiSendEmail(442, 144836, "Template", "", null, null, null, excelFile);
}
