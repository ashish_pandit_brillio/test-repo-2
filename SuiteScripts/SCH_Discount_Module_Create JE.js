// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
			  Script Name : SCH_Discount_Module_Create JE.js
		Author      : Jayesh Dinde
		Date        : 14 Sep 2016
		Description : Create JE for Customer Discount.
	
	
		Script Modification Log:
	
		-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	
	
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.
	
	
		 SCHEDULED FUNCTION
			- scheduledFunction(type)
	
	
		 SUB-FUNCTIONS
			- The following sub-functions are called by the above core functions in order to maintain code
				modularization:
	
				   - NOT USED
	
	*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type) {
	try {

		var vendorList = {
			2: '209190',
			3: '209299',
			7: '209300',
			10: '209301',
			21: '209302',
			23: '209303'
		}

		var i_context = nlapiGetContext();
		var disocunt_module_rcrd_id = i_context.getSetting('SCRIPT', 'custscript_discount_module_id');

		var discount_module_rcrd_obj = nlapiLoadRecord('customrecord_discount_module_for_cust', disocunt_module_rcrd_id);
		var customer = discount_module_rcrd_obj.getFieldValue('custrecord_customer_to_apply_discount');
		var projects_to_exclude = discount_module_rcrd_obj.getFieldValues('custrecord_projects_to_exclude_discount');
		var discount_amount = discount_module_rcrd_obj.getFieldValue('custrecord_discount_amount_to_apply');
		var onsite_to_offshore_conversion_rate = discount_module_rcrd_obj.getFieldValue('custrecord_onsite_to_offshore');
		var discount_module_month = discount_module_rcrd_obj.getFieldText('custrecord_month_to_apply_discount');
		var discount_module_year = discount_module_rcrd_obj.getFieldText('custrecord_year_to_apply_discount');
		var i_account_debit_discount_module = discount_module_rcrd_obj.getFieldValue('custrecord_debit_account_to_apply');
		var i_account_credit_discount_module = discount_module_rcrd_obj.getFieldValue('custrecord_credit_account_to_apply');
		var cust_subsidiary = nlapiLookupField('customer', discount_module_rcrd_obj.getFieldValue('custrecord_customer_to_apply_discount'), 'subsidiary');

		var exclude_tm = discount_module_rcrd_obj.getFieldValue('custrecord_exclude_tm_projects');
		var exclude_fp = discount_module_rcrd_obj.getFieldValue('custrecord_exclude_fp_projects');

		var i_month = discount_module_month;
		var i_year = discount_module_year;

		var d_start_date = get_current_month_start_date(i_month, i_year);
		var d_end_date = get_current_month_end_date(i_month, i_year);

		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);

		var total_days_in_month = daysInMonth(discount_module_month, discount_module_year);

		var emp_detail_arr = new Array();
		var total_onsite = 0;
		var total_offshore = 0;

		var filters_search_allocation = new Array();
		filters_search_allocation[0] = new nlobjSearchFilter('internalid', 'customer', 'anyof', parseInt(customer));
		filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date);
		filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date);
		filters_search_allocation[3] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');

		var filter_counter = 4;
		if (projects_to_exclude) {
			filters_search_allocation[filter_counter] = new nlobjSearchFilter('project', null, 'noneof', projects_to_exclude);
			filter_counter++;
		}

		if (exclude_tm == 'T') {
			filters_search_allocation[filter_counter] = new nlobjSearchFilter('jobbillingtype', 'job', 'noneof', 'TM');
			filter_counter++;
		}

		if (exclude_fp == 'T') {
			filters_search_allocation[filter_counter] = new nlobjSearchFilter('jobbillingtype', 'job', 'noneof', ['FBM', 'FBI']);
			filter_counter++;
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('customer');
		columns[1] = new nlobjSearchColumn('company');
		columns[2] = new nlobjSearchColumn('startdate');
		columns[3] = new nlobjSearchColumn('enddate');
		columns[4] = new nlobjSearchColumn('percentoftime');
		columns[5] = new nlobjSearchColumn('resource');
		columns[6] = new nlobjSearchColumn('subsidiary', 'employee');
		columns[7] = new nlobjSearchColumn('custevent_practice');
		columns[8] = new nlobjSearchColumn('custentity_vertical', 'job');
		columns[9] = new nlobjSearchColumn('territory', 'customer');
		columns[10] = new nlobjSearchColumn('subsidiary', 'customer');

		var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
		if (_logValidation(project_allocation_result)) {
			var sr_no = 0;
			nlapiLogExecution('audit', 'srch len:- ', project_allocation_result.length);
			for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {
				var onsite_flag = 0;

				var customer = project_allocation_result[i_search_indx].getValue('customer');
				var cust_name = project_allocation_result[i_search_indx].getText('customer');
				var cust_territory = project_allocation_result[i_search_indx].getValue('territory', 'customer');
				var cust_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'customer');

				var project = project_allocation_result[i_search_indx].getValue('company');
				var proj_vertical = project_allocation_result[i_search_indx].getValue('custentity_vertical', 'job');

				var resource = project_allocation_result[i_search_indx].getValue('resource');
				var resource_name = project_allocation_result[i_search_indx].getText('resource');
				var resource_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
				var emp_practice = project_allocation_result[i_search_indx].getValue('custevent_practice');

				var startdate = project_allocation_result[i_search_indx].getValue('startdate');
				var enddate = project_allocation_result[i_search_indx].getValue('enddate');
				var percent_of_time = project_allocation_result[i_search_indx].getValue('percentoftime');
				percent_of_time = percent_of_time.toString();
				percent_of_time = percent_of_time.split('.');
				percent_of_time = percent_of_time[0].toString().split('%');

				startdate = nlapiStringToDate(startdate);
				startdate = d_start_date < startdate ? startdate : d_start_date;

				enddate = nlapiStringToDate(enddate);
				enddate = d_end_date > enddate ? enddate : d_end_date;

				var days_worked_in_month = getDatediffIndays(startdate, enddate);

				var final_prcnt_allocation = ((parseFloat(days_worked_in_month) / parseFloat(total_days_in_month)) * parseFloat(percent_of_time)) / parseFloat(100);

				if (parseInt(resource_subsidiary) == 3) {
					total_offshore = parseFloat(total_offshore) + parseFloat(final_prcnt_allocation);
				}
				else {
					total_onsite = parseFloat(total_onsite) + parseFloat(final_prcnt_allocation);
					onsite_flag = 1;
				}

				emp_detail_arr[sr_no] = {
					'resource': resource,
					'resource_name': resource_name,
					'resource_subsi': resource_subsidiary,
					'emp_practice': emp_practice,
					'startdate': startdate,
					'enddate': enddate,
					'customer': customer,
					'cust_name': cust_name,
					'cust_territory': cust_territory,
					'cust_subsidiary': cust_subsidiary,
					'project': project,
					'proj_vertical': proj_vertical,
					'prcnt_of_tym': final_prcnt_allocation,
					'onsite_flag': onsite_flag
				};
				sr_no++;

			}

			nlapiLogExecution('audit', 'onsite:- ' + total_onsite + '::: offshore:- ' + total_offshore, emp_detail_arr.length);
			nlapiLogExecution('audit', 'Onsite allocated:- ' + parseFloat(total_onsite) * parseFloat(onsite_to_offshore_conversion_rate), 'offsite allocated:- ' + parseFloat(total_offshore));

			total_onsite = parseFloat(total_onsite) * parseFloat(onsite_to_offshore_conversion_rate);

			var total_resources = parseFloat(total_onsite) + parseFloat(total_offshore);
			nlapiLogExecution('audit', 'total_resources:- ' + total_resources);

			var average_discount_amnt = parseFloat(discount_amount) / parseFloat(total_resources);
			nlapiLogExecution('audit', 'average cost against emp:- ' + average_discount_amnt);

			var sr_no_journal = 1;
			var total_credit_to_be_tagged = 0;

			var je_discount_module = nlapiCreateRecord('journalentry');
			je_discount_module.setFieldValue('trandate', nlapiDateToString(d_end_date));
			je_discount_module.setFieldValue('subsidiary', cust_subsidiary);
			//je_discount_module.setFieldValue('customform', parseInt(122));

			for (var jk = 0; jk < emp_detail_arr.length; jk++) {
				try {
					var emp_amount_to_be_tagged = parseFloat(average_discount_amnt) * parseFloat(emp_detail_arr[jk].prcnt_of_tym);
					if (emp_detail_arr[jk].onsite_flag == 1) {
						emp_amount_to_be_tagged = parseFloat(emp_amount_to_be_tagged) * parseFloat(onsite_to_offshore_conversion_rate);
					}

					emp_amount_to_be_tagged = emp_amount_to_be_tagged.toString();
					if (emp_amount_to_be_tagged.indexOf(".") > 0) {
						var temp_index = emp_amount_to_be_tagged.indexOf(".") + 3;
						emp_amount_to_be_tagged = emp_amount_to_be_tagged.substr(0, temp_index);
					}
					emp_amount_to_be_tagged = parseFloat(emp_amount_to_be_tagged);

					total_credit_to_be_tagged = parseFloat(total_credit_to_be_tagged) + parseFloat(emp_amount_to_be_tagged);
					total_credit_to_be_tagged = total_credit_to_be_tagged.toFixed(2);

					var proj_rcrd = nlapiLoadRecord('job', emp_detail_arr[jk].project);
					var entity_id = proj_rcrd.getFieldValue('entityid');
					var alt_name = proj_rcrd.getFieldValue('altname');
					var proj_desrcption = entity_id + ' ' + alt_name;

					je_discount_module.selectNewLineItem('line');

					je_discount_module.setCurrentLineItemValue('line', 'account', parseInt(i_account_debit_discount_module));
					je_discount_module.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(sr_no_journal));
					sr_no_journal++;
					je_discount_module.setCurrentLineItemValue('line', 'debit', emp_amount_to_be_tagged);
					je_discount_module.setCurrentLineItemValue('line', 'department', emp_detail_arr[jk].emp_practice);
					je_discount_module.setCurrentLineItemValue('line', 'class', emp_detail_arr[jk].proj_vertical);
					je_discount_module.setCurrentLineItemValue('line', 'custcol_project_name', proj_desrcption);
					je_discount_module.setCurrentLineItemValue('line', 'custcolprj_name', proj_desrcption);
					je_discount_module.setCurrentLineItemValue('line', 'custcol_sow_project', emp_detail_arr[jk].project);
					je_discount_module.setCurrentLineItemValue('line', 'custcol_territory', emp_detail_arr[jk].cust_territory);
					je_discount_module.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', emp_detail_arr[jk].resource_name);
					je_discount_module.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', emp_detail_arr[jk].cust_name);
					je_discount_module.setCurrentLineItemValue('line', 'memo', 'Discount Module JE');
					//Added by Sitaram
					//if(_logValidation(vendorList[cust_subsidiary])){
					//je_discount_module.setCurrentLineItemValue('line', 'entity', vendorList[cust_subsidiary]);												
					//}
					je_discount_module.commitLineItem('line');
				}
				catch (err) {
					nlapiLogExecution('error', 'for error:- ' + err);

				}

			}

			je_discount_module.selectNewLineItem('line');
			//Added by Sitaram
			//if(_logValidation(vendorList[cust_subsidiary])){
			//je_discount_module.setCurrentLineItemValue('line', 'entity', vendorList[cust_subsidiary]);												
			//}
			je_discount_module.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(sr_no_journal));
			je_discount_module.setCurrentLineItemValue('line', 'account', parseInt(i_account_credit_discount_module));
			je_discount_module.setCurrentLineItemValue('line', 'credit', total_credit_to_be_tagged);
			je_discount_module.commitLineItem('line');

			var je_id = nlapiSubmitRecord(je_discount_module, true, true);
			nlapiLogExecution('audit', 'je_id:- ', je_id);

			nlapiSubmitField('customrecord_discount_module_for_cust', disocunt_module_rcrd_id, 'custrecord_journal_created_id', je_id);
		}
		else
			nlapiLogExecution('audit', 'resource allocation result is null');
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
		Send_Exception_Mail(err);
		
	}
}

// END SCHEDULED FUNCTION =============================================

function get_current_month_start_date(i_month, i_year) {
	var d_start_date = '';
	var d_end_date = '';

	var date_format = checkDateFormat();

	if (_logValidation(i_month) && _logValidation(i_year)) {
		if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 1 + '-' + 1;
				d_end_date = i_year + '-' + 1 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 31 + '/' + 1 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 1 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
			if ((i_year % 2) == 0) {
				i_day = 29;
			}
			else {
				i_day = 28;
			}

			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 2 + '-' + 1;
				d_end_date = i_year + '-' + 2 + '-' + i_day;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 2 + '/' + i_year;
				d_end_date = i_day + '/' + 2 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 2 + '/' + 1 + '/' + i_year;
				d_end_date = 2 + '/' + i_day + '/' + i_year;
			}

		}
		else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 3 + '-' + 1;
				d_end_date = i_year + '-' + 3 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 3 + '/' + i_year;
				d_end_date = 31 + '/' + 3 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 3 + '/' + 1 + '/' + i_year;
				d_end_date = 3 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 4 + '-' + 1;
				d_end_date = i_year + '-' + 4 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 4 + '/' + i_year;
				d_end_date = 30 + '/' + 4 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 4 + '/' + 1 + '/' + i_year;
				d_end_date = 4 + '/' + 30 + '/' + i_year;
			}


		}
		else if (i_month == 'May' || i_month == 'MAY') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 5 + '-' + 1;
				d_end_date = i_year + '-' + 5 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 5 + '/' + i_year;
				d_end_date = 31 + '/' + 5 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 5 + '/' + 1 + '/' + i_year;
				d_end_date = 5 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 6 + '-' + 1;
				d_end_date = i_year + '-' + 6 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 6 + '/' + i_year;
				d_end_date = 30 + '/' + 6 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 6 + '/' + 1 + '/' + i_year;
				d_end_date = 6 + '/' + 30 + '/' + i_year;
			}

		}
		else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 7 + '-' + 1;
				d_end_date = i_year + '-' + 7 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 7 + '/' + i_year;
				d_end_date = 31 + '/' + 7 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 7 + '/' + 1 + '/' + i_year;
				d_end_date = 7 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 8 + '-' + 1;
				d_end_date = i_year + '-' + 8 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 8 + '/' + i_year;
				d_end_date = 31 + '/' + 8 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 8 + '/' + 1 + '/' + i_year;
				d_end_date = 8 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 9 + '-' + 1;
				d_end_date = i_year + '-' + 9 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 9 + '/' + i_year;
				d_end_date = 30 + '/' + 9 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 9 + '/' + 1 + '/' + i_year;
				d_end_date = 9 + '/' + 30 + '/' + i_year;
			}

		}
		else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 10 + '-' + 1;
				d_end_date = i_year + '-' + 10 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 10 + '/' + i_year;
				d_end_date = 31 + '/' + 10 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 10 + '/' + 1 + '/' + i_year;
				d_end_date = 10 + '/' + 31 + '/' + i_year;
			}
		}
		else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 11 + '-' + 1;
				d_end_date = i_year + '-' + 11 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 11 + '/' + i_year;
				d_end_date = 30 + '/' + 11 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 11 + '/' + 1 + '/' + i_year;
				d_end_date = 11 + '/' + 30 + '/' + i_year;
			}

		}
		else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 12 + '-' + 1;
				d_end_date = i_year + '-' + 12 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 12 + '/' + i_year;
				d_end_date = 31 + '/' + 12 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 12 + '/' + 1 + '/' + i_year;
				d_end_date = 12 + '/' + 31 + '/' + i_year;
			}

		}
	}//Month & Year
	return d_start_date;
}


function get_current_month_end_date(i_month, i_year) {
	var d_start_date = '';
	var d_end_date = '';

	var date_format = checkDateFormat();

	if (_logValidation(i_month) && _logValidation(i_year)) {
		if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 1 + '-' + 1;
				d_end_date = i_year + '-' + 1 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 31 + '/' + 1 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 1 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
			if ((i_year % 2) == 0) {
				i_day = 29;
			}
			else {
				i_day = 28;
			}

			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 2 + '-' + 1;
				d_end_date = i_year + '-' + 2 + '-' + i_day;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 2 + '/' + i_year;
				d_end_date = i_day + '/' + 2 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 2 + '/' + 1 + '/' + i_year;
				d_end_date = 2 + '/' + i_day + '/' + i_year;
			}

		}
		else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 3 + '-' + 1;
				d_end_date = i_year + '-' + 3 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 3 + '/' + i_year;
				d_end_date = 31 + '/' + 3 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 3 + '/' + 1 + '/' + i_year;
				d_end_date = 3 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 4 + '-' + 1;
				d_end_date = i_year + '-' + 4 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 4 + '/' + i_year;
				d_end_date = 30 + '/' + 4 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 4 + '/' + 1 + '/' + i_year;
				d_end_date = 4 + '/' + 30 + '/' + i_year;
			}


		}
		else if (i_month == 'May' || i_month == 'MAY') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 5 + '-' + 1;
				d_end_date = i_year + '-' + 5 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 5 + '/' + i_year;
				d_end_date = 31 + '/' + 5 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 5 + '/' + 1 + '/' + i_year;
				d_end_date = 5 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 6 + '-' + 1;
				d_end_date = i_year + '-' + 6 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 6 + '/' + i_year;
				d_end_date = 30 + '/' + 6 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 6 + '/' + 1 + '/' + i_year;
				d_end_date = 6 + '/' + 30 + '/' + i_year;
			}

		}
		else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 7 + '-' + 1;
				d_end_date = i_year + '-' + 7 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 7 + '/' + i_year;
				d_end_date = 31 + '/' + 7 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 7 + '/' + 1 + '/' + i_year;
				d_end_date = 7 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 8 + '-' + 1;
				d_end_date = i_year + '-' + 8 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 8 + '/' + i_year;
				d_end_date = 31 + '/' + 8 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 8 + '/' + 1 + '/' + i_year;
				d_end_date = 8 + '/' + 31 + '/' + i_year;
			}

		}
		else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 9 + '-' + 1;
				d_end_date = i_year + '-' + 9 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 9 + '/' + i_year;
				d_end_date = 30 + '/' + 9 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 9 + '/' + 1 + '/' + i_year;
				d_end_date = 9 + '/' + 30 + '/' + i_year;
			}

		}
		else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 10 + '-' + 1;
				d_end_date = i_year + '-' + 10 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 10 + '/' + i_year;
				d_end_date = 31 + '/' + 10 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 10 + '/' + 1 + '/' + i_year;
				d_end_date = 10 + '/' + 31 + '/' + i_year;
			}


		}
		else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 11 + '-' + 1;
				d_end_date = i_year + '-' + 11 + '-' + 30;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 11 + '/' + i_year;
				d_end_date = 30 + '/' + 11 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 11 + '/' + 1 + '/' + i_year;
				d_end_date = 11 + '/' + 30 + '/' + i_year;
			}

		}
		else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
			if (date_format == 'YYYY-MM-DD') {
				d_start_date = i_year + '-' + 12 + '-' + 1;
				d_end_date = i_year + '-' + 12 + '-' + 31;
			}
			if (date_format == 'DD/MM/YYYY') {
				d_start_date = 1 + '/' + 12 + '/' + i_year;
				d_end_date = 31 + '/' + 12 + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') {
				d_start_date = 12 + '/' + 1 + '/' + i_year;
				d_end_date = 12 + '/' + 31 + '/' + i_year;
			}

		}
	}//Month & Year
	return d_end_date;
}

function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function _logValidation(value) {
	if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date = Math.round((toDate - fromDate) / one_day);
	return (date + 1);
}

function daysInMonth(i_month, year) {
	i_month = i_month.trim();

	if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
		i_month = 1;
	}
	else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
		i_month = 2;
	}
	else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
		i_month = 3;
	}
	else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
		i_month = 4;
	}
	else if (i_month == 'May' || i_month == 'MAY') {
		i_month = 5;
	}
	else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
		i_month = 6;
	}
	else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
		i_month = 7;
	}
	else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
		i_month = 8;
	}
	else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
		i_month = 9;
	}
	else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
		i_month = 10;
	}
	else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
		i_month = 11;
	}
	else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
		i_month = 12;
	}

	year = Number(year).toFixed(0);
	return new Date(year, i_month, 0).getDate();
}

function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Discount Module Create JE';
        
        var s_Body = 'This is to inform that Discount Module Create JE is having an issue.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​
