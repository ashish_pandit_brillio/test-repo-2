// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI IIT GSTIN Validation
	Author: Onkar Gogi
	Company: Inspirria
	Date: 01/05/2017
    Description: This script will Validate the GSTIN field


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	11/08/2017			Prashant Lokhande				 Sachin Khiarnar			Change The Validation Of GSTIN Number



	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...
	var operationType;
	var a_subisidiary = new Array;
	var i_vatCode;
	var i_taxcode;
	var s_pass_code;
	var b_Flag_Save = 2;

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY
	
	operationType = type;
	
	var i_AitGlobalRecId = SearchGlobalParameter();
	nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
	
	//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
	if (i_AitGlobalRecId != 0) {
		var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
		
		a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
		nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
		
		i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
		nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
		
		i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
		nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
		
		s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
	} // END if(i_AitGlobalRecId != 0 )
	//  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_GST_Liable_Check()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY
	/*var s_GSTIN_No = nlapiGetFieldValue('custrecord_iit_address_gstn_uid');
	
	if (s_GSTIN_No != null && s_GSTIN_No != '' && s_GSTIN_No != undefined) 
	{
		b_Flag_Save = 0;
	}
	else if(!_logValidation(s_GSTIN_No))
	{
		b_Flag_Save = 1;
	}*/
	
	if(b_Flag_Save == 1)
	{
		alert('Kindly enter value for GSTIN/UID');
		return false;
	}
	else if(b_Flag_Save == 0)
	{
		var i_Gst_State_Name = nlapiGetFieldValue('custrecord_iit_address_gst_state');
		//alert(i_Gst_State_Name);
		if(_logValidation(i_Gst_State_Name))
		{
			return true;
		}
		else
		{
			alert('* Mandatory Field: GST State \n *Kindly Enter GST State');
			return false;
		}
	}
	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField_GSTIN(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY

	if(name == 'custrecord_iit_address_gstn_uid')
	{
		//alert(name);
		var b_Flag = 0;
		
		var s_GSTIN_No = nlapiGetFieldValue('custrecord_iit_address_gstn_uid');
		var country = nlapiGetFieldValue('country');
		//alert(country);
		
		//var b_Check = new RegExp("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[Z]{1}[0-9A-Z]{1}$");
		var b_Check = new RegExp("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9A-Z]{3}$");

		
		if (s_GSTIN_No != null && s_GSTIN_No != '' && s_GSTIN_No != undefined && country =='IN') 
		{
			if (b_Check.test(s_GSTIN_No)) 
			{
				var i_State_Code = nlapiGetFieldValue('custrecord_iit_address_gst_state_code');
				//alert(i_State_Code);
				var i_State_Number = s_GSTIN_No.substring(0, 2);
				//alert(i_State_Number);
				if(_logValidation(i_State_Code) && i_State_Code != i_State_Number)
				{
					b_Flag = 1;
					alert('Please check your GSTIN/UID, as first 2 numbers from GSTIN/UID: '+i_State_Number+ ' does not match State Code: '+i_State_Code);
				}
			}
			else 
			{
				b_Flag = 1;
				
				//alert('Please enter The valid GSTIN No ,\n * GSTIN Number Length 15 Character \n * First Two Numeric 0 to 9 \n * Five Character from A to Z \n * Four Numeric 0 to 9 \n * One Character from A to Z \n * One Numeric 0 to 9 \n * One Character Z \n * One Alpha Numeric A to Z or 0 to 9');
				alert('Please enter The valid GSTIN No ,\n * GSTIN Number Length 15 Character \n * First Two Numeric 0 to 9 \n * Five Character from A to Z \n * Four Numeric 0 to 9 \n * One Character from A to Z \n *Last Three Alpha Numeric A to Z or 0 to 9');
				//return false;
			}
		}
		
		if(b_Flag == parseInt(1))
		{
			nlapiSetFieldValue('custrecord_iit_address_gstn_uid', '',true, true);//
		}
		
	}
	
	if(name == 'custrecord_iit_address_gst_state')
	{
		var b_Flag = 0;
		var i_State = nlapiGetFieldValue('custrecord_iit_address_gst_state');
		//alert(i_State)
		if(_logValidation(i_State))
			var i_State_Code = nlapiLookupField('customrecord_iit_state', i_State, 'custrecord_iit_state_code');
		//alert(i_State_Code)
		
		var s_GSTIN_No = nlapiGetFieldValue('custrecord_iit_address_gstn_uid');
		
		if(_logValidation(i_State_Code) && _logValidation(s_GSTIN_No))
		{
			var i_State_Number = s_GSTIN_No.substring(0, 2);
			
			if(_logValidation(i_State_Code) && i_State_Code != i_State_Number)
			{
				b_Flag = 1;
				alert('Please check your GSTIN/UID, as first 2 numbers from GSTIN/UID: '+i_State_Number+ ' does not match State Code: '+i_State_Code);
			}
			
		}
		if(b_Flag == parseInt(1))
		{
			nlapiSetFieldValue('custrecord_iit_address_gstn_uid', '',true, true);//
		}	
	}
	
	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_Set_Mandatory(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY
	//alert('NAME'+name);
	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));
	if(name == 'custrecord_iit_gst_liable_address')
	{
		var b_GST_Liable = nlapiGetFieldValue('custrecord_iit_gst_liable_address');
		//alert(b_GST_Liable);
		if(b_GST_Liable == 'T')
		{
			
			var s_GSTIN_UID = nlapiGetFieldValue('custrecord_iit_address_gstn_uid');
			//alert('s_GSTIN_UID'+s_GSTIN_UID)
			if(_logValidation(s_GSTIN_UID))
			{
				b_Flag_Save = 0;
			}
			else
			{
				b_Flag_Save = 1;
			}
			/*var o_GSTIN = nlapiGetField('custrecord_iit_address_gstn_uid');
			o_GSTIN.setMandatory(true);*/
		}
		else if(b_GST_Liable == 'F')
		{
			b_Flag_Save = 0;
		}
	}
	
	if(name == 'custrecord_iit_address_gstn_uid')
	{		
		var s_GSTIN_No = nlapiGetFieldValue('custrecord_iit_address_gstn_uid');
		//alert('gSTN-->'+s_GSTIN_No);
	   var b_Check = new RegExp("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9A-Z]{3}$");
		
		if (s_GSTIN_No != null && s_GSTIN_No != '' && s_GSTIN_No != undefined) 
		{
			if (b_Check.test(s_GSTIN_No)) 
			{
				b_Flag_Save = 0;
			}
			else 
			{
				b_Flag_Save = 1;
			}
		}
		else
		{
			var b_GST_Liable = nlapiGetFieldValue('custrecord_iit_gst_liable_address');
			if(b_GST_Liable == 'T')
			{
				b_Flag_Save = 1;
			}
			if(b_GST_Liable == 'F')
			{
				b_Flag_Save = 0;
			}
		}
	}
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function SearchGlobalParameter()
{
	
	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	return i_globalRecId;
}

function _logValidation(value)
{
	if(value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN' && value != ' ')
	{
		return true;
	}
	else
	{
		return false;
	}
}


// END FUNCTION =====================================================