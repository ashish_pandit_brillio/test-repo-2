/**
 * @author Jayesh
 */

function addAutoCheck_Button()
{
	try
	{
		if(type == 'view')
		{
			if(nlapiGetUser() != 39108 && nlapiGetUser() != 8177)
			{
				return;
			}
			form.setScript('customscript_cli_invoice_autocheck');
			
			form.addButton('custpage_autoCheck', 'Auto Check', 'autoCheck_Timesheet(\'  ' + nlapiGetRecordId() + '  \');');
			
			if(nlapiGetFieldValue('custbody_invoice_auto_check_unabled') == 'T')
			{
				var message = '<html>';
				message += '<head>';
				message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
				message += '<meta charset="utf-8" />';
				message += '</head>';
				message += '<body>';
				var i_counter = '0%'
				message += "<div id=\"my-progressbar-container\">";
				message += "            ";
				message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
				message += "<img src='https://system.na1.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:50%;width:50%;align=left;'/>";
				message += "        <\/div>";
				message += '</body>';
				message += '</html>';
				form_obj.addField('custpage_progress_bar', 'inlinehtml','Progress Bar');
				nlapiSetFieldValue('custpage_progress_bar',message);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
} 