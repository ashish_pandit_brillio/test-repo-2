function postRESTlet(dataIn)
{
	try
	{
		/*var dataIn = {
			"RequestType": "GET",
			"DATA_DS": {
    "G_1": {
      "PAYROLL_ACTION_ID": "291337",
      "G_2": {
        "FILE_FRAGMENT": {
          "EmployeeExtract": {
            "REP_CATEGORY_NAME": "Termination_Extract_Future",
            "parameters": {
              "request_id": "300000171986616",
              "FLOW_NAME": "TerminationFuture : 2019-07-01 12:30:00.0",
              "legislative_data_group_id": "",
              "effective_date": "2019-06-03",
              "start_date": "",
              "report_category_id": "300000169804659",
              "action_parameter_group_id": "",
              "Action_From_Date": "",
              "Changes_Only": "ATTRIBUTE",
              "Legal_Employee_Id": ""
            },
            "AssignmentData": [
              {
                "OBJECT_ACTION_ID": "189534618",
                "Assignment": {
                  "Assignment_Number": "E115678",
                  "FusionID": "115678",
                  "Termination_Date": "2019-07-31T00:00:00.000Z"
                }
              },
              {
                "OBJECT_ACTION_ID": "189526596",
                "Assignment": {
                  "Assignment_Number": "E105479",
                  "FusionID": "105479",
                  "Termination_Date": "2019-07-03T00:00:00.000Z"
                }
              }
            ]
          }
        }
      }
    }
  }
		}*/
			nlapiLogExecution('Debug','dataIn',JSON.stringify(dataIn));
			var Data_Array; //To store the JSON value
			var request_Type = 'GET';
			var response = new Response();
			switch(request_Type)
			{
				case M_Constants.Request.Get:
				if(!dataIn instanceof Array)
				{
                    response.Data = "Some error with the data sent";
                    response.Status = false;
				}
			    else
				{
					response.Data = futureTerm(dataIn);
                    response.Status = true;
                }
			break;
			}
          return response;
	}

	catch(error)
	{
		nlapiLogExecution('debug','postRESTlet',error);
		response.Data = error;
		response.status = false;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));

}

function futureTerm(dataIn)
{
	try
	{
		var sysDate = new Date();
		sysDate = nlapiDateToString(sysDate);
		sysDate = nlapiStringToDate(sysDate); //prabhat gupta 06/11/2020 NIS-1821 causing issue while comparing the next year date in string format that's why again converted it to date
		nlapiLogExecution('Debug','System Date',sysDate);
		var o_data_employeeData = dataIn.DATA_DS.G_1.G_2.FILE_FRAGMENT.EmployeeExtract.AssignmentData;
		if(_logValidation(o_data_employeeData))
		{
			
			nlapiLogExecution('DEBUG', 'Checking Whether Object or Array');

			if (isArray(o_data_employeeData))
			{
				nlapiLogExecution('debug','Length is ',o_data_employeeData.length);
				
				for (var count = 0 ; count < o_data_employeeData.length ; count++)
				{
					var o_emp_datafields = o_data_employeeData[count].Assignment;
					nlapiLogExecution('DEBUG', 'o_emp_datafields',o_emp_datafields);
					var s_personNumber = o_emp_datafields.FusionID;
	//-----------------------------------------------------------------------------------------------------------
//prabhat gupta NIS-1760 09/10/2020	
					var resignationNotifyDate = o_emp_datafields.Notification_Date
					nlapiLogExecution('Debug','Resignation Notification Date',resignationNotifyDate);		
					
//-------------------------------------------------------------------------------------------------------------------					
					var futurelwd = o_emp_datafields.Termination_Date
					nlapiLogExecution('Debug','Future Term Date',futurelwd);
					
					var tempfuturelwd = new Date(futurelwd);
					nlapiLogExecution('Debug','Future Term Date tempfuturelwd - >',tempfuturelwd);
					tempfuturelwd = nlapiDateToString(tempfuturelwd);
					tempfuturelwd = nlapiStringToDate(tempfuturelwd); //prabhat gupta 06/11/2020 NIS-1821 causing issue while comparing the next year date in string format that's why again converted it to date
					nlapiLogExecution('Debug','Future Term Date tempfuturelwd DTS - >',tempfuturelwd);
					
					if(tempfuturelwd > sysDate)
					{
						var employeeSearch = nlapiSearchRecord("employee",null,
						[
							["custentity_fusion_empid","is",s_personNumber],"AND",
							["custentity_employee_inactive","is","F"]
						], 
						[
							new nlobjSearchColumn("internalid")
						]
						);
						var emp_id = '';
						if(_logValidation(employeeSearch))
						{
							emp_id = employeeSearch[0].getValue('internalid');
						}
						nlapiLogExecution('Debug','Emp InternalID',emp_id);
					
						if(_logValidation(emp_id))
						{
							var loadRec = nlapiLoadRecord('employee',emp_id);
							
							var inactive_reason = loadRec.getFieldValue('custentity_inactivereason');
							
                            if(inactive_reason == "7"){// Global Transfer Return
                               loadRec.setFieldValue('custentity_inactivereason',"");
							}
							
							loadRec.setFieldValue('custentity_resignation_notify_date' , formatDate(resignationNotifyDate));//prabhat gupta NIS-1760 09/10/2020	
							
							loadRec.setFieldValue('custentity_future_term_date' , formatDate(futurelwd))
							var submitid = nlapiSubmitRecord(loadRec);
						}
						nlapiLogExecution('Debug','Record Submit ID',submitid);
					
					}
				}
			}
			else
			{
				//object
				var o_emp_datafields = o_data_employeeData.Assignment;
					nlapiLogExecution('DEBUG', 'o_emp_datafields',o_emp_datafields);
					var s_personNumber = o_emp_datafields.FusionID;
					
//-----------------------------------------------------------------------------------------------------------
//prabhat gupta NIS-1760 09/10/2020		
					var resignationNotifyDate = o_emp_datafields.Notification_Date
					nlapiLogExecution('Debug','Resignation Notification Date',resignationNotifyDate);		
					
//-------------------------------------------------------------------------------------------------------------------					
					
					
					var futurelwd = o_emp_datafields.Termination_Date
					nlapiLogExecution('Debug','Future Term Date',futurelwd);
					
					var tempfuturelwd = new Date(futurelwd);
					nlapiLogExecution('Debug','Future Term Date tempfuturelwd - >',tempfuturelwd);
					tempfuturelwd = nlapiDateToString(tempfuturelwd);
					tempfuturelwd = nlapiStringToDate(tempfuturelwd); //prabhat gupta 06/11/2020 NIS-1821 causing issue while comparing the next year date in string format that's why again converted it to date
					nlapiLogExecution('Debug','Future Term Date tempfuturelwd DTS - >',tempfuturelwd);
					if(tempfuturelwd >= sysDate)
					{
						var employeeSearch = nlapiSearchRecord("employee",null,
						[
							["custentity_fusion_empid","is",s_personNumber],"AND",
							["custentity_employee_inactive","is","F"]
						], 
						[
							new nlobjSearchColumn("internalid")
						]
						);
						var emp_id = '';
						if(_logValidation(employeeSearch))
						{
							emp_id = employeeSearch[0].getValue('internalid');
						}
						nlapiLogExecution('Debug','Emp InternalID',emp_id);
					
						if(_logValidation(emp_id))
						{
							var loadRec = nlapiLoadRecord('employee',emp_id);
							
							var inactive_reason = loadRec.getFieldValue('custentity_inactivereason');
							
							if(inactive_reason == "7"){// Global Transfer Return
                               loadRec.setFieldValue('custentity_inactivereason',"");
							}
							
							loadRec.setFieldValue('custentity_resignation_notify_date' , formatDate(resignationNotifyDate));//prabhat gupta NIS-1760 09/10/2020	
							
							loadRec.setFieldValue('custentity_future_term_date' , formatDate(futurelwd))
							var submitid = nlapiSubmitRecord(loadRec);
						}
						nlapiLogExecution('Debug','Record Submit ID',submitid);
					
					}
			}
		}
	}

	catch(e)
	{
		nlapiLogExecution('Error','Process Error',e);
      throw e;  //Added code to give the reason for failure on 06-11-2020
		
	}

}


function formatDate(fusionDate) {

    if (fusionDate) {
        var datePart = fusionDate.split('T')[0];
        var dateSplit = datePart.split('-');


        // MM/DD/ YYYY
        var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
            dateSplit[0];
        return netsuiteDateString;
    }
    return "";
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}