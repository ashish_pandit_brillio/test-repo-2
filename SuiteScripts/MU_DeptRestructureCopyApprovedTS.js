//massUpdate('timesheet', 33267);
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Dec 2014     amol.sahijwani
 * 2.00 	  23 Apr 2020	  Praveena Madem   updated sublist values on Timesheet
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
//massUpdate('timesheet', 10561);
function massUpdate(recType, recId) {
	// Load the Time Sheet Record
	var recTS = nlapiLoadRecord(recType, recId, {recordmode: 'dynamic'});
	nlapiLogExecution('AUDIT', 'ID: ', recId);
	/*if(recTS.getFieldValue('approvalstatus') != 3)
		{
			return;
		}*/
	
	var isRecordChanged = false;
	
	var recJson = JSON.stringify(recTS);
	
	var recNewTS = nlapiCreateRecord(recType, {recordmode: 'dynamic'});
	
	try
	{
		o_TimeSheet_values = getTSFieldValues(recTS);
		for(var s_field_name in o_TimeSheet_values)
		{
			recNewTS.setFieldValue(s_field_name,o_TimeSheet_values[s_field_name]);
		}
	}
	catch(exTS)
	{
		nlapiLogExecution('ERROR', 'Timesheet: ' + recId, exTS.message);
		return;
	}
	
	// Array of Subrecord names
	var a_days = ['timebill0', 'timebill1', 'timebill2', 'timebill3', 'timebill4', 'timebill5', 'timebill6'];
	
	var i_employee_id = recTS.getFieldValue('employee');
	
	var i_practice = nlapiLookupField('employee', i_employee_id, 'department');
	
	//var i_practice = rec_employee.getFieldValue('department');
	
	// Get Line Count
	var i_line_count = recTS.getLineItemCount('timeitem');//recTS.getLineItemCount('timegrid');
	var i_previous_project_id = -1;
	var i_previous_vertical_id = -1;
	for(var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++)
	{
		recTS.selectLineItem('timeitem',i_line_indx);
		recNewTS.selectNewLineItem('timeitem');
		
		for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
		{
			var sub_record_name = a_days[i_day_indx];
			var o_sub_record_view = recTS.getCurrentLineItemValue('timeitem', sub_record_name);
			
			if(o_sub_record_view)
				{
					var d_current_date = nlapiAddDays(nlapiStringToDate(recTS.getFieldValue('startdate'), 'date'),i_day_indx);
					var d_first_jan = nlapiStringToDate('01/01/2015');
					
					var i_practice_old = recTS.getCurrentLineItemValue('timeitem','department');
					
					var i_vertical_old = recTS.getCurrentLineItemValue('timeitem','class');
					
					var i_project_id = recTS.getCurrentLineItemValue('timeitem','customer');
					var i_vertical = null;
					if(i_previous_project_id != i_project_id)
						{
							var i_end_customer = nlapiLookupField('job',i_project_id,'custentity_endcustomer');
						
							//var i_vertical = null;
						
							if(i_end_customer != '' && i_end_customer != null && i_end_customer != undefined)
								{
									i_vertical = nlapiLookupField('job', i_project_id, 'custentity_vertical');
								}
							else
								{
									var i_customer = nlapiLookupField('job',i_project_id,'customer.custentity_vertical');
									i_vertical = nlapiLookupField('customer', i_customer, 'custentity_vertical');
								}	
						}
					else
						{
							i_vertical = i_previous_vertical_id;
						}
					
					
					//var o_sub_record_new = recNewTS.createCurrentLineItemSubrecord('timegrid', sub_record_name);
					
					var time_entry_fields = new Array(
							'day',
							'subsidiary',
							'employee',
							'projecttaskassignment',							
							'customer',
							'casetaskevent',
							//'item',
							'location',
							'hours',
							'memo',
							'isbillable',
							'approvalstatus',
							'payrollitem',
							'paidexternally',
							'price',
							'rate',
							//'overriderate',
							'department',
							'class',
							'billingclass',
							'createddate',
							'externalid',
							'isexempt',
							'isproductive',
							'isutilized',
							'timetype',
							'custcol_approvalstatus',
							'custcol_is_provision_created_tr',
							'custcol_projectmanager',
							'custcol_projecttype',
							'custcol_subtierpay',
							'custcolcustcol_temp_customer',
							'custcolprj_name',
							'custcol_employeenamecolumn'
							);
							
							var o_new_values = new Object();
							
							for(var i_field = 0; i_field < time_entry_fields.length; i_field++)
								{
									var s_field_name = time_entry_fields[i_field];
									if(s_field_name == 'department')
										{
											if(d_current_date >= d_first_jan)
												{
													o_new_values[s_field_name] = i_practice;
												}
											else
												{
													o_new_values[s_field_name] = i_practice_old;
												}
											
										}
									else if(s_field_name == 'class')
										{
											if(d_current_date >= d_first_jan)
												{
													o_new_values[s_field_name] = i_vertical;
												}
											else
												{
													o_new_values[s_field_name] = i_vertical_old;
												}
										}
									else
										{
                                               if(o_new_values['isbillable'] == 'F' && (s_field_name == 'price' || s_field_name == 'rate'))
{}
else
{
											o_new_values[s_field_name] = recTS.getCurrentLineItemValue('timeitem',s_field_name);
											}
										}
								}
try{
							for(str_field_name in o_new_values)
							{
								if(o_new_values[s_field_name]){
									recNewTS.setCurrentLineItemValue('timeitem',str_field_name,o_new_values[str_field_name]);
								}
							}
							//o_sub_record_new.setFieldValue('approvalstatus', o_sub_record)
							//o_sub_record_new.setFieldValue('price', -1);
							//o_sub_record_new.setFieldValue('rate', o_day_data.rate);
							//o_sub_record_new.commit();
							
							// Check if record is changed
							if(o_new_values['department'] != i_practice_old || o_new_values['class'] != i_vertical_old)
								{
									isRecordChanged = true;
								}
}
catch(e)
{
	nlapiLogExecution('ERROR', 'Record Id: ' + recId, e.message);
	return;
}
				}
		}
		
		recNewTS.commitLineItem('timeitem');
		
	}
	
	if(isRecordChanged)
		{
			
			var isTsDeleted = false;
			
			try
			{
				nlapiDeleteRecord('timesheet', recId);
				isTsDeleted = true;
			}
			catch(e)
			{
				nlapiLogExecution('ERROR', 'Record Id: ' + recId, e.message);
				return;
			}
		
			if(isTsDeleted)
				{
					try
					{
						i_new_record_id = nlapiSubmitRecord(recNewTS, true, true);
						nlapiLogExecution('AUDIT', 'New TimeSheet Saved', i_new_record_id);
					}
					catch(e)
					{
						nlapiLogExecution('ERROR', 'Error Saving New Time Sheet', recId);
						
						nlapiSendEmail(9122, 'amol.sahijwani@brillio.com', 'Error in Script', recJson);
						
						//i_new_record_id = nlapiSubmitRecord(recTS, true, true);
						//nlapiLogExecution('ERROR', 'Old Time Sheet Re-Saved', i_new_record_id);
					}
			}
		}
	
}


function getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date)
{
	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();
	
	// Get Resource allocations for this week
	var filters = new Array();
	filters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
	//filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
	filters[1]	=	new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_end_date);
	filters[2]	=	new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_start_date);
	filters[3]	=	new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');

	var columns = new Array();
	columns[0]	= new nlobjSearchColumn('custeventbstartdate');
	columns[1]	= new nlobjSearchColumn('custeventbenddate');
	columns[2]	= new nlobjSearchColumn('custeventrbillable');
	columns[3]  = new nlobjSearchColumn('custevent3');
	columns[4]	= new nlobjSearchColumn('custevent_otrate');
	columns[5]  = new nlobjSearchColumn('project');

	var search_results = nlapiSearchRecord('resourceallocation', null, filters, columns);
	
	if(search_results != null && search_results.length > 0)
	{
		for(var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++)
			{
				var i_project_id = search_results[i_search_indx].getValue('project');
				var resource_allocation_start_date = new Date(search_results[i_search_indx].getValue('custeventbstartdate'));
				var resource_allocation_end_date   = new Date(search_results[i_search_indx].getValue('custeventbenddate'));
				var is_resource_billable	= search_results[i_search_indx].getValue('custeventrbillable');
				var stRate					= search_results[i_search_indx].getValue('custevent3');
				var otRate					= search_results[i_search_indx].getValue('custevent_otrate');
				a_resource_allocations[i_search_indx] = {
															project_id: i_project_id,
															start_date:resource_allocation_start_date,
															end_date:resource_allocation_end_date,
															is_billable: is_resource_billable,
															st_rate: stRate,
															ot_rate: otRate
														};
			}
	}
	else
	{
		a_resource_allocations = null;
	}
	
	return a_resource_allocations;
}

function getTSFieldValues(o_old_record)
{
	var o_field_values = new Object();
	o_field_values['approvalstatus']= o_old_record.getFieldValue('approvalstatus');
	o_field_values['customform'] 	= o_old_record.getFieldValue('customform');
	o_field_values['employee'] 		= o_old_record.getFieldValue('employee');
	o_field_values['enddate'] 		= o_old_record.getFieldValue('enddate');
	o_field_values['externalid']	= o_old_record.getFieldValue('externalid');
	o_field_values['iscomplete']	= o_old_record.getFieldValue('iscomplete');
	o_field_values['startdate']		= o_old_record.getFieldValue('startdate');
	o_field_values['subsidiary']		= o_old_record.getFieldValue('subsidiary');
	o_field_values['totalhours']		= o_old_record.getFieldValue('totalhours');
	
	return o_field_values;
}
function _is_Valid(obj) //
{
	if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}
function _correct_time(t_time) //
{
	// function is used to correct the time 
	
	if (_is_Valid(t_time)) //
	{
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
		
		var hrs = t_time.split(':')[0]; 
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		
		if (t_time.indexOf(':') > -1) //
		{
			var mins = t_time.split(':')[1]; 
			//nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
			
			if (_is_Valid(mins)) //
			{
				mins = parseFloat(mins) / parseFloat('60'); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
				
				hrs = parseFloat(hrs) + parseFloat(mins); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);
			}
		}
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		return hrs;
	}
	else //
	{
		return 0;
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
	}
}