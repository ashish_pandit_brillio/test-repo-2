//Get post

function employeeSearch(request, response)
 {
	 if (request.getMethod() == 'GET') 
	 {

			  //Create the form and add fields to it 
			  var form = nlapiCreateForm("Suitelet - Employee List For Timesheet");
			  
			  
		//	   var flt = new Array();
		//	  flt[0] = new nlobjSearchFilter('custentity_implementationteam', null, 'is', 'F');
		  
		
		  
		//	  var cols = new Array();
		//	   cols[0]= new nlobjSearchColumn('entityid');
		//	  cols[1]= new nlobjSearchColumn('internalid');
		  
		
		//   var searchresults = searchRecord('employee', null, flt, cols);
		   
		   var select = form.addField('employee_list', 'select', 'Employee Name','employee');
        select.setMandatory(true);
		/*   
		   if(searchresults!=null)
		   {
			  for (var i = 0; i < searchresults.length; i++) 
			  {

				select.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('entityid'));

			  }
		   }
			
	   
			   
		*/
			   var start_date=form.addField('start_date', 'date', 'start date');
			   start_date.setMandatory(true);
			   var end_date=form.addField('end_date', 'date', 'end date');
				  end_date.setMandatory(true);
			   var Reason = form .addField('remarks','textarea','Remarks');
			   Reason.setMandatory(true);
				//   form.setScript(''); 
		form.setScript('customscript_cli_timesheet_validations');
		form.addSubmitButton('Submit');		  
				
		response.writePage(form);				
						
	}
					  
 

 
 //POST call
 else 
	 {
		var form = nlapiCreateForm("Timesheet has been deleted");
		 
		var employee =request.getParameter('employee_list');		          
		
		var st_date = request.getParameter('start_date');		          
		
		var en_date = request.getParameter('end_date');	
		
		
		var search = nlapiSearchRecord("timebill",null,
				[
					["employee","anyof",employee], 
						"AND", 
					["date","within",st_date,en_date],
					"AND",
					["type","anyof","A"],
				], 
				[
					new nlobjSearchColumn("internalid"), 
					new nlobjSearchColumn("date").setSort(false), 
					new nlobjSearchColumn("employee"), 
					new nlobjSearchColumn("subsidiary","employee",null), 
					new nlobjSearchColumn("approvalstatus","timeSheet",null), 
					new nlobjSearchColumn("startdate","timeSheet",null), 
					new nlobjSearchColumn("enddate","timeSheet",null),
                  new nlobjSearchColumn("internalid","timesheet",null)
				]
		); 


			var msg = request.getParameter('remarks');
			if(search)
			{
				var j=0;
				for(var i=0;i<search.length;i++)
				{
					var id = search[i].getValue('internalid');
					if(id)
					{
						nlapiDeleteRecord('timebill',parseInt(id));
						j++;
					}
			    }
				
							var employee = search[0].getValue('employee');
							var status ;
						
							var mailTemplate = "<p>Hi, </p>";
							mailTemplate += "<p> This is to inform you that a timesheet has been deleted. </p>";

							mailTemplate += "<b>Employee : </b>"
											+ search[0].getText('employee') + "<br/>";
							mailTemplate += "<b>Week : </b>" + search[0].getValue("startdate","timeSheet",null)
											+ " - " + search[0].getValue("enddate","timeSheet",null) + "<br/>";
									mailTemplate += "<b>Status : </b>"
											+ search[0].getText("approvalstatus","timeSheet",null) + "<br/>";
									mailTemplate += "<b>Deleted By : </b>"
											+ nlapiGetContext().getName() + "<br/>";
									mailTemplate += "<b>Deleted On : </b>"
											+ nlapiDateToString(new Date(), 'datetimetz') + "<br/>";
									mailTemplate += "<b>Reason for Deletion : </b>" + msg + "<br/>";

									mailTemplate += "<p>Regards, <br/> Information Systems</p>";
								var subsidiary = search[0].getValue("subsidiary","employee",null);
									if (subsidiary == '2') 
									{
										recipient = 'information.systems@brillio.com,vertica@brillio.com,timesheet@BRILLIO.COM,bhavik.pandya@BRILLIO.COM';
                                      
                                      //'shivam.pandya@brillio.com,lisa.joshy@brillio.com,information.systems@brillio.com,vertica@brillio.com,himanshu.negi@brillio.com,kaushal.thakar@brillio.com';
									} 
									else 
									{
										recipient = 'information.systems@brillio.com,timesheet@BRILLIO.COM,';
									}			
						nlapiLogExecution('debug','Total time entries',i);
						nlapiLogExecution('debug','Record Deleted time entries',j);
						
						if(search.length ==j){
                          nlapiDeleteRecord('timesheet',search[0].getValue("internalid","timesheet",null));
						nlapiSendEmail(442, recipient,
											'A Timesheet has been deleted', mailTemplate, null,
											"nitish.mishra@brillio.com");
						nlapiLogExecution('debug','Mail sent to -> ',recipient);					
						nlapiLogExecution('debug','Record Deleted time entries',j);
						}
			}
		  
				
			

	response.writePage(form);
	 }
	 
}