/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Jun 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            recType Record type internal id
 * @param {Number}
 *            recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {

	nlapiSendEmail('442', 'nitish.mishra@brillio.com', 'test',
			'testing mail from mass update');
}
