							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_SendEmailForDAUpdate.js
	Author      : Ashish Pandit
	Date        : 14 Nov 2019
    Description : User Event to send email for Delivery Anchor create and delete  


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   
Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK  =======================================



// BEGIN BEFORE LOAD ==================================================

function afterSubmitSendEmail_DACreate(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'create')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
			var i_Customer = recordObject.getFieldValue('custrecord_fuel_anchor_customer');
			var s_Customer = recordObject.getFieldText('custrecord_fuel_anchor_customer');
			var i_Practice = recordObject.getFieldValue('custrecord_fuel_anchor_practice');
			var i_Resource = recordObject.getFieldValue('custrecord_fuel_anchor_employee');
			var s_Resource = recordObject.getFieldText('custrecord_fuel_anchor_employee');
			var s_CreatedBy = recordObject.getFieldText('custrecord_fuel_anchor_created_by');
			var a_CC_List = getCCList(i_Customer);
			var a_TO_List = getToList(i_Practice);
			a_TO_List.push(i_Resource);
			if(i_Resource)
			{
				var records = new Array();
				records['entity'] = [94862,41571];
				var s_subject = "Assign DA";
				var s_emailBody = "Dear "+s_Resource+"<br>You have assigned as Delivery Anchor for Account "+s_Customer+" by "+s_CreatedBy+".<br>Please plan for fulfillment.<br><br>Brillio-FUEL <br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com"
				nlapiSendEmail(154256,a_TO_List,s_subject,s_emailBody,a_CC_List,null,records);
			}
        }
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecordObject = nlapiGetOldRecord();
			
			var i_OldPractice = oldRecordObject.getFieldValue('custrecord_fuel_anchor_practice');
			var i_OldCustomer = oldRecordObject.getFieldValue('custrecord_fuel_anchor_customer');
			var s_OldCustomer = oldRecordObject.getFieldText('custrecord_fuel_anchor_customer');
			var i_OldPractice = oldRecordObject.getFieldValue('custrecord_fuel_anchor_practice');
			
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
			var i_Practice = recordObject.getFieldValue('custrecord_fuel_anchor_practice');
			var i_Resource = recordObject.getFieldValue('custrecord_fuel_anchor_employee');
			var s_Resource = recordObject.getFieldText('custrecord_fuel_anchor_employee');
			var s_CreatedBy = recordObject.getFieldText('custrecord_fuel_anchor_created_by');
			var a_CC_List = getCCList(i_OldCustomer);
			var a_TO_List = getToList(i_OldPractice,i_Resource);

			nlapiLogExecution('Debug','i_OldPractice '+i_OldPractice,'i_Practice '+i_Practice);
			nlapiLogExecution('Debug','a_TO_List '+a_TO_List,'i_Resource '+i_Resource);
			if(i_OldPractice != i_Practice)
			{
				var records = new Array();
				records['entity'] = [94862,41571];
				var s_subject = "DA Practice Changed";
				var s_emailBody = "Dear "+s_Resource+"<br>You have been deselected as Delivery Anchor for Account "+s_OldCustomer+" by "+s_CreatedBy+" as your employee practice has changed.<br><br>Brillio-FUEL <br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com"
				nlapiSendEmail(154256,a_TO_List,s_subject,s_emailBody,a_CC_List,null,records);
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
function beforeSubmitSendEmail_DADelete(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'delete')
	{
		try
		{
			var i_Customer = nlapiGetFieldValue('custrecord_fuel_anchor_customer');
			var s_Customer = nlapiGetFieldText('custrecord_fuel_anchor_customer');
			var i_Practice = nlapiGetFieldValue('custrecord_fuel_anchor_practice');
			var i_Resource = nlapiGetFieldValue('custrecord_fuel_anchor_employee');
			var s_Resource = nlapiGetFieldText('custrecord_fuel_anchor_employee');
			var s_CreatedBy = nlapiGetFieldText('custrecord_fuel_anchor_created_by');
			var a_CC_List = getCCList(i_Customer);
			
			var a_TO_List = getToList(i_Practice,i_Resource);
			
			if(i_Resource)
			{
				var records = new Array();
				records['entity'] = [94862,41571];
				var s_subject = "Deselect DA";
				var s_emailBody = "Dear "+s_Resource+"<br>You have been deselected as Delivery Anchor for Account "+s_Customer+" by "+s_CreatedBy+".<br><br>Brillio-FUEL <br>This email was sent from a notification only address.<br>If you need further assistance, please write to fuel.support@brillio.com"
				nlapiLogExecution('Debug','s_emailBody ',s_emailBody);
				nlapiSendEmail(154256,a_TO_List,s_subject,s_emailBody,a_CC_List,null,records);
			}
        }
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
function getCCList(i_Customer)
{
	var a_CCList = new Array();
	a_CCList.push('fuel.support@brillio.com');
	if(i_Customer)
	{
		var customrecord_fuel_delivery_anchorSearch = nlapiSearchRecord("customrecord_fuel_delivery_anchor",null,
		[
		   ["custrecord_fuel_anchor_customer","anyof",i_Customer]
		], 
		[
		   new nlobjSearchColumn("email","CUSTRECORD_FUEL_ANCHOR_EMPLOYEE",null)
		]
		);
		if(customrecord_fuel_delivery_anchorSearch)
		{
			for(var i=0;i<customrecord_fuel_delivery_anchorSearch.length;i++)
			{
				a_CCList.push(customrecord_fuel_delivery_anchorSearch[i].getValue("email","CUSTRECORD_FUEL_ANCHOR_EMPLOYEE",null));
			}
		}
	}
	nlapiLogExecution('debug','a_CCList ',a_CCList);
	return a_CCList;
}
function getToList(i_Practice,i_Resource)
{
	var a_ToList = new Array();
	var practiceHead = nlapiLookupField('department',i_Practice,'custrecord_practicehead');
	if(practiceHead)
		a_ToList.push(practiceHead);
	if(i_Resource)
	{
		var temp = i_Resource.split("");
		for(var i=0; i<temp.length;i++)
		{
			a_ToList.push(temp[i]);	
		}
	}
		
	if(i_Practice)
	{
		var customrecord_practice_spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
		[
		   ["custrecord_practice","anyof",i_Practice]
		], 
		[
		   new nlobjSearchColumn('custrecord_spoc')
		]
		);
		if(customrecord_practice_spocSearch)
		{
			for(var i=0;i<customrecord_practice_spocSearch.length;i++)
			{
				a_ToList.push(customrecord_practice_spocSearch[i].getValue("custrecord_spoc"));
			}
		}
	}
	nlapiLogExecution('debug','a_ToList ',a_ToList);
	return a_ToList;
}