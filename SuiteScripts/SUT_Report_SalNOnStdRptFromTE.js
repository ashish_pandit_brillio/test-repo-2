//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SUT_Report_SalNOnStdRpt_FromTS.js
     Author: Anukaran
     Company: Inspirria Cloud tech
     Date:	  22-07-2020
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
    12/8/2020			Anukaran						Vertica/Nainesh				 Added internal project internal id in search
    13/8/2020			Anukaran						Vertica/Nainesh			Added Bench project in item and remove the open status from approval status criteria
	
    1. if ST hour is greater than and less than week1 hour or week2 hours Record will come in suitelet
	2. If OT, DT, Floating hours greater than 0. Record will show into sublist.	
	3. If first week is statisfy the condition but second week is not then script will show the record on sublist and vice versa.
	4. Script will not show both week 0 data.
	5. Suitelet will show missing timeheet data also.
	
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================

//END GLOBAL VARIABLE BLOCK  =======================================

//BEGIN SUITELET ==================================================

function GenerateReport(request, response) {
    try {
        var a_employeearray = new Array();
        if (request.getMethod() == 'GET') {
            var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS');
            var contextObj = nlapiGetContext();
            //nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)

            var beginUsage = contextObj.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'GET', ' beginUsage===>' + beginUsage)

            //Added Date Field in suitelet
            var period_end_date = form.addField('custpage_startdate', 'Date', 'Period End Date :');

            //Add Week 1 hours and Week 2 hours to the form
            var wk1hours = form.addField('custpage_wk1_hours', 'float', 'Week 1 Hours').setMandatory(true);
            var wk2hours = form.addField('custpage_wk2_hours', 'float', 'Week 2 Hours').setMandatory(true);

            var submitedForm = form.addSubmitButton("Submit");
            //nlapiLogExecution('DEBUG', 'Submited_Log', 'Submit_id = ' +submitedForm);

            form.setScript('customscript_cli_sut_report_salnonstdrpt'); //Call clint script for excel generation
            //nlapiLogExecution('DEBUG', 'BeforeLoadLog', 'calling Client');
            var csv_btn = form.addButton('custombutton', 'Export As CSV', 'callGenerateExcel()');
            //nlapiLogExecution('DEBUG', 'csv_btn_Log', 'csv_btn = ' +csv_btn);

            //Added Sublist in Report
            var sublist1 = form.addSubList('record', 'list', 'Sal-Non-Std Rpt-From TS');
            var s_file = sublist1.addField('custevent_file', 'text', 'GCI(INC) ID');
            var s_Employeefile = sublist1.addField('custevent_employeeid', 'text', 'Fusion ID');
            var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
            var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
            var s_WK_ST = sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
            //s_WK_ST.setDefaultValue('0');
            var s_WK_OT = sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
            var s_WK_DT = sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
            var s_WK1_Timeoff = sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');
            var s_WK1_holyday = sublist1.addField('custevent_holidayweek1', 'text', 'WK1 HOLIDAY');
            var s_WK2_ST = sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
            var s_WK2_OT = sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
            var s_WK2_DT = sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
            var s_WK2_Timeoff = sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');
            var s_WK2_holyday = sublist1.addField('custevent_holidayweek2', 'text', 'WK2 HOLIDAY');
            //var s_total_holyday = sublist1.addField('custevent_holidayweek_total', 'text', 'Total HOLIDAY');
            var s_ot_hours = sublist1.addField('custevent_othours', 'text', 'OT Hours');
            var s_Total_Time_Off = sublist1.addField('custevent_totaltimeoff', 'text', 'Total Timeoff');
            var s_Total_DT_hrs = sublist1.addField('custevent_totaldttime', 'text', 'Total DT');
            var s_Floating_H = sublist1.addField('custevent_floatingh', 'text', 'Float H(if in either wk)');
            //var s_ProjectCity = sublist1.addField('custevent_projectcity', 'text', 'Project_City'); //Comment on 6/8/2020
            // var s_ProjectLocation = sublist1.addField('custevent_projectloc', 'text', 'Project Location'); //Comment on 6/8/2020
            var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Assignment Category');
            var s_Department = sublist1.addField('custevent_department', 'textarea', 'Department');
            var s_FLSA_Code = sublist1.addField('custevent_flsa_code', 'textarea', 'FLSA CODE');
            // var s_User_Notes = sublist1.addField('custevent_usernotes', 'textarea', 'User_Notes');
            //var s_Customer = sublist1.addField('custevent_customer', 'text', 'Customer'); //Comment on 6/8/2020

            response.writePage(form);

        } else {
            var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS');
            var count_search;
            var ot_total_week1 = 0;
            var week_dt_weeek1 = 0;
            var week1timoff_week1 = 0;
            var floatingHoliday_week1 = 0;
            var ot_total_week2 = 0;
            var week_dt_weeek2 = 0;
            var week1timoff_week2 = 0;
            var floatingHoliday_week2 = 0;
            var total_floatingHoliday = 0;
            var w1_holiday = 0;
            var w2_holiday = 0;
            var week1_date;
            var week2_date;

            var contextObj = nlapiGetContext();
            //nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)

            var beginUsage = contextObj.getRemainingUsage();
            // nlapiLogExecution('DEBUG', 'GET', ' beginUsage===>' + beginUsage)

            //Added Date Field in suitelet
            var period_end_date = form.addField('custpage_startdate', 'Date', 'Period End Date :');
            //Add Week 1 hours and Week 2 hours to the form
            var wk1hours = form.addField('custpage_wk1_hours', 'float', 'Week 1 Hours').setMandatory(true);
            var wk2hours = form.addField('custpage_wk2_hours', 'float', 'Week 2 Hours').setMandatory(true);

            var submitedForm = form.addSubmitButton("Submit");
            //nlapiLogExecution('DEBUG', 'Submited_Log', 'Submit_id = ' +submitedForm);

            form.setScript('customscript_cli_sut_report_salnonstdrpt'); //Call clint script for excel generation
            //nlapiLogExecution('DEBUG', 'BeforeLoadLog', 'calling Client');

            var csv_btn = form.addButton('custombutton', 'Export As CSV', 'callGenerateExcel()');

            //Added Sublist in Report
            var sublist1 = form.addSubList('record', 'list', 'Sal-Non-Std Rpt-From TS');
            var s_file = sublist1.addField('custevent_file', 'text', 'GCI(INC) ID');
            var s_Employeefile = sublist1.addField('custevent_employeeid', 'text', 'Fusion ID');
            var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
            var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
            var s_WK_ST = sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
            //s_WK_ST.setDefaultValue('0');
            var s_WK_OT = sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
            var s_WK_DT = sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
            var s_WK1_Timeoff = sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');
            var s_WK1_holyday = sublist1.addField('custevent_holidayweek1', 'text', 'WK1 HOLIDAY');
            var s_WK2_ST = sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
            var s_WK2_OT = sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
            var s_WK2_DT = sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
            var s_WK2_Timeoff = sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');
            var s_WK2_holyday = sublist1.addField('custevent_holidayweek2', 'text', 'WK2 HOLIDAY');
            //var s_total_holyday = sublist1.addField('custevent_holidayweek_total', 'text', 'Total HOLIDAY');
            var s_ot_hours = sublist1.addField('custevent_othours', 'text', 'OT Hours');
            var s_Total_Time_Off = sublist1.addField('custevent_totaltimeoff', 'text', 'Total Timeoff');
            var s_Total_DT_hrs = sublist1.addField('custevent_totaldttime', 'text', 'Total DT');
            var s_Floating_H = sublist1.addField('custevent_floatingh', 'text', 'Float H(if in either wk)');

            // var s_ProjectCity = sublist1.addField('custevent_projectcity', 'text', 'Project_City'); //Comment on 6/8/2020
            // var s_ProjectLocation = sublist1.addField('custevent_projectloc', 'text', 'Project Location'); //Comment on 6/8/2020
            var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Assignment Category');
            var s_Department = sublist1.addField('custevent_department', 'textarea', 'Department');
            var s_FLSA_Code = sublist1.addField('custevent_flsa_code', 'textarea', 'FLSA CODE');
            //var s_User_Notes = sublist1.addField('custevent_usernotes', 'textarea', 'User_Notes');
            // var s_Customer = sublist1.addField('custevent_customer', 'text', 'Customer');  //Comment on 6/8/2020


            var f_week1_hours = request.getParameter('custpage_wk1_hours');
            //nlapiLogExecution('DEBUG', 'Post results** ', ' f_week1_hours =' + f_week1_hours)
            var f_week2_hours = request.getParameter('custpage_wk2_hours');
            //nlapiLogExecution('DEBUG', 'Post results** ', ' f_week2_hours =' + f_week2_hours)
            var s_before_date = request.getParameter('custpage_startdate');
            //nlapiLogExecution('DEBUG', 'Post results** ', ' s_before_date =' + s_before_date)
            var s_date1 = nlapiStringToDate(s_before_date)
            //nlapiLogExecution('DEBUG', 'Post results** ', ' date after string to s_date1 =' + s_date1)
            var fromDate = nlapiAddDays(s_date1, -13)
            // nlapiLogExecution('AUDIT', 'Post results** ', ' fromDate =' + fromDate)
            var s_afterdate = nlapiDateToString(fromDate)
            // nlapiLogExecution('AUDIT', 'Post results** ', ' fromDate after conversion =' + s_afterdate)

            var week2date = nlapiAddDays(s_date1, -6)
            // nlapiLogExecution('AUDIT', 'Post results** ', ' week2date =' + week2date)

            var week2_date = nlapiDateToString(week2date)
            // nlapiLogExecution('AUDIT', 'Post results** ', ' fromDate after conversion =' + week2_date)

            week1_date = s_afterdate;
            // nlapiLogExecution('AUDIT', 'week1_date** ', ' week1_date =' + week1_date)


            //Set the default value in exiting field
            if (_logValidation(s_before_date) && _logValidation(f_week1_hours) && _logValidation(f_week2_hours)) {
                period_end_date.setDefaultValue(s_before_date);
                wk1hours.setDefaultValue(f_week1_hours);
                wk2hours.setDefaultValue(f_week2_hours);
            }

            //Calling Function for get 2 week of data and show after calculation show one result on suitelet
            var timebill_Search = timesheet_Search(s_afterdate, s_before_date);
            // nlapiLogExecution('ERROR', 'saved Search', 'Total RecordS Found= ' + timebill_Search.length);

            if (timebill_Search) {
                // nlapiLogExecution('ERROR', 'saved Search', 'Record on length= ' + timebill_Search.length);

                var x = 1;
                //var y = 1;
                var count_search = timebill_Search.length;
                var emp = [];
                var single_week_employees = [];

                var temp_employee_inc_id;
                var temp_employee_id;
                var temp_employee_F_name;
                var temp_employee_L_name;
                var temp_st_hours;
                var temp_ot_hours;
                var temp_emp_dt;
                var temp_sumOFtimeOFf;
                var temp_sumOFHoliday;

                //var temp_project_city;
                //var temp_project_location;
                var temp_employee_category;
                var temp_department;

                var temp_empIntID_usernotes;


                //nlapiLogExecution('DEBUG', 'saved Search', 'Record on length= '+timebillSearch.length);

                var temp;
                for (var i = 0; i < count_search; i++) {
                    var result = timebill_Search[i];
                    var columns = result.getAllColumns();
                    var columnLen = columns.length;
                    //nlapiLogExecution('DEBUG','Search_logcolumnLen','columnLen = '+columnLen);


                    var calendar_Week = result.getValue(columns[0]);
                    //nlapiLogExecution('DEBUG','Search Data','calendar_Week = '+calendar_Week);
                    var employee_name = result.getValue(columns[1]);
                    //nlapiLogExecution('DEBUG','Search Data','employee_name = '+employee_name);
                    var employee_inc_id = result.getValue(columns[2]);
                    //nlapiLogExecution('DEBUG','Search Data','employee_inc_id = '+employee_inc_id);
                    var employee_id = result.getValue(columns[3]);
                    // nlapiLogExecution('DEBUG','Search Data','employee_id = '+employee_id);
                    var employee_F_name = result.getValue(columns[4]);
                    //nlapiLogExecution('DEBUG','Search Data','employee_F_name = '+employee_F_name);
                    var employee_L_name = result.getValue(columns[5]);
                    //nlapiLogExecution('DEBUG','Search Data','employee_L_name = '+employee_L_name);
                    var st_hours = result.getValue(columns[6]);
                    //nlapiLogExecution('DEBUG','Search Data','st_hours = '+st_hours);
                    var ot_hours = result.getValue(columns[7]);
                    //nlapiLogExecution('DEBUG','Search Data','ot_hours = '+ot_hours);
                    var sumOFtimeOFf = result.getValue(columns[8]);
                    //nlapiLogExecution('DEBUG','Search Data','sumOFtimeOFf##= '+sumOFtimeOFf);
                    var sumOFHoliday = result.getValue(columns[9]);
                    //nlapiLogExecution('DEBUG','Search Data','sumOFHoliday = '+sumOFHoliday);
                    var sumOFfp = result.getValue(columns[10]);
                    //nlapiLogExecution('DEBUG','Search Data','sumOFfp = '+sumOFfp);
                    var sumOFFloating_Holiday = result.getValue(columns[11]);
                    //nlapiLogExecution('DEBUG','Search Data','sumOFFloating_Holiday** = '+sumOFFloating_Holiday);
                    // var project_city = result.getText(columns[12]);   //Comment on 6/8/2020
                    //nlapiLogExecution('DEBUG','Search Data','project_city = '+project_city);
                    //var project_location = result.getText(columns[13]);//Comment on 6/8/2020
                    //nlapiLogExecution('DEBUG','Search Data','project_location = '+project_location);
                    var employee_category = result.getText(columns[12]);
                    //nlapiLogExecution('DEBUG','Search Data','employee_category = '+employee_category);
                    var department = result.getText(columns[13]);
                    //nlapiLogExecution('DEBUG','Search Data','department = '+department);
                    var emp_dt = result.getValue(columns[14]);
                    //nlapiLogExecution('DEBUG','Search Data','emp_dt = '+emp_dt);
                    var empIntID_usernotes = result.getValue(columns[15]);
                    var flsa_code = result.getText(columns[16]);
                    // nlapiLogExecution('DEBUG','Search Data','empIntID_usernotes = '+empIntID_usernotes);


                    //in search getting two result for after the calculation one result need to show.
                    if (emp.indexOf(employee_id) == -1) {
                        // nlapiLogExecution('ERROR', 'i Value', '****empIntID_usernotes= ' + empIntID_usernotes);
                        single_week_employees.push(empIntID_usernotes);
                        //if ((Number(f_week1_hours) < Number(st_hours) ||  ot_hours > 0 || (Number(f_week1_hours) > Number(st_hours) && Number(st_hours) > 0)   || sumOFtimeOFf > 0 || sumOFFloating_Holiday > 0) && (i == 0 || evenNo == 0))
                        if ((Number(f_week1_hours) != Number(st_hours) || ot_hours > 0 || sumOFtimeOFf > 0 || sumOFFloating_Holiday > 0)) // && (i == 0 || evenNo == 0))
                        {
                            //nlapiLogExecution('DEBUG', 'Fisrt IF - x Value', x);
                            // nlapiLogExecution('ERROR', 'A-Fisrt CLOSE ****IF - x Value', employee_inc_id);
                            week1timoff_week1 = 0;
                            ot_total_week1 = 0;
                            week_dt_weeek1 = 0;
                            week1timoff_week1 = 0;
                            floatingHoliday_week1 = 0;

                            sublist1.setLineItemValue('custevent_file', Number(x), employee_inc_id);
                            // nlapiLogExecution('DEBUG', 'B-Fisrt CLOSE ****IF - x Value', x);	

                            sublist1.setLineItemValue('custevent_employeeid', x, employee_id);
                            // nlapiLogExecution('AUDIT', '1-Fisrt****IF - employee_id=>', employee_id + '|employee_F_name=>' + employee_F_name);
                            sublist1.setLineItemValue('custevent_firstname', x, employee_F_name);
                            //nlapiLogExecution('DEBUG', 'C-Fisrt CLOSE ****IF - x Value', x);	

                            sublist1.setLineItemValue('custevent_lastname', x, employee_L_name);
                            sublist1.setLineItemValue('custevent_wkst1', x, st_hours);
                            sublist1.setLineItemValue('custevent_wkot1', x, ot_hours);
                            sublist1.setLineItemValue('custevent_wkdt1', x, emp_dt);

                            // nlapiLogExecution('AUDIT', 'BeforeSEt_sumOFtimeOFf', 'BeforeSEt_sumOFtimeOFf=>' + sumOFtimeOFf);
                            sublist1.setLineItemValue('custevent_wk1timeoff1', x, sumOFtimeOFf);
                            sublist1.setLineItemValue('custevent_holidayweek1', x, sumOFHoliday);
                            // sublist1.setLineItemValue('custevent_projectcity', x, project_city);
                            // sublist1.setLineItemValue('custevent_projectloc', x, project_location);
                            sublist1.setLineItemValue('custevent_employeetype', x, employee_category);
                            sublist1.setLineItemValue('custevent_department', x, department);
                            sublist1.setLineItemValue('custevent_flsa_code', x, flsa_code);

                            // sublist1.setLineItemValue('custevent_customer', x, empIntID_usernotes); //Comment on 6/8/2020

                            ot_total_week1 = parseFloat(ot_total_week1) + parseFloat(ot_hours);
                            week_dt_weeek1 = parseFloat(week_dt_weeek1) + parseFloat(emp_dt);
                            // nlapiLogExecution('AUDIT', '1-Fisrt CLOSE ****IF - Before_sumOFtimeOFf', 'Before Calculate_sumOFtimeOFf' + sumOFtimeOFf);
                            //nlapiLogExecution('AUDIT', '1-Fisrt CLOSE ****IF - week1timoff_week1', 'Before week1timoff_week1' + week1timoff_week1);
                            week1timoff_week1 = parseFloat(week1timoff_week1) + parseFloat(sumOFtimeOFf);
                            // nlapiLogExecution('AUDIT', '1-Fisrt CLOSE ****IF - week1timoff_week1', 'AfterCalculation=>' + week1timoff_week1);

                            w1_holiday = parseFloat(w1_holiday) + parseFloat(sumOFHoliday);
                            floatingHoliday_week1 = parseFloat(floatingHoliday_week1) + parseFloat(sumOFFloating_Holiday);
                            //nlapiLogExecution('AUDIT', '1-Fisrt CLOSE ****IF - x Value', x);
                            emp.push(employee_id);
                            // nlapiLogExecution('ERROR', '2-Fisrt CLOSE ****IF - x Value', x);
                            temp = 0;
                        } else {
                            // Above Firstweek condition fulfilled but second week not then this condition will print the value of first week in suitelet
                            // nlapiLogExecution('ERROR', 'Fisrt ELSE - x Value', x);

                            temp = 1;

                            temp_employee_inc_id = employee_inc_id;
                            temp_employee_id = employee_id;
                            temp_employee_F_name = employee_F_name;
                            temp_employee_L_name = employee_L_name;
                            temp_st_hours = st_hours;
                            temp_ot_hours = ot_hours;
                            temp_emp_dt = emp_dt;
                            temp_sumOFtimeOFf = sumOFtimeOFf;
                            //nlapiLogExecution('AUDIT', '1-Fisrt Else temp_sumOFtimeOFf Value', temp_sumOFtimeOFf);
                            temp_sumOFHoliday = sumOFHoliday;

                            // temp_project_city = project_city;
                            // temp_project_location = project_location;
                            temp_employee_category = employee_category;
                            temp_department = department;
                            temp_empIntID_usernotes = empIntID_usernotes;

                            emp.push(employee_id);
                        }
                        // nlapiLogExecution('ERROR', 'Fisrt IF - CLOSED x Value', x);
                    } else {

                        //nlapiLogExecution('AUDIT', 'second emp.indexOf(employee_id) st_hours', st_hours);
                        //--Start: Getting all single week timesheet employeeinternalid
                        var index = single_week_employees.indexOf(empIntID_usernotes)
                        if (index > -1) {
                            single_week_employees.splice(index, 1);
                        }
                        //--End: Getting all single week timesheet employeeinternalid
                        // if((Number(f_week2_hours) < Number(st_hours) ||  ot_hours > 0 || (Number(f_week2_hours) > Number(st_hours) && Number(st_hours) > 0) || sumOFtimeOFf > 0   || sumOFFloating_Holiday > 0) && evenNo == 1)
                        if ((Number(f_week2_hours) != Number(st_hours) || ot_hours > 0 || sumOFtimeOFf > 0 || sumOFFloating_Holiday > 0)) // && evenNo == 1)
                        {
                            // nlapiLogExecution('ERROR', 'second IF *(Before another IF) - x Value', st_hours);


                            if (temp == 1) {
                                // nlapiLogExecution('ERROR', 'second IF - x Value', x);

                                sublist1.setLineItemValue('custevent_file', x, temp_employee_inc_id);
                                sublist1.setLineItemValue('custevent_employeeid', x, temp_employee_id);
                                sublist1.setLineItemValue('custevent_firstname', x, temp_employee_F_name);
                                sublist1.setLineItemValue('custevent_lastname', x, temp_employee_L_name);
                                sublist1.setLineItemValue('custevent_wkst1', x, temp_st_hours);
                                sublist1.setLineItemValue('custevent_wkot1', x, temp_ot_hours);
                                sublist1.setLineItemValue('custevent_wkdt1', x, temp_emp_dt);
                                // nlapiLogExecution('ERROR', 'I am Temp 1', temp_sumOFtimeOFf);
                                sublist1.setLineItemValue('custevent_wk1timeoff1', x, temp_sumOFtimeOFf);
                                sublist1.setLineItemValue('custevent_holidayweek1', x, temp_sumOFHoliday);

                                //sublist1.setLineItemValue('custevent_projectcity', x, temp_project_city);
                                // sublist1.setLineItemValue('custevent_projectloc', x, temp_project_location);
                                sublist1.setLineItemValue('custevent_employeetype', x, temp_employee_category);
                                sublist1.setLineItemValue('custevent_department', x, temp_department);
                                sublist1.setLineItemValue('custevent_department', x, temp_empIntID_usernotes);
                                sublist1.setLineItemValue('custevent_flsa_code', x, flsa_code);

                                week1timoff_week1 = 0;
                                floatingHoliday_week1 = 0;
                                w1_holiday = 0;
                                week_dt_weeek1 = 0;
                                ot_total_week1 = 0;
                                temp = 0;
                            }


                            ot_total_week2 = parseFloat(ot_total_week2) + parseFloat(ot_hours);
                            var total_OT_hours = parseFloat(ot_total_week1) + parseFloat(ot_total_week2);

                            week_dt_weeek2 = parseFloat(week_dt_weeek2) + parseFloat(emp_dt);
                            var total_dt_hours = parseFloat(week_dt_weeek1) + parseFloat(week_dt_weeek2);


                            // nlapiLogExecution('ERROR', 'sumOFtimeOFf', ' Week2_sumOFtimeOFf**=>' + sumOFtimeOFf);
                            // nlapiLogExecution('ERROR', 'week1timoff_week1', ' week1timoff_week1**=>' + week1timoff_week1);
                            week1timoff_week2 = parseFloat(week1timoff_week2) + parseFloat(sumOFtimeOFf);
                            //nlapiLogExecution('AUDIT','week1timoff_week2','week1timoff_week2***'+week1timoff_week2 + 'week1timoff_week1***=>'+week1timoff_week1);
                            var total_timeOff = parseFloat(week1timoff_week1) + parseFloat(week1timoff_week2);
                            //nlapiLogExecution('AUDIT','total_timeOff','total_timeOff=>'+total_timeOff);

                            //w2_holiday = parseFloat(w2_holiday) + parseFloat(sumOFHoliday);
                            //var total_Holiday = parseFloat(w1_holiday) + parseFloat(w2_holiday);  

                            floatingHoliday_week2 = parseFloat(floatingHoliday_week2) + parseFloat(sumOFFloating_Holiday);
                            total_floatingHoliday = parseFloat(floatingHoliday_week1) + parseFloat(floatingHoliday_week2);


                            sublist1.setLineItemValue('custevent_wkst2', x, st_hours);
                            sublist1.setLineItemValue('custevent_wkot2', x, ot_hours);
                            sublist1.setLineItemValue('custevent_wkdt2', x, emp_dt);

                            sublist1.setLineItemValue('custevent_wk1timeoff2', x, sumOFtimeOFf);
                            sublist1.setLineItemValue('custevent_holidayweek2', x, sumOFHoliday);
                            //sublist1.setLineItemValue('custevent_holidayweek_total', x, total_Holiday);
                            sublist1.setLineItemValue('custevent_othours', x, total_OT_hours);
                            // nlapiLogExecution('AUDIT', 'total_timeOff', 'total_timeOff Before Set=>' + total_timeOff);
                            sublist1.setLineItemValue('custevent_totaltimeoff', x, total_timeOff);
                            //nlapiLogExecution('AUDIT','total_dt_hours','total_dt_hours Before Set=>'+total_dt_hours);
                            sublist1.setLineItemValue('custevent_totaldttime', x, total_dt_hours);
                            sublist1.setLineItemValue('custevent_department', x, department);
                            sublist1.setLineItemValue('custevent_floatingh', x, total_floatingHoliday);
                            sublist1.setLineItemValue('custevent_flsa_code', x, flsa_code);
                            
                            x++;
                        } else {

                            // nlapiLogExecution('ERROR', 'second ELSE FIRsT - x Value', x);
                            // nlapiLogExecution('ERROR', 'second ELSE FIRsT -ot_hours', st_hours);
                            if (temp != 1) {
                                // nlapiLogExecution('ERROR', 'second ELSE TEMP!=1 condition - x Value', x);
                                // nlapiLogExecution('ERROR', 'i Value', 'i Week Two2= ' + i);

                                ot_total_week2 = parseFloat(ot_total_week2) + parseFloat(ot_hours);
                                total_OT_hours = parseFloat(ot_total_week1) + parseFloat(ot_total_week2);

                                week_dt_weeek2 = parseFloat(week_dt_weeek2) + parseFloat(emp_dt);
                                var total_dt_hours = parseFloat(week_dt_weeek1) + parseFloat(week_dt_weeek2);


                                week1timoff_week2 = parseFloat(week1timoff_week2) + parseFloat(sumOFtimeOFf);
                                // nlapiLogExecution('AUDIT', 'week1timoff_week2_Else', 'week1timoff_week2_Else=>' + week1timoff_week2 + 'week1timoff_week1_Else=>' + week1timoff_week1);
                                var total_timeOff = parseFloat(week1timoff_week1) + parseFloat(week1timoff_week2);
                                // nlapiLogExecution('AUDIT', 'total_timeOff_Else', 'total_timeOff_Else =>' + total_timeOff);

                                // w2_holiday = parseFloat(w2_holiday) + parseFloat(sumOFHoliday);
                                //var total_Holiday = parseFloat(w1_holiday) + parseFloat(w2_holiday);

                                floatingHoliday_week2 = parseFloat(floatingHoliday_week2) + parseFloat(sumOFFloating_Holiday);
                                total_floatingHoliday = parseFloat(floatingHoliday_week1) + parseFloat(floatingHoliday_week2);

                                sublist1.setLineItemValue('custevent_wkst2', x, st_hours);
                                sublist1.setLineItemValue('custevent_wkot2', x, ot_hours);
                                sublist1.setLineItemValue('custevent_wkdt2', x, emp_dt);

                                sublist1.setLineItemValue('custevent_wk1timeoff2', x, sumOFtimeOFf);
                                sublist1.setLineItemValue('custevent_holidayweek2', x, sumOFHoliday);
                                //sublist1.setLineItemValue('custevent_holidayweek_total', x, total_Holiday);
                                sublist1.setLineItemValue('custevent_othours', x, total_OT_hours);
                                // nlapiLogExecution('AUDIT', 'total_timeOff_Else', 'total_timeOff_Else before Set =>' + total_timeOff);
                                sublist1.setLineItemValue('custevent_totaltimeoff', x, total_timeOff);
                                sublist1.setLineItemValue('custevent_totaldttime', x, total_dt_hours);
                                sublist1.setLineItemValue('custevent_department', x, department);
                                sublist1.setLineItemValue('custevent_flsa_code', x, flsa_code);
                                sublist1.setLineItemValue('custevent_floatingh', x, total_floatingHoliday);
                                x++;
                            }
                        }


                        ot_total_week1 = 0;
                        ot_total_week2 = 0;

                        week_dt_weeek1 = 0;
                        week_dt_weeek2 = 0;

                        week1timoff_week1 = 0;
                        week1timoff_week2 = 0;

                        floatingHoliday_week1 = 0;
                        floatingHoliday_week2 = 0;

                    }
                    // nlapiLogExecution('ERROR', 'TOTAL OUTSIDE x Value', x);
                } //end of For loop
                // nlapiLogExecution('ERROR', 'SINGLE WEEK EMPLOYEES', single_week_employees);
                // nlapiLogExecution('ERROR', 'Before Call Function SINGLE WEEK EMPLOYEES', 'single_week_employees_length=>' + single_week_employees.length);
                // nlapiLogExecution('ERROR', 'TOTAL OUTSIDE x Value', x);

                //Calling function for missing the timesheet / Duplicate Removed Saved Search
                var time_bill = duplicate_Search_removed(s_afterdate, s_before_date, single_week_employees);

                // nlapiLogExecution('ERROR', 'saved Search**', 'Record Duplicate= ' + time_bill.length);
                var floating_dup_Holiday_week1 = 0;
                var total_dup_floatingHoliday;
                var floating_dup_Holiday_week2 = 0;

                var countsearch = time_bill.length;
                for (var j = 0; j < countsearch; j++) {
                    var result1 = time_bill[j];
                    var columns = result1.getAllColumns();
                    var columnLen = columns.length;

                    var dup_calendar_Week = result1.getValue(columns[0]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_calendar_Week** = ' + dup_calendar_Week);
                    var dup_employee_name = result1.getValue(columns[1]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_employee_name = ' + dup_employee_name);
                    var dup_employee_inc_id = result1.getValue(columns[2]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_employee_inc_id = ' + dup_employee_inc_id);
                    var dup_employee_id = result1.getValue(columns[3]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_employee_id = ' + dup_employee_id);
                    var dup_employee_F_name = result1.getValue(columns[4]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_employee_F_name = ' + dup_employee_F_name);
                    var dup_employee_L_name = result1.getValue(columns[5]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_employee_L_name = ' + dup_employee_L_name);
                    var dup_st_hours = result1.getValue(columns[6]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_st_hours = ' + dup_st_hours);
                    var dup_ot_hours = result1.getValue(columns[7]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_ot_hours = ' + dup_ot_hours);
                    var dup_sumOFtimeOFf = result1.getValue(columns[8]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_sumOFtimeOFf##= ' + dup_sumOFtimeOFf);
                    var dup_sumOFHoliday = result1.getValue(columns[9]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_sumOFHoliday = ' + dup_sumOFHoliday);
                    var dup_sumOFfp = result1.getValue(columns[10]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_sumOFfp = ' + dup_sumOFfp);
                    var dup_sumOFFloating_Holiday = result1.getValue(columns[11]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_sumOFFloating_Holiday** = ' + dup_sumOFFloating_Holiday);
                    //var dup_project_city = result1.getText(columns[12]); //Comment on 6/8/2020
                    // nlapiLogExecution('ERROR','Search Data','dup_project_city = '+dup_project_city);
                    // var dup_project_location = result1.getText(columns[13]); //Comment on 6/8/2020
                    // nlapiLogExecution('ERROR','Search Data','dup_project_location = '+dup_project_location); //Comment on 6/8/2020
                    var dup_employee_category = result1.getText(columns[12]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_employee_category = ' + dup_employee_category);
                    var dup_department = result1.getText(columns[13]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_department = ' + dup_department);
                    var dup_emp_dt = result1.getValue(columns[14]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_emp_dt = ' + dup_emp_dt);
                    var dup_empIntID_usernotes = result1.getValue(columns[15]);
                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_empIntID_usernotes = ' + dup_empIntID_usernotes);
                    var dup_flsa_code = result1.getText(columns[16])

                    // nlapiLogExecution('ERROR', 'Search Data', 'dup_empIntID_usernotes** = ' + dup_empIntID_usernotes);
                    if (dup_calendar_Week == week1_date) {
                        // nlapiLogExecution('ERROR', 'Week1 Set Data', 'Week1 Set Data Enter');
                        floating_dup_Holiday_week1 = parseFloat(floating_dup_Holiday_week1) + parseFloat(dup_sumOFFloating_Holiday);

                        sublist1.setLineItemValue('custevent_file', Number(x), dup_employee_inc_id);
                        sublist1.setLineItemValue('custevent_employeeid', x, dup_employee_id);
                        sublist1.setLineItemValue('custevent_firstname', x, dup_employee_F_name);
                        sublist1.setLineItemValue('custevent_lastname', x, dup_employee_L_name);
                        sublist1.setLineItemValue('custevent_wkst1', x, dup_st_hours);
                        sublist1.setLineItemValue('custevent_wkot1', x, dup_ot_hours);
                        sublist1.setLineItemValue('custevent_wkdt1', x, dup_emp_dt);
                        sublist1.setLineItemValue('custevent_wk1timeoff1', x, dup_sumOFtimeOFf);
                        sublist1.setLineItemValue('custevent_holidayweek1', x, dup_sumOFHoliday);
                        //  sublist1.setLineItemValue('custevent_projectcity', x, dup_project_city); //Comment on 6/8/2020
                        // sublist1.setLineItemValue('custevent_projectloc', x, dup_project_location); //Comment on 6/8/2020
                        sublist1.setLineItemValue('custevent_employeetype', x, dup_employee_category);
                        sublist1.setLineItemValue('custevent_department', x, dup_department);
                        // sublist1.setLineItemValue('custevent_customer', x, dup_empIntID_usernotes); //Comment on 6/8/2020
                        sublist1.setLineItemValue('custevent_wkst2', x, '0.0');
                        sublist1.setLineItemValue('custevent_wkot2', x, '0.0');
                        sublist1.setLineItemValue('custevent_wkdt2', x, '0.0');
                        sublist1.setLineItemValue('custevent_wk1timeoff2', x, '0.0');
                        sublist1.setLineItemValue('custevent_holidayweek2', x, '0.0');
                        sublist1.setLineItemValue('custevent_othours', x, dup_ot_hours);
                        sublist1.setLineItemValue('custevent_totaltimeoff', x, dup_sumOFtimeOFf);
                        sublist1.setLineItemValue('custevent_totaldttime', x, dup_emp_dt);
                        sublist1.setLineItemValue('custevent_floatingh', x, floating_dup_Holiday_week1);
                        sublist1.setLineItemValue('custevent_flsa_code', x, dup_flsa_code);
                        x++;
                        // nlapiLogExecution('ERROR', 'Week1 Set Data', 'Week1 Set Data exist');

                    } else if (dup_calendar_Week == week2_date) {
                        // nlapiLogExecution('ERROR', 'Week2 Set Data', 'Week2 Set Data Enter');

                        floating_dup_Holiday_week2 = parseFloat(floating_dup_Holiday_week2) + parseFloat(dup_sumOFFloating_Holiday);
                        total_dup_floatingHoliday = parseFloat(floating_dup_Holiday_week1) + parseFloat(floating_dup_Holiday_week2);
                        /*  nlapiLogExecution('ERROR','Search Data','week2_date** = '+week2_date);
                           nlapiLogExecution('ERROR','Search Data','week2dup_employee_F_name** = '+dup_employee_F_name);
                            nlapiLogExecution('ERROR','Search Data','week2dup_employee_id** = '+dup_employee_id);*/
                        sublist1.setLineItemValue('custevent_file', Number(x), dup_employee_inc_id);
                        sublist1.setLineItemValue('custevent_employeeid', x, dup_employee_id);
                        sublist1.setLineItemValue('custevent_firstname', x, dup_employee_F_name);
                        sublist1.setLineItemValue('custevent_lastname', x, dup_employee_L_name);
                        sublist1.setLineItemValue('custevent_wkst1', x, '0.0');
                        sublist1.setLineItemValue('custevent_wkot1', x, '0.0');
                        sublist1.setLineItemValue('custevent_wkdt1', x, '0.0');
                        sublist1.setLineItemValue('custevent_wk1timeoff1', x, '0.0');
                        sublist1.setLineItemValue('custevent_holidayweek1', x, '0.0');
                        //sublist1.setLineItemValue('custevent_projectcity', x, dup_project_city); //Comment on 6/8/2020
                        //sublist1.setLineItemValue('custevent_projectloc', x, dup_project_location); //Comment on 6/8/2020
                        sublist1.setLineItemValue('custevent_employeetype', x, dup_employee_category);
                        sublist1.setLineItemValue('custevent_department', x, dup_department);
                        //	sublist1.setLineItemValue('custevent_customer', x, dup_empIntID_usernotes); //Comment on 6/8/2020
                        sublist1.setLineItemValue('custevent_wkst2', x, dup_st_hours);
                        sublist1.setLineItemValue('custevent_wkot2', x, dup_ot_hours);
                        sublist1.setLineItemValue('custevent_wkdt2', x, dup_emp_dt);
                        sublist1.setLineItemValue('custevent_wk1timeoff2', x, dup_sumOFtimeOFf);
                        sublist1.setLineItemValue('custevent_holidayweek2', x, dup_sumOFHoliday);
                        sublist1.setLineItemValue('custevent_othours', x, dup_ot_hours);
                        sublist1.setLineItemValue('custevent_totaltimeoff', x, dup_sumOFtimeOFf);
                        sublist1.setLineItemValue('custevent_totaldttime', x, dup_emp_dt);
                        sublist1.setLineItemValue('custevent_floatingh', x, total_dup_floatingHoliday);
                        sublist1.setLineItemValue('custevent_flsa_code', x, dup_flsa_code);
                        x++;
                        // nlapiLogExecution('ERROR', 'Week2 Set Data', 'Week2 Set Data Exit');
                    }

                } //End of For J
            } //End of if(timebill)


            response.writePage(form);
        }
    } catch (e) {
        // nlapiLogExecution('DEBUG', 'Search results ', ' value of e-====== **************** =' + e.toString())
    }
}

//search for getting all result
function timesheet_Search(s_afterdate, s_before_date) {
    // nlapiLogExecution('ERROR', 'I am in function', 's_afterdate' + s_afterdate)
    // nlapiLogExecution('ERROR', 'I am in function', 's_before_date' + s_before_date)
    //nlapiLogExecution('ERROR','I am in function','single_week_employees**'+single_week_employees)

    var b_SearchResults = [];
    var iRscnt = 1000,
        min = 0,
        max = 1000;

    var timebill_Search = nlapiCreateSearch("timebill",
        [
            ["type", "anyof", "A"],
            "AND",
		  //["item", "anyof", "2425", "2479", "2221", "2222", "2481", "2480"],
		  //["item","anyof","2425","2479","2481","2221","2222","2480","2426"], // Added internal project internal id 12/8/2020
		    ["item","anyof","2426","2425","2479","2481","2221","2222","2480","2224"], //Added bench project 13/8/2020
            "AND",
            ["subsidiary", "anyof", "2"],
            "AND",
          //["approvalstatus", "anyof", "2", "1", "3"],
			["approvalstatus","anyof","3","2"], // Code added 13/08/2020 Removed Open status
            "AND",
            ["employee.employeetype", "anyof", "3"],
            "AND",
            ["employee.custentity_persontype", "anyof", "2"],
            "AND",
            ["date", "within", s_afterdate, s_before_date]
        ],
        [
            new nlobjSearchColumn("startdate", "timeSheet", "GROUP"),
            new nlobjSearchColumn("employee", null, "GROUP"),
            new nlobjSearchColumn("custentity_incid", "employee", "GROUP"),
            new nlobjSearchColumn("custentity_fusion_empid", "employee", "GROUP"),
            new nlobjSearchColumn("firstname", "employee", "GROUP"),
            new nlobjSearchColumn("lastname", "employee", "GROUP"),
            //new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("CASE WHEN {item}='ST' then {durationdecimal} else 0 end"),
            //new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item} IN ('ST','FP') then {durationdecimal} else 0 end"),
			//new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("CASE WHEN {item} IN ('ST','FP','Internal Projects') then {durationdecimal} else 0 end"), //Added internal project 12/8/2020
			new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("CASE WHEN {item} IN ('ST','FP','Internal Projects','Bench Project') then {durationdecimal} else 0 end"), //Added Bench Project 13/08/2020
			new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='OT' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='Leave' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='Holiday' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='FP' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='Floating Holiday' then {durationdecimal} else 0 end"),
            //new nlobjSearchColumn("custentity_projectcity", "job", "GROUP"), //Comment on 6/8/2020
            //new nlobjSearchColumn("custentity_worklocation", "job", "GROUP"), //Comment on 6/8/2020
            //new nlobjSearchColumn("custentity_emp_category", "employee", "GROUP"),//Comment on 6/8/2020
            new nlobjSearchColumn("custentity_assignmentcategory", "employee", "GROUP"),
            new nlobjSearchColumn("department", "employee", "GROUP"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='DT' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("internalid", "employee", "GROUP").setSort(false),
            new nlobjSearchColumn("custentity_flsacode", "employee", "GROUP")
        ]
    );
    // nlapiLogExecution('ERROR', 'I am in function First', 'I am in function First')
    while (iRscnt == 1000) {
        var resultSet1 = timebill_Search.runSearch().getResults(min, max);
        b_SearchResults = b_SearchResults.concat(resultSet1);
        min = max;
        max += 1000;
        iRscnt = resultSet1.length;
        // nlapiLogExecution('audit',' inside while resultSet1.length',resultSet1.length)
    }
    // nlapiLogExecution('audit',' outside while resultSet1.length',resultSet1.length)

    return b_SearchResults;
}



//Function for remove the duplicate record form main search || Missing Timesheet of 1week of 2week.

function duplicate_Search_removed(s_afterdate, s_before_date, single_week_employees) {
    // nlapiLogExecution('ERROR', 'I am in function', 's_afterdate' + s_afterdate)
    // nlapiLogExecution('ERROR', 'I am in function', 'single_week_employees**' + single_week_employees)

    var a_SearchResults = [];
    var iRscnt = 1000,
        min = 0,
        max = 1000;

    var timebillSearch = nlapiCreateSearch("timebill",
        [
            ["type", "anyof", "A"],
            "AND",
		  //["item", "anyof", "2425", "2479", "2221", "2222", "2481", "2480"],
		  //["item","anyof","2425","2479","2481","2221","2222","2480","2426"], // Added internal project internal id 12/8/2020
			["item","anyof","2426","2425","2479","2481","2221","2222","2480","2224"], //Added bench project 13/8/2020
            "AND",
            ["subsidiary", "anyof", "2"],
            "AND",
          //["approvalstatus", "anyof", "2", "1", "3"],
			["approvalstatus","anyof","3","2"], // Code added 13/08/2020 Removed Open status
            "AND",
            ["employee.employeetype", "anyof", "3"],
            "AND",
            ["employee.custentity_persontype", "anyof", "2"],
            "AND",
            ["date", "within", s_afterdate, s_before_date],
            "AND",
            ["employee", "anyof", single_week_employees] //"2622","161179"]
        ],
        [
            new nlobjSearchColumn("startdate", "timeSheet", "GROUP"),
            new nlobjSearchColumn("employee", null, "GROUP"),
            new nlobjSearchColumn("custentity_incid", "employee", "GROUP"),
            new nlobjSearchColumn("custentity_fusion_empid", "employee", "GROUP"),
            new nlobjSearchColumn("firstname", "employee", "GROUP"),
            new nlobjSearchColumn("lastname", "employee", "GROUP"),
            //new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("CASE WHEN {item}='ST' then {durationdecimal} else 0 end"),
            //new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item} IN ('ST','FP') then {durationdecimal} else 0 end"),
            //new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("CASE WHEN {item} IN ('ST','FP','Internal Projects') then {durationdecimal} else 0 end"), //Added internal project 12/8/2020
			new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("CASE WHEN {item} IN ('ST','FP','Internal Projects','Bench Project') then {durationdecimal} else 0 end"), //Added Bench Project 13/08/2020
			new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='OT' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='Leave' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='Holiday' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='FP' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='Floating Holiday' then {durationdecimal} else 0 end"),
            //new nlobjSearchColumn("custentity_projectcity", "job", "GROUP"), //Comment on 6/8/2020
            //new nlobjSearchColumn("custentity_worklocation", "job", "GROUP"), //Comment on 6/8/2020
            //new nlobjSearchColumn("custentity_emp_category", "employee", "GROUP"),//Comment on 6/8/2020
            new nlobjSearchColumn("custentity_assignmentcategory", "employee", "GROUP"),
            new nlobjSearchColumn("department", "employee", "GROUP"),
            new nlobjSearchColumn("formulanumeric", null, "SUM").setFormula("CASE WHEN {item}='DT' then {durationdecimal} else 0 end"),
            new nlobjSearchColumn("internalid", "employee", "GROUP").setSort(false),
            new nlobjSearchColumn("custentity_flsacode", "employee", "GROUP")
        ]
    );
    // nlapiLogExecution('ERROR', 'I am in function Second', 'I am in function Second')
    while (iRscnt == 1000) {
        var resultSet = timebillSearch.runSearch().getResults(min, max);
        a_SearchResults = a_SearchResults.concat(resultSet);
        min = max;
        max += 1000;
        iRscnt = resultSet.length;
    }

    return a_SearchResults;
}


// function get_flsa_code(employee_id) {
//     var flsa_code=nlapiLookupField('employee', employee_id, 'custentity_flsacode', true);
//     // nlapiLogExecution('audit', 'flsa code','employee id: '+employee_id+'flsa code: '+flsa_code);
//     return flsa_code
// }

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN' && value != ' ') {
        return true;
    } else {
        return false;
    }
}