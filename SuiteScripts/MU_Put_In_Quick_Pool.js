/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Sep 2015     nitish.mishra
 *
 */

/**
 * @param {String}
 *            recType Record type internal id
 * @param {Number}
 *            recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	try {
		nlapiInitiateWorkflowAsync('customrecord_ts_quick_approval_pool',
		        recId, 'customworkflow_quick_approval_pool');
	} catch (err) {
		nlapiLogExecution('error', 'put in pool', err);
	}
}
