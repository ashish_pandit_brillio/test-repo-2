// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Salary_Upload_Button_Display_BTPL.js
	Author      : Jayesh Dinde
	Date        : 6 April 2016
    Description : Validations on Salary Upload Process for BTPL Record


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoad_salary_upload(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmit_salary_upload(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_salary_upload(type)
{
	
	form.setScript('customscript_cli_salary_validation_btpl');
	
	var i_recordID = nlapiGetRecordId();
	
	var i_status = nlapiGetFieldValue('custrecord_status_btpl');
	
	var i_JE_array = nlapiGetFieldValues('custrecord_journal_entries_created_btpl')
	
	var vendor_provision_month = nlapiGetFieldText('custrecord_month_btpl');
	var vendor_provision_year = nlapiGetFieldText('custrecord_year_btpl');
	
	if (type == 'view')
	{
		if(nlapiGetFieldValue('custrecord_journal_entries_created_btpl') != null && nlapiGetFieldValue('custrecord_journal_entries_created_btpl') != '')
		{
			var vendor_provision_year = nlapiGetFieldText('custrecord_year_btpl');
			var vendor_provision_rate_type = nlapiGetFieldValue('custrecord_rate_type');
			var vendor_provision_subsidiary_id = nlapiGetFieldValue('custrecord_subsidiary_btpl');
			var vendor_provision_month = nlapiGetFieldText('custrecord_month_btpl');
		
			form.addButton('custpage_xport_je', 'Export JE Data', 'Xport_JE_Data(\'  ' + vendor_provision_year + '  \',\'  ' + vendor_provision_month + '  \',\'  ' + vendor_provision_subsidiary_id + '  \',\'  ' + vendor_provision_rate_type + '  \');');
		}
		
		if(nlapiGetFieldValue('custrecord_completion_status') != 'Complete' && nlapiGetFieldValue('custrecord_status_btpl') != 4)
		{		
			var which_bttn_clickd = nlapiGetFieldValue('custrecord_which_bttn_clicked');
			var column = new Array();
			var filters = new Array();
			filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING','COMPLETE']));	
			if(which_bttn_clickd == 'VALIDATE')
			{
				filters.push(new nlobjSearchFilter('internalid','script','is','861')); //VALIDATE MONTHLY
			}
			else if(which_bttn_clickd == 'VALIDATE_HOURLY')
			{
				filters.push(new nlobjSearchFilter('internalid','script','is','909')); //VALIDATE HOURLY
			}
			else if(which_bttn_clickd == 'CREATE_JE')
			{
				filters.push(new nlobjSearchFilter('internalid','script','is','867')); //CREATE_JE MONTHLY
			}
			else if(which_bttn_clickd == 'CREATE_JE_HOURLY')
			{
				filters.push(new nlobjSearchFilter('internalid','script','is','910')); //CREATE_JE HOURLY
			}
			else if(which_bttn_clickd == 'DELETE_JE')
			{
				filters.push(new nlobjSearchFilter('internalid','script','is','865')); //DELETE_JE MONTHLY
			}
			else
			{
				filters.push(new nlobjSearchFilter('internalid','script','is','908')); // DELETE_JE HOURLY
			}
	
			column.push(new nlobjSearchColumn('name', 'script'));
			column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
			column.push(new nlobjSearchColumn('datecreated'));
			column.push(new nlobjSearchColumn('status'));
			column.push(new nlobjSearchColumn('startdate'));
			column.push(new nlobjSearchColumn('enddate'));
			column.push(new nlobjSearchColumn('queue'));
			column.push(new nlobjSearchColumn('percentcomplete'));
			column.push(new nlobjSearchColumn('queueposition'));
			column.push(new nlobjSearchColumn('percentcomplete'));

			var a_search_results =  nlapiSearchRecord('scheduledscriptinstance', null, filters, column);
			if(_logValidation(a_search_results)) 
		    {
				for(var i=0;i<a_search_results.length;i++)
		    	{
		    		var s_script  = a_search_results[i].getValue('name', 'script');
		
					var d_date_created  = a_search_results[i].getValue('datecreated');
		
		           	var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
		
					var s_script_status  = a_search_results[i].getValue('status');
				   
		    	}
		    }
			
			if(i_percent_complete =='' || i_percent_complete == null || i_percent_complete == undefined)
			{
				s_script_status = 'Not Started';
				i_percent_complete = '0%';
			}

			if(s_script_status == 'Complete')
			{
				i_percent_complete = '100%';
				s_script_status = 'Complete';
			}
			
			nlapiSubmitField('customrecord_salary_upload_process_btpl',nlapiGetRecordId(),'custrecord_completion_status',s_script_status);				
			nlapiSubmitField('customrecord_salary_upload_process_btpl',nlapiGetRecordId(),'custrecord_prcnt_complete',i_percent_complete);
				
			var message = '<html>';
			message += '<head>';
			message += "<meta http-equiv=\"refresh\" content=\"5\" \/>";
			message += '<meta charset="utf-8" />';
			message += '</head>';
			message += '<body>';
			var i_counter = '0%'
			message += "<div id=\"my-progressbar-container\">";
			message += "            ";
			message += "<div class=\"ajax-content\" id=\"test14\" name=\"test14\" style='height:50%;width:50%;' align='left' valign='top'>"; //opacity: .75;
			message += "<img src='https://system.na1.netsuite.com/core/media/media.nl?id=190162&c=3883006&h=1cf2a9f8bd8b922539c8' align='left' style='height:50%;width:50%;align=left;'/>";
			message += "        <\/div>";
			message += '</body>';
			message += '</html>';
			nlapiSetFieldValue('custrecord_progress_bar',message);
		}
		
		var rcrd_id = nlapiGetRecordId();
		var flag = 1;
		var vendor_subsidiary_sub_id = nlapiGetFieldValue('custrecord_subsidiary_btpl');
		var vendor_provision_year = nlapiGetFieldText('custrecord_year_btpl');
		var vendor_provision_debit_acnt = nlapiGetFieldText('custrecord_account_debit_s_p_btpl');
		var vendor_provision_debit_acnt_no = nlapiGetFieldValue('custrecord_account_debit_s_p_btpl');
		var vendor_provision_credit_acnt = nlapiGetFieldText('custrecord_account_credit_s_p_btpl');
		var vendor_provision_credit_acnt_no = nlapiGetFieldValue('custrecord_account_credit_s_p_btpl');
		var vendor_provision_criteria = nlapiGetFieldText('custrecord_criteria_btpl');
		var vendor_provision_subsidiary = nlapiGetFieldText('custrecord_subsidiary_btpl');
		var vendor_provision_subsidiary_id = nlapiGetFieldValue('custrecord_subsidiary_btpl');
		var vendor_provision_month = nlapiGetFieldText('custrecord_month_btpl');
		var which_bttn_clickd = nlapiGetFieldValue('custrecord_which_bttn_clicked');
		var rate_type = nlapiGetFieldValue('custrecord_rate_type');
			
		if((i_status == 4)||(i_status == 2)||(i_status == 8))
		{
			form.addButton('custpage_validate_process','Validate / Process','validate_process(\'  ' + rcrd_id + '  \')'); //validate_process()
		}
		
		if ((i_status == 1) || (i_status == 2))
		{
			
			form.addButton('custpage_load_data', 'Load Vendor Provision Data', 'Load_Data(\'  ' + vendor_subsidiary_sub_id + '  \',\'  ' + vendor_provision_year + '  \',\'  ' + vendor_provision_debit_acnt + '  \',\'  ' + vendor_provision_credit_acnt + '  \',\'  ' + vendor_provision_criteria + '  \',\'  ' + vendor_provision_subsidiary + '  \',\'  ' + vendor_provision_month + '  \',\'  ' + flag + '  \',\'  ' + rate_type + '  \');');
		}
		
		if(i_status == 2)
		{
			form.addButton('custpage_load_data', 'Download Error Logs', 'Down_error_logs(\'  ' + vendor_subsidiary_sub_id + '  \',\'  ' + vendor_provision_year + '  \',\'  ' + vendor_provision_subsidiary + '  \',\'  ' + vendor_provision_month + '  \',\'  ' + rate_type + '  \');');
		}
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven', null, 'is', vendor_provision_month.trim());
		filters[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven', null, 'is', vendor_provision_year.trim());
		filters[2] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null,'is',rate_type);
		filters[3] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_subsidiary_sub_id));
		
		var a_results = nlapiSearchRecord('customrecord_salary_upload_file', null, filters, null);
		if (_logValidation(a_results))
		{
			if(i_status != 6 && i_status != 5 && i_status != 9 && i_status != 3 && i_status != 4)
				form.addButton('custpage_create_je', 'Create Journal Entry', 'create_journal_entry(\'  ' + rcrd_id + '  \',\'  ' + vendor_provision_month + '  \',\'  ' + vendor_provision_year + '  \',\'  ' + vendor_provision_debit_acnt_no + '  \',\'  ' + vendor_provision_credit_acnt_no + '  \',\'  ' + vendor_provision_subsidiary_id + '  \')');
			
			if(i_status != 6 && i_status != 8 && i_status != 9 && i_status != 3 && i_status != 4)
				form.addButton('custpage_delete_ven_sal_recrd', 'Delete Vendor Provision Data', 'delete_vendor_sal_upload_rcrd(\'  ' + vendor_provision_month + '  \',\'  ' + vendor_provision_year + '  \',\'  ' + rcrd_id + '  \',\'  ' + rate_type + '  \',\'  ' + vendor_provision_subsidiary_id + '  \');');
		}
      
     	
		var filters_error_logs = new Array();
		filters_error_logs[0] = new nlobjSearchFilter('custrecord_month_ven_pro', null, 'is', vendor_provision_month.trim());
		filters_error_logs[1] = new nlobjSearchFilter('custrecord_year_ven_pro', null, 'is', vendor_provision_year.trim());
		filters_error_logs[2] = new nlobjSearchFilter('custrecord_rate_type_error_log', null, 'is', rate_type);
		filters_error_logs[3] = new nlobjSearchFilter('custrecord_subsidiary_ven_pro', null, 'is', parseInt(vendor_subsidiary_sub_id));
		
		var i_error_logs_results = nlapiSearchRecord('customrecord_error_logs_vendor_pro', null, filters_error_logs, null);
		if(_logValidation(i_error_logs_results))
		{
			if((i_status != 4)&&(i_status != 2)&&(i_status != 8)&&(i_status != 6)&&(i_status != 9)&&(i_status != 3))
				form.addButton('custpage_validate_process','Validate / Process','validate_process(\'  ' + rcrd_id + '  \')'); //validate_process()
		}
		
		form.addButton('custpage_refresh','Refresh','refresh_salary_upload(\'  ' + which_bttn_clickd + '  \')');	
		
	}
	
	
		
	return true;
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_salary_upload(type)
{ 
  		
  if(type == 'edit')
  {
	  try
	  {	  	
		var i_context = nlapiGetContext();
    
        var s_context_type = i_context.getExecutionContext();
        nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Context Type ' + s_context_type);
				
		if(s_context_type == 'userinterface')
		{		
	  	var i_recordID = nlapiGetRecordId()
		
		var s_record_type = nlapiGetRecordType();
		
		if(_logValidation(i_recordID)&& _logValidation(s_record_type))
		{
		  var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID)	
		  
		  var o_old_recordOBJ = nlapiGetOldRecord();
		  
		  if(_logValidation(o_recordOBJ)&&_logValidation(o_old_recordOBJ))
		  {
		  	// ================ Current Record Values =======================
			
		  	var sal_upload_btpl_date = o_recordOBJ.getFieldValue('custrecord_from_date_btpl');
			nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' sal_upload_btpl_date -->' + sal_upload_btpl_date);
			 			
			// ================ Previous Record Values =======================
			
			var old_sal_upload_btpl_date = o_old_recordOBJ.getFieldValue('custrecord_from_date_btpl');
			nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' old_sal_upload_btpl_date -->' + old_sal_upload_btpl_date);
			
						
			if(!_logValidation(old_sal_upload_btpl_date)&&_logValidation(sal_upload_btpl_date))
			{
				o_recordOBJ.setFieldValue('custrecord_notes_btpl','');
				o_recordOBJ.setFieldValue('custrecord_status_btpl',4);
				
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
			    nlapiLogExecution('DEBUG', 'after submit function',' ------------ Submit ID ----------->' + i_submitID);
		
				// ================= Call Schedule Script ================
				
				 /*var params=new Array();
			     params['custscript_record_id_err'] = i_submitID
				 params['custscript_csv_file_no_err'] = i_old_CSV_FileNo
					 
				 var status = nlapiScheduleScript('customscript_sch_salary_upload_delete_er',null,params);
				 nlapiLogExecution('DEBUG', 'afterSubmit_salary_upload', ' Status -->' + status);*/
								
			}//New date is updated		
		  }//Record OBJ		
		}//Check
			
		}//Context Type
		  	
	  }//TRY
	  catch(exception)
	  {
		  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	  }//CATCH	
  }//TYPE = EDIT
 
	/*try
	{
		nlapiLogExecution('debug','redirect code');
		var vendor_provision_month = nlapiGetFieldText('custrecord_month_btpl');
		var vendor_provision_year = nlapiGetFieldText('custrecord_year_btpl');
		var vendor_provision_debit_acnt = nlapiGetFieldText('custrecord_account_debit_s_p_btpl');
		var vendor_provision_credit_acnt = nlapiGetFieldText('custrecord_account_credit_s_p_btpl');
		var vendor_provision_criteria = nlapiGetFieldText('custrecord_criteria_btpl');
		var vendor_provision_subsidiary = nlapiGetFieldText('custrecord_subsidiary_btpl');
		var vendor_subsidiary_sub_id = nlapiGetFieldValue('custrecord_subsidiary_btpl');
		
		var params=new Array();
		params['custscript_vendor_provision_month'] = vendor_provision_month;
		params['custscript_vendor_provision_year'] = vendor_provision_year;
		params['custscript_vendor_provision_debit_acnt'] = vendor_provision_debit_acnt;
		params['custscript_vendor_provision_credit_acnt'] = vendor_provision_credit_acnt;
		params['custscript_vendor_provision_criteria'] = vendor_provision_criteria;
		params['custscript_vendor_provision_subsidiary'] = vendor_provision_subsidiary;
		params['custscript_vendor_subsidiary_sub_id'] = vendor_subsidiary_sub_id;
				 
		nlapiSetRedirectURL('SUITELET', 'customscript_vendor_provision_data', 'customdeploy_vendor_provision_data', null,params);
		nlapiLogExecution('debug','redirect code1');
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG', 'ERROR MESSAGE:- ',err);
	}*/
  
  
  return true;
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
