/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jul 2018     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
try{
	var context = nlapiGetContext();
	/*var i_month = context.getSetting('SCRIPT', 'custscript_pl_month');
	var i_year = context.getSetting('SCRIPT', 'custscript_pl_year');
	var i_user = context.getSetting('SCRIPT', 'custscript_pl_user');*/
	
	//Test Cases
	var s_month = 'APR';
	var s_year = '2019';
	var i_user = '41571';
	//nlapiLogExecution('DEBUG','i_Month,i_Year,i_user','i_Month:'+i_month+ 'i_Year:'+i_year+ 'i_user:'+i_user);
	/*if(_logValidation(i_month)){
		var s_month = nlapiLookupField('customlist_month',parseInt(i_month),'name');
	}
	else{
		return 'Month cannot be blank!!';
	}
	if(_logValidation(i_year)){
		var s_year = nlapiLookupField('customlist_year',parseInt(i_year),'name');
	}
	else{
		return 'Year cannot be blank!!';
	}*/
	if(_logValidation(i_user)){
		var emp_email = nlapiLookupField('employee',parseInt(i_user),'email');
	}
	nlapiLogExecution('DEBUG','i_Month,i_Year,i_user','s_month:'+s_month+ 's_year:'+s_year+ 'emp_email:'+emp_email);
	var s_start_date = get_current_month_start_date(s_month, s_year);
	var s_end_date = get_current_month_end_date(s_month, s_year);
	
	var d_start_date = nlapiStringToDate(s_start_date);
	var d_end_date = nlapiStringToDate(s_end_date);
	
	var a_data_external = searchTransactions_external_with_Projects(s_start_date,s_end_date);
	
//	var a_data_external_WithoutPro = searchTransactions_external_withOut_Projects(s_start_date,s_end_date);
	try{
	//	a_data_external = a_data_external.concat(a_data_external_WithoutPro);
	}
	catch(e){
		nlapiLogExecution('DEBUG','Concat Error',e);
	}
	nlapiYieldScript();
	var a_data_internal = searchTransactions_Internal_with_Projects(s_start_date,s_end_date);
	
	//var a_data_internal_WithoutPro = searchTransactions_Internal_withOut_Projects(s_start_date,s_end_date);
	
	try{
	var newArray = a_data_external.concat(a_data_internal);
	}
	catch(e){
		nlapiLogExecution('DEBUG','Concat Error',e);
	}
	nlapiYieldScript();

	var excel_result = createExcel(newArray,s_month,s_year);
	//Returns File ID
	var newAttachment = nlapiLoadFile(excel_result);
	
	var a_emp_attachment = new Array();
	a_emp_attachment['entity'] = 41571;

	nlapiSendEmail(442, emp_email, 'PL Summary Report Month '+s_month+'-'+s_year, 'Please find the attachement of pl summary report', null,['sai.vannamareddy@brillio.com'],
			a_emp_attachment, newAttachment);
	nlapiLogExecution('DEBUG','Mail Sent',emp_email);
}
catch(e){
	nlapiLogExecution('DEBUG','Process Error - Main',e);
}
}
//Create Excel logic
function createExcel(a_results,s_month,s_year){
	try{
		/*var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		/*'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'*/
		
		var strVar_excel = '';
		
		var html = "";
		

		html += "Account,";
		html += "Type,";
		html += "Date,";
		html += "Amount Foreign Currency,";
		html += "Period,";
		html += "Transaction Number,";    //'currency_text': currency_text, //f_revRate ////'exchange_rate': ex_rate,
		html += "Currency,";
		//html += "Exchange Rate - USD,";
		html += "Name,";
		html += "Amount USD,";
		html += "Billing From,";
		html += "Billing To,";
		html += "Customer ID,";
		html += "Customer Name,";
		html += "Project ID,";
		html += "Project Name,";
		html += "Project Description,";
		html += "Employee ID,";
		html += "Employee Name,";
		html += "Sub Practice,";
		html += "Parent Practice,";
		html += "Region Setup,";
		html += "Location,";
		html += "Memo,";
		html += "Memo Line,";
		html += "Description,";
		html += "Hour,";
		html += "Bill Rate,";
		html += "Employee Type,";
		html += "Person Type,";
		html += "Onsite/Offshore,";
		html += "Billing Type,";
		html += "Project Category,";
		html += "Project Services,";
		html += "Books,";
		html += "Final Ledger Detailed,";
		html += "Final Ledger,"; //
		html += "Cost Head,";
		html += "Cost Head Detailed,";
		html += "TB Reco,";
		html += "Type-Revised,";
		html += "Revised Client Name-A/C,";
		html += "Practice revised,";
		html += "\r\n";
		
		
	
		
		for(var k=0;k<a_results.length;k++){
			
			var temp = a_results[k];
						
			
			html +=  _logValidation(temp.account) == false ? ' ' : temp.account.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.type) == false ? ' ' : temp.type;
			html += ",";
			html += _logValidation(temp.dt) == false ? ' ' : temp.dt;
			html += ",";
			html += _logValidation(temp.amount_foreign) == false ? ' ' : temp.amount_foreign;
			html += ",";
			html += _logValidation(temp.period) == false ? ' ' : temp.period;
			html += ",";
			html += _logValidation(temp.num) == false ? ' ' : temp.num;
			html += ",";
			html += _logValidation(temp.currency_text) == false ? ' ' : temp.currency_text;
			html += ",";
			//html += _logValidation(temp.exchange_rate) == false ? ' ' : temp.exchange_rate;
			//html += ",";
			html += _logValidation(temp.s_customer) == false ? ' ' : temp.s_customer.replace(/[,.\t\n\r\f\v]/g , ''); //replace(/[^a-zA-Z0-9_]/g, ' '); 
			html += ",";
			html += _logValidation(temp.amount) == false ? ' ' : temp.amount;
			html += ",";
			html += _logValidation(temp.bill_from) == false ? ' ' : temp.bill_from;
			html += ",";
			html += _logValidation(temp.bill_to) == false ? ' ' : temp.bill_to;
			html += ",";
			html += _logValidation(temp.cust_id) == false ? ' ' : temp.cust_id;
			html += ",";
			html += _logValidation(temp.cust_name) == false ? ' ' : temp.cust_name.replace(/[,.\t\n\r\f\v]/g , '');  
			html += ",";
			html += _logValidation(temp.project_id) == false ? ' ' : temp.project_id;
			html += ",";
			html += _logValidation(temp.pro_name) == false ? ' ' : temp.pro_name.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.pro_description) == false ? ' ' : temp.pro_description.replace(/[,.\t\n\r\f\v]/g , '');
			html += ","; 
			html += _logValidation(temp.emp_id) == false ? ' ' : temp.emp_id;
			html += ",";
			html += _logValidation(temp.emp_name) == false ? ' ' : temp.emp_name.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.sub_practice) == false ? ' ' : temp.sub_practice.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.practice) == false ? ' ' : temp.practice;
			html += ",";
			html += _logValidation(temp.s_region) == false ? ' ' : temp.s_region;
			html += ",";
			html += _logValidation(temp.location) == false ? ' ' : temp.location;
			html += ",";
			html += _logValidation(temp.memo_main) == false ? ' ' : temp.memo_main.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.memo_line) == false ? ' ' : temp.memo_line.replace(/[^a-zA-Z0-9_]/g, ' ');
			html += ",";
			html += _logValidation(temp.description) == false ? ' ' : temp.description.replace(/[^a-zA-Z0-9_]/g, ' '); //.replace(/[ \t\n\r\f\v]/g, ' ');
			html += ",";
			html += _logValidation(temp.hour) == false ? ' ' : temp.hour;
			html += ",";
			html += _logValidation(temp.bill_rate) == false ? ' ' : temp.bill_rate;
			html += ",";
			html += _logValidation(temp.emp_type) == false ? ' ' : temp.emp_type;
			html += ",";
			html += _logValidation(temp.person_type) == false ? ' ' : temp.person_type;
			html += ",";
			html += _logValidation(temp.onsite_offshore) == false ? ' ' : temp.onsite_offshore.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.billing_type) == false ? ' ' : temp.billing_type.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.proj_category) == false ? ' ' : temp.proj_category.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.proj_services) == false ? ' ' : temp.proj_services.replace(/[,.\t\n\r\f\v]/g , '');	
			html += ",";
			html += _logValidation(temp.s_subsidiary) == false ? ' ' : temp.s_subsidiary.replace(/[,.\t\n\r\f\v]/g , '');
			html += ",";
			html += _logValidation(temp.s_account_name) == false ? ' ' : temp.s_account_name;
			html += ",";
			html += _logValidation(temp.s_account_name_merge) == false ? ' ' : temp.s_account_name_merge; //
			html += ",";
			html += _logValidation(temp.cost_head) == false ? ' ' : temp.cost_head;
			html += ",";
			html += _logValidation(temp.cost_head_detailed) == false ? ' ' : temp.cost_head_detailed;
			html += ",";
			html += _logValidation(temp.tb_reco) == false ? ' ' : temp.tb_reco;
			html += ",";
			html += _logValidation(temp.type_revised) == false ? ' ' : temp.type_revised;   //amount_foreign
			html += ",";
			html += _logValidation(temp.revised_name) == false ? ' ' : temp.revised_name.replace(/[,.\t\n\r\f\v]/g , '');   //amount_foreign
			html += ",";
			html += _logValidation(temp.mis_practice) == false ? ' ' : temp.mis_practice;   //amount_foreign
			html += "\r\n";
		}
		
		/*strVar_excel += '</table>';
		strVar_excel += ' </td>';	
		strVar_excel += '	</tr>';
		
		strVar_excel += '</table>';*/
	
		var fileName = 'P&L File For Month '+s_month+'-'+s_year + '.csv';
	
	 
		
		var file = nlapiCreateFile(fileName, 'CSV', html);
		file.setFolder('354237');  //Prod
		 var fileID = nlapiSubmitFile(file);
		 nlapiLogExecution('debug','File ID',fileID);
		 
		return fileID;
	}
	catch(err){
		nlapiLogExecution('DEBUG','Excel creation error',err);
	}	
}
//Search for transactions
function searchTransactions_external_with_Projects(s_month_start,s_month_end){

	
try{
	var context = nlapiGetContext();
	var a_project_list = new Array();

	var s_selected_project_name = '';

	
var search = nlapiLoadSearch('transaction', 2450);
var filters = search.getFilters();

//Search for exchange Rate
var f_rev_curr = 0;
var f_cost_curr = 0;
var filters = [];

//var filters = [];
//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
var a_dataVal = {};
var dataRows = [];

var column = new Array();
column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
//column[4] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');
column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
column[6] = new nlobjSearchColumn('custrecord_eur_usd_conv_rate');
column[7] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');

var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
if (currencySearch) {
    for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {

        a_dataVal = {
            s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
            i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
            rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
            cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
            gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_gbp_converstion_rate'),
            crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
            nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate'),
             eur_conv_rate : currencySearch[i_indx].getValue('custrecord_eur_usd_conv_rate')


        };
        if (_logValidation(a_dataVal))
            dataRows.push(a_dataVal);


    }
}




var project_search_results = getTaggedProjectList();

// create html for select using the project data
var s_project_options = '';
var a_selected_project_list = '';
var s_project_number = '';
var s_project_name = '';
for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
    var i_project_id = project_search_results[i].getValue('internalid');
    s_project_number = project_search_results[i]
        .getValue('entityid');
    s_project_name = project_search_results[i].getValue('companyname');



    if (_logValidation(s_project_number)) {
        a_project_list.push(s_project_number);
    }

 }

if (a_selected_project_list == null ||
    a_selected_project_list.length == 0) {
    if (a_project_list.length > 0) {
        a_selected_project_list = a_project_list;

        showAll = true;
    } else {
        a_selected_project_list = new Array();
    }
}
nlapiLogExecution('DEBUG','a_selected_project_list - Before Duplicate',a_selected_project_list.length);
a_selected_project_list = removearrayduplicate(a_selected_project_list);
nlapiLogExecution('DEBUG','a_selected_project_list - After Duplicate',a_selected_project_list.length);
if (a_selected_project_list != null &&
    a_selected_project_list.length != 0) {
    var s_formula = '';

    for (var i = 0; i < a_selected_project_list.length; i++) {
        if (i != 0) {
            s_formula += " OR";
        }

        s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
            a_selected_project_list[i] + "' ";
    }

    var projectFilter = new nlobjSearchFilter('formulanumeric', null,
        'equalto', 1);

    projectFilter.setFormula("CASE WHEN " + s_formula +
        " THEN 1 ELSE 0 END");

    filters = filters.concat([projectFilter]);
} else {
    var projectFilter = new nlobjSearchFilter('formulanumeric', null,
        'equalto', 1);

    projectFilter.setFormula("0");

    filters = filters.concat([projectFilter]);
}

var s_from = s_month_start;
var s_to_ = s_month_end;
filters = filters.concat([new nlobjSearchFilter('trandate', null,
    'within', s_from, s_to_)]);

var columns = search.getColumns();

columns[0].setSort(false);
columns[3].setSort(true);
columns[9].setSort(false);
filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
filters.push(new nlobjSearchFilter('custcol_proj_category_on_a_click',null,'isnot','Bench'));

filters = filters.concat([new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
    'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
])]);

var search_results = searchRecord('transaction', 2450, filters, [
    columns[0], columns[1], columns[2], columns[3], columns[4],columns[5],
    columns[6],columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], 
    columns[16], columns[17], columns[18], columns[19], columns[20],columns[21],columns[22],columns[23],columns[24],columns[25],
    columns[26],columns[27],columns[28],columns[29],columns[30],columns[31],columns[32],columns[33],columns[34],columns[35],columns[36],
    columns[37],columns[38],columns[39],columns[40],columns[41]
]);



// Get the Facility_Cost

var s_facility_cost_filter = '';

for (var i = 0; i < a_selected_project_list.length; i++) {
    s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";

    if (i != a_selected_project_list.length - 1) {
        s_facility_cost_filter += ",";
    }
}

var facility_cost_filters = new Array();
/*facility_cost_filters[0] = new nlobjSearchFilter('formulanumeric',
    null, 'equalto', 1);
if (s_facility_cost_filter != '') {
    facility_cost_filters[0]
        .setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN (' +
            s_facility_cost_filter + ') THEN 1 ELSE 0 END');
} else {
    facility_cost_filters[0].setFormula('0');
}

var facility_cost_columns = new Array();
facility_cost_columns[0] = new nlobjSearchColumn(
    'custrecord_arpm_period');
facility_cost_columns[1] = new nlobjSearchColumn(
    'custrecord_arpm_num_allocated_resources');
facility_cost_columns[2] = new nlobjSearchColumn(
    'custrecord_arpm_facility_cost_per_person');
facility_cost_columns[3] = new nlobjSearchColumn(
    'custrecord_arpm_location');*/

// var facility_cost_search_results = searchRecord(
//      'customrecord_allocated_resources_per_mon', null,
//     facility_cost_filters, facility_cost_columns);

var o_json = new Object();

/*var o_data = {
    'Revenue': [],
    'Discount': [],
    'People_Cost_Employee': [],
    'People_Cost_Contractor': [],
    'Facility_Cost': [],
    'Other_Cost_Travel': [],
    'Other_Cost_Immigration': [],
    'Other_Cost_Professional_Fees': [],
    'Other_Cost_Others': []

};*/

var o_data = new Array();

var new_object = null;

var s_period = '';

var a_period_list = [];

var a_category_list = [];
a_category_list[0] = '';
var a_group = [];

var a_income_group = [];
var j_other_list = {};
var other_list = [];

var col = new Array();
col[0] = new nlobjSearchColumn('custrecord_account_name');
col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
var oth_fil = new Array();
oth_fil[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
var other_Search = nlapiSearchRecord('customrecord_pl_other_costs', null, oth_fil, col);
if (other_Search) {
    for (var i_ind = 0; i_ind < other_Search.length; i_ind++) {
        var acctname = other_Search[i_ind].getValue('custrecord_account_name');

        a_category_list.push(acctname);


        j_other_list = {
            o_acct: other_Search[i_ind].getValue('custrecord_account_name'),
            o_type: other_Search[i_ind].getText('custrecord_type_of_cost')
        };

        other_list.push(j_other_list);


    }
}
//Search for revised client name
var revised_list = [];
var a_customer_id_array =[];
var json_revised = {};
var col_revised = new Array();
col_revised[0] = new nlobjSearchColumn('custrecord_revised_client_cust_id');
col_revised[1] = new nlobjSearchColumn('custrecord_revised_client_name');
var oth_fil_revised = new Array();
oth_fil_revised[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
var other_Search_revised = nlapiSearchRecord('customrecord_revised_client_name_pl', null, oth_fil_revised, col_revised);
if (other_Search_revised) {
    for (var i_indx = 0; i_indx < other_Search_revised.length; i_indx++) {
        var s_cust_id = other_Search_revised[i_indx].getValue('custrecord_revised_client_cust_id');

        a_customer_id_array.push(s_cust_id.trim());


        json_revised = {
            o_cust_id: other_Search_revised[i_indx].getValue('custrecord_revised_client_cust_id').trim(),
            o_rev_name: other_Search_revised[i_indx].getValue('custrecord_revised_client_name')
        };

        revised_list.push(json_revised);


    }
}

//Search for pl data mapping
var account_array = [];
var json_list = {};
var dataMapping = [];
var cols_map = new Array();
cols_map[0] = new nlobjSearchColumn('custrecord_pl_data_map_account');
cols_map[1] = new nlobjSearchColumn('custrecord_pl_data_map_cost_head');
cols_map[2] = new nlobjSearchColumn('custrecord_pl_data_map_cost_detailed');
var oth_fil_map = new Array();
oth_fil_map[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
var pl_data_mapping_search = nlapiSearchRecord('customrecord_pl_data_mapping', null, oth_fil_map, cols_map);
if (pl_data_mapping_search) {
    for (var i_indxx = 0; i_indxx < pl_data_mapping_search.length; i_indxx++) {
        var acctname_ = pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_account');

        account_array.push(acctname_);


        json_list = {
            account_name: pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_account'),
            cost_head: pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_cost_head'),
            cost_head_detailed: pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_cost_detailed')
        };

        dataMapping.push(json_list);


    }
}
nlapiLogExecution('DEBUG','External Project Count',search_results.length);
for (var i = 0; search_results != null && i < search_results.length; i++) { //search_results.length
    var period = search_results[i].getText(columns[0]);
    var s_account_name = search_results[i].getValue(columns[11]);
    
    if(parseInt(s_account_name) == parseInt(978)){
    	//nlapiLogExecution('DEBUG','Cost Head');
    }
    var ex_rate = '';
    var amount = '';
    var account_name = '';
    var cost_head = '';
    var cost_head_detailed = '';
    //Code updated by Deepak, Dated - 21 Mar 17
    var s_month_year = period.split(' ');
    var s_mont = s_month_year[0];
    s_mont = getMonthCompleteName(s_mont);
    var s_year_ = s_month_year[1];
    var f_revRate = 65.65;
    var f_costRate = 65.0;
    var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate,f_eur_conv_rate;
    
//Fetch for PL Mapping Data.. Cost head details
    var o_index_ = account_array.indexOf(s_account_name);
    if (o_index_ != -1) {
    	o_index_ = o_index_;
        account_name = dataMapping[o_index_].account_name;
        cost_head = dataMapping[o_index_].cost_head;
        cost_head_detailed = dataMapping[o_index_].cost_head_detailed;
        
    }
    //Fetch matching cost and rev rate convertion rate
    for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
        if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
            f_revRate = dataRows[data_indx].rev_rate;
            f_costRate = dataRows[data_indx].cost_rate;
            f_gbp_rev_rate = dataRows[data_indx].gbp_rev_rate;
            f_crc_cost_rate = dataRows[data_indx].crc_cost;
            f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
			f_eur_conv_rate = dataRows[data_indx].eur_conv_rate;
        }
    }
   
    
    var transaction_type = search_results[i].getText(columns[8]);

    var transaction_date = search_results[i].getValue(columns[9]);

    var i_subsidiary = search_results[i].getValue(columns[10]);
    
    var s_subsidiary = search_results[i].getText(columns[10]);

    var amount = parseFloat(search_results[i].getValue(columns[1]));
	
	 var amount_ForeignCur = parseFloat(search_results[i].getValue(columns[1]));

    var category = search_results[i].getValue(columns[3]);

   

    var currency = search_results[i].getValue(columns[12]);
    
    var currency_text = search_results[i].getText(columns[12]);

    var exhangeRate = search_results[i].getValue(columns[13]);
    //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

    var s_project_description = search_results[i].getValue(columns[16]);
    var s_customer = search_results[i].getValue(columns[17]);
    var s_revised_name = '';
    var cust_ID = search_results[i].getValue(columns[23]);
    if(_logValidation(cust_ID)){
    	var index_ = a_customer_id_array.indexOf(cust_ID.trim());
    	 if (index_ != -1) {
    		 index_ = index_;
    		 s_revised_name = revised_list[index_].o_rev_name;
    }
    }
    var project_ID = '';
    var projectName = '';
    var s_subPractice = '';
    var s_parentPractice = '';
    var projectSearch = '';
    if (_logValidation(s_project_description)) {
        project_ID = s_project_description.split(' ')[0];
        projectName = s_project_description.split(' ')[1];
        s_project_number = project_ID;
        //s_project_name =  projectName;
    }
    var s_practice = search_results[i].getText(columns[14]);
    s_subPractice = s_practice;
    s_parentPractice = search_results[i].getText(columns[15]);
    if (_logValidation(s_practice))
        s_parentPractice = s_practice.split(':')[0];

  //  nlapiLogExecution('audit', 'accnt name:- ' + s_account_name);

   /* if ((currency == parseInt(2)) && (parseInt(i_subsidiary) != parseInt(2))) {
        amount = parseFloat(amount) * parseFloat(exhangeRate);

    }
    if ((currency == parseInt(4)) && (parseInt(i_subsidiary) != parseInt(2))) {
        amount = parseFloat(amount) * parseFloat(exhangeRate);

    } */
	var flag = false;
	if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
	{
		amount= parseFloat(amount)* parseFloat(exhangeRate);
		amount = parseFloat(amount) /parseFloat(f_revRate);
		flag = true;
		
	}
	var eur_to_inr = 77.25;
	
	//AMount Convertion Logic
	if((parseInt(i_subsidiary) != parseInt(2))&& (currency!= parseInt(1))){
	if (category != 'Revenue' && category != 'Other Income'
        && category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
			if(currency== parseInt(9))
			{
				amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
			}
			else if(currency == parseInt(8)){
			amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
			}
			
			else{
	
		amount= parseFloat(amount)* parseFloat(exhangeRate);		
		amount = parseFloat(amount) /parseFloat(f_costRate);	
		}
		
	} else if(parseInt(currency) == parseInt(4) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary
		amount= parseFloat(amount) / parseFloat(f_eur_conv_rate);
		//amount = parseFloat(amount) /parseFloat(f_revRate);
		
	//f_total_revenue += o_data[s_category][j].amount;
	}
	else if(parseInt(currency) == parseInt(2) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary
		amount= parseFloat(amount) / parseFloat(f_gbp_rev_rate);
		//amount = parseFloat(amount) /parseFloat(f_revRate);
		
	//f_total_revenue += o_data[s_category][j].amount;
	}
	else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
		amount= parseFloat(amount)* parseFloat(exhangeRate);
		amount = parseFloat(amount) /parseFloat(f_revRate);
		
	//f_total_revenue += o_data[s_category][j].amount;
	}
}	//END

    var isWithinDateRange = true;

    var d_transaction_date = nlapiStringToDate(transaction_date,
        'datetimetz');

    if (s_from != '') {
        var d_from = nlapiStringToDate(s_from, 'datetimetz');

        if (d_transaction_date < d_from) {
            isWithinDateRange = false;
        }
    }

    if (s_to_ != '') {
        var d_to = nlapiStringToDate(s_to_, 'datetimetz');

        if (d_transaction_date > d_to) {
            isWithinDateRange = false;
        }
    }

    if (isWithinDateRange == true) {
        var i_index = a_period_list.indexOf(period);

        if (i_index == -1) {
            if (_logValidation(period)) {
                a_period_list.push(period);
            } else {
                //nlapiLogExecution('debug', 'error', '388');
            }

        }

        i_index = a_period_list.indexOf(period);
    }
    //Remove Spaces
    //var memo_m = search_results[i].getValue(columns[31]);
   // nlapiLogExecution('DEBUG','memo_m',memo_m);
    
    if (category != 'People Cost' && category != 'Discount' && category != 'Facility Cost' && category != 'Revenue') {
        var o_index = a_category_list.indexOf(s_account_name);
        if (o_index != -1) {
            o_index = o_index - 1;
            var acct = other_list[o_index].o_acct;
            var typeofact = other_list[o_index].o_type;


            if ((_logValidation(period)) && (_logValidation(amount)) && (_logValidation(search_results[i].getValue(columns[4]))) &&
                (_logValidation(search_results[i].getValue(columns[5]))) && (_logValidation(transaction_type)) &&
                (_logValidation(transaction_date)) && (_logValidation(isWithinDateRange)) && (_logValidation(s_account_name))) {} else {
                //  nlapiLogExecution('debug','error','408');
            }
            if (typeofact == 'Other Direct Cost - Travel') {
                o_data.push({
                	'project_id': s_project_number, //1
                    'period': period,               //2
                    'amount': parseFloat(amount).toFixed(2), //3
                    'num': search_results[i].getValue(columns[4]), //4
                    'sub_practice': s_subPractice.trim(), //5
                    'practice': s_parentPractice.trim(), //6
                    'type': transaction_type, //7
                    'dt': transaction_date,  //8
                    's_customer': s_customer,  //9
                    's_region': search_results[i].getText(columns[20]), //10
                    's_subsidiary': s_subsidiary,  //11
                    's_account_name': 'Other Direct Cost - Travel', //12
                    
                    'account': search_results[i].getText(columns[11]), //13
                    'bill_from': search_results[i].getValue(columns[21]), //14
                    'bill_to': search_results[i].getValue(columns[22]), //15
                    'pro_name': search_results[i].getValue(columns[26]), //16
                    'pro_description': search_results[i].getValue(columns[16]), //17
                    'cust_id': search_results[i].getValue(columns[23]),  //18
                    'cust_name': search_results[i].getValue(columns[24]),  //19
                    'emp_id': search_results[i].getValue(columns[27]),  //20
                    'emp_name': search_results[i].getValue(columns[28]),  //21
                    'location': search_results[i].getText(columns[29]), //22
                    'memo_main': search_results[i].getValue(columns[30]),  //23
                    'memo_line': search_results[i].getValue(columns[31]),  //24
                    'description': search_results[i].getValue(columns[32]), //25
                    'hour': search_results[i].getValue(columns[33]),        //26
                    'bill_rate': search_results[i].getValue(columns[34]),    //27
                    'emp_type': search_results[i].getValue(columns[35]),     //28
                    'person_type': search_results[i].getValue(columns[36]),  //29
                    'onsite_offshore': search_results[i].getValue(columns[37]), //30
                    'billing_type': search_results[i].getValue(columns[38]), //31
                    'proj_category': search_results[i].getValue(columns[39]), //32
                    'tb_reco': 'Considered for Regional',
                    'proj_services': search_results[i].getText(columns[40]),
                    'mis_practice': search_results[i].getValue(columns[41]),
                    'cost_head': cost_head,
                    'cost_head_detailed': cost_head_detailed,
                    'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
					's_account_name_merge': 'Other Direct Cost' //12
                    
                });
            } else if (typeofact == 'Other Direct Cost - Immigration') {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'Other Direct Cost - Immigration',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                        'proj_services': search_results[i].getText(columns[40]),
                        'mis_practice': search_results[i].getValue(columns[41]),
                        'cost_head': cost_head,
                        'cost_head_detailed': cost_head_detailed,
                        'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
						'revised_name': s_revised_name,
                        's_account_name_merge': 'Other Direct Cost'	
                });
            }
            if (typeofact == 'Other Direct Cost - Professional Fees') {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'Other Direct Cost - Professional Fees',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'Other Direct Cost'	
                });
            }
            if (typeofact == 'Other Direct Cost - Others') {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'Other Direct Cost - Others',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'Other Direct Cost'	
                });
            }

        }


    } else {

        if (parseInt(s_account_name) == parseInt(580)) { // Bad Debts
            if (parseFloat(amount) < 0) {
                amount = Math.abs(parseFloat(amount));
            } else {
                amount = '-' + amount;
            }
        }

        //avoiding discounts from account						  
        if (category == 'Revenue' && i_subsidiary == parseInt(2)) {
            if (s_account_name != parseInt(732)) {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'Gross Revenue',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'Gross Revenue'	
                });
                //nlapiLogExecution('audit', 'subs:- ' + i_subsidiary);
                //nlapiLogExecution('audit','account Revenue:- '+s_account_name); 'People_Cost_Employee' : [],
                //'People_Cost_Contractor' : [],
                //  nlapiLogExecution('audit','trnsactnnum:'+search_results[i].getValue(columns[4]));

            }
            //nlapiLogExecution('audit','subs:- '+i_subsidiary);
            //nlapiLogExecution('audit','account Revenue:- '+s_account_name);
        } else if (category == 'Revenue') {
             o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'Gross Revenue',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'Gross Revenue'		
                });
        }
        if (category == 'Discount') {

            o_data.push({
                'project_id': s_project_number,
                'period': period,
               'amount': parseFloat(amount).toFixed(2),
                'num': search_results[i].getValue(columns[4]),
                'sub_practice': s_subPractice.trim(),
                'practice': s_parentPractice.trim(),
                'type': transaction_type,
                'dt': transaction_date,
                's_customer': s_customer,
                's_region': search_results[i].getText(columns[20]),
                's_subsidiary': s_subsidiary,
                's_account_name': 'Discount',
                	'bill_from': search_results[i].getValue(columns[21]),
                    'bill_to': search_results[i].getValue(columns[22]),
                	'account': search_results[i].getText(columns[11]),
                    'pro_name': search_results[i].getValue(columns[26]),
                    'pro_description': search_results[i].getValue(columns[16]),
                    'cust_id': search_results[i].getValue(columns[23]),
                    'cust_name': search_results[i].getValue(columns[24]),
                    'emp_id': search_results[i].getValue(columns[27]),
                    'emp_name': search_results[i].getValue(columns[28]),
                    'location': search_results[i].getText(columns[29]),
                    'memo_main': search_results[i].getValue(columns[30]),
                    'memo_line': search_results[i].getValue(columns[31]),
                    'description': search_results[i].getValue(columns[32]),
                    'hour': search_results[i].getValue(columns[33]),
                    'bill_rate': search_results[i].getValue(columns[34]),
                    'emp_type': search_results[i].getValue(columns[35]),
                    'person_type': search_results[i].getValue(columns[36]),
                    'onsite_offshore': search_results[i].getValue(columns[37]),
                    'billing_type': search_results[i].getValue(columns[38]),
                    'proj_category': search_results[i].getValue(columns[39]),
                    'tb_reco': 'Considered for Regional',
                   'proj_services': search_results[i].getText(columns[40]),
                   'mis_practice': search_results[i].getValue(columns[41]),
                   'cost_head': cost_head,
                   'cost_head_detailed': cost_head_detailed,
                   'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                   's_account_name_merge': 'Discount'			
            });

        } 
            if (parseInt(s_account_name) == parseInt(518) || parseInt(s_account_name) == parseInt(541)) {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'PeopleCost - Contractor',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'People Cost'	
                });
            }
            if (parseInt(s_account_name) == parseInt(978)) {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'PeopleCost - Employee',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'People Cost'	
                });
            }

            if (category == 'Facility Cost' || parseInt(s_account_name) == parseInt(1058)) {
                o_data.push({
                    'project_id': s_project_number,
                    'period': period,
                   'amount': parseFloat(amount).toFixed(2),
                    'num': search_results[i].getValue(columns[4]),
                    'sub_practice': s_subPractice.trim(),
                    'practice': s_parentPractice.trim(),
                    'type': transaction_type,
                    'dt': transaction_date,
                    's_customer': s_customer,
                    's_region': search_results[i].getText(columns[20]),
                    's_subsidiary': s_subsidiary,
                    's_account_name': 'Offshore Facility Cost',
                    	'bill_from': search_results[i].getValue(columns[21]),
                        'bill_to': search_results[i].getValue(columns[22]),
                    	'account': search_results[i].getText(columns[11]),
                        'pro_name': search_results[i].getValue(columns[26]),
                        'pro_description': search_results[i].getValue(columns[16]),
                        'cust_id': search_results[i].getValue(columns[23]),
                        'cust_name': search_results[i].getValue(columns[24]),
                        'emp_id': search_results[i].getValue(columns[27]),
                        'emp_name': search_results[i].getValue(columns[28]),
                        'location': search_results[i].getText(columns[29]),
                        'memo_main': search_results[i].getValue(columns[30]),
                        'memo_line': search_results[i].getValue(columns[31]),
                        'description': search_results[i].getValue(columns[32]),
                        'hour': search_results[i].getValue(columns[33]),
                        'bill_rate': search_results[i].getValue(columns[34]),
                        'emp_type': search_results[i].getValue(columns[35]),
                        'person_type': search_results[i].getValue(columns[36]),
                        'onsite_offshore': search_results[i].getValue(columns[37]),
                        'billing_type': search_results[i].getValue(columns[38]),
                        'proj_category': search_results[i].getValue(columns[39]),
                        'tb_reco': 'Considered for Regional',
                       'proj_services': search_results[i].getText(columns[40]),
                       'mis_practice': search_results[i].getValue(columns[41]),
                       'cost_head': cost_head,
                       'cost_head_detailed': cost_head_detailed,
                       'type_revised': 'Direct',
					'amount_foreign': amount_ForeignCur,
					'currency_text': currency_text, //f_revRate
					//'exchange_rate': ex_rate,
					'revised_name': s_revised_name,
                       's_account_name_merge': 'Offshore Facility Cost'	
                });
            }

            //nlapiLogExecution('audit','cat:- '+category);
        
    }
}




return o_data;
}
catch(ex){
nlapiLogExecution('DEBUG','Summary Error',ex);	
}
}
//Search For Internal Projects
function searchTransactions_Internal_with_Projects(s_month_start_,s_month_end_){
	 try {
	        //var str = request.getBody();

	        var context = nlapiGetContext();

	        //	var i_user_id = nlapiGetUser();

	        var a_project_list = new Array();

	        var s_selected_project_name = '';

	         // var d_today = nlapiDateToString(new Date());
	       /* var d_today = '7/3/2018';
	        d_today = nlapiStringToDate(d_today);


	        var d_day = nlapiAddMonths(d_today, -1);
	        nlapiLogExecution('debug', 'd_day', d_day);
	        var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
	        var s_month_start = d_month_start.getMonth() + 1 + '/' +
	            d_month_start.getDate() + '/' + d_month_start.getFullYear();
	        var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
	        var s_month_end = d_month_end.getMonth() + 1 + '/' +
	            d_month_end.getDate() + '/' + d_month_end.getFullYear();*/

	        var showAll = false;

	        var s_from = '';
	        var s_to = '';




	        var search = nlapiLoadSearch('transaction', 2421);
	        var filters = search.getFilters();

	        //Search for exchange Rate
	        var f_rev_curr = 0;
	        var f_cost_curr = 0;
	        var filters = [];

	        //var filters = [];
	        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
	        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
	        var a_dataVal = {};
	        var dataRows = [];

	        var column = new Array();
	        column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
	        column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
	        column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
	        column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
	        //column[4] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
	        column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
	        column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
	        column[6] = new nlobjSearchColumn('custrecord_eur_usd_conv_rate');

	        var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
	        if (currencySearch) {
	            for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {

	                a_dataVal = {
	                    s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
	                    i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
	                    rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
	                    cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
	                    //gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_revenue'),
	                    crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
	                    nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate'),
	                    eur_conv_rate : currencySearch[i_indx].getValue('custrecord_eur_usd_conv_rate')


	                };
	                if (_logValidation(a_dataVal))
	                    dataRows.push(a_dataVal);


	            }
	        }




	        var project_search_results = getTaggedProjectList_Internal();

	        // create html for select using the project data
	        var s_project_options = '';
	        var a_selected_project_list = '';
	        var s_project_number = '';
	        var s_project_name = '';
	        for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
	            var i_project_id = project_search_results[i].getValue('internalid');
	            s_project_number = project_search_results[i]
	                .getValue('entityid');
	            s_project_name = project_search_results[i].getValue('companyname');



	            if (_logValidation(s_project_number)) {
	                a_project_list.push(s_project_number);
	            }


	            /*var s_selected = '';
	            if ((a_selected_project_list != null && a_selected_project_list
	                    .indexOf(s_project_number) != -1)
	                    || showAll == true) {
	            	s_selected = ' selected="selected" ';

	            	if (s_selected_project_name != '') {
	            		s_selected_project_name += ', ';
	            	}

	            	s_selected_project_name += s_project_number + ' '
	            	        + s_project_name;
	            }

	            s_project_options += '<option value="' + s_project_number + '" '
	                    + s_selected + '>' + s_project_number + ' '
	                    + s_project_name + '</option>';*/
	        }

	        if (a_selected_project_list == null ||
	            a_selected_project_list.length == 0) {
	            if (a_project_list.length > 0) {
	                a_selected_project_list = a_project_list;

	                showAll = true;
	            } else {
	                a_selected_project_list = new Array();
	            }
	        }

	        if (a_selected_project_list != null &&
	            a_selected_project_list.length != 0) {
	            var s_formula = '';

	            for (var i = 0; i < a_selected_project_list.length; i++) {
	                if (i != 0) {
	                    s_formula += " OR";
	                }

	                s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
	                    a_selected_project_list[i] + "' ";
	            }

	            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	                'equalto', 1);

	            projectFilter.setFormula("CASE WHEN " + s_formula +
	                " THEN 1 ELSE 0 END");

	            filters = filters.concat([projectFilter]);
	        } else {
	            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	                'equalto', 1);

	            projectFilter.setFormula("0");

	            filters = filters.concat([projectFilter]);
	        }

	        var s_from = s_month_start_;
	        var s_to_ = s_month_end_;
	        filters = filters.concat([new nlobjSearchFilter('trandate', null,
	            'within', s_from, s_to_)]);

	        var columns = search.getColumns();

	        columns[0].setSort(false);
	        columns[3].setSort(true);
	        columns[9].setSort(false);
	        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
	        filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
	        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
	        //  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));

	        filters.push(new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
	            'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
	        ]));

	        var search_results = searchRecord('transaction', 2421, filters, [
					  columns[0], columns[1], columns[2], columns[3], columns[4],columns[5],
					columns[6],columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], 
					columns[16], columns[17], columns[18], columns[19], columns[20],columns[21],columns[22],columns[23],columns[24],columns[25],
					columns[26],columns[27],columns[28],columns[29],columns[30],columns[31],columns[32],columns[33],columns[34],columns[35],columns[36],
					columns[37],columns[38],columns[39],columns[40],columns[41],columns[42]
	        ]);


	        var o_json = new Object();
	        var o_data = new Array();
	       /* var o_data = {

	            'Account_Management_Salary_Cost': [],
	            'Account_Management_Other_Cost': [],
	            'Regional_Management_Salary_Cost': [],
	            'Regional_Management_Other_Cost': [],
	            'Horizontal_Management_onsite_Salary_Cost': [],
	            'Horizontal_Management_onsite_Other_Cost': [],
	            'Horizontal_Management_offshore_Salary_Cost': [],
	            'Horizontal_Management_offshore_Other_Cost': [],
	            'Horizontal_Investments_Salary_Cost': [],
	            'Horizontal_Investments_Other_Cost': [],
	            'Horizontal_Dedicated_Resources_Salary_Cost': [],
	            'Horizontal_Dedicated_Resources_Other_Cost': [],


	        };*/

	        var new_object = null;

	        var s_period = '';

	        var a_period_list = [];

	        var a_category_list = [];
	        a_category_list[0] = '';
	        var a_group = [];

	        var a_income_group = [];
	        var j_other_list = {};
	        var other_list = [];

	        var col = new Array();
	        col[0] = new nlobjSearchColumn('custrecord_account_name');
	        col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
	        var oth_fil = new Array();
	        oth_fil[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	        var other_Search = nlapiSearchRecord('customrecord_pl_other_costs', null, oth_fil, col);
	        if (other_Search) {
	            for (var i_ind = 0; i_ind < other_Search.length; i_ind++) {
	                var acctname = other_Search[i_ind].getValue('custrecord_account_name');

	                a_category_list.push(acctname);


	                j_other_list = {
	                    o_acct: other_Search[i_ind].getValue('custrecord_account_name'),
	                    o_type: other_Search[i_ind].getText('custrecord_type_of_cost')
	                };

	                other_list.push(j_other_list);


	            }
	        }
	        
	      //Search for revised client name
	        var revised_list = [];
	        var a_customer_id_array =[];
	        var json_revised = {};
	        var col_revised = new Array();
	        col_revised[0] = new nlobjSearchColumn('custrecord_revised_client_cust_id');
	        col_revised[1] = new nlobjSearchColumn('custrecord_revised_client_name');
	        var oth_fil_revised = new Array();
	        oth_fil_revised[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	        var other_Search_revised = nlapiSearchRecord('customrecord_revised_client_name_pl', null, oth_fil_revised, col_revised);
	        if (other_Search_revised) {
	            for (var i_indx = 0; i_indx < other_Search_revised.length; i_indx++) {
	                var s_cust_id = other_Search_revised[i_indx].getValue('custrecord_revised_client_cust_id');

	                a_customer_id_array.push(s_cust_id.trim());


	                json_revised = {
	                    o_cust_id: other_Search_revised[i_indx].getValue('custrecord_revised_client_cust_id').trim(),
	                    o_rev_name: other_Search_revised[i_indx].getValue('custrecord_revised_client_name')
	                };

	                revised_list.push(json_revised);


	            }
	        }
	      //Search for pl data mapping
	        var account_array = [];
	        var json_list = {};
	        var dataMapping = [];
	        var cols_map = new Array();
	        cols_map[0] = new nlobjSearchColumn('custrecord_pl_data_map_int_account');
	        cols_map[1] = new nlobjSearchColumn('custrecord_pl_data_map_int_cost_head');
	        cols_map[2] = new nlobjSearchColumn('custrecord_pl_map_int_cost_detail');
	        var oth_filt_map = new Array();
	        oth_filt_map[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	        var pl_data_mapping_search = nlapiSearchRecord('customrecord_pl_data_map_internal', null, oth_filt_map, cols_map);
	        if (pl_data_mapping_search) {
	            for (var i_indxx = 0; i_indxx < pl_data_mapping_search.length; i_indxx++) {
	                var acctname_ = pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_int_account');

	                account_array.push(acctname_);


	                json_list = {
	                    account_name: pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_int_account'),
	                    cost_head: pl_data_mapping_search[i_indxx].getValue('custrecord_pl_data_map_int_cost_head'),
	                    cost_head_detailed: pl_data_mapping_search[i_indxx].getValue('custrecord_pl_map_int_cost_detail')
	                };

	                dataMapping.push(json_list);


	            }
	        }
	        nlapiLogExecution('DEBUG','Internal Project Count',search_results.length);
	        for (var i = 0; search_results != null && i < search_results.length; i++) {
	        	
	        	if(search_results[i].getValue(columns[42]) == 'T'){
	        	
	            var period = search_results[i].getText(columns[0]);
	            var s_account_name = search_results[i].getValue(columns[11]);
	            var ex_rate = '';
	            var amount = '';
	            var account_name = '';
	            var cost_head = '';
	            var cost_head_detailed = '';
	            //Code updated by Deepak, Dated - 21 Mar 17
	            var s_month_year = period.split(' ');
	            var s_mont = s_month_year[0];
	            s_mont = getMonthCompleteName(s_mont);
	            var s_year_ = s_month_year[1];
	            var f_revRate = 63.65;
	            var f_costRate = 66.0;
	            var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate;
	            //	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
	            //	nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
	            //nlapiLogExecution('audit','accnt name:- '+rate);
	            
	          //Fetch for PL Mapping Data.. Cost head details
	            var o_index_ = account_array.indexOf(s_account_name);
	            if (o_index_ != -1) {
	            	o_index_ = o_index_;
	                account_name = dataMapping[o_index_].account_name;
	                cost_head = dataMapping[o_index_].cost_head;
	                cost_head_detailed = dataMapping[o_index_].cost_head_detailed;
	                
	            }

	            //Fetch matching cost and rev rate convertion rate
	            for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
	                if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
	                    f_revRate = dataRows[data_indx].rev_rate;
	                    f_costRate = dataRows[data_indx].cost_rate;
	                    //f_gbp_rev_rate=dataRows[data_indx].gbp_rev_rate;
	                    f_crc_cost_rate = dataRows[data_indx].crc_cost;
	                    f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
	                }
	            }
	            /*	if(f_gbp_rev_rate==0)
	            	{
	            	f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
	            	}*/

	            var transaction_type = search_results[i].getText(columns[8]);

	            var transaction_date = search_results[i].getValue(columns[9]);

	            var i_subsidiary = search_results[i].getValue(columns[10]);
	            var s_subsidiary = search_results[i].getText(columns[10]);
	            var amount = parseFloat(search_results[i].getValue(columns[1]));

				var amount_ForeignCur = parseFloat(search_results[i].getValue(columns[1]));
					
	            var category = search_results[i].getValue(columns[3]);

	            

	            var currency = search_results[i].getValue(columns[12]);
	            
	            var currency_text = search_results[i].getText(columns[12]);

	            var exhangeRate = search_results[i].getValue(columns[13]);
	            //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

	            var s_project_description = search_results[i].getValue(columns[16]);
	            var s_customer = search_results[i].getValue(columns[17]);
	            var s_Proj_category = search_results[i].getValue(columns[39]);
	            
	            var s_revised_name = '';
	            var cust_ID = search_results[i].getValue(columns[23]);
	            if(_logValidation(cust_ID)){
	            	var index_ = a_customer_id_array.indexOf(cust_ID.trim());
	            	 if (index_ != -1) {
	            		 index_ = index_;
	            		 s_revised_name = revised_list[index_].o_rev_name;
	            }
	            }
	            var project_ID = '';
	            var projectName = '';
	            var s_subPractice = '';
	            var s_parentPractice = '';
	            var projectSearch = '';
	            if (_logValidation(s_project_description)) {
	                project_ID = s_project_description.split(' ')[0];
	                projectName = s_project_description.split(' ')[1];
	                s_project_number = project_ID;
	                //s_project_name =  projectName;
	            }
	            var s_practice = search_results[i].getText(columns[14]);
	            s_subPractice = s_practice;
	            s_parentPractice = search_results[i].getText(columns[15]);
	            if (_logValidation(s_practice))
	                s_parentPractice = s_practice.split(':')[0];

	          //  nlapiLogExecution('audit', 'accnt name:- ' + s_account_name);

	            /*if ((currency == parseInt(2)) && (parseInt(i_subsidiary) != parseInt(2))) {
	                amount = parseFloat(amount) * parseFloat(exhangeRate);

	            }*/
	           /* var flag = false;
	            if ((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue') {
	                amount = parseFloat(amount) * parseFloat(exhangeRate);
	                amount = parseFloat(amount) / parseFloat(f_revRate);
	                flag = true;

	            }
	            //AMount Convertion Logic
	            if ((parseInt(i_subsidiary) != parseInt(2)) && (currency != parseInt(1))) {
	                if (category != 'Revenue' && category != 'Other Income' &&
	                    category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
	                    if (currency == parseInt(9)) {
	                        amount = parseFloat(amount) / parseFloat(f_crc_cost_rate);
	                    } else if (currency == parseInt(8)) {
	                        amount = parseFloat(amount) / parseFloat(f_nok_cost_rate);
	                    }
						else if (currency == parseInt(4)) {
							amount = parseFloat(amount) / parseFloat(f_eur_conv_rate);
												}						
						else {
	                        //f_total_expense += o_data[s_category][j].amount;
	                        amount = parseFloat(amount) / parseFloat(f_costRate);
	                    }

	                } else {

	                    amount = parseFloat(amount) / parseFloat(f_revRate);

	                    //f_total_revenue += o_data[s_category][j].amount;
	                }
	            } //END
*/	            //Copied from external projects converstion logic
	            var flag = false;
	        	if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
	        	{
	        		amount= parseFloat(amount)* parseFloat(exhangeRate);
	        		amount = parseFloat(amount) /parseFloat(f_revRate);
	        		flag = true;
	        		
	        	}
	        	var eur_to_inr = 77.25;
	        	
	        	//AMount Convertion Logic
	        	if((parseInt(i_subsidiary) != parseInt(2))&& (currency!= parseInt(1))){
	        	if (category != 'Revenue' && category != 'Other Income'
	                && category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
	        			if(currency== parseInt(9))
	        			{
	        				amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
	        			}
	        			else if(currency == parseInt(8)){
	        			amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
	        			}
	        			
	        			else{
	        	
	        		amount= parseFloat(amount)* parseFloat(exhangeRate);		
	        		amount = parseFloat(amount) /parseFloat(f_costRate);	
	        		}
	        		
	        	} else if(parseInt(currency) == parseInt(4) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary
	        		amount= parseFloat(amount) / parseFloat(f_eur_conv_rate);
	        		//amount = parseFloat(amount) /parseFloat(f_revRate);
	        		
	        	//f_total_revenue += o_data[s_category][j].amount;
	        	}
	        	else if(parseInt(currency) == parseInt(2) && parseInt(i_subsidiary) == parseInt(7)){  //UK Subsidiary
	        		amount= parseFloat(amount) / parseFloat(f_gbp_rev_rate);
	        		//amount = parseFloat(amount) /parseFloat(f_revRate);
	        		
	        	//f_total_revenue += o_data[s_category][j].amount;
	        	}
	        	else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
	        		amount= parseFloat(amount)* parseFloat(exhangeRate);
	        		amount = parseFloat(amount) /parseFloat(f_revRate);
	        		
	        	//f_total_revenue += o_data[s_category][j].amount;
	        	}
	        }	//END
				

	            var isWithinDateRange = true;

	            var d_transaction_date = nlapiStringToDate(transaction_date,
	                'datetimetz');

	            if (s_from != '') {
	                var d_from = nlapiStringToDate(s_from, 'datetimetz');

	                if (d_transaction_date < d_from) {
	                    isWithinDateRange = false;
	                }
	            }

	            if (s_to != '') {
	                var d_to = nlapiStringToDate(s_to, 'datetimetz');

	                if (d_transaction_date > d_to) {
	                    isWithinDateRange = false;
	                }
	            }

	            if (isWithinDateRange == true) {
	                var i_index = a_period_list.indexOf(period);

	                if (i_index == -1) {
	                    if (_logValidation(period)) {
	                        a_period_list.push(period);
	                    } else {
	                        //nlapiLogExecution('debug', 'error', '388');
	                    }

	                }

	                i_index = a_period_list.indexOf(period);
	            }
	            nlapiLogExecution('audit', 's_Proj_category:category' ,s_Proj_category+' '+category);
	            //Account Management
	            if (category == 'People Cost' && s_Proj_category == 'Account Management') {
	            	
	            	


	            	 o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12
	                    
	                });
	            } else if (s_Proj_category == 'Account Management') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12	
	                    
	                });

	            }
	            //Regional Management
	            if (category == 'People Cost' && s_Proj_category == 'Regional Management') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12	
	                 
	                });

	            } else if (s_Proj_category == 'Regional Management') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12	
	                 
	                });

	            }

	            //Horizontal Management Onsite
	            if (category == 'People Cost' && s_Proj_category == 'Horizontal Management Onsite') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12	
	                    
	                });




	            } else if (s_Proj_category == 'Horizontal Management Onsite') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12	
	                   
	                });

	            }
	            //Horizontal Management Offshore
	            if (category == 'People Cost' && s_Proj_category == 'Horizontal Management Offshore') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12	
	                    
	                });




	            } else if (s_Proj_category == 'Horizontal Management Offshore') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category+' '+'-'+' '+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': s_Proj_category //12
	                   
	                });

	            }
	           //Investment
	            if (category == 'People Cost' && s_Proj_category == 'Investment') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Horizontal Investments - Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': 'Horizontal Investments' //12
	                   
	                });




	            } else if (s_Proj_category == 'Investment') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Horizontal Investments - Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': 'Horizontal Investments' //12
	                    
	                });

	            }

	            //Horizontal Management Offshore
	            if (category == 'People Cost' && s_Proj_category == 'Bench') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Horizontal Dedicated Resources - Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': 'Horizontal Dedicated Resources' //12
	                   
	                });




	            } else if (s_Proj_category == 'Bench') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Horizontal Dedicated Resources - Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'mis_practice': search_results[i].getValue(columns[41]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
							'revised_name': s_revised_name,
	                       's_account_name_merge': 'Horizontal Dedicated Resources' //12
	                    
	                });

	            }
	        }


	        }
	        return o_data;
	        
	    } catch (err) {
	        nlapiLogExecution('Debug', 'RestLet Main ', err);
	    }
}
//Get start date
function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((parseInt(i_year) % 4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function getTaggedProjectList_Internal() {
    try {


        // get the list of project the user has access to
        /*var project_filter = [
                [[ 'custbody_projecttype', 'anyOf', 2 ], 'and' ,
                ['startdate',  'notafter',enDate]), 'and',
                ['enddate',  'notbefore',stDate]), 'and',		       
                [ 'status', 'anyOf', 2 ] ];*/

        var project_search_results = searchRecord('job', 2422, null, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("companyname")
        ]);



        if (project_search_results.length == 0) {
            throw "You don't have any projects under you.";
        }
        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}
//Get End Date
function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((parseInt(i_year) % 4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
//Blank Values
function _logValidation(value) 
{
 if(value!='null' && value != null && value != '- None -' && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function getTaggedProjectList() {
	try {
		

		// get the list of project the user has access to
		/*var project_filter = [
		        [[ 'custbody_projecttype', 'anyOf', 2 ], 'and' ,
		        ['startdate',  'notafter',enDate]), 'and',
		        ['enddate',  'notbefore',stDate]), 'and',		       
		        [ 'status', 'anyOf', 2 ] ];*/

		var project_search_results = searchRecord('job', 'customsearch2430', null,
		        [ new nlobjSearchColumn("entityid"),
		                new nlobjSearchColumn("altname"),
                 new nlobjSearchColumn("internalid"),
                new nlobjSearchColumn("companyname")]);

		

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}
		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}
//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}


function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
//Search For transactions internal without projects
//Search For Internal Projects
function searchTransactions_Internal_withOut_Projects(s_month_start_,s_month_end_){
	 try {
	        //var str = request.getBody();

	        var context = nlapiGetContext();

	        //	var i_user_id = nlapiGetUser();

	        var a_project_list = new Array();

	        var s_selected_project_name = '';

	         // var d_today = nlapiDateToString(new Date());
	       /* var d_today = '7/3/2018';
	        d_today = nlapiStringToDate(d_today);


	        var d_day = nlapiAddMonths(d_today, -1);
	        nlapiLogExecution('debug', 'd_day', d_day);
	        var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
	        var s_month_start = d_month_start.getMonth() + 1 + '/' +
	            d_month_start.getDate() + '/' + d_month_start.getFullYear();
	        var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
	        var s_month_end = d_month_end.getMonth() + 1 + '/' +
	            d_month_end.getDate() + '/' + d_month_end.getFullYear();*/

	        var showAll = false;

	        var s_from = '';
	        var s_to = '';




	        var search = nlapiLoadSearch('transaction', 2421);
	        var filters = search.getFilters();

	        //Search for exchange Rate
	        var f_rev_curr = 0;
	        var f_cost_curr = 0;
	        var filters = [];

	        //var filters = [];
	        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
	        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
	        var a_dataVal = {};
	        var dataRows = [];

	        var column = new Array();
	        column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
	        column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
	        column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
	        column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
	        //column[4] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
	        column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
	        column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
	        //column[5] = new nlobjSearchColumn('custrecord_pl_gbp_cost_rate');

	        var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
	        if (currencySearch) {
	            for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {

	                a_dataVal = {
	                    s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
	                    i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
	                    rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
	                    cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
	                    //gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_revenue'),
	                    crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
	                    nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate')
	                    //gbp_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_cost_rate'),


	                };
	                if (_logValidation(a_dataVal))
	                    dataRows.push(a_dataVal);


	            }
	        }




	        var project_search_results = getTaggedProjectList_Internal();

	        // create html for select using the project data
	        var s_project_options = '';
	        var a_selected_project_list = '';
	        var s_project_number = '';
	        var s_project_name = '';
	        for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
	            var i_project_id = project_search_results[i].getValue('internalid');
	            s_project_number = project_search_results[i]
	                .getValue('entityid');
	            s_project_name = project_search_results[i].getValue('companyname');



	            if (_logValidation(s_project_number)) {
	                a_project_list.push(s_project_number);
	            }


	            /*var s_selected = '';
	            if ((a_selected_project_list != null && a_selected_project_list
	                    .indexOf(s_project_number) != -1)
	                    || showAll == true) {
	            	s_selected = ' selected="selected" ';

	            	if (s_selected_project_name != '') {
	            		s_selected_project_name += ', ';
	            	}

	            	s_selected_project_name += s_project_number + ' '
	            	        + s_project_name;
	            }

	            s_project_options += '<option value="' + s_project_number + '" '
	                    + s_selected + '>' + s_project_number + ' '
	                    + s_project_name + '</option>';*/
	        }

	        if (a_selected_project_list == null ||
	            a_selected_project_list.length == 0) {
	            if (a_project_list.length > 0) {
	                a_selected_project_list = a_project_list;

	                showAll = true;
	            } else {
	                a_selected_project_list = new Array();
	            }
	        }

	        if (a_selected_project_list != null &&
	            a_selected_project_list.length != 0) {
	            var s_formula = '';

	            for (var i = 0; i < a_selected_project_list.length; i++) {
	                if (i != 0) {
	                    s_formula += " OR";
	                }

	                s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
	                    a_selected_project_list[i] + "' ";
	            }

	            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	                'equalto', 1);

	            projectFilter.setFormula("CASE WHEN " + s_formula +
	                " THEN 1 ELSE 0 END");

	          //  filters = filters.concat([projectFilter]);
	        } else {
	            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	                'equalto', 1);

	            projectFilter.setFormula("0");

	          //  filters = filters.concat([projectFilter]);
	        }

	        var s_from = s_month_start_;
	        var s_to_ = s_month_end_;
	        filters = filters.concat([new nlobjSearchFilter('trandate', null,
	            'within', s_from, s_to_)]);

	        var columns = search.getColumns();

	        columns[0].setSort(false);
	        columns[3].setSort(true);
	        columns[9].setSort(false);
	        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
	        filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
	        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
	        //  filters.push(new nlobjSearchFilter('postingperiod',null,'abs','140'));

	        filters.push(new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
	            'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
	        ]));

	        var search_results = searchRecord('transaction', null, filters, [
					  columns[0], columns[1], columns[2], columns[3], columns[4],columns[5],
					columns[6],columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], 
					columns[16], columns[17], columns[18], columns[19], columns[20],columns[21],columns[22],columns[23],columns[24],columns[25],
					columns[26],columns[27],columns[28],columns[29],columns[30],columns[31],columns[32],columns[33],columns[34],columns[35],columns[36],
					columns[37],columns[38],columns[39],columns[40]
	        ]);


	        var o_json = new Object();
	        var o_data = new Array();
	       /* var o_data = {

	            'Account_Management_Salary_Cost': [],
	            'Account_Management_Other_Cost': [],
	            'Regional_Management_Salary_Cost': [],
	            'Regional_Management_Other_Cost': [],
	            'Horizontal_Management_onsite_Salary_Cost': [],
	            'Horizontal_Management_onsite_Other_Cost': [],
	            'Horizontal_Management_offshore_Salary_Cost': [],
	            'Horizontal_Management_offshore_Other_Cost': [],
	            'Horizontal_Investments_Salary_Cost': [],
	            'Horizontal_Investments_Other_Cost': [],
	            'Horizontal_Dedicated_Resources_Salary_Cost': [],
	            'Horizontal_Dedicated_Resources_Other_Cost': [],


	        };*/

	        var new_object = null;

	        var s_period = '';

	        var a_period_list = [];

	        var a_category_list = [];
	        a_category_list[0] = '';
	        var a_group = [];

	        var a_income_group = [];
	        var j_other_list = {};
	        var other_list = [];

	        var col = new Array();
	        col[0] = new nlobjSearchColumn('custrecord_account_name');
	        col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
	        var oth_fil = new Array();
	        oth_fil[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	        var other_Search = nlapiSearchRecord('customrecord_pl_other_costs', null, oth_fil, col);
	        if (other_Search) {
	            for (var i_ind = 0; i_ind < other_Search.length; i_ind++) {
	                var acctname = other_Search[i_ind].getValue('custrecord_account_name');

	                a_category_list.push(acctname);


	                j_other_list = {
	                    o_acct: other_Search[i_ind].getValue('custrecord_account_name'),
	                    o_type: other_Search[i_ind].getText('custrecord_type_of_cost')
	                };

	                other_list.push(j_other_list);


	            }
	        }
	        
	        //Search for pl data mapping
	        var account_array = [];
	        var json_list = {};
	        var dataMapping = [];
	        var cols = new Array();
	        cols[0] = new nlobjSearchColumn('custrecord_pl_data_map_int_account');
	        cols[1] = new nlobjSearchColumn('custrecord_pl_data_map_int_cost_head');
	        cols[2] = new nlobjSearchColumn('custrecord_pl_map_int_cost_detail');
	        var oth_filt = new Array();
	        oth_filt[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	        var pl_data_mapping_search = nlapiSearchRecord('customrecord_pl_data_map_internal', null, oth_filt, cols);
	        if (pl_data_mapping_search) {
	            for (var i_indx = 0; i_indx < pl_data_mapping_search.length; i_indx++) {
	                var acctname_ = pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_int_account');

	                account_array.push(acctname_);


	                json_list = {
	                    account_name: pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_int_account'),
	                    cost_head: pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_int_cost_head'),
	                    cost_head_detailed: pl_data_mapping_search[i_indx].getValue('custrecord_pl_map_int_cost_detail')
	                };

	                dataMapping.push(json_list);


	            }
	        }
	        nlapiLogExecution('DEBUG','Internal Project Count',search_results.length);
	        for (var i = 0; search_results != null && i < search_results.length; i++) {
	            var period = search_results[i].getText(columns[0]);
	            var s_account_name = search_results[i].getValue(columns[11]);
	            var amount = '';
	            var account_name = '';
	            var cost_head = '';
	            var cost_head_detailed = '';
	            //Code updated by Deepak, Dated - 21 Mar 17
	            var s_month_year = period.split(' ');
	            var s_mont = s_month_year[0];
	            s_mont = getMonthCompleteName(s_mont);
	            var s_year_ = s_month_year[1];
	            var f_revRate = 63.65;
	            var f_costRate = 66.0;
	            var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate;
	            //	var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
	            //	nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
	            //nlapiLogExecution('audit','accnt name:- '+rate);
	            
	          //Fetch for PL Mapping Data.. Cost head details
	            var o_index_ = account_array.indexOf(s_account_name);
	            if (o_index_ != -1) {
	            	o_index_ = o_index_;
	                account_name = dataMapping[o_index_].account_name;
	                cost_head = dataMapping[o_index_].cost_head;
	                cost_head_detailed = dataMapping[o_index_].cost_head_detailed;
	                
	            }

	            //Fetch matching cost and rev rate convertion rate
	            for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
	                if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
	                    f_revRate = dataRows[data_indx].rev_rate;
	                    f_costRate = dataRows[data_indx].cost_rate;
	                    //f_gbp_rev_rate=dataRows[data_indx].gbp_rev_rate;
	                    f_crc_cost_rate = dataRows[data_indx].crc_cost;
	                    f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
	                }
	            }
	            /*	if(f_gbp_rev_rate==0)
	            	{
	            	f_gbp_rev_rate=getExchangerates(s_mont,s_year_);
	            	}*/

	            var transaction_type = search_results[i].getText(columns[8]);

	            var transaction_date = search_results[i].getValue(columns[9]);

	            var i_subsidiary = search_results[i].getValue(columns[10]);
	            var s_subsidiary = search_results[i].getText(columns[10]);
	            var amount = parseFloat(search_results[i].getValue(columns[1]));

				var amount_ForeignCur = parseFloat(search_results[i].getValue(columns[1]));
					
	            var category = search_results[i].getValue(columns[3]);

	            

	            var currency = search_results[i].getValue(columns[12]);
	            
	            var currency_text = search_results[i].getText(columns[12]);

	            var exhangeRate = search_results[i].getValue(columns[13]);
	            //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

	            var s_project_description = search_results[i].getValue(columns[16]);
	            var s_customer = search_results[i].getValue(columns[17]);
	            var s_Proj_category = search_results[i].getValue(columns[39]);
	           
	            var project_ID = '';
	            var projectName = '';
	            var s_subPractice = '';
	            var s_parentPractice = '';
	            var projectSearch = '';
	            if (_logValidation(s_project_description)) {
	                project_ID = s_project_description.split(' ')[0];
	                projectName = s_project_description.split(' ')[1];
	                s_project_number = project_ID;
	                //s_project_name =  projectName;
	            }
	            var s_practice = search_results[i].getText(columns[14]);
	            s_subPractice = s_practice;
	            s_parentPractice = search_results[i].getText(columns[15]);
	            if (_logValidation(s_practice))
	                s_parentPractice = s_practice.split(':')[0];

	          //  nlapiLogExecution('audit', 'accnt name:- ' + s_account_name);

	            /*if ((currency == parseInt(2)) && (parseInt(i_subsidiary) != parseInt(2))) {
	                amount = parseFloat(amount) * parseFloat(exhangeRate);

	            }*/
	            var flag = false;
	            if ((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue') {
	                amount = parseFloat(amount) * parseFloat(exhangeRate);
	                amount = parseFloat(amount) / parseFloat(f_revRate);
	                flag = true;

	            }
	            //AMount Convertion Logic
	            if ((parseInt(i_subsidiary) != parseInt(2)) && (currency != parseInt(1))) {
	                if (category != 'Revenue' && category != 'Other Income' &&
	                    category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
	                    if (currency == parseInt(9)) {
	                        amount = parseFloat(amount) / parseFloat(f_crc_cost_rate);
	                    } else if (currency == parseInt(8)) {
	                        amount = parseFloat(amount) / parseFloat(f_nok_cost_rate);
	                    } else {
	                        //f_total_expense += o_data[s_category][j].amount;
	                        amount = parseFloat(amount) / parseFloat(f_costRate);
	                    }

	                } else {

	                    amount = parseFloat(amount) / parseFloat(f_revRate);

	                    //f_total_revenue += o_data[s_category][j].amount;
	                }
	            } //END



	            var isWithinDateRange = true;

	            var d_transaction_date = nlapiStringToDate(transaction_date,
	                'datetimetz');

	            if (s_from != '') {
	                var d_from = nlapiStringToDate(s_from, 'datetimetz');

	                if (d_transaction_date < d_from) {
	                    isWithinDateRange = false;
	                }
	            }

	            if (s_to != '') {
	                var d_to = nlapiStringToDate(s_to, 'datetimetz');

	                if (d_transaction_date > d_to) {
	                    isWithinDateRange = false;
	                }
	            }

	            if (isWithinDateRange == true) {
	                var i_index = a_period_list.indexOf(period);

	                if (i_index == -1) {
	                    if (_logValidation(period)) {
	                        a_period_list.push(period);
	                    } else {
	                        //nlapiLogExecution('debug', 'error', '388');
	                    }

	                }

	                i_index = a_period_list.indexOf(period);
	            }
	            nlapiLogExecution('audit', 's_Proj_category:category' ,s_Proj_category+' '+category);
	            //Account Management
	            if (category == 'People Cost' && s_Proj_category == 'Account Management') {
	            	
	            	


	            	 o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12
	                    
	                });
	            } else if (s_Proj_category == 'Account Management') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12	
	                    
	                });

	            }
	            //Regional Management
	            if (category == 'People Cost' && s_Proj_category == 'Regional Management') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12	
	                 
	                });

	            } else if (s_Proj_category == 'Regional Management') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12	
	                 
	                });

	            }

	            //Horizontal Management Onsite
	            if (category == 'People Cost' && s_Proj_category == 'Horizontal Management Onsite') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12	
	                    
	                });




	            } else if (s_Proj_category == 'Horizontal Management Onsite') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12	
	                   
	                });

	            }
	            //Horizontal Management Offshore
	            if (category == 'People Cost' && s_Proj_category == 'Horizontal Management Offshore') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12	
	                    
	                });




	            } else if (s_Proj_category == 'Horizontal Management Offshore') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12
	                   
	                });

	            }
	           //Investment
	            if (category == 'People Cost' && s_Proj_category == 'Investment') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12
	                   
	                });




	            } else if (s_Proj_category == 'Investment') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12
	                    
	                });

	            }

	            //Horizontal Management Offshore
	            if (category == 'People Cost' && s_Proj_category == 'Bench') {


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Salary Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12
	                   
	                });




	            } else if (s_Proj_category == 'Bench') {

	                //avoiding discounts from account						  


	            	 o_data.push({
	                	'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                   
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': s_Proj_category +'-'+'Other Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Horizontal SGA',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Indirect',
						   'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                       's_account_name_merge': s_Proj_category //12
	                    
	                });

	            }
	        }



	        return o_data;

	    } catch (err) {
	        nlapiLogExecution('Debug', 'RestLet Main ', err);
	    }
}
//Search For transaction external without project tagging

function searchTransactions_external_withOut_Projects(s_month_start,s_month_end){

	
	try{
		var context = nlapiGetContext();
		var a_project_list = new Array();

		var s_selected_project_name = '';

		
	var search = nlapiLoadSearch('transaction', 2450);
	var filters = search.getFilters();

	//Search for exchange Rate
	var f_rev_curr = 0;
	var f_cost_curr = 0;
	var filters = [];

	//var filters = [];
	//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
	//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
	var a_dataVal = {};
	var dataRows = [];

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
	column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
	column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
	column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
	//column[4] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
	column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
	column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
	//column[5] = new nlobjSearchColumn('custrecord_pl_gbp_cost_rate');

	var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
	if (currencySearch) {
	    for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {

	        a_dataVal = {
	            s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
	            i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
	            rev_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
	            cost_rate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
	            //gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_revenue'),
	            crc_cost: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
	            nok_cost_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate')
	            //gbp_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_cost_rate'),


	        };
	        if (_logValidation(a_dataVal))
	            dataRows.push(a_dataVal);


	    }
	}




	var project_search_results = getTaggedProjectList();

	// create html for select using the project data
	var s_project_options = '';
	var a_selected_project_list = '';
	var s_project_number = '';
	var s_project_name = '';
	for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
	    var i_project_id = project_search_results[i].getValue('internalid');
	    s_project_number = project_search_results[i]
	        .getValue('entityid');
	    s_project_name = project_search_results[i].getValue('companyname');



	    if (_logValidation(s_project_number)) {
	        a_project_list.push(s_project_number);
	    }

	 }

	if (a_selected_project_list == null ||
	    a_selected_project_list.length == 0) {
	    if (a_project_list.length > 0) {
	        a_selected_project_list = a_project_list;

	        showAll = true;
	    } else {
	        a_selected_project_list = new Array();
	    }
	}
	nlapiLogExecution('DEBUG','a_selected_project_list - Before Duplicate',a_selected_project_list.length);
	a_selected_project_list = removearrayduplicate(a_selected_project_list);
	nlapiLogExecution('DEBUG','a_selected_project_list - After Duplicate',a_selected_project_list.length);
	if (a_selected_project_list != null &&
	    a_selected_project_list.length != 0) {
	    var s_formula = '';

	    for (var i = 0; i < a_selected_project_list.length; i++) {
	        if (i != 0) {
	            s_formula += " OR";
	        }

	        s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
	            a_selected_project_list[i] + "' ";
	    }

	    var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	        'equalto', 1);

	    projectFilter.setFormula("CASE WHEN " + s_formula +
	        " THEN 1 ELSE 0 END");

	   // filters = filters.concat([projectFilter]);
	} else {
	    var projectFilter = new nlobjSearchFilter('formulanumeric', null,
	        'equalto', 1);

	    projectFilter.setFormula("0");

	   // filters = filters.concat([projectFilter]);
	}

	var s_from = s_month_start;
	var s_to_ = s_month_end;
	filters = filters.concat([new nlobjSearchFilter('trandate', null,
	    'within', s_from, s_to_)]);

	var columns = search.getColumns();

	columns[0].setSort(false);
	columns[3].setSort(true);
	columns[9].setSort(false);
	filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
	filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
	filters.push(new nlobjSearchFilter('custcol_proj_category_on_a_click',null,'isnot','Bench'));

	filters = filters.concat([new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
	    'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
	])]);

	var search_results = searchRecord('transaction', 2450, filters, [
	    columns[0], columns[1], columns[2], columns[3], columns[4],columns[5],
	    columns[6],columns[7], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], 
	    columns[16], columns[17], columns[18], columns[19], columns[20],columns[21],columns[22],columns[23],columns[24],columns[25],
	    columns[26],columns[27],columns[28],columns[29],columns[30],columns[31],columns[32],columns[33],columns[34],columns[35],columns[36],
	    columns[37],columns[38],columns[39],columns[40]
	]);



	// Get the Facility_Cost

	var s_facility_cost_filter = '';

	for (var i = 0; i < a_selected_project_list.length; i++) {
	    s_facility_cost_filter += "'" + a_selected_project_list[i] + "'";

	    if (i != a_selected_project_list.length - 1) {
	        s_facility_cost_filter += ",";
	    }
	}

	var facility_cost_filters = new Array();
	facility_cost_filters[0] = new nlobjSearchFilter('formulanumeric',
	    null, 'equalto', 1);
	if (s_facility_cost_filter != '') {
	    facility_cost_filters[0]
	        .setFormula('CASE WHEN {custrecord_arpm_project.entityid} IN (' +
	            s_facility_cost_filter + ') THEN 1 ELSE 0 END');
	} else {
	    facility_cost_filters[0].setFormula('0');
	}

	var facility_cost_columns = new Array();
	facility_cost_columns[0] = new nlobjSearchColumn(
	    'custrecord_arpm_period');
	facility_cost_columns[1] = new nlobjSearchColumn(
	    'custrecord_arpm_num_allocated_resources');
	facility_cost_columns[2] = new nlobjSearchColumn(
	    'custrecord_arpm_facility_cost_per_person');
	facility_cost_columns[3] = new nlobjSearchColumn(
	    'custrecord_arpm_location');

	// var facility_cost_search_results = searchRecord(
//	      'customrecord_allocated_resources_per_mon', null,
//	     facility_cost_filters, facility_cost_columns);

	var o_json = new Object();

	/*var o_data = {
	    'Revenue': [],
	    'Discount': [],
	    'People_Cost_Employee': [],
	    'People_Cost_Contractor': [],
	    'Facility_Cost': [],
	    'Other_Cost_Travel': [],
	    'Other_Cost_Immigration': [],
	    'Other_Cost_Professional_Fees': [],
	    'Other_Cost_Others': []

	};*/

	var o_data = new Array();

	var new_object = null;

	var s_period = '';

	var a_period_list = [];

	var a_category_list = [];
	a_category_list[0] = '';
	var a_group = [];

	var a_income_group = [];
	var j_other_list = {};
	var other_list = [];

	var col = new Array();
	col[0] = new nlobjSearchColumn('custrecord_account_name');
	col[1] = new nlobjSearchColumn('custrecord_type_of_cost');
	var oth_fil = new Array();
	oth_fil[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	var other_Search = nlapiSearchRecord('customrecord_pl_other_costs', null, oth_fil, col);
	if (other_Search) {
	    for (var i_ind = 0; i_ind < other_Search.length; i_ind++) {
	        var acctname = other_Search[i_ind].getValue('custrecord_account_name');

	        a_category_list.push(acctname);


	        j_other_list = {
	            o_acct: other_Search[i_ind].getValue('custrecord_account_name'),
	            o_type: other_Search[i_ind].getText('custrecord_type_of_cost')
	        };

	        other_list.push(j_other_list);


	    }
	}

	//Search for pl data mapping
	var account_array = [];
	var json_list = {};
	var dataMapping = [];
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('custrecord_pl_data_map_account');
	cols[1] = new nlobjSearchColumn('custrecord_pl_data_map_cost_head');
	cols[2] = new nlobjSearchColumn('custrecord_pl_data_map_cost_detailed');
	var oth_filt = new Array();
	oth_filt[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	var pl_data_mapping_search = nlapiSearchRecord('customrecord_pl_data_mapping', null, oth_filt, cols);
	if (pl_data_mapping_search) {
	    for (var i_indx = 0; i_indx < pl_data_mapping_search.length; i_indx++) {
	        var acctname_ = pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_account');

	        account_array.push(acctname_);


	        json_list = {
	            account_name: pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_account'),
	            cost_head: pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_cost_head'),
	            cost_head_detailed: pl_data_mapping_search[i_indx].getValue('custrecord_pl_data_map_cost_detailed')
	        };

	        dataMapping.push(json_list);


	    }
	}
	nlapiLogExecution('DEBUG','External Project Count',search_results.length);
	for (var i = 0; search_results != null && i < search_results.length; i++) { //search_results.length
	    var period = search_results[i].getText(columns[0]);
	    var s_account_name = search_results[i].getValue(columns[11]);
	    
	    if(parseInt(s_account_name) == parseInt(978)){
	    	//nlapiLogExecution('DEBUG','Cost Head');
	    }
	    
	    var amount = '';
	    var account_name = '';
	    var cost_head = '';
	    var cost_head_detailed = '';
	    //Code updated by Deepak, Dated - 21 Mar 17
	    var s_month_year = period.split(' ');
	    var s_mont = s_month_year[0];
	    s_mont = getMonthCompleteName(s_mont);
	    var s_year_ = s_month_year[1];
	    var f_revRate = 65.65;
	    var f_costRate = 65.0;
	    var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate;
	    
	//Fetch for PL Mapping Data.. Cost head details
	    var o_index_ = account_array.indexOf(s_account_name);
	    if (o_index_ != -1) {
	    	o_index_ = o_index_;
	        account_name = dataMapping[o_index_].account_name;
	        cost_head = dataMapping[o_index_].cost_head;
	        cost_head_detailed = dataMapping[o_index_].cost_head_detailed;
	        
	    }
	    //Fetch matching cost and rev rate convertion rate
	    for (var data_indx = 0; data_indx < dataRows.length; data_indx++) {
	        if (dataRows[data_indx].s_month == s_mont && dataRows[data_indx].i_year == s_year_) {
	            f_revRate = dataRows[data_indx].rev_rate;
	            f_costRate = dataRows[data_indx].cost_rate;
	            //f_gbp_rev_rate=dataRows[data_indx].gbp_rev_rate;
	            f_crc_cost_rate = dataRows[data_indx].crc_cost;
	            f_nok_cost_rate = dataRows[data_indx].nok_cost_rate;
	        }
	    }
	   
	    
	    var transaction_type = search_results[i].getText(columns[8]);

	    var transaction_date = search_results[i].getValue(columns[9]);

	    var i_subsidiary = search_results[i].getValue(columns[10]);
	    
	    var s_subsidiary = search_results[i].getText(columns[10]);

	    var amount = parseFloat(search_results[i].getValue(columns[1]));
		
		 var amount_ForeignCur = parseFloat(search_results[i].getValue(columns[1]));

	    var category = search_results[i].getValue(columns[3]);

	   

	    var currency = search_results[i].getValue(columns[12]);
	    
	    var currency_text = search_results[i].getText(columns[12]);

	    var exhangeRate = search_results[i].getValue(columns[13]);
	    //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

	    var s_project_description = search_results[i].getValue(columns[16]);
	    var s_customer = search_results[i].getValue(columns[17]);

	    var project_ID = '';
	    var projectName = '';
	    var s_subPractice = '';
	    var s_parentPractice = '';
	    var projectSearch = '';
	    if (_logValidation(s_project_description)) {
	        project_ID = s_project_description.split(' ')[0];
	        projectName = s_project_description.split(' ')[1];
	        s_project_number = project_ID;
	        //s_project_name =  projectName;
	    }
	    var s_practice = search_results[i].getText(columns[14]);
	    s_subPractice = s_practice;
	    s_parentPractice = search_results[i].getText(columns[15]);
	    if (_logValidation(s_practice))
	        s_parentPractice = s_practice.split(':')[0];

	  //  nlapiLogExecution('audit', 'accnt name:- ' + s_account_name);

	   /* if ((currency == parseInt(2)) && (parseInt(i_subsidiary) != parseInt(2))) {
	        amount = parseFloat(amount) * parseFloat(exhangeRate);

	    }
	    if ((currency == parseInt(4)) && (parseInt(i_subsidiary) != parseInt(2))) {
	        amount = parseFloat(amount) * parseFloat(exhangeRate);

	    } */
		var flag = false;
		if((currency != parseInt(6)) && (parseInt(i_subsidiary) == parseInt(3)) && category == 'Revenue')
		{
			amount= parseFloat(amount)* parseFloat(exhangeRate);
			amount = parseFloat(amount) /parseFloat(f_revRate);
			flag = true;
			
		}
		var eur_to_inr = 77.25;
		
		//AMount Convertion Logic
		if((parseInt(i_subsidiary) != parseInt(2))&& (currency!= parseInt(1))){
		if (category != 'Revenue' && category != 'Other Income'
	        && category != 'Discount' && parseInt(i_subsidiary) != parseInt(2)) {
				if(currency== parseInt(9))
				{
					amount = parseFloat(amount) /parseFloat(f_crc_cost_rate);
				}
				else if(currency == parseInt(8)){
				amount = parseFloat(amount) /parseFloat(f_nok_cost_rate);
				}
				else if(currency == parseInt(4)){
				amount = parseFloat(amount) /parseFloat(f_eur_conv_rate);
				}
				
				else{
		
			amount= parseFloat(amount)* parseFloat(exhangeRate);		
			amount = parseFloat(amount) /parseFloat(f_costRate);	
			}
			
		}
				else if(flag == false && parseInt(currency) != parseInt(1) && parseInt(i_subsidiary) != parseInt(2)){
			amount= parseFloat(amount)* parseFloat(exhangeRate);
			amount = parseFloat(amount) /parseFloat(f_revRate);
			
		//f_total_revenue += o_data[s_category][j].amount;
		}
	}	//END

	    var isWithinDateRange = true;

	    var d_transaction_date = nlapiStringToDate(transaction_date,
	        'datetimetz');

	    if (s_from != '') {
	        var d_from = nlapiStringToDate(s_from, 'datetimetz');

	        if (d_transaction_date < d_from) {
	            isWithinDateRange = false;
	        }
	    }

	    if (s_to_ != '') {
	        var d_to = nlapiStringToDate(s_to_, 'datetimetz');

	        if (d_transaction_date > d_to) {
	            isWithinDateRange = false;
	        }
	    }

	    if (isWithinDateRange == true) {
	        var i_index = a_period_list.indexOf(period);

	        if (i_index == -1) {
	            if (_logValidation(period)) {
	                a_period_list.push(period);
	            } else {
	                //nlapiLogExecution('debug', 'error', '388');
	            }

	        }

	        i_index = a_period_list.indexOf(period);
	    }
	    //Remove Spaces
	    //var memo_m = search_results[i].getValue(columns[31]);
	   // nlapiLogExecution('DEBUG','memo_m',memo_m);
	    
	    if (category != 'People Cost' && category != 'Discount' && category != 'Facility Cost' && category != 'Revenue') {
	        var o_index = a_category_list.indexOf(s_account_name);
	        if (o_index != -1) {
	            o_index = o_index - 1;
	            var acct = other_list[o_index].o_acct;
	            var typeofact = other_list[o_index].o_type;


	            if ((_logValidation(period)) && (_logValidation(amount)) && (_logValidation(search_results[i].getValue(columns[4]))) &&
	                (_logValidation(search_results[i].getValue(columns[5]))) && (_logValidation(transaction_type)) &&
	                (_logValidation(transaction_date)) && (_logValidation(isWithinDateRange)) && (_logValidation(s_account_name))) {} else {
	                //  nlapiLogExecution('debug','error','408');
	            }
	            if (typeofact == 'Other Direct Cost - Travel') {
	                o_data.push({
	                	'project_id': s_project_number, //1
	                    'period': period,               //2
	                    'amount': parseFloat(amount).toFixed(2), //3
	                    'num': search_results[i].getValue(columns[4]), //4
	                    'sub_practice': s_subPractice.trim(), //5
	                    'practice': s_parentPractice.trim(), //6
	                    'type': transaction_type, //7
	                    'dt': transaction_date,  //8
	                    's_customer': s_customer,  //9
	                    's_region': search_results[i].getText(columns[20]), //10
	                    's_subsidiary': s_subsidiary,  //11
	                    's_account_name': 'Other Direct Cost - Travel', //12
	                    
	                    'account': search_results[i].getText(columns[11]), //13
	                    'bill_from': search_results[i].getValue(columns[21]), //14
	                    'bill_to': search_results[i].getValue(columns[22]), //15
	                    'pro_name': search_results[i].getValue(columns[26]), //16
	                    'pro_description': search_results[i].getValue(columns[16]), //17
	                    'cust_id': search_results[i].getValue(columns[23]),  //18
	                    'cust_name': search_results[i].getValue(columns[24]),  //19
	                    'emp_id': search_results[i].getValue(columns[27]),  //20
	                    'emp_name': search_results[i].getValue(columns[28]),  //21
	                    'location': search_results[i].getText(columns[29]), //22
	                    'memo_main': search_results[i].getValue(columns[30]),  //23
	                    'memo_line': search_results[i].getValue(columns[31]),  //24
	                    'description': search_results[i].getValue(columns[32]), //25
	                    'hour': search_results[i].getValue(columns[33]),        //26
	                    'bill_rate': search_results[i].getValue(columns[34]),    //27
	                    'emp_type': search_results[i].getValue(columns[35]),     //28
	                    'person_type': search_results[i].getValue(columns[36]),  //29
	                    'onsite_offshore': search_results[i].getValue(columns[37]), //30
	                    'billing_type': search_results[i].getValue(columns[38]), //31
	                    'proj_category': search_results[i].getValue(columns[39]), //32
	                    'tb_reco': 'Considered for Regional',
	                    'proj_services': search_results[i].getText(columns[40]),
	                    'cost_head': cost_head,
	                    'cost_head_detailed': cost_head_detailed,
	                    'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
						's_account_name_merge': 'Other Direct Cost' //12
	                    
	                });
	            } else if (typeofact == 'Other Direct Cost - Immigration') {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Other Direct Cost - Immigration',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                        'proj_services': search_results[i].getText(columns[40]),
	                        'cost_head': cost_head,
	                        'cost_head_detailed': cost_head_detailed,
	                        'type_revised': 'Direct',
							'amount_foreign': amount_ForeignCur,
							'currency_text': currency_text, //f_revRate
							//'exchange_rate': ex_rate,
	                        's_account_name_merge': 'Other Direct Cost'	
	                });
	            }
	            if (typeofact == 'Other Direct Cost - Professional Fees') {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Other Direct Cost - Professional Fees',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'Other Direct Cost'	
	                });
	            }
	            if (typeofact == 'Other Direct Cost - Others') {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Other Direct Cost - Others',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'Other Direct Cost'	
	                });
	            }

	        }


	    } else {

	        if (parseInt(s_account_name) == parseInt(580)) { // Bad Debts
	            if (parseFloat(amount) < 0) {
	                amount = Math.abs(parseFloat(amount));
	            } else {
	                amount = '-' + amount;
	            }
	        }

	        //avoiding discounts from account						  
	        if (category == 'Revenue' && i_subsidiary == parseInt(2)) {
	            if (s_account_name != parseInt(732)) {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Gross Revenue',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'Gross Revenue'	
	                });
	                //nlapiLogExecution('audit', 'subs:- ' + i_subsidiary);
	                //nlapiLogExecution('audit','account Revenue:- '+s_account_name); 'People_Cost_Employee' : [],
	                //'People_Cost_Contractor' : [],
	                //  nlapiLogExecution('audit','trnsactnnum:'+search_results[i].getValue(columns[4]));

	            }
	            //nlapiLogExecution('audit','subs:- '+i_subsidiary);
	            //nlapiLogExecution('audit','account Revenue:- '+s_account_name);
	        } else if (category == 'Revenue') {
	             o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Gross Revenue',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'Gross Revenue'		
	                });
	        }
	        if (category == 'Discount') {

	            o_data.push({
	                'project_id': s_project_number,
	                'period': period,
	               'amount': parseFloat(amount).toFixed(2),
	                'num': search_results[i].getValue(columns[4]),
	                'sub_practice': s_subPractice.trim(),
	                'practice': s_parentPractice.trim(),
	                'type': transaction_type,
	                'dt': transaction_date,
	                's_customer': s_customer,
	                's_region': search_results[i].getText(columns[20]),
	                's_subsidiary': s_subsidiary,
	                's_account_name': 'Discount',
	                	'bill_from': search_results[i].getValue(columns[21]),
	                    'bill_to': search_results[i].getValue(columns[22]),
	                	'account': search_results[i].getText(columns[11]),
	                    'pro_name': search_results[i].getValue(columns[26]),
	                    'pro_description': search_results[i].getValue(columns[16]),
	                    'cust_id': search_results[i].getValue(columns[23]),
	                    'cust_name': search_results[i].getValue(columns[24]),
	                    'emp_id': search_results[i].getValue(columns[27]),
	                    'emp_name': search_results[i].getValue(columns[28]),
	                    'location': search_results[i].getText(columns[29]),
	                    'memo_main': search_results[i].getValue(columns[30]),
	                    'memo_line': search_results[i].getValue(columns[31]),
	                    'description': search_results[i].getValue(columns[32]),
	                    'hour': search_results[i].getValue(columns[33]),
	                    'bill_rate': search_results[i].getValue(columns[34]),
	                    'emp_type': search_results[i].getValue(columns[35]),
	                    'person_type': search_results[i].getValue(columns[36]),
	                    'onsite_offshore': search_results[i].getValue(columns[37]),
	                    'billing_type': search_results[i].getValue(columns[38]),
	                    'proj_category': search_results[i].getValue(columns[39]),
	                    'tb_reco': 'Considered for Regional',
	                   'proj_services': search_results[i].getText(columns[40]),
	                   'cost_head': cost_head,
	                   'cost_head_detailed': cost_head_detailed,
	                   'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                   's_account_name_merge': 'Discount'			
	            });

	        } 
	            if (parseInt(s_account_name) == parseInt(518) || parseInt(s_account_name) == parseInt(541)) {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'PeopleCost - Contractor',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'People Cost'	
	                });
	            }
	            if (parseInt(s_account_name) == parseInt(978)) {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'PeopleCost - Employee',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'People Cost'	
	                });
	            }

	            if (category == 'Facility Cost' || parseInt(s_account_name) == parseInt(1058)) {
	                o_data.push({
	                    'project_id': s_project_number,
	                    'period': period,
	                   'amount': parseFloat(amount).toFixed(2),
	                    'num': search_results[i].getValue(columns[4]),
	                    'sub_practice': s_subPractice.trim(),
	                    'practice': s_parentPractice.trim(),
	                    'type': transaction_type,
	                    'dt': transaction_date,
	                    's_customer': s_customer,
	                    's_region': search_results[i].getText(columns[20]),
	                    's_subsidiary': s_subsidiary,
	                    's_account_name': 'Offshore Facility Cost',
	                    	'bill_from': search_results[i].getValue(columns[21]),
	                        'bill_to': search_results[i].getValue(columns[22]),
	                    	'account': search_results[i].getText(columns[11]),
	                        'pro_name': search_results[i].getValue(columns[26]),
	                        'pro_description': search_results[i].getValue(columns[16]),
	                        'cust_id': search_results[i].getValue(columns[23]),
	                        'cust_name': search_results[i].getValue(columns[24]),
	                        'emp_id': search_results[i].getValue(columns[27]),
	                        'emp_name': search_results[i].getValue(columns[28]),
	                        'location': search_results[i].getText(columns[29]),
	                        'memo_main': search_results[i].getValue(columns[30]),
	                        'memo_line': search_results[i].getValue(columns[31]),
	                        'description': search_results[i].getValue(columns[32]),
	                        'hour': search_results[i].getValue(columns[33]),
	                        'bill_rate': search_results[i].getValue(columns[34]),
	                        'emp_type': search_results[i].getValue(columns[35]),
	                        'person_type': search_results[i].getValue(columns[36]),
	                        'onsite_offshore': search_results[i].getValue(columns[37]),
	                        'billing_type': search_results[i].getValue(columns[38]),
	                        'proj_category': search_results[i].getValue(columns[39]),
	                        'tb_reco': 'Considered for Regional',
	                       'proj_services': search_results[i].getText(columns[40]),
	                       'cost_head': cost_head,
	                       'cost_head_detailed': cost_head_detailed,
	                       'type_revised': 'Direct',
						'amount_foreign': amount_ForeignCur,
						'currency_text': currency_text, //f_revRate
						//'exchange_rate': ex_rate,
	                       's_account_name_merge': 'Offshore Facility Cost'	
	                });
	            }

	            //nlapiLogExecution('audit','cat:- '+category);
	        
	    }
	}


	return o_data;
	}
	catch(ex){
	nlapiLogExecution('DEBUG','Summary Error',ex);	
	}
	}
	
	