// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
           Script Name : SCH_DailyTicker_RegionHead.js
        Author      : Jayesh Dinde
        Date        : 8 August 2016
        Description : Send Daily ticker mail to region head.
    
    
        Script Modification Log:
    
        -- Date --			-- Modified By --				--Requested By--				-- Description --
    
    
    
    
    Below is a summary of the process controls enforced by this script file.  The control logic is described
    more fully, below, in the appropriate function headers and code blocks.
    
    
         SCHEDULED FUNCTION
            - scheduledFunction(type)
    
    
         SUB-FUNCTIONS
            - The following sub-functions are called by the above core functions in order to maintain code
                modularization:
    
                   - NOT USED
    
    */
    }
    // END SCRIPT DESCRIPTION BLOCK  ====================================
    
    
    // BEGIN SCHEDULED FUNCTION =============================================
    
    function Region_head_ticker()
    {
        try
        {
            /*------------------Added by Koushalya 28/12/2021-------------------------------------------------------*/

            var filters_customrecord_revenue_location_subsidiarySearch = ["custrecord_offsite_onsite", "anyof", "2"];
            var obj_rev_loc_subSrch = Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
            var obj_offsite_onsite = obj_rev_loc_subSrch["Onsite/Offsite"];

        /*-------------------------------------------------------------------------*/
            var context = nlapiGetContext();
            var last_region_id = '';
            var icnt 	= context.getSetting('script', 'custscript_last_region_processed_id');
            nlapiLogExecution('audit','icnt:- ',icnt);
            if (icnt == null || icnt == '' || icnt == undefined) 
            {
                 last_region_id = '';
            }
            else 
            {
                 last_region_id = icnt;
            }
            
            var billing_from_date = nlapiStringToDate(nlapiDateToString(new Date()));
            
            var filters_search_regions = new Array();
            filters_search_regions[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F'); //east 1484
            // filters_search_regions[1] = new nlobjSearchFilter('internalid', null, 'anyof', ['1','2','3','27']); /// These are all Internal ID and 27 is Verizon Global
            //filters_search_regions[1] = new nlobjSearchFilter('custrecord_region_head',null,'anyof','1538'); Brillio Region
            
            if(_logValidation(last_region_id))
            {
                filters_search_regions[2] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', last_region_id);
            }
            
            var column_search_region = new Array();
            column_search_region[0] = new nlobjSearchColumn('internalid').setSort(true);
            column_search_region[1] = new nlobjSearchColumn('email','custrecord_region_head');
            column_search_region[2] = new nlobjSearchColumn('name');
            
            var region_head_master_result = nlapiSearchRecord('customrecord_region', null, filters_search_regions, column_search_region);
            nlapiLogExecution('DEBUG','region_head_master_result',region_head_master_result.length);
            if (_logValidation(region_head_master_result))
            {
                for(var region_index=0; region_index<region_head_master_result.length; region_index++)
                {	
                    yieldScript(context);
                    
                    var cust_unique_list = new Array();
                    var proj_unique_list = new Array();
                    var proj_deatils_arr = new Array();
                    var region_management_count = new Array();
                    var region_list = new Array();
                    var sr_no = 0;
                    var region_sr_no = 0;
                    var employee_allocated_list = new Array();
                    
                    var region_id = region_head_master_result[region_index].getId();
                    var region_head_mail = region_head_master_result[region_index].getValue('email','custrecord_region_head');
                    var region_name = region_head_master_result[region_index].getValue('name');
                    nlapiLogExecution('DEBUG','region_name===',region_name);
                    var usageEnd = context.getRemainingUsage();
                    if (usageEnd < 1000) 
                    {
                        nlapiLogExecution('DEBUG', 'usageEnd =' + usageEnd + '-->region_id-->' + region_id);
                        Schedulescriptafterusageexceeded(region_id);
                        break;
                    }
                    
                    var filters_search_allocation = new Array();
                    /*if(parseInt(region_id) == parseInt(7))
                    {
                        filters_search_allocation[0] = new nlobjSearchFilter('custentity_region', 'job', 'anyof', parseInt(region_id));
                    }*/
                    //if(parseInt(region_id) == parseInt(1) || parseInt(region_id) == parseInt(2) || parseInt(region_id) == parseInt(3)){
                    filters_search_allocation[0] = new nlobjSearchFilter('custentity_region', 'customer', 'anyof', parseInt(region_id));
                    //}
                    filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', billing_from_date);
                    filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
                    filters_search_allocation[3] = new nlobjSearchFilter('custentity_employee_inactive', 'employee', 'is', 'F');
                    
                    var columns = new Array();
                    columns[0] = new nlobjSearchColumn('custeventrbillable');
                    columns[1] = new nlobjSearchColumn('jobbillingtype','job');
                    columns[2] = new nlobjSearchColumn('custentity_clientpartner','job');
                    columns[3] = new nlobjSearchColumn('custentity_projectmanager','job');
                    columns[4] = new nlobjSearchColumn('custentity_deliverymanager','job');
                    columns[5] = new nlobjSearchColumn('custentity_verticalhead','job');
                    columns[6] = new nlobjSearchColumn('custevent_practice');
                    columns[7] = new nlobjSearchColumn('customer','job');
                    columns[8] = new nlobjSearchColumn('company');
                    columns[9] = new nlobjSearchColumn('custentity_project_allocation_category','job');
                    columns[10] = new nlobjSearchColumn('subsidiary','employee');
                    columns[11] = new nlobjSearchColumn('custentity_clientpartner','customer');
                    columns[12] = new nlobjSearchColumn('custentity_region','customer');
                    columns[13] = new nlobjSearchColumn('custentity_region','job');
                    columns[14] = new nlobjSearchColumn('percentoftime');
                    columns[15] = new nlobjSearchColumn('custentity_practice','job');
                    columns[16] = new nlobjSearchColumn('resource');
                    columns[17] = new nlobjSearchColumn('startdate');
                    columns[18] = new nlobjSearchColumn('enddate');
                    columns[19] = new nlobjSearchColumn('employeestatus','employee');
                    columns[20] = new nlobjSearchColumn('title','employee');
                    columns[21] = new nlobjSearchColumn('custeventbstartdate');
                    columns[22] = new nlobjSearchColumn('custeventbenddate');
                    columns[23] = new nlobjSearchColumn('custentity_reportingmanager','employee');
                    columns[24] = new nlobjSearchColumn('employeetype','employee');
                    columns[25] = new nlobjSearchColumn('hiredate','employee');
                    columns[26] = new nlobjSearchColumn('approver','employee');
                    columns[27] = new nlobjSearchColumn('timeapprover','employee');
                    columns[28] = new nlobjSearchColumn('custevent3');
                    columns[29] = new nlobjSearchColumn('custevent_otrate');
                    columns[30] = new nlobjSearchColumn('custevent_monthly_rate');
                    columns[31] = new nlobjSearchColumn('custentity_legal_entity_fusion','employee');
                    
                    var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
                    if (_logValidation(project_allocation_result))
                    {
                        for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++)
                        {
                            var is_resource_billable = project_allocation_result[i_search_indx].getValue('custeventrbillable');
                            var billing_type = project_allocation_result[i_search_indx].getText('jobbillingtype','job');
                            var client_partner = project_allocation_result[i_search_indx].getText('custentity_clientpartner','job');
                            var proj_manager = project_allocation_result[i_search_indx].getText('custentity_projectmanager','job');
                            var delivery_manager = project_allocation_result[i_search_indx].getText('custentity_deliverymanager','job');
                            var vertical_head = project_allocation_result[i_search_indx].getText('custentity_verticalhead','job');
                            var parctice_id = project_allocation_result[i_search_indx].getValue('custevent_practice');
                            var project_cust = project_allocation_result[i_search_indx].getValue('customer','job');
                            var proj_cust_name = project_allocation_result[i_search_indx].getText('customer','job');
                            var project_id = project_allocation_result[i_search_indx].getValue('company');
                            var project_name = project_allocation_result[i_search_indx].getText('company');
                            var is_proj_bench_category = project_allocation_result[i_search_indx].getValue('custentity_project_allocation_category','job');
                            var emp_susidiary = project_allocation_result[i_search_indx].getValue('subsidiary','employee');
                            var customer_CP_id = project_allocation_result[i_search_indx].getValue('custentity_clientpartner','customer');
                            var customer_CP_name = project_allocation_result[i_search_indx].getText('custentity_clientpartner','customer');
                            var customer_region = project_allocation_result[i_search_indx].getValue('custentity_region','customer');
                            var project_region = project_allocation_result[i_search_indx].getValue('custentity_region','job');
                            var resource_id = project_allocation_result[i_search_indx].getValue('resource');
                            var prcnt_allocated = project_allocation_result[i_search_indx].getValue('percentoftime');
                            prcnt_allocated = prcnt_allocated.toString();
                            prcnt_allocated = prcnt_allocated.split('.');
                            prcnt_allocated = prcnt_allocated[0].toString().split('%');
                            var final_prcnt_allocation = parseFloat(prcnt_allocated) / parseFloat(100);
                            final_prcnt_allocation = parseFloat(final_prcnt_allocation);
                            
                            employee_allocated_list.push(resource_id);
                            
                            var strVar_excel = '';
                            
                            strVar_excel += '<table>';
        
                            strVar_excel += '	<tr>';
                            strVar_excel += ' <td width="100%">';
                            strVar_excel += '<table width="100%" border="1">';
                            
                            strVar_excel += '	<tr>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Region</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Customer Name</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Project Name</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Type</td>';
                            strVar_excel += ' <td width="10%" font-size="11" align="left">Employee Name</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Employeee Subsidiary</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Percent Allocated</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Billable Status</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation Start Date</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation End Date</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Level</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Designation</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Onsite/Offshore</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Start Date</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Billing End Date</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Reporting Manager</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Expense Approver</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Time Approver</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Type</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Hire Date</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Bill Rate</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">OT Rate</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Monthly Rate</td>';
                            
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Client Partner</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Project Manager</td>';
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Delivery Manager</td>';			
                            strVar_excel += ' <td width="6%" font-size="11" align="center">Project Category</td>';
                            strVar_excel += '	</tr>';
                                
                            for (var index_no_dump = 0; index_no_dump < project_allocation_result.length; index_no_dump++)
                            {
                                var region_dump = project_allocation_result[index_no_dump].getText('custentity_region','customer');
                                var customer_dump = project_allocation_result[index_no_dump].getText('customer', 'job');
                                var project_dump = project_allocation_result[index_no_dump].getText('company');
                                var excuting_pract_proj_dump = project_allocation_result[index_no_dump].getText('custentity_practice', 'job');
                                var proj_billing_type_dump = project_allocation_result[index_no_dump].getText('jobbillingtype', 'job');
                                var employee_dump = project_allocation_result[index_no_dump].getText('resource');
                                var emp_practice_dump = project_allocation_result[index_no_dump].getText('custevent_practice');
                                var emp_subsidiary_dump = project_allocation_result[index_no_dump].getText('subsidiary', 'employee');
                                var emp_subsidiary_id_dump = project_allocation_result[index_no_dump].getValue('subsidiary', 'employee');
                                var onsite_offsite_dump = '';
                              
                                nlapiLogExecution('DEBUG','custentity_legal_entity_fusion',project_allocation_result[index_no_dump].getValue('custentity_legal_entity_fusion', 'employee'));
								if (emp_subsidiary_id_dump == 3 && project_allocation_result[index_no_dump].getValue('custentity_legal_entity_fusion', 'employee') == 'Brillio Technologies Private Limited UK') {
								onsite_offsite_dump = 'Onsite';
								} 
                                else {
                                    onsite_offsite_dump = obj_offsite_onsite[emp_subsidiary_id_dump] ? 'Offshore' : 'Onsite';
                                }
                                /*else if (emp_subsidiary_id_dump == 3 || emp_subsidiary_id_dump == 9 || emp_subsidiary_id_dump == 12) //emp_subsidiary_id_dump == 23(in sandbox)
								{
								onsite_offsite_dump = 'Offshore';
								} else {
								onsite_offsite_dump = 'Onsite';
								}*/
                              
                                //if(emp_subsidiary_id_dump == 3)
                                //{
                                //    onsite_offsite_dump = 'Offshore';
                                //}
                                //else
                                //{
                                //    onsite_offsite_dump = 'Onsite';
                                //}	
                                    
                                var prcnt_of_tym_dump = project_allocation_result[index_no_dump].getValue('percentoftime');
                                prcnt_of_tym_dump = prcnt_of_tym_dump.toString();
                                prcnt_of_tym_dump = prcnt_of_tym_dump.split('.');
                                prcnt_of_tym_dump = prcnt_of_tym_dump[0].toString().split('%');
                                var final_prcnt_allocation_dump = parseFloat(prcnt_of_tym_dump) / parseFloat(100);
                                final_prcnt_allocation_dump = parseFloat(final_prcnt_allocation_dump);
                                var is_resource_billable_dump = project_allocation_result[index_no_dump].getValue('custeventrbillable');
                                var allo_strt_date_dump = project_allocation_result[index_no_dump].getValue('startdate');
                                var allo_end_date_dump = project_allocation_result[index_no_dump].getValue('enddate');
                                var client_partner_dump = project_allocation_result[index_no_dump].getText('custentity_clientpartner','customer');
                                var prj_manager_dump = project_allocation_result[index_no_dump].getText('custentity_projectmanager', 'job');
                                var delivery_manager_dump = project_allocation_result[index_no_dump].getText('custentity_deliverymanager', 'job');
                                var proj_category_dump = project_allocation_result[index_no_dump].getText('custentity_project_allocation_category', 'job');
                                var emp_level_dump = project_allocation_result[index_no_dump].getText('employeestatus','employee');
                                var emp_designation_dump = project_allocation_result[index_no_dump].getValue('title','employee');
                                var billing_strt_date_dump = project_allocation_result[index_no_dump].getValue('custeventbstartdate');
                                var billing_end_date_dump = project_allocation_result[index_no_dump].getValue('custeventbenddate');
                                var reporting_manager_dump = project_allocation_result[index_no_dump].getText('custentity_reportingmanager','employee');
                                var emp_type_dump = project_allocation_result[index_no_dump].getText('employeetype','employee');
                                var emp_hire_date_dump = project_allocation_result[index_no_dump].getValue('hiredate','employee');
                                var expense_approver_dump = project_allocation_result[index_no_dump].getText('approver','employee');
                                var time_approver_dump = project_allocation_result[index_no_dump].getText('timeapprover','employee');
                                var bill_rate_dump = project_allocation_result[index_no_dump].getValue('custevent3');
                                if(!bill_rate_dump)
                                    bill_rate_dump = 0;
                                    
                                var ot_rate_dump = project_allocation_result[index_no_dump].getValue('custevent_otrate');
                                if(!ot_rate_dump)
                                    ot_rate_dump = 0;
                                    
                                var monthly_rate_dump = project_allocation_result[index_no_dump].getValue('custevent_monthly_rate');
                                if(!monthly_rate_dump)
                                    monthly_rate_dump = 0;
                                                                                
                                if(is_resource_billable_dump == 'T')
                                {
                                    is_resource_billable_dump = 'Yes';
                                }
                                else
                                    is_resource_billable_dump = 'No';
                                    
                                strVar_excel += '	<tr>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +region_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +customer_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +project_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +excuting_pract_proj_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +proj_billing_type_dump+ '</td>';
                                strVar_excel += ' <td width="10%" font-size="11" align="left">' +employee_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_practice_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_subsidiary_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +final_prcnt_allocation_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +is_resource_billable_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +allo_strt_date_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +allo_end_date_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_level_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_designation_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +onsite_offsite_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_strt_date_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_end_date_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +reporting_manager_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +expense_approver_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +time_approver_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_type_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_hire_date_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +bill_rate_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +ot_rate_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +monthly_rate_dump+ '</td>';
                                
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +client_partner_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +prj_manager_dump+ '</td>';
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +delivery_manager_dump+ '</td>';			
                                strVar_excel += ' <td width="6%" font-size="11" align="center">' +proj_category_dump+ '</td>';
                                strVar_excel += '	</tr>';
                            }
                            
                            strVar_excel += '</table>';
                            strVar_excel += ' </td>';	
                            strVar_excel += '	</tr>';
                            
                            strVar_excel += '</table>';
                            
                            var excel_file_obj = generate_excel(strVar_excel);
                        
                            if(parseInt(project_region) == parseInt(7))
                            {
                                if(region_list.indexOf(project_cust)>=0)
                                {
                                    var existing_region_cust = region_list.indexOf(project_cust);
                                    var regional_cnt = region_management_count[existing_region_cust].regional_count;
                                    regional_cnt = parseInt(regional_cnt) + parseInt(1);
                                    var final_prcnt_allocated = region_management_count[existing_region_cust].final_prcnt_allocation;
                                    final_prcnt_allocated = parseFloat(final_prcnt_allocated) + parseFloat(final_prcnt_allocation);
                                    region_management_count[existing_region_cust] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'regional_count':regional_cnt,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'final_prcnt_allocation':final_prcnt_allocated
                                                            };
                                }
                                else
                                {
                                    region_management_count[region_sr_no] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'regional_count':1,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'final_prcnt_allocation':final_prcnt_allocation 
                                                                    
    
                                                            };
                                    region_list.push(project_cust);
                                    region_sr_no++;
                                }
                            }
                            else
                            //if	
                            {
                                if(is_resource_billable == 'T')
                                {
                                    if(cust_unique_list.indexOf(project_cust)>=0)
                                    {
                                        if(parseInt(is_proj_bench_category) == 4)
                                        {
                                            var cust_already_index = cust_unique_list.indexOf(project_cust);
                                            var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
                                            var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
                                            var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
                                            var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
                                            
                                            var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
                                            var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
                                            var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
                                            var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
                                            
                                            var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
                                            bench_count_final = parseInt(bench_count_final) + parseInt(1);
                                            
                                            proj_deatils_arr[cust_already_index] = {
                                                                'project_id':project_id,
                                                                'project_name':project_name,
                                                                'cust_name':proj_cust_name,
                                                                'cust_id':project_cust,
                                                                'billable_count_onsite':billable_count_onsite,
                                                                'billable_count_offsite':billable_count_offsite,
                                                                'unbillable_count_onsite':unbillabl_count_onsite,
                                                                'unbillable_count_offsite':unbillabl_count_offsite,
                                                                'bench_count':bench_count_final,
                                                                'billing_type':billing_type,
                                                                'client_part':client_partner,
                                                                'proj_manager':proj_manager,
                                                                'delivery_mana':delivery_manager,
                                                                'vertical_head':vertical_head,
                                                                'customer_CP_name':customer_CP_name,
                                                                'customer_CP_id':customer_CP_id,
                                                                'billable_allocation_onsite':billable_allo_onsite,
                                                                'billable_allocation_offsite':billable_allo_offsite,
                                                                'unbillabel_allo_onsite':unbillabl_allo_onsite,
                                                                'unbillable_allo_offsite':unbillabl_allo_offsite															
                                                        };
                                        }
                                        else
                                        {
                                          //  if (parseInt(emp_susidiary) == 3)
                                          if (obj_offsite_onsite[emp_susidiary])
                                            {
                                                var cust_already_index = cust_unique_list.indexOf(project_cust);
                                                var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
                                                var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
                                                billable_count_offsite = parseInt(billable_count_offsite) + parseInt(final_prcnt_allocation);
                                                var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
                                                var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
                                                
                                                var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
                                                var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
                                                billable_allo_offsite = parseFloat(billable_allo_offsite) + parseFloat(final_prcnt_allocation);
                                                var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
                                                var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
                                                
                                                var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
                                                
                                                proj_deatils_arr[cust_already_index] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'billable_count_onsite':billable_count_onsite,
                                                                    'billable_count_offsite':billable_count_offsite,
                                                                    'unbillable_count_onsite':unbillabl_count_onsite,
                                                                    'unbillable_count_offsite':unbillabl_count_offsite,
                                                                    'bench_count':bench_count_final,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'billable_allocation_onsite':billable_allo_onsite,
                                                                    'billable_allocation_offsite':billable_allo_offsite,
                                                                    'unbillabel_allo_onsite':unbillabl_allo_onsite,
                                                                    'unbillable_allo_offsite':unbillabl_allo_offsite
                                                            };
                                            }
                                            else
                                            {
                                                var cust_already_index = cust_unique_list.indexOf(project_cust);
                                                var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
                                                billable_count_onsite = parseInt(billable_count_onsite) + parseInt(1);
                                                var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
                                                var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
                                                var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
                                                
                                                var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
                                                billable_allo_onsite = parseFloat(billable_allo_onsite) + parseFloat(final_prcnt_allocation);
                                                var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
                                                var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
                                                var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
                                                
                                                var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
                                                
                                                proj_deatils_arr[cust_already_index] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'billable_count_onsite':billable_count_onsite,
                                                                    'billable_count_offsite':billable_count_offsite,
                                                                    'unbillable_count_onsite':unbillabl_count_onsite,
                                                                    'unbillable_count_offsite':unbillabl_count_offsite,
                                                                    'bench_count':bench_count_final,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'billable_allocation_onsite':billable_allo_onsite,
                                                                    'billable_allocation_offsite':billable_allo_offsite,
                                                                    'unbillabel_allo_onsite':unbillabl_allo_onsite,
                                                                    'unbillable_allo_offsite':unbillabl_allo_offsite
                                                            };
                                            }
                                        
                                        }
                                    }
                                    else
                                    {
                                        if(parseInt(is_proj_bench_category) == 4)
                                        {
                                            cust_unique_list.push(project_cust);
                                            proj_unique_list.push(project_id);
                                            proj_deatils_arr[sr_no] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'billable_count_onsite':0,
                                                                    'billable_count_offsite':0,
                                                                    'unbillable_count_onsite':0,
                                                                    'unbillable_count_offsite':0,
                                                                    'bench_count':1,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'billable_allocation_onsite':0,
                                                                    'billable_allocation_offsite':0,
                                                                    'unbillabel_allo_onsite':0,
                                                                    'unbillable_allo_offsite':0
                                                            };
                                            sr_no++;
                                        }
                                        else
                                        {
                                          //  if(parseInt(emp_susidiary) == 3)
                                          if (obj_offsite_onsite[emp_susidiary])
                                            {
                                                cust_unique_list.push(project_cust);
                                                proj_unique_list.push(project_id);
                                                proj_deatils_arr[sr_no] = {
                                                                        'project_id':project_id,
                                                                        'project_name':project_name,
                                                                        'cust_name':proj_cust_name,
                                                                        'cust_id':project_cust,
                                                                        'billable_count_onsite':0,
                                                                        'billable_count_offsite':1,
                                                                        'unbillable_count_onsite':0,
                                                                        'unbillable_count_offsite':0,
                                                                        'bench_count':0,
                                                                        'billing_type':billing_type,
                                                                        'client_part':client_partner,
                                                                        'proj_manager':proj_manager,
                                                                        'delivery_mana':delivery_manager,
                                                                        'vertical_head':vertical_head,
                                                                        'customer_CP_name':customer_CP_name,
                                                                        'customer_CP_id':customer_CP_id,
                                                                        'billable_allocation_onsite':0,
                                                                        'billable_allocation_offsite':final_prcnt_allocation,
                                                                        'unbillabel_allo_onsite':0,
                                                                        'unbillable_allo_offsite':0
                                                                };
                                                sr_no++;
                                            }
                                            else
                                            {
                                                cust_unique_list.push(project_cust);
                                                proj_unique_list.push(project_id);
                                                proj_deatils_arr[sr_no] = {
                                                                        'project_id':project_id,
                                                                        'project_name':project_name,
                                                                        'cust_name':proj_cust_name,
                                                                        'cust_id':project_cust,
                                                                        'billable_count_onsite':1,
                                                                        'billable_count_offsite':0,
                                                                        'unbillable_count_onsite':0,
                                                                        'unbillable_count_offsite':0,
                                                                        'bench_count':0,
                                                                        'billing_type':billing_type,
                                                                        'client_part':client_partner,
                                                                        'proj_manager':proj_manager,
                                                                        'delivery_mana':delivery_manager,
                                                                        'vertical_head':vertical_head,
                                                                        'customer_CP_name':customer_CP_name,
                                                                        'customer_CP_id':customer_CP_id,
                                                                        'billable_allocation_onsite':final_prcnt_allocation,
                                                                        'billable_allocation_offsite':0,
                                                                        'unbillabel_allo_onsite':0,
                                                                        'unbillable_allo_offsite':0
                                                                };
                                                sr_no++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if(cust_unique_list.indexOf(project_cust)>=0)
                                    {
                                        if(parseInt(is_proj_bench_category) == 4)
                                        {
                                            var cust_already_index = cust_unique_list.indexOf(project_cust);
                                            var billable_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
                                            var billable_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
                                            var unbillabl_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
                                            var unbillabl_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
                                            
                                            var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
                                            var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
                                            var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
                                            var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
                                            
                                            var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
                                            bench_count_final = parseInt(bench_count_final) + parseInt(1);
                                            
                                            proj_deatils_arr[cust_already_index] = {
                                                                'project_id':project_id,
                                                                'project_name':project_name,
                                                                'cust_name':proj_cust_name,
                                                                'cust_id':project_cust,
                                                                'billable_count_onsite':billable_count_onsite,
                                                                'billable_count_offsite':billable_count_offsite,
                                                                'unbillable_count_onsite':unbillabl_count_onsite,
                                                                'unbillable_count_offsite':unbillabl_count_offsite,
                                                                'bench_count':bench_count_final,
                                                                'billing_type':billing_type,
                                                                'client_part':client_partner,
                                                                'proj_manager':proj_manager,
                                                                'delivery_mana':delivery_manager,
                                                                'vertical_head':vertical_head,
                                                                'customer_CP_name':customer_CP_name,
                                                                'customer_CP_id':customer_CP_id,
                                                                'billable_allocation_onsite':billable_allo_onsite,
                                                                'billable_allocation_offsite':billable_allo_offsite,
                                                                'unbillabel_allo_onsite':unbillabl_allo_onsite,
                                                                'unbillable_allo_offsite':unbillabl_allo_offsite
                                                        };
                                        }
                                        else
                                        {
                                         //   if (parseInt(emp_susidiary) == 3)
                                         if (obj_offsite_onsite[emp_susidiary])
                                            {
                                                var cust_already_index = cust_unique_list.indexOf(project_cust);
                                                var unbillable_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
                                                var unbillable_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
                                                unbillable_count_offsite = parseInt(unbillable_count_offsite) + parseInt(1);
                                                var billabe_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
                                                var billabe_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
                                                
                                                var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
                                                var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
                                                unbillabl_allo_offsite = parseFloat(unbillabl_allo_offsite) + parseFloat(final_prcnt_allocation);
                                                var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
                                                var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
                                                
                                                var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
                                                
                                                proj_deatils_arr[cust_already_index] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'billable_count_onsite':billabe_count_onsite,
                                                                    'billable_count_offsite':billabe_count_offsite,
                                                                    'unbillable_count_onsite':unbillable_count_onsite,
                                                                    'unbillable_count_offsite':unbillable_count_offsite,
                                                                    'bench_count':bench_count_final,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'billable_allocation_onsite':billable_allo_onsite,
                                                                    'billable_allocation_offsite':billable_allo_offsite,
                                                                    'unbillabel_allo_onsite':unbillabl_allo_onsite,
                                                                    'unbillable_allo_offsite':unbillabl_allo_offsite
                                                            };
                                            }
                                            else
                                            {
                                                var cust_already_index = cust_unique_list.indexOf(project_cust);
                                                var unbillable_count_onsite = proj_deatils_arr[cust_already_index].unbillable_count_onsite;
                                                unbillable_count_onsite = parseInt(unbillable_count_onsite) + parseInt(1);
                                                var unbillable_count_offsite = proj_deatils_arr[cust_already_index].unbillable_count_offsite;
                                                var billabe_count_onsite = proj_deatils_arr[cust_already_index].billable_count_onsite;
                                                var billabe_count_offsite = proj_deatils_arr[cust_already_index].billable_count_offsite;
                                                
                                                var unbillabl_allo_onsite = proj_deatils_arr[cust_already_index].unbillabel_allo_onsite;
                                                unbillabl_allo_onsite = parseFloat(unbillabl_allo_onsite) + parseFloat(final_prcnt_allocation);
                                                var unbillabl_allo_offsite = proj_deatils_arr[cust_already_index].unbillable_allo_offsite;
                                                var billable_allo_onsite = proj_deatils_arr[cust_already_index].billable_allocation_onsite;
                                                var billable_allo_offsite = proj_deatils_arr[cust_already_index].billable_allocation_offsite;
                                                
                                                var bench_count_final = proj_deatils_arr[cust_already_index].bench_count;
                                                
                                                proj_deatils_arr[cust_already_index] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'billable_count_onsite':billabe_count_onsite,
                                                                    'billable_count_offsite':billabe_count_offsite,
                                                                    'unbillable_count_onsite':unbillable_count_onsite,
                                                                    'unbillable_count_offsite':unbillable_count_offsite,
                                                                    'bench_count':bench_count_final,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'billable_allocation_onsite':billable_allo_onsite,
                                                                    'billable_allocation_offsite':billable_allo_offsite,
                                                                    'unbillabel_allo_onsite':unbillabl_allo_onsite,
                                                                    'unbillable_allo_offsite':unbillabl_allo_offsite
                                                            };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(parseInt(is_proj_bench_category) == 4)
                                        {
                                            cust_unique_list.push(project_cust);
                                            proj_unique_list.push(project_id);
                                            proj_deatils_arr[sr_no] = {
                                                                    'project_id':project_id,
                                                                    'project_name':project_name,
                                                                    'cust_name':proj_cust_name,
                                                                    'cust_id':project_cust,
                                                                    'billable_count_onsite':0,
                                                                    'billable_count_offsite':0,
                                                                    'unbillable_count_onsite':0,
                                                                    'unbillable_count_offsite':0,
                                                                    'bench_count':1,
                                                                    'billing_type':billing_type,
                                                                    'client_part':client_partner,
                                                                    'proj_manager':proj_manager,
                                                                    'delivery_mana':delivery_manager,
                                                                    'vertical_head':vertical_head,
                                                                    'customer_CP_name':customer_CP_name,
                                                                    'customer_CP_id':customer_CP_id,
                                                                    'billable_allocation_onsite':0,
                                                                    'billable_allocation_offsite':0,
                                                                    'unbillabel_allo_onsite':0,
                                                                    'unbillable_allo_offsite':0
                                                            };
                                            sr_no++;
                                        }
                                        else
                                        {
                                          //  if (parseInt(emp_susidiary) == 3)
                                          if (obj_offsite_onsite[emp_susidiary])
                                            {
                                                cust_unique_list.push(project_cust);
                                                proj_unique_list.push(project_id);
                                                proj_deatils_arr[sr_no] = {
                                                                        'project_id':project_id,
                                                                        'project_name':project_name,
                                                                        'cust_name':proj_cust_name,
                                                                        'cust_id':project_cust,
                                                                        'billable_count_onsite':0,
                                                                        'billable_count_offsite':0,
                                                                        'unbillable_count_onsite':0,
                                                                        'unbillable_count_offsite':1,
                                                                        'bench_count':0,
                                                                        'billing_type':billing_type,
                                                                        'client_part':client_partner,
                                                                        'proj_manager':proj_manager,
                                                                        'delivery_mana':delivery_manager,
                                                                        'vertical_head':vertical_head,
                                                                        'customer_CP_name':customer_CP_name,
                                                                        'customer_CP_id':customer_CP_id,
                                                                        'billable_allocation_onsite':0,
                                                                        'billable_allocation_offsite':0,
                                                                        'unbillabel_allo_onsite':0,
                                                                        'unbillable_allo_offsite':final_prcnt_allocation
                                                                };
                                                sr_no++;
                                            }
                                            else
                                            {
                                                cust_unique_list.push(project_cust);
                                                proj_unique_list.push(project_id);
                                                proj_deatils_arr[sr_no] = {
                                                                        'project_id':project_id,
                                                                        'project_name':project_name,
                                                                        'cust_name':proj_cust_name,
                                                                        'cust_id':project_cust,
                                                                        'billable_count_onsite':0,
                                                                        'billable_count_offsite':0,
                                                                        'unbillable_count_onsite':1,
                                                                        'unbillable_count_offsite':0,
                                                                        'bench_count':0,
                                                                        'billing_type':billing_type,
                                                                        'client_part':client_partner,
                                                                        'proj_manager':proj_manager,
                                                                        'delivery_mana':delivery_manager,
                                                                        'vertical_head':vertical_head,
                                                                        'customer_CP_name':customer_CP_name,
                                                                        'customer_CP_id':customer_CP_id,
                                                                        'billable_allocation_onsite':0,
                                                                        'billable_allocation_offsite':0,
                                                                        'unbillabel_allo_onsite':final_prcnt_allocation,
                                                                        'unbillable_allo_offsite':0
                                                                };
                                                sr_no++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        nlapiLogExecution('audit','proj detail len:-- ',proj_deatils_arr.length);
                        send_mail(proj_deatils_arr,region_head_mail,billing_from_date,region_name,region_management_count,excel_file_obj,employee_allocated_list,region_id);
                    
                        yieldScript(context);
                    }
                }
            }
        }
        catch(err)
        {
            nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
        }
    }
    
    function yieldScript(currentContext) {
    
        if (currentContext.getRemainingUsage() <= 2000) {
            nlapiLogExecution('AUDIT', 'API Limit Exceeded');
            var state = nlapiYieldScript();
    
            if (state.status == "FAILURE") {
                nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
                        + state.reason + ' / Size : ' + state.size);
                return false;
            } else if (state.status == "RESUME") {
                nlapiLogExecution('AUDIT', 'Script Resumed');
            }
        }
    }
    
    function send_mail(proj_deatils_arr,region_head_mail,billing_from_date,region_name,region_management_count,excel_file_obj,employee_allocated_list,region_id)
    {
    
           var mailList = getRegionMailingList();
            nlapiYieldScript();
            
            var today = new Date();
            var date_yesterday = nlapiAddDays(today, -1);
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            var hours = today.getHours();
            var minutes = today.getMinutes();
            
            if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 
            var today = dd+'/'+mm+'/'+yyyy;
            
    
            
            var strVar = '';
            strVar += '<html>';
            strVar += '<body>';
            
            strVar += '<p>Daily Ticker dated '+today+' '+hours+':'+minutes+' (24 Hour Format)</p>';
            
            strVar += '<table>';
            if (_logValidation(proj_deatils_arr))
        {
            var total_billable_head = 0;
            var total_unbillable_head = 0;
            var Total_billable_head_onsite = 0;
            var Total_billabel_head_offsite = 0;
            var Total_unbillable_head_onsite = 0;
            var Total_unbillable_head_offsite = 0;
            var total_billable_head_allo = 0;
            var total_billed_head_diff_report = 0;
            var cp_mail = new Array();
            
            // Diff Report Table
            for (var parent_index = 0; parent_index < proj_deatils_arr.length; parent_index++)
            {
                total_billed_head_diff_report = parseFloat(total_billed_head_diff_report) + parseFloat(proj_deatils_arr[parent_index].billable_allocation_onsite);
                total_billed_head_diff_report = parseFloat(total_billed_head_diff_report) + parseFloat(proj_deatils_arr[parent_index].billable_allocation_offsite);
            }
            
            total_billed_head_diff_report = parseFloat(total_billed_head_diff_report).toFixed(2);
            
            var billed_count_last_day = 0;
            var filters_emp_data = [[ 'custrecord_region_daily_ticker', 'anyof', parseInt(region_id) ], 'and',
                                        [ 'custrecord_date_of_dump', 'on', date_yesterday], 'and',
                                        ['custrecord_project_daily_ticker.custentity_region', 'noneof', parseInt(7)]];
                                        
            var emp_data_rcrd_srch = nlapiSearchRecord('customrecord_daily_emp_head_count_detail', 'customsearch1661', filters_emp_data);
            if (emp_data_rcrd_srch)
            {
                var columns = emp_data_rcrd_srch[0].getAllColumns();
                billed_count_last_day = emp_data_rcrd_srch[0].getValue(columns[0]);
                nlapiLogExecution('audit','yesterday billed count:- '+billed_count_last_day,total_billed_head_diff_report);
            }
                
                if(!billed_count_last_day)
                    billed_count_last_day = 0;
                    
                billed_count_last_day = parseFloat(billed_count_last_day).toFixed(2);
                var total_diff = parseFloat(billed_count_last_day) - parseFloat(total_billed_head_diff_report);
                total_diff = Math.abs(total_diff);
                
                strVar += '	<tr>';
                strVar += ' <td width="100%">';
                strVar += '<table width="100%" border="1">';
                
                    strVar += '	<tr>';
                    strVar += ' <td width="35%" font-size="11" align="center">Billed Count Yesterday</td>';
                    strVar += ' <td width="35%" font-size="11" align="center">Billed Count Today</td>';
                    strVar += ' <td width="30%" font-size="11" align="center">Difference(+/-)</td>';
                    strVar += '	</tr>';
                    
                    strVar += '	<tr>';
                    strVar += ' <td width="35%" font-size="11" align="center">' + parseFloat(billed_count_last_day).toFixed(2) + '</td>';
                    strVar += ' <td width="35%" font-size="11" align="center">' + parseFloat(total_billed_head_diff_report).toFixed(2) + '</td>';
                    strVar += ' <td width="30%" font-size="11" align="center">' + parseFloat(total_diff).toFixed(2) + '</td>';
                    strVar += '	</tr>';
                    
                strVar += '</table>';
                strVar += ' </td>';
                strVar += '	</tr>';
                
                if (billed_count_last_day > total_billed_head_diff_report)
                {
                    var filters_emp_diff_data = [['custrecord_emp_daily_ticker', 'noneof', employee_allocated_list], 'and',
                                                ['custrecord_emp_daily_ticker.custentity_employee_inactive', 'is', 'F'], 'and', 
                                                ['custrecord_date_of_dump', 'on', date_yesterday], 'and',
                                                ['custrecord_resource_billabl_daily_ticker', 'is', 'T'], 'and',
                                                [ 'custrecord_region_daily_ticker', 'anyof', parseInt(region_id) ]];
                    var columns_emp_diff_data = new Array();
                    columns_emp_diff_data[0] = new nlobjSearchColumn('custrecord_emp_daily_ticker');
                    columns_emp_diff_data[1] = new nlobjSearchColumn('custrecord_emp_practice_daily_ticker');
                    columns_emp_diff_data[2] = new nlobjSearchColumn('custrecord_project_daily_ticker');
                    columns_emp_diff_data[3] = new nlobjSearchColumn('custrecord_customer_daily_ticker');
                    columns_emp_diff_data[4] = new nlobjSearchColumn('custrecord_region_daily_ticker');
                    var emp_data_srch_yesterday = nlapiSearchRecord('customrecord_daily_emp_head_count_detail', null, filters_emp_diff_data, columns_emp_diff_data);
                    if (emp_data_srch_yesterday)
                    {
                        //nlapiLogExecution('audit','total diff count:- ',emp_data_srch_yesterday.length);
                        strVar += '	<tr height="10px">';
                        strVar += ' <td width="100%">';
                        strVar += '	</tr>';
                    
                        strVar += '	<tr>';
                        strVar += ' <td width="100%">';
                        strVar += '<table width="100%" border="1">';
                        
                            strVar += '	<tr>';
                            strVar += ' <td width="6%" font-size="11" align="center">Region</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Customer</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Project</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Employee</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
                            strVar += '	</tr>';
                    
                        for(var last_day_emp_index=0; last_day_emp_index < emp_data_srch_yesterday.length; last_day_emp_index++)
                        {
                            strVar += '	<tr>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_region_daily_ticker') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_customer_daily_ticker') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_project_daily_ticker') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_emp_daily_ticker') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custrecord_emp_practice_daily_ticker') + '</td>';
                            strVar += '	</tr>';
                        }
                        
                        strVar += '</table>';
                        strVar += ' </td>';
                        strVar += '	</tr>';
                        
                    }
                }
                else if(billed_count_last_day < total_billed_head_diff_report)
                {
                    var emp_yesterday_unique_list = new Array();
                    var filters_emp_data = [['custrecord_region_daily_ticker', 'anyof', parseInt(region_id)], 'and',
                                            ['custrecord_date_of_dump', 'on', date_yesterday], 'and',
                                            ['custrecord_resource_billabl_daily_ticker', 'is', 'T']];
                    var columns_emp_data = new Array();
                    columns_emp_data[0] = new nlobjSearchColumn('custrecord_emp_daily_ticker');
                    var emp_data_srch_yesterday = nlapiSearchRecord('customrecord_daily_emp_head_count_detail', null, filters_emp_data, columns_emp_data);
                    if (emp_data_srch_yesterday) {
                        for (var emp_index = 0; emp_index < emp_data_srch_yesterday.length; emp_index++) 
                            emp_yesterday_unique_list.push(emp_data_srch_yesterday[emp_index].getValue('custrecord_emp_daily_ticker'));
                    }
                    
                    var filters_emp_diff_data = [['resource', 'noneof', emp_yesterday_unique_list], 'and', 
                                                ['startdate', 'onorbefore', billing_from_date], 'and',
                                                ['enddate', 'onorafter', billing_from_date], 'and',
                                                ['employee.custentity_employee_inactive', 'is', 'F'], 'and',
                                                ['customer.custentity_region', 'anyof', parseInt(region_id)], 'and',
                                                ['custeventrbillable', 'is', 'T']];
                                                    
                    var columns_emp_diff_data = new Array();
                    columns_emp_diff_data[0] = new nlobjSearchColumn('resource');
                    columns_emp_diff_data[1] = new nlobjSearchColumn('customer');
                    columns_emp_diff_data[2] = new nlobjSearchColumn('company');
                    columns_emp_diff_data[3] = new nlobjSearchColumn('custentity_region', 'customer');
                    columns_emp_diff_data[4] = new nlobjSearchColumn('custevent_practice');
                    var emp_data_srch_yesterday = nlapiSearchRecord('resourceallocation', null, filters_emp_diff_data, columns_emp_diff_data);
                    if (emp_data_srch_yesterday)
                    {
                        //nlapiLogExecution('audit','total diff count:- ',emp_data_srch_yesterday.length);
                        strVar += '	<tr height="10px">';
                        strVar += ' <td width="100%">';
                        strVar += '	</tr>';
                    
                        strVar += '	<tr>';
                        strVar += ' <td width="100%">';
                        strVar += '<table width="100%" border="1">';
                        
                            strVar += '	<tr>';
                            strVar += ' <td width="6%" font-size="11" align="center">Region</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Customer</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Project</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Employee</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
                            strVar += '	</tr>';
                    
                        for(var last_day_emp_index=0; last_day_emp_index < emp_data_srch_yesterday.length; last_day_emp_index++)
                        {
                            strVar += '	<tr>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custentity_region', 'customer') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('customer') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('company') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('resource') + '</td>';
                            strVar += ' <td width="6%" font-size="11" align="center">' + emp_data_srch_yesterday[last_day_emp_index].getText('custevent_practice') + '</td>';
                            strVar += '	</tr>';
                        }
                        
                        strVar += '</table>';
                        strVar += ' </td>';
                        strVar += '	</tr>';
                        
                    }
                }
                    
                strVar += '	<tr height="10px">';
                strVar += ' <td width="100%">';
                strVar += '	</tr>';
                        
                strVar += '	<tr>';
                strVar += ' <td width="100%">';
                strVar += '<table width="100%" border="1">';
                
                strVar += '	<tr>';
                strVar += ' <td width="20%" font-size="11" align="center">Customer Name</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Oniste Billed Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Offiste Billed Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Total Billed Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Onsite Unbilled Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Offsite Unbilled Count</td>';
                strVar += ' <td width="10%" font-size="11" align="center">Total Unbilled Count</td>';
                strVar += ' <td width="20%" font-size="11" align="center">Client Partner</td>';
                strVar += '	</tr>';
        
            for(var proj_detail_index=0; proj_detail_index<proj_deatils_arr.length; proj_detail_index++)
            {
                total_billable_head = parseFloat(total_billable_head) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite);
                total_billable_head = parseFloat(total_billable_head) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite);
                
                total_unbillable_head = parseFloat(total_unbillable_head) + parseFloat(proj_deatils_arr[proj_detail_index].unbillabel_allo_onsite);
                total_unbillable_head = parseFloat(total_unbillable_head) + parseFloat(proj_deatils_arr[proj_detail_index].unbillable_allo_offsite);
                
                Total_billable_head_onsite = parseFloat(Total_billable_head_onsite) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite);
                Total_billabel_head_offsite = parseFloat(Total_billabel_head_offsite) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite);
                
                Total_unbillable_head_onsite = parseFloat(Total_unbillable_head_onsite) + parseFloat(proj_deatils_arr[proj_detail_index].unbillabel_allo_onsite);
                Total_unbillable_head_offsite = parseFloat(Total_unbillable_head_offsite) + parseFloat(proj_deatils_arr[proj_detail_index].unbillable_allo_offsite);
                
                total_billable_head_allo = parseFloat(total_billable_head_allo) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite);
                total_billable_head_allo = parseFloat(total_billable_head_allo) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite);
                
                var total_unbillabe_head_custWise = parseFloat(proj_deatils_arr[proj_detail_index].unbillable_allo_offsite) + parseFloat(proj_deatils_arr[proj_detail_index].unbillabel_allo_onsite);
                var total_billabe_head_custWise = parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite) + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite);
                
                    strVar += '	<tr>';
                    strVar += ' <td width="20%" font-size="11">' + proj_deatils_arr[proj_detail_index].cust_name + '</td>';
                    strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_onsite).toFixed(2) + '</td>';
                    strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].billable_allocation_offsite).toFixed(2) + '</td>';
                    strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_billabe_head_custWise).toFixed(2) + '</td>';
                    strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].unbillabel_allo_onsite).toFixed(2) + '</td>';
                    strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(proj_deatils_arr[proj_detail_index].unbillable_allo_offsite).toFixed(2) + '</td>';
                    strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_unbillabe_head_custWise).toFixed(2) + '</td>';
                    strVar += ' <td width="20%" font-size="11">' + proj_deatils_arr[proj_detail_index].customer_CP_name + '</td>';
                    strVar += '	</tr>';
                    
            }	
                strVar += '	<tr>';
                strVar += ' <td width="20%" font-size="11">Total</td>';
                strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(Total_billable_head_onsite).toFixed(2) + '</td>';
                strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(Total_billabel_head_offsite).toFixed(2) + '</td>';
                strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_billable_head).toFixed(2) + '</td>';
                strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(Total_unbillable_head_onsite).toFixed(2) + '</td>';
                strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(Total_unbillable_head_offsite).toFixed(2) + '</td>';
                strVar += ' <td width="10%" font-size="11" align="center">' + parseFloat(total_unbillable_head).toFixed(2) + '</td>';
                strVar += ' <td width="20%" font-size="11"></td>';
                strVar += '	</tr>';
            
                strVar += '</table>';
                strVar += ' </td>';	
                strVar += '	</tr>';
            
            strVar += '</table>';	
        }
        if (_logValidation(region_management_count))
        {
                // Region Management Count Table
                strVar += '<table width="100%">';
                strVar += '	<tr height="20px">';
                strVar += ' <td width="100%" font-size="11" align="center"></td>';
                strVar += '	</tr>';
                
                strVar += '	<tr>';
                strVar += ' <td width="100%">';
                
                    strVar += '<table width="100%" border="1">';
                    
                    strVar += '	<tr>';
                    strVar += ' <td width="80%" font-size="11" align="center">Customer Name</td>';
                    strVar += ' <td width="20%" font-size="11" align="center">Management Count</td>';
                    strVar += '	</tr>';
                
                for (var region_index = 0; region_index < region_management_count.length; region_index++)
                {
                    strVar += '	<tr>';
                    strVar += ' <td width="80%" font-size="11" align="center">' +region_management_count[region_index].cust_name+ '</td>';
                    strVar += ' <td width="20%" font-size="11" align="center">' +region_management_count[region_index].regional_count+ '</td>';
                    strVar += '	</tr>';
                }
                
                    strVar += '</table>';
                    strVar += ' </td>';	
                    strVar += '	</tr>';
                
                strVar += '</table>';
                strVar += '<p>NOTE: Management Count is the management team allocated to this Region.</p>';
                
                
        }
            strVar += '<p>**For any discrepancy in data, please reach out to business.ops@brillio.com or chetan.barot@brillio.com </p>';
            strVar += '<p>Thanks & Regards,</p>';
            strVar += '<p>Team IS</p>';
            
            strVar += '</body>';
            strVar += '</html>';
            
            var arr_record = new Array();
            arr_record['entity'] = 442;
            
            //var bcc = new Array();
            // //bcc[0] = 'manikandan.v@brillio.com';
            // bcc[0] = 'Chetan.barot@brillio.com';
            // //bcc[1] = 'sai.vannamareddy@brillio.com';
            // //bcc[1] = 'shamanth.k@brillio.com';
            // bcc[1] = 'prabhat.gupta@brillio.com';
            // bcc[2] = 'sridhar.v@brillio.com';
            
            //var cc = new Array();
            //cc[0]='gaurav.rohella@BRILLIO.COM';
            //cc[1]='manikandan.v@brillio.com';
            
            //var region_head_mail = 'shamanth.k@brillio.com'	
            //nlapiLogExecution('audit','total_billable_head:- '+total_billable_head);
            nlapiLogExecution('audit','mail:- ',region_head_mail);
            //nlapiSendEmail(442, 'jayesh@inspirria.com', 'Your Build Count - '+parseFloat(total_billable_head).toFixed(2)+' ('+region_name+') dt. '+today, strVar,null,null,null,excel_file_obj);
            //nlapiSendEmail(442,region_head_mail, 'Your Billed Count - '+parseFloat(total_billable_head).toFixed(1)+' ('+region_name+') dt. '+today, strVar,null,bcc,null,excel_file_obj);
          //modified-bidhi
          //region_head_mail = 'shamanth.k@brillio.com',karthik.gv@BRILLIO.COM,hemakumar.s@brillio.com,vinod.subramanyam@brillio.com

          var toMail = new Array();
          var ccMail = new Array();
          var bccMail = new Array();
 
//Sandbox
 /*        var regionHeads = {
            1: "karthik.gv@BRILLIO.COM",
            2: "sitaram.upadhya@brillio.com",
            3: "hemakumar.s@brillio.com",
            27: "sitaram.upadhya@brillio.com",
          }
*/

var regionHeads = {
            1: "karthik.gv@BRILLIO.COM",
            2: "girraj.gupta@brillio.com",
            3: "hemakumar.s@brillio.com",
            27: "girraj.gupta@brillio.com",
          }

          if(mailList[region_id]){

          ccMail = mailList[region_id].filter(function(each){
            return each.receipient=='cc';
            }).map(function(each){
            return each.email;
           })

           bccMail = mailList[region_id].filter(function(each){
            return each.receipient=='bcc';
            }).map(function(each){
            return each.email;
           })



        }
       if(regionHeads[region_id]){
          nlapiSendEmail(442,regionHeads[region_id], 'Your Billed Count - '+parseFloat(total_billable_head).toFixed(1)+' ('+region_name+') dt. '+today, strVar,ccMail,bccMail,arr_record,excel_file_obj);
       }
      
      

      

         
            
            
            
        
    }
    
    function generate_excel(strVar_excel)
    {
        var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
            '<head>'+
            '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
            '<meta name=ProgId content=Excel.Sheet/>'+
            '<meta name=Generator content="Microsoft Excel 11"/>'+
            '<!--[if gte mso 9]><xml>'+
            '<x:excelworkbook>'+
            '<x:excelworksheets>'+
            '<x:excelworksheet=sheet1>'+
            '<x:name>** ESTIMATE FILE**</x:name>'+
            '<x:worksheetoptions>'+
            '<x:selected></x:selected>'+
            '<x:freezepanes></x:freezepanes>'+
            '<x:frozennosplit></x:frozennosplit>'+
            '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
            '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
            '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
            '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
                '<x:activepane>0</x:activepane>'+// 0
                '<x:panes>'+
                '<x:pane>'+
                '<x:number>3</x:number>'+
                '</x:pane>'+
                '<x:pane>'+
                '<x:number>1</x:number>'+
                '</x:pane>'+
                '<x:pane>'+
                '<x:number>2</x:number>'+
            '</x:pane>'+
            '<x:pane>'+
            '<x:number>0</x:number>'+//1
            '</x:pane>'+
            '</x:panes>'+
            '<x:protectcontents>False</x:protectcontents>'+
            '<x:protectobjects>False</x:protectobjects>'+
            '<x:protectscenarios>False</x:protectscenarios>'+
            '</x:worksheetoptions>'+
            '</x:excelworksheet>'+
            '</x:excelworksheets>'+
            '<x:protectstructure>False</x:protectstructure>'+
            '<x:protectwindows>False</x:protectwindows>'+
            '</x:excelworkbook>'+
            
            // '<style>'+
                
                //-------------------------------------
            '</x:excelworkbook>'+
            '</xml><![endif]-->'+
             '<style>'+
            'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
            '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
            '<style>'+
    
            '<!-- /* Style Definitions */'+
    
            //'@page Section1'+
            //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+
    
            'div.Section1'+
            '{ page:Section1;}'+
    
            'table#hrdftrtbl'+
            '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+
    
            '</style>'+
            
            
            '</head>'+
            
    
            '<body>'+
            
            '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
            
            var strVar2 = strVar_excel;
            strVar1 = strVar1 + strVar2;
            var file = nlapiCreateFile('Resource Allocation Data.xls', 'XMLDOC', strVar1);
            return file;
    }
    
    function _logValidation(value) 
    {
     if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
     {
      return true;
     }
     else 
     { 
      return false;
     }
    }
    
    function Schedulescriptafterusageexceeded(region_id)
    {
        try
        {
            var params = new Array();
            var startDate = new Date();
            params['startdate'] = startDate.toUTCString();
            params['custscript_last_region_processed_id'] = region_id;
            var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
            //nlapiLogExecution('DEBUG', 'After Scheduling', 'Script scheduled status=' + status);
        } 
        catch (e) 
        {
            nlapiLogExecution('DEBUG', 'In Scheduled Catch', 'e : ' + e.message);
        }
    }


    function getRegionMailingList(){
        var mailList = {};
        //nlapiSearchRecord("customrecord648",null,
        var regionSearch = nlapiSearchRecord("customrecord_dailyticker_mailing_data",null,
        [
           ["custrecord_type_value","is","Region"]
        ], 
        [
           new nlobjSearchColumn("id",null,"GROUP").setSort(false), 
           new nlobjSearchColumn("custrecord_email_value",null,"GROUP"), 
           new nlobjSearchColumn("custrecord_receipient_name",null,"GROUP"), 
           new nlobjSearchColumn("custrecord_type_value",null,"GROUP"), 
           new nlobjSearchColumn("custrecord_practice_value",null,"GROUP"), 
           new nlobjSearchColumn("custrecord_region_value",null,"GROUP")
        ]
        );
        
        if (regionSearch) {
          
          nlapiLogExecution('debug', 'practice_test',JSON.stringify(regionSearch));
              
             for(var i=0;i<regionSearch.length;i++){
               
            //    nlapiLogExecution('debug', 'i length', i);
            //    var emailId = practiceSearch[i].getValue("custrecord_email_value",null,"GROUP");
            //    var receipient = practiceSearch[i].getValue("custrecord_receipient_name",null,"GROUP");
            //    var type = practiceSearch[i].getValue("custrecord_type_value",null,"GROUP");
            //    var practice = practiceSearch[i].getValue("custrecord_practice_value",null,"GROUP");
               var region = regionSearch[i].getValue("custrecord_region_value",null,"GROUP");
               var toArray = new Array();
               var ccArray = new Array();
               var bccArray = new Array();
               
               if(!mailList[region]){
                  mailList[region] = new Array();
                  

                   mailList[region] = [{
                       'email': regionSearch[i].getValue("custrecord_email_value",null,"GROUP"),
                       'receipient' : regionSearch[i].getValue("custrecord_receipient_name",null,"GROUP"),
                        'type' :regionSearch[i].getValue("custrecord_type_value",null,"GROUP"),
                       'practice' : regionSearch[i].getValue("custrecord_practice_value",null,"GROUP"),
                        'region' : regionSearch[i].getValue("custrecord_region_value",null,"GROUP")
                   }]
               } else {
                   mailList[region].push({
                       'email': regionSearch[i].getValue("custrecord_email_value",null,"GROUP"),
                       'receipient' : regionSearch[i].getValue("custrecord_receipient_name",null,"GROUP"),
                        'type' :regionSearch[i].getValue("custrecord_type_value",null,"GROUP"),
                       'practice' : regionSearch[i].getValue("custrecord_practice_value",null,"GROUP"),
                        'region' : regionSearch[i].getValue("custrecord_region_value",null,"GROUP")
                       
                   })
               }
               
               
             }
            
                                 
        }
          
        
        return mailList
        
        
        
        
        }