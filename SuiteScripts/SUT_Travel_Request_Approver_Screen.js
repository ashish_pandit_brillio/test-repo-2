/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Aug 2015     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

var K_CONSTANTS	=	{'ROLE_IMMIGRATION': 1081};

function suitelet(request, response){

	var file = nlapiLoadFile(57169); //load the HTML file
	var contents = file.getValue(); //get the contents
	
	// Get logged-in user
	var objUser = nlapiGetContext();
	  
	var i_employee_id = objUser.getUser();
	
	var s_listing_page_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_tr_approver_screen', 'customdeploy1');
	
	var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1');
	
	var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname']);
	
	var i_role	=	objUser.getRole();

	var values = new Object();
	
	var mode	=	request.getParameter('mode');
	
	
	
	var isApprovalScreen	=	false;
	
	if(mode == 'my-requests')
		{			
			values['my_requests_url_link']	= 'active';
			values['approve_requests_url_link']	=	'';
			values['page-title']	=	'My Requests';
		}
	else
		{
			
			
			values['my_requests_url_link'] = '';
			values['approve_requests_url_link']	=	'active';
			values['page-title']	=	'TR Approvals';
			
			isApprovalScreen	=	true;
		}
	
	
	
	var a_results_list	=	getRequestList(mode, i_employee_id);// searchRecord('customrecord_travel_request', null, filters, columns);
	
	var s_table_html	=	'<thead><tr><th>Departure Date</th><th>TR ID</th><th>Type</th><th>Employee</th><th>Description</th><th>Project</th><th>Purpose</th><th>Status</th><th><span class="glyphicon glyphicon-tasks"></span></th></tr></thead><tbody>';
	
	for(var i = 0; a_results_list != null && i < a_results_list.length; i++)
		{
			var url	=	nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1') + '&custparam_tr_id=' + a_results_list[i].id.value + '&custparam_mode=approve';

			var is_to_be_approved	=	a_results_list[i].isToBeApproved.value == true?'to-be-approved':'';
			
			s_table_html	+=	'<tr class="status_' + a_results_list[i].status.value + (is_to_be_approved == ''?'':' status_-2') + ' ' + is_to_be_approved + '">';
			s_table_html	+=	'<td>' + a_results_list[i].departureDate.value + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].name.value + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].travelType.text + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].employee.text + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].travelTitle.value + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].project.text + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].travelPurpose.text + '</td>';
			s_table_html	+=	'<td>' + a_results_list[i].status.text + (is_to_be_approved == ''?'':' <span style="color:#F0AD4E;" class="glyphicon glyphicon-inbox"></span>') + '</td>';
			s_table_html	+=	'<td><a href="' + url + '">View</a></td>';
			//s_table_html	+=	'<td>' + search_results[i].getText(columns[7]) + '</td>';
			s_table_html	+=	'</tr>';
		}
	
	s_table_html	+=	'</tbody>';
	
	values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
	values['role']	=	getRoleName(i_employee_id, i_role);
	values['new_request_url']	=	s_new_request_url;
	values['my_requests_url']	=	s_listing_page_url + '&mode=my-requests';
	values['approve_requests_url']	=	s_listing_page_url;
	values['search-filter']	=	getStatusDropdown(isApprovalScreen);
	values['list']	=	s_table_html;
	
	contents	=	replaceValues(contents, values);
	response.write(contents);
}

// Return data
function getRequestList(mode, i_employee_id)
{
	var filters	=	new Array();
	
	if(mode == 'my-requests')
	{
		filters[0]	=	new nlobjSearchFilter('custrecord_tr_employee', null, 'anyof', i_employee_id);
	}
else
	{
		if(K_ROLES.IMMIGRATION.indexOf(i_employee_id) == -1 && K_ROLES.TRAVEL.indexOf(i_employee_id) == -1)
			{
				filters[0]	=	new nlobjSearchFilter('custrecord_tr_approving_manager_id', null, 'is', i_employee_id);
			}
	}

	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	columns[1]	=	new nlobjSearchColumn('custrecord_tr_travel_type');
	columns[2]	=	new nlobjSearchColumn('custrecord_tr_employee');
	columns[3]	=	new nlobjSearchColumn('custrecord_tr_project');
	columns[4]	=	new nlobjSearchColumn('custrecord_tr_short_description');
	columns[5]	=	new nlobjSearchColumn('custrecord_tr_purpose_of_travel');
	columns[6]	=	new nlobjSearchColumn('custrecord_tr_status');
	columns[7]	=	new nlobjSearchColumn('internalid').setSort(true);
	columns[8]	=	new nlobjSearchColumn('custrecord_tr_approving_manager_id');
	columns[9]	=	new nlobjSearchColumn('custrecord_tr_departure_date');
	
	var search_results	=	searchRecord('customrecord_travel_request', null, filters, columns);
	
	// List of Requests
	var a_request_list	=	new Array();
	
	for(var i = 0; search_results != null && i < search_results.length; i++)
	{
		// Request Details
		var o_request	=	new Object();
		
		var url	=	nlapiResolveURL('SUITELET', 'customscript_sut_travel_req_details_form', 'customdeploy1') + '&custparam_tr_id=' + search_results[i].getValue(columns[7]) + '&custparam_mode=approve';

		var is_to_be_approved	=	false;
		
		if(search_results[i].getValue(columns[8]) == i_employee_id && search_results[i].getValue(columns[6]) == 2) // Approver
			{
				is_to_be_approved	=	true;
			}
		else if(K_ROLES.IMMIGRATION.indexOf(i_employee_id) != -1 && search_results[i].getValue(columns[6]) == 3)
			{
				is_to_be_approved	=	true;
			}
		else if(K_ROLES.TRAVEL.indexOf(i_employee_id) != -1 && search_results[i].getValue(columns[6]) == 4)
			{
				is_to_be_approved	=	true;
			}
		
		o_request.id	=	{'value' : search_results[i].getValue(columns[7])};
		o_request.isToBeApproved	=	{'value' : is_to_be_approved};
		o_request.status	=	{'value' : search_results[i].getValue(columns[6]), 'text' : search_results[i].getText(columns[6])};
		o_request.departureDate	=	{'value' : search_results[i].getValue(columns[9])};
		o_request.name	=	{'value' : search_results[i].getValue(columns[0])};
		o_request.travelType	=	{'value' : search_results[i].getValue(columns[1]), 'text' : search_results[i].getText(columns[1])};
		o_request.employee	=	{'value' : search_results[i].getValue(columns[2]), 'text' : search_results[i].getText(columns[2])};
		o_request.travelTitle	=	{'value' : search_results[i].getValue(columns[4]), 'text' : search_results[i].getText(columns[4])};
		o_request.project	=	{'value' : search_results[i].getValue(columns[3]), 'text' : search_results[i].getText(columns[3])};
		o_request.travelPurpose	=	{'value' : search_results[i].getValue(columns[5]), 'text' : search_results[i].getText(columns[5])};
		
		a_request_list.push(o_request);		
	}
	
	return a_request_list;
}

function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}

//Get Role Name
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}

// Get Options for dropdown
function getOptions(a_data, i_selected_value, s_placeholder)
{
	var strOptions = '<option value = "">' + (s_placeholder == undefined?'':s_placeholder) + '</option>';

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += '<option value = "'+a_data[i].value+'" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}

	return strOptions;
}

// Get List values and return options for dropdown
function getListValues(s_list_name, strSelectedValue)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values, strSelectedValue);
}

function getListValuesArray(s_list_name)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return list_values;
}

// Get Status Dropdown
function getStatusDropdown(isApprovalScreen)
{
	var a_status	=	getListValuesArray('customlist_tr_status');
	
	if(isApprovalScreen)
		{
			a_status.push({'display': 'To Be Approved By Me', 'value': -2});
		
			return getOptions(a_status, -2, '-- View All --');	
		}
	else
		{
			return getOptions(a_status, null, '-- View All --');
		}
}