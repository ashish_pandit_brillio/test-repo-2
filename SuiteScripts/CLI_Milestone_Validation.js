//production script
function fieldchange(type,name)
{
	//try
	//{
		var i_sum_of_amt=0;
		var duplicate_check_count=0;
	//alert('name'+name);

	if(name == 'custrecord_fpr_milestone')
	{
		//alert('project_task');
		var project_task=nlapiGetFieldValue('custrecord_fpr_milestone');
		//alert('project_task'+project_task);


		var project_task_load=nlapiLoadRecord('projectTask', project_task);
		var is_milestone=project_task_load.getFieldValue('ismilestone');
		//alert('is_milestone'+is_milestone);
		if(is_milestone == 'F')
			{
				alert('Please Select Only Milestone');
				nlapiSetFieldValue('custrecord_fpr_milestone','',false);
				nlapiSetFieldValue('custrecord_fpr_amount','',false);
				nlapiSetFieldValue('custrecord_fpr_date','',false);
				duplicate_check_count=1;
			}
		//----------------------------------------------------------------------
		if(duplicate_check_count == '0')
		{//duplicate check count if start
		
			var miles_filter=new Array();
			miles_filter[0]=new nlobjSearchFilter('custrecord_fpr_milestone', null,'is', project_task);
			var search_milestone=nlapiSearchRecord('customrecord_fpr_milestone_wi', null, miles_filter, null);
			//alert('search_milestone.length'+search_milestone.length);
			if(search_milestone != null)
				{
					alert('Duplicate Milestone Entry');
					nlapiSetFieldValue('custrecord_fpr_milestone','',false);
				}
			//-----------------------------------------------------------------------
			var project_id=nlapiGetFieldValue('custrecordfpr_projparent');//
			var Load_project=nlapiLoadRecord('job',project_id);
			var billing_schedule=Load_project.getFieldValue('billingschedule');
			//alert('billing_schedule======='+billing_schedule);

			var project_value=parseFloat(Load_project.getFieldValue('jobprice'));
			//alert('project_value'+project_value);
			if(billing_schedule == null || billing_schedule == '')
				{//if start
					alert('Please Defined Billing Schedule');
				}
			else
				{
					var load_billing_schedule=nlapiLoadRecord('billingschedule', billing_schedule);

					var count=load_billing_schedule.getLineItemCount('milestone');
					//alert('count======='+count);
					for(var kk=1 ; kk<=count ; kk++)
						{//for start

						var milestone_id=load_billing_schedule.getLineItemValue('milestone', 'projecttask', kk);
						//alert('milestone_id======='+milestone_id);
						//alert('project_task======='+project_task);

						if(project_task == milestone_id)
							{
								//alert('condition match');
								//var amount=load_billing_schedule.getLineItemValue('milestone', 'comments', kk);//
								//alert('amount-->'+amount);
								//if(amount == null || amount == '' || amount == 'undefined')
								{
									var milestone_per=parseFloat(load_billing_schedule.getLineItemValue('milestone','milestoneamount', kk));
									alert('milestone_per: '+milestone_per);
									var amount=parseFloat(project_value*milestone_per/100);
									
									if(isNaN(amount))
										{
											amount=0;
										}
									
									//alert('amount--'+amount);
								}
								var due_date=load_billing_schedule.getLineItemValue('milestone','milestonedate', kk);
								nlapiSetFieldValue('custrecord_fpr_amount',amount);
								nlapiSetFieldValue('custrecord_fpr_date',due_date);
							}

						}//for close
				}//if close
		}//duplicate check count if close
	}
	//------------------------------------------------------
	if(name == 'custrecord_fpr_amount')
	{//if start

		//alert('i_project_id');
		var i_project_id=nlapiGetFieldValue('custrecordfpr_projparent');
		//alert('i_project_id'+i_project_id);

		var project_load=nlapiLoadRecord('job',i_project_id);

		var project_amt=project_load.getFieldValue('custentity_projectvalue');
		//alert('project_amt'+project_amt);

		var milestone_filter=new Array();
		milestone_filter[0]=new nlobjSearchFilter('custrecordfpr_projparent', null, 'is',i_project_id);

		var milestone_column=new Array();
		milestone_column[0]=new nlobjSearchColumn('custrecord_fpr_amount');

		var search_Milestone_Rec=nlapiSearchRecord('customrecord_fpr_milestone_wi', null, milestone_filter, milestone_column);
		//nlapiLogExecution('DEBUG','search_Milestone_Rec',search_Milestone_Rec.length);
		//alert('search_Milestone_Rec'+search_Milestone_Rec.length);

		if(search_Milestone_Rec != null)
		{
			for(var jj=0 ; jj<search_Milestone_Rec.length ; jj++)
			{

			  var i_amount=parseFloat(search_Milestone_Rec[jj].getValue('custrecord_fpr_amount'));
			 // alert('i_amount'+i_amount);
			// nlapiLogExecution('DEBUG','i_amount',i_amount);


			  i_sum_of_amt=i_sum_of_amt+i_amount;
			 	//alert('i_sum_of_amt'+i_sum_of_amt);

			}
		}
		var i_amount_new=parseFloat(nlapiGetFieldValue('custrecord_fpr_amount'));
		//alert('i_amount_new-->'+i_amount_new);
var maximum_allowed_value = (parseFloat(project_amt) - i_sum_of_amt).toFixed(2);
		 i_sum_of_amt=(i_sum_of_amt+i_amount_new).toFixed(2);


		 if((i_sum_of_amt) > parseFloat(project_amt))
			{
			 //alert('i_sum_of_amt'+i_sum_of_amt);

				alert('Amount Value Can Not Be Greater Than Project Amount Value. Maximum Allowed value is ' +  maximum_allowed_value);
				nlapiSetFieldValue('custrecord_fpr_amount','',false);
				return false;
			}
		 else
			 {
			 //alert('inside else');
			 }

	}//if close
	//}
	/*catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);
	}//CATCH*/
}
////////////////////////////////////////////////////////////////////////////////////
function validateLine_Practice(type)
{//validate fun start

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION
check revenue record tab ,effort expended and effort remaining field is not more than 100 or less than 100.

          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
	var i_sum_of_percentage=0;
	var count=0;
	//  VALIDATE LINE CODE BODY
//------------------------------------------------------------------------------------
	var i_practice_index=nlapiGetCurrentLineItemIndex('recmachcustrecord_fpr_prac_project_par');
	//alert('i_practice_index'+i_practice_index);
	nlapiLogExecution('DEBUG','i_practice_index',i_practice_index);

	var current_practice=parseFloat(nlapiGetCurrentLineItemValue('recmachcustrecord_fpr_prac_project_par','custrecord_fpr_practice'));
	//alert('current_practice ========'+current_practice);

	for(var jj=1 ; jj<=i_practice_index ; jj++)
		{

		  if(jj == i_practice_index)
			  {
			  	var i_percentage=parseFloat(nlapiGetCurrentLineItemValue('recmachcustrecord_fpr_prac_project_par','custrecord_fpr_percentage'));
				//alert('i_percentage'+i_percentage);
				nlapiLogExecution('DEBUG','i_percentage',i_percentage);

			  }
		  else
		  	{
			  var i_percentage=parseFloat(nlapiGetLineItemValue('recmachcustrecord_fpr_prac_project_par','custrecord_fpr_percentage',jj));
			 // alert('i_percentage'+i_percentage);
			  nlapiLogExecution('DEBUG','i_percentage',i_percentage);

			  var practice=parseFloat(nlapiGetLineItemValue('recmachcustrecord_fpr_prac_project_par','custrecord_fpr_practice',jj));
			 // alert('practice ========'+practice);

			  if(practice == current_practice)
				  {
				 // alert('inside if');
				  count=1;
				  }

		  	}

		  	i_sum_of_percentage=i_sum_of_percentage+i_percentage;

		}
	//alert('i_sum_of_percentage'+i_sum_of_percentage);
	nlapiLogExecution('DEBUG','i_sum_of_percentage',i_sum_of_percentage);

	if(i_sum_of_percentage <= '100' && count == '0')
		{//if start

		    return true;
		}//if close
	else
		{//else start
		   if(count == '1')
			   {
			   alert('Practice Can Not Be Duplicate');
			   nlapiSetCurrentLineItemValue('recmachcustrecord_fpr_prac_project_par','custrecord_fpr_practice','');
			   return false;
			   }
		   else
			   {
			   alert('Percentage Not More Than 100');
			   return false;
			   }


		}//else close

}//validate fun close
//-------------------------------------page init---------------------------
function  pageInit(type)
{
	//alert(type);
	if(type == 'edit')
		{
	      //alert('type');
	      //document.getElementById('newrec162').style.display='none';
		}

}
//--------------------------------------------------------------------------
function save_milestone()
{//if start
	var i_sum_of_percentage=0;
	var i_practice_count=nlapiGetCurrentLineItemIndex('recmachcustrecord_fpr_prac_project_par');
	//alert('i_practice_count'+i_practice_count);
	nlapiLogExecution('DEBUG','i_practice_count',i_practice_count);

	for(var jj=1 ; jj<=i_practice_count ; jj++)
		{
			  var i_percentage=parseFloat(nlapiGetLineItemValue('recmachcustrecord_fpr_prac_project_par','custrecord_fpr_percentage',jj));
			  //alert('i_percentage'+i_percentage);
			  if(isNaN(i_percentage))
				  {
				  i_percentage=0;
				  }
			  nlapiLogExecution('DEBUG','i_percentage',i_percentage);
				i_sum_of_percentage=i_sum_of_percentage+i_percentage;
				//alert('i_sum_of_percentage'+i_sum_of_percentage);

		 }
		if(i_sum_of_percentage < '100')
			{
			alert('Percentage Value Can Not Be Less Than 100');
			return false;
			}
		else{
			return true;
		}
		return true;
}//if close