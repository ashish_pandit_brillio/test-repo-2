/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */
 define(["N/record", "N/search", "N/file", "N/format", "./_get_time_lib.js","N/https"], function (record, search, file, format, utility,https) {
    function _post(context) {
        try {
            var datainObj = context;
            var filterArr = []
            log.debug({
                title: 'datainObj',
                details: datainObj
            });
            var currentDateAndTime = sysDate()
            log.debug({
                title: 'currentDateAndTime',
                details: currentDateAndTime
            });
            var receivedDate = datainObj.lastUpdatedTimestamp;
            log.debug({
                title: 'currentDateAndTime',
                details: currentDateAndTime
            });
            if(receivedDate){
                filterArr.push(search.createFilter({
                    "name": "lastmodified",
                    "operator": search.Operator.WITHIN,
                    "values": [receivedDate, currentDateAndTime]
                  }));
            }
            log.debug({
                title: 'filterAr',
                details: filterArr
            })
            var customrecord_taleo_location_masterSearchObj = search.create({
                type: "customrecord_fuel_skill_age",
                filters:filterArr,
                columns:
                [
                   
                   search.createColumn({name: "custrecord_fuel_skillage_skill", label: "frfSkill"}),
                   search.createColumn({name: "custrecord_fuel_skillage_practice", label: "Practice"}),
                   search.createColumn({name: "custrecord_fuel_frfage_hire_count", label: "Hire Count"}),
                   search.createColumn({name: "custrecord_fuel_frfage_classification", label: "Classification"}),
                   search.createColumn({name: "custrecord_fuel_frfage_location", label: "Location"}),
                   search.createColumn({name: "isinactive", label: "Inactive"})
                ]
            });
            var searchResultCount = customrecord_taleo_location_masterSearchObj.runPaged().count;
            var dataRow = [];
            var id;
            var frfSkill;
            var practice;
            var hireCount;
            var classification;
            var location;
            var isInactive;
            log.debug("customrecord_taleo_location_masterSearchObj result count", searchResultCount);
            customrecord_taleo_location_masterSearchObj.run().each(function (result) {
            var data = {};
                id =  result.id
                data.internalId = {
                    "name":id
                }
                isInactive = result.getValue({
                    name: 'isinactive'
                })
                data.isInactive = {
                    "name": isInactive
                }
                frfSkill = result.getText({
                    name: 'custrecord_fuel_skillage_skill'
                })
                data.frfSkill = {
                    "name": frfSkill
                }
                practice = result.getText({
                    name: 'custrecord_fuel_skillage_practice'
                })
                data.practice = {
                    "name":practice
                }
                hireCount = result.getValue({
                    name: 'custrecord_fuel_frfage_hire_count'
                })
                data.hireCount = {
                    "name": hireCount
                }
                classification = result.getValue({
                    name: 'custrecord_fuel_frfage_classification'
                })
                data.classification = {
                    "name": classification
                }
                location = result.getText({
                    name: 'custrecord_fuel_frfage_location'
                })
                data.location = {
                    "name": location
                }
                dataRow.push(data)
                return true;
            });
            return {
                timeStamp :currentDateAndTime,
                data :dataRow,
                status: true
            };
        } catch (error) {
            log.debug({
                title: error.name,
                details:error.message
            })
            return {
                timeStamp : '',
                data :error.name,
                status : false
            };
        }
    }
    return {
        post: _post,
    }
});
