							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : FUEL | UES Delivery Anchor Notification 
	Author      : Ashish Pandit
	Date        : 09 Oct 2019
    Description : User Event to update MariaDB on Field change of Delivery Anchor   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function afterSubmitCheckFields(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'create')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var i_customer = recordObject.getFieldValue('custrecord_fuel_anchor_customer');
			var i_employee = recordObject.getFieldValues('custrecord_fuel_anchor_employee');
			var i_deliveryAnchor = getEmpIds(i_employee);
			var s_practiceName = recordObject.getFieldText('custrecord_fuel_anchor_practice');
			var s_accountName = recordObject.getFieldText('custrecord_fuel_anchor_customer');
			
			var body = {};
			var method;
			method = "POST";
			body.internalId = Number(nlapiGetRecordId());
			body.practiceName = s_practiceName;
			body.accountName = s_accountName;
			if(i_deliveryAnchor.length > 0)
			{
				var emailArray = new Array();
				for(var i = 0;i<i_deliveryAnchor.length;i++)
				{
					//emailArray.push(nlapiLookupField("employee", i_deliveryAnchor[i], "email"));
					emailArray.push({"name" : nlapiLookupField("employee", i_deliveryAnchor[i], "email"),"id":Number(i_deliveryAnchor[i])});
				}
				body.emailId = emailArray;
			}
			else
				body.emailId = "";
			body.accountInternalId = Number(i_customer); 
			
			var url = "https://fuelnode1.azurewebsites.net/deliveryanchor";
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecord = nlapiGetOldRecord();
			
			var i_OldCustomer = oldRecord.getFieldValue('custrecord_fuel_anchor_customer');
			var i_OldEmployee = oldRecord.getFieldValues('custrecord_fuel_anchor_employee');
			var i_OldPractice = oldRecord.getFieldValue('custrecord_fuel_anchor_practice');
			
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var i_NewCustomer = recordObject.getFieldValue('custrecord_fuel_anchor_customer');
			var i_NewEmployee = recordObject.getFieldValues('custrecord_fuel_anchor_employee');
			nlapiLogExecution('debug','i_NewEmployee ',i_NewEmployee.length);
			var i_NewPractice = recordObject.getFieldValue('custrecord_fuel_anchor_practice');
			var i_deliveryAnchor = getEmpIds(i_NewEmployee);
			var s_practiceName = recordObject.getFieldText('custrecord_fuel_anchor_practice');
			var s_accountName = recordObject.getFieldText('custrecord_fuel_anchor_customer');
			
			if(i_OldCustomer != i_NewCustomer || JSON.stringify(i_OldEmployee) != JSON.stringify(i_NewEmployee) || i_OldPractice !=i_NewPractice)
			{
				var body = {};
				var method;
				method = "POST";
				body.internalId = Number(nlapiGetRecordId());
				body.practiceName = s_practiceName;
				body.accountName = s_accountName;
				if(i_deliveryAnchor.length > 0)
				{
					var emailArray = new Array();
					for(var i = 0;i<i_deliveryAnchor.length;i++)
					{
						//emailArray.push(nlapiLookupField("employee", i_deliveryAnchor[i], "email"));
						emailArray.push({"name" : nlapiLookupField("employee", i_deliveryAnchor[i], "email"),"id":Number(i_deliveryAnchor[i])});
						nlapiLogExecution('debug','i_deliveryAnchor[i] ',i_deliveryAnchor[i]);
					}
					body.emailId = emailArray;
				}
				else
					body.emailId = "";
				body.accountInternalId = Number(i_NewCustomer); 
				
				var url = "https://fuelnode1.azurewebsites.net/deliveryanchor";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response Update Mode: ", JSON.stringify(response));
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
function beforeSubmitCheckFields(type)
{
	 if(type == 'delete')
	{
		try
		{
			var i_recordId = nlapiGetRecordId();
			var i_user = nlapiGetFieldValues('custrecord_fuel_anchor_employee');
			nlapiLogExecution('debug','i_user ',i_user);
			var body = {};
			var method = "DELETE";
			/*
			var i_deliveryAnchor = getEmpIds(JSON.stringify(i_user));
			var emailArray = new Array();
			if(i_deliveryAnchor.length > 0)
			{
				for(var i = 0;i<i_deliveryAnchor.length;i++)
				{
					emailArray.push(nlapiLookupField("employee", parseInt(i_deliveryAnchor[i]), "email"));
				}
			}
			*/
			//body.regionName = nlapiGetFieldValue('name'); 
			var url = "https://fuelnode1.azurewebsites.net/deliveryanchor/"+i_recordId;
			//body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================

/************************************************/
function getEmpIds(s_employee)
{
	var resultArray = new Array();
	if(_logValidation(s_employee))
	{
		nlapiLogExecution('Debug','s_employee in function',s_employee);
		//var temp = s_employee.split(',');
		for(var i=0; i<s_employee.length;i++)
		{
			resultArray.push(s_employee[i]);
		}
	}
	nlapiLogExecution('debug','resultArray ',resultArray);
	return resultArray;
}