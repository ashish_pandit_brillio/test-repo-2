/**
 * @author Nikhil
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_Check_Status.js
	Author:
	Company:
	Date:
    Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_check_status()
{	
	var s_recordtype=nlapiGetRecordType();
	if (s_recordtype == 'invoice')
	 {
		var line_count=nlapiGetLineItemCount('item');
		for(var i=1;i<=line_count;i++)
		{
			var project = nlapiGetLineItemValue('item', 'job',i);
			if (project != null && project != '' && project != undefined) 
			{
				//var record_obj = nlapiLoadRecord('job', project);
			//	if (record_obj != null && record_obj != '' && record_obj != undefined) 
				{
				//	var status = record_obj.getFieldValue('entitystatus');
				
				      var a = new Array();
			          a['User-Agent-x'] = 'SuiteScript-Call';
					  
					  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=180&deploy=1&custscript_project_check_status=' + project, null, a);
			      		                    
					  var status = resposeObject.getBody();	
					nlapiLogExecution('debug', 'status', status);
					if (status == 4) 
					{
						alert('status is pending');
						return false;
					}
					if (status == 1) 
					{
						alert('status is closed');
						return false;
					}
					if (status == 17) 
					{
						return true;
					}
					if (status == 2) 
					{
						return true;
					}
				}
			}

		}
	}
	if (s_recordtype == 'timebill') 
	{
		var line_count = nlapiGetLineItemCount('timeitem');
		for (var i = 1; i <= line_count; i++) {
			var project = nlapiGetLineItemValue('timeitem', 'customer',i);
				if (project != null && project != '' && project != undefined) 
				{
				//	var record_obj = nlapiLoadRecord('job', project);
				//	if (record_obj != null && record_obj != '' && record_obj != undefined) 
					{
						
					  var a = new Array();
			          a['User-Agent-x'] = 'SuiteScript-Call';
					  
					  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=180&deploy=1&custscript_project_check_status=' + project, null, a);
			      		                    
					  var status = resposeObject.getBody();	
									
					//  var status = record_obj.getFieldValue('entitystatus');
					  
					if (status == 4) 
					{
						alert('status is pending');
						return false;
					}
					if (status == 1) 
					{
						alert('status is closed');
						return false;
					}
					if (status == 17) 
					{
						return true;
					}
					if (status == 2) 
					{
						return true;
					}
					}
				}
		}
	}

 return true;
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_check_status(type)
{
	var project;
	
	var s_recordtype=nlapiGetRecordType();
	
	if (s_recordtype == 'invoice') 
	{
		project = nlapiGetCurrentLineItemValue('item', 'job');
	}
	if(s_recordtype=='timebill')
	{
		project = nlapiGetCurrentLineItemValue('timeitem', 'customer');
	}
	
	//alert(' Project -->'+project)
	
		if (project != null && project != '' && project != undefined) 
		{
		//	var record_obj = nlapiLoadRecord('job', project);
			
		//	if (record_obj != null && record_obj != '' && record_obj != undefined) 
			{
		//		var status = record_obj.getFieldValue('entitystatus');
			//	alert(' Status -->'+status)
			
			var a = new Array();
	          a['User-Agent-x'] = 'SuiteScript-Call';
			  
			  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=180&deploy=1&custscript_project_check_status=' + project, null, a);
	      		                    
			  var status = resposeObject.getBody();	
				
					if (status == 4) 
					{
						alert('status is pending');
						return false;
					}
					if (status == 1) 
					{
						alert('status is closed');
						return false;
					}
					if (status == 17) 
					{
						return true;
					}
					if (status == 2) 
					{
						return true;
					}
			}
		}
	return true;
}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================




// END FUNCTION =====================================================
