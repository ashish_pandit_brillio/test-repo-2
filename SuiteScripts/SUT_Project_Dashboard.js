/**
 * Project Dashboard
 * 
 * Version Date Author Remarks 1.00 07 Jul 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var user = nlapiGetUser();

		// input from request
		var requestedProjectId = request.getParameter('project');
		var requestedStartDate = request.getParameter('startdate');
		var requestedEndDate = request.getParameter('enddate');

		// Testing data
		if (user == 41571) {
			requestedProjectId = [];
			requestedStartDate = '7/1/2018';
			requestedEndDate = '7/30/2018';
		}

		if (user == 41571) {
			user = 15195;
		}

		var taggedProjects = getTaggedProjectList(user);
		
		if (!requestedProjectId || requestedProjectId.length == 0) {
			requestedProjectId = [];

			taggedProjects.forEach(function(pr) {
				requestedProjectId.push(pr.getId());
			});
		}
		requestedProjectId.push(60962);
		var exchangeRateTable = {
		    INR : nlapiExchangeRate('INR', 'USD', requestedEndDate),
		    USD : 1,
		    GBP : nlapiExchangeRate('GBP', 'USD', requestedEndDate),
		    EUR : nlapiExchangeRate('EUR', 'USD', requestedEndDate)
		};

		// get invoice and collection details
		var collectionData = getCollectionDetails(requestedProjectId,
		        requestedStartDate, requestedEndDate);

		var pnlReport = nlapiResolveURL('SUITELET',
		        'customscript_sut_prj_profitability_rept', 'customdeploy1');
		var resourceForecastUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_resource_forecast',
		        'customdeploy_sut_resource_forecast');
		var revenueProjectionUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_job_month_wise_rev_proj',
		        'customdeploy_sut_job_month_wise_rev_proj');
		var allocationReportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_pm_weekly_allocation',
		        'customdeploy_sut_pm_weekly_allocation');
		var costMetricsUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_project_cost_metrics',
		        'customdeploy_sut_project_cost_metrics');
		var timesheetReportUrl = nlapiResolveURL('SUITELET',
		        'customscript_sut_timesheet_report_v2',
		        'customdeploy_sut_timesheet_report_v2');

		var projectSearch = getProjectTransactionFilterData(requestedProjectId);
		projectSearch.forEach(function(job) {
			pnlReport += '&project=' + job.getValue('entityid');
			resourceForecastUrl += "&custpage_selected_project=" + job.getId();
			revenueProjectionUrl += "&custpage_project=" + job.getId();
			costMetricsUrl += "&custpage_selected_project=" + job.getId();
			timesheetReportUrl += "&custpage_project=" + job.getId();
		});

		var htmlText = generateReport(request, requestedProjectId,
		        requestedStartDate, requestedEndDate, exchangeRateTable,
		        collectionData, pnlReport, resourceForecastUrl,
		        revenueProjectionUrl, allocationReportUrl, costMetricsUrl,
		        timesheetReportUrl);

		var form = nlapiCreateForm("Project Cockpit");
		form.addField('custpage_project_1', 'inlinehtml').setDefaultValue(
		        htmlText);
		form.setScript('customscript_js_sut_job_dashboard');

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function generateReport(request, projectList, startDate, endDate,
        exchangeRateTable, collectionData, pnlReportUrl, resourceForecastUrl,
        revenueProjectionUrl, allocationReportUrl, costMetricsUrl,
        timesheetReportUrl)
{
	try {
		var cssFileUrl = "https://system.na1.netsuite.com/core/media/media.nl?id=219990&c=3883006&h=cb39138a6ff301175cc3&mv=ir30u32w&_xt=.css&whence=";

		var context = nlapiGetContext();
		if (context.getEnvironment() == "SANDBOX") {

		} else if (context.getEnvironment() == "PRODUCTION") {

		}

		var htmlText = "";
		htmlText += "<link href='" + cssFileUrl + "' rel='stylesheet'/>";
		htmlText += "<table class='table-structure'>";

		// Tabs
		htmlText += "<tr class='pd-row-tabs'>";
		htmlText += "<td><div class='tab-disabled' id='divTabSummary'><p>Summary</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabInfo' onclick='displayTabData(\"divTabDataInfo\", \"divTabInfo\")'><p>List</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabCollection' onclick='displayTabData(\"divTabDataCollection\",\"divTabCollection\")'><p>Collection</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabPnL' onclick='displayTabData(\"divTabDataPnL\",\"divTabPnL\", \""
		        + pnlReportUrl + "\")'><p>P&L</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabAllocation' onclick='displayTabData(\"divTabDataAllocation\",\"divTabAllocation\", \""
		        + allocationReportUrl + "\")'><p>Allocation</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabCostMetrics' onclick='displayTabData(\"divTabDataCostMetrics\",\"divTabCostMetrics\", \""
		        + costMetricsUrl + "\")'><p>Cost Metrics</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabRevProjection' onclick='displayTabData(\"divTabDataRevProjection\",\"divTabRevProjection\", \""
		        + revenueProjectionUrl
		        + "\")'><p>Revenue Projection</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabRescForecast' onclick='displayTabData(\"divTabDataRescForecast\",\"divTabRescForecast\", \""
		        + resourceForecastUrl
		        + "\")'><p>Resource Forecast</p></div></td>";
		htmlText += "<td><div class='pd-tab' id='divTabTimesheet' onclick='displayTabData(\"divTabDataTimesheet\",\"divTabTimesheet\")'><p>Timesheet</p></div></td>";
		htmlText += "</tr>";

		// Data
		htmlText += "<tr class='pd-row-data'>";
		htmlText += "<td colspan='9'>";

		htmlText += "<div class='pd-tab-data' id='divTabDataSummary'>"
		        + getProjectSummary(projectList, startDate, endDate) + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataInfo'>"
		        + createProjectDetailsList(projectList) + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataCollection'>"
		        + collectionData + " </div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataPnL'>"
		        + "<iframe style='width:100%;height:600px;' src='"
		        + pnlReportUrl + "&ifrmcntnr=T'></iframe>" + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataAllocation'>"
		        + "<iframe style='width:100%;height:600px;' src='"
		        + allocationReportUrl + "&ifrmcntnr=T'></iframe>" + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataCostMetrics'>"
		        + "<iframe style='width:100%;height:600px;' src='"
		        + costMetricsUrl + "&ifrmcntnr=T'></iframe>" + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataRevProjection'>"
		        + "<iframe style='width:100%;height:600px;' src='"
		        + revenueProjectionUrl + "&ifrmcntnr=T'></iframe>" + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataRescForecast'>"
		        + "<iframe style='width:100%;height:600px;' src='"
		        + resourceForecastUrl + "&ifrmcntnr=T'></iframe>" + "</div>";
		htmlText += "<div class='pd-tab-data' id='divTabDataTimesheet'>"
		        + "<iframe style='width:100%;height:600px;' src='"
		        + timesheetReportUrl + "&ifrmcntnr=T'></iframe>" + "</div>";

		htmlText += "</td>";
		htmlText += "</tr>";
		htmlText += "</table>";

		return htmlText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'generateReport', err);
		throw err;
	}
}

function createProjectDetailsList(projectList) {
	try {
		var search = nlapiSearchRecord('job', null, [ new nlobjSearchFilter(
		        'internalid', null, 'anyof', projectList) ], [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('altname'),
		        new nlobjSearchColumn('entitystatus').setSort(),
		        new nlobjSearchColumn('jobtype'),
		        new nlobjSearchColumn('customer'),
		        new nlobjSearchColumn('billingschedule'),
		        new nlobjSearchColumn('jobbillingtype'),
		        new nlobjSearchColumn('custentity_t_and_m_monthly') ]);

		var html = "";

		if (search) {
			var context = nlapiGetContext();
			var projectDetailsUrl = nlapiResolveURL('SUITELET',
			        'customscript_sut_pm_project_details',
			        'customdeploy_sut_pm_project_details')
			        + "&pi=";
			var allocationUrl = nlapiResolveURL('SUITELET',
			        'customscript_sut_pm_weekly_allocation',
			        'customdeploy_sut_pm_weekly_allocation')
			        + "&project=";
			var pnlReport = nlapiResolveURL('SUITELET',
			        'customscript_sut_prj_profitability_rept', 'customdeploy1')
			        + "&project=";
			var resourceForecast = nlapiResolveURL('SUITELET',
			        'customscript_sut_resource_forecast',
			        'customdeploy_sut_resource_forecast')
			        + "&custpage_selected_project=";
			var revenueProjection = nlapiResolveURL('SUITELET',
			        'customscript_sut_job_month_wise_rev_proj',
			        'customdeploy_sut_job_month_wise_rev_proj')
			        + "&custpage_project=";
			var timesheetReport = nlapiResolveURL('SUITELET',
			        'customscript_sut_timesheet_report_v2',
			        'customdeploy_sut_timesheet_report_v2')
			        + "&custpage_project=";
			var opsMetrics = nlapiResolveURL('SUITELET',
			        'customscript_sut_project_cost_metrics',
			        'customdeploy_sut_project_cost_metrics')
			        + "&custpage_selected_project=";
			var milestoneList = nlapiResolveURL('SUITELET',
			        'customscript_sut_milestone_list',
			        'customdeploy_sut_milestone_list')
			        + "&pi=";

			search
			        .forEach(function(job) {
				        html += "<table class='project-info-table'>";

				        html += "<tr>";
				        html += "<td rowspan='7' style='width:30px;'><a title='More Project Info' target='_blank' href='"
				                + projectDetailsUrl
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=219996&c=3883006&h=12913b2451942c83436e&whence='/></a></td>";
				        html += "<td colspan='5'><b>Name : </b>"
				                + job.getValue('entityid') + " "
				                + job.getValue('altname') + "</td>";
				        html += "</tr>";

				        html += "<tr>";
				        html += "<td><b>Status : </b>"
				                + job.getText('entitystatus') + "</td>";
				        html += "<td rowspan='2'><a title='P&L Performance'  target='_blank' href='"
				                + pnlReport
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=219998&c=3883006&h=68624a40efe3c3f9f510&whence='/></a></td>";
				        html += "<td rowspan='2'><a target='_blank' title='Revenue Projection'  href='"
				                + revenueProjection
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=219999&c=3883006&h=db8e443ec28984e937a4&whence='/></a></td>";
				        html += "<td rowspan='2'><a target='_blank' title='Cost Metrics'  href='"
				                + opsMetrics
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=219999&c=3883006&h=db8e443ec28984e937a4&whence='/></a></td>";
				        html += "</tr>";

				        html += "<tr>";
				        html += "<td><b>Type : </b>" + job.getText('jobtype')
				                + "</td>";
				        html += "</tr>";

				        html += "<tr>";
				        html += "<td><b>Billing : </b>"
				                + job.getText('jobbillingtype')
				                + (job.getValue('custentity_t_and_m_monthly') == 'T' ? " Monthly"
				                        : "") + "</td>";
				        html += "<td rowspan='2'><a title='Allocation Details'  target='_blank' href='"
				                + allocationUrl
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=219997&c=3883006&h=4750274869bb5a1ef94e&whence='/></a></td>";
				        html += "<td rowspan='2'><a target='_blank' title='Resource Forecast'  href='"
				                + resourceForecast
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=220000&c=3883006&h=55a3d0f9ee9e51ea6882&whence='/></a></td>";
				        html += "<td rowspan='2'><a target='_blank' title='Timesheet Report'  href='"
				                + timesheetReport
				                + job.getId()
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=220002&c=3883006&h=551875a8c88fc27fb2cd&whence='/></a></td>";
				        html += "</tr>";

				        html += "<tr>";
				        html += "<td><b>Customer : </b>"
				                + job.getText('customer') + "</td>";
				        html += "</tr>";

				        html += "<tr>";
				        html += "<td></td>";
				        html += "<td rowspan='2'><a title='Milestone List'  target='_blank' href='"
				                + milestoneList
				                + job.getId()
				                + "&bs="
				                + job.getValue('billingschedule')
				                + "'>"
				                + "<img style='height:20px;width:20px;'"
				                + "src='https://system.na1.netsuite.com/core/media/media.nl?id=220204&c=3883006&h=5a6424ab5a2a74c1f0b7&whence='/></a></td>";
				        html += "<td rowspan='2'></td>";
				        html += "<td rowspan='2'></td>";
				        html += "</tr>";

				        html += "<tr>";
				        html += "<td></td>";
				        html += "</tr>";

				        html += "</table>";
			        });
		}

		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createProjectDetailsList', err);
		throw err;
	}
}

function getCollectionDetails(projectList, startDate, endDate) {
	try {

		nlapiLogExecution('debug', 'projectList', JSON.stringify(projectList));
		nlapiLogExecution('debug', 'startDate', startDate);
		nlapiLogExecution('debug', 'endDate', endDate);

		var invoiceSearch = nlapiSearchRecord('transaction', null, [
		        new nlobjSearchFilter('formuladate', null, 'within', startDate,
		                endDate).setFormula('{trandate}'),
		        new nlobjSearchFilter('name', null, 'anyof', projectList),
		        new nlobjSearchFilter('mainline', null, 'is', 'F'),
		        new nlobjSearchFilter('type', null, 'anyof', 'CustInvc') ], [
		        new nlobjSearchColumn('name', null, 'group').setSort(),
		        new nlobjSearchColumn('entity', null, 'group'),
		        new nlobjSearchColumn('trandate', null, 'group'),
		        new nlobjSearchColumn('tranid', null, 'group'),
		        new nlobjSearchColumn('statusref', null, 'group'),
		        new nlobjSearchColumn('amount', null, 'sum'),
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group') ]);

		var projectList = {};

		var invoiceTable = "";
		invoiceTable += "<tr class='pd-data-table-header'>";
		invoiceTable += "<td>";
		invoiceTable += "#";
		invoiceTable += "</td>";
		invoiceTable += "<td>";
		invoiceTable += "Date";
		invoiceTable += "</td>";
		invoiceTable += "<td>";
		invoiceTable += "Amount";
		invoiceTable += "</td>";
		invoiceTable += "<td>";
		invoiceTable += "Status";
		invoiceTable += "</td>";
		invoiceTable += "</tr>";

		if (invoiceSearch) {

			invoiceSearch.forEach(function(invoice) {
				var entityid = invoice.getValue('name', null, 'group');

				var rowHtml = "";
				rowHtml += "<tr class='pd-data-table-row'>";
				// rowHtml += "<td>";
				// rowHtml += invoice.getText('entityid', null, 'group');
				// rowHtml += "</td>";
				rowHtml += "<td>";
				rowHtml += invoice.getValue('tranid', null, 'group');
				rowHtml += "</td>";
				rowHtml += "<td>";
				rowHtml += invoice.getValue('trandate', null, 'group');
				rowHtml += "</td>";
				rowHtml += "<td>";
				rowHtml += invoice.getValue('fxamount', null, 'sum') + " "
				        + invoice.getText('currency', null, 'group');
				rowHtml += "</td>";
				rowHtml += "<td>";
				rowHtml += invoice.getText('statusref', null, 'group');
				rowHtml += "</td>";
				rowHtml += "</tr>";

				if (!projectList[entityid]) {
					projectList[entityid] = {
					    InvoiceList : [],
					    InvoiceHtml : '<tr><td colspan="4">'
					            + invoice.getText('name', null, 'group')
					            + '</td></tr>' + invoiceTable
					};
				}

				projectList[entityid].InvoiceList.push(invoice);
				projectList[entityid].InvoiceHtml += rowHtml;
			});
		}

		var finalHtml = "";

		for ( var p in projectList) {
			finalHtml += "<table class='pd-data-table'>";
			finalHtml += projectList[p].InvoiceHtml;
			finalHtml += "</table>";
		}

		return finalHtml;

		var totalPaidAmount = 0, totalUnpaidAmount = 0, totalPaidCount = 0, totalUnpaidCount = 0;

		var invoiceTable = "<table class='pd-table'>";
		invoiceTable += "<tr>";
		invoiceTable += "<td>";
		invoiceTable += "#";
		invoiceTable += "</td>";
		invoiceTable += "<td>";
		invoiceTable += "Date";
		invoiceTable += "</td>";
		invoiceTable += "<td>";
		invoiceTable += "Amount";
		invoiceTable += "</td>";
		invoiceTable += "<td>";
		invoiceTable += "Status";
		invoiceTable += "</td>";
		invoiceTable += "</tr>";

		if (invoiceSearch) {

			invoiceSearch
			        .forEach(function(invoice) {

				        invoiceTable += "<tr>";
				        invoiceTable += "<td>";
				        invoiceTable += invoice
				                .getText('entity', null, 'group');
				        invoiceTable += "</td>";
				        invoiceTable += "<td>";
				        invoiceTable += invoice.getValue('tranid', null,
				                'group');
				        invoiceTable += "</td>";
				        invoiceTable += "<td>";
				        invoiceTable += invoice.getValue('trandate', null,
				                'group');
				        invoiceTable += "</td>";
				        invoiceTable += "<td>";
				        invoiceTable += invoice.getValue('fxamount', null,
				                'sum')
				                + " "
				                + invoice.getText('currency', null, 'group');
				        invoiceTable += "</td>";
				        invoiceTable += "<td>";
				        invoiceTable += invoice.getText('statusref', null,
				                'group');
				        invoiceTable += "</td>";
				        invoiceTable += "</tr>";

				        if (invoice.getValue('statusref', null, 'group') == 'CustInvc:B') {
					        totalPaidAmount += parseFloat(invoice.getValue(
					                'fxamount', null, 'sum'));
					        totalPaidCount += 1;
				        } else {
					        totalUnpaidAmount += parseFloat(invoice.getValue(
					                'fxamount', null, 'sum'));
					        totalUnpaidCount += 1;
				        }
			        })

			nlapiLogExecution('debug', 'no. of invoices', invoiceSearch.length);
		} else {
			nlapiLogExecution('debug', 'no. of invoices', 0);
		}

		invoiceTable += "</table>";

		var html = "";

		html += "<ul class='data-list'>";
		html += "<li><div class='pd-column-label'>Payment Received</div><div class='pd-column-value'>"
		        + totalPaidAmount + "</div></li>";
		html += "<li><div class='pd-column-label'>Payment Pending</div><div class='pd-column-value'>"
		        + totalUnpaidAmount + "</div></li>";
		html += "<li><div class='pd-column-label'>Invoices Paid</div><div class='pd-column-value'>"
		        + totalPaidCount + "</div></li>";
		html += "<li><div class='pd-column-label'>Invoices Pending Payment</div><div class='pd-column-value'>"
		        + totalUnpaidCount + "</div></li>";
		html += "</ul>";
		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getCollectionDetails', err);
		throw err;
	}
}

function getDayDiff(fromDate, toDate) {
	fromDate = nlapiStringToDate(fromDate);
	toDate = nlapiStringToDate(toDate);

	if (fromDate > toDate) {
		return -999;
	}

	var dayDiff = 1;
	for (;; dayDiff++) {
		var newDate = nlapiAddDays(fromDate, dayDiff);

		if (newDate > toDate) {
			break;
		}
	}

	return dayDiff;
}

function formatCurrencyUSD(value) {
	return value ? (value.toFixed(2) + " USD") : ' - ';
}

function getCompareImage(a, b) {
	a = a ? a : 0;
	b = b ? b : 0;

	var image = a > b ? "https://system.na1.netsuite.com/core/media/media.nl?id=219993&c=3883006&h=61d652df419f29b89b8c&whence="
	        : "https://system.na1.netsuite.com/core/media/media.nl?id=219992&c=3883006&h=2f401667cfe9a8c54840&whence=";

	image = a == b ? "https://system.na1.netsuite.com/core/media/media.nl?id=220641&c=3883006&h=eaa79992cf94ebfd477b&whence="
	        : image;

	return "<img class='compare-image' src='" + image + "'/>"

}

function getMonthRange(startDate, endDate) {
	var d_startDate = nlapiStringToDate(startDate);
	var d_less_1_startDate = nlapiAddMonth(d_startDate, -1);
	var d_plus_1_startDate = nlapiAddMonth(d_startDate, 1);

	return [ formatDate1(d_less_1_startDate), formatDate1(d_startDate),
	        formatDate1(d_plus_1_startDate) ];
}

function formatDate1(d) {
	var s = nlapiDateToString(d, 'date');
	var ar = s.split('/');
	return ar[2] + "-" + (ar[0].length > 1 ? ar[0] : ("0" + ar[0]));
}

function getForecastMonths(selectedDate, noOfForecastMonths) {
	try {
		var currentDate = nlapiStringToDate(selectedDate);

		// get start date of the current month
		var d_firstStartDate = getMonthStartDate(currentDate);
		var arrDates = [];

		for (var i = 0; i < noOfForecastMonths; i++) {
			var newDate = nlapiAddMonths(d_firstStartDate, i);
			var s_startDate = nlapiDateToString(newDate, 'date');
			var s_endDate = getMonthEndDate(newDate);

			arrDates.push({
			    StartDate : s_startDate,
			    EndDate : s_endDate,
			    TotalDays : getDayDiff(s_startDate, s_endDate),
			    MonthName : ''// getMonthName(newDate)
			});
		}

		return arrDates;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getForecastMonths', err);
		throw err;
	}
}

function getAllocationDetails(arrDates, filterProjectList) {
	var mainObject = {};

	for (var i = 0; i < arrDates.length; i++) {
		var monthName = arrDates[i].MonthName;
		var m = formatDate1(nlapiStringToDate(arrDates[i].StartDate));

		var arrFilters = [
		        new nlobjSearchFilter('jobtype', 'job', 'anyof', '2'),
		        new nlobjSearchFilter('formuladate', null, 'notafter',
		                arrDates[i].EndDate).setFormula('{startdate}'),
		        new nlobjSearchFilter('formuladate', null, 'notbefore',
		                arrDates[i].StartDate).setFormula('{enddate}') ];

		if (filterProjectList && filterProjectList.length > 0) {
			arrFilters.push(new nlobjSearchFilter('project', null, 'anyof',
			        filterProjectList));
		}

		// search the allocation active during the period
		var allocationSearch = searchRecord('resourceallocation', null,
		        arrFilters, [ new nlobjSearchColumn('entityid', 'job'),
		                new nlobjSearchColumn('company'),
		                new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate'),
		                new nlobjSearchColumn('percentoftime') ]);

		for (var j = 0; j < allocationSearch.length; j++) {
			// get the person month
			var personMonth = getPersonUtilization(allocationSearch[j]
			        .getValue('startdate'), allocationSearch[j]
			        .getValue('enddate'), allocationSearch[j]
			        .getValue('percentoftime'), arrDates[i].StartDate,
			        arrDates[i].EndDate, arrDates[i].TotalDays, 8);

			var projectName = allocationSearch[j].getValue('entityid', 'job');

			nlapiLogExecution('debug', 'project name', projectName)

			if (!mainObject[projectName]) {
				mainObject[projectName] = {};
			}

			if (!mainObject[projectName][m]) {
				mainObject[projectName][m] = 0;
			}

			mainObject[projectName][m] += parseFloat(personMonth);
		}
	}

	nlapiLogExecution('debug', 'allocation data done', JSON
	        .stringify(mainObject));
	return mainObject;
}

function getProjectRevenueData(projectList, startDate, endDate) {
	try {
		var filters = [
		        new nlobjSearchFilter('custrecord_prp_project', null, 'anyof',
		                projectList),
		        new nlobjSearchFilter('custrecord_prp_start_date', null,
		                'within', startDate, endDate),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custrecord_prp_is_past', null, 'is', 'F') ];

		var revenueSearch = nlapiSearchRecord(
		        'customrecord_job_month_rev_projection',
		        null,
		        filters,
		        [
		                new nlobjSearchColumn('entityid',
		                        "CUSTRECORD_PRP_PROJECT", 'group'),
		                new nlobjSearchColumn('custrecord_prp_start_date',
		                        null, 'group').setFunction('month'),
		                new nlobjSearchColumn("custrecord_eprp_usd_revenue",
		                        'CUSTRECORD_EPRP_PROJECT_REV_PROJECTION', 'sum') ]);

		return revenueSearch;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectRevenueData', err);
		throw err;
	}
}

function getProjectSummary(projectList, startDate, endDate) {
	return "";
	try {
		var searchData = getProjectTransactionFilterData(projectList);
		var d_startDate = nlapiStringToDate(startDate);
		var d_less_1_startDate = nlapiAddMonths(d_startDate, -1);
		var d_plus_1_startDate = nlapiAddMonths(d_startDate, 1);
		var d_plus_1_endDate = nlapiAddDays(nlapiAddMonths(d_plus_1_startDate,
		        1), -1);

		var projectData = {};
		searchData
		        .forEach(function(job) {
			        var altname = job.getValue('altname');

			        projectData[job.getValue('entityid')] = {
				        Name : job.getValue('entityid') + " "
				                + altname.split(':')[1]
			        };
			        projectData[job.getValue('entityid')][formatDate1(d_less_1_startDate)] = {
			            Revenue : 0,
			            Cost : 0,
			            Margin : '-'
			        };
			        projectData[job.getValue('entityid')][formatDate1(d_startDate)] = {
			            Revenue : 0,
			            Cost : 0,
			            Margin : '-'
			        };
			        projectData[job.getValue('entityid')][formatDate1(d_plus_1_startDate)] = {
			            Revenue : 0,
			            Cost : 0,
			            Margin : '-'
			        };
		        });

		nlapiLogExecution('debug', 'project search done', JSON
		        .stringify(projectData));

		var revSearch = getProjectRevenueData(projectList, nlapiDateToString(
		        d_less_1_startDate, 'date'), nlapiDateToString(
		        d_plus_1_endDate, 'date'));

		if (revSearch) {

			revSearch.forEach(function(rev) {
				var projectId = rev.getValue('entityid',
				        "CUSTRECORD_PRP_PROJECT", 'group');
				var amount = parseFloat(rev.getValue(
				        "custrecord_eprp_usd_revenue",
				        'CUSTRECORD_EPRP_PROJECT_REV_PROJECTION', 'sum'));
				var month = rev.getValue('custrecord_prp_start_date', null,
				        'group');

				projectData[projectId][month].Revenue += amount;
			});
		}

		nlapiLogExecution('debug', 'projection search done', JSON
		        .stringify(projectData));

		// Cost Search
		var filter = createSummaryCostFilter(projectList, startDate, endDate,
		        searchData);
		getCostSummary(projectData, filter, endDate);

		nlapiLogExecution('debug', 'cost data added');

		// margin calculation
		for ( var p in projectData) {

			for ( var m in projectData[p]) {
				if (projectData[p][m].Cost > 0) {
					projectData[p][m].Margin = ((projectData[p][m].Revenue - projectData[p][m].Cost) / projectData[p][m].Cost) * 100;
				}
			}
		}

		nlapiLogExecution('debug', 'data', JSON.stringify(projectData));

		// resource forecast
		var arrDates = getForecastMonths(nlapiDateToString(d_less_1_startDate),
		        3);
		var allocationData = getAllocationDetails(arrDates, projectList);

		var html = "";
		html += "<table class='summary-table'>";
		html += "<tr class='pd-data-table-header'>";

		html += "<td>";
		html += "Project";
		html += "</td>";

		html += "<td>";
		html += "Last Month Revenue";
		html += "</td>";

		html += "<td>";
		html += "Last Month Cost";
		html += "</td>";

		html += "<td>";
		html += "Revenue";
		html += "</td>";

		html += "<td>";
		html += "Cost";
		html += "</td>";

		html += "<td>";
		html += "Margin";
		html += "</td>";

		html += "<td>";
		html += "Next Month Revenue";
		html += "</td>";

		html += "<td>";
		html += "Next Month Cost";
		html += "</td>";

		html += "<td>";
		html += "Allocation (Last Month)";
		html += "</td>";

		html += "<td>";
		html += "Allocation (This Month)";
		html += "</td>";

		html += "<td>";
		html += "Allocation (Next Month)";
		html += "</td>";

		html += "</tr>";

		for ( var p in projectData) {
			html += "<tr class='pd-data-table-row in-progress'>";

			html += "<td>";
			html += projectData[p].Name;
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_less_1_startDate)].Revenue);
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_less_1_startDate)].Cost);
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_startDate)].Revenue);
			html += getCompareImage(
			        projectData[p][formatDate1(d_startDate)].Revenue,
			        projectData[p][formatDate1(d_less_1_startDate)].Revenue);
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_startDate)].Cost);
			html += getCompareImage(
			        projectData[p][formatDate1(d_startDate)].Cost,
			        projectData[p][formatDate1(d_less_1_startDate)].Cost);
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_startDate)].Margin);
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_plus_1_startDate)].Revenue);
			html += getCompareImage(
			        projectData[p][formatDate1(d_plus_1_startDate)].Revenue,
			        projectData[p][formatDate1(d_startDate)].Revenue);
			html += "</td>";

			html += "<td>";
			html += formatFloat(projectData[p][formatDate1(d_plus_1_startDate)].Cost);
			html += getCompareImage(
			        projectData[p][formatDate1(d_plus_1_startDate)].Cost,
			        projectData[p][formatDate1(d_startDate)].Cost);
			html += "</td>";

			// Allocation
			if (allocationData[p]) {
				html += "<td>";
				html += allocationData[p][formatDate1(d_less_1_startDate)] ? formatFloat(allocationData[p][formatDate1(d_less_1_startDate)])
				        : '-';
				html += "</td>";

				html += "<td>";
				html += allocationData[p][formatDate1(d_startDate)] ? formatFloat(allocationData[p][formatDate1(d_startDate)])
				        : '-';
				html += getCompareImage(
				        allocationData[p][formatDate1(d_startDate)],
				        allocationData[p][formatDate1(d_less_1_startDate)]);
				html += "</td>";

				html += "<td>";
				html += allocationData[p][formatDate1(d_plus_1_startDate)] ? formatFloat(allocationData[p][formatDate1(d_plus_1_startDate)])
				        : '-';
				html += getCompareImage(
				        allocationData[p][formatDate1(d_plus_1_startDate)],
				        allocationData[p][formatDate1(d_startDate)]);
				html += "</td>";
			} else {
				html += "<td>-</td>";
				html += "<td>-</td>";
				html += "<td>-</td>";
			}

			html += "</tr>";
		}

		html += "</table>";
		return html;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectSummary', err);
		throw err;
	}
}

function getMonthEndDate(currentDate) {
	return nlapiDateToString(nlapiAddDays(nlapiAddMonths(currentDate, 1), -1),
	        'date');
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getProjectTransactionFilterData(projectList) {
	return nlapiSearchRecord('job', null, new nlobjSearchFilter('internalid',
	        null, 'anyof', projectList), [ new nlobjSearchColumn('entityid'),
	        new nlobjSearchColumn('altname'),
	        new nlobjSearchColumn('jobbillingtype'),
	        new nlobjSearchColumn('subsidiary') ]);
}

function createSummaryRevenueFilter(projectList, startDate, endDate, searchData)
{
	var filter = [];

	for (var i = 0; i < searchData.length; i++) {
		var project = searchData[i];

		var projectFilter = [];
		projectFilter.push([ 'custcolprj_name', 'startswith',
		        (project.getValue('entityid') + " ") ]);
		projectFilter.push('AND');

		if (project.getValue('jobbillingtype') == 'TM') {
			projectFilter.push([ 'account', 'anyof', [ '647', '644' ] ]);
		} else if (project.getValue('jobbillingtype') == 'FBM'
		        || project.getValue('jobbillingtype') == 'FBI') {

			if (project.getValue('subsidiary') == 2) {
				projectFilter.push([ 'account', 'anyof', [ '773' ] ]);
			} else {
				projectFilter.push([ 'account', 'anyof', [ '773', '732' ] ]);
			}
		}

		filter.push(projectFilter);

		if (i + 1 < searchData.length) {
			filter.push('OR');
		}
	}

	return [ [ 'trandate', 'within', '5/1/2016', '7/31/2016' ], 'AND', filter ];
}

function createSummaryCostFilter(projectList, startDate, endDate, searchData) {
	var filter = [];

	for (var i = 0; i < searchData.length; i++) {
		var project = searchData[i];

		var projectFilter = [];
		filter.push([ 'custcolprj_name', 'startswith',
		        (project.getValue('entityid') + " ") ]);

		if (i + 1 < searchData.length) {
			filter.push('OR');
		}
	}

	return [ [ 'trandate', 'within', startDate, endDate ], 'AND', filter ];
}

function getCostSummary(projectData, filter, endDate) {

	var transactionSearch = nlapiSearchRecord('transaction', 1435, filter,
	        [ new nlobjSearchColumn('trandate').setFunction('month') ]);

	var exchangeRateTable = {
	    INR : nlapiExchangeRate('INR', 'USD', endDate),
	    USD : 1,
	    GBP : nlapiExchangeRate('GBP', 'USD', endDate),
	    EUR : nlapiExchangeRate('EUR', 'USD', endDate)
	};

	// loop and divide in onsite / offsite cost
	for (var i = 0; i < transactionSearch.length; i++) {
		var category = transactionSearch[i].getValue('formulatext');
		var amount = parseFloat(transactionSearch[i].getValue('fxamount'));
		var currency = transactionSearch[i].getText('currency');
		var convertedAmount = exchangeRateTable[currency] * amount;
		var subsidiary = transactionSearch[i].getValue('subsidiary');
		var projectName = transactionSearch[i].getValue('custcolprj_name');
		var projectId = projectName.split(' ')[0];
		var month = transactionSearch[i].getValue('trandate');

		switch (category) {
			case "Salary Cost":
			case "Contractor Cost":
			case "Travel":
			case "Others":

				if (!projectData[projectId]) {
					projectData[projectId] = {}
				}

				if (!projectData[projectId][month]) {
					projectData[projectId][month] = {
					    Revenue : 0,
					    Cost : 0,
					    Margin : '-'
					}
				}

				projectData[projectId][month].Cost += convertedAmount;
			break;
		}
	}
}

function formatFloat(v) {
	var p = parseFloat(v);
	return isNaN(p) ? v : Math.round(p);
}