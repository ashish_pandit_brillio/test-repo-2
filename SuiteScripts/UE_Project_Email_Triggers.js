/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Feb 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord project
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	try {
		nlapiLogExecution('debug', 'type', type);
		if (type == "create") {
			var i_project_id = nlapiGetRecordId();
			nlapiLogExecution('debug', 'Project Id', i_project_id);

			// get the project details
			var o_project_details = {
				project_name : nlapiGetFieldValue('companyname'),
				description : nlapiGetFieldValue('comments'),
				project_manager : nlapiGetFieldText('custentity_projectmanager'),
				start_date : nlapiGetFieldValue('startdate'),
				end_date : nlapiGetFieldValue('enddate'),
				project_type : nlapiGetFieldText('jobtype'),
				vertical : nlapiGetFieldText('custentity_vertical'),
				executing_unit : nlapiGetFieldText('custentity_practice'),
				delivery_manager : nlapiGetFieldText('custentity_deliverymanager'),
				account : nlapiGetFieldText('parent'),
				overall_expense : nlapiGetFieldValue('custentity_projectvalue'),
				currency : nlapiGetFieldText('custentity_project_currency')
			};

			// get list of all employees to whom this mail is to be send
			var a_employee_list = getRecepientList();

			// send emails
			var o_employee_details = null;
			var o_mail_content = null;
			a_employee_list.forEach(function(i_employee_id) {
				o_employee_details = nlapiLookupField('employee', i_employee_id, [ 'firstname',
					'email', 'internalid' ]);
				o_mail_content = projectCreatedMailTemplate(o_project_details, o_employee_details);
				nlapiSendEmail(constant.Mail_Author.InformationSystem, o_employee_details.email,
						o_mail_content.Subject, o_mail_content.Body, null,
						'nitish.mishra@brillio.com', {
							'entity' : i_project_id
						});
				nlapiLogExecution('debug', 'Send To', o_employee_details.email);
			});
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'Project Id : ' + nlapiGetRecordId(), err);
		throw err;
	}
}

function projectCreatedMailTemplate(o_project_details, o_employee_details) {
	var s_mail_body = "";
	s_mail_body += "<p>Hi " + o_employee_details.firstname + ",</p>";
	s_mail_body += "<p>A new project has been created in NetSuite with the following details : <br/>";
	s_mail_body += "<ul>";
	s_mail_body += "<li><b>Project Name : </b>" + checkEmptyForMail(o_project_details.project_name)
			+ "</li>";
	s_mail_body += "<li><b>Vertical : </b>" + checkEmptyForMail(o_project_details.vertical)
			+ "</li>";
	s_mail_body += "<li><b>Executing Practice : </b>"
			+ checkEmptyForMail(o_project_details.executing_unit) + "</li>";
	s_mail_body += "<li><b>Description : </b>" + checkEmptyForMail(o_project_details.description)
			+ "</li>";
	s_mail_body += "<li><b>Start Date : </b>" + checkEmptyForMail(o_project_details.start_date)
			+ "</li>";
	s_mail_body += "<li><b>End Date : </b>" + checkEmptyForMail(o_project_details.end_date)
			+ "</li>";
	s_mail_body += "<li><b>Type : </b>" + checkEmptyForMail(o_project_details.project_type)
			+ "</li>";
	s_mail_body += "<li><b>Account : </b>" + checkEmptyForMail(o_project_details.account) + "</li>";

	s_mail_body += "<li><b>Project Manager : </b>"
			+ checkEmptyForMail(o_project_details.project_manager) + "</li>";
	s_mail_body += "<li><b>Delivery Manager : </b>"
			+ checkEmptyForMail(o_project_details.delivery_manager) + "</li>";

	s_mail_body += "<li><b>Overall / Project Value : </b>"
			+ (isNotEmpty(o_project_details.overall_expense) ? (o_project_details.overall_expense
					+ " " + o_project_details.currency) : "-") + "</li>";

	s_mail_body += "</ul>";
	s_mail_body += "</p>";
	s_mail_body += "<p>Regards,<br/>Information Systems</p>";

	return {
		Subject : 'New Project Created',
		Body : addMailTemplate(s_mail_body)
	};
}

function checkEmptyForMail(value) {
	return isNotEmpty(value) ? value : '';
}

function getRecepientList() {
	var i_athresh_murali_krishnappa = "7932";
	var i_smitha_thumbikkat = "1677";
	return [ i_athresh_murali_krishnappa, i_smitha_thumbikkat ];
}