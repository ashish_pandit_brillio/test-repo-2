// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Salary_Upload_Update.js
	Author      : Shweta Chopde
	Date        : 6 Aug 2014
    Description : Update the JEs created


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
  var i_file_status = '';
  
  try
  {
    var i_context = nlapiGetContext();  
	var i_CSV_File_ID =  i_context.getSetting('SCRIPT','custscript_csv_file_id_update');
	var i_monthly_employee =  i_context.getSetting('SCRIPT','custscript_monthly_employee_update');
	var i_hourly_employee =  i_context.getSetting('SCRIPT','custscript_hourly_employee_update');
	var i_account_debit =  i_context.getSetting('SCRIPT','custscript_account_debit_update');
	var i_account_credit =  i_context.getSetting('SCRIPT','custscript_account_credit_update');
	var i_recordID =  i_context.getSetting('SCRIPT','custscript_record_id_update');
	var a_JE_Array =  i_context.getSetting('SCRIPT','custscript_je_array_update');
					
	nlapiLogExecution('DEBUG', 'schedulerFunction',' Record ID -->' + i_recordID);	
	nlapiLogExecution('DEBUG', 'schedulerFunction',' CSV File ID -->' + i_CSV_File_ID);
    nlapiLogExecution('DEBUG', 'schedulerFunction',' Monthly Employee -->' + i_monthly_employee);
    nlapiLogExecution('DEBUG', 'schedulerFunction',' Hourly Employee -->' + i_hourly_employee);
    nlapiLogExecution('DEBUG', 'schedulerFunction',' Account Credit -->' + i_account_debit);
    nlapiLogExecution('DEBUG', 'schedulerFunction',' Account Debit -->' + i_account_credit);
		
	 var a_CRT_array_values = new Array()	
	 var i_data_CRT = new Array()
	 i_data_CRT =  a_JE_Array;
     
	 if(_logValidation(i_data_CRT)) 
	 {
	 	  for(var dct=0;dct<i_data_CRT.length;dct++)
		  {
			 	a_CRT_array_values = i_data_CRT.split(',')
			    break;				
		  }	
	 	
	 }
     
	nlapiLogExecution('DEBUG', 'schedulerFunction','CRT Array Values-->' +a_CRT_array_values);
	nlapiLogExecution('DEBUG', 'schedulerFunction',' CRT Array Values Length-->' + a_CRT_array_values.length);	
		
	// ============ SALARIED EMPLOYEE - FILE PROCESS ===================
	if(i_monthly_employee == 'T')
	{					
    	i_file_status = monthly_employee_file_process_data(i_CSV_File_ID,i_recordID,i_account_debit,i_account_credit,i_hourly_employee,i_monthly_employee,a_CRT_array_values);				
	}//Salaried Employee File Processing
	
	// ================ HOURLY EMPLOYEE - FILE PROCESS =====================
	if(i_hourly_employee == 'T')
	{
		i_file_status = hourly_employee_file_process_data(i_CSV_File_ID,i_recordID,i_account_debit,i_account_credit,i_hourly_employee,i_monthly_employee,a_CRT_array_values)			
	}//Hourly Employee File Processing	
				
		
  }
   catch(exception)
   {
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
   }
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function decode_base64(s)
{
	var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
	var n = [[65, 91], [97, 123], [48, 58], [47, 48], [43, 44]];
	
	for (z in n) 
	{
	    for (i = n[z][0]; i < n[z][1]; i++) 
		{
	        v.push(w(i));
	    }
	}
	for (i = 0; i < 64; i++) 
	{
	    e[v[i]] = i;
	}
	
	for (i = 0; i < s.length; i += 72) 
	{
	    var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
	    for (x = 0; x < o.length; x++) 
		{
	        c = e[o.charAt(x)];
	        b = (b << 6) + c;
	        l += 6;
	        while (l >= 8) 
			{
	            r += w((b >>> (l -= 8)) % 256);
	        }
	    }
	}
	return r;
}

//-------------------------------------------------------------------------------------------------------------------------

function parseCSV (csvString) 
{
	var fieldEndMarker  = /([,\015\012] *)/g; /* Comma is assumed as field separator */
	var qFieldEndMarker = /("")*"([,\015\012] *)/g; /* Double quotes are assumed as the quote character */
	var startIndex = 0;
	var records = [], currentRecord = [];
    do 
    {
    // If the to-be-matched substring starts with a double-quote, use the qFieldMarker regex, otherwise use fieldMarker.
    var endMarkerRE = (csvString.charAt (startIndex) == '"')  ? qFieldEndMarker : fieldEndMarker;
    endMarkerRE.lastIndex = startIndex;
    var matchArray = endMarkerRE.exec (csvString);
    if (!matchArray || !matchArray.length) 
	{
        break;
    }
    var endIndex = endMarkerRE.lastIndex - matchArray[matchArray.length-1].length;
    var match = csvString.substring (startIndex, endIndex);
    if (match.charAt(0) == '"') 
	{ // The matching field starts with a quoting character, so remove the quotes
        match = match.substring (1, match.length-1).replace (/""/g, '"');
    }
    currentRecord.push (match);
    var marker = matchArray[0];
    if (marker.indexOf (',') < 0)
	{ // Field ends with newline, not comma
        records.push (currentRecord);
        currentRecord = [];
    }
    startIndex = endMarkerRE.lastIndex;
    }   while (true);
	if (startIndex < csvString.length)
	{ // Maybe something left over?
	    var remaining = csvString.substring (startIndex).trim();
	    if (remaining) currentRecord.push (remaining);
	}
	if (currentRecord.length > 0)
	{ // Account for the last record
	    records.push (currentRecord);
	}
	return records;
};


function search_employee(i_employeeID)
{
	var i_employee_entity_ID ='';
	var i_subsidiary ='';
	var i_department='';
	var i_location = '';
	var i_location_txt = '';
	var i_currency = '';
	var i_subsidiary_txt = '';
	var i_recordID='';
	
	var a_return_array = new Array();
	
	if(_logValidation(i_employeeID))
	{
     var filter = new Array();
	 filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employeeID);
	 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('entityid');
	columns[1] = new nlobjSearchColumn('subsidiary');
	columns[2] = new nlobjSearchColumn('department');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('internalid');
		
	var a_search_results = nlapiSearchRecord('employee', null, filter, columns);
	
	if (a_search_results != null && a_search_results != '' && a_search_results != undefined) 
	{
		i_employee_entity_ID = a_search_results[0].getValue('entityid');
		nlapiLogExecution('DEBUG', 'search_employee', ' Employee ID -->' + i_employee_entity_ID);
		
		i_recordID = a_search_results[0].getText('internalid');
		nlapiLogExecution('DEBUG', 'search_employee', ' Record ID-->' + i_recordID);
							
		i_subsidiary_txt = a_search_results[0].getText('subsidiary');
		nlapiLogExecution('DEBUG', 'search_employee', ' Employee Subsidiary Txt-->' + i_subsidiary_txt);
		
		i_subsidiary = a_search_results[0].getValue('subsidiary');
		nlapiLogExecution('DEBUG', 'search_employee', ' Employee Subsidiary -->' + i_subsidiary);		
					
		i_department = a_search_results[0].getValue('department');
		nlapiLogExecution('DEBUG', 'search_employee', ' Employee Department -->' + i_department);
		
		i_location = a_search_results[0].getValue('location');
		nlapiLogExecution('DEBUG', 'search_employee', ' Employee Location -->' + i_location);
		
		i_location_txt = a_search_results[0].getText('location');
		nlapiLogExecution('DEBUG', 'search_employee', ' Employee Location Txt-->' + i_location_txt);
			
	}//Search Results		
	}//Employee ID	
	
	a_return_array[0] = i_employee_entity_ID+'&&##&&'+i_subsidiary+'&&##&&'+i_department+'&&##&&'+i_location+'&&##&&'+i_location_txt+'&&##&&'+i_subsidiary_txt+'&&##&&'+i_recordID;           
	
	return a_return_array;
}//Search Employee

function check_for_numeric_value(i_number)
{ 
    var result = '';
    if(_logValidation(i_number))
	{
		result = isNaN(i_number)
		nlapiLogExecution('DEBUG', 'check_no_of_days',' No Of Days Number / Text -->' + result);	
				
	}//Number Validation  
	return result;		
}// Check if a number is a numeric or it contains text

function get_subsidiary_text(i_subsidiary)
{
  var s_subsidiary = '';	
 if(_logValidation(i_subsidiary))
 {
 	var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary',i_subsidiary);
	
	if(_logValidation(o_subsidiaryOBJ))
	{
		s_subsidiary = o_subsidiaryOBJ.getFieldValue('name');
		nlapiLogExecution('DEBUG', 'get_subsidiary_text',' Subsidiary -->' + s_subsidiary);	
		
	}//Subsidiary OBJ
 }//Subsidiary	
 return s_subsidiary;
}//Get Subsidiary

function check_month_year_format(i_month_year)
{
	var is_month = false;
	var s_error_1 = '';
	var s_error_2 = '';
	var s_error = '';
	if(_logValidation(i_month_year))
	{
	  var a_split_m_y_array = new Array();
	  
	  if(i_month_year.indexOf('-')>-1)
	  {
		  a_split_m_y_array = i_month_year.split('-');
		  
		  var i_month = a_split_m_y_array[0];
		  
		  var i_year = a_split_m_y_array[1];
		  
		  var i_year_format = check_for_numeric_value(i_year);
		  nlapiLogExecution('DEBUG', 'check_month_year_format',' Year Format -->' + i_year_format);			
			
		  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
		  {
		  	is_month = true ;		
		  }		
		  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'May' || i_month == 'MAY')
		  {
		  	is_month = true ;		
		  }	  
		  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
		  {
		 	is_month = true ;		
		  }	
		  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
		  {
		  	is_month = true ;		
		  }  
		  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
		  {
		 	is_month = true ;		
		  }	
		  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
		  {
		  	is_month = true ;		
		  }	
					
			if(is_month == false)
			{
				s_error_1 = 'Invalid Year : Year is not in number format . \n' 			
			}
			if(i_year_format == true)
			{
				s_error_2 = ' Invalid Month : Month is not in MM format .\n'
			}			
		    s_error = s_error_1+''+s_error_2;		  	
	  }// Contains -
	  else
	  {	  	 
	   s_error+=' Invalid Date Format : Date is not in MM-YYYY Format . \n';
	   nlapiLogExecution('DEBUG', 'check_month_year_format',' Error -->' + s_error);	
	  }		
	}//Month & Year	
	return s_error;
}//Month Year Format


function check_currency(i_currency)
{
	var i_currencyID = '';
	if(_logValidation(i_currency))
	{
		var filters = new Array();
	    filters[0] = new nlobjSearchFilter('name', null, 'is',i_currency)
	    
	    var column= new Array();	
	    column[0]= new nlobjSearchColumn('internalid')
	 
	    var a_results = nlapiSearchRecord('currency',null,filters,column);
		
		if(_logValidation(a_results))
		{		
		i_currencyID = a_results[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'check_currency', ' Currency ID -->' + i_currencyID);	
			
		}//		
	}//Currency V	
	return i_currencyID;
}//Currency


function hourly_employee_file_process_data(i_CSV_File_ID,i_recordID,i_account_debit,i_account_credit,i_hourly_employee,i_monthly_employee,a_CRT_array_values)
{
	var i_hnt = 0;
	var i_file_status = '';
	var i_ent = 0;
	var i_file_status = 'SUCCESS';
	if(_logValidation(i_CSV_File_ID))
	{				
		//==================== Read CSV File Contents =========================
					
		var o_fileOBJ = nlapiLoadFile(i_CSV_File_ID);
        
		var s_file_contents = o_fileOBJ.getValue();
        nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', 'Type -->' + o_fileOBJ.getType());
        
		if (o_fileOBJ.getType() == 'EXCEL')
		{
	        nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', '@@@@@@@@@@@  Excel File @@@@@@@@@@@ ');
	        var filedecodedrows = decode_base64(s_file_contents);
	    }
	    else
		{
	        nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', '@@@@@@@@@@@  Other Than Excel File @@@@@@@@@@@ ');
	        var filedecodedrows = s_file_contents;				       
	    }
		var a_file_array =  filedecodedrows.split(/\n/g);
        nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' No. Of File Rows -->' + a_file_array.length);
  
		var i_data_length = parseInt(a_file_array.length) - parseInt(1);
		nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Data Length -->' + i_data_length);
		 
		for (var i = 1 ; i <i_data_length; i++) 
		{
			  var s_error = '';
			  var s_error_1 ='';
			  var s_error_2 ='';
			  var s_error_3 ='';
			  var s_error_4 ='';
			  var s_error_5 ='';
			  var s_error_6 ='';
			  var s_error_7 ='';
			  var s_error_8 ='';
			  var s_error_9 ='';
			  
			  var s_error_fld = '';
			  var s_error_1_fld ='';
			  var s_error_2_fld ='';
			  var s_error_3_fld ='';
			  var s_error_4_fld ='';
			  var s_error_5_fld ='';
			  var s_error_6_fld ='';
			  var s_error_7_fld ='';
			  var s_error_8_fld ='';
			  var s_error_9_fld ='';		
			  
			  var s_error_a ='';
			  var s_error_b ='';
			  var s_error_c ='';
			  var s_error_d ='';
			  var s_error_e ='';
			  var s_error_f ='';
			  var s_error_g ='';
			  var s_error_h ='';
			  var s_error_i ='';
			  
			  var s_error_a_fld ='';
			  var s_error_b_fld ='';
			  var s_error_c_fld ='';
			  var s_error_d_fld ='';
			  var s_error_e_fld ='';
			  var s_error_f_fld ='';
			  var s_error_g_fld ='';
			  var s_error_h_fld ='';
			  var s_error_i_fld ='';
			  	  
		  
			a_file_array[i] = parseCSV(a_file_array[i]);
						
			var i_temp_data = a_file_array[i].toString()
			
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', 'Temp Data -->' + i_temp_data);
						
			var a_row_data = i_temp_data.split(',')
			
			var i_month_year = a_row_data[0]
			
			var i_subsidiary = a_row_data[1]
			
			var i_employeeID = a_row_data[2]			
			
			var i_location = a_row_data[3]
			
			var i_currency = a_row_data[4]
								
			var i_revised_date = a_row_data[5]
			
			var i_ST_Rate_per_hour = a_row_data[6]
			
			var i_OT_Rate_per_hour = a_row_data[7]
			
			
					
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee ID -->' + i_employeeID);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Month & Year -->' + i_month_year);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary -->' + i_subsidiary);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location -->' + i_location);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Currency -->' + i_currency);			
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Revised Date -->' + i_revised_date);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ST Rate Per Hour -->' + i_ST_Rate_per_hour);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' OT Rate Per Hour -->' + i_OT_Rate_per_hour);
			
			var i_month;
		    var i_year;
		
		  var a_split_m_y_array = new Array();
	  
	      if(_logValidation(i_month_year))
		  {
		  	 if (i_month_year.indexOf('-') > -1) 
			  {
			  	a_split_m_y_array = i_month_year.split('-');
			  	
			  	i_month = a_split_m_y_array[0];
			  	
			  	i_year = a_split_m_y_array[1];
			  }//Index Of 		  	
		  }//Month & Year
		  
		  nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Month -->' + i_month);			
		  nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Year -->' + i_year);			
		 								
										
		   if(_logValidation(i_revised_date))
		   {
		   	 i_revised_date = nlapiStringToDate(i_revised_date);
		     i_revised_date = nlapiDateToString(i_revised_date);
		     nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Revised Date -->' + i_revised_date);			
		 
		   }								
			var i_currency_c = check_currency(i_currency);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Currency -->' + i_currency_c);
						
			var a_employee_details = search_employee(i_employeeID)
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee Details -->' + a_employee_details);
			
			var a_emp_split_array = new Array();
			var i_practice;
			var i_subsidiary_e;
			var i_employeeID;
			var i_location_e;
			var i_location_txt_e;
			var i_currency_e;
			var i_subsidiary_txt_e;
			var i_employee_recID;
		
			if(_logValidation(a_employee_details))
			{
				a_emp_split_array = a_employee_details[0].split('&&##&&')
				
				i_employeeID = a_emp_split_array[0];				
				
				i_subsidiary_e = a_emp_split_array[1];
				
				i_practice = a_emp_split_array[2];
								
				i_location_e = a_emp_split_array[3];
				
				i_location_txt_e = a_emp_split_array[4];	
				
				i_subsidiary_txt_e = a_emp_split_array[5];	
				
				i_employee_recID = a_emp_split_array[6];		
				
			}//Employee Details
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee ID E -->' + i_employeeID);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Practice E-->' + i_practice);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary E-->' + i_subsidiary_e);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location E -->' + i_location_e);			
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location Txt E -->' + i_location_txt_e);			
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary Txt E -->' + i_subsidiary_txt_e);			
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee rec ID -->' + i_employee_recID);			
			
		//	var s_subsidiary_txt = get_subsidiary_text(i_subsidiary_e);
		//	nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary Text -->' + s_subsidiary_txt);			
			
			var is_ST_Rate_per_hour_format = check_for_numeric_value(i_ST_Rate_per_hour);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ST Rate Per Hour Format -->' + is_ST_Rate_per_hour_format);
			
			var is_OT_Rate_per_hour_format = check_for_numeric_value(i_OT_Rate_per_hour);
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' OT Rate Per Hour Format -->' + is_OT_Rate_per_hour_format);
						
			  var s_error_fld = '';
			  var s_error_1_fld ='';
			  var s_error_2_fld ='';
			  var s_error_3_fld ='';
			  var s_error_4_fld ='';
			  var s_error_5_fld ='';
			  var s_error_6_fld ='';
			  var s_error_7_fld ='';
			  var s_error_8_fld ='';
			  var s_error_9_fld ='';
			
				  
			  if(!_logValidation(i_subsidiary))
			{
				s_error_a = ' Subsidiary not present : Subsidiaty is not entered in CSV file.\n'
				s_error_a_fld = 'CSV Subsidiary'
			}	
			  if(!_logValidation(i_location))
			{
				s_error_b = ' Location not present : Location is not entered in CSV file.\n'
				s_error_b_fld = 'CSV Location'
			}	
			  if(!_logValidation(i_currency))
			{
				s_error_c = ' Currency not present : Currency is not entered in CSV file.\n'
				s_error_c_fld = 'CSV Currency'
			}		
			 if(!_logValidation(i_month_year))
			{
				s_error_d = ' Month & Year not present : Month & Year is not entered in CSV file.\n'
				s_error_d_fld = 'CSV Month & Year'
			}	
				  if(!_logValidation(i_employeeID))
			{
				s_error_e = ' Employee not present : Employee is not entered in CSV file.\n'
				s_error_e_fld = 'CSV Employee'
			}		
			 if(!_logValidation(i_ST_Rate_per_hour))
			{
				s_error_f = ' ST Rate(Per Hour) not present : ST Rate(Per Hour) is not entered in CSV file.\n'
				s_error_f_fld = 'CSV ST Rate(Per Hour)'
			}
			 if(!_logValidation(i_OT_Rate_per_hour))
			{
				s_error_g = ' OT Rate(Per Hour) not present : OT Rate(Per Hour) is not entered in CSV file.\n'
				s_error_g_fld = 'CSV OT Rate(Per Hour)'
			}
				 				
			if(_logValidation(i_subsidiary_txt_e)&&_logValidation(i_subsidiary))
			{
				if(i_subsidiary_txt_e.toString()!=i_subsidiary.toString())
				{
				  nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary Does not match ........');
				  
				  s_error_1 = ' Invalid Employee Subsidiary : Subsidiary does not match with subsidiary entered in file .\n'								
				  s_error_1_fld = 'Employee Subsidiary\n';
				
				}
			}					
			if(is_ST_Rate_per_hour_format == true)
			{
			    s_error_2 = ' Invalid ST Rate Amount : It should be number or numeric value .\n'	
				s_error_2_fld = 'ST Rate (Per hour)\n';
			}
			if(is_OT_Rate_per_hour_format == true)
			{
			    s_error_3 = ' Invalid OT Rate Amount : It should be number or numeric value .\n'	
				s_error_3_fld = 'OT Rate (Per hour)t\n';
			}
			if(!_logValidation(i_practice))
			{
				s_error_4 = ' Practice not present : Employee does not have Practice .\n'
				s_error_4_fld = 'Employee Practice'
			}		
		    var is_date_format = check_month_year_format(i_month_year)
		    nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Month Year Format -->' + is_date_format);
			
			if(_logValidation(is_date_format))
			{
				s_error_5 = is_date_format ;
				s_error_5_fld = 'Month & Year\n'
			}			
			if(_logValidation(i_location_txt_e)&&_logValidation(i_location))
			{
				if(i_location_txt_e.toString()!=i_location.toString())
				{
				  nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location Does not match ........');
				  
				  s_error_6 = ' Invalid Employee Location : Location does not match with location entered in file .\n'
				  s_error_6_fld = 'Employee Location\n'								
				}
			}
			if(!_logValidation(i_location_e))
			{			  	  
			   s_error_7 = ' Location not present  : Location is not present on Employee.\n'
			   s_error_7_fld = 'Employee Location\n'								
			}			
			if(!_logValidation(i_location))
			{			  	  
			   s_error_8 = ' Location not present  : Location is not entered in CSV File.\n'
			   s_error_8_fld = 'Location\n'								
			}
	
			s_error = s_error_1+''+s_error_2+''+s_error_3+''+s_error_4+''+s_error_5+''+s_error_6+''+s_error_7+''+s_error_8+''+s_error_a+''+s_error_b+''+s_error_c+''+s_error_d+''+s_error_e+''+s_error_f+''+s_error_g
			nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Error -->'+s_error);		
						
			s_error_fld	= s_error_1_fld+''+s_error_2_fld+''+s_error_3_fld+''+s_error_4_fld+''+s_error_5_fld+''+s_error_6_fld+''+s_error_7_fld+''+s_error_8_fld+''+s_error_a_fld+''+s_error_b_fld+''+s_error_c_fld+''+s_error_d_fld+''+s_error_e_fld+''+s_error_f_fld+''+s_error_g_fld 
		    nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Error Field -->'+s_error_fld);		
			
		if(_logValidation(i_employeeID))
		{				
		// ===================== Create Salary Upload - Hourly Records ===================
			
		if(!_logValidation(s_error))
		{		
		update_hourly_custom_records(i_location_e,i_practice,a_CRT_array_values,i_ST_Rate_per_hour,i_OT_Rate_per_hour,i_revised_date,i_currency,i_location,i_recordID,i_employee_recID,i_subsidiary_e,i_location_e,i_month,i_year,i_CSV_File_ID)						
		}//Non - Error Records
		if(_logValidation(s_error))
		{
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* Hourly Error Block ************************'+i_hnt);
			
			i_ent++;
			var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');		
				
            o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p',i_ent);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p',s_error_fld);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p',s_error);			
			o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p',i_employeeID);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p',i_subsidiary);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p',i_month);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p',i_year);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent',i_recordID);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_error_logs',i_CSV_File_ID);	
			o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e',i_CSV_File_ID);	
									
			var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ,true,true);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
		
		    if(_logValidation(i_submit_err_ID))
			{
			  i_file_status = 'ERROR'
			}		
		}//Error Records
		}//Employee Present	
		
		}		
	}
	return i_file_status;
}//hourly_employee_file_process_data

function update_hourly_custom_records(i_location_e,i_practice,a_CRT_array_values,i_ST_Rate_per_hr,i_OT_Rate_per_hr,i_revised_date,i_currency,i_location,i_recordID,i_employee_recID,i_subsidiary_e,i_location_e,i_month,i_year,i_CSV_File_ID)					
{
  var i_sal_ID=	i_recordID;
	
  nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' Month -->'+i_month);		
		
  nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' Year -->'+i_year);		

  nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' CSV File ID-->'+i_CSV_File_ID);		

  nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' Employee ID -->'+i_employee_recID);		

  nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' Subsidiary -->'+i_subsidiary_e);		

  nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' Record ID -->'+i_recordID);		
	
	if(_logValidation(i_CSV_File_ID)&&_logValidation(i_employee_recID)&&_logValidation(i_subsidiary_e)&&_logValidation(i_month)&&_logValidation(i_year))
	{
	var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_month_process_s_u', null, 'is',i_month)
	filters[1] = new nlobjSearchFilter('custrecord_year_process_s_u', null, 'is',i_year)
    filters[2] = new nlobjSearchFilter('custrecord_csv_file_process_s_u', null,'is',i_CSV_File_ID)
	filters[3] = new nlobjSearchFilter('custrecord_employee_type_s_u', null,'is',2)	
	filters[4] = new nlobjSearchFilter('custrecord_employee_id_rec', null,'is',i_employee_recID)
	filters[5] = new nlobjSearchFilter('custrecord_subsidiary_id_h', null,'is',i_subsidiary_e)
	filters[6] = new nlobjSearchFilter('custrecord_salary_upload_id_h', null,'is',i_recordID)
	  
    var column= new Array();	
    column[0]= new nlobjSearchColumn('internalid')
	
	var a_results = nlapiSearchRecord('customrecord_salary_upload_hourly_file',null,filters,column);
	
	nlapiLogExecution('DEBUG', 'update_hourly_custom_records', 'a_results -->'+a_results);		
	
	
	if(_logValidation(a_results))
	{	 		
	  for(var i=0;i<a_results.length;i++)
	  {
	   nlapiLogExecution('DEBUG', 'update_hourly_custom_records', '  --------------------- No. --------------- -->' + (i+1));	
		  						
	   var i_recordID = a_results[i].getValue('internalid');
	   nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' Record ID -->' + i_recordID);	
	
	   if(_logValidation(i_recordID))
	   {
	   	 var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_hourly_file',i_recordID)
		 
		  if(_logValidation(o_recordOBJ))
		  {
		  	o_recordOBJ.setFieldValue('custrecord_st_rate_per_hour',i_ST_Rate_per_hr) 
			o_recordOBJ.setFieldValue('custrecord_ot_rate_per_hour',i_OT_Rate_per_hr) 
			o_recordOBJ.setFieldValue('custrecord_revision_date',i_revised_date)  
			o_recordOBJ.setFieldValue('custrecord_currency_p_s_u',i_currency)  
			o_recordOBJ.setFieldValue('custrecord_location_p_s_u',i_location)  
			
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
			nlapiLogExecution('DEBUG', 'update_hourly_custom_records', ' ***************** Submit ID ************ -->' + i_submitID);	
	
		  }//Record OBJ		
	   }//Record ID	
	  }		
	  
	update_journal_entry_hourly(i_location_e,i_practice,i_CSV_File_ID,a_CRT_array_values,i_employee_recID,i_submitID,i_sal_ID);
			
			
	}//Results
		
	}// Validations	
	
}//Salaried Custom Records

function update_journal_entry_hourly(i_location_e,i_practice,i_CSV_File_ID,a_CJRT_array_values,i_employee_recID,i_salary_process_monthly_ID,i_sal_ID)
{
  var a_employee_array = new Array();
  var a_data_array = new Array();	
  var a_process_emp_array = new Array();
  var a_project_array = new Array();
  var a_mon_data_array = new Array();
  var a_employee_revision_array = new Array();
  var a_employee_revision_date_array = new Array();
  var i_rnt =  0;
  var i_m_flag = 0 ;
  var a_JE_array = new Array();
  var i_sal_recordID;
  var a_salary_upload_monthly_ID_array = new Array();
  
  
  
  var a_employee_array = new Array();
   var a_employee_revision_date_array = new Array();
   var a_data_array = new Array();	
   var a_employee_revision_array = new Array();
   var i_rnt =  0;
   var i_m_flag = 0 ;
   var a_JE_array = new Array();
   var i_sal_recordID;
   
   var a_credit_total = new Array();
   
   
   
	//if(_logValidation(i_sal_ID))
	{
	//	 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', '  --i_sal_ID-- -->' + i_sal_ID);	
		/*
  	
		var filters = new Array();
    //    filters[1] = new nlobjSearchFilter('custbody_salary_upload_process_je', null, 'is',i_sal_ID);
			
	    var column= new Array();	
	    column[0]= new nlobjSearchColumn('internalid')
		
		var a_results = nlapiSearchRecord('journalentry',null,filters,column);
		
*/
		if(_logValidation(a_CJRT_array_values))
		{
		 nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', '  a_CJRT_array_values -->' + a_CJRT_array_values);	
	  	 nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', '  a_CJRT_array_values length -->' + a_CJRT_array_values.length);	
			
		  for(var kt = 0 ;kt<a_CJRT_array_values.length;kt++)
		  {
		  	 nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', '  --------------------- JV No. --------------- -->' + (kt+1));	
		  						
		 //   var i_recordID = a_CRT_array_values[kt];
		    nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', ' Journal Record ID -->' + a_CJRT_array_values[kt]);	
		
		    if(_logValidation(a_CJRT_array_values[kt]))
			{
			  var o_recordOBJ = nlapiLoadRecord('journalentry',a_CJRT_array_values[kt])
			  
			  if(_logValidation(o_recordOBJ))
			  {
			  	//if(a_CRT_array_values.indexOf(i_recordID)>-1)
				{							
			  	var i_line_count = o_recordOBJ.getLineItemCount('line');
				 nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', ' Ei_line_count -->' + i_line_count);	
			
			     if(_logValidation(i_line_count))
			     {
					for(var nt = 1 ;nt<=i_line_count;nt++)
					{
					  var i_employeeID = o_recordOBJ.getLineItemValue('line','entity',nt)
					   nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', ' Employee ID -->' + i_employeeID);	
		               nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', ' Employee Rec ID -->' + i_employee_recID);	
			      
				       var i_projectIDJE = o_recordOBJ.getLineItemValue('line','custcol_sow_project',nt)
					    nlapiLogExecution('DEBUG', 'update_journal_entry_hourly', ' Project ID -->' + i_projectIDJE);	
						
			  
					   if(i_employee_recID == i_employeeID)
					   {					   	
					   		//var i_project_wise_total = 0 ;
					   	 	
	
	   var a_employee_array = new Array();
   var a_employee_revision_date_array = new Array();
   var a_data_array = new Array();	
   var a_employee_revision_array = new Array();
   var i_rnt =  0;
   var i_m_flag = 0 ;
   var a_JE_array = new Array();
   var i_sal_recordID;
   // ======================= Search  Salary Upload Monthly Records ===================
  
  if(_logValidation(i_sal_ID)&&_logValidation(i_CSV_File_ID)&&_logValidation(i_employee_recID))
  {  
	var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_csv_file_process_s_u', null, 'is',i_CSV_File_ID)
	filters[1] = new nlobjSearchFilter('custrecord_salary_upload_id_h', null, 'is',i_sal_ID)
    filters[2] = new nlobjSearchFilter('custrecord_employee_type_s_u', null,'is',2)
	filters[3] = new nlobjSearchFilter('custrecord_employee_id_rec', null,'is',i_employee_recID)
   
       
    var column= new Array();	
    column[0]= new nlobjSearchColumn('internalid')
	column[1]= new nlobjSearchColumn('custrecord_employee_id_rec')
    column[2]= new nlobjSearchColumn('custrecord_subsidiary_id_h')
    column[3]= new nlobjSearchColumn('custrecord_location_id_h')
    column[4]= new nlobjSearchColumn('custrecord_practice_p_s_u')
    column[5]= new nlobjSearchColumn('custrecord_currency_p_s_u')
    column[6]= new nlobjSearchColumn('custrecord_revision_date')
    column[7]= new nlobjSearchColumn('custrecord_st_rate_per_hour')
    column[8]= new nlobjSearchColumn('custrecord_ot_rate_per_hour')
    column[9]= new nlobjSearchColumn('custrecord_month_process_s_u')
    column[10]= new nlobjSearchColumn('custrecord_year_process_s_u') 
 
    var a_results = nlapiSearchRecord('customrecord_salary_upload_hourly_file',null,filters,column);
	
  	if(_logValidation(a_results))
	{
		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' ************************** Monthly Upload Results Length **************************  -->' + a_results.length);	
		
		for(var i=0;i<a_results.length;i++)
		{
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', '  --------------------- No. --------------- -->' + (i+1));	
			
			
		   var i_recordID = a_results[i].getValue('internalid');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Record ID -->' + i_recordID);	
		
		   var i_employeeID = a_results[i].getValue('custrecord_employee_id_rec');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Employee ID -->' + i_employeeID);	
		
		   var i_subsidiary = a_results[i].getValue('custrecord_subsidiary_id_h');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Subsidiary  -->' + i_subsidiary);	
				
		   var i_location = a_results[i].getValue('custrecord_location_id_h');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Location -->' + i_location);	
				   
		   var i_practice = a_results[i].getValue('custrecord_practice_p_s_u');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Practice -->' + i_practice);	
		
		   var i_currency = a_results[i].getValue('custrecord_currency_p_s_u');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Currency -->' + i_currency);	
		
		   var d_revision_date = a_results[i].getValue('custrecord_revision_date');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Revision Date -->' + d_revision_date);
		   
		   var i_ST_Rate_per_hr = a_results[i].getValue('custrecord_st_rate_per_hour');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' ST Rate per Hour -->' + i_ST_Rate_per_hr);	
		
		   var i_OT_Rate_per_hr = a_results[i].getValue('custrecord_ot_rate_per_hour');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' OT Rate per Hour -->' + i_OT_Rate_per_hr);	
		   
		   var i_month = a_results[i].getValue('custrecord_month_process_s_u');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Month -->' + i_month);	
		
		   var i_year = a_results[i].getValue('custrecord_year_process_s_u');
		   nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Year -->' + i_year);	
		   
		   for(var it = 1;it<=50;it++)
		   {
		   	 if(i_year == it)
			 {
			 	i_year = '20'+i_year;
				break;
			 }
		   }
		 nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' After Year -->' + i_year);	
		  
			
		   a_employee_array.push(i_employeeID);
		   
		   if(_logValidation(d_revision_date))
		   {
		  // 	 i_rnt++;
		   	 a_employee_revision_date_array[i_rnt++]=i_employeeID+'%%%^@@@'+d_revision_date;	
			 a_employee_revision_array.push(i_employeeID); 	  
		   }
		   
	//	   a_employee_array =  removearrayduplicate(a_employee_array)
		 
		   a_data_array[i] = i_recordID+'###%%###'+i_month+'###%%###'+i_year+'###%%###'+i_employeeID+'###%%###'+i_subsidiary+'###%%###'+i_location+'###%%###'+i_practice+'###%%###'+i_currency+'###%%###'+d_revision_date+'###%%###'+i_ST_Rate_per_hr+'###%%###'+i_OT_Rate_per_hr
					
		}//Results Loop	
		
		  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Employee Array -->' + a_employee_array);	
		  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Employee Array -->' + a_employee_array.length);	
		
		  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Data Array -->' + a_data_array);	
		  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Data Array -->' + a_data_array.length);
		  
		  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Employee Revision Date Array -->' + a_employee_revision_date_array);	
		  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Employee Revision Date Array -->' + a_employee_revision_date_array.length);	
				  	   
	        var  i_internal_ID  ;
			var i_vertical ;
			var i_practice ;
			var i_projectID ;
			var i_rate ;
			var i_duration;
			var i_item ;
			var i_hours_per_week;
			var i_allocated_hours ;
			var i_project_name;
		  	
		 {
				
			{
						
				{					
					if (_logValidation(a_data_array)) 
					{
						var a_split_array = new Array();
						for (var me = 0; me < a_data_array.length; me++) 
						{
							var i_project_ID_CR = '';
							var i_project_wise_total = 0;
							a_split_array = a_data_array[me].split('###%%###');
							
							var i_recordID = a_split_array[0];
							var i_month = a_split_array[1];
							var i_year = a_split_array[2];
							var i_employeeID = a_split_array[3];
							var i_subsidiary = a_split_array[4];
							var i_location = a_split_array[5];
							var i_practice = a_split_array[6];
							var i_currency = a_split_array[7];
							var d_revision_date = a_split_array[8];							
							var i_ST_Rate_per_hr = a_split_array[9];
                            var i_OT_Rate_per_hr = a_split_array[10];
												
							{
												 
						 var d_start_date = '';
						 var d_end_date = '';
						 var i_day;
						 var i_total_sat_sun = '';
		 				  	nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' _----------------------- Employee ID ----------------- --> '+i_employeeID);
								  
						  	if (_logValidation(a_employee_revision_date_array)) 
							{
								nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' 11111111111 a_employee_revision_array.indexOf(i_employeeID) -->' + a_employee_revision_array.indexOf(i_employeeID));
			
								if(a_employee_revision_array.indexOf(i_employeeID)>-1)
								{									
									var a_split_emp_r_date_arr = new Array();
									for(var rt = 0 ;rt<a_employee_revision_date_array.length;rt++)
									{
										a_split_emp_r_date_arr = a_employee_revision_date_array[rt].split('%%%^@@@')
										
										var i_emp_rv_dateID = a_split_emp_r_date_arr[0];
										var d_revision_rv_date = a_split_emp_r_date_arr[1];
										
										if(i_employeeID == i_emp_rv_dateID && d_revision_date != d_revision_rv_date)
										{																					
											d_start_date = get_start_date(i_year,i_month)
											var d_revision_rv_date_1 = nlapiStringToDate(d_revision_rv_date);
										//	nlapiLogExecution('DEBUG', 'hourly_JE_creation', '1 d_revision_rv_date_1 -->' +d_revision_rv_date_1);
			
											d_revision_rv_date_1 = nlapiAddDays(d_revision_rv_date_1,parseInt(-1))
										//	nlapiLogExecution('DEBUG', 'hourly_JE_creation', '2 d_revision_rv_date_1 -->' +d_revision_rv_date_1);
			
											d_revision_rv_date_1 = nlapiDateToString(d_revision_rv_date_1);
										//	nlapiLogExecution('DEBUG', 'hourly_JE_creation', '3 d_revision_rv_date_1 -->' +d_revision_rv_date_1);
			
											d_end_date = d_revision_rv_date_1;
											
									//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' aaaaaaad_end_date -->' +d_end_date);
			                         //       nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' aaaaaaad_start_date -->' +d_start_date);
			                           
											
										}
										if(i_employeeID == i_emp_rv_dateID && d_revision_date == d_revision_rv_date)
										{
										  d_start_date = d_revision_rv_date	
										  d_end_date = get_end_date(i_year,i_month)
										  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' bbbbbd_end_date -->' +d_end_date);
			                       //    nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' bbbbbbbbd_start_date -->' +d_start_date);
			                           
										}										
										
									}									
								}//Loop								
							}
							
							if (_logValidation(a_employee_revision_date_array)) 	
							{
						//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' 222222222 a_employee_revision_array.indexOf(i_employeeID) -->' + a_employee_revision_array.indexOf(i_employeeID));
			
								if((a_employee_revision_array.indexOf(i_employeeID)== -1))
								{
									 if (_logValidation(i_month) && _logValidation(i_year)) 
									 {
									 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
										  {
										  	d_start_date = 1+'/'+1+'/'+i_year; 
											d_end_date = 31+'/'+1+'/'+i_year;
										  //  i_total_sat_sun =   get_no_saturdays(0,i_year)
											
										  }	
										  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
										  {
										  	if(parseInt(i_year) % parseInt (4) == 0)
											{
												i_day = 29;
											}
											else
											{
												i_day = 28;
											}
											
										  	d_start_date = 1+'/'+2+'/'+i_year; 
											d_end_date = i_day+'/'+2+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(1,i_year)			
										  }		
										  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
										  {
										  	d_start_date = 1+'/'+3+'/'+i_year; 
											d_end_date = 31+'/'+3+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(2,i_year)
										  }	
										  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
										  {
										  	d_start_date = 1+'/'+4+'/'+i_year; 
											d_end_date = 30+'/'+4+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(3,i_year)		
										  }	
										  else if(i_month == 'May' || i_month == 'MAY')
										  {
										  	d_start_date = 1+'/'+5+'/'+i_year; 
											d_end_date = 31+'/'+5+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(4,i_year)			
										  }	  
										  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
										  {
										 	d_start_date = 1+'/'+6+'/'+i_year; 
											d_end_date = 30+'/'+6+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(5,i_year)			
										  }	
										  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
										  {
										  	d_start_date = 1+'/'+7+'/'+i_year; 
											d_end_date = 31+'/'+7+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(6,i_year)			
										  }	
										  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
										  {
										  	d_start_date = 1+'/'+8+'/'+i_year; 
											d_end_date = 31+'+'/'+'+8+'+'/'+'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(7,i_year)		
										  }  
										  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
										  {
										  	d_start_date = 1+'/'+9+'/'+i_year; 
											d_end_date = 30+'/'+9+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(8,i_year)	
										  }	
										  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
										  {
										 	d_start_date = 1+'/'+10+'/'+i_year; 
											d_end_date = 31+'/'+10+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(9,i_year)			
										  }	
										  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
										  {
										  	d_start_date = 1+'/'+11+'/'+i_year; 
											d_end_date = 30+'/'+11+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(10,i_year)			
										  }	
										  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
										  {
										  	d_start_date = 1+'/'+12+'/'+i_year; 
											d_end_date = 31+'/'+12+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(11,i_year)			
										  }	
									 }//Month & Year
									
								}
								
							}
								if (!_logValidation(a_employee_revision_date_array))
								{
								//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' 333333333333 -->' );
			
									if (_logValidation(i_month) && _logValidation(i_year)) 
									 {
									 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
										  {
										  	d_start_date = 1+'/'+1+'/'+i_year; 
											d_end_date = 31+'/'+1+'/'+i_year;
										//    i_total_sat_sun =   get_no_saturdays(0,i_year)
											
										  }	
										  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
										  {
										  	if(parseInt(i_year) % parseInt (4) == 0)
											{
												i_day = 29;
											}
											else
											{
												i_day = 28;
											}
											
										  	d_start_date = 1+'/'+2+'/'+i_year; 
											d_end_date = i_day+'/'+2+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(1,i_year)			
										  }		
										  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
										  {
										  	d_start_date = 1+'/'+3+'/'+i_year; 
											d_end_date = 31+'/'+3+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(2,i_year)
										  }	
										  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
										  {
										  	d_start_date = 1+'/'+4+'/'+i_year; 
											d_end_date = 30+'/'+4+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(3,i_year)		
										  }	
										  else if(i_month == 'May' || i_month == 'MAY')
										  {
										  	d_start_date = 1+'/'+5+'/'+i_year; 
											d_end_date = 31+'/'+5+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(4,i_year)			
										  }	  
										  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
										  {
										 	d_start_date = 1+'/'+6+'/'+i_year; 
											d_end_date = 30+'/'+6+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(5,i_year)			
										  }	
										  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
										  {
										  	d_start_date = 1+'/'+7+'/'+i_year; 
											d_end_date = 31+'/'+7+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(6,i_year)			
										  }	
										  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
										  {
										  	d_start_date = 1+'/'+8+'/'+i_year; 
											d_end_date = 31+'+'/'+'+8+'+'/'+'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(7,i_year)		
										  }  
										  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
										  {
										  	d_start_date = 1+'/'+9+'/'+i_year; 
											d_end_date = 30+'/'+9+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(8,i_year)	
										  }	
										  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
										  {
										 	d_start_date = 1+'/'+10+'/'+i_year; 
											d_end_date = 31+'/'+10+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(9,i_year)			
										  }	
										  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
										  {
										  	d_start_date = 1+'/'+11+'/'+i_year; 
											d_end_date = 30+'/'+11+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(10,i_year)			
										  }	
										  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
										  {
										  	d_start_date = 1+'/'+12+'/'+i_year; 
											d_end_date = 31+'/'+12+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(11,i_year)			
										  }	
									 }//Month & Year
																		
								} 	
						  
				 // ================= Get Month Details =======================					
				
					 nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Start Date -->' + d_start_date);	
		             nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' End Date -->' + d_end_date);	
							
					  i_total_sat_sun = getWeekend(d_start_date,d_end_date)
					  									 
					 	 var a_pr_split_array = new Array();
						 var a_project_ID_arr;
						 var a_hourly_cal_details;
						 var a_hourly_cal_details_array  = search_hourly_calculation_details(i_employeeID,d_start_date,d_end_date)	
					//	 nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Hourly Cal Details Arr Details -->' + a_hourly_cal_details_array)						 
						   if (_logValidation(a_hourly_cal_details_array)) 
						   {
						   	 a_pr_split_array = a_hourly_cal_details_array[0].split('^^^^***^^^^');
							 a_project_ID_arr = a_pr_split_array[0];
							 a_hourly_cal_details = a_pr_split_array[1];							
						   }
							
					
					 var a_PRT_array_values = new Array()	
					 var i_data_PRT = new Array()
					 i_data_PRT =  a_project_ID_arr;
				 
				      for(var dct=0;dct<i_data_PRT.length;dct++)
					  {
						 	a_PRT_array_values = i_data_PRT.split(',')
						    break;				
					  }
					a_PRT_array_values = removearrayduplicate(a_PRT_array_values)	
									 
					 var a_CRT_array_values = new Array()	
					 var i_data_CRT = new Array()
					 i_data_CRT =  a_hourly_cal_details;
				 
				      for(var dct=0;dct<i_data_CRT.length;dct++)
					  {
						 	a_CRT_array_values = i_data_CRT.split(',')
						    break;				
					  }	
				
					nlapiLogExecution('DEBUG', 'hourly_JE_creation','CRT Array Values-->' +a_CRT_array_values);
					nlapiLogExecution('DEBUG', 'hourly_JE_creation',' CRT Array Values Length-->' + a_CRT_array_values.length);
				
					 {
					 	if (_logValidation(a_CRT_array_values) && _logValidation(a_PRT_array_values)) 
				        {
					        var i_internal_ID;
			 				var i_vertical;
			 				var i_practice;
			 				var i_projectID;
			 				var i_rate ;
			 				var i_duration;
			 				var i_item ;
			 				var i_hours_per_week;
							var i_project_name;
					
					 		for (var pid = 0; pid < a_PRT_array_values.length; pid++) 
							{
					 			var i_OT_hours = 0;
					 			var i_ST_hours = 0;
					 			var i_project_wise_cost_ST = 0;
					 			var i_project_wise_cost_OT = 0;
					 			
					 			for (var pe = 0; pe < a_CRT_array_values.length; pe++) 
								{
					 				a_split_project_array = a_CRT_array_values[pe].split('&&&%%&&&');
					 				
					 				i_internal_ID = a_split_project_array[0];
					 				i_vertical = a_split_project_array[1];
					 				i_practice = a_split_project_array[2];
					 				i_projectID = a_split_project_array[3];
					 				i_rate = a_split_project_array[4];
					 				i_duration = a_split_project_array[5];
					 				i_item = a_split_project_array[6];
					 				i_hours_per_week = a_split_project_array[7];
									i_project_name = a_split_project_array[8];
									
					 				i_project_ID_CR = i_projectID
					 				if (i_projectID == a_PRT_array_values[pid]) 
									{
					 					if (i_item == 'OT') 
										{
					 						i_OT_hours = parseFloat(i_OT_hours) + parseFloat(i_duration);
					 					}
					 					else 
										{
					 						i_ST_hours = parseFloat(i_ST_hours) + parseFloat(i_duration);
					 					}
					 				}//Project 
									}
									i_no_of_hours_ST = i_ST_hours
									i_no_of_hours_OT = i_OT_hours
									i_ST_per_hour_rate_from_file = i_ST_Rate_per_hr;
									i_OT_per_hour_rate_from_file = i_OT_Rate_per_hr;
									
									i_project_wise_cost_ST = parseFloat(i_no_of_hours_ST) * parseFloat(i_ST_per_hour_rate_from_file)
									i_project_wise_cost_OT = parseFloat(i_no_of_hours_OT) * parseFloat(i_OT_per_hour_rate_from_file)
									i_project_wise_cost_ST = parseFloat(i_project_wise_cost_ST).toFixed(2);
									i_project_wise_cost_OT = parseFloat(i_project_wise_cost_OT).toFixed(2);
																		
									i_project_wise_total = parseFloat(i_project_wise_total) + parseFloat(i_project_wise_cost_ST) + parseFloat(i_project_wise_cost_OT);
									i_project_wise_total = parseFloat(i_project_wise_total).toFixed(2);
									nlapiLogExecution('DEBUG', 'hourly_JE_creation', 'i_project_wise_total -->' + i_project_wise_total);
									nlapiLogExecution('DEBUG', 'hourly_JE_creation', 'i_project_wise_cost_ST -->' + i_project_wise_cost_ST);
									nlapiLogExecution('DEBUG', 'hourly_JE_creation', 'i_project_wise_cost_OT -->' + i_project_wise_cost_OT);
									
									//nlapiLogExecution('DEBUG', 'hourly_JE_creation',' cccccccccccccc i_item -->' +i_item);
									
									//	   if(i_emp_nt<11)
									{
										
										if (i_project_wise_cost_OT != 0) 
										{											
											
											if (!_logValidation(i_practice))
											{
											  i_practice = '';												
											}
											if (!_logValidation(i_vertical))
											{
											  i_vertical = '';												
											}
											if (!_logValidation(i_project_name))
											{
											   i_project_name = '';
											}
											if (!_logValidation(i_projectID))
											{
											   i_projectID = '';
											}
											
											nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_projectID -->' + i_projectID);	
		                                    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_project_name -->' + i_project_name);	
		                                    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_practice -->' + i_practice);	
		
		                                    if(i_projectID == i_projectIDJE)
											{
												
												o_recordOBJ.setLineItemValue('line','debit',nt,i_project_wise_cost_OT)
												o_recordOBJ.setLineItemValue('line','location',nt,i_location_e)
												o_recordOBJ.setLineItemValue('line','department',nt,i_practice)
									    		nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Project ******************');
										   //     break; 
												
												
											}//Project IFJE
		
		
		
		
										}
										if (i_project_wise_cost_ST != 0) 
										{
									//		a_process_array.push(i_recordID);
										
											//		nlapiLogExecution('DEBUG', 'hourly_JE_creation','  In ST Block .....');
											
											if (!_logValidation(i_practice))
											{
											  i_practice = '';												
											}
											if (!_logValidation(i_vertical))
											{
											  i_vertical = '';												
											}
											if (!_logValidation(i_project_name))
											{
											   i_project_name = '';
											}
											if (!_logValidation(i_projectID))
											{
											   i_projectID = '';
											}
											nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_projectID -->' + i_projectID);	
		                                    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_project_name -->' + i_project_name);	
		                                    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_practice -->' + i_practice);	
		
										
										    if(i_projectID == i_projectIDJE)
											{
												o_recordOBJ.setLineItemValue('line','debit',nt,i_project_wise_cost_ST)
												o_recordOBJ.setLineItemValue('line','location',nt,i_location_e)
												o_recordOBJ.setLineItemValue('line','department',nt,i_practice)
									    		nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Project ******************');
										   //     break; 
												i_set_flag =0;			
												
												
											}//Project IFJE
		
										}
									}
									
								}
								
							}
						}
					
								
							}//EMployee not more than 1o
							
					 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_project_ID_CR -->' + i_project_ID_CR);	
		             nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_projectIDJE -->' + i_projectIDJE);	
		             nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' i_project_wise_total -->' + i_project_wise_total);	
		
				   
					  	
							
						}
					}
					
			
				}			
				
		
			}
						
		 }
		  
			
	}//Results	
	
	

  }//Validations	
	              
			if(i_project_ID_CR == i_projectIDJE)
			{
				a_project_a.push(i_projectIDJE);
				a_credit_total.push(nt+'&&&***&&&'+i_projectIDJE+'&&&***&&&'+i_project_wise_total)
			}	  
				  
				  
				  
				  
	/*
 if(i_project_ID_CR == i_projectIDJE)
					{
						  nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Previous Debit Line  -->' + nt);	
		                  nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Current Credit Line -->' + (nt+1));	
		                 
						   o_recordOBJ.setLineItemValue('line','credit',(parseInt(nt)+parseInt(1)),i_project_wise_total)
										
					}	
*/
					   	
					   }//Employee Validation					
					}//Loop				
			     }//Line Count					
				}//Journal Present	
				
				nlapiLogExecution('DEBUG', 'monthly_JE_creation','a_credit_total-->' +a_credit_total);
				
				/*
if(_logValidation(a_credit_total))
				{
					var a_split_credit_arr = new Array();
					
					for (var pty = 0; pty < a_project_a.length; pty++) 
					{
						for(var ty=0;ty<a_credit_total.length;ty++)
						{
						  a_split_credit_arr = a_credit_total[ty].split('&&&***&&&')
						  
						  var i_s_no = a_split_credit_arr[0]
						  
						  var i_project = a_split_credit_arr[1]
						  
						  var i_total =a_split_credit_arr[2]
						  
						  if(a_project_a[pty] == i_project)
						  {
						  	
						  }
							
							
						}//Loop
						
					}
					
				}
*/
				
				
				var i_journal_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
				nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_journal_submitID-->' +i_journal_submitID);
				
			
										
			  }//Record OBJ				
			}//Record ID			
		  }//Loop			
		}//Results		
	}//Salary Process ID	
	
}//Update Journal Entry

function monthly_employee_file_process_data(i_CSV_File_ID,i_recordID,i_account_debit,i_account_credit,i_hourly_employee,i_monthly_employee,a_CRT_array_values)
{
	var i_mnt = 0;
	var i_ent = 0;
	var i_file_status = 'SUCCESS';
	
	if(_logValidation(i_CSV_File_ID))
	{				
		//==================== Read CSV File Contents =========================
					
		var o_fileOBJ = nlapiLoadFile(i_CSV_File_ID);
        
		var s_file_contents = o_fileOBJ.getValue();
        nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', 'Type -->' + o_fileOBJ.getType());
        
		if (o_fileOBJ.getType() == 'EXCEL')
		{
	        nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', '@@@@@@@@@@@  Excel File @@@@@@@@@@@ ');
	        var filedecodedrows = decode_base64(s_file_contents);
	    }
	    else
		{
	        nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', '@@@@@@@@@@@  Other Than Excel File @@@@@@@@@@@ ');
	        var filedecodedrows = s_file_contents;				       
	    }
		var a_file_array =  filedecodedrows.split(/\n/g);
        nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' No. Of File Rows -->' + a_file_array.length);
  
		var i_data_length = parseInt(a_file_array.length) - parseInt(1);
		nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Data Length -->' + i_data_length);
		 
		for (var i = 1 ; i <i_data_length; i++) 
		{
			  var s_error = '';
			  var s_error_1 ='';
			  var s_error_2 ='';
			  var s_error_3 ='';
			  var s_error_4 ='';
			  var s_error_5 ='';
			  var s_error_6 ='';
			  var s_error_7 ='';
			  var s_error_8 ='';
			  var s_error_9 ='';
			  
			  var s_error_fld = '';
			  var s_error_1_fld ='';
			  var s_error_2_fld ='';
			  var s_error_3_fld ='';
			  var s_error_4_fld ='';
			  var s_error_5_fld ='';
			  var s_error_6_fld ='';
			  var s_error_7_fld ='';
			  var s_error_8_fld ='';
			  var s_error_9_fld ='';			  
		  
			a_file_array[i] = parseCSV(a_file_array[i]);
						
			var i_temp_data = a_file_array[i].toString()
			
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', 'Temp Data -->' + i_temp_data);
						
			var a_row_data = i_temp_data.split(',')
			
			var i_month_year = a_row_data[0]
			
			var i_employeeID = a_row_data[1]
			
			var i_subsidiary = a_row_data[2]
			
			var i_location = a_row_data[3]
			
			var i_currency = a_row_data[4]
								
			var i_revised_date = a_row_data[5]
			
			var i_amount = a_row_data[6]
			
			
									
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee ID -->' + i_employeeID);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month & Year -->' + i_month_year);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary -->' + i_subsidiary);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Location -->' + i_location);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Currency -->' + i_currency);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Amount -->' + i_amount);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Revised Date -->' + i_revised_date);
					
    	  var a_split_revised_date_array = new Array();
	  
	      if(_logValidation(i_revised_date))
		  {
		  	a_split_revised_date_array = i_revised_date.split('/')
			
			var i_day = a_split_revised_date_array[0];
			
			var i_month = a_split_revised_date_array[1];
			
			var i_year =  a_split_revised_date_array[2];
			
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Day -->' + i_day);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month -->' + i_month);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Year -->' + i_year);
			
		  }
         
		  if(_logValidation(i_revised_date))
		   {
		   	 i_revised_date = nlapiStringToDate(i_revised_date);
		     i_revised_date = nlapiDateToString(i_revised_date);
		     nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Revised Date -->' + i_revised_date);			
		 
		   }	
		  var i_month;
		  var i_year;
		
		  var a_split_m_y_array = new Array();
	  
	      if(_logValidation(i_month_year))
		  {
		  	 if (i_month_year.indexOf('-') > -1) 
			  {
			  	a_split_m_y_array = i_month_year.split('-');
			  	
			  	i_month = a_split_m_y_array[0];
			  	
			  	i_year = a_split_m_y_array[1];
			  }//Index Of 		  	
		  }//Month & Year
		  
		  nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month -->' + i_month);			
		  nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Year -->' + i_year);			
		 			
			var i_currency_c = check_currency(i_currency);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Currency -->' + i_currency_c);
						
			var a_employee_details = search_employee(i_employeeID)
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee Details -->' + a_employee_details);
			
			var a_emp_split_array = new Array();
			var i_practice;
			var i_subsidiary_e;
			var i_employeeID;
			var i_location_e;
			var i_location_txt_e;
			var i_currency_e;
			var i_subsidiary_txt_e;
			var i_employee_recID;
			if(_logValidation(a_employee_details))
			{
				a_emp_split_array = a_employee_details[0].split('&&##&&')
				
				i_employeeID = a_emp_split_array[0];				
				
				i_subsidiary_e = a_emp_split_array[1];
				
				i_practice = a_emp_split_array[2];	
				
				i_location_e = a_emp_split_array[3];
				
				i_location_txt_e = a_emp_split_array[4];	
				
				i_subsidiary_txt_e = a_emp_split_array[5];		
				
				i_employee_recID = a_emp_split_array[6];
				
				
			}//Employee Details
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee ID E -->' + i_employeeID);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Practice E-->' + i_practice);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary E-->' + i_subsidiary_e);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Location E -->' + i_location_e);			
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Location Txt E -->' + i_location_txt_e);			
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary Txt E -->' + i_subsidiary_txt_e);			
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee record ID -->' + i_employee_recID);			
			
		//	var s_subsidiary_txt = get_subsidiary_text(i_subsidiary_e);
		//	nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary Text -->' + s_subsidiary_txt);			
			
			var is_amount_format = check_for_numeric_value(i_amount);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Amount Format -->' + is_amount_format);
			
			
			  var s_error_fld = '';
			  var s_error_1_fld ='';
			  var s_error_2_fld ='';
			  var s_error_3_fld ='';
			  var s_error_4_fld ='';
			  var s_error_5_fld ='';
			  var s_error_6_fld ='';
			  var s_error_7_fld ='';
			  var s_error_8_fld ='';
			  var s_error_9_fld ='';
			
						
			if(_logValidation(i_subsidiary_txt_e)&&_logValidation(i_subsidiary))
			{
				if(i_subsidiary_txt_e.toString()!=i_subsidiary.toString())
				{
				  nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary Does not match ........');
				  
				  s_error_1 = ' Invalid Employee Subsidiary : Subsidiary does not match with subsidiary entered in file .\n'								
				  s_error_1_fld = 'Employee Subsidiary\n';
				
				}
			}					
			if(is_amount_format == true)
			{
			    s_error_2 = ' Invalid Salary Amount : It should be number or numeric value .\n'	
				s_error_2_fld = 'Cost\n';
			}
			if(!_logValidation(i_practice))
			{
				s_error_3 = ' Practice not present : Employee does not have Practice .\n'
				s_error_3_fld = 'Employee Practice'
			}		
		    var is_date_format = check_month_year_format(i_month_year)
		    nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month Year Format -->' + is_date_format);
			
			if(_logValidation(is_date_format))
			{
				s_error_4 = is_date_format ;
				s_error_4_fld = 'Month & Year\n'
			}			
			if(_logValidation(i_location_txt_e)&&_logValidation(i_location))
			{
				if(i_location_txt_e.toString()!=i_location.toString())
				{
				  nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Location Does not match ........');
				  
				  s_error_5 = ' Invalid Employee Location : Location does not match with location entered in file .\n'
				  s_error_5_fld = 'Employee Location\n'								
				}
			}
			if(!_logValidation(i_location_e))
			{			  	  
			   s_error_6 = ' Location not present  : Location is not present on Employee.\n'
			   s_error_6_fld = 'Employee Location\n'								
			}			
			if(!_logValidation(i_location))
			{			  	  
			   s_error_7 = ' Location not present  : Location is not entered in CSV File.\n'
			   s_error_7_fld = 'Location\n'								
			}
			s_error = s_error_1+''+s_error_2+''+s_error_3+''+s_error_4+''+s_error_5+''+s_error_6+''+s_error_7;
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Error -->'+s_error);		
						
			s_error_fld	= s_error_1_fld+''+s_error_2_fld+''+s_error_3_fld+''+s_error_4_fld+''+s_error_5_fld+''+s_error_6_fld+''+s_error_7_fld;   
		    nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Error Field -->'+s_error_fld);		
			
			
			
		
		// ===================== Create Salary Upload - Monthly Records ===================
				
		if(!_logValidation(s_error))
		{				
			update_salaried_custom_records(i_location_e,i_practice,a_CRT_array_values,i_amount,i_revised_date,i_currency,i_location,i_recordID,i_employee_recID,i_subsidiary_e,i_location_e,i_month,i_year,i_CSV_File_ID)					
							
		}//Non - Error Records
		if(_logValidation(s_error))
		{
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* Monthly Error Block ************************'+i_mnt);
			
			i_ent++;
			var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');		
				
            o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p',i_ent);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p',s_error_fld);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p',s_error);			
			o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p',i_employeeID);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p',i_subsidiary);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p',i_month);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p',i_year);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent',i_recordID);
			o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_error_logs',i_CSV_File_ID);	
			o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e',i_employee_recID);	
								
			var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ,true,true);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
		
		    if(_logValidation(i_submit_err_ID))
			{
			  i_file_status = 'ERROR'
			}		  
		
		}//Error Records
		
		}		
	}
	return i_file_status;
}//monthly_employee_file_process_data



function update_salaried_custom_records(i_location_e,i_practice,a_CRT_array_values,i_amount,i_revised_date,i_currency,i_location,i_recordID,i_employee_recID,i_subsidiary_e,i_location_e,i_month,i_year,i_CSV_File_ID)					
{
  var i_sal_ID=	i_recordID;
	
  nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' Month -->'+i_month);		
		
  nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' Year -->'+i_year);		

  nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' CSV File ID-->'+i_CSV_File_ID);		

  nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' Employee ID -->'+i_employee_recID);		

  nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' Subsidiary -->'+i_subsidiary_e);		

  nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' Record ID -->'+i_recordID);		
	
	if(_logValidation(i_CSV_File_ID)&&_logValidation(i_employee_recID)&&_logValidation(i_subsidiary_e)&&_logValidation(i_month)&&_logValidation(i_year))
	{
	var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_month_m_s_p_u', null, 'is',i_month)
	filters[1] = new nlobjSearchFilter('custrecord_year_m_s_p_u', null, 'is',i_year)
    filters[2] = new nlobjSearchFilter('custrecord_csv_file_m_s_p_u', null,'is',i_CSV_File_ID)
	filters[3] = new nlobjSearchFilter('custrecord_emp_type_m_s_p_u', null,'is',1)	
	filters[4] = new nlobjSearchFilter('custrecord_employee_rec_id', null,'is',i_employee_recID)
	filters[5] = new nlobjSearchFilter('custrecord_subsidiary_id_s', null,'is',i_subsidiary_e)
	filters[6] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null,'is',i_recordID)
	  
    var column= new Array();	
    column[0]= new nlobjSearchColumn('internalid')
	
	var a_results = nlapiSearchRecord('customrecord_salary_upload_monthly_file',null,filters,column);
	
	nlapiLogExecution('DEBUG', 'update_salaried_custom_records', 'a_results -->'+a_results);		
	
	
	if(_logValidation(a_results))
	{	 		
	  for(var i=0;i<a_results.length;i++)
	  {
	   nlapiLogExecution('DEBUG', 'update_salaried_custom_records', '  --------------------- No. --------------- -->' + (i+1));	
		  						
	   var i_recordID = a_results[i].getValue('internalid');
	   nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' Record ID -->' + i_recordID);	
	
	   if(_logValidation(i_recordID))
	   {
	   	 var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_monthly_file',i_recordID)
		 
		  if(_logValidation(o_recordOBJ))
		  {
		  	o_recordOBJ.setFieldValue('custrecord_cost_m_s_p_u',i_amount)  
			o_recordOBJ.setFieldValue('custrecord_revision_date_m_s_p_u',i_revised_date)  
			o_recordOBJ.setFieldValue('custrecord_currency_m_s_p_u',i_currency)  
			o_recordOBJ.setFieldValue('custrecord_location_m_s_p_u',i_location)  
			
			var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
			nlapiLogExecution('DEBUG', 'update_salaried_custom_records', ' ***************** Submit ID ************ -->' + i_submitID);	
	
		  }//Record OBJ		
	   }//Record ID	
	  }		
	  
	update_journal_entry_salaried(i_location_e,i_practice,i_CSV_File_ID,a_CRT_array_values,i_employee_recID,i_submitID,i_sal_ID);
			
			
	}//Results
		
	}// Validations	
	
}//Salaried Custom Records

function update_journal_entry_salaried(i_location_e,i_practice,i_CSV_File_ID,a_CJRT_array_values,i_employee_recID,i_salary_process_monthly_ID,i_sal_ID)
{
  var a_employee_array = new Array();
  var a_data_array = new Array();	
  var a_process_emp_array = new Array();
  var a_project_array = new Array();
  var a_mon_data_array = new Array();
  var a_employee_revision_array = new Array();
  var a_employee_revision_date_array = new Array();
  var i_rnt =  0;
  var i_m_flag = 0 ;
  var a_JE_array = new Array();
  var i_sal_recordID;
  var a_salary_upload_monthly_ID_array = new Array();
  var employee_array_credit = new Array();
	//if(_logValidation(i_sal_ID))
	{
	//	 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', '  --i_sal_ID-- -->' + i_sal_ID);	
		/*
  	
		var filters = new Array();
    //    filters[1] = new nlobjSearchFilter('custbody_salary_upload_process_je', null, 'is',i_sal_ID);
			
	    var column= new Array();	
	    column[0]= new nlobjSearchColumn('internalid')
		
		var a_results = nlapiSearchRecord('journalentry',null,filters,column);
		
*/
		if(_logValidation(a_CJRT_array_values))
		{
		 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', '  a_CJRT_array_values -->' + a_CJRT_array_values);	
	  	 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', '  a_CJRT_array_values length -->' + a_CJRT_array_values.length);	
			
		  for(var kt = 0 ;kt<a_CJRT_array_values.length;kt++)
		  {
		  	 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', '  --------------------- JV No. --------------- -->' + (kt+1));	
		  						
		 //   var i_recordID = a_CRT_array_values[kt];
		    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Journal Record ID -->' + a_CJRT_array_values[kt]);	
		
		    if(_logValidation(a_CJRT_array_values[kt]))
			{
			  var o_recordOBJ = nlapiLoadRecord('journalentry',a_CJRT_array_values[kt])
			  
			  if(_logValidation(o_recordOBJ))
			  {
			  	//if(a_CRT_array_values.indexOf(i_recordID)>-1)
				{							
			  	var i_line_count = o_recordOBJ.getLineItemCount('line');
				 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Ei_line_count -->' + i_line_count);	
			
			     if(_logValidation(i_line_count))
			     {
					for(var nt = 1 ;nt<=i_line_count;nt++)
					{
					  var i_employeeID_JE = o_recordOBJ.getLineItemValue('line','entity',nt)
					   nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Employee ID -->' + i_employeeID_JE);	
		              /*
 nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Employee Rec ID -->' + i_employee_recID);	
			      
*/
				       var i_projectIDJE = o_recordOBJ.getLineItemValue('line','custcol_sow_project',nt)
					    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Project ID -->' + i_projectIDJE);	
						
						 var i_creditJE = o_recordOBJ.getLineItemValue('line','credit',nt)
					    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Credit -->' + i_creditJE);	
						
						 var i_debitJE = o_recordOBJ.getLineItemValue('line','debit',nt)
					    nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' Debit -->' + i_debitJE);	
						
					 if(_logValidation(i_CSV_File_ID)&&_logValidation(i_sal_ID)&&_logValidation(i_employeeID_JE))
					   {					   	
						var i_project_wise_total = 0 ;
					   	 	
	var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_csv_file_m_s_p_u', null, 'is',i_CSV_File_ID)
	filters[1] = new nlobjSearchFilter('custrecord_salary_upload_id_m', null, 'is',i_sal_ID)
    filters[2] = new nlobjSearchFilter('custrecord_emp_type_m_s_p_u', null,'is',1)
	filters[3] = new nlobjSearchFilter('custrecord_employee_rec_id', null,'is',i_employeeID_JE)
     
       
    var column= new Array();	
    column[0]= new nlobjSearchColumn('internalid')
	column[1]= new nlobjSearchColumn('custrecord_employee_rec_id')
    column[2]= new nlobjSearchColumn('custrecord_subsidiary_id_s')
    column[3]= new nlobjSearchColumn('custrecord_location_id_s')
    column[4]= new nlobjSearchColumn('custrecord_practice_m_s_p_u')
    column[5]= new nlobjSearchColumn('custrecord_currency_m_s_p_u')
    column[6]= new nlobjSearchColumn('custrecord_revision_date_m_s_p_u')
	column[7]= new nlobjSearchColumn('custrecord_month_m_s_p_u')
    column[8]= new nlobjSearchColumn('custrecord_year_m_s_p_u')
	column[9]= new nlobjSearchColumn('custrecord_cost_m_s_p_u')
	
    var a_results = nlapiSearchRecord('customrecord_salary_upload_monthly_file',null,filters,column);
	nlapiLogExecution('DEBUG', 'update_journal_entry_salaried', ' a_results -->' + a_results);	
			
  	if (_logValidation(a_results)) 
	{
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ************************** Monthly Upload Results Length **************************  -->' + a_results.length);
		
		for (var i = 0; i < a_results.length; i++) 
		{
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', '  ---------------------SM  No. --------------- -->' + (i + 1));
			
			var i_recordID = a_results[i].getValue('internalid');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Record ID -->' + i_recordID);
			
			var i_employeeID = a_results[i].getValue('custrecord_employee_rec_id');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Employee ID -->' + i_employeeID);
			
			var i_subsidiary = a_results[i].getValue('custrecord_subsidiary_id_s');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Subsidiary  -->' + i_subsidiary);
			
			var i_location = a_results[i].getValue('custrecord_location_id_s');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Location -->' + i_location);
			
			var i_practice = a_results[i].getValue('custrecord_practice_m_s_p_u');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Practice -->' + i_practice);
			
			var i_currency = a_results[i].getValue('custrecord_currency_m_s_p_u');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Currency -->' + i_currency);
			
			var d_revision_date = a_results[i].getValue('custrecord_revision_date_m_s_p_u');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Revision Date -->' + d_revision_date);
			
			var i_month = a_results[i].getValue('custrecord_month_m_s_p_u');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Month -->' + i_month);
			
			var i_year = a_results[i].getValue('custrecord_year_m_s_p_u');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Year -->' + i_year);
			
			var i_cost = a_results[i].getValue('custrecord_cost_m_s_p_u');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Cost -->' + i_cost);
			
			for (var it = 1; it <= 50; it++) {
				if (i_year == it) {
					i_year = '20' + i_year;
					break;
				}
			}
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' After Year -->' + i_year);
			
			if (_logValidation(d_revision_date)) {
				// 	 i_rnt++;
				a_employee_revision_date_array[i_rnt++] = i_employeeID + '%%%^@@@' + d_revision_date;
				a_employee_revision_array.push(i_employeeID);
			}
			
			
			a_employee_array.push(i_employeeID);
			  employee_array_credit.push(i_employeeID);
		   
		   
	      employee_array_credit =  removearrayduplicate(employee_array_credit)
		 
			a_data_array[i] = i_recordID + '###%%###' + i_month + '###%%###' + i_year + '###%%###' + i_employeeID + '###%%###' + i_subsidiary + '###%%###' + i_location + '###%%###' + i_practice + '###%%###' + i_currency + '###%%###' + d_revision_date + '###%%###' + i_cost;
			
		}//Results Loop	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Employee Array -->' + a_employee_array);
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Employee Array -->' + a_employee_array.length);
		
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Data Array -->' + a_data_array);
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Data Array -->' + a_data_array.length);
		
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Employee Revision Date Array -->' + a_employee_revision_date_array);
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Employee Revision Date Array -->' + a_employee_revision_date_array.length);
		
	}	
	
	// ==================== Employee Arra===============
				
			//		if (_logValidation(employee_array_credit)) 
					{
			//			for (var bg = 0; bg < employee_array_credit.length; bg++) 
						{
					//		var i_project_wise_total = 0;
							
																
				if(_logValidation(a_data_array))
				{
				   var a_split_array = new Array();	
				   for(var me=0;me<a_data_array.length;me++)
				   {
				   	 a_split_array = a_data_array[me].split('###%%###');
					
					 var i_recordID = a_split_array[0];
					 var i_month = a_split_array[1];
					 var i_year = a_split_array[2];
					 var i_employeeID = a_split_array[3];
					 var i_subsidiary = a_split_array[4];
					 var i_location = a_split_array[5];
					 var i_practice = a_split_array[6];
					 var i_currency = a_split_array[7];
					 var d_revision_date = a_split_array[8];
					 var i_cost = a_split_array[9];
					 
					 
					 
					 
		//			    if(employee_array_credit[bg] == i_employeeID)
						{
									//	 	if((a_process_array.indexOf(i_recordID) == -1))
					{					
					 // ================= Get Month Details =======================
					 
				  						 
						 var d_start_date = '';
						 var d_end_date = '';
						 var i_day;
						 var i_total_sat_sun = '';
		 				  	nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' _----------------------- Employee ID ----------------- --> '+i_employeeID);
								  
						  	if (_logValidation(a_employee_revision_date_array)) 
							{
						//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' 11111111111 a_employee_revision_array.indexOf(i_employeeID) -->' + a_employee_revision_array.indexOf(i_employeeID));
			
								if(a_employee_revision_array.indexOf(i_employeeID)>-1)
								{									
									var a_split_emp_r_date_arr = new Array();
									for(var rt = 0 ;rt<a_employee_revision_date_array.length;rt++)
									{
										a_split_emp_r_date_arr = a_employee_revision_date_array[rt].split('%%%^@@@')
										
										var i_emp_rv_dateID = a_split_emp_r_date_arr[0];
										var d_revision_rv_date = a_split_emp_r_date_arr[1];
										
								/*
		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' i_emp_rv_dateID -->' +i_emp_rv_dateID);
			                            nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' d_revision_rv_date -->' +d_revision_rv_date);
			
*/
										
										if(i_employeeID == i_emp_rv_dateID && d_revision_date != d_revision_rv_date)
										{																					
											d_start_date = get_start_date(i_year,i_month)
											var d_revision_rv_date_1 = nlapiStringToDate(d_revision_rv_date);
									//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', '1 d_revision_rv_date_1 -->' +d_revision_rv_date_1);
			
											d_revision_rv_date_1 = nlapiAddDays(d_revision_rv_date_1,parseInt(-1))
									//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', '2 d_revision_rv_date_1 -->' +d_revision_rv_date_1);
			
											d_revision_rv_date_1 = nlapiDateToString(d_revision_rv_date_1);
									//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', '3 d_revision_rv_date_1 -->' +d_revision_rv_date_1);
			
											d_end_date = d_revision_rv_date_1;
									/*
		
											nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' aaaaaaad_end_date -->' +d_end_date);
			                                nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' aaaaaaad_start_date -->' +d_start_date);
			                        
*/   
											
										}
										if(i_employeeID == i_emp_rv_dateID && d_revision_date == d_revision_rv_date)
										{
										  d_start_date = d_revision_rv_date	
										  d_end_date = get_end_date(i_year,i_month)
									//	  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' bbbbbd_end_date -->' +d_end_date);
			                      //     nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' bbbbbbbbd_start_date -->' +d_start_date);
			                           
										}										
										
									}									
								}//Loop								
							}
							
							if (_logValidation(a_employee_revision_date_array)) 	
							{
						//		nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' 222222222 a_employee_revision_array.indexOf(i_employeeID) -->' + a_employee_revision_array.indexOf(i_employeeID));
			
								if((a_employee_revision_array.indexOf(i_employeeID)== -1))
								{
									 if (_logValidation(i_month) && _logValidation(i_year)) 
									 {
									 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
										  {
										  	d_start_date = 1+'/'+1+'/'+i_year; 
											d_end_date = 31+'/'+1+'/'+i_year;
										  //  i_total_sat_sun =   get_no_saturdays(0,i_year)
											
										  }	
										  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
										  {
										  	if(parseInt(i_year) % parseInt (4) == 0)
											{
												i_day = 29;
											}
											else
											{
												i_day = 28;
											}
											
										  	d_start_date = 1+'/'+2+'/'+i_year; 
											d_end_date = i_day+'/'+2+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(1,i_year)			
										  }		
										  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
										  {
										  	d_start_date = 1+'/'+3+'/'+i_year; 
											d_end_date = 31+'/'+3+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(2,i_year)
										  }	
										  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
										  {
										  	d_start_date = 1+'/'+4+'/'+i_year; 
											d_end_date = 30+'/'+4+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(3,i_year)		
										  }	
										  else if(i_month == 'May' || i_month == 'MAY')
										  {
										  	d_start_date = 1+'/'+5+'/'+i_year; 
											d_end_date = 31+'/'+5+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(4,i_year)			
										  }	  
										  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
										  {
										 	d_start_date = 1+'/'+6+'/'+i_year; 
											d_end_date = 30+'/'+6+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(5,i_year)			
										  }	
										  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
										  {
										  	d_start_date = 1+'/'+7+'/'+i_year; 
											d_end_date = 31+'/'+7+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(6,i_year)			
										  }	
										  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
										  {
										  	d_start_date = 1+'/'+8+'/'+i_year; 
											d_end_date = 31+'+'/'+'+8+'+'/'+'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(7,i_year)		
										  }  
										  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
										  {
										  	d_start_date = 1+'/'+9+'/'+i_year; 
											d_end_date = 30+'/'+9+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(8,i_year)	
										  }	
										  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
										  {
										 	d_start_date = 1+'/'+10+'/'+i_year; 
											d_end_date = 31+'/'+10+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(9,i_year)			
										  }	
										  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
										  {
										  	d_start_date = 1+'/'+11+'/'+i_year; 
											d_end_date = 30+'/'+11+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(10,i_year)			
										  }	
										  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
										  {
										  	d_start_date = 1+'/'+12+'/'+i_year; 
											d_end_date = 31+'/'+12+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(11,i_year)			
										  }	
									 }//Month & Year
									
								}
								
							}
								if (!_logValidation(a_employee_revision_date_array))
								{
							//			nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' 333333333333 -->' );
			
									if (_logValidation(i_month) && _logValidation(i_year)) 
									 {
									 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
										  {
										  	d_start_date = 1+'/'+1+'/'+i_year; 
											d_end_date = 31+'/'+1+'/'+i_year;
										//    i_total_sat_sun =   get_no_saturdays(0,i_year)
											
										  }	
										  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
										  {
										  	if(parseInt(i_year) % parseInt (4) == 0)
											{
												i_day = 29;
											}
											else
											{
												i_day = 28;
											}
											
										  	d_start_date = 1+'/'+2+'/'+i_year; 
											d_end_date = i_day+'/'+2+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(1,i_year)			
										  }		
										  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
										  {
										  	d_start_date = 1+'/'+3+'/'+i_year; 
											d_end_date = 31+'/'+3+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(2,i_year)
										  }	
										  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
										  {
										  	d_start_date = 1+'/'+4+'/'+i_year; 
											d_end_date = 30+'/'+4+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(3,i_year)		
										  }	
										  else if(i_month == 'May' || i_month == 'MAY')
										  {
										  	d_start_date = 1+'/'+5+'/'+i_year; 
											d_end_date = 31+'/'+5+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(4,i_year)			
										  }	  
										  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
										  {
										 	d_start_date = 1+'/'+6+'/'+i_year; 
											d_end_date = 30+'/'+6+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(5,i_year)			
										  }	
										  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
										  {
										  	d_start_date = 1+'/'+7+'/'+i_year; 
											d_end_date = 31+'/'+7+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(6,i_year)			
										  }	
										  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
										  {
										  	d_start_date = 1+'/'+8+'/'+i_year; 
											d_end_date = 31+'+'/'+'+8+'+'/'+'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(7,i_year)		
										  }  
										  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
										  {
										  	d_start_date = 1+'/'+9+'/'+i_year; 
											d_end_date = 30+'/'+9+'/'+i_year;	
										//	i_total_sat_sun =   get_no_saturdays(8,i_year)	
										  }	
										  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
										  {
										 	d_start_date = 1+'/'+10+'/'+i_year; 
											d_end_date = 31+'/'+10+'/'+i_year;
										//	i_total_sat_sun =   get_no_saturdays(9,i_year)			
										  }	
										  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
										  {
										  	d_start_date = 1+'/'+11+'/'+i_year; 
											d_end_date = 30+'/'+11+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(10,i_year)			
										  }	
										  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
										  {
										  	d_start_date = 1+'/'+12+'/'+i_year; 
											d_end_date = 31+'/'+12+'/'+i_year;
										//	i_total_sat_sun =  get_no_saturdays(11,i_year)			
										  }	
									 }//Month & Year
																		
								} 	
				/*
	 nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Start Date -->' + d_start_date);	
		             nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' End Date -->' + d_end_date);	
					
*/ 
					 i_total_days = getDatediffIndays(d_start_date,d_end_date);
							
					  i_total_sat_sun = getWeekend(d_start_date,d_end_date)
					  			
						//a_project_data_array+'^^^^***^^^^'+a_data_array
						
						
						 var a_pr_split_array = new Array();
						 var a_project_ID_arr;
						 var a_monthly_cal_details;
						 var a_monthly_cal_details_array  = search_monthly_calculation_details(i_employeeID,d_start_date,d_end_date)	
					//	 nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Monthly Cal Details Arr Details -->' + a_monthly_cal_details_array)						 
						   if (_logValidation(a_monthly_cal_details_array)) 
						   {
						   	 a_pr_split_array = a_monthly_cal_details_array[0].split('^^^^***^^^^');
							 a_project_ID_arr = a_pr_split_array[0];
							 a_monthly_cal_details = a_pr_split_array[1];
							
						   }
					/*
		
							
					 nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Project ID Arr Details -->' + a_project_ID_arr);	
		             nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Project ID Arr  Details Length -->' + a_project_ID_arr.length);	
					 nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Monthly Calculation Details -->' + a_monthly_cal_details);	
		             nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Monthly Calculation Details Length -->' + a_monthly_cal_details.length);	
		             nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' No. Of Sat n Sun -->' + i_total_sat_sun);	
		            			
					 
*/
					 var a_PRT_array_values = new Array()	
					 var i_data_PRT = new Array()
					 i_data_PRT =  a_project_ID_arr;
				 
				      for(var dct=0;dct<i_data_PRT.length;dct++)
					  {
						 	a_PRT_array_values = i_data_PRT.split(',')
						    break;				
					  }
					a_PRT_array_values = removearrayduplicate(a_PRT_array_values)	
				/*

					nlapiLogExecution('DEBUG', 'monthly_JE_creation','PRT Array Values-->' +a_PRT_array_values);
					nlapiLogExecution('DEBUG', 'monthly_JE_creation',' PRT Array Values Length-->' + a_PRT_array_values.length);
					
*/					 
					 var a_CRT_array_values = new Array()	
					 var i_data_CRT = new Array()
					 i_data_CRT =  a_monthly_cal_details;
				 
				      for(var dct=0;dct<i_data_CRT.length;dct++)
					  {
						 	a_CRT_array_values = i_data_CRT.split(',')
						    break;				
					  }	
				
					nlapiLogExecution('DEBUG', 'monthly_JE_creation','CRT Array Values-->' +a_CRT_array_values);
					nlapiLogExecution('DEBUG', 'monthly_JE_creation',' CRT Array Values Length-->' + a_CRT_array_values.length);
					
					var a_split_project_array = new Array();
					
				//	 if (i_emp_nt < 11) 
					 {  var a_split_project_results_array = new Array();
					 	if (_logValidation(a_CRT_array_values)&&_logValidation(a_PRT_array_values)) 
					    {												
						for(var pid=0;pid<a_PRT_array_values.length;pid++)
						{
						  var i_total_duration = 0;
						  var i_hours_per_week_total = 0;							   
						  var i_total_salary_for_month = 0;
						  var i_employee_salary_per_day = 0;
						  var i_allocated_hours_on_project = 0;
						  var i_no_of_days_worked_on_project = 0;
						  var i_no_days_in_month = 0;
						   
				        var  i_internal_ID  ;
						var i_vertical ;
						var i_practice ;
						var i_projectID ;
						var i_rate ;
						var i_duration;
						var i_item ;
						var i_hours_per_week;
						var i_allocated_hours ;
						var i_project_name;
				  
				          a_split_project_results_array = a_PRT_array_values[pid].split('^^&&&**^^');
					 			
				          	var i_project_ID_PE = a_split_project_results_array[0];
				 			//	var i_project_name_PE = a_split_project_results_array[1];
								var i_vertical_PE = a_split_project_results_array[1];
				 				var i_practice_PE = a_split_project_results_array[2];
						 
						  i_total_salary_for_month = i_cost;
						  i_no_days_in_month = parseInt(i_total_days)-parseInt(i_total_sat_sun);
						
							for (var pe = 0; pe < a_CRT_array_values.length; pe++) 
							{
								a_split_project_array = a_CRT_array_values[pe].split('&&&%%&&&');
													
								i_internal_ID   = a_split_project_array[0];
								i_vertical  = a_split_project_array[1];
								i_practice = a_split_project_array[2];
								i_projectID = a_split_project_array[3];
								i_rate = a_split_project_array[4];
								i_duration = a_split_project_array[5];
								i_item = a_split_project_array[6];
								i_hours_per_week = a_split_project_array[7];
								i_allocated_hours = a_split_project_array[8];
								i_project_name = a_split_project_array[9];
					 
							
								if(i_projectID == i_project_ID_PE)
								{
									i_total_duration = parseFloat(i_total_duration)+parseFloat(i_duration);
									i_hours_per_week_total = i_allocated_hours;									
								}						
							}							
							   i_actual_hours_worked_on_project = i_total_duration;
							   i_allocated_hours_on_project = i_hours_per_week_total;
							   
							   nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_actual_hours_worked_on_project->' +i_actual_hours_worked_on_project);
							   nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_allocated_hours_on_project-->' +i_allocated_hours_on_project);
							   
							    i_employee_salary_per_day = parseFloat(i_total_salary_for_month) / parseFloat(i_no_days_in_month)
								i_no_of_days_worked_on_project = ((parseFloat(i_actual_hours_worked_on_project))/parseFloat(8)) 
								i_project_wise_cost = parseFloat(i_employee_salary_per_day) * parseFloat(i_no_of_days_worked_on_project)
								i_project_wise_cost = parseFloat(i_project_wise_cost).toFixed(2);
							
							     nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_employee_salary_per_day-->' +i_employee_salary_per_day);
							   nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_no_of_days_worked_on_project-->' +i_no_of_days_worked_on_project);
							  nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_project_wise_cost-->' +i_project_wise_cost);
							  
							
							    i_project_wise_total = parseFloat(i_project_wise_total)+parseFloat(i_project_wise_cost);
							    i_project_wise_total = parseFloat(i_project_wise_total).toFixed(2);
                                 nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_project_wise_total-->' +i_project_wise_total);
							
							   
							   if (!_logValidation(i_practice))
								{
								  i_practice = '';												
								}
								if (!_logValidation(i_vertical))
								{
								  i_vertical = '';												
								}
								if (!_logValidation(i_project_name))
								{
								   i_project_name = '';
								}
								if (!_logValidation(i_projectID))
								{
								   i_projectID = '';
								}
							   		
								
								if(i_projectIDJE == i_project_ID_PE)
								{
									o_recordOBJ.setLineItemValue('line','debit',nt,i_project_wise_cost)
									o_recordOBJ.setLineItemValue('line','location',nt,i_location_e)
									o_recordOBJ.setLineItemValue('line','department',nt,i_practice)
						    		nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Project ******************');
							        break; 
								}
						
						
								
						}//Project ID Array						
					    }						
					 }									
				  }//Data Array Loop
							
							
						}//Employee Validate
			
	
						
					}
									
				}//Data Array
							
							
							
							
							
						}
					}
			
						   	
					   }//Employee Validation	
					   
			             	nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Project Credit (parseInt(nt)+parseInt(1)) ******************'+(parseInt(nt)+parseInt(1)));
									nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Project Credit (parseInt(nt)) ******************'+(parseInt(nt)));
							nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Project Credit Total ******************'+i_project_wise_total);
									nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Credit ******************'+i_creditJE);
							nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** Debit ******************'+i_debitJE);
							
						if(_logValidation(i_creditJE)&&!_logValidation(i_debitJE))
						{						
							
							nlapiLogExecution('DEBUG', 'monthly_JE_creation',' ********************** i_project_wise_total ******************'+i_project_wise_total);
							
							o_recordOBJ.setLineItemValue('line','credit',(parseInt(nt)),i_project_wise_total)
						}				
					}//Loop				
			     }//Line Count					
				}//Journal Present	
					
				var i_journal_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
				nlapiLogExecution('DEBUG', 'monthly_JE_creation','i_journal_submitID-->' +i_journal_submitID);
							
										
			  }//Record OBJ				
			}//Record ID			
		  }//Loop			
		}//Results		
	}//Salary Process ID	
	
}//Update Journal Entry


function removearrayduplicate(array)
{
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) 
	{
        for (var j = 0; j < array.length; j++) 
		{
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}



function  search_hourly_calculation_details(i_employee,d_start_date,d_end_date)					
{		
	var i_internal_ID;
	var i_date;
	var i_employee;
	var i_project;
	var i_item;
	var i_duration;
	var i_type;
	var i_rate;
	var i_projectID;
	var i_employeeID;
	var i_practice;
	var i_vertical;
	var i_hours_per_week;
	var a_data_array = new Array();
	var a_project_data_array = new Array();
	var a_project_ID_arr = new Array();
	var i_project_name;
	
	 if (_logValidation(i_employee) && _logValidation(d_start_date)&& _logValidation(d_end_date)) 
	 {
	  var filters = new Array();	  
	
	  filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
	  filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date)
	  filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date)
		
	  var i_search_results = nlapiSearchRecord('timebill','customsearch_salaried_time_search_s_u_2',filters, null);
		
		if (_logValidation(i_search_results))
		{
	//	nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' ----------- Hourly Time Search Length --------->' + i_search_results.length);	
		
		for (var c = 0; c < i_search_results.length; c++) 
		{
			var a_search_transaction_result = i_search_results[c];
			
			if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) 
			{
				break;
			}
			var columns = a_search_transaction_result.getAllColumns();
			
			var columnLen = columns.length;
			
			for (var hg = 0; hg < columnLen; hg++) 
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
							
				if (label == 'Internal ID') 
				{
					i_internal_ID = value;
				}
				if (label == 'Date') 
				{
					i_date = value;
				}
				if (label == 'Employee') 
				{
					i_employee = value;
				}
				if (label == 'Customer') 
				{
					i_project = value;
				}				
				if (label == 'Item') 
				{
					i_item = value;
				}
				if (label == 'Duration') 
				{
					i_duration = value;
				}
				if (label == 'Type') 
				{
					i_type = value;
				}
				if (label == 'Rate') 
				{
					i_rate = value;
				}					
				if (label == 'Project ID') 
				{
					i_projectID = value;
				}
				if (label == 'Employee ID') 
				{
					i_employeeID = value;
				}
				if (label == 'Practice') 
				{
					i_practice = value;
				}
				if (label == 'Vertical') 
				{
					i_vertical = value;
				}
				if (label == 'Hours Per Week') 
				{
					i_hours_per_week = value;
				}
				if (label == 'Project Name') 
				{
					i_project_name = value;
				}			
				
			}			
			
	/*

	nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_vertical -->' + i_vertical);	
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_practice -->' + i_practice);	
	
*/	/*
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_projectID -->' + i_projectID);	
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_rate -->' + i_rate);	
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_duration -->' + i_duration);	
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_item -->' + i_item);	
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_internal_ID -->' + i_internal_ID);
		nlapiLogExecution('DEBUG', 'search_hourly_calculation_details', ' i_hours_per_week -->' + i_hours_per_week);	
		
*/
		a_project_ID_arr[c]  = i_projectID +'^^&&&**^^'+i_vertical+'^^&&&**^^'+i_practice
		
		
		a_data_array[c] = i_internal_ID+'&&&%%&&&'+i_vertical+'&&&%%&&&'+i_practice+'&&&%%&&&'+i_projectID+'&&&%%&&&'+i_rate+'&&&%%&&&'+i_duration+'&&&%%&&&'+i_item+'&&&%%&&&'+i_hours_per_week
								
		}
			
		}
	
	 }
	
	 a_project_data_array[0] = a_project_ID_arr+'^^^^***^^^^'+a_data_array;
	
	return a_project_data_array;
}//Monthly Details


function  search_monthly_calculation_details(i_employee,d_start_date,d_end_date)					
{		
	var i_internal_ID;
	var i_date;
	var i_employee;
	var i_project;
	var i_item;
	var i_duration;
	var i_type;
	var i_rate;
	var i_projectID;
	var i_employeeID;
	var i_practice;
	var i_vertical;
	var i_hours_per_week;
	var a_data_array = new Array();
	var a_project_data_array = new Array();
	var a_project_ID_arr = new Array();
	var i_project_name;
	
	 if (_logValidation(i_employee) && _logValidation(d_start_date)&& _logValidation(d_end_date)) 
	 {
	  var filters = new Array();	  
	 // filters[0] = new nlobjSearchFilter('employee', null, 'is', 2162)
	   filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
	  filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date)
	   filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date)
		
	 	var i_search_results = nlapiSearchRecord('timebill','customsearch_salaried_time_search_s_u',filters, null);
		
	//	var i_search_results = nlapiSearchRecord('timebill',null,filters, null);
		
		if (_logValidation(i_search_results))
		{
	//		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ----------- Salaried Time Search Length --------->' + i_search_results.length);	
		
		for (var c = 0; c < i_search_results.length; c++) 
		{
			var a_search_transaction_result = i_search_results[c];
			
			if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) 
			{
				break;
			}
			var columns = a_search_transaction_result.getAllColumns();
			
			var columnLen = columns.length;
			
			for (var hg = 0; hg < columnLen; hg++) 
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
							
				if (label == 'Internal ID') 
				{
					i_internal_ID = value;
				}
				if (label == 'Date') 
				{
					i_date = value;
				}
				if (label == 'Employee') 
				{
					i_employee = value;
				}
				if (label == 'Customer') 
				{
					i_project = value;
				}				
				if (label == 'Item') 
				{
					i_item = value;
				}
				if (label == 'Duration') 
				{
					i_duration = value;
				}
				if (label == 'Type') 
				{
					i_type = value;
				}
				if (label == 'Rate') 
				{
					i_rate = value;
				}					
				if (label == 'Project ID') 
				{
					i_projectID = value;
				}
				if (label == 'Employee ID') 
				{
					i_employeeID = value;
				}
				if (label == 'Practice') 
				{
					i_practice = value;
				}
				if (label == 'Vertical') 
				{
					i_vertical = value;
				}
				if (label == 'Hours Per Week') 
				{
					i_hours_per_week = value;
				}	
				if (label == 'Allocated Hours') 
				{
					i_allocated_hours = value;
				}	
				if (label == 'Project Name') 
				{
					i_project_name = value;
				}	
				
	
		
				
			}			
			
		/*
nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_vertical -->' + i_vertical);	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_practice -->' + i_practice);	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_projectID -->' + i_projectID);	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_rate -->' + i_rate);	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_duration -->' + i_duration);	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_item -->' + i_item);	
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_internal_ID -->' + i_internal_ID);
		nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' i_hours_per_week -->' + i_hours_per_week);	
	
*/
	//	a_project_ID_arr.push(i_projectID);
		
		a_project_ID_arr[c]  = i_projectID +'^^&&&**^^'+i_vertical+'^^&&&**^^'+i_practice		
		
		a_data_array[c] = i_internal_ID+'&&&%%&&&'+i_vertical+'&&&%%&&&'+i_practice+'&&&%%&&&'+i_projectID+'&&&%%&&&'+i_rate+'&&&%%&&&'+i_duration+'&&&%%&&&'+i_item+'&&&%%&&&'+i_hours_per_week+'&&&%%&&&'+i_allocated_hours        
								
		}
			
		}
	
	 }
	 
	 a_project_data_array[0] = a_project_ID_arr+'^^^^***^^^^'+a_data_array;
	
	return a_project_data_array;
}//Monthly Details



function get_month_details(i_month,i_year)
{
 var d_start_date = '';
 var d_end_date = '';
 var i_day;
 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if(parseInt(i_year) % parseInt (4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
		
	  	d_start_date = 1+'/'+2+'/'+i_year; 
		d_end_date = i_day+'/'+2+'/'+i_year;			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 30+'/'+1+'/'+i_year;			
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;			
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {
	 	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 30+'/'+1+'/'+i_year;			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;			
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 30+'/'+1+'/'+i_year;		
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {
	 	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;			
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 30+'/'+1+'/'+i_year;			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
		d_end_date = 31+'/'+1+'/'+i_year;			
	  }	
 }//Month & Year	
}

 function get_no_saturdays(i_month,i_year)
//function get_no_saturdays()
{
var i_no_of_sat = 0	;
var i_no_of_sun = 0	;
	
var d = new Date();
var getTot = daysInMonth(i_month,i_year); //Get total days in a month
var sat = new Array();   //Declaring array for inserting Saturdays
var sun = new Array();   //Declaring array for inserting Sundays
for(var i=1;i<=getTot;i++)
{    //looping through days in month
    var newDate = new Date(i_month,i_year,i)
    if(newDate.getDay()==0)
	{   //if Sunday
        sat.push(i);
		i_no_of_sat++;
    }
    if(newDate.getDay()==6)
	{   //if Saturday
        sun.push(i);
		i_no_of_sun++;
    }

}

 var i_total_days_sat_sun = parseInt(i_no_of_sat)+parseInt(i_no_of_sun);
 return i_total_days_sat_sun ;
}

function daysInMonth(month,year) 
{
    return new Date(year, month, 0).getDate();
}
function removearrayduplicate(array)
{
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) 
	{
        for (var j = 0; j < array.length; j++) 
		{
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}
function get_todays_date()
{
	var today;
	// ============================= Todays Date ==========================================
		
	  var date1 = new Date();
      nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
   //  nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
     var day = istdate.getDate();
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
	 
     var month = istdate.getMonth()+1;
    // nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	 var year=istdate.getFullYear();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' +year);
	  
	 var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' +date_format);
	 
	 if (date_format == 'YYYY-MM-DD')
	 {
	 	today = year + '-' + month + '-' + day;		
	 } 
	 if (date_format == 'DD/MM/YYYY') 
	 {
	  	today = day + '/' + month + '/' + year;
	 }
	 if (date_format == 'MM/DD/YYYY')
	 {
	  	today = month + '/' + day + '/' + year;
	 }	 
	 return today;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function get_start_date(i_year,i_month)
{
	var d_start_date;
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {
	  	d_start_date = 1+'/'+1+'/'+i_year; 
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if(parseInt(i_year) % parseInt (4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
		
	  	d_start_date = 1+'/'+2+'/'+i_year; 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {
	  	d_start_date = 1+'/'+3+'/'+i_year; 
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	d_start_date = 1+'/'+4+'/'+i_year; 
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {
	  	d_start_date = 1+'/'+5+'/'+i_year; 
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {
	 	d_start_date = 1+'/'+6+'/'+i_year; 
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {
	  	d_start_date = 1+'/'+7+'/'+i_year; 
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {
	  	d_start_date = 1+'/'+8+'/'+i_year; 
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {
	  	d_start_date = 1+'/'+9+'/'+i_year; 
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {
	 	d_start_date = 1+'/'+10+'/'+i_year; 
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {
	  	d_start_date = 1+'/'+11+'/'+i_year; 
	 }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {
	  	d_start_date = 1+'/'+12+'/'+i_year; 
	  }	
 }//Month & Year
// nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Start Date -->' + d_start_date);	
 return d_start_date;
	
}

function get_end_date(i_year,i_month)
{	
 var d_end_date = '';	       		
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {						  
		d_end_date = 31+'/'+1+'/'+i_year;							
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if(parseInt(i_year) % parseInt (4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}						
	
		d_end_date = i_day+'/'+2+'/'+i_year;
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {						
		d_end_date = 31+'/'+3+'/'+i_year;						
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {						
		d_end_date = 30+'/'+4+'/'+i_year;										
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {						  
		d_end_date = 31+'/'+5+'/'+i_year;								
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {						
		d_end_date = 30+'/'+6+'/'+i_year;									
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {						 
		d_end_date = 31+'/'+7+'/'+i_year;										
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {						  
		d_end_date = 31+'+'/'+'+8+'+'/'+'+i_year;							
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {						  	 
		d_end_date = 30+'/'+9+'/'+i_year;						
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {						 
		d_end_date = 31+'/'+10+'/'+i_year;								
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {						  
		d_end_date = 30+'/'+11+'/'+i_year;								
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {						  
		d_end_date = 31+'/'+12+'/'+i_year;									
	  }	
 }//Month & Year
					
	//	             nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' End Date -->' + d_end_date);	
					
	
	
	return d_end_date;
	
	
}


function getWeekend(dString1,dString2) 
{ 
  var aDay = 24*60*60*1000;	
  var Sat = 0;
  var Sun =0;
 /*
 
  var d1 = new Date(Date.parse(dString1)); //"MM/DD/YYYY"
  var d2 = new Date(Date.parse(dString2));
  
*/
  var d1 = nlapiStringToDate(dString1);
  var d2 = nlapiStringToDate(dString2);
 
  
 /*
 var weekend = 
  {
    Sat:0,
    Sun:0
  }
*/
  for (var d,i=d1.getTime(), n=d2.getTime();i<=n;i+=aDay) 
  {
      d=new Date(i).getDay();
     // document.write("<br>"+new Date(i)+":"+d); // remove when happy
	 //nlapiLogExecution('DEBUG', 'hourly_JE_creation', new Date(i)+":"+d);	

      if (d===6) Sat++;
      if (d===0) Sun++;
  }
  //return weekend;

  var i_total_days = parseInt(Sat)+parseInt(Sun);	
  
   /*
 
  nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' Sat -->' + Sat);	
nlapiLogExecution('DEBUG', 'hourly_JE_creation', 'Sun-->' + Sun);	
nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' i_total_days -->' + i_total_days);	

  
*/
  return i_total_days;	
  
  				
}

function getDatediffIndays(startDate, endDate)
{
     var one_day=1000*60*60*24;
     var fromDate = startDate;
    
	 var toDate=endDate
     nlapiLogExecution('DEBUG', 'getDatediffIndays', 'From Date = '+fromDate+'To Date = '+toDate);
   
     var x=fromDate.split("/");
     var y=toDate.split("/");
     var date1=new Date(x[2],(x[1]-1),x[0]);
     var date2=new Date(y[2],(y[1]-1),y[0]);
	 /*

	  var date1= nlapiStringToDate(fromDate);
	  var date2 = nlapiStringToDate(toDate)
	 
*/
	 nlapiLogExecution('DEBUG', 'getDatediffIndays', 'date1 = '+date1);

     nlapiLogExecution('DEBUG', 'getDatediffIndays', 'date2 = '+date2);


     var month1=x[1]-1;
     var month2=y[1]-1;
	 nlapiLogExecution('DEBUG', 'getDatediffIndays', 'month1 = '+month1);

nlapiLogExecution('DEBUG', 'getDatediffIndays', 'month2 = '+month2);


 nlapiLogExecution('DEBUG', 'getDatediffIndays', 'date2.getTime() = '+date2.getTime());

nlapiLogExecution('DEBUG', 'getDatediffIndays', 'date1.getTime() = '+date1.getTime());


     var Diff = Math.ceil((date2.getTime()-date1.getTime())/(one_day));
    nlapiLogExecution('DEBUG', 'getDatediffIndays', 'Date difference Return Value = '+Diff);

     return (Diff+1);
}


// END FUNCTION =====================================================

