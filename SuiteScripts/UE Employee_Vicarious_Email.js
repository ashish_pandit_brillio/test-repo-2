/**
 *@NApiVersion 2.0
 *@NModuleScope Public
 *@NScriptType UserEventScript
**/

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
/*
  Date		    : 31/01/2022
  Author		  : Raghav Gupta
  Remarks		  : Userevent Script to set vicariousemail permission on an employee record whenever a record is created or edited.
  Script Name	: UE Employee_Vicarious_Email.js
  Script Type	: Userevent Script (2.0)

  Script Modification Log:
  -- Date --			-- Modified By --				--Requested By--				-- Description --

  Function used and their Descriptions:
  myBeforeSubmit FUNCTION         : Function to set vicarious email permission in create or edit mode.
*/
// END SCRIPT DESCRIPTION BLOCK  ===================


define(['N/record'], function (record) {
  // Begin myBeforeSubmit
  function myBeforeSubmit(context) {
    log.debug('script sctarted');
    try {
      // log.debug('context', context);
      // log.debug('context', context.type);
      if (context.type == 'edit' || context.type == 'create') {
        var currentRecord = context.newRecord;
        var count = 0;
        var index = 0
        var flag = false;

        var inactive_status = currentRecord.getValue('custentity_employee_inactive');
        // log.debug('inactive_status', inactive_status);
        if (inactive_status == 'T') {

          // var comment = currentRecord.getValue('comments');
          // if (comment == 'abc') {
          count = currentRecord.getLineCount('empperms');
          if (count != 0) {
            for (var i = 0; i < count; i++) {
              var sublist_value = currentRecord.getSublistValue('empperms', 'permkey1', i);
              if (sublist_value == 'ADMI_VICARIOUS_EMAILS') {
                index = i;
                flag = true;
              }
            }
            if (flag) {
              currentRecord.setSublistValue('empperms', 'permlevel1', index, 0);
            }
          }
        }
        else {
          var i_person_type = currentRecord.getValue('custentity_employeetype');
          var i_employee_type = currentRecord.getValue('custentity_persontype');
          if (i_person_type == 1 || i_employee_type == 2) {
            count = currentRecord.getLineCount('empperms');
            if (count != 0) {
              for (var i = 0; i < count; i++) {
                var sublist_value = currentRecord.getSublistValue('empperms', 'permkey1', i);
                if (sublist_value == 'ADMI_VICARIOUS_EMAILS') {
                  index = i
                  flag = true;
                }
              }
            }
            if (count == 0) {
              currentRecord.setSublistValue('empperms', 'permkey1', 0, 'ADMI_VICARIOUS_EMAILS');
              currentRecord.setSublistValue('empperms', 'permlevel1', 0, 4);
            }
            else if (flag) {
              currentRecord.setSublistValue('empperms', 'permkey1', index, 'ADMI_VICARIOUS_EMAILS');
              currentRecord.setSublistValue('empperms', 'permlevel1', index, 4);
            }
            else if (flag == false) {
              currentRecord.setSublistValue('empperms', 'permkey1', count, 'ADMI_VICARIOUS_EMAILS');
              currentRecord.setSublistValue('empperms', 'permlevel1', count, 4);
            }
          }
        }
      }
      log.debug('script ended');
    }
    catch (err) {
      log.error('error', err);
    }
  }
  // End myBeforeSubmit
  return {
    beforeSubmit: myBeforeSubmit
  };
});
