/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Nov 2018     Sai Saranya
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){

	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var a_project_list = new Array();
		var a_project_search_results = '';
		
	
		// design form which will be displayed to user
		var o_form_obj = nlapiCreateForm("FLASH Detail Page");
		var user = request.getParameter('user');
		//var user = 3169;                                                                                  
		//if(user == 62082) //Added user Deepak to check the functionality 
		//   user =7905;
		nlapiLogExecution('DEBUG', 'delivery manager', user);
		 
		var a_project_filters = new Array();
		a_project_filters[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
		a_project_filters[1] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
		a_project_filters[2] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'FBM');
		a_project_filters[3] = new nlobjSearchFilter('custentity_fp_rev_rec_type', null, 'anyof', [1,2,4]);
		a_project_filters[4] = new nlobjSearchFilter('custentity_discount_project',null,'is','F');
		a_project_filters[5] = new nlobjSearchFilter('custentity_deliverymanager',null,'anyof',user);
		a_project_filters[6] = new nlobjSearchFilter('enddate',null,'onorafter','startofthismonth');
		//a_project_filters[6] = new nlobjSearchFilter('internalid', null, 'anyof',136816);
		var a_proj_column = new Array();
		a_proj_column[0]= new nlobjSearchColumn('customer');
		a_proj_column[1]= new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_proj_column[2]= new nlobjSearchColumn('custentity_projectvalue');
		a_proj_column[3]= new nlobjSearchColumn('custentity_ytd_rev_recognized');
		//a_proj_column[0]= new nlobjSearchColumn('customer');
		var a_project_search_results = searchRecord('job', null, a_project_filters, a_proj_column);
		// ******************************************** T&M project Search *************************************************//
		var a_project_filter = [['custentity_deliverymanager', 'anyof', user],'and',
			['status', 'noneof', 1], 'and',
			['jobtype', 'anyof', 2], 'and',
			['jobbillingtype', 'anyof', 'TM'],'and',
			['enddate','onorafter','startofthismonth'],'and',
			['custentity_discount_project','is','F']
			];
			var a_columns_proj_srch = new Array();
			a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
			a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
			a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
			a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
			var a_project_search_results_tm = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
			// ******************************************** T&M project Search Ends *************************************************//
		if(a_project_search_results)
		{
		
			var a_JSON = {};
			var s_create_update_mode = '';
		
			var tm_linkUrl = nlapiResolveURL('SUITELET','1626','customdeploy_sut_displayprojectdetails');
			var other_linkUrl = nlapiResolveURL('SUITELET','1516','customdeploy_mnth_end_other');
			var a_get_logged_in_user_exsiting_revenue_cap = '';
			var a_revenue_cap_filter = ['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', user];
						
			
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_project');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
			a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
			a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
			 a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if(a_get_logged_in_user_exsiting_revenue_cap)
			{
				var a_existig_cap_projects = new Array();
				var a_existing_proj_cap_details = new Array();
				var i_index_existing_pro = 0;
				for(var i_existing_cap = 0; i_existing_cap < a_get_logged_in_user_exsiting_revenue_cap.length; i_existing_cap++)
				{
					if(a_existig_cap_projects.indexOf(a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project')) < 0)
					{
						//nlapiLogExecution('audit','existing proj index:- ',a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						a_existig_cap_projects.push(a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						a_existing_proj_cap_details[i_index_existing_pro] = {
																				'proj_intenal_id': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'),
																				'pro_revenue_share': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_total'),
																				'existing_rcrd_id': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getId(),
																				'existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_approval_status'),
																				's_existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getText('custrecord_revenue_share_approval_status')
																				
																			};
						i_index_existing_pro++;
					}					
				}	
			}
			//****************** For FP OTHER DATA*******************//
			var a_get_fp_other_projects = '';
			var a_other_filter =	['custrecord_fp_rev_rec_others_projec.custentity_deliverymanager', 'anyof', user];
								
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('created').setSort(true);
			a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
			a_get_fp_other_projects = searchRecord('customrecord_fp_rev_rec_others_parent', null, a_other_filter, a_columns_existing_cap_srch);
			if(a_get_fp_other_projects)
			{
				var a_existig_other_projects = new Array();
				var a_existing_other_cap_details = new Array();
				var i_index_existing_pro = 0;
				for(var i_existing_cap = 0; i_existing_cap < a_get_fp_other_projects.length; i_existing_cap++)
				{
					if(a_existig_other_projects.indexOf(a_get_fp_other_projects[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec')) < 0)
					{
						//nlapiLogExecution('audit','existing proj index:- ',a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						
						a_existig_other_projects.push(a_get_fp_other_projects[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'));
						a_existing_other_cap_details[i_index_existing_pro] = {
																				'proj_intenal_id': a_get_fp_other_projects[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'),
																				'existing_rcrd_id': a_get_fp_other_projects[i_existing_cap].getId(),
																				'existing_rcrd_status': a_get_fp_other_projects[i_existing_cap].getValue('custrecord_revenue_other_approval_status'),
																				's_existing_rcrd_status': a_get_fp_other_projects[i_existing_cap].getText('custrecord_revenue_other_approval_status')
																				
																			};
						i_index_existing_pro++;
					}					
				}	
			}
			//
			for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var i_project_value_ytd = a_project_search_results[i_pro_index].getValue('custentity_ytd_rev_recognized');
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				var i_project_sow_value = a_project_search_results[i_pro_index].getValue('custentity_projectvalue');
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				var proj_type= a_project_search_results[i_pro_index].getValue('custentity_fp_rev_rec_type');
				if(proj_type == 1)
				{
					var f_project_existing_revenue = 0;
					var i_existing_revenue_share_rcrd_id = 0;
					var b_is_proj_value_chngd_flag = 'F';
					var s_existing_revenue_share_status = '';
					var linkUrl	=	nlapiResolveURL('SUITELET', '1139', 'customdeploy_sut_fp_rev_rec_proj_details');
					if(a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId()) >= 0)
					{
						//nlapiLogExecution('audit','a_existig_cap_projects '+a_project_search_results[i_pro_index].getId,a_existig_cap_projects.length);
						s_create_update_mode = 'Update';
						
						s_revenue_share_status = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].s_existing_rcrd_status;
						if(!s_revenue_share_status)
						{
							s_revenue_share_status = 'Saved';
						}
						
						f_project_existing_revenue = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].pro_revenue_share;
						i_existing_revenue_share_rcrd_id = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_id;
						s_existing_revenue_share_status = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_status;
					}
					
					var i_project_value_ytd = a_project_search_results[i_pro_index].getValue('custentity_ytd_rev_recognized');
					
					if(!i_project_value_ytd)
						i_project_value_ytd = 0;
					
				
					if((parseFloat(f_project_existing_revenue) != parseFloat(i_project_sow_value)) && s_create_update_mode != 'Create')
					{
						s_existing_revenue_share_status = '';
						s_revenue_share_status = 'Saved';
						b_is_proj_value_chngd_flag = 'T';
						
					}
				a_JSON = {
							project_cutomer: a_project_search_results[i_pro_index].getValue('customer'),
							project_id: a_project_search_results[i_pro_index].getId(),
							project_value: i_project_sow_value,
							revenue_share_status: s_revenue_share_status,
						
							project_rev_rec_type: a_project_search_results[i_pro_index].getValue('custentity_fp_rev_rec_type'),
							suitelet_url: linkUrl + '&proj_id=' + a_project_search_results[i_pro_index].getId() +'&mode=' + s_create_update_mode +'&existing_rcrd_id=' +i_existing_revenue_share_rcrd_id+'&revenue_share_status='+s_existing_revenue_share_status+'&proj_val_chngd='+b_is_proj_value_chngd_flag
						};
				
				a_project_list.push(a_JSON);
					}
				else if(proj_type == 4 || proj_type == 2)
				{
					
					var a_JSON = {};
					var s_create_update_mode = '';
					var linkUrl	=	nlapiResolveURL('SUITELET', '1516', 'customdeploy_mnth_end_other');
				//	var linkUrl2	=	nlapiResolveURL('SUITELET', '1515', 'customdeploy_fp_rev_rec_othrs_setup');
					
	
						var i_existing_revenue_share_rcrd_id = 0;
						var b_is_proj_value_chngd_flag = 'F';
						var s_existing_revenue_share_status = '';
						if(a_existig_other_projects.indexOf(a_project_search_results[i_pro_index].getId()) >= 0)
						{
							//nlapiLogExecution('audit','a_existig_cap_projects '+a_project_search_results[i_pro_index].getId,a_existig_cap_projects.length);
							var s_revenue_share_status_text = a_existing_other_cap_details[a_existig_other_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_status;
							var s_revenue_share_status = a_existing_other_cap_details[a_existig_other_projects.indexOf(a_project_search_results[i_pro_index].getId())].s_existing_rcrd_status;
							if(s_revenue_share_status_text==2)
							s_create_update_mode = 'Update';
							
							
							i_existing_revenue_share_rcrd_id = a_existing_other_cap_details[a_existig_other_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_id;
							s_existing_revenue_share_status = a_existing_other_cap_details[a_existig_other_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_status;
						}				
							
						
				
						if(s_create_update_mode != 'Create' )
						{
							
							a_JSON = {
									project_cutomer: a_project_search_results[i_pro_index].getValue('customer'),
									project_id: a_project_search_results[i_pro_index].getId(),
									project_value: i_project_sow_value,
									revenue_share_status: s_revenue_share_status,
									//project_existing_value: f_project_existing_revenue,
									project_rev_rec_type: a_project_search_results[i_pro_index].getValue('custentity_fp_rev_rec_type'),
									suitelet_url: linkUrl + '&proj_id=' + a_project_search_results[i_pro_index].getId() +'&mode=' + s_create_update_mode +'&existing_rcrd_id=' +i_existing_revenue_share_rcrd_id+'&revenue_share_status='+s_existing_revenue_share_status+'&proj_val_chngd='+b_is_proj_value_chngd_flag
								};
						}
						a_project_list.push(a_JSON);
					
				}
			}
		}
		if(a_project_search_results_tm)
		{	
			var a_JSON = {};
			var s_create_update_mode = '';
			var linkUrl	= nlapiResolveURL('SUITELET', 'customscript_sut_displayprojectdetails', 'customdeploy_sut_displayprojectdetails'); 
			for(var i_pro_index = 0; i_pro_index < a_project_search_results_tm.length; i_pro_index++)
			{
				var i_project_internal_id = a_project_search_results_tm[i_pro_index].getId();
				var f_project_existing_revenue = 0;
				var i_existing_revenue_share_rcrd_id = 0;
				var b_is_proj_value_chngd_flag = 'F';
				var s_existing_revenue_share_status = '';
				var i_project_sow_value = a_project_search_results_tm[i_pro_index].getValue('custentity_projectvalue');
				if(!i_project_sow_value)
					i_project_sow_value =0;
				var s_revenue_share_status = 'No Active Allocation';
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				s_revenue_share_status = 'Active';
				a_JSON = {
						project_cutomer: a_project_search_results_tm[i_pro_index].getValue('customer'),
						project_id: a_project_search_results_tm[i_pro_index].getId(),
						project_value: i_project_sow_value,
						revenue_share_status: s_revenue_share_status,
						project_rev_rec_type: a_project_search_results_tm[i_pro_index].getValue('custentity_fp_rev_rec_type'),
						suitelet_url: linkUrl + '&proj_id=' + a_project_search_results_tm[i_pro_index].getId()
					};
					a_project_list.push(a_JSON);
			}
		
		}
	
		else
		{
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
		}
		//create sublist for project
		if(a_project_search_results_tm || a_project_search_results)
		{
			var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
			f_form_sublist.addField('project_cutomer','select','Customer','customer').setDisplayType('inline');
			f_form_sublist.addField('project_id','select','Project','job').setDisplayType('inline');
			f_form_sublist.addField('project_value','currency','Project Value');
			f_form_sublist.addField('revenue_share_status','text','Project Setup Status');
			//f_form_sublist.addField('project_existing_value','currency','Captured Revenue Share');
			f_form_sublist.addField('project_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
			f_form_sublist.addField('suitelet_url', 'url','').setLinkText('Update');
			f_form_sublist.setLineItemValues(a_project_list);
		}
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','function suiteletFunction_FP_RevRec_Dashboard','ERROR MESSAGE :- '+err);
	}
}
