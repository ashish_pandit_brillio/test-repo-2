function getTagged_Active_TM(i_project_id) {
    try {
        //  var searchRes = searchRecord('resourceallocation', 'customsearch1521', null, null);
        var praticeData = practiceSearch();
        if (_logValidation(i_project_id)) {
            //search tm data
            var filtr = [];
            filtr.push(new nlobjSearchFilter('custrecord_tm_data_project', null, 'anyof', i_project_id));
          	filtr.push(new nlobjSearchFilter('custrecord_rampup_type', null, 'isnot', 'Total'));
			filtr.push(new nlobjSearchFilter('custrecord_is_confirm', 'custrecord_tm_json_parent', 'is', 'T'));//send the JSON for only confimed Numbers
			
            var col = [];
            col.push(new nlobjSearchColumn('internalid').setSort(true));
            col.push(new nlobjSearchColumn('custrecord_tm_month'));
            col.push(new nlobjSearchColumn('custrecord_tm_year'));
            col.push(new nlobjSearchColumn('custrecord_tm_amount'));
            col.push(new nlobjSearchColumn('custrecord_rampup_type'));
            col.push(new nlobjSearchColumn('custrecord_tm_sub_practice'));
            col.push(new nlobjSearchColumn('custrecord_total_share'));
            col.push(new nlobjSearchColumn('custrecord_tm_json_parent'));
            col.push(new nlobjSearchColumn('custrecord_tm_data_project'));
			col.push(new nlobjSearchColumn('custentity_practice','custrecord_tm_data_project',null));

            var customrecord_tm_month_total_Search = searchRecord('customrecord_forecast_revenue_tm', null, filtr, col);
            //Loop to fetch project TM data
            var pro_duplicate = [];
            var json_parent = '';
            var dataRow_filtered = [];
            var s_subPrac = [];
           // var dataRow = [];
            if (_logValidation(customrecord_tm_month_total_Search)) {
				//nlapiLogExecution('debug','customrecord_tm_month_total_Search.length ',customrecord_tm_month_total_Search.length);
                for (var j = 0; j < customrecord_tm_month_total_Search.length; j++) {
                	 var dataRow = [];
                	 var JSON_O ={};
                	 var i_subPractice_value = '';
                	 var subPrac_parse = '';
                    if (pro_duplicate.indexOf(customrecord_tm_month_total_Search[j].getValue('custrecord_tm_data_project')) < 0 ||
                        parseInt(json_parent) == parseInt(customrecord_tm_month_total_Search[j].getValue('custrecord_tm_json_parent'))) {

                        //Logic to fetch the data based on subpractice
                    	
                    	json_parent = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_json_parent');
                        var i_project = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_data_project');
                        var i_ID = customrecord_tm_month_total_Search[j].getValue('internalid');
                        var context = nlapiGetContext();
                        i_subPractice_value = customrecord_tm_month_total_Search[j].getValue('custrecord_total_share');
                        var s_month = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_month');
                        var s_year = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_year');
                        var f_amount = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_amount');
                        var s_rampUp_type = customrecord_tm_month_total_Search[j].getValue('custrecord_rampup_type');
                        var i_subPractice = customrecord_tm_month_total_Search[j].getValue('custrecord_tm_sub_practice');
                        var s_subPractice = customrecord_tm_month_total_Search[j].getText('custrecord_tm_sub_practice');
						
                        //nlapiLogExecution('debug','s_rampUp_type ',s_rampUp_type);
                        if(s_rampUp_type != 'Total' && _logValidation(i_subPractice_value)){
                        subPrac_parse = JSON.parse(i_subPractice_value);
                        if (_logValidation(subPrac_parse)) {
                            for (var k = 0; k < subPrac_parse.length; k++) {
                                var s_temp = subPrac_parse[k];
                                var s_subpractice = s_temp.practice;
                                var f_share = s_temp.share;
                            }
                        }
                        }
                        // var a_pract = remove_dulpi(unique_practice);
                        var job_lookUp = nlapiLookupField('job', parseInt(i_project), ['custentity_project_currency', 'custentity_vertical', 'customer','entityid','companyname'], true);
                        var s_proj_currency = job_lookUp.custentity_project_currency;

						 
                      
                        var i_master_currency=1;
                        //Logic to restrict JSON only for future months
                        var d_today = nlapiDateToString(new Date());
                        // d_today = nlapiAddMonths(nlapiStringToDate(d_today), -1);
                        //var d_today = '1/1/2018';
                        d_today = nlapiStringToDate(d_today);

                        var getCurrentMonth = d_today.getMonth() + 1;
                        var getCurrentYear = d_today.getFullYear();

                        //Report Months Capture

                        var job_lookUp_Obj = nlapiLookupField('job', parseInt(i_project), ['entityid', 'companyname']);
                        //var data_month_ind = data_month_All[indxx].split('_');

                        //var data_m_ind = data_month_ind[0];
                        var d_month = getMonthCompleteIndex(s_month);
                        var data_year = s_year.split('.')[0];
                        //Get Start & End Date
                        var date_obj = get_month_start_end_date(s_month, data_year);
                        //	d_month = parseInt(d_month);
						var projectPractice = customrecord_tm_month_total_Search[j].getText('custentity_practice','custrecord_tm_data_project');
                       

                        if (_logValidation(subPrac_parse) && s_rampUp_type == '1') {

                            for (var k = 0; k < subPrac_parse.length; k++) {
                                var s_temp = subPrac_parse[k];
                                var s_subpractice = s_temp.practice;
								 //Add the logic to check whether delivery practice or not
                                
                                for(var jj = 0; jj < praticeData.length; jj++){
                                	if(praticeData[jj].name.trim() == s_subpractice.trim()){
                                		
                                		var is_delivery = praticeData[jj].IsDelivery;
                                		if(is_delivery == 'F'){
                                			s_subpractice = projectPractice;
                                		}
                                	}
                                }
                                var f_share = s_temp.share;

                                if (parseInt(d_month) >= parseInt(getCurrentMonth) && parseInt(data_year) >= parseInt(getCurrentYear)) {

                                	JSON_O = {
                                			'Vertical': job_lookUp.custentity_vertical,
                                            'Department': s_subpractice,
                                            'ParentDepartment': s_subpractice.split(':')[0].trim(),
                                            'ProjectID': job_lookUp_Obj.entityid,
                                            'ProjectName': job_lookUp_Obj.companyname,
                                            'CustomerName': job_lookUp.customer,
                                            'Amount': (i_master_currency * parseFloat(f_share)).toFixed(2),
                                            'ProjectCurrency': s_proj_currency
                                          //  'Type': s_rampUp_type
                                        };
                                	dataRow.push(JSON_O);
                                 
                              } else if (parseInt(data_year) > parseInt(getCurrentYear)) {

                              	JSON_O = {
                              			'Vertical': job_lookUp.custentity_vertical,
                                        'Department': s_subpractice,
                                        'ParentDepartment': s_subpractice.split(':')[0].trim(),
                                        'ProjectID': job_lookUp_Obj.entityid,
                                        'ProjectName': job_lookUp_Obj.companyname,
                                        'CustomerName': job_lookUp.customer,
                                        'Amount': (i_master_currency * parseFloat(f_share)).toFixed(2),
                                        'ProjectCurrency': s_proj_currency
                                       // 'Type': s_rampUp_type
                                        };
                              	dataRow.push(JSON_O);
                                   
                                }
                                
                            }
                        }
                        //IF Not Total type
                        if ( s_rampUp_type == '1' && (parseInt(d_month) >= parseInt(getCurrentMonth) && parseInt(data_year) >= parseInt(getCurrentYear))) {

                        	JSON_O = {
                        			'Vertical': job_lookUp.custentity_vertical,
                                    'Department': s_subPractice,
                                    'ParentDepartment': s_subPractice.split(':')[0].trim(),
                                    'ProjectID': job_lookUp_Obj.entityid,
                                    'ProjectName': job_lookUp_Obj.companyname,
                                    'CustomerName': job_lookUp.customer,
                                    'Amount': (i_master_currency * f_amount).toFixed(2),
                                    'ProjectCurrency': s_proj_currency
                                   // 'Type': s_rampUp_type
                                };
                        	dataRow.push(JSON_O);
                         
                      } else if (s_rampUp_type == '1' &&  (parseInt(data_year) > parseInt(getCurrentYear))) {

                      	JSON_O = {
                      			'Vertical': job_lookUp.custentity_vertical,
                                'Department': s_subPractice,
                                'ParentDepartment': s_subPractice.split(':')[0].trim(),
                                'ProjectID': job_lookUp_Obj.entityid,
                                'ProjectName': job_lookUp_Obj.companyname,
                                'CustomerName': job_lookUp.customer,
                                'Amount': (i_master_currency * f_amount).toFixed(2),
                                'ProjectCurrency': s_proj_currency
                                };
							dataRow.push(JSON_O);
                           
                        }
                        
                        pro_duplicate.push(customrecord_tm_month_total_Search[j].getValue('custrecord_tm_data_project'));

                    }
					//nlapiLogExecution('debug','dataRow  ',JSON.stringify(dataRow));
                    if(_logValidation(dataRow)){
                    dataRow_filtered.push(dataRow);
                    }
                }
            }
        }
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Data Search Error', e);
        throw e;
    }
    return dataRow_filtered;
}
//search for practice master
function practiceSearch(){
	try{
		var departmentSearch = nlapiSearchRecord("department",null,
				[
				   ["isinactive","is","F"]
				], 
				[
				   new nlobjSearchColumn("name").setSort(false), 
				   new nlobjSearchColumn("custrecord_parent_practice"), 
				   new nlobjSearchColumn("custrecord_is_delivery_practice")
				]
				);
		var datarow = [];
		
		if(departmentSearch){
			for(var k=0;k<departmentSearch.length;k++){
				
				var json = {
						'name': departmentSearch[k].getValue('name'),
						'parentPractice': departmentSearch[k].getValue('custrecord_parent_practice'),
						'IsDelivery': departmentSearch[k].getValue('custrecord_is_delivery_practice')
						
				}
				datarow.push(json);
			}
		}
		return datarow;
		
	}
	catch(e){
		nlapiLogExecution('ERROR','Get Project Internal ID Function',e);
	}
}
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
function getMonthCompleteIndex(month) {
    var s_mont_indx = '';
    if (month == 'JAN')
        s_mont_indx = 1;
    if (month == 'FEB')
        s_mont_indx = 2;
    if (month == 'MAR')
        s_mont_indx = 3;
    if (month == 'APR')
        s_mont_indx = 4;
    if (month == 'MAY')
        s_mont_indx = 5;
    if (month == 'JUN')
        s_mont_indx = 6;
    if (month == 'JUL')
        s_mont_indx = 7;
    if (month == 'AUG')
        s_mont_indx = 8;
    if (month == 'SEP')
        s_mont_indx = 9;
    if (month == 'OCT')
        s_mont_indx = 10;
    if (month == 'NOV')
        s_mont_indx = 11;
    if (month == 'DEC')
        s_mont_indx = 12;

    return s_mont_indx;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
function get_month_start_end_date(i_month, i_year) {
    var d_start_date = '';
    var d_end_date = '';

    var date_format = checkDateFormat();

    if (_logValidation(i_month) && _logValidation(i_year)) {
        if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 1 + '-' + 1;
                d_end_date = i_year + '-' + 1 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
               d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 1 + '/' + 1 + '/' + i_year;
                d_end_date = 1 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
            if ((i_year % 4) == 0) {
                i_day = 29;
            } else {
                i_day = 28;
            }

            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 2 + '-' + 1;
                d_end_date = i_year + '-' + 2 + '-' + i_day;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 2 + '/' + 1 + '/' + i_year;
                d_end_date = 2 + '/' + i_day + '/' + i_year;
            }

        } else if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 3 + '-' + 1;
                d_end_date = i_year + '-' + 3 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
               d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 3 + '/' + 1 + '/' + i_year;
                d_end_date = 3 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 4 + '-' + 1;
                d_end_date = i_year + '-' + 4 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 4 + '/' + 1 + '/' + i_year;
                d_end_date = 4 + '/' + 30 + '/' + i_year;
            }


        } else if (i_month == 'May' || i_month == 'MAY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 5 + '-' + 1;
                d_end_date = i_year + '-' + 5 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 5 + '/' + 1 + '/' + i_year;
                d_end_date = 5 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 6 + '-' + 1;
                d_end_date = i_year + '-' + 6 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
               d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 6 + '/' + 1 + '/' + i_year;
                d_end_date = 6 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 7 + '-' + 1;
                d_end_date = i_year + '-' + 7 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 7 + '/' + 1 + '/' + i_year;
                d_end_date = 7 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 8 + '-' + 1;
                d_end_date = i_year + '-' + 8 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 8 + '/' + 1 + '/' + i_year;
                d_end_date = 8 + '/' + 31 + '/' + i_year;
            }

        } else if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 9 + '-' + 1;
                d_end_date = i_year + '-' + 9 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 9 + '/' + 1 + '/' + i_year;
                d_end_date = 9 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 10 + '-' + 1;
                d_end_date = i_year + '-' + 10 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 10 + '/' + 1 + '/' + i_year;
                d_end_date = 10 + '/' + 31 + '/' + i_year;
            }
        } else if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 11 + '-' + 1;
                d_end_date = i_year + '-' + 11 + '-' + 30;
            }
            if (date_format == 'M/D/YYYY') {
                d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 11 + '/' + 1 + '/' + i_year;
                d_end_date = 11 + '/' + 30 + '/' + i_year;
            }

        } else if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
            if (date_format == 'YYYY-MM-DD') {
                d_start_date = i_year + '-' + 12 + '-' + 1;
                d_end_date = i_year + '-' + 12 + '-' + 31;
            }
            if (date_format == 'M/D/YYYY') {
                 d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
            }
            if (date_format == 'MM/DD/YYYY') {
                d_start_date = 12 + '/' + 1 + '/' + i_year;
                d_end_date = 12 + '/' + 31 + '/' + i_year;
            }

        }
    } //Month & Year
    return d_start_date + '#' + d_end_date;
}