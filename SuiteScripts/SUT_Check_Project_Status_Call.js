/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Check_Project_Status_Call.js
	Author      : Shweta Chopde
	Date        : 5 June 2014
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
	var i_status;
	try
	{
	 if (request.getMethod() == 'GET') 
	 {
	 	 var i_project = request.getParameter('custscript_project_check_status');
         nlapiLogExecution('DEBUG', ' suiteletFunction',' Project  -->' + i_project);
	   
	     if(i_project!=null && i_project!=''&&i_project!=undefined)
		 {			
		 	 var o_projectOBJ = nlapiLoadRecord('job',i_project);
			 
			  if(o_projectOBJ!=null && o_projectOBJ!=''&&o_projectOBJ!=undefined)
			  {
			  	i_status = o_projectOBJ.getFieldValue('entitystatus')
				nlapiLogExecution('DEBUG', ' suiteletFunction',' Status  -->' + i_status);
				
			  }//Project OBJ			
		 }//Project		
	 }//GET		
	}
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);		
	}
	
	 response.write(i_status);
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
