/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		if (request.getMethod() == 'GET') //
		{
			var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS-Costa-Rica');
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :');
			var sublist1 = form.addSubList('record', 'list',
					'Costa-Rica-Report');

			sublist1.addField('custevent_employee', 'text', 'Employee');
			sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');

			sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
			sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
			sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');
			sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
			sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
			sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
			sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');

			sublist1.addField('custevent_wkst3', 'text', 'WK3 ST');
			sublist1.addField('custevent_wkot3', 'text', 'WK3 OT');
			sublist1.addField('custevent_wkdt3', 'text', 'WK3 DT');
			sublist1.addField('custevent_wk1timeoff3', 'text', 'WK3 TIMEOFF');

			form.addSubmitButton('Submit');
			response.writePage(form);

		} else {
			var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS-Costa-Rica');
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :');
			var sublist1 = form.addSubList('record', 'list',
					'Costa-Rica-Report');

			sublist1.addField('custevent_employee', 'text', 'Employee');
			sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');

			sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
			sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
			sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');
			sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
			sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
			sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
			sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');

			sublist1.addField('custevent_wkst3', 'text', 'WK3 ST');
			sublist1.addField('custevent_wkot3', 'text', 'WK3 OT');
			sublist1.addField('custevent_wkdt3', 'text', 'WK3 DT');
			sublist1.addField('custevent_wk1timeoff3', 'text', 'WK3 TIMEOFF');

			var toDate = request.getParameter('custpage_startdate');
			var s_afterdate = request.getParameter('custpage_fromdate');
			nlapiLogExecution('debug', 'date value', toDate);
			var fromDate = nlapiAddDays(nlapiStringToDate(toDate), -14);
			var toDate = nlapiStringToDate(toDate);
			var array = _SearchData(fromDate, toDate);
			sublist1.setLineItemValues(array);
			response.writePage(form);
		}
	} catch (e) {
		nlapiLogExecution('DEBUG', 'Search results exception ', +e)
	}
}

function _SearchData(fromDate, toDate) {
	try {
		var tem_te_id = new Array();
		var emp = new Array();
		var a_filters = new Array();
		fromDate = nlapiDateToString(fromDate);
		toDate = nlapiDateToString(toDate);
		a_filters[0] = new nlobjSearchFilter('date', null, 'within', [
				fromDate, toDate ]);

		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('employee');
		a_columns[1] = new nlobjSearchColumn('item');
		a_columns[2] = new nlobjSearchColumn('date');
		a_columns[3] = new nlobjSearchColumn('hours');
		a_columns[3] = new nlobjSearchColumn('internalid');
		a_columns[3] = new nlobjSearchColumn('timesheet');

		var searchresult_T = searchRecord('timeentry',
				'customsearch_salnonstdrptte_4', a_filters, a_columns);

		var unique_array = removearrayduplicate(emp);
		var st = JSON.stringify(searchresult_T);
		searchresult_T = JSON.parse(st);
		var arywk = [];
		var st_fp = 0;
		var ot = 0;
		var dt = 0;
		var timeoff = 0;
		var st_fp2 = 0;
		var ot2 = 0;
		var timeoff2 = 0;
		var st_fp3 = 0;
		var ot3 = 0;
		var timeoff3 = 0;
		var p = 0;
		q = 0;
		var m = 0;
		var itemArray = new Array();
		var emp_array = new Array();
		
		var stratdate = fromDate;
		for ( var i in searchresult_T) {

			var JSON_Temp = {};
			var curr_col = searchresult_T[i].columns;
			if (i != 0) {
				var temp_col = searchresult_T[i - 1].columns;
				if (timesheet != curr_col.timesheet) {
					arywk[p] = st_fp;
					arywk[p + 1] = ot;
					arywk[p + 2] = timeoff;
					st_fp = 0;
					ot = 0;
					timeoff = 0;
					p = p + 3;
					st_fp2 =0
					ot2=0;
					timeoff2 = 0;
					st_fp3 = 0;
					ot3= 0;
					timeoff3 = 0;
				}
			}
			if (i != 0) {
				if (temp_col.employee.name != curr_col.employee.name) {
					stratdate = fromDate;
					itemArray[m] = {
						'custevent_employee' : temp_col.employee.name,
						'custevent_wkst1' : arywk[0],
						'custevent_wkot1' : arywk[1],
						'custevent_wkdt1' : '0',
						'custevent_wk1timeoff1' : arywk[2],
						'custevent_wkst2' : arywk[3],
						'custevent_wkot2' : arywk[4],
						'custevent_wkdt2' : '0',
						'custevent_wk1timeoff2' : arywk[5],
						'custevent_wkst3' : arywk[6],
						'custevent_wkot3' : arywk[7],
						'custevent_wkdt3' : '0',
						'custevent_wk1timeoff3' : arywk[8]
					};
					m = m + 1;
					p = 0;
					arywk = [];
				}
			}

			var employee = curr_col.employee;
			var hours = parseInt(curr_col.hours);
			var date = curr_col.date;
			var item = curr_col.item;
			var timesheet = curr_col.timesheet;
			var diffDays = nlapiStringToDate(date).getDate() - nlapiStringToDate(fromDate).getDate(); 
			if(stratdate ==  date){
			
			
			if (item == 'FP' || item == 'ST') {
				st_fp = st_fp + hours;

			}
			if (item == 'OT') {
				ot = ot + hours;

			}
			if (item == 'Leave') {
				timeoff = timeoff + hours;

			}
			
			
			}
			
			else if(diffDays <=10 ){
				if (item == 'FP' || item == 'ST') {
					st_fp2 = st_fp2 + hours;

				}
				if (item == 'OT') {
					ot2 = ot2 + hours;

				}
				if (item == 'Leave') {
					timeoff2 = timeoff2 + hours;

				}
				arywk[3] = st_fp2;
				arywk[4] = ot2;
				arywk[5] = timeoff2;
				p=0;
			}
			else if(diffDays >10 ){
				st_fp2 =0
				ot2=0;
				ot2=0;
				if (item == 'FP' || item == 'ST') {
					st_fp3 = st_fp3 + hours;

				}
				if (item == 'OT') {
					ot3 = ot3 + hours;

				}
				if (item == 'Leave') {
					timeoff3 = timeoff3 + hours;

				}
				arywk[6] = st_fp3;
				arywk[7] = ot3;
				arywk[8] = timeoff3;
				p=0;
			}
			if(nlapiStringToDate(stratdate).getDay() == 5){
				stratdate = nlapiAddDays(nlapiStringToDate(stratdate), 3);
				stratdate =nlapiDateToString(stratdate, 'mm/dd/yyyy');
			}else{
				stratdate = nlapiAddDays(nlapiStringToDate(stratdate), 1);
				stratdate =nlapiDateToString(stratdate, 'mm/dd/yyyy');
			}
			
			

		}

		return itemArray;

	}

	catch (e) {
		nlapiLogExecution('DEBUG', 'Search results exception ', e);
		throw e;
	}

	function removearrayduplicate(cp) {
		var newArray = new Array();
		label: for (var i = 0; i < cp.length; i++) {
			for (var j = 0; j < cp.length; j++) {
				if (newArray[j] == cp[i])
					continue label;
			}
			newArray[newArray.length] = cp[i];
		}
		return newArray;
	}

}
