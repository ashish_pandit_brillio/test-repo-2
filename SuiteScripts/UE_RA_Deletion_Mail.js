/**
 * Send an email whenever an allocation is deleted
 * 
 * Version  Date             Author      Remarks 
 * 1.00     13 May 2016 Nitish Mishra
 * 1.01     17 Feb 2020 Praveena Madem	Added triggering point of deletion email
 */

//Commented by praveena
/*
function userEventAfterSubmit(type) {

	try {

		if (type == 'delete') {
			var mailTemplate = allocationDeletionMailTemplate();
			nlapiSendEmail(
			        442,
			        "business.ops@brillio.com; information.systems@brillio.com",
			        mailTemplate.Subject, mailTemplate.Body, null, 9673, null,
			        null);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}
*/
//Added by Praveena 
function userEventBeforeSubmit(type) {

	try {

		if (type == 'delete') {
			var mailTemplate = allocationDeletionMailTemplate();
			nlapiSendEmail(
			        442,
			        "business.ops@brillio.com; information.systems@brillio.com",
			        mailTemplate.Subject, mailTemplate.Body, null, 9673, null,
			        null);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}
function allocationDeletionMailTemplate() {
	var htmltext = '<p>Hi,</p>';

	htmltext += "<p>This is to inform you that an allocation has been deleted. </p>";

	htmltext += "<b>Employee : </b>" + nlapiGetFieldText('allocationresource')
	        + "<br/>";
	htmltext += "<b>Project : </b>" + nlapiGetFieldText('project') + "<br/>";
	htmltext += "<b>Start Date : </b>" + nlapiGetFieldValue('startdate')
	        + "<br/>";
	htmltext += "<b>End Date : </b>" + nlapiGetFieldValue('enddate') + "<br/>";
	htmltext += "<b>Billable : </b>"
	        + (nlapiGetFieldValue('custeventrbillable') == 'T' ? 'Yes' : 'No')
	        + "<br/>";
	htmltext += "<b>Site : </b>" + nlapiGetFieldText('custevent4') + "<br/>";
	htmltext += "<b>Shadow : </b>"
	        + (nlapiGetFieldValue('custevent_ra_is_shadow') == 'T' ? 'Yes'
	                : 'No') + "<br/>";
	htmltext += "<b>Deleted By : </b>" + nlapiGetContext().getName() + "<br/>";
	htmltext += "<b>Deleted On : </b>"
	        + nlapiDateToString(new Date(), 'datetimetz') + "<br/>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	return {
	    Subject : 'An Allocation Has Been Deleted - '
	            + nlapiGetFieldText('allocationresource'),
	    Body : addMailTemplate(htmltext)
	};
}