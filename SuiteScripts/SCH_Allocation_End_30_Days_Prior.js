/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 May 2017     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
try{
	var context = nlapiGetContext();
	var mail_counter = 0;
	var current_date = nlapiStringToDate(nlapiDateToString(new Date()));
	nlapiLogExecution('DEBUG', 'Start current_date:-- ', current_date);
	var employee_allocated_list = new Array();
	
	var currentDate = sysDate(); // returns the date
    var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
    var currentDateAndTime = currentDate + ' ' + currentTime;
   // nlapiSetFieldValue('custentity_date_time', currentDateAndTime);
    nlapiLogExecution('DEBUG', 'User Event Script', currentDateAndTime);
	
	var proj_Status = [];
	proj_Status.push('2');
	proj_Status.push('4');
	var filters_PM = [ [ ['status', 'anyof', 2 ], 'or',
							[ 'status', 'anyof', 4 ]], 'and',
								[ 'type', 'anyof', 2 ]
								
							  ];
	
	var columns_PM = new Array();
	columns_PM[0] = new nlobjSearchColumn('internalid');
	columns_PM[1] = new nlobjSearchColumn('entityid');
	//columns_PM[2] = new nlobjSearchColumn('email', 'custentity_projectmanager');
	//columns_PM[2] = new nlobjSearchColumn('email', 'custentity_deliverymanager');
	columns_PM[2] = new nlobjSearchColumn('custentity_projectmanager');
	columns_PM[3] = new nlobjSearchColumn('custentity_deliverymanager');
	//columns_PM[5] = new nlobjSearchColumn('email','custrecord_onsite_practice_head');
	//columns_PM[6] = new nlobjSearchColumn('email', 'custentity_deliverymanager');
	
	//var project_PM_Res = nlapiSearchRecord('job', null, filters_PM, columns_PM);
	
	var searchResult = nlapiCreateSearch('job',filters_PM,columns_PM);	
	var resultset_ = searchResult.runSearch();
		var project_PM_Res = [];
		var searchid_ = 0;
		
		var currentContext_C = nlapiGetContext();
		var remaining_usage = currentContext_C.getRemainingUsage();
		//nlapiLogExecution('DEBUG','OFC Cost remaining_usage',remaining_usage);
		do {
			var resultslice_ = resultset_.getResults(searchid_, searchid_ + 1000);
			for ( var re in resultslice_) {
				if(currentContext_C.getRemainingUsage() <= 1000){
				nlapiYieldScript();
				}
				project_PM_Res.push(resultslice_[re]);
				searchid_++;
			}
		} while (resultslice_.length >= 1000);
	//End
	
	if (_logValidation(project_PM_Res))
	{
		for (var i_indx = 0; i_indx < project_PM_Res.length; i_indx++)
		{
			var usageEnd = context.getRemainingUsage();
			if (usageEnd < 1000) 
			{
				yieldScript(context);
			}
			
			
			var i_project_pm = project_PM_Res[i_indx].getValue('custentity_projectmanager');
			
			if(_logValidation(i_project_pm)){
			/*var filters_allocation = [ 
										
										[ 'job.custentity_projectmanager', 'anyof', i_project_pm ]
									  ];*/
						var filters_allocation = [];
						filters_allocation[0] = new nlobjSearchFilter('custentity_projectmanager','job','anyof', i_project_pm);
						
						var columns = new Array();
						columns[0] 	= new nlobjSearchColumn('internalid');
						columns[1] = new nlobjSearchColumn('custeventrbillable');
						columns[2] = new nlobjSearchColumn('jobbillingtype', 'job');
						columns[3] = new nlobjSearchColumn('custentity_clientpartner', 'job');
						columns[4] = new nlobjSearchColumn('custentity_projectmanager', 'job');
						columns[5] = new nlobjSearchColumn('custentity_deliverymanager', 'job');
						columns[6] = new nlobjSearchColumn('custentity_verticalhead', 'job');
						columns[7] = new nlobjSearchColumn('custevent_practice');
						columns[8] = new nlobjSearchColumn('customer', 'job');
						columns[9] = new nlobjSearchColumn('company');
						columns[10] = new nlobjSearchColumn('custentity_project_allocation_category', 'job');
						columns[11] = new nlobjSearchColumn('subsidiary', 'employee');
						columns[12] = new nlobjSearchColumn('custentity_clientpartner', 'customer');
						columns[13] = new nlobjSearchColumn('custentity_region', 'customer');
						columns[14] = new nlobjSearchColumn('custentity_region', 'job');
						columns[15] = new nlobjSearchColumn('percentoftime');
						columns[16] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
						columns[17] = new nlobjSearchColumn('custentity_practice', 'job');
						columns[18] = new nlobjSearchColumn('department', 'employee');
						columns[19] = new nlobjSearchColumn('resource');
						columns[20] = new nlobjSearchColumn('startdate');
						columns[21] = new nlobjSearchColumn('enddate');
						columns[22] = new nlobjSearchColumn('employeestatus','employee');
						columns[23] = new nlobjSearchColumn('title','employee');
						columns[24] = new nlobjSearchColumn('custeventbstartdate');
						columns[25] = new nlobjSearchColumn('custeventbenddate');
						columns[26] = new nlobjSearchColumn('custentity_reportingmanager','employee');
						columns[27] = new nlobjSearchColumn('employeetype','employee');
						columns[28] = new nlobjSearchColumn('hiredate','employee');
						columns[29] = new nlobjSearchColumn('approver','employee');
						columns[30] = new nlobjSearchColumn('timeapprover','employee');
						columns[31] = new nlobjSearchColumn('custevent3');
						columns[32] = new nlobjSearchColumn('custevent_otrate');
						columns[33] = new nlobjSearchColumn('custevent_monthly_rate');
						columns[34] = new nlobjSearchColumn('startdate', 'job');
						columns[35] = new nlobjSearchColumn('enddate', 'job');
						columns[36] = new nlobjSearchColumn('custentity_reportingmanager','employee');
						columns[37] = new nlobjSearchColumn('email','employee');
						columns[38] = new nlobjSearchColumn('internalid','employee');
						
						var project_allocation_result = nlapiSearchRecord('resourceallocation', 'customsearch_30_days_prior_notification', filters_allocation, columns);
						if (_logValidation(project_allocation_result)){
							for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++)
							{
								var usageEnd = context.getRemainingUsage();
								if (usageEnd < 1000) 
								{
									yieldScript(context);
								}
								if(employee_allocated_list.indexOf(project_allocation_result[i_search_indx].getId())>=0)
								{
									
								}
								else
								{
								var is_resource_billable = project_allocation_result[i_search_indx].getText('custeventrbillable');
								var billing_type = project_allocation_result[i_search_indx].getText('jobbillingtype', 'job');
								var client_partner = project_allocation_result[i_search_indx].getText('custentity_clientpartner', 'job');
								var client_partner_id = project_allocation_result[i_search_indx].getValue('custentity_clientpartner', 'job');
								var proj_manager = project_allocation_result[i_search_indx].getText('custentity_projectmanager', 'job');
								var proj_manager_id = project_allocation_result[i_search_indx].getValue('custentity_projectmanager', 'job');
								var delivery_manager = project_allocation_result[i_search_indx].getText('custentity_deliverymanager', 'job');
								var delivery_manager_id = project_allocation_result[i_search_indx].getValue('custentity_deliverymanager', 'job');
								var vertical_head = project_allocation_result[i_search_indx].getText('custentity_verticalhead', 'job');
								var parctice_id = project_allocation_result[i_search_indx].getValue('custevent_practice');
								var parctice_name = project_allocation_result[i_search_indx].getText('custevent_practice');
								var parctice_id_parent = project_allocation_result[i_search_indx].getValue('custrecord_parent_practice', 'custevent_practice');
								var parctice_parent_name = project_allocation_result[i_search_indx].getText('custrecord_parent_practice', 'custevent_practice');
								var project_cust = project_allocation_result[i_search_indx].getValue('customer', 'job');
								var proj_cust_name = project_allocation_result[i_search_indx].getText('customer', 'job');
								var project_id = project_allocation_result[i_search_indx].getValue('company');
								var project_name = project_allocation_result[i_search_indx].getText('company');
								var is_proj_bench_category = project_allocation_result[i_search_indx].getValue('custentity_project_allocation_category', 'job');
								var emp_susidiary = project_allocation_result[i_search_indx].getText('subsidiary', 'employee');
								var emp_level_practice = project_allocation_result[i_search_indx].getText('department', 'employee');
								var customer_CP_id = project_allocation_result[i_search_indx].getValue('custentity_clientpartner', 'customer');
								var customer_CP_name = project_allocation_result[i_search_indx].getText('custentity_clientpartner', 'customer');
								var customer_region = project_allocation_result[i_search_indx].getText('custentity_region', 'customer');
								var project_region = project_allocation_result[i_search_indx].getText('custentity_region', 'job');
								var execusting_practice = project_allocation_result[i_search_indx].getText('custentity_practice', 'job');
								var resource_id = project_allocation_result[i_search_indx].getValue('resource');
								var resource_name = project_allocation_result[i_search_indx].getText('resource');
								var emp_reporting_manager = project_allocation_result[i_search_indx].getValue('custentity_reportingmanager','employee');
								var emp_email = project_allocation_result[i_search_indx].getValue('email','employee');
								var emp_user_id = project_allocation_result[i_search_indx].getValue('internalid','employee');
								var allocation_rec = project_allocation_result[i_search_indx].getValue('internalid');
								var end_date = project_allocation_result[i_search_indx].getValue('enddate');
								var start_date = project_allocation_result[i_search_indx].getValue('startdate');
								
								//Get Reporting Manager Email
								var RM_Lookup = nlapiLookupField('employee',emp_reporting_manager,['email']);
								var RM_Email = RM_Lookup.email;
								//Get Project Manager Email
								var PM_Lookup = nlapiLookupField('employee',proj_manager_id,['email']);
								var PM_Email = PM_Lookup.email;
								//Get Delievery Manager Email
								var DM_Lookup = nlapiLookupField('employee',delivery_manager_id,['email']);
								var DM_Email = DM_Lookup.email;
								//Get Delievery Manager Email
								var CP_Lookup = nlapiLookupField('employee',client_partner_id,['email']);
								var CP_Email = CP_Lookup.email;
								
								var emp_lookUP = nlapiLookupField('employee',parseInt(resource_id),['firstname','email']);
								var emp_name = emp_lookUP.firstname;
								
								
								var filtr = [];
								filtr[0]= new nlobjSearchFilter('resource',null,'anyof',resource_id);
								filtr[1]= new nlobjSearchFilter('internalid',null,'noneof',allocation_rec);
								
								var cols_R = [];
								/*cols_R[0] = new nlobjSearchColumn('resource');
								cols_R[1] = new nlobjSearchColumn('enddate');
								cols_R[2] = new nlobjSearchColumn('startdate');*/
								cols_R[0] = new nlobjSearchColumn('internalid');
								
								var searchObj = nlapiSearchRecord('resourceallocation','customsearch_check_if_any_allocation_fou', filtr ,cols_R);
								if(_logValidation(searchObj) && searchObj.length > 0){
									
								}
								else{
								
								//Create Excel Header
								/*var strVar_excel = '';
								
								strVar_excel += '<table>';

								strVar_excel += '	<tr>';
								strVar_excel += ' <td width="100%">';
								strVar_excel += '<table width="100%" border="1">';
								
								strVar_excel += '	<tr>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Region</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Customer Name</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Project Name</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Type</td>';
								strVar_excel += ' <td width="10%" font-size="11" align="left">Employee Name</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Practice</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Employeee Subsidiary</td>';
								//strVar_excel += ' <td width="6%" font-size="11" align="center">Percent Allocated</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Billable Status</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation Start Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Allocation End Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Project Start Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Project End Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Level</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Designation</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Onsite/Offshore</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Billing Start Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Billing End Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Reporting Manager</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Expense Approver</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Time Approver</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Employee Type</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Hire Date</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Bill Rate</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">OT Rate</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Monthly Rate</td>';
								
								strVar_excel += ' <td width="6%" font-size="11" align="center">Client Partner</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Project Manager</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">Delivery Manager</td>';			
								strVar_excel += ' <td width="6%" font-size="11" align="center">Project Category</td>';
								strVar_excel += '	</tr>';
								
								strVar_excel += '	<tr>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +project_region+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +customer_region+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +project_name+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +execusting_practice+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_type+ '</td>';
								strVar_excel += ' <td width="10%" font-size="11" align="left">' +resource_name+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_level_practice+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_susidiary+ '</td>';
								//strVar_excel += ' <td width="6%" font-size="11" align="center">' +final_prcnt_allocation_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +is_resource_billable+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +start_date+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +end_date+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +d_proj_strt_date+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +d_proj_end_date+ '</td>';								
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_level_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_designation_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +onsite_offsite_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_strt_date_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +billing_end_date_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +reporting_manager_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +expense_approver_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +time_approver_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_type_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +emp_hire_date_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +bill_rate_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +ot_rate_dump+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +monthly_rate_dump+ '</td>';
								
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +client_partner+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +proj_manager+ '</td>';
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +delivery_manager+ '</td>';			
								strVar_excel += ' <td width="6%" font-size="11" align="center">' +is_proj_bench_category+ '</td>';
								strVar_excel += '	</tr>';
						
								strVar_excel += '</table>';
								strVar_excel += ' </td>';	
								strVar_excel += '	</tr>';
								
								strVar_excel += '</table>';
								
								var excel_file_obj = generate_excel(strVar_excel,i_indx);*/
								
								//Email Body
								var strVar = '';
								strVar += '<html>';
								strVar += '<body>';
								
								strVar += '<p>Dear '+ emp_name +',</p>';
								//strVar += '<br/>';
								strVar += '<p>This email is being generated because your current project and assignment is scheduled to wind down / end within next 4 weeks from today. ';
								strVar += 'Your Reporting Manager, Client Partner and Resource Fulfillment teams will also receive this notification which will trigger exploring chances of extension of your current assignment and / or the search for other suitable opportunities for you within Brillio in the US, regardless of geographic restrictions.';
								//strVar += ' You are hereby requested to contact your reporting manager to learn about possible extension of your current allocation / assignment and other opportunities.</p>';
								
								strVar += '<p>While Brillio will make all efforts to extend your current assignment or find you other suitable opportunities, should none be available at the end of this 4-week period,';
								strVar += ' you may be required to consider relocating to other locations in the US or our offices in India. </p>';
								
								strVar += '<p>Please reach out to Reporting manager or Client partner to learn about possible extension of your current assignment and other opportunities within Brillio and plan accordingly.';
								strVar += ' You will receive an update from your reporting manager on this at least 2 weeks prior to your current assignment end date.</p>';
								//strVar += '<a href="https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&proj_id='+nlapiGetRecordId()+'>Project Setup</a>';
								
								//strVar += '<p>If you have received an email with the subject <b>“Allocation Ending Notification”</b> please ignore the message.<br/>';
								//strVar += 'Please do not hesitate to reach out to your account / reporting managers should you have any questions or concerns.</p>';
								//strVar += ' You are hereby requested to contact your reporting manager to learn about possible extension of your current allocation / assignment and other opportunities.</p>';
								
								strVar += '<p>Thanks & Regards,</p>';
								strVar += '<p>Resource Fulfilment Team</p>';
									
								strVar += '</body>';
								strVar += '</html>';
								
								//End of Email Body
								
								var receipt = [];
								receipt[0] = emp_email;
								var cc = Array();
								cc[0] = PM_Email;
								cc[1] = DM_Email;
								cc[2] = CP_Email;
								cc[3] = RM_Email;
								cc[4] = 'bhumika.sharma@brillio.com';
								cc[5] = 'swetha.b@brillio.com'; 
								cc[6] =	'Sajee.Sivaraman@brillio.com';
								
								//removing duplicates innarray
								cc = removearrayduplicate(cc);
								
								var bcc = [];
								bcc[0] = 'deepak.srinivas@brillio.com'
								//nlapiLogExecution('audit', 'proj detail len:-- ', proj_data.length);
								//Sending Email Functionality
								var today = new Date();
								var date_yesterday = nlapiAddDays(today, -1);
								var dd = today.getDate();
								var mm = today.getMonth()+1;
								var yyyy = today.getFullYear();
								var hours = today.getHours();
								var minutes = today.getMinutes();
								
								if(dd<10){
								    dd='0'+dd
								} 
								if(mm<10){
								    mm='0'+mm
								} 
								var today = dd+'/'+mm+'/'+yyyy;
								
								var a_emp_attachment = new Array();
								a_emp_attachment['entity'] = emp_user_id;
								//nlapiSendEmail(442, bcc, 'Notification: Allocation Ending In 30-Days dt '+today, strVar,bcc,bcc,a_emp_attachment);
								nlapiSendEmail(442, receipt, 'Allocation Ending Notification', strVar,cc,bcc,a_emp_attachment);
								nlapiLogExecution('debug', 'Email Sent Resource:-- ',emp_email);
								mail_counter ++;
								//TimeStamp to update the allocation Record
								 	
							
								    var rec_update = nlapiLoadRecord('resourceallocation',parseInt(allocation_rec));
								    rec_update.setFieldValue('custevent_30_days_prior_notification',currentDateAndTime);
								    var id = nlapiSubmitRecord(rec_update);
								    employee_allocated_list.push(allocation_rec);
								  //  nlapiLogExecution('DEBUG', 'Allocation Rec id', id);
								    yieldScript(context);
									}
								}
							}
						}
				}
		}
	}
	if(mail_counter <= 0){
		
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 41571;
		
		var strVar_ = '';
		
		strVar_ += '<html>';
		strVar_ += '<body>';
		
		strVar_ += '<p>No Data Found on date '+ currentDateAndTime +'.</p>';
		
		strVar_ += '<p>Thanks & Regards,</p>';
		strVar_ += '<p>Team IS</p>';
			
		strVar_ += '</body>';
		strVar_ += '</html>';
		nlapiSendEmail(442, 'deepak.srinivas@brillio.com', 'Notification Allocation ending in next 30-Days from today', strVar_,null,null,a_emp_attachment);
	}
}
catch (err)
{
	nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function yieldScript(currentContext) {

	nlapiLogExecution('AUDIT', 'API Limit Exceeded');
	var state = nlapiYieldScript();

	if (state.status == "FAILURE") {
		nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
		        + state.reason + ' / Size : ' + state.size);
		return false;
	} else if (state.status == "RESUME") {
		nlapiLogExecution('AUDIT', 'Script Resumed');
	}
}

function generate_excel(strVar_excel,counter)
{
	var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		var fileName = 'Resource Allocation Data  '+ counter +' - '
	        + nlapiDateToString(new Date(), 'date') + '.xls';
		var strVar2 = strVar_excel;
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile(fileName, 'XMLDOC', strVar1);
		
		//file.setFolder('163597'); 
		var id2=nlapiSubmitFile(file);
		nlapiLogExecution('DEBUG','File Created ID  :--',id2);
		return file;
}

function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
}

function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
	    meridian += "pm";
	} else {
	    meridian += "am";
	}
	if (hours > 12) {

	    hours = hours - 12;
	}
	if (minutes < 10) {
	    minutes = "0" + minutes;
	}
	if (seconds < 10) {
	    seconds = "0" + seconds;
	}
	str += hours + ":" + minutes + ":" + seconds + " ";

	return str + meridian;
	}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}
