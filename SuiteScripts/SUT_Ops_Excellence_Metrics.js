/**
 * Operation Excellence Metrics
 * 
 * Version Date Author Remarks 1.00
 * 
 * 24 Jun 2016 Nitish Mishra
 * 
 */

function scheduled(type) {
	try {
		var inrToUsd = 0.015152;
		var gbpToUsd = 1.44939;
		var currentDate = '';

		var d_currentDate = null;

		if (currentDate) {
			d_currentDate = nlapiStringToDate(currentDate);
		} else {
			d_currentDate = new Date();
			currentDate = nlapiDateToString(d_currentDate, 'date');
		}

		var requestedStartDate = '1/1/2016';// nlapiDateToString(
		// getMonthStartDate(d_currentDate), 'date');
		var requestedEndDate = '7/11/2016';// nlapiDateToString(
		// getMonthEndDate(nlapiStringToDate(requestedStartDate)), 'date');

		var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL",
		        "AUG", "SEP", "OCT", "NOV", "DEC" ];
		var monthName = monthNames[d_currentDate.getMonth()];

		// Year field
		var yearName = (d_currentDate.getFullYear()).toFixed(0)

		// User Section
		var userId = nlapiGetUser();
		var practiceList = getPracticeList(userId);
		nlapiLogExecution('debug', 'practiceList', practiceList.length);

		// creating a list of projects for filters
		var projectList = [];

		nlapiLogExecution('debug', 'start date', requestedStartDate);
		nlapiLogExecution('debug', 'end date', requestedEndDate);
		var totalDays = getDayDiff(requestedStartDate, requestedEndDate);
		nlapiLogExecution('debug', 'totalDays', totalDays);

		var mainObject = {};

		// get a list of all projects and create a hash table for name to
		// internal id
		var jobSearch = searchRecord('job', null, [
		        [ 'isinactive', 'is', 'F' ],
		        'AND',
		        [ [ 'enddate', 'notbefore', 'startofthisyear' ], 'OR',
		                [ 'enddate', 'isempty' ] ] ], [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('jobtype'),
		        new nlobjSearchColumn('altname'),
		        new nlobjSearchColumn('customer') ]);

		var jobList = {};
		jobSearch
		        .forEach(function(job) {

			        if (!mainObject[job.getValue('customer')]) {
				        mainObject[job.getValue('customer')] = {
				            Name : job.getText('customer'),
				            ProjectList : {}
				        }
			        }

			        if (!mainObject[job.getValue('customer')].ProjectList[job
			                .getId()]) {
				        mainObject[job.getValue('customer')].ProjectList[job
				                .getId()] = {
				            Name : job.getValue('entityid') + " "
				                    + job.getValue('altname'),
				            Type : job.getText('jobtype'),
				            PracticeList : {}
				        };
			        }
		        });

		// get allocation data
		getAllocationData(requestedStartDate, requestedEndDate, projectList,
		        mainObject, totalDays, practiceList, userId);

		nlapiLogExecution('debug', 'projectList', projectList.length);

		// add the salary and expense cost
		siteWiseProjectCost(requestedStartDate, requestedEndDate, mainObject,
		        projectList, inrToUsd, gbpToUsd, practiceList, jobList);

		var csvText = generateTableCsv(mainObject);
		var fileName = 'Ops Cost Metrics ' + monthName + ' ' + yearName
		        + '.csv';
		var file = nlapiCreateFile(fileName, 'CSV', csvText);
		nlapiSendEmail('442', 'nitish.mishra@brillio.com', 'Ops Report',
		        'Ops Report', null, null, null, file);
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

function suitelet(request, response) {
	try {
		var inrToUsd = request.getParameter('custpage_inr');
		var gbpToUsd = request.getParameter('custpage_gbp');

		// create display form
		var form = nlapiCreateForm('Project Cost Metrics');
		form.addSubmitButton('Refresh');
		form.addFieldGroup('custpage_grp_0', 'Select Options');
		form.addFieldGroup('custpage_grp_1', 'Table');

		var inrField = form.addField('custpage_inr', 'float', 'INR to USD',
		        null, 'custpage_grp_0');
		inrField.setBreakType('startcol');
		if (inrToUsd) {
			inrField.setDefaultValue(inrToUsd);
		}

		var gbpField = form.addField('custpage_gbp', 'float', 'GBP To USD',
		        null, 'custpage_grp_0');
		gbpField.setBreakType('startcol');
		if (gbpToUsd) {
			gbpField.setDefaultValue(gbpToUsd);
		}

		// date field
		var dateField = form.addField('custpage_date', 'date', 'Date', null,
		        'custpage_grp_0');
		dateField.setBreakType('startcol');

		var currentDate = request.getParameter('custpage_date');
		var d_currentDate = null;

		if (currentDate) {
			d_currentDate = nlapiStringToDate(currentDate);
		} else {
			d_currentDate = new Date();
			currentDate = nlapiDateToString(d_currentDate, 'date');
		}

		dateField.setDefaultValue(currentDate);
		var requestedStartDate = nlapiDateToString(
		        getMonthStartDate(d_currentDate), 'date'), requestedEndDate = nlapiDateToString(
		        getMonthEndDate(nlapiStringToDate(requestedStartDate)), 'date');

		// Month Field
		var monthField = form.addField('custpage_month', 'text', 'Month', null,
		        'custpage_grp_0');
		monthField.setDisplayType('inline');
		monthField.setBreakType('startcol');
		var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL",
		        "AUG", "SEP", "OCT", "NOV", "DEC" ];
		var monthName = monthNames[d_currentDate.getMonth()];
		monthField.setDefaultValue(monthName);

		// Year field
		var yearField = form.addField('custpage_year', 'text', 'Year', null,
		        'custpage_grp_0');
		yearField.setBreakType('startcol');
		yearField.setDisplayType('inline');
		var yearName = (d_currentDate.getFullYear()).toFixed(0)
		yearField.setDefaultValue(yearName);

		// User Section
		var userId = nlapiGetUser();
		var practiceList = getPracticeList(userId);
		nlapiLogExecution('debug', 'practiceList', practiceList.length);

		var requestedProjectId = request.getParameter('custpage_project');

		// creating a list of projects for filters
		var projectList = [];

		var csvLink = "<a href='/app/site/hosting/scriptlet.nl?script=941&deploy=1&"
		        + "custpage_date="
		        + currentDate
		        + "&custpage_project="
		        + requestedProjectId
		        + "&custpage_gbp="
		        + gbpToUsd
		        + "&custpage_inr="
		        + inrToUsd
		        + "&mode=CSV' target='_blank'>"
		        + "<img style='height: 40px; margin-top: 20px;' src='https://system.na1.netsuite.com/core/media/media.nl?id=145877&c=3883006&h=db69121d869e7d0ba460'></a>";

		var csvLinkField = form.addField('custpage_download', 'inlinehtml', '',
		        null, 'custpage_grp_0');
		csvLinkField.setBreakType('startcol');
		csvLinkField.setDefaultValue(csvLink);

		nlapiLogExecution('debug', 'start date', requestedStartDate);
		nlapiLogExecution('debug', 'end date', requestedEndDate);
		var totalDays = getDayDiff(requestedStartDate, requestedEndDate);
		nlapiLogExecution('debug', 'totalDays', totalDays);

		var mainObject = {};

		// get allocation data
		getAllocationData(requestedStartDate, requestedEndDate, projectList,
		        mainObject, totalDays, practiceList, userId);

		nlapiLogExecution('debug', 'projectList', projectList.length);

		// add the salary and expense cost
		siteWiseProjectCostB(requestedStartDate, requestedEndDate, mainObject,
		        projectList, inrToUsd, gbpToUsd, practiceList);

		var mode = request.getParameter('mode');

		if (mode == 'CSV') {
			var csvText = generateTableCsv(mainObject);
			var fileName = 'Ops Cost Metrics ' + monthName + ' ' + yearName
			        + '.csv';
			var file = nlapiCreateFile(fileName, 'CSV', csvText);
			response.setContentType('CSV', fileName);
			response.write(file.getValue());
		} else {
			var html = generateTableStructure(mainObject);
			var tableField = form.addField('custpage_html', 'inlinehtml', null,
			        null, 'custpage_grp_1')
			tableField.setDefaultValue(html);
			tableField.setBreakType('startcol');
			response.writePage(form);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function getAllocationData(requestedStartDate, requestedEndDate, projectList,
        mainObject, totalDays, practiceList, userId)
{
	try {
		var filters = [
		        new nlobjSearchFilter('formuladate', null, 'notafter',
		                requestedEndDate).setFormula('{startdate}'),
		        new nlobjSearchFilter('formuladate', null, 'notbefore',
		                requestedStartDate).setFormula('{enddate}'),
		        new nlobjSearchFilter('department', 'employee', 'anyof',
		                practiceList) ];

		// if (userId == '35819' || userId == '9673') {
		// filters.push(new nlobjSearchFilter(
		// 'custentity_project_allocation_category', 'job', 'noneof',
		// [ 6 ]));
		// }

		var allocationSearch = searchRecord('resourceallocation', 1425, filters);

		nlapiLogExecution('debug', 'allocation data', allocationSearch.length);
		var totalPm = 0;

		if (allocationSearch) {

			allocationSearch
			        .forEach(function(allocation) {
				        var customer = allocation.getValue('customer');
				        var region = allocation
				                .getText('territory', 'customer');
				        var project = allocation.getValue('project');
				        projectList.push(project);
				        var entityId = allocation.getValue('entityid', 'job');
				        var practice = allocation.getValue('department',
				                'employee');
				        var personType = allocation.getValue(
				                'custentity_persontype', 'employee');
				        var isOnsite = allocation.getValue('custevent4') == '1';
				        var isBillable = allocation
				                .getValue('custeventrbillable') == 'T';
				        var percentageAllocation = allocation
				                .getValue('percentoftime');
				        var personMonth = parseFloat((getPersonUtilization(
				                allocation.getValue('startdate'), allocation
				                        .getValue('enddate'),
				                percentageAllocation, requestedStartDate,
				                requestedEndDate, totalDays, 8)).toFixed(2));
				        var jobType = allocation.getText('jobtype', 'job');
				        var projectType = allocation.getValue('jobbillingtype',
				                'job');
				        var subsidiary = allocation.getValue('subsidiary',
				                'job');

				        totalPm += parseFloat(personMonth);

				        // Employee Level
				        var actualLevel = allocation.getText('employeestatus',
				                'employee');

				        var updatedLevel = null;

				        if (actualLevel == 'CW' || actualLevel == 'PT'
				                || actualLevel == 'TS') {
					        updatedLevel = 'Others';
				        } else {
					        updatedLevel = parseInt(actualLevel);
				        }

				        // check if the customer exists
				        if (!mainObject[customer]) {
					        mainObject[customer] = {
					            Name : allocation.getText('customer'),
					            Region : region,
					            ProjectList : {}
					        };
				        }

				        // check if the project exists
				        if (!mainObject[customer].ProjectList[project]) {
					        mainObject[customer].ProjectList[project] = {
					            Name : allocation.getText('project'),
					            Type : jobType,
					            BillingType : allocation.getText(
					                    'jobbillingtype', 'job')
					                    + (allocation.getValue(
					                            'custentity_t_and_m_monthly',
					                            'job') == 'T' ? ' Monthly' : ''),
					            Category : allocation
					                    .getText(
					                            'custentity_project_allocation_category',
					                            'job'),
					            PracticeList : {}
					        }
				        }

				        // check if practice exists
				        if (!mainObject[customer].ProjectList[project].PracticeList[practice]) {
					        mainObject[customer].ProjectList[project].PracticeList[practice] = new OpsMetricsObject();
					        mainObject[customer].ProjectList[project].PracticeList[practice].Name = allocation
					                .getText('department', 'employee');
				        }

				        mainObject[customer].ProjectList[project].PracticeList[practice].Total += personMonth;

				        if (isOnsite) {
					        mainObject[customer].ProjectList[project].PracticeList[practice].TotalOnsite += personMonth;

					        if (isBillable) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].TotalBilled += personMonth;
						        mainObject[customer].ProjectList[project].PracticeList[practice].OnsiteBilled += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].TotalUnbilled += personMonth;
						        mainObject[customer].ProjectList[project].PracticeList[practice].OnsiteUnbilled += personMonth;
					        }
				        } else {
					        mainObject[customer].ProjectList[project].PracticeList[practice].TotalOffsite += personMonth;

					        if (isBillable) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].TotalBilled += personMonth;
						        mainObject[customer].ProjectList[project].PracticeList[practice].OffsiteBilled += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].TotalUnbilled += personMonth;
						        mainObject[customer].ProjectList[project].PracticeList[practice].OffsiteUnbilled += personMonth;
					        }
				        }

				        // Contract
				        if (personType == 1) {

					        if (isOnsite) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].ContractOnsite += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].ContractOffsite += personMonth;
					        }
				        } // Employee
				        else if (personType == 2) {
					        // salariedCount
					        // +=
					        // personMonth;
				        }

				        // level
				        // wise
				        // division
				        if (updatedLevel == 'Others') {

					        if (isBillable) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelBilled += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelUnbilled += personMonth;
					        }
					        mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelTotal += personMonth;
				        } else if (updatedLevel >= 6) {

					        if (isBillable) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowBilled += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowUnbilled += personMonth;
					        }
					        mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowTotal += personMonth;
				        } else if (updatedLevel <= 5) {

					        if (isBillable) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveBilled += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveUnbilled += personMonth;
					        }
					        mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveTotal += personMonth;
				        }

				        if (updatedLevel >= 8) {

					        if (isBillable) {
						        mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineBilled += personMonth;
					        } else {
						        mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineUnbilled += personMonth;
					        }
					        mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineTotal += personMonth;
				        }
			        });
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function generateTableStructure(mainObject) {
	var html = "";
	// table css
	html += "<style>";
	html += ".ops-metrics-table td {border:1px solid black;}";
	html += ".ops-metrics-table tr:nth-child(even) {background: #ffffe6}";
	html += ".ops-metrics-table tr:nth-child(odd) {background: #e6ffe6}";
	html += ".ops-metrics-table tr:nth-child(even):hover {background: #ffff99}";
	html += ".ops-metrics-table tr:nth-child(odd):hover {background: #80ff80}";
	html += ".ops-metrics-table .header td {background-color: #001a33 !important; color : white !important;}";
	html += "</style>";

	// table
	html += "<table class='ops-metrics-table'>";

	html += "<tr class='header'>";

	html += "<td>";
	html += "Region";
	html += "</td>";

	html += "<td>";
	html += "Customer";
	html += "</td>";

	html += "<td>";
	html += "Project";
	html += "</td>";

	html += "<td>";
	html += "Project Type";
	html += "</td>";

	html += "<td>";
	html += "Billing Type";
	html += "</td>";

	html += "<td>";
	html += "Category";
	html += "</td>";

	html += "<td>";
	html += "Practice";
	html += "</td>";

	html += "<td>";
	html += "Onsite Billed";
	html += "</td>";

	html += "<td>";
	html += "Onsite Unbilled";
	html += "</td>";

	html += "<td>";
	html += "Offsite Billed";
	html += "</td>";

	html += "<td>";
	html += "Offsite Unbilled";
	html += "</td>";

	html += "<td>";
	html += "Total";
	html += "</td>";

	html += "<td>";
	html += "Total Billed";
	html += "</td>";

	html += "<td>";
	html += "Total Unbilled";
	html += "</td>";

	html += "<td>";
	html += "Total Onsite";
	html += "</td>";

	html += "<td>";
	html += "Total Offsite";
	html += "</td>";

	html += "<td>";
	html += "Contract Onsite";
	html += "</td>";

	html += "<td>";
	html += "Contract Offsite";
	html += "</td>";

	html += "<td>";
	html += "5 or Above Billed";
	html += "</td>";

	html += "<td>";
	html += "5 or Above Unbilled";
	html += "</td>";

	html += "<td>";
	html += "6 or Below Billed";
	html += "</td>";

	html += "<td>";
	html += "6 or Below Unbilled";
	html += "</td>";

	html += "<td>";
	html += "8 or 9 Billed";
	html += "</td>";

	html += "<td>";
	html += "8 or 9 Unbilled";
	html += "</td>";

	html += "<td>";
	html += "Other Levels Billed";
	html += "</td>";

	html += "<td>";
	html += "Other Levels Unbilled";
	html += "</td>";

	html += "<td>";
	html += "Contractor Cost Onsite";
	html += "</td>";

	html += "<td>";
	html += "Contractor Cost Offsite";
	html += "</td>";

	html += "<td>";
	html += "Salaried Cost Onsite";
	html += "</td>";

	html += "<td>";
	html += "Salaried Cost Offsite";
	html += "</td>";

	html += "<td>";
	html += "Total Salary Cost Onsite";
	html += "</td>";

	html += "<td>";
	html += "Total Salary Cost Offsite";
	html += "</td>";

	html += "<td>";
	html += "Total Salary Cost";
	html += "</td>";

	html += "<td>";
	html += "Expenses";
	html += "</td>";
	
	html += "<td>";
	html += "Travel";
	html += "</td>";

	html += "<td>";
	html += "Total Project Cost";
	html += "</td>";

	html += "</tr>";

	// table header

	for ( var customer in mainObject) {

		for ( var project in mainObject[customer].ProjectList) {

			for ( var practice in mainObject[customer].ProjectList[project].PracticeList) {
				html += "<tr>";

				html += "<td>";
				html += mainObject[customer].Region;
				html += "</td>";

				html += "<td>";
				html += mainObject[customer].Name;
				html += "</td>";

				html += "<td>";
				html += mainObject[customer].ProjectList[project].Name;
				html += "</td>";

				html += "<td>";
				html += mainObject[customer].ProjectList[project].Type;
				html += "</td>";

				html += "<td>";
				html += mainObject[customer].ProjectList[project].BillingType;
				html += "</td>";

				html += "<td>";
				html += mainObject[customer].ProjectList[project].Category;
				html += "</td>";

				html += "<td>";
				html += mainObject[customer].ProjectList[project].PracticeList[practice].Name;
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OnsiteBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OnsiteUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OffsiteBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OffsiteUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].Total);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalOnsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalOffsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractOnsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractOffsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelBilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelUnbilled);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOnsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOffsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOnsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOffsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOnsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOffsite);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalSalaryCost);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].Expense);
				html += "</td>";

				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].Travel);
				html += "</td>";
				
				html += "<td>";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost);
				html += "</td>";

				html += "</tr>";
			}
		}
	}

	html += "</table>";

	return html;
}

function generateTableCsv(mainObject) {
	var html = "";
	html += "Region,";
	html += "Customer,";
	html += "Project,";
	html += "Project Type,";
	html += "Billing Type,";
	html += "Category,";
	html += "Practice,";
	html += "Onsite Billed,";
	html += "Onsite Unbilled,";
	html += "Offsite Billed,";
	html += "Offsite Unbilled,";
	html += "Total,";
	html += "Total Billed,";
	html += "Total Unbilled,";
	html += "Total Onsite,";
	html += "Total Offsite,";
	html += "Contract Onsite,";
	html += "Contract Offsite,";
	html += "5 or Above Billed,";
	html += "5 or Above Unbilled,";
	html += "6 or Below Billed,";
	html += "6 or Below Unbilled,";
	html += "8 or 9 Billed,";
	html += "8 or 9 Unbilled,";
	html += "Other Levels Billed,";
	html += "Other Levels Unbilled,";
	html += "Contractor Cost Onsite,";
	html += "Contractor Cost Offsite,";
	html += "Salaried Cost Onsite,";
	html += "Salaried Cost Offsite,";
	html += "Total Salary Cost Onsite,";
	html += "Total Salary Cost Offsite,";
	html += "Total Salary Cost,";
	html += "Expenses,";
	html += "Travel,";
	html += "Total Project Cost,";
	html += "\r\n";

	// table header

	for ( var customer in mainObject) {

		for ( var project in mainObject[customer].ProjectList) {

			for ( var practice in mainObject[customer].ProjectList[project].PracticeList) {
				html += removeComma(mainObject[customer].Region);
				html += ",";
				html += removeComma(mainObject[customer].Name);
				html += ",";
				html += removeComma(mainObject[customer].ProjectList[project].Name);
				html += ",";
				html += removeComma(mainObject[customer].ProjectList[project].Type);
				html += ",";
				html += removeComma(mainObject[customer].ProjectList[project].BillingType);
				html += ",";
				html += removeComma(mainObject[customer].ProjectList[project].Category);
				html += ",";
				html += removeComma(mainObject[customer].ProjectList[project].PracticeList[practice].Name);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OnsiteBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OnsiteUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OffsiteBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OffsiteUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].Total);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalOnsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalOffsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractOnsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractOffsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].FiveOrAboveUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SixOrBelowUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].EightOrNineUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelBilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].OtherLevelUnbilled);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOnsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOffsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOnsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOffsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOnsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOffsite);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalSalaryCost);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].Expense);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].Travel);
				html += ",";
				html += formatCurrency(mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost);
				html += "\r\n";
			}
		}
	}

	return html;
}

function siteWiseProjectCost(startDate, endDate, mainObject, projectList,
        inrToUsd, gbpToUsd, practiceList)
{
	try {
		// get a list of all projects and create a hash table for name to
		// internal id
		var jobSearch = searchRecord('job', null, [ new nlobjSearchFilter(
		        'isinactive', null, 'is', 'F') ], [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('altname'),
		        new nlobjSearchColumn('customer') ]);

		var jobList = {};
		jobSearch.forEach(function(job) {
			jobList[job.getValue('entityid')] = {
			    InternalId : job.getId(),
			    Customer : job.getValue('customer'),
			    CustomerName : job.getText('customer'),
			    Name : job.getValue('entityid') + " " + job.getValue('altname')
			}
		});

		// get all the salary and expense cost
		var transactionSearch = searchRecord('transaction', 1435,
		        [ new nlobjSearchFilter('formuladate', null, 'within',
		                startDate, endDate).setFormula('{trandate}') ]);

		nlapiLogExecution('debug',
		        'transaction search without practice filter',
		        transactionSearch.length);

		// get all the salary and expense cost
		var transactionSearch = searchRecord('transaction', 1435,
		        [
		                new nlobjSearchFilter('department', null, 'anyof',
		                        practiceList),
		                new nlobjSearchFilter('formuladate', null, 'within',
		                        startDate, endDate).setFormula('{trandate}') ]);

		nlapiLogExecution('debug', 'transaction search',
		        transactionSearch.length);

		var exchangeRateTable = {
		    INR : nlapiExchangeRate('INR', 'USD', endDate),
		    USD : 1,
		    GBP : nlapiExchangeRate('GBP', 'USD', endDate)
		};

		nlapiLogExecution('debug', 'exchange rate table', JSON
		        .stringify(exchangeRateTable));

		if (inrToUsd) {
			exchangeRateTable.INR = inrToUsd;
		}

		if (gbpToUsd) {
			exchangeRateTable.GBP = gbpToUsd;
		}

		nlapiLogExecution('debug', 'exchange rate table', JSON
		        .stringify(exchangeRateTable));

		var excludedCount = 0;

		// loop and divide in onsite / offsite cost
		for (var i = 0; i < transactionSearch.length; i++) {
			var projectText = transactionSearch[i].getValue('custcolprj_name');
			var category = transactionSearch[i].getValue('formulatext');
			var amount = parseFloat(transactionSearch[i].getValue('fxamount'));
			var currency = transactionSearch[i].getText('currency');
			var convertedAmount = exchangeRateTable[currency] * amount;
			var practice = transactionSearch[i].getValue('department');
			var subsidiary = transactionSearch[i].getValue('subsidiary');
			var type = transactionSearch[i].getText('type');
			
			nlapiLogExecution('debug','Project Name',projectText);
			nlapiLogExecution('debug','Project currency',currency);
			nlapiLogExecution('debug','Project Transaction Amount',amount);

			if (category == 'Exclude') {
				excludedCount += 1;
				continue;
			}

			if (projectText) {

				var currentProjectEntityId = projectText.trim().split(' ')[0];

				if (jobList[currentProjectEntityId]) {
					var project = jobList[currentProjectEntityId].InternalId;
					var customer = jobList[currentProjectEntityId].Customer;

					if (!customer || !project || !practice) {
						continue;
					}

					if (!mainObject[customer]) {
						// continue;
						mainObject[customer] = {
						    Name : jobList[currentProjectEntityId].CustomerName,
						    ProjectList : {}
						};
					}

					if (!mainObject[customer].ProjectList[project]) {
						// continue;
						mainObject[customer].ProjectList[project] = {
						    Name : jobList[currentProjectEntityId].Name,
						    PracticeList : {}
						};
					}

					if (!mainObject[customer].ProjectList[project].PracticeList[practice]) {
						// continue;
						mainObject[customer].ProjectList[project].PracticeList[practice] = new OpsMetricsObject();
						mainObject[customer].ProjectList[project].PracticeList[practice].Name = transactionSearch[i]
						        .getText('department');
					}

					if (category == 'Travel') {
						mainObject[customer].ProjectList[project].PracticeList[practice].Travel += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else if (category == 'Others') {
						mainObject[customer].ProjectList[project].PracticeList[practice].Expense += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else if (category == 'Salary Cost') {
						if (subsidiary == 2) {
							mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOnsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOnsite += convertedAmount;
						} else {
							mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOffsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOffsite += convertedAmount;
						}

						mainObject[customer].ProjectList[project].PracticeList[practice].TotalSalaryCost += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else if (category == 'Contractor Cost') {

						if (subsidiary == 2) { // Onsite
							mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOnsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOnsite += convertedAmount;
						} else { // Offsite
							mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOffsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOffsite += convertedAmount;
						}

						mainObject[customer].ProjectList[project].PracticeList[practice].TotalSalaryCost += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else {
						nlapiLogExecution('debug', 'category mistmatch',
						        category);
					}
				} else {

				}
			}
		}

		nlapiLogExecution('debug', 'excluded count', excludedCount);
	} catch (err) {
		nlapiLogExecution('ERROR', 'siteWiseProjectCost', err);
		throw err;
	}
}

function getMonthEndDate(currentDate) {
	return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var s_startDate = month + "/1/" + year;
	var d_startDate = nlapiStringToDate(s_startDate);
	return d_startDate;
}

function getPracticeList(userId) {
	try {
		// new nlobjSearchFilter('isinactive', null, 'is', 'F')
		var filters = [];

		if (userId == '41571' || userId == '35819' || userId=='1582') {

		} else {
			filters.push(new nlobjSearchFilter('custrecord_practicehead', null,
			        'anyof', userId))
		}

		// get the practices
		var practices = [];
		var practiceSearch = nlapiSearchRecord('department', null, filters);

		if (practiceSearch) {

			practiceSearch.forEach(function(practice) {
				practices.push(practice.getId());
			});
		} else {
			throw "You are not authorized to access this report.";
		}

		return practices;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPracticeList', err);
		throw err;
	}
}

function OpsMetricsObject() {
	this.OffsiteBilled = 0;
	this.OffsiteUnbilled = 0;
	this.OnsiteBilled = 0;
	this.OnsiteUnbilled = 0;
	this.TotalOnsite = 0;
	this.TotalOffsite = 0;
	this.TotalBilled = 0;
	this.TotalUnbilled = 0;
	this.Total = 0;
	this.ContractOnsite = 0;
	this.ContractOffsite = 0;
	this.EightOrNineBilled = 0;
	this.EightOrNineUnbilled = 0;
	this.EightOrNineTotal = 0;
	this.FiveOrAboveBilled = 0;
	this.FiveOrAboveUnbilled = 0;
	this.FiveOrAboveTotal = 0;
	this.SixOrBelowBilled = 0;
	this.SixOrBelowUnbilled = 0;
	this.SixOrBelowTotal = 0;
	this.OtherLevelBilled = 0;
	this.OtherLevelUnbilled = 0;
	this.OtherLevelTotal = 0;
	this.SalariedCostOnsite = 0;
	this.SalariedCostOffsite = 0;
	this.ContractCostOnsite = 0;
	this.ContractCostOffsite = 0;
	this.TotalCostOnsite = 0;
	this.TotalCostOffsite = 0;
	this.TotalSalaryCost = 0;
	this.Expense = 0;
	this.Travel = 0;
	this.TotalCost = 0;
}

function formatCurrency(value) {
	return value;
}

function siteWiseProjectCostB(startDate, endDate, mainObject, projectList,
        inrToUsd, gbpToUsd, practiceList, jobList)
{
	try {
		// get all the customer details
		var customerSearch = searchRecord('customer', null, [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'), ], [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('altname'),
		        new nlobjSearchColumn('territory') ]);

		var customerList = {};
		customerSearch.forEach(function(customer) {
			customerList[customer.getValue('entityid')] = {
			    Region : customer.getText('territory'),
			    Name : customer.getValue('entityid') + " "
			            + customer.getValue('altname'),
			    Id : customer.getId()
			};
		});

		nlapiLogExecution('debug', 'customers list', customerSearch.length);
		nlapiLogExecution('debug', 'customers list', JSON
		        .stringify(customerList));

		// get a list of all projects and create a hash table for name to
		// internal id
		var jobSearch = searchRecord('job', null, [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		// new nlobjSearchFilter('enddate', null, 'notbefore',
		// '1/1/2015')
		],
		        [
		                new nlobjSearchColumn('entityid'),
		                new nlobjSearchColumn('jobtype'),
		                new nlobjSearchColumn('altname'),
		                new nlobjSearchColumn('customer'),
		                new nlobjSearchColumn('territory', 'customer'),
		                new nlobjSearchColumn('jobbillingtype'),
		                new nlobjSearchColumn('custentity_t_and_m_monthly'),
		                new nlobjSearchColumn(
		                        'custentity_project_allocation_category') ]);

		var jobList = {};
		jobSearch
		        .forEach(function(job) {
			        jobList[job.getValue('entityid')] = {
			            InternalId : job.getId(),
			            Type : job.getText('jobtype'),
			            Customer : job.getValue('customer'),
			            CustomerName : job.getText('customer'),
			            CustomerRegion : job.getText('territory', 'customer'),
			            Name : job.getValue('entityid') + " "
			                    + job.getValue('altname'),
			            BillingType : job.getText('jobbillingtype')
			                    + (job.getValue('custentity_t_and_m_monthly') == 'T' ? ' Monthly'
			                            : ''),
			            Category : job
			                    .getText('custentity_project_allocation_category')
			        }
		        });

		jobList['NA'] = {
		    InternalId : '',
		    Type : '',
		    Customer : '',
		    CustomerName : '',
		    CustomerRegion : '',
		    Name : '',
		    BillingType : '',
		    Category : ''
		}

		// get all the salary and expense cost
		var transactionSearch = searchRecord('transaction', 1435,
		        [
		                new nlobjSearchFilter('department', null, 'anyof',
		                        practiceList),
		                new nlobjSearchFilter('formuladate', null, 'within',
		                        startDate, endDate).setFormula('{trandate}') ]);

		nlapiLogExecution('debug', 'transaction search',
		        transactionSearch.length);

		var exchangeRateTable = {
		    INR : nlapiExchangeRate('INR', 'USD', endDate),
		    USD : 1,
		    GBP : nlapiExchangeRate('GBP', 'USD', endDate),
		    EUR : nlapiExchangeRate('EUR', 'USD', endDate)
		};

		nlapiLogExecution('debug', 'exchange rate table', JSON
		        .stringify(exchangeRateTable));

		if (inrToUsd) {
			exchangeRateTable.INR = inrToUsd;
		}

		if (gbpToUsd) {
			exchangeRateTable.GBP = gbpToUsd;
		}

		nlapiLogExecution('debug', 'exchange rate table', JSON
		        .stringify(exchangeRateTable));

		var excludedCount = 0;

		// loop and divide in onsite / offsite cost
		for (var i = 0; i < transactionSearch.length; i++) {
			var projectText = transactionSearch[i].getValue('custcolprj_name');

			var customerTextA = transactionSearch[i]
			        .getValue('custcolcustcol_temp_customer');
			var customerTextB = transactionSearch[i]
			        .getText('custcol_customer_list');
			var customerName = customerTextA ? customerTextA : customerTextB;

			var category = transactionSearch[i].getValue('formulatext');
			var amount = parseFloat(transactionSearch[i].getValue('fxamount'));
			var currency = transactionSearch[i].getText('currency');
			var convertedAmount = exchangeRateTable[currency] * amount;
			var practice = transactionSearch[i].getValue('department');
			var subsidiary = transactionSearch[i].getValue('subsidiary');
			var type = transactionSearch[i].getText('type');
			
			if(projectText == 'MICO02199 CaseWellness - FY17' && category == 'Travel' && currency =='INR'){
				nlapiLogExecution('debug','Project Name',projectText);
				nlapiLogExecution('debug','Project currency',currency);
				nlapiLogExecution('debug','Project Transaction Amount',amount);
				nlapiLogExecution('debug','Project Transaction exchangeRateTable_currency',exchangeRateTable[currency]);
				nlapiLogExecution('debug','Project Transaction convertedAmount',convertedAmount);
			}
			if (category == 'Exclude') {
				excludedCount += 1;
				continue;
			}

			if (!projectText) {
				projectText = 'NA';
			}

			if (projectText) {
				var currentProjectEntityId = (projectText.trim().split(' ')[0])
				        .trim();

				if (currentProjectEntityId == "8") {
					currentProjectEntityId = "8 Vuclip02089";
				}

				if (jobList[currentProjectEntityId]) {
					var project = jobList[currentProjectEntityId].InternalId;
					var customer = jobList[currentProjectEntityId].Customer;
					var customerName = jobList[currentProjectEntityId].CustomerName;
					var region = jobList[currentProjectEntityId].CustomerRegion;

					if (currentProjectEntityId == 'NA') {

						if (!customerName) {
							continue;
						}

						customerCode = (customerName.trim().split(' ')[0])
						        .trim();

						nlapiLogExecution('debug', 'project name is blank '
						        + customerName, customerCode + ' '
						        + convertedAmount);

						project = 'NA';
						customer = customerList[customerCode].Id;
						customerName = customerList[customerCode].Name;
						region = customerList[customerCode].Region;
					}

					if (!customer || !project || !practice) {
						continue;
					}

					if (!mainObject[customer]) {
						mainObject[customer] = {
						    Name : customerName,
						    Region : region,
						    ProjectList : {}
						};
					}

					if (!mainObject[customer].ProjectList[project]) {
						mainObject[customer].ProjectList[project] = {
						    Name : jobList[currentProjectEntityId].Name,
						    Type : jobList[currentProjectEntityId].Type,
						    BillingType : jobList[currentProjectEntityId].BillingType,
						    Category : jobList[currentProjectEntityId].Category,
						    PracticeList : {}
						};
					}

					if (!mainObject[customer].ProjectList[project].PracticeList[practice]) {
						// continue;
						mainObject[customer].ProjectList[project].PracticeList[practice] = new OpsMetricsObject();
						mainObject[customer].ProjectList[project].PracticeList[practice].Name = transactionSearch[i]
						        .getText('department');
					}

					if (category == 'Salary Cost') {
						if (subsidiary == 2) {
							mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOnsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOnsite += convertedAmount;
						} else {
							mainObject[customer].ProjectList[project].PracticeList[practice].SalariedCostOffsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOffsite += convertedAmount;
						}

						mainObject[customer].ProjectList[project].PracticeList[practice].TotalSalaryCost += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else if (category == 'Contractor Cost') {

						if (subsidiary == 2) { // Onsite
							mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOnsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOnsite += convertedAmount;
						} else { // Offsite
							mainObject[customer].ProjectList[project].PracticeList[practice].ContractCostOffsite += convertedAmount;
							mainObject[customer].ProjectList[project].PracticeList[practice].TotalCostOffsite += convertedAmount;
						}

						mainObject[customer].ProjectList[project].PracticeList[practice].TotalSalaryCost += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else if (category == 'Travel') {
						mainObject[customer].ProjectList[project].PracticeList[practice].Travel += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					} else {
						mainObject[customer].ProjectList[project].PracticeList[practice].Expense += convertedAmount;
						mainObject[customer].ProjectList[project].PracticeList[practice].TotalCost += convertedAmount;
					}
				}
			}
		}

		nlapiLogExecution('debug', 'excluded count', excludedCount);
	} catch (err) {
		nlapiLogExecution('ERROR', 'siteWiseProjectCost B', err);
		throw err;
	}
}