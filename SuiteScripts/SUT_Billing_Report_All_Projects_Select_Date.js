// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SUT_Billing_Report_All_Projects
     Author: Vikrant
     Company: Aashna CloudTech
     Date: 04-09-2014
     Description: Initial draft of the report which will give the data of time entered project wise for a project manager.
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - SUT_Billing_Report_All_Projects(request, response)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
    //  Initialize any Global Variables, in particular, ERRORging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function SUT_Billing_Report_All_Projects(request, response)//
{

    /*  Suitelet:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     -
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  SUITELET CODE BODY
    
    
    if (request.getMethod() == 'GET') // 
    {
        try //
        {
            //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', '********** Execution Started **********')
            
        	//Create the form and add fields to it 
            var form = nlapiCreateForm("Time Sheet Report - Finance");
            form.addField('custpage_start_date', 'date', 'Select Start Date: ');
            form.addField('custpage_end_date', 'date', 'Select End Date: ');
            
            // Start Date
            var d_custpage_start_date = request.getParameter('custpage_start_date');
            
            // End Date
            var d_custpage_end_date = request.getParameter('custpage_end_date');
            
            //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'd_custpage_date : ' + d_custpage_date)
            
            //form.setScript('customscript_cli_daywise_for_month');
            //form.addButton('custombutton', 'Export', 'CLI_Export_Timesheet_rpt(' + d_custpage_date + ')');
            
            var id = request.getParameter('id');
            
            form.addSubmitButton('Submit');
                        
            //var current_user = nlapiGetUser();
            //current_user = '1606'; // 100663-Janardhan Mallya Andar
            //current_user = '1705'; // 105333-Manisha Gattani
            //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'current_user : ' + current_user);
            
            var a_projects_list = new Array();
            
            var filters = new Array();
            var columns = new Array();
            
            //filters[filters.length] = new nlobjSearchFilter('custentity_projectmanager', null, 'anyof', current_user);
            
            columns[columns.length] = nlobjSearchColumn('internalid');
            columns[columns.length] = nlobjSearchColumn('companyname');
            
            //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'Before Search !!!')
            var search_result = nlapiSearchRecord('job', 'customsearch_projects_with_name', filters, null);
            //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'After Search !!!')
            
            if (_validate(search_result)) //
            {
                var result = search_result[0];
                var all_column = result.getAllColumns();
                //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'all_column : ' + all_column.length);
                
                var s_project_id = '';
                var s_project_name = '';
                
                //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'search_result : ' + search_result.length)
                
                for (var i_counter = 0; i_counter < search_result.length; i_counter++) //
                {
                    result = search_result[i_counter];
                    //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'result : ' + result);
                    
                    s_project_id = result.getValue(all_column[0]);
                    //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 's_project_id : ' + s_project_id);
                    
                    s_project_name = result.getValue(all_column[1]);
                    //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 's_project_name : ' + s_project_name);
                    
                    a_projects_list.push(s_project_id + "##" + s_project_name);
                }
            }
            
            var select = form.addField('custpage_selectfield', 'select', 'Select Project : ');
            select.addSelectOption('', '');
            
            if (_validate(a_projects_list)) //
            {
                //nlapiLogExecution('DEBUG', 'SUT_Billing_Report_All_Projects', 'a_projects_list : ' + a_projects_list.length)
                for (var j_counter = 0; j_counter < a_projects_list.length; j_counter++) //
                {
                    select.addSelectOption(a_projects_list[j_counter].split("##")[0], a_projects_list[j_counter].split("##")[1]);
                }
            }
            response.writePage(form);
        } 
        catch (ex) //
        {
            nlapiLogExecution('ERROR', 'SUT_Billing_Report_All_Projects', 'Ex : ' + ex)
            nlapiLogExecution('ERROR', 'SUT_Billing_Report_All_Projects', 'Ex.message : ' + ex.message)
        }
    }
    else // POST Method
    {
        var s_custpage_start_date = request.getParameter('custpage_start_date');
        //nlapiLogExecution('DEBUG', 'POST Method', 'd_custpage_date : ' + d_custpage_date);
        var s_custpage_end_date	= request.getParameter('custpage_end_date');
        
        var s_project = request.getParameter('custpage_selectfield');
        //nlapiLogExecution('DEBUG', 'POST Method', 's_project : ' + s_project);
        export_CSV(s_custpage_start_date, s_custpage_end_date, s_project);
    }
}

function export_CSV(s_start_date, s_end_date, s_project)//
{
    try//
    {
        //alert(param);
        //alert('s_start_date : ' + param.split('@')[0] + ', s_project : ' + param.split('@')[1]);
        
        //current_user = '1606'; // 100663-Janardhan Mallya Andar
        
        //nlapiLogExecution('DEBUG', 'export_CSV', '********  Into export_CSV');
        //nlapiLogExecution('DEBUG', 'export_CSV', 'param : ' + param);
        
        //var s_start_date = param.split('@')[0];
        //nlapiLogExecution('DEBUG', 'export_CSV', 's_start_date : ' + s_start_date);
        
        //var s_project = param.split('@')[1];
        //nlapiLogExecution('DEBUG', 'export_CSV', 's_project : ' + s_project);
        
        var r_project_rec = nlapiLoadRecord('job', s_project);
        var s_project_name = r_project_rec.getFieldValue('companyname');
        
		var a_all_resource_List = new Array();
        var a_entered_Emp = new Array();
        
        
		
        s_start_date = nlapiStringToDate(s_start_date);
        var d_start_date = s_start_date;
        //nlapiLogExecution('DEBUG', 'export_CSV', '1 s_start_date : ' + s_start_date);
        
        var i_date = s_start_date.getDate();
        i_date = 1 - i_date;
		//nlapiLogExecution('DEBUG', 'export_CSV', '1 i_date : ' + i_date);
		
        var i_month = s_start_date.getMonth() + 1;
        var i_year = s_start_date.getFullYear();
        
        //s_start_date = '01/' + i_month + '/' + i_year;
        
        //s_start_date = d_start_date;//nlapiAddDays(s_start_date, i_date)
		//nlapiLogExecution('DEBUG', 'export_CSV', '2 s_start_date : ' + s_start_date);
        
        //s_start_date = nlapiStringToDate(s_start_date);
        var s_end_date = nlapiStringToDate(s_end_date);//nlapiAddMonths(s_start_date, 1);
        //s_end_date = nlapiAddDays(s_end_date, -1);
        //nlapiLogExecution('DEBUG', 'export_CSV', 's_end_date : ' + s_end_date);
        var d_end_date = s_end_date;
        //alert('s_end_date : ' + s_end_date + ', s_start_date : ' + s_start_date);
        
        var one_day = 1000 * 60 * 60 * 24;
        
        var i_date_diff = s_end_date.getTime() - s_start_date.getTime();
        
        var i_days_diff = Math.round(i_date_diff / one_day); //s_end_date - s_start_date;
        i_days_diff = parseInt(i_days_diff + 1) + 1;
        //nlapiLogExecution('DEBUG', 'export_CSV', 'i_days_diff : ' + i_days_diff);
        
        var a_day_list = new Array();
        
        for (var i_counter = 0; i_counter < i_days_diff; i_counter++) //
        {
        	d_display_date = nlapiAddDays(s_start_date, i_counter);
            a_day_list[i_counter] = d_display_date.getDate() + ' - ' + getMonthName(d_display_date.getMonth());//nlapiAddDays(s_start_date, i_counter);// i_counter + 1;
        }
        
        var a_project_activity = new Array(i_days_diff);
        var a_leave = new Array(i_days_diff);
        var a_holiday = new Array(i_days_diff);
        var a_ot = new Array(i_days_diff);
        var a_total = new Array(i_days_diff);
        
        var a_project_activity_a = new Array(i_days_diff);
        var a_leave_a = new Array(i_days_diff);
        var a_holiday_a = new Array(i_days_diff);
        var a_ot_a = new Array(i_days_diff);
        var a_total_a = new Array(i_days_diff);
        
        var a_project_activity_total = new Array(i_days_diff);
        var a_leave_total = new Array(i_days_diff);
        var a_holiday_total = new Array(i_days_diff);
        var a_ot_total = new Array(i_days_diff);
        
        var i_PA_month_total = 0; // Monthly total hours for Project Activities task 
        var i_OT_month_total = 0; // Monthly total hours for OT task
        var i_LEAVE_month_total = 0; // Monthly total hours for Leave task
        var i_HOLIDAY_month_total = 0; // Monthly total hours for Holiday task
		
		var i_PA_month_total_a = 0; // Monthly total hours(Approved) for Project Activities task 
        var i_OT_month_total_a = 0; // Monthly total hours(Approved) for OT task
        var i_LEAVE_month_total_a = 0; // Monthly total hours(Approved) for Leave task
        var i_HOLIDAY_month_total_a = 0; // Monthly total hours(Approved) for Holiday task
		
        var i_TOTAL_month_total = 0; // Monthly total hours
		var i_TOTAL_month_total_a = 0; // Monthly total hours(Approved)
        //var CSV_String = ',' + s_project_name + '\n';
        
        var CSV_String = 'Project Name : ' + s_project_name + ',Submitted Hours,' + a_day_list + ',Approved Hours,' + a_day_list + ',Total (submitted hours),Total (Approved hours)\n';
        
        //alert(CSV_String);
        //return;
        
        var i_lastInternalID = 0; // used for looping when records are greater than 1000
        var previousInternalID = '';
        
        var temp_emp = '';
        var temp_emp_name = '';
        var temp_fusion_id = '';
        var emp = '';
        var date = '';
        var hrs = 0;
        
        var context = nlapiGetContext();
        var remaining_usage = context.getRemainingUsage();
        
        s_start_date = nlapiDateToString(s_start_date);
        s_end_date = nlapiDateToString(s_end_date);
        
		if (_validate(s_project)) //
        {
            var filters1 = new Array();
            var columns1 = new Array();
            
            filters1[filters1.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', s_project);
            filters1[filters1.length] = new nlobjSearchFilter('enddate', null, 'onorafter', s_start_date);
			
            columns1[columns1.length] = new nlobjSearchColumn('resource', null, 'group');
            
            var search_result_1 = nlapiSearchRecord('resourceallocation', null, filters1, columns1);
            
            if (_validate(search_result_1)) //
            {
                //nlapiLogExecution('DEBUG', 'export_CSV', 'search_result_1 : ' + search_result_1.length);
                var all_columns_1 = search_result_1[0].getAllColumns();
                
                for (var t_counter = 0; t_counter < search_result_1.length; t_counter++) //
                {
                    //nlapiLogExecution('DEBUG', 'export_CSV', 'search_result_1[t_counter].getText(all_columns_1[0]) : ' + search_result_1[t_counter].getText(all_columns_1[0]));
                    a_all_resource_List.push(search_result_1[t_counter].getText(all_columns_1[0]));
                }
            }
            else //
            {
                //nlapiLogExecution('DEBUG', 'export_CSV', 'Invalid search_result_1 : ' + search_result_1);
            }
        }
        
        //nlapiLogExecution('DEBUG', 'export_CSV', 'a_all_resource_List : ' + a_all_resource_List.length);
		
        if (_validate(s_start_date)) //
        {
            var counter = 1;
            
            do //
            {
                var filters = new Array();
                
                //nlapiLogExecution('DEBUG', 'export_CSV', 's_start_date : ' + s_start_date + ', s_end_date : ' + s_end_date);
                filters[filters.length] = new nlobjSearchFilter('date', null, 'within', s_start_date, s_end_date);
                filters[filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', i_lastInternalID);
                filters[filters.length] = new nlobjSearchFilter('customer', null, 'anyof', s_project);
                
                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_lastInternalID : ' + i_lastInternalID);
                
                var search_result = nlapiSearchRecord('timebill', 'customsearch_timesheet_report_llc_month', filters, null);
                
                if (_validate(search_result)) //
                {
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'search_result.length : ' + search_result.length);
                    
                    var trandate = ''; //result.getValue(all_column[0]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'trandate : ' + trandate);
                    
                    var line_count = search_result.length;
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'line_count : ' + line_count);
                    
                    var s_task_name = ''; //result.getValue(all_column[3]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 's_task_name : ' + s_task_name);
                    
                    var i_hrs = ''; //result.getValue(all_column[4]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_hrs : ' + i_hrs);
                    
                    var item = ''; //result.getValue(all_column[5]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'item : ' + item);
                    
                    var item_name = ''; //result.getText(all_column[5]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'item_name : ' + item_name);
                    
                    var fusion_ID = ''; //result.getValue(all_column[6]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'fusion_ID : ' + fusion_ID);
                    
                    var i_emp_id = '';
                    var s_emp_name = '';
                    var s_is_approved = '';
                    
                    // variable declaration was here earlier
                    
                    var result = search_result[0];
                    var all_column = result.getAllColumns();
                    
                    for (var i = 0; i < line_count; i++) //
                    {
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', '********* New Row ************ ' + counter++);
                        //previousInternalID = i_lastInternalID;
                        
                        result = search_result[i];
                        //var all_column = result.getAllColumns();//
                        
                        var text = '';
                        var value = '';
                        var label = '';
                        
                        for (var j_counter = 0; j_counter < all_column.length; j_counter++) //
                        {
                            text = result.getText(all_column[j_counter]);
                            value = result.getValue(all_column[j_counter]);
                            label = all_column[j_counter].getLabel();
                            
                            if (label == 'date') //
                            {
                                trandate = value;
                            }
                            
                            if (label == 'emp_id') //
                            {
                                i_emp_id = value;
                            }
                            
                            if (label == 'employee') //
                            {
                                s_emp_name = text;
                            }
                            
                            if (label == 'Task Name') //
                            {
                                s_task_name = value;
                            }
                            
                            if (label == 'duration') //
                            {
                                i_hrs = value;
                            }
                            
                            if (label == 'is_approved') //
                            {
                                s_is_approved = value;
                            }
							
							if (label == 'approval_status') //
                            {
                                s_is_approved = text;
                            }
                        }
                        
                        if (i == 0) // For the first data row
                        {
                            temp_emp = i_emp_id;
                            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_emp : ' + temp_emp);
                            
                            temp_emp_name = s_emp_name;
                        }
                        
                        // from the next data row
                        i_lastInternalID = temp_emp; // to get the last employee id for next search of 1000 records
                        emp = i_emp_id;
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'emp : ' + emp);
						//nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 's_is_approved : ' + s_is_approved);
                        
                        if (temp_emp != emp) // if employee changes then put all values to CSV String and make all variable to initial value
                        {
                            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'New Employee....');
                            
                            for (var k_counter = 0; k_counter < a_total; k_counter++) //
                            {
                                if (!isNaN(a_total[k_counter])) //
                                {
                                    i_TOTAL_month_total = parseFloat(i_TOTAL_month_total) + parseFloat(a_total[k_counter]);
                                }
                                
                                if (!isNaN(a_total_a[k_counter])) //
                                {
                                    i_TOTAL_month_total = parseFloat(i_TOTAL_month_total) + parseFloat(a_total_a[k_counter]);
									i_TOTAL_month_total_a = parseFloat(i_TOTAL_month_total_a) + parseFloat(a_total_a[k_counter]);
                                }
                            }
                            a_entered_Emp.push(temp_emp_name);
                            
                            CSV_String += temp_emp_name + ',ST,' + a_project_activity + ',,' + a_project_activity_a + ',' + i_PA_month_total + ',' + i_PA_month_total_a + '\n';
                            
                            CSV_String += '' + ',OT,' + a_ot + ',,' + a_ot_a + ',' + i_OT_month_total + ',' + i_OT_month_total_a + '\n';
                            
                            CSV_String += '' + ',Leave,' + a_leave + ',,' + a_leave_a + ',' + i_LEAVE_month_total + ',' + i_LEAVE_month_total_a + '\n';
                            
                            CSV_String += '' + ',Holiday,' + a_holiday + ',,' + a_holiday_a + ',' + i_HOLIDAY_month_total + ',' + i_HOLIDAY_month_total_a + '\n';
                            
                            i_TOTAL_month_total = parseFloat(i_PA_month_total) + parseFloat(i_OT_month_total) + parseFloat(i_LEAVE_month_total) + parseFloat(i_HOLIDAY_month_total);
                            i_TOTAL_month_total_a = parseFloat(i_PA_month_total_a) + parseFloat(i_OT_month_total_a) + parseFloat(i_LEAVE_month_total_a) + parseFloat(i_HOLIDAY_month_total_a);
                            
                            CSV_String += '' + ',Total,' + a_total + ',,' + a_total_a + ',' + i_TOTAL_month_total + ',' + i_TOTAL_month_total_a + '\n';
                            
                            a_project_activity = new Array(i_days_diff);
                            a_leave = new Array(i_days_diff);
                            a_holiday = new Array(i_days_diff);
                            a_ot = new Array(i_days_diff);
                            
                            a_project_activity_a = new Array(i_days_diff);
                            a_leave_a = new Array(i_days_diff);
                            a_holiday_a = new Array(i_days_diff);
                            a_ot_a = new Array(i_days_diff);
                            
                            a_total = new Array(i_days_diff);
                            a_total_a = new Array(i_days_diff);
                            
                            i_PA_month_total = 0;
                            i_OT_month_total = 0;
                            i_LEAVE_month_total = 0;
                            i_HOLIDAY_month_total = 0;
                            
                            i_PA_month_total_a = 0;
                            i_OT_month_total_a = 0;
                            i_LEAVE_month_total_a = 0;
                            i_HOLIDAY_month_total_a = 0;
							
							i_TOTAL_month_total = 0;
                            i_TOTAL_month_total_a = 0;
                            
							if(i < 900) //
							{
								break;
							}
							
                            i_lastInternalID = i_emp_id;
                            temp_emp = i_emp_id;
                            temp_emp_name = s_emp_name;
							
                        }
                        
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 's_is_approved : ' + s_is_approved);
                        
                        trandate = nlapiStringToDate(trandate);
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'trandate : ' + trandate);
                        
                        var i_day_of_month = getDayDiff(trandate, d_start_date);//trandate.getDate();
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_day_of_month : ' + i_day_of_month);
                        
                        //i_day_of_month = i_day_of_month - 1;
                        //alert('i_day_of_month : ' + i_day_of_month);
                        
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 's_task_name : ' + s_task_name);
                        
                        if (isNaN(a_total[i_day_of_month])) //
                        {
                            a_total[i_day_of_month] = 0;
                        }
                        
                        if (isNaN(a_total_a[i_day_of_month])) //
                        {
                            a_total_a[i_day_of_month] = 0;
                        }
                        
                        if (s_task_name == 'Project Activities') //
                        {
                            if (s_is_approved == 'Pending Approval') //
                            {
                                var temp_hrs = a_project_activity[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) // if thr is some value in array on given day
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(i_hrs);
                                    a_project_activity[i_day_of_month] = temp_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_project_activity[i_day_of_month] = i_hrs;
                                }
                                
                                i_PA_month_total = parseFloat(i_PA_month_total) + parseFloat(i_hrs);
                                a_total[i_day_of_month] = parseFloat(a_total[i_day_of_month]) + parseFloat(i_hrs);
                                
                                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_project_activity[i_day_of_month] : ' + a_project_activity[i_day_of_month]);
                            }
                            else //
                            {
                                var temp_hrs = a_project_activity_a[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) // if thr is some value in array on given day
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(i_hrs);
                                    a_project_activity_a[i_day_of_month] = temp_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_project_activity_a[i_day_of_month] = i_hrs;
                                }
                                i_PA_month_total_a = parseFloat(i_PA_month_total_a) + parseFloat(i_hrs);
                                a_total_a[i_day_of_month] = parseFloat(a_total_a[i_day_of_month]) + parseFloat(i_hrs);
                                
                                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_project_activity_a[i_day_of_month] : ' + a_project_activity_a[i_day_of_month]);
                            }
                        }
                        
                        if (s_task_name == 'OT') //
                        {
                            if (s_is_approved == 'Pending Approval') //
                            {
                                var temp_hrs = a_ot[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) //
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(a_ot[i_day_of_month]);
                                    a_ot[i_day_of_month] = temp_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_ot[i_day_of_month] = i_hrs;
                                }
                                i_OT_month_total = parseFloat(i_OT_month_total) + parseFloat(i_hrs);
                                a_total[i_day_of_month] = parseFloat(a_total[i_day_of_month]) + parseFloat(i_hrs);
                            }
                            else //
                            {
                                var temp_hrs = a_ot_a[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) // if thr is some value in array on given day
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(i_hrs);
                                    a_ot_a[i_day_of_month] = temp_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_ot_a[i_day_of_month] = i_hrs;
                                }
								i_OT_month_total_a = parseFloat(i_OT_month_total_a) + parseFloat(i_hrs);
                                a_total_a[i_day_of_month] = parseFloat(a_total_a[i_day_of_month]) + parseFloat(i_hrs);
                                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_ot_a[i_day_of_month] : ' + a_ot_a[i_day_of_month]);
                            }
                        }
                        
                        if (s_task_name == 'Leave') //
                        {
                            if (s_is_approved == 'Pending Approval') //
                            {
                                var temp_hrs = a_leave[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) //
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(a_leave[i_day_of_month]);
                                    a_leave[i_day_of_month] = i_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_leave[i_day_of_month] = i_hrs;
                                }
                                i_LEAVE_month_total = parseFloat(i_LEAVE_month_total) + parseFloat(i_hrs);
                                a_total[i_day_of_month] = parseFloat(a_total[i_day_of_month]) + parseFloat(i_hrs);
                            }
                            else //
                            {
                                var temp_hrs = a_leave_a[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) // if thr is some value in array on given day
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(i_hrs);
                                    a_leave_a[i_day_of_month] = temp_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_leave_a[i_day_of_month] = i_hrs;
                                }
								i_LEAVE_month_total_a = parseFloat(i_LEAVE_month_total_a) + parseFloat(i_hrs);
                                a_total_a[i_day_of_month] = parseFloat(a_total_a[i_day_of_month]) + parseFloat(i_hrs);
                                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_leave_a[i_day_of_month] : ' + a_leave_a[i_day_of_month]);
                            }
                        }
                        
                        if (s_task_name == 'Holiday') //
                        {
                            if (s_is_approved == 'Pending Approval') //
                            {
                                var temp_hrs = a_holiday[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) //
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(a_holiday[i_day_of_month]);
                                    a_holiday[i_day_of_month] = i_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_holiday[i_day_of_month] = i_hrs;
                                }
                                i_HOLIDAY_month_total = parseFloat(i_HOLIDAY_month_total) + parseFloat(i_hrs);
                                a_total[i_day_of_month] = parseFloat(a_total[i_day_of_month]) + parseFloat(i_hrs);
                            }
                            else //
                            {
                                var temp_hrs = a_holiday_a[i_day_of_month];
                                
                                if (!isNaN(temp_hrs)) // if thr is some value in array on given day
                                {
                                    temp_hrs = parseFloat(temp_hrs) + parseFloat(i_hrs);
                                    a_holiday_a[i_day_of_month] = temp_hrs;
                                }
                                else // if hrs are blank in array
                                {
                                    a_holiday_a[i_day_of_month] = i_hrs;
                                }
								i_HOLIDAY_month_total_a = parseFloat(i_HOLIDAY_month_total_a) + parseFloat(i_hrs);
                                a_total_a[i_day_of_month] = parseFloat(a_total_a[i_day_of_month]) + parseFloat(i_hrs);
                                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_holiday_a[i_day_of_month] : ' + a_holiday_a[i_day_of_month]);
                            }
                        }
						
						if (i == line_count - 1) // if employee changes then put all values to CSV String and make all variable to initial value
                        {
                            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'New Employee....');
                            
                            for (var k_counter = 0; k_counter < a_total; k_counter++) //
                            {
                                if (!isNaN(a_total[k_counter])) //
                                {
                                    i_TOTAL_month_total = parseFloat(i_TOTAL_month_total) + parseFloat(a_total[k_counter]);
                                }
                                
                                if (!isNaN(a_total_a[k_counter])) //
                                {
                                    i_TOTAL_month_total = parseFloat(i_TOTAL_month_total) + parseFloat(a_total_a[k_counter]);
                                    i_TOTAL_month_total_a = parseFloat(i_TOTAL_month_total_a) + parseFloat(a_total_a[k_counter]);
                                }
                            }
                            
                            a_entered_Emp.push(temp_emp_name);
                            
                            CSV_String += temp_emp_name + ',ST,' + a_project_activity + ',,' + a_project_activity_a + ',' + i_PA_month_total + ',' + i_PA_month_total_a + '\n';
                            
                            CSV_String += '' + ',OT,' + a_ot + ',,' + a_ot_a + ',' + i_OT_month_total + ',' + i_OT_month_total_a + '\n';
                            
                            CSV_String += '' + ',Leave,' + a_leave + ',,' + a_leave_a + ',' + i_LEAVE_month_total + ',' + i_LEAVE_month_total_a + '\n';
                            
                            CSV_String += '' + ',Holiday,' + a_holiday + ',,' + a_holiday_a + ',' + i_HOLIDAY_month_total + ',' + i_HOLIDAY_month_total_a + '\n';
                            
							i_TOTAL_month_total = parseFloat(i_PA_month_total) + parseFloat(i_OT_month_total) + parseFloat(i_LEAVE_month_total) + parseFloat(i_HOLIDAY_month_total);
                            i_TOTAL_month_total_a = parseFloat(i_PA_month_total_a) + parseFloat(i_OT_month_total_a) + parseFloat(i_LEAVE_month_total_a) + parseFloat(i_HOLIDAY_month_total_a);
							
                            CSV_String += '' + ',Total,' + a_total + ',,' + a_total_a + ',' + i_TOTAL_month_total + ',' + i_TOTAL_month_total_a + '\n';
                            
                            a_project_activity = new Array(i_days_diff);
                            a_leave = new Array(i_days_diff);
                            a_holiday = new Array(i_days_diff);
                            a_ot = new Array(i_days_diff);
                            
                            a_project_activity_a = new Array(i_days_diff);
                            a_leave_a = new Array(i_days_diff);
                            a_holiday_a = new Array(i_days_diff);
                            a_ot_a = new Array(i_days_diff);
                            
                            a_total = new Array(i_days_diff);
                            a_total_a = new Array(i_days_diff);
                            
                            i_PA_month_total = 0;
                            i_OT_month_total = 0;
                            i_LEAVE_month_total = 0;
                            i_HOLIDAY_month_total = 0;
							
							i_PA_month_total_a = 0;
                            i_OT_month_total_a = 0;
                            i_LEAVE_month_total_a = 0;
                            i_HOLIDAY_month_total_a = 0;
                            
                            i_TOTAL_month_total = 0;
                            i_TOTAL_month_total_a = 0;
                            
                            i_lastInternalID = i_emp_id;
                            temp_emp = i_emp_id;
                            temp_emp_name = s_emp_name;
							
							// Printing Remaining Employee who haven't filled any hour in given duration of report.
                            
                            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_all_resource_List.length : ' + a_all_resource_List);
                            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_entered_Emp.length : ' + a_entered_Emp);
                            
                            if (a_all_resource_List.length > a_entered_Emp.length) // 
                            {
                                for (var y_counter = 0; y_counter < a_all_resource_List.length; y_counter++) //
                                {
                                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_all_resource_List[y_counter] : ' + a_all_resource_List[y_counter]);
                                    var flag_present = false;
                                    
                                    for (var u_counter = 0; u_counter < a_entered_Emp.length; u_counter++) //
                                    {
                                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_all_resource_List[y_counter] : ' + a_all_resource_List[y_counter]);
                                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'a_entered_Emp[u_counter] : ' + a_entered_Emp[u_counter]);
                                        
                                        if (a_all_resource_List[y_counter] == a_entered_Emp[u_counter]) //
                                        {
                                            flag_present = true;
                                            break;
                                        }
                                    }
                                    
                                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'flag_present : ' + flag_present);
                                    
                                    if (flag_present == false) //
                                    {
                                        CSV_String += a_all_resource_List[y_counter] + ',ST,' + a_project_activity + ',,' + a_project_activity_a + ',' + i_PA_month_total + ',' + i_PA_month_total_a + '\n';
                                        
                                        CSV_String += '' + ',OT,' + a_ot + ',,' + a_ot_a + ',' + i_OT_month_total + ',' + i_OT_month_total_a + '\n';
                                        
                                        CSV_String += '' + ',Leave,' + a_leave + ',,' + a_leave_a + ',' + i_LEAVE_month_total + ',' + i_LEAVE_month_total_a + '\n';
                                        
                                        CSV_String += '' + ',Holiday,' + a_holiday + ',,' + a_holiday_a + ',' + i_HOLIDAY_month_total + ',' + i_HOLIDAY_month_total_a + '\n';
                                        
                                        CSV_String += '' + ',Total,' + a_total + ',,' + a_total_a + ',' + i_TOTAL_month_total + ',' + i_TOTAL_month_total_a + '\n';
                                    }
                                }
                            }
                        }
                        
                        remaining_usage = context.getRemainingUsage();
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'remaining_usage : ' + remaining_usage);
                    
                    } // end for loop
                } // end of validate search result
                else //
                {
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'search_result is invalid : ' + search_result);
                }
            } // end of Do
            //while (search_result_0 != null)
            while (search_result != null)
            {
                //return CSV_String;
            }
            
            // Export file to CSV
            export_File(CSV_String);
            
            //nlapiLogExecution('DEBUG', 'CSV_Data', 'After while loop !!!');
        }
        else //
        {
            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'from_date is invalid : ' + from_date);
        }
    } 
    catch (ex) //
    {
        //alert('Exception : ' + ex.message)
        nlapiLogExecution('ERROR', 'exception', ex);
        nlapiLogExecution('ERROR', 'exception', ex.message);
    }
}

function export_File(CSV_String) //
{
    // File export
    //nlapiLogExecution('DEBUG', 'export_File', '**** Into export_File ***');
    nlapiLogExecution('DEBUG','CSV', CSV_String);
    var fileName = 'Time Sheet Report - Finance'
    var Datetime = new Date();
	Datetime = nlapiDateToString(Datetime);
	
    var CSVName = fileName + " - " + Datetime + '.csv';
    //nlapiLogExecution('DEBUG', 'CSV_Data', 'CSV_DATA : ' + CSV_String);
    
    var file = nlapiCreateFile(CSVName, 'CSV', CSV_String.toString());
    //nlapiLogExecution('DEBUG', 'export_File', '**** File Created ***');
    
    //file.setFolder(635);
    //var file_id = nlapiSubmitFile(file);
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'file_id ' + file_id);
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.setContentType('CSV', CSVName);
    //response.setContentType('CSV', CSVName, 'inline');
    //nlapiLogExecution('DEBUG', 'export_File', '**** Before exporting ***');
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'file.getValue() ' + file.getValue());
    //nlapiLogExecution('DEBUG', 'export_File', 'File : ' + file.getValue());
    response.write(file.getValue());
    
}
// Return Month Name
function getMonthName(i_month)
{
	var a_months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	return a_months[i_month];
}
// Return days difference
function getDayDiff(d_end_date, d_start_date)
{
	var one_day = 1000 * 60 * 60 * 24;
    
    var i_date_diff = d_end_date.getTime() - d_start_date.getTime();
    
    var i_days_diff = Math.round(i_date_diff / one_day); //s_end_date - s_start_date;
    i_days_diff = parseInt(i_days_diff + 1);
    
    return i_days_diff;
}

function _validate(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined')//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

function get_Formated_Date(d_date) //
{
    var today = new Date(d_date);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    
    d_date = mm + '/' + dd + '/' + yyyy;
    
    return d_date;
}

function get_todays_date(d_date)//
{
    var today;
    // ============================= Todays Date ==========================================
    
    var date1 = new Date();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
    
    date1 = d_date;
    
    var offsetIST = 5.5;
    
    //To convert to UTC datetime by subtracting the current Timezone offset
    var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
    
    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
    var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
    var day = istdate.getDate();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
    
    var month = istdate.getMonth() + 1;
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);
    
    var year = istdate.getFullYear();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' + year);
    
    var date_format = checkDateFormat();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' + date_format);
    
    if (date_format == 'YYYY-MM-DD') {
        today = year + '-' + month + '-' + day;
    }
    if (date_format == 'DD/MM/YYYY') {
        today = day + '/' + month + '/' + year;
    }
    if (date_format == 'MM/DD/YYYY') {
        today = month + '/' + day + '/' + year;
    }
    var s_month = '';
    
    if (month == 1) {
        s_month = 'Jan'
        
    }
    else 
        if (month == 2) {
            s_month = 'Feb'
            
        }
        else 
            if (month == 3) {
            
                s_month = 'Mar'
            }
            else 
                if (month == 4) {
                    s_month = 'Apr'
                    
                }
                else 
                    if (month == 5) {
                        s_month = 'May'
                        
                    }
                    else 
                        if (month == 6) {
                        
                            s_month = 'Jun'
                        }
                        else 
                            if (month == 7) {
                                s_month = 'Jul'
                                
                            }
                            else 
                                if (month == 8) {
                                    s_month = 'Aug'
                                    
                                }
                                else 
                                    if (month == 9) {
                                        s_month = 'Sep'
                                        
                                    }
                                    else 
                                        if (month == 10) {
                                            s_month = 'Oct'
                                            
                                        }
                                        else 
                                            if (month == 11) {
                                            
                                                s_month = 'Nov'
                                            }
                                            else 
                                                if (month == 12) {
                                                
                                                    s_month = 'Dec'
                                                }
    
    if (date_format == 'DD-Mon-YYYY') {
        today = day + '-' + s_month + '-' + year;
    }
    
    
    return today;
}


// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
