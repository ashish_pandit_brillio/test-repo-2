 /**
 * Module Description
 * User Event script to handle employee record creation and updation from the XML file generated from Fusion
 * 
 * Version               Date                      Author                           Remarks
 * 1.00             13th November, 2015         Manikandan V          This script saves the form data into 100 days questionnaire custom record from 30 days * questionnaire.html 
 *
 */


function feedbackSuitelet(request, response)
{
  var method=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(78866);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);          //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call");
  
  var feedback = new Object();
  feedback['US_Employee_ID'] = request.getParameter('TextBox1');
  
  feedback['US_Email_Id'] = request.getParameter('TextBox2');
  feedback['US_Employee_Name'] = request.getParameter('TextBox3');
  
  
  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Comments1'] = request.getParameter('txtComments1');
  
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Comments2'] = request.getParameter('txtComments2');
  
  feedback['Response3'] = request.getParameter('OptionsGroup3');
  feedback['Comments3'] = request.getParameter('txtComments3');

  feedback['Response4'] = request.getParameter('OptionsGroup4');
  feedback['Comments4'] = request.getParameter('txtComments4');
  
  feedback['Response5'] = request.getParameter('OptionsGroup5');
  feedback['Comments5'] = request.getParameter('txtComments5');
  
  feedback['Response6'] = request.getParameter('OptionsGroup6');
  feedback['Comments6'] = request.getParameter('txtComments6');
  
  feedback['Response7'] = request.getParameter('OptionsGroup7');
  feedback['Comments7'] = request.getParameter('txtComments7');
  
  feedback['Response8'] = request.getParameter('OptionsGroup8');
  feedback['Comments8'] = request.getParameter('txtComments8');
  
  feedback['Response9'] = request.getParameter('OptionsGroup9');
  feedback['Comments9'] = request.getParameter('txtComments9');
  feedback['Comments10'] = request.getParameter('txtComments10');
  feedback['Comments11'] = request.getParameter('txtComments11');
  feedback['Comments12'] = request.getParameter('txtComments12');
  feedback['Comments13'] = request.getParameter('txtComments13');
  feedback['Comments14'] = request.getParameter('txtComments14');
  
  feedback['Response10'] = request.getParameter('OptionsGroup10');
  feedback['Response11'] = request.getParameter('OptionsGroup11');
  feedback['Response12'] = request.getParameter('OptionsGroup12');
  feedback['Response13'] = request.getParameter('OptionsGroup13');
  feedback['Response14'] = request.getParameter('OptionsGroup14');
  
  
  
  
  
  
  var status = saveRequest(feedback);
   
    var thanks_note = nlapiLoadFile(588851);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
	try
	{
             var feedback_form = nlapiCreateRecord('customrecord_100_days_response_us');
                         feedback_form.setFieldValue('custrecord_us_employee_id', feedback.US_Employee_ID);
	                     feedback_form.setFieldValue('custrecord_us_employee_name', feedback.US_Employee_Name);
                         feedback_form.setFieldValue('custrecord_us_email_id', feedback.US_Email_Id);
	                     

			   
              feedback_form.setFieldValue('custrecord_question_01',"I am enjoying my job or role at Brillio");
              feedback_form.setFieldValue('custrecord_question_02', " I feel that my role gives me a good scope for professional development");
              feedback_form.setFieldValue('custrecord_question_03', "My Manager is available and approachable in times of need");
			  
			  feedback_form.setFieldValue('custrecord_question_04',"My Manager connects with me to provide advice and constructive feedback on my performance on timely manner");
			  feedback_form.setFieldValue('custrecord_question_05',"I am aware that the company policies and processes are available on Brillio Sharepoint portal");
			  feedback_form.setFieldValue('custrecord_question_06',"The HR team provides timely help with my queries and concerns");
			  feedback_form.setFieldValue('custrecord_question_07',"I am aware about or participate in the monthly engagement events that happen in my location");
			  feedback_form.setFieldValue('custrecord_question_08',"I have discussed my aspirations with my Manager");
			  feedback_form.setFieldValue('custrecord_question_09',"I am aware of the training and development options for me and I have discussed my training plans with my manager");
			  feedback_form.setFieldValue('custrecord_question_10',"I would like to be a part of ");
			  feedback_form.setFieldValue('custrecord_question_011',"I would like Brillio to");
			  feedback_form.setFieldValue('custrecord_question_012',"I would like my manager to");
			  feedback_form.setFieldValue('custrecord_question_013',"I would like the HR to");
			  feedback_form.setFieldValue('custrecord_question_014',"I would like to Thank, appreciate or acknowledge");
			  
              

               feedback_form.setFieldValue('custrecord_answer_01', feedback.Response1);
               feedback_form.setFieldValue('custrecord_answer_02', feedback.Response2);
               feedback_form.setFieldValue('custrecord_answer_03', feedback.Response3);
			   
			   feedback_form.setFieldValue('custrecord_answer_04', feedback.Response4);
			   feedback_form.setFieldValue('custrecord_answer_05', feedback.Response5);
			   feedback_form.setFieldValue('custrecord_answer_06', feedback.Response6);
			   feedback_form.setFieldValue('custrecord_answer_07', feedback.Response7);
			   feedback_form.setFieldValue('custrecord_answer_08', feedback.Response8);
			   feedback_form.setFieldValue('custrecord_answer_09', feedback.Response9);
			   
			   
			   
			   
			                                       feedback_form.setFieldValue('custrecord_suggestions_01', feedback.Comments1);
                                                   feedback_form.setFieldValue('custrecord_suggestions_02', feedback.Comments2);
                                                   feedback_form.setFieldValue('custrecord_suggestions_03', feedback.Comments3);
                                                   feedback_form.setFieldValue('custrecord_suggestions_04', feedback.Comments4);
                                                   feedback_form.setFieldValue('custrecord_suggestions_05', feedback.Comments5);
                                                   feedback_form.setFieldValue('custrecord_suggestions_06', feedback.Comments6);
                                                   feedback_form.setFieldValue('custrecord_suggestions_07', feedback.Comments7);
                                                   feedback_form.setFieldValue('custrecord_suggestions_08', feedback.Comments8);
                                                   
                                                   feedback_form.setFieldValue('custrecord_suggestions_09', feedback.Comments9);
												   
												   
												   
												   feedback_form.setFieldValue('custrecord_comments_01', feedback.Comments10);
												   feedback_form.setFieldValue('custrecord_comments_02', feedback.Comments11);
												   feedback_form.setFieldValue('custrecord_comments_03', feedback.Comments12);
												   feedback_form.setFieldValue('custrecord_comments_04', feedback.Comments13);
												   feedback_form.setFieldValue('custrecord_comments_05', feedback.Comments14);
			   
			   
			   
			   
			   
			   
			   
			   
			   
               
               
              
               
                         
     
              
                         var id = nlapiSubmitRecord(feedback_form, false,true);
                         nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log','');
}
}
 }