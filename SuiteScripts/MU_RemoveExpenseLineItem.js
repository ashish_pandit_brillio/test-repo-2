/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Jan 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var recER	=	nlapiLoadRecord(recType, recId);
	
	var num_lines = recER.getLineItemCount('expense');
	
	for(var i = 1; i <= num_lines; i++)
		{
			var f_amount = recER.getLineItemValue('expense', 'amount',i);
			
			if(f_amount == '0.01' || f_amount == '-0.01')
				{
					recER.removeLineItem('expense', i);
				}
		}
	
	nlapiSubmitRecord(recER);
}
