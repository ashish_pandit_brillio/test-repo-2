//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

/*
		Script Name:	Restlet_Allocation_details_for_SFDC.js
		Author: 		Nihal Mulani
	    Date:           16 Oct 2020
		Description:	JSONObj.


		Script Modification Log:

		-- Date --		-- Modified By --			       --Requested By--				-- Description --

}*/
//END SCRIPT DESCRIPTION BLOCK  ====================================

/**
 * @NApiVersion 2.0
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(['N/record','N/search','N/format' ],function(record,search,format){
	function sendOffboardingData(datain) 
	{
		log.debug('Start');
		var date = datain.lastmodifieddate;
		log.debug('date',date);
		if(date)
		{
			var Timestamp = format.format({
				value: new Date(),
				type: format.Type.DATETIME,
				//timezone: format.Timezone.ASIA_MUSCAT
			});
			var stampSplit = Timestamp.split(" ");
			var datesplit = stampSplit[0].split("/");
			var Time = stampSplit[1].split(":");
			var timeStamp = datesplit[0]+"/"+datesplit[1]+"/"+datesplit[2]+" "+Time[0]+":"+Time[1]+" "+stampSplit[2]
			log.debug('timeStamp',timeStamp);

			var loadSearch = getOffboardingData(date,timeStamp);

			return {
				"status":"success",
				"lastmodifieddate":timeStamp,
				"data":loadSearch
			}
		}
		else
		{
			return {
				"status":"error",
				"message":"Last modified date required."
			}
		}
	}
	function getOffboardingData(fromDate , toDate)
	{
		var ActualSearch = search.load({
			id: 'customsearch3436'
		});

		ActualSearch.filters.push(search.createFilter({
			name : 'lastmodifieddate',
			operator : search.Operator.WITHIN,
			values : [fromDate,toDate]
		}));

		var results = [];
		var count = 0;
		var pageSize = 1000;
		var start = 0;
		do {
			var subresults = ActualSearch.run().getRange({
				start: start,
				end: start + pageSize
			});

			results = results.concat(subresults);
			count = subresults.length;
			start += pageSize;
		} while (count == pageSize);
		log.debug('loadActualSearch:', results.length);
		var tempArr = [];
		var ActualData = new Array();
		for (var res = 0; res < results.length; res++)
		{
			var internalID = results[res].getValue({name: "internalid", label: "Internal ID"});
			var email = results[res].getValue({
				name: "email",
				//	summary: "GROUP",
			});
			email = (Brillio_Location == "- None -") ? null : email;

			var Brillio_Location = results[res].getValue({
				name: "custentity_list_brillio_location_e",
				//	summary: "GROUP",
				label: "Brillio Location"
			});
			Brillio_Location = (Brillio_Location == "- None -") ? null : Brillio_Location;
			var Person_Type = results[res].getText({
				name: "custentity_employeetype",
				//	summary: "GROUP",
				label: "Person Type"
			});
			Person_Type = (Person_Type == "- None -") ? null : Person_Type;
			var Legal_Entity = results[res].getValue({
				name: "custentity_legal_entity_fusion",
				//	summary: "GROUP",
				label: "Legal Entity Fusion"
			});
			Legal_Entity = (Legal_Entity == "- None -") ? null : Legal_Entity;
			var Level = results[res].getText({
				name: "employeestatus",
				//	summary: "GROUP",
				label: "Employee Status"
			});
			Level = (Level == "- None -") ? null : Level;
			var Job_Title = results[res].getValue({
				name: "title",
				//	summary: "GROUP",
				label: "Job Title"
			});
			Job_Title = (Job_Title == "- None -") ? null : Job_Title;
			var Practice = results[res].getText({
				name: "departmentnohierarchy",
				//	summary: "GROUP",
				label: "Practice (no hierarchy)"
			}); 
			Practice = (Practice == "- None -") ? null : Practice;
			var Work_City = results[res].getValue({
				name: "custentity_workcity",
				//	summary: "GROUP",
				label: "Work City"
			});
			Work_City = (Work_City == "- None -") ? null : Work_City;
			var Function = results[res].getText({
				name: "custentity_emp_function",
				//	summary: "GROUP",
				label: "Function"
			});
			Function = (Function == "- None -") ? null : Function;

			var Hire_Type = results[res].getValue({
				name: "custentity_fresher",
				//	summary: "GROUP",
				label: "Fresher"
			});
			Hire_Type = (Hire_Type == "- None -") ? null : Hire_Type;
			var Sub_Practice = results[res].getText({
				name: "custrecord_parent_practice",
				join: "department",
				//	summary: "GROUP",
				label: "Parent Practice"
			});
			Sub_Practice = (Sub_Practice == "- None -") ? null : Sub_Practice;
			var Future_Term_Date = results[res].getValue({
				name: "custentity_future_term_date",
				//	summary: "GROUP",
				label: "Future Term Date"
			});
			Future_Term_Date = (Future_Term_Date == "- None -") ? null : Future_Term_Date;
			var Last_Working_Date = results[res].getValue({
				name: "custentity_lwd",
				//	summary: "GROUP",
				label: "Last Working Date"
			});
			Last_Working_Date = (Last_Working_Date == "- None -") ? null : Last_Working_Date;
			var Fusion_Employee_Id = results[res].getValue({
				name: "custentity_fusion_empid",
				//	summary: "GROUP",
				label: "Fusion Employee Id"
			});
			Fusion_Employee_Id = (Fusion_Employee_Id == "- None -") ? null : Fusion_Employee_Id;
			/*var ff = JSON.stringify(results[res]);
			var asdas = JSON.parse(ff);
			var Q1 = asdas.values["GROUP(formulatext)"];
			var Q2 = asdas.values["GROUP(formulatext)_1"];*/

			var Subsidiary = results[res].getValue({
				name: "subsidiarynohierarchy",
				//	summary: "GROUP",
				label: "Subsidiary"
			});
			Subsidiary = (Subsidiary == "- None -") ? null : Subsidiary;
			var Employee_Id = results[res].getValue({
				name: "formulatext1",
				//	summary: "GROUP",
				formula: "Concat({firstname}, concat(concat(' ', {middlename}), concat(' ', {lastname})))",
				label: "Employee Id"
			});
			Employee_Id = (Employee_Id == "- None -") ? null : Employee_Id;


			var reporting_manager_email = results[res].getValue({
				name: "email",
				join: "CUSTENTITY_REPORTINGMANAGER",
				//	summary: "GROUP",
				label: "Email"
			})
			reporting_manager_email = (reporting_manager_email == "- None -") ? null : reporting_manager_email;					  
			var hrbp_email =  results[res].getValue({
				name: "email",
				join: "CUSTENTITY_EMP_HRBP",
				//	summary: "GROUP",
				label: "Email"
			});
			hrbp_email = (hrbp_email == "- None -") ? null : hrbp_email;

			var visa_type =  results[res].getValue({
				name: "custrecord_visa",
				join: "CUSTRECORD_VISAID",
				//	summary: "GROUP",
				label: "Visa Type"
			});
			visa_type = (visa_type == "- None -") ? null : visa_type;
			//	 log.debug('visa_type',visa_type);
			var H1B_transfer = "No"; 
			if(visa_type == "21"){ 

				var visa_valid_till = results[res].getValue({
					name: "custrecord_validtill",
					join: "CUSTRECORD_VISAID",
					//	summary: "MAX",
					label: "Visa Valid till"
				});
				visa_valid_till = (visa_valid_till == "- None -") ? null : visa_valid_till;

				if(_logValidation(visa_valid_till)){
					var today_date = toDate.split(" ")[0];

					visa_valid_till = new Date(visa_valid_till);
					today_date = new Date(today_date);

					if(today_date <= visa_valid_till){
						H1B_transfer = "Yes";
					}
				}
			}


			var flag = results[res].getValue({
				name: "formulatext",
				//	summary: "MAX",
				formula: "CASE WHEN {systemnotes.oldvalue} IS NULL THEN 'Insert' WHEN {systemnotes.newvalue} IS NULL THEN 'Revoke' else 'Update' end",
				label: "Formula (Text)"
			});
			flag = (flag == "- None -") ? null : flag;
			//"Hire_Type":Hire_Type, commented because it's needed as of now

			var firstname = results[res].getValue({name: "firstname", label: "First Name"});
			var middlename = results[res].getValue({name: "middlename", label: "Middle Name"});
			var lastname = results[res].getValue({name: "lastname", label: "Last Name"});
			var fullname = firstname+' '+middlename+' '+lastname;


			if(tempArr.indexOf(internalID) == -1)
			{

				//	log.debug("Subsidiary",Subsidiary);
				ActualData.push({
					"Employee_Id":Fusion_Employee_Id,
					"Email":email,
					"Display_Name":fullname.trim(),
					"Base_Location":Brillio_Location,
					"Offered_Type":Person_Type?Person_Type.trim():Person_Type,
							"H1B_transfer":H1B_transfer,
							"HR_Coordinator": hrbp_email  ,
							"ManagerMail" : reporting_manager_email,
							"Legal_Entity":Legal_Entity,
							"Levels":Level,
							"Roles":Job_Title,
							"Practice":Sub_Practice,
							"Work_Location":Work_City,
							"Function":Function,

							"Department":Practice,
							"Future_Term_Date":Future_Term_Date,
							"Last_Working_Date":Last_Working_Date,
							"BU":SubMapping(Subsidiary),
							"flag":flag
				});
				tempArr.push(internalID);
			}
			else
			{
				continue;
			}
		}
		return ActualData;

	}

	function SubMapping(name)
	{
		if (name =='3')
		{
			return "Brillio Technologies India BU"
		}
		else if(name == '2')
		{
			return "Brillio LLC United States BU"
		}
		else if(name == '10')
		{
			return "Brillio Canada Inc BU"
		}
		else if(name == '7')
		{
			return "BRILLIO UK LIMITED BU"
		}
		else if(name == '21')
		{
			return "Cognetik Corp"
		}else
		{
			return null;
		}
	}
	function _logValidation(value) {
		if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
			return true;
		} else {
			return false;
		}
	}

	return {
		'post': sendOffboardingData        
	};
});