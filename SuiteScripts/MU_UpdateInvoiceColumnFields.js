/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Dec 2014     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	
	var record = nlapiLoadRecord(recType, recId);
	
	var i_billable_time_count = record.getLineItemCount('time');
	
	var customer = record.getFieldText('entity');
	
	var project = record.getFieldText('job');
	
	nlapiLogExecution('AUDIT', nlapiGetRecordId(), 'Project: ' + project + ', Customer: ' + customer);
	
	var a_employee_names = new Object();
	
	for (var i = 1; i <= i_billable_time_count; i++)
	{
		var isApply = record.getLineItemValue('time','apply',i);
		
		var item	=	record.getLineItemValue('time','item',i);
		
		if(isApply == 'T' && (item == '2221' || item == '2222' || item == '2425'))
			{
				var employeeId = record.getLineItemValue('time', 'employee', i);
				if(a_employee_names[employeeId] == undefined)
					{
						a_employee_names[employeeId] = nlapiLookupField('employee', employeeId, 'entityid');
					}
				var employeeName = a_employee_names[employeeId];//
				
				record.setLineItemValue('time', 'custcol_employeenamecolumn', i, employeeName); // Set Employee
				record.setLineItemValue('time', 'custcolprj_name', i, project); // Set Project
				record.setLineItemValue('time', 'custcolcustcol_temp_customer', i, customer); // Set Customer
			}
	}
	
	nlapiSubmitRecord(record, true, true);
}
