// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:Ues etds disable edit button
	Author:Nikhil jain
	Company:Aashna cloudtech Pvt. Ltd.
	Date:13/03/2015
	Description: Disable the edit button on quarterly detail record


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_VenStatDisable_EditBut(type,form)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
	//if (type == 'View') 
	{
		var recordType = nlapiGetRecordType()
		//nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " recordType " + recordType)
		
		var recordid = nlapiGetRecordId()
		//nlapiLogExecution('DEBUG', 'afterSubmitRecord check ', "record_id" + recordid)
		
		if (recordid != '' && recordid != null && recordid != undefined) 
		{
		
			var record_obj = nlapiLoadRecord(recordType, recordid)
			
			if (record_obj != '' && record_obj != null && record_obj != undefined) {
				form.setScript('customscript_cli_vendorstatement');
				form.addButton('custpage_refresh', 'Refresh', 'refresh()');
				
				var status = record_obj.getFieldValue('custrecord_ait_venstat_status')
				
				if (status == 'In-Progress') {
					var edit_btn = form.getButton('edit')
					
					if (edit_btn != null) {
						edit_btn.setDisabled(true)
					}
				}
			}
		}
	}
	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
