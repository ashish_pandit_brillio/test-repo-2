/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 May 2017     deepak.srinivas
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	var response = new Response();
	try{
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		nlapiLogExecution('debug', 'current_date', current_date);
		
		var requestType = dataIn.RequestType;
		
		var currentDate_Time = date_time();
        var receivedDate_Time = dataIn.Data ? dataIn.Data.Date : '';
		if(!_logValidation(receivedDate_Time)){
			throw 'Date Cannot be blank!!';
			return false;
		}
		switch (requestType) {

		case M_Constants.Request.Get:

			
				response.Data = getAllActiveEmployeeName(current_date,receivedDate_Time);
				response.Status = true;
		}
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(response));
	return response;
}
	
function getAllActiveEmployeeName(s_currentDate,s_receivedDate) {
		try {
			var d_currentDate = nlapiStringToDate(s_currentDate);
			var d_receivedDate = nlapiStringToDate(s_receivedDate);
			var employeeList = [];
			var emp_id ='';
			var emp_type ='';
			var person_type ='';
			var emp_fusion_id ='';
			var emp_frst_name ='';
			var emp_middl_name ='';
			var emp_lst_name ='';
			var emp_full_name ='';
			var emp_id ='';
			var emp_id ='';
			
			//Manager
			var mgr_fusion_id ='';
			var mgr_frst_name ='';
			var mgr_middl_name ='';
			var mgr_lst_name ='';
			var mgr_full_name ='';
			var mgr_email ='';
			
			
			var cols = [];
			cols.push(new nlobjSearchColumn('firstname'));
			cols.push(new nlobjSearchColumn('middlename'));
			cols.push(new nlobjSearchColumn('lastname'));
			cols.push(new nlobjSearchColumn('custentity_fusion_empid'));
			cols.push(new nlobjSearchColumn('department'));
			cols.push(new nlobjSearchColumn('title'));
			cols.push(new nlobjSearchColumn('custentity_employeetype'));
			cols.push(new nlobjSearchColumn('custentity_persontype'));
			cols.push(new nlobjSearchColumn('email')); //custentity_emp_attendance_tracking
			cols.push(new nlobjSearchColumn('custentity_employee_inactive'));
			cols.push(new nlobjSearchColumn('custentity_emp_attendance_tracking'));
			cols.push(new nlobjSearchColumn('gender'));
			cols.push(new nlobjSearchColumn('employeetype'));
			cols.push(new nlobjSearchColumn('custentity_employee_inactive'));
			cols.push(new nlobjSearchColumn('email','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('custentity_fusion_empid','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('firstname','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('middlename','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('lastname','custentity_reportingmanager'));
			cols.push(new nlobjSearchColumn('subsidiary'));
			cols.push(new nlobjSearchColumn('custentity_lwd'));
			cols.push(new nlobjSearchColumn('employeestatus'));
			cols.push(new nlobjSearchColumn('phone'));
			cols.push(new nlobjSearchColumn('hiredate'));
			//Absence details:
			cols.push(new nlobjSearchColumn('custentity_personid'));
			cols.push(new nlobjSearchColumn('custentity_employer_id'));
			cols.push(new nlobjSearchColumn('custentity_assignment_num'));
			
			//Brillio Location
			cols.push(new nlobjSearchColumn('custentity_list_brillio_location_e'));
			
			
			var employeeSearch = searchRecord('employee', null, [
			        new nlobjSearchFilter('lastmodifieddate', null,
			                'within', d_receivedDate,d_currentDate)], cols);
					
			var dataRow = [];
			var JSON = {};
			if (employeeSearch) {
					for(var i=0;i<employeeSearch.length;i++){
					    emp_id = employeeSearch[i].getId();
						emp_type = employeeSearch[i].getText('custentity_employeetype');
					    person_type = employeeSearch[i].getText('custentity_persontype');
						emp_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid');
						emp_frst_name = employeeSearch[i].getValue('firstname');
						emp_middl_name = employeeSearch[i].getValue('middlename');
						emp_lst_name = employeeSearch[i].getValue('lastname');
						
						
						
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
							
						if(emp_middl_name)
							emp_full_name = emp_full_name + ' ' + emp_middl_name;
							
						if(emp_lst_name)
							emp_full_name = emp_full_name + ' ' + emp_lst_name;
						
						//manager
						mgr_fusion_id = employeeSearch[i].getValue('custentity_fusion_empid','custentity_reportingmanager');
						mgr_frst_name = employeeSearch[i].getValue('firstname','custentity_reportingmanager');
						mgr_middl_name = employeeSearch[i].getValue('middlename','custentity_reportingmanager');
						mgr_lst_name = employeeSearch[i].getValue('lastname','custentity_reportingmanager');
						
						if(mgr_frst_name)
							mgr_full_name = mgr_frst_name;
							
						if(mgr_middl_name)
							mgr_full_name = mgr_full_name + ' ' + mgr_middl_name;
							
						if(mgr_lst_name)
							mgr_full_name = mgr_full_name + ' ' + mgr_lst_name;
						
						
						
						JSON = {
								ID: emp_id,
								EmployeeID:emp_fusion_id,
								Emp_Display_Name: emp_full_name,
								EmployeeType: emp_type,
								PersonType: person_type,
								Designation: employeeSearch[i].getValue('title'),
								FirstName: employeeSearch[i].getValue('firstname'),
								MiddleName: employeeSearch[i].getValue('middlename'),
								LastName: employeeSearch[i].getValue('lastname'),
								Department: employeeSearch[i].getText('department'),
								LegalEntity: employeeSearch[i].getText('subsidiary'),
								Employee_InActive: employeeSearch[i].getValue('custentity_employee_inactive'),
								WorkEmail: employeeSearch[i].getValue('email'),
								Emp_Level: employeeSearch[i].getText('employeestatus'),
								AttendanceTracking: employeeSearch[i].getText('custentity_emp_attendance_tracking'),
								Phone: employeeSearch[i].getValue('phone'),
								HireDate: employeeSearch[i].getValue('hiredate'),
								TerminationDate: employeeSearch[i].getValue('custentity_lwd'),
								Gender: employeeSearch[i].getValue('gender'),
								Hourly_Salaried: employeeSearch[i].getText('employeetype'),
								ManagerEmail: employeeSearch[i].getValue('email','custentity_reportingmanager'),
								ManagerPersonNumber: employeeSearch[i].getValue('custentity_fusion_empid','custentity_reportingmanager'),
								//JSON:
								PersonID: employeeSearch[i].getValue('custentity_personid'),
								EmployerId: employeeSearch[i].getValue('custentity_employer_id'),
								AssignmentNumber: employeeSearch[i].getValue('custentity_assignment_num'),
								ManagerDisplayName: mgr_full_name,
								//Location
								BrillioLocation: employeeSearch[i].getValue('custentity_list_brillio_location_e')
								
						
								
						};
						dataRow.push(JSON);
						
					}
					
				
			}

			return dataRow;
		} catch (err) {
			nlapiLogExecution('error', 'getAllActiveEmployeeName', err);
			throw err;
		}
	}
function date_time() {
    var date = new Date();
    var date_ = nlapiDateToString(date);
    var am_pm = "am";
    var hour = date.getHours();

    var currentDateTime = new Date();
    var companyTimeZone = nlapiLoadConfiguration('companyinformation').getFieldText('timezone');
    var timeZoneOffSet = (companyTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(companyTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(/:30/gi, '.5'));
    var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
    var companyDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);

    if (hour >= 12) {
        am_pm = "pm";
    }
    if (hour > 12) {
        hour = hour - 12;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }

    var minute = date.getMinutes();
    if (minute < 10) {
        minute = "0" + minute;
    }
    var sec = date.getSeconds();
    if (sec < 10) {
        sec = "0" + sec;
    }

    return date_ + ' ' + hour + ':' + minute + ' ' + am_pm;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}