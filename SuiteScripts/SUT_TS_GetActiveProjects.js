/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Dec 2014     amol.sahijwani
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	var i_employee_id = request.getParameter('employee_id');
	var week_end_date = request.getParameter('week_end_date');
	var week_start_date = request.getParameter('week_start_date');
	
	week_end_date = nlapiDateToString(nlapiStringToDate(week_end_date, 'date'), 'date');
	week_start_date = nlapiDateToString(nlapiStringToDate(week_start_date, 'date'), 'date');
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
	filters[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', week_end_date);
	filters[2] = new nlobjSearchFilter('enddate', null, 'onorafter', week_start_date);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('company');
	columns[1] = new nlobjSearchColumn('startdate');
	columns[2] = new nlobjSearchColumn('enddate');
	columns[3] = new nlobjSearchColumn('internalid');
	
	var search_results = nlapiSearchRecord('resourceallocation', null, filters, columns);
	
	var a_result = new Array();
	// Get TimeZone
	var o_config = nlapiLoadConfiguration('userpreferences');
	var timezone = o_config.getFieldValue('timezone');
	
	nlapiLogExecution('DEBUG', 'Time Zone', timezone);
	
	if(search_results != null)
		{
			var count = search_results.length;
			
			for(var i = 0; i < count; i++)
				{
					var o_search_result = search_results[i];
					var o_allocation = new Object();
					
					o_allocation.project = o_search_result.getValue('company');
					
					var o_res_allocation = nlapiLoadRecord('resourceallocation', o_search_result.getValue('internalid'));
					
					o_allocation.start_date = o_res_allocation.getFieldValue('startdate');//nlapiStringToDate(o_search_result.getValue('startdate'), 'date');
					o_allocation.end_date = o_res_allocation.getFieldValue('enddate');//nlapiStringToDate(o_search_result.getValue('enddate'), 'date');
					nlapiLogExecution('DEBUG', 'Date', timezone);
					a_result.push(o_allocation);
				}
		}
	
	response.write(JSON.stringify(a_result));
}
