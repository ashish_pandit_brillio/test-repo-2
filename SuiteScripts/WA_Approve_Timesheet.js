/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Mar 2015     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	nlapiLogExecution('AUDIT', 'TimeSheet ID: ', nlapiGetRecordId());
	// Load the Time Sheet Record
	var recTS = nlapiLoadRecord('timesheet', nlapiGetRecordId(), {recordmode: 'dynamic'});

	// Array of Subrecord names
	var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

	// Get Line Count
	var i_line_count = recTS.getLineItemCount('timegrid');//recTS.getLineItemCount('timegrid');

	for(var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++)
		{
			
			//nlapiLogExecution('AUDIT', 'Test', JSON.stringify(a_days_data));		
						
			recTS.selectLineItem('timegrid',i_line_indx);
			for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var o_sub_record_view = recTS.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
					if(o_sub_record_view)
						{
							var o_sub_record = recTS.editCurrentLineItemSubrecord('timegrid', sub_record_name);
							o_sub_record.setFieldValue('approvalstatus', 3);
							o_sub_record.commit();
						}

				}
			recTS.commitLineItem('timegrid');
		}
	nlapiSubmitRecord(recTS);
}
