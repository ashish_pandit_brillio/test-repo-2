	/*	Script Name:    UES T&M Revenue Creation 
	 Author:        Sai Saranya
	 Company:		Brillio
	 Date:
	 Version:
	 
	 Description:	This script Call Schedule Script to Create a new record for revenue amount for T&M projects.
	 * Script Modification Log:
	 * 
	 -- Date --		-- Modified By --			--Requested By--				-- Description --
	 11 June 2018         Supriya                    Deepak                  
	 05 Feb 2020		Praveena Madem				Deepak						_BRS_delete_forecastmaster_data function to delete forecase master data while deleting resource allocation.
    Below is a summary of the process controls enforced by this script file.  The control logic is described
	 more fully, below, in the appropriate function headers and code blocks.
	 
	 BEFORE LOAD
	 - beforeLoadRecord(type)
	 Not Used
	 
	 BEFORE SUBMIT
	 - beforeSubmitRecord(type)
	 Not Used
	 
	 AFTER SUBMIT
	 - afterSubmitRecord(type)
	 afterSubmitRecord(type)
	 
	 SUB-FUNCTIONS
	 - The following sub-functions are called by the above core functions in order to maintain code
	 modularization:
	
	 */
	function beforeLoadRecord(type, form) {

	    return true;

	}
	// END BEFORE LOAD ====================================================
	// BEGIN BEFORE SUBMIT ================================================

	function beforeSubmitRecord_ait_adjustTDS_Amt(type) {

	    return true;
	}

	// END BEFORE SUBMIT ==============================================

	// BEGIN AFTER SUBMIT =============================================
	function afterSubmitRecord(type) {
	    //if ((type == 'create') || (type == 'edit') || (type == 'delete')) {
	    //
	     nlapiLogExecution('DEBUG', 'Resource alloc');
			if ((type == 'create') || (type == 'edit')){
	        var project_array = new Array()
	        var id = nlapiGetRecordId();

	        var project_id = nlapiGetFieldValue('project');
	        var i_resource_id = nlapiGetFieldValue('allocationresource');
	        var s_resource_startdate = nlapiGetFieldValue('startdate');
	        var s_resource_enddate = nlapiGetFieldValue('enddate');

	        var billable = nlapiGetFieldValue('custeventrbillable');
	        var project_details = nlapiLookupField('job', project_id, ['jobbillingtype', 'jobtype', 'customer']);
	        var project_type = project_details.jobbillingtype;
	        var internal_exterbal = project_details.jobtype;
             var check_oldprojecttype=false;
              
              // added on 03-11-2020 to Check the old project type
              if(type=='edit'){
                var old = nlapiGetOldRecord();
                var newrec=nlapiGetNewRecord();
                var old_project = old.getFieldValue('project');
                var new_project = newrec.getFieldValue('project');
                var old_projecttype=nlapiLookupField('job', old_project, ['jobbillingtype']);
                check_oldprojecttype=old_projecttype['jobbillingtype']='TM' && old_project!=new_project  ? true : false;
              }
	       // if (billable == 'T' && project_type == 'TM') {
	       //check_oldprojecttype==true Added logic to allow to delete forecast master data for the old project when old project is belongs T&M on 03-11-2020
	     if (billable == 'T' && (project_type == 'TM' ||  check_oldprojecttype==true)) {
				project_array['custscript_ra_project'] = project_id ;
				project_array['custscript_ra_resource'] = i_resource_id ;
				project_array['custscript_ra_startdate'] = s_resource_startdate ;
				project_array['custscript_ra_enddate'] = s_resource_enddate ;
				project_array['custscript_ra_customer'] = project_details['customer'];
                project_array['custscript_ra_internalid'] = nlapiGetRecordId();//Added on 02-11-2020
            
		 nlapiLogExecution('DEBUG', 'Resource allocation record field details', "resource allocation id=" + id + " : project_id=" + project_id + " : i_resource_id=" + i_resource_id);
	            try {
					nlapiLogExecution('DEBUG', 'Call Sch',new Date());
	             // var status = nlapiScheduleScript('customscript_forecast_creation_ra',null,project_array1);
				  //var status = nlapiScheduleScript('customscript_sch_tm_forecase_master_crea',null,project_array);
                  
                   var url = nlapiResolveURL('SUITELET', 'customscript_sut_tm_forecastmastercreate', 'customdeploy1',true);
	                url += '&custscript_ra_project=' + project_id + '&custscript_ra_resource=' + i_resource_id + '&custscript_ra_startdate=' + s_resource_startdate + '&custscript_ra_enddate=' + s_resource_enddate + '&custscript_ra_customer=' + project_details['customer']+ '&custscript_ra_internalid=' + nlapiGetRecordId();
	                nlapiLogExecution('DEBUG','url',url);
	            nlapiRequestURL(url, null, null, 'GET');
	              //  nlapiLogExecution('DEBUG', 'END SUITELET', new Date());
                  
	              nlapiLogExecution('DEBUG', 'END Call Sch',new Date());
	            } catch (e) {
	                nlapiLogExecution('DEBUG', 'ERROR IN CALLING Suitelet', e);
	            }
	        }

	    }


	} // afterSubmitRecord(type)	


	function _BRS_delete_forecastmaster_data() {
      nlapiLogExecution('DEBUG','type',type);
	    if (type == 'delete') {
	        try {
				nlapiLogExecution('DEBUG','type',type);
              var context = nlapiGetContext();
              var old = nlapiGetOldRecord();
               
               
               var project_id = old.getFieldValue('project');
              var i_resource_id =old.getFieldValue('allocationresource');
               var billable = old.getFieldValue('custeventrbillable');
              
           /*   var project_id = nlapiGetFieldValue('project');
              var i_resource_id = nlapiGetFieldValue('allocationresource');
              var s_allocationStartDate = nlapiGetFieldValue('startdate');
              var s_allocationEndDate = nlapiGetFieldValue('enddate');
              var billable = nlapiGetFieldValue('custeventrbillable');
              */
             
              var i_internalid=nlapiGetRecordId();
			nlapiLogExecution('DEBUG','project_id',project_id);
			nlapiLogExecution('DEBUG','i_resource_id',i_resource_id);
			nlapiLogExecution('DEBUG','i_internalid',i_internalid);
              var project_details = nlapiLookupField('job', project_id, ['jobbillingtype', 'jobtype', 'customer']);
               var project_type = project_details.jobbillingtype;
              var internal_exterbal = project_details.jobtype;
              
	            if (billable == 'T' && project_type == 'TM') {
				
	                if (!project_id || !i_resource_id)
	                    return;
					
						var forecast_fil = new Array();
						forecast_fil[0] = new nlobjSearchFilter('custrecord_forecast_project_name', null, 'anyof', project_id);
						forecast_fil[1] = new nlobjSearchFilter('custrecord_forecast_employee', null, 'anyof', i_resource_id); //ADDED EMPLOYEE as Filter on 03.02.2020
						//forecast_fil[2] = new nlobjSearchFilter('custrecord_forecast_ra_id', null, 'anyof', i_internalid);
						forecast_fil[2] = new nlobjSearchFilter('custrecord_forecast_ra_id', null, 'equalto', parseInt(i_internalid));
                  nlapiLogExecution('DEBUG','i_internalid',i_internalid);
						var record_del = searchRecord('customrecord_forecast_master_data', null, forecast_fil, [new nlobjSearchColumn('internalid'),new nlobjSearchColumn('custrecord_forecast_month_startdate'),new nlobjSearchColumn('custrecord_forecast_month_enddate'),new nlobjSearchColumn('custrecord39'),new nlobjSearchColumn('custrecord_forecast_ra_id')]);
						if (record_del) {
							nlapiLogExecution('DEBUG', 'Records are deleted Length', record_del.length);
							for (var del_ind = 0; del_ind < record_del.length; del_ind++) {
									//DELETE FORECASTING MASTER DATA
								 nlapiDeleteRecord('customrecord_forecast_master_data', record_del[del_ind].getValue('internalid'));
								
							}
							// nlapiLogExecution('DEBUG', 'Records are deleted');
						}
	                nlapiLogExecution('DEBUG', 'Records are deleted');
	            }
	        } catch (e) {
				throw e;
	            nlapiLogExecution('DEBUG', 'error while deleting forecase master data', e);
	        }

	    }
	}
	
	
	
	
	function getWorkingDays(startDate, endDate) {
    try {
        var d_startDate = nlapiStringToDate(startDate);
        var d_endDate = nlapiStringToDate(endDate);
        var numberOfWorkingDays = 0;

        for (var i = 0;; i++) {
            var currentDate = nlapiAddDays(d_startDate, i);

            if (currentDate > d_endDate) {
                break;
            }

            if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
                numberOfWorkingDays += 1;
            }
        }

        return numberOfWorkingDays;
    } catch (err) {
        nlapiLogExecution('error', 'getWorkingDays', err);
        throw err;
    }
}

function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}



function get_holidays(start_date, end_date, employee, project, customer) {

	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');
	var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date, emp_subsidiary);
	} else {
		return get_customer_holidays(start_date, end_date, emp_subsidiary,
		        customer);
	}
}

function get_company_holidays(start_date, end_date, subsidiary) {
	var holiday_list = [];

	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	// nlapiLogExecution('debug', 'start_date', start_date);
	// nlapiLogExecution('debug', 'end_date', end_date);
//	 nlapiLogExecution('debug', 'subsidiary', subsidiary);

	var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
	        'customsearch_company_holiday_search', [
	                new nlobjSearchFilter('custrecord_date', null, 'within',
	                        start_date, end_date),
	                new nlobjSearchFilter('custrecordsubsidiary', null,
	                        'anyof', subsidiary) ], [ new nlobjSearchColumn(
	                'custrecord_date') ]);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			holiday_list.push(search_company_holiday[i]
			        .getValue('custrecord_date'));
		}
	}

	return holiday_list;
}

function get_customer_holidays(start_date, end_date, subsidiary, customer) {
	var holiday_list = [];

	//start_date = nlapiDateToString(start_date, 'date');
	//end_date = nlapiDateToString(end_date, 'date');

	 nlapiLogExecution('debug', 'start_date', start_date);
	 nlapiLogExecution('debug', 'end_date', end_date);
	 nlapiLogExecution('debug', 'subsidiary', subsidiary);
	 nlapiLogExecution('debug', 'customer', customer);

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecordcustomersubsidiary', null,
	                        'anyof', subsidiary),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [ new nlobjSearchColumn(
	                'custrecordholidaydate', null, 'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			holiday_list.push(search_customer_holiday[i].getValue(
			        'custrecordholidaydate', null, 'group'));
		}
	}

	return holiday_list;
}