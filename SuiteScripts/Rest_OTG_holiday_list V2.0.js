
var monthArray = [' ','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
function postRESTlet_holiday(dataIn) {
	var response = new Response();
	var current_date = nlapiDateToString(new Date());
	//Log for current date
	nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
	
	//nlapiLogExecution('debug', 'datain', dataIn);
	
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	nlapiLogExecution('debug', 'Email id', dataIn.EmailId);
	//var emailId  = 'arjun.r@brillio.com';
	var response = new Response();
	var currentDate_ = nlapiStringToDate(current_date);
	var currentDate = currentDate_.getDate();
	var currentMonth = currentDate_.getMonth();
	var currentYear = currentDate_.getFullYear();
	nlapiLogExecution('debug', 'currentYear', currentYear);
	try {
	//var employeeId = getUserUsingEmailId(emailId);
	
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var flag_R = false;
		var flag_projectManager = false;
		//Search If logged user is reporting manager or not
		var filter = [];
		filter[0] = new nlobjSearchFilter('custentity_reportingmanager',null,'anyof',employeeId); //custentity_employee_inactive
		filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
		var col = [];
		col[0] = new nlobjSearchColumn('internalid');
		col[1] = new nlobjSearchColumn('entityid');
		col[2] = new nlobjSearchColumn('firstname');
		
		var reportingManager_Res = nlapiSearchRecord('employee',null,filter,col);
		if(reportingManager_Res){
			nlapiLogExecution('debug', 'Reportee Count', reportingManager_Res.length);
			if(reportingManager_Res.length > 0){
				flag_R = true;
			}
		}
		
		//Check if the logged in user is PM or not --- CODEX
		var statusList = [2,4]; //Active && Pending
		var filter_P = [];
		filter_P[0] = new nlobjSearchFilter('custentity_projectmanager',null,'anyof',employeeId); //custentity_employee_inactive
		filter_P[1] = new nlobjSearchFilter('status',null,'anyof',statusList); //jobtype
		filter_P[2] = new nlobjSearchFilter('jobtype',null,'anyof',2); 
		filter_P[3] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		
		var col_P = [];
		col_P[0] = new nlobjSearchColumn('internalid');
		col_P[1] = new nlobjSearchColumn('entityid');
		col_P[2] = new nlobjSearchColumn('custentity_projectmanager');
		
		
		var project_Manager_Res = nlapiSearchRecord('job',null,filter_P,col_P);
		if(project_Manager_Res){
			nlapiLogExecution('debug', 'PM Flag Count', project_Manager_Res.length);
			if(project_Manager_Res.length > 0){
				flag_projectManager = true;
			}
		}
		
		var emp_lookUp_Obj = nlapiLookupField('employee',parseInt(employeeId),['subsidiary','department','hiredate','entityid']);
		var i_subsidiary_V = emp_lookUp_Obj.subsidiary;
		var i_department_V = emp_lookUp_Obj.department;
		var i_hiredate_V = emp_lookUp_Obj.hiredate;
		var logged_user = emp_lookUp_Obj.entityid;
		nlapiLogExecution('debug','subsidiary',i_subsidiary_V);
		
		var JSON_hireDate = {};
		var dataRow_HireDate = [];
		
		JSON_hireDate = {
				LoggedUser : logged_user,
				HireDate : i_hiredate_V
		};
		dataRow_HireDate.push(JSON_hireDate);
		
		
		var employeeLocation = nlapiLookupField('employee', parseInt(employeeId),
        'location');

		var locationArray = [ employeeLocation ];
		var currentLocation = employeeLocation;

		while (currentLocation && parseInt(i_subsidiary_V) != parseInt(18)) {
		var locationRec = nlapiLoadRecord('location', currentLocation);
		currentLocation = locationRec.getFieldValue('parent');
	
		if (currentLocation) {
			locationArray.push(currentLocation);
		}
	}

		nlapiLogExecution('debug', 'location array', JSON
        .stringify(locationArray));
		
		var dataRow_Holiday = [];
		var holiday_list_ = [];
		var JSON_Holiday = {};
		var counter_H = 0;
		//var searchHolidayList = nlapiSearchRecord('customrecord_holiday',null,filters,cols);
		
		var searchHolidayList = nlapiSearchRecord(
		        'customrecord_mobile_holiday_calendar', null, [
		                new nlobjSearchFilter('custrecord_mhc_location', null,
		                        'anyof', locationArray),
		                new nlobjSearchFilter('custrecord_mhc_date', null,
		                        'onorafter', 'startofthisyear'),
		                new nlobjSearchFilter('custrecord_mhc_date', null,
		                        'onorbefore', 'nextfiscalyear'),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('custrecord_mhc_date').setSort(),
		                new nlobjSearchColumn('custrecord_mhc_title'),
		                new nlobjSearchColumn('custrecord_mhc_image') ]);
		
		if(searchHolidayList){
			for(var indx=0; indx < searchHolidayList.length; indx++){
				var title = searchHolidayList[indx].getValue('custrecord_mhc_title');
				var holiday_date_ = searchHolidayList[indx].getValue('custrecord_mhc_date');
				var holiday_date = nlapiStringToDate(holiday_date_);
				var h_date = holiday_date.getDate();
				var h_month = holiday_date.getMonth() + 1;
				h_month = monthArray[h_month];
				var h_year = holiday_date.getFullYear();
				//if(counter_H < 3 && holiday_list_.indexOf(title)<0)
				{ //currentMonth >=b_month &&
					counter_H ++;
					JSON_Holiday = {
							Holiday_Desc : searchHolidayList[indx].getValue('custrecord_mhc_title'),
							Holiday_Date : h_date,
							IN:'',
							Holiday_Month: h_month,
							Holiday_Year: h_year,
							HolidayDate: holiday_date_
					};
					holiday_list_.push(searchHolidayList[indx].getValue('custrecord_mhc_title'));
					dataRow_Holiday.push(JSON_Holiday);
				}
			}
		}
		//Removing Duplicates from Holiday
		dataRow_Holiday = removearrayduplicate(dataRow_Holiday);
		
		    var first_JSON = {};
			var first_Array = [];
			
			var second_JSON = {};
			var second_Array = [];
			var final_Array = [];
			
			first_JSON = {
         			
         			HolidayList: dataRow_Holiday,
         			
			};
			first_Array.push(first_JSON);
			
				
				
				
				
				//Check logged user is Expesne Approver
			var flag_E = false;
			var filter = [];
			filter[0] = new nlobjSearchFilter('approver',null,'anyof',employeeId); //custentity_employee_inactive
			filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
			var col = [];
			col[0] = new nlobjSearchColumn('internalid');
			col[1] = new nlobjSearchColumn('entityid');
			col[2] = new nlobjSearchColumn('firstname');
		
			var expenseApprover = nlapiSearchRecord('employee',null,filter,col);
			if(expenseApprover){
			nlapiLogExecution('debug', 'Reportee Count', expenseApprover.length);
			if(expenseApprover.length > 0){
				flag_E = true;
				}
			}
			//Check if logged user is Time Approver
			var flag_T = false;
			var filter = [];
			filter[0] = new nlobjSearchFilter('timeapprover',null,'anyof',employeeId); //custentity_employee_inactive
			filter[1] = new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		
			var col = [];
			col[0] = new nlobjSearchColumn('internalid');
			col[1] = new nlobjSearchColumn('entityid');
			col[2] = new nlobjSearchColumn('firstname');
		
			var timeApprover = nlapiSearchRecord('employee',null,filter,col);
			if(timeApprover){
			nlapiLogExecution('debug', 'Reportee Count', timeApprover.length);
			if(timeApprover.length > 0){
				flag_T = true;
				}
			}
			//Check if logged user is DM/CP/Practice Head
			var flag_P = false;
			var jobSearch = nlapiSearchRecord("job",null,
			[
				[[["custentity_deliverymanager","anyof",employeeId]],
				"OR",
				[["custentity_clientpartner","anyof",employeeId]],
				"OR",
				[["custentity_practice.custrecord_practicehead","anyof",employeeId]],
				"AND",[["custentity_deliverymanager.custentity_employee_inactive","is","F"]],
				"AND",[["custentity_clientpartner.custentity_employee_inactive","is","F"]]]
			], 
			[
				new nlobjSearchColumn("custentity_deliverymanager"), 
				new nlobjSearchColumn("custentity_clientpartner"), 
				new nlobjSearchColumn("custrecord_practicehead","CUSTENTITY_PRACTICE",null)
			]
			);
			if(_logValidation(jobSearch))
			{
				if(jobSearch.length > 0)
				{
					flag_P = true;
				}
			}
			
			var final_JSON = {};
         
			// response
			response.Data = [
			                 	final_JSON = {
			                 			CurrentDate: current_date,
			                 			ProjectManagerFlag: flag_projectManager,
                                  		ReportingManager : flag_R,
			                 			RequestJSON: first_Array,
			                 			
			                 	}
			                	
			                 			
			                 			
			                 			
			                 	
		];
			
			
			nlapiLogExecution('DEBUG','response.Data',JSON.stringify(response.Data));
			response.Status = true;
		 
		
	}
	 catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet_holiday', err);
		response.Data = err;
	
	}
return response;
}

var sort_by = function(field, reverse, primer){

	   var key = primer ? 
	       function(x) {return primer(x[field])} : 
	       function(x) {return x[field]};

	   reverse = !reverse ? 1 : -1;

	   return function (a, b) {
	       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	     } 
	}

function predicateBy(prop){
	   return function(a,b){
	      if( a[prop] > b[prop]){
	          return 1;
	      }else if( a[prop] < b[prop] ){
	          return -1;
	      }
	      return 0;
	   }
	}
//Function to remove duplicates
function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}