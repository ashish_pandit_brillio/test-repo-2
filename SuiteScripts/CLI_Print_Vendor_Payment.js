
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: CLI_Print_Vendor_Payment.js
	Author: Jayesh V Dinde
	Company: Aashna Cloudtech PVT LTD
	Date: 9 May 2016
    Description: This script is used to invoke suitelet to design pdf layout and change label color for button.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
            
           - changeButtonColor()
           - Print_Vendor_Payment

*/
}

// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================


changeButtonColor();
function changeButtonColor()
{
	// Print Button-------------------
	$('#tdbody_custpage_print_vendor_pay').css({ 'color': 'blue', 'font-size': '150%' });
	$('#tdbody_secondarycustpage_print_vendor_pay').css({ 'color': 'blue', 'font-size': '150%' });
}

function Print_Vendor_Payment(rcrd_id,recrd_type)
{
	window.open("/app/site/hosting/scriptlet.nl?script=879&deploy=1&rcrd_id="+rcrd_id+"&recrd_type="+recrd_type);
}