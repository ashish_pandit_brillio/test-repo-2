/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Aug 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function fieldChanged(type, name, linenum){


    if (name == 'custpage_startdate') {
        var s_period = nlapiGetFieldValue('custpage_startdate')
        //alert("S_period=======" + s_period)
        if (s_period != null && s_period != '' && s_period != 'undefined') {
            var date = nlapiStringToDate(s_period)
            //alert("date=======" + date)
            if (date != null && date != '' && date != 'undefined') {
                var day = date.getDay()
                //alert("day=======" + day)
                if (day != 0) {
                    alert("Please select start date as Sunday.")
                    nlapiSetFieldValue('custpage_startdate', '')
                }
            }
        }
        
    }
 
}
