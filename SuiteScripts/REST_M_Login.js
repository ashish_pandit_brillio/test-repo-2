/**
 * RESTlet for authenticating the user credentials
 * 
 * Version Date Author Remarks 1.00 03 Nov 2015 nitish.mishra
 * 
 */

function postRESTlet(dataIn) {
	nlapiLogExecution('debug', 'input', JSON.stringify(dataIn));

	var response = new Response();

	var loggedUser = dataIn.UserName;
	var password = dataIn.Password;

	nlapiLogExecution('debug', 'loggedUser', loggedUser);
	nlapiLogExecution('debug', 'password', password);

	if (loggedUser && password) {

		// get the employee record
		var employeeSearch = nlapiSearchRecord('employee', null, [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custentity_employee_inactive', null,
		                'is', 'F'),
		        new nlobjSearchFilter('email', null, 'is', loggedUser) ],
		        [ new nlobjSearchColumn('custentity_mobility_password') ]);

		if (employeeSearch) {
			var encryptedPassword = nlapiEncrypt(password, 'SHA1');

			if (employeeSearch[0].getValue('custentity_mobility_password') == encryptedPassword) {
				response.Data = {
				    InternalId : employeeSearch[0].getId(),
				    AccessToken : 'Nitish@123'
				};
				response.Status = true;
			} else {
				response.Data = "Incorrect User Name or Password.";
			}
		} else {
			response.Data = "Incorrect User Name or Password.";
		}
	} else {
		response.Data = "Both User Id and Password are mandatory.";
	}

	return response;
}
