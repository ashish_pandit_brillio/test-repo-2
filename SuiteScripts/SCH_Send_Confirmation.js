/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Aug 2016     shruthi.l
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	try{

		var columns = new Array();
		columns[0]=new nlobjSearchColumn('firstname');
		columns[1]=new nlobjSearchColumn('email');
		columns[2]= new nlobjSearchColumn('custentity_actual_hire_date');
		columns[3]= new nlobjSearchColumn('custentity_probationenddate');
		columns[4]=new nlobjSearchColumn('email','custentity_reportingmanager');
		//columns[5]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');
		// Commented by shravan for HRBP integration on 7-dec-2020
		columns[5]=new nlobjSearchColumn('custentity_emp_hrbp');
		/// Added by shravan for HRBP integration on 7-dec-2020
		columns[6]=new nlobjSearchColumn('custentity_extended_probation_date');
		columns[7]=new nlobjSearchColumn('custentity_notconfirmed');

			var emp_search = nlapiSearchRecord('employee', 'customsearch_employee_confirmation_trigg', null,columns);
			//var emp_search = nlapiSearchRecord('employee', 'customsearch_confirmation_trigger_single', null,columns);
			//var hr_bp_id = emp_search[0].getValue('custrecord_hrbusinesspartner','department');
		   var hr_bp_email = null;
		   
		   //if(isNotEmpty(hr_bp_id)){
			//		hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
			//	}
		  

		nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

		if(isNotEmpty(emp_search)){
			
			for(var i=0;i<emp_search.length;i++){
				
				var hr_bp_id = emp_search[i].getValue('custentity_emp_hrbp');
				nlapiLogExecution('debug', 'hr_bp_id', hr_bp_id);
				if(isNotEmpty(hr_bp_id)){
					hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
					
				}
				else{
						hr_bp_email = 'hrbpteam@brillio.com';//'hrb@brillio.com';
					} /// Added by shravan K
				
				sendServiceMails(
					emp_search[i].getValue('firstname'),
					emp_search[i].getValue('email'),
					emp_search[i].getValue('custentity_actual_hire_date'),
					emp_search[i].getValue('custentity_probationenddate'),
					emp_search[i].getValue('email','custentity_reportingmanager'),hr_bp_email,
					emp_search[i].getId());
			}
		}

		}
		catch(err){
		nlapiLogExecution('error', 'Main', err);
		}
		}

		function sendServiceMails(firstName, email,custentity_actual_hire_date,custentity_probationenddate,reporting_mng_email,hr_bp_email,emp_id){
			
			try{
		                //var AttachmentID=32680;
		                //var fileObj = nlapiLoadFile(71339);
				var mailTemplate = serviceTemplate(firstName,custentity_actual_hire_date,custentity_probationenddate);		
				nlapiLogExecution('debug', 'chekpoint',email);
				nlapiLogExecution('debug', 'hr_bp_email==',hr_bp_email);
				nlapiLogExecution('debug', 'emp_id==',emp_id);
				nlapiLogExecution('debug', 'reporting_mng_email==',reporting_mng_email);
				nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,[reporting_mng_email,'japnit.sethi@brillio.com' ,hr_bp_email],null,{entity: emp_id});
				nlapiLogExecution('debug', 'mailTemplate.MailBody==',mailTemplate.MailBody);
			}

			catch(err){
		nlapiLogExecution('error', 'sendServiceMails', err);
		throw err;
		     }
		}

		function serviceTemplate(firstName,custentity_actual_hire_date,custentity_probationenddate) 
		{
		    var htmltext = '';
		    
		    htmltext += '<table border="0" width="100%"><tr>';
		    htmltext += '<td colspan="4" valign="top">';
		    //htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Dear ' + firstName + ',</p>';
		//htmltext += '&nbsp';
		 
		//htmltext += &nbsp;
		htmltext += '<p> Your performance for the period  '+custentity_actual_hire_date+' to '+custentity_probationenddate+' has been assessed and we are pleased to inform you that your employment has been confirmed in Brillio.</p>';
		htmltext += '<p> We wish you the very best in your career with us. We are confident that your continued efforts and commitment will help Brillio tackle exciting challenges ahead and take it to greater heights in the future.</p>';

		htmltext += "<p>Please contact your reporting manager for detailed feedback on your performance.</p>";
		htmltext += '<p>Thanks & Regards,</p>';
		htmltext += '<p>Team HR</p>';
		 
		 
		    htmltext += '</td></tr>';
		    htmltext += '</table>';
		    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
		    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		    htmltext += '<tr>';
		    htmltext += '<td align="right">';
		    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
		    htmltext += '</td>';
		    htmltext += '</tr>';
		    htmltext += '</table>';
		    htmltext += '</body>';
		    htmltext += '</html>';
		 
		    return {
		        MailBody : htmltext,
		        MailSubject : "Employee Confirmation"      
		    };
		}