/**
 * Set the PR Item Status as per the project
 * 
 * Version Date Author Remarks 1.00 25 Feb 2016 Nitish Mishra
 * 
 */

function clientPostSourcing(type, name) {
	try {
		if (type == 'recmachcustrecord_purchaserequest'
		        && name == 'custrecord_project') {
			var projectType = nlapiGetCurrentLineItemValue(type,
			        'custrecord_pritem_job_type');
			var prStatus = nlapiGetCurrentLineItemValue(type,
			        'custrecord_prastatus');

			if (prStatus == 2 || prStatus == 30) {
				var newPrStatus = projectType == 1 ? 2 : 30;
				nlapiSetCurrentLineItemValue(type, 'custrecord_prastatus',
				        newPrStatus);
			}
		}

		return true;
	} catch (err) {
		nlapiLogExecution('ERROR', 'clientPostSourcing', err);
		alert('Some error occured');
	}
}
