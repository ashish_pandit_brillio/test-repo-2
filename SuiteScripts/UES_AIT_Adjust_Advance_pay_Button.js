// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES_AIT_Adjust_Advance_pay_button.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt. Ltd.
	Date:		30 Nov 2015
	Description:display an adjust advance button on vendor bill


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_adjustadvance_Button(type){

	/*  On before load:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	//  BEFORE LOAD CODE BODY
	
	try {
		if (type == 'view') {
			var Flag = 0;
			var a_subisidiary = new Array()
			var s_pass_code;
			
			if (type != 'delete') {
				var billid = nlapiGetRecordId();
				nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill ', " billid " + billid)
				
				var recordType = nlapiGetRecordType()
				nlapiLogExecution('DEBUG', 'beforeSubmitRecord Bill ', " recordType " + recordType)
				
				if (billid != null && billid != '' && billid != undefined && recordType != null && recordType != '' && recordType != undefined) {
					var o_billobj = nlapiLoadRecord(recordType, billid)
					
					var i_AitGlobalRecId = SearchGlobalParameter();
					nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
					
					if (i_AitGlobalRecId != 0) {
						var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
						
						a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
						nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
						
						var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
						nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
						
						var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
						nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
						
						var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
						nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
						
						
					}// end if(i_AitGlobalRecId != 0 )
					var i_subsidiary = o_billobj.getFieldValue('subsidiary')
					nlapiLogExecution('DEBUG', 'Bill ', "i_subsidiary->" + i_subsidiary);
					var i_vendor = o_billobj.getFieldValue('entity')
					nlapiLogExecution('DEBUG', 'Bill ', "i_vendor->" + i_vendor);
					
					var i_currency = o_billobj.getFieldValue('currency')
					nlapiLogExecution('DEBUG', 'Bill ', "i_currency->" + i_currency);
					
					Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
					
					//==================================Begin :-Search to Display an Adjust Advance Button===========================//
					
					if (Flag == 1) {
						var a_bill_filters = new Array();
						a_bill_filters.push(new nlobjSearchFilter('internalid', null, 'anyOf', billid))
						a_bill_filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'))
						a_bill_filters.push(new nlobjSearchFilter('voided', null, 'is', 'F'))
						
						var a_bill_column = new Array();
						
						a_bill_column[0] = new nlobjSearchColumn('internalid')
						
						var o_bill_searchresult = nlapiSearchRecord('vendorbill', 'customsearch_open_bills_to_adjust_advanc', a_bill_filters, a_bill_column)
						
						if (o_bill_searchresult != null && o_bill_searchresult != '' && o_bill_searchresult != undefined) {
						
							var filters = new Array();
							
							var context = nlapiGetContext();
							var i_subcontext = context.getFeature('SUBSIDIARIES');
							nlapiLogExecution('DEBUG', 'Bill ', "i_subcontext->" + i_subcontext);
							if (i_subcontext != false) {
								if (i_subsidiary != null && i_subsidiary != '' && i_subsidiary != undefined) {
									filters.push(new nlobjSearchFilter('subsidiary', null, 'anyOf', i_subsidiary))
								}
							}
							if (i_currency != null && i_currency != '' && i_currency != undefined) {
								filters.push(new nlobjSearchFilter('currency', null, 'anyOf', i_currency))
							}
							filters.push(new nlobjSearchFilter('entity', null, 'is', i_vendor))
							filters.push(new nlobjSearchFilter('custbody_tds_advancepay', null, 'is', 'T'))
							filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'))
							filters.push(new nlobjSearchFilter('mainline', 'custbody_tds_billcredit_ref', 'is', 'T'))
							filters.push(new nlobjSearchFilter('voided', null, 'is', 'F'))
							
							var column = new Array();
							
							column[0] = new nlobjSearchColumn('trandate')
							column[1] = new nlobjSearchColumn('entity')
							
							var s_searchresult = nlapiSearchRecord('check', 'customsearch_ait_adjust_advanvce_pay', filters, column)
							
							if (s_searchresult != null && s_searchresult != '' && s_searchresult != undefined) {
								nlapiLogExecution('DEBUG', 'Bill ', "s_searchresult->" + s_searchresult);
								
								form.setScript('customscript_popup_adjust_advancepayform');
								form.addButton('custpage_adjustadvance', 'Adjust Advance', 'popupAdjustAdvanceTDSform(' + billid + ')');
							}
						}
					}
					//==================================End :- Search to Display an Adjust Advance Button===========================//
				
				}
			}
		}
	} 
	catch (exception) {
		nlapiLogExecution('DEBUG', 'Bill ', "exception->" + exception);
	}
	return true;
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
	

	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
  function SearchGlobalParameter()	
	{
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
		{
            for (var i = 0; i < s_serchResult.length; i++) 
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
    
// END FUNCTION =====================================================
