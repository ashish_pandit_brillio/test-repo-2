//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=368
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Dec 2014     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function sourceColumnsFields(type) {

    try {
		
		var excel_file_obj = '';	
        var err_row_excel = '';	
        var strVar_excel = '';	
         /*----------Added by Koushalya 28/12/2021---------*/
         var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
         var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
         var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
         var onsite_offsite = '';
          /*-------------------------------------------------*/
		
		
        strVar_excel += '<table>';	
        strVar_excel += '	<tr>';	
        strVar_excel += ' <td width="100%">';	
        strVar_excel += '<table width="100%" border="1">';	
        strVar_excel += '	<tr>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';	
        strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';	
        strVar_excel += '	</tr>';
		
        var current_date = nlapiDateToString(new Date());
        //Log for current date
        nlapiLogExecution('DEBUG', 'Execution Started Current Date', 'current_date...' + current_date);
        var context = nlapiGetContext();
        // var invoice_id = context.getSetting('SCRIPT', 'custscript_ra_invoice_id');
        //if(!invoice_id)
        //	return;
	var invoice_id = context.getSetting('SCRIPT', 'custscript_ra_invoice');//3248024;//
	var i_employee=context.getSetting('SCRIPT', 'custscript_ra_employee_id');//274702;//
	if(!invoice_id || !i_employee)
	{
		return;
	}
		
	
        var je_list = new Array();
        var filter = new Array();
       // var a_results_je = searchRecord('transaction', 'customsearch2552', null, null); //customsearch1773
        if (_logValidation(invoice_id)) {
            //for (var counter = 0; counter < a_results_je.length; counter++) 
			{
				var counter=0;
				try{
               // if (je_list.indexOf(invoice_id) >= 0) // && je_list.length > 50)
                {

                }
//				else
	{
                    nlapiLogExecution('audit', 'j:-- ', counter);
                    var usageEnd = context.getRemainingUsage();
                    if (usageEnd < 1000) {
                       // yieldScript(context);
                    }
                    nlapiLogExecution('DEBUG', 'invoice_id', invoice_id);
                    var record = nlapiLoadRecord('invoice', invoice_id); //332853
					 var transactionNum = record.getFieldValue('tranid')
                    var i_billable_time_count = record.getLineItemCount('time');
                    var update_status = record.getFieldValue('custbody_is_je_updated_for_emp_type');

                    var customer = record.getFieldText('entity');
                    var customerobje = nlapiLookupField('customer', record.getFieldValue('entity'), ['custentity_region','entityid','altname']);
					var region_id_cust=customerobje.custentity_region;
                    var project = record.getFieldText('job');
                    var proj_internal_id = record.getFieldValue('job');
                    var proj_rcrd = nlapiLoadRecord('job', proj_internal_id);
                    var billing_type = proj_rcrd.getFieldText('jobbillingtype');
                    var region_id = proj_rcrd.getFieldValue('custentity_region');
                    var project_region = proj_rcrd.getFieldText('custentity_region');
                    var parent_practice = proj_rcrd.getFieldValue('custentity_practice');
                    parent_practice = nlapiLookupField('department', parent_practice, 'custrecord_parent_practice', true);
                    if (!region_id)
                        region_id = region_id_cust;

                    var proj_name = proj_rcrd.getFieldValue('altname');
                    var proj_category = proj_rcrd.getFieldText('custentity_project_allocation_category');
                    var proj_category_val = proj_rcrd.getFieldValue('custentity_project_allocation_category');
                    var billing_from_date = record.getFieldValue('custbody_billfrom');
                    var billing_to_date = record.getFieldValue('custbody_billto');
					var oneem_proj_id = proj_rcrd.getFieldValue('entityid');
					

                    var territory = nlapiLookupField('job', record.getFieldValue('job'), 'customer.territory');

                    //nlapiLogExecution('AUDIT', nlapiGetRecordId(), 'Project: ' + project + ', Customer: ' + customer);

                    var a_employee_names = new Object();
                    var a_resource_allocations = new Array();
                    var emp_list = new Array();

                    if (_logValidation(billing_from_date)) {
                        billing_from_date = nlapiStringToDate(billing_from_date);
                        billing_to_date = nlapiStringToDate(billing_to_date);

                        var filters_search_allocation = new Array();
                        
						if(i_employee)
						{
							
							
							filters_search_allocation=[[[["project","anyof",proj_internal_id]],"OR",[["resource","anyof",i_employee]]], 
						   "AND", 
						   ["enddate","onorafter",billing_to_date], 
						   "AND", 
						   ["startdate","onorbefore",billing_to_date]
						   ];
						}
						else
						{
							filters_search_allocation[filters_search_allocation.length] = new nlobjSearchFilter('project', null, 'anyof', proj_internal_id);
							filters_search_allocation[filters_search_allocation.length] = new nlobjSearchFilter('startdate', null, 'onorbefore', billing_to_date);
							filters_search_allocation[filters_search_allocation.length] = new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
                       
						}
						 var columns = new Array();
                        columns[0] = new nlobjSearchColumn('resource');
                        columns[1] = new nlobjSearchColumn('employeetype', 'employee');
                        columns[2] = new nlobjSearchColumn('custentity_persontype', 'employee');
                        columns[3] = new nlobjSearchColumn('subsidiary', 'employee');
                        columns[4] = new nlobjSearchColumn('custentity_fusion_empid', 'employee');
                        columns[5] = new nlobjSearchColumn('firstname', 'employee');
                        columns[6] = new nlobjSearchColumn('middlename', 'employee');
                        columns[7] = new nlobjSearchColumn('lastname', 'employee');
                        columns[8] = new nlobjSearchColumn('companyname', 'customer');
                        columns[9] = new nlobjSearchColumn('entityid', 'customer');
                        columns[10] = new nlobjSearchColumn('entityid', 'job');
                        columns[11] = new nlobjSearchColumn('custentity_practice', 'job');
                        columns[12] = new nlobjSearchColumn('department', 'employee');
                        columns[13] = new nlobjSearchColumn('custentity_legal_entity_fusion', 'employee');
                        var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
						
						
                        if (_logValidation(project_allocation_result)) {
                            for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {
                                var i_employee_id = project_allocation_result[i_search_indx].getValue('resource');
                                var s_emp_type = project_allocation_result[i_search_indx].getText('employeetype', 'employee');
                                var s_emp_person_type = project_allocation_result[i_search_indx].getText('custentity_persontype', 'employee');
                                var i_emp_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
                                var fusion_id = project_allocation_result[i_search_indx].getValue('custentity_fusion_empid', 'employee');
                                var first_name = project_allocation_result[i_search_indx].getValue('firstname', 'employee');
                                var middl_name = project_allocation_result[i_search_indx].getValue('middlename', 'employee');
                                var lst_name = project_allocation_result[i_search_indx].getValue('lastname', 'employee');
                                var cust_id = project_allocation_result[i_search_indx].getValue('entityid', 'customer');
                                var cust_name = project_allocation_result[i_search_indx].getValue('companyname', 'customer');
                                var proj_entity_id = project_allocation_result[i_search_indx].getValue('entityid', 'job');
                                var proj_department = project_allocation_result[i_search_indx].getValue('custentity_practice', 'job');
                                var proj_department_text = project_allocation_result[i_search_indx].getText('custentity_practice', 'job');
                                var emp_department = project_allocation_result[i_search_indx].getValue('department', 'employee');
                                var emp_department_text = project_allocation_result[i_search_indx].getText('department', 'employee');
                                var custentity_legal_entity_fusion = project_allocation_result[i_search_indx].getValue('custentity_legal_entity_fusion', 'employee');
                                
								
								if(i_employee==i_employee_id)
								{
									cust_id=customerobje.entityid;
									cust_name=customerobje.altname;
									proj_entity_id=oneem_proj_id;
									proj_department=proj_rcrd.getFieldValue('custentity_practice');
									proj_department_text=proj_rcrd.getFieldText('custentity_practice');
									
								}
								a_resource_allocations[i_search_indx] = {
                                    'emp_id': i_employee_id,
                                    'emp_type': s_emp_type,
                                    'person_type': s_emp_person_type,
                                    'subsidiary': i_emp_subsidiary,
                                    'fusion_id': fusion_id,
                                    'frst_name': first_name,
                                    'mddl_name': middl_name,
                                    'lst_name': lst_name,
                                    'cust_id': cust_id,
                                    'cust_name': cust_name,
                                    'proj_entity_id': proj_entity_id,
                                    'proj_dep_v': proj_department,
                                    'proj_dep_t': proj_department_text,
                                    'emp_dep_c': emp_department,
                                    'emp_dep_t': emp_department_text,
                                    'custentity_legal_entity_fusion': custentity_legal_entity_fusion
                                };
                                emp_list.push(i_employee_id);
                            }
                        }
                    }
					
					nlapiLogExecution("DEBUG",'emp_list',JSON.stringify(emp_list));
					
					 var misPracticeFlagTime = true;	
					 var prevMisPractice = '';	
					 var misCounter = 0;

                    for (var i = 1; i <= i_billable_time_count; i++) {
                        var usageEndline = context.getRemainingUsage();
                        if (usageEndline < 1000) {
                           // yieldScript(context);
                        }
                        var emp_full_name = '';
                        var misc_practice = '';
                        var core_practice = '';
                        var practice_flag_update = false;
                        var isApply = record.getLineItemValue('time', 'apply', i);

                        var item = record.getLineItemValue('time', 'item', i);

                        //if(isApply == 'T' && (item == '2221' || item == '2222' || item == '2425' || item == '2633'))
                        if (isApply == 'T') {
                            var employeeId = record.getLineItemValue('time', 'employee', i);
							if (misCounter > 0 && !_logValidation(prevMisPractice)) { //misCounter>0 && prevMisPractice==''	
                                    misPracticeFlagTime = false;	
                                }
                            if (a_employee_names[employeeId] == undefined) {
                                a_employee_names[employeeId] = nlapiLookupField('employee', employeeId, 'entityid');
                            }
                            var employeeName = a_employee_names[employeeId]; //

                            

                            if (emp_list.indexOf(employeeId) >= 0) {
								record.setLineItemValue('time', 'custcol_employeenamecolumn', i, employeeName); // Set Employee
								record.setLineItemValue('time', 'custcolprj_name', i, project); // Set Project
								record.setLineItemValue('time', 'custcolcustcol_temp_customer', i, customer); // Set Customer
								record.setLineItemValue('time', 'custcol_territory', i, territory);
                                var emp_posi = emp_list.indexOf(employeeId);
                                var emp_subsidiary = a_resource_allocations[emp_posi].subsidiary;
                                var entity_fusion = a_resource_allocations[emp_posi].custentity_legal_entity_fusion;
                                var emp_practice = a_resource_allocations[emp_posi].emp_dep_c;
                                var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
                                var isinactive_Practice_e = is_practice_active_e.isinactive;
                                nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
                                core_practice = is_practice_active_e.custrecord_is_delivery_practice;
                                nlapiLogExecution('debug', 'core_practice', core_practice);
                                if (emp_subsidiary == 3 && entity_fusion == 'Brillio Technologies Private Limited UK') {
                                    onsite_offsite = 'Onsite';
                                } 
                                else{
                                    onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                                }
                               
                                record.setLineItemValue('time', 'custcol_employee_type', i, a_resource_allocations[emp_posi].emp_type);
                                record.setLineItemValue('time', 'custcol_person_type', i, a_resource_allocations[emp_posi].person_type);
                                record.setLineItemValue('time', 'custcol_onsite_offsite', i, onsite_offsite);
                                record.setLineItemValue('time', 'custcol_billing_type', i, billing_type);

                                if (parseInt(emp_practice) == parseInt(497))
                                    emp_practice = 528;
                                if (parseInt(emp_practice) == parseInt(328))
                                    emp_practice = 523;
                                if (parseInt(emp_practice) == parseInt(517))
                                    emp_practice = 531;
                                
                                record.setLineItemValue('time', 'custcol_customer_entityid', i, a_resource_allocations[emp_posi].cust_id);
                                record.setLineItemValue('time', 'custcol_cust_name_on_a_click_report', i, a_resource_allocations[emp_posi].cust_name);

                                record.setLineItemValue('time', 'custcol_project_entity_id', i, a_resource_allocations[emp_posi].proj_entity_id);
                                record.setLineItemValue('time', 'custcol_proj_name_on_a_click_report', i, proj_name);
                                record.setLineItemValue('time', 'custcol_region_master_setup', i, region_id);
                                record.setLineItemValue('time', 'custcol_parent_executing_practice', i, parent_practice);
                                record.setLineItemValue('time', 'custcol_project_region', i, project_region);
                                record.setLineItemValue('time', 'custcol_proj_category_on_a_click', i, proj_category);

                                if (a_resource_allocations[emp_posi].frst_name)
                                    emp_full_name = a_resource_allocations[emp_posi].frst_name;

                                if (a_resource_allocations[emp_posi].mddl_name)
                                    emp_full_name = emp_full_name + ' ' + a_resource_allocations[emp_posi].mddl_name;

                                if (a_resource_allocations[emp_posi].lst_name)
                                    emp_full_name = emp_full_name + ' ' + a_resource_allocations[emp_posi].lst_name;

                                record.setLineItemValue('time', 'custcol_employee_entity_id', i, a_resource_allocations[emp_posi].fusion_id);
                                record.setLineItemValue('time', 'custcol_emp_name_on_a_click_report', i, emp_full_name);

                            }
                            var existing_practice = record.getLineItemValue('time', 'department', i);
                            var is_delivery = ''
                            if (existing_practice) {
                                is_delivery = nlapiLookupField('department', parseInt(existing_practice), 'custrecord_is_delivery_practice');
                            }
                            //Added for MIS Practice Update
                            if (proj_category_val) {
                                if (_logValidation(existing_practice) && is_delivery == 'T') {
                                    misc_practice = record.getLineItemText('time', 'department', i);
                                    practice_flag_update = true;
                                } else {
                                    if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T') && practice_flag_update == false) {
                                        misc_practice = emp_practice;
                                        misc_practice = a_resource_allocations[emp_posi].emp_dep_t;
                                    } else {
                                        misc_practice = record.getLineItemValue('time', 'department', i);
                                        if (!_logValidation(misc_practice)) {
                                            misc_practice = a_resource_allocations[emp_posi].proj_dep_v;
                                            var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                            isinactive_Practice_e = is_practice_active.isinactive;
                                            misc_practice = a_resource_allocations[emp_posi].proj_dep_t;
                                        } else if (_logValidation(misc_practice) && is_delivery == 'F') {
                                            misc_practice = a_resource_allocations[emp_posi].proj_dep_v;
                                            var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                            isinactive_Practice_e = is_practice_active.isinactive;
                                            misc_practice = a_resource_allocations[emp_posi].proj_dep_t;
                                        } else {
                                            var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                            var isinactive_Practice_e = is_practice_active.isinactive;
                                            misc_practice = record.getLineItemText('time', 'department', i);
                                        }
                                    }
                                }
                            }
                            nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
                            if (misc_practice && isinactive_Practice_e == 'F') {
                                record.setLineItemValue('time', 'custcol_mis_practice', i, misc_practice);
                            }
							
							 misCounter++;	
                             prevMisPractice = record.getLineItemValue('time', 'custcol_mis_practice', i);
							
                            //
                        }
                    }
                    //Billable Expensee
					
					var misPracticeFlagBill = true;	
					prevMisPractice = '';	
					misCounter = 0;

                    var i_billable_expense_count = record.getLineItemCount('expcost');
                    for (var i = 1; i <= i_billable_expense_count; i++) {
                        var misc_practice = '';
                        var core_practice = '';
                        var isApply = record.getLineItemValue('expcost', 'apply', i);

                        if (isApply == 'T') {
                            var employeeId = record.getLineItemValue('expcost', 'employee', i);

                            if (emp_list.indexOf(employeeId) >= 0) {
								 if (misCounter > 0 && !_logValidation(prevMisPractice)) {	
									misPracticeFlagBill = false;	
								}
                                var emp_posi = emp_list.indexOf(employeeId);
                                var emp_subsidiary = a_resource_allocations[emp_posi].subsidiary;

                                var emp_practice = a_resource_allocations[emp_posi].emp_dep_c;
                                var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
                                var entity_fusion = a_resource_allocations[emp_posi].custentity_legal_entity_fusion;
                                var isinactive_Practice_e = is_practice_active_e.isinactive;
                                nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
                                core_practice = is_practice_active_e.custrecord_is_delivery_practice;
                                nlapiLogExecution('debug', 'core_practice', core_practice);

                                if (emp_subsidiary == 3 && entity_fusion == 'Brillio Technologies Private Limited UK') {
                                    onsite_offsite = 'Onsite';
                                }
                                else{
                                    onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                                }
                                /* else if (emp_subsidiary == 3 || emp_subsidiary == 12) {
                                    var onsite_offsite = 'Offsite';
                                } else {
                                    var onsite_offsite = 'Onsite';
                                }*/
                                record.setLineItemValue('expcost', 'department', i, a_resource_allocations[emp_posi].emp_dep_c);
                                record.setLineItemValue('expcost', 'custcol_employee_type', i, a_resource_allocations[emp_posi].emp_type);
                                record.setLineItemValue('expcost', 'custcol_person_type', i, a_resource_allocations[emp_posi].person_type);
                                record.setLineItemValue('expcost', 'custcol_onsite_offsite', i, onsite_offsite);
                                record.setLineItemValue('expcost', 'custcol_billing_type', i, billing_type);

                                var cust_entity_id = customer.split(' ');
                                cust_entity_id = cust_entity_id[0];
                                record.setLineItemValue('expcost', 'custcol_customer_entityid', i, cust_entity_id);

                                var pro_entity_id = project.split(' ');
                                pro_entity_id = pro_entity_id[0];
                                record.setLineItemValue('expcost', 'custcol_project_entity_id', i, pro_entity_id);

                                var emp_name = record.getLineItemValue('expcost', 'employeedisp', i);
                                var emp_id = '';
                                if (_logValidation(emp_name)) {
                                    emp_id = emp_name.split('-');
                                    emp_id = emp_id[0];
                                }
                                record.setLineItemValue('expcost', 'custcol_employee_entity_id', i, emp_id);

                                record.setLineItemValue('expcost', 'custcol_region_master_setup', i, region_id);
                                record.setLineItemValue('expcost', 'custcol_parent_executing_practice', i, parent_practice);
                                record.setLineItemValue('expcost', 'custcol_project_region', i, project_region);

                                //Added for MIS Practice Update
                                if (proj_category_val) {
                                    if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
                                        misc_practice = emp_practice;
                                        misc_practice = a_resource_allocations[emp_posi].emp_dep_t;
                                    } else {
                                        misc_practice = a_resource_allocations[emp_posi].proj_dep_v;
                                        var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
                                        isinactive_Practice_e = is_practice_active.isinactive;
                                        misc_practice = a_resource_allocations[emp_posi].proj_dep_t;
                                    }
                                }
                                nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
                                if (misc_practice && isinactive_Practice_e == 'F') {
                                    record.setLineItemValue('expcost', 'custcol_mis_practice', i, misc_practice);
                                }
								
								prevMisPractice = record.getLineItemValue('expcost', 'custcol_mis_practice', i);	
                                misCounter++;
                            }
                        }
                    }
					
					if (misPracticeFlagBill == false || misPracticeFlagTime == false) {	
                            err_row_excel += '	<tr>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'invoice' + '</td>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + invoice_id + '</td>';	
                            err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'Please enter MIS practice' + '</td>';	
                            err_row_excel += '	</tr>';	
                        }
                    

                    record.setFieldValue('custbody_update_expense_using_sch', 'T');
                    var submitted_id = nlapiSubmitRecord(record, {
                        disabletriggers: true,
                        enablesourcing: true
                    });
                    nlapiLogExecution('DEBUG', 'submitted_id', submitted_id);
                    je_list.push(submitted_id);
                    //yieldScript(context);
                }
				} catch (err){
					 //Added try-catch logic by Sitaram 06/08/2021
					nlapiLogExecution('DEBUG', 'err', err);
					err_row_excel += '	<tr>';
					err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'invoice' + '</td>';
					err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum  + '</td>';
					err_row_excel += ' <td width="6%" font-size="11" align="center">' + invoice_id + '</td>';
					err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';
					
					err_row_excel += '	</tr>';
					
				}
				
            } //for loop
        }
		 if(_logValidation(err_row_excel)){
				var tailMail = '';
				tailMail += '</table>';
				tailMail += ' </td>';
				tailMail += '</tr>';
				tailMail += '</table>';
				
				strVar_excel = strVar_excel+err_row_excel+tailMail
				//excel_file_obj = generate_excel(strVar_excel);
				var mailTemplate = "";
                mailTemplate += '<html>';
                mailTemplate += '<body>';
				mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";
				mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";
				mailTemplate += "<p><b> Script Id:- "+context.getScriptId() +"</b></p>";
                mailTemplate += "<p><b> Script Deployment Id:- "+context.getDeploymentId() +"</b></p>";
				mailTemplate += "<br/>"
                mailTemplate += "<br/>"
                mailTemplate += strVar_excel
                mailTemplate += "<br/>"
				mailTemplate += "<p>Regards, <br/> Information Systems</p>";
                mailTemplate += '</body>';
                mailTemplate += '</html>';
				nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);
			}
        var timestp_E = timestamp();
        nlapiLogExecution('DEBUG', 'Execution Ended Current Date', 'timestp_E...' + timestp_E);
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Process Error', e);
    }
}

function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function timestamp() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    str += hours + ":" + minutes + ":" + seconds + " ";

    return str + meridian;
}

function yieldScript(currentContext) {

    nlapiLogExecution('AUDIT', 'API Limit Exceeded');
    var state = nlapiYieldScript();

    if (state.status == "FAILURE") {
        nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
            state.reason + ' / Size : ' + state.size);
        return false;
    } else if (state.status == "RESUME") {
        nlapiLogExecution('AUDIT', 'Script Resumed');
    }
}

function PL_curencyexchange_rate(b_recentRecord) {
    var a_dataVal = {};
    var column = new Array();
    column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
    column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
    column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
    column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
    column[4] = new nlobjSearchColumn('custrecord_pl_crc_cost_rate');
    column[5] = new nlobjSearchColumn('custrecord_pl_nok_cost_rate');
    column[6] = new nlobjSearchColumn('custrecord_eur_usd_conv_rate');
    column[7] = new nlobjSearchColumn('custrecord_gbp_converstion_rate');
    column[8] = new nlobjSearchColumn('custrecord_aud_to_usd');
    column[9] = new nlobjSearchColumn('custrecord_usd_ron_conversion_rate');
    column[10] = new nlobjSearchColumn('custrecord_usd_cad_conversion_rate');

    if (b_recentRecord == 'T') {
        column[column.length] = new nlobjSearchColumn('internalid').setSort(true);
    }

    var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates', null, null, column);
    if (currencySearch) {
		
		//Fetch Recent P&L currency exchange rate table
        if (b_recentRecord == 'T') {
          var i_indx=0;
           a_dataVal = {
                s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
                i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
                inr_to_usd_conv_revrate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
                inr_to_usd_conv_costrate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
                gbp_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_gbp_converstion_rate'),
                crc_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
                nok_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate'),
                eur_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_eur_usd_conv_rate'),
                aud_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_aud_to_usd'),
                cad_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_usd_cad_conversion_rate'),
                ron_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_usd_ron_conversion_rate')
            }


            for (var key in a_dataVal) {
				if(key!="s_month" && key!="i_year")
                a_dataVal[key] = 1 / (parseFloat(a_dataVal[key]));
            }
           nlapiLogExecution("DEBUG","a_dataVal",JSON.stringify(a_dataVal));
        }
		else{
		//Fetch all P&L currency exchange rate table data
        for (var i_indx = 0; i_indx < currencySearch.length; i_indx++) {
            var indexvalue = currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month') + "_" + currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year');
            a_dataVal[indexvalue] = {
                s_month: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_month'),
                i_year: currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year'),
                inr_to_usd_conv_revrate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_revnue_r'),
                inr_to_usd_conv_costrate: currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_cost'),
                gbp_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_gbp_converstion_rate'),
                crc_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_pl_crc_cost_rate'),
                nok_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_pl_nok_cost_rate'),
                eur_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_eur_usd_conv_rate'),
                aud_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_aud_to_usd'),
                cad_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_usd_cad_conversion_rate'),
                ron_to_usd_conv_rate: currencySearch[i_indx].getValue('custrecord_usd_ron_conversion_rate')

            };
        }
		}
    }
    return a_dataVal;
}



function _conversionrateIntoUSD(books_id, trans_Accounting_line_Amount, category, exchangerate, pl_currencyexchangerate) {
var trans_Accounting_line_Amount=trans_Accounting_line_Amount*exchangerate;

var total_USD=0;
var f_revRate,f_costRate,f_gbp_rev_rate,f_crc_cost_rate,f_nok_cost_rate,f_eur_conv_rate,f_aud_conv_rate,f_ron_conv_rate,f_cad_conv_rate;

    if (pl_currencyexchangerate) {
			f_revRate = pl_currencyexchangerate.inr_to_usd_conv_revrate;
			f_costRate = pl_currencyexchangerate.inr_to_usd_conv_costrate;
			f_gbp_rev_rate = pl_currencyexchangerate.gbp_to_usd_conv_rate;
			f_crc_cost_rate = pl_currencyexchangerate.crc_to_usd_conv_rate;
			f_nok_cost_rate = pl_currencyexchangerate.nok_to_usd_conv_rate;
			f_eur_conv_rate = pl_currencyexchangerate.eur_to_usd_conv_rate;
			f_aud_conv_rate = pl_currencyexchangerate.aud_to_usd_conv_rate;
			f_ron_conv_rate = pl_currencyexchangerate.ron_to_usd_conv_rate;
			f_cad_conv_rate = pl_currencyexchangerate.cad_to_usd_conv_rate;
		}
    
	
	//books_id - Subsidiary internal id
		if ((books_id == 3 || books_id == 9 || books_id == 13)) {
			if (category == "Revenue" || category == "Discount") {
				var usd_inr_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(f_revRate);
			   total_USD = usd_inr_total.toFixed(3);
			} else {
				var usd_inr_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(f_costRate);
				total_USD = usd_inr_total.toFixed(3);
			}

		}
    //IF  subsidiary = UK  THEN USD-TO-GBP / AMOUNT
    if (books_id == 7) {
        var usd_to_EUR_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(f_gbp_rev_rate);
        total_USD = usd_to_EUR_total.toFixed(3);
    }

    //IF  subsidiary = Canada THEN USD-TO-CAD / AMOUNT
    if (books_id == 10) {
        var usd_to_AUD_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(f_cad_conv_rate);
        total_USD = usd_to_AUD_total.toFixed(3);
    }

    //IF  subsidiary = Cognetik Srl THEN USD-TO-RON / AMOUNT
    if (books_id == 12) {
        var usd_to_RON_total = parseFloat(trans_Accounting_line_Amount) / parseFloat(f_ron_conv_rate);
        total_USD = usd_to_RON_total.toFixed(3);
    }

    //IF  subsidiary IN (Inc, BLLC,Cognetik Corp,Comity Inc.) THEN AMOUNT
    if (books_id == 1 || books_id == 2 || books_id == 11 || books_id == 8 || books_id == 14) {
        total_USD = trans_Accounting_line_Amount;
    }

return total_USD;
}


function _Currency_exchangeRate(s_proj_currency, o_currencyexchange_rates,category) {
    var i_master_currency = 1;

    if (s_proj_currency == 'USD')
        i_master_currency = 1;
    if (s_proj_currency == 'INR')
        i_master_currency = o_currencyexchange_rates.inr_to_usd_conv_costrate;
    if (s_proj_currency == 'EUR')
        i_master_currency = o_currencyexchange_rates.eur_to_usd_conv_rate;
    if (s_proj_currency == 'GBP')
        i_master_currency = o_currencyexchange_rates.gbp_to_usd_conv_rate;
    if (s_proj_currency == 'AUD')
        i_master_currency = o_currencyexchange_rates.aud_to_usd_conv_rate;
    if (s_proj_currency == 'RON')
        i_master_currency = o_currencyexchange_rates.ron_to_usd_conv_rate;
    if (s_proj_currency == 'CAD')
        i_master_currency = o_currencyexchange_rates.cad_to_usd_conv_rate;
  if (s_proj_currency == 'INR' && category=="Revenue")
        i_master_currency = o_currencyexchange_rates.inr_to_usd_conv_revrate;

    return i_master_currency;

}
function Search_revenue_Location_subsdidary(array_filters) {
    var filters = [];
    filters.push(["isinactive", "is", "F"]);

    if (array_filters) {
        filters.push("And");
        filters.push(array_filters);
    }

    var obj_rev_loc_subSrch = nlapiSearchRecord("customrecord_revenue_fields_mapping", null,
        filters,
        [
            new nlobjSearchColumn("custrecord_revenue_sourcetype", null, "GROUP").setSort(false),
            new nlobjSearchColumn("custrecord_source_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_revenue_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_subsidairy", null, "GROUP"),
            new nlobjSearchColumn("custrecord_revenue_subsidiary", null, "GROUP"),
            new nlobjSearchColumn("custrecord_cost_location", null, "GROUP"),
            new nlobjSearchColumn("custrecord_cost_in_dollor", null, "GROUP"),
            new nlobjSearchColumn("custrecord_offsite_onsite", null, "GROUP"),
            new nlobjSearchColumn("custrecord_currency_symbol", null, "GROUP"),
            new nlobjSearchColumn("custrecord_plcurrency", null, "GROUP"),
            new nlobjSearchColumn("custrecord_pl_rate_mapping", null, "GROUP"),
            new nlobjSearchColumn("custrecord_pl_revenue_mapping", null, "GROUP")
        ]
    );

    var o_rev_loc_sub = {
        "Location": {},
        "Subsidairy": {},
        "Cost Location": {},
        "Cost Reference": {},
        "Onsite/Offsite": {},
        'CurrencySymbol': {},
        'forecastExchangerates': {},
        'plcost': {},
        'plrevenue': {},
        'PLfldids': {}
    };
	
	if(obj_rev_loc_subSrch){
		
    for (var i = 0; obj_rev_loc_subSrch.length > i; i++) {
        var i_sourcetype = obj_rev_loc_subSrch[i].getText("custrecord_revenue_sourcetype", null, "GROUP");
        switch (i_sourcetype) {
            case "Location":
                o_rev_loc_sub.Location[obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_location', null, "GROUP");
                break;

            case "Subsidairy":
                o_rev_loc_sub["Subsidairy"]
                    [obj_rev_loc_subSrch[i].getValue('custrecord_subsidairy', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_subsidiary', null, "GROUP");
                o_rev_loc_sub["Onsite/Offsite"]
                    [obj_rev_loc_subSrch[i].getValue('custrecord_subsidairy', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_offsite_onsite', null, "GROUP");
                break;

            case "Cost Location":
                o_rev_loc_sub["Cost Location"][obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_cost_location', null, "GROUP");
                o_rev_loc_sub["Cost Reference"][obj_rev_loc_subSrch[i].getValue('custrecord_source_location', null, "GROUP")] = obj_rev_loc_subSrch[i].getValue('custrecord_cost_in_dollor', null, "GROUP");
                break;
            case "Currency":

                var i_map_subsidairy = obj_rev_loc_subSrch[i].getValue('custrecord_revenue_subsidiary', null, "GROUP");
                var s_pl_revenue_map = obj_rev_loc_subSrch[i].getValue('custrecord_pl_revenue_mapping', null, "GROUP");
                var s_pl_cost_map = obj_rev_loc_subSrch[i].getValue('custrecord_pl_rate_mapping', null, "GROUP");
                var s_currencey = obj_rev_loc_subSrch[i].getText('custrecord_plcurrency', null, "GROUP");
                o_rev_loc_sub["forecastExchangerates"][s_currencey] = s_pl_cost_map;
                o_rev_loc_sub["CurrencySymbol"][s_currencey] = obj_rev_loc_subSrch[i].getValue('custrecord_currency_symbol', null, "GROUP");
                if (i_map_subsidairy) {
                    o_rev_loc_sub["plcost"][i_map_subsidairy] = s_pl_cost_map;
                }
                if (_logValidation(s_pl_revenue_map)) {
                    o_rev_loc_sub["plrevenue"][i_map_subsidairy] = s_pl_revenue_map;
                    o_rev_loc_sub["PLfldids"][s_pl_revenue_map] = s_pl_revenue_map;
                }

                if (!o_rev_loc_sub["PLfldids"][s_pl_cost_map] && _logValidation(s_pl_cost_map)) {
                    o_rev_loc_sub["PLfldids"][s_pl_cost_map] = s_pl_cost_map;
                }

                break;
        }
    }
	}
    return o_rev_loc_sub;
}



function _logValidation(value) {
    if (value != null && value!= '- None -' && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}