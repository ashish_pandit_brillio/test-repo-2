/**
 *@NApiVersion 2.x
 *@NScriptType WorkflowActionScript
 */

define(["N/runtime","N/email","N/render","N/file","SuiteScripts/moment","N/record","N/search",'N/xml','N/format'], function (runtime,email,render,file,moment,record,search,xml,format) {

    function onAction(scriptContext) {
        try {

            var tpl_id = 0;

            var newRecord = scriptContext.newRecord;
            
            var subsidiary = newRecord.getText({
                fieldId:"subsidiary"
            });

            if(subsidiary=="Brillio Technologies Private Limited")
            {
                tpl_id = "31"
            }else{
                tpl_id = "32"
            }
          
            var inv_file = onRequest(newRecord.id);

            inv_file.folder = 3627808;

          var fileid =   inv_file.save();



            var mergeResult = render.mergeEmail({
                templateId: tpl_id,
                entity:null,// entity_obj,
                recipient:null,//{ type:"employee",id: emailRecipients},
                supportCaseId: null,
                transactionId: newRecord.id,
                customRecord: null
            });
           
            email.send({
                author: runtime.getCurrentUser().id,
                recipients: newRecord.getValue("entity"),
                subject: mergeResult.subject,
                body: mergeResult.body,
                attachments:[inv_file],
              relatedRecords: {
						transactionId: newRecord.id,
                		entityId:41571
					},
            });


          record.attach({
                record: {
                    type: 'file',
                    id: fileid
                },
                to: {
                    type: 'vendorpayment',
                    id:  newRecord.id
                }
            });

            var print_count =  parseInt(newRecord.getValue({
                fieldId:"custbody_print_count"
            }))||0;

            print_count += 1;
            
            newRecord.setValue({
                fieldId:"custbody_print_count",
                value:print_count
            })

            //newRecord.save();

        }catch(e)
        {
            log.error("error",e)
        }
       
    }


    function getConsultant(tran)
    {
        var consultant_lk = search.lookupFields({
            type:"transaction",
            id:tran,
            columns:["custbody_consultantname"]
        });

        log.debug("Res",consultant_lk)
        if(consultant_lk.custbody_consultantname.length>0)
        return consultant_lk.custbody_consultantname[0].text;
        else
        return "";
    }

    function getSearch(type, filters, columns) {
        try {
            var dynamic_search = search.create({
                type: type,
                filters: filters,
                columns: columns
            });
            var result_out = [];
            var myPagedData = dynamic_search.runPaged();
            log.debug("mypageddata",myPagedData)
            myPagedData.pageRanges.forEach(function (pageRange) {
                    var myPage = myPagedData.fetch({
                        index: pageRange.index
                    });
                    myPage.data.forEach(function (res) {
                        var values = {};
                        //iterate over the collection of columns for the value
                        myPagedData.searchDefinition.columns.forEach(function (c, i, a) {
    
                            var key_name = "";
    
                            if (c.join)
                                key_name = c.join + "_" + c.name
                            else
                                key_name = c.name;
    
                            var value = res.getText(c);
    
                            if (value == null) {
                                values[key_name] = res.getValue({
                                    name: c
                                });
                            } else {
                                values[key_name] = {
                                    text: res.getText(c),
                                    value: res.getValue(c)
                                };
                            }
                        });
                        result_out.push(values);
                    });
                });

                return result_out;
            }catch(e)
            {
                log.debug("ex",e)
                return [];
            }
        
            
        }
    
        function getSublist(rec,sublist,cols)
        {
            var count = rec.getLineCount({
                sublistId:sublist
            });

            var result = [];

            for(var i=0;i<count;i++)
            {
                var temp = {};

                for(var j=0; j<cols.length;j++)
                {
                    temp[cols[j]] = {
                        value:rec.getSublistValue({
                            sublistId:sublist,
                            fieldId:cols[j],
                            line:i
                        }),
                        text:rec.getSublistText({
                            sublistId:sublist,
                            fieldId:cols[j],
                            line:i
                        })
                    }
                }
                result.push(temp);
            }
            return result;
        }

        function sum(list,key,join)
        {
            log.debug("sum:key",key)
            log.debug("sum:list",list)
            var sum = 0;
            for(var i=0;i<list.length;i++)
            {
                var row = list[i];
                log.debug("sum:row",row);

                if(join)
                {
                    log.debug("sum:key",parseFloat(row[key][join]||0))
                    sum += parseFloat(row[key][join]||0);
                }else
                {
                    sum += parseFloat(row[key]||0);
                }
                
            }
            return sum;
        }

    
    function onRequest(rec_id) {
        try {

         
			var billpaymentRec=record.load({type:'vendorpayment',id:rec_id, isDynamic:true});
			var linecount=billpaymentRec.getLineCount({sublistId: 'apply'});
			var logourl=xml.escape('https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=6237&c=3883006_SB1&h=NKuqwNZ1CAWE-94JcGIcNWtl8Mx3NoF0y1OGBbQGz3Kq8QJN')
			
			var billpaymentdate=billpaymentRec.getValue("trandate");
			var billpaydoc=billpaymentRec.getValue('tranid');
			var transactionnumber=billpaymentRec.getValue('transactionnumber');
			
			var vendor=billpaymentRec.getValue("entity");
			var vendRec=record.load({type:'vendor',id:vendor, isDynamic:true});
			var billaddr1=xml.escape(vendRec.getValue("billaddr1"));
			var billaddr2=xml.escape(vendRec.getValue("billaddr2"));
			var billaddressee=xml.escape(vendRec.getValue("billaddressee"));
			var billcity=xml.escape(vendRec.getValue("billcity"));
			var billcountry=xml.escape(vendRec.getValue("billcountry"));
			var billstate=xml.escape(vendRec.getValue("billstate"));
			var billzip=xml.escape(vendRec.getValue("billzip"));
			
			var vendfax=xml.escape(vendRec.getValue("fax"));
			var vendemail=xml.escape(vendRec.getValue("email"));
			
			if(billcountry == 'IN')
				billcountry='India';
			billpaymentdate=new Date(billpaymentdate);
var datedata=format.format({value: billpaymentdate,type: format.Type.DATE});
			var xmls = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
				 xmls += "<pdf>";
				 xmls += "<head>";
				 xmls += "<meta name=\"my pdf\" value=\"Invoice\"/>";
				 xmls += "<macrolist>";
	 
	    xmls += "<macro id='nlheader'>"
		xmls +='<table style="border-collapse: collapse;" width="100%">'
						xmls += "<tr>"	
						xmls +='<td width="50%"><img src =\ "'+logourl +'\" align = \"center\" style=\"float: left; margin-left: 17px;height: 100px;width: 100px;\"/></td>'; 
						xmls +='<td align="right"><br /><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;align:right;"><span color="#585858"><b>Payment No.<br />Date<br />Cheque No </b></span></span></span></td>'
						xmls +='<td align="right"><br /><span style="font-size:13px;"><span style="font-family:arial,helvetica,sans-serif;align:right;">: '+transactionnumber+'<br />: '+datedata+'<br />: '+billpaydoc+'</span></span></td>'
	
						xmls += '</tr>'												
						xmls += '</table>'
			xmls += "</macro>"
	                   
						 xmls+='<macro id="nlfooter">'
						xmls+='<table class="footer" style="width: 100%;"><tr>'
						xmls+='<td border-top="1" border-top-color="#C0C0C0">&nbsp;</td>'
						xmls+='</tr>'
						xmls+='<tr>'
						xmls+='<td align="right"><pagenumber/><span style="font-size: 12px;"> of </span><totalpages/></td>'
						xmls+='</tr></table>'
						xmls+='</macro>'
						xmls += "</macrolist>";
						xmls += "</head>"
						xmls += '<body header="nlheader" header-height="16%" footer="nlfooter" footer-height="50pt" padding="0.5in 0.5in 0.5in 0.5in" size="Letter">';
						
						
						xmls+='<table style="width: 100%; margin-top: 10px;"><tr>';
						xmls+='	<td class="addressheader">&nbsp;</td>';
						xmls+='	<td class="addressheader">&nbsp;</td>';
						xmls+='	<td border-left="2" border-left-color="#D3D3D3" class="addressheader" colspan="3"><span style="font-size:14px;"><span color="#585858"><b>PAYER</b></span></span></td>';
						xmls+='	<td border-left="2" border-left-color="#D3D3D3" class="addressheader" colspan="4" style="margin-left: 10px;"><span style="font-size: 14px;"><b>PAYEE</b></span></td>';
						xmls+='	</tr>';
						xmls+='	<tr>';
						xmls+='	<td class="address">&nbsp;</td>';
						xmls+='	<td class="address">&nbsp;</td>';
						xmls+='	<td border-left="2" border-left-color="#D3D3D3" class="address" style="font-size:13px;" colspan="3">Brillio Technologies Private Limited<br/>5th Floor, Primal Eco Space Campus , <br/>Priteck Park<br/>5th Floor, 4A Block<br/>Bellandur, Bangalore Karnataka 560037<br/>India<br/>Phone No. : 1800 067 744<br/>Fax No. : <br/>Web : <br/>Email : apindia@brillio.com</td>';
						
						xmls+='<td border-left="2" border-left-color="#D3D3D3" class="address" colspan="4" style="margin-left: 10px;font-size:13px;">'+billaddressee+'<br/>'+billaddr1+'<br/>'+billaddr2+'<br/>'+billcity+' '+billstate+' '+billzip+'<br/>'+billcountry+'<br/>Fax:'+vendfax+'<br/>Email:'+vendemail+'</td>'
						//xmls+='	<td border-left="2" border-left-color="#D3D3D3" class="address" colspan="4" style="margin-left: 10px;"><br />${record.address}<br /><span style="font-size:12px;">${record.custbody_vendoraddress}<#if record.entity.fax?has_content><br />Fax No. : ${record.entity.fax}</#if><br /><#if record.entity.email?has_content>Email :</#if> <u>${record.entity.email}</u></span></td>';
						xmls+='	</tr>';
						
						xmls+='</table>';
						xmls+='<span style="font-size:14px;"><b>Payment Amount Specification : </b></span>'
						
						xmls+='<table style="width: 100%; margin-top: 14px;">'
						xmls+='<thead>';
						xmls+='	<tr style="font-size:13px;">';
						xmls+='	<th align="center" width="15%" background-color="#D3D3D3">Invoice Date</th>';
						xmls+='	<th background-color="#D3D3D3" width="15%">Invoice No.</th>';
						xmls+='	<th background-color="#D3D3D3" width="22%">Consultant Name</th>';
						xmls+='	<th background-color="#D3D3D3" width="13%">Description</th>';
						xmls+='	<th align="right" background-color="#D3D3D3" width="10%">Invoice Amt</th>';
						xmls+='	<th align="right" background-color="#D3D3D3" width="10%">TDS Amt</th>';
						xmls+='	<th align="right" background-color="#D3D3D3" width="15%" border-left="1" border-left-color="#C0C0C0" >Paid Amt</th>';
						xmls+='	</tr>';
						xmls+='</thead>';
						

						 for(var i=0;i<linecount;i++)
							{
								
								  var applydate = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'applydate',line: i});
								  var billNumber = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'refnum',line: i});
								  var currency = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'currency',line: i});
								  var doc = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'doc',line: i});
								  var duedate = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'duedate',line: i});
								  var total = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'total',line: i});
								  var type = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'type',line: i});
								  var due = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'due',line: i});
								  var trantype = billpaymentRec.getSublistValue({sublistId: 'apply',fieldId: 'trantype',line: i});
								  log.debug('doc',doc);
								  var salesorderSearchObj = search.create({
								   type: "vendorbill",
								   filters:
								   [
									  ["type","anyof","VendBill"], 
									  "AND", 
									  ["mainline","is","T"], 
									  "AND", 
									  ["internalid","anyof",doc]
								   ],
								   columns:
								   []
								});
								var billsearch=executeSearch(salesorderSearchObj);
                              var grossamttotal=0;
                              
								if(billsearch)
								{
									for(var j=0;j<billsearch.length;j++)
									{
                                       var invDate='';
										var billId=billsearch[j].id;
										log.debug('billId',billId);
										 var rec=record.load({type:'vendorbill',id:billId, isDynamic:true});	
                                       invDate=rec.getValue('custbody_vendorinvoicedate')
										 var ConsultantName=rec.getValue('custbody_consultantname');
										var itemcount='';
										var sublist='expense';
											itemcount=rec.getLineCount({sublistId: 'expense'});
										if(itemcount==0){
											itemcount=rec.getLineCount({sublistId: 'item'});
											sublist='item';
										}
										
										log.debug('itemcount',itemcount);
										for(var k=0;k<itemcount;k++)
										{
										var custcol_tdsamount=rec.getSublistValue({sublistId: sublist,fieldId: 'custcol_tdsamount',line: k});
                                          var grossamt=rec.getSublistValue({sublistId: sublist,fieldId: 'grossamt',line: k});
										if(custcol_tdsamount)
											break;
										}
                                      for(var k=0;k<itemcount;k++)
										{
                                          var grossamt=rec.getSublistValue({sublistId: sublist,fieldId: 'grossamt',line: k});
                                          if(grossamt > 0)
										grossamttotal=parseFloat(grossamttotal)+parseFloat(grossamt)
											
										}
									}	
								}
								if(!custcol_tdsamount)
									custcol_tdsamount=0;
                              if(invDate){
								var applydatedata=format.format({value: invDate,type: format.Type.DATE});
                              }
                              else
                                {
                                  var applydatedata='';
                                }
								var invAmt=parseFloat(total)-parseFloat(custcol_tdsamount);
								var consultName='';
								if(ConsultantName)
								{
								var custValue = search.lookupFields({
									type: 'employee',
									id:ConsultantName,
									columns: ['firstname','lastname']
								});
								var fname=custValue['firstname'];
								var lname=custValue['lastname'];
								 consultName=fname+' '+lname;
								 
								}
										xmls+='<tr style="font-size:12px;">';
										xmls+='	<td align="center" line-height="150%" width="15%">'+applydatedata+'</td>';
										xmls+='	<td width="15%">'+billNumber+'</td>';
										xmls+='	<td width="22%">'+consultName+'</td>';
										xmls+='	<td style="text-align: center;" width="13%">'+type+'</td>';
										xmls+='	<td align="center" width="10%">'+formatCurrency(grossamttotal)+'</td>';
										xmls+='	<td align="center" width="10%">'+formatCurrency(custcol_tdsamount)+'</td>';
										xmls+='	<td align="right" width="15%" border-left="1" border-left-color="#C0C0C0" >'+formatCurrency(total)+'</td>';
										xmls+='	</tr>';
										
							}
							
							var taxtotal=billpaymentRec.getValue("taxtotal");
							if(!taxtotal || taxtotal == undefined || taxtotal == '' || taxtotal == null)
								taxtotal=0;
							var currency=billpaymentRec.getText("currency");
							var total=billpaymentRec.getValue("total");
		    
									xmls+='</table>';
									        xmls+='<table class="total" style="width: 100%;top:0px;">';
											xmls+='	<tr style="font-size:12px;">';
											xmls+='<th align="center" background-color="#FFFFFF" colspan="8">&nbsp;</th>'
											xmls+='<th background-color="#FFFFFF" colspan="6">&nbsp;</th>'
											xmls+='<th></th>'
											xmls+='	<th align="center" background-color="#D3D3D3" colspan="4"></th>';
											xmls+='	<th background-color="#D3D3D3" colspan="7"><b>Payment Amount</b></th>';
											xmls+='	<th align="right" background-color="#D3D3D3" colspan="4">'+currency+'</th>';
											xmls+='	<th align="right" background-color="#D3D3D3" border-left="1" border-left-color="#C0C0C0" colspan="4">'+formatCurrency(total)+'</th>';
											xmls+='	</tr></table>';
											xmls+='<span style="top:30px;"><b>Note:&nbsp;</b>Please write to&nbsp;<a data-auth="NotApplicable" data-linkindex="0" href="mailto:APINDIA@brillio.com" id="LPlnk316225" rel="noopener noreferrer" target="_blank">APINDIA@brillio.com</a>&nbsp;for any queries.</span>'
										 
		  xmls += "</body>\n</pdf>";
						 			var file1 =render.xmlToPdf({xmlString: xmls}); 	
          file1.name='Bill Payment'+billpaydoc+'.pdf';
          return file1;
          
          
          
        /*    var rec = record.load({
                type:"vendorpayment",
                id:rec_id
            });

            var apply_count = rec.getLineCount({
                sublistId:"apply"
            });

            var apply = [];

            for(var i=0;i<apply_count;i++)
            {
                var row = {};

                var tran = rec.getSublistValue({
                    sublistId: "apply",
                    fieldId: "doc",
                    line: i
                });
                
                var applydate = rec.getSublistValue({
                    sublistId: "apply",
                    fieldId: "applydate",
                    line: i
                });
                
                var refnum = rec.getSublistValue({
                    sublistId: "apply",
                    fieldId: "refnum",
                    line: i
                });
                
                var type = rec.getSublistValue({
                    sublistId: "apply",
                    fieldId: "type",
                    line: i
                });
                
                var amount = rec.getSublistValue({
                    sublistId: "apply",
                    fieldId: "amount",
                    line: i
                });
                
                row.consultant = getConsultant(tran);
                row.amount = amount;
                row.applydate = new moment( applydate).format("DD/MM/YYYY");
                row.type = type;
                row.amount = amount;
                row.refnum = refnum;

                var bill = record.load({
                    type:"vendorbill",
                    id:tran
                });
                var get_item_lines = getSublist(bill,"item",
                ["item","amount","quantity","grossamount","rate","taxamount"]);

                log.debug("get_item_lines",get_item_lines);
               
                var get_expense_lines = getSublist(bill,"expense",
                ["account","amount","category","grossamt","tax1amt","taxamount"]);

                log.debug("get_expense_lines",get_expense_lines);

                var item_sum = sum(get_item_lines,"grossamt","value");
                log.debug("item_sum",item_sum);
                var expense_sum = sum(get_expense_lines,"grossamt","value");
                log.debug("expense_sum",expense_sum);

                 row.invoice = item_sum+expense_sum
                 var tds = getSearch("customrecord_tdsbillrelation",[
                    ["custrecord_billbillno","is",tran],
                    "AND",
                    ["isinactive","is",false]
                ],["custrecord_billtdsamount","internalid","custrecord_billbillno"]);



                log.debug("tds",tds);

                var sum_tds = sum(tds,"custrecord_billtdsamount");

                log.debug("sumtds",sum_tds);


                row.tds = sum_tds;
          

                apply.push(row)

            }

            
            var credit_count = rec.getLineCount({
                sublistId:"credit"
            });

            var cons_name_credit = [];

            for(var i=0;i<credit_count;i++)
            {
                var tran = rec.getSublistValue({
                    sublistId: "credit",
                    fieldId: "doc",
                    line: i
                });

                var consultant_lk = search.lookupFields({
                    type:"transaction",
                    id:tran,
                    columns:["custbody_consultantname"]
                });

                log.debug("Res",consultant_lk)

                if(consultant_lk.custbody_consultantname.length>0)
                cons_name_apply.push(consultant_lk.custbody_consultantname[0].text)
                else
                cons_name_apply.push("")
            }

            var tpl_file = file.load({
                id:"SuiteScripts/payment_template.txt"
            });

            tpl_file_str = tpl_file.getContents();
            //replace(/${custbody_consultantname}/g,"testuser");

            var renderer = render.create();

            renderer.templateContent = tpl_file_str

            renderer.addRecord('record',rec);

            renderer.addCustomDataSource({
                format: render.DataSource.OBJECT,
                alias: "JSON",
                data: {
                    credit:cons_name_credit,
                    apply:apply
                }
                });


            var invoicePdf = renderer.renderAsPdf();
     
            return invoicePdf;*/

        }catch(e)
        {
            log.error("e",e)
            
        }
    }
 function executeSearch(srch) {
					var results = [];

					//var pagedData = srch.runPaged();
					var pagedData = srch.runPaged({
						pageSize: 1000
					});
					pagedData.pageRanges.forEach(function(pageRange) {
						var page = pagedData.fetch({
							index: pageRange.index
						});
						page.data.forEach(function(result) {
							results.push(result);
						});
					});

					return results;
				};
				function formatCurrency(num) {
				num = num.toString().replace(/\$|\,/g,'');
				if(isNaN(num))
				num = "0";
				sign = (num == (num = Math.abs(num)));
				num = Math.floor(num*100+0.50000000001);
				cents = num%100;
				num = Math.floor(num/100).toString();
				if(cents<10)
				cents = "0" + cents;
				for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
				num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
				return (((sign)?'':'-') + num + '.' + cents);
				}

    return {
        onAction: onAction
    }

});
