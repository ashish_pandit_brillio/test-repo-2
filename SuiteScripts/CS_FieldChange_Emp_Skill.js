/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 July 2017     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){

	try{
		if(name == 'custpage_field0'){
			 var objUser = nlapiGetContext();
			var i_employee_id = objUser.getUser();
			var i_role = nlapiGetRole();
			var i_practice = nlapiGetFieldValue('custpage_field0');
			var filters =[];
			//filters.push(new nlobjSearchFilter('custrecord_parent_practice','department','anyof',i_practice));
			filters.push(new nlobjSearchFilter('department',null,'anyof',i_practice));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filters.push(new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'));
			var cols = [];
			cols.push(new nlobjSearchColumn('internalid'));
			cols.push(new nlobjSearchColumn('entityid'));
			cols.push(new nlobjSearchColumn('custentity_employee_primary_skill'));//custentity_employee_primary_skill
			cols.push(new nlobjSearchColumn('custentity_employee_secondary_skill'));//custentity_employee_secondary_skill
			cols.push(new nlobjSearchColumn('custentity_course_status'));
            cols.push(new nlobjSearchColumn('custentity_competency'));
			
			//var searchRes = nlapiSearchRecord('employee',null,filters,cols);
			nlapiRemoveSelectOption('custpage_field1', null);
			var count = nlapiGetLineItemCount('employee');
			var res;		
			if( i_employee_id == 37501 || i_employee_id == 30484 || i_employee_id == 106232|| i_employee_id == 8231){		
			var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript1334', 'customdeploy1') + '&i_department_interal_id='+i_practice, null, null);		
             res = response.getBody();	
             var searchRes = JSON.parse(res);	
			}		
          		
          else{		
        	  var searchRes = nlapiSearchRecord('employee',null,filters,cols);		
          }
			var objUser = nlapiGetContext();
            var i_employee_id = objUser.getUser();
            var i_role = nlapiGetRole();
			if(searchRes){
			    if(i_employee_id != 37501 && i_employee_id != 30484 && i_employee_id != 106232 && i_employee_id != 8231  )  {
			for(var i = 1; i <= searchRes.length; i++)
				{
						
	                    
							if(searchRes[i].getValue('custentity_employee_primary_skill')!= null && searchRes[i].getValue('custentity_employee_secondary_skill')!=null){
							nlapiSelectLineItem('employee_', i)
							nlapiSetCurrentLineItemValue('employee_', 'eid', searchRes[i].getValue('entityid'));
							nlapiSetCurrentLineItemValue('employee_', 'ps_id', searchRes[i].getValue('custentity_employee_primary_skill'));
							nlapiSetCurrentLineItemValue('employee_', 'ss_id', searchRes[i].getValue('custentity_employee_secondary_skill'));
                            nlapiSetCurrentLineItemValue('employee_', 'comp_id', searchRes[i].getValue('custentity_competency'));
							nlapiCommitLineItem('employee_');
							}
							nlapiInsertSelectOption('custpage_field1', searchRes[i].getValue('internalid'), searchRes[i].getValue('entityid'), false);
							
	                            }
	                            
	                            
							
				}
				if( i_employee_id == 37501 || i_employee_id == 30484 || i_employee_id == 106232|| i_employee_id == 8231){
					for (var i in searchRes) {
			        	 var clo = searchRes[i].columns;
			        	 var id = searchRes[i].id;
			             var enty =  clo.entityid;
			        	 var pms =clo.custentity_employee_primary_skill;
			        	 var secskl = clo.custentity_employee_secondary_skill;
			        	 var sts = clo.custentity_course_status;
                         var compt = clo.custentity_competency
			
					nlapiSelectLineItem('employee_', i)
					nlapiSetCurrentLineItemValue('employee_', 'eid',enty);
					nlapiSetCurrentLineItemValue('employee_', 'ps_id', pms);
					nlapiSetCurrentLineItemValue('employee_', 'ss_id', secskl);
					nlapiSetCurrentLineItemValue('employee_', 'cs_id',sts );
                      nlapiSetCurrentLineItemValue('employee_', 'comp_id',compt );
					nlapiCommitLineItem('employee_');
					//nlapiInsertSelectOption('custpage_field1', id, enty, false);

					}
				}
			}
			
			}
		}
		
		
	
	catch(e){
		nlapiLogExecution('DEBUG','Client Field Change Error',e);
	}
}

function pageinit(url){

	try{
		if(url == 'create'){
		
			var i_practice = nlapiGetFieldValue('custpage_field0');
			
			var filters =[];
			filters.push(new nlobjSearchFilter('custrecord_parent_practice','department','anyof',i_practice));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			
			var cols = [];
			cols.push(new nlobjSearchColumn('internalid'));
			cols.push(new nlobjSearchColumn('entityid'));
			
			var searchRes = nlapiSearchRecord('employee',null,filters,cols);
			nlapiRemoveSelectOption('custpage_field1', null);
			
			//nlapiInsertSelectOption('custpage_field1', -1, '', true);
			if(searchRes){
			for(var i = 0; i < searchRes.length; i++)
				{
						
				nlapiInsertSelectOption('custpage_field1', searchRes[i].getValue('internalid'), searchRes[i].getValue('entityid'), false);
						
				}
		}
		}
		if(url !='create'){
			window.open(url,'_self');
		}
		
	}
	
	catch(e){
		nlapiLogExecution('DEBUG','Client Field Change Error',e);
	}
	
 
}

