/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 09 Mar 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {

	try {

		if (request.getMethod() == "GET") {

			var form = nlapiCreateForm('Move Sub Task');

			var sublist = form.addSubList('custpage_subtask', 'list',
			        'Sub Task');

			sublist.addField('id', 'select', 'id', 'customrecord_move_task_mapping');

			var taskTitle = sublist.addField('title', 'text', 'title');
			taskTitle.setMandatory(true);

			sublist.addField('inactive', 'checkbox', 'Inactive');

			// search for all active sub-task
			var subTaskSearch = nlapiSearchRecord('customrecord_move_task_mapping', null,
			        [ new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
			        [ new nlobjSearchColumn('name') ]);

			var subTaskList = [];

			if (subTaskSearch) {

				subTaskSearch.forEach(function(subtask) {
					subTaskList.push({
					    'old' : 'T',
					    'id' : subtask.getId(),
					    'title' : subtask.getValue('name'),
					    'inactive' : 'F'
					});
				});
			}

			sublist.setLineItemValues(subTaskList);

			response.writePage(form);
		} else {

		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}
