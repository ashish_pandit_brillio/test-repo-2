/**
* Module Description
* User Event script to handle employee record creation and updation from the XML file generated from Fusion
* 
 * Version               Date                      Author                           Remarks
* 1.00             13th October, 2015         Manikandan v           This script saves the form data into 30 days questionnaire custom record from 30 days * questionnaire.html 
 *
*/


function feedbackSuitelet(request, response)
{
  var method=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(69372);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);          //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call");
 
  var feedback = new Object();
  feedback['Employee_ID'] = request.getParameter('TextBox1');
  feedback['Employee_Name'] = request.getParameter('TextBox3');
  feedback['Email_ID'] = request.getParameter('TextBox2');
  feedback['Designation'] = request.getParameter('TextBox4');
  feedback['Department'] = request.getParameter('TextBox5');
  feedback['Location'] = request.getParameter('TextBox6');
  
  
  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Comments1'] = request.getParameter('txtComments1');
  
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Comments2'] = request.getParameter('txtComments2');
  
  feedback['Response3'] = request.getParameter('OptionsGroup3');
  feedback['Comments3'] = request.getParameter('txtComments3');

  feedback['Response4'] = request.getParameter('OptionsGroup4');
  feedback['Comments4'] = request.getParameter('txtComments4');
  
  feedback['Response5'] = request.getParameter('OptionsGroup5');
  feedback['Comments5'] = request.getParameter('txtComments5');
  
  feedback['Response6'] = request.getParameter('OptionsGroup6');
  feedback['Comments6'] = request.getParameter('txtComments6');
  
  feedback['Response7'] = request.getParameter('OptionsGroup7');
  feedback['Comments7'] = request.getParameter('txtComments7');
  
  feedback['Response8'] = request.getParameter('OptionsGroup8');
  feedback['Comments8'] = request.getParameter('txtComments8');
  
  feedback['Response9'] = request.getParameter('OptionsGroup9');
  feedback['Response10'] = request.getParameter('OptionsGroup10');
  feedback['Response11'] = request.getParameter('OptionsGroup11');
  feedback['Response12'] = request.getParameter('OptionsGroup12');
  feedback['Response13'] = request.getParameter('OptionsGroup13');
  feedback['Response14'] = request.getParameter('OptionsGroup14');
  feedback['Response15'] = request.getParameter('OptionsGroup15');
  feedback['Comments10'] = request.getParameter('txtComments9');
  feedback['Comments11'] = request.getParameter('txtComments10');
  
  
  var status = saveRequest(feedback);
   
    var thanks_note = nlapiLoadFile(70206);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
                try
                {
               var feedback_form = nlapiCreateRecord('customrecord_30_days_response_india');
               feedback_form.setFieldValue('custrecord30_day_fusion_id_india', feedback.Employee_ID);
               feedback_form.setFieldValue('custrecord_30_day_name_india', feedback.Employee_Name);
               feedback_form.setFieldValue('custrecord_30_day_email_india', feedback.Email_ID);
               feedback_form.setFieldValue('custrecord_30_day_designation_india', feedback.Designation);
               feedback_form.setFieldValue('custrecord_30_day_department_india', feedback.Department);
			   feedback_form.setFieldValue('custrecord_30_day_location_india', feedback.Location);
			   
                                                   
               feedback_form.setFieldValue('custrecord_ques_1',"My Experience of the hiring process at Brillio has been");
               feedback_form.setFieldValue('custrecord_ques_2', "My assessment of the Day 1 joining process and orientation( includes explanation on policies, organization structure)  has been");
               feedback_form.setFieldValue('custrecord_ques_3', "My Experience with the support provided by the IT team in setting up my system and the required configuration has been");
			   feedback_form.setFieldValue('custrecord_ques_4', "My Experience with the support provided by the Admin team in setting up my workstation, telephone has been");
			   feedback_form.setFieldValue('custrecord_ques_5', "My welcome into the team and introduction to the team members by my Manager has been");
               feedback_form.setFieldValue('custrecord_ques_6', "Support provided by the Transition coach has been");
               feedback_form.setFieldValue('custrecord_ques_7', "Did you have enough clarity on the goals and the KRA’s set");
               feedback_form.setFieldValue('custrecord_ques_8', "My manager’s support in providing clarity of the current project allotted has been");
               feedback_form.setFieldValue('custrecord_ques_9', "My Experience with Information systems at Brillio");
			   feedback_form.setFieldValue('custrecord_timesheet', "Time sheet");
			    feedback_form.setFieldValue('custrecord_expenses', "Expenses");
				 feedback_form.setFieldValue('custrecord_purchase_request', "Purchase Request");
				  feedback_form.setFieldValue('custrecord_leave_india', "Leave");
				   feedback_form.setFieldValue('custrecord_performance_management_system', "Performance Management Systems");
				    feedback_form.setFieldValue('custrecord_yammer', "Yammer");
               feedback_form.setFieldValue('custrecord_ques_10', "How do you rate the buddy program ?");
			   feedback_form.setFieldValue('custrecord_ques_11', "My overall experience or the impression in the first 30 days. Any Recommendations");
			   
                                                                                                                                                                                                  
                                                   
                                                   

                                                   feedback_form.setFieldValue('custrecord_ans_1', feedback.Response1);
                                                   feedback_form.setFieldValue('custrecord_ans_2', feedback.Response2);
                                                   feedback_form.setFieldValue('custrecord_ans_3', feedback.Response3);
                                                   feedback_form.setFieldValue('custrecord_ans_4', feedback.Response4);
                                                   feedback_form.setFieldValue('custrecord_ans_5', feedback.Response5);
                                                   feedback_form.setFieldValue('custrecord_ans_6', feedback.Response6);
                                                   feedback_form.setFieldValue('custrecord_ans_7', feedback.Response7);
                                                   feedback_form.setFieldValue('custrecord_ans_8', feedback.Response8);
												   
												   feedback_form.setFieldValue('custrecord_act_1', feedback.Response9);
												   feedback_form.setFieldValue('custrecord_act_2', feedback.Response10);
												   feedback_form.setFieldValue('custrecord_act_3', feedback.Response11);
												   feedback_form.setFieldValue('custrecord_act_4', feedback.Response12);
												   feedback_form.setFieldValue('custrecord_act_5', feedback.Response13);
												   feedback_form.setFieldValue('custrecord_act_6', feedback.Response14);
												    feedback_form.setFieldValue('custrecord_ans_10', feedback.Response15);
												   
												   
												   
												   
												   
												   feedback_form.setFieldValue('custrecord_suggestions_1', feedback.Comments1);
                                                   feedback_form.setFieldValue('custrecord_suggestions_2', feedback.Comments2);
                                                   feedback_form.setFieldValue('custrecord_suggestions_3', feedback.Comments3);
                                                   feedback_form.setFieldValue('custrecord_suggestions_4', feedback.Comments4);
                                                   feedback_form.setFieldValue('custrecord_suggestions_5', feedback.Comments5);
                                                   feedback_form.setFieldValue('custrecord_suggestions_6', feedback.Comments6);
                                                   feedback_form.setFieldValue('custrecord_suggestions_7', feedback.Comments7);
                                                   feedback_form.setFieldValue('custrecord_suggestions_8', feedback.Comments8);
                                                   
                                                   feedback_form.setFieldValue('custrecord_suggestions_10', feedback.Comments11);
						feedback_form.setFieldValue('custrecord_suggestions_11', feedback.Comments10);
                                                   
												   
												   
												   
												   
												   
												   
                                                   
               
               
              
               
               
     
              
               var id = nlapiSubmitRecord(feedback_form, false,true);
               nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log','');
}
}
}
