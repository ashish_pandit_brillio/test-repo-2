/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Aug 2016     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
var days = ['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];
function scheduled(type) {
	try{
		var json={};
		var dataRow =[];
		//var filters = Array();
		//filters.push(new nlobjSearchFilter('employee', null, 'anyof', employeeID);

		var cols = Array();
		cols.push(new nlobjSearchColumn('resource'));
		cols.push(new nlobjSearchColumn('custevent3'));
		//cols.push(new nlobjSearchColumn('startdate'));
		//cols.push(new nlobjSearchColumn('enddate'));

		var resourceSearch = nlapiSearchRecord('resourceallocation',2503,null,cols);

		if(resourceSearch){
			for(var i=0;i<resourceSearch.length;i++){
				json ={
						Employee: resourceSearch[i].getValue('resource'),
						Bill_Rate: resourceSearch[i].getValue('custevent3'),
						//	Start_Date: resourceSearch[i].getValue('startdate'),
						//	End_Date: resourceSearch[i].getValue('enddate')

				};
				dataRow.push(json);
			}

		}
		for(var j=0;j<dataRow.length;j++){
			var resObj = dataRow[j];
			var employee = resObj.Employee;
			var bill_rate  = resObj.Bill_Rate;
			//var startDate = resObj.Start_Date;
			//	var endDate = resObj.End_Date;
			//Get TimeSheets
			var timesheetObj = searchTimeSheets(employee,bill_rate);
		}
	}
	catch(e){
		nlapiLogExecution('debug','Process Error',e);
	}
}

function searchTimeSheets(employeeID, bill_rate){
	try{
		var billRate = parseFloat(bill_rate);
		var stDate = nlapiStringToDate(nlapiDateToString(new Date('3/3/2019')));
		var enDate = nlapiStringToDate(nlapiDateToString(new Date('9/14/2019')));
		var employeeData = nlapiLookupField('employee',employeeID,['entityid']);
		var emp =  employeeData.entityid;
		nlapiLogExecution('debug','Employee ',emp);
		nlapiLogExecution('debug','bill_rate ',bill_rate);
		//
		var timeSheet_Res = nlapiSearchRecord("timebill",null,
				[
					["date","within","03/03/2019","09/14/2019"], 
					"AND", 
					["status","is","T"], 
					"AND", 
					["billable","is","T"],
					"AND", 
					["formulatext: {rate}","isempty",""], 
					"AND", 
					["customer","anyof","23694"], 
					"AND", 
					["item","anyof","2222"],"AND",
					["employee","anyof",employeeID]
					],[new nlobjSearchColumn("internalid"),
						new nlobjSearchColumn("timesheet")]);
		if(timeSheet_Res){
			for(var tt =0;tt<timeSheet_Res.length;tt++)
			{
				var Obj_timebill_Rec = nlapiSubmitField('timebill', timeSheet_Res[tt].getValue("internalid"), ['price','rate'], ['(Custom)',billRate]);
			}
		}
		//
		/*	var filters = [
				         new nlobjSearchFilter('formuladate', null, 'notafter',
				        		enDate).setFormula('{startdate}'),
				        new nlobjSearchFilter('formuladate', null, 'notbefore',
				        		stDate).setFormula('{enddate}'),
				        new nlobjSearchFilter('employee', null, 'anyof', employeeID)
				       ];
		// new nlobjSearchFilter('billingstatus', 'timebill', 'is', 'F')

		/*var filter = Array();

		filter.push(new nlobjSearchFilter('employee', null, 'anyof', employeeID));
		filter.push(new nlobjSearchFilter('startdate', null, 'onorbefore', enDate ));//startdate
		filter.push(new nlobjSearchFilter('endate', null, 'onorafter', stDate ));*/ 

		/*	var col = Array();
		col.push(new nlobjSearchColumn('internalid'));
	//	col.push(new nlobjSearchColumn('startdate'));
		//col.push(new nlobjSearchColumn('enddate'));

		var timeSheet_Res = nlapiSearchRecord('timesheet',null,filters,col);*/
		/*		if(timeSheet_Res){
			for(var k=0;k<timeSheet_Res.length;k++){
				var timeSheet_ID = timeSheet_Res[k].getValue('timesheet');
				//	var st_Date_T = timeSheet_Res[k].getValue('startdate');
				//	var end_Date_T = timeSheet_Res[k].getValue('enddate');
				//	st_Date_T = nlapiStringToDate(st_Date_T);
				//	end_Date_T = nlapiStringToDate(end_Date_T);
				//Compare
				//	var st_date_R = nlapiStringToDate(st_date);
				//	var en_date_R = nlapiStringToDate(end_date);

				//
				//	if(st_date_R >= st_Date_T && en_date_R >= end_Date_T  ){
				nlapiLogExecution('debug','TimeSheet Edit' ,timeSheet_ID );
				var loadRecord = nlapiLoadRecord('timesheet',timeSheet_ID);

				var lineCount = loadRecord.getLineItemCount('timeitem');
				for(var m=1;m<=lineCount;m++){

					loadRecord.selectLineItem('timeitem',m);


					for(var n=0;n<7;n++){

						var i_item_id =loadRecord.getCurrentLineItemValue('timeitem','item');
						if(i_item_id == '2222')
						{
							var Obj_timebill_Rec = nlapiSubmitField('timebill', timeSheet_Res[k].getValue("internalid"), ['isbillable'], ['F']);
						}
												var sub_day = days[n];
						var o_sub_record_view = loadRecord.viewCurrentLineItemSubrecord('timeitem', sub_day);
						if(o_sub_record_view){
						var i_item_id = o_sub_record_view.getFieldValue('item');
						if(i_item_id == '2222'){
						var sub_rec = loadRecord.editCurrentLineItemSubrecord('timeitem',sub_day);
						if(sub_rec){
							nlapiLogExecution('debug','Bill Rate ',billRate);
							sub_rec.setFieldText('price','(Custom)');
						sub_rec.setFieldValue('rate',billRate);
						sub_rec.commit();
						}
					}
					}
					}
					loadRecord.commitLineItem('timeitem');
				}

				var id = nlapiSubmitRecord(loadRecord);
				nlapiLogExecution('debug','Submitted ID',id);
				//}
			}
		}*/


	}
	catch(e){

		nlapiLogExecution('debug','Process Error',e);
	}
}