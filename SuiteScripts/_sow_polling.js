/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */
define(["N/record", "N/search", "N/file", "N/format", "./_get_time_lib.js", "N/https"], function (record, search, file, format, utility, https) {
    function _post(context) {
        try {
            var datainObj = context;
            var filterArr = []
            log.debug({
                title: 'datainObj',
                details: datainObj
            });
            var currentDateAndTime = sysDate()
            log.debug({
                title: 'currentDateAndTime',
                details: currentDateAndTime
            });
            var receivedDate = datainObj.lastUpdatedTimestamp;
            log.debug({
                title: 'currentDateAndTime',
                details: currentDateAndTime
            });
            if (receivedDate) {
                filterArr.push(search.createFilter({
                    "name": "lastmodifieddate",
                    "operator": search.Operator.WITHIN,
                    "values": [receivedDate, currentDateAndTime]
                }));
                filterArr.push(search.createFilter({
                    "name": "mainline",
                    "operator": search.Operator.IS,
                    "values": ['T']
                }));
                //"mainline","is","T"], 
            }
            else {
                filterArr.push(search.createFilter({
                    "name": "mainline",
                    "operator": search.Operator.IS,
                    "values": ['T']
                }));
                //"mainline","is","T"], 
            }
            log.debug({
                title: 'filterAr',
                details: filterArr
            })

            var customrecord_taleo_location_masterSearchObj = search.create({
                type: search.Type.SALES_ORDER,
                filters: filterArr,
                columns:
                    [

                        search.createColumn({ name: "tranid", label: "Document Number" }),
                        search.createColumn({ name: "custbody_opp_id_sfdc", label: "Opportunity ID" }),
                        search.createColumn({
                            name: "entityid",
                            join: "jobMain",
                            label: "ID"
                         }),
                        search.createColumn({ name: "statusref", label: "Status" }),
                        search.createColumn({ name: "datecreated", label: "Date Created" }),
                        search.createColumn({ name: "entity", label: "Name" }),
                        search.createColumn({ name: "fxamount", label: "Amount (Foreign Currency)" }),
                        search.createColumn({ name: "currency", label: "Currency" }),
                        search.createColumn({name: "startdate", label: "Start Date"}),
                        search.createColumn({name: "enddate", label: "End Date"})
                    ]
            });
            var searchResultCount = customrecord_taleo_location_masterSearchObj.runPaged().count;
            var dataRow = [];
            var id;
            var oppId;
            var projectId;
            var status;
            var dateCreated;
            var entity;
            var amount;
            var currency;
            var startDate;
            var endDate;
            log.debug("customrecord_taleo_location_masterSearchObj result count", searchResultCount);
            var myResults = getAllResults(customrecord_taleo_location_masterSearchObj);
            myResults.forEach(function(result) {
                var data = {};
                id = result.getValue({
                    name: 'tranid'
                })
                data.tranId = {
                    "name": id
                }
                oppId = result.getValue({
                    name: 'custbody_opp_id_sfdc'
                })
                data.oppId = {
                    "name": oppId
                }
                projectId = result.getValue({
                    name: "entityid",
                    join: "jobMain",
                })
                data.projectId = {
                    "name": projectId
                }
                status = result.getText({
                    name: 'statusref'
                })
                data.status = {
                    "name": status
                }
                dateCreated = result.getValue({
                    name: 'datecreated'
                })
                data.dateCreated = {
                    "name": dateCreated
                }
                entity = result.getText({
                    name: 'entity'
                })
                data.entity = {
                    "name": entity
                }
                amount = result.getValue({
                    name: 'fxamount'
                })
                data.amount = {
                    "name": amount
                }
                currency = result.getText({
                    name: 'currency'
                })
                data.currency = {
                    "name": currency
                }
                startDate = result.getValue({
                    name: 'startdate'
                })
                data.startDate = {
                    "name": startDate
                }
                endDate = result.getValue({
                    name: 'enddate'
                })
                data.endDate= {
                    "name": endDate
                }
                dataRow.push(data)
                return true;
            });
            return {
                timeStamp: currentDateAndTime,
                data: dataRow,
                status: true
            };
        } catch (error) {
            log.debug({
                title: error.name,
                details: error.message
            })
            return {
                timeStamp: '',
                data: error.name,
                status: false
            };
        }
    }
    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({start:searchid,end:searchid+1000});
            resultslice.forEach(function(slice) {
                searchResults.push(slice);
                searchid++;
                }
            );
        } while (resultslice.length >=1000);
        return searchResults;
    }   
    return {
        post: _post,
    }
});
