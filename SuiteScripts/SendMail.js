/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Dec 2017     bidhi.raj
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	var practise = nlapiGetFieldValue('custbody_sow_exc_practise');
	if (practise) {
		try{
		var head = nlapiLookupField('department', practise,
				'custrecord_practicehead')
		var head_mail = nlapiLookupField('employee', head, 'email');
		var emp_id = nlapiLookupField('employee', head, 'internalid');
		var sowId = nlapiGetFieldValue('tranid');
		var customer = nlapiGetFieldText('entity');
		var currency = nlapiGetFieldText('currency');
		var amount = nlapiGetFieldValue('total');
		if(!amount){
			amount = 0;
		}
		
		//var body = 'approved do neeedful';
		var attach = nlapiLoadFile(678275);
		var html = '';
		html += '<html>';
		html += '<head>';
		html += '<table border="0" width="100%">';
		html += '</head>';
		html += '<body>';
		html += '<p>Hi,</p>';
		html += '<p>Please find the SOW details created against your sub practise below.</p>';
		html += '</br>';
		html += '<table border="2" width="100%">';
		html += '<thead>';
		html += '<tr text-align: left padding: 8px><td><b>Sow ID</b></td><td><b>Customer</b></td><td><b>Currency</b></td><td><b>Amount</b></td></tr>';
		html += '</thead>';
		html += '<tr>';
		html += '<td>' + sowId + '</td>';
		html += '<td>' + customer + '</td>';
		html += '<td>' + currency + '</td>';
		html += '<td>' + amount + '</td>';
		html += '</tr>';
		html += '</table>';
		html += '</br>';
		html += '<p>Thanks & Regards,</p>';
		html += '<p>Information Systems</p>';
		html += '</body>';
		html += '</html>';
		
		
		nlapiSendEmail(442, head_mail,  'Sow Created', html,null,null,null,attach, {entity: emp_id});
		//nlapiSendEmail(442, 'bidhi.raj@brillio.com', 'Sow Created', html,null, null, null, attach, true, null, null);
		//nlapiLogExecution('debug', 'practshd', head_mail);
		}catch (e) {
			// TODO: handle exception
			nlapiLogExecution('error', 'error', e);
		}
	}
}
