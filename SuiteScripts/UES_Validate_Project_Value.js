/**
 * @author Nikhil
 */
//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
   	Script Name : UES_Validate_Project_Value.js
	Author      :
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

	 */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================



//BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
//END GLOBAL VARIABLE BLOCK  =======================================





//BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	 */



	//  LOCAL VARIABLES

	//  BEFORE LOAD CODE BODY


	return true;



}

//END BEFORE LOAD ====================================================





//BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord_project_value(type)
{
	try{
		var lineCount = nlapiGetLineItemCount('mediaitem');
		var so_total ;
		if(type!='delete')
		{

			var i_record = nlapiGetRecordId();

			var project_value = nlapiGetFieldValue('jobprice');
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' project_value -->' + project_value);

			var i_customer = nlapiGetFieldText('parent');
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' Customer -->' + i_customer);

			if(i_customer!='COIN-01 Collabera Inc')
			{
				var sow_recordOBJ = search_sow(i_record);
				if (sow_recordOBJ != null && sow_recordOBJ != undefined && sow_recordOBJ != '')
				{
					var so_total = sow_recordOBJ.getFieldValue('total');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  total-->' + so_total);
				}

				if(_logValidation(so_total) && _logValidation(project_value))
				{
					if( parseFloat(so_total) < parseFloat(project_value))
					{
						throw 'Project value should always be equal to or lesser than the SO Value';
						return false;
					}

				}


			}


		}

		/*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


		 */


		//  LOCAL VARIABLES


		//  BEFORE SUBMIT CODE BODY


		return true;
	}
	catch(e)
	{
		nlapiLogExecution("Debug","Error is:",e.message);
	}
}

//END BEFORE SUBMIT ==================================================





//BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
	/*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


	 */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

//END AFTER SUBMIT ===============================================





//BEGIN FUNCTION ===================================================
function search_sow(i_record)
{
	if (sow_recordOBJ != null && sow_recordOBJ != undefined && sow_recordOBJ != '')
	{
		var Filters = new Array();
		var Column  = new Array();

		Filters[0]=new nlobjSearchFilter('internalid','job','is',i_record)

		Column[0]= new nlobjSearchColumn('internalid');
		Column[1]= new nlobjSearchColumn('name');

		var result = nlapiSearchRecord('salesorder', null,Filters,Column);
		nlapiLogExecution('DEBUG','In getResourcerate','search=='+result);

		if(result!=null)
		{
			var internalid = result[0].getValue('internalid');
			nlapiLogExecution('DEBUG','In SO data','internalid=='+internalid);

			var name = result[0].getValue('name');
			nlapiLogExecution('DEBUG','In SO data','name=='+name);

			var sow_recordOBJ = nlapiLoadRecord('salesorder', internalid);
			return sow_recordOBJ;
		}
	}

}//Search Result
//END FUNCTION =====================================================
function _logValidation(value)
{
	if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		return false;
	}
}