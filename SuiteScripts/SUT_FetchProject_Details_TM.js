/*
Author: Deepak MS 25-06-2020
Description:  Fetch Project values
*/

function validateProject(request,response)
{
	try
	{
		var flag = true;
		var i_project = request.getParameter('i_project_interal_id');
		//var i_project = '198248';
		var jobSearch = nlapiSearchRecord("job",null,
[
   ["internalid","anyof",i_project]
], 
[
   new nlobjSearchColumn("email","CUSTENTITY_CLIENTPARTNER",null), 
   new nlobjSearchColumn("email","custentity_projectmanager",null), 
   new nlobjSearchColumn("email","CUSTENTITY_DELIVERYMANAGER",null),
   new nlobjSearchColumn("custentity_account_delivery_owner","customer",null)
]
);
		//var emails = nlapiLookupField('job',parseInt(i_project),
		//['custentity_clientpartner.email','custentity_projectmanager.email','custentity_deliverymanager.email']);
		var e_cp = jobSearch[0].getValue("email","CUSTENTITY_CLIENTPARTNER",null);
		var e_dm = jobSearch[0].getValue("email","custentity_projectmanager",null);
		var e_pm = jobSearch[0].getValue("email","CUSTENTITY_DELIVERYMANAGER",null);
		var i_ado=jobSearch[0].getValue("custentity_account_delivery_owner","customer",null);
		var JsonCategory = {};
		JsonCategory = {
                        CP: e_cp,
                        DM: e_dm,
						PM: e_pm,
						i_ADO:i_ado
                    }
					
      var JSON_Data = [];
			JSON_Data.push(JsonCategory);
//nlapiLogExecution('ERROR','JsonCategory',(JsonCategory));
//nlapiLogExecution('ERROR','JSON_Data',JSON.stringify(JSON_Data));
		response.write(JSON.stringify(JsonCategory));
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','validatePractice','ERROR MESSAGE :- '+err);
	}
	}