

function _test()
{
	var currentContext  = 	nlapiGetContext();
	var load_invoice=nlapiLoadRecord('invoice','1914334');
		var i_line_count=load_invoice.getLineItemCount('time');
		var i_numbers={};
		var i_LineAppliedCount=0;
		for(var i=1;i<=i_line_count;i++)
		{
			//nlapiLogExecution('debug','i',i);
		var i_emp_id=load_invoice.getLineItemValue('time','employee',i);
		var s_billable_date=load_invoice.getLineItemValue('time','billeddate',i);
		if((i_emp_id=="148587" && s_billable_date=="12/23/2019") || (i_emp_id=="148587" && s_billable_date=="12/24/2019"))
		{
			
		 load_invoice.setLineItemValue('time', 'apply', i, 'F');
		 load_invoice.setLineItemValue('time', 'taxcode', i, '');
		 nlapiLogExecution('debug','i_LineAppliedCount',i_LineAppliedCount);
	     i_LineAppliedCount++;
		}
		//yieldScript(currentContext);
}
//var i_recordId =  nlapiSubmitRecord(load_invoice, {disabletriggers : true, enablesourcing : true}); 
		//nlapiLogExecution('debug','i_recordId',i_recordId);
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}