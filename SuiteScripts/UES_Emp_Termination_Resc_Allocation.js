/**
 * Set all resource allocation's End Date to the Last working date whenever the
 * Last working date is entered or changed
 * 
 * Version Date Author Remarks 1.00 28 Jan 2015 Nitish Mishra
 * 2.00 13 Mar 2020 Praveena Madem 		Added logic to call suitelet to execute userevent scripts on resource allocation update dacc
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord employee
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	try {
		if (type == 'edit' || type == 'xedit') {
			var recOldRecord = nlapiGetOldRecord(), oldTerminationDate = recOldRecord
			        .getFieldValue('custentity_lwd'), newTerminationDate = nlapiGetFieldValue('custentity_lwd');

			if (isNotEmpty(newTerminationDate)&& oldTerminationDate != newTerminationDate) {
				//below logic was Added By praveena 
				// Get resource allocations associated to this employee,
				// having allocation end date after the termination date
				nlapiLogExecution('debug', 'newTerminationDate', newTerminationDate);
				var dataobj={"i_employee_id":nlapiGetRecordId(),"newTerminationDate":nlapiGetFieldValue('custentity_lwd')};
				nlapiLogExecution('debug', 'dataobj', JSON.stringify(dataobj));
				var url = nlapiResolveURL('SUITELET', 'customscript_sut_manual_emp_termination', 'customdeploy1',true);
				var response_data = nlapiRequestURL(url,JSON.stringify(dataobj), {"content-type": 'application/json'}, 'POST');//Call Suitelet
                nlapiLogExecution('debug', 'SUITLET END DATE', new Date());
				var obj_responsebody = JSON.parse(response_data.getBody());
				nlapiLogExecution('debug', 'response_data.getBody()',response_data.getBody());
				nlapiLogExecution('debug', 'obj_responsebody',obj_responsebody);
				if (response_data.getCode()=="200" && obj_responsebody.success=='T') {
				}
				else{
				var resultedata = "Code:"+ obj_responsebody.Code+'  Details:'+obj_responsebody.Details+' '+ 'Employee ID:- ' + ' ' + nlapiGetFieldValue('custentity_fusion_empid');
				throw resultedata;
				
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'Before Submit', err);
		throw err;
	}
}

function userEventAfterSubmit(type){
	try{
		if (type == 'edit' || type == 'xedit') {
			
			var recOldRecord = nlapiGetOldRecord();
			var old__emp_inactive = recOldRecord.getFieldValue('custentity_employee_inactive');
			
			var old_inactive_Reason = recOldRecord.getFieldValue('custentity_inactivereason'); // 2 => Global Transfer  7 => Global Transfer Revert
			
			var emp_record_id = nlapiGetRecordId();
			
			var loadRec = nlapiLoadRecord(nlapiGetRecordType(), emp_record_id);
			var latest_emp_inactive = loadRec.getFieldValue('custentity_employee_inactive');
          var latest_inactive_Reason = loadRec.getFieldValue('custentity_inactivereason');
			var s_lwd = loadRec.getFieldValue('custentity_lwd');
			var i_subsidiary = loadRec.getFieldValue('subsidiary');
			
			nlapiLogExecution('ERROR', 'old__emp_inactive', old__emp_inactive);
          nlapiLogExecution('ERROR', 'latest_emp_inactive', latest_emp_inactive);
          nlapiLogExecution('ERROR', 'old_inactive_Reason', old_inactive_Reason);
          nlapiLogExecution('ERROR', 'latest_inactive_Reason', loadRec.getFieldValue('custentity_inactivereason'));
			
			if(latest_emp_inactive == 'T' && old__emp_inactive == 'F'){
				
				//search for employee ctc data
				var cost_per_fte_search = nlapiSearchRecord("customrecord_cost_per_fte",null,
						[
						   ["custrecord_cost_p_fte_employee","anyof",nlapiGetRecordId()], 
						   "AND", 
						   ["custrecord_cost_p_fte_legal_entity","anyof",i_subsidiary], 
						   "AND", 
						   ["custrecord_cost_p_fte_end_date","onorafter",nlapiStringToDate(s_lwd)], 
						   "AND", 
						   ["custrecord_cost_p_fte_employee.custentity_employee_inactive","is","T"], 
						   "AND", 
						   ["custrecord_cost_p_fte_employee.custentity_implementationteam","is","F"]
						], 
						[
						   new nlobjSearchColumn("internalid"), 
						   new nlobjSearchColumn("custrecord_cost_p_fte_currency_code"), 
						   new nlobjSearchColumn("custrecord_cost_p_fte_start_date"), 
						   new nlobjSearchColumn("custrecord_cost_p_fte_end_date").setSort(true), 
						   new nlobjSearchColumn("custrecord_cost_p_fte_legal_entity")
						]
						);
				if(_logValidation(cost_per_fte_search))
				{					
					
					for(var i = 0; i < cost_per_fte_search.length; i++){
						var i_recID = cost_per_fte_search[i].getValue('internalid');
						var s_startDate = cost_per_fte_search[i].getValue('custrecord_cost_p_fte_start_date');
						var s_endDate = cost_per_fte_search[i].getValue('custrecord_cost_p_fte_end_date');
						
						if(nlapiStringToDate(s_startDate)> nlapiStringToDate(s_lwd) && nlapiStringToDate(s_endDate)> nlapiStringToDate(s_lwd)){
							var id = nlapiDeleteRecord('customrecord_cost_per_fte',i_recID);
							nlapiLogExecution('DEBUG','Deleted Record ID',id);
						}
						else if(nlapiStringToDate(s_endDate)>= nlapiStringToDate(s_lwd) && nlapiStringToDate(s_startDate) <= nlapiStringToDate(s_lwd)){
							var id = nlapiSubmitField('customrecord_cost_per_fte',i_recID,'custrecord_cost_p_fte_end_date',s_lwd);
							nlapiLogExecution('DEBUG','Processed Record ID',id);
						}
						
					}
				}
			}
			
			if((old_inactive_Reason == 2 ) && (latest_emp_inactive == "F")){
				
				var emp_fusion_id = loadRec.getFieldValue('custentity_fusion_empid');
				
				if(_logValidation(emp_fusion_id)){
					
					
					globalTransferEmployeeTagging(emp_fusion_id, emp_record_id);
					
					
				}
				
			}				
				
				
		
	}
	
	if(type == 'create'){
		
		var  creation_reason = nlapiGetFieldValue('custentity_inactivereason');
					nlapiLogExecution('ERROR', 'creation_reason ON Create Rec', creation_reason);
       
		if(creation_reason == '7'){ // 7 =>> Global Transfer Revert
		
		      var emp_record_id = nlapiGetRecordId();
			  
			  var emp_fusion_id = nlapiGetFieldValue('custentity_fusion_empid');
			  
			  globalTransferEmployeeTagging(emp_fusion_id, emp_record_id);
			
			
		}
		
	}
	
}
	catch (err) {
		nlapiLogExecution('ERROR', 'After Submit', err);
		
	}
}
		
function _logValidation(value) 
{
			if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
			{
				return true;
			}
			else 
			{ 
				return false;
			}
}

function globalTransferEmployeeTagging(fusionId, emp_record_id){
	
	var emp_experience_search = nlapiSearchRecord("customrecord_fu_emp_experience_data",null,
						[
						   ["custrecord_fu_emp_name.custentity_fusion_empid","is",fusionId], 						   
						   "AND", 
						   ["custrecord_fu_emp_name.custentity_implementationteam","is","F"]
						], 
						[
						   new nlobjSearchColumn("internalid"),
						   new nlobjSearchColumn("custrecord_fu_emp_name")						   
						]
						);
				if(_logValidation(emp_experience_search)){
					
					for(var i = 0; i < emp_experience_search.length; i++){
						
						var rec_id = emp_experience_search[i].getId();
						
						nlapiSubmitField('customrecord_fu_emp_experience_data',rec_id,'custrecord_fu_emp_name',emp_record_id);
					}
					
					
				}
	
}