/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Apr 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var rec	=	nlapiLoadRecord('invoice', recId);
	var invoice_number	=	rec.getFieldValue('tranid');
	var invoice_date	=	rec.getFieldValue('trandate');
	var invoice_billing_start_date	=	rec.getFieldValue('custbody_billfrom');
	var invoice_billing_end_date	=	rec.getFieldValue('custbody_billto');
	var i_billing_item_count	=	rec.getLineItemCount('time');
	var f_invoice_amount	=	rec.getFieldValue('total');
	
	var str_csv_line	=	'Invoice Internal ID, Invoice Number, Invoice Date, Billing Start Date, Billing End Date, Employee, Quantity, Bill Rate, TimeEntry Date, Invoice Amount\n';
	
	for(var i = 1; i <= i_billing_item_count; i++)
		{
			var apply = rec.getLineItemValue('time', 'apply', i);
			if (apply == 'T') 
			{
				var qty = rec.getLineItemValue('time', 'qty', i);
				var employee = rec.getLineItemValue('time', 'employee', i);
				var billed_date = rec.getLineItemValue('time', 'billeddate', i);
				var bill_rate = rec.getLineItemValue('time', 'rate', i);
				str_csv_line += [recId, invoice_number, invoice_date,invoice_billing_start_date, invoice_billing_end_date,
					employee, qty, bill_rate, billed_date, f_invoice_amount].toString() + '\n';
			}
		}
	
	var objFile	=	nlapiLoadFile(20017);
	
	var newFile = nlapiCreateFile('invoice_data.csv', 'CSV', objFile.getValue() + str_csv_line); 
	newFile.setFolder(6213);//csvFolderId); 
	newFile.setEncoding('UTF-8'); 
	nlapiSubmitFile(newFile);
	
	
	//var newObjFile	=	new nlob
}
