/**
 * @author Jayesh
 */

function vendor_aging_report(request,response)
{
	try
	{
		
		var form_obj = nlapiCreateForm("Vendor Aging Report");
		
		if (request.getMethod() == 'GET')
		{
				var fld_start_date = form_obj.addField('custpage_vendor', 'select', 'Vendor', 'vendor');
			var fld_start_date = form_obj.addField('custpage_st_date', 'date', 'Start Date');
			var fld_end_date = form_obj.addField('enddate', 'date', 'End Date');
			//var Gbp_Inr = form_obj.addField('gbptoinr', 'currency', 'GBP TO INR');//.setMandatory(true);
			//var Usd_Inr = form_obj.addField('usdtoinr', 'currency', 'USD TO INR');//.setMandatory(true);
			//var Usd_Inr = form_obj.addField('usdtoinr', 'currency', 'USD TO INR');//.setMandatory(true);
			form_obj.addSubmitButton('Export Report');
			
		}
		
		if (request.getMethod() == 'POST')
		{
			var bill_data_arr = new Array();
			var bill_unique_list = new Array();
			var sr_no = 0;
			
			var i_vendor = request.getParameter('custpage_vendor');
          	var s_st_date = request.getParameter('custpage_st_date');
			var s_end_date = request.getParameter('enddate');
			
			//var exchangeamtusd = request.getParameter('usdtoinr');
			//var exchangeamtgbp = request.getParameter('gbptoinr');
			
		//	var d_start_date = nlapiStringToDate(s_start_date, 'date');
		//	var d_end_date = nlapiStringToDate(s_end_date, 'date');
			var vendor_actice_array = new Array();			
			//if(context.getRemainingUsage()< 100)
			{
				var params = new Array();
				
				//var o_field = form_obj.addField('dispalymessage', 'inlinehtml', 'Message');
				//o_field.setDefaultValue(strVar);
				//var d_start_date = nlapiStringToDate(s_start_date, 'date');
				//var d_end_date = nlapiStringToDate(s_end_date, 'date');
				params['custscript_vendor'] = i_vendor;
				params['custscript_start_date'] = s_st_date;
				params['custscript_end_date'] = s_end_date;
				//params['custscript_gbp_inr'] = exchangeamtgbp;
				//params['custscript_usd_inr'] = exchangeamtusd;
				nlapiScheduleScript('customscript_sch_send_email_vb_aging_rep', 'customdeploy1', params);
				
			}
			var field = form_obj.addField('dispalymessage', 'inlinehtml');
			var strVar="";
			strVar += "<html>";
			strVar += "<p>Email will be sent to you shortly.<\/p>";
			strVar += "<\/html>";
			field.setDefaultValue(strVar);

		}
		
		response.writePage(form_obj);
		
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
		
	}
}

