/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 25 Mar 2015 amol.sahijwani
 * 
 */

/**
 * @param {nlobjRequest}
 *        request Request object
 * @param {nlobjResponse}
 *        response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	
	if(request.getMethod() == 'POST')
		{
			var i_script_id	=	(nlapiGetContext()).getScriptId();
			var i_deployment_id	=	(nlapiGetContext()).getDeploymentId();
		
			response.sendRedirect('SUITELET', i_script_id, i_deployment_id, false, {'custpage_year': request.getParameter('custpage_year')});
			
			//var url = nlapiResolveURL('SUITELET', i_script_id, i_deployment_id);
			
			//nlapiRequestURL(url);
		}
	
	if(request.getMethod() == 'GET' && request.getParameter('custpage_year') == null)
	{
		var form = nlapiCreateForm('Revenue Trend Report');
		
		// Add From Date Field
		var fld_year =
				form.addField('custpage_year', 'select', 'Select Year').setMandatory(true);
		
		fld_year.addSelectOption('2015', '2015', false);
		fld_year.addSelectOption('2016', '2016', true);
		fld_year.addSelectOption('2017', '2017', false);
		fld_year.addSelectOption('2018', '2018');
		fld_year.addSelectOption('2019', '2019');
		
		form.addSubmitButton('Load Data');
		
		response.writePage(form);
	}
	
	if(request.getParameter('custpage_year') != null)
		{
			nlapiLogExecution('audit', 'Accessed By');
			try {

				var s_year	=	request.getParameter('custpage_year');
				
				var filters = new Array();
				filters[0] =
						new nlobjSearchFilter('custrecord_rev_rep_data_upd_log_status', null, 'is', 'Done')
	
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_rev_rep_data_update_date').setSort(true);
	
				var search_result_last_updated =
						nlapiSearchRecord('customrecord_rev_rep_data_update_log', null, filters, columns);
				var s_last_updated = '';
				if (search_result_last_updated != null) {
					s_last_updated =
							search_result_last_updated[0].getValue('custrecord_rev_rep_data_update_date');
				}
				// Instantiate a report definition to work with
				var reportDefinition = nlapiCreateReportDefinition();
	
				// Create the search to feed the report
				var columns = new Array();
	
				// Add search to the report and map the search columns to the reports
				// columns
				columns[0] = new nlobjSearchColumn('custrecord_rev_rep_data_vertical', null, 'group');
				columns[1] = new nlobjSearchColumn('formulatext', null, 'group');
				columns[1].setFormula("CASE WHEN {custrecord_rev_rep_data_end_customer} IS NULL OR {custrecord_rev_rep_data_end_customer} = '' THEN {custrecord_rev_rep_data_customer} ELSE {custrecord_rev_rep_data_customer} || '(' || {custrecord_rev_rep_data_end_customer} || ')' END");
				columns[2] = new nlobjSearchColumn('custrecord_rev_rep_data_project', null, 'group');
				columns[3] = new nlobjSearchColumn('custrecord_rev_rep_data_employee', null, 'group');
				columns[4] =
						new nlobjSearchColumn('custrecord_rev_rep_data_parent_practice', null, 'group');
				columns[5] = new nlobjSearchColumn('custrecord_rev_rep_data_total_amount', null, 'sum');
				columns[6] = new nlobjSearchColumn('formulatext', null, 'group');
				columns[6]
						.setFormula("TO_CHAR(TO_DATE({custrecord_rev_rep_data_year} || '-' || {custrecord_rev_rep_data_month} || '-01', 'YYYY-mm-dd'), 'Month') || {custrecord_rev_rep_data_year}");
				columns[7] =
						new nlobjSearchColumn('custrecord_rev_rep_data_year', null, 'group').setSort(false);
				columns[8] = new nlobjSearchColumn('custrecord_rev_rep_data_practice', null, 'group');
				columns[9] =
						new nlobjSearchColumn('custrecord_rev_rep_data_month', null, 'group')
								.setSort(false);
	
	
				var deployment = nlapiGetContext().getDeploymentId();
				var report_access = getUserReportAccessList();
	
				if (deployment == "customdeploy_rev_rep_trend_vertical") {
					var filters = new Array();
					filters[0] =
							new nlobjSearchFilter('custrecord_rev_rep_data_year', null, 'equalto', s_year);
					filters.push(new nlobjSearchFilter('formulatext', null, 'is', 'T')
							.setFormula(getVerticalListFormula(report_access.VerticalList.Texts)));
	
					// Define the rows/column hierarchy and the actual column data
					var vertical =
							reportDefinition.addRowHierarchy('custrecord_rev_rep_data_vertical',
									'Vertical', 'TEXT');
	
					var customer =
							reportDefinition.addRowHierarchy('custrecord_rev_rep_data_customer',
									'Customer', 'TEXT');
				}
				else if (deployment == "customdeploy_rev_rep_trend_practice") {
					var filters =
							[
								new nlobjSearchFilter('internalid', null, 'anyof',
										getDepartmentList(report_access.PracticeList.Values, s_year))
							];
	
					var practice =
							reportDefinition.addRowHierarchy('custrecord_rev_rep_data_parent_practice',
									'Practice', 'TEXT');
	
					var practice =
							reportDefinition.addRowHierarchy('custrecord_rev_rep_data_practice',
									'Sub Practice', 'TEXT');
	
					var customer =
							reportDefinition.addRowHierarchy('custrecord_rev_rep_data_customer',
									'Customer', 'TEXT');
				}
				else if (deployment == "customdeploy_sut_rev_rep_trend_customer") {
	
					var filters =
							[
								new nlobjSearchFilter('internalid', null, 'anyof',
										getCustomerFormula(report_access.CustomerList, s_year))
							];
	
					var customer =
							reportDefinition.addRowHierarchy('custrecord_rev_rep_data_customer',
									'Customer', 'TEXT');
	
					var practice =
						reportDefinition.addRowHierarchy('custrecord_rev_rep_data_parent_practice',
								'Practice', 'TEXT');
	
					var practice =
						reportDefinition.addRowHierarchy('custrecord_rev_rep_data_practice',
								'Sub Practice', 'TEXT');
				}
				else {
					throw "Access Not Allowed";
				}
	
	
				var project =
						reportDefinition.addRowHierarchy('custrecord_rev_rep_data_project', 'Project',
								'TEXT');
				var employee =
						reportDefinition.addRowHierarchy('custrecord_rev_rep_data_employee', ' ', 'TEXT');
	
				var entstat =
						reportDefinition.addColumnHierarchy('custrecord_rev_rep_data_month', 'Month', null,
								'TEXT');
				var total =
						reportDefinition.addColumn('custrecord_rev_rep_data_total_amount', true, ' ',
								entstat, 'CURRENCY', null);
	
				reportDefinition.addSearchDataSource('customrecord_revenue_report_data', null, filters,
						columns, {
							'custrecord_rev_rep_data_vertical' : columns[0],
							'custrecord_rev_rep_data_customer' : columns[1],
							'custrecord_rev_rep_data_project' : columns[2],
							'custrecord_rev_rep_data_employee' : columns[3],
							'custrecord_rev_rep_data_parent_practice' : columns[4],
							'custrecord_rev_rep_data_total_amount' : columns[5],
							'custrecord_rev_rep_data_month' : columns[6],
							'custrecord_rev_rep_data_year' : columns[7],
							'custrecord_rev_rep_data_practice' : columns[8]
						});
	
				// Create a form to build the report on
				var form =
						nlapiCreateReportForm('Revenue Trend Report - Jan ' + s_year + ' to Dec ' + s_year + ' (Last Updated on '
								+ s_last_updated + ')');
	
				// Build the form from the report definition
				var pvtTable = reportDefinition.executeReport(form);
	
				// Write the form to the browser
				response.writePage(form);
			}
			catch (err) {
				displayErrorForm(err);
			}
		}
	
}

function getCustomerFormula(customerList, s_year) {

	try {

		// check if no customer specified
		if (isArrayEmpty(customerList)) {
			throw "No customer specified";
		}

		// create a list of all the practices to be searched
		var a_department_value_list = [];
		customerList.forEach(function(customer) {

			customer.Practice.forEach(function(practice) {

				a_department_value_list.push(practice);
			});
		});

		// search on department record to get the department - sub
		// department list
		var dept_column =
				new nlobjSearchColumn('formulatext')
						.setFormula("CASE WHEN INSTR({name} , ' : ', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , ' : ', 1)) ELSE {name} END");
		var sub_dept_column =
				new nlobjSearchColumn('formulatext')
						.setFormula("case when {name} = {namenohierarchy} then '' else {namenohierarchy} end");

		var department_search = nlapiSearchRecord('department', null, [
			new nlobjSearchFilter('internalid', null, 'anyof', a_department_value_list)
		], [
			dept_column, sub_dept_column
		]);

		// create a hash table for dept - sub dept
		var department_list = [];
		department_search.forEach(function(dept) {

			if (dept.getValue(sub_dept_column)) {
				department_list[dept.getId()] = {
					Parent : dept.getValue(dept_column),
					Child : dept.getValue(sub_dept_column)
				};
			}
		});

		// create the filter for the revenue report data record (customer ,
		// practice, sub-practice )
		var revenue_record_customer_filter = [];

		for (var i = 0; i < customerList.length; i++) {

			for (var j = 0; j < customerList[i].Practice.length; j++) {
				var practiceId = customerList[i].Practice[j];

				if (department_list[practiceId]) {
					var sub_dept_level_filter =
							[
								[
									'custrecord_rev_rep_data_customer', 'is',
									customerList[i].CustomerText
								],
								'and',
								[
									'custrecord_rev_rep_data_parent_practice', 'is',
									department_list[practiceId].Parent.trim()
								],
								'and',
								[
									'custrecord_rev_rep_data_practice', 'is',
									department_list[practiceId].Child
								]
							];

					revenue_record_customer_filter.push(sub_dept_level_filter);

					if (j < customerList[i].Practice.length - 1) {
						revenue_record_customer_filter.push('or');
					}
				}
			}
		}




		var record_id_list = [];

		if (isArrayNotEmpty(revenue_record_customer_filter)) {
			var revenue_record_filter = [
				revenue_record_customer_filter, 'and', [
					'custrecord_rev_rep_data_year', 'equalto', s_year
				]
			];

			var revenue_data_record_search =
					searchRecord('customrecord_revenue_report_data', null, revenue_record_filter);

			// create a list of all the revenue data record internal id
			// list to be passed as filter to report generator
			if (isArrayNotEmpty(revenue_data_record_search)) {

				revenue_data_record_search.forEach(function(rec) {

					record_id_list.push(rec.getId());
				});
			}
		}


		record_id_list.push('@NONE@');
		return record_id_list;
	}
	catch (err) {
		nlapiLogExecution('error', 'getCustomerList', err);
		throw err;
	}
}

function getDepartmentList(a_department_value_list, s_year) {

	try {

		if (isArrayEmpty(a_department_value_list)) {
			throw "No department specified";
		}

		// search on department record to get the department - sub
		// department list
		var dept_column =
				new nlobjSearchColumn('formulatext')
						.setFormula("CASE WHEN INSTR({name} , ' : ', 1) > 0 THEN SUBSTR({name}, 1 , INSTR({name} , ' : ', 1)) ELSE {name} END");
		var sub_dept_column =
				new nlobjSearchColumn('formulatext')
						.setFormula("case when {name} = {namenohierarchy} then '' else {namenohierarchy} end");

		var department_search = nlapiSearchRecord('department', null, [
			new nlobjSearchFilter('internalid', null, 'anyof', a_department_value_list)
		], [
			dept_column, sub_dept_column
		]);

		var filter = [];
		if (department_search) {

			for (var i = 0; i < department_search.length; i++) {
				var parent_dept =
						[
							'custrecord_rev_rep_data_parent_practice', 'is',
							department_search[i].getValue(dept_column).trim()
						];

				var sub_dept =
						[
							'custrecord_rev_rep_data_practice', 'is',
							department_search[i].getValue(sub_dept_column).trim()
						];

				filter.push([
					parent_dept, 'and', sub_dept
				]);

				if (i + 1 < department_search.length) {
					filter.push('or');
				}
			}
		}

		var search_filter = [
			filter, 'and', [
				'custrecord_rev_rep_data_year', 'equalto', s_year
			]
		];

		var search = searchRecord('customrecord_revenue_report_data', null, search_filter);

		if (search) {
			var new_filter = [];

			search.forEach(function(rec) {

				new_filter.push(rec.getId());
			});

			return new_filter;
		}
		return null;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentList', err);
		throw err;
	}
}

function getVerticalListFormula(a_vertical_text_list) {

	try {
		if (isArrayEmpty(a_vertical_text_list)) {
			throw "No Verticals Specified";
		}

		var count_vertical = a_vertical_text_list.length;

		var filter = "Case when {custrecord_rev_rep_data_vertical} = any(";

		for (var i = 0; i < count_vertical; i++) {
			if (i > 0) {
				filter += ",";
			}

			filter += "'" + a_vertical_text_list[i] + "'";
		}

		filter += ") then 'T' else 'F' end";
		return filter;
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getVerticalList', err);
		throw err;
	}
}
