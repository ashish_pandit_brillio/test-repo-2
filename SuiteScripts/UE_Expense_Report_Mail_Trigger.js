/**
 * Send email notification for expense report
 * 
 * Version Date Author Remarks 1.00 11 Feb 2015 nitish.mishra
 * 
 */

function userEventAfterSubmit(type) {

	try {
		nlapiLogExecution('debug', 'type', type);
		if (type == "create") {
			var expenseId = nlapiGetRecordId();
			nlapiLogExecution('debug','expenseId',expenseId);
			if (nlapiLookupField('expensereport', expenseId, 'statusRef') == 'pendingSupApproval') {
				sendMailsForCreate(expenseId);
			}
		}

		if (type == "edit" || type == "approve") {
			var expenseId = nlapiGetRecordId();
			// ndMailsForCreate(expenseId);
			sendMailsForEdit(expenseId);
		}

		if (type == "reject") {
			// var expenseId = nlapiGetRecordId();
			// sendMailsForReject(expenseId);
			// sendMailsForEdit(expenseId);
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}
function sendMailsForCreate(expenseId) {

	try {

		var expenseDetails = getExpenseDetails(expenseId);
		nlapiLogExecution('debug', 'expense details', JSON
		        .stringify(expenseDetails));

		// for employee
		var employeeMailContent = employeeExpenseSubmittedMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

		// for 1st level approver
		sendExpenseApproverMails(expenseDetails, expenseId);
		/*
		 * var firstApproverMailContent =
		 * firstApproverExpenseSubmittedMailTemplate(expenseDetails);
		 * nlapiSendEmail(constant.Mail_Author.InformationSystem,
		 * expenseDetails.FirstLevelApprover.email,
		 * firstApproverMailContent.Subject, firstApproverMailContent.Body,
		 * null, null, { entity : expenseDetails.FirstLevelApprover.internalid,
		 * transaction : expenseId });
		 */

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForCreate', err);
	}
}

function sendExpenseApproverMails(expenseDetails, expenseId) {

	try {

		// Client Partner

		try {
			for (var i = 0; i < expenseDetails.ClientPartnerList.length; i++) {
				var clientPartnerEmailContent = clientPartnerExpenseSubmittedMailTemplate(
				        expenseDetails, expenseDetails.ClientPartnerList[i]);

				nlapiSendEmail(
				        constant.Mail_Author.InformationSystem,
				        expenseDetails.ClientPartnerList[i].email,
				        clientPartnerEmailContent.Subject,
				        clientPartnerEmailContent.Body,
				        null,
				        null,
				        {
				            entity : expenseDetails.ClientPartnerList[i].internalid,
				            transaction : expenseId
				        });
			}

			nlapiLogExecution('debug', 'email send to client partner');
		} catch (e) {
			nlapiLogExecution('error', 'email send to client partner', e);
		}

		// Delivery Manager Email

		// no delivery manager in any project
		if (expenseDetails.DeliveryManagerList.length < 1) {

			var firstApproverMailContent = firstApproverExpenseSubmittedMailTemplate(expenseDetails);
			nlapiSendEmail(constant.Mail_Author.InformationSystem,
			        expenseDetails.FirstLevelApprover.email,
			        firstApproverMailContent.Subject,
			        firstApproverMailContent.Body, null, null, {
			            entity : expenseDetails.FirstLevelApprover.internalid,
			            transaction : expenseId
			        });
			// only one delivery manager
		} else if (expenseDetails.DeliveryManagerList.length == 1) {

			// if same as expense approver send only one mail
			if (expenseDetails.DeliveryManagerList[0].internalid == expenseDetails.FirstLevelApprover.internalid) {
				var firstApproverMailContent = firstApproverExpenseSubmittedMailTemplate(expenseDetails);
				nlapiSendEmail(
				        constant.Mail_Author.InformationSystem,
				        expenseDetails.FirstLevelApprover.email,
				        firstApproverMailContent.Subject,
				        firstApproverMailContent.Body,
				        null,
				        null,
				        {
				            entity : expenseDetails.FirstLevelApprover.internalid,
				            transaction : expenseId
				        });
			} else {// else CC to expense approver
				var firstApproverMailContent = firstApproverExpenseSubmittedMailTemplate(expenseDetails);
				nlapiSendEmail(
				        constant.Mail_Author.InformationSystem,
				        expenseDetails.FirstLevelApprover.email,
				        firstApproverMailContent.Subject,
				        firstApproverMailContent.Body,
				        null,
				        null,
				        {
				            entity : expenseDetails.FirstLevelApprover.internalid,
				            transaction : expenseId
				        });

				var deliveryManagerEmailContent = deliveryManagerExpenseSubmittedMailTemplate(
				        expenseDetails, expenseDetails.DeliveryManagerList[0]);

				nlapiSendEmail(
				        constant.Mail_Author.InformationSystem,
				        expenseDetails.DeliveryManagerList[0].email,
				        deliveryManagerEmailContent.Subject,
				        deliveryManagerEmailContent.Body,
				        null,
				        null,
				        {
				            entity : expenseDetails.DeliveryManagerList[0].internalid,
				            transaction : expenseId
				        });
				nlapiLogExecution('DEBUG','DM email->',expenseDetails.DeliveryManagerList[0].email)		
			}

		} else {
			// send all item mail to expense approver
			var firstApproverMailContent = firstApproverExpenseSubmittedMailTemplate(expenseDetails);
			nlapiSendEmail(constant.Mail_Author.InformationSystem,
			        expenseDetails.FirstLevelApprover.email,
			        firstApproverMailContent.Subject,
			        firstApproverMailContent.Body, null, null, {
			            entity : expenseDetails.FirstLevelApprover.internalid,
			            transaction : expenseId
			        });

			// take a delivery manager, get his email template and send mails
			expenseDetails.DeliveryManagerList
			        .forEach(function(deliveryManager) {

				        var deliveryManagerEmailContent = deliveryManagerExpenseSubmittedMailTemplate(
				                expenseDetails, deliveryManager);

				        nlapiSendEmail(constant.Mail_Author.InformationSystem,
				                deliveryManager.email,
				                deliveryManagerEmailContent.Subject,
				                deliveryManagerEmailContent.Body,
				                expenseDetails.FirstLevelApprover.email, null,
				                {
				                    entity : deliveryManager.internalid,
				                    transaction : expenseId
				                });
						nlapiLogExecution('DEBUG','DM email->',expenseDetails.DeliveryManagerList[0].email)		
			        });
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendExpenseApproverMails', err);
	}
}

function sendMailsForReject(expenseId) {

	try {

		var oldRecord = nlapiGetOldRecord();
		var newStatus = nlapiLookupField('expensereport', nlapiGetRecordId(),
		        'statusRef');
		nlapiLogExecution('debug', 'old status', oldRecord
		        .getFieldValue('statusRef'));
		nlapiLogExecution('debug', 'new status', newStatus);

		// check if the status has changed
		if (newStatus != oldRecord.getFieldValue('statusRef')) {

			switch (newStatus) {

				case "inProgress":
				break;
				case "pendingSupApproval":
					sendMailsForCreate(expenseId);
				break;
				case "pendingAcctApproval":
					sendMailsForFirstLevelApproval(expenseId);
				break;
				case "rejectedBySup":
					sendMailsForFirstLevelRejection(expenseId);
				break;
				case "approvedByAcct":
					sendMailsForSecondLevelApproval(expenseId);
				break;
				case "rejectedByAcct":
					sendMailsForSecondLevelRejection(expenseId);
				break;
				case "rejectedByAcctOverride":
					sendMailsForSecondLevelRejectionOverride(expenseId);
				break;
				case "paidInFull":
					sendMailsForPaidInFull(expenseId);
				break;
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForReject', err);
	}
}

function sendMailsForEdit(expenseId) {

	try {

		var oldRecord = nlapiGetOldRecord();
		var newStatus = nlapiLookupField('expensereport', nlapiGetRecordId(),
		        'statusRef');
		var oldStatus = oldRecord.getFieldValue('statusRef');
		nlapiLogExecution('debug', 'old status', oldStatus);
		nlapiLogExecution('debug', 'new status', newStatus);

		// check if the status has changed
		if (newStatus != oldStatus) {

			switch (newStatus) {

				case "inProgress":
				break;
				case "pendingSupApproval":

					if (oldStatus == "inProgress") {
						sendMailsForCreate(expenseId);
					}
				break;
				case "pendingAcctApproval":

					if (oldStatus == "pendingSupApproval") {
						sendMailsForFirstLevelApproval(expenseId);
					}
				break;
				case "rejectedBySup":
					sendMailsForFirstLevelRejection(expenseId);
				break;
				case "approvedByAcct":

					if (oldStatus == "pendingAcctApproval") {
						sendMailsForSecondLevelApproval(expenseId);
					}
				break;
				case "rejectedByAcct":
					sendMailsForSecondLevelRejection(expenseId);
				break;
				case "rejectedByAcctOverride":
					sendMailsForSecondLevelRejectionOverride(expenseId);
				break;
				case "paidInFull":

					if (oldStatus == "approvedByAcct") {
						sendMailsForPaidInFull(expenseId);
					}
				break;
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForEdit', err);
	}
}

function sendMailsForFirstLevelRejection(expenseId) {

	try {
		var expenseDetails = getExpenseDetails(expenseId);

		// for employee
		var employeeMailContent = supervisorRejectionMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForFirstLevelRejection', err);
	}
}

function sendMailsForSecondLevelRejection(expenseId) {

	try {
		var expenseDetails = getExpenseDetails(expenseId);

		// for employee
		var employeeMailContent = financeRejectionMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForSecondLevelRejection', err);
	}
}

function sendMailsForSecondLevelRejectionOverride(expenseId) {

	try {
		var expenseDetails = getExpenseDetails(expenseId);

		// for employee
		var employeeMailContent = financeOverridenRejectionMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForSecondLevelRejectionOverride',
		        err);
	}
}

function sendMailsForPaidInFull(expenseId) {

	try {
		var expenseDetails = getExpenseDetails(expenseId);

		// for employee
		var employeeMailContent = paidInFullMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForPaidInFull', err);
	}
}

function sendMailsForFirstLevelApproval(expenseId) {

	try {
		var expenseDetails = getExpenseDetails(expenseId);

		// for employee
		var employeeMailContent = employeeExpenseFirstLevelApprovalMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

		// for 2nd level approver
		 var secondApproverMailContent =
		 secondApproverExpenseFirstLevelApprovalMailTemplate(expenseDetails);
		 nlapiSendEmail(constant.Mail_Author.InformationSystem,
		 expenseDetails.SecondLevelApprover.email,
		 secondApproverMailContent.Subject,
		 secondApproverMailContent.Body, null, null, {
		 entity : expenseDetails.SecondLevelApprover.internalid,
		 transaction : expenseId
		 });

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForFirstLevelApproval', err);
	}
}

function sendMailsForSecondLevelApproval(expenseId) {

	try {
		var expenseDetails = getExpenseDetails(expenseId);

		// for employee
		var employeeMailContent = employeeExpenseSecondLevelApprovalMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendMailsForSecondLevelApproval', err);
	}
}

function getDocumentReceivedDate(expenseId) {
	try {
		var receivedDate = '';

		var searchResult = nlapiSearchRecord('expensereport', 1039,
		        new nlobjSearchFilter('internalid', null, 'anyof', expenseId));

		if (searchResult) {
			receivedDate = searchResult[0].getValue('date', 'systemnotes',
			        'max');

			receivedDate = nlapiDateToString(nlapiStringToDate(receivedDate),
			        'date');
		}

		return receivedDate;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDocumentReceivedDate', err);
		throw err;
	}
}

function getExpenseDetails(expenseId) {

	try {
		var expenseReportRec = nlapiLoadRecord('expensereport', expenseId);

		var expenseDetails = {};

		// employee details
		var employeeDetails = nlapiLookupField('employee', expenseReportRec
		        .getFieldValue('entity'), [ 'firstname', 'lastname', 'email',
		        'subsidiary' ]);
		employeeDetails.id = expenseReportRec.getFieldText('entity');
		employeeDetails.internalid = expenseReportRec.getFieldValue('entity');

		var employeeDetailsText = nlapiLookupField('employee', expenseReportRec
		        .getFieldValue('entity'), [ 'department' ], true);

		// employeeDetails.vertical = employeeDetailsText;
		employeeDetails.department = employeeDetailsText.department;
		// employeeDetails.subsidiary = employeeDetailsText.subsidiary;

		expenseDetails.Employee = employeeDetails;
		nlapiLogExecution('debug', 'employee', JSON.stringify(employeeDetails));

		// 1st level approver details
		var firstApproverId = expenseReportRec
		        .getFieldValue(constant.ExpenseReport.FirstLevelApprover);

		if (isNotEmpty(firstApproverId)) {
			var firstApprover = nlapiLookupField('employee', firstApproverId, [
			        'firstname', 'lastname', 'email' ]);
			firstApprover.id = expenseReportRec
			        .getFieldText(constant.ExpenseReport.FirstLevelApprover);
			firstApprover.internalid = firstApproverId;

			expenseDetails.FirstLevelApprover = firstApprover;
			nlapiLogExecution('debug', 'first approver', JSON
			        .stringify(firstApprover));
		} else {
			// pull the from the employee record / reporting manager
		}

		// 2nd level approver details
		var secondLevelApprovalId = expenseReportRec
		        .getFieldValue('custbody_expenseapprover');

		if (isNotEmpty(secondLevelApprovalId)) {
			var secondApprover = nlapiLookupField('employee',
			        secondLevelApprovalId, [ 'firstname', 'lastname', 'email' ]);
			secondApprover.id = expenseReportRec
			        .getFieldText('custbody_expenseapprover');
			secondApprover.internalid = secondLevelApprovalId;

			expenseDetails.SecondLevelApprover = secondApprover;
			nlapiLogExecution('debug', 'second approver', JSON
			        .stringify(secondApprover));
		}

		expenseDetails.Expense = {
		    Reference : expenseReportRec.getFieldValue('tranid'),
		    Purpose : isEmpty(expenseReportRec.getFieldValue('memo')) ? ''
		            : expenseReportRec.getFieldValue('memo'),
		    Status : expenseReportRec.getFieldValue('status'),
		    InternalId : expenseReportRec.getId(),
		    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
		    VAT : expenseReportRec.getFieldValue('tax1amt'),
		    PostingDate : expenseReportRec.getFieldValue('trandate'),
		    TransactionDate : expenseReportRec
		            .getFieldValue('custbody_transactiondate'),
		    AccountName : expenseReportRec.getFieldText('account'),
		    Account : expenseReportRec.getFieldValue('account'),
		    // Reason : '',
		    Currency : nlapiLookupField('expensereport', expenseId, 'currency',
		            true),
		    DocumentReceived : expenseReportRec
		            .getFieldValue('custbody_exp_doc_received'),
		    DocumentReceivedDate : '',
		    ItemList : []
		// ApproverRemark : expenseReportRec.getFieldValue(''),
		};

		expenseDetails.Expense.Summary = {
		    Expenses : expenseReportRec.getFieldValue('total'),
		    NonReimbursableExpenses : expenseReportRec
		            .getFieldValue('nonreimbursable'),
		    ReimbursableExpenses : expenseReportRec
		            .getFieldValue('reimbursable'),
		    AdvanceToApply : expenseReportRec.getFieldValue('advance'),
		    TotalReimbursableAmount : expenseReportRec.getFieldValue('amount')
		};

		// get all line items
		var lineItemCount = expenseReportRec.getLineItemCount('expense');
		var expenseList = [];
		var isReimbursable = null;
		var projectId = '';
		var projectIdList = [];

		for (var line = 1; line <= lineItemCount; line++) {
			isReimbursable = expenseReportRec.getLineItemValue('expense',
			        'isnonreimbursable', line);
			projectId = expenseReportRec.getLineItemValue('expense',
			        'customer', line);
			projectIdList.push(projectId);

			if (isFalse(isReimbursable)) {
				expenseList.push({
				    ProjectText : expenseReportRec.getLineItemValue('expense',
				            'custcolprj_name', line),
				    ProjectId : expenseReportRec.getLineItemValue('expense',
			        'customer', line),
				    Vertical : expenseReportRec.getLineItemValue('expense',
				            'class_display', line),
				    CategoryText : expenseReportRec.getLineItemValue('expense',
				            'category_display', line),
				    ForeignAmount : expenseReportRec.getLineItemValue(
				            'expense', 'foreignamount', line),
				    Amount : expenseReportRec.getLineItemValue('expense',
				            'amount', line),
				    Currency : expenseReportRec.getLineItemText('expense',
				            'currency', line),
				    DeliveryManager : null,
				    ClientPartner : null
				});
			}
		}

		// get all the delivery managers
		nlapiLogExecution('debug','expenseList', JSON.stringify(expenseList));
		projectIdList = _.uniq(projectIdList);
		getDeliveryManager(projectIdList, expenseList, firstApprover,
		        expenseDetails);

		return expenseDetails;

	} catch (err) {
		nlapiLogExecution('ERROR', 'getExpenseDetails', err);
		throw err;
	}
}

function getDeliveryManager(projectIdList, expenseList, expenseApprover,
        expenseDetails)
{

	var deliveryManagerList = [];
	var clientPartnerList = [];

	searchRecord(
	        'job',
	        null,
	        [ new nlobjSearchFilter('internalid', null, 'anyof', projectIdList) ],
	        [
	                new nlobjSearchColumn('custentity_deliverymanager'),
	                new nlobjSearchColumn('firstname',
	                        'custentity_deliverymanager'),
	                new nlobjSearchColumn('email', 'custentity_deliverymanager'),

	                new nlobjSearchColumn('custentity_clientpartner'),
	                new nlobjSearchColumn('firstname',
	                        'custentity_clientpartner'),
	                new nlobjSearchColumn('email', 'custentity_clientpartner') ])
	        .forEach(
	                function(project) {

		                projectId = project.getId();
		                deliveryManager = {
		                    firstname : project.getValue('firstname',
		                            'custentity_deliverymanager'),
		                    email : project.getValue('email',
		                            'custentity_deliverymanager'),
		                    internalid : project
		                            .getValue('custentity_deliverymanager'),
		                    total : 0
		                };

		                var clientPartner = {
		                    firstname : project.getValue('firstname',
		                            'custentity_clientpartner'),
		                    email : project.getValue('email',
		                            'custentity_clientpartner'),
		                    internalid : project
		                            .getValue('custentity_clientpartner'),
		                    total : 0
		                };

		                nlapiLogExecution('debug', 'CP', JSON
		                        .stringify(clientPartner));

		                if (isNotEmpty(deliveryManager.internalid)) {
			                deliveryManagerList.push(deliveryManager);

			                expenseList.forEach(function(expense) {

				                if (expense.ProjectId == projectId) {
					                expense.DeliveryManager = deliveryManager;
				                }
			                });
		                } else {
			                // expense approver gets the mails
			                // deliveryManagerList.push(expenseApprover.internalid);
		                }

		                if (isNotEmpty(clientPartner.internalid)) {
			                clientPartnerList.push(clientPartner);

			                expenseList.forEach(function(expense) {

				                if (expense.ProjectId == projectId) {
					                expense.ClientPartner = clientPartner;
				                }
			                });
		                } else {

		                }
	                });

	expenseDetails.Expense.ItemList = expenseList;

	deliveryManagerList = _.uniq(deliveryManagerList);
	clientPartnerList = _.uniq(clientPartnerList);

	nlapiLogExecution('debug', 'delivery managers a', JSON
	        .stringify(deliveryManagerList));

	nlapiLogExecution('debug', 'client partner a', JSON
	        .stringify(clientPartnerList));

	// check if multiple different delivery managers are there
	// deliveryManagerList = _.uniq(deliveryManagerList, function(item, key,
	// internalid) {
	// return item.internalid;
	// });

	// nlapiLogExecution('debug', 'delivery managers b',
	// JSON.stringify(deliveryManagerList));

	expenseDetails.DeliveryManagerList = deliveryManagerList;
	expenseDetails.ClientPartnerList = clientPartnerList;
}

// Mail Templates
function getExpenseDetailsTemplate(expenseDetails, deliveryManager,
        btplEmpNote, addDocReceived)
{

	var htmltext = '<p>';
	htmltext += '<b>Date of Submission : </b>'
	        + expenseDetails.Expense.TransactionDate + '<br/>';
	htmltext += '<b>Employee Name : </b>' + expenseDetails.Employee.firstname
	        + ' ' + expenseDetails.Employee.lastname + '<br/>';
	htmltext += '<b>Expense Id : </b>' + expenseDetails.Expense.Reference
	        + '<br/>';
	htmltext += '<b>Purpose : </b>' + expenseDetails.Expense.Purpose + '<br/>';
	// htmltext += '<b>Vertical : </b>' + expenseDetails.Employee.vertical +
	// '<br/>';
	htmltext += '<b>Department : </b>' + expenseDetails.Employee.department
	        + '<br/>';

	if (addDocReceived) {
		htmltext += '<b>Document Received : </b>'
		        + (expenseDetails.Expense.DocumentReceived == 'T' ? 'Yes'
		                : 'No') + '<br/>';

		htmltext += '<b>Document Received Date: </b>'
		        + expenseDetails.Expense.DocumentReceivedDate + '<br/>';
	}

	if (expenseDetails.Expense.Account != '113') { // unapproved expenses
		htmltext += '<b>Account Name : </b>'
		        + expenseDetails.Expense.AccountName + '<br/>';
	}

	if (isEmpty(deliveryManager)) {
		htmltext += '<b>Total Reimbursable Amount : </b>'
		        + expenseDetails.Expense.Summary.TotalReimbursableAmount + ' '
		        + expenseDetails.Expense.Currency + '<br/>';
	}

	/*
	 * if (isNotEmpty(expenseDetails.Reason)) { htmltext += '<b>Reason : </b>' +
	 * expenseDetails.Expense.Reason + '<br/>'; }
	 */

	htmltext += '</p>';

	var itemList = expenseDetails.Expense.ItemList;

	if (isArrayNotEmpty(itemList)) {
		var deliveryManagerAmount = 0;
		htmltext += '<table border="1">';

		htmltext += '<tr>';
		htmltext += '<td style="background:#E3E1E0">Category</td>';
		htmltext += '<td style="background:#E3E1E0">Project</td>';
		htmltext += '<td style="background:#E3E1E0">Vertical</td>';
		htmltext += '<td style="background:#E3E1E0">Amount</td>';
		// htmltext += '<td>Currency</td>';
		htmltext += '<td style="background:#E3E1E0">Amount ( '
		        + expenseDetails.Expense.Currency + ' )</td>';
		htmltext += '</tr>';

		itemList
		        .forEach(function(item) {

			        if (isNotEmpty(deliveryManager)
			                && item.DeliveryManager
			                && deliveryManager.internalid != item.DeliveryManager.internalid) {
				        return true;
			        } else {
				        htmltext += '<tr>';
				        htmltext += '<td>' + item.CategoryText + '</td>';
				        htmltext += '<td>' + item.ProjectText + '</td>';
				        htmltext += '<td>' + item.Vertical + '</td>';
				        htmltext += '<td>' + item.ForeignAmount + ' '
				                + item.Currency + '</td>';
				        htmltext += '<td>' + item.Amount + ' '
				                + expenseDetails.Expense.Currency + '</td>';
				        htmltext += '</tr>';
				        deliveryManagerAmount += parseFloat(item.Amount);
			        }
		        });

		if (isNotEmpty(deliveryManager)) {
			htmltext += '<tr>';
			htmltext += '<td></td>';
			htmltext += '<td></td>';
			htmltext += '<td></td>';
			// htmltext += '<td></td>';
			htmltext += '<td style="background:#E3E1E0"><b>Total</b></td>';
			htmltext += '<td style="background:#E3E1E0">'
			        + deliveryManagerAmount.toFixed(2) + ' '
			        + expenseDetails.Expense.Currency + '</td>';
			htmltext += '</tr>';
		}

		htmltext += '</table>';
	}

	if (isNotEmpty(btplEmpNote)
	        && expenseDetails.Employee.subsidiary == constant.Subsidiary.IN) {
		htmltext += btplEmpNote;
	}

	return htmltext;
}

function getClientPartnerExpenseDetailsTemplate(expenseDetails, clientPartner) {

	var htmltext = '<p>';
	htmltext += '<b>Date of Submission : </b>'
	        + expenseDetails.Expense.TransactionDate + '<br/>';
	htmltext += '<b>Employee Name : </b>' + expenseDetails.Employee.firstname
	        + ' ' + expenseDetails.Employee.lastname + '<br/>';
	htmltext += '<b>Expense Id : </b>' + expenseDetails.Expense.Reference
	        + '<br/>';
	htmltext += '<b>Purpose : </b>' + expenseDetails.Expense.Purpose + '<br/>';
	// htmltext += '<b>Vertical : </b>' + expenseDetails.Employee.vertical +
	// '<br/>';
	htmltext += '<b>Department : </b>' + expenseDetails.Employee.department
	        + '<br/>';

	htmltext += '</p>';

	var itemList = expenseDetails.Expense.ItemList;


	if (isArrayNotEmpty(itemList)) {
		var deliveryManagerAmount = 0;
		htmltext += '<table border="1">';

		htmltext += '<tr>';
		htmltext += '<td style="background:#E3E1E0">Category</td>';
		htmltext += '<td style="background:#E3E1E0">Project</td>';
		htmltext += '<td style="background:#E3E1E0">Vertical</td>';
		htmltext += '<td style="background:#E3E1E0">Amount</td>';
		// htmltext += '<td>Currency</td>';
		htmltext += '<td style="background:#E3E1E0">Amount ( '
		        + expenseDetails.Expense.Currency + ' )</td>';
		htmltext += '</tr>';

		itemList
		        .forEach(function(item) {

			        if (clientPartner
			                && item.ClientPartner
			                && clientPartner.internalid == item.ClientPartner.internalid) {
				        htmltext += '<tr>';
				        htmltext += '<td>' + item.CategoryText + '</td>';
				        htmltext += '<td>' + item.ProjectText + '</td>';
				        htmltext += '<td>' + item.Vertical + '</td>';
				        htmltext += '<td>' + item.ForeignAmount + ' '
				                + item.Currency + '</td>';
				        htmltext += '<td>' + item.Amount + ' '
				                + expenseDetails.Expense.Currency + '</td>';
				        htmltext += '</tr>';
				        deliveryManagerAmount += parseFloat(item.Amount);
			        }
		        });

		htmltext += '<tr>';
		htmltext += '<td></td>';
		htmltext += '<td></td>';
		htmltext += '<td></td>';

		htmltext += '<td style="background:#E3E1E0"><b>Total</b></td>';
		htmltext += '<td style="background:#E3E1E0">'
		        + deliveryManagerAmount.toFixed(2) + ' '
		        + expenseDetails.Expense.Currency + '</td>';
		htmltext += '</tr>';

		htmltext += '</table>';
	}

	return htmltext;
}

function employeeExpenseSubmittedMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expenses have been submitted to the expense approver in NetSuite. Please find the details below : </p>';

	/*var btplNotes = "<p><b>NOTE : Please drop original supporting bills, along with your"
	        + " expenses reimbursement claim after your reporting manager approval,"
	        + " in BRILLIO REIMBURSEMENT DROP BOX by making entry in the log book."
	        + " Finance Team will process your claim only after receipt of original bills."
	        + " Please note that bills dropped before 15th will be paid by 25th and dropped"
	        + " between 16 to 31st will be paid by 10th of succeeding month</b></p>";*/

	htmltext += getExpenseDetailsTemplate(expenseDetails, null, null);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Submitted',
	    Body : addMailTemplate(htmltext)
	};
}

function supervisorRejectionMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expenses has been rejected by the expense approver. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Rejected By Expense Approver',
	    Body : addMailTemplate(htmltext)
	};
}

function financeRejectionMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expenses has been rejected by the finance approver. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Rejected By Finance Approver',
	    Body : addMailTemplate(htmltext)
	};
}

function financeOverridenRejectionMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expenses has been rejected by the finance approver. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Rejected (Overridden) By Finance Approver',
	    Body : addMailTemplate(htmltext)
	};
}

function paidInFullMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expenses have been paid. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference + ' - Paid',
	    Body : addMailTemplate(htmltext)
	};
}

function firstApproverExpenseSubmittedMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.FirstLevelApprover.firstname
	        + ',</p>';
	htmltext += '<p>An expense has been submitted for your approval by '
	        + expenseDetails.Employee.firstname + ' '
	        + expenseDetails.Employee.lastname
	        + '. Please find the details below : </p>';
    htmltext += "<p>We spare no expenses in making life simpler today! </p>";
	htmltext += "<p>Download On the Go today! Click on the link: https://onthegoprod.blob.core.windows.net/onthego/index.html</p>";
	htmltext += getExpenseDetailsTemplate(expenseDetails);

	/*
	 * var recordUrl = nlapiResolveURL('RECORD', 'expensereport',
	 * expenseDetails.Expense.InternalId, 'EDIT'); htmltext += "<p><a href=" +
	 * recordUrl + ">Approval Link</a> Use this link if you are already logged
	 * into NetSuite <br/>"; var netSuiteSharepointLogin =
	 * "https://sts.brillio.com/adfs/ls/IdpInitiatedSignOn.aspx"; htmltext += "<a
	 * href=" + netSuiteSharepointLogin + ">NetSuite Login Link</a> Use this
	 * link if to log into NetSuite <br/></p>";
	 */

	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Pending Supervisor Approval'+'(use OnTheGo for easy approval)',
	    Body : addMailTemplate(htmltext)
	};
}

function employeeExpenseFirstLevelApprovalMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expenses have been approved by the expense approver in NetSuite and is pending finance approval.'
	        + ' Please find the details below : </p>';

	/*var btplNotes = "<p><b>NOTE : Please drop original supporting bills, along with your"
	        + " expenses reimbursement claim after your reporting manager approval,"
	        + " in BRILLIO REIMBURSEMENT DROP BOX by making entry in the log book."
	        + " Finance Team will process your claim only after receipt of original bills."
	        + " Please note that bills dropped before 15th will be paid by 25th and dropped"
	        + " between 16 to 31st will be paid by 10th of succeeding month</b></p>";*/

	htmltext += getExpenseDetailsTemplate(expenseDetails, null, null);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Pending Finance Approval',
	    Body : addMailTemplate(htmltext)
	};
}

function secondApproverExpenseFirstLevelApprovalMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.SecondLevelApprover.firstname
	        + ',</p>';
	htmltext += '<p>'
	        + expenseDetails.Employee.firstname
	        + ' '
	        + expenseDetails.Employee.lastname
	        + '\'s expense has been approved by the supervisor and is pending finance approval. Please find the details below  : </p>'; // <===
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Pending Finance Approval',
	    Body : addMailTemplate(htmltext)
	};
}

function employeeExpenseSecondLevelApprovalMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';
	htmltext += '<p>Your expense has been approved by the finance approver. Please find the details below : </p>';
	htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Finance Approved',
	    Body : addMailTemplate(htmltext)
	};
}

function deliveryManagerExpenseSubmittedMailTemplate(expenseDetails,
        deliveryManager)
{

	var htmltext = '<p>Hi ' + deliveryManager.firstname + ',</p>';

	htmltext += '<p style="color:red"><u><b>NOTE : This email is just for your information (however the expense will be routed to the expense approver accordingly)</b></u></p>';

	htmltext += '<p>This is to inform you that '
	        + expenseDetails.Employee.firstname
	        + ' '
	        + expenseDetails.Employee.lastname
	        + ' has raised an expense for the project under your management. </p>';

	htmltext += getExpenseDetailsTemplate(expenseDetails, deliveryManager);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Expenses raised in your project',
	    Body : addMailTemplate(htmltext)
	};
}

function clientPartnerExpenseSubmittedMailTemplate(expenseDetails,
        clientPartner)
{

	var htmltext = '<p>Hi ' + clientPartner.firstname + ',</p>';

	htmltext += '<p style="color:red"><u><b>NOTE : This email is just for your information (however the expense will be routed to the expense approver accordingly)</b></u></p>';

	htmltext += '<p>This is to inform you that '
	        + expenseDetails.Employee.firstname
	        + ' '
	        + expenseDetails.Employee.lastname
	        + ' has raised an expense for the project under your management. </p>';

	htmltext += getClientPartnerExpenseDetailsTemplate(expenseDetails,
	        clientPartner);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Expenses raised in your project',
	    Body : addMailTemplate(htmltext)
	};
}

function generateSummaryTable(summary) {

	var htmltext = "";
	htmltext += "<table>";

	htmltext += '<tr><td align="right">Expenses</td><td>' + summary.Expenses
	        + '</td></tr>';
	htmltext += '<tr><td align="right">Non-reimbursable Expenses</td><td>'
	        + summary.NonReimbursableExpenses + '</td></tr>';
	htmltext += '<tr><td align="right">Reimbursable Expenses </td><td>'
	        + summary.ReimbursableExpenses + '</td></tr>';
	htmltext += '<tr><td align="right">Advance to Apply </td><td>'
	        + summary.AdvanceToApply + '</td></tr>';
	htmltext += '<tr><td align="right">Total Reimbursable Amount</td><td>'
	        + summary.TotalReimbursableAmount + '</td></tr>';

	htmltext += "</table>";
	return htmltext;
}

// action script
function sendDocumentReminderMail() {
	try {
		var expenseId = nlapiGetRecordId();
		var expenseDetails = getExpenseDetails(expenseId);
		nlapiLogExecution('debug', 'nlapiGetRecordId', expenseId);

		/*var employeeMailContent = documentReminderMailTemplate(expenseDetails);
		nlapiSendEmail(constant.Mail_Author.InformationSystem,
		        expenseDetails.Employee.email, employeeMailContent.Subject,
		        employeeMailContent.Body, null, null, {
		            entity : expenseDetails.Employee.internalid,
		            transaction : expenseId
		        });*/

	} catch (err) {
		nlapiLogExecution('ERROR', 'sendDocumentReminderMail', err);
	}
}

/*function documentReminderMailTemplate(expenseDetails) {

	var htmltext = '<p>Hi ' + expenseDetails.Employee.firstname + ',</p>';

	htmltext += "<p><b>NOTE : Please drop original supporting bills, along with your"
	        + " expenses reimbursement claim after your reporting manager approval,"
	        + " in BRILLIO REIMBURSEMENT DROP BOX by making entry in the log book."
	        + " Finance Team will process your claim only after receipt of original bills."
	        + " Please note that bills dropped before 15th will be paid by 25th and dropped"
	        + " between 16 to 31st will be paid by 10th of succeeding month</b></p>";

	htmltext += '<p><b>Please ignore if already submitted.</b></p>';

	// htmltext += getExpenseDetailsTemplate(expenseDetails);
	htmltext += '<p>Regards,<br/>';
	htmltext += 'NetSuite Support Team</p>';

	return {
	    Subject : 'Expense : ' + expenseDetails.Expense.Reference
	            + ' - Document Reminder',
	    Body : addMailTemplate(htmltext)
	};
}*/