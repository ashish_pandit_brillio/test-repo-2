/**
 * @author Nikhil Jain
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES TDS Check Advancepay to Vendor
	Author:Nikhil jain
	Company:Aashna cloudtech Pvt. Ltd.
	Date:18-12-2014
	Description:Create a bill credit record for advance pay to vendor


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	30 July 2015         Nikhil                          sachin k                       add a reneweal process


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord_tds_advancepay(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
		
		var a_subisidiary = new Array();
		var s_pass_code;
		if (type != 'delete') 
		{
			var checkid = nlapiGetRecordId()
			nlapiLogExecution('DEBUG', 'afterSubmitRecord check ', " checkid " + checkid)
			
			var recordType = nlapiGetRecordType()
			nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " recordType " + recordType)
			
			var o_checkobj = nlapiLoadRecord(recordType, checkid)
			nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " recordType " + recordType)
			
			if (o_checkobj != null && o_checkobj != '' && o_checkobj != undefined) 
			{
			
				var b_Adavncetoved = o_checkobj.getFieldValue('custbody_tds_advancepay')
				nlapiLogExecution('DEBUG', 'afterSubmitRecord check', " advance to vendor " + b_Adavncetoved)
				
				if (b_Adavncetoved == 'T') 
				{
					var i_AitGlobalRecId = SearchGlobalParameter();
					nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
					
					if (i_AitGlobalRecId != 0) 
					{
						var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
						
						a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
						nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
						
					    s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
						nlapiLogExecution('DEBUG','Bill ', "s_pass_code->"+s_pass_code);
					}// end if(i_AitGlobalRecId != 0 )
					//var a_subsidiary1 = a_subisidiary.toString();
					
					var Flag = 0;
					var i_subsidiary = o_checkobj.getFieldValue('subsidiary')
					
					Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
					/*
if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') {
					
						for (var y = 0; y < a_subisidiary.length; y++) {
							var i_subsidiary = o_checkobj.getFieldValue('subsidiary')
							
							//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
							if (a_subisidiary[y] == i_subsidiary) {
								Flag = 1;
								break;
							}// END if (a_subisidiary[y] == i_subsidiary)
						}// END for (var y = 0; y < a_subisidiary.length; y++)
					}// END  if (a_subsidiary1.indexOf(',') > -1)
					else {
						// === IF COMMA NOT FOUND ===
						var i_subsidiary = o_checkobj.getFieldValue('subsidiary')
						
						//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
						if (a_subisidiary[0] == i_subsidiary) {
							Flag = 1;
						}// END  if (a_subsidiary1 == i_subsidiary)
					}// END  if (a_subsidiary1 == i_subsidiary)
					//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
					var context = nlapiGetContext();
					var i_subcontext = context.getFeature('SUBSIDIARIES');
					if (i_subcontext == false) {
						Flag = 1;
					}
					
*/
					if (Flag == 1) {
					
						var i_Bill_ref_no = o_checkobj.getFieldValue('custbody_tds_billcredit_ref')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "Bill credit id" + i_Bill_ref_no)
						
						var voided = o_checkobj.getFieldValue('voided')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "voided" + voided)
						
						var i_Location = o_checkobj.getFieldValue('location')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "i_Location" + i_Location)
						
						if (voided == 'F') {
						
							if (i_Bill_ref_no != '' && i_Bill_ref_no != null && i_Bill_ref_no != undefined) {
							
								var o_billcredit_obj = nlapiLoadRecord('vendorcredit', i_Bill_ref_no);
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "bill credit object" + o_billcredit_obj)
								
								if (o_billcredit_obj != null && o_billcredit_obj != '' && o_billcredit_obj != undefined) {
								
									var memo = o_checkobj.getFieldValue('memo');
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "memo " + memo)
									
									//var currency = o_checkobj.getFieldValue('currency');
									o_billcredit_obj.setFieldValue('location', i_Location)
									o_billcredit_obj.setFieldValue('memo', memo)
									
									
									//----------------------------Begin code to remove bill credit expense line item--------------------------------// 
									var remove_line = o_billcredit_obj.getLineItemCount('expense');
									
									for (var n = remove_line; n > 0; n--) {
										//nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "before removeexpense" + n)
										o_billcredit_obj.removeLineItem('expense', n)
										nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "removeexpense" + n)
									}
									
									//----------------------------End code to remove bill credit expense line item--------------------------------// 
									
									
									//----------------------------Begin code to add expense line item--------------------------------------------//
									var exp_linecount = o_checkobj.getLineItemCount('expense');
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "expense line count" + exp_linecount)
									
									if (exp_linecount != null && exp_linecount != '' && exp_linecount != undefined) 
									{
										for (var i = 1; i <= exp_linecount; i++) 
										{
											var category = o_checkobj.getLineItemValue('expense', 'category', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "category" + category)
											
											var account = o_checkobj.getLineItemValue('expense', 'account', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "account" + account)
											
											
											var taxcode = o_checkobj.getLineItemValue('expense', 'taxcode', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "taxcode" + taxcode)
											
											var department = o_checkobj.getLineItemValue('expense', 'department', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "department" + department)
											
											var location = o_checkobj.getLineItemValue('expense', 'location', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "location" + location)
											
											var s_class = o_checkobj.getLineItemValue('expense', 'class', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_class" + s_class)
											
											var TDS_applied = o_checkobj.getLineItemValue('expense', 'custcol_tdsapply', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "account" + account)
											
											var s_projectDesc = o_checkobj.getLineItemValue('expense', 'custcolprj_name', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectDesc" + s_projectDesc)
											
											var s_projectID = o_checkobj.getLineItemValue('expense', 'custcol_project_entity_id', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectID" + s_projectID)
											
											if (TDS_applied == 'T') {
												var amount = o_checkobj.getLineItemValue('expense', 'amount', i)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check base amount tds applied', "amount" + amount)
												
												o_billcredit_obj.setFieldValue('custbody_advance_tds_vendor', 'T');
												
												i = i + 1;
												
											}
											else {
												var amount = o_checkobj.getLineItemValue('expense', 'amount', i)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "amount" + amount)
											}
											
											o_billcredit_obj.selectNewLineItem('expense')
											o_billcredit_obj.setCurrentLineItemValue('expense', 'category', category)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'account', account)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'amount', amount)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'taxcode', taxcode)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'department', department)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'location', location)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'class', s_class)
											//Added project fields
											o_billcredit_obj.setCurrentLineItemValue('expense', 'custcolprj_name', s_projectDesc)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'custcol_project_entity_id', s_projectID)
											
											o_billcredit_obj.commitLineItem('expense')
										}// End line count
										if (parseInt(exp_linecount) > parseInt(0)) {
											var loc = nlapiGetLineItemValue('expense', 'location', 1)
											if (loc != null && loc != '' && loc != undefined) {
												o_billcredit_obj.setFieldValue('location', loc)
											}
										}
									}//End-if expense line
									//----------------------------End code to add expense line item--------------------------------------------//
									
									
									//----------------------------Begin code to remove bill credit item line item--------------------------------// 
									var remove_item_linecount = o_billcredit_obj.getLineItemCount('item');
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "item line count" + remove_item_linecount)
									
									if (remove_item_linecount != null && remove_item_linecount != '' && remove_item_linecount != undefined) {
										for (var k = remove_item_linecount; k >= 1; k--) {
											o_billcredit_obj.removeLineItem('item', k)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "remove item" + k)
										}
									}
									
									//----------------------------Begin code to remove bill credit item line item--------------------------------// 
									
									
									//----------------------------Begin code to add bill credit item line item--------------------------------// 
									var item_linecount = o_checkobj.getLineItemCount('item');
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "item line count" + item_linecount)
									
									if (item_linecount != null && item_linecount != '' && item_linecount != undefined) {
										for (var l = 1; l <= item_linecount; l++) {
											var item = o_checkobj.getLineItemValue('item', 'item', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "item" + item)
											
											var quantity = o_checkobj.getLineItemValue('item', 'quantity', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "quantity" + quantity)
											
											var rate = o_checkobj.getLineItemValue('item', 'rate', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "rate" + rate)
											
											var taxcode = o_checkobj.getLineItemValue('item', 'taxcode', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "taxcode" + taxcode)
											
											var department = o_checkobj.getLineItemValue('item', 'department', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "department" + department)
											
											var location = o_checkobj.getLineItemValue('item', 'location', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "location" + location)
											
											var s_class = o_checkobj.getLineItemValue('item', 'class', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "class" + s_class)
											
											var TDS_applied = o_checkobj.getLineItemValue('item', 'custcol_tdsapply', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "account" + account)
											
											var s_projectDesc = o_checkobj.getLineItemValue('item', 'custcolprj_name', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectDesc" + s_projectDesc)
											
											var s_projectID = o_checkobj.getLineItemValue('item', 'custcol_project_entity_id', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectID" + s_projectID)
											
											if (TDS_applied == 'T') {
												var amount = o_checkobj.getLineItemValue('item', 'amount', l)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check base amount tds applied', "amount" + amount)
												
												o_billcredit_obj.setFieldValue('custbody_advance_tds_vendor', 'T');
												
												l = l + 1;
												
											}
											else {
											
												var amount = o_checkobj.getLineItemValue('item', 'amount', l)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "amount" + amount)
											}
											
											o_billcredit_obj.selectNewLineItem('item')
											o_billcredit_obj.setCurrentLineItemValue('item', 'item', item)
											o_billcredit_obj.setCurrentLineItemValue('item', 'quantity', quantity)
											o_billcredit_obj.setCurrentLineItemValue('item', 'rate', rate)
											o_billcredit_obj.setCurrentLineItemValue('item', 'amount', amount)
											o_billcredit_obj.setCurrentLineItemValue('item', 'taxcode', taxcode)
											o_billcredit_obj.setCurrentLineItemValue('item', 'department', department)
											o_billcredit_obj.setCurrentLineItemValue('item', 'location', location)
											o_billcredit_obj.setCurrentLineItemValue('item', 'class', s_class)
											//Added project fields
											o_billcredit_obj.setCurrentLineItemValue('item', 'custcolprj_name', s_projectDesc)
											o_billcredit_obj.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_projectID)
											o_billcredit_obj.commitLineItem('item')
										}//End for l
										if (parseInt(item_linecount) > parseInt(0)) {
										
											var loc = nlapiGetLineItemValue('expense', 'location', 1)
											
											if (loc != null && loc != '' && loc != undefined) {
												o_billcredit_obj.setFieldValue('location', loc)
											}
										}
									}//End-if line item count
									//----------------------------End code to add bill credit item line item--------------------------------// 
									var updated_billcredit_id = nlapiSubmitRecord(o_billcredit_obj,true,true)
									nlapiLogExecution('DEBUG', 'new bill credit created', "update billcredit id" + updated_billcredit_id)
									
								}
							}
							else {
								//----------------------------Begin code to create bill credit --------------------------------// 
								var vendor = o_checkobj.getFieldValue('entity')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "vendor" + vendor)
								
								var subsidiary = o_checkobj.getFieldValue('subsidiary')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "subsidiary" + subsidiary)
								
								var memo = o_checkobj.getFieldValue('memo')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "memo" + memo)
								
								var trandate = o_checkobj.getFieldValue('trandate')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "trandate" + trandate)
								
								var currency = o_checkobj.getFieldValue('currency')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "currency" + currency)
								
								var i_Location = o_checkobj.getFieldValue('location')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "i_Location" + i_Location)
								
								var o_billcredit_obj = nlapiCreateRecord('vendorcredit')
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "bill credit object" + o_billcredit_obj)
								
								if (o_billcredit_obj != null && o_billcredit_obj != '' && o_billcredit_obj != undefined) {
									o_billcredit_obj.setFieldValue('entity', vendor)
									o_billcredit_obj.setFieldValue('subsidiary', subsidiary)
									o_billcredit_obj.setFieldValue('memo', memo)
									o_billcredit_obj.setFieldValue('currency', currency)
									o_billcredit_obj.setFieldValue('location', i_Location)
									o_billcredit_obj.setFieldValue('custbody_tds_advance_checkdate', trandate)
									o_billcredit_obj.setFieldValue('custbody_tds_advance_check', checkid)
									
									var exp_linecount = o_checkobj.getLineItemCount('expense');
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "expense line count" + exp_linecount)
									
									if (exp_linecount != null && exp_linecount != '' && exp_linecount != undefined) {
										for (var i = 1; i <= exp_linecount; i++) {
											var category = o_checkobj.getLineItemValue('expense', 'category', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "category" + category)
											
											var account = o_checkobj.getLineItemValue('expense', 'account', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "account" + account)
											
											var taxcode = o_checkobj.getLineItemValue('expense', 'taxcode', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "taxcode" + taxcode)
											
											var department = o_checkobj.getLineItemValue('expense', 'department', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "department" + department)
											
											var location = o_checkobj.getLineItemValue('expense', 'location', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "location" + location)
											
											var s_class = o_checkobj.getLineItemValue('expense', 'class', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_class" + s_class)
											
											var TDS_applied = o_checkobj.getLineItemValue('expense', 'custcol_tdsapply', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "account" + account)
											
											var s_projectDesc = o_checkobj.getLineItemValue('expense', 'custcolprj_name', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectDesc" + s_projectDesc)
											
											var s_projectID = o_checkobj.getLineItemValue('expense', 'custcol_project_entity_id', i)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectID" + s_projectID)
											
											if (TDS_applied == 'T') {
												var amount = o_checkobj.getLineItemValue('expense', 'amount', i)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check base amount tds applied', "amount" + amount)
												
												o_billcredit_obj.setFieldValue('custbody_advance_tds_vendor', 'T');
												
												i = i + 1;
												
											}
											else {
												var amount = o_checkobj.getLineItemValue('expense', 'amount', i)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "amount" + amount)
											}
											o_billcredit_obj.selectNewLineItem('expense')
											o_billcredit_obj.setCurrentLineItemValue('expense', 'category', category)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'account', account)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'amount', amount)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'taxcode', taxcode)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'department', department)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'location', location)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'class', s_class)
											//Added project fields
											o_billcredit_obj.setCurrentLineItemValue('expense', 'custcolprj_name', s_projectDesc)
											o_billcredit_obj.setCurrentLineItemValue('expense', 'custcol_project_entity_id', s_projectID)
											o_billcredit_obj.commitLineItem('expense')
										}// End line count
										if (parseInt(exp_linecount) > parseInt(0)) {
											var loc = nlapiGetLineItemValue('expense', 'location', 1)
											if (loc != null && loc != '' && loc != undefined) {
												o_billcredit_obj.setFieldValue('location', loc)
											}
										}
									}//End-if expense line
									var item_linecount = o_checkobj.getLineItemCount('item');
									nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "item line count" + item_linecount)
									
									if (item_linecount != null && item_linecount != '' && item_linecount != undefined) {
										for (var l = 1; l <= item_linecount; l++) {
											var item = o_checkobj.getLineItemValue('item', 'item', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "item" + item)
											
											var quantity = o_checkobj.getLineItemValue('item', 'quantity', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "quantity" + quantity)
											
											var rate = o_checkobj.getLineItemValue('item', 'rate', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "rate" + rate)
											
											
											var taxcode = o_checkobj.getLineItemValue('item', 'taxcode', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "taxcode" + taxcode)
											
											var department = o_checkobj.getLineItemValue('item', 'department', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "department" + department)
											
											var location = o_checkobj.getLineItemValue('item', 'location', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "location" + location)
											
											var s_class = o_checkobj.getLineItemValue('item', 'class', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "class" + s_class)
											
											var TDS_applied = o_checkobj.getLineItemValue('item', 'custcol_tdsapply', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "account" + account)
											
											var s_projectDesc = o_checkobj.getLineItemValue('item', 'custcolprj_name', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectDesc" + s_projectDesc)
											
											var s_projectID = o_checkobj.getLineItemValue('item', 'custcol_project_entity_id', l)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "s_projectID" + s_projectID)
											
											if (TDS_applied == 'T') {
												var amount = o_checkobj.getLineItemValue('item', 'amount', l)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check base amount tds applied', "amount" + amount)
												
												o_billcredit_obj.setFieldValue('custbody_advance_tds_vendor', 'T');
												
												l = l + 1;
												
											}
											else {
											
												var amount = o_checkobj.getLineItemValue('item', 'amount', l)
												nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "amount" + amount)
											}
											o_billcredit_obj.selectNewLineItem('item')
											o_billcredit_obj.setCurrentLineItemValue('item', 'item', item)
											o_billcredit_obj.setCurrentLineItemValue('item', 'quantity', quantity)
											o_billcredit_obj.setCurrentLineItemValue('item', 'rate', rate)
											o_billcredit_obj.setCurrentLineItemValue('item', 'amount', amount)
											o_billcredit_obj.setCurrentLineItemValue('item', 'taxcode', taxcode)
											o_billcredit_obj.setCurrentLineItemValue('item', 'department', department)
											o_billcredit_obj.setCurrentLineItemValue('item', 'location', location)
											o_billcredit_obj.setCurrentLineItemValue('item', 'class', s_class)
											//Added project fields
											o_billcredit_obj.setCurrentLineItemValue('item', 'custcolprj_name', s_projectDesc)
											o_billcredit_obj.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_projectID)
											o_billcredit_obj.commitLineItem('item')
										}//End for i
										if (parseInt(item_linecount) > parseInt(0)) {
											var loc = nlapiGetLineItemValue('expense', 'location', 1)
											if (loc != null && loc != '' && loc != undefined) {
												o_billcredit_obj.setFieldValue('location', loc)
											}
										}
									}//End-if line item count
									var billcredit_id = nlapiSubmitRecord(o_billcredit_obj,true,true)
									nlapiLogExecution('DEBUG', 'new bill credit created', "billcredit id" + billcredit_id)
									
									o_checkobj.setFieldValue('custbody_tds_billcredit_ref', billcredit_id)
									
									var updated_checkid = nlapiSubmitRecord(o_checkobj,true,true)
									nlapiLogExecution('DEBUG', 'new bill credit created', "updated check id" + updated_checkid)
								}//End-if Bill credit obj
							//----------------------------Begin code to create bill credit -----------------------------------------------// 
							}
							
							
						}
					}//End if(Flag ==1)
				}//end-if(Advance to vendor=='T')
			}//End if(o_checkobj != null)
		}
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
 // == FUNCTION FOR SEARCHING SUBSIDIARY GOLBAL PARAMETER ==
    function SearchGlobalParameter()	
	{
    
        var a_filters = new Array();
        var a_column = new Array();
        var i_globalRecId = 0;
        
        a_column.push(new nlobjSearchColumn('internalid'));
        
        var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column)
        
        if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
		{
            for (var i = 0; i < s_serchResult.length; i++) 
			{
                i_globalRecId = s_serchResult[0].getValue('internalid');
                nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
                
            }
            
        }
        
        
        return i_globalRecId;
    }
    
}
// END FUNCTION =====================================================
