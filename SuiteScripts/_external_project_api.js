/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/runtime'],
    function (obj_Record, obj_Search, obj_Runtime) {
        function Send_Project_data(obj_Request) {
            try {
                log.debug('requestBody ==', JSON.stringify(obj_Request));
                var obj_Usage_Context = obj_Runtime.getCurrentScript();
                var s_Request_Type = obj_Request.RequestType;
                log.debug('s_Request_Type ==', s_Request_Type);
                var s_Request_Data = obj_Request.RequestData;
                log.debug('s_Request_Data ==', s_Request_Data);
                var obj_Response_Return = {};
                if (s_Request_Data == 'EXTERNALPROJECT') {
                    obj_Response_Return = get_Project_Data(obj_Search);
                }
            }
            catch (s_Exception) {
                log.debug('s_Exception==', s_Exception);
            } //// 
            log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
            log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
            return obj_Response_Return;
        } ///// Function Send_Project_data


        return {
            post: Send_Project_data
        };

    }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Project_Data(obj_Search) {
    try {
        var jobSearchObj = obj_Search.create({
            type: "job",
            filters:
                [
                    ["type", "anyof", "2"],
                    "AND",
                    ["jobresource", "anyof", "@ALL@"]
                ],
            columns:
                [
                    obj_Search.createColumn({ name: "entityid", label: "Project ID" }),
                    obj_Search.createColumn({
                        name: "altname",
                        sort: obj_Search.Sort.ASC,
                        label: "Name"
                    }),
                    obj_Search.createColumn({ name: "customer", label: "Customer Name" }),
                    obj_Search.createColumn({
                        name: "custentity_clientpartner",
                        join: "customer",
                        label: "Client Partner"
                    }),
                    obj_Search.createColumn({
                        name: "territory",
                        join: "customer",
                        label: "Customer Territory"
                    }),
                    obj_Search.createColumn({
                        name: "subsidiarynohierarchy",
                        join: "customer",
                        label: "Customer Subsidiary"
                    }),
                    obj_Search.createColumn({ name: "entitystatus", label: "Project Status" }),
                    obj_Search.createColumn({ name: "jobtype", label: "Project Type" }),
                    obj_Search.createColumn({ name: "custentity_sowdate", label: "SOW Date" }),
                    obj_Search.createColumn({ name: "comments", label: "Comments" }),
                    obj_Search.createColumn({ name: "custentity_deliverymodel", label: "Delivery Model" }),
                    obj_Search.createColumn({ name: "jobbillingtype", label: "Billing Type" }),
                    obj_Search.createColumn({ name: "custentity_t_and_m_monthly", label: "Is T&M Monthly" }),
                    obj_Search.createColumn({ name: "billingschedule", label: "Billing Schedule" }),
                    obj_Search.createColumn({ name: "startdate", label: "Project Start Date" }),
                    obj_Search.createColumn({ name: "enddate", label: "Project End Date" }),
                    obj_Search.createColumn({ name: "custentity_projectmanager", label: "Project Manager" }),
                    obj_Search.createColumn({ name: "custentity_deliverymanager", label: "Delivery Manager" }),
                    obj_Search.createColumn({
                        name: "internalid",
                        join: "CUSTENTITY_PRACTICE",
                        label: "Internal ID"
                    }),
                    obj_Search.createColumn({ name: "custentity_location", label: "Location" }),
                    obj_Search.createColumn({ name: "category", label: "Category" }),
                    obj_Search.createColumn({ name: "custentity_worklocation", label: "Work Location" }),
                    obj_Search.createColumn({ name: "billingschedule", label: "Billing Schedule" }),
                    obj_Search.createColumn({ name: "custentity_projectvalue", label: "Project Value" }),
                    obj_Search.createColumn({ name: "custentity_otapplicable", label: "OT Applicable" }),
                    obj_Search.createColumn({ name: "custentity3", label: "OT Rate % (ST)" })
                ]
        });
        var searchResultCount = jobSearchObj.runPaged().count;
        log.debug("jobSearchObj result count", searchResultCount);
        var myResults = getAllResults(jobSearchObj);
        var arr_Proj_json = [];
        myResults.forEach(function (result) {
            var obj_json_Container = {}
            obj_json_Container = {
                'Internal_ID': result.id,
                'Project_ID': result.getValue({ name: "entityid" }),
                'Project_Name': result.getValue({ name: 'altname' }),
                'Customer_Name': result.getValue({ name: 'customer' }),
                'Client_Partner': result.getValue({ name: "custentity_clientpartner", join: "customer" }),
                'Customer_Territory': result.getText({ name: "territory", join: "customer" }),
                'Customer_Subsidiary': result.getValue({ name: "subsidiarynohierarchy", join: "customer" }),
                'Project_Status': result.getText({ name: "entitystatus" }),
                'Project_Type': result.getText({ name: "jobtype" }),
                'SOW_Date': result.getValue({ name: "custentity_sowdate" }),
                'Comments': result.getValue({ name: "comments" }),
                'Delivery_Model': result.getText({ name: "custentity_deliverymodel"}),
                'Billing_Type': result.getText({ name: "jobbillingtype" }),
                'Is_T&M_Monthly': result.getValue({ name: "custentity_t_and_m_monthly" }),
                'Billing_Schedule':result.getText({ name: "billingschedule" }),
                'Start_Date': result.getValue({ name: "startdate" }),
                'End_Date': result.getValue({ name: "enddate" }),
                'Project_Manager':result.getValue({ name: "custentity_projectmanager"}),
                'Delivery_Manager':result.getValue({ name: "custentity_deliverymanager"}),
                'Practice':result.getValue({name: "internalid", join: "CUSTENTITY_PRACTICE"}),
                'Location':result.getValue({name: "custentity_location"}),
                'Category':result.getText({name: "category"}),
                'Work_Location':result.getValue({name: "custentity_worklocation"}),
                'Project_Value':result.getValue({name: "custentity_projectvalue"}),
                'OT_Applicable':result.getValue({name: "custentity_otapplicable"}),
                'OT_Rate_Percent(ST)':result.getValue({name: "custentity3"}),
            };
            arr_Proj_json.push(obj_json_Container);
            return true;
        });
        return arr_Proj_json;
    } //// End of try
    catch (s_Exception) {
        log.debug('get_Project_Data s_Exception== ', s_Exception);
    } //// End of catch 
} ///// End of function get_Project_Data()


function getAllResults(s) {
    var result = s.run();
    var searchResults = [];
    var searchid = 0;
    do {
        var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
        resultslice.forEach(function (slice) {
            searchResults.push(slice);
            searchid++;
        }
        );
    } while (resultslice.length >= 1000);
    return searchResults;
}