/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Currency_data(obj_Request) {
         try {
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var s_Request_Param = obj_Request.RequestParameter;
             log.debug('s_Request_Data ==', s_Request_Data);
             var obj_Response_Return = {};
             if (s_Request_Data == 'CURRENCYEXCHANGERATE' && s_Request_Param == 'MONTHLY') {
                 obj_Response_Return = get_Monthly_Data(obj_Search);
             }
             else if(s_Request_Data == 'CURRENCYEXCHANGERATE' && s_Request_Param == 'YEARLY'){
                obj_Response_Return = get_Yearly_Data(obj_Search);
             }
         } 
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Currency_data


     return {
         post: Send_Currency_data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_Yearly_Data(obj_Search){
 try {
    var customrecord_ypl_currency_exchange_ratesSearchObj = obj_Search.create({
        type: "customrecord_ypl_currency_exchange_rates",
        filters:
        [
        ],
        columns:
        [
           obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
           obj_Search.createColumn({name: "custrecord_ypl_currency_exchange_year", label: "Year"}),
           obj_Search.createColumn({name: "custrecord_ypl_currency_exchange_revnue", label: "USD-INR Revenue Rate"}),
           obj_Search.createColumn({name: "custrecord_ypl_currency_exchange_cost", label: "USD-INR Cost Rate"}),
           obj_Search.createColumn({name: "custrecord_ypl_gbp_converstion_rate", label: "USD to GBP"}),
           obj_Search.createColumn({name: "custrecord_ypl_eur_usd_conv_rate", label: "USD to EUR"}),
           obj_Search.createColumn({name: "custrecord_ypl_aud_to_usd", label: "USD to AUD"}),
           obj_Search.createColumn({name: "custrecord_ypl_usd_cad_conversion_rate", label: "USD to CAD"}),
           obj_Search.createColumn({name: "custrecord_ypl_usd_ron_conversion_rate", label: "USD to RON"})
        ]
     });
     var searchResultCount = customrecord_ypl_currency_exchange_ratesSearchObj.runPaged().count;
     log.debug("customrecord_ypl_currency_exchange_ratesSearchObj result count",searchResultCount);
     var myResults = getAllResults(customrecord_ypl_currency_exchange_ratesSearchObj);
     var arr_Curr_json = [];
     myResults.forEach(function (result) {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
             'Year': result.getText({ name: "custrecord_ypl_currency_exchange_year"}),
             'USD_INR_Revenue_Rate':result.getValue({ name: "custrecord_ypl_currency_exchange_revnue"}),
             'USD_INR_Cost_Rate':result.getValue({ name: "custrecord_ypl_currency_exchange_cost"}),
             'USD_GBP': result.getValue({ name: "custrecord_ypl_gbp_converstion_rate"}),
             'USD_EUR':result.getValue({ name: "custrecord_ypl_eur_usd_conv_rate"}),
             'USD_AUD':result.getValue({ name: "custrecord_ypl_aud_to_usd"}),
             'USD_CAD':result.getValue({ name: "custrecord_ypl_usd_cad_conversion_rate"}),
             'USD_RON':result.getValue({ name: "custrecord_ypl_usd_ron_conversion_rate"}),
         };
         arr_Curr_json.push(obj_json_Container);
         return true;
     });
     return arr_Curr_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('get_Employee_Data s_Exception== ', s_Exception);
 } //// End of catch 
} 

function get_Monthly_Data(obj_Search){
    try {
        var customrecord_pl_currency_exchange_ratesSearchObj = obj_Search.create({
            type: "customrecord_pl_currency_exchange_rates",
            filters:
            [
            ],
            columns:
            [
               obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
               obj_Search.createColumn({name: "custrecord_pl_currency_exchange_month", label: "Month"}),
               obj_Search.createColumn({name: "custrecord_pl_currency_exchange_year", label: "Year"}),
               obj_Search.createColumn({name: "custrecord_pl_currency_exchange_revnue_r", label: "USD-INR Revenue Rate"}),
               obj_Search.createColumn({name: "custrecord_pl_currency_exchange_cost", label: "USD-INR Cost Rate"}),
               obj_Search.createColumn({name: "custrecord_pl_crc_cost_rate", label: "CRC Cost Conversion"}),
               obj_Search.createColumn({name: "custrecord_pl_nok_cost_rate", label: "NOK Cost Rate"}),
               obj_Search.createColumn({name: "custrecord_gbp_converstion_rate", label: "USD to GBP"}),
               obj_Search.createColumn({name: "custrecord_eur_usd_conv_rate", label: "USD to EUR"}),
               obj_Search.createColumn({name: "custrecord_aud_to_usd", label: "USD to AUD"}),
               obj_Search.createColumn({name: "custrecord_usd_cad_conversion_rate", label: "USD to CAD"}),
               obj_Search.createColumn({name: "custrecord_usd_ron_conversion_rate", label: "USD to RON"})
            ]
         });
         var searchResultCount = customrecord_pl_currency_exchange_ratesSearchObj.runPaged().count;
         log.debug("customrecord_pl_currency_exchange_ratesSearchObj result count",searchResultCount);
        var myResults = getAllResults(customrecord_pl_currency_exchange_ratesSearchObj);
        var arr_Curr_json = [];
        myResults.forEach(function (result) {
            var obj_json_Container = {}
            obj_json_Container = {
                'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
                'Month': result.getText({ name: "custrecord_pl_currency_exchange_month"}),
                'Year': result.getText({ name: "custrecord_pl_currency_exchange_year"}),
                'USD_INR_Revenue_Rate':result.getValue({ name: "custrecord_pl_currency_exchange_revnue_r"}),
                'USD_INR_Cost_Rate':result.getValue({ name: "custrecord_pl_currency_exchange_cost"}),
                'CRC_COST_CONVERSION':result.getValue({ name: "custrecord_pl_crc_cost_rate"}),
                'NOK_COST_RATE':result.getValue({ name: "custrecord_pl_nok_cost_rate"}),
                'USD_GBP': result.getValue({ name: "custrecord_gbp_converstion_rate"}),
                'USD_EUR':result.getValue({ name: "custrecord_eur_usd_conv_rate"}),
                'USD_AUD':result.getValue({ name: "custrecord_aud_to_usd"}),
                'USD_CAD':result.getValue({ name: "custrecord_usd_cad_conversion_rate"}),
                'USD_RON':result.getValue({ name: "custrecord_usd_ron_conversion_rate"}),
            };
            arr_Curr_json.push(obj_json_Container);
            return true;
        });
        return arr_Curr_json;
    } //// End of try
    catch (s_Exception) {
        log.debug('get_Employee_Data s_Exception== ', s_Exception);
    } //// End of catch 
   } 
function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}