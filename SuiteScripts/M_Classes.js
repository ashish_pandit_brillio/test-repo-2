function Response() {
	this.Status = false;
  	//this.TimeStamp = "";
	this.Data = "";
  
}

function AccessAllowed(count, hasAccess, info, imageUrl, label, data,
        taskTitle, redirect, additionalInfo)
{
	this.Count = count;
	this.HasAccess = hasAccess ? hasAccess : false;
	this.Info = info;
	this.ImageUrl = imageUrl;
	this.Label = label;
	this.Data = data;
	this.TaskTitle = taskTitle;
	this.Redirect = redirect;
	this.AdditionalInfo = additionalInfo ? additionalInfo : '';
}

function TimesheetQuickApprovalPool() {
	this.TimesheetId = "";
	this.EmployeeName = "";
	this.WeekStartDate = "";
	this.WeekEndDate = "";
	this.ST = "00:00";
	this.OT = "00:00";
	this.Leave = "00:00";
	this.Holiday = "00:00";
	this.TotalApprovable = "";
	this.Checked = false;
}

function ExpensesListItem() {
	this.InternalId = "";
	this.ExpenseName = "";
	this.EmployeeName = "";
	this.TotalAmount = "";
	this.Currency = "";
	this.Subsidiary = "";
	this.TransactionDate = "";
	this.Memo = "";
	this.Checked = false;
}

function TravelListItem() {
	this.InternalId = "";
	this.EmployeeName = "";
	this.Purpose = "";
	this.TravelId = "";
	this.DepartureDate = "";
	this.Type = "";
	this.Project = "";
	this.Checked = false;
}

function PuchaseRequestListItem() {
	this.InternalId = "";
	this.PrNo = "";
	this.EmployeeName = "";
	this.ItemName = "";
	this.CurrencyText = "";
	this.TotalCost = "";
	this.StatusText = "";
}