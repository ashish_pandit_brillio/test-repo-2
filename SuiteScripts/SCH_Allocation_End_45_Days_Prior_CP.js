/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 May 2017     deepak.srinivass
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
    try {
        var context = nlapiGetContext();

        var current_date = nlapiStringToDate(nlapiDateToString(new Date()));
        nlapiLogExecution('DEBUG', 'Start current_date:-- ', current_date);
        var employee_allocated_list = new Array();

        var currentDate = sysDate(); // returns the date
        var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
        var currentDateAndTime = currentDate + ' ' + currentTime;
        // nlapiSetFieldValue('custentity_date_time', currentDateAndTime);
        nlapiLogExecution('DEBUG', 'User Event Script', currentDateAndTime);

        var proj_Status = [];
        proj_Status.push('2');
        proj_Status.push('4');
        var filters_PM = [
            [
                ['status', 'anyof', 2], 'or',
                ['status', 'anyof', 4]
            ], 'and',
            ['type', 'anyof', 2]

        ];

        var columns_PM = new Array();
        columns_PM[0] = new nlobjSearchColumn('internalid');
        columns_PM[1] = new nlobjSearchColumn('entityid');
        //columns_PM[2] = new nlobjSearchColumn('email', 'custentity_projectmanager');
        //columns_PM[2] = new nlobjSearchColumn('email', 'custentity_deliverymanager');
        columns_PM[2] = new nlobjSearchColumn('custentity_projectmanager');
        columns_PM[3] = new nlobjSearchColumn('custentity_deliverymanager');
        columns_PM[4] = new nlobjSearchColumn('custentity_clientpartner');
        //columns_PM[5] = new nlobjSearchColumn('email','custrecord_onsite_practice_head');
        //columns_PM[6] = new nlobjSearchColumn('email', 'custentity_deliverymanager');

        //var project_PM_Res = nlapiSearchRecord('job', null, filters_PM, columns_PM);

        var searchResult = nlapiCreateSearch('job', filters_PM, columns_PM);
        var resultset_ = searchResult.runSearch();
        var project_PM_Res = [];
        var searchid_ = 0;
        var mail_counter = 0;
        var slNo = 1;
        var sl_No = 1; //Added on 03/08/2020
        var emp_level_practice_id; //Added on 03/08/2020
        var currentContext_C = nlapiGetContext();
        var remaining_usage = currentContext_C.getRemainingUsage();
        //nlapiLogExecution('DEBUG','OFC Cost remaining_usage',remaining_usage);
        do {
            var resultslice_ = resultset_.getResults(searchid_, searchid_ + 1000);
            for (var re in resultslice_) {
                if (currentContext_C.getRemainingUsage() <= 1000) {
                    nlapiYieldScript();
                }
                project_PM_Res.push(resultslice_[re]);
                searchid_++;
            }
        } while (resultslice_.length >= 1000);
        //End
        var brillioProject = 0; //Added on 03/08/2020
		//nlapiLogExecution('DEBUG','project_PM_Res_length_log','project_PM_Res.length=>' +project_PM_Res.length); //Added on 03/08/2020
        if (_logValidation(project_PM_Res)) {
            for (var i_indx = 0; i_indx < project_PM_Res.length; i_indx++) 
			{
                var usageEnd = context.getRemainingUsage();
                if (usageEnd < 1000) {
                    yieldScript(context);
                }


                var i_project_pm = project_PM_Res[i_indx].getValue('custentity_projectmanager');
                var i_project_cp = project_PM_Res[i_indx].getValue('custentity_clientpartner');
				//nlapiLogExecution('DEBUG','i_project_cp_log','i_project_cp=>' +i_project_cp); //Added on 03/08/2020

                if (_logValidation(i_project_cp)) {
                    /*var filters_allocation = [ 
                    							
                    							[ 'job.custentity_projectmanager', 'anyof', i_project_pm ]
                    						  ];*/
                    //Table Creation
                    // Table For Practice total count
                    //strVar += ' <tr><td width="20%" font-size="11" align="center">Total Head Count Detail</td></tr>';
                    //strVar += '	<tr>';
                    //strVar += ' <td width="100%">';
                    var strVar = '';
                    strVar += '<table width="100%" border="1">';
                    strVar += '	<tr>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Sl No.</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Account</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee ID</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee Name</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee Sub-Practice</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Reporting Manager</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Client Partner</td>';
                    //strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project Manager</td>';
                    //strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Delievery Manager</td>';
                    strVar += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Allocation End Date</td>';
                    strVar += '	</tr>';

                    var str_Var = '';
                    str_Var += '<table width="100%" border="1">';
                    str_Var += '	<tr>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Sl No.</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Account</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Project</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee ID</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee Name</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Employee Sub-Practice</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Reporting Manager</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Client Partner</td>';
                    str_Var += ' <td width="9%" font-size="11" align="center" bgcolor="#D3D3D3">Allocation End Date</td>';
                    str_Var += '	</tr>';


                    var filters_allocation = [];
                    filters_allocation[0] = new nlobjSearchFilter('custentity_clientpartner', 'job', 'anyof', i_project_cp);

                    var columns = new Array();
                    columns[0] = new nlobjSearchColumn('internalid');
                    columns[1] = new nlobjSearchColumn('custeventrbillable');
                    columns[2] = new nlobjSearchColumn('jobbillingtype', 'job');
                    columns[3] = new nlobjSearchColumn('custentity_clientpartner', 'job');
                    columns[4] = new nlobjSearchColumn('custentity_projectmanager', 'job');
                    columns[5] = new nlobjSearchColumn('custentity_deliverymanager', 'job');
                    columns[6] = new nlobjSearchColumn('custentity_verticalhead', 'job');
                    columns[7] = new nlobjSearchColumn('custevent_practice');
                    columns[8] = new nlobjSearchColumn('customer', 'job');
                    columns[9] = new nlobjSearchColumn('company');
                    columns[10] = new nlobjSearchColumn('custentity_project_allocation_category', 'job');
                    columns[11] = new nlobjSearchColumn('subsidiary', 'employee');
                    columns[12] = new nlobjSearchColumn('custentity_clientpartner', 'customer');
                    columns[13] = new nlobjSearchColumn('custentity_region', 'customer');
                    columns[14] = new nlobjSearchColumn('custentity_region', 'job');
                    columns[15] = new nlobjSearchColumn('percentoftime');
                    columns[16] = new nlobjSearchColumn('custrecord_parent_practice', 'custevent_practice');
                    columns[17] = new nlobjSearchColumn('custentity_practice', 'job');
                    columns[18] = new nlobjSearchColumn('department', 'employee');
                    columns[19] = new nlobjSearchColumn('resource');
                    columns[20] = new nlobjSearchColumn('startdate');
                    columns[21] = new nlobjSearchColumn('enddate');
                    columns[22] = new nlobjSearchColumn('employeestatus', 'employee');
                    columns[23] = new nlobjSearchColumn('title', 'employee');
                    columns[24] = new nlobjSearchColumn('custeventbstartdate');
                    columns[25] = new nlobjSearchColumn('custeventbenddate');
                    columns[26] = new nlobjSearchColumn('custentity_reportingmanager', 'employee');
                    columns[27] = new nlobjSearchColumn('employeetype', 'employee');
                    columns[28] = new nlobjSearchColumn('hiredate', 'employee');
                    columns[29] = new nlobjSearchColumn('approver', 'employee');
                    columns[30] = new nlobjSearchColumn('timeapprover', 'employee');
                    columns[31] = new nlobjSearchColumn('custevent3');
                    columns[32] = new nlobjSearchColumn('custevent_otrate');
                    columns[33] = new nlobjSearchColumn('custevent_monthly_rate');
                    columns[34] = new nlobjSearchColumn('startdate', 'job');
                    columns[35] = new nlobjSearchColumn('enddate', 'job');
                    columns[36] = new nlobjSearchColumn('custentity_reportingmanager', 'employee');
                    columns[37] = new nlobjSearchColumn('email', 'employee');
                    columns[38] = new nlobjSearchColumn('internalid', 'employee');
                    columns[39] = new nlobjSearchColumn('custentity_fusion_empid', 'employee');
                    columns[40] = new nlobjSearchColumn('firstname', 'employee');
                    columns[41] = new nlobjSearchColumn('middlename', 'employee');
                    columns[42] = new nlobjSearchColumn('lastname', 'employee');
                    var RM_list = [];
                    var email_flag = true;
                    var email_flag1 = true;
					
		//Getting deployment ID
		var deploymentId = nlapiGetContext().getDeploymentId();
		nlapiLogExecution('DEBUG','projectdeploymentId_log','deploymentId=>' +deploymentId); //Added on 07/08/2020
		
		var project_allocation_result = '';
          switch (deploymentId) {

              case "customdeploy1":
			  
			     project_allocation_result = nlapiSearchRecord('resourceallocation', 'customsearch1879', filters_allocation, columns);
				
                  break;
				  
              case "customdeploy_sch_allocation_cp":   
			  
			    project_allocation_result = nlapiSearchRecord('resourceallocation', 'customsearch3401', filters_allocation, columns);
                  break;
            }
                  


                    if (_logValidation(project_allocation_result)) {
                        for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {
                            nlapiLogExecution('debug', 'project_allocation_result Length:-- ', project_allocation_result.length);
                            var usageEnd = context.getRemainingUsage();
                            if (usageEnd < 1000) {
                                yieldScript(context);
                            }
                            if (employee_allocated_list.indexOf(project_allocation_result[i_search_indx].getId()) >= 0) {

                            } else {

                                var emp_full_name = '';
                                mail_counter = mail_counter + 1;
                                var is_resource_billable = project_allocation_result[i_search_indx].getText('custeventrbillable');
                                var billing_type = project_allocation_result[i_search_indx].getText('jobbillingtype', 'job');
                                var client_partner = project_allocation_result[i_search_indx].getText('custentity_clientpartner', 'job');
                                var client_partner_id = project_allocation_result[i_search_indx].getValue('custentity_clientpartner', 'job');
                                var proj_manager = project_allocation_result[i_search_indx].getText('custentity_projectmanager', 'job');
                                var proj_manager_id = project_allocation_result[i_search_indx].getValue('custentity_projectmanager', 'job');
                                var delivery_manager = project_allocation_result[i_search_indx].getText('custentity_deliverymanager', 'job');
                                var delivery_manager_id = project_allocation_result[i_search_indx].getValue('custentity_deliverymanager', 'job');
                                var vertical_head = project_allocation_result[i_search_indx].getText('custentity_verticalhead', 'job');
                                var parctice_id = project_allocation_result[i_search_indx].getValue('custevent_practice');
                                var parctice_name = project_allocation_result[i_search_indx].getText('custevent_practice');
                                var parctice_id_parent = project_allocation_result[i_search_indx].getValue('custrecord_parent_practice', 'custevent_practice');
                                var parctice_parent_name = project_allocation_result[i_search_indx].getText('custrecord_parent_practice', 'custevent_practice');
                                var project_cust = project_allocation_result[i_search_indx].getValue('customer', 'job');
                                var proj_cust_name = project_allocation_result[i_search_indx].getText('customer', 'job');
                                var project_id = project_allocation_result[i_search_indx].getValue('company');
                                var project_name = project_allocation_result[i_search_indx].getText('company');
                                var is_proj_bench_category = project_allocation_result[i_search_indx].getValue('custentity_project_allocation_category', 'job');
                                var emp_susidiary = project_allocation_result[i_search_indx].getText('subsidiary', 'employee');
                                var emp_level_practice = project_allocation_result[i_search_indx].getText('department', 'employee');
                                nlapiLogExecution('DEBUG', 'emp_level_practice', emp_level_practice);
                                emp_level_practice_id = project_allocation_result[i_search_indx].getValue('department', 'employee'); //Added on 03/08/2020
                                nlapiLogExecution('DEBUG', 'emp_level_practice_id', emp_level_practice_id);
                                var customer_CP_id = project_allocation_result[i_search_indx].getValue('custentity_clientpartner', 'customer');
                                var customer_CP_name = project_allocation_result[i_search_indx].getText('custentity_clientpartner', 'customer');
                                var customer_region = project_allocation_result[i_search_indx].getText('custentity_region', 'customer');
                                var project_region = project_allocation_result[i_search_indx].getText('custentity_region', 'job');
                                var execusting_practice = project_allocation_result[i_search_indx].getText('custentity_practice', 'job');
                                var resource_id = project_allocation_result[i_search_indx].getValue('resource');
                                var resource_name = project_allocation_result[i_search_indx].getText('resource');
                                var emp_reporting_manager = project_allocation_result[i_search_indx].getValue('custentity_reportingmanager', 'employee');
                                var emp_reporting_manager_name = project_allocation_result[i_search_indx].getText('custentity_reportingmanager', 'employee');
                                var emp_email = project_allocation_result[i_search_indx].getValue('email', 'employee');
                                var emp_user_id = project_allocation_result[i_search_indx].getValue('internalid', 'employee');
                                var allocation_rec = project_allocation_result[i_search_indx].getValue('internalid');
                                var end_date = project_allocation_result[i_search_indx].getValue('enddate');
                                var start_date = project_allocation_result[i_search_indx].getValue('startdate');

                                var emp_id = project_allocation_result[i_search_indx].getValue('custentity_fusion_empid', 'employee');
                                var emp_frst_name = project_allocation_result[i_search_indx].getValue('firstname', 'employee');
                                var emp_middl_name = project_allocation_result[i_search_indx].getValue('middlename', 'employee');
                                var emp_lst_name = project_allocation_result[i_search_indx].getValue('lastname', 'employee');

                                if (emp_frst_name)
                                    emp_full_name = emp_frst_name;

                                if (emp_middl_name)
                                    emp_full_name = emp_full_name + ' ' + emp_middl_name;

                                if (emp_lst_name)
                                    emp_full_name = emp_full_name + ' ' + emp_lst_name;
                                //Get Reporting Manager Email
                                var RM_Lookup = nlapiLookupField('employee', emp_reporting_manager, ['firstname', 'email']);
                                var RM_Email = RM_Lookup.email;
                                var RM_name = RM_Lookup.firstname;
                                RM_list.push(RM_Email);
                                //Get Project Manager Email
                                var PM_Lookup = nlapiLookupField('employee', proj_manager_id, ['firstname', 'email']);
                                var PM_Email = PM_Lookup.email;
                                //Get Delievery Manager Email
                                var DM_Lookup = nlapiLookupField('employee', delivery_manager_id, ['firstname', 'email']);
                                var DM_Email = DM_Lookup.email;
                                //Get Delievery Manager Email
                                var CP_Lookup = nlapiLookupField('employee', client_partner_id, ['firstname', 'email']);
                                var CP_Email = CP_Lookup.email;
                                var CP_name = CP_Lookup.firstname;
                                RM_list.push(CP_Email);

                                var emp_lookUP = nlapiLookupField('employee', parseInt(resource_id), ['firstname', 'email']);
                                var emp_name = emp_lookUP.firstname;


                                var filtr = [];
                                filtr[0] = new nlobjSearchFilter('resource', null, 'anyof', resource_id);
                                filtr[1] = new nlobjSearchFilter('internalid', null, 'noneof', allocation_rec);

                                var cols_R = [];
                                /*cols_R[0] = new nlobjSearchColumn('resource');
                                cols_R[1] = new nlobjSearchColumn('enddate');
                                cols_R[2] = new nlobjSearchColumn('startdate');*/
                                cols_R[0] = new nlobjSearchColumn('internalid');

                                var searchObj = nlapiSearchRecord('resourceallocation', 'customsearch_check_if_any_allocation_fou', filtr, cols_R);
                                
                                if (_logValidation(searchObj) && searchObj.length > 0) {
									var count_searchObj = searchObj.length
                                nlapiLogExecution('DEBUG', 'COUNT_SEARCH_OBJ', 'count_searchObj=>' + count_searchObj);

                                } else {

                                    //Create Excel Header
                                    email_flag = false;
                                    strVar += '	<tr>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + slNo + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + proj_cust_name + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + project_name + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + emp_id + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + emp_full_name + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + emp_level_practice + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + emp_reporting_manager_name + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + client_partner + '</td>';
                                    strVar += ' <td width="9%" font-size="11" align="center">' + end_date + '</td>';
                                    strVar += '	</tr>';

                                    //adding Condition for birllio Analyitcs project  //Added on 03/08/2020
                                    if (emp_level_practice_id == 322 || emp_level_practice_id ==511) {
                                        email_flag1 = false;
                                        str_Var += '<tr>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + sl_No + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + proj_cust_name + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + project_name + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + emp_id + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + emp_full_name + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + emp_level_practice + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + emp_reporting_manager_name + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + client_partner + '</td>';
                                        str_Var += ' <td width="9%" font-size="11" align="center">' + end_date + '</td>';
                                        str_Var += '</tr>';

                                        brillioProject = 1;
                                    }

                                    slNo++;
                                    sl_No++; //Added on 3/08/2020

                                    mail_counter++;
                                    //TimeStamp to update the allocation Record



                                    employee_allocated_list.push(allocation_rec);
                                    //  nlapiLogExecution('DEBUG', 'Allocation Rec id', id);
                                    yieldScript(context);
                                }

                            }
                        }
                        if (email_flag == false) 
						{
                            strVar += '</table>';
							str_Var += '</table>';
                            //Send Mail
                            var strVar_ = '';
                            strVar_ += '<html>';
                            strVar_ += '<body>';

                            strVar_ += '<p>Dear Client Partner and Reporting Manager,</p>';
                            //strVar_ += '<br/>';
                            strVar_ += '<p>Here is the list of employees with their current project allocation ending in the next 6-7 weeks. Please work with your clients on extensions and update the Business Operations team accordingly. ';
                            strVar_ += 'Please note that employees will get a system generated email, 4 weeks prior to the allocation end date notifying them about the project wind down. ';
                            strVar_ += 'Hence it is advised to extend the allocation end dates in NetSuite within the next 2 weeks if the project is getting extended.</p>';

                            //strVar += '<p> While Brillio will make all efforts to find you suitable opportunities, should none be available at the end of this 4-week period, you may be required to consider relocating to other locations in the US or our offices in India, or separate from the company depending on project demands.';
                            //strVar += ' Please prepare yourself accordingly.  You will receive an update from your reporting manager on this at least 2 weeks prior to your current allocation / assignment end date. </p>';
                            //strVar += '<p> Do not hesitate to contact your reporting manager or HR should you need any further clarifications.</p>';
                            //strVar += '<a href="https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1044&deploy=1&proj_id='+nlapiGetRecordId()+'>Project Setup</a>';
                            strVar_ += '<br/>';
                            strVar_ += strVar;
                            //strVar += '<p>If you have received an email with the subject <b>“Allocation Ending Notification”</b> please ignore the message.<br/>';
                            //strVar += 'Please do not hesitate to reach out to your account / reporting managers should you have any questions or concerns.</p>';
                            //strVar += ' You are hereby requested to contact your reporting manager to learn about possible extension of your current allocation / assignment and other opportunities.</p>';
                            strVar_ += '<br/>';
                            strVar_ += '<p>Thanks & Regards,</p>';
                            strVar_ += '<p>Resource Fulfilment Team</p>';

                            strVar_ += '</body>';
                            strVar_ += '</html>';




                            //End of Email Body

                            /*var receipt = [];
                            receipt[0] = emp_email;
                            var cc = Array();
                            cc[0] = PM_Email;
                            cc[1] = DM_Email;
                            cc[2] = CP_Email;
                            cc[3] = RM_Email;
                            cc[4] = 'swapna.bhatt@brillio.com';
                            cc[5] = 'suchin.bhat@brillio.com';*/
                            RM_list = removearrayduplicate(RM_list);
                            var receipt = [];
                            for (var j = 0; j < RM_list.length; j++) {
                                receipt[j] = RM_list[j];
                            }
                            //receipt = RM_list;
                            //receipt[1] = RM_Email;

                            var cc = Array();
                            cc[0] = 'bhumika.sharma@brillio.com';
                            cc[1] = 'swetha.b@brillio.com';
                            //cc[2] = 'Jayanth.Selvappullai@brillio.com';
                            /*cc[1] = DM_Email;
                            cc[2] = CP_Email;
                            cc[3] = RM_Email;
                            cc[4] = 'swapna.bhatt@brillio.com';
                            cc[5] = 'suchin.bhat@brillio.com';*/
                            //removing duplicates innarray
                            //cc = removearrayduplicate(cc);

                            //Adding code for specific users  //added on 03/08/2020
                            var receiptant_3users = Array();
                            receiptant_3users[0] = 'Smitha.Thumbikkat@brillio.com';
                            receiptant_3users[1] = 'archana.r@brillio.com';
                            receiptant_3users[2] = 'senjuti.c1@brillio.com';
                            receiptant_3users[3] = 'anukaran@inspirria.com';

                            var bcc = [];
                            bcc[0] = 'deepak.srinivas@brillio.com'
                            //nlapiLogExecution('audit', 'proj detail len:-- ', proj_data.length);
                            //Sending Email Functionality
                            var today = new Date();
                            var date_yesterday = nlapiAddDays(today, -1);
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            var hours = today.getHours();
                            var minutes = today.getMinutes();

                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }
                            var today = dd + '/' + mm + '/' + yyyy;

                            var a_emp_attachment = new Array();
                            a_emp_attachment['entity'] = 3165;
                            //nlapiSendEmail(442, bcc, 'Notification: Allocation Ending In 30-Days dt '+today, strVar,bcc,bcc);
                            if (_logValidation(receipt))
                                nlapiSendEmail(442, receipt, 'Allocation Ending Notification', strVar_, cc, bcc, a_emp_attachment);
                            //nlapiSendEmail(442, bcc, 'Allocation Ending Notification', strVar_,bcc,null,a_emp_attachment);
                            nlapiLogExecution('debug', 'Email Sent Resource:-- ', receipt);

                            //Send email to 3 persons only	
                            if ((brillioProject == 1 || email_flag1 == false )&& deploymentId == 'customdeploy_sch_allocation_cp') 
							{
								nlapiLogExecution('debug', 'Email Send to 3 Persons Condition', 'Email Send to 3 Persons Condition');
                                var strVar__ = '';
                                strVar__ += '<html>';
                                strVar__ += '<body>';

                                strVar__ += '<p>Dear All,</p>';

                                strVar__ += '<p>Here is the list of employees with their current project allocation ending in the next 6-7 weeks. Please work with your clients on extensions and update the Business Operations team accordingly. ';
                                strVar__ += 'Please note that employees will get a system generated email, 4 weeks prior to the allocation end date notifying them about the project wind down. ';
                                strVar__ += 'Hence it is advised to extend the allocation end dates in NetSuite within the next 2 weeks if the project is getting extended.</p>';
                                strVar__ += '<br/>';
                                strVar__ += str_Var;
                                strVar__ += '<br/>';
                                strVar__ += '<p>Thanks & Regards,</p>';
                                strVar__ += '<p>Resource Fulfilment Team</p>';
                                strVar__ += '</body>';
                                strVar__ += '</html>';

                                nlapiSendEmail(442, receiptant_3users, 'Allocation Ending Notification', strVar__, null, null, a_emp_attachment);
								nlapiLogExecution('debug', 'Email Sent Resource Conditions: -- ', receiptant_3users);
                            }

                            email_flag = true;
							email_flag1 == true;
                            slNo = 1;
                            sl_No = 1;
                        }
                    }
                }
            }
        }
        if (mail_counter <= 0) {

            var a_emp_attachment = new Array();
            a_emp_attachment['entity'] = 41571;

            var strVar_ = '';

            strVar_ += '<html>';
            strVar_ += '<body>';

            strVar_ += '<p>No Data Found on date ' + currentDateAndTime + '.</p>';

            strVar_ += '<p>Thanks & Regards,</p>';
            strVar_ += '<p>Team IS</p>';

            strVar_ += '</body>';
            strVar_ += '</html>';
            nlapiSendEmail(442, 'deepak.srinivas@brillio.com', 'CP: Notification Allocation ending in next 45-Days from today', strVar_, null, null, a_emp_attachment);
            nlapiLogExecution('debug', 'Email Sent For No Resource:-- ');
            
			
			//Send email to 3 persons only	
			if(deploymentId == 'customdeploy_sch_allocation_cp')
			{
                var strVar__ = '';
                strVar__ += '<html>';
                strVar__ += '<body>';
                strVar__ += '<p>No Data Found on date ' + currentDateAndTime + '.</p>';
                strVar__ += '<p>Thanks & Regards,</p>';
                strVar__ += '<p>Team IS</p>';
                strVar__ += '</body>';
                strVar__ += '</html>';
                nlapiSendEmail(442, 'deepak.srinivas@brillio.com', 'CP: Notification Allocation ending in next 45-Days from today', strVar__, null, null, a_emp_attachment);
			}
        }
    } catch (err) {
        nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
    }
}

function _logValidation(value) {
    if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function yieldScript(currentContext) {

    nlapiLogExecution('AUDIT', 'API Limit Exceeded');
    var state = nlapiYieldScript();

    if (state.status == "FAILURE") {
        nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' +
            state.reason + ' / Size : ' + state.size);
        return false;
    } else if (state.status == "RESUME") {
        nlapiLogExecution('AUDIT', 'Script Resumed');
    }
}

function generate_excel(strVar_excel, counter) {
    var strVar1 = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
        '<head>' +
        '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>' +
        '<meta name=ProgId content=Excel.Sheet/>' +
        '<meta name=Generator content="Microsoft Excel 11"/>' +
        '<!--[if gte mso 9]><xml>' +
        '<x:excelworkbook>' +
        '<x:excelworksheets>' +
        '<x:excelworksheet=sheet1>' +
        '<x:name>** ESTIMATE FILE**</x:name>' +
        '<x:worksheetoptions>' +
        '<x:selected></x:selected>' +
        '<x:freezepanes></x:freezepanes>' +
        '<x:frozennosplit></x:frozennosplit>' +
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>' +
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>' +
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>' +
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>' +
        '<x:activepane>0</x:activepane>' + // 0
        '<x:panes>' +
        '<x:pane>' +
        '<x:number>3</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>1</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>2</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>0</x:number>' + //1
        '</x:pane>' +
        '</x:panes>' +
        '<x:protectcontents>False</x:protectcontents>' +
        '<x:protectobjects>False</x:protectobjects>' +
        '<x:protectscenarios>False</x:protectscenarios>' +
        '</x:worksheetoptions>' +
        '</x:excelworksheet>' +
        '</x:excelworksheets>' +
        '<x:protectstructure>False</x:protectstructure>' +
        '<x:protectwindows>False</x:protectwindows>' +
        '</x:excelworkbook>' +

        // '<style>'+

        //-------------------------------------
        '</x:excelworkbook>' +
        '</xml><![endif]-->' +
        '<style>' +
        'p.MsoFooter, li.MsoFooter, div.MsoFooter' +
        '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}' +
        '<style>' +

        '<!-- /* Style Definitions */' +

        //'@page Section1'+
        //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

        'div.Section1' +
        '{ page:Section1;}' +

        'table#hrdftrtbl' +
        '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->' +

        '</style>' +


        '</head>' +


        '<body>' +

        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
    var fileName = 'Resource Allocation Data  ' + counter + ' - ' +
        nlapiDateToString(new Date(), 'date') + '.xls';
    var strVar2 = strVar_excel;
    strVar1 = strVar1 + strVar2;
    var file = nlapiCreateFile(fileName, 'XMLDOC', strVar1);

    //file.setFolder('163597'); 
    var id2 = nlapiSubmitFile(file);
    nlapiLogExecution('DEBUG', 'File Created ID  :--', id2);
    return file;
}

function sysDate() {
    var date = new Date();
    var tdate = date.getDate();
    var month = date.getMonth() + 1; // jan = 0
    var year = date.getFullYear();
    return currentDate = month + '/' + tdate + '/' + year;
}

function timestamp() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    str += hours + ":" + minutes + ":" + seconds + " ";

    return str + meridian;
}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i])
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}