/**
 * @author Deepak
 */

function suiteletFunction(request, response)
{
	try
	{
		var i_recordID = request.getParameter('custscript_record_id');
		i_recordID = i_recordID.trim();
		nlapiLogExecution('DEBUG','rcrd_id:-- ',i_recordID);
		
		var params=new Array();
		params['custscript_exclude_ofc_cost_id'] = i_recordID;
		
		var status = nlapiScheduleScript('customscript_sch_ofc_cost_create_je',null,params);
		
		nlapiSubmitField('customrecord_ofc_cost_against_customer', parseInt(i_recordID), 'custrecord_ofc_cost_status', 'Pending');
		
		nlapiSetRedirectURL('RECORD', 'customrecord_ofc_cost_against_customer', i_recordID, null,null);
	} 
	catch (err) {
		nlapiLogExecution('DEBUG', 'ERROR MESSAGE:-- ', err);
	}
}