/*
   	 Script Name:   SUT TDS Payment list section wise
	 Author:        Nikhil jain
	 Company:		Aashna Cloudtech Pvt. Ltd.
	 Date:
	 Version:
	 Description:	Vendor TDS payment form section wise
	 Below is a summary of the process controls enforced by this script file.  The control logic is described
	 more fully, below, in the appropriate function headers and code blocks.

{

	Script Modification Log:


	-- Date --			-- Modified By --				--Requested By--				-- Description --
  
 
Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)
         VendorLiabilityForm(request, response)

     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:
            populateaccount(request, response, alist)
			addReturnButton(form)
			setSubList(sublist, form, isForm, results, request, response)
			searchBill(request, response)
			popluattaxagency(request, response, taxagency)
			popluatlocation(request, response, taxagency)
			accountbal()
			processLineItem(name, request, isDevice)
               - NOT USED
}
*/

// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY



}

	//===== Begin: Declaration of Global variable which is used in script ====
	var tdslist = 'tdslist';
	var name = 'name'
	var custrecord2 = 'custrecord2';
	var custrecord_tds_threshold = 'custrecord_tdsthreshold';
	var custrecord_net_per = 'custrecord_netper';
	var custrecord_tds_account = 'custrecord_tdsaccount';
	var apply = 'apply'
	var type = 'type'
	var internalid = 'internalid'
	var b_Mand_Class;
	var b_Mand_department;
	var b_Mand_location;
	
	// ==== End: Declaration of Global variable which is used in script ====

	// ==== Begin: Function for Creating TDS Payable form ====
	function VendorLiabilityForm(request, response)
	{
	    if (request.getMethod() == 'GET')
		{
			
			
        var form = nlapiCreateForm('TDS Payment List Form');

        form.setScript('customscript_cli_tds_liability_list');
		
		// ===== GET THE VALUES OF GLOBAL SUBSIDIARY PARAMETERS ====
			var i_AitGlobalRecId =  SearchGlobalParameter();
			nlapiLogExecution('DEBUG','Bill ', "i_AitGlobalRecId"+i_AitGlobalRecId);
			
			//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
			if(i_AitGlobalRecId != 0 )
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter',i_AitGlobalRecId);
				
				a_subisidiary= o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG','Bill ', "a_subisidiary->"+a_subisidiary);
				
				i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
				nlapiLogExecution('DEBUG','Bill ', "i_vatCode->"+i_vatCode);
				
				i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
				nlapiLogExecution('DEBUG','Bill ', "i_taxcode->"+i_taxcode);
				
				b_Mand_location = o_AitGloRec.getFieldValue('custrecord_ait_location');
				nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_location->" + b_Mand_location);
				
				b_Mand_Class = o_AitGloRec.getFieldValue('custrecord_ait_class');
				nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_Class->" + b_Mand_Class);
				
				b_Mand_department = o_AitGloRec.getFieldValue('custrecord_ait_department');
				nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_department->" + b_Mand_department);
				
				
			}

        var acclist = form.addField('account', 'select', 'Bank A/C');
        acclist.setMandatory(true);
        acclist.addSelectOption('', '');
        populateaccount(request, response, acclist);

        var bankblance = form.addField('bankbal', 'text', 'Balance');
        bankblance.setDisplayType('inline');

        var sysdate = new Date;
        var date1 = nlapiDateToString(sysdate);
        var pdate = form.addField('pdate', 'date', 'Date');
        pdate.setMandatory(true);
        pdate.setDefaultValue(date1);

        form.addField('memo', 'text', 'Memo');

        form.addField('cheque', 'text', 'Cheque No');

        var totalamt = form.addField('totalamt', 'currency', 'Total Amount');
        totalamt.setDisplayType('inline');

        var taxagency = form.addField('taxage', 'select', 'Tax Agency');
        taxagency.setMandatory(true);
        taxagency.addSelectOption('', '');
        popluattaxagency(request, response, taxagency);

        var class1 = form.addField('class', 'select', 'Class', 'classification');
		if (b_Mand_Class == 'T') 
		{
			class1.setMandatory(true);
		}
        var department = form.addField('department', 'select', 'Departement', 'department');
		if (b_Mand_department == 'T') 
		{
			department.setMandatory(true);
		}	
        var loc = form.addField('location', 'select', 'Location');
        loc.addSelectOption('', '');
		if (b_Mand_location == 'T') 
		{
			loc.setMandatory(true);
		}
        popluatlocation(request, response, loc);

        var bsrcode = form.addField('bsrcode', 'text', 'BSR Code');
        bsrcode.setDisplayType('entry');

        var paymentdate = form.addField('paymentdate', 'date', 'Payment Date');
        paymentdate.setDisplayType('entry');

        var Challan_No = form.addField('challanno', 'text', 'Challan No');
        Challan_No.setDisplayType('entry');

        var postingperiod = request.getParameter('accountingperiod');
        nlapiLogExecution('DEBUG', 'Bill ', "postingperiod->" + postingperiod);
		
        var subsidiary = request.getParameter('subsidiary');
		
        var tds_section = request.getParameter('tds_section');
		
		var assessee_type = request.getParameter('assessee_type');
		
		var fromdate = request.getParameter('fromdate');
		
		var todate = request.getParameter('todate');
		
		var postingperiodfield = form.addField('accountingperiod', 'select', 'Posting Period', 'accountingperiod');//.setDisplayType('hidden');
		if(postingperiod == '' || postingperiod == null)	
			postingperiodfield.setDefaultValue('-1');
		else
			postingperiodfield.setDefaultValue(postingperiod);
		
		var fromdatefield = form.addField('fromdate','date','fromdate').setDisplayType('hidden');
		fromdatefield.setDefaultValue(fromdate);
				
		var todatefield = form.addField('todate','date','todate').setDisplayType('hidden');
		todatefield.setDefaultValue(todate);


        var subsidiaryfield = form.addField('subsidiary', 'select', 'Subsidiary', 'subsidiary');
        subsidiaryfield.setDefaultValue(subsidiary);
        subsidiaryfield.setDisplayType('inline');
		
		var tds = search_tds_type(tds_section,assessee_type);
		
        var tdsfield = form.addField('custpage_tdstype', 'multiselect', 'TDS', 'customrecord_tdsmaster');
        tdsfield.setDefaultValue(tds);
        tdsfield.setDisplayType('inline');
		
		var section = form.addField('tds_section', 'select', 'TDS Section', 'customlist_tds_section');
       	section.setDefaultValue(tds_section);
     	section.setDisplayType('inline');
		
		var tds_assessee = form.addField('tds_assessee_type', 'select', 'TDS Asseessee', 'customlist_assessee_code');
        tds_assessee.setDefaultValue(assessee_type);
        tds_assessee.setDisplayType('inline');
		
		// Add Export-Excel Button on TDS Payment List Section Wise Form
		form.addButton('custpage_addButton','Export-Excel','generateExcel_TDS_Payment_List_SectioinWise()');
		
        form.addTab("tab1", "TDS Detail");
        form.addSubmitButton('Submit');
        addReturnButton(form);
	
        //Code to Add The List
       /*
 var attResult = searchBill(request, response);
        var sublist1 = form.addSubList(tdslist, 'list', tdslist, 'tab1');
		//var mark_all = sublist1.addMarkAllButtons()
		sublist1.addButton('custpage_markall','Mark All',"MarkAll();");
		sublist1.addButton('custpage_unmarkall','UnMark All',"UnMarkAll();");
        var rs = setSubList(sublist1, form, 1, attResult);
*/
		var sublist1 = form.addSubList(tdslist, 'list', tdslist, 'tab1');
        var attResult = searchBill(sublist1, form, 1, request, response);
		sublist1.addButton('custpage_markall','Mark All',"MarkAll();");
		sublist1.addButton('custpage_unmarkall','UnMark All',"UnMarkAll();");

        response.writePage(form);
    }
    else
        if (request.getMethod() == 'POST')
		{
            processLineItem(tdslist, request, 1);
            nlapiSetRedirectURL('SUITELET', 'customscript_tds_pay_sectionwisecriteria', '1', false);
        }
	}

	// ==== End :Function for Creating TDS Payable form ===


	// ==== Begin :function for  Populate Account List ===
	function populateaccount(request, response, alist)
	{

	    var subsidiary = request.getParameter('subsidiary')
	    var filchk1 = new Array();
	    var columnsCheck1 = new Array();
	    filchk1.push( new nlobjSearchFilter('type', null, 'is', 'Bank'));
	    filchk1.push( new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
	    filchk1.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	    columnsCheck1.push( new nlobjSearchColumn('name'));
	    columnsCheck1.push( new nlobjSearchColumn('internalid'));

	    var searchResultsCheck1 = nlapiSearchRecord('account', null, filchk1, columnsCheck1);
	    for (i = 0; searchResultsCheck1 != null && i < searchResultsCheck1.length; i++)
		{
	        alist.addSelectOption(searchResultsCheck1[i].getValue('internalid'), searchResultsCheck1[i].getValue('name'), false);

	    }

	}

	//=== END :function for  Populate Account List ===

	// === Begin:Add return to criteria button ===
	function addReturnButton(form)
	{
	    var url = nlapiResolveURL('SUITELET', 'customscript_tds_pay_sectionwisecriteria', '1');
	    var script = "{ window.location = '" + url + "'}";
	    form.addButton('criteriabutton', 'Return to Criteria', script);
	    form.addPageLink('breadcrumb', 'Payment Criteria', nlapiResolveURL('SUITELET', 'customscript_tds_pay_sectionwisecriteria', '1'))
	}

	// === End: Add return to criteria button ===
	// === BEGIN : CODE FOR SETTING SUBMIST VALUE  ===
	/*
function setSubList(sublist, form, isForm, results, request, response)
	{

	    sublist.addField('apply', 'checkbox', 'Apply');
	    if (isForm == 1)
		{
	        //sublist.addField('custrecord2','text','Type')
	        var id = sublist.addField('internalid', 'text', 'Internal Id')
	        id.setDisplayType('hidden')
	        sublist.addField('custrecord_billdate', 'text', 'Date');
	        sublist.addField('custrecord_billbillno_display', 'text', 'Bill No');
	        sublist.addField('custrecord_billvendorrel_display', 'text', 'Vendor Name')

	        sublist.addField('custrecord_billtdstype_display', 'text', 'TDS Type')
	        sublist.addField('custrecord_bill_tds_section_display', 'text', 'TDS Section')
			sublist.addField('custrecord_billassessee_code_display', 'text', 'TDS Asseessee')
	        var TdsAccount = sublist.addField('custrecord_billtdsaccount', 'text', 'TDS Account')
	        TdsAccount.setDisplayType('hidden')
	        sublist.addField('custrecord_billtdsaccount_display', 'text', 'TDS Account Name')
	        sublist.addField('custrecord_billbillamount', 'text', 'Bill Amount');
	        sublist.addField('custrecord_billtdsamount', 'text', 'Total TDS Amount');
	        sublist.addField('custrecord_billtdspayable', 'text', 'TDS Payable Amount')
	    }
	    sublist.setLineItemValues(results);

	    return sublist;
	}
	// === END : CODE FOR SETTING SUBMIST VALUE  ===
	// === BEGIN : CODE FOR SEARCHING BILL  ===
	function searchBill(request, response)
	{
	    var fromdate = request.getParameter('fromdate')
	    var todate = request.getParameter('todate')
	    var subsidiary = request.getParameter('subsidiary')
		var tds_section = request.getParameter('tds_section')
		var tds_assesssee=request.getParameter('assessee_type')
	    var filters = new Array();
	    filters.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
	    filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	    filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
	    filters.push( new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	    filters.push( new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
		filters.push( new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
	    filters.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	    filters.push( new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
		//filters.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','check'));
	  	filters.push( new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is','F'))
		filters.push( new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is','T'))
		filters.push( new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is','check'))
		
	    var column = new Array();
	    column.push( new nlobjSearchColumn('custrecord_billvendorrel'));
	    column.push( new nlobjSearchColumn('custrecord_billbillno'));
	    column.push( new nlobjSearchColumn('custrecord_billbillamount'));
	    column.push( new nlobjSearchColumn('custrecord_billtdsamount'));
	    column.push( new nlobjSearchColumn('custrecord_billdate'));
	    column.push( new nlobjSearchColumn('custrecord_billtdstype'));
	    column.push( new nlobjSearchColumn('custrecord_billtdssection'));
	    column.push( new nlobjSearchColumn('custrecord_billtdsaccount'));
	    column.push( new nlobjSearchColumn('internalid'));
	    column.push( new nlobjSearchColumn('custrecord_billtdspayable'));
		column.push( new nlobjSearchColumn('custrecord_bill_tds_section'));
	    column.push( new nlobjSearchColumn('custrecord_billassessee_code'));

	    var results = nlapiSearchRecord('customrecord_tdsbillrelation',null, filters, column);
	    if(results != null)
		{
			//return results;
		}
		
		var filter1 = new Array();
	    filter1.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
	    filter1.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	    filter1.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
	    filter1.push( new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	  	filter1.push( new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
		filter1.push( new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
	    filter1.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	    filter1.push( new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
	    filter1.push( new nlobjSearchFilter('approvalstatus', 'custrecord_billbillno', 'is',2))
		filter1.push( new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is','T'))
		//filter1.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','bill'));
		filter1.push( new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is','F'))
		filter1.push( new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is','vendorbill'))
		
		
	    var res = nlapiSearchRecord('customrecord_tdsbillrelation',null, filter1, column);
	    if(res != null)
		{
			//return results;
			if (results != null)
			 {
				var results = res.concat(results);
			//return results;
			 }
			 else
			 {
			 	var results = res;
			 }
		}
		var filter2 = new Array();
	    filter2.push( new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
	    filter2.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	    filter2.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
	    filter2.push( new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
	  	filter2.push( new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
		filter2.push( new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
	    filter2.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	    filter2.push( new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
		// filter2.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','journal'));
		filter2.push( new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is','F'))
		filter2.push( new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is','journalentry'))
		//filter2.push( new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is','T'))
		var res2 = nlapiSearchRecord('customrecord_tdsbillrelation',null, filter2, column);
	    if(res2 != null)
		{
			var res3 = new Array()
			var i_new_bill = 0;
			var i_new_internalid = 0;
			for (var k = 0; k < res2.length; k++) 
			{
				//var bill = res2[k].getValue('custrecord_billbillno')
				//nlapiLogExecution('DEBUG','Bill ', "bill 3"+bill);
				
				var internal_id = res2[k].getValue('internalid')
				//nlapiLogExecution('DEBUG','Bill ', "internal_id 3"+internal_id);
				
				if(internal_id != i_new_internalid)
				{		
						var i_array_push = res2[k].getValue('internalid')
						
						res3.push(i_array_push)
						//nlapiLogExecution('DEBUG', 'Bill ', "res 3" + res3);
						
						//var i_new_bill = res2[k].getValue('custrecord_billbillno')
						//nlapiLogExecution('DEBUG','Bill ', "bill 3"+i_new_bill);
				
						var i_new_internalid = res2[k].getValue('internalid')
						//nlapiLogExecution('DEBUG','Bill ', "internal_id 3"+i_new_internalid);
				}
				
			}
			var filter3 = new Array();
			filter3.push( new nlobjSearchFilter('internalid', null, 'anyOf',res3));
			var res2 = nlapiSearchRecord('customrecord_tdsbillrelation',null, filter3, column);
			if (results != null) 
			{
				var results = res2.concat(results);
			}
			else
			{
				var results = res2
			}
			
		}
	 return results;
	}
*/
	function searchBill(sublist, form, isForm, request, response)
	{
		var postingperiod = request.getParameter('accountingperiod');
		var fromdate = request.getParameter('fromdate')
		var todate = request.getParameter('todate')
		var subsidiary = request.getParameter('subsidiary')
		var tds_section = request.getParameter('tds_section')
		var tds_assesssee = request.getParameter('assessee_type')
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
		if(_logValidation(postingperiod))
	    {
	    	filters.push( new nlobjSearchFilter('custrecord_iit_tds_posting_period', null, 'is', postingperiod));
	    }
	    else
	    {
	    	filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
		    filters.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
	    }
		//filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
		//filters.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
		filters.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
		filters.push(new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
		filters.push(new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
		//filters.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','check'));
		filters.push(new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is', 'F'))
		filters.push(new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is', 'T'))
		filters.push(new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is', 'check'))
		
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_billvendorrel');
		column[1] = new nlobjSearchColumn('custrecord_billbillno');
		column[2] = new nlobjSearchColumn('custrecord_billbillamount');
		column[3] = new nlobjSearchColumn('custrecord_billtdsamount');
		column[4] = new nlobjSearchColumn('custrecord_billdate');
		column[5] = new nlobjSearchColumn('custrecord_billtdstype');
		column[6] = new nlobjSearchColumn('custrecord_billtdssection');
		column[7] = new nlobjSearchColumn('custrecord_billtdsaccount');
		column[8] = new nlobjSearchColumn('internalid');
		column[9] = new nlobjSearchColumn('custrecord_billtdspayable');
		column[10] = new nlobjSearchColumn('custrecord_bill_tds_section');
		column[11] = new nlobjSearchColumn('custrecord_billassessee_code');
		
		column[12] = new nlobjSearchColumn('custrecord_tdsperrate');
		column[13] = new nlobjSearchColumn('custentity_vendor_panno', 'custrecord_billvendorrel', null);
		column[14] = new nlobjSearchColumn('custentity_ao_certificate_number', 'custrecord_billvendorrel', null);
		column[15] = new nlobjSearchColumn('custentity_ao_cert_from_date', 'custrecord_billvendorrel', null);
		column[16] = new nlobjSearchColumn('custentity_ao_cert_to_date', 'custrecord_billvendorrel', null);
		column[17] = new nlobjSearchColumn('transactionnumber', 'CUSTRECORD_BILLBILLNO', null);
		column[18] = new nlobjSearchColumn('custrecord_tdsperrate');
		column[19] = new nlobjSearchColumn('custrecord_tds_transaction_type');
		
		var results = nlapiSearchRecord('customrecord_tdsbillrelation', null, filters, column);
		if (results != null) 
		{
		//return results;
		}
		
		var filter1 = new Array();
		filter1.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
		if(_logValidation(postingperiod))
	    {
	    	filter1.push( new nlobjSearchFilter('custrecord_iit_tds_posting_period', null, 'is', postingperiod));
	    }
	    else
	    {
	    	filter1.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	    	filter1.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
	    }
		/*filter1.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
		filter1.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));*/
		filter1.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
		filter1.push(new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
		filter1.push(new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
		filter1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter1.push(new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
		filter1.push(new nlobjSearchFilter('approvalstatus', 'custrecord_billbillno', 'is', 2))
		filter1.push(new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is', 'T'))
		//filter1.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','bill'));
		filter1.push(new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is', 'F'))
		filter1.push(new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is', 'vendorbill'))
		
		
		var res = nlapiSearchRecord('customrecord_tdsbillrelation', null, filter1, column);
		if (res != null) 
		{
			//return results;
			if (results != null) 
			{
				var results = res.concat(results);
			//return results;
			}
			else 
			{
				var results = res;
			}
		}
		var filter2 = new Array();
		filter2.push(new nlobjSearchFilter('custrecord_billstatus', null, 'is', 'Open'));
		if(_logValidation(postingperiod))
	    {
			filter2.push( new nlobjSearchFilter('custrecord_iit_tds_posting_period', null, 'is', postingperiod));
	    }
	    else
	    {
	    	filter2.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
	    	filter2.push( new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));
	    }
		/*filter2.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorafter', fromdate));
		filter2.push(new nlobjSearchFilter('custrecord_billdate', null, 'onorbefore', todate));*/
		filter2.push(new nlobjSearchFilter('custrecord_billsubsidiary', null, 'is', subsidiary));
		filter2.push(new nlobjSearchFilter('custrecord_bill_tds_section', null, 'is', tds_section));
		filter2.push(new nlobjSearchFilter('custrecord_billassessee_code', null, 'is', tds_assesssee));
		filter2.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter2.push(new nlobjSearchFilter('custrecord_billbillno', null, 'noneof', '@NONE@'))
		// filter2.push( new nlobjSearchFilter('custrecord_tds_transaction_type', null, 'is','journal'));
		filter2.push(new nlobjSearchFilter('voided', 'custrecord_billbillno', 'is', 'F'))
		filter2.push(new nlobjSearchFilter('recordtype', 'custrecord_billbillno', 'is', 'journalentry'))
		//filter2.push( new nlobjSearchFilter('mainline', 'custrecord_billbillno', 'is','T'))
		var res2 = nlapiSearchRecord('customrecord_tdsbillrelation', null, filter2, column);
		if (res2 != null) 
		{
			var res3 = new Array()
			var i_new_bill = 0;
			var i_new_internalid = 0;
			for (var k = 0; k < res2.length; k++) {
				//var bill = res2[k].getValue('custrecord_billbillno')
				//nlapiLogExecution('DEBUG','Bill ', "bill 3"+bill);
				
				var internal_id = res2[k].getValue('internalid')
				//nlapiLogExecution('DEBUG','Bill ', "internal_id 3"+internal_id);
				
				if (internal_id != i_new_internalid) 
				{
					var i_array_push = res2[k].getValue('internalid')
					
					res3.push(i_array_push)
					//nlapiLogExecution('DEBUG', 'Bill ', "res 3" + res3);
					
					//var i_new_bill = res2[k].getValue('custrecord_billbillno')
					//nlapiLogExecution('DEBUG','Bill ', "bill 3"+i_new_bill);
					
					var i_new_internalid = res2[k].getValue('internalid')
				//nlapiLogExecution('DEBUG','Bill ', "internal_id 3"+i_new_internalid);
				}
				
			}
			var filter3 = new Array();
			filter3.push(new nlobjSearchFilter('internalid', null, 'anyOf', res3));
			var res2 = nlapiSearchRecord('customrecord_tdsbillrelation', null, filter3, column);
			if (results != null) 
			{
				var results = res2.concat(results);
			}
			else 
			{
				var results = res2
			}
			
		}
		
		// return results;
		
		sublist.addField('apply', 'checkbox', 'Apply');
		if (isForm == 1) 
		{
			//sublist.addField('custrecord2','text','Type')
			var id = sublist.addField('internalid', 'text', 'Internal Id')
			id.setDisplayType('hidden')
			sublist.addField('custrecord_billdate', 'text', 'Date');
			sublist.addField('custrecord_billbillno_display', 'text', 'Bill No');
			sublist.addField('custrecord_billvendorrel_display', 'text', 'Vendor Name')
			
			sublist.addField('custrecord_billtdstype_display', 'text', 'TDS Type')
			sublist.addField('custrecord_tdsperrate', 'text', 'TDS Rate')
			sublist.addField('custrecord_bill_tds_section_display', 'text', 'TDS Section')
			sublist.addField('custrecord_billassessee_code_display', 'text', 'TDS Asseessee')
			var TdsAccount = sublist.addField('custrecord_billtdsaccount', 'select', 'TDS Account', 'account')
			TdsAccount.setDisplayType('inline')
			//TdsAccount.setDisplayType('hidden')
			//sublist.addField('custrecord_billtdsaccount_display', 'text', 'TDS Account Name')
			sublist.addField('custrecord_billbillamount', 'text', 'Bill Amount');
			sublist.addField('custrecord_billtdsamount', 'text', 'Total TDS Amount');
			sublist.addField('custrecord_billtdspayable', 'text', 'TDS Payable Amount')
			sublist.addField('custentity_vendor_panno', 'text', 'PAN No.')
			sublist.addField('custentity_ao_certificate_number', 'text', 'AO Certificate No,')
			sublist.addField('custentity_ao_cert_from_date', 'text', 'AO Certificate From Date')
			sublist.addField('custentity_ao_cert_to_date', 'text', 'AO Certificate To Date')
			sublist.addField('custentity_transaction_number', 'text', 'Transaction Number')
			sublist.addField('custentity_tds_rate', 'text', 'TDS Rate')
			sublist.addField('custentity_tds_transaction_type', 'text', 'TDS Transaction Type')
		}
		if (results != '' && results != null && results != undefined) 
		{
			var j = 0;
			for (var z = 0; z < results.length; z++) 
			{
				j = j + 1;
				
				var custrecord_billvendorrel = results[z].getText('custrecord_billvendorrel')
				sublist.setLineItemValue('custrecord_billvendorrel_display', j, custrecord_billvendorrel)
				
				var custrecord_billbillno = results[z].getText('custrecord_billbillno')
				sublist.setLineItemValue('custrecord_billbillno_display', j, custrecord_billbillno)
				
				var custrecord_billbillamount = results[z].getValue('custrecord_billbillamount')
				sublist.setLineItemValue('custrecord_billbillamount', j, custrecord_billbillamount)
				
				var custrecord_billtdsamount = results[z].getValue('custrecord_billtdsamount')
				sublist.setLineItemValue('custrecord_billtdsamount', j, custrecord_billtdsamount)
				
				var custrecord_billdate = results[z].getValue('custrecord_billdate')
				sublist.setLineItemValue('custrecord_billdate', j, custrecord_billdate)
				
				var custrecord_billtdstype = results[z].getText('custrecord_billtdstype')
				sublist.setLineItemValue('custrecord_billtdstype_display', j, custrecord_billtdstype)
				
				var custrecord_bill_tds_section = results[z].getText('custrecord_bill_tds_section')
				sublist.setLineItemValue('custrecord_bill_tds_section_display', j, custrecord_bill_tds_section)
				
				var custrecord_billassessee_code_display = results[z].getText('custrecord_billassessee_code')
				sublist.setLineItemValue('custrecord_billassessee_code_display', j, custrecord_billassessee_code_display)
				
				var custrecord_billtdsaccount = results[z].getValue('custrecord_billtdsaccount')
				sublist.setLineItemValue('custrecord_billtdsaccount', j, custrecord_billtdsaccount)
				
				var internalid = results[z].getValue('internalid')
				sublist.setLineItemValue('internalid', j, internalid)
				
				var custrecord_billtdspayable = results[z].getValue('custrecord_billtdspayable')
				sublist.setLineItemValue('custrecord_billtdspayable', j, custrecord_billtdspayable)
				
				var custrecord_tdsperrate = results[z].getValue('custrecord_tdsperrate')
				sublist.setLineItemValue('custrecord_tdsperrate', j, custrecord_tdsperrate)
				
				var pan_no = results[z].getValue(column[13]);
				sublist.setLineItemValue('custentity_vendor_panno', j, pan_no)
				
				var ao_certificate_no = results[z].getValue(column[14]);
				sublist.setLineItemValue('custentity_ao_certificate_number', j, ao_certificate_no)
				
				var ao_certificate_from_date = results[z].getValue(column[15]);
				sublist.setLineItemValue('custentity_ao_cert_from_date', j, ao_certificate_from_date)
				
				var ao_certificate_to_date = results[z].getValue(column[16]);
				sublist.setLineItemValue('custentity_ao_cert_to_date', j, ao_certificate_to_date)
				
				var transaction_number = results[z].getValue(column[17]);
				sublist.setLineItemValue('custentity_transaction_number', j, transaction_number)
				
				var tds_rate = results[z].getValue(column[18]);
				sublist.setLineItemValue('custentity_tds_rate', j, tds_rate)
				
				var tds_transaction_type = results[z].getValue(column[19]);
				sublist.setLineItemValue('custentity_tds_transaction_type', j, tds_transaction_type)
				
			}
		}
	}
    // === END : CODE FOR SEARCHING BILL  ===
	//=== Begin:populate the tax agency ===
	function popluattaxagency(request, response, taxagency)
	{
    var subsidiary = request.getParameter('subsidiary')
    var ved_filchk1 = new Array();
    var ved_columnsCheck1 = new Array();

    ved_filchk1.push( new nlobjSearchFilter('category', null, 'is', 3));
    ved_filchk1.push( new nlobjSearchFilter('subsidiary', null, 'is', subsidiary));
    ved_filchk1.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));

    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
    ved_columnsCheck1.push( new nlobjSearchColumn('entityid'));
    ved_columnsCheck1.push( new nlobjSearchColumn('category'));

    var ved_searchResultsCheck1 = nlapiSearchRecord('vendor', null, ved_filchk1, ved_columnsCheck1);
    if (ved_searchResultsCheck1 != null)
	{
        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
		{
            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'), ved_searchResultsCheck1[i].getValue('entityid'), false);

        }
     }

   }
    // === End: populate the tax agency ====
	// === BEGIN :CPOPULATE LOCATION ====
	function popluatlocation(request, response, taxagency)
	{
	    var subsidiary = request.getParameter('subsidiary')
	    var ved_filchk1 = new Array();
	    var ved_columnsCheck1 = new Array();

	    ved_filchk1.push( new nlobjSearchFilter('subsidiary', null, 'anyof', subsidiary));

	    ved_columnsCheck1.push( new nlobjSearchColumn('internalid'));
	    ved_columnsCheck1.push( new nlobjSearchColumn('name'));

	    var ved_searchResultsCheck1 = nlapiSearchRecord('location', null, ved_filchk1, ved_columnsCheck1);
	    if (ved_searchResultsCheck1 != null)
		{
	        for (i = 0; ved_searchResultsCheck1 != null && i < ved_searchResultsCheck1.length; i++)
			{
	            taxagency.addSelectOption(ved_searchResultsCheck1[i].getValue('internalid'), ved_searchResultsCheck1[i].getValue('name'), false);

	        }
	    }

	}
	// === END  :CPOPULATE LOCATION ====
	// === Begin: Populate the account balance ===
	function accountbal()
	{
	    var tds_filchk1 = new Array();
	    var tds_columnchk = new Array()
	    tds_columnchk[0] = new nlobjSearchColumn('custrecord_tdsaccount')
	    var tds_result = new nlapiSearchRecord('customrecord_tdsmaster', null, null, tds_columnchk)
	    var tds_acclist = new Array();
	    var q = 0
	    for (i = 0; i < tds_result.length; i++)
		{
	        var tdsacc = tds_result[i].getValue('custrecord_tdsaccount');
	        var acc_filchk1 = new Array();
	        var acc_columnsCheck1 = new Array();

	        acc_filchk1.push( new nlobjSearchFilter('internalid', null, 'is', tdsacc));

	        acc_columnsCheck1.push( new nlobjSearchColumn('balance'));
	        acc_columnsCheck1.push( new nlobjSearchColumn('name'));

	        var acc_searchResultsCheck1 = nlapiSearchRecord('account', null, acc_filchk1, acc_columnsCheck1);

	        for (var j = 0; acc_searchResultsCheck1 != null && j < acc_searchResultsCheck1.length; j++)
			{
	            var acc = acc_searchResultsCheck1[j].getValue('name')
	            var accbal = acc_searchResultsCheck1[j].getValue('balance')
	            tds_acclist[q] = acc
	            tds_acclist[q + 1] = accbal
	            q = q + 2
	        }
	    }
	    return (tds_acclist)
	}

	// === End: Populate the account balance ===
	// === Begin: Process the Line Item ===
	function processLineItem(name, request, isDevice){
	
		try 
		{
			
			var a_tdsrecord = new Array();
			
			
			nlapiLogExecution('DEBUG', 'processLineItem', 'name->' + name)
			var bankname = request.getParameter('account')
			nlapiLogExecution('DEBUG', 'processLineItem', 'bankname->' + bankname)
			
			var posdate = request.getParameter('pdate')
			nlapiLogExecution('DEBUG', 'processLineItem', 'posdate->' + posdate)
			
			var memo = request.getParameter('memo')
			nlapiLogExecution('DEBUG', 'processLineItem', 'memo->' + memo)
			
			var taxagency = request.getParameter('taxage')
			nlapiLogExecution('DEBUG', 'processLineItem', 'taxagency->' + taxagency)
			
			var class1 = request.getParameter('class')
			nlapiLogExecution('DEBUG', 'processLineItem', 'class1->' + class1)
			
			var location1 = request.getParameter('location')
			nlapiLogExecution('DEBUG', 'processLineItem', 'location1->' + location1)
			
			var department = request.getParameter('department')
			nlapiLogExecution('DEBUG', 'processLineItem', 'department->' + department)
			
			var chequeno = request.getParameter('cheque')
			nlapiLogExecution('DEBUG', 'processLineItem', 'chequeno->' + chequeno)
			
			var subsidiary = request.getParameter('subsidiary')
			nlapiLogExecution('DEBUG', 'processLineItem', 'subsidiary->' + subsidiary)
			
			var bsrcode = request.getParameter('bsrcode')
			nlapiLogExecution('DEBUG', 'processLineItem', 'bsrcode->' + bsrcode)
			
			var paymentdate = request.getParameter('paymentdate')
			nlapiLogExecution('DEBUG', 'processLineItem', 'paymentdate->' + paymentdate)
			
			var challanno = request.getParameter('challanno')
			nlapiLogExecution('DEBUG', 'processLineItem', 'challanno->' + challanno)
			
			var chkrecord = nlapiCreateRecord('check')
			chkrecord.setFieldValue('account', bankname);
			chkrecord.setFieldValue('entity', taxagency);
			chkrecord.setFieldValue('trandate', posdate);
			chkrecord.setFieldValue('memo', memo);
			chkrecord.setFieldValue('class', class1);
			chkrecord.setFieldValue('location', location1);
			chkrecord.setFieldValue('department', department);
			
			
			var count1 = request.getLineItemCount(name);
			var p = 1
			for (var i = 1; i <= count1; i++) 
			{
				var isselect = request.getLineItemValue(name, 'apply', i)
				if (isselect == 'T') 
				{
					var tds_acc = request.getLineItemValue(name, 'custrecord_billtdsaccount', i)
					nlapiLogExecution('DEBUG', 'processLineItem', 'TDS account is' + tds_acc);
					var amount = request.getLineItemValue(name, 'custrecord_billtdsamount', i) // custrecord_billtdspayable
					nlapiLogExecution('DEBUG', 'processLineItem', 'TDS amount is' + amount)
					chkrecord.setLineItemValue('expense', 'account', p, tds_acc)
					nlapiLogExecution('DEBUG', 'processLineItem', 'tds_acc->' + tds_acc)
					chkrecord.setLineItemValue('expense', 'amount', p, amount)
					nlapiLogExecution('DEBUG', 'processLineItem', 'set amount->' + amount)
					chkrecord.setLineItemValue('expense', 'location', p, location1)
					chkrecord.setLineItemValue('expense', 'class', p, class1)
					chkrecord.setLineItemValue('expense', 'department', p, department)
					
					
					
					p = p + 1
					
					//Begin:Updateing Bill relation record
					
					var internalid = request.getLineItemValue(name, 'internalid', i)
					nlapiLogExecution('DEBUG', 'processLineItem', 'TDS internal id is' + internalid);
					var tdsvalue = request.getLineItemValue(name, 'custrecord_billtdspayable', i)
					nlapiLogExecution('DEBUG', 'processLineItem', 'tdsvalue' + tdsvalue);
					nlapiLogExecution('DEBUG', 'processLineItem', 'amount' + amount);
					var newbalance = parseFloat(tdsvalue) - parseFloat(amount)
					
					var record = nlapiLoadRecord('customrecord_tdsbillrelation', internalid);
					record.setFieldValue('custrecord_billtdspayable', newbalance);
					nlapiLogExecution('DEBUG', 'processLineItem', 'newbalance' + newbalance);
					if (newbalance == 0) 
					{
						record.setFieldValue('custrecord_billstatus', 'Close');
					}
					record.setFieldValue('custrecord_billbankname', bankname)
					record.setFieldValue('custrecord_billtdspayabledate', posdate)
					record.setFieldValue('custrecord_billtaxagency', taxagency)
					record.setFieldValue('custrecord_bill_bsrcode', bsrcode)
					record.setFieldValue('custrecord_bill_paymentdate', paymentdate)
					record.setFieldValue('custrecord_billchallanno', challanno)
					record.setFieldValue('custrecord_tds_cheque_no', chequeno)
					
					var updatedId = nlapiSubmitRecord(record, true);
					nlapiLogExecution('DEBUG', 'processLineItem', 'updatedId Custom record' + updatedId)
					//End:Updateing Bill relation record
					a_tdsrecord.push({ID: updatedId,value: tdsvalue});
				}
			}
			nlapiLogExecution('DEBUG', 'processLineItem', 'before Submit')
			var id = nlapiSubmitRecord(chkrecord, true, true);
			nlapiLogExecution('DEBUG', 'processLineItem', 'after Submit->' + id)
		} 
		catch (exception) {
			for (i = 0; i < a_tdsrecord.length; i++) {
				var internal_id = a_tdsrecord[i].ID;
				nlapiLogExecution('DEBUG', 'processLineItem', 'internal_id' + internal_id)
				var record = nlapiLoadRecord('customrecord_tdsbillrelation', internal_id);
				record.setFieldValue('custrecord_billstatus', 'open');
				record.setFieldValue('custrecord_billtdspayable', a_tdsrecord[i].value);
				nlapiLogExecution('DEBUG', 'processLineItem', 'internal_id' + a_tdsrecord[i].value)
				record.setFieldValue('custrecord_billbankname', '')
				record.setFieldValue('custrecord_billtaxagency', '')
				record.setFieldValue('custrecord_bill_bsrcode', '')
				record.setFieldValue('custrecord_bill_paymentdate', '')
				record.setFieldValue('custrecord_billchallanno', '')
				record.setFieldValue('custrecord_tds_cheque_no', '')
				record.setFieldValue('custrecord_billtdspayabledate', '')
				var updatedId = nlapiSubmitRecord(record, true);
				nlapiLogExecution('DEBUG', 'exception processLineItem', 'updatedId Custom record' + updatedId)
				
			}
			//nlapiLogExecution('DEBUG', 'exception processLineItem', 'error' + e)
			throw exception;
		}
		
	}
  //End: Process the Line Item
  
  function search_tds_type(tds_section,assessee_type)
  {
  		var tds_type = new Array();
  	 	var filters = new Array();
	    filters.push( new nlobjSearchFilter('custrecord_tds_section', null, 'is',tds_section));
		filters.push( new nlobjSearchFilter('custrecord_assessecode', null, 'is',assessee_type));
		
		var column = new Array();
	    column.push( new nlobjSearchColumn('internalid'));
	    
	    var tds_type_results = nlapiSearchRecord('customrecord_tdsmaster',null, filters, column);
	    if(tds_type_results != null)
		{
			for(var i=0;i<tds_type_results.length;i++)
			{
				
				 tds_type.push(tds_type_results[i].getValue('internalid'))
			

			}
			return tds_type;
		}
	    
		
  	
  }
    //Begin:Function to get the global parameter
function SearchGlobalParameter()
{
	
	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	
	
	return i_globalRecId;
}

function _logValidation(value) 
{
	 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined'&& value != 'NaN' && value != NaN) 
	 {
	  return true;
	 }
	 else 
	 { 
	  return false;
	 }
}
   //----------------------------------------------------------------------------------------------------------------------------
   // END SUITELET ====================================================