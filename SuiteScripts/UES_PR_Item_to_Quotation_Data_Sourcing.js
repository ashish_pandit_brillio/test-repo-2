/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name : UES_PR_Item_to_Quotation_Data_Sourcing.js Author : Shweta
	 * Chopde Company : Aashna Cloudtech Date : 11 April 2014 Description : To
	 * source the Item data to Quotation Analysis
	 * 
	 * 
	 * Script Modification Log: -- Date -- -- Modified By -- --Requested By-- --
	 * Description --
	 * 
	 * 3 July 2014 Shweta Shekar Sourced Practice Head with Executing Paernt
	 * Head
	 * 
	 * 
	 * 7 July 2014 Shweta Shekar Sourcing Finance Head
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * BEFORE LOAD - beforeLoadRecord(type)
	 * 
	 * BEFORE SUBMIT - beforeSubmitRecord(type)
	 * 
	 * AFTER SUBMIT - afterSubmitRecord(type)
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization: - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...
}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type) {

	/*
	 * On before load: - EXPLAIN THE PURPOSE OF THIS FUNCTION -
	 * 
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES
	// BEFORE LOAD CODE BODY
	return true;

}

// END BEFORE LOAD ====================================================

// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type) {
	/*
	 * On before submit: - PURPOSE -
	 * 
	 * FIELDS USED:
	 * 
	 * --Field Name-- --ID--
	 * 
	 * 
	 */

	// LOCAL VARIABLES
	// BEFORE SUBMIT CODE BODY
	return true;

}

// END BEFORE SUBMIT ==================================================

// BEGIN AFTER SUBMIT =============================================
/**
 * 
 * @param {Object}
 *            type
 * 
 * Description --> For the type create iterate through PR
 * ÃƒÆ’Ã†â€™Ãƒâ€
 * Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ
 * Items one by one , get all the details & add the new line for Quotation
 * Analysis & add the respective data for it
 */
function afterSubmit_Item_Data_Sourcing(type) {
	nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing', ' Type. -->'
	        + type);

	if (type == 'create') {
		try {
			var i_recordID = nlapiGetRecordId()
			var s_record_type = nlapiGetRecordType()

			var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID)

			if (_logValidation(o_recordOBJ)) {
				var i_line_item_count = o_recordOBJ
				        .getLineItemCount('recmachcustrecord_purchaserequest');

				var i_reference_no = o_recordOBJ
				        .getFieldValue('custrecord_prno')
				nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
				        ' Reference No . -->' + i_reference_no);

				if (_logValidation(i_line_item_count)) {
					for (var i = 1; i <= i_line_item_count; i++) {
						var i_SR_No = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_prlineitemno', i)

						var i_item = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_prmatgrpcategory', i)

						var i_description = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_prmatldescription', i)

						var i_currency = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_currencyforpr', i)

						var i_quantity = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_quantity', i)

						var i_unit = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_unit', i)

						var i_vertical = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_verticals', i)

						var i_project = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_project', i)

						var i_subsidiary = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest',
						        'custrecord_subsidiaryforpr', i)

						var i_pr_item = o_recordOBJ.getLineItemValue(
						        'recmachcustrecord_purchaserequest', 'id', i)

						var i_executing_practice_head = get_project_executing_practice_head(i_project)
						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' Executing Practice Head -->'
						                + i_executing_practice_head);

						var i_vertical_head = get_vertical_head(i_executing_practice_head)
						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' Vertical Head -->' + i_vertical_head);

						// var i_vertical_head = get_vertical_head(i_vertical)

						if (i_vertical_head == null || i_vertical_head == ''
						        || i_vertical_head == undefined) {
							i_vertical_head = ''
						}
						var i_finance_head;
						if (i_subsidiary == 2) {
							i_finance_head = 1538
						}// Brillio - LLC
						if (i_subsidiary == 3) {
							i_finance_head = 1615;
						}// Brillio Technologies
						var i_PR_ID = search_PR_Item_recordID_1(i_recordID,
						        i_SR_No, i_item)

						if (_logValidation(i_PR_ID)) {
							var o_PR_OBJ = nlapiLoadRecord(
							        'customrecord_pritem', i_PR_ID)

							if (_logValidation(o_PR_OBJ)) {
								var i_value_per_unit = o_PR_OBJ
								        .getFieldValue('custrecord_valueperunit');

								var i_total_value = o_PR_OBJ
								        .getFieldValue('custrecord_totalvalue');

								if (!_logValidation(i_total_value)) {
									i_total_value = 0
								}

								if (!_logValidation(i_value_per_unit)) {
									i_value_per_unit = 0
								}
								o_PR_OBJ.setFieldValue('custrecord_qtyremain',
								        i_quantity);
								o_PR_OBJ.setFieldValue('custrecord_totalvalue',
								        i_total_value);
								o_PR_OBJ.setFieldValue(
								        'custrecord_valueperunit',
								        i_value_per_unit);
								o_PR_OBJ.setFieldValue('custrecord_qa_status',
								        1);
								o_PR_OBJ.setFieldValue('custrecord_prno1',
								        i_reference_no);

							}
							var i_PR_submitID = nlapiSubmitRecord(o_PR_OBJ,
							        false, false)
							nlapiLogExecution('DEBUG',
							        'afterSubmit_Item_Data_Sourcing',
							        ' ************* PR Submit ID ********** -->'
							                + i_PR_submitID);

						}// PR ID

						// ======================= Quotation Analysis
						// ===========================

						// added by Nitish - 1/6/2016
						var quotationRecord = nlapiCreateRecord('customrecord_quotationanalysis');
						quotationRecord.setFieldValue('custrecord_srno',
						        i_SR_No);
						quotationRecord.setFieldValue('custrecord_qa_pr_item',
						        i_pr_item);
						quotationRecord.setFieldValue('custrecord_prquote',
						        nlapiGetRecordId());
						quotationRecord
						        .setFieldValue('custrecord_item', i_item);
						quotationRecord.setFieldValue('custrecord_description',
						        i_description);
						quotationRecord.setFieldValue('custrecord_qty',
						        i_quantity);
						quotationRecord.setFieldValue('custrecord_uom', i_unit);
						quotationRecord.setFieldValue('custrecord_currency',
						        i_currency);
						quotationRecord.setFieldValue(
						        'custrecord_verticalheadforquotation',
						        i_vertical_head);
						quotationRecord.setFieldValue('custrecord_currency',
						        i_currency);
						quotationRecord.setFieldValue('custrecord_financehead',
						        i_finance_head);
						// quotationRecord.setFieldValue('custrecord_approvalstatusforquotation',
						// 5);
						var qaId = nlapiSubmitRecord(quotationRecord, true,
						        true);
						nlapiLogExecution('debug', 'quotation created');

					}// Loop

				}// Line Item Count

				var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true)
				nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
				        ' ******************* Submit ID *******************  -->'
				                + i_submitID);

			}// Record Object
		}// TRY
		catch (exception) {
			nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->'
			        + exception);
		}// CATCH

	}// Create
	if (type == 'edit') {
		var a_non_remove_array = new Array();
		var a_remove_array = new Array();

		try {
			var i_recordID = nlapiGetRecordId()
			var s_record_type = nlapiGetRecordType()

			var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID)

			if (_logValidation(o_recordOBJ)) {
				var i_line_item_count = o_recordOBJ
				        .getLineItemCount('recmachcustrecord_purchaserequest');

				var i_line_quotation_analysis_count = o_recordOBJ
				        .getLineItemCount('recmachcustrecord_prquote');

				if (_logValidation(i_line_item_count)) {
					if (_logValidation(i_line_quotation_analysis_count)) {
						if (i_line_item_count == i_line_quotation_analysis_count) {
							for (var a = 1; a <= i_line_item_count; a++) {
								var i_SR_No = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_purchaserequest',
								        'custrecord_prlineitemno', a)

								var i_item = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_purchaserequest',
								        'custrecord_prmatgrpcategory', a)

								var i_description = o_recordOBJ
								        .getLineItemValue(
								                'recmachcustrecord_purchaserequest',
								                'custrecord_prmatldescription',
								                a)

								var i_currency = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_purchaserequest',
								        'custrecord_currencyforpr', a)

								var i_quantity = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_purchaserequest',
								        'custrecord_quantity', a)

								var i_unit = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_purchaserequest',
								        'custrecord_unit', a)

								var i_approval_status = o_recordOBJ
								        .getLineItemValue(
								                'recmachcustrecord_purchaserequest',
								                'custrecord_prastatus', a)

								for (var b = 1; b <= i_line_quotation_analysis_count; b++) {
									var i_SR_No_QA = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_prquote',
									                'custrecord_srno', b)

									var i_item_QA = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_prquote',
									                'custrecord_item', a)

									if ((i_SR_No == i_SR_No_QA)
									        && (i_approval_status == 7)) {
										var i_item_QA = o_recordOBJ
										        .getLineItemValue(
										                'recmachcustrecord_prquote',
										                'custrecord_item', a)

										var i_description_QA = o_recordOBJ
										        .getLineItemValue(
										                'recmachcustrecord_prquote',
										                'custrecord_description',
										                a)

										var i_currency_QA = o_recordOBJ
										        .getLineItemValue(
										                'recmachcustrecord_prquote',
										                'custrecord_currency',
										                a)

										var i_quantity_QA = o_recordOBJ
										        .getLineItemValue(
										                'recmachcustrecord_prquote',
										                'custrecord_qty', a)

										var i_unit_QA = o_recordOBJ
										        .getLineItemValue(
										                'recmachcustrecord_prquote',
										                'custrecord_uom', a)

										if (i_description != i_description_QA) {
											o_recordOBJ
											        .setLineItemValue(
											                'recmachcustrecord_prquote',
											                'custrecord_description',
											                b, i_description);
										}
										if (i_currency != i_currency_QA) {
											o_recordOBJ
											        .setLineItemValue(
											                'recmachcustrecord_prquote',
											                'custrecord_currency',
											                b, i_currency);
										}
										if (i_quantity != i_quantity_QA) {
											o_recordOBJ
											        .setLineItemValue(
											                'recmachcustrecord_prquote',
											                'custrecord_qty',
											                b, i_quantity);
										}
										if (i_unit != i_unit_QA) {
											o_recordOBJ
											        .setLineItemValue(
											                'recmachcustrecord_prquote',
											                'custrecord_uom',
											                b, i_unit);
										}
										if ((i_description != i_description_QA)
										        || (i_currency != i_currency_QA)
										        || (i_quantity != i_quantity_QA)
										        || (i_unit != i_unit_QA)) {
											var i_QA_recordID = search_quotation_analysis_recordID(
											        i_recordID, i_SR_No_QA,
											        i_item_QA)

											if (_logValidation(i_QA_recordID)) {
												var o_QA_OBJ = nlapiLoadRecord(
												        'customrecord_quotationanalysis',
												        i_QA_recordID)

												if (_logValidation(o_QA_OBJ)) {
													if (i_description != i_description_QA) {
														o_QA_OBJ
														        .setFieldValue(
														                'custrecord_description',
														                i_description);
													}
													if (i_currency != i_currency_QA) {
														o_QA_OBJ
														        .setFieldValue(
														                'custrecord_currency',
														                i_currency);
													}
													if (i_quantity != i_quantity_QA) {
														o_QA_OBJ
														        .setFieldValue(
														                'custrecord_qty',
														                i_quantity);
													}
													if (i_unit != i_unit_QA) {
														o_QA_OBJ
														        .setFieldValue(
														                'custrecord_uom',
														                i_unit);
													}

													// get the PR Item record
													var pr_item = o_QA_OBJ
													        .getFieldValue('custrecord_qa_pr_item');

													var prItemRecord = nlapiLoadRecord(
													        'customrecord_pritem',
													        pr_item,
													        {
														        'recordmode' : 'dynamic'
													        });
													prItemRecord
													        .setFieldValue(
													                'custrecord_prastatus',
													                '');
													nlapiSubmitRecord(
													        prItemRecord, true,
													        true);
													nlapiLogExecution('debug',
													        'PR Item status changed to QA done');

													var i_QA_submitID = nlapiSubmitRecord(
													        o_QA_OBJ, true,
													        true)
													nlapiLogExecution(
													        'DEBUG',
													        'afterSubmit_Item_Data_Sourcing',
													        ' ********************** QA Submit ID ********************** -->'
													                + i_QA_submitID);

													// added by Nitish
													// nlapiInitiateWorkflow(
													// 'customrecord_quotationanalysis',
													// i_QA_submitID,
													// 'customworkflow69');
													// nlapiLogExecution('debug',
													// '/QA pushed into
													// workflow');
												}
											}
										}

										break;
									}// Serial Number Check

									if ((i_SR_No == i_SR_No_QA)
									        && ((i_approval_status == 5)
									                || (i_approval_status == 6) || (i_approval_status == 8))) {
										nlapiLogExecution(
										        'DEBUG',
										        'afterSubmit_Item_Data_Sourcing',
										        'In Removed Block .......... ');

										var i_QA_recordID = search_quotation_analysis_recordID(
										        i_recordID, i_SR_No_QA,
										        i_item_QA)
										nlapiLogExecution(
										        'DEBUG',
										        'afterSubmit_Item_Data_Sourcing',
										        ' QA Record ID -->'
										                + i_QA_recordID);

										if (_logValidation(i_QA_recordID)) {
											nlapiDeleteRecord(
											        'customrecord_quotationanalysis',
											        i_QA_recordID)

										}// QA Record ID

									}

								}// Loop Quotation Analysis Count

							}// Loop Line Count

						}// Equal Line Count
						if ((i_line_item_count != i_line_quotation_analysis_count)
						        && (i_line_quotation_analysis_count > i_line_item_count)) {
							for (var a = 1; a <= i_line_item_count; a++) {
								var i_SR_No = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_purchaserequest',
								        'custrecord_prlineitemno', a)

								var i_approval_status = o_recordOBJ
								        .getLineItemValue(
								                'recmachcustrecord_purchaserequest',
								                'custrecord_prastatus', a)

								for (var b = 1; b <= i_line_quotation_analysis_count; b++) {
									var i_SR_No_QA = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_prquote',
									                'custrecord_srno', b)

									if ((i_approval_status == 5)
									        || (i_approval_status == 6)
									        || (i_approval_status == 8)) {
										a_non_remove_array.push(i_SR_No_QA)

									}// Equal Serial No.

								}// Loop Quotation Analysis Count

							}// Loop Line Count

							nlapiLogExecution('DEBUG',
							        'afterSubmit_Item_Data_Sourcing',
							        ' Non Remove Array -->'
							                + a_non_remove_array);

							for (var c = 1; c <= i_line_quotation_analysis_count; c++) {
								var i_SR_No_QA = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_prquote',
								        'custrecord_srno', c)
								var i_item_QA = o_recordOBJ.getLineItemValue(
								        'recmachcustrecord_prquote',
								        'custrecord_item', c)

								if (_logValidation(i_SR_No_QA)) {
									if (a_non_remove_array.indexOf(i_SR_No_QA) == -1) {
										var i_QA_recordID = search_quotation_analysis_recordID(
										        i_recordID, i_SR_No_QA,
										        i_item_QA)
										nlapiLogExecution(
										        'DEBUG',
										        'afterSubmit_Item_Data_Sourcing',
										        ' QA Record ID -->'
										                + i_QA_recordID);

										if (_logValidation(i_QA_recordID)) {
											nlapiDeleteRecord(
											        'customrecord_quotationanalysis',
											        i_QA_recordID)

										}// QA Record ID

									}

								}

							}

						}// Unequal Line Count

					}// Line Quotation Analysis Count

				}// Line Item Count

			}// Record Obj

		} catch (exception) {
			nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->'
			        + exception);
		}// CATCH

	}// EDIT

	return true;
}

// END AFTER SUBMIT ===============================================

// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object}
 *            value
 * 
 * Description --> If the value is blank /null/undefined returns false else
 * returns true
 */
function _logValidation(value) {
	if (value != null && value.toString() != null && value != ''
	        && value != undefined && value.toString() != undefined
	        && value != 'undefined' && value.toString() != 'undefined'
	        && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
/**
 * 
 * @param {Object}
 *            i_recordID
 * @param {Object}
 *            i_SR_No_QA
 * @param {Object}
 *            i_item_QA
 * 
 * Description --> For the criteria Item, Serial No & Purchase Request search
 * Quotation Analysis record & returns the Quotation Analysis internal ID
 */
function search_quotation_analysis_recordID(i_recordID, i_SR_No_QA, i_item_QA) {
	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_prquote', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_srno', null, 'is', i_SR_No_QA);
	filter[2] = new nlobjSearchFilter('custrecord_item', null, 'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_item');
	columns[2] = new nlobjSearchColumn('custrecord_srno');
	columns[3] = new nlobjSearchColumn('custrecord_prquote');

	var a_seq_searchresults = nlapiSearchRecord(
	        'customrecord_quotationanalysis', null, filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty].getValue('custrecord_prquote');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty].getValue('custrecord_srno');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty].getValue('custrecord_item');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_item-->' + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP
	}

	return i_internal_id;
}
/**
 * 
 * @param {Object}
 *            i_vertical
 * 
 * Description --> For the respective vertical get the vertical head
 */
/*
 * function get_vertical_head(i_vertical) { if(_logValidation(i_vertical)) { var
 * i_vertical_head;
 * 
 * var filter = new Array(); filter[0] = new nlobjSearchFilter('internalid',
 * null, 'is',i_vertical);
 * 
 * var columns = new Array(); columns[0] = new
 * nlobjSearchColumn('custrecord_verticalhead');
 * 
 * var a_seq_searchresults =
 * nlapiSearchRecord('classification',null,filter,columns);
 * 
 * if (a_seq_searchresults != null && a_seq_searchresults != '' &&
 * a_seq_searchresults != undefined) { i_vertical_head =
 * a_seq_searchresults[0].getValue('custrecord_verticalhead'); } return
 * i_vertical_head; }
 * 
 * }//Vertical Head
 */
/**
 * 
 * @param {Object}
 *            i_recordID
 * @param {Object}
 *            i_SR_No_QA
 * @param {Object}
 *            i_item_QA
 * 
 * Description --> For the criteria Item, Serial No & Purchase Request search
 * the PR ÃƒÆ’Ã†â€™Ãƒâ€
 * Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ
 * Item record & returns the PR ÃƒÆ’Ã†â€™Ãƒâ€
 * Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ
 * Item internal ID
 */
function search_PR_Item_recordID_1(i_recordID, i_SR_No_QA, i_item_QA) {
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID =='
	        + i_recordID);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA =='
	        + i_SR_No_QA);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA =='
	        + i_item_QA);

	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',
	        parseInt(i_SR_No_QA));
	filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null,
	        'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
	columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
	columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');

	var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem', null,
	        filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
		        'a_seq_searchresults ==' + a_seq_searchresults.length);

		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty]
			        .getValue('custrecord_purchaserequest');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty]
			        .getValue('custrecord_prlineitemno');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty]
			        .getValue('custrecord_prmatgrpcategory');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->'
			        + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP

	}

	return i_internal_id;
}

function get_project_executing_practice_head(i_project) {
	if (_logValidation(i_project)) {
		var i_executing_practice_head;

		var filter = new Array();
		filter[0] = new nlobjSearchFilter('internalid', null, 'is', i_project);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custentity_practice');

		var a_seq_searchresults = nlapiSearchRecord('job', null, filter,
		        columns);

		if (a_seq_searchresults != null && a_seq_searchresults != ''
		        && a_seq_searchresults != undefined) {
			i_executing_practice_head = a_seq_searchresults[0]
			        .getValue('custentity_practice');
		}
		return i_executing_practice_head;

	}

}// Vertical Head

function get_vertical_head(i_executing_practice_head) {
	var i_practice_head;

	if (_logValidation(i_executing_practice_head)) {
		var o_practiceOBJ = nlapiLoadRecord('department',
		        i_executing_practice_head)

		if (_logValidation(o_practiceOBJ)) {
			var i_parent = o_practiceOBJ.getFieldValue('parent');

			if (_logValidation(i_parent)) {
				var o_practice_parentOBJ = nlapiLoadRecord('department',
				        i_parent)

				if (_logValidation(o_practice_parentOBJ)) {
					i_practice_head = o_practice_parentOBJ
					        .getFieldValue('custrecord_practicehead');

				}// Practice Parent OBJ

			}// Parent
			else {
				i_practice_head = o_practiceOBJ
				        .getFieldValue('custrecord_practicehead');
			}

		}// Practice OBJ

	}// Validation Practice
	return i_practice_head;
}// Vertical Head
// END FUNCTION =====================================================
