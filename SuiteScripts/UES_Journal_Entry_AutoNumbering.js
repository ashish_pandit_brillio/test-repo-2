/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Journal_Entry_AutoNumbering.js
	Author      : Shweta Chopde
	Date        : 5 May 2014
	Description : Generate a Auto Number on Journal Entry


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoad_JE_buttons(type)
{
  var i_recordID = nlapiGetRecordId();
  var i_user = nlapiGetUser();
  var i_finance_head = nlapiGetFieldValue('custbody_financehead')	
  var i_approval_status = nlapiGetFieldValue('custbodyapprovalstatusonjv')	
  if(type == 'view')
  {
  	if((i_user == i_finance_head) && i_approval_status == 1)
	{
		form.setScript('customscript_cli_journal_entry_auto_no_v')
	  	form.addButton('custpage_je_approve','Approve','approve_je('+i_recordID+')');
		form.addButton('custpage_je_reject','Reject','reject_je('+i_recordID+')');
		
	}//Finance & User 	
  	
  }//VIEW
  
  return true;
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_JE_AutoNumbering(type)
{
	var a_serial_number_array;
	var i_sequence_number;
	var i_serial_no_ID;
	var s_JE_prefix = 'JE'
	var s_IJE_prefix = 'IJE'
	var s_prefix=''
	
	
	var o_contextOBJ = nlapiGetContext();
    
    var s_context_type = o_contextOBJ.getExecutionContext();
    nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Context Type -->' + s_context_type);
		
	nlapiLogExecution('DEBUG', 'afterLoadRecord', 'Type -->' + type);
	
		
    if(type == 'edit' && s_context_type == 'userevent')
	{
	 try
	  {
	  	var i_recordID = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' Record ID -->' + i_recordID);
		
		var s_record_type = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' Record Type -->' + s_record_type);
		
		if(s_record_type == 'intercompanyjournalentry')
		{
			s_prefix = s_IJE_prefix
		}
		if(s_record_type == 'journalentry')
		{
			s_prefix = s_JE_prefix
		}
		if(_logValidation(i_recordID)&&_logValidation(s_record_type))
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);
			
			if(_logValidation(o_recordOBJ))
			{
				var i_approved = o_recordOBJ.getFieldValue('approved')
				nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' Approved -->' + i_approved);
		
				var i_status = o_recordOBJ.getFieldValue('custbodyapprovalstatusonjv')
				nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' Status -->' + i_status);
		
		        if(i_status == 2)
			    {
				  var a_split_array = new Array()
			   	  
				  if(s_record_type == 'intercompanyjournalentry')
				  {
					a_serial_number_array = search_serial_number_generation_IJE();
				  }
				  if(s_record_type == 'journalentry')
				  {
					a_serial_number_array = search_serial_number_generation_JE();
				  }  
				 			  
				  if(_logValidation(a_serial_number_array))
			      {
				  	a_split_array = a_serial_number_array[0].split('###')
					
					i_sequence_number = a_split_array[0]
					
					i_serial_no_ID = a_split_array[1]
				
				 }//Serial Number Array
				 
				 var i_next_sequence_no = parseInt(i_sequence_number)+parseInt(1)
			     nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' Sequence Number -->' + i_sequence_number);
			     nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' Next Sequence No -->' + i_next_sequence_no);
			  
			   if(i_next_sequence_no.toString().length == 1)
			   {
			   	 i_next_sequence_no = '0'+i_next_sequence_no
			   }
			 
			    var i_auto_numberID = s_prefix+''+i_next_sequence_no.toString()
			    nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' ****************** Auto Number ID ***************** -->' + i_auto_numberID);
			  
			      if(_logValidation(i_serial_no_ID))
				  {
				  	o_serial_numberOBJ =  nlapiLoadRecord('customrecord_serial_number_generation',i_serial_no_ID)
					
					 if(_logValidation(o_serial_numberOBJ))
					 {
					 	o_serial_numberOBJ.setFieldValue('custrecord_current_sequence_number',i_next_sequence_no.toString())
			            o_serial_numberOBJ.setFieldValue('custrecord_current_serial_number',i_auto_numberID)
						
					    o_recordOBJ.setFieldValue('custbody_refnumber',i_auto_numberID)
						
						var i_submitID = nlapiSubmitRecord(o_recordOBJ, true,true)
			            var i_serial_no_submitID = nlapiSubmitRecord(o_serial_numberOBJ, true,true)
			   
			            nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' ****************** JE Submit ID ***************** -->' + i_submitID);
			  
		                nlapiLogExecution('DEBUG', 'afterSubmit_JE_AutoNumbering',' ****************** Serial No Submit ID  ***************** -->' + i_serial_no_submitID);
			  
					 
					 }//Serial Number OBJ
					
				  }//Serial No ID			  
			  
			    }//Approved for Posting
				
			}//Record OBJ
			
		}//Record ID & Record Type		
		
	  }//TRY
	  catch(exception)
	  {
	  	 nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	  }//CATCH	
		
	}//CREATE	
  
	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function search_serial_number_generation_JE()
{ 
   var i_sequence_number;
   var a_return_array = new Array()
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_transaction_type', null, 'is',1);
   filter[1] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'is','Journal Entry');
 	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_current_sequence_number');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,filter,columns);
   
   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
   {					 
     	
		i_internal_id = a_seq_searchresults[0].getValue('internalid');
	    nlapiLogExecution('DEBUG', 'search_serial_number_generation_JE', ' Internal ID -->' + i_internal_id);
				
		i_sequence_number = a_seq_searchresults[0].getValue('custrecord_current_sequence_number');
	    nlapiLogExecution('DEBUG', 'search_serial_number_generation_JE', ' Sequence Number -->' + i_sequence_number);
			
						 				
   }//Search Results	
	a_return_array[0] = i_sequence_number +'###'+ i_internal_id
		
	return a_return_array;	
}

function search_serial_number_generation_IJE()
{ 
   var i_sequence_number;
   var a_return_array = new Array()
   var i_internal_id;
   
   var filter = new Array();
   filter[0] = new nlobjSearchFilter('custrecord_transaction_type', null, 'is',1);
   filter[1] = new nlobjSearchFilter('custrecord_purchase_request_type', null, 'is','Intercompany Journal Entry');
 	
   var columns = new Array();
   columns[0] = new nlobjSearchColumn('internalid');
   columns[1] = new nlobjSearchColumn('custrecord_current_sequence_number');
   
   var a_seq_searchresults = nlapiSearchRecord('customrecord_serial_number_generation',null,filter,columns);
   
   if(a_seq_searchresults!=null && a_seq_searchresults!='' && a_seq_searchresults!=undefined)
   {					 
     	
		i_internal_id = a_seq_searchresults[0].getValue('internalid');
	    nlapiLogExecution('DEBUG', 'search_serial_number_generation_IJE', ' Internal ID -->' + i_internal_id);
				
		i_sequence_number = a_seq_searchresults[0].getValue('custrecord_current_sequence_number');
	    nlapiLogExecution('DEBUG', 'search_serial_number_generation_IJE', ' Sequence Number -->' + i_sequence_number);
			
						 				
   }//Search Results	
	a_return_array[0] = i_sequence_number +'###'+ i_internal_id
		
	return a_return_array;	
}
// END FUNCTION =====================================================
