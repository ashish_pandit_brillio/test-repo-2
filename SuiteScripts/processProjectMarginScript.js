/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/runtime', 'N/search','N/file', 'N/encode','N/email','N/url','N/https','/SuiteScripts/LibararyFiles2/config'],

		function(runtime,search,file,encode,email,url,https) {

	function execute(scriptContext) {
		var scriptObj = runtime.getCurrentScript();
		var projectResult = null;
		var loggedInUser = scriptObj.getParameter({
			name: 'custscript_login_email'
		});
		if(loggedInUser == "aazamali@inspirria.com" || loggedInUser == "shamanth.k@brillio.com" ){
			loggedInUser = "Sridhar.K@brillio.com";
		}
		var loggedInUserId = scriptObj.getParameter({
			name: 'custscript1902_userid'
		});
		if(loggedInUserId == "144836" || loggedInUserId == "97260"){
			loggedInUserId = "1542"
		}
		log.debug("Script parameter of custscript1: " +
				scriptObj.getParameter({
					name: 'custscript_login_email'
				}));
		log.debug("Script parameter userId: " +
				loggedInUserId);
		// check for practice spoc
		var i_practice = "";
		var customrecord_practice_spocSearchObj = getPractice(loggedInUserId,search);
		var searchResult = customrecord_practice_spocSearchObj.run();
		if(searchResult){
			resultObject = searchResult.getRange(0, 1);
			if(isArrayNotEmpty(resultObject)){
				i_practice = resultObject[0].getValue({
					name: "custrecord_practice"
				});
			}
		}
		// check for Admin 
		if(config.admin[loggedInUser]){
			log.debug("utility",config.admin[loggedInUser]);
			log.debug("user","Admin");
			projectResult = getProjectForAdmin(search)
		}else{
			log.debug("user","Manager");
			projectResult = getProjectForManager(loggedInUserId,search);
		}
		// check for project for project manage
		if(isArrayNotEmpty(projectResult)){
			var xmlStr = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
			xmlStr += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
			xmlStr += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
			xmlStr += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
			xmlStr += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
			xmlStr += 'xmlns:html="http://www.w3.org/TR/REC-html40">';
			xmlStr += '<Styles>'
				+ '<Style ss:ID="s63">'
				+ '<Font x:CharSet="204" ss:Size="12" ss:Color="#000000" ss:Bold="1" ss:Underline="Single"/>'
				+ '</Style>' + '</Styles>';

			xmlStr += '<Worksheet ss:Name="Sheet1">';
			xmlStr += '<Table>'
				+ '<Row>'
				+ '<Cell><Data ss:Type="String"> Project </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Revenue - YTD </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Cost - YTD </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Sold Margin% </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Actual Margin </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> % Actual Margin </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> total Revenue - Project total </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Profit / Loss </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Project Practice </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Customer </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> Start Date </Data></Cell>'
				+ '<Cell><Data ss:Type="String"> End Date </Data></Cell>'
				+ '</Row>';
			for (var int = 0; int < projectResult.length; int++) {
				var projectId = projectResult[int].getValue("internalid");
				var suiteurl = url.resolveScript({
					scriptId: 'customscript_brillio_get_margin_back_sut',
					deploymentId: 'customdeploy_brillio_get_margin_back_sut',
					returnExternalUrl: true,
					params: {'projectId':projectId}
				});
				var response = https.get({
					url: suiteurl
				});
				var jsonObj = JSON.parse(response.body);
				var ytd_rev = parseFloat(jsonObj[0].ytd_rev);
				var ytd_exp = parseFloat(jsonObj[0].ytd_exp);
				var actual_margin = ytd_rev - ytd_exp;
				log.error("actual_margin",actual_margin);
				var actual_margin_per = 0;
				if(ytd_rev != "0"){
					actual_margin_per = ((actual_margin/ytd_rev)*100).toFixed(2);
					log.error("actual_margin_per",actual_margin_per);
				}else{
					actual_margin_per = "-"
				}
				var sold_margin = projectResult[int].getValue("custentity_sold_margin_percent");
				var profit = "";
				if(sold_margin){
					profit = (actual_margin - (ytd_rev/parseFloat(sold_margin))).toFixed(2);
				}else{                                         
					profit = (actual_margin - ytd_rev).toFixed(2)
				}
				var totalRevenue = ytd_rev - parseFloat(projectResult[int].getValue("custentity_projectvalue")); 				
				xmlStr += '<Row>'
					+ '<Cell><Data ss:Type="String">'+projectResult[int].getValue("companyname")+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+ytd_rev.toFixed(2)+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+ytd_exp.toFixed(2)+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+sold_margin+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+actual_margin.toFixed(2)+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+actual_margin_per+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+totalRevenue.toFixed(2)+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+profit+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+projectResult[int].getText("custentity_practice")+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+projectResult[int].getText("customer")+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+projectResult[int].getValue("startdate")+'</Data></Cell>'
					+ '<Cell><Data ss:Type="String">'+projectResult[int].getValue("enddate")+'</Data></Cell>'
					
					+ '</Row>';
			}
			xmlStr += '</Table></Worksheet></Workbook>';
			var strXmlEncoded = encode.convert({
				string : xmlStr,
				inputEncoding : encode.Encoding.UTF_8,
				outputEncoding : encode.Encoding.BASE_64
			});
			var objXlsFile = file.create({
				name : 'marginPL.xls',
				fileType : file.Type.EXCEL,
				contents : strXmlEncoded
			});
			/*email.send({
				author: 442,
				recipients:"aazamali@inspirria.com",//loggedInUser,
                cc : ["deepak.srinivas@brillio.com","sai.vannamareddy@brillio.com","aazamali@inspirria.com"],
				subject: 'Margin Report',
				body: 'Hi'+"\n"+"please find attached margin report"+"\n"+"Thanks & Regards"+"\n"+"Information System",
				attachments: [objXlsFile]
			});*/
          	
		}
	}

	return {
		execute: execute
	};

});
function getPractice(userId,search) {
	var searchResult = search.create({
		type: "customrecord_practice_spoc",
		filters: [
		          ["custrecord_spoc", "anyof", userId]
		          ],
		          columns: [
		                    search.createColumn({
		                    	name: "custrecord_practice",
		                    	label: "Practice"
		                    })
		                    ]
	});
	return searchResult;
}
function getProjectForManager(userId,search) {
	var jobSearchObj = search.create({
		type: "job",
		filters:
			[
			 ["status","noneof","1"], 
			 "AND", 
			 ["custentity_project_allocation_category","anyof","1"], 
			 "AND", 
			 ["type","anyof","2"], 
			 "AND",[ 
			        ["custentity_projectmanager","anyof",userId],"OR",
			        ["custentity_deliverymanager","anyof",userId],"OR",
			        ["custentity_clientpartner","anyof",userId],"OR",
			        ["customer.custentity_clientpartner","anyof",userId],"OR",
		            ["custentity_practice.custrecord_practicehead",'anyof',userId]
			 ]
			 ],
			 columns:
				 [
				  search.createColumn({name: "companyname"}),
				  search.createColumn({name: "custentity_sold_margin_percent"}),
				  search.createColumn({name: "custentity_practice"}),
				  search.createColumn({name: "customer"}),
				  search.createColumn({name: "startdate"}),
				  search.createColumn({name: "enddate"}),
				  search.createColumn({name: "custentity_projectvalue"}),
				  search.createColumn({name: "internalid"})
				  ]
	});
	if(jobSearchObj){
		var resultSet = jobSearchObj.run();
		var searchResultCount = 0;
		var resultSlice = null;
		var finalResult = [];

		do {
			resultSlice = resultSet.getRange(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					finalResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);
	}
	return finalResult;
}
function getProjectForAdmin(search) {
	var jobSearchObj = search.create({
		type: "job",
		filters:
			[
			 ["status","noneof","1"], 
			 "AND", 
			 ["custentity_project_allocation_category","anyof","1"], 
			 "AND", 
			 ["type","anyof","2"]
			 ],
			 columns:
				 [
				  search.createColumn({name: "companyname"}),
				  search.createColumn({name: "custentity_sold_margin_percent"}),
				  search.createColumn({name: "custentity_practice"}),
				  search.createColumn({name: "customer"}),
				  search.createColumn({name: "startdate"}),
				  search.createColumn({name: "enddate"}),
				  search.createColumn({name: "custentity_projectvalue"}),
				  search.createColumn({name: "internalid"})
				  ]
	});
	if(jobSearchObj){
		var resultSet = jobSearchObj.run();
		var searchResultCount = 0;
		var resultSlice = null;
		var finalResult = [];

		do {
			resultSlice = resultSet.getRange(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					finalResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);
	}
	return finalResult;
}