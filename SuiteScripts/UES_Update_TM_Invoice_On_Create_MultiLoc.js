/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 May 2015     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject,
 *            cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF)
 *            markcomplete (Call, Task) reassign (Case) editforecast (Opp,
 *            Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	var i_proj_internal_id = nlapiGetFieldValue('job');
	var projectType = nlapiLookupField('job', i_proj_internal_id,
			'jobbillingtype');

	nlapiLogExecution('debug', 'project type', projectType);

	if (projectType == 'TM') {
		
		var subsidiary = nlapiGetFieldValue('subsidiary');
		var taxCode = nlapiGetFieldValue('custbody_inv_tax_code');
		var doApplyTaxCode = (projectType == 'TM')
				&& (subsidiary == constant.Subsidiary.IN)
				&& isNotEmpty(taxCode);
		var taxCodeAlreadySet = false;
		var autoCheckTimesheet = nlapiGetFieldValue('custbody_auto_check_timesheet');

		if (isTrue(autoCheckTimesheet)) {
			// Billing From and To
			var billingFrom = nlapiGetFieldValue('custbody_billfrom');
			var billingTo = nlapiGetFieldValue('custbody_billto');
			
			// check if both the fields have values
			if (isNotEmpty(billingFrom) && isNotEmpty(billingTo)) {
				billFrom = nlapiStringToDate(billingFrom);
				billTo = nlapiStringToDate(billingTo);
				taxCodeAlreadySet = doApplyTaxCode; // setting it to true so we
				// don't need to set tax code again
				
				var b_is_multi_loc_invoice = nlapiGetFieldValue('custbody_is_multi_loc_invoice');
				if(b_is_multi_loc_invoice == 'T')
				{
					var a_resource_allocations = new Array();
					var a_emp_list = new Array();
				
					var filters_search_allocation = new Array();
					filters_search_allocation[0] = new nlobjSearchFilter('project', null, 'anyof', i_proj_internal_id);
					filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', billTo);
					filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', billFrom);
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('resource');
					columns[1] = new nlobjSearchColumn('enddate');
					columns[2] = new nlobjSearchColumn('custevent_gstworklocation');
					
					var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
					if (project_allocation_result)
					{
						for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++)
						{
							var i_employee_id = project_allocation_result[i_search_indx].getValue('resource');
							var s_end_date = project_allocation_result[i_search_indx].getValue('enddate');
							var i_gst_state = project_allocation_result[i_search_indx].getValue('custevent_gstworklocation');
							
							a_resource_allocations[i_search_indx] = {
								'i_employee_id': i_employee_id,
								's_end_date': s_end_date,
								'i_gst_state': i_gst_state
							};
							a_emp_list.push(i_employee_id);
						}
					}
					
					var i_gst_place_of_spply = nlapiGetFieldValue('custbody_iit_pos_invoice');
				}
				
				var lineCount = nlapiGetLineItemCount('time');

				for (var linenum = 1; linenum <= lineCount; linenum++) {
					var billedDate = nlapiStringToDate(nlapiGetLineItemValue(
							'time', 'billeddate', linenum));

					if (billFrom <= billedDate && billTo >= billedDate)
					{
						//nlapiLogExecution('audit','b_is_multi_loc_invoice',b_is_multi_loc_invoice);
						//nlapiLogExecution('audit','i_gst_place_of_spply',i_gst_place_of_spply);
						if(b_is_multi_loc_invoice == 'T')
						{
							var i_lineLevel_emp = nlapiGetLineItemValue(
							'time', 'employee', linenum);
							var i_existing_emp_index = a_emp_list.indexOf(i_lineLevel_emp);
							if(i_existing_emp_index >= 0)
							{
								var s_emp_allo_end_date = a_resource_allocations[i_existing_emp_index].s_end_date;
								s_emp_allo_end_date = nlapiStringToDate(s_emp_allo_end_date);
								
								if(s_emp_allo_end_date < billedDate)
								{
									for(var i_loop_emp=i_existing_emp_index; i_loop_emp<a_emp_list.length; i_loop_emp++)
									{
										if(a_emp_list[i_loop_emp] == i_lineLevel_emp)
										{
											var s_emp_allo_end_date = a_resource_allocations[i_loop_emp].s_end_date;
											s_emp_allo_end_date = nlapiStringToDate(s_emp_allo_end_date);
								
											if(s_emp_allo_end_date < billedDate)
											{
												
											}
											else
											{
												var i_gst_state_final_allo = a_resource_allocations[i_loop_emp].i_gst_state;
											
												if(i_gst_state_final_allo == i_gst_place_of_spply)
												{
													nlapiSetLineItemValue('time', 'apply', linenum, 'T');
					
													if (doApplyTaxCode) {
														nlapiSetLineItemValue('time', 'taxcode', linenum,
																taxCode);
													}
												}
												else
												{
													nlapiSetLineItemValue('time', 'apply', linenum, 'F');
												}
											}
										}
										else
										{
											nlapiSetLineItemValue('time', 'apply', linenum, 'F');
										}
									}
								}
								else
								{
									var i_gst_state_final_allo = a_resource_allocations[i_existing_emp_index].i_gst_state;
									
									if(i_gst_state_final_allo == i_gst_place_of_spply)
									{
										nlapiLogExecution('audit','check timesheet');
										nlapiSetLineItemValue('time', 'apply', linenum, 'T');
		
										if (doApplyTaxCode) {
											nlapiSetLineItemValue('time', 'taxcode', linenum,
													taxCode);
										}
									}
									else
									{
										nlapiLogExecution('audit','uncheck timesheet');
										nlapiSetLineItemValue('time', 'apply', linenum, 'F');
									}
								}
							}
							else
							{
								nlapiSetLineItemValue('time', 'apply', linenum, 'F');
							}
						}
						else
						{
							nlapiSetLineItemValue('time', 'apply', linenum, 'T');

							if (doApplyTaxCode) {
								nlapiSetLineItemValue('time', 'taxcode', linenum,
										taxCode);
							}
						}
						
					} else {
						nlapiSetLineItemValue('time', 'apply', linenum, 'F');
					}

					/*if (billedDate > billTo) {
						break;
					}*/
				}
				nlapiLogExecution('debug', 'auto check done');
			} else {
				throw "Billing From and To are empty";
			}

			nlapiSetFieldValue('custbody_auto_check_timesheet', 'F');
		}

		if (doApplyTaxCode && !taxCodeAlreadySet) {
			nlapiLogExecution('debug', 'setting tax code', 2);
			var lineCount = nlapiGetLineItemCount('time');
nlapiLogExecution('debug', 'lineCount ', lineCount );
			var billingTo = nlapiGetFieldValue('custbody_billto');
			billTo = nlapiStringToDate(billingTo);

			for (var linenum = 1; linenum <= lineCount; linenum++) {
				var isApplied = nlapiGetLineItemValue('time', 'apply', linenum);

				if (isTrue(isApplied)) {
					nlapiSetLineItemValue('time', 'taxcode', linenum, taxCode);
nlapiLogExecution('debug', 'set', 'set');
				} 
			}
		}
		
		if(nlapiGetUser() == 39108)
		{
			return false;
		}
	}
}
