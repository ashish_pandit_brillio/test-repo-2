function afterSubmitRecord(type)
{
	if(type == "create" || type == "edit"){
		var recObj = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var externalHire = recObj.getFieldValue("custrecord_frf_details_external_hire");
		if(externalHire == "T"){
			var rrfNumber = getRRFNumber(nlapiGetRecordId());
			recObj.setFieldValue("custrecord_frf_details_ns_rrf_number",rrfNumber);
			//nlapiSubmitField(nlapiGetRecordType(),nlapiGetRecordId(),"custrecord_frf_details_ns_rrf_number",rrfNumber);
			nlapiSubmitRecord(recObj);
		}	
	}	
}
function getRRFNumber(recId) {
	var str = "" + recId;
	var pad = "00000000";
	var s_number = pad.substring(0, pad.length - str.length) + str;
	return "R"+s_number;
}