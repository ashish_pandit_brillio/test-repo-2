/**
 * Create a form to allow the user change the manager / approver of any entity
 * when an employee gets terminated
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Mar 2015     nitish.mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			var mode = request.getParameter('mode');
			var empId = request.getParameter('empid');

			if (isNotEmpty(empId)) {

				if (mode == "export") {
					createInactiveMappingPDF(empId);
				} else {
					// create a form to show all the depended entities on the
					// selected employee
					createInactiveMappingForm(empId);
				}
			} else {
				// create a form to allow user to select the employee
				createSearchEmployeeForm();
			}
		} else if (request.getMethod() == "POST") {
			// update the project and employee record, and show a result page
			inactivateEmployee(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err);
	}
}

function createSearchEmployeeForm() {
	try {
		var getForm = nlapiCreateForm(
		        'TA / EA / RM / PM / DM / CP change form', false);
		getForm.setScript('customscript_cs_emp_inactive_form');
		var emp_list = getForm.addField('custpage_emp', 'select',
		        'Select an employee', 'employee');

		// search and add all inactive employees to the dropdown
		// searchRecord('employee', null,
		// [ 'custentity_employee_inactive', 'is', 'T' ],
		// [ new nlobjSearchColumn('entityid') ]).forEach(function(emp) {
		// emp_list.addSelectOption(emp.getId(), emp.getValue('entityid'));
		// });

		// emp_list.addSelectOption('', '', true);
		getForm.addButton('getemployee', 'Get Employee Details',
		        'redirectToEmpForm');
		response.writePage(getForm);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createSearchEmployeeForm', err);
		throw err;
	}
}

// client script function to redirect the user to the inactive employee form on
// button click
function redirectToEmpForm() {
	try {
		var empId = nlapiGetFieldValue('custpage_emp');

		if (isNotEmpty(empId)) {
			var url = nlapiResolveURL('SUITELET',
			        'customscript_sut_emp_inactivation_form',
			        'customdeploy_sut_emp_inactivation_form')
			        + "&unlayered=T&empid=" + empId;
			window.location = url;
		} else {
			alert('Please select an employee');
		}
	} catch (err) {
		alert(err.message);
	}
}

// post method - update the employee and project record, show a resultant page
function inactivateEmployee(request) {
	try {
		// employee
		var a_employee_list = [];

		// check the expense approver section
		var i_count = request.getLineItemCount('custpage_list_exp_approver');
		var employeeId = null, approver = null;

		for (var linenum = 1; linenum <= i_count; linenum++) {
			employeeId = request.getLineItemValue('custpage_list_exp_approver',
			        'employee', linenum);
			approver = request.getLineItemValue('custpage_list_exp_approver',
			        'expenseapprover', linenum);

			if (isNotEmpty(employeeId) && isNotEmpty(approver)) {
				a_employee_list.push({
				    Employee : employeeId,
				    ExpenseApprover : approver,
				    TimeApprover : ''
				});
			} else {
				// throw "Some data missing";
			}
		}

		// check the time approver section
		i_count = request.getLineItemCount('custpage_list_time_approver');
		for (var linenum = 1; linenum <= i_count; linenum++) {
			employeeId = request.getLineItemValue(
			        'custpage_list_time_approver', 'employee', linenum);
			approver = request.getLineItemValue('custpage_list_time_approver',
			        'timeapprover', linenum);

			if (isNotEmpty(employeeId) && isNotEmpty(approver)) {

				var pos = -1;
				for (var i = 0; i < a_employee_list.length; i++) {
					if (a_employee_list[i].Employee == employeeId) {
						pos = i;
						break;
					}
				}

				if (pos == -1) {
					a_employee_list.push({
					    Employee : employeeId,
					    ExpenseApprover : '',
					    TimeApprover : approver
					});
				} else {
					a_employee_list[i].TimeApprover = approver;
				}
			} else {
				// throw "Some data missing";
			}
		}

		// check the reporting manager section
		i_count = request.getLineItemCount('custpage_list_rep_manager');
		nlapiLogExecution('debug', 'reporting manager count', i_count);
		for (var linenum = 1; linenum <= i_count; linenum++) {
			employeeId = request.getLineItemValue('custpage_list_rep_manager',
			        'employee', linenum);
			approver = request.getLineItemValue('custpage_list_rep_manager',
			        'reportingmanager', linenum);

			if (isNotEmpty(employeeId) && isNotEmpty(approver)) {

				var pos = -1;
				for (var i = 0; i < a_employee_list.length; i++) {
					if (a_employee_list[i].Employee == employeeId) {
						pos = i;
						break;
					}
				}

				if (pos == -1) {
					a_employee_list.push({
					    Employee : employeeId,
					    ExpenseApprover : '',
					    TimeApprover : '',
					    ReportingManager : approver
					});
				} else {
					a_employee_list[i].ReportingManager = approver;
				}
			} else {
				// alert('Please correct the entries for '
				// + nlapiGetLineItemText('custpage_list_rep_manager',
				// 'employee', linenum));
				// return null;
			}
		}

		nlapiLogExecution('debug', 'a_employee_list', JSON
		        .stringify(a_employee_list));

		// Project
		// check the project manager section
		i_count = request.getLineItemCount('custpage_list_project_manager');
		var projectId = null, manager = null;
		var a_project_list = [];

		for (var linenum = 1; linenum <= i_count; linenum++) {
			projectId = request.getLineItemValue(
			        'custpage_list_project_manager', 'project', linenum);
			manager = request.getLineItemValue('custpage_list_project_manager',
			        'projectmanager', linenum);

			if (isNotEmpty(projectId) && isNotEmpty(manager)) {
				a_project_list.push({
				    Project : projectId,
				    ProjectManager : manager,
				    ClientPartner : '',
				    DeliveryManager : ''
				});
			} else {
				// throw "Some data missing";
			}
		}

		// check the delivery manager section
		i_count = request.getLineItemCount('custpage_list_delivery_manager');
		for (var linenum = 1; linenum <= i_count; linenum++) {
			projectId = request.getLineItemValue(
			        'custpage_list_delivery_manager', 'project', linenum);
			manager = request.getLineItemValue(
			        'custpage_list_delivery_manager', 'deliverymanager',
			        linenum);

			if (isNotEmpty(projectId) && isNotEmpty(manager)) {

				var pos = -1;
				for (var i = 0; i < a_project_list.length; i++) {
					if (a_project_list[i].Project == projectId) {
						pos = i;
						break;
					}
				}

				if (pos == -1) {
					a_project_list.push({
					    Project : projectId,
					    ProjectManager : '',
					    ClientPartner : '',
					    DeliveryManager : manager
					});
				} else {
					a_project_list[pos].DeliveryManager = manager;
				}
			} else {
				// throw "Some data missing";
			}
		}

		// check the client partner section
		i_count = request.getLineItemCount('custpage_list_client_partner');
		for (var linenum = 1; linenum <= i_count; linenum++) {
			projectId = request.getLineItemValue(
			        'custpage_list_client_partner', 'project', linenum);
			manager = request.getLineItemValue('custpage_list_client_partner',
			        'clientpartner', linenum);

			if (isNotEmpty(projectId) && isNotEmpty(manager)) {

				var pos = -1;
				for (var i = 0; i < a_project_list.length; i++) {
					if (a_project_list[i].Project == projectId) {
						pos = i;
						break;
					}
				}

				if (pos == -1) {
					a_project_list.push({
					    Project : projectId,
					    ProjectManager : '',
					    ClientPartner : manager,
					    DeliveryManager : ''
					});
				} else {
					a_project_list[pos].ClientPartner = manager;
				}
			} else {
				// throw "Some data missing";
			}
		}

		// customer client partner update
		var a_customer_list = [];
		i_count = request.getLineItemCount('custpage_list_c_client_partner');
		for (var linenum = 1; linenum <= i_count; linenum++) {
			customerId = request.getLineItemValue(
			        'custpage_list_c_client_partner', 'customer', linenum);
			manager = request.getLineItemValue(
			        'custpage_list_c_client_partner', 'clientpartner', linenum);

			if (isNotEmpty(customerId) && isNotEmpty(manager)) {
				a_customer_list.push({
				    Customer : customerId,
				    ClientPartner : manager
				});
			} else {
				// throw "Some data missing";
			}
		}

		var formStatus = {
		    Employee : a_employee_list,
		    Project : a_project_list,
		    Customer : a_customer_list
		};

		// inactivate the employees
		if (isNotEmpty(formStatus)) {
			var a_fields = [];
			var a_values = [];
			var a_expense_approvee = [];
			var errors = [];

			// update the employee records
			formStatus.Employee.forEach(function(emp) {
				try {
					delete a_fields;
					delete a_values;
					a_fields = [];
					a_values = [];

					if (emp.ExpenseApprover) {
						a_fields = [ 'approver' ];
						a_values = [ emp.ExpenseApprover ];
						a_expense_approvee.push(emp.Employee);
					}

					if (emp.TimeApprover) {
						a_fields.push('timeapprover');
						a_values.push(emp.TimeApprover);
					}

					if (emp.ReportingManager) {
						a_fields.push('custentity_reportingmanager');
						a_values.push(emp.ReportingManager);
					}

					nlapiSubmitField('employee', emp.Employee, a_fields,
					        a_values);
				} catch (err) {
					errors.push({
					    type : 'Employee',
					    name : nlapiLookupField('employee', emp.Employee,
					            'entityid'),
					    error : err
					});
				}
			});

			// update the project records
			formStatus.Project
			        .forEach(function(project) {
				        try {
					        delete a_fields;
					        delete a_values;
					        a_fields = [];
					        a_values = [];

					        if (project.ProjectManager) {
						        a_fields = [ 'custentity_projectmanager' ];
						        a_values = [ project.ProjectManager ];
					        }

					        if (project.DeliveryManager) {
						        a_fields.push('custentity_deliverymanager');
						        a_values.push(project.DeliveryManager);
					        }

					        if (project.ClientPartner) {
						        a_fields.push('custentity_clientpartner');
						        a_values.push(project.ClientPartner);
					        }

					        nlapiSubmitField('job', project.Project, a_fields,
					                a_values);
				        } catch (err) {
					        errors.push({
					            type : 'Project',
					            name : nlapiLookupField('job', project.Project,
					                    'entityid'),
					            error : err
					        });
				        }
			        });

			// update the customer records
			formStatus.Customer.forEach(function(customer) {
				try {
					delete a_fields;
					delete a_values;
					a_fields = [];
					a_values = [];

					if (customer.ClientPartner) {
						a_fields = [ 'custentity_clientpartner' ];
						a_values = [ customer.ClientPartner ];
					}

					nlapiSubmitField('customer', customer.Customer, a_fields,
					        a_values);
				} catch (err) {
					errors.push({
					    type : 'Customer',
					    name : nlapiLookupField('customer', customer.Customer,
					            'entityid'),
					    error : err
					});
				}
			});

			// create the resultant form
			var postForm = nlapiCreateForm('Update Result');
			if (isArrayNotEmpty(errors)) {
				var error_list = postForm.addSubList('custpage_error_list',
				        'list', 'Errors');
				error_list.addField('type', 'text', 'Type');
				error_list.addField('name', 'text', 'Name');
				error_list.addField('error', 'text', 'Error');
				error_list.setLineItemValues(errors);
			} else {
				postForm.addField('custpage_msg', 'text', 'Result')
				        .setDisplayType('inline').setDefaultValue(
				                'All Records Updated');
			}

			response.writePage(postForm);
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'inactivateEmployee', err);
		throw err;
	}
}

function createInactiveMappingForm(employeeId) {
	try {

		if (isEmpty(employeeId)) {
			throw "No employee selected";
		}

		// employeeId = nlapiGetUser();
		var getForm = nlapiCreateForm(
		        'TA / EA / RM / PM / DM / CP change form', false);

		// inactive employee record
		getForm.setScript('customscript_cs_emp_inactive_form');
		getForm.addButton('custpage_btn_export', 'Export To Excel',
		        'exportToExcel');
		getForm.addField('custpage_main_emp', 'select', 'Employee', 'employee')
		        .setDisplayType('inline').setDefaultValue(employeeId);

		// get dependent employees
		var a_employee_list = getRelatedEmployees(employeeId);

		getForm.addTab('custpage_employee', 'Employees');

		// Expense approver section
		getForm.addSubTab('custpage_expense_approver', 'Expense Approver',
		        'custpage_employee');
		getForm
		        .addField('custpage_all_expense', 'select',
		                'New Expense Approver', 'employee',
		                'custpage_expense_approver');
		var expense_approver_sublist = getForm.addSubList(
		        'custpage_list_exp_approver', 'list', 'Expense Approver',
		        'custpage_expense_approver');
		expense_approver_sublist.addButton('custpage_btn_expense',
		        'Update All', 'updateExpenseApprover');
		expense_approver_sublist.addButton('custpage_btn_expense_2',
		        'Update Selected', 'updateSelectedExpenseApprover');
		expense_approver_sublist.addField('employee', 'select', 'Name',
		        'employee').setDisplayType('inline');
		expense_approver_sublist.addField('expenseapprover', 'select',
		        'Expense Approver', 'employee');// .setMandatory(true);
		expense_approver_sublist.addField('select', 'checkbox', 'Select');
		expense_approver_sublist
		        .setLineItemValues(a_employee_list.ExpenseApproverList);

		// Reporting Manager section
		getForm.addSubTab('custpage_reporting_manager', 'Reporting Manager',
		        'custpage_employee');
		getForm.addField('custpage_all_rm', 'select', 'New Reporting Manager',
		        'employee', 'custpage_reporting_manager');
		var rep_manager_sublist = getForm.addSubList(
		        'custpage_list_rep_manager', 'list', 'Reporting Manager',
		        'custpage_reporting_manager');
		rep_manager_sublist.addButton('custpage_btn_rm', 'Update All',
		        'updateReportingManager');
		rep_manager_sublist.addButton('custpage_btn_rm_2', 'Update Selected',
		        'updateSelectedReportingManager');
		rep_manager_sublist.addField('employee', 'select', 'Name', 'employee')
		        .setDisplayType('inline');
		rep_manager_sublist.addField('reportingmanager', 'select',
		        'Reporting Manager', 'employee');
		rep_manager_sublist.addField('select', 'checkbox', 'Select');
		rep_manager_sublist
		        .setLineItemValues(a_employee_list.ReportingManagerList);

		// Time approver section
		getForm.addSubTab('custpage_time_approver', 'Time Approver',
		        'custpage_employee');
		getForm.addField('custpage_all_time', 'select', 'New Time Approver',
		        'employee', 'custpage_time_approver');
		var time_approver_sublist = getForm.addSubList(
		        'custpage_list_time_approver', 'list', 'Time Approver',
		        'custpage_time_approver');
		time_approver_sublist.addButton('custpage_btn_time', 'Update All',
		        'updateTimeApprover');
		time_approver_sublist.addButton('custpage_btn_time_2',
		        'Update Selected', 'updateSelectedTimeApprover');
		time_approver_sublist
		        .addField('employee', 'select', 'Name', 'employee')
		        .setDisplayType('inline');
		time_approver_sublist.addField('timeapprover', 'select',
		        'Time Approver', 'employee');
		time_approver_sublist.addField('select', 'checkbox', 'Select');
		time_approver_sublist
		        .setLineItemValues(a_employee_list.TimeApproverList);

		// get dependent project details
		var a_project_list = getRelatedProjects(employeeId);

		getForm.addTab('custpage_project', 'Project');

		// project manager section
		getForm.addSubTab('custpage_project_manager', 'Project Manager',
		        'custpage_project');
		getForm.addField('custpage_all_pm', 'select', 'New Project Manager',
		        'employee', 'custpage_project_manager');
		var project_manager_sublist = getForm.addSubList(
		        'custpage_list_project_manager', 'list', 'Project Manager',
		        'custpage_project_manager');
		project_manager_sublist.addButton('custpage_btn_pm', 'Update All',
		        'updateProjectManager');
		project_manager_sublist.addButton('custpage_btn_pm_2',
		        'Update Selected', 'updateSelectedProjectManager');
		project_manager_sublist.addField('project', 'select', 'Name', 'job')
		        .setDisplayType('inline');
		project_manager_sublist.addField('projectmanager', 'select',
		        'Project Manager', 'employee');
		project_manager_sublist.addField('select', 'checkbox', 'Select');
		project_manager_sublist
		        .setLineItemValues(a_project_list.ProjectManagerList);

		// delivery manager section
		getForm.addSubTab('custpage_delivery_manager', 'Delivery Manager',
		        'custpage_project');
		getForm.addField('custpage_all_dm', 'select', 'New Delivery Manager',
		        'employee', 'custpage_delivery_manager');
		var delivery_manager_sublist = getForm.addSubList(
		        'custpage_list_delivery_manager', 'list', 'Delivery Manager',
		        'custpage_delivery_manager');
		delivery_manager_sublist.addButton('custpage_btn_dm', 'Update All',
		        'updateDeliveryManager');
		delivery_manager_sublist.addButton('custpage_btn_dm_2',
		        'Update Selected', 'updateSelectedDeliveryManager');
		delivery_manager_sublist.addField('project', 'select', 'Name', 'job')
		        .setDisplayType('inline');
		delivery_manager_sublist.addField('deliverymanager', 'select',
		        'Delivery Manager', 'employee');
		delivery_manager_sublist.addField('select', 'checkbox', 'Select');
		delivery_manager_sublist
		        .setLineItemValues(a_project_list.DeliveryManagerList);

		// client partner section - customer
		var customerClientList = [];
		var customerClientSearch = nlapiSearchRecord('customer', null, [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custentity_clientpartner', null,
		                'anyof', employeeId) ]);

		if (customerClientSearch) {
			customerClientSearch.forEach(function(customer) {
				customerClientList.push({
				    customer : customer.getId(),
				    employee : employeeId,
				    clientpartner : ''
				});
			});
		}

		getForm.addSubTab('custpage_c_client_partner',
		        'Client Partner - Customer', 'custpage_project');
		getForm.addField('custpage_all_c_cp', 'select', 'New Client Partner',
		        'employee', 'custpage_c_client_partner');

		var c_client_partner_sublist = getForm.addSubList(
		        'custpage_list_c_client_partner', 'list',
		        'Client Partner - Customer', 'custpage_c_client_partner');
		c_client_partner_sublist.addButton('custpage_btn_c_cp', 'Update All',
		        'updateCustomerClientPartner');
		c_client_partner_sublist.addButton('custpage_btn_c_cp_2',
		        'Update Selected', 'updateSelectedCustomerClientPartner');
		c_client_partner_sublist.addButton('custpage_btn_c_p_cp',
		        'Update In Project', 'updateCustomerClientPartnerToProject');
		c_client_partner_sublist.addField('customer', 'select', 'Name',
		        'Customer').setDisplayType('inline');
		c_client_partner_sublist.addField('clientpartner', 'select',
		        'Client Partner', 'employee');
		c_client_partner_sublist.addField('select', 'checkbox', 'Select');
		c_client_partner_sublist.setLineItemValues(customerClientList);

		// client partner section
		getForm.addSubTab('custpage_client_partner',
		        'Client Partner - Project', 'custpage_project');
		getForm.addField('custpage_all_cp', 'select', 'New Client Partner',
		        'employee', 'custpage_client_partner');
		var client_partner_sublist = getForm.addSubList(
		        'custpage_list_client_partner', 'list',
		        'Client Partner- Project', 'custpage_client_partner');
		client_partner_sublist.addButton('custpage_btn_cp', 'Update All',
		        'updateClientPartner');
		client_partner_sublist.addButton('custpage_btn_cp_2',
		        'Update Selected', 'updateSelectedClientPartner');
		client_partner_sublist.addField('customer', 'select', 'Customer',
		        'customer').setDisplayType('hidden');
		client_partner_sublist.addField('project', 'select', 'Name', 'job')
		        .setDisplayType('inline');
		client_partner_sublist.addField('clientpartner', 'select',
		        'Client Partner', 'employee');
		client_partner_sublist.addField('select', 'checkbox', 'Select');
		client_partner_sublist
		        .setLineItemValues(a_project_list.ClientPartnerList);

		getForm.addSubmitButton('Submit');
		// getForm.addButton('custpage_btn_submit', 'Update', 'updateEmployee');

		response.writePage(getForm);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createInactiveMappingForm', err);
		throw err;
	}
}

function createInactiveMappingPDF(employeeId) {
	try {

		if (isEmpty(employeeId)) {
			throw "No employee selected";
		}

		var csv = "";

		var resourceName = nlapiLookupField('employee', employeeId, 'entityid');
		csv += resourceName;
		csv += "\n";

		// get dependent employees
		var a_employee_list = getRelatedEmployees(employeeId);

		csv += "\nExpense Approver\n";
		a_employee_list.ExpenseApproverList.forEach(function(emp) {
			csv += emp.name + "\n";
		});

		csv += "\nReporting Manager\n";
		a_employee_list.ReportingManagerList.forEach(function(emp) {
			csv += emp.name + "\n";
		});

		csv += "\nTime Approver\n";
		a_employee_list.TimeApproverList.forEach(function(emp) {
			csv += emp.name + "\n";
		});

		// get dependent project details
		var a_project_list = getRelatedProjects(employeeId);

		csv += "\nProject Manager\n";
		a_project_list.ProjectManagerList.forEach(function(project) {
			csv += project.name + "\n";
		});

		csv += "\nDelivery Manager\n";
		a_project_list.DeliveryManagerList.forEach(function(project) {
			csv += project.name + "\n";
		});

		var customerClientSearch = nlapiSearchRecord('customer', null, [
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custentity_clientpartner', null,
		                'anyof', employeeId) ], [
		        new nlobjSearchColumn('entityid'),
		        new nlobjSearchColumn('altname') ]);
		csv += "\nClient Partner - Customer\n";
		if (customerClientSearch) {
			customerClientSearch.forEach(function(customer) {
				csv += customer.getValue('entityid') + " "
				        + customer.getValue('altname') + "\n";
			});
		}

		csv += "\nClient Partner - Project\n";
		a_project_list.ClientPartnerList.forEach(function(project) {
			csv += project.name + "\n";
		});

		var file = nlapiCreateFile(resourceName + ".csv", 'CSV', csv);
		response.setContentType('CSV', resourceName + ".csv");
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'createInactiveMappingPDF', err);
		throw err;
	}
}

// ---------------- Client Script ----

function pageInit() {

}

function updateEmployee() {
	try {
		var formStatus = validateForm();

		if (isNotEmpty(formStatus)) {
			var a_fields = [];
			var a_values = [];
			var a_expense_approvee = [];
			var errors = '';

			// update the employee records
			formStatus.Employee
			        .forEach(function(emp) {
				        try {
					        delete a_fields;
					        delete a_values;
					        a_fields = [];
					        a_values = [];

					        if (emp.ExpenseApprover) {
						        a_fields = [ 'approver' ];
						        a_values = [ emp.ExpenseApprover ];
						        a_expense_approvee.push(emp.Employee);
					        }

					        if (emp.TimeApprover) {
						        a_fields.push('timeapprover');
						        a_values.push(emp.TimeApprover);
					        }

					        if (emp.ReportingManager) {
						        a_fields.push('custentity_reportingmanager');
						        a_values.push(emp.ReportingManager);
					        }

					        nlapiSubmitField('employee', emp.Employee,
					                a_fields, a_values);
				        } catch (err) {
					        errors += 'Employee : '
					                + nlapiLookupField('employee',
					                        emp.Employee, 'name') + "\n";
				        }
			        });

			// update the project records
			formStatus.Project
			        .forEach(function(project) {
				        try {
					        delete a_fields;
					        delete a_values;
					        a_fields = [];
					        a_values = [];

					        if (project.ProjectManager) {
						        a_fields = [ 'custentity_projectmanager' ];
						        a_values = [ project.ProjectManager ];
					        }

					        if (project.DeliveryManager) {
						        a_fields.push('custentity_deliverymanager');
						        a_values.push(project.DeliveryManager);
					        }

					        nlapiSubmitField('job', project.Project, a_fields,
					                a_values);
				        } catch (err) {
					        errors += 'Project : '
					                + nlapiLookupField('job', project.Project,
					                        'name') + "\n";
				        }
			        });

			alert('Submitted.\nPlease inactive the employee');

			if (isNotEmpty(errors)) {
				alert('Some records failed : \n' + errors);
				var url = nlapiResolveURL('SUITELET',
				        constant.Suitelet.Script.Employee_Inactive,
				        constant.Suitelet.Deployement.Employee_Inactive);
				window.location = url + "&empid="
				        + nlapiGetFieldValue('custpage_main_emp')
				        + "&unlayered=T";
			} else {
				// redirect back to the employee record
				window.location = nlapiResolveURL('RECORD', 'employee',
				        nlapiGetFieldValue('custpage_main_emp'), 'EDIT');
			}

			return true;
		} else {
			return false;
		}
	} catch (err) {
		alert(err.message);
	}
}

// read all the new values and validate the form
function validateForm() {
	// employee
	var a_employee_list = [];

	// check the expense approver section
	var i_count = nlapiGetLineItemCount('custpage_list_exp_approver');
	var employeeId = null, approver = null;

	for (var linenum = 1; linenum <= i_count; linenum++) {
		employeeId = nlapiGetLineItemValue('custpage_list_exp_approver',
		        'employee', linenum);
		approver = nlapiGetLineItemValue('custpage_list_exp_approver',
		        'expenseapprover', linenum);

		if (isNotEmpty(employeeId) && isNotEmpty(approver)) {
			a_employee_list.push({
			    Employee : employeeId,
			    ExpenseApprover : approver,
			    TimeApprover : '',
			    ReportingManager : ''
			});
		} else {
			alert('Please correct the entries for '
			        + nlapiGetLineItemText('custpage_list_exp_approver',
			                'employee', linenum));
			return null;
		}
	}

	// check the time approver section
	i_count = nlapiGetLineItemCount('custpage_list_time_approver');
	for (var linenum = 1; linenum <= i_count; linenum++) {
		employeeId = nlapiGetLineItemValue('custpage_list_time_approver',
		        'employee', linenum);
		approver = nlapiGetLineItemValue('custpage_list_time_approver',
		        'timeapprover', linenum);

		if (isNotEmpty(employeeId) && isNotEmpty(approver)) {

			var pos = -1;
			for (var i = 0; i < a_employee_list.length; i++) {
				if (a_employee_list[i].Employee == employeeId) {
					pos = i;
					break;
				}
			}

			if (pos == -1) {
				a_employee_list.push({
				    Employee : employeeId,
				    ExpenseApprover : '',
				    TimeApprover : approver,
				    ReportingManager : ''
				});
			} else {
				a_employee_list[i].TimeApprover = approver;
			}
		} else {
			alert('Please correct the entries for '
			        + nlapiGetLineItemText('custpage_list_time_approver',
			                'employee', linenum));
			return null;
		}
	}

	// check the reporting manager section
	i_count = nlapiGetLineItemCount('custpage_list_rep_manager');
	for (var linenum = 1; linenum <= i_count; linenum++) {
		employeeId = nlapiGetLineItemValue('custpage_list_rep_manager',
		        'employee', linenum);
		approver = nlapiGetLineItemValue('custpage_list_rep_manager',
		        'reportingmanager', linenum);

		if (isNotEmpty(employeeId) && isNotEmpty(approver)) {

			var pos = -1;
			for (var i = 0; i < a_employee_list.length; i++) {
				if (a_employee_list[i].Employee == employeeId) {
					pos = i;
					break;
				}
			}

			if (pos == -1) {
				a_employee_list.push({
				    Employee : employeeId,
				    ExpenseApprover : '',
				    TimeApprover : '',
				    ReportingManager : approver
				});
			} else {
				a_employee_list[i].ReportingManager = approver;
			}
		} else {
			alert('Please correct the entries for '
			        + nlapiGetLineItemText('custpage_list_rep_manager',
			                'employee', linenum));
			return null;
		}
	}

	// Project
	// check the project manager section
	i_count = nlapiGetLineItemCount('custpage_list_project_manager');
	var projectId = null, manager = null;
	var a_project_list = [];

	for (var linenum = 1; linenum <= i_count; linenum++) {
		projectId = nlapiGetLineItemValue('custpage_list_project_manager',
		        'project', linenum);
		manager = nlapiGetLineItemValue('custpage_list_project_manager',
		        'projectmanager', linenum);

		if (isNotEmpty(projectId) && isNotEmpty(manager)) {
			a_project_list.push({
			    Project : projectId,
			    ProjectManager : manager,
			    DeliveryManager : ''
			});
		} else {
			alert('Please correct the entries for '
			        + nlapiGetLineItemText('custpage_list_project_manager',
			                'project', linenum));
			return null;
		}
	}

	// check the delivery manager section
	i_count = nlapiGetLineItemCount('custpage_list_delivery_manager');
	for (var linenum = 1; linenum <= i_count; linenum++) {
		projectId = nlapiGetLineItemValue('custpage_list_delivery_manager',
		        'project', linenum);
		manager = nlapiGetLineItemValue('custpage_list_delivery_manager',
		        'deliverymanager', linenum);

		if (isNotEmpty(projectId) && isNotEmpty(manager)) {

			var pos = -1;
			for (var i = 0; i < a_project_list.length; i++) {
				if (a_project_list[i].Project == projectId) {
					pos = i;
					break;
				}
			}

			if (pos == -1) {
				a_project_list.push({
				    Project : projectId,
				    ProjectManager : '',
				    DeliveryManager : manager
				});
			} else {
				a_project_list[pos].DeliveryManager = manager;
			}
		} else {
			alert('Please correct the entries for '
			        + nlapiGetLineItemText('custpage_list_delivery_manager',
			                'project', linenum));
			return null;
		}
	}

	return {
	    Employee : a_employee_list,
	    Project : a_project_list
	};
}

// --- client script part

function updateCustomerClientPartnerToProject() {
	var customerMap = {};
	var customerCount = nlapiGetLineItemCount('custpage_list_c_client_partner');

	for (var linenum = 1; linenum <= customerCount; linenum++) {
		var clientPartner = nlapiGetLineItemValue(
		        'custpage_list_c_client_partner', 'clientpartner', linenum);

		if (clientPartner) {
			customerMap[nlapiGetLineItemValue('custpage_list_c_client_partner',
			        'customer', linenum)] = clientPartner;
		}
	}

	// console.log("customerMap " + JSON.stringify(customerMap));
	var projectCount = nlapiGetLineItemCount('custpage_list_client_partner');
	// console.log("projectCount " + projectCount);

	for (var linenum = 1; linenum <= projectCount; linenum++) {
		var customer = nlapiGetLineItemValue('custpage_list_client_partner',
		        'customer', linenum);
		// console.log("customer " + customer);
		// console.log("customerMap[customer] " + customerMap[customer]);

		if (customer && customerMap[customer]) {
			nlapiSelectLineItem('custpage_list_client_partner', linenum);
			nlapiSetCurrentLineItemValue('custpage_list_client_partner',
			        'clientpartner', customerMap[customer]);
			nlapiCommitLineItem('custpage_list_client_partner');
		}
	}
}

// set the entered time approver as the time approver of all the employees
// update selected part
function updateSelectedReportingManager() {
	var value = nlapiGetFieldValue('custpage_all_rm');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_rep_manager',
		        'reportingmanager', value);
		clearSelection('custpage_list_rep_manager');
	} else {
		alert("Please select an employee");
	}
}

function updateSelectedClientPartner() {
	var value = nlapiGetFieldValue('custpage_all_cp');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_client_partner',
		        'clientpartner', value);
		clearSelection('custpage_list_client_partner');
	} else {
		alert("Please select an employee");
	}
}

function updateSelectedCustomerClientPartner() {
	var value = nlapiGetFieldValue('custpage_all_c_cp');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_c_client_partner',
		        'clientpartner', value);
		clearSelection('custpage_list_c_client_partner');
	} else {
		alert("Please select an employee");
	}
}

function updateSelectedExpenseApprover() {
	var value = nlapiGetFieldValue('custpage_all_expense');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_exp_approver',
		        'expenseapprover', value);
		clearSelection('custpage_list_exp_approver');
	} else {
		alert("Please select an employee");
	}
}

function updateSelectedTimeApprover() {
	var value = nlapiGetFieldValue('custpage_all_time');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_time_approver', 'timeapprover',
		        value);
		clearSelection('custpage_list_time_approver');
	} else {
		alert("Please select an employee");
	}
}

// set the entered project manager as the manager for all the projects

function updateSelectedProjectManager() {
	var value = nlapiGetFieldValue('custpage_all_pm');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_project_manager',
		        'projectmanager', value);
		clearSelection('custpage_list_project_manager');
	} else {
		alert("Please select an employee");
	}
}

function updateSelectedDeliveryManager() {
	var value = nlapiGetFieldValue('custpage_all_dm');

	if (isNotEmpty(value)) {
		updateSelectedInSection('custpage_list_delivery_manager',
		        'deliverymanager', value);
		clearSelection('custpage_list_delivery_manager');
	} else {
		alert("Please select an employee");
	}
}

// update all part
function updateReportingManager() {
	var value = nlapiGetFieldValue('custpage_all_rm');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_rep_manager', 'reportingmanager',
		        value);
	} else {
		alert("Please select an employee");
	}
}

function updateExpenseApprover() {
	var value = nlapiGetFieldValue('custpage_all_expense');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_exp_approver', 'expenseapprover',
		        value);
	} else {
		alert("Please select an employee");
	}
}

function updateTimeApprover() {
	var value = nlapiGetFieldValue('custpage_all_time');
	// alert(value);

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_time_approver', 'timeapprover', value);
	} else {
		alert("Please select an employee");
	}
}

// set the entered project manager as the manager for all the projects

function updateProjectManager() {
	var value = nlapiGetFieldValue('custpage_all_pm');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_project_manager', 'projectmanager',
		        value);
	} else {
		alert("Please select an employee");
	}
}

function updateClientPartner() {
	var value = nlapiGetFieldValue('custpage_all_cp');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_client_partner', 'clientpartner',
		        value);
	} else {
		alert("Please select an employee");
	}
}

function updateCustomerClientPartner() {
	var value = nlapiGetFieldValue('custpage_all_c_cp');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_c_client_partner', 'clientpartner',
		        value);
	} else {
		alert("Please select an employee");
	}
}

function updateDeliveryManager() {
	var value = nlapiGetFieldValue('custpage_all_dm');

	if (isNotEmpty(value)) {
		updateAllInSection('custpage_list_delivery_manager', 'deliverymanager',
		        value);
	} else {
		alert("Please select an employee");
	}
}

function updateAllInSection(sublistName, fieldName, value) {

	var i_count = nlapiGetLineItemCount(sublistName);
	// alert(i_count);

	for (var linenum = 1; linenum <= i_count; linenum++) {
		// alert(value);
		nlapiSelectLineItem(sublistName, linenum)
		// alert(nlapiGetLineItemValue(sublistName, fieldName, linenum));
		nlapiSetCurrentLineItemValue(sublistName, fieldName, value);
		nlapiCommitLineItem(sublistName);
		// alert(nlapiGetLineItemValue(sublistName, fieldName, linenum));

	}
}

function updateSelectedInSection(sublistName, fieldName, value) {
	var i_count = nlapiGetLineItemCount(sublistName);

	for (var linenum = 1; linenum <= i_count; linenum++) {
		nlapiSelectLineItem(sublistName, linenum);

		if (isTrue(nlapiGetCurrentLineItemValue(sublistName, 'select'))) {
			nlapiSetCurrentLineItemValue(sublistName, fieldName, value);
			nlapiCommitLineItem(sublistName);
		}
	}
}

function clearSelection(sublistName) {
	var i_count = nlapiGetLineItemCount(sublistName);

	for (var linenum = 1; linenum <= i_count; linenum++) {

		nlapiSelectLineItem(sublistName, linenum);

		if (isTrue(nlapiGetCurrentLineItemValue(sublistName, 'select'))) {
			nlapiSetCurrentLineItemValue(sublistName, 'select', 'F');
			nlapiCommitLineItem(sublistName);
		}
	}
}

// -- suitelet
function getPendingTimesheets(a_employee_list) {
	return searchRecord('timesheet', null, [
	        new nlobjSearchFilter('employee', null, 'anyof', a_employee_list),
	        new nlobjSearchFilter('approvalstatus', null, 'anyof', [ '2' ]) ],
	        [ new nlobjSearchColumn('employee') ]);
}

function getPendingExpenses(a_employee_list) {
	return searchRecord('expensereport', null, [
	        new nlobjSearchFilter('entity', null, 'anyof', a_employee_list),
	        new nlobjSearchFilter('status', null, 'anyof', [ 'ExpRept:B' ]) ],
	        [ new nlobjSearchColumn('entity') ]);
}

function getPendingPurchaseReuqest() {
	return [];
}

// --- client scirpt -- export
function exportToExcel(request) {
	try {
		var empId = nlapiGetFieldValue('custpage_main_emp');

		var url = nlapiResolveURL('SUITELET',
		        'customscript_sut_emp_inactivation_form',
		        'customdeploy_sut_emp_inactivation_form')
		        + "&unlayered=T&mode=export&empid=" + empId;
		window.location = url;
	} catch (err) {
		alert(err.message);
	}
}

function getSublistDataForExport(subListId, columnId, sublistName) {
	var i_count = nlapiGetLineItemCount(subListId);
	var text = sublistName + "\n";

	for (var linenum = 1; linenum <= i_count; linenum++) {
		text += nlapiGetLineItemText(subListId, columnId, linenum);
		text += "\n";
	}

	return text;
}