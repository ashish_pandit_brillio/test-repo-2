function rounding_off_on_save(type){
if(type == 'create' || type == 'edit'){
var i_subsidiary = nlapiGetFieldValue('subsidiary');
var i_currency = nlapiGetFieldValue('currency');
var expense_line_count = nlapiGetLineItemCount('expense');
var item_line_count = nlapiGetLineItemCount('item');


				
				for (var j = 1; j <= expense_line_count; j++)
				{
					var amount = nlapiGetLineItemValue('expense', 'amount', j);
					var amount = nlapiGetLineItemValue('expense', 'account', j);
					//var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
					//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
					//var is_swach_cess_reverse_true_mainLine = nlapiGetLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', j); custcol_st_notcredit
					if (parseInt(i_subsidiary) == 3 && parseInt(i_currency) != parseInt(1))
					{
						var amount_round_off = nlapiGetLineItemValue('expense', 'grossamt', j);
						amount_round_off = Math.round(amount_round_off);
						nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
						nlapiSetLineItemValue('expense', 'grossamt', j, amount_round_off);
					}
				}
				
				for (var m = 1; m <= item_line_count; m++)
				{
					var amount = nlapiGetLineItemValue('item', 'amount', m);
					//var taxAmount = parseFloat(amount) * (parseFloat(0.5) / 100);
					//taxAmount = applySTRoundMethod(stRoundMethod, taxAmount);
					var is_swach_cess_reverse_true_mainLine = nlapiGetLineItemValue('item', 'custcol_ait_swachh_cess_reverse', m);
					if (parseInt(i_subsidiary) == 3 && parseInt(i_currency) != parseInt(1))
					{
						var amount_round_off = nlapiGetLineItemValue('item', 'amount', m);
						amount_round_off = Math.round(amount_round_off);
						nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
						nlapiSetLineItemValue('item', 'amount', m, amount_round_off);
					}
				}
				
var usertotal_ = nlapiGetFieldValue('usertotal');
usertotal_ = Math.round(usertotal_);
nlapiSetFieldValue('usertotal',usertotal_);
}  
} 
function vendorbill_before_loadbeforeLoad(type,form)
{

try{
 if (type == 'copy'){
var newId = nlapiGetRecordId();
var newType = nlapiGetRecordType();
var lineCount = nlapiGetLineItemCount('expense');
nlapiLogExecution('DEBUG','lineCount',lineCount);
var lineCount_i = nlapiGetLineItemCount('item');
nlapiLogExecution('DEBUG','lineCount_i',lineCount_i);
for(var t=1;t<=lineCount;t++){
var y1 = nlapiSetLineItemValue('expense', 'department', t, '');
var y2 = nlapiSetLineItemValue('expense', 'custcol_territory', t, '');
var y3 = nlapiSetLineItemValue('expense', 'custcol_employeenamecolumn', t, '');
var y4 = nlapiSetLineItemValue('expense', 'custcol_employee_entity_id', t, '');
var y5 = nlapiSetLineItemValue('expense', 'custcolprj_name', t, '');
var y6 = nlapiSetLineItemValue('expense', 'custcol_customer_entityid', t, '');
var y7 = nlapiSetLineItemValue('expense', 'custcol_project_entity_id', t, '');
var y8 = nlapiSetLineItemValue('expense', 'custcol_region_master_setup', t, '');
var y9 = nlapiSetLineItemValue('expense', 'department_display', t, '');
var y0 = nlapiSetLineItemValue('expense', 'location', t, '');
var yot = nlapiSetLineItemValue('expense', 'location_display', t, '');
}
for(var q=1;q<=lineCount_i;q++){
var y1 = nlapiSetLineItemValue('item', 'department', q, '');
var y2 = nlapiSetLineItemValue('item', 'custcol_territory', q, '');
var y3 = nlapiSetLineItemValue('item', 'custcol_employeenamecolumn', q, '');
var y4 = nlapiSetLineItemValue('item', 'custcol_employee_entity_id', q, '');
var y5 = nlapiSetLineItemValue('item', 'custcolprj_name', q, '');
var y6 = nlapiSetLineItemValue('item', 'custcol_customer_entityid', q, '');
var y7 = nlapiSetLineItemValue('item', 'custcol_project_entity_id', q, '');
var y8 = nlapiSetLineItemValue('item', 'custcol_region_master_setup', q, '');
var y9 = nlapiSetLineItemValue('item', 'department_display', q, '');
var yot = nlapiSetLineItemValue('item', 'location_display', q, '');
}
nlapiLogExecution('DEBUG','<Before Load Script> type:'+type+', RecordType: '+newType+', Id:'+newId);
}
}
catch(e){
nlapiLogExecution('debug','Error in Before load',e);
}
}

function beforeSubmit_Update_GSTin(type){

try{
var s_billaddress = nlapiGetFieldValue('billaddress'); //billzip
		var i_vendor = nlapiGetFieldValue('entity');
		var i_billzip = nlapiGetFieldValue('billzip');
		var id_gstin_tagged = nlapiGetFieldValue('custbody_vendor_gstin');
		
		if(i_vendor){
		var vendorSearch = nlapiSearchRecord("vendor",null,
				[
		   ["internalid","anyof",i_vendor]
		], 
		[
		   new nlobjSearchColumn("entityid").setSort(false), 
		   new nlobjSearchColumn("custrecord_iit_address_gstn_uid","Address",null), 
		   new nlobjSearchColumn("zipcode","Address",null)
		]
		);
		if(vendorSearch){	
		for(var i=0;i<vendorSearch.length;i++){
		var line_zipCode = vendorSearch[i].getValue("zipcode","Address");		
		if(line_zipCode == i_billzip){
		var gstin_num = vendorSearch[i].getValue("custrecord_iit_address_gstn_uid","Address");
		if(!_logValidation(id_gstin_tagged))
		nlapiSetFieldValue('custbody_vendor_gstin',gstin_num);
		}
		}
		}
		}


}
catch(e){
nlapiLogExecution('debug','Before Submit Error',e);
throw e;
}
}

function update_SBC_Manadatory_Fields(type){
var submit_record_flag = 0;
var context = nlapiGetContext();
 /*----------Added by Koushalya 28/12/2021---------*/
 var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
 var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
 var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
  /*-------------------------------------------------*/

if(type == 'create' || type == 'edit')
{
  if(nlapiGetRecordId() == 431440)
    return;


  
var vendor_bill_rcrd = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
var transaction_internal = nlapiGetRecordId();

var excel_file_obj = '';
		var err_row_excel = '';
		var strVar_excel = '';

		strVar_excel += '<table>';
		strVar_excel += '	<tr>';
		strVar_excel += ' <td width="100%">';
		strVar_excel += '<table width="100%" border="1">';
		strVar_excel += '	<tr>';
		strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';
		strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';
		strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';
		strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';
		strVar_excel += '	</tr>';

try{
var i_currency = vendor_bill_rcrd.getFieldValue('currency');
var transactionNum = vendor_bill_rcrd.getFieldValue('tranid');
var i_subsidiary = vendor_bill_rcrd.getFieldValue('subsidiary');
var expense_line_count = vendor_bill_rcrd.getLineItemCount('expense');
var amt = vendor_bill_rcrd.getFieldValue('usertotal');
var amount_round_off = Math.round(amt);
var gst_not_credit_flag = false;
nlapiLogExecution('audit', 'main amount:- ', amount_round_off);
vendor_bill_rcrd.setFieldValue('usertotal',amount_round_off);	
				if(parseInt(expense_line_count) > parseInt(25)){
				nlapiSubmitField(nlapiGetRecordType(),nlapiGetRecordId(),'custbody_update_expense_using_sch','T');
				return;
				}
				//Logic for expense out
				/*for (var jb = 1; jb <= expense_line_count; jb++)
				{
			//	vendor_bill_rcrd.getLineItemValue('expense', 'custcol_st_notcredit', jb); //custcol_st_notcredit
				var st_notcredit = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_st_notcredit', jb); //custcol_st_notcredit
				if(st_notcredit == 'T')
				gst_not_credit_flag = true;
				nlapiLogExecution('audit', 'gst_not_credit_flag:- ', gst_not_credit_flag);
				}*/
				
				for (var j = 1; j <= expense_line_count; j++)
				{
					var is_swach_cess_reverse_true_mainLine = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_ait_swachh_cess_reverse', j);
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					/* if (parseInt(i_subsidiary) == parseInt(3) && parseInt(i_currency) != parseInt(1))
					{
						var amount_round_off = vendor_bill_rcrd.getLineItemValue('expense', 'grossamt', j);
						var i_account = vendor_bill_rcrd.getLineItemValue('expense', 'account', j); //custcol_st_notcredit
						if(gst_not_credit_flag == true && (i_account == 1134 || i_account == 1135 || i_account ==  1136 )){
						nlapiLogExecution('audit', 'gst_not_credit_flag amount_round_off:- ', amount_round_off);
						}
						else{
						amount_round_off = Math.round(amount_round_off);
						nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('expense', 'grossamt', j, amount_round_off);
						submit_record_flag == 1;
						}
					} */
				if(is_swach_cess_reverse_true_mainLine == 'T'){
				var emp_type = '';
				var person_type = '';
				var onsite_offsite = '';
				var s_employee_name_split = '';
				var cust_full_name_with_id = '';
				
				var emp_fusion_id = '';
				var emp_frst_name = '';
				var emp_middl_name = '';
				var emp_lst_name = '';
				var emp_full_name = '';
				var emp_practice = '';
				var s_proj_desc_split = '';
				var s_employee_name =  '';
				var s_employee_id = '';
				var s_employee_name_id = '';
				var proj_desc = '';
				var project_entity_id = '';
				var s_proj_desc_id = '';
				var existing_practice = '';
				var proj_full_name_with_id = '';
				var employee_with_id = '';
				var existing_territory = '';
				var emp_location = '';
				var existing_location = '';
				
							
				
				existing_practice = vendor_bill_rcrd.getLineItemValue('expense', 'department',j);
				existing_location = vendor_bill_rcrd.getLineItemValue('expense', 'location',j);
				existing_territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory',j);
				s_employee_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
				s_employee_id = vendor_bill_rcrd.getLineItemValue('expense','custcol_employee_entity_id',j);	
							if(_logValidation(s_employee_id)){
							 s_employee_name_split = s_employee_id;
							 nlapiLogExecution('audit','s_employee_name_split expense',s_employee_name_split);
							}
							else if(_logValidation(s_employee_name)){
							s_employee_name_id = s_employee_name.split('-');
							s_employee_name_split = s_employee_name_id[0];
							 nlapiLogExecution('audit','s_employee_name',s_employee_name);
							}
				if(_logValidation(s_employee_name) || _logValidation(s_employee_id))
				{
					//var s_employee_name_id = emp_name_selected.split('-');
					//var s_employee_name_split = s_employee_name_id[0];
								
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp))
					{
						var emp_id = a_results_emp[0].getId();
						var emp_type = a_results_emp[0].getText('employeetype');
						nlapiLogExecution('audit','emp_type:-- ',emp_type);
						person_type = a_results_emp[0].getText('custentity_persontype');
						emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
						emp_frst_name = a_results_emp[0].getValue('firstname');
						emp_middl_name = a_results_emp[0].getValue('middlename');
						emp_lst_name = a_results_emp[0].getValue('lastname');
						emp_location = a_results_emp[0].getValue('location');
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
											
							if(emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
							if(emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
						if(!_logValidation(emp_type))
						{
							emp_type = '';
						}
						//var person_type = a_results_emp[0].getText('custentity_persontype');
						nlapiLogExecution('audit','person_type:-- ',person_type);
						if(!_logValidation(person_type))
						{
							person_type = '';
						}
						if(!_logValidation(emp_fusion_id))
						{
							emp_fusion_id = '';
						}
						nlapiLogExecution('audit','person_type:-- ',person_type);
						emp_practice = a_results_emp[0].getValue('department');
						if(emp_practice){
										var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
										}
						var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
						if (_logValidation(emp_subsidiary))
						{
							if(emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK')
							{
								onsite_offsite = 'Onsite';
							}
							else{
								onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
							}
							/*else if(emp_subsidiary == 3 || emp_subsidiary == 12)
							{
								onsite_offsite = 'Offsite';
							}else 
							{
								onsite_offsite = 'Onsite';
							}	*/
						}
						if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
						vendor_bill_rcrd.selectLineItem('expense', j);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', employee_with_id);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_type', emp_type);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_person_type', person_type);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_onsite_offsite', onsite_offsite);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_fusion_id);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_emp_name_on_a_click_report', emp_full_name);
						if(existing_location){
						}
						else{
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'location', emp_location);
						}
						if(existing_practice){
						}
						else if(emp_practice && isinactive_Practice_e == 'F'){
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'department', emp_practice);
						}
						vendor_bill_rcrd.commitLineItem('expense');
						submit_record_flag = 1;
}
}

				proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
				var customer_ID = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', j);
				project_entity_id = vendor_bill_rcrd.getLineItemValue('expense','custcol_project_entity_id',j);
				nlapiLogExecution('audit','project_entity_id',project_entity_id);
				if(project_entity_id){
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_desc){
								s_proj_desc_id = proj_desc.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_desc',proj_desc);
							}
				
					if (_logValidation(s_proj_desc_split))
					{
						s_proj_desc_split = s_proj_desc_split.trim();
						var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'contains',s_proj_desc_split);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_search_proj[1] = new nlobjSearchColumn('customer');
						column_search_proj[2] = new nlobjSearchColumn('custentity_region','customer');
						column_search_proj[3] = new nlobjSearchColumn('entityid');
						column_search_proj[4] = new nlobjSearchColumn('altname');
						column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_search_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_search_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_search_proj[9] = new nlobjSearchColumn('territory','customer');
						column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
						
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results))
						{
							var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
							var i_region_id = search_proj_results[0].getValue('custentity_region');
							var i_proj_rcrd = nlapiLoadRecord('job',search_proj_results[0].getId());
							var i_proj_name = i_proj_rcrd.getFieldValue('altname');
							var i_proj_entity_id = search_proj_results[0].getValue('entityid');
							var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
							var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
							var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
							var i_project_service = search_proj_results[0].getValue('custentity_project_services');													
							
								if(i_proj_practice){
										var is_practice_active_p = nlapiLookupField('department',parseInt(i_proj_practice),['isinactive']);
										var isinactive_Practice_p = is_practice_active_p.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_p',isinactive_Practice_p);
										}
							var i_cust_name = search_proj_results[0].getValue('companyname','customer');
							var i_cust_entity_id = search_proj_results[0].getValue('entityid','customer');
							var i_cust_territory = search_proj_results[0].getValue('territory','customer');
							if (!_logValidation(proj_billing_type))
								proj_billing_type = '';
								
							vendor_bill_rcrd.selectLineItem('expense', j);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_billing_type', proj_billing_type);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', s_proj_desc_split);
							
							var proj_cust = search_proj_results[0].getText('customer');
							if (_logValidation(proj_cust))
							{
								var cust_entity_id = proj_cust.split(' ');
								cust_entity_id = cust_entity_id[0];
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_entity_id);
							}
							if(existing_practice || s_employee_name_split){
							}
							else if(i_proj_practice && isinactive_Practice_p == 'F'){
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'department', i_proj_practice);
							}
							if(i_proj_entity_id)
								proj_full_name_with_id = i_proj_entity_id +' '+ i_proj_name;
							
														
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_full_name_with_id);
							
							if(project_entity_id){
							}
							else{							
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', i_proj_entity_id);
							}
							if(existing_territory){
							}
							else{
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', i_cust_territory);
							}
							if(cust_entity_id)
							cust_full_name_with_id = cust_entity_id +' '+ i_cust_name;
							
							var cust_region = search_proj_results[0].getValue('custentity_region','customer');
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_full_name_with_id);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_region_master_setup', cust_region);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_cust_name_on_a_click_report', i_cust_name);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_name_on_a_click_report', i_proj_name);
							//vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_region_master_setup', i_region_id);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_category_on_a_click', i_proj_category);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_transaction_project_services', i_project_service);
							
							vendor_bill_rcrd.commitLineItem('expense');

							submit_record_flag = 1;
}
}
}
	else{
				var emp_type = '';
				var person_type = '';
				var onsite_offsite = '';
				var s_employee_name_split = '';
				var cust_full_name_with_id = '';
				
				var emp_fusion_id = '';
				var emp_frst_name = '';
				var emp_middl_name = '';
				var emp_lst_name = '';
				var emp_full_name = '';
				var emp_practice = '';
				var s_proj_desc_split = '';
				var s_employee_name =  '';
				var s_employee_id = '';
				var s_employee_name_id = '';
				var proj_desc = '';
				var project_entity_id = '';
				var s_proj_desc_id = '';
				var existing_practice = '';
				var proj_full_name_with_id = '';
				var employee_with_id = '';
				var existing_territory = '';
				var emp_location = '';
				var existing_location = ''
				
				
							
				existing_territory = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_territory',j);
				existing_location = vendor_bill_rcrd.getLineItemValue('expense', 'location',j);
				existing_practice = vendor_bill_rcrd.getLineItemValue('expense', 'department',j);
				s_employee_name = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_employeenamecolumn', j);
				s_employee_id = vendor_bill_rcrd.getLineItemValue('expense','custcol_employee_entity_id',j);	
							if(_logValidation(s_employee_id)){
							 s_employee_name_split = s_employee_id;
							 nlapiLogExecution('audit','s_employee_name_split expense',s_employee_name_split);
							}
							else if(_logValidation(s_employee_name)){
							s_employee_name_id = s_employee_name.split('-');
							s_employee_name_split = s_employee_name_id[0];
							 nlapiLogExecution('audit','s_employee_name',s_employee_name);
							}
				if(_logValidation(s_employee_name) || _logValidation(s_employee_id))
				{
					//var s_employee_name_id = emp_name_selected.split('-');
					//var s_employee_name_split = s_employee_name_id[0];
								
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp))
					{
						var emp_id = a_results_emp[0].getId();
						var emp_type = a_results_emp[0].getText('employeetype');
						nlapiLogExecution('audit','emp_type:-- ',emp_type);
						person_type = a_results_emp[0].getText('custentity_persontype');
						emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
						emp_frst_name = a_results_emp[0].getValue('firstname');
						emp_middl_name = a_results_emp[0].getValue('middlename');
						emp_lst_name = a_results_emp[0].getValue('lastname');
						emp_location = a_results_emp[0].getValue('location');
						
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
											
							if(emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
							if(emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
						if(!_logValidation(emp_type))
						{
							emp_type = '';
						}
						//var person_type = a_results_emp[0].getText('custentity_persontype');
						nlapiLogExecution('audit','person_type:-- ',person_type);
						if(!_logValidation(person_type))
						{
							person_type = '';
						}
						if(!_logValidation(emp_fusion_id))
						{
							emp_fusion_id = '';
						}
						nlapiLogExecution('audit','person_type:-- ',person_type);
						emp_practice = a_results_emp[0].getValue('department');
						if(emp_practice){
										var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
										}
						var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
						if (_logValidation(emp_subsidiary))
						{
							if(emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK')
							{
								onsite_offsite = 'Onsite';
							}
							else{
								onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
							}
							/*else if(emp_subsidiary == 3 || emp_subsidiary == 12)
							{
								onsite_offsite = 'Offsite';
							}else 
							{
								onsite_offsite = 'Onsite';
							}*/	
						}
						if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
						vendor_bill_rcrd.selectLineItem('expense', j);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employeenamecolumn', employee_with_id);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_type', emp_type);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_person_type', person_type);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_onsite_offsite', onsite_offsite);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', emp_fusion_id);
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_emp_name_on_a_click_report', emp_full_name);
						if(existing_location){
						}
						else{
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'location', emp_location);
						}
						if(existing_practice){
						}
						else if(emp_practice && isinactive_Practice_e == 'F'){
						vendor_bill_rcrd.setCurrentLineItemValue('expense', 'department', emp_practice);
						}
						vendor_bill_rcrd.commitLineItem('expense');
						submit_record_flag = 1;
}
}

				proj_desc = vendor_bill_rcrd.getLineItemValue('expense', 'custcolprj_name', j);
				var customer_ID = vendor_bill_rcrd.getLineItemValue('expense', 'custcol_customer_entityid', j);
				project_entity_id = vendor_bill_rcrd.getLineItemValue('expense','custcol_project_entity_id',j);
				nlapiLogExecution('audit','project_entity_id',project_entity_id);
				if(project_entity_id){
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_desc){
								s_proj_desc_id = proj_desc.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_desc',proj_desc);
							}
				
					if (_logValidation(s_proj_desc_split))
					{
						s_proj_desc_split = s_proj_desc_split.trim();
						var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'contains',s_proj_desc_split);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_search_proj[1] = new nlobjSearchColumn('customer');
						column_search_proj[2] = new nlobjSearchColumn('custentity_region','customer');
						column_search_proj[3] = new nlobjSearchColumn('entityid');
						column_search_proj[4] = new nlobjSearchColumn('altname');
						column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_search_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_search_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_search_proj[9] = new nlobjSearchColumn('territory','customer');
						column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
						
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results))
						{
							var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
							var i_region_id = search_proj_results[0].getValue('custentity_region');
							var i_proj_rcrd = nlapiLoadRecord('job',search_proj_results[0].getId());
							var i_proj_name = i_proj_rcrd.getFieldValue('altname');
							var i_proj_entity_id = search_proj_results[0].getValue('entityid');
							var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
							var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
							var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
							var i_project_service = search_proj_results[0].getValue('custentity_project_services');													
									
							var i_cust_name = search_proj_results[0].getValue('companyname','customer');
							var i_cust_entity_id = search_proj_results[0].getValue('entityid','customer');
							var i_cust_territory = search_proj_results[0].getValue('territory','customer');
							if (!_logValidation(proj_billing_type))
								proj_billing_type = '';
							if(i_proj_practice){
										var is_practice_active_p = nlapiLookupField('department',parseInt(i_proj_practice),['isinactive']);
										var isinactive_Practice_p = is_practice_active_p.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_p',isinactive_Practice_p);
										}	
							vendor_bill_rcrd.selectLineItem('expense', j);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_billing_type', proj_billing_type);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', s_proj_desc_split);
							
							var proj_cust = search_proj_results[0].getText('customer');
							if (_logValidation(proj_cust))
							{
								var cust_entity_id = proj_cust.split(' ');
								cust_entity_id = cust_entity_id[0];
								vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_entity_id);
							}
							if(existing_practice || s_employee_name_split){
							}
							else if(i_proj_practice && isinactive_Practice_p == 'F'){
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'department', i_proj_practice);
							}
							if(i_proj_entity_id)
								proj_full_name_with_id = i_proj_entity_id +' '+ i_proj_name;
														
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_full_name_with_id);
							
							if(project_entity_id){
							}
							else{							
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', i_proj_entity_id);
							}
							if(existing_territory){
							}
							else{
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', i_cust_territory);
							}
							if(cust_entity_id)
							cust_full_name_with_id = cust_entity_id +' '+ i_cust_name;
							
							var cust_region = search_proj_results[0].getValue('custentity_region','customer');
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_full_name_with_id);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_region_master_setup', cust_region);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_cust_name_on_a_click_report', i_cust_name);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_name_on_a_click_report', i_proj_name);
							//vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_region_master_setup', i_region_id);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_category_on_a_click', i_proj_category);
							//vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', i_cust_territory);
							vendor_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_transaction_project_services', i_project_service);
							vendor_bill_rcrd.commitLineItem('expense');

							submit_record_flag = 1;
}
}
}
} //End of For - loop
//For item Sublist
var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
var item_gst_not_credit_flag = false;
//Logic for expense out
				/*for (var jv = 1; jv <= item_line_count; jv++)
				{
				var st_notcredit = vendor_bill_rcrd.getLineItemValue('item', 'custcol_st_notcredit', jv); //custcol_st_notcredit
				if(st_notcredit == 'T')
				item_gst_not_credit_flag = true;
				//nlapiLogExecution('audit', 'item_gst_not_credit_flag:- ', item_gst_not_credit_flag);
				}*/
				for (var k = 1; k <= item_line_count; k++)
				{
					var is_swach_cess_reverse_true_mainLine = vendor_bill_rcrd.getLineItemValue('item', 'custcol_ait_swachh_cess_reverse', k);
					var i_subsidiary = nlapiGetFieldValue('subsidiary');
					
					var i_account_ = vendor_bill_rcrd.getLineItemValue('item', 'account', k); //custcol_st_notcredit
						
						
						
				/*	if (parseInt(i_subsidiary) == parseInt(3) && parseInt(i_currency) != parseInt(1))
					{
						var amount_round_off_ = vendor_bill_rcrd.getLineItemValue('item', 'grossamt', k);
						if(item_gst_not_credit_flag == true && (i_account_ == 1134 || i_account_ == 1135 || i_account_ ==  1136 )){
						nlapiLogExecution('audit', 'gst_not_credit_flag amount_round_off:- ', amount_round_off_);
						}
						else{
						//var amount_round_off_ = vendor_bill_rcrd.getLineItemValue('item', 'amount', k);
						//amount_round_off = Math.round(amount_round_off);
						amount_round_off_ = Math.round(amount_round_off_);
						nlapiLogExecution('audit', 'Item main amount rounded:- ', amount_round_off_);
						//vendor_bill_rcrd.setLineItemValue('item', 'amount', k, amount_round_off_);
						vendor_bill_rcrd.setLineItemValue('item', 'grossamt', k, amount_round_off_);
						submit_record_flag == 1;
						}
						/*var amount_round_off = vendor_bill_rcrd.getLineItemValue('expense', 'grossamt', j);
						amount_round_off = Math.round(amount_round_off);
						nlapiLogExecution('audit', 'main amount rounded:- ', amount_round_off);
						vendor_bill_rcrd.setLineItemValue('expense', 'grossamt', j, amount_round_off);
						submit_record_flag == 1; 
					} */
					
				if(is_swach_cess_reverse_true_mainLine == 'T'){
				var emp_type = '';
				var person_type = '';
				var onsite_offsite = '';
				var s_employee_name_split = '';
				var cust_full_name_with_id = '';
				
				var emp_fusion_id = '';
				var emp_frst_name = '';
				var emp_middl_name = '';
				var emp_lst_name = '';
				var emp_full_name = '';
				var emp_practice = '';
				var s_proj_desc_split = '';
				var s_employee_name =  '';
				var s_employee_id = '';
				var s_employee_name_id = '';
				var proj_desc = '';
				var project_entity_id = '';
				var s_proj_desc_id = '';
				var existing_practice = '';
				var proj_full_name_with_id = '';
				var employee_with_id = '';
				var existing_territory = '';
				var emp_location = '';
				var existing_location = '';
							
				existing_territory = vendor_bill_rcrd.getLineItemValue('item', 'custcol_territory',k);
				existing_location = vendor_bill_rcrd.getLineItemValue('item', 'location',k);
				existing_practice = vendor_bill_rcrd.getLineItemValue('item', 'department',k);
				s_employee_name = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', k);
				s_employee_id = vendor_bill_rcrd.getLineItemValue('item','custcol_employee_entity_id',k);	
							if(_logValidation(s_employee_id)){
							 s_employee_name_split = s_employee_id;
							 nlapiLogExecution('audit','s_employee_name_split expense',s_employee_name_split);
							}
							else if(_logValidation(s_employee_name)){
							s_employee_name_id = s_employee_name.split('-');
							s_employee_name_split = s_employee_name_id[0];
							 nlapiLogExecution('audit','s_employee_name',s_employee_name);
							}
				if(_logValidation(s_employee_name) || _logValidation(s_employee_id))
				{
					//var s_employee_name_id = emp_name_selected.split('-');
					//var s_employee_name_split = s_employee_name_id[0];
								
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp))
					{
						var emp_id = a_results_emp[0].getId();
						var emp_type = a_results_emp[0].getText('employeetype');
						nlapiLogExecution('audit','emp_type:-- ',emp_type);
						person_type = a_results_emp[0].getText('custentity_persontype');
						emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
						emp_frst_name = a_results_emp[0].getValue('firstname');
						emp_middl_name = a_results_emp[0].getValue('middlename');
						emp_lst_name = a_results_emp[0].getValue('lastname');
						emp_location = a_results_emp[0].getValue('location');
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
											
							if(emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
							if(emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
						if(!_logValidation(emp_type))
						{
							emp_type = '';
						}
						//var person_type = a_results_emp[0].getText('custentity_persontype');
						nlapiLogExecution('audit','person_type:-- ',person_type);
						if(!_logValidation(person_type))
						{
							person_type = '';
						}
						if(!_logValidation(emp_fusion_id))
						{
							emp_fusion_id = '';
						}
						nlapiLogExecution('audit','person_type:-- ',person_type);
						emp_practice = a_results_emp[0].getValue('department');
						
						if(emp_practice){
										var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
										}
										
						var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
						if (_logValidation(emp_subsidiary))
						{
							if(emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK')
							{
								onsite_offsite = 'Onsite';
							}
							else{
								onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
							}
							/*else if(emp_subsidiary == 3 || emp_subsidiary == 12)
							{
								onsite_offsite = 'Offsite';
							}else 
							{
								onsite_offsite = 'Onsite';
							}	*/
						}
						if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
						vendor_bill_rcrd.selectLineItem('item', k);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employeenamecolumn', employee_with_id);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_type', emp_type);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_person_type', person_type);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_onsite_offsite', onsite_offsite);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_entity_id', emp_fusion_id);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', emp_full_name);
						if(existing_location){
						}
						else{
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'location', emp_location);
						}
						if(existing_practice){
						}
						else if(isinactive_Practice_e =='F' && emp_practice){
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', emp_practice);
						}
						vendor_bill_rcrd.commitLineItem('item');
						submit_record_flag = 1;
}
}

				proj_desc = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', k);
				var customer_ID = vendor_bill_rcrd.getLineItemValue('item', 'custcol_customer_entityid', k);
				project_entity_id = vendor_bill_rcrd.getLineItemValue('item','custcol_project_entity_id',k);
				nlapiLogExecution('audit','project_entity_id',project_entity_id);
				if(project_entity_id){
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_desc){
								s_proj_desc_id = proj_desc.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_desc',proj_desc);
							}
				
					if (_logValidation(s_proj_desc_split))
					{
						s_proj_desc_split = s_proj_desc_split.trim();
						var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'contains',s_proj_desc_split);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_search_proj[1] = new nlobjSearchColumn('customer');
						column_search_proj[2] = new nlobjSearchColumn('custentity_region','customer');
						column_search_proj[3] = new nlobjSearchColumn('entityid');
						column_search_proj[4] = new nlobjSearchColumn('altname');
						column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_search_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_search_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_search_proj[9] = new nlobjSearchColumn('territory','customer');
						column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
						
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results))
						{
							var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
							var i_region_id = search_proj_results[0].getValue('custentity_region');
							var i_proj_rcrd = nlapiLoadRecord('job',search_proj_results[0].getId());
							var i_proj_name = i_proj_rcrd.getFieldValue('altname');
							var i_proj_entity_id = search_proj_results[0].getValue('entityid');
							var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
							var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
							var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
							var i_project_service = search_proj_results[0].getValue('custentity_project_services');		

								if(i_proj_practice){
										var is_practice_active_p = nlapiLookupField('department',parseInt(i_proj_practice),['isinactive']);
										var isinactive_Practice_p = is_practice_active_p.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_p',isinactive_Practice_p);
										}							
									
							var i_cust_name = search_proj_results[0].getValue('companyname','customer');
							var i_cust_entity_id = search_proj_results[0].getValue('entityid','customer');
							var i_cust_territory = search_proj_results[0].getValue('territory','customer');
							if (!_logValidation(proj_billing_type))
								proj_billing_type = '';
								
							vendor_bill_rcrd.selectLineItem('item', k);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_billing_type', proj_billing_type);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_proj_desc_split);
							
							var proj_cust = search_proj_results[0].getText('customer');
							if (_logValidation(proj_cust))
							{
								var cust_entity_id = proj_cust.split(' ');
								cust_entity_id = cust_entity_id[0];
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_customer_entityid', cust_entity_id);
							}
							if(existing_practice || s_employee_name_split){
							}
							else if(isinactive_Practice_p == 'F' && i_proj_practice){
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', i_proj_practice);
							}
							if(i_proj_entity_id)
								proj_full_name_with_id = i_proj_entity_id +' '+ i_proj_name;
														
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolprj_name', proj_full_name_with_id);
							
							if(project_entity_id){
							}
							else{							
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', i_proj_entity_id);
							}
							if(existing_territory){
							}
							else{
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
							}
							if(cust_entity_id)
							cust_full_name_with_id = cust_entity_id +' '+ i_cust_name;
							
							var cust_region = search_proj_results[0].getValue('custentity_region','customer');
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer', cust_full_name_with_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', cust_region);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', i_cust_name);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', i_proj_name);
							//vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', i_region_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', i_proj_category);
							//vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_transaction_project_services', i_project_service);
							vendor_bill_rcrd.commitLineItem('item');

							submit_record_flag = 1;
}
}
}
	else{
				var emp_type = '';
				var person_type = '';
				var onsite_offsite = '';
				var s_employee_name_split = '';
				var cust_full_name_with_id = '';
				
				var emp_fusion_id = '';
				var emp_frst_name = '';
				var emp_middl_name = '';
				var emp_lst_name = '';
				var emp_full_name = '';
				var emp_practice = '';
				var s_proj_desc_split = '';
				var s_employee_name =  '';
				var s_employee_id = '';
				var s_employee_name_id = '';
				var proj_desc = '';
				var project_entity_id = '';
				var s_proj_desc_id = '';
				var existing_practice = '';
				var proj_full_name_with_id = '';
				var employee_with_id = '';
				var existing_territory = '';
				var emp_location = '';
				var existing_location = '';
							
				existing_territory = vendor_bill_rcrd.getLineItemValue('item', 'custcol_territory',k);
				existing_location = vendor_bill_rcrd.getLineItemValue('item', 'location',k);
				existing_practice = vendor_bill_rcrd.getLineItemValue('item', 'department',k);
				s_employee_name = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', k);
				s_employee_id = vendor_bill_rcrd.getLineItemValue('item','custcol_employee_entity_id',k);	
							if(_logValidation(s_employee_id)){
							 s_employee_name_split = s_employee_id;
							 nlapiLogExecution('audit','s_employee_name_split expense',s_employee_name_split);
							}
							else if(_logValidation(s_employee_name)){
							s_employee_name_id = s_employee_name.split('-');
							s_employee_name_split = s_employee_name_id[0];
							 nlapiLogExecution('audit','s_employee_name',s_employee_name);
							}
				if(_logValidation(s_employee_name) || _logValidation(s_employee_id))
				{
					//var s_employee_name_id = emp_name_selected.split('-');
					//var s_employee_name_split = s_employee_name_id[0];
								
					var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('custentity_persontype');
					column_emp[1] = new nlobjSearchColumn('employeetype');
					column_emp[2] = new nlobjSearchColumn('subsidiary');
					column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
					column_emp[4] = new nlobjSearchColumn('firstname');
					column_emp[5] = new nlobjSearchColumn('middlename');
					column_emp[6] = new nlobjSearchColumn('lastname');
					column_emp[7] = new nlobjSearchColumn('department');
					column_emp[8] = new nlobjSearchColumn('location');
					column_emp[9] = new nlobjSearchColumn('custentity_legal_entity_fusion');
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp))
					{
						var emp_id = a_results_emp[0].getId();
						var emp_type = a_results_emp[0].getText('employeetype');
						nlapiLogExecution('audit','emp_type:-- ',emp_type);
						person_type = a_results_emp[0].getText('custentity_persontype');
						emp_fusion_id = a_results_emp[0].getValue('custentity_fusion_empid');
						emp_frst_name = a_results_emp[0].getValue('firstname');
						emp_middl_name = a_results_emp[0].getValue('middlename');
						emp_lst_name = a_results_emp[0].getValue('lastname');
						emp_location = a_results_emp[0].getValue('location');
						if(emp_frst_name)
							emp_full_name = emp_frst_name;
											
							if(emp_middl_name)
								emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
							if(emp_lst_name)
								emp_full_name = emp_full_name + ' ' + emp_lst_name;
						if(!_logValidation(emp_type))
						{
							emp_type = '';
						}
						//var person_type = a_results_emp[0].getText('custentity_persontype');
						nlapiLogExecution('audit','person_type:-- ',person_type);
						if(!_logValidation(person_type))
						{
							person_type = '';
						}
						if(!_logValidation(emp_fusion_id))
						{
							emp_fusion_id = '';
						}
						nlapiLogExecution('audit','person_type:-- ',person_type);
						emp_practice = a_results_emp[0].getValue('department');
						
						if(emp_practice){
										var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
										}
										
						var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
						if (_logValidation(emp_subsidiary))
						{
							if(emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK')
							{
								onsite_offsite = 'Onsite';
							}
							else{
								onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
							}
							/*else if(emp_subsidiary == 3 || emp_subsidiary == 12)
							{
								onsite_offsite = 'Offsite';
							}else 
							{
								onsite_offsite = 'Onsite';
							}*/	
						}
						if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
						vendor_bill_rcrd.selectLineItem('item', k);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employeenamecolumn', employee_with_id);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_type', emp_type);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_person_type', person_type);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_onsite_offsite', onsite_offsite);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_employee_entity_id', emp_fusion_id);
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report', emp_full_name);
						if(existing_location){
						}
						else{
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'location', emp_location);
						}
						if(existing_practice){
						}
						else if(is_practice_active_e == 'F' && emp_practice){
						vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', emp_practice);
						}
						vendor_bill_rcrd.commitLineItem('item');
						submit_record_flag = 1;
}
}

				proj_desc = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', k);
				var customer_ID = vendor_bill_rcrd.getLineItemValue('item', 'custcol_customer_entityid', k);
				project_entity_id = vendor_bill_rcrd.getLineItemValue('item','custcol_project_entity_id',k);
				nlapiLogExecution('audit','project_entity_id',project_entity_id);
				nlapiLogExecution('audit','proj_desc',proj_desc);
				if(project_entity_id){
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_desc){
								s_proj_desc_id = proj_desc.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_desc',proj_desc);
							}
				
					if (_logValidation(s_proj_desc_split))
					{
						s_proj_desc_split = s_proj_desc_split.trim();
						var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'contains',s_proj_desc_split);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
						column_search_proj[1] = new nlobjSearchColumn('customer');
						column_search_proj[2] = new nlobjSearchColumn('custentity_region','customer');
						column_search_proj[3] = new nlobjSearchColumn('entityid');
						column_search_proj[4] = new nlobjSearchColumn('altname');
						column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
						column_search_proj[6] = new nlobjSearchColumn('companyname','customer');
						column_search_proj[7] = new nlobjSearchColumn('entityid','customer');
						column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
						column_search_proj[9] = new nlobjSearchColumn('territory','customer');
						column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
						column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
						
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results))
						{
							var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
							var i_region_id = search_proj_results[0].getValue('custentity_region');
							var i_proj_rcrd = nlapiLoadRecord('job',search_proj_results[0].getId());
							var i_proj_name = i_proj_rcrd.getFieldValue('altname');
							var i_proj_entity_id = search_proj_results[0].getValue('entityid');
							var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
							var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
							var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
							var i_project_service = search_proj_results[0].getValue('custentity_project_services');													
									
							var i_cust_name = search_proj_results[0].getValue('companyname','customer');
							var i_cust_entity_id = search_proj_results[0].getValue('entityid','customer');
							var i_cust_territory = search_proj_results[0].getValue('territory','customer');
							
							if(i_proj_practice){
										var is_practice_active_p = nlapiLookupField('department',parseInt(i_proj_practice),['isinactive']);
										var isinactive_Practice_p = is_practice_active_p.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_p',isinactive_Practice_p);
										}
										
										
							if (!_logValidation(proj_billing_type))
								proj_billing_type = '';
								
							vendor_bill_rcrd.selectLineItem('item', k);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_billing_type', proj_billing_type);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', s_proj_desc_split);
							
							var proj_cust = search_proj_results[0].getText('customer');
							if (_logValidation(proj_cust))
							{
								var cust_entity_id = proj_cust.split(' ');
								cust_entity_id = cust_entity_id[0];
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_customer_entityid', cust_entity_id);
							}
							if(existing_practice || s_employee_name_split){
							}
							else if(is_practice_active_p == 'F' && i_proj_practice){
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'department', i_proj_practice);
							}
							if(i_proj_entity_id)
								proj_full_name_with_id = i_proj_entity_id +' '+ i_proj_name;
							
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolprj_name', proj_full_name_with_id);
							
														
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_project_entity_id', i_proj_entity_id);
							if(existing_territory){
								}
								else{
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
								}
							
							if(cust_entity_id)
							cust_full_name_with_id = cust_entity_id +' '+ i_cust_name;
							
							var cust_region = search_proj_results[0].getValue('custentity_region','customer');
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcolcustcol_temp_customer', cust_full_name_with_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', cust_region);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report', i_cust_name);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report', i_proj_name);
							//vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_region_master_setup', i_region_id);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', i_proj_category);
							//vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_territory', i_cust_territory);
							vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_transaction_project_services', i_project_service);
							vendor_bill_rcrd.commitLineItem('item');

							submit_record_flag = 1;
}
}
}
					
} //End of For - loop
//if(submit_record_flag == 1)
{
	var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd,true,true);
				nlapiLogExecution('debug','submitted bill id:-- ',vendor_bill_submitted_id);
}
} catch (err){
	//Added try-catch logic by Sitaram 06/08/2021
	nlapiLogExecution('audit','err',err);
	err_row_excel += '	<tr>';
	err_row_excel += ' <td width="6%" font-size="11" align="center">' + nlapiGetRecordType() + '</td>';
	err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum  + '</td>';
	err_row_excel += ' <td width="6%" font-size="11" align="center">' + nlapiGetRecordId() + '</td>';
	err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';
	
	err_row_excel += '	</tr>';
					 
	
					
	}
}//if completed

if(_logValidation(err_row_excel)){
				var tailMail = '';
				tailMail += '</table>';
				tailMail += ' </td>';
				tailMail += '</tr>';
				tailMail += '</table>';
				
				strVar_excel = strVar_excel+err_row_excel+tailMail
				//excel_file_obj = generate_excel(strVar_excel);
				var mailTemplate = "";
                mailTemplate += '<html>';
                mailTemplate += '<body>';
				mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";
				mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";
				mailTemplate += "<br/>"
				mailTemplate += "<p><b> Script Id:- "+context.getScriptId() +"</b></p>";
                mailTemplate += "<p><b> Script Deployment Id:- "+context.getDeploymentId() +"</b></p>";
                mailTemplate += "<br/>"
                mailTemplate += strVar_excel
                mailTemplate += "<br/>"
				mailTemplate += "<p>Regards, <br/> Information Systems</p>";
                mailTemplate += '</body>';
                mailTemplate += '</html>';
			//	nlapiSendEmail(442, 'ap@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);
				
				}



}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}