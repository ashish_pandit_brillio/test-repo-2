/**
 * Action script to push the payment details to the custom record for income
 * statement
 * 
 * Version Date Author Remarks 1.00 08 Sep 2015 Nitish Mishra
 * 
 */

function workflowAction() {

	try {
		var paymentRecord = nlapiLoadRecord('customerpayment',
		        nlapiGetRecordId());

		var commonPaymentDetails = new MonthlyIncomeStatement();
		commonPaymentDetails.TypeId = 'customerpayment';
		commonPaymentDetails.TypeName = 'Payment';
		commonPaymentDetails.TranId = paymentRecord.getId();
		commonPaymentDetails.Date = paymentRecord.getFieldValue('trandate');
		commonPaymentDetails.Number = paymentRecord.getFieldValue('tranid');
		commonPaymentDetails.Location = paymentRecord.getFieldValue('location');
		commonPaymentDetails.TotalAmount = paymentRecord
		        .getFieldValue('payment');
		commonPaymentDetails.TotalDiscount = 0;

		var linecount = paymentRecord.getLineItemCount('apply');

		// get the overall discount applied amount for the payment
		for (var linenum = 1; linenum <= linecount; linenum++) {

			if (paymentRecord.getLineItemValue('apply', 'apply', linenum) == 'T') {
				var discountAmount = paymentRecord.getLineItemValue('apply',
				        'disc', linenum);

				if (discountAmount) {
					commonPaymentDetails.TotalDiscount += parseFloat(discountAmount);
				}
			}
		}

		for (var linenum = 1; linenum <= linecount; linenum++) {

			if (paymentRecord.getLineItemValue('apply', 'apply', linenum) == 'T') {
				var invoiceId = paymentRecord.getLineItemValue('apply',
				        'internalid', linenum);
				var invoiceDiscountApplied = parseFloat(paymentRecord
				        .getLineItemValue('apply', 'disc', linenum));
				var invoiceAmountApplied = parseFloat(paymentRecord
				        .getLineItemValue('apply', 'amount', linenum));

				if (invoiceDiscountApplied) {
					var employeeWiseDetails = getDiscountBreakDownDetails(
					        invoiceId, invoiceDiscountApplied,
					        invoiceAmountApplied);

					for (var i = 0; i < employeeWiseDetails.length; i++) {
						employeeWiseDetails[i].Account = '515';
						employeeWiseDetails[i].TypeId = 'customerpayment';
						employeeWiseDetails[i].TypeName = 'Payment';
						employeeWiseDetails[i].TranId = paymentRecord.getId();
						employeeWiseDetails[i].Date = paymentRecord
						        .getFieldValue('trandate');
						employeeWiseDetails[i].Number = paymentRecord
						        .getFieldValue('tranid');
						employeeWiseDetails[i].Location = paymentRecord
						        .getFieldValue('location');
						employeeWiseDetails[i].TotalAmount = paymentRecord
						        .getFieldValue('payment');
						employeeWiseDetails[i].TotalDiscount = commonPaymentDetails.TotalDiscount;
						// employeeWiseDetails[i].AmountApplied =
						// invoiceAmountApplied;

						pushToIncomeStatement(employeeWiseDetails[i]);
					}
				}
			}
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'workflowAction', err);
	}
}

function getPaymentSourcingData(invoiceId) {
	try {
		var invoiceRecord = nlapiLoadRecord('invoice', invoiceId);

		// get the vertical from the customer
		var customerId = invoiceRecord.getFieldValue('entity');
		var vertical = nlapiLookupField('customer', customerId,
		        'custentity_vertical');

		// check the billing type of the project
		var projectId = invoiceRecord.getFieldValue('job');
		var projectBillingType = nlapiLookupField('job', projectId,
		        'jobbillingtype');
		var practice = null;

		// get the employees in the invoice
		var employeeList = [];

		var timeCardCount = invoiceRecord.getLineItemCount('time');
		for (var linenum = 1; linenum <= timeCardCount; linenum++) {

			if (invoiceRecord.getLineItemValue('time', 'apply', linenum) == 'T') {
				employeeList.push(invoiceRecord.getLineItemValue('time',
				        'employee', linenum));

				// if more than one employee, then break - take the project
				// practice
				employeeList = _.uniq(employeeList);
				if (employeeList.length > 1) {
					break;
				}
			}
		}

		var employeeId = null;

		// if one employee only, set it in the employee field
		if (employeeList.length == 1) {
			employeeId = employeeList[0];

			// if T&M and 1 employee, use the employee practice
			if (projectBillingType == 'TM') {
				practice = nlapiLookupField('employee', employeeId,
				        'department');
			}
		}

		// if no practice, use the project practice
		// for FBM and T&M multiple employee
		if (isEmpty(practice)) {
			practice = nlapiLookupField('job', projectId, 'custentity_practice');
		}

		return {
		    Vertical : vertical,
		    Practice : practice,
		    Employee : employeeId,
		    Customer : customerId,
		    Project : projectId,
		    Number : invoiceRecord.getFieldValue('tranid')
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPaymentSourcingData', err);
		throw err;
	}
}

function pushToIncomeStatement(input) {

	try {
		nlapiLogExecution('debug', 'input', JSON.stringify(input));

		// create a new custom monthly income statement record
		var incomeStatement = nlapiCreateRecord(K_RECORDS.MonthlyIncomeStatement);

		// set the values
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Account,
		        input.Account);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.TypeId,
		        input.TypeId);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.TypeName,
		        input.TypeName);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.TranId,
		        input.TranId);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Date,
		        input.Date);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Number,
		        input.Number);

		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Amount,
		        input.Amount);

		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Customer,
		        input.Customer);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Project,
		        input.Project);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Employee,
		        input.Employee);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Practice,
		        input.Practice);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Vertical,
		        input.Vertical);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Location,
		        input.Location);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.Description,
		        input.Description);

		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.AmountApplied,
		        input.InvoiceAppliedAmount);
		incomeStatement.setFieldValue(
		        K_MONTHLY_INCOME_STATEMENT.DiscountApplied,
		        input.InvoiceDiscountAmount);

		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.TotalAmount,
		        input.TotalAmount);
		incomeStatement.setFieldValue(K_MONTHLY_INCOME_STATEMENT.TotalDiscount,
		        input.TotalDiscount);

		// submit the record
		var id = nlapiSubmitRecord(incomeStatement);
		nlapiLogExecution('DEBUG', 'MIS Created', id);
	} catch (err) {
		nlapiLogExecution('ERROR', 'pushToIncomeStatement', err);
	}
}

function getDiscountBreakDownDetails(invoiceId, netDiscountApplied,
        netInvoiceAmountApplied)
{
	try {
		var invoiceRecord = nlapiLoadRecord('invoice', invoiceId);

		// get the vertical from the customer
		var customerId = invoiceRecord.getFieldValue('entity');
		var vertical = nlapiLookupField('customer', customerId,
		        'custentity_vertical');

		// check the billing type of the project
		var projectId = invoiceRecord.getFieldValue('job');
		var projectBillingType = nlapiLookupField('job', projectId,
		        'jobbillingtype');
		nlapiLogExecution('debug', 'Billing Type', projectBillingType);

		var employeeMainList = [];
		var totalInvoiceAmount = 0;

		// if T&M
		if (projectBillingType == "TM") {

			// get the employees in the invoice
			var employeeList = [];
			var timeCardCount = invoiceRecord.getLineItemCount('time');

			for (var linenum = 1; linenum <= timeCardCount; linenum++) {

				if (invoiceRecord.getLineItemValue('time', 'apply', linenum) == 'T') {
					var employee = invoiceRecord.getLineItemValue('time',
					        'employee', linenum);
					var amount = invoiceRecord.getLineItemValue('time',
					        'amount', linenum);
					totalInvoiceAmount += parseFloat(amount);

					var practice = invoiceRecord.getLineItemValue('time',
					        'department', linenum);
					var empIndex = _.indexOf(employeeList, employee);

					// check if employee already added to the list
					if (empIndex == -1) {
						employeeList.push(employee);
						employeeMainList.push({
						    Employee : employee,
						    Practice : practice,
						    Amount_Temp : parseFloat(amount)
						});
					} else {
						employeeMainList[empIndex].Amount_Temp += parseFloat(amount);
					}
				}
			}

			nlapiLogExecution('debug', 'No. of employees in invoice',
			        employeeMainList.length);
			nlapiLogExecution('debug', 'Employee Details', JSON
			        .stringify(employeeMainList));
			nlapiLogExecution('debug', 'Employee Details '
			        + employeeMainList[0].Employee,
			        employeeMainList[0].Amount_Temp);
		} else {
			totalInvoiceAmount = 1;

			employeeMainList.push({
			    Employee : '',
			    Practice : nlapiLookupField('job', projectId,
			            'custentity_practice'),
			    Amount_Temp : 1
			});
		}

		// Proportionate the discount amongst the employees in the invoice
		for (var i = 0; i < employeeMainList.length; i++) {
			employeeMainList[i].Amount = (employeeMainList[i].Amount_Temp / totalInvoiceAmount)
			        * netDiscountApplied;
			employeeMainList[i].InvoiceAppliedAmount = netInvoiceAmountApplied;
			employeeMainList[i].InvoiceDiscountAmount = netDiscountApplied;
			employeeMainList[i].Customer = customerId;
			employeeMainList[i].Vertical = vertical;
			employeeMainList[i].Project = projectId;
			employeeMainList[i].Description = "Invoice #"
			        + invoiceRecord.getFieldValue('tranid');
		}

		return employeeMainList;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getDiscountBreakDownDetails', err);
		throw err;
	}
}
