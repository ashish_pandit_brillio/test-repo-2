/**
 * @author Jayesh
 */

function afterSubmit_FP_RevRec(type)
{
	try
	{  //Email trigger to Finance Team to update the value for Rev Rec Type
		if(type == 'create' || 'edit')
		{
			var i_pro_id = nlapiGetRecordId();
			var o_project = nlapiLoadRecord('job',parseInt(i_pro_id));
			var s_pm = o_project.getFieldValue('custentity_projectmanager');
			var s_proj_id = o_project.getFieldValue('entityid');
			var s_proj_name = o_project.getFieldValue('companyname');
			var s_billing_type = o_project.getFieldValue('jobbillingtype');
			
			var s_old_rev_rec_type ='';
			if(type != 'create')
			{
				var o_rev_rec_oldRec  = nlapiGetOldRecord(); //Get Old Record rev rec Value
				s_old_rev_rec_type = o_rev_rec_oldRec.getFieldValue('custentity_fp_rev_rec_type');
				nlapiLogExecution('DEBUG','jobbillingtype',s_billing_type);
			}
			
			var s_rev_rec_type = o_project.getFieldValue('custentity_fp_rev_rec_type');
			if(s_rev_rec_type)
			{
				var s_project_region = o_project.getFieldText('custentity_region');
				var s_project_subsidiary = o_project.getFieldText('subsidiary');
				var s_project_cust = o_project.getFieldText('parent');
				var s_project_strt = o_project.getFieldValue('startdate');
				var s_project_end = o_project.getFieldValue('enddate');
				var s_project_prac = o_project.getFieldText('custentity_practice');
				
				var pm_email = '';
				var cp_email = '';
				var dm_email = '';
				var cust_cp_email = '';
				var s_cust_cp ='';
				var s_customer = o_project.getFieldValue('parent');
				if(s_customer){
					var customer_look_up = nlapiLookupField('customer',parseInt(s_customer),['custentity_clientpartner']);
					//var s_cust_cp = customer_look_up.custentity_clientpartner;
				   s_cust_cp = customer_look_up.custentity_clientpartner;
				}
				if(s_cust_cp){
					var emp_lookup_cust_cp = nlapiLookupField('employee',parseInt(s_cust_cp),['firstname','email'])
					var cust_cp_firstname = emp_lookup_cust_cp.firstname;
					cust_cp_email = emp_lookup_cust_cp.email;
				}
			
				var s_pm = nlapiGetFieldValue('custentity_projectmanager');
				var s_cp = nlapiGetFieldValue('custentity_clientpartner');
				var s_dm = nlapiGetFieldValue('custentity_deliverymanager');
			
				if(s_pm){
				var emp_lookup = nlapiLookupField('employee',parseInt(s_pm),['firstname','email'])
				var pm_firstname = emp_lookup.firstname;
				pm_email = emp_lookup.email;
				nlapiLogExecution('DEBUG','PROJ MANAGER EMAIL',pm_email);
				}
				if(s_cp){
					var emp_lookup_s_cp = nlapiLookupField('employee',parseInt(s_cp),['firstname','email'])
					var cp_firstname = emp_lookup_s_cp.firstname;
					cp_email = emp_lookup_s_cp.email;
				}
				if(s_dm){
					var emp_lookup_s_dm = nlapiLookupField('employee',parseInt(s_dm),['firstname','email'])
					var dm_firstname = emp_lookup_s_dm.firstname;
					dm_email = emp_lookup_s_dm.email;
				}
				
              
              //Added by Sitaram 02-02-2021 for DI practise mail trigger
              if(type == 'create'){
                 var di_department = nlapiGetContext().getSetting('SCRIPT', 'custscript_project_department')
                 //nlapiLogExecution('DEBUG','parameter_department',di_department);
                 var s_project_prac_val = o_project.getFieldValue('custentity_practice');
             // s_project_prac_val: 588
               if(s_project_prac_val== di_department){
                 nlapiLogExecution('audit','s_proj_name',s_proj_name+ ", "+pm_email+", "+dm_email);
	             DIDEMailTrigger(s_proj_name,pm_email,dm_email,s_proj_id,"job","")
                 nlapiLogExecution('audit','Mail sent for DI practise project creation');
              }
                
              }
              
              
              
				
				if(s_billing_type == 'FBM' && s_old_rev_rec_type != 1 && s_rev_rec_type == 1)
                // if(s_billing_type == 'FBM' &&  s_rev_rec_type == 1)
				{ //For FP Milestone
					var strVar = '';
					strVar += '<html>';
					strVar += '<body>';
					
					strVar += '<p>Hello PM,</p>';
					strVar += '<p>The Fixed price project with following details has been created.</p>';
					strVar += '<p>Please complete and submit the Revenue share and Effort Budget for the project immediately for review and approval. Final approval from Practice head will be required after submission.</p>';
		
					strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1138&deploy=1&proj_id='+nlapiGetRecordId()+'>Link to Project setup page in Netsuite</a>';
					
					strVar += '<p>Region:- '+s_project_region+'<br>';
					strVar += 'Customer:- '+s_project_cust+'<br>';
					strVar += 'Project:- '+s_proj_name+'<br>';
					strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
					strVar += 'Executing Practice:- '+s_project_prac+'</p>';
					
					strVar += '<p>Regards,</p>';
					strVar += '<p>Business Ops team</p>';
						
					strVar += '</body>';
					strVar += '</html>';
					
					var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = '39108';
					
					var a_RECIPIENT = new Array();
					a_RECIPIENT[0]= pm_email;
					a_RECIPIENT[1]= cp_email;
					a_RECIPIENT[2]= dm_email;
					a_RECIPIENT[3]= cust_cp_email;
					
					var a_group_mail_id = new Array();
					a_group_mail_id.push('billing@brillio.com');
					a_group_mail_id.push('team.fpa@brillio.com');
					
					if( s_project_subsidiary != 'Comity Designs, Inc.')
					{
					nlapiSendEmail(442, a_RECIPIENT, 'FP Project Created: '+s_proj_name, strVar,a_group_mail_id,'sai.vannamareddy@brillio.com',a_emp_attachment);
					nlapiLogExecution('audit','mail sent for project creation');
					}
				}
					
				//Added for FP Others Trigger
				if ((s_billing_type == 'FBM')&&  (s_old_rev_rec_type!=4) &&(s_rev_rec_type==4 ||s_rev_rec_type == 2 ))
                //  if ((s_billing_type == 'FBM')&& (s_rev_rec_type==4))
				{ //For FP Others
					var strVar = '';
					strVar += '<html>';
					strVar += '<body>';
					
					strVar += '<p>Hello PM/DM ,</p>';
					strVar += '<p>The Fixed price project with following details has been created.</p>';
					
					strVar += '<p>Kindly update the Monthly Revenue and Effort Plan for the project using link given below.</p>';
					strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1514&deploy=1&proj_id='+nlapiGetRecordId()+'&mode=Create&existing_rcrd_id=0&revenue_share_status>Link To Project SetUp Page</a>';
					//strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1152&deploy=1&proj_id='+nlapiGetRecordId()+'&mode=Create&existing_rcrd_id=0&revenue_share_status>Project Setup Page</a>';
				
					strVar += '<p>Project:- '+s_proj_id+'-'+s_proj_name+'<br>';
					strVar += 'Customer:- '+s_project_cust+'<br>';					
					strVar += 'Project Start :'+s_project_strt+' and End Date :'+s_project_end+'<br>';
					strVar += 'Executing Practice:- '+s_project_prac+'<br>';
					strVar += 'Region:- '+s_project_region+'</p>';
					strVar += '<p>Thanks & Regards,</p>';
					strVar += '<p>Team IS</p>';
						
					strVar += '</body>';
					strVar += '</html>';
					
					var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = '41571';
					
					var a_RECIPIENT = new Array();
					a_RECIPIENT[0]= pm_email;
					a_RECIPIENT[1]= cp_email;
					a_RECIPIENT[2]= dm_email;
					a_RECIPIENT[3]= cust_cp_email;
					var a_group_mail_id = new Array();
					a_group_mail_id.push('billing@brillio.com');
					a_group_mail_id.push('team.fpa@brillio.com');
					if(s_project_subsidiary != 'Comity Designs, Inc.')
					{
					nlapiSendEmail(442, a_RECIPIENT, 'FP Rev Rec Project Created', strVar,a_group_mail_id,'sai.vannamareddy@brillio.com',a_emp_attachment);
					nlapiLogExecution('audit','mail sent for project creation');
					}
				}
			}
		}
	if(type == 'edit'){
		var i_old_pro_Obj = nlapiGetOldRecord();
		var s_old_rev_rec_type = i_old_pro_Obj.getFieldValue('custentity_fp_rev_rec_type');
		var i_pro_id = nlapiGetRecordId();
		var o_project = nlapiLoadRecord('job',parseInt(i_pro_id));
		var s_pm = o_project.getFieldValue('custentity_projectmanager');
		var s_cp = o_project.getFieldValue('custentity_clientpartner');
		var s_dm = o_project.getFieldValue('custentity_deliverymanager');
		var s_customer = o_project.getFieldValue('parent');
		if(s_customer){
			var customer_look_up = nlapiLookupField('customer',parseInt(s_customer),['custentity_clientpartner']);
			var s_cust_cp = customer_look_up.custentity_clientpartner;
		}
		if(s_cust_cp){
			var emp_lookup_cust_cp = nlapiLookupField('employee',parseInt(s_cust_cp),['firstname','email'])
			var cust_cp_firstname = emp_lookup_cust_cp.firstname;
			var cust_cp_email = emp_lookup_cust_cp.email;
		}
		var s_proj_id = o_project.getFieldValue('entityid');
		var s_proj_name = o_project.getFieldValue('companyname'); //custentity_fp_rev_rec_type custentity_deliverymanager
		var s_rev_rec_type = o_project.getFieldValue('custentity_fp_rev_rec_type');
		var s_billing_type = o_project.getFieldValue('jobbillingtype');
		var s_billing_type_T = o_project.getFieldText('jobbillingtype');
		if(s_pm){
			var emp_lookup = nlapiLookupField('employee',parseInt(s_pm),['firstname','email'])
			var pm_firstname = emp_lookup.firstname;
			var pm_email = emp_lookup.email;
		}
		if(s_cp){
			var emp_lookup_s_cp = nlapiLookupField('employee',parseInt(s_cp),['firstname','email'])
			var cp_firstname = emp_lookup_s_cp.firstname;
			var cp_email = emp_lookup_s_cp.email;
		}
		if(s_dm){
			var emp_lookup_s_dm = nlapiLookupField('employee',parseInt(s_dm),['firstname','email'])
			var dm_firstname = emp_lookup_s_dm.firstname;
			var dm_email = emp_lookup_s_dm.email;
		}
		
		if((s_old_rev_rec_type == '' || s_old_rev_rec_type ==null)&& s_rev_rec_type==1){
		var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
		
		strVar += '<p>Hi '+pm_firstname+' ,</p>';
		strVar += '<p>"Project : '+ s_proj_id +' - '+ s_proj_name +'" the revenue recognizition type has been updated by Finance team.';
		strVar += ' Please complete and submit the Revenue share and Effort Budget for the project immediately.';
		strVar += ' Final approval from Practice head will also be required after submission.</p>';

		
		strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1049&deploy=1>Project Setup</a>';
		
		strVar += '<p>Thanks & Regards,</p>';
		strVar += '<p>Team IS</p>';
			
		strVar += '</body>';
		strVar += '</html>';
		
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = '39108';
		
		var a_RECIPIENT = new Array();
		a_RECIPIENT[0]='jayesh@inspirria.com';
		a_RECIPIENT[1]= pm_email;
		a_RECIPIENT[2]= cp_email;
		a_RECIPIENT[3]= dm_email;
		a_RECIPIENT[4]= cust_cp_email;
		
		//nlapiSendEmail(442,a_RECIPIENT, 'FP Rev Rec Module', strVar,null,null,a_emp_attachment);
		nlapiLogExecution('audit','mail sent');
		}
	}
		//IF record is updated
		if(type == 'edit')
		{
			var o_oldRec  = nlapiGetOldRecord(); //Get Old Record SOW Value
			var i_old_sow_val = o_oldRec.getFieldValue('custentity_projectvalue');
			var i_pro_id_ = nlapiGetRecordId();
			var o_project_ = nlapiLoadRecord('job',parseInt(i_pro_id_));
			var i_new_sow_value = o_project_.getFieldValue('custentity_projectvalue');
			var s_pm = o_project_.getFieldValue('custentity_projectmanager');
			var s_cp = o_project_.getFieldValue('custentity_clientpartner');
			var s_dm = o_project_.getFieldValue('custentity_deliverymanager');
			var s_customer = o_project_.getFieldValue('parent');
			if(s_customer){
				var customer_look_up = nlapiLookupField('customer',parseInt(s_customer),['custentity_clientpartner']);
				var s_cust_cp = customer_look_up.custentity_clientpartner;
			}
			if(s_cust_cp){
				var emp_lookup_cust_cp = nlapiLookupField('employee',parseInt(s_cust_cp),['firstname','email'])
				var cust_cp_firstname = emp_lookup_cust_cp.firstname;
				var cust_cp_email = emp_lookup_cust_cp.email;
			}
			var s_proj_id = o_project_.getFieldValue('entityid');
			var s_proj_name = o_project_.getFieldValue('companyname'); //custentity_fp_rev_rec_type custentity_deliverymanager
			var s_rev_rec_type = o_project_.getFieldValue('custentity_fp_rev_rec_type');
			var s_billing_type = o_project_.getFieldValue('jobbillingtype');
			var s_billing_type_T = o_project_.getFieldText('jobbillingtype');
			
			if(s_rev_rec_type)
			{
				if(s_pm){
					var emp_lookup = nlapiLookupField('employee',parseInt(s_pm),['firstname','email'])
					var pm_firstname = emp_lookup.firstname;
					var pm_email = emp_lookup.email;
				}
				if(s_cp){
					var emp_lookup_s_cp = nlapiLookupField('employee',parseInt(s_cp),['firstname','email'])
					var cp_firstname = emp_lookup_s_cp.firstname;
					var cp_email = emp_lookup_s_cp.email;
				}
				if(s_dm){
					var emp_lookup_s_dm = nlapiLookupField('employee',parseInt(s_dm),['firstname','email'])
					var dm_firstname = emp_lookup_s_dm.firstname;
					var dm_email = emp_lookup_s_dm.email;
				}
		
				//IF Change in SOW Value	
				if( (i_old_sow_val != i_new_sow_value )&& (s_rev_rec_type==1))
				{
					var s_project_region = o_project_.getFieldText('custentity_region');
					var s_project_cust = o_project_.getFieldText('parent');
					var s_project_strt = o_project_.getFieldValue('startdate');
					var s_project_end = o_project_.getFieldValue('enddate');
					var s_project_prac = o_project_.getFieldText('custentity_practice');
					
					var strVar = '';
					strVar += '<html>';
					strVar += '<body>';
					
					strVar += '<p>Hello PM,</p>';
					strVar += '<p>The TCV for the Fixed price project with the following details has been changed due to change request.';
					strVar += ' Please update and submit the revised revenue share for the extended duration immediately for review and approval.</p>';
					
					strVar += '<p><b>NOTE:If this is not setup in NS immediately, Salesforce forecast will be under stated to the tune of this extension / CR $ amount!</b></p>';
					
					strVar += '<p>Region:- ' + s_project_region + '<br>';
					strVar += 'Customer:- ' + s_project_cust + '<br>';
					strVar += 'Project:- ' + s_proj_name + '<br>';
					strVar += 'Project Start ' + s_project_strt + ' and End Date ' + s_project_end + '<br>';
					strVar += 'Executing Practice:- ' + s_project_prac + '<br>';
					strVar += 'Previous Project value:- ' + parseFloat(i_old_sow_val) + '<br>';
					strVar += 'New Project value:- ' + parseFloat(i_new_sow_value) + '</p>';
					
					strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1138&deploy=1>Link to Project setup page in Netsuite</a>';
					
					strVar += '<p>Regards,</p>';
					strVar += '<p>Business Ops Team</p>';
					
					strVar += '</body>';
					strVar += '</html>';
					
					var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = '39108';
					
					var a_RECIPIENT = new Array();
					a_RECIPIENT[0] = pm_email;
					a_RECIPIENT[1] = cp_email;
					a_RECIPIENT[2] = dm_email;
					a_RECIPIENT[3] = cust_cp_email;
					
					var a_group_mail_id = new Array();
					a_group_mail_id.push('billing@brillio.com');
					a_group_mail_id.push('team.fpa@brillio.com');
					if(s_project_subsidiary != 'Comity Designs, Inc.')
					{
					nlapiSendEmail(442, a_RECIPIENT, 'FP Project Value Changed: ' + s_proj_id, strVar, a_group_mail_id, null, a_emp_attachment);
					nlapiLogExecution('audit', 'mail sent for sow value changed');
					}
				}
				else {
                  if((i_old_sow_val != i_new_sow_value)&&(s_rev_rec_type==4 || s_rev_rec_type == 2))
				{
					var s_project_region = o_project_.getFieldText('custentity_region');
					var s_project_cust = o_project_.getFieldText('parent');
					var s_project_strt = o_project_.getFieldValue('startdate');
					var s_project_end = o_project_.getFieldValue('enddate');
					var s_project_prac = o_project_.getFieldText('custentity_practice');
					
					var strVar = '';
					strVar += '<html>';
					strVar += '<body>';
					
					strVar += '<p>Hello PM,</p>';
					strVar += '<p>The TCV for the Fixed price project with the following details has been changed due to change request/extension renewal.';
					strVar += 'Please update and submit the revised revenue share for the extended duration immediately for review and approval.</p>';
					
					strVar += '<p><b>NOTE:If this is not setup in NS immediately, Salesforce forecast will be under stated to the tune of this extension / CR $ amount!</b></p>';
					
					strVar += '<p>Region:- ' + s_project_region + '<br>';
					strVar += 'Customer:- ' + s_project_cust + '<br>';
					strVar += 'Project:- ' + s_proj_name + '<br>';
					strVar += 'Project Start ' + s_project_strt + ' and End Date ' + s_project_end + '<br>';
					strVar += 'Executing Practice:- ' + s_project_prac + '<br>';
					strVar += 'Previous Project value:- ' + parseFloat(i_old_sow_val) + '<br>';
					strVar += 'New Project value:- ' + parseFloat(i_new_sow_value) + '</p>';
					
					strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1514&deploy=1>Link to Project setup page in Netsuite</a>';
					
					strVar += '<p>Regards,</p>';
					strVar += '<p>Business Ops Team</p>';
					
					strVar += '</body>';
					strVar += '</html>';
					
					var a_emp_attachment = new Array();
					a_emp_attachment['entity'] = '7905';
					
					var a_RECIPIENT = new Array();
					a_RECIPIENT[0] = pm_email;
					a_RECIPIENT[1] = cp_email;
					a_RECIPIENT[2] = dm_email;
					a_RECIPIENT[3] = cust_cp_email;
					
					var a_group_mail_id = new Array();
					a_group_mail_id.push('billing@brillio.com');
					a_group_mail_id.push('team.fpa@brillio.com');
					if(s_project_subsidiary != 'Comity Designs, Inc.')
					{
					nlapiSendEmail(442, a_RECIPIENT, 'FP Project Value Changed: ' + s_proj_id, strVar, a_group_mail_id, null, a_emp_attachment);
					nlapiLogExecution('audit', 'mail sent for sow value changed');
					}
				}
            }
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}