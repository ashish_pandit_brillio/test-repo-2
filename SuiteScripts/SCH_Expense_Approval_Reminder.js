// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: SCH_Expense_Approval_Reminder
     Author: Vikrant
     Company: Aashna CloudTech
     Date: 09-10-2014
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - scheduledFunction(type)
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function SCH_Expense_Approval_Reminder(type) //
{
    /*  On scheduled function:
     
     - PURPOSE
     
     -
     
     FIELDS USED:
     
     --Field Name--				--ID--
     
     */
    //  LOCAL VARIABLES
    
    
    
    //  SCHEDULED FUNCTION CODE BODY
    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', '******* Execution Started *******');
    
    var context = nlapiGetContext();
    var usageRemaining = context.getRemainingUsage();
    
    var counter = context.getSetting('SCRIPT', 'custscript_exp_counter');
    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'counter : ' + counter);
    
    var row_number = 1;
    var initial_ID = 0;
    
    get_Employee_Email_Name();
    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'employee_email_Array : ' + employee_email_Array.length);
    
    try //
    {
        do //
        {
            if (validate(counter)) //
            {
                if (counter > 0) //
                {
                    initial_ID = counter;
                }
            }
            
            //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'before searching expenses......');
            
            var temp_exp_id = '';
            // Getting Date before 2 days
            var d_today = nlapiDateToString(new Date());
			//d_today= '10/01/2014'
            d_today = nlapiStringToDate(d_today);
			//nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'd_today : ' + d_today);
            
			var day = d_today.getDay();
			var d_two_day_before = '';
			
			if (day == 1 || day == 2) // If today is Monday then take date of Wednesday and if Tuesday then take Thursday
			{
				d_two_day_before = nlapiAddDays(d_today, -5);
			}
			else //
 				if (day != 3) // If Today is not Wednesday
				{
					d_two_day_before = nlapiAddDays(d_today, -3);
				}
				
            if (d_two_day_before != '') //
			{
				d_two_day_before = nlapiDateToString(d_two_day_before);
				nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'd_two_day_before : ' + d_two_day_before);
			}
            
			var filters = new Array();
			filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', initial_ID);
			
			if(day == 3) // If today is Wednesday then system should take expenses from Thursday to Sunday
			{
				var d_from_date = nlapiAddDays(d_today, -5);
				var d_to_date = nlapiAddDays(d_today, -3);
				nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'd_from_date : ' + d_from_date);
				nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'd_to_date : ' + d_to_date);
				
				filters[filters.length] = new nlobjSearchFilter('trandate', null, 'within', d_from_date, d_to_date);
			}
			else // System will take 3 days difference only
			{
				filters[filters.length] = new nlobjSearchFilter('trandate', null, 'on', d_two_day_before);
			}
            
            nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'initial_ID : ' + initial_ID);
            
            var search_Result = nlapiSearchRecord('expensereport', 'customsearch_expense_pending_approval', filters, null);
            
            if (validate(search_Result)) //
            {
                //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'search_Result.length : ' + search_Result.length);
                
                var all_columns = search_Result[0].getAllColumns();
                
                for (i_counter = 0; i_counter < search_Result.length; i_counter++) //
                {
                    var result = search_Result[i_counter];
                    var label = '';
                    var text = '';
                    var value = '';
                    var column = '';
                    
                    var i_internal_id = '';
                    var d_date = '';
                    
                    var i_emp_id = '';
                    var s_emp_email = '';
                    var s_emp_name = '';
                    
                    var s_exp_number = '';
                    var s_memo = '';
                    var s_amount = '';
                    var s_project_name = '';
                    var i_project_type = '';
                    
                    var i_practice_head = '';
                    var s_practice_head_name = '';
                    var s_practice_head_email = '';
                    
                    var i_reporting_manager = '';
                    var s_reporting_manager_name = '';
                    var s_reporting_manager_email = '';
                    
                    var i_delivery_manager = '';
                    var s_delivery_manager_name = '';
                    var s_delivery_manager_email = '';
                    
                    
                    for (var counter_j = 0; counter_j < all_columns.length; counter_j++) //
                    {
                        column = all_columns[counter_j];
                        
                        label = column.getLabel();
                        text = result.getText(column);
                        value = result.getValue(column);
                        
                        if (label == 'internalid')//
                        {
                            i_internal_id = value;
                        }
                        
                        if (label == 'date')//
                        {
                            d_date = value;
                        }
                        
                        if (label == 'emp_id')//
                        {
                            i_emp_id = value;
                        }
                        
                        for (var k_counter = 0; k_counter < employee_email_Array.length; k_counter++) //
                        {
                            var k_emp_id = employee_email_Array[k_counter].split('##')[0];
                            
                            if (i_emp_id == k_emp_id) //
                            {
                                s_emp_name = employee_email_Array[k_counter].split('##')[1];
                                s_emp_email = employee_email_Array[k_counter].split('##')[2];
                                break;
                            }
                        }
                        k_counter = 0;
                        
                        if (label == 'exp_number')//
                        {
                            s_exp_number = value;
                        }
                        
                        if (label == 'memo')//
                        {
                            s_memo = value;
                        }
                        
                        if (label == 'amount')//
                        {
                            s_amount = value;
                        }
                        
                        if (label == 'supervisor')//
                        {
                            i_reporting_manager = value;
                        }
                        
                        for (var k_counter = 0; k_counter < employee_email_Array.length; k_counter++) //
                        {
                            var k_emp_id = employee_email_Array[k_counter].split('##')[0];
                            
                            if (i_reporting_manager == k_emp_id) //
                            {
                                s_reporting_manager_name = employee_email_Array[k_counter].split('##')[1];
                                s_reporting_manager_email = employee_email_Array[k_counter].split('##')[2];
                                break;
                            }
                        }
                        k_counter = 0;
                        
                        if (label == 'project_name')//
                        {
                            s_project_name = value;
                        }
                        
                        if (label == 'delivery_manager')//
                        {
                            i_delivery_manager = value;
                        }
                        
                        for (var k_counter = 0; k_counter < employee_email_Array.length; k_counter++) //
                        {
                            var k_emp_id = employee_email_Array[k_counter].split('##')[0];
                            
                            if (i_delivery_manager == k_emp_id) //
                            {
                                s_delivery_manager_name = employee_email_Array[k_counter].split('##')[1];
                                s_delivery_manager_email = employee_email_Array[k_counter].split('##')[2];
                                break;
                            }
                        }
                        k_counter = 0;
                        
                        if (label == 'project_type')//
                        {
                            i_project_type = value;
                        }
                        
                        if (label == 'practice_head')//
                        {
                            i_practice_head = value;
                        }
                        
                        for (var k_counter = 0; k_counter < employee_email_Array.length; k_counter++) //
                        {
                            var k_emp_id = employee_email_Array[k_counter].split('##')[0];
                            
                            if (i_practice_head == k_emp_id) //
                            {
                                s_practice_head_name = employee_email_Array[k_counter].split('##')[1];
                                s_practice_head_email = employee_email_Array[k_counter].split('##')[2];
                                break;
                            }
                        }
                        k_counter = 0;
                    }
                    
                    if (temp_exp_id == i_emp_id) // this will remove duplicate mail sending to same reporting manager
                    {
                        continue;
                    }
                    temp_exp_id = i_emp_id;
                    
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', '*******' + row_number + '*******');
                    
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'i_internal_id : ' + i_internal_id);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'd_date : ' + d_date);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'i_emp_id : ' + i_emp_id);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_emp_name : ' + s_emp_name);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_emp_email : ' + s_emp_email);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_exp_number : ' + s_exp_number);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_amount : ' + s_amount);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'i_project_type : ' + i_project_type);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'i_reporting_manager : ' + i_reporting_manager);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_reporting_manager_name : ' + s_reporting_manager_name);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_reporting_manager_email : ' + s_reporting_manager_email);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'i_practice_head : ' + i_practice_head);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_practice_head_name : ' + s_practice_head_name);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_practice_head_email : ' + s_practice_head_email);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'i_delivery_manager : ' + i_delivery_manager);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_delivery_manager_name : ' + s_delivery_manager_name);
                    //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 's_delivery_manager_email : ' + s_delivery_manager_email);
                    
                    var htmltext = '<table border="0" width="100%">';
                    htmltext += '<tr>';
                    htmltext += '<td colspan="4" valign="top">';
                    htmltext += '<p>Hi,</p>';
                    htmltext += '<p></p>';
                    htmltext += '<p>The below expense has been pending for your approval for over two days. Kindly decide on the same.</p>';
                    htmltext += '<p></p>';
					htmltext += '<p><strong>Details of Expense</strong></p>';
                    //htmltext += '<p></p>';
                    //htmltext += '<p></p>';
                    htmltext += '</td></tr>';
                    htmltext += '<tr><td><table><tr><td>Expense Date : </td><td>' + d_date + '</td></tr>';
                    htmltext += '<tr><td>Employee Name : </td><td>' + s_emp_name + '</td></tr>';
                    htmltext += '<tr><td>Expense Number : </td><td>' + s_exp_number + '</td></tr>';
                    //htmltext += '<tr><td>Memo : </td><td>' + s_memo + '</td></tr>';
                    htmltext += '<tr><td>Project : </td><td>' + s_project_name + '</td></tr>';
                    htmltext += '<tr><td>Amount : </td><td>' + s_amount + '</td></tr>';
                    htmltext += '</table></td></tr>';
                    htmltext += '<tr></tr>';
                    htmltext += '<tr></tr>';
                    htmltext += '<p>Regards,</p>';
                    htmltext += '<p>Information Systems</p>';
					htmltext += '<p>For any system(NetSuite) related issues, Please write to <a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a></p>';
                    htmltext += '</table>';
                    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
                    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                    htmltext += '<tr>';
                    htmltext += '<td align="right">';
                    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
                    htmltext += '</td>';
                    htmltext += '</tr>';
                    htmltext += '</table>';
                    htmltext += '</body>';
                    htmltext += '</html>';
                    
                    nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'htmltext : ' + htmltext);
                    
                    try //
                    {
                        var subject_line = 'Expense is pending for your approval!!!';
                        var recipient = '';
                        var CC = '';
                        if (i_project_type == '1') // if it is internal project
                        {
                            recipient = s_reporting_manager_email;
                        }
                        else //
                             if (i_project_type == '2') // if it is external project
                            {
                                recipient = s_delivery_manager_email;
                            }
                            else //
                            {
                            
                            }
                        
                        //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'recipient : ' + recipient);
                        
                        //nlapiSendEmail(author, recipient, subject, body, cc, bcc, records, attachments, notifySenderOnBounce, internalOnly);
                        //nlapiSendEmail(442, 'vinod.n@brillio.com', subject_line, htmltext);
                        //nlapiSendEmail(442, 'vikrant@aashnacloudtech.com', subject_line, htmltext);
                        
						CC = s_emp_email;
						
                        nlapiSendEmail(442, recipient, subject_line, htmltext, CC);
                        
                        initial_ID = i_internal_id; // this variable is used to send for new call of script on usage limit exceeds
                        counter = i_internal_id; // 
                        row_number++;
                        
                        nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'Receipants Email ID : ' + recipient);
                        
                        usageRemaining = context.getRemainingUsage();
                        //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder ', 'usageRemaining --> ' + usageRemaining);
                        
                        if (usageRemaining < 60) // use this at time of sending to all...
                        //if (i == 10) // for testing purpose
                        {
                            // Define SYSTEM parameters to schedule the script to re-schedule.
                            var params = new Array();
                            params['status'] = 'scheduled';
                            params['runasadmin'] = 'T';
                            var startDate = new Date();
                            params['startdate'] = startDate.toUTCString();
                            
                            // Define CUSTOM parameters to schedule the script to re-schedule.
                            params['custscript_exp_counter'] = initial_ID;
                            
                            //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder ', ' Script Status --> Before schedule...');
                            
                            var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
                            //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder ', ' Script Status -->' + status);
                            
                            ////If script is scheduled then successfully then check for if status=queued
                            if (status == 'QUEUED') //
                            {
                                nlapiLogExecution('DEBUG', ' SCH_Expense_Approval_Reminder', ' Script is rescheduled ....................');
                            }
                            
                            return;
                        }
                        
                        // COMMMENT BELOW LINE WHILE EXECUTING IN PRODUCTION 
                        //return;
                    } 
                    catch (ex)//
                    {
                        nlapiLogExecution('Error', 'SCH_Expense_Approval_Reminder', 'Not sent ID : ' + s_emp_email + ' ex : ' + ex);
                        //nlapiLogExecution('Error', 'Sent E-Mail id', 'ex : ' + ex);
                    }
                }
            }
            else //
            {
                //nlapiLogExecution('DEBUG', 'SCH_Expense_Approval_Reminder', 'search_Result is invalid : ' + search_Result);
            }
            
            //return;
        } //
        while (search_Result != null) //
        {
            nlapiLogExecution('ERROR', 'SCH_Expense_Approval_Reminder', '******* Execution Completed *******');
            return;
        }
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'Exception', 'Ex : ' + e);
        nlapiLogExecution('ERROR', 'Exception', 'Ex message : ' + e.message);
    }
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
    function validate(value) //
    {
        if (value != null && value != '' && value != 'undefined') // 
        {
            return true;
        }
        return false;
    }
    
    function getDate() //
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10)//
        {
            dd = '0' + dd
        }
        if (mm < 10)//
        {
            mm = getMonth(mm);
        }
        
        var today = mm + ' ' + dd + ',' + yyyy;
        //nlapiLogExecution('DEBUG', 'getDate', 'today : ' + today);
        return today;
    }
    
    function getMonth(mm) //
    {
        if (mm == 1)//
        {
            return 'January';
        }
        if (mm == 2)//
        {
            return 'February';
        }
        if (mm == 3)//
        {
            return 'March';
        }
        if (mm == 4)//
        {
            return 'April';
        }
        if (mm == 5)//
        {
            return 'May';
        }
        if (mm == 6)//
        {
            return 'June';
        }
        if (mm == 7)//
        {
            return 'July';
        }
        if (mm == 8)//
        {
            return 'August';
        }
        if (mm == 9)//
        {
            return 'September';
        }
        if (mm == 10)//
        {
            return 'October';
        }
        if (mm == 11)//
        {
            return 'November';
        }
        if (mm == 12)//
        {
            return 'December';
        }
    }
    
    var employee_email_Array = new Array();
    
    function get_Employee_Email_Name() //
    {
        try //
        {
            var i_initial_id = 0;
            var search_result = null;
            var i_array_counter = 0;
            do //
            {
                var filters = new Array();
                filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', i_initial_id);
                
                search_result = nlapiSearchRecord('employee', 'customsearch_employee_with_email', filters, null);
                
                if (validate(search_result)) //
                {
                    var all_columns = search_result[0].getAllColumns();
                    var result = '';
                    
                    for (var i = 0; i < search_result.length; i++) //
                    {
                        result = search_result[i];
                        
                        employee_email_Array[i_array_counter] = result.getValue(all_columns[0]) + "##" + result.getValue(all_columns[1]) + "##" + result.getValue(all_columns[2]);
                        i_initial_id = result.getValue(all_columns[0]);
                        i_array_counter++;
                    }
                    
                }
            }
            while (search_result != null && search_result != '' && search_result != 'undefined') //
            {
            
            }
        } 
        catch (ex) //
        {
            //nlapiLogExecution('DEBUG', 'get_Employee_Email_Name', 'ex : ' + ex);
            //nlapiLogExecution('DEBUG', 'get_Employee_Email_Name', 'ex : ' + ex.message);
        }
    }
}
// END FUNCTION =====================================================
