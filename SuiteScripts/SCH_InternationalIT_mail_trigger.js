function scheduledTrDate()
{
    nlapiLogExecution('Debug','started');
	try
	{
		var travelSearch = nlapiSearchRecord("customrecord_travel_request",null,
		[
			["custrecord_tr_status","noneof","1","2","7","5"], 
			"AND", 
			["custrecord_tr_departure_date","notbefore","today"], 
			"AND", 
			["custrecord_tr_departure_date","notafter","threedaysfromnow"], 
			"AND", 
			["custrecord_tr_travel_type","is","2"], 
			"AND", 
			["custrecord_mail_trigger","is","F"]
		], 
		[
			new nlobjSearchColumn("name").setSort(false), 
			new nlobjSearchColumn("custrecord_tr_travel_type"), 
			new nlobjSearchColumn("custrecord_tr_employee"), 
			new nlobjSearchColumn("custrecord_tr_departure_date"), 
			new nlobjSearchColumn("email","CUSTRECORD_TR_EMPLOYEE",null),
			new nlobjSearchColumn("internalid")
		]
		);
		
	
		if(_logValidation(travelSearch))
		{
			for(var i = 0; i < travelSearch.length; i++)
			{
                try{

				var trInternalId = travelSearch[i].getValue('internalid');
				var employeeEmail = travelSearch[i].getValue('email','CUSTRECORD_TR_EMPLOYEE',null);
				body = bodyContent();
				body1 = bodyContent1();
				//IT mailer
				nlapiSendEmail(442,employeeEmail,'IT Helpline during your international travel',body.MailBody,'nagarajan.r@brillio.com','shamanth.k@brillio.com',null,null);
				
				//Spam Notification Mailer
				nlapiSendEmail(442,employeeEmail,'Safety Measures',body1.MailBody,'hr@brillio.com','shamanth.k@brillio.com',null,null);
				
				var Id = nlapiSubmitRecord('customrecord_travel_request',trInternalId,'custrecord_mail_trigger','T');
				
				nlapiLogExecution('Debug','Submit ID',Id);	
				nlapiLogExecution('Debug','Email sent to ->',employeeEmail);
                }
                catch(err)
                {
                    nlapiLogExecution('error','error',err)
                    Send_Exeception_Mail(err,travelSearch[i].getValue('internalid'));
                }
			}
		}

	}
	catch (err)
	{
		nlapiLogExecution('Debug','Error in main block',err);
	}
    nlapiLogExecution('Debug','ended');
}

function bodyContent(empName)
{ 
	var htmltext = '';
    
   htmltext += '<html>';
   htmltext += '<body>';
  
   htmltext += '<p>Dear Brillian,</p>';
   
   htmltext += '<p>Here is an important communication about IT support during your travel in US.   BRillio IT Support Center (Known as BRISC) is reachable 24X7 during your stay in US</p>';
   htmltext +='<p>To contact the helpdesk via <b>phone</b>, please call  at +1 201-322-3754. Your call will be directed to a voice greeting which will further lead you to an IT helpdesk support engineer</p>';
   htmltext +='<p>If engineer is busy on another call, you will automatically be directed to a voicemail, where you’d need to leave a message with the below details:</p>';
   htmltext += '<p> -          Your name</p>';
   htmltext += '<p> -          Your phone number</p>';
   htmltext += '<p> -          Your location</p>';
   htmltext += '<p> -          A detailed issue/request description of your problem</p>';
   htmltext += '<p>After leaving your message, an automatic generated ticket will be created and the IT helpdesk support engineer will call you back asap.</p>';
   
   htmltext += '<p>Alternatively, you can use BRISC ticketing tool (https://brisc.brillio.com).  Login using your Brillio email ID and password, and open a ticket.</p>';
   
   htmltext += '<p> You will have an option to select “incident” or “request”. Post this step, the IT helpdesk support engineer will contact you ASAP to resolve the matter.</p>';
   
   htmltext += '<p><b><u>Should you visit any of our Brillio offices in US, to access internet connection </b></u></p>';
   htmltext += '<ul>';
   htmltext += '<li>Select the SSID “Bri-Location-HIFI”( Example: BRI-NJ_HIFI)</li>';
   htmltext += '<li>The password is brillio007</li>';
   htmltext += '</ul>'
   
   htmltext += '<p>If you don’t see help coming your way soon enough, you may reach IT Manager – Nagarajan at nagarajan.r@brillio.com / +1 669 210 4320</p>';
   htmltext += '<p>BRISC team wishes you a safe and happy journey!<br/>';
   htmltext += 'Team BRISC</p>';
 

    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext
             
    };
}

function bodyContent1(empName)
{ 
	var htmltext = '';
    
   htmltext += '<html>';
   htmltext += '<body>';
  
   htmltext += '<p>All,</p>';
   
   htmltext += '<p>You would have received an email on IT Support contact details.Hope you have saved the IT Support phone number (+1 201-322-3754)</p>';
   htmltext +='<p>Here is <b><u>another important communication regarding scam calls.</b></u>In recent times, scam calls are on the rise.  Please BE AWARE and protect yourself!</p>';
   htmltext +='<p>Many of these calls originate with a 911 or an unknown phone number such as a normal US telephone number.If someone tells you they are calling you from IRS, Social Security Administration or other Federal agencies demanding for money,<b><u>always politely ask for their call back number</b></u> and <b><u>tell them you will take help from your attorney and reach out to them.</b></u></p>';
   htmltext += '<p>Please note that IRS, Social Security Administration never calls tax payers over the telephone.</p>';
   htmltext += '<p><b><u>DO NOT</u></b> provide such callers your SSN, personal identifying information such as date of birth, age, address or bank pin numbers.</p>';
   htmltext += '<p>Even if they insist,<b><u>DO NOT</b></u> wire money to unknown bank accounts, or take the trouble of buying gifts cards that you can pay them over the phone (these cards cannot be traced).</p>';
   htmltext += '<p>In general, these callers may sounds intimidating and aggressive and may event threaten to bring a warrant for arrest.Once again we reiterate IRS or Social Administration NEVER call tax payers over the telephone</p>'
   
   htmltext += '<p>Below are a couple of links to read up for your own awareness.</p>';
   
   
   htmltext += '<ul>';
   htmltext += '<li>https://www.irs.gov/uac/tax-scams-consumer-alerts</li>';
   htmltext += '<li>https://www.reference.com/government-politics/spot-irs-phone-scam-6634e2cf691b59a5?aq=how+to+report+irs+phone+scam&qo=cdpArticles</li>';
   htmltext += '<li>https://blog.ssa.gov/scam-awareness/</li>';
   htmltext += '<li>https://blog.ssa.gov/dont-be-a-scam-victim-youre-in-control/</li>';
   htmltext += '</ul>'
   
   htmltext += '<p>In the event you are a victim of a scam, please notify your local police immediately.</p>';
   htmltext += '<p>Be safe and cautious!</p>';

   htmltext += '<p>Regards,<br/>'
   htmltext += 'Brillio HR, Brillio IT operations</p>';

    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext
             
    };
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function Send_Exeception_Mail(error, trInternalId)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on internationalIT_mail_trigger';
        
        var s_Body = 'This is to inform that internationIT_mail_trigger notification is having an issue for employee “'+trInternalId+'” and System is not able to send the email notification to employees.';
        s_Body += '<br/>Issue: Code: '+error.code+' Message: '+error.message;
        s_Body += '<br/>Script: '+s_ScriptID;
        s_Body += '<br/>Deployment: '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exeception_Mail', err.message); 
		
    }​​​​​
}​​​​​