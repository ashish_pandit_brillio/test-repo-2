/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Apr 2016     amol.sahijwani
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	
	var context	=	nlapiGetContext();
	
	var d_today	=	new Date();
	
	var d_start_date	=	nlapiAddMonths(nlapiAddDays(d_today, -1 * d_today.getDate() + 1), -1);
	
	var d_end_date		=	nlapiAddDays(nlapiAddMonths(d_start_date, 1), -1);

	var a_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	var period = a_months[d_start_date.getMonth()] + ' ' + d_start_date.getFullYear();

	/*var check_if_already_updated = nlapiSearchRecord('customrecord_allocated_resources_per_mon', null, [new nlobjSearchFilter('custrecord_arpm_period', null, 'is', period)], null);
	
	if(check_if_already_updated != null)
		{
			nlapiLogExecution('ERROR', 'Data Already Exists for Period' + period, '');
			return;
		}*/
	
	var search_results = searchRecord('resourceallocation', 'customsearch1306', [new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date), new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date)], null);

	// Get Existing Records
	var existing_records = searchRecord('customrecord_allocated_resources_per_mon', null,
											[new nlobjSearchFilter('custrecord_arpm_period', null, 'is', period)], [new nlobjSearchColumn('custrecord_arpm_project'), new nlobjSearchColumn('custrecord_arpm_location')]); 
	
	var o_existing_records_data = new Object();
	
	for(var i = 0; existing_records != null && i < existing_records.length; i++)
		{
			var i_project_id	=	existing_records[i].getValue('custrecord_arpm_project');
			if(o_existing_records_data[i_project_id] == undefined)
				{
					o_existing_records_data[i_project_id]	=	new Object();
				}
			
			o_existing_records_data[i_project_id][existing_records[i].getValue('custrecord_arpm_location')]	=	existing_records[i].id;
		}
	
	var search_columns;

	for(var i = 0; search_results != null && i < search_results.length; i++)
	{
		yieldScript(context);
		
		search_columns = search_results[0].getAllColumns();

		var project = search_results[i].getValue(search_columns[0]);
		var location = search_results[i].getValue(search_columns[1]);
		var number_of_resources = search_results[i].getValue(search_columns[2]);

		var record	=	null;
		
		if(o_existing_records_data[project] != undefined && o_existing_records_data[project][location] != undefined)
			{
				record	=	nlapiLoadRecord('customrecord_allocated_resources_per_mon', o_existing_records_data[project][location]);
			}
		else
			{
				record = nlapiCreateRecord('customrecord_allocated_resources_per_mon', {recordmode: 'dynamic'});
			}
		
		record.setFieldValue('custrecord_arpm_project', project);
		record.setFieldValue('custrecord_arpm_location', location);
		record.setFieldValue('custrecord_arpm_period', period);
		record.setFieldValue('custrecord_arpm_num_allocated_resources', number_of_resources);
		nlapiSubmitRecord(record, true, true);
	}

	var a = 1;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 900) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}