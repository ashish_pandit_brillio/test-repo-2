//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=368
/**
* Module Description
*
* Version    Date            Author           Remarks
* 1.00       25 Dec 2014     amol.sahijwani
*
*/
/**
* The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
* @appliedtorecord recordType
*
* @param {String} type Operation types: create, edit, delete, xedit
*                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
*                      pack, ship (IF)
*                      markcomplete (Call, Task)
*                      reassign (Case)
*                      editforecast (Opp, Estimate)
* @returns {Void}
*/
function sourceColumnsFields(type) {
var context = nlapiGetContext();
/*----------Added by Koushalya 28/12/2021---------*/
var filters_customrecord_revenue_location_subsidiarySearch = ["custrecord_offsite_onsite", "anyof", "2"];
var obj_rev_loc_subSrch = Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
var obj_offsite_onsite = obj_rev_loc_subSrch["Onsite/Offsite"];
var onsite_offsite = '';
/*-------------------------------------------------*/
if (type == 'delete') {
return;
}
var excel_file_obj = '';
var err_row_excel = '';
var strVar_excel = '';
strVar_excel += '<table>';
strVar_excel += '	<tr>';
strVar_excel += ' <td width="100%">';
strVar_excel += '<table width="100%" border="1">';
strVar_excel += '	<tr>';
strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';
strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';
strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';
strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';
strVar_excel += '	</tr>';
var record = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
try {
var transactionNum = record.getFieldValue('tranid');
var invoiceInternalID = nlapiGetRecordId()
var i_billable_time_count = record.getLineItemCount('time');
nlapiLogExecution('AUDIT', 'i_billable_time_count', i_billable_time_count);
var update_status = record.getFieldValue('custbody_is_je_updated_for_emp_type');
// if(update_status == 'T')
{
nlapiLogExecution('AUDIT', 'updated returned recordid', nlapiGetRecordId());
//return;
}
if (parseInt(i_billable_time_count) > parseInt(150)) {
record.setFieldValue('custbody_is_je_updated_for_emp_type', 'T');
nlapiSubmitRecord(record, true, true);
//var invoice_array = new Array();
//invoice_array['custscript_ra_invoice_id'] = nlapiGetRecordId();
//var status = nlapiScheduleScript('customscript_sch_tm_invoice_update','customdeploy_sch_tm_invoice_update',invoice_array);
nlapiLogExecution('AUDIT', 'call sch script', nlapiGetRecordId());
return;
}
var customer = record.getFieldText('entity');
var region_id_cust = nlapiLookupField('customer', record.getFieldValue('entity'), 'custentity_region');
var project = record.getFieldText('job');
var proj_internal_id = record.getFieldValue('job');
var proj_rcrd = nlapiLoadRecord('job', proj_internal_id);
var billing_type = proj_rcrd.getFieldText('jobbillingtype');
var region_id = proj_rcrd.getFieldValue('custentity_region');
var project_region = proj_rcrd.getFieldText('custentity_region');
var parent_practice = proj_rcrd.getFieldValue('custentity_practice');
parent_practice = nlapiLookupField('department', parent_practice, 'custrecord_parent_practice', true);
if (!region_id)
region_id = region_id_cust;
var proj_name = proj_rcrd.getFieldValue('altname');
var proj_category = proj_rcrd.getFieldText('custentity_project_allocation_category');
var proj_category_val = proj_rcrd.getFieldValue('custentity_project_allocation_category');
var billing_from_date = record.getFieldValue('custbody_billfrom');
var billing_to_date = record.getFieldValue('custbody_billto');
var territory = nlapiLookupField('job', record.getFieldValue('job'), 'customer.territory');
nlapiLogExecution('AUDIT', nlapiGetRecordId(), 'Project: ' + project + ', Customer: ' + customer);
var a_employee_names = new Object();
var a_resource_allocations = new Array();
var emp_list = new Array();
if (_logValidation(billing_from_date)) {
billing_from_date = nlapiStringToDate(billing_from_date);
billing_to_date = nlapiStringToDate(billing_to_date);
var filters_search_allocation = new Array();
filters_search_allocation[0] = new nlobjSearchFilter('project', null, 'anyof', proj_internal_id);
filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', billing_to_date);
filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
var columns = new Array();
columns[0] = new nlobjSearchColumn('resource');
columns[1] = new nlobjSearchColumn('employeetype', 'employee');
columns[2] = new nlobjSearchColumn('custentity_persontype', 'employee');
columns[3] = new nlobjSearchColumn('subsidiary', 'employee');
columns[4] = new nlobjSearchColumn('custentity_fusion_empid', 'employee');
columns[5] = new nlobjSearchColumn('firstname', 'employee');
columns[6] = new nlobjSearchColumn('middlename', 'employee');
columns[7] = new nlobjSearchColumn('lastname', 'employee');
columns[8] = new nlobjSearchColumn('companyname', 'customer');
columns[9] = new nlobjSearchColumn('entityid', 'customer');
columns[10] = new nlobjSearchColumn('entityid', 'job');
columns[11] = new nlobjSearchColumn('custentity_practice', 'job');
columns[12] = new nlobjSearchColumn('department', 'employee');
columns[13] = new nlobjSearchColumn('custentity_legal_entity_fusion', 'employee');
var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
if (_logValidation(project_allocation_result)) {
for (var i_search_indx = 0; i_search_indx < project_allocation_result.length; i_search_indx++) {
var i_employee_id = project_allocation_result[i_search_indx].getValue('resource');
var s_emp_type = project_allocation_result[i_search_indx].getText('employeetype', 'employee');
var s_emp_person_type = project_allocation_result[i_search_indx].getText('custentity_persontype', 'employee');
var i_emp_subsidiary = project_allocation_result[i_search_indx].getValue('subsidiary', 'employee');
var fusion_id = project_allocation_result[i_search_indx].getValue('custentity_fusion_empid', 'employee');
var first_name = project_allocation_result[i_search_indx].getValue('firstname', 'employee');
var middl_name = project_allocation_result[i_search_indx].getValue('middlename', 'employee');
var lst_name = project_allocation_result[i_search_indx].getValue('lastname', 'employee');
var cust_id = project_allocation_result[i_search_indx].getValue('entityid', 'customer');
var cust_name = project_allocation_result[i_search_indx].getValue('companyname', 'customer');
var proj_entity_id = project_allocation_result[i_search_indx].getValue('entityid', 'job');
var proj_department = project_allocation_result[i_search_indx].getValue('custentity_practice', 'job');
var proj_department_text = project_allocation_result[i_search_indx].getText('custentity_practice', 'job');
var emp_department = project_allocation_result[i_search_indx].getValue('department', 'employee');
var emp_department_text = project_allocation_result[i_search_indx].getText('department', 'employee');
var custentity_legal_entity_fusion = project_allocation_result[i_search_indx].getValue('custentity_legal_entity_fusion', 'employee');
a_resource_allocations[i_search_indx] = {
'emp_id': i_employee_id,
'emp_type': s_emp_type,
'person_type': s_emp_person_type,
'subsidiary': i_emp_subsidiary,
'fusion_id': fusion_id,
'frst_name': first_name,
'mddl_name': middl_name,
'lst_name': lst_name,
'cust_id': cust_id,
'cust_name': cust_name,
'proj_entity_id': proj_entity_id,
'proj_dep_v': proj_department,
'proj_dep_t': proj_department_text,
'emp_dep_c': emp_department,
'emp_dep_t': emp_department_text,
'custentity_legal_entity_fusion': custentity_legal_entity_fusion
};
emp_list.push(i_employee_id);
}
}
}
var misPracticeFlagTime = true;
var prevMisPractice = '';
var misCounter = 0;
for (var i = 1; i <= i_billable_time_count; i++) {
var emp_full_name = '';
var misc_practice = '';
var core_practice = '';
var isApply = record.getLineItemValue('time', 'apply', i);
var item = record.getLineItemValue('time', 'item', i);
//if(isApply == 'T' && (item == '2221' || item == '2222' || item == '2425' || item == '2633'))
if (isApply == 'T') {
var employeeId = record.getLineItemValue('time', 'employee', i);
if (a_employee_names[employeeId] == undefined) {
a_employee_names[employeeId] = nlapiLookupField('employee', employeeId, 'entityid');
}
var employeeName = a_employee_names[employeeId]; //
if (misCounter > 0 && !_logValidation(prevMisPractice)) {
misPracticeFlagTime = false;
}
record.setLineItemValue('time', 'custcol_employeenamecolumn', i, employeeName); // Set Employee
record.setLineItemValue('time', 'custcolprj_name', i, project); // Set Project
record.setLineItemValue('time', 'custcolcustcol_temp_customer', i, customer); // Set Customer
record.setLineItemValue('time', 'custcol_territory', i, territory);
record.setLineItemValue('time', 'custcol_project_region', i, project_region);
record.setLineItemValue('time', 'custcol_parent_executing_practice', i, parent_practice);
if (emp_list.indexOf(employeeId) >= 0) {
var emp_posi = emp_list.indexOf(employeeId);
var emp_subsidiary = a_resource_allocations[emp_posi].subsidiary;
var entity_fusion = a_resource_allocations[emp_posi].custentity_legal_entity_fusion;
var emp_practice = a_resource_allocations[emp_posi].emp_dep_c;
var is_practice_active_e = nlapiLookupField('department', parseInt(emp_practice), ['isinactive', 'custrecord_is_delivery_practice']);
var isinactive_Practice_e = is_practice_active_e.isinactive;
nlapiLogExecution('debug', 'isinactive_Practice_e', isinactive_Practice_e);
core_practice = is_practice_active_e.custrecord_is_delivery_practice;
nlapiLogExecution('debug', 'core_practice', core_practice);
if (emp_subsidiary == 3 && entity_fusion == 'Brillio Technologies Private Limited UK') {
onsite_offsite = 'Onsite';
}
else {
onsite_offsite = obj_offsite_onsite[emp_subsidiary] ? 'Offshore' : 'Onsite';
}
/*else if (emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 12) {
var onsite_offsite = 'Offsite';
} else {
var onsite_offsite = 'Onsite';
}*/
record.setLineItemValue('time', 'custcol_employee_type', i, a_resource_allocations[emp_posi].emp_type);
record.setLineItemValue('time', 'custcol_person_type', i, a_resource_allocations[emp_posi].person_type);
record.setLineItemValue('time', 'custcol_onsite_offsite', i, onsite_offsite);
record.setLineItemValue('time', 'custcol_billing_type', i, billing_type);
record.setLineItemValue('time', 'custcol_customer_entityid', i, a_resource_allocations[emp_posi].cust_id);
record.setLineItemValue('time', 'custcol_cust_name_on_a_click_report', i, a_resource_allocations[emp_posi].cust_name);
record.setLineItemValue('time', 'custcol_project_entity_id', i, a_resource_allocations[emp_posi].proj_entity_id);
record.setLineItemValue('time', 'custcol_proj_name_on_a_click_report', i, proj_name);
record.setLineItemValue('time', 'custcol_region_master_setup', i, region_id);
record.setLineItemValue('time', 'custcol_proj_category_on_a_click', i, proj_category);
if (a_resource_allocations[emp_posi].frst_name)
emp_full_name = a_resource_allocations[emp_posi].frst_name;
if (a_resource_allocations[emp_posi].mddl_name)
emp_full_name = emp_full_name + ' ' + a_resource_allocations[emp_posi].mddl_name;
if (a_resource_allocations[emp_posi].lst_name)
emp_full_name = emp_full_name + ' ' + a_resource_allocations[emp_posi].lst_name;
record.setLineItemValue('time', 'custcol_employee_entity_id', i, a_resource_allocations[emp_posi].fusion_id);
record.setLineItemValue('time', 'custcol_emp_name_on_a_click_report', i, emp_full_name);
}
//Added for MIS Practice Update
if (proj_category_val) {
if ((parseInt(proj_category_val) == parseInt(1)) && (core_practice == 'T')) {
misc_practice = emp_practice;
misc_practice = a_resource_allocations[emp_posi].emp_dep_t;
} else {
misc_practice = a_resource_allocations[emp_posi].proj_dep_v;
var is_practice_active = nlapiLookupField('department', parseInt(misc_practice), ['isinactive']);
isinactive_Practice_e = is_practice_active.isinactive;
misc_practice = a_resource_allocations[emp_posi].proj_dep_t;
}
}
nlapiLogExecution('audit', 'Pratice', 'misc_practice:' + misc_practice);
if (misc_practice && isinactive_Practice_e == 'F') {
record.setLineItemValue('time', 'custcol_mis_practice', i, misc_practice);
}
misCounter++;
prevMisPractice = record.getLineItemValue('time', 'custcol_mis_practice', i);
//
}
}
var i_billable_expense_count = nlapiGetLineItemCount('expcost');
for (var i = 1; i <= i_billable_expense_count; i++) {
var isApply = record.getLineItemValue('expcost', 'apply', i);
if (isApply == 'T') {
var employeeId = record.getLineItemValue('expcost', 'employee', i);
if (emp_list.indexOf(employeeId) >= 0) {
var emp_posi = emp_list.indexOf(employeeId);
var emp_subsidiary = a_resource_allocations[emp_posi].subsidiary;
var entity_fusion = a_resource_allocations[emp_posi].custentity_legal_entity_fusion;
if (emp_subsidiary == 3 && entity_fusion == 'Brillio Technologies Private Limited UK') {
onsite_offsite = 'Onsite';
}
else {
onsite_offsite = obj_offsite_onsite[emp_subsidiary] ? 'Offshore' : 'Onsite';
}
/* else if (emp_subsidiary == 3 || emp_subsidiary == 9 || emp_subsidiary == 12) {
var onsite_offsite = 'Offsite';
} else {
var onsite_offsite = 'Onsite';
}*/
record.setLineItemValue('expcost', 'custcol_employee_type', i, a_resource_allocations[emp_posi].emp_type);
record.setLineItemValue('expcost', 'custcol_person_type', i, a_resource_allocations[emp_posi].person_type);
record.setLineItemValue('expcost', 'custcol_onsite_offsite', i, onsite_offsite);
record.setLineItemValue('expcost', 'custcol_billing_type', i, billing_type);
var cust_entity_id = customer.split(' ');
cust_entity_id = cust_entity_id[0];
record.setLineItemValue('expcost', 'custcol_customer_entityid', i, cust_entity_id);
var pro_entity_id = project.split(' ');
pro_entity_id = pro_entity_id[0];
record.setLineItemValue('expcost', 'custcol_project_entity_id', i, pro_entity_id);
var emp_name = record.getLineItemValue('expcost', 'employeedisp', i);
var emp_id = '';
if (_logValidation(emp_name)) {
emp_id = emp_name.split('-');
emp_id = emp_id[0];
}
record.setLineItemValue('expcost', 'custcol_employee_entity_id', i, emp_id);
record.setLineItemValue('expcost', 'custcol_region_master_setup', i, region_id);
}
record.setLineItemValue('expcost', 'custcol_project_region', i, project_region);
record.setLineItemValue('expcost', 'custcol_parent_executing_practice', i, parent_practice);
}
}
if (misPracticeFlagTime == false) {
err_row_excel += '	<tr>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'invoice' + '</td>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + nlapiGetRecordId() + '</td>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'MIS Practice is blank' + '</td>';
err_row_excel += '	</tr>';
}
nlapiSubmitRecord(record, true, true);
} catch (err) {
nlapiLogExecution('DEBUG', 'Error message', err);
err_row_excel += '	<tr>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + 'invoice' + '</td>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + nlapiGetRecordId() + '</td>';
err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.message + '</td>';
err_row_excel += '	</tr>'; //nlapiGetRecordType(), nlapiGetRecordId()
}
if (_logValidation(err_row_excel)) {
var tailMail = '';
tailMail += '</table>';
tailMail += ' </td>';
tailMail += '</tr>';
tailMail += '</table>';
strVar_excel = strVar_excel + err_row_excel + tailMail
//excel_file_obj = generate_excel(strVar_excel);
var mailTemplate = "";
mailTemplate += '<html>';
mailTemplate += '<body>';
mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";
mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";
mailTemplate += "<br/>"
mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";
mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";
mailTemplate += "<br/>"
mailTemplate += strVar_excel
mailTemplate += "<br/>"
mailTemplate += "<p>Regards, <br/> Information Systems</p>";
mailTemplate += '</body>';
mailTemplate += '</html>';
// nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);
}
}
function _logValidation(value) {
if (value != 'null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value != 'undefined' && value != 'NaN' && value != NaN) {
return true;
} else {
return false;
}
}