    /**
     * @NApiVersion 2.x
     * @NScriptType ClientScript
     */
    define(['N/url','N/currentRecord','N/format','N/ui/dialog'],
        function (url,currentRecord,format,dialog) {
        function fieldChanged(context) {
            // Navigate to selected page
            if (context.fieldId == 'custpage_pageid') {
                var pageId = context.currentRecord.getValue({
                        fieldId : 'custpage_pageid'
                    });

                pageId = parseInt(pageId.split('_')[1]);

                document.location = url.resolveScript({
                        scriptId : getParameterFromURL('script'),
                        deploymentId : getParameterFromURL('deploy'),
                        params : {
                            'page' : pageId
                        }
                    });
            }
        }

        function getSuiteletPage(suiteletScriptId, suiteletDeploymentId, pageId) {
            document.location = url.resolveScript({
                    scriptId : suiteletScriptId,
                    deploymentId : suiteletDeploymentId,
                    params : {
                        'page' : pageId
                    }
                });
        }
        function Filteroptions(suiteletScriptId, suiteletDeploymentId)
        { 
            var currentRecordObj = currentRecord.get(); 
            if(currentRecordObj.getValue('custpage_st_date') && currentRecordObj.getValue('custpage_end_date') =="")
            {
                var options = {
                    title: "Alert",
                    message: "Enter End Date."
                 };
                 dialog.alert(options).then(success).catch(failure);
                return false;
            }
            if(currentRecordObj.getValue('custpage_end_date') && currentRecordObj.getValue('custpage_st_date') =="")
            {
                 var options = {
                    title: "Alert",
                    message: "Enter Start Date."
                 };
                 dialog.alert(options).then(success).catch(failure);
                return false;
            }
           
            var startdate ,endDate ,jobstatus;
            if(currentRecordObj.getValue('custpage_st_date') && currentRecordObj.getValue('custpage_end_date'))
            {
                  startdate =  DateFormat(currentRecordObj.getValue('custpage_st_date'));        
                  endDate =  DateFormat(currentRecordObj.getValue('custpage_end_date'));
            }
           
            var subsidiary =  currentRecordObj.getValue('custpage_subsidiary');
            jobstatus =  currentRecordObj.getValue('custpage_jobstatus');
            var custpage_customer =  currentRecordObj.getValue('custpage_customer');
            var custpage_job =  currentRecordObj.getValue('custpage_job');
            document.location = url.resolveScript({
                scriptId : suiteletScriptId,
                deploymentId : suiteletDeploymentId,
                params : {
                    'startdate' : startdate,
                    'endDate':endDate,
                    'subsidiary':subsidiary,
                    'jobstatus':jobstatus,
                    'customer':custpage_customer,
                    'job':custpage_job
                }
            });
        }
        function getParameterFromURL(param) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == param) {
                    return decodeURIComponent(pair[1]);
                }
            }
            return (false);
        }
        function DateFormat(initialFormattedDateString)
        {
            var parsedDateStringAsRawDateObject = format.parse({
                value: initialFormattedDateString,
                type: format.Type.DATE
            });
            return format.format({
                value: parsedDateStringAsRawDateObject,
                type: format.Type.DATE
            });
        }
        return {
            fieldChanged : fieldChanged,
            getSuiteletPage : getSuiteletPage,
            Filteroptions:Filteroptions
        };

    });
