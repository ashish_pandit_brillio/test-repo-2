function GetQuestions(datain)
{
    nlapiLogExecution('debug', 'started');

    var search = nlapiSearchRecord('customrecord_30days_question_master', null, null, [
        new nlobjSearchColumn('custrecord_question_master_type'),
        new nlobjSearchColumn('custrecord_question'),
        new nlobjSearchColumn('custrecord_recommendation')
    ]);

var search_options = nlapiSearchRecord('customrecord_survey_options_master', null, null, [
        new nlobjSearchColumn('custrecord_option_type'),
        new nlobjSearchColumn('custrecord_option_1'),
        new nlobjSearchColumn('custrecord_option_2'),
        new nlobjSearchColumn('custrecord_option_3'),
        new nlobjSearchColumn('custrecord_option_4')
    ]);

    var questions = [];

    if(search){

        search.forEach(function(searchRow){
            questions.push({
                Question : searchRow.getValue('custrecord_question'),
                Option1 : searchRow.getValue('custrecord_option_1'),
                Option2 : searchRow.getValue('custrecord_option_2'),
                Option3 : searchRow.getValue('custrecord_option_3'),
                Option4 : searchRow.getValue('custrecord_option_4'),
                Recommendation : searchRow.getValue('custrecord_recommendation')
            });
        });     
    }
    
    nlapiLogExecution('debug', 'questions length', questions.length);
    return questions;
}