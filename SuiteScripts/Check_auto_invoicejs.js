/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Jan 2018     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */

function userEventAfterSubmit(type){
	
	
  var loadProject =nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
  var custArray = new Array();
  custArray.push('65865');
  custArray.push('8312');
  custArray.push('4297');
  custArray.push('4204');
  custArray.push('8080');
  custArray.push('10514');
  for(var i in custArray){
	  if( loadProject.getFieldValue('parent')==custArray[i]){
		  loadProject.setFieldValue('custentity_exclude_auto_invoice', 'T');
	  }
	 
  }
  nlapiSubmitRecord(loadProject);
}
