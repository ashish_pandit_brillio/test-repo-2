/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         15th  Feb 2016             Manikandan v          Send PF user Guide to the Employees scheduled at 9 a.m.
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');

// search for employee with complete first month from  today

var emp_search = nlapiSearchRecord('employee', 'customsearch_pf_transfer_guideline', null,columns);

nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

if(isNotEmpty(emp_search)){
	
	for(var i=0;i<emp_search.length;i++){
		
		
		sendServiceMails(
			emp_search[i].getValue('firstname'),
			emp_search[i].getValue('email'),
			emp_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, emp_id){
	
	try{
                //var AttachmentID=32680;
                var fileObj = nlapiLoadFile(125065);
		var mailTemplate = serviceTemplate(firstName);		
		nlapiLogExecution('debug', 'chekpoint',email);
		nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,null,null,null,fileObj, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
//htmltext += &nbsp;
htmltext += '<p> This is to inform you that, your Provident Fund no is updated in PF portal</p>';

htmltext += '<p> Please refer the attached user guide for transferring your previous PF </p>';
htmltext +='<p>For any clarification, do raise a query on HR Hub.</p>';

htmltext += '<p>Thanks & Regards,</p>';
htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Provident Fund Transfer User Guide"      
    };
}

