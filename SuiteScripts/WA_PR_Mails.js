/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Nov 2015     nitish.mishra
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	try {
		var sendTo = 'nitish.mishra@gmail.com';// nlapiGetFieldValue('');
		nlapiSendEmail(442, sendTo, 'PR Test', 'PR Test');
		nlapiLogExecution('debug', 'mail send to ' + sendTo);
	} catch (err) {
		nlapiLogExecution('ERROR', 'workflowAction', err);
	}
}
