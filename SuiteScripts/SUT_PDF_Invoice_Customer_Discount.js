/**
 * Generate PDF for invoice with customer discount
 * 
 * Version Date Author Remarks 1.00 20 Aug 2015 Nitish Mishra
 * 
 */
var a_data_array = new Array();
	var a_hours_array = new Array();
	var a_employee_array = new Array();
	var a_line_array = new Array();
	var i_inc_cnt = 0;
	var i_cnt = 0;
	var i_hours_per_day;
	var i_hours_per_week;
	var i_days_for_month;
	var i_OT_applicable;
	var i_OT_rate_ST;
	var i_week_days;
	var a_location_data_array = new Array();
	var i_rate = 0;
	var otrVar;
	var ctrVar;
	var i_flag = 0;
	var i_project;
	var i_project_text;
	var i_total_amount_INV = 0;
	var a_split_f_array = new Array();
	var i_cnt = 0;
	var i_S_No_cnt = 0;
	var strVar = "";
	var a_employee_array = new Array();
	var a_employee_name_array = new Array();
	var i_cnt = 0;
	var linenum = '17';
	var pagening = 4;
	var pagecount = 0;
	var f_tenure_disc = '';
	var f_volume_disc = '';
	var i_tenure_disc_percent = 0;
	var i_volume_disc_percent =0;
	var f_without_disc_total = 0;
	//var s_remittance_details = "Accounts Receivable, Brillio LLC;\n100 Town Square Place;\nSuite 308, Jersey City\n NJ 07310"

	var flag_name = '';
function suitelet(request, response) {
	try {
		var invoiceId = request.getParameter('invoice');

		if (invoiceId) {
			var xmlContent = "";
			var cssFileUrl = nlapiEscapeXML("https://system.na1.netsuite.com/core/media/media.nl?id=38485&c=3883006&h=ff923e0edfab23fa6bf7&_xt=.css");

			var companyDetails = getCompanyDetails();
			var invoiceDetails = getInvoiceDetails(request
			        .getParameter('invoice'));
			var customerDetails = getCustomerDetails(invoiceDetails.Customer);
			var projectDetails = getProjectDetails(invoiceDetails.Project);

			xmlContent += "<?xml version='1.0'?>";
			xmlContent += "<pdf><head>";
			xmlContent += "<link SRC='" + cssFileUrl + "' type='stylesheet'/>";
			xmlContent += "<meta name='title' value='Invoice # "
			        + invoiceDetails.InvoiceNumber + "'/>";
			xmlContent += "<macrolist>";
			xmlContent += "<macro id='pageFooter' footer-height='100mm'>";
			xmlContent += "<div style='footer-div'>";
			xmlContent += "<hr class='footer-line' width='100%'/>";
			xmlContent += "<p class='footer' align='center'>Brillio LLC, certifies that this invoice is correct, that payment has not been received and"
			        + " that it is presented with the<br/>"
			        + " understanding that the amount to be paid there under can be audited to verify.</p>";
			xmlContent += "</div>";
			xmlContent += "</macro>";
			xmlContent += "</macrolist>";

			xmlContent += "</head>";
			xmlContent += "<body footer='pageFooter' footer-height='20mm'>";
			xmlContent += mainPage(invoiceDetails, customerDetails,
			        companyDetails, projectDetails,invoiceId);
			xmlContent += "</body>";
			xmlContent += "</pdf>";

			response.renderPDF(xmlContent);
		} else {
			response.writePage('No Invoice Id Provided');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		// throw err;
	}
}

function mainPage(invoiceDetails, customerDetails, companyDetails,
        projectDetails,invoiceId)
{
	try {
		var companyLogoUrl = nlapiEscapeXML(companyDetails.LogoUrl);

		var pdfText = "";
		pdfText += "<table width='100%' class='table1'>"; // T1

		// ---------------------------------------------------
		pdfText += "<tr class='underline-row main-header-row'>";

		pdfText += "<td width='50%'>";
		pdfText += "<img class='companyLogo' src='" + companyLogoUrl + "'/>";
		pdfText += "</td>";

		pdfText += "<td align='right'>";
		pdfText += "<div class='address-div'><p>";
		pdfText += companyDetails.Name
		        + "<br/>"
		        + companyDetails.AddressLine1
		        + "<br/>"
		        + companyDetails.AddressLine2
		        + "<br/>"
		        + companyDetails.City
		        + ", "
		        + companyDetails.State
		        + " "
		        + companyDetails.Zip
		        + "<br/>"
		        + (companyDetails.Phone ? ("Phone: " + companyDetails.Phone + "<br/>")
		                : "")
		        + (companyDetails.Fax ? ("Fax: " + companyDetails.Fax + "")
		                : "");
		pdfText += "</p></div>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		// pdfText += "<tr><td colspan='2'>";
		// pdfText += "<hr width='100%'/>";
		// pdfText += "</td></tr>";

		// ---------------------------------------------------

		pdfText += "<tr class='add-row-padding'>";

		pdfText += "<td>";
		pdfText += "<span class='label'>Invoice Date : </span>";
		pdfText += "<span class='value'>"
		        + formatDateText(invoiceDetails.InvoiceDate) + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "<span class='label'>Invoice Number : </span>";
		pdfText += "<span class='value'>" + invoiceDetails.InvoiceNumber
		        + "</span>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label close-span'>Client Information : </span>";
		pdfText += "<div class='address-block'><p>" + customerDetails.Email
		        + "<br/>" + invoiceDetails.BillingAddressLine1 + "<br/>"
		        + invoiceDetails.BillingAddressLine2 + "<br/>"
		        + invoiceDetails.BillingAddressCity + " , "
		        + invoiceDetails.BillingAddressState + " "
		        + invoiceDetails.BillingAddressZip + "</p></div>";
		pdfText += "</td>";

		pdfText += "<td>";
		pdfText += "<span class='label close-span'>Remit Payment To : </span>";
		pdfText += "<div class='address-block'><p>" + companyDetails.Name
		        + "<br/>" + companyDetails.AddressLine1 + "<br/>"
		        + companyDetails.AddressLine2 + "<br/>" + companyDetails.City
		        + ", " + companyDetails.State + " " + companyDetails.Zip
		        + "</p></div>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------
		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label'>Project : </span>";
		pdfText += "<span class='value'>" + projectDetails.ProjectId
		        + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "<span class='label'>Terms : </span>";
		pdfText += "<span class='value'>" + invoiceDetails.Terms + "</span>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label'>PO Number : </span>";
		pdfText += "<span class='value'>" + invoiceDetails.PONumber + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<span class='label'>Period Beginning "
		        + formatDate(invoiceDetails.BillingFrom) + " through "
		        + formatDate(invoiceDetails.BillingTo) + "</span>";
		pdfText += "</td>";
		pdfText += "<td>";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------

		pdfText += "</table>"; // T1

		// ---------------------------------------------------
		// ---------------------------------------------------

		pdfText += "<table width='100%' class='table2'>"; // T2

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td>";
		pdfText += "<p><b>Invoice Comments : </b></p>";
		pdfText += insertInvoiceDetailsTable(invoiceDetails,invoiceId);
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "<tr>";

		pdfText += "<td class='blankDiv'>";
		pdfText += "";
		pdfText += "</td>";

		pdfText += "</tr>";

		// ---------------------------------------------------

		// pdfText += "<tr>";

		// pdfText += "<td align='center'>";

		// pdfText += "<hr width='100%'/>";

		// pdfText += "<div class='footer'>";
		// pdfText += "<p align='center'>Brillio LLC, certifies that this
		// invoice is correct, that payment has not been received and"
		// + " that it is presented with the<br/>"
		// + " understanding that the amount to be paid there under can be
		// audited to verify.</p>";
		// pdfText += "</div>";

		// pdfText += "</td>";

		// pdfText += "</tr>";

		// ---------------------------------------------------

		pdfText += "</table>"; // T2

		// ---------------------------------------------------
		// ---------------------------------------------------

		return pdfText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'mainPage', err);
		throw err;
	}
}

function insertInvoiceDetailsTable(invoiceDetails,invoiceId) {
	var f_inv_total = 1;
	if(invoiceId){
		var o_recordOBJ = nlapiLoadRecord('invoice',parseInt(invoiceId));
		var i_billable_time_count = o_recordOBJ.getLineItemCount('time');
		var i_item_count = o_recordOBJ.getLineItemCount('item');
		f_inv_total = o_recordOBJ.getFieldValue('custbody_inv_last_sales_addition');
	}
	var table = "<table width='100%' class='table2'>";

	table += "<tr class='invoice-table-header'>";
	table += "<td>Details</td>";
	table += "<td>ST Rate</td>";
	table += "<td>ST Units</td>";
	table += "<td>OT Rate</td>";
	table += "<td>OT Units</td>";
	table += "<td align=\"right\">Amount</td>";
	table += "</tr>";

	//added new logic
	if (_logValidation(i_billable_time_count)) {
		for (var i = 1; i <= i_billable_time_count; i++) {
			var i_date = o_recordOBJ.getLineItemValue('time',
			        'billeddate', i)

			var i_employee = o_recordOBJ.getLineItemValue('time',
			        'employeedisp', i)

			var i_project = o_recordOBJ.getLineItemValue('time',
			        'job', i)

			var i_item = o_recordOBJ.getLineItemValue('time',
			        'item', i)

			var i_rate = o_recordOBJ.getLineItemValue('time',
			        'rate', i)

			var i_hours = o_recordOBJ.getLineItemValue('time',
			        'quantity', i)

			var i_marked = o_recordOBJ.getLineItemValue('time',
			        'apply', i)

			/*
			 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Date
			 * --> ' + i_date);
			 * 
			 * nlapiLogExecution('DEBUG', 'suiteletFunction', '
			 * Employee --> ' + i_employee);
			 * 
			 * nlapiLogExecution('DEBUG', 'suiteletFunction', '
			 * Project --> ' + i_project);
			 * 
			 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item
			 * --> ' + i_item);
			 * 
			 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Rate
			 * --> ' + i_rate);
			 */

			// nlapiLogExecution('DEBUG', 'suiteletFunction', '
			// Hours --> ' + i_hours);
			if (i_marked == 'T') {
				var d_start_of_week = get_start_of_week(i_date)
				var d_end_of_week = get_end_of_week(i_date)

				var d_SOW_date = nlapiStringToDate(d_start_of_week);
				var d_EOW_date = nlapiStringToDate(d_end_of_week);

				d_SOW_date = get_date(d_SOW_date)

				d_EOW_date = get_date(d_EOW_date)

				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// d_SOW_date --> ' + d_SOW_date);
				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// d_EOW_date --> ' + d_EOW_date);

				if (i_item == 2222 || i_item == 2425
				        || i_item == 2221) // Amol: added 2221(FP)
				// item
				{
					a_employee_array[i_cnt++] = i_employee + '&&&'
					     //   + d_EOW_date + '&&&' + d_SOW_date
					         + i_rate;
					a_employee_name_array.push(i_employee); 
				}// Time & Material

				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// *******************Start & End Of Week
				// *************** --> [' + d_SOW_date + ' , ' +
				// d_EOW_date + ']');
			}

		}// Billable Loop
	}// Billable Line Count Loop
	
	a_employee_array = removearrayduplicate(a_employee_array)
	a_employee_array = a_employee_array.sort();
	// nlapiLogExecution('DEBUG', 'suiteletFunction', '
	// ******************* Employee Array *************** -->'
	// +a_employee_array);

	a_employee_name_array = removearrayduplicate(a_employee_name_array)
	a_employee_name_array = a_employee_name_array.sort();
	// nlapiLogExecution('DEBUG', 'suiteletFunction', '
	// ******************* Employee Name Array *************** -->'
	// +a_employee_array);
	
	var i_employee_arr_n;
	var vb = 0;
	for (var k = 0; k < a_employee_array.length; k++) {
		var i_ST_rate = 0;
		var i_OT_rate = 0;
		var i_ST_units = 0;
		var i_OT_units = 0;
		var i_hours = 0
		var i_minutes = 0;
		var i_hours_ST = 0;
		var i_minutes_ST = 0;
		var i_hours_OT = 0;
		var i_minutes_OT = 0;
		var flag = 0;

		a_split_array = a_employee_array[k].split('&&&')

		var i_employee_arr = a_split_array[0]

		//var i_end_of_week_arr = a_split_array[1]

		//var i_start_of_week_arr = a_split_array[2]

		var i_rate_emp = a_split_array[1]

		for (var lt = 1; lt <= i_billable_time_count; lt++) {
			var i_date_new = o_recordOBJ.getLineItemValue('time',
			        'billeddate', lt)

			var i_employee_new = o_recordOBJ.getLineItemValue(
			        'time', 'employeedisp', lt)

			var i_project_new = o_recordOBJ.getLineItemValue(
			        'time', 'job', lt)

			var i_item_new = o_recordOBJ.getLineItemValue('time',
			        'item', lt)

			var i_rate_new = o_recordOBJ.getLineItemValue('time',
			        'rate', lt)

			var i_hours_new = o_recordOBJ.getLineItemValue('time',
			        'quantity', lt)

			var i_marked = o_recordOBJ.getLineItemValue('time',
			        'apply', lt)
			 
			 if(parseFloat(i_rate_new) == parseFloat(i_rate_emp)){
				 
			       
				      
			  for(var jk = 1; jk<=i_item_count ; jk++){      
			      //Get Volume And Tenure Discount
				  var i_item_new_i = o_recordOBJ.getLineItemValue('item','item', jk);
				  var i_rate_new_i = o_recordOBJ.getLineItemValue('item','rate', jk);
					
					if (i_item_new_i == 2524) { //Brillio Tenure Discount
						f_tenure_disc = i_rate_new_i;
						//f_tenure_disc = Math.abs(f_tenure_disc);
						//f_tenure_disc = - f_tenure_disc;
						//f_tenure_disc = f_tenure_disc.toString();
						//f_tenure_disc = escape_special_chars(f_tenure_disc);
						i_tenure_disc_percent =  parseFloat(f_tenure_disc) / parseFloat(f_inv_total) ;
						i_tenure_disc_percent = parseFloat(i_tenure_disc_percent) * 100;
						i_tenure_disc_percent = parseFloat(i_tenure_disc_percent) * -1;
						i_tenure_disc_percent = Math.round(i_tenure_disc_percent);
						//i_tenure_disc_percent = parseInt(i_tenure_disc_percent) * -1;
						//i_tenure_disc_percent = escape_special_chars(i_tenure_disc_percent);
						i_tenure_disc_percent = i_tenure_disc_percent +'.0%'
					}
					if(i_item_new_i == 2523){ //Brillio Volume Discount
						f_volume_disc = i_rate_new_i;
						//f_volume_disc = f_volume_disc.toString();
						//f_volume_disc = escape_special_chars(f_volume_disc);
						i_volume_disc_percent =  parseFloat(f_volume_disc) / parseFloat(f_inv_total) ;
						i_volume_disc_percent = parseFloat(i_volume_disc_percent) * 100;
						i_volume_disc_percent = parseFloat(i_volume_disc_percent) * -1;
						i_volume_disc_percent = Math.round(i_volume_disc_percent);
						//i_volume_disc_percent = parseInt(i_volume_disc_percent) * -1;
						//i_volume_disc_percent = escape_special_chars(i_volume_disc_percent);
						i_volume_disc_percent = i_volume_disc_percent +'.0%'
					}  
			  }
			if (i_marked == 'T') {
				if ((i_employee_arr == i_employee_new)) {
					var d_start_of_week_new = get_start_of_week(i_date_new)
					var d_end_of_week_new = get_end_of_week(i_date_new)

					/*
					 * nlapiLogExecution('DEBUG',
					 * '-----------------------------------------------------');
					 * nlapiLogExecution('DEBUG', 'i_date_new', ' 1
					 * --> ' + i_date_new);
					 * nlapiLogExecution('DEBUG',
					 * 'suiteletFunction', ' 1 --> ' +
					 * d_SOW_date_B); nlapiLogExecution('DEBUG',
					 * 'suiteletFunction', ' 2 --> ' +
					 * d_EOW_date_B); nlapiLogExecution('DEBUG',
					 * 'i_hours_new', ' 2 --> ' + i_hours_new);
					 * nlapiLogExecution('DEBUG',
					 * '-----------------------------------------------------');
					 */

					var d_SOW_date_B = nlapiStringToDate(d_start_of_week_new);
					var d_EOW_date_B = nlapiStringToDate(d_end_of_week_new);

					/*
					 * nlapiLogExecution('DEBUG',
					 * 'suiteletFunction', '3 --> ' + d_SOW_date_B);
					 * nlapiLogExecution('DEBUG',
					 * 'suiteletFunction', ' 4 --> ' +
					 * d_EOW_date_B);
					 * 
					 */
					d_SOW_date_B = get_date(d_SOW_date_B)
					d_EOW_date_B = get_date(d_EOW_date_B)

					/*
					 * nlapiLogExecution('DEBUG',
					 * 'suiteletFunction',' 5 --> ' + d_SOW_date_B);
					 * nlapiLogExecution('DEBUG',
					 * 'suiteletFunction', ' 6--> ' + d_EOW_date_B);
					 */

					//if ((d_SOW_date_B == i_start_of_week_arr)
					//        && (d_EOW_date_B == i_end_of_week_arr)
					//        && (i_rate_emp == i_rate_new)) 
					{

						/*
						 * nlapiLogExecution('DEBUG',
						 * '-----------------------------------------------------');
						 * nlapiLogExecution('DEBUG', 'i_date_new', '
						 * 1 --> ' + i_date_new);
						 * nlapiLogExecution('DEBUG',
						 * 'suiteletFunction', ' 1 --> ' +
						 * d_SOW_date_B); nlapiLogExecution('DEBUG',
						 * 'suiteletFunction', ' 2 --> ' +
						 * d_EOW_date_B); nlapiLogExecution('DEBUG',
						 * 'i_hours_new', ' 2 --> ' + i_hours_new);
						 * nlapiLogExecution('DEBUG', 'i_item_new', '
						 * 2 --> ' + i_item_new);
						 * nlapiLogExecution('DEBUG',
						 * '-----------------------------------------------------');
						 */

						if (flag != i_employee_arr) {
							i_employee_arr_n = i_employee_arr;
						} else {
							i_employee_arr_n = '';
						}
						var a_split_time_array = new Array();
						a_split_time_array = i_hours_new.split(':')
						var hours = a_split_time_array[0]
						var minutes = a_split_time_array[1]

						if (minutes == '0.00' || minutes == '0'
						        || minutes == 0) {
						} else {
							minutes = parseFloat(minutes) / 60;
							minutes = parseFloat(minutes)
							        .toFixed(2);
							// nlapiLogExecution('DEBUG', 'minutes',
							// ' ------ --> ' + minutes);
						}

						if (i_item_new == 2222
						        || i_item_new == 2221) // Amol:
						// added
						// 2221(FP)
						// item
						{
							i_hours_ST = parseFloat(i_hours_ST)
							        + parseFloat(hours)
							// i_minutes_ST =
							// parseFloat(i_minutes_ST) +
							// parseFloat(minutes);
							if (minutes == '0.75'
							        || minutes == '0.25') {
								// nlapiLogExecution('DEBUG',
								// 'i_minutes_ST', ' ------ --> ' +
								// i_minutes_ST);
								// nlapiLogExecution('DEBUG',
								// 'i_hours_ST', ' ------ --> ' +
								// i_hours_ST);
							}

							// i_minutes_ST=parseFloat(i_minutes_ST).toFixed(2);

							// i_minutes_ST=(parseFloat(i_minutes_ST)/60).toFixed(2);
							// nlapiLogExecution('DEBUG',
							// 'i_minutes_ST', ' ------ --> ' +
							// i_minutes_ST);

							i_hours_ST = parseFloat(i_hours_ST)
							        + parseFloat(minutes);
							i_hours_ST = parseFloat(i_hours_ST)
							        .toFixed(2);

							i_ST_rate = parseFloat(i_hours_ST)
							        * parseFloat(i_rate_new)
							i_ST_units = parseFloat(i_rate_new)
							        .toFixed(2);

						}// Time & Material

						if (i_item_new == 2425) {
							i_hours_OT = parseFloat(i_hours_OT)
							        + parseFloat(hours)
							// i_minutes_OT =
							// parseFloat(i_minutes_OT) +
							// parseFloat(minutes);

							// i_minutes_OT=parseFloat(i_minutes_OT).toFixed(2);

							// i_minutes_OT=(parseFloat(i_minutes_OT)/60).toFixed(2);

							i_hours_OT = parseFloat(i_hours_OT)
							        + parseFloat(minutes);

							i_hours_OT = parseFloat(i_hours_OT)
							        .toFixed(2);
							i_OT_rate = parseFloat(i_hours_OT);
							i_OT_units = parseFloat(i_rate_new)
							        .toFixed(2);

							/*
							 * nlapiLogExecution('DEBUG',
							 * '---------------------------------------------------');
							 * nlapiLogExecution('DEBUG', 'hours', ' -
							 * --> ' + hours);
							 * nlapiLogExecution('DEBUG',
							 * 'd_SOW_date_B', ' - --> ' +
							 * d_SOW_date_B);
							 * nlapiLogExecution('DEBUG',
							 * 'd_EOW_date_B', ' - --> ' +
							 * d_EOW_date_B);
							 * nlapiLogExecution('DEBUG',
							 * '---------------------------------------------------');
							 */

						}// OT Item
						
						
						// nlapiLogExecution('DEBUG',
						// 'suiteletFunction', ' 7 --> ' +
						// d_SOW_date_B);

					}// Start & End Date Validation
				}// Employee / Project / Item

			}// Marked T
		  }	
		}// Billable Count

		i_amount = (parseFloat(i_hours_ST) * parseFloat(i_ST_units))
		        + (parseFloat(i_hours_OT) * parseFloat(i_OT_units));
		i_amount = parseFloat(i_amount);
		i_amount = i_amount.toFixed(2);

		//f_without_disc_total = parseFloat(f_without_disc_total) + parseFloat(i_amount);
		
		i_total_amount_INV = parseFloat(i_total_amount_INV)
		        + parseFloat(i_amount);
		i_total_amount_INV = parseFloat(i_total_amount_INV);
		i_total_amount_INV = i_total_amount_INV.toFixed(2);

		var s_employee_addr_txt = '';
		s_employee_addr_txt = escape_special_chars(i_employee_arr);

		
		table += "<tr>";
		table += "<td><\/td>";
		table += "</tr>";
		
		table += "<tr>";
		if (flag == 0 && flag_name != i_employee_arr) {
			if (a_employee_name_array.indexOf(i_employee_arr) != -1) {
				vb++;
				// strVar += " <td border-bottom=\"0\"
				// border-left=\"0\" font-family=\"Helvetica\"
				// align=\"left\"
				// font-size=\"8\">"+parseInt(vb)+"<\/td>";

				//table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
				//        + s_employee_addr_txt + "<\/td>";
				table += "<td>"+ s_employee_addr_txt + "<\/td>";
				flag = 1;
				flag_name = i_employee_arr
			}

		} else {
			// strVar += " <td border-bottom=\"0\" border-left=\"0\"
			// font-family=\"Helvetica\" align=\"left\"
			// font-size=\"8\"><\/td>";
			//table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
			table += "<td><\/td>";
		}

		// nlapiLogExecution('DEBUG', 'i_hours_OT', ' - --> ' +
		// i_hours_OT);

		//table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
		//        + i_end_of_week_arr + "<\/td>";
		/*table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
		        + parseFloat(i_ST_units) + "<\/td>";
		table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
		        + parseFloat(i_hours_ST) + "<\/td>";
		table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
		        + parseFloat(i_OT_units) + "<\/td>";// i_hours_OT//i_OT_rate
		table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
		        + parseFloat(i_hours_OT) + "<\/td>";// i_OT_units
		table += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
		        + i_amount + "<\/td>";*/
		
		table += "<td>"+ parseFloat(i_ST_units) + "<\/td>";
		table += "<td>"+ parseFloat(i_hours_ST) + "<\/td>";
		table += "<td>"+ parseFloat(i_OT_units) + "<\/td>";// i_hours_OT//i_OT_rate
		table += "<td>"+ parseFloat(i_hours_OT) + "<\/td>";// i_OT_units
		table += "<td align='right' class='underline-cell'>( "
	        + formatCurrency(i_amount) + " )</td>";
		table += "<\/tr>";

		pagecount++;

		if (pagecount == linenum && a_employee_array.length != '17')// if(pagecount
		// ==
		// '17'
		// &&
		// a_employee_array.length!=pagecount)
		{
			pagecount = 0;
			linenum = 40;
			table += "<\/table>";
			table += " <p style=\"page-break-after: always\"><\/p>";

			table += "<table border=\"0\" width=\"100%\">";
			// ----------------------------------------------------------

			table += "<tr>";
			// strVar += "<td colspan=\"1\" border-left=\"0\"
			// border-top=\"0.3\" border-bottom=\"0\"
			// font-family=\"Helvetica\" align=\"center\"
			// font-size=\"9\"></td>";
			table += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount("
			        + s_currency_symbol + ")<\/b></td>";
			table += "</tr>";

			table += "<tr>";
			// strVar += "<td colspan=\"1\" border-left=\"0\"
			// border-top=\"0\" border-bottom=\"0.3\"
			// align=\"center\" font-family=\"Helvetica\"
			// font-size=\"9\"><b>SL#<\/b></td>";
			table += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
			table += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
			table += "</tr>";
			// ----------------------------------------------------------
		}
	}// Loop
	
	/*table += "<tr class='invoice-table-cell'>";
	table += "<td><b>" + invoiceDetails.EmployeeName + "</b></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "</tr>";

	table += "<tr>";
	table += "<td></td>";
	table += "<td>" + formatCurrency(invoiceDetails.ST_Rate) + "</td>";
	table += "<td>" + invoiceDetails.ST_Unit + "</td>";
	table += "<td>" + formatCurrency(invoiceDetails.OT_Rate) + "</td>";
	table += "<td>" + invoiceDetails.OT_Unit + "</td>";
	table += "<td align='right'>" + formatCurrency(invoiceDetails.GrossAmount)
	        + "</td>";
	table += "</tr>";*/

	/*table += "<tr>";
	table += "<td>Less : VMS Fee Discount</td>";
	table += "<td>" + invoiceDetails.FixedFeeDiscountPercent + "</td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right'>( "
	        + formatCurrency(invoiceDetails.FixedFeeDiscountAmount) + " )</td>";
	table += "</tr>";*/
	
	/*table += "<tr>";
	table += "<td><b>Total</b></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right'><b>"+ formatCurrency(i_total_amount_INV) + "</b></td>";
	table += "</tr>";*/
	
	table += "<tr>";
	table += "<td><\/td>";
	table += "</tr>";
	
	table += "<tr>";
	table += "<td>Less : Tenure Discount</td>";
	if(i_tenure_disc_percent)
	table += "<td>" + (i_tenure_disc_percent) + "</td>";	
	else	
	table += "<td>" + invoiceDetails.TenureDiscountPercent + "</td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	if(f_tenure_disc)
		table += "<td align='right' class='underline-cell'>( "+ formatCurrency(f_tenure_disc) + " )</td>";
	else
	table += "<td align='right' class='underline-cell'>( "+ formatCurrency(invoiceDetails.TenureDiscountAmount) + " )</td>";
	table += "</tr>";

	/*table += "<tr>";
	table += "<td><b>Total</b></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td align='right'><b>"+ formatCurrency(i_total_amount_INV) + "</b></td>";
	table += "</tr>";*/

	table += "<tr>";
	table += "<td>Less : Volume Discount</td>";
	if(i_volume_disc_percent)
		table += "<td>" + (i_volume_disc_percent) + "</td>";
		else
	table += "<td>" + invoiceDetails.VolumeDiscountPercent + "</td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	if(f_volume_disc)
		table += "<td align='right' class='underline-cell'>( "+ formatCurrency(f_volume_disc) + " )</td>";
	else
	table += "<td align='right' class='underline-cell'>( "+ formatCurrency(invoiceDetails.VolumeDiscountAmount) + " )</td>";
	table += "</tr>";

	table += "<tr class='invoice-table-footer'>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td></td>";
	table += "<td>Invoice Total</td>";
	table += "<td></td>";
	table += "<td align='right'>"
	        + formatCurrency(o_recordOBJ.getFieldValue('total')) + "</td>";
	table += "</tr>";

	table += "</table>";

	return table;
}

function getCompanyDetails() {
	try {
		var subsidiaryId = 2;
		var subsidiaryRecord = nlapiLoadRecord('subsidiary', subsidiaryId);

		return {
		    LogoUrl : constant.Images.CompanyLogo,
		    Name : subsidiaryRecord.getFieldValue('addressee'),
		    AddressLine1 : subsidiaryRecord.getFieldValue('addr1'),
		    AddressLine2 : subsidiaryRecord.getFieldValue('addr2'),
		    City : subsidiaryRecord.getFieldValue('city'),
		    State : subsidiaryRecord.getFieldValue('state'),
		    Zip : subsidiaryRecord.getFieldValue('zip'),
		    Phone : subsidiaryRecord.getFieldValue('addrphone'),
		    Fax : subsidiaryRecord.getFieldValue('fax')
		};
	} catch (err) {
		nlapiLogExecution('error', 'getCompanyDetails', err);
		throw err;
	}
}

function getInvoiceDetails(invoiceId) {
	try {
		var invoiceRecord = nlapiLoadRecord('invoice', invoiceId);
		var discountRecordId = invoiceRecord
		        .getFieldValue('custbody_inv_cdd_temp_field');
		var discountRecord = nlapiLoadRecord(
		        'customrecord_customer_discount_details', discountRecordId);
		var employeeId = null;

		if (discountRecord) {
			employeeId = discountRecord
			        .getFieldValue('custrecord_cdd_employee')
		}

		if (!employeeId) {
			employeeId = invoiceRecord.getLineItemValue('time', 'employee', 1);
		}

		var employeeDetails = nlapiLookupField('employee', employeeId, [
		        'firstname', 'lastname' ]);

		return {
		    Customer : invoiceRecord.getFieldValue('entity'),
		    Project : invoiceRecord.getFieldValue('job'),
		    InvoiceDate : invoiceRecord.getFieldValue('trandate'),
		    InvoiceNumber : invoiceRecord.getFieldValue('tranid'),
		    Terms : '1% 15 net 30',
		    PONumber : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('otherrefnum')),
		    BillingFrom : invoiceRecord.getFieldValue('custbody_billfrom'),
		    BillingTo : invoiceRecord.getFieldValue('custbody_billto'),
		    EmployeeName : employeeDetails.firstname + " "
		            + employeeDetails.lastname,
		    ST_Rate : discountRecord.getFieldValue('custrecord_cdd_st_amount'),
		    ST_Unit : discountRecord.getFieldValue('custrecord_cdd_st_hours'),
		    OT_Rate : discountRecord.getFieldValue('custrecord_cdd_ot_amount'),
		    OT_Unit : discountRecord.getFieldValue('custrecord_cdd_ot_hours'),
		    GrossAmount : discountRecord
		            .getFieldValue('custrecord_cdd_gross_amount'),
		    FixedFeeDiscountPercent : discountRecord
		            .getFieldValue('custrecord_cdd_fixed_fee_percent'),
		    FixedFeeDiscountAmount : discountRecord
		            .getFieldValue('custrecord_cdd_fixed_fee_discount'),
		    TenureDiscountPercent : discountRecord
		            .getFieldValue('custrecord_cdd_tenure_disc_percent'),
		    TenureDiscountAmount : discountRecord
		            .getFieldValue('custrecord_cdd_tenure_discount'),
		    VolumeDiscountPercent : discountRecord
		            .getFieldValue('custrecord_cdd_volume_discount_percent'),
		    VolumeDiscountAmount : discountRecord
		            .getFieldValue('custrecord_cdd_volume_discount'),
		    AmountLessVmsTenure : discountRecord
		            .getFieldValue('custrecord_cdd_gross_amount')
		            - discountRecord
		                    .getFieldValue('custrecord_cdd_fixed_fee_discount')
		            - discountRecord
		                    .getFieldValue('custrecord_cdd_tenure_discount'),
		    FinalInvoiceAmount : discountRecord
		            .getFieldValue('custrecord_cdd_final_invoice_amount'),
		    BillingAddress : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddress')),
		    Terms : nlapiEscapeXML(invoiceRecord.getFieldText('terms')),
		    BillingAddressLine1 : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddr1')),
		    BillingAddressLine2 : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddr2')),
		    BillingAddressLine3 : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billaddr2')),
		    BillingAddressCity : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billcity')),
		    BillingAddressState : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billstate')),
		    BillingAddressZip : nlapiEscapeXML(invoiceRecord
		            .getFieldValue('billzip'))
		};
	} catch (err) {
		nlapiLogExecution('error', 'getInvoiceDetails', err);
		throw err;
	}
}

function getCustomerDetails(customerId) {
	try {
		var customerDetailValue = nlapiLookupField('customer', customerId, [
		        'altname', 'email' ]);

		return {
		    Name : customerDetailValue.altname,
		    Email : nlapiEscapeXML(customerDetailValue.email)
		};
	} catch (err) {
		nlapiLogExecution('error', 'getCustomerDetails', err);
		throw err;
	}
}

function getProjectDetails(projectId) {
	try {
		var projectDetailsValue = nlapiLookupField('job', projectId,
		        [ 'entityid' ]);

		return {
			ProjectId : projectDetailsValue.entityid
		};
	} catch (err) {
		nlapiLogExecution('error', 'getProjectDetails', err);
		throw err;
	}
}

function formatCurrency(value) {
	value = parseFloat(value);
	return nlapiEscapeXML("$" + " "
	        + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
}

function formatDateText(value) {
	value = nlapiStringToDate(value);

	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var newDate = month[value.getMonth()] + " " + value.getDate() + ", "
	        + value.getFullYear();

	return newDate;
}

function formatDate(value) {
	return value;
	value = nlapiStringToDate(value);

	var mm = parseInt(value.getMonth()) + 1;
	if (mm < 9) {
		mm = "0" + mm;
	}

	var dd = parseInt(value.getDate());
	if (dd < 9) {
		dd = "0" + dd;
	}

	var yy = value.getFullYear();
	yy = yy.toString().substring(2, 4);

	var newDate = mm + "-" + dd + "-" + yy;

	return newDate;
}
function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}
function _logValidation(value) {
	if (value != 'null' && value != null && value.toString() != null
	        && value != '' && value != undefined
	        && value.toString() != undefined && value != 'undefined'
	        && value.toString() != 'undefined' && value.toString() != 'NaN'
	        && value != NaN) {
		return true;
	} else {
		return false;
	}
}
function get_start_of_week(date) {

	// nlapiLogExecution('DEBUG','date',' date -->'+date);
	// If no date object supplied, use current date
	// Copy date so don't modify supplied date
	var now = nlapiStringToDate(date)

	// set time to some convenient value
	now.setHours(0, 0, 0, 0);

	if (now.getDay() == '0') {

		// Get the previous Monday
		var monday = new Date(now);
		monday.setDate(monday.getDate() - 7 + 1);
		monday = nlapiDateToString(monday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> '
		// + monday);

		// Get next Sunday
		var sunday = new Date(now);
		sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
		sunday = nlapiDateToString(sunday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> '
		// + sunday);
		// Return array of date objects
		// nlapiLogExecution('DEBUG','date',' date -->'+date);
		// nlapiLogExecution('DEBUG','monday',' monday -->'+monday);

	} else {

		// Get the previous Monday
		var monday = new Date(now);
		monday.setDate(monday.getDate() - monday.getDay() + 1);
		monday = nlapiDateToString(monday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> '
		// + monday);

		// Get next Sunday
		var sunday = new Date(now);
		sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
		sunday = nlapiDateToString(sunday)

	}
	// nlapiLogExecution('DEBUG','date',' date -->'+date);
	// nlapiLogExecution('DEBUG','now.getDay()',now.getDay());
	return monday;
}

function get_end_of_week(date) {
	// If no date object supplied, use current date
	// Copy date so don't modify supplied date
	var now = nlapiStringToDate(date)

	// set time to some convenient value
	now.setHours(0, 0, 0, 0);
	var monday = new Date(now);
	var lastday = new Date(new Date(now).getFullYear(), new Date(now)
	        .getMonth() + 1, 0)
	// nlapiLogExecution('DEBUG','lastday',lastday.getDate());
	// nlapiLogExecution('DEBUG','monday.getDate()',monday.getDate());
	var f = parseInt(monday.getDate());
	var f1 = parseInt(lastday.getDate());
	// ----------------------------------------------------------------------
	if (f == f1) {
		var sunday = date;

		// nlapiLogExecution('DEBUG','inside if');
	} else {
		// nlapiLogExecution('DEBUG','now.getDay()', now.getDay());
		if (now.getDay() == '0') {
			// Get the previous Monday

			monday.setDate(monday.getDate() - monday.getDay() + 1);
			monday = nlapiDateToString(monday)

			// Get next Sunday
			var sunday = new Date(now);
			sunday.setDate(sunday.getDate() - 7 + 7);
			sunday = nlapiDateToString(sunday)

		} else {
			// Get the previous Monday
			// var monday = new Date(now);
			monday.setDate(monday.getDate() - monday.getDay() + 1);
			monday = nlapiDateToString(monday)
			// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday
			// --> ' + monday);

			// Get next Sunday
			var sunday = new Date(now);
			sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
			// nlapiLogExecution('DEBUG','sunday', ' sunday --> ' + sunday);
			// ------------------------------------------------------------------
			if (sunday > lastday) {
				// nlapiLogExecution('DEBUG','inside second if');
				sunday = lastday;

			}
			// -----------------------------------------------------------------
			sunday = nlapiDateToString(sunday)

		}
		// --------------------------------------------------------------------------------------

	}
	return sunday;
}

function get_date(date1) {
	var today;
	// ============================= Todays Date
	// ==========================================

	var offsetIST = 5.5;

	// To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime()
	        + (date1.getTimezoneOffset() * 60000));

	// Then cinver the UTS date to the required time zone offset like back to
	// 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));

	var day = istdate.getDate();

	var month = istdate.getMonth() + 1;

	var year = istdate.getFullYear();

	var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format =='
	// +date_format);

	if (month == 1) {
		month = '01'
	} else if (month == 2) {
		month = '02'
	} else if (month == 3) {
		month = '03'
	}

	else if (month == 4) {
		month = '04'
	} else if (month == 5) {
		month = '05'
	} else if (month == 6) {
		month = '06'
	} else if (month == 7) {
		month = '07'
	} else if (month == 8) {
		month = '08'
	} else if (month == 9) {
		month = '09'
	}
	if (day == 1) {
		day = '01'
	} else if (day == 2) {
		day = '02'
	} else if (day == 3) {
		day = '03'
	}

	else if (day == 4) {
		day = '04'
	} else if (day == 5) {
		day = '05'
	} else if (day == 6) {
		day = '06'
	} else if (day == 7) {
		day = '07'
	} else if (day == 8) {
		day = '08'
	} else if (day == 9) {
		day = '09'
	}
	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {
		today = month + '/' + day + '/' + year;
	}
	return today;
}

function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function escape_special_chars(text) {
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.1234567890";
	var text_new = ''

	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			if (iChars.indexOf(text[y]) == -1) {
				text_new += text[y]
			}

		}// Loop
	}// Text Validation
	return text_new;
}