							
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : FUEL | UES Region table Notification
	Author      : Ashish Pandit
	Date        : 17 Sept 2019
    Description : User Event to update MariaDB on Field change of Region   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

function afterSubmitCheckFields(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'create')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var i_regionHead = recordObject.getFieldValue('custrecord_region_head');
			var s_regionName = recordObject.getFieldValue('name');

			var body = {};
			var method;
			method = "POST";
			if(i_regionHead)
				body.emailId = nlapiLookupField("employee", i_regionHead, "email"); 
			else
				body.emailId = "";
			body.regionName = s_regionName; 
			
			var url = "https://fuelnode1.azurewebsites.net/region";
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecord = nlapiGetOldRecord();
			var i_OldRegionHead = oldRecord.getFieldValue('custrecord_region_head');
			var s_OldRegionName = oldRecord.getFieldValue('name');
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var i_NewRegionHead = recordObject.getFieldValue('custrecord_region_head');
			var s_NewRegionName = recordObject.getFieldValue('name');
			nlapiLogExecution('debug','i_OldRegionHead '+i_OldRegionHead,'i_NewRegionHead '+i_NewRegionHead);
			if(i_OldRegionHead != i_NewRegionHead || s_OldRegionName != s_NewRegionName)
			{
				var body = {};
				var method;
				method = "POST";
				if(i_NewRegionHead)
					body.emailId = nlapiLookupField("employee", i_NewRegionHead, "email"); 
				else
					body.emailId = "";
				body.regionName = s_NewRegionName; 
				
				var url = "https://fuelnode1.azurewebsites.net/region";
				nlapiLogExecution('debug','body  in edit ',JSON.stringify(body));
				body = JSON.stringify(body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
function beforeSubmitCheckFields(type)
{
	 if(type == 'delete')
	{
		try
		{
			var i_recordId = nlapiGetRecordId();
			var s_regionName = nlapiGetFieldValue('name');
			var body = {};
			var method = "DELETE";
			//body.regionName = nlapiGetFieldValue('name'); 
			var url = "https://fuelnode1.azurewebsites.net/region/regionName/"+s_regionName;
			//body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
// END BEFORE LOAD ====================================================


function _logValidation(value) 
{
 if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
// END FUNCTION =====================================================
