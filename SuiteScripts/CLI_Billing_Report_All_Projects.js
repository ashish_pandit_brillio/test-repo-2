// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: CLI_Daywise_for_month
     Author: Vikrant
     Company: Aashna
     Date: 14-10-2014
     Description: Script to export the report for time sheet entered for Managers project wise
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================


function export_CSV(param)//
{
    try//
    {
        //alert(param);
        //alert('s_start_date : ' + param.split('@')[0] + ', s_project : ' + param.split('@')[1]);
        
        var createPDFURL = nlapiResolveURL("SUITELET", 'customscript_sut_billing_report_all_proj', 'customdeploy1', false);
        
        //pass the internal id of the current record
        createPDFURL += '&id=' + 'Export' + "&param=" + param;
        // alert(createPDFURL);
        
        //show the PDF file 
        newWindow = window.open(createPDFURL);
        
		return;
		
        var s_start_date = param.split('@')[0];
        var s_project = param.split('@')[1];
        s_start_date = nlapiStringToDate(s_start_date);
        
        var i_month = s_start_date.getMonth() + 1;
        var i_year = s_start_date.getFullYear();
        
        s_start_date = '01/' + i_month + '/' + i_year;
        
        s_start_date = nlapiStringToDate(s_start_date);
        var s_end_date = nlapiAddMonths(s_start_date, 1);
        s_end_date = nlapiAddDays(s_end_date, -1);
        
        //alert('s_end_date : ' + s_end_date + ', s_start_date : ' + s_start_date);
        
        var one_day = 1000 * 60 * 60 * 24;
        var i_start_date = s_start_date.getTime();
        var i_end_date = s_end_date.getTime();
        
        var i_date_diff = i_end_date - i_start_date;
        
        var i_days_diff = Math.round(i_date_diff / one_day); //s_end_date - s_start_date;
        i_date_diff = i_date_diff + 2;
        //alert('i_days_diff : ' + i_days_diff);
        
        var a_day_list = new Array();
        
        for (var i_counter = 0; i_counter < i_days_diff; i_counter++) //
        {
            a_day_list[i_counter] = i_counter + 1;
        }
        
        var a_project_activity = new Array(i_days_diff);
        var a_leave = new Array(i_days_diff);
        var a_holiday = new Array(i_days_diff);
        var a_ot = new Array(i_days_diff);
        
        var a_project_activity_total = new Array(i_days_diff);
        var a_leave_total = new Array(i_days_diff);
        var a_holiday_total = new Array(i_days_diff);
        var a_ot_total = new Array(i_days_diff);
        
        var i_PA_month_total = 0; // Monthly total hours for Project Activities task 
        var i_OT_month_total = 0; // Monthly total hours for OT task
        var i_LEAVE_month_total = 0; // Monthly total hours for Leave task
        var i_HOLIDAY_month_total = 0; // Monthly total hours for Holiday task
        var CSV_String = 'Project Name,' + s_project + '\n';
        CSV_String += ',,,' + a_day_list + '\n';
        
        //alert(CSV_String);
        return;
        
        var i_lastInternalID = 0; // used for looping when records are greater than 1000
        var previousInternalID = '';
        
        var temp_emp = '';
        var temp_inc_id = '';
        var temp_fusion_id = '';
        var emp = '';
        var date = '';
        var hrs = 0;
        
        var context = nlapiGetContext();
        var remaining_usage = context.getRemainingUsage();
        
        if (_validate(s_start_date)) //
        {
            var counter = 1;
            
            do //
            {
                //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'search_result_0.length : ' + search_result_0.length);
                
                var filters = new Array();
                filters[filters.length] = new nlobjSearchFilter('date', null, 'within', s_start_date, s_end_date);
                filters[filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', i_lastInternalID);
                //filters[filters.length] = new nlobjSearchFilter('employee', null, 'is', emp_id_0);
                nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_lastInternalID : ' + i_lastInternalID);
                
                var search_result = nlapiSearchRecord('timebill', 'customsearch_timesheet_report_llc_month', filters, null);
                
                if (_validate(search_result)) //
                {
                    alert('Into search_result : ' + search_result.length);
                    
                    var trandate = ''; //result.getValue(all_column[0]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'trandate : ' + trandate);
                    
                    var line_count = search_result.length;
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'line_count : ' + line_count);
                    
                    var s_task_name = ''; //result.getValue(all_column[3]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 's_task_name : ' + s_task_name);
                    
                    var i_hrs = ''; //result.getValue(all_column[4]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'i_hrs : ' + i_hrs);
                    
                    var item = ''; //result.getValue(all_column[5]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'item : ' + item);
                    
                    var item_name = ''; //result.getText(all_column[5]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'item_name : ' + item_name);
                    
                    var fusion_ID = ''; //result.getValue(all_column[6]);
                    //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'fusion_ID : ' + fusion_ID);
                    
                    var i_emp_id = '';
                    var s_emp_name = '';
                    var s_is_approved = '';
                    
                    // variable declaration was here earlier
                    
                    var result = search_result[0];
                    var all_column = result.getAllColumns();
                    
                    for (var i = 0; i < line_count; i++) //
                    {
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', '********* New Row ************ ' + counter++);
                        //previousInternalID = i_lastInternalID;
                        
                        result = search_result[i];
                        //var all_column = result.getAllColumns();//
                        
                        var text = '';
                        var value = '';
                        var label = '';
                        
                        for (var j_counter = 0; j_counter < all_column.length; j_counter++) //
                        {
                            text = result.getText(all_column[j_counter]);
                            value = result.getValue(all_column[j_counter]);
                            label = all_column[j_counter].getLabel();
                            
                            if (label == 'date') //
                            {
                                trandate = value;
                            }
                            
                            if (label == 'emp_id') //
                            {
                                i_emp_id = value;
                            }
                            
                            if (label == 'employee') //
                            {
                                s_emp_name = text;
                            }
                            
                            if (label == 'Task Name') //
                            {
                                s_task_name = text;
                            }
                            
                            if (label == 'duration') //
                            {
                                i_hrs = value;
                            }
                            
                            if (label == 'is_approved') //
                            {
                                s_is_approved = value;
                            }
                        }
                        
                        if (i == 0) // For the first data row
                        {
                            temp_emp = i_emp_id;
                            //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'temp_emp : ' + temp_emp);
                        }
                        
                        // from the next data row
                        i_lastInternalID = temp_emp; // to get the last employee id for next search of 1000 records
                        emp = i_emp_id;
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'emp : ' + emp);
                        
                        if (temp_emp != emp) // if employee changes then put all values to CSV String and make all variable to initial value
                        {
                            CSV_String += s_emp_name + ',ST,' + a_project_activity + '\n';
                            CSV_String += '' + ',OT,' + a_ot + '\n';
                            CSV_String += '' + ',Leave,' + a_leave + '\n';
                            CSV_String += '' + ',Holiday,' + a_holiday + '\n';
                            
                            a_project_activity = new Array(i_days_diff);
                            a_leave = new Array(i_days_diff);
                            a_holiday = new Array(i_days_diff);
                            a_ot = new Array(i_days_diff);
                            
                            export_File(CSV_String);
                            
                            return;
                        }
                        
                        trandate = nlapiStringToDate(trandate);
                        alert('trandate : ' + trandate);
                        
                        var i_day_of_month = trandate.getDate();
                        alert('i_day_of_month : ' + i_day_of_month);
                        
                        i_day_of_month = i_day_of_month - 1;
                        alert('i_day_of_month : ' + i_day_of_month);
                        
                        if (s_task_name == 'Project Activities') //
                        {
                            var i_hrs = a_project_activity[i_day_of_month];
                            
                            if (!isNaN(i_hrs)) // if thr is some value in array on given day
                            {
                                i_hrs = parseFloat(i_hrs) + parseFloat(a_project_activity[i_day_of_month]);
                                a_project_activity[i_day_of_month] = i_hrs;
                            }
                            else // if hrs are blank in array
                            {
                                a_project_activity[i_day_of_month] = i_hrs;
                            }
                        }
                        
                        if (s_task_name == 'OT') //
                        {
                            var i_hrs = a_ot[i_day_of_month];
                            
                            if (!isNaN(i_hrs)) //
                            {
                                i_hrs = parseFloat(i_hrs) + parseFloat(a_ot[i_day_of_month]);
                                a_ot[i_day_of_month] = i_hrs;
                            }
                            else // if hrs are blank in array
                            {
                                a_ot[i_day_of_month] = i_hrs;
                            }
                        }
                        
                        if (s_task_name == 'leave') //
                        {
                            var i_hrs = a_leave[i_day_of_month];
                            
                            if (!isNaN(i_hrs)) //
                            {
                                i_hrs = parseFloat(i_hrs) + parseFloat(a_leave[i_day_of_month]);
                                a_leave[i_day_of_month] = i_hrs;
                            }
                            else // if hrs are blank in array
                            {
                                a_leave[i_day_of_month] = i_hrs;
                            }
                        }
                        
                        if (s_task_name == 'holiday') //
                        {
                            var i_hrs = a_holiday[i_day_of_month];
                            
                            if (!isNaN(i_hrs)) //
                            {
                                i_hrs = parseFloat(i_hrs) + parseFloat(a_holiday[i_day_of_month]);
                                a_holiday[i_day_of_month] = i_hrs;
                            }
                            else // if hrs are blank in array
                            {
                                a_holiday[i_day_of_month] = i_hrs;
                            }
                        }
                        
                        remaining_usage = context.getRemainingUsage();
                        //nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'remaining_usage : ' + remaining_usage);
                    
                    } // end for loop
                } // end of validate search result
                else //
                {
                    nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'search_result is invalid : ' + search_result);
                }
            } // end of Do
            //while (search_result_0 != null)
            while (search_result != null)
            {
                //return CSV_String;
            }
            
            nlapiLogExecution('DEBUG', 'CSV_Data', 'After while loop !!!');
        }
        else //
        {
            nlapiLogExecution('DEBUG', 'SUT_Timesheet_Report', 'from_date is invalid : ' + from_date);
        }
    } //
catch (ex) //
    {
        alert('Exception : ' + ex.message)
        nlapiLogExecution('ERROR', 'exception', ex);
        nlapiLogExecution('ERROR', 'exception', ex.message);
    }
}

function export_File(CSV_String) //
{
    // File export
    var fileName = 'TS_Report_'
    var Datetime = new Date();
    var CSVName = fileName + " - " + Datetime + '.csv';
    //nlapiLogExecution('DEBUG', 'CSV_Data', 'CSV_DATA : ' + CSV_String);
    
    var file = nlapiCreateFile(CSVName, 'CSV', CSV_String.toString());
    
    //window.open(file.getValue());
    
    //file.setFolder(635);
    //var file_id = nlapiSubmitFile(file);
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'file_id ' + file_id);
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    //response.setContentType('CSV', CSVName);
    response.setContentType('CSV', CSVName, 'inline');
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'file.getValue() ' + file.getValue());
    response.write(file.getValue());

}

function _validate(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined')//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

function get_Formated_Date(d_date) //
{
    var today = new Date(d_date);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    
    d_date = mm + '/' + dd + '/' + yyyy;
    
    return d_date;
}

function get_todays_date(d_date)//
{
    var today;
    // ============================= Todays Date ==========================================
    
    var date1 = new Date();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);
    
    date1 = d_date;
    
    var offsetIST = 5.5;
    
    //To convert to UTC datetime by subtracting the current Timezone offset
    var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);
    
    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
    var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);
    
    var day = istdate.getDate();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);
    
    var month = istdate.getMonth() + 1;
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);
    
    var year = istdate.getFullYear();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' + year);
    
    var date_format = checkDateFormat();
    //nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format ==' + date_format);
    
    if (date_format == 'YYYY-MM-DD') {
        today = year + '-' + month + '-' + day;
    }
    if (date_format == 'DD/MM/YYYY') {
        today = day + '/' + month + '/' + year;
    }
    if (date_format == 'MM/DD/YYYY') {
        today = month + '/' + day + '/' + year;
    }
    var s_month = '';
    
    if (month == 1) {
        s_month = 'Jan'
        
    }
    else 
        if (month == 2) {
            s_month = 'Feb'
            
        }
        else 
            if (month == 3) {
            
                s_month = 'Mar'
            }
            else 
                if (month == 4) {
                    s_month = 'Apr'
                    
                }
                else 
                    if (month == 5) {
                        s_month = 'May'
                        
                    }
                    else 
                        if (month == 6) {
                        
                            s_month = 'Jun'
                        }
                        else 
                            if (month == 7) {
                                s_month = 'Jul'
                                
                            }
                            else 
                                if (month == 8) {
                                    s_month = 'Aug'
                                    
                                }
                                else 
                                    if (month == 9) {
                                        s_month = 'Sep'
                                        
                                    }
                                    else 
                                        if (month == 10) {
                                            s_month = 'Oct'
                                            
                                        }
                                        else 
                                            if (month == 11) {
                                            
                                                s_month = 'Nov'
                                            }
                                            else 
                                                if (month == 12) {
                                                
                                                    s_month = 'Dec'
                                                }
    
    if (date_format == 'DD-Mon-YYYY') {
        today = day + '-' + s_month + '-' + year;
    }
    
    
    return today;
}


// BEGIN PAGE INIT ==================================================

function pageInit(type){
   
    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord(){
    /*  On save record:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    
    //  SAVE RECORD CODE BODY
    
    
    return true;
    
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum){

    /*  On validate field:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  VALIDATE FIELD CODE BODY
    
    
    return true;
    
}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum){
   
    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

    //alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name){
   
    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type){
  
    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type){

    /*  On validate line:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  VALIDATE LINE CODE BODY
    
    
    return true;
    
}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type){
   
    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================
// BEGIN FUNCTION ===================================================
// END FUNCTION =====================================================
