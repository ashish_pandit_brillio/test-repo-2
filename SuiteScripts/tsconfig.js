var config  = {
  "requestType": {
    "ProActive": "1",
    "Rotation": "3",
    "Conversion" : "4",
    "Notice" : "9",	
    "New Position" : "7",
    "Extension" : "6",
    "Replacement" : "10",
    "Augmentation" : "8",
    "Replacement Attrition" : "11", //prabhat Gupta
    "Replacement Rotation" : "12"      //prabhat Gupta
  },
  "fitmentType": {    
    "Brillio Fitment" : "1", //prabhat Gupta 09/09/2020
    "Account Fitment" : "2"      //prabhat Gupta 09/09/2020
  },
  "level": {    
    "1" : "1", //prabhat Gupta 09/10/2020
    "2" : "2",      //prabhat Gupta 09/10/2020
	"3" : "3",
	"3A" : "3A",
	"4" : "4",
	"5" : "5",
	"6" : "6",
	"6A" : "6A",
	"7" : "7",
	"8" : "8",
	"9" : "9",
	"CW" : "CW"
  },
  "positionType": {
     "Contingent Worker" : "9",
     "Direct Contract" : "10",
     "Intern" : "11",
     "Third Party Contract" : "12",
     "Campus" : "8",
     "Employee" : "13"
  },
  "billingCategory" : {
    "T" : "1",
     "F" : "2"
  },
  "priority" : {
    "Critical" : "1",
    "High" : "2",
    "Medium" : "3",
    "Low" : "4"
  },
  "department" : {
    "Advanced Technology Group" : "DEPTCOD-0000142615126",
    "Brillio Analytics":"DEPTCOD-0000078388734",
    "DFO" : "DEPTCOD-0000164916366",
    "Product Engineering" : "DEPTCOD-0000164916348",
    "Digital Infrastructure" : "DEPTCOD-0000117187662",
    "Global Design Studio" :"DEPTCOD-0000164916276",
    "Design & Experience Studio": "DEPTCOD-0000164916276",
    "Technology Advisory &Consulting": "DEPTCOD-0000142615126",
    "Cognetik": "DEPTCOD-0000203230719",
	 "Information Systems" : "DEPTCOD-0000020917401",
	"Delivery Excellence" : "DEPTCOD-0000078419114", 
	"Marketing" : "DEPTCOD-0000020917308", 
	"Human Resource" : "DEPTCOD-0000000150617",
	"IT" : "DEPTCOD-0000078395016",
	"PSC" : "DEPTCOD-0000193556517",
	"Delivery Office": "DEPTCOD-0000193015918",
	"Sustainability": "DEPTCOD-0000071506275",
	 "FMG": "DEPTCOD-0000020917385",
	 "Finance and Administration": "DEPTCOD-0000020917377",
     "PPE" : "DEPTCOD-0000210173711",
     "CEP" : "DEPTCOD-0000210173912",
    "Data Analytics & Engineering" : "DEPTCOD-0000210173873",
"Cloud Enginnering Studio":"DEPTCOD-0000210173886",
"Design Studio":"DEPTCOD-0000210173925"

  },
  "educationLevel" : {
    "1" : "BACHELOR_S_DEGREE_16_YEARS",
    "2" : "ASSOCIATE_S_DEGREE_COLLEGE_DIPLOMA_13_YEARS",
    "3" : "MASTER_S_DEGREE_18_YEARS"
  },
  "IndiaApprover" : {
		"Advanced Technology Group" : "Chander.Damodaran@brillio.com",
		"Brillio Analytics" : "senjuti.c1@brillio.com", //NIS-1426 changed from vikas to senjti on 24 july 2020//Updated as Vikash from Yogesh as per request from Bharath on 29 JAN 20
		"DFO" : "nandan.kamat@brillio.com",// NIS-1771 changed from suraj gupta to nandan
		"Digital Infrastructure" : "bharath.s@brillio.com",
		"Global Design Studio" : "benjamin.l@brillio.com",
		"Product Engineering" : "Sharathkumar.s@brillio.com",
		"Design & Experience Studio": "benjamin.l@brillio.com",
		"Technology Advisory &Consulting": "Chander.Damodaran@brillio.com",
         "Cognetik": "sudip.dandapat@brillio.com",
		 "Information Systems" : "ananda.hirekerur@brillio.com",
		"Delivery Excellence" : "mosesraj.r@brillio.com", 
		"Marketing" : "shishir.saxena@brillio.com", 
		"Human Resource" : "megan.a@brillio.com",
		"IT" : "sivakumar.ramanujam@brillio.com",
		"PSC" : "yogesh.gokhale@brillio.com",
		"Delivery Office": "aftab.ullah@brillio.com",
		"Sustainability": "abhishek.ranjan@brillio.com",
         "FMG": "nithish.murthy@brillio.com",
		 "Finance and Administration": "maneesh.agarwal@brillio.com",
    "PPE" : "Sharathkumar.s@brillio.com",
     "CEP" : "nandan.kamat@brillio.com",
    "Data Analytics & Engineering" : "senjuti.c1@brillio.com",
"Cloud Enginnering Studio":"Chander.Damodaran@brillio.com",
"Design Studio":"dave.bernstein@brillio.com"
	},
	"restOfWorldApprover" : {
		"Advanced Technology Group" : "Chander.Damodaran@brillio.com",
		"Brillio Analytics" : "senjuti.c1@brillio.com", //NIS-1426 changed from vikas to senjti on 24 july 2020
		"DFO" : "nandan.kamat@brillio.com", // NIS-1771 changed from suraj gupta to nandan
		"Digital Infrastructure" : "bharath.s@brillio.com",//Sravan.Bharanikana@brillio.com
		"Global Design Studio" : "benjamin.l@brillio.com",
		"Product Engineering" : "Sharathkumar.s@brillio.com",
		"Design & Experience Studio": "benjamin.l@brillio.com",
		"Technology Advisory &Consulting": "Chander.Damodaran@brillio.com",
         "Cognetik": "sudip.dandapat@brillio.com",
		 "Information Systems" : "ananda.hirekerur@brillio.com",
		"Delivery Excellence" : "mosesraj.r@brillio.com", 
		"Marketing" : "shishir.saxena@brillio.com", 
		"Human Resource" : "megan.a@brillio.com",
		"IT" : "sivakumar.ramanujam@brillio.com",
		"PSC" : "yogesh.gokhale@brillio.com",
		"Delivery Office": "aftab.ullah@brillio.com",
		"Sustainability": "abhishek.ranjan@brillio.com",
         "FMG": "nithish.murthy@brillio.com",
		 "Finance and Administration": "maneesh.agarwal@brillio.com",
      "PPE" : "Sharathkumar.s@brillio.com",
     "CEP" : "nandan.kamat@brillio.com",
      "Data Analytics & Engineering" : "senjuti.c1@brillio.com",
"Cloud Enginnering Studio":"Chander.Damodaran@brillio.com",
"Design Studio":"dave.bernstein@brillio.com"
	},
  "employeeStatus" : {
    "Regular" : "REGULAR",
    "Contractual" : "CONTRACTUAL",
    "Temporary" : "TEMPORARY",
    "Limited Term" : "LIMITED_TERM"
  },
  "nonBilliableApprover" :{                                           // NS-1242 Prabhat Gupta
    	"Advanced Technology Group" : "sridhar.r@brillio.com",
		"Brillio Analytics" : "sridhar.r@brillio.com",
		"DFO" : "sridhar.r@brillio.com",
		"Digital Infrastructure" : "sridhar.r@brillio.com",
		"Global Design Studio" : "sridhar.r@brillio.com",
		"Product Engineering" : "sridhar.r@brillio.com",
		"Design & Experience Studio": "sridhar.r@brillio.com",
		"Technology Advisory &Consulting": "sridhar.r@brillio.com",
		"Information Systems" : "ananda.hirekerur@brillio.com",
		"Delivery Excellence" : "mosesraj.r@brillio.com", 
		"Marketing" : "shishir.saxena@brillio.com", 
		"Human Resource" : "megan.a@brillio.com",
		"IT" : "sivakumar.ramanujam@brillio.com",
		"PSC" : "yogesh.gokhale@brillio.com",
		"Delivery Office": "aftab.ullah@brillio.com",
		"Sustainability": "abhishek.ranjan@brillio.com",
         "FMG": "nithish.murthy@brillio.com",
		 "Finance and Administration": "maneesh.agarwal@brillio.com",
          "PPE" : "sridhar.r@brillio.com",
          "CEP" : "sridhar.r@brillio.com",
    "Data Analytics & Engineering" : "sridhar.r@brillio.com",
"Cloud Enginnering Studio":"sridhar.r@brillio.com",
"Design Studio":"sridhar.r@brillio.com"
  }
}