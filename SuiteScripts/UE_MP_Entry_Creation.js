/**
 * Populate the entry fields based on the allocation
 * 
 * Version Date Author Remarks 1.00 18 Apr 2016 Nitish Mishra
 * Version Date Author Remarks 2.00 16 Mar 2020 Praveena Madem Added logics to update the Internalid of searched record type
 * 1	Onsite
 * 2	Offsite
 */

function userEventAfterSubmit(type) {
	try {

		if (type == 'create' || type == 'edit' || type == 'xedit') {
			var provisionEntry = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());

			// set fields from allocation
			var allocation = nlapiLoadRecord('resourceallocation',provisionEntry.getFieldValue('custrecord_pe_resource_allocation'));

			var provisionRecord = nlapiLoadRecord('customrecord_provision_master', provisionEntry.getFieldValue('custrecord_pe_provision_master'));

			// set the percentage allocation
			var Onsite_Offsite = allocation.getFieldValue('custevent4');
			var percentage = allocation.getFieldValue('percentoftime');
			provisionEntry.setFieldValue('custrecord_pe_percentage_allocation',percentage);
			var percentageNumeric = (parseFloat(percentage.split("%")[0]) / 100);

			// dates
			var allocationStartDate = allocation.getFieldValue('custeventbstartdate');
			var allocationEndDate = allocation.getFieldValue('custeventbenddate');

			var provisionStartDate = provisionEntry.getFieldValue('custrecord_pe_provision_from_date');
			var provisionEndDate = provisionEntry.getFieldValue('custrecord_pe_provision_to_date');
			var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
			var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
			var d_provisionStartDate = nlapiStringToDate(provisionStartDate);
			var d_provisionEndDate = nlapiStringToDate(provisionEndDate);

			var provisionMonthStartDate = nlapiStringToDate((d_provisionStartDate.getMonth() + 1)+ '/1/' + d_provisionStartDate.getFullYear());
			var nextProvisionMonthStartDate = nlapiAddMonths(provisionMonthStartDate, 1);
			var provisionMonthEndDate = nlapiAddDays(nextProvisionMonthStartDate, -1);
			// nlapiLogExecution('debug', 'provisionMonthStartDate',provisionMonthStartDate);
			// nlapiLogExecution('debug', 'provisionMonthEndDate',provisionMonthEndDate);

			var startDate = d_allocationStartDate > d_provisionStartDate ? allocationStartDate: provisionStartDate;
			var endDate = d_allocationEndDate < d_provisionEndDate ? allocationEndDate: provisionEndDate;
			provisionEntry.setFieldValue('custrecord_pe_from_date', startDate);
			provisionEntry.setFieldValue('custrecord_pe_to_date', endDate);

			// not using
			var workingMonth = getWorkingMonth(d_provisionStartDate,d_provisionEndDate);
			provisionEntry.setFieldValue('custrecord_pe_working_month',workingMonth);

			var employee = allocation.getFieldValue('allocationresource');
			var project = allocation.getFieldValue('project');
			var customer = nlapiLookupField('job', project, 'customer');

			provisionEntry.setFieldValue('custrecord_pe_employee', employee);
			provisionEntry.setFieldValue('custrecord_pe_project', project);
			provisionEntry.setFieldValue('custrecord_pe_customer', customer);

			var employeeDetails = nlapiLookupField('employee', employee, ['internalid', 'subsidiary', 'department' ]);
			var projectDetails = nlapiLookupField('job', project, [
			        'custentity_t_and_m_monthly',
			        'custentity_project_currency', 'custentity_vertical',
			        'entityid', 'altname','custentity_hoursperday','custentity_onsite_hours_per_day']);
			var customerDetails = nlapiLookupField('customer', customer,[ 'territory' ]);
			
			var onsiteHrs = projectDetails.custentity_onsite_hours_per_day;
			var offsiteHrs = projectDetails.custentity_hoursperday;

			// set the classification fields
			var projectFullName = projectDetails.altname;
			var colonIndex = projectFullName.indexOf(":");
			var projectName = projectFullName.substring(colonIndex).replace(/:/g, " ");
			nlapiLogExecution('debug', 'projectFullName', projectFullName);
			nlapiLogExecution('debug', 'projectName', projectName);
			nlapiLogExecution('debug', 'project name', projectDetails.entityid+ " " + projectName.trim());

			provisionEntry.setFieldValue('custrecord_pe_project_name',projectDetails.entityid + " " + projectName.trim());
			provisionEntry.setFieldValue('custrecord_pe_subsidiary',employeeDetails.subsidiary);
			provisionEntry.setFieldValue('custrecord_pe_practice',employeeDetails.department);
			provisionEntry.setFieldValue('custrecord_pe_vertical',projectDetails.custentity_vertical);
			provisionEntry.setFieldValue('custrecord_pe_region',customerDetails.territory);
			provisionEntry.setFieldValue('custrecord_pe_currency',projectDetails.custentity_project_currency);

			// is monthly
			provisionEntry.setFieldValue('custrecord_pe_is_monthly',projectDetails.custentity_t_and_m_monthly);

			// days calculation
			var workingDays = parseFloat(getWorkingDays(nlapiDateToString(
			        provisionMonthStartDate, 'date'), nlapiDateToString(
			        provisionMonthEndDate, 'date')));
			var holidayDetailsInMonth = get_holidays(provisionMonthStartDate,
			        provisionMonthEndDate, employee, project, customer);
			var holidayCountInMonth = holidayDetailsInMonth.length;

			var notSubmittedDayDetails = get_not_submitted_days(startDate,
			        endDate, employee, project, customer, allocation.getId());

			var notSubmittedDayDetailsMonthly = getNotSubmittedDaysMonthly(
			        startDate, endDate, employee, project, customer, allocation
			                .getId());

			// submitted days
			var approvedDays = getApprovedDays(startDate, endDate, employee,
			        project);
			var approvedHours = getApprovedHours(startDate, endDate, employee,
			        project);
			provisionEntry.setFieldValue('custrecord_pe_day_approved',
			        approvedDays);
			provisionEntry.setFieldValue('custrecord_pe_hour_approved',
			        approvedHours);

			// submitted, not approved days
			var submittedHours = getSubmittedHours(startDate, endDate,
			        employee, project);
			var submittedDays = getSubmittedDays(startDate, endDate, employee,
			        project);
			provisionEntry.setFieldValue('custrecord_pe_day_submitted',
			        submittedDays);
			provisionEntry.setFieldValue('custrecord_pe_hour_submitted',
			        submittedHours);

			// not submitted days
			var notSubmittedDays = notSubmittedDayDetailsMonthly.NotSubmittedDays;
			provisionEntry.setFieldValue('custrecord_pe_day_not_submitted',
			        notSubmittedDays);
			// daily rate calculation
			var billRate = parseFloat(allocation.getFieldValue('custevent3'));
			billRate = billRate ? parseFloat(billRate) : 0;
			var monthlyRate = allocation
			        .getFieldValue('custevent_monthly_rate');
			monthlyRate = monthlyRate ? parseFloat(monthlyRate) : 0;

			// holidays not included in working days
			var dailyRateCalculateWorkingDays = workingDays
			        - holidayCountInMonth;

			var sumOfDays = notSubmittedDays + submittedDays + approvedDays;

			if (sumOfDays > dailyRateCalculateWorkingDays) {
				dailyRateCalculateWorkingDays = sumOfDays;
			}

			var dailyRate = monthlyRate * workingMonth
			        / dailyRateCalculateWorkingDays;
			provisionEntry.setFieldValue('custrecord_pe_bill_rate', billRate);
			provisionEntry.setFieldValue('custrecord_pe_monthly_rate',
			        monthlyRate);
			provisionEntry.setFieldValue('custrecord_pe_daily_rate', dailyRate);

			// working days in the entire month
			provisionEntry.setFieldValue('custrecord_pe_total_working_days',
			        workingDays);
			provisionEntry.setFieldValue('custrecord_pe_holidays',
			        holidayCountInMonth);

			// working days in the allocation
			provisionEntry.setFieldValue(
			        'custrecord_pe_working_day_less_holiday',
			        dailyRateCalculateWorkingDays);

			// Amount calculations
			provisionEntry.setFieldValue('custrecord_pe_month_amt_approved',
			        approvedDays * dailyRate);
			provisionEntry.setFieldValue('custrecord_pe_hourly_amt_approved',
			        approvedHours * billRate);
			provisionEntry.setFieldValue('custrecord_pe_month_amt_submitted',
			        submittedDays * dailyRate);
			provisionEntry.setFieldValue('custrecord_pe_hourly_amt_submitted',
			        submittedHours * billRate);
			provisionEntry.setFieldValue(
			        'custrecord_pe_month_amt_not_submitted', notSubmittedDays
			                * dailyRate * percentageNumeric);
			if(Onsite_Offsite == "1") // Onsite
			{
				onsiteHrs = onsiteHrs?onsiteHrs:8;
				provisionEntry.setFieldValue('custrecord_pe_hour_not_submitted',
			        notSubmittedDays * Number(onsiteHrs) * percentageNumeric);
					nlapiLogExecution('DEBUG', 'Onsite HRS set',onsiteHrs);
					nlapiLogExecution('DEBUG', 'billRate onsiteHrs',billRate);
				nlapiLogExecution('DEBUG', 'percentageNumeric onsiteHrs',percentageNumeric);
								provisionEntry.setFieldValue(
			        'custrecord_pe_hourly_amt_not_submitted', notSubmittedDays
			                * Number(onsiteHrs) * billRate * percentageNumeric);
              nlapiLogExecution("DEBUG","onsiteHrs AMOUNT",notSubmittedDays * Number(onsiteHrs) * billRate * percentageNumeric);
			}
			else if(Onsite_Offsite == "2") // Offsite
			{
				offsiteHrs = offsiteHrs?offsiteHrs:8;
				provisionEntry.setFieldValue('custrecord_pe_hour_not_submitted',
			        notSubmittedDays * Number(offsiteHrs) * percentageNumeric);
					nlapiLogExecution('DEBUG', 'Offsite HRS set',offsiteHrs);
					nlapiLogExecution('DEBUG', 'billRate Offsite',billRate);
				nlapiLogExecution('DEBUG', 'percentageNumeric Offsite',percentageNumeric);
								provisionEntry.setFieldValue(
			        'custrecord_pe_hourly_amt_not_submitted', notSubmittedDays
			                * Number(offsiteHrs) * billRate * percentageNumeric);
              nlapiLogExecution("DEBUG","offsiteHrs AMOUNT",notSubmittedDays * Number(offsiteHrs) * billRate * percentageNumeric);
			}
			else{
			provisionEntry.setFieldValue('custrecord_pe_hour_not_submitted',
			        notSubmittedDays * 8 * percentageNumeric);
					nlapiLogExecution('DEBUG', '8 HRS set');
					
								provisionEntry.setFieldValue(
			        'custrecord_pe_hourly_amt_not_submitted', notSubmittedDays
			                * 8 * billRate * percentageNumeric);
			}

			// submit the changes
			nlapiSubmitRecord(provisionEntry);
			nlapiLogExecution('DEBUG', 'Record Submitted');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
		createErrorLog(nlapiGetFieldValue('custrecord_pe_provision_master'),
		        err, null);
	}
}

function createErrorLog(provisionMasterId, errorComment, link) {
	try {
		var rec = nlapiCreateRecord('customrecord_provision_error_log');
		rec.setFieldValue('custrecord_pel_provision_master', provisionMasterId);
		rec.setFieldValue('custrecord_pel_error_comment', errorComment);

		if (link) {
			rec.setFieldValue('custrecord_pel_record_link', link);
		}
		var id = nlapiSubmitRecord(rec);
		nlapiLogExecution('DEBUG', 'Error Log Created', id);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createErrorLog', err);
		throw err;
	}
}

function get_not_submitted_days(d_start_date, d_end_date, employee, project,
        customer, allocationId)
{
	try {
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);

		// nlapiLogExecution('debug', 'Not Submitted Started');

		// nlapiLogExecution('debug', 'd_start_date', d_start_date);
		// nlapiLogExecution('debug', 'd_end_date', d_end_date);
		// nlapiLogExecution('debug', 'employee', employee);
		// nlapiLogExecution('debug', 'project', project);
		// nlapiLogExecution('debug', 'customer', customer);

		// 1. create a array containing all days from start to end
		var main_array = [];

		// 2. loop from start to end date and mark saturday sunday as submitted
		for (var i = 0;; i++) {
			var d_current_date = nlapiAddDays(d_start_date, i);

			// if the date exceeds the end date, exit the loop
			if (d_current_date > d_end_date) {
				break;
			}

			// check if saturday / sunday
			var day = d_current_date.getDay();
			if (day == 0 || day == 6) {
				main_array[i] = 3;
			} else {
				main_array[i] = 1;
			}
		}

		// nlapiLogExecution('debug', 'data', JSON.stringify(main_array));
		// nlapiLogExecution('debug', 'no. of days', main_array.length);

		// 3. mark holidays as submitted
		var holiday_list = get_holidays(d_start_date, d_end_date, employee,
		        project, customer);
		// nlapiLogExecution('debug', 'no. of holidays', holiday_list.length);
		var holidayCount = holiday_list.length;

		for (var i = 0; i < holiday_list.length; i++) {
			var d_holiday_date = nlapiStringToDate(holiday_list[i]);
			var n = getDatediffIndays(nlapiDateToString(d_start_date),
			        holiday_list[i]) - 1;
			// nlapiLogExecution('debug', 'holiday : ' + n);
			main_array[n] = 3;
		}

		// nlapiLogExecution('debug', 'data', JSON.stringify(main_array));

		// 4. mark the no. of allocated days
		var search_allocation = nlapiSearchRecord('resourceallocation', null,
		        [ new nlobjSearchFilter('internalid', null, 'anyof',
		                allocationId) ], [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate') ]);

		if (search_allocation) {
			// nlapiLogExecution('debug', 'no. of allocations',
			// search_allocation.length);

			for (var i = 0; i < search_allocation.length; i++) {
				var allocation_start_date = nlapiStringToDate(search_allocation[i]
				        .getValue('startdate'));
				var allocation_end_date = nlapiStringToDate(search_allocation[i]
				        .getValue('enddate'));

				var start_date_point = allocation_start_date > d_start_date ? allocation_start_date
				        : d_start_date;

				for (var d = 0;; d++) {
					var d_current_date = nlapiAddDays(start_date_point, d);

					if (d_current_date > allocation_end_date
					        || d_current_date > d_end_date) {
						break;
					}

					var n = getDatediffIndays(nlapiDateToString(d_start_date),
					        nlapiDateToString(d_current_date)) - 1;

					if (main_array[n] == 1) {
						main_array[n] = 2;
					}
				}
			}
		}

		// allocated working day calculation
		var allocatedWorkingDays = 0;
		for (var i = 0; i < main_array.length; i++) {

			if (main_array[i] == 2) {
				allocatedWorkingDays += 1;
			}
		}

		// nlapiLogExecution('debug', 'allocation data', JSON
		// .stringify(main_array));

		// 5. get the timesheet between the start date and end date
		var search_timesheet = nlapiSearchRecord('timesheet', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
          new nlobjSearchFilter('totalhours', null, 'greaterthan', "0"),
		        new nlobjSearchFilter('approvalstatus', null, 'noneof',
		                [ 1, 4 ]),
		        new nlobjSearchFilter('timesheetdate', null, 'within',
		                d_start_date, d_end_date) ], [ new nlobjSearchColumn(
		        'startdate') ]);

		// 6. loop through the timesheets
		if (search_timesheet) {
			// nlapiLogExecution('debug', 'no. of timesheets',
			// search_timesheet.length);

			// 6a. if timesheet is submitted, mark entire week as submitted
			for (var i = 0; i < search_timesheet.length; i++) {
				var timesheet_date = search_timesheet[i].getValue('startdate');
				var n = getDatediffIndays(nlapiDateToString(d_start_date),
				        timesheet_date) - 1;

				if (n >= 0) {

					for (var j = n; j < n + 7; j++) {

						if (main_array[j] == 2) {
							main_array[j] = 3;
						}
					}
				} else {

					for (var j = 0; j < n + 7; j++) {

						if (main_array[j] == 2) {
							main_array[j] = 3;
						}
					}
				}
			}
		}

		// nlapiLogExecution('debug', 'data', JSON.stringify(main_array));

		// 7. count the not submitted days in the main array
		var not_submitted_days = 0;
		var submitted = 0;

		for (var i = 0; i < main_array.length; i++) {

			if (main_array[i] == 2) {
				not_submitted_days = not_submitted_days + 1;
			} else {
				submitted = submitted + 1;
			}
		}

		 nlapiLogExecution('debug', 'not_submitted_days', not_submitted_days);
		 nlapiLogExecution('debug', 'submitted', submitted);
		 nlapiLogExecution('debug', 'Not Submitted Ended');

		// 8. return the count
		return {
		    Holidays : holidayCount,
		    NotSubmittedDays : not_submitted_days,
		    AllocatedWorkingDays : allocatedWorkingDays
		}
	} catch (err) {
		nlapiLogExecution('error', 'not submitted days', err);
	}
}

function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date1 = nlapiStringToDate(fromDate);
	var date2 = nlapiStringToDate(toDate);
	var date3 = Math.round((date2 - date1) / one_day);
	return (date3 + 1);
}

function get_holidays(start_date, end_date, employee, project, customer) {

	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');
	var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date, emp_subsidiary);
	} else {
		return get_customer_holidays(start_date, end_date, emp_subsidiary,
		        customer);
	}
}

function get_company_holidays(start_date, end_date, subsidiary) {
	var holiday_list = [];

	start_date = nlapiDateToString(start_date, 'date');
	end_date = nlapiDateToString(end_date, 'date');

	// nlapiLogExecution('debug', 'start_date', start_date);
	// nlapiLogExecution('debug', 'end_date', end_date);
	// nlapiLogExecution('debug', 'subsidiary', subsidiary);

	var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
	        'customsearch_company_holiday_search', [
	                new nlobjSearchFilter('custrecord_date', null, 'within',
	                        start_date, end_date),
	                new nlobjSearchFilter('custrecordsubsidiary', null,
	                        'anyof', subsidiary) ], [ new nlobjSearchColumn(
	                'custrecord_date') ]);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			holiday_list.push(search_company_holiday[i]
			        .getValue('custrecord_date'));
		}
	}

	return holiday_list;
}

function get_customer_holidays(start_date, end_date, subsidiary, customer) {
	var holiday_list = [];

	start_date = nlapiDateToString(start_date, 'date');
	end_date = nlapiDateToString(end_date, 'date');

	// nlapiLogExecution('debug', 'start_date', start_date);
	// nlapiLogExecution('debug', 'end_date', end_date);
	// nlapiLogExecution('debug', 'subsidiary', subsidiary);
	// nlapiLogExecution('debug', 'customer', customer);

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecordcustomersubsidiary', null,
	                        'anyof', subsidiary),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [ new nlobjSearchColumn(
	                'custrecordholidaydate', null, 'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			holiday_list.push(search_customer_holiday[i].getValue(
			        'custrecordholidaydate', null, 'group'));
		}
	}

	return holiday_list;
}

function getWorkingDays(startDate, endDate) {
	try {
		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);
		var numberOfWorkingDays = 0;

		for (var i = 0;; i++) {
			var currentDate = nlapiAddDays(d_startDate, i);

			if (currentDate > d_endDate) {
				break;
			}

			if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
				numberOfWorkingDays += 1;
			}
		}

		return numberOfWorkingDays;
	} catch (err) {
		nlapiLogExecution('error', 'getWorkingDays', err);
		throw err;
	}
}

function getSubmittedHours(startDate, endDate, employee, project) {
	try {
		var search = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate),
		        new nlobjSearchFilter('approvalstatus', null, 'anyof', '2'),
		        new nlobjSearchFilter('billable', null, 'is', 'T'),
		        new nlobjSearchFilter('status', null, 'is', 'T') ],
		        [ new nlobjSearchColumn('durationdecimal', null, 'sum') ]);

		if (search) {
			var hours = search[0].getValue('durationdecimal', null, 'sum');
			hours = hours ? parseFloat(hours) : 0;
			return hours;
		} else {
			return 0;
		}
	} catch (err) {
		nlapiLogExecution('error', 'getSubmittedHours', err);
		throw err;
	}
}

function getSubmittedDays(startDate, endDate, employee, project) {
	try {
		var search = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate),
		        new nlobjSearchFilter('approvalstatus', null, 'anyof', '2'),
		        new nlobjSearchFilter('billable', null, 'is', 'T'),
		        new nlobjSearchFilter('status', null, 'is', 'T') ],
		        [ new nlobjSearchColumn('date', null, 'count') ]);

		if (search) {
			var days = search[0].getValue('date', null, 'count');
			days = days ? parseFloat(days) : 0;
			return days;
		} else {
			return 0;
		}
	} catch (err) {
		nlapiLogExecution('error', 'getSubmittedDays', err);
		throw err;
	}
}

function getApprovedHours(startDate, endDate, employee, project) {
	try {
		var search = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate),
		        new nlobjSearchFilter('approvalstatus', null, 'anyof', '3'),
		        new nlobjSearchFilter('billable', null, 'is', 'T'),
		        new nlobjSearchFilter('status', null, 'is', 'T') ],
		        [ new nlobjSearchColumn('durationdecimal', null, 'sum') ]);

		if (search) {
			var hours = search[0].getValue('durationdecimal', null, 'sum');
			hours = hours ? parseFloat(hours) : 0;
			return hours;
		} else {
			return 0;
		}
	} catch (err) {
		nlapiLogExecution('error', 'getApprovedHours', err);
		throw err;
	}
}

function getApprovedDays(startDate, endDate, employee, project) {
	try {
		var search = nlapiSearchRecord('timebill', null, [//Changed By praveena on 16-03-2020
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('date', null, 'within', startDate,
		                endDate),
		        new nlobjSearchFilter('approvalstatus', null, 'anyof', '3'),
		        new nlobjSearchFilter('billable', null, 'is', 'T'),
		        new nlobjSearchFilter('status', null, 'is', 'T') ],
		        [ new nlobjSearchColumn('date', null, 'count') ]);

		if (search) {
			var days = search[0].getValue('date', null, 'count');
			days = days ? parseFloat(days) : 0;
			return days;
		} else {
			return 0;
		}
	} catch (err) {
		nlapiLogExecution('error', 'getApprovedDays', err);
		throw err;
	}
}


function getWorkingMonth(fromDate, toDate) {
	var month = 1;
	for (;; month++) {
		var newDate = nlapiAddMonths(fromDate, month);

		if (newDate >= toDate) {
			break;
		}
	}

	return month;
}

function getDayDiff(fromDate, toDate) {
	fromDate = nlapiStringToDate(fromDate);
	toDate = nlapiStringToDate(toDate);

	if (fromDate > toDate) {
		return -999;
	}

	var dayDiff = 1;
	for (;; dayDiff++) {
		var newDate = nlapiAddDays(fromDate, dayDiff);

		if (newDate > toDate) {
			break;
		}
	}

	return dayDiff;
}

function getNotSubmittedDaysMonthly(d_start_date, d_end_date, employee,
        project, customer, allocationId)
{
	try {
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date);

		// nlapiLogExecution('debug', 'Not Submitted Started');

		// nlapiLogExecution('debug', 'd_start_date', d_start_date);
		// nlapiLogExecution('debug', 'd_end_date', d_end_date);
		// nlapiLogExecution('debug', 'employee', employee);
		// nlapiLogExecution('debug', 'project', project);
		// nlapiLogExecution('debug', 'customer', customer);

		// 1. create a array containing all days from start to end
		var main_array = [];

		// 2. loop from start to end date and mark saturday sunday as submitted
		for (var i = 0;; i++) {
			var d_current_date = nlapiAddDays(d_start_date, i);

			// if the date exceeds the end date, exit the loop
			if (d_current_date > d_end_date) {
				break;
			}

			// check if saturday / sunday
			var day = d_current_date.getDay();
			if (day == 0 || day == 6) {
				main_array[i] = 3;
			} else {
				main_array[i] = 1;
			}
		}

		// nlapiLogExecution('debug', 'data', JSON.stringify(main_array));
		// nlapiLogExecution('debug', 'no. of days', main_array.length);

		// 3. mark holidays as submitted
		var holiday_list = get_holidays(d_start_date, d_end_date, employee,
		        project, customer);
		// nlapiLogExecution('debug', 'no. of holidays', holiday_list.length);
		var holidayCount = holiday_list.length;

		for (var i = 0; i < holiday_list.length; i++) {
			var d_holiday_date = nlapiStringToDate(holiday_list[i]);
			var n = getDatediffIndays(nlapiDateToString(d_start_date),
			        holiday_list[i]) - 1;
			// nlapiLogExecution('debug', 'holiday : ' + n);
			main_array[n] = 3;
		}

		// nlapiLogExecution('debug', 'data', JSON.stringify(main_array));

		// 4. mark the no. of allocated days
		var search_allocation = nlapiSearchRecord('resourceallocation', null,
		        [ new nlobjSearchFilter('internalid', null, 'anyof',
		                allocationId) ], [ new nlobjSearchColumn('startdate'),
		                new nlobjSearchColumn('enddate') ]);

		// nlapiLogExecution('debug', 'no. of allocations',
		// search_allocation.length);

		for (var i = 0; i < search_allocation.length; i++) {
			var allocation_start_date = nlapiStringToDate(search_allocation[i]
			        .getValue('startdate'));
			var allocation_end_date = nlapiStringToDate(search_allocation[i]
			        .getValue('enddate'));

			var start_date_point = allocation_start_date > d_start_date ? allocation_start_date
			        : d_start_date;

			for (var d = 0;; d++) {
				var d_current_date = nlapiAddDays(start_date_point, d);

				if (d_current_date > allocation_end_date
				        || d_current_date > d_end_date) {
					break;
				}

				var n = getDatediffIndays(nlapiDateToString(d_start_date),
				        nlapiDateToString(d_current_date)) - 1;

				if (main_array[n] == 1) {
					main_array[n] = 2;
				}
			}
		}

		// allocated working day calculation
		var allocatedWorkingDays = 0;
		for (var i = 0; i < main_array.length; i++) {

			if (main_array[i] == 2) {
				allocatedWorkingDays += 1;
			}
		}

		// nlapiLogExecution('debug', 'allocation data', JSON
		// .stringify(main_array));

		// 5. get the timesheet between the start date and end date
		var search_timesheet = nlapiSearchRecord('timesheet', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('approvalstatus', null, 'noneof',
		                [ 1, 4 ]),
          new nlobjSearchFilter('totalhours', null, 'greaterthan', "0"),
		        //new nlobjSearchFilter('timesheetdate', null, 'within',
		        new nlobjSearchFilter('date', 'timebill', 'within',
		                d_start_date, d_end_date) ], [ new nlobjSearchColumn(
		        'startdate') ]);

		// 6. loop through the timesheets
		if (search_timesheet) {
			// nlapiLogExecution('debug', 'no. of timesheets',
			// search_timesheet.length);

			// 6a. if timesheet is submitted, mark entire week as submitted
			for (var i = 0; i < search_timesheet.length; i++) {
				var timesheet_date = search_timesheet[i].getValue('startdate');
				var n = getDatediffIndays(nlapiDateToString(d_start_date),
				        timesheet_date) - 1;

				if (n >= 0) {

					for (var j = n; j < n + 7; j++) {

						if (main_array[j] == 2) {
							main_array[j] = 3;
						}
					}
				} else {

					for (var j = 0; j < n + 7; j++) {

						if (main_array[j] == 2) {
							main_array[j] = 3;
						}
					}
				}
			}
		}

		// nlapiLogExecution('debug', 'data', JSON.stringify(main_array));

		// 7. count the not submitted days in the main array
		var not_submitted_days = 0;
		var submitted = 0;

		for (var i = 0; i < main_array.length; i++) {

			if (main_array[i] == 2) {
				not_submitted_days = not_submitted_days + 1;
			} else {
				submitted = submitted + 1;
			}
		}

		// nlapiLogExecution('debug', 'not_submitted_days', not_submitted_days);
		// nlapiLogExecution('debug', 'submitted', submitted);
		// nlapiLogExecution('debug', 'Not Submitted Ended');

		// 8. return the count
		return {
		    Holidays : holidayCount,
		    NotSubmittedDays : not_submitted_days,
		    AllocatedWorkingDays : allocatedWorkingDays
		}
	} catch (err) {
		nlapiLogExecution('error', 'not submitted months', err);
	}
}