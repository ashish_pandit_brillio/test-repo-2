//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=974
/**
 * @author Jayesh
 */
function aftersubmit_set_emp_type(type) {
    var context = nlapiGetContext();
    try {
         /*----------Added by Koushalya 28/12/2021---------*/
         var filters_customrecord_revenue_location_subsidiarySearch=["custrecord_offsite_onsite","anyof","2"];
         var obj_rev_loc_subSrch= Search_revenue_Location_subsdidary(filters_customrecord_revenue_location_subsidiarySearch);
         var obj_offsite_onsite=obj_rev_loc_subSrch["Onsite/Offsite"];
          /*-------------------------------------------------*/
 
        if (type == 'delete')
            return;

        var submit_record_flag = 0;
        var i_recordID = nlapiGetRecordId();
        var s_record_type = nlapiGetRecordType();

        var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID);

        var transactionNum = o_recordOBJ.getFieldValue('tranid');
        var transaction_internal = i_recordID;

        var expense_line_count = o_recordOBJ.getLineItemCount('expense');
        var excel_file_obj = '';
        var err_row_excel = '';
        var strVar_excel = '';

        strVar_excel += '<table>';
        strVar_excel += '	<tr>';
        strVar_excel += ' <td width="100%">';
        strVar_excel += '<table width="100%" border="1">';
        strVar_excel += '	<tr>';
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Type</td>';
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Number</td>';
        strVar_excel += ' <td width="6%" font-size="11" align="center">Transaction Internal ID</td>';
        strVar_excel += ' <td width="6%" font-size="11" align="center">Error Message</td>';
        strVar_excel += '	</tr>';
        for (var k = 1; k <= expense_line_count; k++) {
            var emp_type = '';
            var person_type = '';
            var onsite_offsite = '';
            var emp_name_selected = o_recordOBJ.getLineItemValue('expense', 'custcol_employeenamecolumn', k);
            if (_logValidation(emp_name_selected)) {
                var s_employee_name_id = emp_name_selected.split('-');
                var s_employee_name_split = s_employee_name_id[0];

                var filters_emp = new Array();
                filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
                var column_emp = new Array();
                column_emp[0] = new nlobjSearchColumn('custentity_persontype');
                column_emp[1] = new nlobjSearchColumn('employeetype');
                column_emp[2] = new nlobjSearchColumn('subsidiary');
                column_emp[3] = new nlobjSearchColumn('custentity_legal_entity_fusion');

                var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
                nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
                if (_logValidation(a_results_emp)) {
                    var emp_id = a_results_emp[0].getId();
                    var emp_type = a_results_emp[0].getText('employeetype');
                    nlapiLogExecution('audit', 'emp_type:-- ', emp_type);
                    if (!_logValidation(emp_type)) {
                        emp_type = '';
                    }
                    var person_type = a_results_emp[0].getText('custentity_persontype');
                    nlapiLogExecution('audit', 'person_type:-- ', person_type);
                    if (!_logValidation(person_type)) {
                        person_type = '';
                    }
                    var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
                    if (_logValidation(emp_subsidiary)) {
                        if (emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
                            onsite_offsite = 'Onsite';
                        }
                        else{
                            onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                        }
                        /* else if (emp_subsidiary == 3 || emp_subsidiary == 12) {
                            onsite_offsite = 'Offsite';
                        } else {
                            onsite_offsite = 'Onsite';
                        }*/
                    }
                }

                o_recordOBJ.selectLineItem('expense', k);
                o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_employee_type', emp_type);
                o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_person_type', person_type);
                o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_onsite_offsite', onsite_offsite);
                o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', s_employee_name_split);
                o_recordOBJ.commitLineItem('expense');

                submit_record_flag = 1;
            }

            var proj_name_selected = o_recordOBJ.getLineItemValue('expense', 'custcolprj_name', k);
            if (_logValidation(proj_name_selected)) {
                proj_full_name = proj_name_selected.toString();
                proj_id_array = proj_full_name.split(' ');
                proj_id = proj_id_array[0];
                if (_logValidation(proj_id)) {
                    var filters_search_proj = new Array();
                    filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'is', proj_id);
                    var column_search_proj = new Array();
                    column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
                    column_search_proj[1] = new nlobjSearchColumn('customer');

                    var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
                    if (_logValidation(search_proj_results)) {
                        var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
                        if (!_logValidation(proj_billing_type))
                            proj_billing_type = '';

                        var cust_name = search_proj_results[0].getText('customer');
                        var cust_id = '';
                        if (_logValidation(cust_name)) {
                            cust_id = cust_name.split(' ');
                            cust_id = cust_id[0];
                        }

                        o_recordOBJ.selectLineItem('expense', k);
                        o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_billing_type', proj_billing_type);
                        o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id);
                        o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
                        o_recordOBJ.commitLineItem('expense');

                        submit_record_flag = 1;

                    }
                }
            }
        }

        var item_line_count = o_recordOBJ.getLineItemCount('item');
        for (var d = 1; d <= item_line_count; d++) {
            var emp_type = '';
            var person_type = '';
            var onsite_offsite = '';
            var emp_name_selected = o_recordOBJ.getLineItemValue('item', 'custcol_employeenamecolumn', d);
            if (_logValidation(emp_name_selected)) {
                var s_employee_name_id = emp_name_selected.split('-');
                var s_employee_name_split = s_employee_name_id[0];

                var filters_emp = new Array();
                filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
                var column_emp = new Array();
                column_emp[0] = new nlobjSearchColumn('custentity_persontype');
                column_emp[1] = new nlobjSearchColumn('employeetype');
                column_emp[2] = new nlobjSearchColumn('subsidiary');
                column_emp[3] = new nlobjSearchColumn('custentity_legal_entity_fusion');
                var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
                nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
                if (_logValidation(a_results_emp)) {
                    var emp_id = a_results_emp[0].getId();
                    var emp_type = a_results_emp[0].getText('employeetype');
                    nlapiLogExecution('audit', 'emp_type:-- ', emp_type);
                    if (!_logValidation(emp_type)) {
                        emp_type = '';
                    }
                    var person_type = a_results_emp[0].getText('custentity_persontype');
                    nlapiLogExecution('audit', 'person_type:-- ', person_type);
                    if (!_logValidation(person_type)) {
                        person_type = '';
                    }
                    var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
                    if (_logValidation(emp_subsidiary)) {
                        if (emp_subsidiary == 3 && a_results_emp[0].getValue('custentity_legal_entity_fusion') == 'Brillio Technologies Private Limited UK') {
                            onsite_offsite = 'Onsite';
                        }
                        else{
                            onsite_offsite = obj_offsite_onsite[emp_subsidiary]?'Offshore':'Onsite';    
                        } 
                        /*else if (emp_subsidiary == 3 || emp_subsidiary == 12) {
                            onsite_offsite = 'Offsite';
                        } else {
                            onsite_offsite = 'Onsite';
                        }*/
                    }
                }

                o_recordOBJ.selectLineItem('item', d);
                o_recordOBJ.setCurrentLineItemValue('item', 'custcol_employee_type', emp_type);
                o_recordOBJ.setCurrentLineItemValue('item', 'custcol_person_type', person_type);
                o_recordOBJ.setCurrentLineItemValue('item', 'custcol_onsite_offsite', onsite_offsite);
                o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', s_employee_name_split);
                o_recordOBJ.commitLineItem('item');

                submit_record_flag = 1;
            }

            var proj_name_selected = o_recordOBJ.getLineItemValue('item', 'custcolprj_name', d);
            if (_logValidation(proj_name_selected)) {
                proj_full_name = proj_name_selected.toString();
                proj_id_array = proj_full_name.split(' ');
                proj_id = proj_id_array[0];
                if (_logValidation(proj_id)) {
                    var filters_search_proj = new Array();
                    filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'is', proj_id);
                    var column_search_proj = new Array();
                    column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
                    column_search_proj[1] = new nlobjSearchColumn('customer');

                    var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
                    if (_logValidation(search_proj_results)) {
                        var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
                        if (!_logValidation(proj_billing_type))
                            proj_billing_type = '';

                        var cust_name = search_proj_results[0].getText('customer');
                        var cust_id = '';
                        if (_logValidation(cust_name)) {
                            cust_id = cust_name.split(' ');
                            cust_id = cust_id[0];
                        }

                        o_recordOBJ.selectLineItem('item', d);
                        o_recordOBJ.setCurrentLineItemValue('item', 'custcol_billing_type', proj_billing_type);
                        o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_project_entity_id', proj_id);
                        o_recordOBJ.setCurrentLineItemValue('expense', 'custcol_customer_entityid', cust_id);
                        o_recordOBJ.commitLineItem('item');

                        submit_record_flag = 1;
                    }
                }
            }
        }

        if (submit_record_flag == 1) {
            var vendor_credit_submitted_id = nlapiSubmitRecord(o_recordOBJ, true, true);
            nlapiLogExecution('debug', 'submitted bill credit id:-- ', vendor_credit_submitted_id);
        }

    } catch (err) {
        err_row_excel += '	<tr>';
        err_row_excel += ' <td width="6%" font-size="11" align="center">' + s_record_type + '</td>';
        err_row_excel += ' <td width="6%" font-size="11" align="center">' + transactionNum + '</td>';
        err_row_excel += ' <td width="6%" font-size="11" align="center">' + transaction_internal + '</td>';
        err_row_excel += ' <td width="6%" font-size="11" align="center">' + err.code+"   "+err.message+ '</td>';//JSON.stringify(err) + '</td>';

        err_row_excel += '	</tr>';

    }
    if (_logValidation(err_row_excel)) {
        var tailMail = '';
        tailMail += '</table>';
        tailMail += ' </td>';
        tailMail += '</tr>';
        tailMail += '</table>';

        strVar_excel = strVar_excel + err_row_excel + tailMail
        //excel_file_obj = generate_excel(strVar_excel);
        var mailTemplate = "";
        mailTemplate += '<html>';
        mailTemplate += '<body>';
        mailTemplate += "<p> This is to inform that following transactions are having Errors and therefore, system is not able to set the project/MIS mandatory fields</p>";
        mailTemplate += "<p> Please make sure, transaction is updated with proper details else may not able to see the project/customer/employee details at Income Statement level.</p>";
        mailTemplate += "<br/>"
        mailTemplate += "<p><b> Script Id:- " + context.getScriptId() + "</b></p>";
        mailTemplate += "<p><b> Script Deployment Id:- " + context.getDeploymentId() + "</b></p>";
        mailTemplate += "<br/>"
        mailTemplate += strVar_excel
        mailTemplate += "<br/>"
        mailTemplate += "<p>Regards, <br/> Information Systems</p>";
        mailTemplate += '</body>';
        mailTemplate += '</html>';
        //nlapiSendEmail(442, 'billing@brillio.com', 'Issue with updating the MIS data for the transactions', mailTemplate, 'netsuite.support@brillio.com', null, null, null);
    }

}

function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function generate_excel(strVar_excel) {
    var strVar1 = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
        '<head>' +
        '<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>' +
        '<meta name=ProgId content=Excel.Sheet/>' +
        '<meta name=Generator content="Microsoft Excel 11"/>' +
        '<!--[if gte mso 9]><xml>' +
        '<x:excelworkbook>' +
        '<x:excelworksheets>' +
        '<x:excelworksheet=sheet1>' +
        '<x:name>** ESTIMATE FILE**</x:name>' +
        '<x:worksheetoptions>' +
        '<x:selected></x:selected>' +
        '<x:freezepanes></x:freezepanes>' +
        '<x:frozennosplit></x:frozennosplit>' +
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>' +
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>' +
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>' +
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>' +
        '<x:activepane>0</x:activepane>' + // 0
        '<x:panes>' +
        '<x:pane>' +
        '<x:number>3</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>1</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>2</x:number>' +
        '</x:pane>' +
        '<x:pane>' +
        '<x:number>0</x:number>' + //1
        '</x:pane>' +
        '</x:panes>' +
        '<x:protectcontents>False</x:protectcontents>' +
        '<x:protectobjects>False</x:protectobjects>' +
        '<x:protectscenarios>False</x:protectscenarios>' +
        '</x:worksheetoptions>' +
        '</x:excelworksheet>' +
        '</x:excelworksheets>' +
        '<x:protectstructure>False</x:protectstructure>' +
        '<x:protectwindows>False</x:protectwindows>' +
        '</x:excelworkbook>' +

        // '<style>'+

        //-------------------------------------
        '</x:excelworkbook>' +
        '</xml><![endif]-->' +
        '<style>' +
        'p.MsoFooter, li.MsoFooter, div.MsoFooter' +
        '{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}' +
        '<style>' +

        '<!-- /* Style Definitions */' +

        //'@page Section1'+
        //'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

        'div.Section1' +
        '{ page:Section1;}' +

        'table#hrdftrtbl' +
        '{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->' +

        '</style>' +


        '</head>' +


        '<body>' +

        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'

    var strVar2 = strVar_excel;
    strVar1 = strVar1 + strVar2;
    var file = nlapiCreateFile('Resource Allocation Data.xls', 'XMLDOC', strVar1);
    return file;
}