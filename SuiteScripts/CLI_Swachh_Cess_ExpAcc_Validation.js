// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI_Swachh_cess_expAcc_validation.js
	Author:		Nikhil Jain
	Company:	Aashna cloudtech Pvt. Ltd.
	Date:		15/01/2016
    Description:validate the line level item expense account


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
	     23 May 2016          Nikhil                                                       remove hardcodings of tax types 



	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...

	var a_subisidiary = new Array;
	var i_vatCode;
	var i_taxcode;
	var s_pass_code
	var tdsRoundMethod;
	var a_st_details = new Array();
	var st_details_obj = {};
	var st_serachRes;

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_swachh_exp_validate(type){
	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	//  PAGE INIT CODE BODY
	//==== CALL A GLOBAL PARAMETER FUNCTION ======
	var i_AitGlobalRecId = SearchGlobalParameter();
	nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
	
	//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
	if (i_AitGlobalRecId != 0) {
		var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
		
		a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
		nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
		
		i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
		nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
		
		i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
		nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
		
		s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
		nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
		
		tdsRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_tds_roundoff');
		nlapiLogExecution('DEBUG', 'Bill ', "tdsRoundMethod->" + tdsRoundMethod);
		
		//================================Begin:- Code to Get the Service tax Details=============================//
		
		var st_filters = new Array();
		var st_columns = new Array();
		
		st_filters.push(new nlobjSearchFilter('custrecord_ait_service_tax_global_link', null, 'anyOf', i_AitGlobalRecId))
		st_filters.push(new nlobjSearchFilter('custrecord_ait_service_expense_out', null, 'is', 'T'))
		st_filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'))
		
		st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_type'))
		st_columns.push(new nlobjSearchColumn('custrecord_ait_service_tax_rate'))
		st_columns.push(new nlobjSearchColumn('custrecord_ait_service_expense_out'))
		
		var st_serachRes = nlapiSearchRecord('customrecord_ait_service_tax_details', null, st_filters, st_columns)
		
		if (st_serachRes != null && st_serachRes != '' && st_serachRes != undefined) {
			for (var t = 0; t < st_serachRes.length; t++) {
				var st_taxtype = st_serachRes[t].getValue('custrecord_ait_service_tax_type')
				var st_rate = st_serachRes[t].getValue('custrecord_ait_service_tax_rate')
				var st_expense = st_serachRes[t].getValue('custrecord_ait_service_expense_out')
				
				st_details_obj[st_taxtype] = st_rate + '@' + st_expense
				
				a_st_details.push(st_details_obj);
			}
		}
		//===============================End:- Code to Get the Service tax Details=============================//
	
	}
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
	
	
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_check_expense_acc(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY
	
	if (type == 'item') 
	{
	
		var i_exp_acc = nlapiGetCurrentLineItemValue('item', 'custcol_ait_expense_acc');
		
		if (i_exp_acc != '' && i_exp_acc != null && i_exp_acc != undefined) 
		{
		
		}
		else
		{
			var Flag = isindia_subsidiary(a_subisidiary, s_pass_code)
			if (Flag == 1)
			 {
				var taxcode = nlapiGetCurrentLineItemValue('item', 'taxcode');
				
				var s_taxAccounts = getTaxaccount(taxcode, st_details_obj, a_st_details,st_serachRes);
				
				if (s_taxAccounts != null && s_taxAccounts != '' && s_taxAccounts != undefined) {
				
					alert('Please Enter the value of Expense/Cogs Account')
					return false;
				}
				
			}
		}
		
	}
	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================




// BEGIN FUNCTION ===================================================
function SearchGlobalParameter()
{
	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	
	return i_globalRecId;
}
function getTaxaccount(taxcode, st_details_obj, a_st_details,st_serachRes)
{
	var taxAccountarray = new Array();
		
	try 
	{
		if (st_serachRes != null && st_serachRes != '' && st_serachRes != undefined) 
		{
			var taxgrpobj = nlapiLoadRecord('taxgroup', taxcode) //Load Tax group
			var taxgrplineitemcount = taxgrpobj.getLineItemCount('taxitem'); //Get line item on tax group
			nlapiLogExecution('DEBUG', 'getTaxaccount', "taxgrplineitemcount" + taxgrplineitemcount)
			
			var p = 0
			
			for (var j = 1; j <= taxgrplineitemcount; j++) 
			{
				var taxname = taxgrpobj.getLineItemValue('taxitem', 'taxname', j) //Get Tax name
				nlapiLogExecution('DEBUG', 'getTaxaccount ', "taxname->" + taxname)
				
				if (taxname != null && taxname != '' && taxname != undefined) 
				{
					var codeobj = nlapiLoadRecord('salestaxitem', taxname) //Get tax code object
					var taxtype = codeobj.getFieldValue('taxtype') //Get account code
					nlapiLogExecution('DEBUG', 'getTaxaccount', "taxtype->" + taxtype)
					
					if (taxtype in st_details_obj) 
					{
						var accountcode = codeobj.getFieldValue('purchaseaccount') //Get account code
						nlapiLogExecution('DEBUG', 'getTaxaccount', "accountcode->" + accountcode)
						
						var rate = codeobj.getFieldValue('rate') //Get account code
						nlapiLogExecution('DEBUG', 'getTaxaccount', "rate->" + rate)
						
						taxAccountarray[p++] = accountcode
						taxAccountarray[p++] = rate
					}
					
				} // END if (taxname != null)
			} // END for (var j = 1; j <= taxgrplineitemcount; j++)
		}
	} 
	catch (e) {
		nlapiLogExecution('DEBUG', 'getTaxaccount', "Not a tax Group")
	}
	return taxAccountarray
}// END getTaxaccount(taxcode)
 



// END FUNCTION =====================================================
