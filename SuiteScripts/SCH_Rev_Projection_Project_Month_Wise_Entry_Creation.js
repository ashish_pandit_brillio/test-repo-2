/**
 * Create entries for each month for the active projects
 * 
 * Version Date Author Remarks
 * 
 * 1.00 22 Jun 2016 Nitish Mishra
 * 
 */

function scheduled(type) {
	try {
		var context = nlapiGetContext();

		var searchResult = searchRecord('customrecord_job_month_rev_projection');

		for (var i = 0; i < searchResult.length; i++) {
			nlapiSubmitField('customrecord_job_month_rev_projection',
				searchResult[i].getId(), 'custrecord_prp_project', 'T');
			yieldScript(context);
		}

		nlapiLogExecution('debug', 'all recognised deleted');
		delete searchResult;

		var newYear = nlapiStringToDate('1/1/2016');
		var projectList = [];

		searchRecord('job', '1421').forEach(function (project) {
			var startDate = nlapiStringToDate(project.getValue('startdate'));

			if (startDate < newYear) {
				startDate = newYear;
			}

			projectList.push({
				Id: project.getId(),
				StartDate: nlapiDateToString(startDate, 'date'),
				EndDate: project.getValue('enddate'),
			});
		});

		nlapiLogExecution('debug', 'project count', projectList.length);

		for (var i = 0; i < projectList.length; i++) {
			var project = projectList[i];
			var monthBreakUp = getMonthsBreakup(
				nlapiStringToDate(project.StartDate),
				nlapiStringToDate(project.EndDate));

			for (var j = 0; j < monthBreakUp.length; j++) {
				var months = monthBreakUp[j];
				try {
					var rec = nlapiCreateRecord(
						'customrecord_job_month_rev_projection', {
						recordmode: 'dynamic'
					});
					rec.setFieldValue('custrecord_prp_project', project.Id);
					rec.setFieldValue('custrecord_prp_start_date',months.Start);
					var recId = nlapiSubmitRecord(rec, true, true);
					nlapiLogExecution('debug', 'record created', recId);
					yieldScript(context);
				} catch (err) {
					nlapiLogExecution('ERROR', 'Failed To Create Break-Up For '
						+ project.Id + " for " + months.Start, err);
					Send_Exception_Mail(err);
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
		Send_Exception_Mail(err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	var dateBreakUp = [];
	var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		.getMonth(), 1);

	dateBreakUp.push({
		Start: nlapiDateToString(new_start_date, 'date'),
		End: ''
	});

	for (var i = 1; ; i++) {
		var new_date = nlapiAddMonths(new_start_date, i);

		if (new_date > d_endDate) {
			break;
		}

		dateBreakUp.push({
			Start: nlapiDateToString(new_date, 'date'),
			End: ''
		});
	}

	return dateBreakUp;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
				+ state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Rev Projection Project Wise';
        
        var s_Body = 'This is to inform that Rev Projection Project Wise is having an issue and System is not able to send the email notification to employees.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​

