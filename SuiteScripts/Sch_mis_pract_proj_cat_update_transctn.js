//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=940
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_Set_SBC_Lines_Memo_on_bill.js
	Author: Jayesh V Dinde
	Company: Aashna CLoudtech PVT LTD.
	Date: 23 June 2016
	Description: This script will update SBC lines MEMO field as per brillio standard memo on vendor bill.
				 These are populated through AIT script. Need for this script was, we can't edit AIT script hence new script created.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


*/

}

function afterSubmit_UpdateMemo(type)
{
	var submit_record_flag = 0;	
	try
	{
		
		var je_list = new Array();
		var filter = new Array();
		var context = nlapiGetContext();
		var a_results_je = searchRecord('transaction', 'customsearch2479', null, null);
		if (_logValidation(a_results_je))
		{	
			for(var counter = 0; counter<a_results_je.length ; counter++)
			{
				if(je_list.indexOf(a_results_je[counter].getId())>=0)
				{
						
				}
				else
				{
					var vendor_bill_rcrd = nlapiLoadRecord('journalentry',a_results_je[counter].getId());
					var expense_line_count = vendor_bill_rcrd.getLineItemCount('time');
                
					nlapiLogExecution('debug','Expense Line Count',expense_line_count);
					for (var k = 1; k <= expense_line_count; k++)
					{
                      	  var isApply = vendor_bill_rcrd.getLineItemValue('time','apply',k);
                      var usageEnd = context.getRemainingUsage();
						if (usageEnd < 1000) 
						{
							yieldScript(context);
						}
                      if(isApply == 'T')
                        {
							
						var s_employee_name_split = '';
						var emp_practice = '';
						var emp_practice_text = '';
						var s_proj_desc_split = '';
						var s_employee_name = '';
						var s_employee_id = '';
						var s_employee_name_id = '';
						var proj_desc = '';
						var project_entity_id = '';
						var s_proj_desc_id = '';
						var existing_practice = '';
						var proj_full_name_with_id = '';
						var employee_with_id = '';
						var proj_category_val='';
						var core_practice = '';
						var  misc_practice = '';
						s_employee_name = vendor_bill_rcrd.getLineItemValue('time', 'employee', k);
						s_employee_id = vendor_bill_rcrd.getLineItemValue('time', 'custcol_employee_entity_id', k);
						if (_logValidation(s_employee_id)) {
							s_employee_name_split = s_employee_id;
							nlapiLogExecution('audit', 's_employee_name_split expense', s_employee_name_split);
						}
						else 
							if (_logValidation(s_employee_name)) {
								//s_employee_name_id = s_employee_name.split('-');
                             s_employee_name_split= nlapiLookupField('employee', s_employee_name, 'entityid');
								//s_employee_name_split = s_employee_name_id[0];
								nlapiLogExecution('audit', 's_employee_name', s_employee_name);
							}
						if (_logValidation(s_employee_name) || _logValidation(s_employee_id)) {
							var filters_emp = new Array();
							filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
							var column_emp = new Array();
							column_emp[0] = new nlobjSearchColumn('custentity_persontype');
							column_emp[1] = new nlobjSearchColumn('employeetype');
							column_emp[2] = new nlobjSearchColumn('subsidiary');
							column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
							column_emp[4] = new nlobjSearchColumn('department');
							column_emp[5] = new nlobjSearchColumn('location');
							var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
							nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
							if (_logValidation(a_results_emp)) {
								var emp_id = a_results_emp[0].getId();
								emp_practice = a_results_emp[0].getValue('department');
								if(emp_practice){
									var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
									var isinactive_Practice_e = is_practice_active_e.isinactive;
									nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
									core_practice=is_practice_active_e.custrecord_is_delivery_practice;
									nlapiLogExecution('debug','core_practice',core_practice);
								}
								emp_practice_text = a_results_emp[0].getText('department');
								var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
					
							}
							
						}
						proj_desc = vendor_bill_rcrd.getLineItemValue('time', 'custcolprj_name', k);
						var customer_ID = vendor_bill_rcrd.getLineItemValue('time', 'custcol_customer_entityid', k);
						project_entity_id = vendor_bill_rcrd.getLineItemValue('time', 'custcol_project_entity_id', k);
						nlapiLogExecution('audit', 'project_entity_id', project_entity_id);
						if (project_entity_id) {
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit', 's_proj_desc_split', s_proj_desc_split);
						}
						else 
							if (proj_desc) {
								s_proj_desc_id = proj_desc.substr(0, 9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit', 'proj_desc', proj_desc);
							}
						if (_logValidation(s_proj_desc_split)) {
							s_proj_desc_split = s_proj_desc_split.trim();
							var filters_search_proj = new Array();
							filters_search_proj[0] = new nlobjSearchFilter('entityid', null, 'contains', s_proj_desc_split);
							var column_search_proj = new Array();
							column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
							column_search_proj[1] = new nlobjSearchColumn('customer');
							column_search_proj[2] = new nlobjSearchColumn('custentity_region', 'customer');
							column_search_proj[3] = new nlobjSearchColumn('entityid');
							column_search_proj[4] = new nlobjSearchColumn('altname');
							column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
							column_search_proj[6] = new nlobjSearchColumn('companyname', 'customer');
							column_search_proj[7] = new nlobjSearchColumn('entityid', 'customer');
							column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
							column_search_proj[9] = new nlobjSearchColumn('territory', 'customer');
							column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
							column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
							
							var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
							if (_logValidation(search_proj_results)) {
								var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
								var i_region_id = search_proj_results[0].getValue('custentity_region');
								var i_proj_rcrd = nlapiLoadRecord('job', search_proj_results[0].getId());
								var i_proj_name = i_proj_rcrd.getFieldValue('altname');
								var i_proj_entity_id = search_proj_results[0].getValue('entityid');
								var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
								var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
								var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
								var i_project_service = search_proj_results[0].getValue('custentity_project_services');
								proj_category_val = search_proj_results[0].getValue('custentity_project_allocation_category');
								var i_cust_name = search_proj_results[0].getValue('companyname', 'customer');
								var i_cust_entity_id = search_proj_results[0].getValue('entityid', 'customer');
								var i_cust_territory = search_proj_results[0].getValue('territory', 'customer');
								if(i_proj_practice){
									var is_practice_active_p = nlapiLookupField('department',parseInt(i_proj_practice),['isinactive']);
									var isinactive_Practice_p = is_practice_active_p.isinactive;
									nlapiLogExecution('debug','isinactive_Practice_p',isinactive_Practice_p);
								}
																
								//vendor_bill_rcrd.selectLineItem('time', k);			
								//
								//
								if(proj_category_val)
								{
									if((parseInt(proj_category_val)==parseInt(1)) && (core_practice =='T'))
									{
										misc_practice=emp_practice;
										var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
										isinactive_Practice_e = is_practice_active.isinactive;
										misc_practice = emp_practice_text;
									}
									else 
									{
										misc_practice=search_proj_results[0].getValue('custentity_practice');
										var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
										isinactive_Practice_e = is_practice_active.isinactive;
										misc_practice=search_proj_results[0].getText('custentity_practice');
									}
								}
											  nlapiLogExecution('audit','Pratice','misc_practice:'+misc_practice);
								if(misc_practice && isinactive_Practice_e == 'F'){
                                 // vendor_bill_rcrd.setLineItemValue('time', 'custcol_mis_practice', k, misc_practice);
								vendor_bill_rcrd.setLineItemValue('time', 'custcol_mis_practice', k, misc_practice);
								}

								vendor_bill_rcrd.setLineItemValue('time', 'custcol_proj_category_on_a_click',k, i_proj_category);
								//vendor_bill_rcrd.commitLineItem('time');
								
								submit_record_flag = 1;
							}
						}
                    }
					}
					//var line= 'expense';
                 var line= 'line';
					var i_line_count = vendor_bill_rcrd.getLineItemCount(line);
	
					for (var i = 1; i <= i_line_count; i++)
					{
						var usageEnd = context.getRemainingUsage();
						if (usageEnd < 1000) 
						{
							yieldScript(context);
						}	
						var emp_type = '';
						var emp_fusion_id = '';
						var person_type = '';
						var onsite_offsite = '';
						var s_employee_name_split = '';
						var emp_frst_name = '';
						var emp_middl_name = '';
						var emp_lst_name = '';
						var emp_full_name = '';
						var emp_blnk_flag = 0;
						var emp_practice = '';
						var emp_practice_text = '';
						var emp_location = '';
						var employee_with_id = '';
						var proj_name = '';
						var proj_entity_id = '';
						var proj_billing_type = '';
						var s_proj_desc_split = '';
						var region_id = '';
						var proj_category = '';
						var proj_vertical= '';
						var proj_full_name_with_id = '';
						var proj_practice = '';
						var project_id = '';
						var i_project_service = '';
						var proj_category_val='';
						var core_practice = '';
						var cust_name = '';
						var cust_entity_id = '';
						var cust_id = '';
						var cust_internal_id = '';
						var cust_territory = '';
						var customer_id = '';
						var customerObj = '';
						var customer_region = '';
						var cust_name_with_id = '';
						var s_cust_id_split = ''; 
						var  misc_practice = '';
						var isinactive_Practice_mis = '';
						var s_employee_name = vendor_bill_rcrd.getLineItemValue(line,'custcol_employeenamecolumn',i);
						var s_employee_id = vendor_bill_rcrd.getLineItemValue(line,'custcol_employee_entity_id',i);	
						if(s_employee_id){
							s_employee_name_split = s_employee_id;
							nlapiLogExecution('audit','s_employee_name_split',s_employee_name_split);
						}
						else if(s_employee_name){
							var s_employee_name_id = s_employee_name.split('-');
							s_employee_name_split = s_employee_name_id[0];
							nlapiLogExecution('audit','s_employee_name',s_employee_name);
						}
						if (s_employee_name || s_employee_id)
						{
							var filters_emp = new Array();
							filters_emp[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_employee_name_split);
							var column_emp = new Array();
							column_emp[0] = new nlobjSearchColumn('custentity_persontype');
							column_emp[1] = new nlobjSearchColumn('employeetype');
							column_emp[2] = new nlobjSearchColumn('subsidiary');
							column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
							column_emp[4] = new nlobjSearchColumn('firstname');
							column_emp[5] = new nlobjSearchColumn('middlename');
							column_emp[6] = new nlobjSearchColumn('lastname');
							column_emp[7] = new nlobjSearchColumn('department');
							column_emp[8] = new nlobjSearchColumn('location');
									
							var a_results_emp_id = nlapiSearchRecord('employee', null, filters_emp, column_emp);
							if (_logValidation(a_results_emp_id))
							{
									var emp_id = a_results_emp_id[0].getId();
									emp_type = a_results_emp_id[0].getText('employeetype');
									person_type = a_results_emp_id[0].getText('custentity_persontype');
									emp_fusion_id = a_results_emp_id[0].getValue('custentity_fusion_empid');
									emp_frst_name = a_results_emp_id[0].getValue('firstname');
									emp_middl_name = a_results_emp_id[0].getValue('middlename');
									emp_lst_name = a_results_emp_id[0].getValue('lastname');
									
									var emp_subsidiary = a_results_emp_id[0].getValue('subsidiary');
									emp_practice = a_results_emp_id[0].getValue('department');
									emp_practice_text = a_results_emp_id[0].getText('department');
									if(emp_practice){
										var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
										core_practice=is_practice_active_e.custrecord_is_delivery_practice;
										nlapiLogExecution('debug','core_practice',core_practice);
									}
									emp_location = a_results_emp_id[0].getValue('location');
									submit_record_flag = 1;
							}
						}
						else
							emp_blnk_flag = 1;
							//Customer
							var proj_desc = vendor_bill_rcrd.getLineItemValue(line,'custcolprj_name',i);
							var proj_desc_id = vendor_bill_rcrd.getLineItemValue(line,'custcol_project_entity_id',i);
							if(proj_desc_id){
								s_proj_desc_split = proj_desc_id;
								nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
							}
							else if(proj_desc){
								s_proj_desc_id = proj_desc.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_desc',proj_desc);
							}
							if (proj_desc || proj_desc_id )
							{
								s_proj_desc_split = s_proj_desc_split.trim();
								var filters_proj = new Array();
								filters_proj[0] = new nlobjSearchFilter('entityid', null, 'startswith', s_proj_desc_split);
								var column_proj = new Array();
								column_proj[0] = new nlobjSearchColumn('jobbillingtype');
								column_proj[1] = new nlobjSearchColumn('customer');
								column_proj[2] = new nlobjSearchColumn('custentity_region');
								column_proj[3] = new nlobjSearchColumn('entityid');
								column_proj[4] = new nlobjSearchColumn('altname');
								column_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
								column_proj[6] = new nlobjSearchColumn('companyname','customer');
								column_proj[7] = new nlobjSearchColumn('entityid','customer');
								column_proj[8] = new nlobjSearchColumn('custentity_vertical');
								column_proj[9] = new nlobjSearchColumn('territory','customer');
								column_proj[10] = new nlobjSearchColumn('custentity_practice');
								column_proj[11] = new nlobjSearchColumn('internalid','customer');
								column_proj[12] = new nlobjSearchColumn('internalid');
								column_proj[13] = new nlobjSearchColumn('custentity_project_services');
								var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
								if (a_results_proj)
								{
									project_id = a_results_proj[0].getValue('internalid');
									proj_billing_type = a_results_proj[0].getText('jobbillingtype');
									var proj_rcrd = nlapiLoadRecord('job',parseInt(project_id));
									proj_name = proj_rcrd.getFieldValue('altname');
									proj_entity_id = a_results_proj[0].getValue('entityid');
									proj_category = a_results_proj[0].getText('custentity_project_allocation_category');
									proj_vertical = a_results_proj[0].getValue('custentity_vertical');
									i_project_service = a_results_proj[0].getValue('custentity_project_services');
									proj_category_val = a_results_proj[0].getValue('custentity_project_allocation_category');
									if(emp_blnk_flag == 1)
										proj_practice = a_results_proj[0].getValue('custentity_practice');
										
									if(proj_practice){
											var is_practice_active = nlapiLookupField('department',parseInt(proj_practice),['isinactive']);
											var isinactive_Practice = is_practice_active.isinactive;
										nlapiLogExecution('debug','isinactive_Practice',isinactive_Practice);
									}
									submit_record_flag = 1;
								}
							}
							if(proj_category_val)
							{
								if((parseInt(proj_category_val)==parseInt(1)) && (core_practice =='T'))
								{
									misc_practice=emp_practice;
									var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
									var isinactive_Practice_mis = is_practice_active.isinactive;
									misc_practice = emp_practice_text;
								}
								else 
								{
									misc_practice=a_results_proj[0].getValue('custentity_practice');
									var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
									var isinactive_Practice_mis = is_practice_active.isinactive;
									misc_practice = a_results_proj[0].getText('custentity_practice');
								}
							}
							nlapiLogExecution('audit','Pratice','misc_practice:'+misc_practice);
							if(misc_practice && isinactive_Practice_mis == 'F'){
							vendor_bill_rcrd.setLineItemValue(line, 'custcol_mis_practice', i, misc_practice);
							}//
							vendor_bill_rcrd.setLineItemValue(line, 'custcol_proj_category_on_a_click', i, proj_category);
					
					}
					//var je_submit_id = nlapiSubmitRecord(vendor_bill_rcrd,true,true);
                 // var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd,true,true);
					var item_line_count = vendor_bill_rcrd.getLineItemCount('item');
					for (var d = 1; d <= item_line_count; d++)
					{
						var usageEnd = context.getRemainingUsage();
						if (usageEnd < 1000) 
						{
							yieldScript(context);
						}	
						var emp_frst_name = '';
						var emp_practice = '';
						var emp_practice_text = '';
						var s_proj_desc_split = '';
						var s_employee_name_split = '';
						var proj_name_selected = '';
						var s_employee_name = '';
						var s_employee_id = '';
						var s_employee_name_id = '';
						var proj_name_selected = '';
						var project_entity_id = '';
						var s_proj_desc_id = '';
						var existing_practice = '';
						var proj_full_name_with_id = '';
						var employee_with_id = '';
					
						var  misc_practice = '';
						var proj_category_val='';
						var core_practice = '';
				
						existing_practice = vendor_bill_rcrd.getLineItemValue('item', 'department',d);
						s_employee_name = vendor_bill_rcrd.getLineItemValue('item', 'custcol_employeenamecolumn', d);
						s_employee_id = vendor_bill_rcrd.getLineItemValue('item','custcol_employee_entity_id',d);	
						if(_logValidation(s_employee_id)){
							 s_employee_name_split = s_employee_id;
							 nlapiLogExecution('audit','s_employee_name_split item',s_employee_name_split);
						}
						else if(_logValidation(s_employee_name)){
							s_employee_name_id = s_employee_name.split('-');
							 s_employee_name_split = s_employee_name_id[0];
							 nlapiLogExecution('audit','s_employee_name',s_employee_name);
						}
						if(_logValidation(s_employee_name) || _logValidation(s_employee_id))
						{
							var filters_emp = new Array();
							filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',s_employee_name_split);
							var column_emp = new Array();	
							column_emp[0] = new nlobjSearchColumn('custentity_persontype');
							column_emp[1] = new nlobjSearchColumn('employeetype');
							column_emp[2] = new nlobjSearchColumn('subsidiary');
							column_emp[3] = new nlobjSearchColumn('custentity_fusion_empid');
							column_emp[4] = new nlobjSearchColumn('department');
							column_emp[5] = new nlobjSearchColumn('location');
							var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
							nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
							if (_logValidation(a_results_emp))
							{
								var emp_id = a_results_emp[0].getId();
								var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
								emp_practice = a_results_emp[0].getValue('department');		
								emp_practice_text = a_results_emp[0].getText('department');								
								if(emp_practice){
										var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive','custrecord_is_delivery_practice']);
										var isinactive_Practice_e = is_practice_active_e.isinactive;
										nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
										core_practice=is_practice_active_e.custrecord_is_delivery_practice;
										nlapiLogExecution('debug','core_practice',core_practice);
								}
								
								submit_record_flag = 1;
							}
						}
						proj_name_selected = vendor_bill_rcrd.getLineItemValue('item', 'custcolprj_name', d);
						project_entity_id = vendor_bill_rcrd.getLineItemValue('item','custcol_project_entity_id',d);
						if(project_entity_id){
							s_proj_desc_split = project_entity_id;
							nlapiLogExecution('audit','s_proj_desc_split',s_proj_desc_split);
						}
						else if(proj_name_selected){
								s_proj_desc_id = proj_name_selected.substr(0,9);
								s_proj_desc_split = s_proj_desc_id;
								nlapiLogExecution('audit','proj_name_selected',proj_name_selected);
						}
						if (_logValidation(s_proj_desc_split))
						{
							var filters_search_proj = new Array();
							filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'is',s_proj_desc_split);
							var column_search_proj = new Array();
							column_search_proj[0] = new nlobjSearchColumn('jobbillingtype');
							column_search_proj[1] = new nlobjSearchColumn('customer');
							column_search_proj[2] = new nlobjSearchColumn('custentity_region','customer');
							column_search_proj[3] = new nlobjSearchColumn('entityid');
							column_search_proj[4] = new nlobjSearchColumn('altname');
							column_search_proj[5] = new nlobjSearchColumn('custentity_project_allocation_category');
							column_search_proj[6] = new nlobjSearchColumn('companyname','customer');
							column_search_proj[7] = new nlobjSearchColumn('entityid','customer');
							column_search_proj[8] = new nlobjSearchColumn('custentity_vertical');
							column_search_proj[9] = new nlobjSearchColumn('territory','customer');
							column_search_proj[10] = new nlobjSearchColumn('custentity_practice');
							column_search_proj[11] = new nlobjSearchColumn('custentity_project_services');
						
							var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
							if (_logValidation(search_proj_results))
							{
								var proj_billing_type = search_proj_results[0].getText('jobbillingtype');
								var i_region_id = search_proj_results[0].getValue('custentity_region');
								var i_proj_rcrd = nlapiLoadRecord('job',search_proj_results[0].getId());
								var i_proj_name = i_proj_rcrd.getFieldValue('altname');
								var i_proj_entity_id = search_proj_results[0].getValue('entityid');
								var i_proj_category = search_proj_results[0].getText('custentity_project_allocation_category');
								var i_proj_vertical = search_proj_results[0].getValue('custentity_vertical');
								var i_proj_practice = search_proj_results[0].getValue('custentity_practice');
								var i_project_service = search_proj_results[0].getValue('custentity_project_services');													
								proj_category_val = search_proj_results[0].getValue('custentity_project_allocation_category');	
								if(i_proj_practice){
									var is_practice_active_p = nlapiLookupField('department',parseInt(i_proj_practice),['isinactive']);
									var isinactive_Practice_p = is_practice_active_p.isinactive;
									nlapiLogExecution('debug','isinactive_Practice_p',isinactive_Practice_p);
								}
										
								vendor_bill_rcrd.selectLineItem('item', d);
								
								if(proj_category_val)
								{
									if((parseInt(proj_category_val)==parseInt(1)) && (core_practice =='T'))
									{
										misc_practice=emp_practice;
										var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
										isinactive_Practice_e = is_practice_active.isinactive;
										misc_practice = emp_practice_text;
									}
									else 
									{
										misc_practice=search_proj_results[0].getValue('custentity_practice');
										var is_practice_active = nlapiLookupField('department',parseInt(misc_practice),['isinactive']);
										isinactive_Practice_e = is_practice_active.isinactive;
										misc_practice=search_proj_results[0].getText('custentity_practice');
									}
								}
								nlapiLogExecution('audit','Pratice','misc_practice:'+misc_practice);
								if(misc_practice && isinactive_Practice_e == 'F'){
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_mis_practice', misc_practice);
								}
							//			
								vendor_bill_rcrd.setCurrentLineItemValue('item', 'custcol_proj_category_on_a_click', i_proj_category);
								vendor_bill_rcrd.commitLineItem('item');
								submit_record_flag = 1;	
							}
						}								
					}
			
					if(submit_record_flag == 1)
					{
						var vendor_bill_submitted_id = nlapiSubmitRecord(vendor_bill_rcrd,true,true);
						nlapiLogExecution('debug','submitted bill id:-- ',vendor_bill_submitted_id);
					}
					je_list.push(vendor_bill_submitted_id);
					yieldScript(context);
				}
			}
			nlapiLogExecution('DEBUG', 'usageEnd --j-->' + counter);
			
		}			
	//
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',e);
	}
	
	return true;

}

// END AFTER SUBMIT ===============================================



// BEGIN FUNCTION ===================================================
{

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

}
function yieldScript(currentContext) {

		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function timestamp() {
var str = "";

var currentTime = new Date();
var hours = currentTime.getHours();
var minutes = currentTime.getMinutes();
var seconds = currentTime.getSeconds();
var meridian = "";
if (hours > 12) {
    meridian += "pm";
} else {
    meridian += "am";
}
if (hours > 12) {

    hours = hours - 12;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}
str += hours + ":" + minutes + ":" + seconds + " ";

return str + meridian;
}