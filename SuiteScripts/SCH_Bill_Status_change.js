function changeBillStatus()
{
	try
	{
		//var finance_managers = [];
		//finance_managers[0] = '8177';
		//finance_managers[1] = '1615';
		//finance_managers[2] = '78911';
		var vendorbillSearch = nlapiSearchRecord("transaction",null,
		[
			["type","anyof","VendBill"], 
			"AND", 
			["internalid","anyof","2004906"],
			"AND", 
			["mainline","is","T"]
		], 
		[
			new nlobjSearchColumn("internalid")
		]
		);
		for(var i = 0 ; i < vendorbillSearch.length; i++)
		{
			var bill_id = vendorbillSearch[i].getValue('internalid');
			var rec = nlapiLoadRecord('vendorbill',bill_id);
			rec.setFieldValue('approvalstatus',1);
			var id = nlapiSubmitRecord(rec);
			nlapiLogExecution('debug', 'Bill id', bill_id);
			nlapiLogExecution('debug', 'submit_id', id);
		}
		
		/*var invoiceSearch = nlapiSearchRecord("invoice",null,
		[
			["type","anyof","CustInvc"], 
			"AND", 
			["createdby","anyof","170842"], 
			"AND", 
			["mainline","is","T"]
		], 
		[
			new nlobjSearchColumn("ordertype").setSort(false), 
			new nlobjSearchColumn("internalid"), 
			new nlobjSearchColumn("trandate"), 
			new nlobjSearchColumn("tranid")
		]
		);
		
		var finance_managers = [];
		finance_managers[0] = 8177;
		if(invoiceSearch)
		{
			for(var i = 0 ; i < invoiceSearch.length ; i++)
			{
				var bill_id = invoiceSearch[i].getValue('internalid');
				
				var rec = nlapiLoadRecord('invoice',bill_id);
				rec.setFieldValues('custbody_financemanager',finance_managers);
				var id = nlapiSubmitRecord(rec);
				
				nlapiLogExecution('debug', 'bill id', bill_id);
				nlapiLogExecution('debug', 'submit_id', id);
			}
		}*/
		
		/*var journalentrySearch = nlapiSearchRecord("journalentry",null,
		[
			["type","anyof","Journal"], 
				"AND", 
			["internalid","anyof","1285983"], 
				"AND", 
			["mainline","is","T"]
		], 
		[
			new nlobjSearchColumn("internalid")
		]
		);
		for(var i = 0 ; i < journalentrySearch.length; i++)
		{
			var JE_id = journalentrySearch[i].getValue('internalid');
			var rec = nlapiLoadRecord('journalentry',JE_id);
			rec.setFieldValue('approved', 'T');
			var id = nlapiSubmitRecord(rec);
			nlapiLogExecution('debug', 'JE id', JE_id);
			nlapiLogExecution('debug', 'submit_id', id);
		}*/
		
		/*var customerSearch = nlapiSearchRecord("customer",null,
		[
			["isinactive","is","T"], 
			"AND", 
			["datecreated","on","11/27/2019 11:59 pm"]
		], 
		[		
			new nlobjSearchColumn("internalid"), 
			new nlobjSearchColumn("isinactive")
		]
		);
		
		for(var i = 0; i < customerSearch.length; i++)
		{
			var id = customerSearch[i].getValue('internalid');
			var rec = nlapiLoadRecord('customer',id);
			rec.setFieldValue('isinactive','F');
			nlapiSubmitRecord(rec);
		}*/
	}
	catch (err)
	{
		nlapiLogExecution('Debug','Error in the process',err);
	}
}