// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SUT_Report_SalNOnStdRptFromTS.js
     Author:
     Company:Aashna Cloud tech
     Date:20-08-2014
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     20-08-2014
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// END GLOBAL VARIABLE BLOCK  =======================================


// BEGIN SUITELET ==================================================

function GenerateReport(request, response){

    try {
        var a_employeearray = new Array();
        if (request.getMethod() == 'GET') {
            var s_period = '';
            var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS');
            var oldvalues = false;
            var flag = 0;
            form.setScript('customscriptcli_checkdayofweek')
            var contextObj = nlapiGetContext();
            nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)
            
            var beginUsage = contextObj.getRemainingUsage();
            //  nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
            
            var select = form.addField('custpage_startdate', 'Date', 'Start Date :');
            
            var s_beforedate = request.getParameter('custpage_startdate');
            nlapiLogExecution('DEBUG', 'Search results ', ' s_beforedate =' + s_beforedate)
            var s_afterdate = request.getParameter('custpage_fromdate');///why this code?
            nlapiLogExecution('DEBUG', 'Search results ', ' s_afterdate =' + s_afterdate)
            var sublist1 = form.addSubList('record', 'list', 'Sal-Non-Std Rpt-From TS');
            var id = request.getParameter('id');
            nlapiLogExecution('DEBUG', 'Post results ', ' id=' + id)
            if (id == 'Export') {
                var param = request.getParameter('param');
                nlapiLogExecution('DEBUG', 'Post results ', ' param=' + param)
                
                var datesplit = param.toString().split('@')
                nlapiLogExecution('DEBUG', 'Post results ', ' datesplit=' + datesplit)
                
                s_beforedate = datesplit[0]
                nlapiLogExecution('DEBUG', 'Post results ', ' s_beforedate=' + s_beforedate)
                s_afterdate = datesplit[1]
                nlapiLogExecution('DEBUG', 'Post results ', ' s_afterdate=' + s_afterdate)
            }
            
            if (s_beforedate == null) {
                oldvalues = false;
            }
            else {
                oldvalues = true;
            }
            
            
            if (s_afterdate == null) {
                oldvalues = false;
            }
            else {
            
                oldvalues = true;
            }
            nlapiLogExecution('DEBUG', 'GET', ' oldvalues===>' + oldvalues)
            ///////////////////////////////////// POST ON SUBMIT ////////////////////////////
            if (oldvalues) {
                form.addButton('custombutton', 'Export As CSV', 'fxn_generatePDF(\'' + s_beforedate + "@" + s_afterdate + '\');');
                
                var id = request.getParameter('id');
                nlapiLogExecution('DEBUG', 'Post results ', ' id=' + id)
                if (id == 'Export') {
                    var param = request.getParameter('param');
                    nlapiLogExecution('DEBUG', 'Post results ', ' param=' + param)
                    
                    var datesplit = param.toString().split('@')
                    nlapiLogExecution('DEBUG', 'Post results ', ' datesplit=' + datesplit)
                    
                    s_beforedate = datesplit[0]
                    s_afterdate = datesplit[1]
                }
                
                var searchData = SearchData(s_beforedate, s_afterdate)
                nlapiLogExecution('DEBUG', 'Post results ', ' searchData=' + searchData.length)
                
                var SearchDTDetailresult = SearchDTData(s_beforedate, s_afterdate)
                nlapiLogExecution('DEBUG', 'Post results ', ' SearchDTDetailresult=' + SearchDTDetailresult.length)
                var weeklyDTTotal = getDTTotalsweekly(sublist1, SearchDTDetailresult, s_afterdate)
                
                var data = setValuesublist(sublist1, searchData, s_afterdate, weeklyDTTotal)
                
                /////detail search fr OT time sheet fr cal of DT
                
                
                // nlapiLogExecution('DEBUG', 'Post results ', ' searchData=' + data.length)
                nlapiLogExecution('DEBUG', 'Post results ', 's_beforedate=' + s_beforedate)
                nlapiLogExecution('DEBUG', 'Post results ', 's_afterdate=' + s_afterdate)
                for (var x = 0; x < data.length; x++) {
                    var i_split_1 = data[x].toString().split('##')
                    //    nlapiLogExecution('DEBUG', 'Post results ', ' i_split_1=' + i_split_1)
                    var i_date = i_split_1[0]
                    // nlapiLogExecution('DEBUG', 'Post results ', ' i_date=' + i_date)
                    var s_employee = i_split_1[1]
                    // nlapiLogExecution('DEBUG', 'Post results ', ' s_employee=' + s_employee)
                    var s_projectTask = i_split_1[2]
                    // nlapiLogExecution('DEBUG', 'Post results ', ' s_projectTask=' + s_projectTask)
                    a_employeearray.push(i_date)
                    a_employeearray.push(s_employee)
                    a_employeearray.push(s_projectTask)
                }
                nlapiLogExecution('DEBUG', 'Post results ', ' a_employeearray=' + a_employeearray)
            }
            
            if (oldvalues) {
                select.setDefaultValue(s_beforedate);
                
            }
            
            if (id != 'Export') {
                form.addSubmitButton('Submit');
                response.writePage(form);
            }
            else {
                Callsuitelet(sublist1, param);
            }
            
        }
        else {
            var date = request.getParameter('custpage_startdate');
            //nlapiLogExecution('DEBUG', 'Post results ', ' date =' + date)
            var date1 = nlapiStringToDate(date)
            //  nlapiLogExecution('DEBUG', 'Post results ', ' date after string to date1 =' + date1)
            var fromDate = nlapiAddDays(date1, -13)
            // nlapiLogExecution('DEBUG', 'Post results ', ' fromDate =' + fromDate)
            fromDate = nlapiDateToString(fromDate)
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate after conversion =' + fromDate)
            
            
            var params = new Array();
            params['custpage_startdate'] = date;
            params['custpage_fromdate'] = fromDate;
         //   nlapiSetRedirectURL('SUITELET', 'customscriptsut_report_salnonstdrptfromt', 'customdeploy1', false, params);
		 nlapiSetRedirectURL('SUITELET', 'customscriptsut_report_salnonstdrptfrom', 'customdeploy1', false, params);
        }
    } 
    catch (e) {
        nlapiLogExecution('DEBUG', 'Search results ', ' value of e-====== **************** =' + e)
    }
}

function setValuesublist(sublist1, searchData, s_afterdate1, weeklyDTTotal){
    var s_CoCode = sublist1.addField('custevent_cocode', 'text', 'Co Code');
    var s_batchid = sublist1.addField('custevent_batchid', 'text', 'Batch ID');
    var s_file = sublist1.addField('custevent_file', 'text', 'File #');
    var s_Employeefile = sublist1.addField('custevent_employeeid', 'text', 'Employee ID');
    var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
    var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
    var s_WK_ST = sublist1.addField('custevent_wkst1', 'text', 'WK1 ST');
    s_WK_ST.setDefaultValue('0');
    
    var s_WK_OT = sublist1.addField('custevent_wkot1', 'text', 'WK1 OT');
    var s_WK_DT = sublist1.addField('custevent_wkdt1', 'text', 'WK1 DT');
    var s_WK1_Timeoff = sublist1.addField('custevent_wk1timeoff1', 'text', 'WK1 TIMEOFF');
    var s_WK2_ST = sublist1.addField('custevent_wkst2', 'text', 'WK2 ST');
    var s_WK2_OT = sublist1.addField('custevent_wkot2', 'text', 'WK2 OT');
    var s_WK2_DT = sublist1.addField('custevent_wkdt2', 'text', 'WK2 DT');
    var s_WK2_Timeoff = sublist1.addField('custevent_wk1timeoff2', 'text', 'WK2 TIMEOFF');
    var s_ot_hours = sublist1.addField('custevent_othours', 'text', 'OT Hours');
    var s_Total_Time_Off = sublist1.addField('custevent_totaltimeoff', 'text', 'Total Timeoff');
    var s_Floating_H = sublist1.addField('custevent_floatingh', 'text', 'Float H(if in either wk)');
    
    var s_ProjectCity = sublist1.addField('custevent_projectcity', 'text', 'Project_City');
    var s_ProjectLocation = sublist1.addField('custevent_projectloc', 'text', 'Project Location');
    var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Employee_Type');
    var s_Department = sublist1.addField('custevent_department', 'textarea', 'Department');
    var s_User_Notes = sublist1.addField('custevent_usernotes', 'textarea', 'User_Notes');
    var s_id = ''
    var linenumber = 1;
    var tempInc = ''
    var currentLinenumber = 0;
    var tempdate = '';
    
    /////////////get stardateweekno
    s_afterdate1 = nlapiStringToDate(s_afterdate1);
    //************** pass the date as parameter to getWeekNumber function *****************//
    var startdateweek = getWeekNumber(s_afterdate1)
    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
    //////////////////////////////////////
    
    
    var a_result_array = new Array();
    var i_totalTime = 0;
    if (searchData.length != '' && searchData.length != null && searchData.length != 'undefined') {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        for (var c = 0; c < searchData.length; c++) {
            var i_Calendar_Week = ''
            var i_Employee = ''
            var i_Duration = ''
            var i_Project_Task_Name = '';
            var i_Duration_decimal = 0;
            
            
            var i_Employee_First_Name = '';
            var i_Employee_Last_Name = '';
            var i_Employee_Inc_Id = '';
            var Employee_Type = ''
            var Employment_Category = ''
            var i_note = ''
            var a_search_transaction_result = searchData[c];
            ////nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'a_search_transaction_result  -->' + a_search_transaction_result);
            // nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'c  -->' + c);
            var columns = a_search_transaction_result.getAllColumns();
            // //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columns  -->' + columns);
            
            var columnLen = columns.length;
            ////nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'columnLen  -->' + columnLen);
            
            
            for (var hg = 0; hg < columnLen; hg++) {
            
                var column = columns[hg];
                ////nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                var label = column.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
                
                var value = a_search_transaction_result.getValue(column)
                var text = a_search_transaction_result.getText(column)
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                if (label == 'Calendar Week') {
                    i_Calendar_Week = value
                }
                if (label == 'Employee') {
                    i_Employee = value;
                }
                if (label == 'Duration') {
                    i_Duration = value;
                }
                if (label == 'Project Task : Name') {
                    i_Project_Task_Name = value;
                }
                if (label == 'Duration:Decimal') {
                    i_Duration_decimal = parseInt(value);
                }
                if (label == 'Employee : First Name') {
                    i_Employee_First_Name = value;
                }
                if (label == 'Employee : Last Name') {
                    i_Employee_Last_Name = value;
                }
                if (label == 'Employee : Inc Id') {
                    i_Employee_Inc_Id = value;
                }
                /*
                 if (label == 'Employee : Type') {
                 Employee_Type = text;
                 }
                 */
                if (label == 'Employee : Employment Category') {
                    Employment_Category = text;
                }
                if (label == 'Note') {
                    i_note = value
                }
                if (label == 'User_Notes') {
                    i_note = value
                }
                if (label == 'Department') {
                    s_Department = text;
                }
                if (label == 'Emp ID') {
                    s_Employeefile = value;
                }
                if (label == 'Project Location') {
                    s_ProjectLocation = text;
                    
                }
                if (label == 'Project City') {
                    s_ProjectCity = text;
                }
                
                if (label == 'EmpIntID') {
                    s_id = value;
                }
                //  nlapiLogExecution('DEBUG', 'SearchData', 'hg===' + hg)
            }
            //column for loop end
            
            if (s_Employeefile != '' && s_Employeefile != null && s_Employeefile != 'undefined' && s_Employeefile != '- None -') {
                s_Employeefile = s_Employeefile
            }
            else {
                s_Employeefile = ''
            }
            
            
            if (i_Employee_First_Name != '' && i_Employee_First_Name != null && i_Employee_First_Name != 'undefined' && i_Employee_First_Name != '- None -') {
                i_Employee_First_Name = i_Employee_First_Name
            }
            else {
                i_Employee_First_Name = ''
            }
            
            if (i_Employee_Last_Name != '' && i_Employee_Last_Name != null && i_Employee_Last_Name != 'undefined' && i_Employee_Last_Name != '- None -') {
                i_Employee_Last_Name = i_Employee_Last_Name
            }
            else {
                i_Employee_Last_Name = ''
            }
            
            if (s_Department != '' && s_Department != null && s_Department != 'undefined' && s_Department != '- None -') {
                s_Department = s_Department
            }
            else {
                s_Department = ''
            }
            if (i_Employee_Inc_Id != '' && i_Employee_Inc_Id != null && i_Employee_Inc_Id != 'undefined' && i_Employee_Inc_Id != '- None -') {
                i_Employee_Inc_Id = i_Employee_Inc_Id
            }
            else {
                i_Employee_Inc_Id = ''
            }
            if (i_note != '' && i_note != null && i_note != 'undefined' && i_note != '- None -') {
                i_note = i_note
            }
            else {
                i_note = ''
            }
            if (Employment_Category != '' && Employment_Category != null && Employment_Category != 'undefined' && Employment_Category != '- None -') {
                Employment_Category = Employment_Category
            }
            else {
                Employment_Category = ''
            }
            /*
             if (Employee_Type != '' && Employee_Type != null && Employee_Type != 'undefined' && Employee_Type != '- None -') {
             Employee_Type = Employee_Type
             }
             else {
             Employee_Type = ''
             }
             */
            if (s_ProjectLocation != '' && s_ProjectLocation != null && s_ProjectLocation != 'undefined' && s_ProjectLocation != '- None -') {
                s_ProjectLocation = s_ProjectLocation
            }
            else {
                s_ProjectLocation = ''
            }
            if (s_ProjectCity != '' && s_ProjectCity != null && s_ProjectCity != 'undefined' && s_ProjectCity != '- None -') {
                s_ProjectCity = s_ProjectCity
            }
            else {
                s_ProjectCity = ''
            }
            
            //////////////////Uppercase conversion of taksnames for comparison to get week hrs as all ids per project are different/////////////
            i_Project_Task_Name = i_Project_Task_Name.toString().toUpperCase();
            
            var s_OTTask = "OT"
            var s_STTask = "Project Activities"
            var s_LeaveTask = "Leave"
            var s_HolidayTask = "Holiday"
            var s_FHTask = "Floating Holiday"
            
            
            s_OTTask = s_OTTask.toString().toUpperCase();
            s_STTask = s_STTask.toString().toUpperCase();
            s_LeaveTask = s_LeaveTask.toString().toUpperCase();
            s_HolidayTask = s_HolidayTask.toString().toUpperCase();
            s_FHTask = s_FHTask.toString().toUpperCase();
            /*
             nlapiLogExecution('DEBUG', 'SearchData', 's_OTTask===' + s_OTTask)
             nlapiLogExecution('DEBUG', 'SearchData', 's_STTask===' + s_STTask)
             nlapiLogExecution('DEBUG', 'SearchData', 's_LeaveTask===' + s_LeaveTask)
             nlapiLogExecution('DEBUG', 'SearchData', 's_HolidayTask===' + s_HolidayTask)
             nlapiLogExecution('DEBUG', 'SearchData', 's_HolidayTask===' + s_HolidayTask)
             nlapiLogExecution('DEBUG', 'SearchData', 's_FHTask===' + s_FHTask)
             */
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
            
            //  nlapiLogExecution('DEBUG', 'SearchData', 'tempInc===' + tempInc)
            //  nlapiLogExecution('DEBUG', 'SearchData', 's_id===' + s_id)
            /////calculate current date week/////////////////////
            
            i_Calendar_Week = nlapiStringToDate(i_Calendar_Week);
            i_Calendar_Week = nlapiAddDays(i_Calendar_Week, 1)
            //************** pass the date as parameter to getWeekNumber function *****************//
            var currentdateweek = getWeekNumber(i_Calendar_Week)
            //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_Calendar_Week=' + i_Calendar_Week);
           // nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
            
            /*
             if (currentdateweek < startdateweek) {
             currentdateweek = startdateweek
             }
             */
         //   nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'currentdateweek=' + currentdateweek);
            //////////////////////////////////////////////////////
            var defaultvalue = 0.0;
            
            if (tempInc != s_id) {
                i_totalTime = 0;
                
                sublist1.setLineItemValue('custevent_cocode', linenumber, 'TBF');
                sublist1.setLineItemValue('custevent_batchid', linenumber, 'VP0518');
                sublist1.setLineItemValue('custevent_file', linenumber, i_Employee_Inc_Id);
                sublist1.setLineItemValue('custevent_employeeid', linenumber, s_Employeefile);
                sublist1.setLineItemValue('custevent_firstname', linenumber, i_Employee_First_Name);
                sublist1.setLineItemValue('custevent_lastname', linenumber, i_Employee_Last_Name);
                sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                sublist1.setLineItemValue('custevent_department', linenumber, s_Department);
                sublist1.setLineItemValue('custevent_usernotes', linenumber, i_note.toString());
                sublist1.setLineItemValue('custevent_wkst1', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_wkot1', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_wkot2', linenumber, parseInt(defaultvalue));
                
                sublist1.setLineItemValue('custevent_wkst2', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_othours', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_totaltimeoff', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_wkdt1', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_wkdt2', linenumber, parseInt(defaultvalue));
                sublist1.setLineItemValue('custevent_projectcity', linenumber, s_ProjectCity);
                sublist1.setLineItemValue('custevent_projectloc', linenumber, s_ProjectLocation);
                sublist1.setLineItemValue('custevent_floatingh', linenumber, parseInt(defaultvalue));
                if (i_Project_Task_Name == s_OTTask) {
                    if (startdateweek == currentdateweek) {
                        sublist1.setLineItemValue('custevent_wkot1', linenumber, parseInt(i_Duration_decimal));
                    }
                    else {
                        sublist1.setLineItemValue('custevent_wkot2', linenumber, parseInt(i_Duration_decimal));
                    }
                    
                }
                else 
                    if (i_Project_Task_Name == s_STTask) {
                        if (startdateweek == currentdateweek) {
                            sublist1.setLineItemValue('custevent_wkst1', linenumber, parseInt(i_Duration_decimal));
                        }
                        else {
                            sublist1.setLineItemValue('custevent_wkst2', linenumber, parseInt(i_Duration_decimal));
                        }
                    }
                    else 
                        if (i_Project_Task_Name == s_LeaveTask || i_Project_Task_Name == s_HolidayTask) {
                            // nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                            if (i_Project_Task_Name == s_LeaveTask) {
                                i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                            //nlapiLogExecution('DEBUG', 'SearchData', 'if i_totalTime in Leave===' + i_totalTime);
                            }
                            else 
                                if (i_Project_Task_Name == s_HolidayTask) {
                                    i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                // nlapiLogExecution('DEBUG', 'SearchData', 'if i_totalTime in Holiday===' + i_totalTime);
                                }
                            
                            if (startdateweek == currentdateweek) {
                                sublist1.setLineItemValue('custevent_wk1timeoff1', linenumber, i_totalTime);
                            //  nlapiLogExecution('DEBUG', 'SearchData', 'custevent_wk1timeoff1===' + i_totalTime);
                            // nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_First_Name===' + i_Employee_First_Name)
                            /// nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_Last_Name===' + i_Employee_Last_Name)
                            }
                            else {
                                sublist1.setLineItemValue('custevent_wk1timeoff2', linenumber, i_totalTime);
                            ///nlapiLogExecution('DEBUG', 'SearchData', 'custevent_wk1timeoff2===' + i_totalTime);
                            ///nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_First_Name===' + i_Employee_First_Name)
                            // nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_Last_Name===' + i_Employee_Last_Name)
                            }
                            
                        }
                        else 
                            if (i_Project_Task_Name == s_FHTask && parseInt(i_Duration_decimal) > 0) {
                            
                                sublist1.setLineItemValue('custevent_floatingh', linenumber, parseInt(1));
                            }
                            else {
                                sublist1.setLineItemValue('custevent_floatingh', linenumber, parseInt(0));
                            }
                currentLinenumber = linenumber;
                linenumber++;
            }
            else 
                if (tempInc == s_id) {
                    if (i_Project_Task_Name == s_OTTask) {
                        if (startdateweek == currentdateweek) {
                            sublist1.setLineItemValue('custevent_wkot1', currentLinenumber, parseInt(i_Duration_decimal));
                        }
                        else {
                            sublist1.setLineItemValue('custevent_wkot2', currentLinenumber, parseInt(i_Duration_decimal));
                        }
                        
                        
                    }
                    else 
                        if (i_Project_Task_Name == s_STTask) {
                            if (startdateweek == currentdateweek) {
                                sublist1.setLineItemValue('custevent_wkst1', currentLinenumber, parseInt(i_Duration_decimal));
                            }
                            else {
                                sublist1.setLineItemValue('custevent_wkst2', currentLinenumber, parseInt(i_Duration_decimal));
                            }
                            
                            
                        }
                        else 
                            if (i_Project_Task_Name == s_LeaveTask || i_Project_Task_Name == s_HolidayTask) {
                                if (i_Project_Task_Name == s_LeaveTask) {
                                    i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                }
                                else 
                                    if (i_Project_Task_Name == s_HolidayTask) {
                                        nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                                        i_totalTime = parseFloat(i_totalTime) + parseFloat(i_Duration_decimal)
                                        
                                    }
                                
                                if (startdateweek == currentdateweek) {
                                
                                    sublist1.setLineItemValue('custevent_wk1timeoff1', currentLinenumber, i_totalTime);
                                    
                                }
                                else {
                                    nlapiLogExecution('DEBUG', 'SearchData', 'In week off 2');
                                    sublist1.setLineItemValue('custevent_wk1timeoff2', currentLinenumber, i_totalTime);
                                    
                                }
                            }
                            else 
                                if (i_Project_Task_Name == s_FHTask && parseInt(i_Duration_decimal) > 0) {
                                    nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_First_Name===' + i_Employee_First_Name)
                                    nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_Last_Name===' + i_Employee_Last_Name)
                                    nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                                    sublist1.setLineItemValue('custevent_floatingh', currentLinenumber, parseInt(1));
                                }
                                else {
                                    nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_First_Name===' + i_Employee_First_Name)
                                    nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee_Last_Name===' + i_Employee_Last_Name)
                                    nlapiLogExecution('DEBUG', 'SearchData', 'i_Project_Task_Name===' + i_Project_Task_Name)
                                    sublist1.setLineItemValue('custevent_floatingh', currentLinenumber, parseInt(0));
                                }
                    
                    ////////////////DT Column set/////////////////////// 
                    
                    var value = setDTvalue(sublist1, currentLinenumber, weeklyDTTotal, s_id, startdateweek, currentdateweek);
					nlapiLogExecution('DEBUG', 'SearchData', 'Settiing DT value');
                    //////////////OT and Week off totals columns calculations//////////////////////////////////////
                    var week1OT = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
                    var week2OT = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
                    
                    
                    // nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                    if (week1OT == '' || week1OT == 'undefined' || week1OT == null) {
                        week1OT = 0;
                    }
                    if (week2OT == '' || week2OT == 'undefined' || week2OT == null) {
                        week2OT = 0;
                    }
                    // nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                    var OTweekstotals = parseInt(week1OT) + parseInt(week2OT);
                    // nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + parseInt(week1OT) + ',,' + parseInt(week2OT));
                    // nlapiLogExecution('DEBUG', 'SearchData', 'OTweekstotals===' + OTweekstotals);
                    sublist1.setLineItemValue('custevent_othours', currentLinenumber, parseInt(OTweekstotals));
                    
                    var week1off = sublist1.getLineItemValue('custevent_wk1timeoff1', currentLinenumber);
                    var week2off = sublist1.getLineItemValue('custevent_wk1timeoff2', currentLinenumber);
                    // nlapiLogExecution('DEBUG', 'SearchData', 'week1OT,week2OT===' + week1OT + ',,' + week2OT);
                    if (week1off == '' || week1off == 'undefined' || week1off == null) {
                        week1off = 0;
                    }
                    if (week2off == '' || week2off == 'undefined' || week2off == null) {
                        week2off = 0;
                    }
                    //  nlapiLogExecution('DEBUG', 'SearchData', 'week1off,week2off===' + week1off + ',,' + week2off);
                    var weeksOFFtotals = parseInt(week1off) + parseInt(week2off);
                    // nlapiLogExecution('DEBUG', 'SearchData', 'weeksOFFtotals===' + weeksOFFtotals);
                    sublist1.setLineItemValue('custevent_totaltimeoff', currentLinenumber, parseInt(weeksOFFtotals));
                ////////////////////////////////////////////////////////////////////////////////////////////////
                }
            
            tempInc = s_id
            
            
        }
    }
    //  nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array)
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array.length)
    return a_result_array;
}

function SearchData(s_beforedate, s_afterdate){
    /*
     nlapiLogExecution('DEBUG', 'SearchData', 'In Search Data')
     nlapiLogExecution('DEBUG', 'SearchData', 's_beforedate===' + s_beforedate)
     nlapiLogExecution('DEBUG', 'SearchData', 's_afterdate===' + s_afterdate)
     */
    var a_filters = new Array();
    //a_filters[0] = new nlobjSearchFilter('date', null, 'on', s_beforedate);
    a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
    a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
    
    
    var a_columns = new Array();
    a_columns[0] = new nlobjSearchColumn('employee');
    a_columns[1] = new nlobjSearchColumn('item');
    
    var searchresult = nlapiSearchRecord('timebill', 'customsearchsalnonstdrptts', a_filters, null);
    return searchresult
}


/*
 Function name :-getWeekNumber()
 Parameter :- date
 return type :- returns week number of given year
 */
function getWeekNumber(d){
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'weekNo=' + weekNo);
    // Return array of year and week number
    return weekNo;
}

function Callsuitelet(sublist1, param){


    var count = sublist1.getLineItemCount();
    //alert(count);
    
    var globalArray = new Array();
    var arr = new Array();
    var month1 = nlapiGetFieldText('custpage_startdate');
    
    var curUser = nlapiGetUser()
    // alert(month1)
    for (var i = 1; i <= count; i++) {
        var cocode = sublist1.getLineItemValue('custevent_cocode', i);
        //alert(custevent_pruchesdetalis);
        var batchid = sublist1.getLineItemValue('custevent_batchid', i);
        //alert(custevent_devicesold);
        var file = sublist1.getLineItemValue('custevent_file', i);
        //alert('custevent_month0' + custevent_month0);
        var firstname = sublist1.getLineItemValue('custevent_firstname', i);
        //alert('custevent_month1' + custevent_month1);
        var lastname = sublist1.getLineItemValue('custevent_lastname', i);
        
        if (file != '' && file != null && file != 'undefined') {
            file = file
        }
        else {
            file = ''
        }
        
        if (firstname != '' && firstname != null && firstname != 'undefined') {
            firstname = firstname
        }
        else {
            firstname = ''
        }
        if (lastname != '' && lastname != null && lastname != 'undefined') {
            lastname = lastname
        }
        else {
            lastname = ''
        }
        //alert('custevent_month2' + custevent_month2);
        var wkst1 = sublist1.getLineItemValue('custevent_wkst1', i);
        //alert('custevent_month3' + custevent_month3);
        var wkot1 = sublist1.getLineItemValue('custevent_wkot1', i);
        //alert('custevent_month4' + custevent_month4);
        var wkdt1 = sublist1.getLineItemValue('custevent_wkdt1', i);
        //alert('custevent_month5' + custevent_month5);
        var wk1timeoff1 = sublist1.getLineItemValue('custevent_wk1timeoff1', i);
        //alert('custevent_month6' + custevent_month6);
        var wkst2 = sublist1.getLineItemValue('custevent_wkst2', i);
        //alert('custevent_month7' + custevent_month7);
        var wkot2 = sublist1.getLineItemValue('custevent_wkot2', i);
        //alert('custevent_month8' + custevent_month8);
        var wkdt2 = sublist1.getLineItemValue('custevent_wkdt2', i);
        //alert('custevent_month9' + custevent_month9);
        var wk1timeoff2 = sublist1.getLineItemValue('custevent_wk1timeoff2', i);
        //alert('custevent_month10' + custevent_month10);
        var totaltimeoff = sublist1.getLineItemValue('custevent_totaltimeoff', i);
        var floatingh = sublist1.getLineItemValue('custevent_floatingh', i);
        
        
        var projectcity = sublist1.getLineItemValue('custevent_projectcity', i);
        var employeetype = sublist1.getLineItemValue('custevent_employeetype', i);
        var usernotes = sublist1.getLineItemValue('custevent_usernotes', i);
        if (projectcity != '' && projectcity != null && projectcity != 'undefined') {
            projectcity = projectcity
        }
        else {
            projectcity = ''
        }
        if (usernotes != '' && usernotes != null && usernotes != 'undefined') {
            usernotes = usernotes
        }
        else {
            usernotes = ''
        }
        if (employeetype != '' && employeetype != null && employeetype != 'undefined') {
            employeetype = employeetype
        }
        else {
            employeetype = ''
        }
        usernotes = usernotes.toString().replace(/[|]/g, " ")
        usernotes = usernotes.toString().replace(/[,]/g, " ")
        var employeeid = sublist1.getLineItemValue('custevent_employeeid', i);
        var department = sublist1.getLineItemValue('custevent_department', i);
        var projectloc = sublist1.getLineItemValue('custevent_projectloc', i);
        if (i == 1) {
            arr[i] = "Co Code,Batch ID,File #,Employee ID,First Name,Last Name,WK1 ST,WK1 OT,WK1 DT,WK1 TIMEOFF,WK2 ST,WK2 OT,WK2 TIMEOFF,OT Hours,Total Timeoff,Float H(if in either wk),Project Location,Project_City,Employee_Type,Department,User Notes\n";
        }
        else {
            arr[i] = cocode + "," + batchid + "," + file + "," + employeeid + "," + firstname + "," + lastname + "," + wkst1 + "," + wkot1 + "," + wkdt1 + "," + wk1timeoff1 + "," + wkst2 + "," + wkot2 + "," + wkdt2 + "," + wk1timeoff2 + "," + totaltimeoff + ',' + floatingh + ',' + projectloc + "," + projectcity + ',' + employeetype + ',' + department + "," + usernotes + "\n";
        }
        
        
        globalArray[i] = (arr[i]);
    }
    var fileName = 'Sal- NOn- Std  Rpt - From TS'
    var Datetime = new Date();
    var CSVName = fileName + " - " + Datetime + '.csv';
    var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
    
    nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.setContentType('CSV', CSVName);
    
    nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' PDF Name-->  ');
    response.write(file.getValue());
    
}

function setDTvalue(sublist1, currentLinenumber, weeklyDTTotal, s_id, startdateweek, currentdateweek)
{
	nlapiLogExecution('DEBUG', 'SearchData', 'weeklyDTTotal===' + weeklyDTTotal)
    if (weeklyDTTotal != null && weeklyDTTotal != '' && weeklyDTTotal != 'undefined') 
	{
        for (var i = 0; i < weeklyDTTotal.length; i++) 
		{
            var split = weeklyDTTotal[i].toString().split("##")
            nlapiLogExecution('DEBUG', 'SearchData', 'split===' + split)
            var i_DTTime = split[0]
            nlapiLogExecution('DEBUG', 'SearchData', 'i_DTTime===' + i_DTTime)
            var i_employeeid = split[1]
            nlapiLogExecution('DEBUG', 'SearchData', 'i_employeeid===' + i_employeeid)
            var i_weekno = split[2]
            nlapiLogExecution('DEBUG', 'SearchData', 'i_employeeid===' + i_employeeid)
            
            if (i_employeeid == s_id) {
                if (startdateweek == i_weekno) {
                    sublist1.setLineItemValue('custevent_wkdt1', currentLinenumber, parseInt(i_DTTime));
                }
                else {
                    sublist1.setLineItemValue('custevent_wkdt2', currentLinenumber, parseInt(i_DTTime));
                }
            }
        }
    }
    
    /*
    
     var dt = 0
     var week1OT1 = sublist1.getLineItemValue('custevent_wkot1', currentLinenumber);
     nlapiLogExecution('DEBUG', 'SearchData', 'week1OT1===' + week1OT1)
     var week2OT1 = sublist1.getLineItemValue('custevent_wkot2', currentLinenumber);
     nlapiLogExecution('DEBUG', 'SearchData', 'week2OT1===' + week2OT1)
    
    
     var week1ST1 = sublist1.getLineItemValue('custevent_wkst1', currentLinenumber);
    
    
     nlapiLogExecution('DEBUG', 'SearchData', 'week1ST1===' + week1ST1)
    
    
     var week2ST1 = sublist1.getLineItemValue('custevent_wkst2', currentLinenumber);
    
    
     nlapiLogExecution('DEBUG', 'SearchData', 'week2ST1===' + week2ST1)
    
    
     
    
    
     if (week1ST1 == 40 && week1OT1 > 28) {
    
    
     dt = parseFloat(week1OT1) - parseFloat(28)
    
    
     nlapiLogExecution('DEBUG', 'SearchData', 'dt===' + dt)
    
    
     sublist1.setLineItemValue('custevent_wkdt1', currentLinenumber, parseInt(dt));
    
    
     }
    
    
     if (week2ST1 == 40 && week2OT1 > 28) {
    
    
     dt = parseFloat(week2OT1) - parseFloat(28)
    
    
     nlapiLogExecution('DEBUG', 'SearchData', 'dt===' + dt)
    
    
     sublist1.setLineItemValue('custevent_wkdt2', currentLinenumber, parseInt(dt));
    
    
     }
    
    
     */
    
    
}


function getDTTotalsweekly(sublist1, searchData, s_afterdate1)
{


    /////////////get stardateweekno
    s_afterdate1 = nlapiStringToDate(s_afterdate1);
    //************** pass the date as parameter to getWeekNumber function *****************//
    var startdateweek = getWeekNumber(s_afterdate1)
    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
    //////////////////////////////////////
    
    
    var a_week1_array = new Array();
    var a_weekarray = new Array();
    var finalarray = new Array();
    if (searchData.length != '' && searchData.length != null && searchData.length != 'undefined') {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        for (var c = 0; c < searchData.length; c++) 
		{
            var i_Calendar_Week = ''
            var i_Duration_decimal = 0;
            
            var i_Employee = '';
            
            i_Employee = searchData[c].getValue('employee');
             nlapiLogExecution('DEBUG', 'SearchData', 'i_Employee===' + i_Employee)
            i_Calendar_Week = searchData[c].getValue('date');
            
            i_Duration_decimal = searchData[c].getValue('hours');
            i_Duration_decimal = parseFloat(i_Duration_decimal)
            nlapiLogExecution('DEBUG', 'SearchData', 'i_Duration_decimal===' + i_Duration_decimal)
            var i_Calendar_Week1 = nlapiStringToDate(i_Calendar_Week);
            // i_Calendar_Week = nlapiAddDays(i_Calendar_Week, 1)
            // nlapiLogExecution('DEBUG', 'SearchData', 'i_Calendar_Week===' + i_Calendar_Week)
            var currentdateweek = getWeekNumber(i_Calendar_Week1)
            nlapiLogExecution('DEBUG', 'SearchData', 'currentdateweek===' + currentdateweek)
            //s_afterdate1 = nlapiStringToDate(s_afterdate1);
            var startdateweek = getWeekNumber(s_afterdate1)
            // nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'startdateweek=' + startdateweek);
            
            if (i_Duration_decimal > 4) {
                a_weekarray.push(i_Employee + "##" + currentdateweek)
                a_week1_array.push(i_Employee + "##" + i_Duration_decimal + "##" + currentdateweek)
            }
            
        }
        nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'a_week1_array=' + a_week1_array);
        nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'a_weekarray=' + a_weekarray);
        var uniqueweekArray = removearrayduplicate(a_weekarray)
        nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'uniqueweekArray=' + uniqueweekArray);
        for (var a = 0; a < uniqueweekArray.length; a++) 
		{
            var i_totalTime = 0;
            var i_totalTime_1 = 0;
            var i_split = uniqueweekArray[a].toString().split("##")
            // nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_split=' + i_split);
            
            var i_empid1 = i_split[0]
            //   nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_empid1=' + i_empid1);
            
            var i_weekno1 = i_split[1]
            // nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno1=' + i_weekno1);
            
            for (var b = 0; b < a_week1_array.length; b++) {
                var i_split_1 = a_week1_array[b].toString().split("##")
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_split_1=' + i_split_1);
                
                var i_empid2 = i_split_1[0]
                //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_empid2=' + i_empid2);
                
                var i_hours = i_split_1[1]
                /// nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_hours=' + i_hours);
                
                var i_weekno2 = i_split_1[2]
                // nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno2=' + i_weekno2);
                
                if (i_empid1 == i_empid2 && i_weekno1 == i_weekno2) {
                    if (i_hours > 4) {
                        i_totalTime = parseFloat(i_hours) - parseFloat(4)
                    }
                    i_totalTime_1 = parseFloat(i_totalTime_1) + parseFloat(i_totalTime)
                    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno1=' + i_weekno1);
                    
                    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_weekno2=' + i_weekno2);
                    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_empid1=' + i_empid1);
                    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_empid2=' + i_empid2);
                    nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'i_totalTime_1=' + i_totalTime_1);
                }
            }
            
            
            finalarray.push(i_totalTime_1 + "##" + i_empid1 + "##" + i_weekno1)
        }
        
    }
    
    return finalarray;
}
function SearchDTData(s_beforedate, s_afterdate){
    /*
     nlapiLogExecution('DEBUG', 'SearchDTData', 'In Search Data')
     nlapiLogExecution('DEBUG', 'SearchDTData', 's_beforedate===' + s_beforedate)
     nlapiLogExecution('DEBUG', 'SearchDTData', 's_afterdate===' + s_afterdate)
     */
    var a_filters = new Array();
    //a_filters[0] = new nlobjSearchFilter('date', null, 'on', s_beforedate);
    a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_beforedate);
    a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_afterdate);
    
    
    var a_columns = new Array();
    a_columns[0] = new nlobjSearchColumn('employee');
    a_columns[1] = new nlobjSearchColumn('item');
    a_columns[2] = new nlobjSearchColumn('date');
    a_columns[3] = new nlobjSearchColumn('hours');
    
    
    var searchresult = nlapiSearchRecord('timebill', 'customsearchsalnonstdrptts_2', a_filters, a_columns);
    return searchresult
}

function removearrayduplicate(array){
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}