/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 March 2020     Prabhat Gupta
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
 function getRESTlet(dataIn) {

    return {};
}

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {

    try {

        //dataIn = {"lastUpdatedTimestamp":""}
        var response = new Response();
        //nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
        var currentDateAndTime = sysDate();



        var receivedDate = dataIn.lastUpdatedTimestamp;
        //nlapiLogExecution('DEBUG', 'receivedDate', 'receivedDate...' + receivedDate);
        nlapiLogExecution('DEBUG', 'Current Date', 'currentDateAndTime...' + currentDateAndTime);

        //var receivedDate = '7/12/2019 1:00 am';
        //Check if the timestamp is empty
        if (!_logValidation(receivedDate)) { // for empty timestanp in this we will give whole data as a response
            var filters = new Array();
            filters = [

                ["custentity_employee_inactive", "is", "F"],
                "AND",
                ["custentity_implementationteam", "is", "F"],

            ];
            //	nlapiLogExecution('debug', 'filters ', filters);
        } else {
            var filters = new Array(); // this filter will provide the result within the current date and given date     
            filters = [

                ["custentity_implementationteam", "is", "F"],
                "AND",
                ["lastmodifieddate", "within", receivedDate, currentDateAndTime] // prabhat gupta NIS-1349 28/5/2020				
            ];
            //	nlapiLogExecution('debug', 'filters ', filters);
        }

        // created search by grouping 
        var searchResults = searchRecord("employee", null,
            filters,
            [
                new nlobjSearchColumn("internalid"),
                new nlobjSearchColumn("custentity_employee_inactive"),
                new nlobjSearchColumn("custentity_fusion_empid"),
                new nlobjSearchColumn("firstname"),
                new nlobjSearchColumn("middlename"),
                new nlobjSearchColumn("lastname"),
                new nlobjSearchColumn("title"),
                new nlobjSearchColumn("email"),
                new nlobjSearchColumn("department"),
                new nlobjSearchColumn("middlename"),
                new nlobjSearchColumn("lastname"),
                new nlobjSearchColumn("custentity_actual_hire_date"),
                new nlobjSearchColumn("custentity_reportingmanager"),
                new nlobjSearchColumn("employeestatus"),
                new nlobjSearchColumn("custevent4", "resourceAllocation", null),
                new nlobjSearchColumn("custentity_employeetype"), //prabhat gupta
                new nlobjSearchColumn("custentity_persontype"), //prabhat gupta
                new nlobjSearchColumn("location"), //prabhat gupta
                new nlobjSearchColumn("subsidiary"), //prabhat gupta
                //----------------------------------------------------------------------------------------------------------					//prabhat gupta 09/10/2020 NIS-1760

                new nlobjSearchColumn("custentity_resignation_notify_date"),
                new nlobjSearchColumn("custentity_fresher"),
                new nlobjSearchColumn("custentity_future_term_date"),
                new nlobjSearchColumn("custentity_inactivereason"),
                new nlobjSearchColumn("custentity_transferto"),
                new nlobjSearchColumn("custentity_lwd"),
				new nlobjSearchColumn("phone"),
				new nlobjSearchColumn("custentity_emp_hrbp"),
				new nlobjSearchColumn("entityid"),
                new nlobjSearchColumn("custentity_personalemailid"),
                new nlobjSearchColumn("custentity_list_brillio_location_e")
				
                //--------------------------------------------------------------------------------------------------------					

            ]
        );

        var all_practice = practice();

        var all_visaDetail = visaDetail();
        var employeeData = [];

        if (searchResults) {


            for (var i = 0; i < searchResults.length; i++) {

                var employee = {};
                employee.internalId = Number(searchResults[i].getId());
                employee.employeeId = Number(searchResults[i].getValue("custentity_fusion_empid")) || '';

                var emp_frst_name = searchResults[i].getValue('firstname') || '';
                var emp_middl_name = searchResults[i].getValue('middlename') || '';
                var emp_lst_name = searchResults[i].getValue('lastname') || '';
                var emp_full_name = '';
                if (emp_frst_name)
                    emp_full_name = emp_frst_name;

                if (emp_middl_name)
                    emp_full_name = emp_full_name + ' ' + emp_middl_name;

                if (emp_lst_name)
                    emp_full_name = emp_full_name + ' ' + emp_lst_name;


                employee.firstName = emp_frst_name || '';
                employee.middleName = emp_middl_name || '';
                employee.lastName = emp_lst_name || '';
                employee.fullName = emp_full_name || '';
                employee.designation = searchResults[i].getValue("title") || '';
				employee.personalEmailId = searchResults[i].getValue("custentity_personalemailid") || '';
                employee.emailId = searchResults[i].getValue("email") || '';
                var department = searchResults[i].getValue("department");


                if (department) {
                    var j = 0;
                    while (j < all_practice.length) {

                        if (department == all_practice[j].id) {

                            var emp_practice = all_practice[j];
                            break;
                        }
                        j++;
                    }
                }


                employee.practiceData = emp_practice || '';

                var emp_id = searchResults[i].getId();

                if (emp_id) {
                    var k = 0;
                    var emp_visa = [];
                    while (k < all_visaDetail.length) {

                        if (emp_id == all_visaDetail[k].employeeInternalId) {

                            var emp_visaRecord = all_visaDetail[k];
                            emp_visa.push(emp_visaRecord);
                        }
                        k++;
                    }
                }




                employee.visaDetail = emp_visa;


                employee.actualHireDate = searchResults[i].getValue('custentity_actual_hire_date') || '';
                employee.reportingManager = searchResults[i].getValue('custentity_reportingmanager') || '';
                employee.level = searchResults[i].getText('employeestatus') || '';
                //---------------------------------------------------------------------------------------------------------------------			
                //prabhat gupta
                employee.onsiteOffsite = searchResults[i].getText("custevent4", "resourceAllocation", null) || '';
                employee.employeeType = searchResults[i].getText("custentity_employeetype") || ''; // intern direct contract and many more
                employee.personType = searchResults[i].getText("custentity_persontype") || ''; // only employee and Contingent Worker
                employee.location = searchResults[i].getText("location") || '';
                employee.entityId = searchResults[i].getValue('subsidiary'); // prabhat gupta

                //  var subsidiary = searchResults[i].getText('subsidiary').split(":") ; // prabhat gupta

                //   employee.entityName = subsidiary[subsidiary.length-1] ;  // prabhat gupta

                employee.entityName = searchResults[i].getText('subsidiary'); // prabhat gupta
                //------------------------------------------------------------------------------------------------------------------

                //------------------------------------------------------------------------------------------------------------------------
                //prabhat gupta 09/10/2020 NIS-1760
				employee.contactNumber = searchResults[i].getValue('phone') || "";
                employee.resignationNotificationDate = searchResults[i].getValue('custentity_resignation_notify_date') || "";
                employee.fresher = searchResults[i].getValue('custentity_fresher') || "";
                employee.futureTermDate = searchResults[i].getValue('custentity_future_term_date') || "";

                var gtValue = searchResults[i].getValue('custentity_inactivereason') || "";

                if (_logValidation(gtValue)) {

                    employee.globelTransfer = {
                        "reasonId": gtValue,
                        "reason": searchResults[i].getText('custentity_inactivereason') || "",
                        "transferToId": searchResults[i].getValue('custentity_transferto') || "",
                        "transferTo": searchResults[i].getText('custentity_transferto') || "",
                        "lwd": employee.entityName = searchResults[i].getValue('custentity_lwd') || ""
                    }

                } else {
                    employee.globelTransfer = "";
                }



                //-------------------------------------------------------------------------------------------------------------------------		
                employee.isEmployeeInactive = searchResults[i].getValue("custentity_employee_inactive") || '';
                nlapiLogExecution("DEBUG", "i: ", i);
				
				employee.hrbpId = searchResults[i].getValue("custentity_emp_hrbp") || '';
				employee.hrbp = searchResults[i].getText("custentity_emp_hrbp") || '';
                employee.entityNameId = searchResults[i].getValue("entityid") || '';
                employee.BrillioLocation = searchResults[i].getValue("custentity_list_brillio_location_e") || '';
                employeeData.push(employee);

            }
            //  nlapiLogExecution("DEBUG", "i: ",searchResults[i].getId());

        }
        response.timeStamp = currentDateAndTime;
        response.data = employeeData;
        //response.data = dataRow;
        response.status = true;
        //	nlapiLogExecution('debug', 'response',JSON.stringify(response));
    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.timeStamp = '';
        response.data = err;
        response.status = false;
    }
    return response;

}
//validate blank entries
function _logValidation(value) {
    if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}
//Get current date

//Returns timestamp
function timestamp() {
    var str = "";

    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    //str += hours + ":" + minutes + ":" + seconds + " ";
    str += hours + ":" + minutes + " ";
    return str + meridian;
}

function Response() {
    this.status = "";
    this.timeStamp = "";
    this.data = "";

}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (savedSearch) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}
//function to format date object into NetSuite's mm/dd/yyyy format.
function formatNSDate(dateObj) {
    if (dateObj) {
        var nsFormatDate = dateObj.getFullYear() + '-' + addZeros(dateObj.getMonth() + 1, 2) + '-' + addZeros(dateObj.getDate(), 2);
        return nsFormatDate;
    }
    return null;
}

//function to add leading zeros on date parts.
function addZeros(num, len) {
    var str = num.toString();
    while (str.length < len) {
        str = '0' + str;
    }
    return str;
}


function visaDetail() {

    var customrecord_visa_detail = searchRecord("customrecord_empvisadetails", null,
        [
            ["isinactive", "is", "F"],
            "AND",
            ["custrecord_validtill", "notbefore", "today"],
			"AND", 
			["custrecord_visaid.custentity_employee_inactive","is","F"]
        ],
        [
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("custrecord_visa"),
            new nlobjSearchColumn("custrecord_visaid"),
            new nlobjSearchColumn("custrecord_validtill")
        ]
    );

    var data = [];
    if (customrecord_visa_detail) {


        for (var i = 0; i < customrecord_visa_detail.length; i++) {
            data.push({
                "id": customrecord_visa_detail[i].getId(),
                "visaType": customrecord_visa_detail[i].getText("custrecord_visa"),
                "validTill": customrecord_visa_detail[i].getValue("custrecord_validtill"),
                "employeeInternalId": customrecord_visa_detail[i].getValue("custrecord_visaid"),
                "employee": customrecord_visa_detail[i].getText("custrecord_visaid")

            });
        }

    }
    return data;
}


function practice() {

    var practiceSearch = nlapiSearchRecord("department", null,
        [
            ["isinactive", "is", "F"]

        ],
        [
            new nlobjSearchColumn("name"),
            new nlobjSearchColumn("custrecord_parent_practice"),
           // new nlobjSearchColumn("custrecord_hrbusinesspartner")
		   // Commented by shravan for HRBP integration on 7-dec-2020

        ]
    );

    var data = [];
    if (practiceSearch) {


        for (var i = 0; i < practiceSearch.length; i++) {
            data.push({
                "id": practiceSearch[i].getId(),
                "practice": practiceSearch[i].getValue("name"),
                "parentPracticeId": practiceSearch[i].getValue("custrecord_parent_practice"),
                "parentPractice": practiceSearch[i].getText("custrecord_parent_practice")
               // "hrbpId": practiceSearch[i].getValue("custrecord_hrbusinesspartner"),
              //  "hrbp": practiceSearch[i].getText("custrecord_hrbusinesspartner")
			  // Commented by shravan for HRBP integration on 7-dec-2020
            });
        }

    }
    return data;
}