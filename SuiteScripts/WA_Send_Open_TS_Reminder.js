/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 May 2015     nitish.mishra
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	try {

		var empId = nlapiGetFieldValue('employee');
		var empDetails = nlapiLookupField('employee', empId, [ 'firstname',
				'email' ]);

		var start_date = nlapiStringToDate(nlapiGetFieldValue('startdate'));
		var end_date = nlapiAddDays(start_date, 6);
		var s_week_end_date = nlapiDateToString(end_date, 'date');
		var s_week_start_date = nlapiDateToString(start_date, 'date');

		var mailData = openTimesheetReminder(empDetails.firstname,
				s_week_start_date, s_week_end_date);

		nlapiSendEmail('442', empDetails.email, mailData.Subject,
				mailData.Body, null, null, {
					entity : empId
				});
	} catch (err) {
		nlapiLogExecution('error', 'workflowAction', err);
	}
}

function openTimesheetReminder(first_name, week_start_date, week_end_date) {

	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + first_name + ',</p>';
	htmltext += '<p></p>';
	htmltext += '<p>This is to inform you that you have clicked on autofill but did not submit the timesheet for the week <b>'
			+ week_start_date + " - " + week_end_date
			+ '</b>.</p>';
	htmltext += '<p>Your timesheet will be automatically deleted in next 1 hour if you do not click on Submit.</p>';
	htmltext += '<p></p>';	
	htmltext += '</td></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<p><br/>Regards,<br/>';
	htmltext += 'Information Systems</p>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		Subject : 'Timesheet In Open Status : ' + week_start_date + " - "
				+ week_end_date,
		Body : htmltext
	};
}