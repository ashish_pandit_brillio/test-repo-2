/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 May 2015     amol.sahijwani
 * 2.00		  14 Apr 2020	 Praveena 			repalced searched id and record type
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	
	var a_project_list	=	[8080, 4208, 5749];//, 8385, 8386, 8387, 8388, 8389];
	
	var a_resource_allocation_data	= getResourceAllocations(a_project_list);
	
	var filters	=	new Array();
	filters[0]	=	new nlobjSearchFilter('date', null, 'within', 'lastweek');
	
	var columns	=	new Array();
	//columns[0]	=	new nlobjSearchcolumn('')
	
	var a_time_entries	= searchRecord('timebill', 'customsearch_ts_status_rpt', filters, null)//repalced searched id and record type
	
	var o_time_data	=	new Object();
	
	var s_html	=	'<table><tr><th>Employee</th><th>Project Name</th><th>Start date</th><th>End Date</th><th>Percent of Time</th><th>Aproved Hours</th><th>Pending Approval Hours</th><th>Open Hours</th><th>Not Submitted</th></tr>';
	
	for(var i = 0; i < a_time_entries.length; i++)
		{
		
			var columns	=	a_time_entries[i].getAllColumns();
			
			var i_employee	=	a_time_entries[i].getValue(columns[0]);
			var f_duration	=	a_time_entries[i].getValue(columns[3]);
			var i_status	=	a_time_entries[i].getValue(columns[5]);
			
			var o_ts		=	{'employee': i_employee, 'duration': f_duration, 'status': i_status};
			
			if(o_time_data[i_employee] == undefined)
				{
					o_time_data[i_employee]		=	new Array();
					//o_time_data[i_employee][0]	=	o_ts;//{'employee': i_employee, 'duration': f_duration, 'status': i_status};
				}
			//else
				//{
					o_time_data[i_employee].push(o_ts);
				//}
		}
	//response.write(JSON.stringify(o_time_data));
	for(var i = 0; i < a_resource_allocation_data.length; i++)//_employee in a_resource_allocation_data)
		{
		
			var a_time_data	=	o_time_data[a_resource_allocation_data[i]['employee']];
		
			var f_approved_hours			=	0.0;
			var f_pending_approval_hours	=	0.0;
			var f_open_hours	=	0.0;
			var s_not_submitted	= '';
			for(var j = 0; a_time_data != undefined && j < a_time_data.length; j++)
				{
					if(a_time_data[j].status == 3)
						{
							f_approved_hours	=	a_time_data[j].duration;
						}
					else if(a_time_data[j].status == 2)
						{
							f_pending_approval_hours	=	a_time_data[j].duration;
						}
					else if(a_time_data[j].status == 1)
						{
							f_open_hours	=	a_time_data[j].duration;
						}
				}
			if(a_time_data == undefined)
				{
					s_not_submitted	=	'Not Submitted';
				}
			s_html	+=	'<tr>';
			s_html	+=	'<td>' + a_resource_allocation_data[i]['employee_name'] + '</td>';
			s_html	+=	'<td>' + a_resource_allocation_data[i]['project_name'] + '</td>';
			s_html	+=	'<td>' + a_resource_allocation_data[i]['s_start_date'] + '</td>';
			s_html	+=	'<td>' + a_resource_allocation_data[i]['s_end_date'] + '</td>';
			s_html	+=	'<td>' + a_resource_allocation_data[i]['percent_of_time'] + '</td>';
			s_html	+=	'<td>' + f_approved_hours + '</td>';
			s_html	+=	'<td>' + f_pending_approval_hours + '</td>';s_html	+= '<td>' + f_open_hours + '</td>';s_html	+=	'<td>' + s_not_submitted + '</td>';
			s_html	+=	'</tr>';
		}response.write('hi' + s_html + '</table>');
}

function getResourceAllocations(a_project_list)
{
	var filters	=	new Array();
	filters[0]	=	new nlobjSearchFilter('customer', 'job', 'anyof', a_project_list);
	
	var search_results	=	searchRecord('resourceallocation', 'customsearch_rs_time_entry_report', filters, null);
	
	var o_resource_allocations	=	new Array();
	
	for(var i = 0; search_results != null && i < search_results.length; i++)
		{
			var columns		=	search_results[i].getAllColumns();
		
			var i_employee	=	search_results[i].getValue(columns[0]);var s_employee_name	=	search_results[i].getText(columns[0]);
			var i_project	=	search_results[i].getValue(columns[1]);var s_project_name	=	search_results[i].getText(columns[1]);
			var i_percentage_of_time	=	search_results[i].getValue(columns[2]);
			var s_start_date	=	search_results[i].getValue(columns[3]);
			var s_end_date		=	search_results[i].getValue(columns[4]);
		
			o_resource_allocations[i]	=	{'employee': i_employee, 'employee_name': s_employee_name, 'project': i_project, 'project_name': s_project_name,'percent_of_time': i_percentage_of_time, 's_start_date': s_start_date, 's_end_date': s_end_date};
		
		}
	nlapiLogExecution('DEBUG', 'Length: ', search_results.length);
	return o_resource_allocations;
	
}