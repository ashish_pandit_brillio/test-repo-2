/**
 * @author Jayesh
 */

function AfterSubmit()
{
	try
	{
		var o_bill_rcrd = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var expense_line_count = o_bill_rcrd.getLineItemCount('expense');
		var item_line_count = o_bill_rcrd.getLineItemCount('item');
		if(item_line_count > 0)
		{
			var proj_selected = o_bill_rcrd.getLineItemValue('item','custcolprj_name',1);
			var cust_selected = o_bill_rcrd.getLineItemValue('item','custcolcustcol_temp_customer',1);
			var territory = o_bill_rcrd.getLineItemValue('item','custcol_territory',1);
			
			var practice = o_bill_rcrd.getLineItemValue('item','department',1);
			var vertical = o_bill_rcrd.getLineItemValue('item','class',1);
			
			var s_project_id = o_bill_rcrd.getLineItemValue('item','custcol_project_entity_id',1);
			if(!s_project_id)
				s_project_id = '';
				
			var s_project_name = o_bill_rcrd.getLineItemValue('item','custcol_proj_name_on_a_click_report',1);
			if(!s_project_name)
				s_project_name = '';
				
			var s_employee_id = o_bill_rcrd.getLineItemValue('item','custcol_employee_entity_id',1);
			if(!s_employee_id)
				s_employee_id = '';
				
			var s_employee_name = o_bill_rcrd.getLineItemValue('item','custcol_emp_name_on_a_click_report',1);
			if(!s_employee_name)
				s_employee_name = '';
				
			var s_customer_id = o_bill_rcrd.getLineItemValue('item','custcol_customer_entityid',1);
			if(!s_customer_id)
				s_customer_id = '';
				
			var s_customer_name = o_bill_rcrd.getLineItemValue('item','custcol_cust_name_on_a_click_report',1);
			if(!s_customer_name)
				s_customer_name = '';
		}
		
		var is_st_reverse_bill = o_bill_rcrd.getLineItemValue('expense','custcol_st_reverseapply',1);
		var is_st_not_credit_bill = o_bill_rcrd.getLineItemValue('expense','custcol_st_notcredit',1);
			
		for(var line_no = 1; line_no <= expense_line_count; line_no++)
		{
			var is_st_line = o_bill_rcrd.getLineItemValue('expense','custcol_st_reverseline',line_no);
			//if(is_st_reverse_bill == 'T' || is_st_not_credit_bill == 'T')
			{
				var is_st_reverse_sbc = o_bill_rcrd.getLineItemValue('expense','custcol_ait_swachh_cess_reverse',line_no);
				
				if(is_st_line == 'T')
				{
					if(item_line_count <= 0)
					{
						var proj_selected = o_bill_rcrd.getLineItemValue('expense','custcolprj_name',1);
						var cust_selected = o_bill_rcrd.getLineItemValue('expense','custcolcustcol_temp_customer',1);
						var territory = o_bill_rcrd.getLineItemValue('expense','custcol_territory',1);
						var practice = o_bill_rcrd.getLineItemValue('expense','department',1);
						var vertical = o_bill_rcrd.getLineItemValue('expense','class',1);
						
						var s_project_id = o_bill_rcrd.getLineItemValue('expense','custcol_project_entity_id',1);
						if(!s_project_id)
							s_project_id = '';
							
						var s_project_name = o_bill_rcrd.getLineItemValue('expense','custcol_proj_name_on_a_click_report',1);
						if(!s_project_name)
							s_project_name = '';
							
						var s_employee_id = o_bill_rcrd.getLineItemValue('expense','custcol_employee_entity_id',1);
						if(!s_employee_id)
							s_employee_id = '';
							
						var s_employee_name = o_bill_rcrd.getLineItemValue('expense','custcol_emp_name_on_a_click_report',1);
						if(!s_employee_name)
							s_employee_name = '';
							
						var s_customer_id = o_bill_rcrd.getLineItemValue('expense','custcol_customer_entityid',1);
						if(!s_customer_id)
							s_customer_id = '';
							
						var s_customer_name = o_bill_rcrd.getLineItemValue('expense','custcol_cust_name_on_a_click_report',1);
						if(!s_customer_name)
							s_customer_name = '';
						
						//nlapiLogExecution('audit','proj_selected:- '+proj_selected+'::: cust_selected:- '+cust_selected);
					}
					
					//nlapiLogExecution('audit','proj_selected:- '+s_project_id+'::: cust_selected:- '+s_customer_id);
					
					o_bill_rcrd.selectLineItem('expense',line_no);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcolprj_name', proj_selected);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcolcustcol_temp_customer', cust_selected);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_territory', territory);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'department', practice);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'class', vertical);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_project_entity_id', s_project_id);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_employee_entity_id', s_employee_id);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_customer_entityid', s_customer_id);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_proj_name_on_a_click_report', s_project_name);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_emp_name_on_a_click_report', s_employee_name);
					o_bill_rcrd.setCurrentLineItemValue('expense', 'custcol_cust_name_on_a_click_report', s_customer_name);
					
					o_bill_rcrd.commitLineItem('expense');
				}
			}
		}
		
		var i_bill_id = nlapiSubmitRecord(o_bill_rcrd);
		nlapiLogExecution('audit','vendor bill id:- ',i_bill_id);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}
}
