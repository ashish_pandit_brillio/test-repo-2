/**
 * @author Jayesh
 */

function field_Changed(type,name)
{
	if(name == 'custbody_billto')
	{
		var i_project_id = nlapiGetFieldValue('job');
		var s_billing_from_date = nlapiGetFieldValue('custbody_billfrom');
		s_billing_from_date = nlapiStringToDate(s_billing_from_date);
		var s_billing_to_date = nlapiGetFieldValue('custbody_billto');
		
		var filters_search_sow = new Array();
		filters_search_sow[0] = new nlobjSearchFilter('internalid','job','is',i_project_id);
		filters_search_sow[1] = new nlobjSearchFilter('type',null,'is','SalesOrd');
		
		var columns_search_sow = new Array();
		columns_search_sow[0] = new nlobjSearchColumn('datecreated').setSort(true);
		columns_search_sow[1] = new nlobjSearchColumn('tranid');
		columns_search_sow[2] = new nlobjSearchColumn('custbody_projectstartdate');
		columns_search_sow[3] = new nlobjSearchColumn('custbody_projectenddate');
		
		var a_search_sow = nlapiSearchRecord('transaction', null, filters_search_sow, columns_search_sow);
		if(a_search_sow)
		{
			var s_sow_proj_strt_date = a_search_sow[0].getValue('custbody_projectstartdate');
			s_sow_proj_strt_date = nlapiStringToDate(s_sow_proj_strt_date);
			if(s_sow_proj_strt_date <= s_billing_from_date)
			{
				//var s_sow_tranid = a_search_sow[0].getValue('tranid');
				var s_sow_po_no = nlapiLookupField('salesorder',a_search_sow[0].getId(),'otherrefnum');
				nlapiSetFieldValue('otherrefnum',s_sow_po_no);
			}
			else
			{
				for(var i_sow_index=1; i_sow_index<a_search_sow.length; i_sow_index++)
				{
					var s_sow_proj_strt_date = a_search_sow[i_sow_index].getValue('custbody_projectstartdate');
					s_sow_proj_strt_date = nlapiStringToDate(s_sow_proj_strt_date);
					if(s_sow_proj_strt_date <= s_billing_from_date)
					{
						//var s_sow_tranid = a_search_sow[i_sow_index].getValue('tranid');
						var s_sow_po_no = nlapiLookupField('salesorder',a_search_sow[i_sow_index].getId(),'otherrefnum');
						nlapiSetFieldValue('otherrefnum',s_sow_tranid);
						break;
					}
				}
			}
		}
	}
}