/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Jun 2015     Nitish Mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var invoiceId = request.getParameter('invoiceid');
		//invoiceId = 1;
		if (invoiceId) {

			var invoiceDetails = {
				InvoiceNumber : 'Brillio/D/14-15/0376',
				InvoiceDate : '31-Oct-14',
				ContactPerson : 'Amit Nerurkar',
				ReferenceNumber : 'PO No. 3100069891 Dtd. 03-07-2014'
			};

			var pdfTemplate = getPdfTemplate(invoiceDetails);

			var xml = '<?xml version="1.0"?>';			
			xml += '<pdf>';
			xml += '<body style="font-size:small;">';
			xml += pdfTemplate;
			xml += '</body>';
			xml += '</pdf>';

			response.renderPDF(xml);
		} else {
			nlapiLogExecution('debug', 'Invoice Id Not Provided');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function getPdfTemplate(invoiceDetails) {
	try {

		var htmlText = "";

		htmlText += "<table style = 'border:1px solid black; width : 100%;'>";

		htmlText += getFirstRow();
		htmlText += getBlankRow();
		htmlText += getThirdRow(invoiceDetails.InvoiceNumber,
				invoiceDetails.InvoiceDate);
		htmlText += getForthRow(invoiceDetails.ContactPerson);
		htmlText += getFifthRow(invoiceDetails.ReferenceNumber);

		htmlText += "</table>";
		return htmlText;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getPdfTemplate', err);
		throw err;
	}
}

function getFirstRow() {
	var htmlText = "";
	htmlText += "<tr><td style='border:.5px solid black;'>";
	htmlText += "<table>";
	htmlText += "<tr>"
			+ "<td style='text-align: left;'><b>Brillio Technologies Pvt Ltd</b><br/>5th Floor, Thejaswini, Technopark, Trivandrum, Kerala<br/>T2700182 / 40774000 F 2700073</td>"
			+ "<td style = 'text-align: right;'></td>" + "</tr>"
			+ "<tr><td><b>Invoice</b></td></tr>";
	htmlText += "</table>";
	htmlText += "</td></tr>";
	return htmlText;
}

function getBlankRow() {
	var htmlText = "";
	htmlText += "<tr><td style = 'border:0.5px solid black; height:20px;'>";
	htmlText += "</td></tr>";
	return htmlText;
}

function getThirdRow(invoiceNumber, invoiceDate) {
	var htmlText = "";
	htmlText += "<tr><td style = 'border:0.5px solid black;'>";

	htmlText += "<table><tr style='text-decoration:bold;'><td style='width:50%'>Invoice No : "
			+ invoiceNumber
			+ "</td>"
			+ "<td style='width:50%'><b>Date</b> : "
			+ invoiceDate + "</td></tr></table>";

	htmlText += "</td></tr>";
	return htmlText;
}

function getForthRow(contactPerson) {
	var htmlText = "";
	htmlText += "<tr><td style = 'border:0.5px solid black;'>";
	htmlText += "Contact Person : " + contactPerson;
	htmlText += "</td></tr>";
	return htmlText;
}

function getFifthRow(referenceNo) {
	var htmlText = "";
	htmlText += "<tr><td style = 'border:0.5px solid black;'>";
	htmlText += "Reference : " + referenceNo;
	htmlText += "</td></tr>";
	return htmlText;
}