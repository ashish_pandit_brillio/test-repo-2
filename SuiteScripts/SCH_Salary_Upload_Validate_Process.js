	// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
		   Script Name : SCH_Salary_Upload_Validate_Process.js
		Author      : Shweta Chopde
		Date        : 30 July 2014
		Description : Processing of file
	
	
		Script Modification Log:
	
		-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	
	
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.
	
	
		 SCHEDULED FUNCTION
			- scheduledFunction(type)
	
	
		 SUB-FUNCTIONS
			- The following sub-functions are called by the above core functions in order to maintain code
				modularization:
	
				   - NOT USED
	
	*/
	}
	// END SCRIPT DESCRIPTION BLOCK  ====================================
	
	
	// BEGIN SCHEDULED FUNCTION =============================================
	
	function schedulerFunction(type) //
	{
		var context = nlapiGetContext();
		var currentUser = context.getUser();
		var currentUserEmail = context.getEmail();
		var receipentsArray = new Array()
		receipentsArray.push(currentUserEmail);
		receipentsArray.push('netsuite.support@brillio.com');
		receipentsArray.push('billing@brillio.com');
		nlapiLogExecution('DEBUG', 'schedulerFunction', '*** into schedulerFunction ***');
		
		var i_mnt = 0;
		var i_hnt = 0;
		var i_file_status = '';
		var f_is_monthly = 0;
		var f_is_hourly = 0;
		var i_recordID = '';
		var i_CSV_File_ID = '';
		 
		try //
		{
			var i_context = nlapiGetContext();
			i_CSV_File_ID = i_context.getSetting('SCRIPT', 'custscript_csv_file_id_s_u');
			var i_monthly_employee = i_context.getSetting('SCRIPT', 'custscript_monthly_employee_s_u');
			var i_hourly_employee = i_context.getSetting('SCRIPT', 'custscript_hourly_employee_s_u');
			var i_account_debit = i_context.getSetting('SCRIPT', 'custscript_account_debit_s_u');
			var i_account_credit = i_context.getSetting('SCRIPT', 'custscript_account_credit_s_u');
			i_recordID = i_context.getSetting('SCRIPT', 'custscript_record_id_s_u');
			var i_flag = i_context.getSetting('SCRIPT', 'custscript_flag');
			
			var i_Salaried_OT = i_context.getSetting('SCRIPT', 'custscript_sal_emp_ot_hrs_sch');
			var i_Salaried_OT_Diff = i_context.getSetting('SCRIPT', 'custscript_sal_emp_ot_hrs_diff_sch');
			var i_Hourly_Diff = i_context.getSetting('SCRIPT', 'custscript_hourly_emp_diff_hrs_sch');
			
			var i_hourly_FP = i_context.getSetting('SCRIPT', 'custscript_hourly_emp_fp_sch'); //
			var i_hourly_FP_DIFF = i_context.getSetting('SCRIPT', 'custscript_hourly_emp_fp_diff_sch');
			
			var i_hourly_FP_Internal = i_context.getSetting('SCRIPT', 'custscript_hourly_emp_internal_hrs_sch'); //
			var i_hourly_FP_Internal_Diff = i_context.getSetting('SCRIPT', 'custscript_hourly_emp_internal_diff_sch');
			
			nlapiLogExecution('DEBUG', 'schedulerFunction', '1 i_flag -->' + i_flag);
			
			if (i_flag == null || i_flag == '') //
			{
				i_flag = 0;
			}
			
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Record ID -->' + i_recordID);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' CSV File ID -->' + i_CSV_File_ID);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Monthly Employee -->' + i_monthly_employee);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Hourly Employee -->' + i_hourly_employee);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Account Credit -->' + i_account_debit);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' Account Debit -->' + i_account_credit);
			nlapiLogExecution('DEBUG', 'schedulerFunction', '2 i_flag -->' + i_flag);
			
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' i_Salaried_OT -->' + i_Salaried_OT);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' i_Salaried_OT_Diff -->' + i_Salaried_OT_Diff);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' i_Hourly_Diff -->' + i_Hourly_Diff);
			
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' i_hourly_FP -->' + i_hourly_FP);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' i_hourly_FP_DIFF -->' + i_hourly_FP_DIFF);
			
			var i_usage_begin = i_context.getRemainingUsage();
			//nlapiLogExecution('DEBUG', 'schedulerFunction', ' Usage Begin -->' + i_usage_begin);
			
			var i_counter = i_context.getSetting('SCRIPT', 'custscript_counter_v');
			
			if (i_counter != null && i_counter != '' && i_counter != undefined) //
			{
				i_counter = i_counter;
			}//if
			else //
			{
				i_counter = 0;
			}
			
			// ============ SALARIED EMPLOYEE - FILE PROCESS ===================
			if (i_monthly_employee == 'T') //
			{
				i_file_status = monthly_employee_file_process_data(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff);
				f_is_monthly = 1;
			}//Salaried Employee File Processing
			// Added by Vikrant to handle new cases
			
			// ============ SALARIED EMPLOYEE OT - FILE PROCESS ===================
			if (i_Salaried_OT == 'T') //
			{
				i_file_status = Salaried_Emp_OT_Data_Process(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff);
				f_is_monthly = 1;
			}//Salaried Employee File Processing
			// ============ SALARIED EMPLOYEE OT DIFF - FILE PROCESS ===================
			
			if (i_Salaried_OT_Diff == 'T') //
			{
				i_file_status = monthly_employee_file_process_data(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff);
				f_is_monthly = 1;
			}//Salaried Employee File Processing
			// End of Added by Vikrant to handle new cases 
			
			
			
			
			// ================ HOURLY EMPLOYEE - FILE PROCESS =====================s
			if (i_hourly_employee == 'T') //
			{
				i_file_status = hourly_employee_file_process_data(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff, i_hourly_FP_Internal, i_hourly_FP_Internal_Diff)
				f_is_hourly = 1;
			}//Hourly Employee File Processing
			// Added by Vikrant to handle new cases 
			
			// ================ HOURLY EMPLOYEE OT DIFF - FILE PROCESS =====================s
			if (i_Hourly_Diff == 'T' || i_hourly_FP == 'T' || i_hourly_FP_DIFF == 'T' || i_hourly_FP_Internal == 'T' || i_hourly_FP_Internal_Diff == 'T') //
			{
				i_file_status = hourly_employee_file_process_data(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff, i_hourly_FP_Internal, i_hourly_FP_Internal_Diff)
				f_is_hourly = 1;
			} //Hourly Employee File Processing	
			// End of Added by Vikrant to handle new cases 
			
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' ------------ File Status ----------->' + i_file_status);
			
			
			if (i_file_status == 'ERROR') //
			{
				var s_notes = 'File has been removed from the system.<br> You need to upload it again after removing errors from files.';
				if (_logValidation(i_recordID)) //
				{
					var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID);
					
					if (_logValidation(o_recordOBJ)) //
					{
						o_recordOBJ.setFieldValue('custrecord_file_status', 2)
						o_recordOBJ.setFieldValue('custrecord_notes', s_notes)
						o_recordOBJ.setFieldValue('custrecord_csv_file_id_no', i_CSV_File_ID)
						
						if (f_is_monthly == 1) //
						{
							o_recordOBJ.setFieldValue('custrecord_is_monthly_record_created', 'T')
						}//Monthly	
						if (f_is_hourly == 1) //
						{
							o_recordOBJ.setFieldValue('custrecord_is_hourly_record_created', 'T')
						}//Hourly			
						var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
						
						if (_logValidation(i_submitID)) //
						{
							var i_submit_fileID = nlapiDeleteFile(i_CSV_File_ID);
							//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ File Submit ID ----------->' + i_submit_fileID);
						}
					}//Record OBJ				
				}//Record ID		
				// ================= Call Schedule Script ================
				
				var params = new Array();
				params['custscript_file_id_delete'] = i_CSV_File_ID
				params['custscript_record_id_delete'] = i_recordID
				params['custscript_hourly_employee_delete'] = i_hourly_employee
				params['custscript_salaried_employee_delete'] = i_monthly_employee
				
				var status = nlapiScheduleScript('customscript_sch_salary_upload_delete_no', null, params);
				//nlapiLogExecution('DEBUG', 'schedulerFunction', ' Status -->' + status);
			
			}//ERROR
			if (i_file_status == 'SUCCESS') //
			{
				if (_logValidation(i_recordID)) //
				{
					var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
					
					if (_logValidation(o_recordOBJ)) //
					{
						var s_notes = 'Uploaded file has been processed & its correct .<br> Please click on Create JE for further Journal Entry creation .';
						
						o_recordOBJ.setFieldValue('custrecord_notes', s_notes);
						o_recordOBJ.setFieldValue('custrecord_file_status', 1);
						
						var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
					}
				}
			}
			//------------------added by swati-------------------------------------
			if (i_file_status == 'In Progress') //
			{
				if (_logValidation(i_recordID)) //
				{
					var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
					
					if (_logValidation(o_recordOBJ)) //
					{
					
						o_recordOBJ.setFieldValue('custrecord_file_status', 3);
						
						var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
					}
				}
			}
			//--------------------------------------------------------------------
		}
		catch (exception) //
		{
			nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
			var mailTemplate = "";
			var emailBody = exception
			mailTemplate += "<p> This is to inform that the below errror occurred while running Salary Upload Validate Process</p>";
			mailTemplate += "<br/>"
			mailTemplate += emailBody
			mailTemplate += "<p>Regards, <br/> Information Systems</p>";
			nlapiSendEmail( '442', receipentsArray, 'Error in Salary Upload Validate Process',
			mailTemplate);
			var s_notes = 'File has been removed from the system.<br> You need to upload it again after removing errors from files.';
			s_notes = s_notes + '/n Error : ' + exception;
			
			if (_logValidation(i_recordID)) //
			{
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID);
				
				if (_logValidation(o_recordOBJ)) //
				{
					o_recordOBJ.setFieldValue('custrecord_file_status', 2)
					o_recordOBJ.setFieldValue('custrecord_notes', s_notes)
					o_recordOBJ.setFieldValue('custrecord_csv_file_id_no', i_CSV_File_ID)
					
					if (f_is_monthly == 1) //
					{
						o_recordOBJ.setFieldValue('custrecord_is_monthly_record_created', 'T')
					}//Monthly	
					if (f_is_hourly == 1) //
					{
						o_recordOBJ.setFieldValue('custrecord_is_hourly_record_created', 'T')
					}//Hourly			
					var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
					//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
					
					if (_logValidation(i_submitID)) //
					{
						var i_submit_fileID = nlapiDeleteFile(i_CSV_File_ID);
						//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ File Submit ID ----------->' + i_submit_fileID);
					}
				}//Record OBJ				
			}//Record ID		
			// ================= Call Schedule Script ================
			
			var params = new Array();
			params['custscript_file_id_delete'] = i_CSV_File_ID
			params['custscript_record_id_delete'] = i_recordID
			params['custscript_hourly_employee_delete'] = i_hourly_employee
			params['custscript_salaried_employee_delete'] = i_monthly_employee
			
			var status = nlapiScheduleScript('customscript_sch_salary_upload_delete_no', null, params);
			//nlapiLogExecution('DEBUG', 'schedulerFunction', ' Status -->' + status);
		}
		
		nlapiLogExecution('DEBUG', 'schedulerFunction', '*** Execution Complete ***');
	}
	
	// END SCHEDULED FUNCTION ===============================================
	
	
	
	
	
	// BEGIN FUNCTION ===================================================
	/**
	 * 
	 * @param {Object} value
	 * 
	 * Description --> If the value is blank /null/undefined returns false else returns true
	 */
	function _logValidation(value) 
	{
	 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	 {
	  return true;
	 }
	 else 
	 { 
	  return false;
	 }
	}
	
	function decode_base64(s)
	{
		var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
		var n = [[65, 91], [97, 123], [48, 58], [47, 48], [43, 44]];
		
		for (z in n) 
		{
			for (i = n[z][0]; i < n[z][1]; i++) 
			{
				v.push(w(i));
			}
		}
		for (i = 0; i < 64; i++) 
		{
			e[v[i]] = i;
		}
		
		for (i = 0; i < s.length; i += 72) 
		{
			var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
			for (x = 0; x < o.length; x++) 
			{
				c = e[o.charAt(x)];
				b = (b << 6) + c;
				l += 6;
				while (l >= 8) 
				{
					r += w((b >>> (l -= 8)) % 256);
				}
			}
		}
		return r;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	
	function parseCSV (csvString) 
	{
		var fieldEndMarker  = /([,\015\012] *)/g; /* Comma is assumed as field separator */
		var qFieldEndMarker = /("")*"([,\015\012] *)/g; /* Double quotes are assumed as the quote character */
		var startIndex = 0;
		var records = [], currentRecord = [];
		do 
		{
		// If the to-be-matched substring starts with a double-quote, use the qFieldMarker regex, otherwise use fieldMarker.
		var endMarkerRE = (csvString.charAt (startIndex) == '"')  ? qFieldEndMarker : fieldEndMarker;
		endMarkerRE.lastIndex = startIndex;
		var matchArray = endMarkerRE.exec (csvString);
		if (!matchArray || !matchArray.length) 
		{
			break;
		}
		var endIndex = endMarkerRE.lastIndex - matchArray[matchArray.length-1].length;
		var match = csvString.substring (startIndex, endIndex);
		if (match.charAt(0) == '"') 
		{ // The matching field starts with a quoting character, so remove the quotes
			match = match.substring (1, match.length-1).replace (/""/g, '"');
		}
		currentRecord.push (match);
		var marker = matchArray[0];
		if (marker.indexOf (',') < 0)
		{ // Field ends with newline, not comma
			records.push (currentRecord);
			currentRecord = [];
		}
		startIndex = endMarkerRE.lastIndex;
		}   while (true);
		if (startIndex < csvString.length)
		{ // Maybe something left over?
			var remaining = csvString.substring (startIndex).trim();
			if (remaining) currentRecord.push (remaining);
		}
		if (currentRecord.length > 0)
		{ // Account for the last record
			records.push (currentRecord);
		}
		return records;
	};
	
	
	function search_employee(i_employeeID) //
	{
		var i_employee_entity_ID = '';
		var i_subsidiary = '';
		var i_department = '';
		var i_location = '';
		var i_location_txt = '';
		var i_currency = '';
		var i_subsidiary_txt = '';
		var i_recordID = '';
		var i_employee_type = '';
		
		var a_return_array = new Array();
		
		if (_logValidation(i_employeeID)) //
		{
			i_employeeID = i_employeeID.trim();
			var filter = new Array();
			filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employeeID);
			filter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
			
			
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('entityid');
			columns[1] = new nlobjSearchColumn('subsidiary');
			columns[2] = new nlobjSearchColumn('department');
			columns[3] = new nlobjSearchColumn('location');
			columns[4] = new nlobjSearchColumn('internalid');
			columns[5] = new nlobjSearchColumn('employeetype');
			
			
			var a_search_results = nlapiSearchRecord('employee', null, filter, columns);
			
			if (a_search_results != null && a_search_results != '' && a_search_results != undefined) //
			{
				i_employee_entity_ID = a_search_results[0].getValue('entityid');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee ID -->' + i_employee_entity_ID);
				
				i_recordID = a_search_results[0].getText('internalid');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Record ID-->' + i_recordID);
				
				i_subsidiary_txt = a_search_results[0].getText('subsidiary');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Subsidiary Txt-->' + i_subsidiary_txt);
				
				i_subsidiary = a_search_results[0].getValue('subsidiary');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Subsidiary -->' + i_subsidiary);
				
				i_department = a_search_results[0].getValue('department');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Department -->' + i_department);
				
				i_location = a_search_results[0].getValue('location');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Location -->' + i_location);
				
				i_employee_type = a_search_results[0].getValue('employeetype');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Type -->' + i_employee_type);
				
				i_location_txt = a_search_results[0].getText('location');
				//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Location Txt-->' + i_location_txt);
				
			}//Search Results		
		}//Employee ID	
		a_return_array[0] = i_employee_entity_ID + '&&##&&' + i_subsidiary + '&&##&&' + i_department + '&&##&&' + i_location + '&&##&&' + i_location_txt + '&&##&&' + i_subsidiary_txt + '&&##&&' + i_recordID + '&&##&&' + i_employee_type
		
		return a_return_array;
	}//Search Employee
	
	function check_for_numeric_value(i_number) //
	{
		var result = '';
		if (_logValidation(i_number)) //
		{
			result = isNaN(i_number);
			nlapiLogExecution('DEBUG', 'check_no_of_days', ' No Of Days Number / Text -->' + result);
			nlapiLogExecution('DEBUG', 'check_no_of_days', ' No Of Days Number / Number -->' + i_number);
			
			if (result == false) // False means it is a valid number 
			{
				nlapiLogExecution('DEBUG', 'check_no_of_days', '*** Number is valid ***');
				
				if (parseFloat(i_number) <= 0) //
				{
					nlapiLogExecution('DEBUG', 'check_no_of_days', '*** Number is less than 0 ***');
					result = true;
				}
			}
			
		}//Number Validation 
		//nlapiLogExecution('DEBUG', 'check_no_of_days',' No Of Days Number / result -->' + result);		
		return result;
	}// Check if a number is a numeric or it contains text
	
	function get_subsidiary_text(i_subsidiary)
	{
	  var s_subsidiary = '';	
	 if(_logValidation(i_subsidiary))
	 {
		 var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary',i_subsidiary);
		
		if(_logValidation(o_subsidiaryOBJ))
		{
			s_subsidiary = o_subsidiaryOBJ.getFieldValue('name');
			//nlapiLogExecution('DEBUG', 'get_subsidiary_text',' Subsidiary -->' + s_subsidiary);	
			
		}//Subsidiary OBJ
	 }//Subsidiary	
	 return s_subsidiary;
	}//Get Subsidiary
	
	function get_Month(i_month) //
	{
		if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
			return '01';
		}
		else 
			if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
				return '02';
			}
			else 
				if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
					return '03';
				}
				else 
					if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
						return '04';
					}
					else 
						if (i_month == 'May' || i_month == 'MAY') {
							return '05';
						}
						else 
							if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
								return '06';
							}
							else 
								if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
									return '07';
								}
								else 
									if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
										return '08';
									}
									else 
										if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
											return '09';
										}
										else 
											if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
												return '10';
											}
											else 
												if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
													return '11';
												}
												else 
													if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
														return '12';
													}
	}
	
	function check_month_year_format(i_month_year)
	{
		var is_month = false;
		var s_error_1 = '';
		var s_error_2 = '';
		var s_error = '';
		if(_logValidation(i_month_year))
		{
		  var a_split_m_y_array = new Array();
		  
		  if(i_month_year.indexOf('-')>-1)
		  {
			  a_split_m_y_array = i_month_year.split('-');
			  
			  var i_month = a_split_m_y_array[0];
			  
			  var i_year = a_split_m_y_array[1];
			  
			  var i_year_format = check_for_numeric_value(i_year);
			  //nlapiLogExecution('DEBUG', 'check_month_year_format',' Year Format -->' + i_year_format);			
				
			  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
			  {
				  is_month = true ;
			  }	
			  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
			  {
				  is_month = true ;		
			  }		
			  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
			  {
				  is_month = true ;		
			  }	
			  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
			  {
				  is_month = true ;		
			  }	
			  else if(i_month == 'May' || i_month == 'MAY')
			  {
				  is_month = true ;		
			  }	  
			  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
			  {
				 is_month = true ;		
			  }	
			  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
			  {
				  is_month = true ;		
			  }	
			  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
			  {
				  is_month = true ;		
			  }  
			  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
			  {
				  is_month = true ;		
			  }	
			  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
			  {
				 is_month = true ;		
			  }	
			  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
			  {
				  is_month = true ;		
			  }	
			  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
			  {
				  is_month = true ;		
			  }	
						
				if(is_month == false)
				{
					s_error_1 = 'Invalid Year : Year is not in number format . \n' 			
				}
				if(i_year_format == true)
				{
					s_error_2 = ' Invalid Month : Month is not in MM format .\n'
				}			
				s_error = s_error_1+''+s_error_2;		  	
		  }// Contains -
		  else
		  {	  	 
		   s_error+=' Invalid Date Format : Date is not in MM-YYYY Format . \n';
		   //nlapiLogExecution('DEBUG', 'check_month_year_format',' Error -->' + s_error);	
		  }		
		}//Month & Year	
		return s_error;
	}//Month Year Format
	
	
	function check_currency(i_currency) //
	{
		var i_currencyID = '';
		if(_logValidation(i_currency)) //
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'is',i_currency)
			
			var column= new Array();	
			column[0]= new nlobjSearchColumn('internalid')
		 
			var a_results = nlapiSearchRecord('currency',null,filters,column);
			
			if(_logValidation(a_results)) //
			{		
			i_currencyID = a_results[0].getValue('internalid');
			//nlapiLogExecution('DEBUG', 'check_currency', ' Currency ID -->' + i_currencyID);	
				
			}//		
		}//Currency V	
		return i_currencyID;
	}//Currency
	
	
	function monthly_employee_file_process_data(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff) //
	{
		var i_mnt = 0;
		var i_ent = 0;
		var i_file_status = 'SUCCESS';
		var a_currency_array = new Array();
		
		if (_logValidation(i_CSV_File_ID)) //
		{
			//==================== Read CSV File Contents =========================
			
			var o_fileOBJ = nlapiLoadFile(i_CSV_File_ID);
			
			var s_file_contents = o_fileOBJ.getValue();
			//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', 'Type -->' + o_fileOBJ.getType());
			
			if (o_fileOBJ.getType() == 'EXCEL') //
			{
				//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', '@@@@@@@@@@@  Excel File @@@@@@@@@@@ ');
				var filedecodedrows = decode_base64(s_file_contents);
			}
			else //
			{
				//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', '@@@@@@@@@@@  Other Than Excel File @@@@@@@@@@@ ');
				var filedecodedrows = s_file_contents;
			}
			
			var a_file_array = filedecodedrows.split(/\n/g);
			//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' No. Of File Rows -->' + a_file_array.length);
			
			var i_data_length = parseInt(a_file_array.length) - parseInt(1);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Data Length -->' + i_data_length);
			
			//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' a_file_array -->' + a_file_array);
			
			if (i_counter == 0) //
			{
				var i_cnt = parseInt(i_counter) + 1
			}
			else //
			{
				var i_cnt = parseInt(i_counter) + 1
			}
			
			for (var i = i_cnt; i < i_data_length; i++) //
			{
				nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ******** Line No . ****** -->' + i);
				
				var s_error = '';
				var s_error_1 = '';
				var s_error_2 = '';
				var s_error_3 = '';
				var s_error_4 = '';
				var s_error_5 = '';
				var s_error_6 = '';
				var s_error_7 = '';
				var s_error_8 = '';
				var s_error_9 = '';
				
				var s_error_a = '';
				var s_error_b = '';
				var s_error_c = '';
				var s_error_d = '';
				var s_error_e = '';
				var s_error_f = '';
				var s_error_g = '';
				var s_error_h = '';
				var s_error_i = '';
				var s_error_j = '';
				var s_error_allocate = '';
				var s_error_Currency = '';
				
				var s_error_a_fld = '';
				var s_error_b_fld = '';
				var s_error_c_fld = '';
				var s_error_d_fld = '';
				var s_error_e_fld = '';
				var s_error_f_fld = '';
				var s_error_g_fld = '';
				var s_error_h_fld = '';
				var s_error_i_fld = '';
				var s_error_j_fld = '';
				//var s_error_Currency_fld = '';
				
				var s_error_fld = '';
				var s_error_1_fld = '';
				var s_error_2_fld = '';
				var s_error_3_fld = '';
				var s_error_4_fld = '';
				var s_error_5_fld = '';
				var s_error_6_fld = '';
				var s_error_7_fld = '';
				var s_error_8_fld = '';
				var s_error_9_fld = '';
				var s_error_allocate_fld = '';
				var s_error_Currency_fld = '';
				
				var s_error_File_Type = '';
				var s_error_File_type_fld = '';
				
				
				a_file_array[i] = parseCSV(a_file_array[i]);
				
				var i_temp_data = a_file_array[i].toString()
				
				//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', 'Temp Data -->' + i_temp_data);
				
				var a_row_data = i_temp_data.split(',')
				
				var i_Number_OF_Columns = a_row_data.length;
				nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', 'i_Number_OF_Columns -->' + i_Number_OF_Columns);
				
				if (i_Salaried_OT_Diff == 'T') //
				{
					if (i_Number_OF_Columns != 7) //
					{
						s_error_File_Type = ' File Format : Uploaded file is not in expected format, Please verify the same .\n'
						s_error_File_type_fld = ' File Format'
					}
				}
				else //
				{
					if (i_Number_OF_Columns != 6) //
					{
						s_error_File_Type = ' File Format : Uploaded file is not in expected format, Please verify the same .\n'
						s_error_File_type_fld = ' File Format'
					}
				}
				
				if (s_error_File_type_fld != ' File Format') // Means there is no error
				{
					var i_month_year = a_row_data[0]
					
					var i_employeeID_CSV = a_row_data[1]
					
					var i_subsidiary = a_row_data[2]
					
					var i_location = a_row_data[3]
					
					var i_currency = a_row_data[4]
					
					var i_amount = '';
					var i_revised_date = '';
					
					if (i_Salaried_OT_Diff == 'T') //
					{
						i_revised_date = a_row_data[5];
						i_amount = a_row_data[6];
					}
					else //
					{
						i_amount = a_row_data[5];
					}
					
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee ID -->' + i_employeeID_CSV);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month & Year -->' + i_month_year);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary -->' + i_subsidiary);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Location -->' + i_location);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Currency -->' + i_currency);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Amount -->' + i_amount);
					
					var a_error_currency = '';
					var a_error_currency_fld = '';
					
					a_currency_array.push(i_currency);
					
					// Removing duplicates from Array
					a_currency_array = removearrayduplicate(a_currency_array);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Currency Array -->' + a_currency_array);
					
					if (_logValidation(a_currency_array)) //
					{
						//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Currency Array Length-->' + a_currency_array.length);
						
						if (a_currency_array.length > 1) //
						{
							a_error_currency = ' Mutiple Currencies : Please select unique currency in a CSV file .\n'
							a_error_currency_fld = ' CSV Currency'
						}
					}
					
					var a_split_revised_date_array = new Array();
					
					var i_month;
					var i_year;
					
					var a_split_m_y_array = new Array();
					var s_Month_Year_Parameter = '';
					
					if (_logValidation(i_month_year)) //
					{
						if (i_month_year.indexOf('-') > -1) {
							a_split_m_y_array = i_month_year.split('-');
							
							i_month = a_split_m_y_array[0];
							
							i_year = a_split_m_y_array[1];
							
							s_Month_Year_Parameter = 20 + i_year + '-' + get_Month(i_month);
							nlapiLogExecution('DEBUG', 'Date Format', 's_Month_Year_Parameter ; ' + s_Month_Year_Parameter);
						}//Index Of 		  	
					}//Month & Year
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month -->' + i_month);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Year -->' + i_year);
					
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' i_employeeID_CSV -->' + i_employeeID_CSV);
					
					// Checking Currency
					var i_currency_c = check_currency(i_currency);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Currency -->' + i_currency_c);
					
					// Searching for Employee
					var a_employee_details = search_employee(i_employeeID_CSV)
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee Details -->' + a_employee_details);
					
					var a_emp_split_array = new Array();
					var i_practice;
					var i_subsidiary_e;
					var i_employeeID;
					var i_location_e;
					var i_location_txt_e;
					var i_currency_e;
					var i_subsidiary_txt_e;
					var i_employee_recID;
					var i_employee_type_e;
					
					if (_logValidation(a_employee_details)) //
					{
						a_emp_split_array = a_employee_details[0].split('&&##&&')
						
						i_employeeID = a_emp_split_array[0];
						
						i_subsidiary_e = a_emp_split_array[1];
						
						i_practice = a_emp_split_array[2];
						
						i_location_e = a_emp_split_array[3];
						
						i_location_txt_e = a_emp_split_array[4];
						
						i_subsidiary_txt_e = a_emp_split_array[5];
						
						i_employee_recID = a_emp_split_array[6];
						
						i_employee_type_e = a_emp_split_array[7];
						
					}	//Employee Details
					
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', '*** Employee Details ***');
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' i_practice -->' + i_practice);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' i_location_e -->' + i_location_e);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' i_location_txt_e -->' + i_location_txt_e);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' i_subsidiary_txt_e -->' + i_subsidiary_txt_e);				
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee ID E -->' + i_employeeID);				
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee record ID -->' + i_employee_recID);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Employee Type -->' + i_employee_type_e);
					
					// Check for numeric values
					var is_amount_format = check_for_numeric_value(i_amount);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Amount Format -->' + is_amount_format);
					
					
					var s_error_fld = '';
					var s_error_1_fld = '';
					var s_error_2_fld = '';
					var s_error_3_fld = '';
					var s_error_4_fld = '';
					var s_error_5_fld = '';
					var s_error_6_fld = '';
					var s_error_7_fld = '';
					var s_error_8_fld = '';
					var s_error_9_fld = '';
					var s_error_allocate_fld = '';
					var s_error_Currency = '';
					
					
					var d_start_date = get_current_month_start_date(i_month, i_year)
					var d_end_date = get_current_month_end_date(i_month, i_year)
					nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Start Date..... ' + d_start_date);
					
					nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....End Date..... ' + d_end_date); //employee_array_credit
					if (_logValidation(d_start_date)) //
					{
						d_start_date = nlapiStringToDate(d_start_date);
						d_start_date = nlapiDateToString(d_start_date);
					}
					if (_logValidation(d_end_date)) //
					{
						d_end_date = nlapiStringToDate(d_end_date);
						d_end_date = nlapiDateToString(d_end_date);
					}
					//======================================================================================
					
					// Check for employee over and under allocation
					// i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff
					if (i_Salaried_OT != 'T' && i_Salaried_OT_Diff != 'T') // Check on if it is a normal case, if it is an OT cases do not check for Resource allocation
					{
						nlapiLogExecution('DEBUG', 'check_emp_allocation', '*** Checking for resource validatoin ***');
						
						var check_emp_allocation = check_emp_over_or_under_allocated(i_employee_recID, d_start_date, d_end_date)
						nlapiLogExecution('DEBUG', 'check_emp_allocation', check_emp_allocation);
						//=======================================================================================				
						
						var a_allocate_array = get_allocation_details(i_employee_recID, d_start_date, d_end_date)
						//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Allocate Array..... ' + a_allocate_array);
						
						var i_percent_time_total = 0;
						
						if (_logValidation(a_allocate_array)) //
						{
							var a_split_allocate_array = new Array();
							
							for (var a = 0; a < a_allocate_array.length; a++) //
							{
								if (_logValidation(a_allocate_array[a])) //
								{
									a_split_allocate_array = a_allocate_array[a].split('^^&&^^');
									
									var i_percent_time = a_split_allocate_array[0];
									
									var i_project = a_split_allocate_array[1];
									
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time..... ' + i_percent_time);
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Project..... ' + i_project);
									
									i_percent_time = parseFloat(i_percent_time);	//100.0% 
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA..... ' + i_percent_time);
									
									i_percent_time_total = parseFloat(i_percent_time_total) + parseFloat(i_percent_time)
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA Total ..... ' + i_percent_time_total);
								
								}//a_allocate_array[a]
							}//A		
						}//a_allocate_array		
						
						nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA Total ..... ' + i_percent_time_total);
						
						if (i_percent_time_total >= 100) //
						{
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....100 % Allocated .... ' + i_percent_time_total);
						}
						else //
						{
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Not 100% Allocated ..... ' + i_percent_time_total);
							s_error_allocate = ' Employee not 100% Allocated  : Employee is not 100% allocated for the project.\n'
							s_error_allocate_fld = ' Employee 100 % Allocation'
							//     break;
						}
						//================================================================================================
						if (check_emp_allocation == 1) //
						{
							s_error_a = 'Employee Over Allocated  : Employee is Over allocated for the project.\n'
							s_error_a_fld = 'Employee Over Allocation'
						}
						else 
							if (check_emp_allocation == 2) //
							{
								s_error_a = 'Employee Under Allocated  : Employee is Under allocated for the project.\n'
								s_error_a_fld = 'Employee Under Allocation'
							}
						//===============================================================================================
					}
					// End of  Check for employee over and under allocation
					
					
					if (!_logValidation(i_employee_type_e)) //
					{
						s_error_h = ' Employee Type not present : Employee Type is not present on Employee.\n'
						s_error_h_fld = 'Employee Type'
					}
					else //
					{
						if (i_employee_type_e != '3') // 3 is internal ID of Salaried Employee type
						{
							// If employee is not Salaried means not 3 then show error.
							s_error_h = ' Employee Type is invalid : Employee Type should be "Salaried" on Employee.\n'
							s_error_h_fld = 'Employee Type'
						}
					}
					
					if (!_logValidation(i_currency_c)) //
					{
						s_error_Currency = ' Currency not present : Currency is not entered in CSV file.\n'
						s_error_Currency_fld = 'Currency'
					}
					
					if (_logValidation(i_employee_type_e)) //
					{
						if (i_monthly_employee == 'T' && i_employee_type_e != 3) //
						{
							s_error_i = ' Employee Type incorrect : Please check correct Employee Type on Salary Record.\n'
							s_error_i_fld = 'Employee Type'
						}
					}
					
					if (!_logValidation(i_subsidiary)) //
					{
						s_error_a = ' Subsidiary not present : Subsidiaty is not entered in CSV file.\n'
						s_error_a_fld = 'CSV Subsidiary'
					}
					
					if (!_logValidation(i_location)) //
					{
						s_error_b = ' Location not present : Location is not entered in CSV file.\n'
						s_error_b_fld = 'CSV Location'
					}
					
					if (!_logValidation(i_currency)) //
					{
						s_error_c = ' Currency not present : Currency is not entered in CSV file.\n'
						s_error_c_fld = 'CSV Currency'
					}
					
					if (!_logValidation(i_month_year)) //
					{
						s_error_d = ' Month & Year not present : Month & Year is not entered in CSV file.\n'
						s_error_d_fld = 'CSV Month & Year'
					}
					else //
					{
						// Search for the records created after monthly provision is done for given month
						var a_Filters = new Array();
						var a_Columns = new Array();
						
						a_Filters[0] = new nlobjSearchFilter('custrecord_month_of_date', null, 'is', s_Month_Year_Parameter);
						a_Filters[1] = new nlobjSearchFilter('custrecord_processed', null, 'is', 'T');
						a_Filters[2] = new nlobjSearchFilter('employeetype', 'custrecord_employee_id', 'is', '3'); // 3 means employee should be salaried
						a_Columns[0] = new nlobjSearchColumn('custrecord_month_of_date');
						
						var s_Search_Rec = searchRecord('customrecord_time_bill_rec', null, a_Filters, a_Columns);
						
						// if records are present then go ahead or
						
						if (_logValidation(s_Search_Rec)) //
						{
							if (s_Search_Rec.length <= 0) //
							{
								s_error_j = '"Monthly Provision" should be processed first for this month, to process "Salary Upload".\n'
								s_error_j_fld = 'CSV Month & Year'
								//alert('"Monthly Provision" should be created first for this month to process the "Salary Upload"');
							}
						}
						else //
						{
							s_error_j = ' "Monthly Provision" should be created first for this month to process the "Salary Upload".\n'
							s_error_j_fld = 'CSV Month & Year'
							//alert('"Monthly Provision" should be created first for this month to process the "Salary Upload"');
						}
						// if records are not present then show alert and stop the process			
					}
					
					if (!_logValidation(i_employeeID)) //
					{
						s_error_e = ' Employee not present : Employee is not entered in CSV file.\n'
						s_error_e_fld = 'CSV Employee'
					}
					
					if (!_logValidation(i_amount)) //
					{
						s_error_f = ' Amount not present : Amount is not entered in CSV file.\n'
						s_error_f_fld = 'CSV Amount'
					}
					
					if (_logValidation(i_subsidiary_txt_e) && _logValidation(i_subsidiary)) //
					{
						if (i_subsidiary_txt_e.toString() != i_subsidiary.toString()) //
						{
							//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Subsidiary Does not match ........');
							
							s_error_1 = ' Invalid Employee Subsidiary : Subsidiary does not match with subsidiary entered in file .\n'
							s_error_1_fld = 'Employee Subsidiary\n';
						}
					}
					
					if (is_amount_format == true) //
					{
						s_error_2 = ' Invalid Salary Amount : It should be number or numeric value .\n'
						s_error_2_fld = 'Cost\n';
					}
					
					if (!_logValidation(i_practice)) //
					{
						s_error_3 = ' Practice not present : Employee does not have Practice .\n'
						s_error_3_fld = 'Employee Practice'
					}
					
					var is_date_format = check_month_year_format(i_month_year)
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Month Year Format -->' + is_date_format);
					
					if (_logValidation(is_date_format)) //
					{
						s_error_4 = is_date_format;
						s_error_4_fld = 'Month & Year\n'
					}
					
					if (_logValidation(i_location_txt_e) && _logValidation(i_location)) //
					{
						if (i_location_txt_e.toString() != i_location.toString()) //
						{
							//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Location Does not match ........');
							
							s_error_5 = ' Invalid Employee Location : Location does not match with location entered in file .\n'
							s_error_5_fld = 'Employee Location\n'
						}
					}
					
					if (!_logValidation(i_location_e)) //
					{
						s_error_6 = ' Location not present  : Location is not present on Employee.\n'
						s_error_6_fld = 'Employee Location\n'
					}
					
					if (!_logValidation(i_location)) //
					{
						s_error_7 = ' Location not present  : Location is not entered in CSV File.\n'
						s_error_7_fld = 'Location\n'
					}
				}
				
				s_error = s_error_1 + '' + s_error_2 + '' + s_error_3 + '' + s_error_4 + '' + s_error_5 + '' + s_error_6 + '' + s_error_7 + '' + s_error_a + '' + s_error_b + '' + s_error_c + '' + s_error_d + '' + s_error_e + '' + s_error_f + '' + s_error_h + '' + s_error_i + '' + a_error_currency + '' + s_error_allocate + '' + s_error_j + '' + s_error_Currency + '' + s_error_File_Type
				//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Error -->' + s_error);
				
				s_error_fld = s_error_1_fld + '' + s_error_2_fld + '' + s_error_3_fld + '' + s_error_4_fld + '' + s_error_5_fld + '' + s_error_6_fld + '' + s_error_7_fld + '' + s_error_a_fld + '' + s_error_b_fld + '' + s_error_c_fld + '' + s_error_d_fld + '' + s_error_e_fld + '' + s_error_f_fld + '' + s_error_h_fld + '' + s_error_i_fld + '' + a_error_currency_fld + '' + s_error_allocate_fld + '' + s_error_j_fld + '' + s_error_Currency_fld + '' + s_error_File_type_fld
				//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Error Field -->' + s_error_fld);
				
				// ===================== Create Salary Upload - Monthly Records ===================
				
				if (!_logValidation(s_error)) //
				{
					try //
					{
						i_mnt++;
						nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* Monthly Employee ************************' + i_mnt);
						
						var o_salary_prOBJ = nlapiCreateRecord('customrecord_salary_upload_monthly_file');
						
						o_salary_prOBJ.setFieldValue('custrecord_month_m_s_p_u', i_month);
						o_salary_prOBJ.setFieldValue('custrecord_year_m_s_p_u', i_year);
						o_salary_prOBJ.setFieldValue('custrecord_csv_file_m_s_p_u', i_CSV_File_ID);
						o_salary_prOBJ.setFieldValue('custrecord_emp_type_m_s_p_u', 1);
						o_salary_prOBJ.setFieldValue('custrecord_emp_id_m_s_p_u', i_employeeID);
						o_salary_prOBJ.setFieldValue('custrecord_subsidiary_m_s_p_u', i_subsidiary);
						o_salary_prOBJ.setFieldValue('custrecord_location_m_s_p_u', i_location);
						o_salary_prOBJ.setFieldValue('custrecord_practice_m_s_p_u', i_practice);
						o_salary_prOBJ.setFieldValue('custrecord_currency_m_s_p_u', i_currency);
						o_salary_prOBJ.setFieldValue('custrecord_error_logs_m_s_p_u', s_error);
						o_salary_prOBJ.setFieldValue('custrecord_cost_m_s_p_u', i_amount);
						//	o_salary_prOBJ.setFieldValue('custrecord_revision_date_m_s_p_u',i_revised_date);
						o_salary_prOBJ.setFieldValue('custrecord_error_field_m_s_u', s_error_fld);
						o_salary_prOBJ.setFieldValue('custrecord_salary_upload_id_m', i_recordID);
						o_salary_prOBJ.setFieldValue('custrecord_employee_rec_id', i_employee_recID);
						o_salary_prOBJ.setFieldValue('custrecord_location_id_s', i_location_e);
						o_salary_prOBJ.setFieldValue('custrecord_subsidiary_id_s', i_subsidiary_e);
						o_salary_prOBJ.setFieldValue('custrecord_currency_id_m_sp_u', i_currency_c);
						
						
						var i_submitID = nlapiSubmitRecord(o_salary_prOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload Monthly Submit ID *************** -->' + i_submitID);
					} 
					catch (er) //
					{
						nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* 1 Monthly Error Block ************************' + i_mnt);
						
						i_ent++;
						var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');
						
						o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p', i_ent);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p', er);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p', er);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p', i_employeeID);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p', i_subsidiary);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p', i_month);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p', i_year);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent', i_recordID);
						
						//o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_error_logs',i_CSV_File_ID);	
						o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e', i_CSV_File_ID);
						
						var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
						
						if (_logValidation(i_submit_err_ID)) //
						{
							i_flag = 1;
							i_file_status = 'ERROR'
							//nlapiLogExecution('DEBUG', 'i_submit_err_ID i_file_status ' + i_file_status);
						}
					}
					
				}//Non - Error Records
				
				if (_logValidation(s_error)) //
				{
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* 2 Monthly Error Block ************************' + i_mnt);
					
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', 's_error -->' + s_error);
					
					i_ent++;
					var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');
					
					o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p', i_ent);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p', s_error_fld);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p', s_error);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p', i_employeeID);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p', i_subsidiary);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p', i_month);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p', i_year);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent', i_recordID);
					
					//o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_error_logs',i_CSV_File_ID);	
					o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e', i_CSV_File_ID);
					
					var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
					
					if (_logValidation(i_submit_err_ID)) //
					{
						//nlapiLogExecution('DEBUG', '_logValidation(s_error) i_file_status ' + i_file_status);
						i_file_status = 'ERROR'
						i_flag = 1;
					}
					
				}//Error Records
				if (i_flag == 1) //
				{
					i_file_status = 'ERROR'
				}
				
				var i_usage_end = i_context.getRemainingUsage();
				//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Usage At End [' + i + '] -->' + i_usage_end);
				
				if (i_usage_end < 500) //
				{
					i_file_status = 'In Progress';
					schedule_script_after_usage_exceeded(i, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag);
					break;
				}
			}
		}
		return i_file_status;
	}//monthly_employee_file_process_data
	
	
	function Salaried_Emp_OT_Data_Process(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff) //
	{
		var i_mnt = 0;
		var i_ent = 0;
		var i_file_status = 'SUCCESS';
		var a_currency_array = new Array();
		
		if (_logValidation(i_CSV_File_ID)) //
		{
			//==================== Read CSV File Contents =========================
			
			var o_fileOBJ = nlapiLoadFile(i_CSV_File_ID);
			
			var s_file_contents = o_fileOBJ.getValue();
			//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', 'Type -->' + o_fileOBJ.getType());
			
			if (o_fileOBJ.getType() == 'EXCEL') //
			{
				//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', '@@@@@@@@@@@  Excel File @@@@@@@@@@@ ');
				var filedecodedrows = decode_base64(s_file_contents);
			}
			else //
			{
				//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', '@@@@@@@@@@@  Other Than Excel File @@@@@@@@@@@ ');
				var filedecodedrows = s_file_contents;
			}
			
			var a_file_array = filedecodedrows.split(/\n/g);
			//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' No. Of File Rows -->' + a_file_array.length);
			
			var i_data_length = parseInt(a_file_array.length) - parseInt(1);
			//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Data Length -->' + i_data_length);
			
			if (i_counter == 0) //
			{
				var i_cnt = parseInt(i_counter) + 1
			}
			else //
			{
				var i_cnt = parseInt(i_counter) + 1
			}
			
			for (var i = i_cnt; i < i_data_length; i++) //
			{
				//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ****************** Line No . ***************** -->' + i);
				
				var s_error = '';
				var s_error_1 = '';
				var s_error_2 = '';
				var s_error_3 = '';
				var s_error_4 = '';
				var s_error_5 = '';
				var s_error_6 = '';
				var s_error_7 = '';
				var s_error_8 = '';
				var s_error_9 = '';
				
				var s_error_a = '';
				var s_error_b = '';
				var s_error_c = '';
				var s_error_d = '';
				var s_error_e = '';
				var s_error_f = '';
				var s_error_g = '';
				var s_error_h = '';
				var s_error_i = '';
				var s_error_j = '';
				var s_error_allocate = '';
				var s_error_Currency = '';
				
				var s_error_a_fld = '';
				var s_error_b_fld = '';
				var s_error_c_fld = '';
				var s_error_d_fld = '';
				var s_error_e_fld = '';
				var s_error_f_fld = '';
				var s_error_g_fld = '';
				var s_error_h_fld = '';
				var s_error_i_fld = '';
				var s_error_j_fld = '';
				
				var s_error_fld = '';
				var s_error_1_fld = '';
				var s_error_2_fld = '';
				var s_error_3_fld = '';
				var s_error_4_fld = '';
				var s_error_5_fld = '';
				var s_error_6_fld = '';
				var s_error_7_fld = '';
				var s_error_8_fld = '';
				var s_error_9_fld = '';
				var s_error_allocate_fld = '';
				var s_error_Currency_fld = '';
				
				var s_error_File_Type = '';
				var s_error_File_Type_fld = '';
				
				
				a_file_array[i] = parseCSV(a_file_array[i]);
				
				var i_temp_data = a_file_array[i].toString()
				
				//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', 'Temp Data -->' + i_temp_data);
				
				var a_row_data = i_temp_data.split(',')
				
				var i_Number_OF_Columns = a_row_data.length;
				nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', 'i_Number_OF_Columns -->' + i_Number_OF_Columns);
				
				if (i_Number_OF_Columns != 7) //
				{
					s_error_File_Type = ' File Format : Uploaded file is not in expected file format, Please verify the same .\n'
					s_error_File_Type_fld = ' File Format'
				}
				else //
				{
					var i_month_year = a_row_data[0]
					
					var i_employeeID_CSV = a_row_data[1]
					
					var i_subsidiary = a_row_data[2]
					
					var i_location = a_row_data[3]
					
					var i_currency = a_row_data[4]
					
					var i_revised_date = a_row_data[5]
					
					var i_OT_Rate = a_row_data[6]
					
					//var i_Total_OT_Hours = a_row_data[7]
					
					/*//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Employee ID -->' + i_employeeID_CSV);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Month & Year -->' + i_month_year);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Subsidiary -->' + i_subsidiary);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Location -->' + i_location);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Currency -->' + i_currency);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_OT_Rate -->' + i_OT_Rate);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_Total_OT_Hours -->' + i_Total_OT_Hours);*/
					var a_error_currency = '';
					var a_error_currency_fld = '';
					
					a_currency_array.push(i_currency);
					
					// Removing duplicates from Array
					a_currency_array = removearrayduplicate(a_currency_array);
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Currency Array -->' + a_currency_array);
					
					if (_logValidation(a_currency_array)) //
					{
						//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Currency Array Length-->' + a_currency_array.length);
						
						if (a_currency_array.length > 1) //
						{
							a_error_currency = ' Mutiple Currencies : Please select unique currency in a CSV file .\n'
							a_error_currency_fld = ' CSV Currency'
						}
					}
					
					var a_split_revised_date_array = new Array();
					
					var i_month;
					var i_year;
					
					var a_split_m_y_array = new Array();
					var s_Month_Year_Parameter = '';
					
					if (_logValidation(i_month_year)) //
					{
						if (i_month_year.indexOf('-') > -1) {
							a_split_m_y_array = i_month_year.split('-');
							
							i_month = a_split_m_y_array[0];
							
							i_year = a_split_m_y_array[1];
							
							s_Month_Year_Parameter = 20 + i_year + '-' + get_Month(i_month);
							//nlapiLogExecution('DEBUG', 'Date Format', 's_Month_Year_Parameter ; ' + s_Month_Year_Parameter);
						}//Index Of 		  	
					}//Month & Year
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Month -->' + i_month);
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Year -->' + i_year);
					
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' i_employeeID_CSV -->' + i_employeeID_CSV);
					
					// Checking Currency
					var i_currency_c = check_currency(i_currency);
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Currency -->' + i_currency_c);
					
					// Searching for Employee
					var a_employee_details = search_employee(i_employeeID_CSV)
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Employee Details -->' + a_employee_details);
					
					var a_emp_split_array = new Array();
					var i_practice;
					var i_subsidiary_e;
					var i_employeeID;
					var i_location_e;
					var i_location_txt_e;
					var i_currency_e;
					var i_subsidiary_txt_e;
					var i_employee_recID;
					var i_employee_type_e;
					
					if (_logValidation(a_employee_details)) //
					{
						a_emp_split_array = a_employee_details[0].split('&&##&&')
						
						i_employeeID = a_emp_split_array[0];
						
						i_subsidiary_e = a_emp_split_array[1];
						
						i_practice = a_emp_split_array[2];
						
						i_location_e = a_emp_split_array[3];
						
						i_location_txt_e = a_emp_split_array[4];
						
						i_subsidiary_txt_e = a_emp_split_array[5];
						
						i_employee_recID = a_emp_split_array[6];
						
						i_employee_type_e = a_emp_split_array[7];
						
					}//Employee Details
					/*  //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_practice -->' + i_practice);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_location_e -->' + i_location_e);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_location_txt_e -->' + i_location_txt_e);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_subsidiary_txt_e -->' + i_subsidiary_txt_e);
					 
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Employee ID E -->' + i_employeeID);
					 
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Employee record ID -->' + i_employee_recID);
					 //nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Employee Type -->' + i_employee_type_e);	*/
					// Check for numeric values
					var is_amount_format = check_for_numeric_value(i_OT_Rate);
					nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' i_OT_Rate -->' + i_OT_Rate);
					nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Amount Format -->' + is_amount_format);
					
					//var is_Total_Hours_Check = check_for_numeric_value(i_Total_OT_Hours);
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Total Amount Format -->' + is_Total_Hours_Check);
					
					
					var s_error_fld = '';
					var s_error_1_fld = '';
					var s_error_2_fld = '';
					var s_error_3_fld = '';
					var s_error_4_fld = '';
					var s_error_5_fld = '';
					var s_error_6_fld = '';
					var s_error_7_fld = '';
					var s_error_8_fld = '';
					var s_error_9_fld = '';
					var s_error_allocate_fld = '';
					
					
					var d_start_date = get_current_month_start_date(i_month, i_year)
					var d_end_date = get_current_month_end_date(i_month, i_year)
					//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Start Date..... ' + d_start_date);
					
					//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....End Date..... ' + d_end_date); //employee_array_credit
					if (_logValidation(d_start_date)) //
					{
						d_start_date = nlapiStringToDate(d_start_date);
						d_start_date = nlapiDateToString(d_start_date);
					}
					if (_logValidation(d_end_date)) //
					{
						d_end_date = nlapiStringToDate(d_end_date);
						d_end_date = nlapiDateToString(d_end_date);
					}
					//======================================================================================
					
					// Check for employee over and under allocation
					// i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff
					if (i_Salaried_OT != 'T' && i_Salaried_OT_Diff != 'T') // Check on if it is a normal case, if it is an OT cases do not check for Resource allocation
					{
						//nlapiLogExecution('DEBUG', 'check_emp_allocation', '*** Checking for resource validatoin ***');
						
						var check_emp_allocation = check_emp_over_or_under_allocated(i_employee_recID, d_start_date, d_end_date)
						//nlapiLogExecution('DEBUG', 'check_emp_allocation', check_emp_allocation);
						//=======================================================================================				
						
						var a_allocate_array = get_allocation_details(i_employee_recID, d_start_date, d_end_date)
						//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Allocate Array..... ' + a_allocate_array);
						
						var i_percent_time_total = 0;
						
						if (_logValidation(a_allocate_array)) //
						{
							var a_split_allocate_array = new Array();
							
							for (var a = 0; a < a_allocate_array.length; a++) //
							{
								if (_logValidation(a_allocate_array[a])) //
								{
									a_split_allocate_array = a_allocate_array[a].split('^^&&^^');
									
									var i_percent_time = a_split_allocate_array[0];
									
									var i_project = a_split_allocate_array[1];
									
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time..... ' + i_percent_time);
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Project..... ' + i_project);
									
									i_percent_time = parseFloat(i_percent_time);//100.0% 
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA..... ' + i_percent_time);
									
									i_percent_time_total = parseFloat(i_percent_time_total) + parseFloat(i_percent_time)
									//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA Total ..... ' + i_percent_time_total);
								
								}//a_allocate_array[a]
							}//A		
						}//a_allocate_array				 	
						if (i_percent_time_total >= 100) //
						{
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....100 % Allocated .... ' + i_percent_time_total);
						}
						else //
						{
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Not 100% Allocated ..... ' + i_percent_time_total);
							s_error_allocate = ' Employee not 100% Allocated  : Employee is not 100% allocated for the project.\n'
							s_error_allocate_fld = ' Employee 100 % Allocation'
							//     break;
						}
						//================================================================================================
						if (check_emp_allocation == 1) //
						{
							s_error_a = 'Employee Over Allocated  : Employee is Over allocated for the project.\n'
							s_error_a_fld = 'Employee Over Allocation'
						}
						else 
							if (check_emp_allocation == 2) //
							{
								s_error_a = 'Employee Under Allocated  : Employee is Under allocated for the project.\n'
								s_error_a_fld = 'Employee Under Allocation'
							}
						//===============================================================================================
					}
					// End of  Check for employee over and under allocation
					
					
					if (!_logValidation(i_employee_type_e)) //
					{
						s_error_h = ' Employee Type not present : Employee Type is not present on Employee.\n'
						s_error_h_fld = 'Employee Type'
					}
					
					if (_logValidation(i_employee_type_e)) //
					{
						if (i_monthly_employee == 'T' && i_employee_type_e != 3) //
						{
							s_error_i = ' Employee Type incorrect : Please check correct Employee Type on Salary Record.\n'
							s_error_i_fld = 'Employee Type'
						}
					}
					
					
					if (!_logValidation(i_currency_c)) //
					{
						s_error_Currency = ' Currency not present : Currency is not entered in CSV file.\n'
						s_error_Currency_fld = 'Currency'
					}
					
					if (!_logValidation(i_subsidiary)) //
					{
						s_error_a = ' Subsidiary not present : Subsidiaty is not entered in CSV file.\n'
						s_error_a_fld = 'CSV Subsidiary'
					}
					
					if (!_logValidation(i_location)) //
					{
						s_error_b = ' Location not present : Location is not entered in CSV file.\n'
						s_error_b_fld = 'CSV Location'
					}
					
					if (!_logValidation(i_currency)) //
					{
						s_error_c = ' Currency not present : Currency is not entered in CSV file.\n'
						s_error_c_fld = 'CSV Currency'
					}
					
					if (!_logValidation(i_month_year)) //
					{
						s_error_d = ' Month & Year not present : Month & Year is not entered in CSV file.\n'
						s_error_d_fld = 'CSV Month & Year'
					}
					else //
					{
						// Search for the records created after monthly provision is done for given month
						var a_Filters = new Array();
						var a_Columns = new Array();
						
						a_Filters[0] = new nlobjSearchFilter('custrecord_month_of_date', null, 'is', s_Month_Year_Parameter);
						a_Filters[1] = new nlobjSearchFilter('custrecord_processed', null, 'is', 'T');
						a_Filters[2] = new nlobjSearchFilter('employeetype', 'custrecord_employee_id', 'is', '3'); // 3 means employee should be salaried
						a_Columns[0] = new nlobjSearchColumn('custrecord_month_of_date');
						
						var s_Search_Rec = searchRecord('customrecord_time_bill_rec', null, a_Filters, a_Columns);
						
						// if records are present then go ahead or
						
						if (_logValidation(s_Search_Rec)) //
						{
							nlapiLogExecution('DEBUG', 'MP_validate', 's_Search_Rec : ' + s_Search_Rec.length);
							if (s_Search_Rec.length <= 0) //
							{
								s_error_j = ' "Monthly Provision" should be processed first for this month, to process "Salary Upload".\n'
								s_error_j_fld = 'CSV Month & Year'
								//alert('"Monthly Provision" should be created first for this month to process the "Salary Upload"');
							}
						}
						else //
						{
							nlapiLogExecution('DEBUG', 'MP_validate', 's_Search_Rec : ' + s_Search_Rec);
							s_error_j = ' "Monthly Provision" should be created first for this month to process the "Salary Upload".\n'
							s_error_j_fld = 'CSV Month & Year'
							//alert('"Monthly Provision" should be created first for this month to process the "Salary Upload"');
						}
						// if records are not present then show alert and stop the process			
					}
					
					if (!_logValidation(i_employeeID)) //
					{
						s_error_e = ' Employee not present : Employee is not entered in CSV file.\n'
						s_error_e_fld = 'CSV Employee'
					}
					
					if (!_logValidation(i_OT_Rate)) //
					{
						s_error_f = ' Amount not present : Amount is not entered in CSV file.\n'
						s_error_f_fld = 'CSV Amount'
					}
					
					//if (!_logValidation(i_Total_OT_Hours)) //
					{
						//s_error_f = ' "Total OT Hours" not present : "Total OT Hours" is not entered in CSV file.\n'
						//s_error_f_fld = 'CSV Amount'
					}
					
					if (_logValidation(i_subsidiary_txt_e) && _logValidation(i_subsidiary)) //
					{
						if (i_subsidiary_txt_e.toString() != i_subsidiary.toString()) //
						{
							//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Subsidiary Does not match ........');
							
							s_error_1 = ' Invalid Employee Subsidiary : Subsidiary does not match with subsidiary entered in file .\n'
							s_error_1_fld = 'Employee Subsidiary\n';
						}
					}
					
					if (is_amount_format == true) //
					{
						s_error_2 = ' Invalid Salary Amount : It should be number or numeric value .\n'
						s_error_2_fld = 'Cost\n';
					}
					
					if (!_logValidation(i_practice)) //
					{
						s_error_3 = ' Practice not present : Employee does not have Practice .\n'
						s_error_3_fld = 'Employee Practice'
					}
					
					var is_date_format = check_month_year_format(i_month_year)
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Month Year Format -->' + is_date_format);
					
					if (_logValidation(is_date_format)) //
					{
						s_error_4 = is_date_format;
						s_error_4_fld = 'Month & Year\n'
					}
					
					if (_logValidation(i_location_txt_e) && _logValidation(i_location)) //
					{
						if (i_location_txt_e.toString() != i_location.toString()) //
						{
							//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Location Does not match ........');
							
							s_error_5 = ' Invalid Employee Location : Location does not match with location entered in file .\n'
							s_error_5_fld = 'Employee Location\n'
						}
					}
					
					if (!_logValidation(i_location_e)) //
					{
						s_error_6 = ' Location not present  : Location is not present on Employee.\n'
						s_error_6_fld = 'Employee Location\n'
					}
					
					if (!_logValidation(i_location)) //
					{
						s_error_7 = ' Location not present  : Location is not entered in CSV File.\n'
						s_error_7_fld = 'Location\n'
					}
				}
				
				s_error = s_error_1 + '' + s_error_2 + '' + s_error_3 + '' + s_error_4 + '' + s_error_5 + '' + s_error_6 + '' + s_error_7 + '' + s_error_a + '' + s_error_b + '' + s_error_c + '' + s_error_d + '' + s_error_e + '' + s_error_f + '' + s_error_h + '' + s_error_i + '' + a_error_currency + '' + s_error_allocate + '' + s_error_j + '' + s_error_Currency + '' + s_error_File_Type
				//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Error -->' + s_error);
				
				s_error_fld = s_error_1_fld + '' + s_error_2_fld + '' + s_error_3_fld + '' + s_error_4_fld + '' + s_error_5_fld + '' + s_error_6_fld + '' + s_error_7_fld + '' + s_error_a_fld + '' + s_error_b_fld + '' + s_error_c_fld + '' + s_error_d_fld + '' + s_error_e_fld + '' + s_error_f_fld + '' + s_error_h_fld + '' + s_error_i_fld + '' + a_error_currency_fld + '' + s_error_allocate_fld + '' + s_error_j_fld + '' + s_error_Currency_fld + '' + s_error_File_Type_fld
				//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' Error Field -->' + s_error_fld);
				
				// ===================== Create Salary Upload - Monthly Records ===================
				
				if (!_logValidation(s_error)) //
				{
					try //
					{
						i_mnt++;
						//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ************************* Monthly Employee ************************' + i_mnt);
						
						var o_salary_prOBJ = nlapiCreateRecord('customrecord_salary_upload_monthly_file');
						
						o_salary_prOBJ.setFieldValue('custrecord_month_m_s_p_u', i_month);
						o_salary_prOBJ.setFieldValue('custrecord_year_m_s_p_u', i_year);
						o_salary_prOBJ.setFieldValue('custrecord_csv_file_m_s_p_u', i_CSV_File_ID);
						o_salary_prOBJ.setFieldValue('custrecord_emp_type_m_s_p_u', 1);
						o_salary_prOBJ.setFieldValue('custrecord_emp_id_m_s_p_u', i_employeeID);
						o_salary_prOBJ.setFieldValue('custrecord_subsidiary_m_s_p_u', i_subsidiary);
						o_salary_prOBJ.setFieldValue('custrecord_location_m_s_p_u', i_location);
						o_salary_prOBJ.setFieldValue('custrecord_practice_m_s_p_u', i_practice);
						o_salary_prOBJ.setFieldValue('custrecord_currency_m_s_p_u', i_currency);
						o_salary_prOBJ.setFieldValue('custrecord_error_logs_m_s_p_u', s_error);
						o_salary_prOBJ.setFieldValue('custrecord_cost_m_s_p_u', i_OT_Rate);
						//o_salary_prOBJ.setFieldValue('custrecord_total_ot_hours', i_Total_OT_Hours); // added by Vikrant
						o_salary_prOBJ.setFieldValue('custrecord_revision_date_m_s_p_u', i_revised_date);
						o_salary_prOBJ.setFieldValue('custrecord_error_field_m_s_u', s_error_fld);
						o_salary_prOBJ.setFieldValue('custrecord_salary_upload_id_m', i_recordID);
						o_salary_prOBJ.setFieldValue('custrecord_employee_rec_id', i_employee_recID);
						o_salary_prOBJ.setFieldValue('custrecord_location_id_s', i_location_e);
						o_salary_prOBJ.setFieldValue('custrecord_subsidiary_id_s', i_subsidiary_e);
						o_salary_prOBJ.setFieldValue('custrecord_currency_id_m_sp_u', i_currency_c);
						
						
						var i_submitID = nlapiSubmitRecord(o_salary_prOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ***************** Salary Upload Monthly Submit ID *************** -->' + i_submitID);
					} 
					catch (er) //
					{
						//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ************************* Monthly Error Block ************************' + i_mnt);
						
						i_ent++;
						var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');
						
						o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p', i_ent);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p', er);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p', er);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p', i_employeeID);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p', i_subsidiary);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p', i_month);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p', i_year);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent', i_recordID);
						
						//o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_error_logs',i_CSV_File_ID);	
						o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e', i_CSV_File_ID);
						
						var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
						
						if (_logValidation(i_submit_err_ID)) //
						{
							i_flag = 1;
							i_file_status = 'ERROR'
							//nlapiLogExecution('DEBUG', 'i_submit_err_ID i_file_status ' + i_file_status);
						}
					}
					
				}//Non - Error Records
				if (_logValidation(s_error)) //
				{
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ************************* Monthly Error Block ************************' + i_mnt);
					
					i_ent++;
					var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');
					
					o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p', i_ent);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p', s_error_fld);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p', s_error);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p', i_employeeID);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p', i_subsidiary);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p', i_month);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p', i_year);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent', i_recordID);
					
					//o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_error_logs',i_CSV_File_ID);	
					o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e', i_CSV_File_ID);
					
					var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
					//nlapiLogExecution('DEBUG', 'Salaried_Emp_OT_Data_Process', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
					
					if (_logValidation(i_submit_err_ID)) //
					{
						//nlapiLogExecution('DEBUG', '_logValidation(s_error) i_file_status ' + i_file_status);
						i_file_status = 'ERROR'
						i_flag = 1;
					}
					
				}//Error Records
				if (i_flag == 1) //
				{
					i_file_status = 'ERROR'
				}
				
				var i_usage_end = i_context.getRemainingUsage();
				//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Usage At End [' + i + '] -->' + i_usage_end);
				
				if (i_usage_end < 500) //
				{
					i_file_status = 'In Progress';
					schedule_script_after_usage_exceeded(i, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_flag);
					break;
				}
			}
		}
		return i_file_status;
	}//Salaried_Emp_OT_Data_Process
	
	
	
	//function hourly_employee_file_process_data(i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee) //
	function hourly_employee_file_process_data (i_context, i_counter, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee, i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff,i_hourly_FP_Internal, i_hourly_FP_Internal_Diff)
	{
		var i_hnt = 0;
		var i_file_status = '';
		var i_ent = 0;
		var i_file_status = 'SUCCESS';
		var a_currency_array = new Array()
		nlapiLogExecution('audit', 'hourly_employee_file_process_data', '*** Into hourly_employee_file_process_data ***');
		
		if (_logValidation(i_CSV_File_ID)) //
		{
			//==================== Read CSV File Contents =========================
			
			var o_fileOBJ = nlapiLoadFile(i_CSV_File_ID);
			
			var s_file_contents = o_fileOBJ.getValue();
			//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', 'Type -->' + o_fileOBJ.getType());
			
			if (o_fileOBJ.getType() == 'EXCEL') //
			{
				//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', '@@@@@@@@@@@  Excel File @@@@@@@@@@@ ');
				var filedecodedrows = decode_base64(s_file_contents);
			}
			else //
			{
				//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', '@@@@@@@@@@@  Other Than Excel File @@@@@@@@@@@ ');
				var filedecodedrows = s_file_contents;
			}
			//nlapiLogExecution('audit', 'hourly_employee_file_process_data',filedecodedrows);
			var a_file_array = filedecodedrows.split(/\n/g);
			//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' No. Of File Rows -->' + a_file_array.length);
			
			var i_data_length = parseInt(a_file_array.length) - parseInt(1);
			nlapiLogExecution('audit', 'hourly_employee_file_process_data', ' Data Length -->' + i_data_length);
			
			
			if (i_counter == 0) //
			{
				var i_cnt = parseInt(i_counter) + 1
			}
			else //
			{
				var i_cnt = parseInt(i_counter)
			}
			
			for (var i = i_cnt; i < i_data_length; i++) //
			{
				//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ****************** Line No . ***************** -->' + i);
				
				var s_error = '';
				var s_error_1 = '';
				var s_error_2 = '';
				var s_error_3 = '';
				var s_error_4 = '';
				var s_error_5 = '';
				var s_error_6 = '';
				var s_error_7 = '';
				var s_error_8 = '';
				var s_error_9 = '';
				var s_error_10 = '';
				
				var s_error_fld = '';
				var s_error_1_fld = '';
				var s_error_2_fld = '';
				var s_error_3_fld = '';
				var s_error_4_fld = '';
				var s_error_5_fld = '';
				var s_error_6_fld = '';
				var s_error_7_fld = '';
				var s_error_8_fld = '';
				var s_error_9_fld = '';
				var s_error_10_fld = '';
				var s_error_Currency_fld = '';
				var s_error_File_Type_fld = '';
				
				var s_error_a = '';
				var s_error_b = '';
				var s_error_c = '';
				var s_error_d = '';
				var s_error_e = '';
				var s_error_f = '';
				var s_error_g = '';
				var s_error_h = '';
				var s_error_i = '';
				var s_error_j = '';
				var s_error_Currency = '';
				var s_error_File_Type = '';
				
				var s_error_a_fld = '';
				var s_error_b_fld = '';
				var s_error_c_fld = '';
				var s_error_d_fld = '';
				var s_error_e_fld = '';
				var s_error_f_fld = '';
				var s_error_g_fld = '';
				var s_error_h_fld = '';
				var s_error_i_fld = '';
				var s_error_j_fld = '';
				var s_error_k_fld = '';
				
				
				a_file_array[i] = parseCSV(a_file_array[i]);
				
				var i_temp_data = a_file_array[i].toString()
				
				//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', 'Temp Data -->' + i_temp_data);
				
				var a_row_data = i_temp_data.split(',');
				
				var i_Number_OF_Columns = a_row_data.length;
				//nlapiLogExecution('audit', 'hourly_employee_file_process_data', 'Temp Data -->' + i_temp_data);
				//nlapiLogExecution('audit', 'i_Number_OF_Columns -->' + i_Number_OF_Columns,'counter :- '+i);
				
				if (i_Number_OF_Columns != 8) //
				{
					s_error_File_Type = ' File Format : Uploaded file is not in correct format, Please check the file format.\n'
					s_error_File_Type_fld = ' File Format'
				}
				else //
				{
					var i_month_year = a_row_data[0]
					
					var i_subsidiary = a_row_data[1]
					
					var i_employeeID_CSV = a_row_data[2]
					
					var i_location = a_row_data[3]
					
					var i_currency = a_row_data[4]
					
					var i_revised_date = a_row_data[5]
					
					var i_ST_Rate_per_hour = a_row_data[6]
					
					var i_OT_Rate_per_hour = a_row_data[7]
					
					nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee ID -->' + i_employeeID);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Month & Year -->' + i_month_year);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary -->' + i_subsidiary);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location -->' + i_location);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Currency -->' + i_currency);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Revised Date -->' + i_revised_date);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ST Rate Per Hour -->' + i_ST_Rate_per_hour);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' OT Rate Per Hour -->' + i_OT_Rate_per_hour);
					
					
					var a_error_currency = '';
					var a_error_currency_fld = '';
					
					a_currency_array.push(i_currency);
					
					a_currency_array = removearrayduplicate(a_currency_array)
					nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Currency Array -->' + a_currency_array);
					
					if (_logValidation(a_currency_array)) //
					{
						//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Currency Array Length-->' + a_currency_array.length);
						
						if (a_currency_array.length > 1) //
						{
							a_error_currency = ' Mutiple Currencies : Please select unique currency in a CSV file .\n'
							a_error_currency_fld = ' CSV Currency'
						}
					}
					
					var i_month;
					var i_year;
					
					var a_split_m_y_array = new Array();
					var s_Month_Year_Parameter = '';
					
					if (_logValidation(i_month_year)) //
					{
						if (i_month_year.indexOf('-') > -1) //
						{
							a_split_m_y_array = i_month_year.split('-');
							
							i_month = a_split_m_y_array[0];
							
							i_year = a_split_m_y_array[1];
							
							s_Month_Year_Parameter = 20 + i_year + '-' + get_Month(i_month);
							nlapiLogExecution('DEBUG', 'Date Format', 's_Month_Year_Parameter ; ' + s_Month_Year_Parameter);
						}//Index Of 		  	
					}//Month & Year
					if (_logValidation(i_revised_date)) //
					{
					
						//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Month -->' + i_month);
						//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Year -->' + i_year);
						
						i_revised_date = nlapiStringToDate(i_revised_date);
						i_revised_date = nlapiDateToString(i_revised_date);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Revised Date -->' + i_revised_date);
					
					}
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' i_employeeID_CSV -->' + i_employeeID_CSV);
					
					
					var i_currency_c = check_currency(i_currency);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Currency -->' + i_currency_c);
					
					var a_employee_details = search_employee(i_employeeID_CSV)
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee Details -->' + a_employee_details);
					
					var a_emp_split_array = new Array();
					var i_practice;
					var i_subsidiary_e;
					var i_employeeID;
					var i_location_e;
					var i_location_txt_e;
					var i_currency_e;
					var i_subsidiary_txt_e;
					var i_employee_recID;
					var i_employee_type_e;
					
					if (_logValidation(a_employee_details)) //
					{
						a_emp_split_array = a_employee_details[0].split('&&##&&')
						
						i_employeeID = a_emp_split_array[0];
						
						i_subsidiary_e = a_emp_split_array[1];
						
						i_practice = a_emp_split_array[2];
						
						i_location_e = a_emp_split_array[3];
						
						i_location_txt_e = a_emp_split_array[4];
						
						i_subsidiary_txt_e = a_emp_split_array[5];
						
						i_employee_recID = a_emp_split_array[6];
						
						i_employee_type_e = a_emp_split_array[7];
						
					} //Employee Details
					
					/*
					 //nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee ID E -->' + i_employeeID);
					 //nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Practice E-->' + i_practice);
					 //nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary E-->' + i_subsidiary_e);
					 //nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location E -->' + i_location_e);
					 //nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location Txt E -->' + i_location_txt_e);
					 //nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary Txt E -->' + i_subsidiary_txt_e);
					 */
					 
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee rec ID -->' + i_employee_recID);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Employee Type -->' + i_employee_type_e);
					
					//	var s_subsidiary_txt = get_subsidiary_text(i_subsidiary_e);
					//	//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary Text -->' + s_subsidiary_txt);			
					
					var is_ST_Rate_per_hour_format = check_for_numeric_value(i_ST_Rate_per_hour);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ST Rate Per Hour Format -->' + is_ST_Rate_per_hour_format);
					
					var is_OT_Rate_per_hour_format = check_for_numeric_value(i_OT_Rate_per_hour);
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' OT Rate Per Hour Format -->' + is_OT_Rate_per_hour_format);
					
					var s_error_fld = '';
					var s_error_1_fld = '';
					var s_error_2_fld = '';
					var s_error_3_fld = '';
					var s_error_4_fld = '';
					var s_error_5_fld = '';
					var s_error_6_fld = '';
					var s_error_7_fld = '';
					var s_error_8_fld = '';
					var s_error_9_fld = '';
					var s_error_10_fld = '';
					
					if (!_logValidation(i_employee_type_e)) //
					{
						s_error_h = ' Employee Type not present : Employee Type is not present on Employee.\n'
						s_error_h_fld = 'Employee Type'
					}
					if (_logValidation(i_employee_type_e)) //
					{
						if (i_hourly_employee == 'T' && i_employee_type_e != 1) //
						{
							s_error_i = ' Employee Type incorrect : Please check correct Employee Type on Salary Record.\n'
							s_error_i_fld = 'Employee Type'
							
						}
						
					}
					
					if (!_logValidation(i_subsidiary)) //
					{
						s_error_a = ' Subsidiary not present : Subsidiaty is not entered in CSV file.\n'
						s_error_a_fld = 'CSV Subsidiary'
					}
					
					if (!_logValidation(i_currency_c)) //
					{
						s_error_Currency = ' Currency not present : Currency is not entered in CSV file.\n'
						s_error_Currency_fld = 'Currency'
					}
					
					
					if (!_logValidation(i_location)) //
					{
						s_error_b = ' Location not present : Location is not entered in CSV file.\n'
						s_error_b_fld = 'CSV Location'
					}
					if (!_logValidation(i_currency)) //
					{
						s_error_c = ' Currency not present : Currency is not entered in CSV file.\n'
						s_error_c_fld = 'CSV Currency'
					}
					if (!_logValidation(i_month_year)) //
					{
						s_error_d = ' Month & Year not present : Month & Year is not entered in CSV file.\n'
						s_error_d_fld = 'CSV Month & Year'
					}
					else //
					{
						//if (i_hourly_employee == 'T') //
						{
							// Search for the records created after monthly provision is done for given month
							var a_Filters = new Array();
							var a_Columns = new Array();
							
							a_Filters[0] = new nlobjSearchFilter('custrecord_month_of_date', null, 'is', s_Month_Year_Parameter);
							a_Filters[1] = new nlobjSearchFilter('custrecord_processed', null, 'is', 'T');
							a_Filters[2] = new nlobjSearchFilter('employeetype', 'custrecord_employee_id', 'is', '1'); // 1 means employee should be Hourly
							a_Columns[0] = new nlobjSearchColumn('custrecord_month_of_date');
							
							var s_Search_Rec = searchRecord('customrecord_time_bill_rec', null, a_Filters, a_Columns);
							
							// if records are present then go ahead or
							
							if (_logValidation(s_Search_Rec)) //
							{
								nlapiLogExecution('DEBUG', 'MP_validate', 's_Search_Rec : ' + s_Search_Rec.length);
								if (s_Search_Rec.length <= 0) //
								{
									s_error_j = ' "Monthly Provision" should be processed first for this month, to process "Salary Upload".\n'
									s_error_j_fld = 'CSV Month & Year'
								//alert('"Monthly Provision" should be created first for this month to process the "Salary Upload"');
								}
							}
							else //
							{
								nlapiLogExecution('DEBUG', 'MP_validate', 's_Search_Rec : ' + s_Search_Rec);
								
								s_error_j = ' "Monthly Provision" should be created first for this month to process the "Salary Upload".\n'
								s_error_j_fld = 'CSV Month & Year'
							//alert('"Monthly Provision" should be created first for this month to process the "Salary Upload"');
							}
						}
					// if records are not present then show alert and stop the process			
					}
					
					if (!_logValidation(i_employeeID)) //
					{
						s_error_e = ' Employee not present : Employee is not entered in CSV file.\n'
						s_error_e_fld = 'CSV Employee'
					}
					if (!_logValidation(i_ST_Rate_per_hour)) //
					{
						s_error_f = ' ST Rate(Per Hour) not present : ST Rate(Per Hour) is not entered in CSV file.\n'
						s_error_f_fld = 'CSV ST Rate(Per Hour)'
					}
					if (!_logValidation(i_OT_Rate_per_hour)) //
					{
						s_error_g = ' OT Rate(Per Hour) not present : OT Rate(Per Hour) is not entered in CSV file.\n'
						s_error_g_fld = 'CSV OT Rate(Per Hour)'
					}
					
					if (_logValidation(i_subsidiary_txt_e) && _logValidation(i_subsidiary)) //
					{
						if (i_subsidiary_txt_e.toString() != i_subsidiary.toString()) //
						{
							//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Subsidiary Does not match ........');
							
							s_error_1 = ' Invalid Employee Subsidiary : Subsidiary does not match with subsidiary entered in file .\n'
							s_error_1_fld = 'Employee Subsidiary\n';
							
						}
					}
					if (is_ST_Rate_per_hour_format == true) //
					{
						s_error_2 = ' Invalid ST Rate Amount : It should be number or numeric value .\n'
						s_error_2_fld = 'ST Rate (Per hour)\n';
					}
					if (is_OT_Rate_per_hour_format == true) //
					{
						s_error_3 = ' Invalid OT Rate Amount : It should be number or numeric value .\n'
						s_error_3_fld = 'OT Rate (Per hour)t\n';
					}
					if (!_logValidation(i_practice)) //
					{
						s_error_4 = ' Practice not present : Employee does not have Practice .\n'
						s_error_4_fld = 'Employee Practice'
					}
					var is_date_format = check_month_year_format(i_month_year)
					//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Month Year Format -->' + is_date_format);
					
					if (_logValidation(is_date_format)) //
					{
						s_error_5 = is_date_format;
						s_error_5_fld = 'Month & Year\n'
					}
					if (_logValidation(i_location_txt_e) && _logValidation(i_location)) //
					{
						if (i_location_txt_e.toString() != i_location.toString()) //
						{
							//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Location Does not match ........');
							
							s_error_6 = ' Invalid Employee Location : Location does not match with location entered in file .\n'
							s_error_6_fld = 'Employee Location\n'
						}
					}
					if (!_logValidation(i_location_e)) //
					{
						s_error_7 = ' Location not present  : Location is not present on Employee.\n'
						s_error_7_fld = 'Employee Location\n'
					}
					if (!_logValidation(i_location)) //
					{
						s_error_8 = ' Location not present  : Location is not entered in CSV File.\n'
						s_error_8_fld = 'Location\n'
					}
				}
				
				s_error = s_error_1 + '' + s_error_2 + '' + s_error_3 + '' + s_error_4 + '' + s_error_5 + '' + s_error_6 + '' + s_error_7 + '' + s_error_8 + '' + s_error_a + '' + s_error_b + '' + s_error_c + '' + s_error_d + '' + s_error_e + '' + s_error_f + '' + s_error_g + '' + s_error_h + '' + s_error_i + '' + a_error_currency+ '' + s_error_j+ '' + s_error_Currency + '' + s_error_File_Type
				//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Error -->' + s_error);
				
				s_error_fld = s_error_1_fld + '' + s_error_2_fld + '' + s_error_3_fld + '' + s_error_4_fld + '' + s_error_5_fld + '' + s_error_6_fld + '' + s_error_7_fld + '' + s_error_8_fld + '' + s_error_a_fld + '' + s_error_b_fld + '' + s_error_c_fld + '' + s_error_d_fld + '' + s_error_e_fld + '' + s_error_f_fld + '' + s_error_g_fld + '' + s_error_h_fld + '' + s_error_i_fld + '' + a_error_currency_fld+ '' + s_error_j_fld+ '' + s_error_Currency_fld + '' + s_error_File_Type_fld
				//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' Error Field -->' + s_error_fld);
				
				//	if(_logValidation(i_employeeID)) //
				{
					// ===================== Create Salary Upload - Hourly Records ===================
					
					if (!_logValidation(s_error)) //
					{
					
						try //
						{
							i_hnt++;
							//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ************************* Hourly Employee ************************' + i_hnt);
							
							var o_salary_prOBJ = nlapiCreateRecord('customrecord_salary_upload_hourly_file');
							
							o_salary_prOBJ.setFieldValue('custrecord_month_process_s_u', i_month);
							o_salary_prOBJ.setFieldValue('custrecord_year_process_s_u', i_year);
							o_salary_prOBJ.setFieldValue('custrecord_csv_file_process_s_u', i_CSV_File_ID);
							o_salary_prOBJ.setFieldValue('custrecord_employee_type_s_u', 2);
							o_salary_prOBJ.setFieldValue('custrecord_employee_id_p_s_u', i_employeeID);
							o_salary_prOBJ.setFieldValue('custrecord_subsidiary_p_s_u', i_subsidiary);
							o_salary_prOBJ.setFieldValue('custrecord_location_p_s_u', i_location);
							o_salary_prOBJ.setFieldValue('custrecord_practice_p_s_u', i_practice);
							o_salary_prOBJ.setFieldValue('custrecord_currency_p_s_u', i_currency);
							o_salary_prOBJ.setFieldValue('custrecord_error_logs', s_error);
							o_salary_prOBJ.setFieldValue('custrecord_revision_date', i_revised_date);
							o_salary_prOBJ.setFieldValue('custrecord_st_rate_per_hour', i_ST_Rate_per_hour);
							o_salary_prOBJ.setFieldValue('custrecord_ot_rate_per_hour', i_OT_Rate_per_hour);
							o_salary_prOBJ.setFieldValue('custrecord_error_field_p_s_u', s_error_fld);
							o_salary_prOBJ.setFieldValue('custrecord_salary_upload_id_h', i_recordID);
							o_salary_prOBJ.setFieldValue('custrecord_employee_id_rec', i_employee_recID);
							o_salary_prOBJ.setFieldValue('custrecord_location_id_h', i_location_e);
							o_salary_prOBJ.setFieldValue('custrecord_subsidiary_id_h', i_subsidiary_e);
							o_salary_prOBJ.setFieldValue('custrecord_currency_id_s_u', i_currency_c);
							
							
							
							var i_submitID = nlapiSubmitRecord(o_salary_prOBJ, true, true);
							//nlapiLogExecution('DEBUG', 'hourly_employee_file_process_data', ' ***************** Submit ID *************** -->' + i_submitID);
							
							
						} 
						catch (eyt) //
						{
							//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* Hourly Error Block ************************' + i_hnt);
							
							var i_recordID = i_context.getSetting('SCRIPT', 'custscript_record_id_s_u');
							var o_salary_uploadOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID);
							
							var s_notes = exception;
							o_salary_uploadOBJ.setFieldValue('custrecord_file_status', 7);
							o_salary_uploadOBJ.setFieldValue('custrecord_notes', s_notes);
							
							//o_salary_uploadOBJ.setFieldValue('custrecord_journal_entries_created','')
							
							var i_submitID = nlapiSubmitRecord(o_salary_uploadOBJ, true, true);
							//nlapiLogExecution('DEBUG', 'hourly_JE_creation', ' *********************** Submit ID ****************** -->' + i_submitID);
							
							
							
							i_ent++;
							var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');
							
							o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p', i_ent);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p', s_error_fld);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p', s_error);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p', i_employeeID);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p', i_subsidiary);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p', i_month);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p', i_year);
							o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent', i_recordID);
							
							o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e', i_CSV_File_ID);
							/*
							 o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e',i_CSV_File_ID);
							 
							 */
							var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
							//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
							
							if (_logValidation(i_submit_err_ID)) //
							{
								i_file_status = 'ERROR'
							}
							
							
						}
						
						
					}//Non - Error Records
					if (_logValidation(s_error)) //
					{
						//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* Hourly Error Block ************************' + i_hnt);
						
						i_ent++;
						var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_salary_process');
						
						o_salary_pr_errorOBJ.setFieldValue('custrecord_s_no_s_p', i_ent);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_error_field_s_p', s_error_fld);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details_s_p', s_error);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_s_p', i_employeeID);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_s_p', i_subsidiary);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_month_s_p', i_month);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_year_s_p', i_year);
						o_salary_pr_errorOBJ.setFieldValue('custrecord_saluploadparent', i_recordID);
						
						o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e', i_CSV_File_ID);
						/*
						 o_salary_pr_errorOBJ.setFieldValue('custrecord_csv_file_id_no_e',i_CSV_File_ID);
						 
						 */
						var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
						//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
						
						if (_logValidation(i_submit_err_ID)) //
						{
							i_file_status = 'ERROR'
						}
						
					}//Error Records
				}//Employee Present	
				var i_usage_end = i_context.getRemainingUsage();
				//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Usage At End [' + i + '] -->' + i_usage_end);
				
				if (i_usage_end < 500) //
				{
					schedule_script_after_usage_exceeded(i, i_CSV_File_ID, i_recordID, i_account_debit, i_account_credit, i_hourly_employee, i_monthly_employee);
					break;
				}
				
			}
		}
		return i_file_status;
	}//hourly_employee_file_process_data
	
	
	
	function schedule_script_after_usage_exceeded(i,i_CSV_File_ID,i_recordID,i_account_debit,i_account_credit,i_hourly_employee,i_monthly_employee,i_flag)
	{
		////Define all parameters to schedule the script for voucher generation.
		 var params=new Array();
		 params['status']='scheduled';
		  params['runasadmin']='T';
		 params['custscript_counter_v']=i;
		 params['custscript_csv_file_id_s_u']=i_CSV_File_ID;
		 params['custscript_monthly_employee_s_u']=i_monthly_employee;
		 params['custscript_hourly_employee_s_u']=i_hourly_employee;
		 params['custscript_account_debit_s_u']=i_account_debit;
		 params['custscript_account_credit_s_u']=i_account_credit;
		 params['custscript_record_id_s_u']=i_recordID;
		 params['custscript_flag']=i_flag;
		 
		 nlapiLogExecution('DEBUG','Before Scheduling','i_flag -->'+ i_flag);
		 
		// params['custscript_csv_file_name_s_u']=i_CSV_fileID;
			  
		 var startDate = new Date();
		  params['startdate']=startDate.toUTCString();
		
		 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
		 //nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status -->'+ status);
		 
		 ////If script is scheduled then successfuly then check for if status=queued
		 if (status == 'QUEUED') 
		  {
			//nlapiLogExecution('DEBUG', ' SCHEDULED', ' Script Is Re - Scheduled for -->'+i);
		  }
	}//fun close
	
	function removearrayduplicate(array)
	{
		var newArray = new Array();
		label: for (var i = 0; i < array.length; i++) 
		{
			for (var j = 0; j < array.length; j++) 
			{
				if (newArray[j] == array[i]) 
					continue label;
			}
			newArray[newArray.length] = array[i];
		}
		return newArray;
	}
	
	function get_allocation_details(i_employee, d_start_date, d_end_date) //
	{
		var a_allocate_array = new Array();
		var flag_1 = 0;
		var flag_2 = 0;
		var flag_3 = 0;
		
		if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('resource', null, 'is', i_employee)
			// filters[1] = new nlobjSearchFilter('startdate',null,'onorbefore',d_start_date)
			//filters[2] = new nlobjSearchFilter('startdate',null,'onorafter',d_start_date)
			//filters[3] = new nlobjSearchFilter('enddate',null,'onorbefore',d_end_date)
			//filters[2] = new nlobjSearchFilter('enddate',null,'onorafter',d_end_date)
			
			var column = new Array();
			column[0] = new nlobjSearchColumn('internalid')
			column[1] = new nlobjSearchColumn('startdate')
			column[2] = new nlobjSearchColumn('enddate')
			column[3] = new nlobjSearchColumn('percentoftime')
			column[4] = new nlobjSearchColumn('project')
			
			var a_results = nlapiSearchRecord('resourceallocation', null, filters, column);
			
			if (_logValidation(a_results)) //
			{
				//nlapiLogExecution('DEBUG', 'get_allocation_details', ' ************************** Resource Allocation Results Length **************************  -->' + a_results.length);
				
				for (var i = 0; i < a_results.length; i++) //
				{
					flag_1 = 0;
					flag_2 = 0;
					flag_3 = 0;
					//nlapiLogExecution('DEBUG', 'get_allocation_details', '  --------------------- No. --------------- -->' + (i + 1));
					
					var i_recordID = a_results[i].getValue('internalid');
					//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Record ID -->' + i_recordID);
					
					var d_start_date_s = a_results[i].getValue('startdate');
					//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Start Date -->' + d_start_date_s);
					
					var d_end_date_s = a_results[i].getValue('enddate');
					//nlapiLogExecution('DEBUG', 'get_allocation_details', 'End Date-->' + d_end_date_s);
					
					var i_percent_time = a_results[i].getValue('percentoftime');
					//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Percent Of Time -->' + i_percent_time);
					
					var i_project = a_results[i].getValue('project');
					//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Project -->' + i_project);
					//100.0%
					
					//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Month Start Date -->' + d_start_date + '*****' + 'Month End Date -->' + d_end_date);
					
					
					if ((Date.parse(d_start_date_s) <= Date.parse(d_end_date)) && (Date.parse(d_start_date_s) >= Date.parse(d_start_date))) //
					{
						flag_1 = 1;
						//nlapiLogExecution('DEBUG', ' get_allocation_details', ' Start Date is present ......');
					}
					
					if ((Date.parse(d_end_date_s) >= Date.parse(d_start_date)) && (Date.parse(d_end_date_s) <= Date.parse(d_end_date))) //
					{
						flag_2 = 1;
						//nlapiLogExecution('DEBUG', ' get_allocation_details', ' End Date is present ......');
					}
					
					if ((d_start_date_s <= d_start_date && d_end_date <= d_end_date_s))	//  d_start_date_s <= d_start_date&& d_end_date <= d_end_date_s
					{
						flag_3 = 1;
						//nlapiLogExecution('DEBUG', ' flag_3', ' flag_3 ......');
					}
					
					if ((d_start_date_s >= d_start_date && d_end_date <= d_end_date_s))	//  d_start_date_s <= d_start_date&& d_end_date <= d_end_date_s
					{
						flag_3 = 1;
						//nlapiLogExecution('DEBUG', ' flag_3', ' flag_3 ......');
					}
					
					var d_end_date_s = new Date(d_end_date_s);
					var mm = d_end_date_s.getMonth();
					var dd = d_end_date_s.getDate();
					var yy = d_end_date_s.getFullYear();
					
					var date1 = dd + '/' + mm + '/' + yy;
					
					var d_end_date = new Date(d_end_date);
					var mm1 = d_end_date.getMonth();
					var dd1 = d_end_date.getDate();
					var yy1 = d_end_date.getFullYear();
					
					var date2 = dd1 + '/' + mm1 + '/' + yy1;
					
					var date3 = nlapiStringToDate(date1);
					var date4 = nlapiStringToDate(date2);
					
					if (date3 > date4 || yy > yy1) //
					{
						flag_3 = 1;
					}
					else 
						if (yy == yy1 && mm > mm1) //
						{
							flag_3 = 1;
						}
						else 
							if (yy == yy1 && mm == mm1 && (dd > dd1 || dd == dd1)) //
							{
								flag_3 = 1;
							}
					
					//nlapiLogExecution('DEBUG', ' get_allocation_details', ' ---------- Flag 1 -->' + flag_1 + ' ---------Flag 2 -->' + flag_2 + ' ---------Flag 3 -->' + flag_3);
					if ((flag_1 == 1) || (flag_2 == 1) || (flag_3 == 1)) //
					{
						a_allocate_array[i] = i_percent_time + '^^&&^^' + i_project
					}
					
					//a_allocate_array[i] = '';
				}
			}
		} //Validation
		return a_allocate_array;
	} //Allocation
	
	
	function get_current_month_start_date(i_month,i_year)
	{
	   var d_start_date = '';
	   var d_end_date = '';
		
	   var date_format = checkDateFormat();	 
		
	 if (_logValidation(i_month) && _logValidation(i_year)) 
	 {
		   if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
		  {	  	
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 1 + '-' + 1;		
				d_end_date = i_year + '-' + 1 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 31 + '/' + 1 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 1 + '/' + 31 + '/' + i_year;
			 }	 
					
		  }	
		  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
		  {
			  if((parseInt(i_year) % parseInt(4)) == 0)
			{
				i_day = 29;
			}
			else
			{
				i_day = 28;
			}
				
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 2 + '-' + 1;		
				d_end_date = i_year + '-' + 2 + '-' + i_day;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 2 + '/' + i_year;
				d_end_date = i_day + '/' + 2 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 2 + '/' + 1 + '/' + i_year;
				d_end_date = 2 + '/' + i_day + '/' + i_year;
			 }	 
				
		  }		
		  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 3 + '-' + 1;		
				d_end_date = i_year + '-' + 3 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 3 + '/' + i_year;
				d_end_date = 31 + '/' + 3 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 3 + '/' + 1 + '/' + i_year;
				d_end_date = 3 + '/' + 31 + '/' + i_year;
			 }	 
		
		  }	
		  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
		  {
			  if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 4 + '-' + 1;		
				d_end_date = i_year + '-' + 4 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 4 + '/' + i_year;
				d_end_date = 30 + '/' + 4 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 4 + '/' + 1 + '/' + i_year;
				d_end_date = 4 + '/' + 30 + '/' + i_year;
			 }	 
		 
		
		  }	
		  else if(i_month == 'May' || i_month == 'MAY')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 5 + '-' + 1;		
				d_end_date = i_year + '-' + 5 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 5 + '/' + i_year;
				d_end_date = 31 + '/' + 5 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 5 + '/' + 1 + '/' + i_year;
				d_end_date = 5 + '/' + 31 + '/' + i_year;
			 }	 
		
		  }	  
		  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
		  {	 
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 6 + '-' + 1;		
				d_end_date = i_year + '-' + 6 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 6 + '/' + i_year;
				d_end_date = 30 + '/' + 6 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 6 + '/' + 1 + '/' + i_year;
				d_end_date = 6 + '/' + 30 + '/' + i_year;
			 }	 
				
		  }	
		  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 7 + '-' + 1;		
				d_end_date = i_year + '-' + 7 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 7 + '/' + i_year;
				d_end_date = 31 + '/' + 7 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 7 + '/' + 1 + '/' + i_year;
				d_end_date = 7 + '/' + 31 + '/' + i_year;
			 }	 
			
		  }	
		  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 8 + '-' + 1;		
				d_end_date = i_year + '-' + 8 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 8 + '/' + i_year;
				d_end_date = 31 + '/' + 8 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 8 + '/' + 1 + '/' + i_year;
				d_end_date = 8 + '/' + 31 + '/' + i_year;
			 }	 
			
		  }  
		  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
		  {  	
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 9 + '-' + 1;		
				d_end_date = i_year + '-' + 9 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 9 + '/' + i_year;
				d_end_date = 30 + '/' + 9 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 9 + '/' + 1 + '/' + i_year;
				d_end_date = 9 + '/' + 30 + '/' + i_year;
			 }	 
		
		  }	
		  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
		  {	 	
			if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 10 + '-' + 1;		
				d_end_date = i_year + '-' + 10 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 10 + '/' + i_year;
				d_end_date = 31 + '/' + 10 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 10 + '/' + 1 + '/' + i_year;
				d_end_date = 10 + '/' + 31 + '/' + i_year;
			 }		
		  }	
		  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
		  {	  
			if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 11 + '-' + 1;		
				d_end_date = i_year + '-' + 11 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 11 + '/' + i_year;
				d_end_date = 30 + '/' + 11 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 11 + '/' + 1 + '/' + i_year;
				d_end_date = 11 + '/' + 30 + '/' + i_year;
			 }	 
				
		  }	
		  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
		  {	 
			if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 12 + '-' + 1;		
				d_end_date = i_year + '-' + 12 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 12 + '/' + i_year;
				d_end_date = 31 + '/' + 12 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 12 + '/' + 1 + '/' + i_year;
				d_end_date = 12 + '/' + 31 + '/' + i_year;
			 }	 
				
		  }	
	 }//Month & Year
	 return d_start_date;
	}
	
	
	function get_current_month_end_date(i_month,i_year)
	{
		var d_start_date = '';
		var d_end_date = '';
		
		var date_format = checkDateFormat();
			 
	 if (_logValidation(i_month) && _logValidation(i_year)) 
	 {
		   if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
		  {	  	
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 1 + '-' + 1;		
				d_end_date = i_year + '-' + 1 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 31 + '/' + 1 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 1 + '/' + 1 + '/' + i_year;
				d_end_date = 1 + '/' + 31 + '/' + i_year;
			 }	 
					
		  }	
		  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
		  {
			  if((parseInt(i_year) % parseInt(4)) == 0)
			{
				i_day = 29;
			}
			else
			{
				i_day = 28;
			}
				
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 2 + '-' + 1;		
				d_end_date = i_year + '-' + 2 + '-' + i_day;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 2 + '/' + i_year;
				d_end_date = i_day + '/' + 2 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 2 + '/' + 1 + '/' + i_year;
				d_end_date = 2 + '/' + i_day + '/' + i_year;
			 }	 
				
		  }		
		  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 3 + '-' + 1;		
				d_end_date = i_year + '-' + 3 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 3 + '/' + i_year;
				d_end_date = 31 + '/' + 3 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 3 + '/' + 1 + '/' + i_year;
				d_end_date = 3 + '/' + 31 + '/' + i_year;
			 }	 
		
		  }	
		  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
		  {
			  if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 4 + '-' + 1;		
				d_end_date = i_year + '-' + 4 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 4 + '/' + i_year;
				d_end_date = 30 + '/' + 4 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 4 + '/' + 1 + '/' + i_year;
				d_end_date = 4 + '/' + 30 + '/' + i_year;
			 }	 
		 
		
		  }	
		  else if(i_month == 'May' || i_month == 'MAY')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 5 + '-' + 1;		
				d_end_date = i_year + '-' + 5 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 5 + '/' + i_year;
				d_end_date = 31 + '/' + 5 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 5 + '/' + 1 + '/' + i_year;
				d_end_date = 5 + '/' + 31 + '/' + i_year;
			 }	 
		
		  }	  
		  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
		  {	 
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 6 + '-' + 1;		
				d_end_date = i_year + '-' + 6 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 6 + '/' + i_year;
				d_end_date = 30 + '/' + 6 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 6 + '/' + 1 + '/' + i_year;
				d_end_date = 6 + '/' + 30 + '/' + i_year;
			 }	 
				
		  }	
		  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 7 + '-' + 1;		
				d_end_date = i_year + '-' + 7 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 7 + '/' + i_year;
				d_end_date = 31 + '/' + 7 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 7 + '/' + 1 + '/' + i_year;
				d_end_date = 7 + '/' + 31 + '/' + i_year;
			 }	 
			
		  }	
		  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
		  {	  
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 8 + '-' + 1;		
				d_end_date = i_year + '-' + 8 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 8 + '/' + i_year;
				d_end_date = 31 + '/' + 8 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 8 + '/' + 1 + '/' + i_year;
				d_end_date = 8 + '/' + 31 + '/' + i_year;
			 }	 
			
		  }  
		  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
		  {  	
			 if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 9 + '-' + 1;		
				d_end_date = i_year + '-' + 9 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 9 + '/' + i_year;
				d_end_date = 30 + '/' + 9 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 9 + '/' + 1 + '/' + i_year;
				d_end_date = 9 + '/' + 30 + '/' + i_year;
			 }	 
		
		  }	
		  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
		  {	 	
			if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 10 + '-' + 1;		
				d_end_date = i_year + '-' + 10 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 10 + '/' + i_year;
				d_end_date = 31 + '/' + 10 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 10 + '/' + 1 + '/' + i_year;
				d_end_date = 10 + '/' + 31 + '/' + i_year;
			 }	 
		
			
		  }	
		  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
		  {	  
			if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 11 + '-' + 1;		
				d_end_date = i_year + '-' + 11 + '-' + 30;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 11 + '/' + i_year;
				d_end_date = 30 + '/' + 11 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 11 + '/' + 1 + '/' + i_year;
				d_end_date = 11 + '/' + 30 + '/' + i_year;
			 }	 
				
		  }	
		  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
		  {	 
			if (date_format == 'YYYY-MM-DD')
			 {
				 d_start_date = i_year + '-' + 12 + '-' + 1;		
				d_end_date = i_year + '-' + 12 + '-' + 31;		
			 } 
			 if (date_format == 'DD/MM/YYYY') 
			 {
				  d_start_date = 1 + '/' + 12 + '/' + i_year;
				d_end_date = 31 + '/' + 12 + '/' + i_year;
			 }
			 if (date_format == 'MM/DD/YYYY')
			 {
				  d_start_date = 12 + '/' + 1 + '/' + i_year;
				d_end_date = 12 + '/' + 31 + '/' + i_year;
			 }	 
				
		  }	
	 }//Month & Year
	 return d_end_date;
	}
	
	function checkDateFormat()
	{
		var context = nlapiGetContext();
	
		var dateFormatPref = context.getPreference('dateformat');
	
		return dateFormatPref;
	}
	//--------------------------------------------------------------------------
	function getDatediffIndays(startDate, endDate)
	{
		var one_day=1000*60*60*24;
		var fromDate = startDate;
		var toDate=endDate
	
		var date1 = nlapiStringToDate(fromDate);
	
		var date2 = nlapiStringToDate(toDate);
	
		var date3=Math.round((date2-date1)/one_day);
	
		return (date3+1);
	}
	//-----------------------------------------------------------------------
	
	function getWeekend(startDate, endDate) //
	{
		var i_no_of_sat = 0;
		var i_no_of_sun = 0;
		
		nlapiLogExecution('DEBUG', 'Check Date', 'startDate : ' + startDate);
		nlapiLogExecution('DEBUG', 'Check Date', 'endDate : ' + endDate);
		
		var date_format = checkDateFormat();
		
		var startDate_1 = startDate
		var endDate_1 = endDate
		
		startDate = nlapiStringToDate(startDate);
		endDate = nlapiStringToDate(endDate);
		
		nlapiLogExecution('DEBUG', 'Check Date', '2 startDate : ' + startDate);
		nlapiLogExecution('DEBUG', 'Check Date', '2 endDate : ' + endDate);
		
		var i_count_day = startDate.getDate();
		
		var i_count_last_day = endDate.getDate();
		
		i_month = startDate.getMonth() + 1;
		
		i_year = startDate.getFullYear();
		
		nlapiLogExecution('DEBUG', 'Check Date', '2 startDate_1 : ' + startDate_1);
		nlapiLogExecution('DEBUG', 'Check Date', '2 endDate_1 : ' + endDate_1);
		
		var d_f = new Date();
		var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month
		var sat = new Array(); //Declaring array for inserting Saturdays
		var sun = new Array(); //Declaring array for inserting Sundays
		
		for (var i = i_count_day; i <= i_count_last_day; i++) //
		{
			//looping through days in month
			if (date_format == 'YYYY-MM-DD') //
			{
				var newDate = i_year + '-' + i_month + '-' + i;
			}
			if (date_format == 'DD/MM/YYYY') //
			{
				var newDate = i + '/' + i_month + '/' + i_year;
			}
			if (date_format == 'MM/DD/YYYY') //
			{
				var newDate = i_month + '/' + i + '/' + i_year;
			}
			
			newDate = nlapiStringToDate(newDate);
			
			if (newDate.getDay() == 0) //
			{ //if Sunday
				sat.push(i);
				i_no_of_sat++;
			}
			if (newDate.getDay() == 6) //
			{ //if Saturday
				sun.push(i);
				i_no_of_sun++;
			}
		}
		
		var i_total_days_sat_sun = parseInt(i_no_of_sat) + parseInt(i_no_of_sun);
		nlapiLogExecution('DEBUG', 'Check Date', '2 i_total_days_sat_sun : ' + i_total_days_sat_sun);
		
		return i_total_days_sat_sun;
	}
	
	//=============================================================================================
	function check_emp_over_or_under_allocated(i_employee, d_start_date, d_end_date) //
	{
	  
		  nlapiLogExecution('DEBUG', 'Nihal i_employee',i_employee);
		  nlapiLogExecution('DEBUG', 'Nihal d_start_date',d_start_date);
		  nlapiLogExecution('DEBUG', 'Nihal d_end_date',d_end_date);
		var i_internal_ID;
		
		var i_hours_per_week;
		var i_project_name;
		var i_allocated_hours;
		var flag = 0;
		
		var month_start_date = d_start_date;
		var month_end_date = d_end_date;
		
		//--------------------------employee load to search hire date and termination date-------------
		var o_empOBJ = nlapiLoadRecord('employee', i_employee) //
		if (_logValidation(o_empOBJ)) //
		{//Employee OBJ
			var i_hire_date = o_empOBJ.getFieldValue('hiredate');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Hire Date  -->' + i_hire_date);
			
			//var i_resigned_date = o_empOBJ.getFieldValue('releasedate'); // This line is commented due to update in logic : we are now using custom field for validation.
			var i_resigned_date = o_empOBJ.getFieldValue('custentity_lwd');
			nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Resigned Date -->' + i_resigned_date);
		
		}//Employee OBJ
		
		var i_hire_date_dt_format = null;
		if(i_hire_date!= null) //
		{
			i_hire_date_dt_format = nlapiStringToDate(i_hire_date);
		}
		
		var i_resigned_date_dt_format = null;
		if(_logValidation(i_resigned_date)) //
		{
			nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Parsing Date ***');
			i_resigned_date_dt_format = nlapiStringToDate(i_resigned_date);
		}
		
		var s_month_start_date_dt_format = nlapiStringToDate(month_start_date);
		var s_month_end_date_dt_format = nlapiStringToDate(month_end_date);
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		if (_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //i_month
		{
			nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 1 ***');
			if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is current month .....');
				
				d_start_date = i_hire_date
				d_end_date = d_end_date
			}
			else //
			{
				nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is not current month .....');
				d_start_date = d_start_date
				d_end_date = d_end_date
			}
			
		}//Hire Date is not blank & Termination Date is blank
		if (_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
		{
			nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 2 ***');
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' both r present');
			if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
				{
					//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Hire Date is current month .....');
					d_start_date = i_hire_date;
					
					//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Resigned Date is current month .....');
					d_end_date = i_resigned_date
				}
				else //
				{
					//	//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Hire Date is current month .....');
					d_start_date = i_hire_date;
					
					//	//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Resigned Date is current month .....');
					d_end_date = d_end_date
				}
			}
			else 
				if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
				{
					d_start_date = d_start_date
					d_end_date = i_resigned_date;
				}
				else //
				{
					d_start_date = d_start_date
					d_end_date = d_end_date
				}
		}//Hire Date is not blank & Termination Date is not blank
		
		if (!_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
		{
			nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 3 ***');
			if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' no hire date but resigned present .....');
				
				d_start_date = d_start_date
				d_end_date = i_resigned_date
				
			}
			else //
			{
				d_start_date = d_start_date
				d_end_date = d_end_date
				
			}
			
		}//Hire Date is blank & Termination Date is not blank
		if (!_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //
		{
			nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 4 ***');
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' No Hire Date & Resigned Date .....');
			
			d_start_date = d_start_date;
			d_end_date = d_start_date;
			
		}//Hire Date is blank & Termination Date is blank
		if (_logValidation(d_start_date)) //
		{
			d_start_date = nlapiStringToDate(d_start_date);
			d_start_date = nlapiDateToString(d_start_date);
		}
		
		if (_logValidation(d_end_date)) //
		{
			d_end_date = nlapiStringToDate(d_end_date);
			d_end_date = nlapiDateToString(d_end_date)
			
		}
		
		nlapiLogExecution('DEBUG', 'Test', 'd_start_date : ' + d_start_date);
		nlapiLogExecution('DEBUG', 'Test', 'd_end_date : ' + d_end_date);
		
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		var i_total_days = getDatediffIndays(d_start_date, d_end_date);
		nlapiLogExecution('DEBUG', 'i_total_days', i_total_days);
		
		var i_total_sat_sun = getWeekend(d_start_date, d_end_date);
		nlapiLogExecution('DEBUG', 'i_total_sat_sun', i_total_sat_sun);
		
		var tot_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
		nlapiLogExecution('DEBUG', 'tot_hrs', tot_hrs); //i_total_days
		//---------------------------------------------------------------------------------------------
		
		if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
			//filters[1] = new nlobjSearchFilter('date', null, 'onorafter', month_start_date)
			//filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', month_end_date)
			
			filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date);
			filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date);
			
			var i_search_results = searchRecord('timebill', 'customsearch_salaried_new_ti_2', filters, null);
			// Nihal has doubt on search
			if (_logValidation(i_search_results)) //
			{
				//if start
				//nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ----------- Salaried Time Search Length --------->' + i_search_results.length);
				
				var a_search_transaction_result = i_search_results[0];
				
				if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) //
				{
				
				}
				var columns = a_search_transaction_result.getAllColumns();
				
				var columnLen = columns.length;
				var i_duration = a_search_transaction_result.getValue(columns[2]);
			
				/*for (var hg = 0; hg < columnLen; hg++)
				 {
				 var column = columns[hg];
				 var label = column.getLabel();
				 var value = a_search_transaction_result.getValue(column)
				 var text = a_search_transaction_result.getText(column)
				 if (label == 'Duration')
				 {
				 var i_duration = value;
				 }
				 }*/
				nlapiLogExecution('DEBUG','i_duration',i_duration);
				
				if (parseFloat(i_duration) > parseFloat(tot_hrs)) //
				{
					flag = 1;
				}
				
				if (parseFloat(i_duration) < parseFloat(tot_hrs)) //
				{
					flag = 2;
				}
			}// if close
		}
		return flag;
	} //Monthly Details
	//=============================================================================================
	// END FUNCTION =====================================================