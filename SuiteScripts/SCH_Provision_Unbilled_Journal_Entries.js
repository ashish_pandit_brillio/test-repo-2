// @author Shweta

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
		Script Name : SCH_Provision_Unbilled_Journal_Entries.js
		Author      : Shweta Chopde
		Date        : 15 May 2014
		Description : Create Journal Entries for Unbilled Expense / Time Records
	
		Script Modification Log:
	
		-- Date --			-- Modified By --				--Requested By--				-- Description --
	
	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.
	
	
		 SCHEDULED FUNCTION - scheduledFunction(type)
	
		 SUB-FUNCTIONS - The following sub-functions are called by the above core functions in order to maintain code
				modularization:
				 - NOT USED
	*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type) {
	var i_criteria_text = '';
	try {
		var i_context = nlapiGetContext();

		var i_time_counter = i_context.getSetting('SCRIPT', 'custscript_time_counter');
		nlapiLogExecution('DEBUG', 'i_time_counter', ' i_time_counter -->' + i_time_counter);//custscript_time_counter_curr

		var i_curr_counter = i_context.getSetting('SCRIPT', 'custscript_time_counter_curr');
		nlapiLogExecution('DEBUG', 'i_curr_counter', ' i_curr_counter -->' + i_curr_counter);

		if (_logValidation(i_curr_counter)) {
			i_curr_counter = i_curr_counter;
		}//if
		else {
			i_curr_counter = 0;
		}

		var i_jv_id = i_context.getSetting('SCRIPT', 'custscript_jvid');
		nlapiLogExecution('DEBUG', 'i_jv_id', ' i_jv_id -->' + i_jv_id);

		if (_logValidation(i_time_counter)) {
			i_time_counter = i_time_counter;
		}//if
		else {
			i_time_counter = 0;
		}
		var i_expense_counter = i_context.getSetting('SCRIPT', 'custscript_expense_counter');


		if (_logValidation(i_expense_counter)) {
			i_expense_counter = i_expense_counter;
		}//if
		else {
			i_expense_counter = 0;
		}

		var i_data_process = i_context.getSetting('SCRIPT', 'custscript_data_process_provision');
		var i_custom_id = i_context.getSetting('SCRIPT', 'custscript_custom_id_process');
		var d_current_date = i_context.getSetting('SCRIPT', 'custscript_current_date_sch');
		var i_unbilled_type = i_context.getSetting('SCRIPT', 'custscript_unbilled_type_sch');
		var i_unbilled_receivable_GL = i_context.getSetting('SCRIPT', 'custscript_unbilled_receivable_sch');
		var i_unbilled_revenue_GL = i_context.getSetting('SCRIPT', 'custscript_unbilled_revenue_sch');
		var d_reverse_date = i_context.getSetting('SCRIPT', 'custscript_reverse_date_sch');
		var a_data_array = i_context.getSetting('SCRIPT', 'custscript_data_array');
		var a_data_criteria_array = i_context.getSetting('SCRIPT', 'custscript_data_criteria');
		var i_criteria = i_context.getSetting('SCRIPT', 'custscript_criteria_type_sch');



		if (i_criteria == 1) {
			i_criteria_text = 'Approved - Unbilled';
		}
		else if (i_criteria == 2) {
			i_criteria_text = 'Submitted but Not approved - Unbilled';
		}
		else {
			i_criteria_text = 'Not submitted - Allocated';
		}


		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Criteria -->' + i_criteria);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Data Process -->' + i_data_process);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Custom ID  -->' + i_custom_id);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Current Date -->' + d_current_date);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Unbilled Type -->' + i_unbilled_type);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Unbilled Receivable GL -->' + i_unbilled_receivable_GL);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Unbilled Revenue GL -->' + i_unbilled_revenue_GL);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Reverse Date -->' + d_reverse_date);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Data Array  -->' + a_data_array);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Data Array  -->' + a_data_array.length);
		nlapiLogExecution('DEBUG', 'schedulerFunction', ' Data Criteria Array  -->' + a_data_criteria_array);

		if (i_unbilled_type == 1) {
			expense_report_details(i_criteria, i_context, a_data_criteria_array, a_data_array, d_current_date, i_unbilled_receivable_GL, i_unbilled_revenue_GL, d_reverse_date, i_time_counter, i_unbilled_type, i_data_process, i_custom_id, i_jv_id, i_curr_counter)
		}//EXPENSE		 
		if (i_unbilled_type == 2) {
			time_tracking_details(i_criteria, i_context, a_data_criteria_array, a_data_array, d_current_date, i_unbilled_receivable_GL, i_unbilled_revenue_GL, d_reverse_date, i_time_counter, i_unbilled_type, i_data_process, i_custom_id, i_jv_id, i_curr_counter, i_criteria_text)
		}//TIME	

		if (_logValidation(i_custom_id)) {
			var i_deleteID = nlapiDeleteRecord('customrecord_provision_processed_records', i_custom_id);
			nlapiLogExecution('DEBUG', 'schedulerFunction', ' ************ Delete Custom rec ID  ******** -->' + i_deleteID);

		}


	}
	catch (err) {
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + err);
		var i_deleteID = nlapiDeleteRecord('customrecord_provision_processed_records', i_custom_id);
		nlapiLogExecution('DEBUG', 'ERROR', ' ************ Delete Custom rec ID  ******** -->' + i_deleteID);
		Send_Exception_Mail(err);


	}//CATCH

}//Scheduler Function

// END SCHEDULED FUNCTION ===============================================


// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}
function time_tracking_details(i_criteria, i_context, a_data_criteria_array, a_data_array, d_current_date, i_unbilled_receivable_GL, i_unbilled_revenue_GL, d_reverse_date, i_time_counter, i_unbilled_type, i_data_process, i_custom_id, i_jv_id, i_curr_counter, i_criteria_text) {//i_data_process

	nlapiLogExecution('DEBUG', 'function call');

	var i_employee;
	var i_project;
	var i_vertical;
	var i_practice;
	var i_amount;
	var i_currency;
	var i_hours;
	var i_rate;
	var i_internal_id;
	var i_subsidiary;
	var i_customer;
	var i_item;
	var i_month_of_date;
	var flag = 0;

	var a_TT_array_values_Curr = new Array();
	if (_logValidation(a_data_array)) {
		var a_TT_array_values = new Array()
		var i_data_TT = new Array()
		i_data_TT = a_data_array;

		nlapiLogExecution('DEBUG', 'time_tracking_details', 'Data TT Array Values Length -->' + i_data_TT.length);

		for (var dt = 0; dt < i_data_TT.length; dt++) {
			a_TT_array_values = i_data_TT.split('curr')
			break;
		}

		var i_data_CRT = new Array()
		i_data_CRT = a_data_criteria_array;

		//var sch_count=1;
		nlapiLogExecution('DEBUG', 'time_tracking_details', 'TT Array Values-->' + a_TT_array_values);
		nlapiLogExecution('DEBUG', 'time_tracking_details', ' TT Array Values Length-->' + a_TT_array_values.length);
		var first_loop_count = 0;
		nlapiLogExecution('DEBUG', 'i_time_counter', ' i_time_counter=============>' + i_time_counter);
		nlapiLogExecution('DEBUG', 'i_curr_counter', ' i_curr_counter==============>' + i_curr_counter);

		for (var sk = i_curr_counter, sch_count1 = 1; sk < a_TT_array_values.length; sk++, sch_count1++) {
			first_loop_count = sk;

			var o_journalOBJ = nlapiCreateRecord('journalentry');
			o_journalOBJ.setFieldValue('trandate', d_current_date);
			o_journalOBJ.setFieldValue('reversaldate', d_reverse_date);
			o_journalOBJ.setFieldValue('reversaldefer', 'T');//
			o_journalOBJ.setFieldValue('custbody_financehead', '3167');//

			var o_time_expense_recOBJ = nlapiCreateRecord('customrecord_timeexpenseprovision');
			var a_TR_ID_array = new Array();

			var tot_credit = 0;
			nlapiLogExecution('DEBUG', 'a_TT_array_values[sk]=============>', ' a_TT_array_values[sk]' + a_TT_array_values[sk]);
			if (a_TT_array_values[sk] == '' || a_TT_array_values[sk] == null || a_TT_array_values[sk] == 'undefined') {
			}
			else {
				first_loop_count++;
				var str = a_TT_array_values[sk].toString();
				a_TT_array_values_Curr = str.split(',');
				nlapiLogExecution('DEBUG', 'a_TT_array_values_Curr===========>', 'a_TT_array_values_Curr' + a_TT_array_values_Curr);

				for (var ss = i_time_counter, sch_count = 1; ss < a_TT_array_values_Curr.length; ss++) {
					// var ss=0;
					if (a_TT_array_values_Curr[ss] == '' || a_TT_array_values_Curr[ss] == 'undefined') {
					}
					else {
						a_split_data_array = a_TT_array_values_Curr[ss].split('#####');

						i_employee = a_split_data_array[0];
						i_project = a_split_data_array[1];
						i_vertical = a_split_data_array[2];
						i_practice = a_split_data_array[3];
						i_amount = a_split_data_array[4];
						i_currency = a_split_data_array[5];
						i_hours = a_split_data_array[6];
						i_rate = a_split_data_array[7];
						i_exchange_rate = a_split_data_array[8];
						i_internal_id = a_split_data_array[9];
						i_subsidiary = parseInt(a_split_data_array[10]);
						i_customer = parseInt(a_split_data_array[11]);
						i_item = parseInt(a_split_data_array[13]);
						i_month_of_date = (a_split_data_array[14]);

						nlapiLogExecution('DEBUG', 'i_month_of_date' + i_month_of_date, i_month_of_date);

						if (i_customer == null || i_customer == '') {
						}
						else {
							var load_customer = nlapiLoadRecord('customer', i_customer);
							var cust_id = load_customer.getFieldValue('entityid');
							var cust_name = load_customer.getFieldValue('altname');

							var customer_name = cust_id + ' ' + cust_name;
						}

						if (sch_count == '1') {
							o_journalOBJ.setFieldValue('subsidiary', i_subsidiary);
							o_journalOBJ.setFieldValue('currency', i_currency);
						}
						var i_project_name = get_project_name(i_project);

						var project_fld = i_project_name.split('##');
						var project_description = project_fld[0];
						var project_name = project_fld[1];

						nlapiLogExecution('DEBUG', 'i_project', i_project);
						nlapiLogExecution('DEBUG', 'i_internal_id', i_internal_id);

						if (i_internal_id == null || i_internal_id == '' || i_internal_id == 'undefined') {
						}
						else {
							tot_credit = parseFloat(tot_credit) + parseFloat(i_amount);
							tot_credit = tot_credit.toFixed(2);
							i_amount = parseFloat(i_amount);
							i_amount = i_amount.toFixed(2);

							a_TR_ID_array.push(i_internal_id)

							nlapiLogExecution('DEBUG', 'i_amount', i_amount);
							nlapiLogExecution('DEBUG', 'credit line added');

							o_journalOBJ.selectNewLineItem('line')

							//o_journalOBJ.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(i)+parseInt(2));
							o_journalOBJ.setCurrentLineItemValue('line', 'account', i_unbilled_revenue_GL);
							o_journalOBJ.setCurrentLineItemValue('line', 'credit', i_amount);
							o_journalOBJ.setCurrentLineItemValue('line', 'custcol_employee_id', i_employee);
							o_journalOBJ.setCurrentLineItemValue('line', 'department', i_practice);
							o_journalOBJ.setCurrentLineItemValue('line', 'class', i_vertical);
							// Added by Sitaram
							o_journalOBJ.setCurrentLineItemValue('line', 'custcolprj_name', project_description);//set project description
							o_journalOBJ.setCurrentLineItemValue('line', 'custcol_hour', i_hours);//custcol_temp_project
							o_journalOBJ.setCurrentLineItemValue('line', 'custcol_billable_rate', i_rate);
							o_journalOBJ.setCurrentLineItemValue('line', 'memo', i_criteria_text + '-' + i_month_of_date);

							if (i_criteria == 3) {
							}
							else {
								o_journalOBJ.setCurrentLineItemValue('line', 'custcol_itemlistinje', i_item);
							}

							//---------------------------------------------------------------
							//var i_project_fld=i_project_name.split('');
							nlapiLogExecution('DEBUG', 'i_curr_counter', ' project name==============>' + project_name);

							o_journalOBJ.setCurrentLineItemValue('line', 'custcol_project_name', project_name);//set project name
							o_journalOBJ.setCurrentLineItemValue('line', 'custcol_sow_project', i_project);////set project if
							//---------------------------------------------------------------

							if (isNaN(i_customer)) {
							}
							else {
								//o_journalOBJ.setCurrentLineItemValue('line','custcol_customer_list',i_customer);
								o_journalOBJ.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', customer_name);
							}
							o_journalOBJ.commitLineItem('line');

						}

						sch_count++
						//var count='';	
						if (sch_count == 250)//if (i_usage_end < 250) 
						{
							flag = 1;
							break;
						}
						else {
							schedule_count = sch_count;
						}
					}

				}//for close
				nlapiLogExecution('DEBUG', 'tot_credit', tot_credit);
				nlapiLogExecution('DEBUG', 'debit line added');
				//--------------------------------------------------------------
				o_journalOBJ.selectNewLineItem('line')
				o_journalOBJ.setCurrentLineItemValue('line', 'account', i_unbilled_receivable_GL);
				o_journalOBJ.setCurrentLineItemValue('line', 'debit', tot_credit);
				// Added by Sitaram
				o_journalOBJ.commitLineItem('line');
				//-------------------------------------------------------------------------
				nlapiLogExecution('DEBUG', 'before if check array ref a_TR_ID_array', a_TR_ID_array);

				{
					o_journalOBJ.setFieldValue('custbody_time_bill', a_TR_ID_array);//custbody_time_bill  
				}

				o_journalOBJ.setFieldValue('custbody_approvalstatusonje', 2);
				o_journalOBJ.setFieldValue('approved', 'T');
				o_journalOBJ.setFieldValue('custbody_approved_by_provision', 'T');

				var i_journal_entry_SubmitID = nlapiSubmitRecord(o_journalOBJ, true, true)
				nlapiLogExecution('DEBUG', ' create_journal_entry ', ' *********** Journal Entry Submit ID  *********** -->' + i_journal_entry_SubmitID);
				//--------------------------------------------------------------------------------------------------------------------------------------

				var i_usage_end = i_context.getRemainingUsage();
				//nlapiLogExecution('DEBUG', 'first time_tracking_details','Usage End -->' + i_usage_end);

				var count = '';
				var curr_count = '';
				if (i_journal_entry_SubmitID != null)// && schedule_count >= 100
				{

					var fun_status = update_for_provision_created_TR_3(a_TR_ID_array, i_journal_entry_SubmitID, d_reverse_date);
					nlapiLogExecution('DEBUG', 'fun_status', 'fun_status' + fun_status);

					nlapiLogExecution('DEBUG', 'first time_tracking_details', 'Usage End -->' + i_usage_end);
					nlapiLogExecution('DEBUG', 'sch_count1', sch_count1);
					nlapiLogExecution('DEBUG', 'sk', sk);


					count = ss + 1;

					if (flag == '1') {
						curr_count = sk;
					}
					else {
						curr_count = parseInt(sk) + 1;
					}

					if (flag == '0') {
						count = 0;
					}

					nlapiLogExecution('DEBUG', 'curr_count', ' ******************i***************** ' + curr_count);
					nlapiLogExecution('DEBUG', 'count', ' ******************i***************** ' + count);
					var params = new Array();//i_data_process
					//params['custscript_jvid']=jv_id;
					params['custscript_time_counter'] = count;
					params['custscript_time_counter_curr'] = curr_count;
					params['custscript_current_date_sch'] = d_current_date
					params['custscript_unbilled_type_sch'] = i_unbilled_type
					params['custscript_unbilled_receivable_sch'] = i_unbilled_receivable_GL
					params['custscript_unbilled_revenue_sch'] = i_unbilled_revenue_GL
					params['custscript_reverse_date_sch'] = d_reverse_date
					params['custscript_data_array'] = a_data_array
					params['custscript_data_process_provision'] = i_data_process
					params['custscript_custom_id_process'] = i_custom_id
					params['custscript_data_criteria'] = a_data_criteria_array
					params['custscript_criteria_type_sch'] = i_criteria

					//------------------------------------------------------------
					// params['status']='scheduled';
					// params['runasadmin']='T';


					// var startDate = new Date();
					// params['startdate']=startDate.toUTCString();

					var status = nlapiScheduleScript('customscriptsch_provision_unbilled_journ', 'customdeploy1', params);
					nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_TR', ' Script Scheduled Status -->' + status);

					////If script is scheduled then successfuly then check for if status=queued
					if (status == 'QUEUED') {
						nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_TR', 'Script rescheduled for ' + ss);
						break;
					}
				}
				//---------------------------------------------------------------------------------


				//--------------------------------------------------------------------------------------------------------------------------------------

				if (_logValidation(i_journal_entry_SubmitID)) {
					//a_TR_ID_array = removearrayduplicate(a_TR_ID_array);		
					//nlapiLogExecution('DEBUG', 'time_tracking_details',' a_TR_ID_array -->' +a_TR_ID_array);

					var fields = new Array();
					var values = new Array();
					nlapiLogExecution('DEBUG', 'sch_count1', sch_count1);
					if (i_criteria == 3) {
						if (sch_count1 == '1') {
							nlapiLogExecution('DEBUG', '1111111111111111111111111111111');
							var params = new Array();
							params['custscript_date_array'] = a_TR_ID_array.toString();
							params['custscript_reverse'] = d_reverse_date;
							params['custscript_jvid'] = i_journal_entry_SubmitID;

							var status = nlapiScheduleScript('customscript_sch_update_custom_time_bill', 'customdeploy1', params);
							nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_ER', ' Script Scheduled Status -->' + status);
						}
						else if (sch_count1 == '2') {
							nlapiLogExecution('DEBUG', '2222222222222222222222222222222222');
							var params = new Array();
							params['custscript_date_array'] = a_TR_ID_array.toString();
							params['custscript_reverse'] = d_reverse_date;
							params['custscript_jvid'] = i_journal_entry_SubmitID;

							var status = nlapiScheduleScript('customscript_sch_update_custom_time_bill', 'customdeploy2', params);
							nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_ER', ' Script Scheduled Status -->' + status);

						}
						else if (sch_count1 == '3') {

							nlapiLogExecution('DEBUG', '3333333333333333333333333333333333');
							var params = new Array();
							params['custscript_date_array'] = a_TR_ID_array.toString();
							params['custscript_reverse'] = d_reverse_date;
							params['custscript_jvid'] = i_journal_entry_SubmitID;

							var status = nlapiScheduleScript('customscript_sch_update_custom_time_bill', 'customdeploy3', params);
							nlapiLogExecution('DEBUG', 'sch_count3 schedule_script_after_usage_exceeded_ER', ' Script Scheduled Status -->' + status);

						}
						else if (sch_count1 == '4') {

							var params = new Array();
							params['custscript_date_array'] = a_TR_ID_array.toString();
							params['custscript_reverse'] = d_reverse_date;
							params['custscript_jvid'] = i_journal_entry_SubmitID;

							var status = nlapiScheduleScript('customscript_sch_update_custom_time_bill', 'customdeploy4', params);
							nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_ER', ' Script Scheduled Status -->' + status);

						}
						else if (sch_count1 == '5') {

							var params = new Array();
							params['custscript_date_array'] = a_TR_ID_array.toString();
							params['custscript_reverse'] = d_reverse_date;
							params['custscript_jvid'] = i_journal_entry_SubmitID;

							var status = nlapiScheduleScript('customscript_sch_update_custom_time_bill', 'customdeploy5', params);
							nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_ER', ' Script Scheduled Status -->' + status);

						}
						else if (sch_count1 == '6') {

							var params = new Array();
							params['custscript_date_array'] = a_TR_ID_array.toString();
							params['custscript_reverse'] = d_reverse_date;
							params['custscript_jvid'] = i_journal_entry_SubmitID;

							var status = nlapiScheduleScript('customscript_sch_update_custom_time_bill', 'customdeploy6', params);
							nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_ER', ' Script Scheduled Status -->' + status);

						}
					}
					else {
					}

				}
			}//else close		

			var i_usage_end = i_context.getRemainingUsage();
			nlapiLogExecution('DEBUG', 'after first loop time_tracking_details', 'Usage End  -->' + i_usage_end);


		}//Data Array Loop				
	}//Data Array

}//Time Tracking
function expense_report_details(i_criteria, i_context, a_data_criteria_array, a_data_array, d_current_date, i_unbilled_receivable_GL, i_unbilled_revenue_GL, d_reverse_date, i_time_counter, i_unbilled_type, i_data_process, i_custom_id, i_jv_id, i_curr_counter) {
	var i_employee;
	var i_project;
	var i_vertical;
	var i_practice;
	var i_amount;
	var i_currency;
	var i_internal_id;
	var i_subsidiary;
	var flag = 0;

	var a_expense_array = new Array();
	var a_ER_array_values_Curr = new Array();
	//2: '209190',
	var vendorList = {
		2: '209299',
		3: '209299',
		5: '209360',
		6: '209361',
		7: '209300',
		8: '209358',
		9: '209359',
		10: '209301',
		21: '209302',
		23: '209303',

	}
	if (_logValidation(a_data_array)) {
		var a_ER_array_values = new Array()
		var i_data_ER = new Array()
		i_data_ER = a_data_array;

		for (var dt = 0; dt < i_data_ER.length; dt++) {
			a_ER_array_values = i_data_ER.split('curr')
			break;
		}

		nlapiLogExecution('DEBUG', 'expense_report_details', 'ER Array Values-->' + a_ER_array_values);
		//	 nlapiLogExecution('DEBUG', 'expense_report_details',' ER Array Values Length-->' + a_ER_array_values.length);

		var a_CRT_array_values = new Array()
		var i_data_CRT = new Array()
		i_data_CRT = a_data_criteria_array;



		nlapiLogExecution('DEBUG', 'time_tracking_details', 'CRT Array Values-->' + a_CRT_array_values);
		// ========================== Create Journal Entry ========================

		a_expense_array.push(i_internal_id);

		// ============ Time & Expense Provision ==============			
		for (var i = i_curr_counter; i < a_ER_array_values.length; i++) {//data array loop start
			var a_ER_ID_array = new Array();
			var tot_credit = 0;
			var o_journalOBJ = nlapiCreateRecord('journalentry')

			o_journalOBJ.setFieldValue('trandate', d_current_date);
			o_journalOBJ.setFieldValue('reversaldate', d_reverse_date);
			o_journalOBJ.setFieldValue('reversaldefer', 'T');
			var o_time_expense_recOBJ = nlapiCreateRecord('customrecord_timeexpenseprovision');

			if (a_ER_array_values[i] == '' || a_ER_array_values[i] == null) {
			}
			else {
				var str = a_ER_array_values[i].toString();
				a_ER_array_values_Curr = str.split(',');
				nlapiLogExecution('DEBUG', 'a_ER_array_values_Curr===========>', 'a_ER_array_values_Curr' + a_ER_array_values_Curr.length);

				for (var ss = i_time_counter, sch_count = 1; ss < a_ER_array_values_Curr.length; ss++, sch_count++) {
					nlapiLogExecution('DEBUG', 'a_ER_array_values_Curr===========>', 'a_ER_array_values_Curr' + a_ER_array_values_Curr[ss]);

					var str_2 = a_ER_array_values_Curr[ss].toString();
					a_split_data_array = str_2.split('#####')
					nlapiLogExecution('DEBUG', 'a_split_data_array===========>', 'a_split_data_array' + a_split_data_array[ss]);
					i_employee = a_split_data_array[0];
					i_project = a_split_data_array[1];
					i_vertical = a_split_data_array[2];
					i_practice = a_split_data_array[3];
					i_date = a_split_data_array[4];
					i_amount = a_split_data_array[5];
					i_currency = a_split_data_array[6];
					i_exchange_rate = a_split_data_array[7];
					i_internal_id = a_split_data_array[8];
					i_subsidiary = parseInt(a_split_data_array[9]);
					i_location = a_split_data_array[10];
					i_customer = a_split_data_array[11];//sk
					nlapiLogExecution('DEBUG', 'i_subsidiary', i_subsidiary);
					nlapiLogExecution('DEBUG', 'i_customer', i_customer);

					if (i_customer == null || i_customer == '') {
					}
					else {
						var load_customer = nlapiLoadRecord('customer', i_customer);
						var cust_id = load_customer.getFieldValue('entityid');
						var cust_name = load_customer.getFieldValue('altname');

						var customer_name = cust_id + ' ' + cust_name;
					}

					if (sch_count == '1') {
						o_journalOBJ.setFieldValue('subsidiary', i_subsidiary);
						o_journalOBJ.setFieldValue('currency', i_currency);
					}

					tot_credit = parseFloat(tot_credit) + parseFloat(i_amount);
					tot_credit = tot_credit.toFixed(2);
					i_amount = parseFloat(i_amount);
					i_amount = i_amount.toFixed(2);
					var i_project_name = get_project_name(i_project);
					var project_fld = i_project_name.split('##');
					var project_description = project_fld[0];
					var project_name = project_fld[1];

					a_ER_ID_array.push(i_internal_id)

					o_journalOBJ.selectNewLineItem('line');
					o_journalOBJ.setCurrentLineItemValue('line', 'account', i_unbilled_revenue_GL);
					o_journalOBJ.setCurrentLineItemValue('line', 'custcol_employee_id', i_employee);
					o_journalOBJ.setCurrentLineItemValue('line', 'credit', i_amount);
					o_journalOBJ.setCurrentLineItemValue('line', 'department', i_practice);
					o_journalOBJ.setCurrentLineItemValue('line', 'class', i_vertical);

					o_journalOBJ.setCurrentLineItemValue('line', 'custcolprj_name', project_description);//custcol_temp_project
					// Added by Sitaram
					nlapiLogExecution('DEBUG', 'parseInt(i_subsidiary)', parseInt(i_subsidiary));
					nlapiLogExecution('DEBUG', 'vendorList[parseInt(i_subsidiary)]', vendorList[parseInt(i_subsidiary)]);

					//---------------------------------------------------------------
					//var i_project_fld=i_project_name.split('');				
					o_journalOBJ.setCurrentLineItemValue('line', 'custcol_project_name', project_name);//set project name
					o_journalOBJ.setCurrentLineItemValue('line', 'custcol_sow_project', i_project);////set project if
					//---------------------------------------------------------------

					if (isNaN(i_customer)) {
					}
					else {
						o_journalOBJ.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', customer_name);
					}
					o_journalOBJ.commitLineItem('line');

					var i_usage_end = i_context.getRemainingUsage();
					nlapiLogExecution('DEBUG', 'expense_report_details', 'Usage End [' + i + '] -->' + i_usage_end);

					if (sch_count == 250)//if (i_usage_end < 250) 
					{
						flag = 1;
						break;
					}
					else {
						schedule_count = sch_count;
					}
				}//for close

				//-------------------------------------------------------------------
				o_journalOBJ.selectNewLineItem('line');
				o_journalOBJ.setCurrentLineItemValue('line', 'account', i_unbilled_receivable_GL);
				o_journalOBJ.setCurrentLineItemValue('line', 'debit', tot_credit);
				// Added by Sitaram
				o_journalOBJ.commitLineItem('line');
				//-------------------------------------------------------------------
				nlapiLogExecution('DEBUG', 'a_ER_ID_array', a_ER_ID_array);
				o_journalOBJ.setFieldValue('custbody_expense_report_id', a_ER_ID_array);
				o_journalOBJ.setFieldValue('custbody_approvalstatusonje', 2);
				o_journalOBJ.setFieldValue('approved', 'T');
				o_journalOBJ.setFieldValue('custbody_approved_by_provision', 'T');

				var i_journal_entry_SubmitID = nlapiSubmitRecord(o_journalOBJ, true, true)
				nlapiLogExecution('DEBUG', ' create_journal_entry ', ' *********** Journal Entry Submit ID  *********** -->' + i_journal_entry_SubmitID);

				if (_logValidation(i_journal_entry_SubmitID)) {
					a_ER_ID_array = removearrayduplicate(a_ER_ID_array);


					if (a_ER_ID_array.length == 1) {
						o_time_expense_recOBJ.setFieldValue('custrecord_expense_report_pr', a_ER_ID_array);
					}
					else {
						o_time_expense_recOBJ.setFieldValues('custrecord_expense_report_pr', a_ER_ID_array);
					}
					o_time_expense_recOBJ.setFieldValue('custrecord_journal_entry_reverse_date', d_reverse_date);
					o_time_expense_recOBJ.setFieldValue('custrecord_journalentry', i_journal_entry_SubmitID);

					var i_time_expense_submitID = nlapiSubmitRecord(o_time_expense_recOBJ, true, true)
					nlapiLogExecution('DEBUG', ' update_for_provision_created_ER ', ' *********** Time Expense Record Submit ID  *********** -->' + i_time_expense_submitID);

					var fun_status = update_for_provision_created_ER(a_ER_ID_array)
					nlapiLogExecution('DEBUG', 'fun_status', 'fun_status' + fun_status);

					nlapiLogExecution('DEBUG', 'first time_tracking_details', 'Usage End -->' + i_usage_end);

					count = ss + 1;

					if (flag == '1') {
						curr_count = i;
					}
					else {
						curr_count = parseInt(i) + 1;
					}

					if (flag == '0') {
						count = 0;
					}

					nlapiLogExecution('DEBUG', 'curr_count', ' ******************i***************** ' + curr_count);
					nlapiLogExecution('DEBUG', 'count', ' ******************i***************** ' + count);
					var params = new Array();//i_data_process
					//params['custscript_jvid']=jv_id;
					params['custscript_time_counter'] = count;
					params['custscript_time_counter_curr'] = curr_count;
					params['custscript_current_date_sch'] = d_current_date
					params['custscript_unbilled_type_sch'] = i_unbilled_type
					params['custscript_unbilled_receivable_sch'] = i_unbilled_receivable_GL
					params['custscript_unbilled_revenue_sch'] = i_unbilled_revenue_GL
					params['custscript_reverse_date_sch'] = d_reverse_date
					params['custscript_data_array'] = a_data_array
					params['custscript_data_process_provision'] = i_data_process
					params['custscript_custom_id_process'] = i_custom_id
					params['custscript_data_criteria'] = a_data_criteria_array
					params['custscript_criteria_type_sch'] = i_criteria

					//------------------------------------------------------------
					var status = nlapiScheduleScript('customscriptsch_provision_unbilled_journ', 'customdeploy1', params);
					nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_TR', ' Script Scheduled Status -->' + status);

					////If script is scheduled then successfuly then check for if status=queued
					if (status == 'QUEUED') {
						nlapiLogExecution('DEBUG', 'schedule_script_after_usage_exceeded_TR', 'Script rescheduled for ' + ss);
						break;
					}
				}
				//-------------------------------------------------------------------
			}//else close
		}//Data Array Loop
	}//Data Array		
}//Expense Report
function get_employee_subsidiary_TR(i_employee) {
	var i_subsidiary;

	if (_logValidation(i_employee)) {
		var o_employeeOBJ = nlapiLoadRecord('employee', i_employee)

		if (_logValidation(o_employeeOBJ)) {
			i_subsidiary = o_employeeOBJ.getFieldValue('subsidiary')
		}//Employee OBJ			
	}//Employee	
	return i_subsidiary;
}
function get_employee_subsidiary_ER(i_employee) {
	var i_subsidiary;

	if (_logValidation(i_employee)) {
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employee);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('subsidiary');

		var a_search_results = nlapiSearchRecord('employee', null, filter, columns);

		if (_logValidation(a_search_results)) {
			i_subsidiary = a_search_results[0].getValue('subsidiary');
		}//Search Results	
	}//Employee
	return i_subsidiary;
}

function update_for_provision_created_ER(a_ER_ID_array) {
	if (_logValidation(a_ER_ID_array)) {
		for (var t = 0; t < a_ER_ID_array.length; t++) {
			var o_expenseOBJ = nlapiLoadRecord('expensereport', a_ER_ID_array[t])

			if (_logValidation(o_expenseOBJ)) {
				o_expenseOBJ.setFieldValue('custbody_is_provision_created', 'T')

				var i_expense_submitID = nlapiSubmitRecord(o_expenseOBJ, true, true)
				nlapiLogExecution('DEBUG', ' update_for_provision_created_ER ', ' *********** Expense Report Submit ID  *********** -->' + i_expense_submitID);

			}//Expense OBJ	
		}//Expense ID
	}
}
function update_for_provision_created_TR(a_TR_ID_array) {
	nlapiLogExecution('DEBUG', ' update_for_provision_created_TR ', ' *********** Time Track ID Array  *********** -->' + a_TR_ID_array);

	if (_logValidation(a_TR_ID_array)) {
		nlapiLogExecution('DEBUG', ' update_for_provision_created_TR ', ' *********** Time Track ID Array Length *********** -->' + a_TR_ID_array.length);

		for (var t = 0; t < a_TR_ID_array.length; t++) {
			var o_timeOBJ = nlapiLoadRecord('timebill', a_TR_ID_array[t])

			if (_logValidation(o_timeOBJ)) {
				o_timeOBJ.setFieldValue('custcol_is_provision_created_tr', 'T')

				var i_time_submitID = nlapiSubmitRecord(o_timeOBJ, true, true)
				nlapiLogExecution('DEBUG', ' update_for_provision_created_TR ', ' *********** Time Track Submit ID  *********** -->' + i_time_submitID);

			}//Time OBJ		
		}//Loop	    	
	}//Time ID	
}//TR Provision Created
//---------------------------------------------------------------------
function update_for_provision_created_TR_3(a_TR_ID_array, i_journal_entry_SubmitID, d_reverse_date) {
	if (_logValidation(a_TR_ID_array)) {
		nlapiLogExecution('DEBUG', ' update_for_provision_created_TR ', ' *********** Time Track ID Array Length *********** -->' + a_TR_ID_array.length);

		for (var t = 0; t < a_TR_ID_array.length; t++) {
			var o_timeOBJ = nlapiLoadRecord('customrecord_time_bill_rec', a_TR_ID_array[t])

			if (_logValidation(o_timeOBJ)) {
				o_timeOBJ.setFieldValue('custrecord_processed', 'T')
				o_timeOBJ.setFieldValue('custrecord_journal_entry_ref', i_journal_entry_SubmitID);
				o_timeOBJ.setFieldValue('custrecordreverse_date_fld', d_reverse_date);


				var i_time_submitID = nlapiSubmitRecord(o_timeOBJ, true, true)
				//nlapiLogExecution('DEBUG', ' update_for_provision_created_TR ', ' *********** Time Track Submit ID  *********** -->' + i_time_submitID);

			}//Time OBJ		
		}//Loop	    	
	}//Time ID	
}
//---------------------------------------------------------------------
function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}

function get_project_name(i_project) {
	var i_project_name = '';
	if (_logValidation(i_project)) {
		var o_projectOBJ = nlapiLoadRecord('job', i_project);

		if (_logValidation(o_projectOBJ)) {
			i_project = o_projectOBJ.getFieldValue('companyname');
			i_entity_id = o_projectOBJ.getFieldValue('entityid');
			var i_project_name = i_entity_id + ' ' + i_project;
		}//Project OBJ 	
	}//Project
	return i_project_name + '##' + i_entity_id;
}//Project

// END FUNCTION =====================================================

function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on Provision Unbilled JournalEntries ';
        
        var s_Body = 'This is to inform that Provision Unbilled JournalEntries is having an issue and System is not able to send the details.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​

