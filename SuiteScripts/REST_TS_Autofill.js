/**
 * Based on the allocations, fill in the employees timesheet for the provided
 * week and return the internal id of the new timesheet
 * 
 * Version Date Author Remarks 1.00 Apr 21 2015 Nitish Mishra
 * Version Date Author Remarks 1.00 Mar 17 2020 Praveena Madem	Changed sublist ids of timesheet and time tracking fields array
 */

function suitelet(request, response) {

	nlapiLogExecution('debug', 'start', new Date());
	var result = null;
	try {
		var startDate = request.getParameter("startdate");
		var endDate = nlapiDateToString(nlapiAddDays(
				nlapiStringToDate(startDate), 6), 'date');
		var employee = request.getParameter("employee");

		// get active allocations, hours ,etc.
		var allocationDetails = getActiveAllocations(startDate, endDate,
				employee);
		nlapiLogExecution('ERROR', 'allocationDetails', JSON.stringify(allocationDetails));		
		// create an open timesheet
		var timesheetId = autoGenerateTimesheet(startDate, employee,
				allocationDetails);
        nlapiLogExecution('ERROR', 'timesheetId', timesheetId);		
		result = {
			Status : true,
			Message : timesheetId
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		result = {
			Status : false,
			Message : err
		};
	}
	nlapiLogExecution('debug', 'end', new Date());
	response.write(JSON.stringify(result));
}

// create a new timesheet record using the details retrieved
function autoGenerateTimesheet(weekStartDate, employee, allocationDetails) {

	try {
		// create a new timesheet record
		var newTimesheet = nlapiCreateRecord('timesheet', {
			recordmode : 'dynamic'
		});

		
var daysOfWeek = [ 'timebill0', 'timebill1', 'timebill2', 'timebill3','timebill4', 'timebill5', 'timebill6' ];//Changed by praveena due to migration instaed of week days


		// var employeeDetails = nlapiLookupField('employee', employee, [
		// 'subsidiary', 'location'
		// ]);

		// set the main fields
		newTimesheet.setFieldValue('approvalstatus', '1');
		// newTimesheet.setFieldValue('customform', '');
		newTimesheet.setFieldValue('employee', employee);
		newTimesheet.setFieldValue('startdate', weekStartDate);
		// newTimesheet.setFieldValue('enddate', '');
		newTimesheet.setFieldValue('iscomplete', 'F');
		// newTimesheet.setFieldValue('subsidiary', employeeDetails.subsidiary);
		// newTimesheet.setFieldValue('totalhours', 40);

		// nlapiLogExecution('debug', 'allocationDetails',
		// JSON.stringify(allocationDetails));

      
      var d_weekStartDate=nlapiStringToDate(weekStartDate);
      var i_start_week=d_weekStartDate.getDay();
		// fill in the timesheet hours
		allocationDetails.forEach(function(project) {
			//Logic Added by Praveena 0n 19-03-2020 due to migration changes 
			newTimesheet.selectNewLineItem('timeitem');
			newTimesheet.setCurrentLineItemValue('timeitem','customer',project.Project);
			newTimesheet.setCurrentLineItemValue('timeitem','casetaskevent',project.Task);
			for (var i = 1; i < 6; i++) {
				//if (isNumber(project.Hours[i])) {
			if(_logValidation(project.Hours[i])){
              if(i_start_week==1)
                {
                  newTimesheet.setCurrentLineItemValue('timeitem','hours'+Number(i-1),project.Hours[i]);
                  newTimesheet.setCurrentLineItemValue('timeitem','rate',project.BillRate);
                  
                }
			else
              {
                newTimesheet.setCurrentLineItemValue('timeitem','hours'+i,project.Hours[i]);
                newTimesheet.setCurrentLineItemValue('timeitem','rate',project.BillRate);
              }
              
					}
			}
			newTimesheet.commitLineItem('timeitem');
			
			//Commented by Praveena 0n 19-03-2020 due to migration changes 
			/*
			for (var i = 1; i < 6; i++) {

				if (isNumber(project.Hours[i])) {
				   
					newTimesheet.setCurrentLineItemValue('timeitem','hours'+i,project.Hours[i]);
					
					/*
					// nlapiLogExecution('debug', 'project.Hours[i]',
					// project.Hours[i]);
					
					var newEntry = newTimesheet.createCurrentLineItemSubrecord(
							'timeitem', daysOfWeek[i]);
					// newEntry.setFieldValue('subsidiary',
					// employeeDetails.subsidiary);
					newEntry.setFieldValue('employee', employee);
					newEntry.setFieldValue('customer', project.Project);
					// newEntry.setFieldValue('location',
					// employeeDetails.location);
					// newEntry.setFieldText('projecttaskassignment',
					// project.Event);
					newEntry.setFieldValue('casetaskevent', project.Task);
					// newEntry.setFieldValue('isbillable', 'T');
					newEntry.setFieldValue('hours', project.Hours[i]);
					newEntry.commit();
					*/
			/*		
				}
				
				newTimesheet.commitLineItem('timeitem');
			}
			*/
		});

		var timesheetId =nlapiSubmitRecord(newTimesheet,{disabletriggers : true, enablesourcing : false}); //nlapiSubmitRecord(newTimesheet);
		return timesheetId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'autoGenerateTimesheet', err);
		throw err;
	}
}

function isNumber(n) {

	return typeof n == 'number' && !isNaN(n) && isFinite(n);
}

function getActiveAllocations(weekStartDate, weekEndDate, employee) {

	try {
		
		var hours = "";
		
		// get active allocations
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
				new nlobjSearchFilter('startdate', null, 'notafter',
						weekEndDate),
				new nlobjSearchFilter('enddate', null, 'notbefore',
						weekStartDate),
				new nlobjSearchFilter('resource', null, 'anyof', employee) ], [
				new nlobjSearchColumn('project'),
				new nlobjSearchColumn('startdate'),
				new nlobjSearchColumn('jobtype', 'job'),
				new nlobjSearchColumn('jobbillingtype', 'job'),
				new nlobjSearchColumn('formulanumeric')
						.setFormula('{percentoftime}'),
				new nlobjSearchColumn('enddate'),
				new nlobjSearchColumn('percentoftime'),
				new nlobjSearchColumn('subsidiary','employee'),
				new nlobjSearchColumn('custevent1'),
                new nlobjSearchColumn('custevent3')
                         
        ]);
          //Added by Sitaram

		if (isNotEmpty(allocationSearch)) {
			var weeks = [];
			var projects = [];
			var projectList = [];
			var activityNames = [];

			for (var j = 0; j < 7; j++) {
				weeks[j] = [];
				var currentDate = nlapiAddDays(
						nlapiStringToDate(weekStartDate), j);

				for (var i = 0; i < allocationSearch.length; i++) {
                  
                    var billRate = allocationSearch[i].getValue('custevent3');
                    nlapiLogExecution('ERROR', 'billRate Value ', billRate);
					var allocationStart = nlapiStringToDate(allocationSearch[i]
							.getValue('startdate'));
					var allocationEnd = nlapiStringToDate(allocationSearch[i]
							.getValue('enddate'));
                  
                   nlapiLogExecution('ERROR', 'allocationStart ', allocationStart);
					var project = allocationSearch[i].getValue('project');
					var projectName = allocationSearch[i].getText('project');
					var event = allocationSearch[i].getValue('custevent1');
					var projectType = allocationSearch[i].getValue('jobtype',
							'job');
					var billingType = allocationSearch[i].getValue(
							'jobbillingtype', 'job');
					var serviceItem = null;
					var percent = allocationSearch[i]
							.getValue('formulanumeric');
					var empSubsidiary  = allocationSearch[i].getValue(
							'subsidiary', 'employee');
					var proj_lookUp = nlapiLookupField('job',parseInt(project),
						['custentity_hoursperday','custentity_hoursperweek','custentity_onsite_hours_per_day','custentity_onsite_hours_per_week']);			
					
					if(empSubsidiary == parseInt(3))
					{
						var hours_day = proj_lookUp.custentity_hoursperday;
						if(_logValidation(hours_day))
						{
							hours = percent * hours_day;
						}
						else
						{
							hours = percent * 8
						}

					}
					else
					{
						var hours_day = proj_lookUp.custentity_onsite_hours_per_day;
						if(_logValidation(hours_day))
						{
							hours = percent * hours_day;
						}
						else
						{
							hours = percent * 8
						}
					}
					//hours = percent * 8;
					hours = parseFloat(hours.toFixed(2));
					// nlapiLogExecution('debug', 'Hours', hours);

					activityNames = [ "Project Activites", "Project activites",
							"Project activities", "project activities",
							"Bench Task", "Project Activities-Offsite",
							"Project Activities-Onsite", "Operations",
							"Project Activities", "Project activity",
							"project activity", "Project Activity" ];

					// check project type to get the project activites name
					// will be used later to assign task
					// internal project
					/*
					 * if (projectType == '1') {
					 * 
					 * 
					 * serviceItem = '2426'; // Internal Projects } else { //
					 * external project
					 * 
					 * if (billingType == 'TM') { // T&M activityNames = [
					 * "Operations", "Project Activities", "Project activity" ];
					 * serviceItem = '2222'; // ST } else { // FBI & FBM
					 * activityNames = [ "Operations", "Project Activities",
					 * "Project activity" ]; serviceItem = '2221';// FP } }
					 */

					// create an array for days and hours to fill
					if (currentDate >= allocationStart
							&& currentDate <= allocationEnd) {
						weeks[j].push(project);

						var index = _.indexOf(projectList, project);

						if (index == -1) {
							projectList.push(project);
							projects.push({
								Project : project,
								Task : '',
                                BillRate : billRate,
								Event : serviceItem,
								// Days : [

								// ],
								Hours : [

								]
							});
							index = projects.length - 1;
						}

						// var date1 = currentDate);
						var day = currentDate.getDay();
						// projects[index].Days[day] = currentDate;
						projects[index].Hours[day] =hours;
					}
				}
			}

			// nlapiLogExecution('debug', 'weeks A', JSON.stringify(weeks));
			// nlapiLogExecution('debug', 'projects A',
			// JSON.stringify(projects));
			// nlapiLogExecution('debug', 'checkpoint', 1);
			// nlapiLogExecution('debug', 'activityNames',
			// JSON.stringify(activityNames));
			// nlapiLogExecution('debug', 'projects B',
			// JSON.stringify(projects));
			// nlapiLogExecution('debug', 'checkpoint', 2);
			// nlapiLogExecution('debug', 'activityNames',
			// JSON.stringify(activityNames));

			// creating a filter for project tasks
			var filter = [];
			var addOr = false;
			activityNames.forEach(function(name) {

				if (addOr) {
					filter.push('or');
				}

				filter.push([ 'title', 'is', name ]);

				addOr = true;
			});

			// nlapiLogExecution('debug', 'filter', JSON.stringify(filter));

			// searching the project task records for project activities
			var taskSearch = nlapiSearchRecord('projecttask', null, [ filter,
					'and', [ 'company', 'anyof', projectList ] ],
					[ new nlobjSearchColumn('company') ]);

			if (!taskSearch) {
				throw "Project Task not found";
			}

			taskSearch.forEach(function(task) {

				for (var i = 0; i < projects.length; i++) {

					if (projects[i].Project == task.getValue('company')) {
						projects[i].Task = task.getId();
					}
				}
			});

			 nlapiLogExecution('debug', 'projects', JSON.stringify(projects));
		} else {
			throw "No active allocation";
		}

		return projects;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveAllocations', err);
		throw err;
	}
}

//validate blank entries
function _logValidation(value) 
{
if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
}
else 
 { 
  return false;
}
}