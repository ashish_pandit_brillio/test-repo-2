// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:CLI_HourlyReport.js
     Author:Rujuta K
     Company:
     Date:
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================


// END PAGE INIT ====================================================

function pageInit(type){

}

function fxn_generateHourPDF(param){
   // alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscriptsut_report_hourlyreportfromt', 'customdeploysut_report_hourlyreportfromt', false);
    
    //pass the internal id of the current record
    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
    // alert(createPDFURL1);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL1);
}

function fxn_generateHourPDF1(param){
   // alert("param" + param)
    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscript_report_hourlyreportfromtnew', 'customdeploy1', false);
    
    //pass the internal id of the current record
    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
    // alert(createPDFURL1);
    
    //show the PDF file 
    newWindow = window.open(createPDFURL1);
}

function fxn_generateHourPDF_Dec(param){
	   // alert("param" + param)
	    //<em class='rn_Highlight'>call</em> the <em class='rn_Highlight'>Suitelet</em> created in Step 1
	    var createPDFURL1 = nlapiResolveURL("SUITELET", 'customscript_report_hourlyreportfromte', 'customdeploy1', false);
	    
	    //pass the internal id of the current record
	    createPDFURL1 += '&id=' + 'Export' + "&param=" + param;
	    // alert(createPDFURL1);
	    
	    //show the PDF file 
	    newWindow = window.open(createPDFURL1);
	}
function fieldChanged(type, name, linenum){


    if (name == 'custpage_startdate') {
        var s_period = nlapiGetFieldValue('custpage_startdate')
        //alert("S_period=======" + s_period)
        if (s_period != null && s_period != '' && s_period != 'undefined') {
            var date = nlapiStringToDate(s_period)
            //alert("date=======" + date)
            if (date != null && date != '' && date != 'undefined') {
                var day = date.getDay()
                //alert("day=======" + day)
                if (day != 6) {
                    alert("Please select enddate as Saturday.")
                    nlapiSetFieldValue('custpage_startdate', '')
                }
            }
        }
        
    }
 
}







// BEGIN SAVE RECORD ================================================
// END FIELD CHANGED ================================================

