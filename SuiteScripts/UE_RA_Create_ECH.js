/**
 * 
 * Create "Employee Cumulative Hours" record whenever a new allocation is
 * created for FEHL
 * 
 * Version Date Author Remarks 1.00 31 Mar 2016 Nitish Mishra
 * 
 */

function userEventAfterSubmit(type) {
	try {
		if (type == 'create') {
			var project = nlapiGetFieldValue('project');
			var employee = nlapiGetFieldValue('allocationresource');

			if (nlapiLookupField('job', project, 'customer') == '4297') {

				// check if ECH already exists
				var search = nlapiSearchRecord(
				        'customrecord_employee_cumulative_hour', null, [
				                new nlobjSearchFilter('custrecord_ech_project',
				                        null, 'anyof', project),
				                new nlobjSearchFilter(
				                        'custrecord_ech_employee', null,
				                        'anyof', employee),
				                new nlobjSearchFilter('isinactive', null, 'is',
				                        'F') ]);

				// inactivate all if exists
				if (search) {

					for (var i = 0; i < search.length; i++) {
						nlapiSubmitField(
						        'customrecord_employee_cumulative_hour',
						        search[i].getId(), 'isinactive', 'T');
						nlapiLogExecution('DEBUG', 'ECH Record Inactivated',
						        'Project : ' + project);
					}
				}

				// create a new ECH record
				var rec = nlapiCreateRecord('customrecord_employee_cumulative_hour');
				rec.setFieldValue('custrecord_ech_project',
				        nlapiGetFieldValue('project'));
				rec.setFieldValue('custrecord_ech_employee',
				        nlapiGetFieldValue('allocationresource'));
				rec.setFieldValue('custrecord_ech_cumulative_hours', 0);
				rec.setFieldValue('custrecord_ech_last_invoiced_hours', 0);
				nlapiSubmitRecord(rec);
				nlapiLogExecution('DEBUG', 'ECH Record Created', 'Project : '
				        + project);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventAfterSubmit', err);
	}
}
