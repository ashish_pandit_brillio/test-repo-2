function beforeSubmitRecord(type)
{
	if(type == "edit"){
		//var recObj = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var externalHire = nlapiGetFieldValue("custrecord_frf_details_external_hire");
		var taleoExtracted = nlapiGetFieldValue("custrecord_frf_details_talep_created");
		var oldRec = nlapiGetOldRecord();
		var oldexternalHire = oldRec.getFieldValue("custrecord_frf_details_external_hire"); 	
		if(externalHire == "T" && taleoExtracted == "T" && oldexternalHire == "F"){
			throw "It is not allowed to make this FRF as external";
		}	
	}	
}