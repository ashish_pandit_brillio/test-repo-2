/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Dec 2016     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
   try{
	   if(type == 'create' || type == 'edit'){
		   alert('“Expense Entry” feature is now live on the Brillio On The Go app !  You can enter your expense within seconds, scan your bills live and submit at the end of your trip.  It is as simple as that!');
	   }
	   	var main_div = document.getElementsByClassName("uir-page-title")[0];
		var newDiv = document.createElement("div");
		newDiv.innerHTML = "<span style='color: black;font-size: 14px;float: left;border: 1px solid;padding: 5px;background-color: beige;'><b>Expense Entry feature is now live on the Brillio On The Go app !  You can enter your expense within seconds, scan your bills live and submit at the end of your trip.  It is as simple as that! </b></span>";
		main_div.appendChild(newDiv);
	   
   }
   catch(e){
		nlapiLogExecution('ERROR','Process Error',e);
	}
}

function fieldChange_Project_type(type, name, linenum)
{
try{
	//if (name == 'custpage_startdate') {
	
	//}
	var subsidiary = nlapiGetFieldValue('subsidiary');
	//alert('subsidiary:'+subsidiary);
	if(name == 'customer')  
	{			
		var i_project_interal_id = nlapiGetCurrentLineItemValue('expense', 'customer');
		
		var response = nlapiRequestURL(nlapiResolveURL('SUITELET', 'customscript_sut_exp_validate_proj', 'customdeploy_sut_exp_validate_proj') + '&i_project_interal_id='+i_project_interal_id, null, null);
		var res = response.getBody();
		//alert('res:' +res);
		//var project_lookUp = nlapiLookupField('job',parseInt(i_project_interal_id),['entitystatus']);
	//	var project_status = project_lookUp.entitystatus;
		if(res == 'false'){
			alert('Selected project is closed. Request to select only active project !!');
			nlapiSetCurrentLineItemValue('expense','customer','');
			return false;
		}
	}
	if(subsidiary)
    {
	if(name == 'custcolexpense_emp_category')  
	{			
		var i_exp_category = nlapiGetCurrentLineItemValue('expense', 'custcolexpense_emp_category');
		
	//	alert('i_exp_category:'+i_exp_category);
		if(i_exp_category){
			//alert('Selected project is closed. Request to select only active project !!');
			nlapiSetCurrentLineItemValue('expense','category',i_exp_category);
			return true;
		}
	}
	}
  
	return true;
}	
catch(e){
	nlapiLogExecution('ERROR','Process Error',e);
}
}

function validateLine(type){
	try{
		var i_subsidiary = nlapiGetFieldValue('subsidiary');
		if(parseInt(i_subsidiary) == parseInt(10) && nlapiGetCurrentLineItemValue('expense','taxcode') == ''){
		nlapiSetCurrentLineItemValue('expense','taxcode',2905);
		//return true;
		}
		var i_expense_category = nlapiGetCurrentLineItemValue('expense','category');
		var i_project = nlapiGetCurrentLineItemValue('expense','customer');
		if((parseInt(i_expense_category) == parseInt(70) || parseInt(i_expense_category) == parseInt(48)) && parseInt(i_project) != parseInt(6960)){
		alert('Please select L&D project for certification claims !!');
		nlapiSetCurrentLineItemValue('expense','customer','');
		}
		return true;	
	}
	catch(e){
	nlapiLogExecution('ERROR','Process Error',e);
}
}

function onSave(){
	try{
		var lineCount =  nlapiGetLineItemCount('expense');
		for(var i=1;i<=lineCount;i++){
			var i_expense_category = nlapiGetLineItemValue('expense','category',i);
			var i_project = nlapiGetLineItemValue('expense','customer',i);
		if((parseInt(i_expense_category) == parseInt(70) || parseInt(i_expense_category) == parseInt(48)) && parseInt(i_project) != parseInt(6960)){
		alert('Please select L&D project for certification claims at line '+i);
		nlapiSetLineItemValue('expense','customer',i,'');
		}
		}
		return true;	
	}
	catch(e){
	nlapiLogExecution('ERROR','Process Error',e);
}
}