/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 13 Jul 2015 nitish.mishra
 * 
 */

function beforeLoad(type, form) {
	try {

		// default all the custom field values
		if (type == 'create' || type == 'copy') {
			nlapiSetFieldValue('custbody_inv_tenure_discount', 0);
			nlapiSetFieldValue('custbody_inv_last_invoiced_hours', 0);
			nlapiSetFieldValue('custbody_inv_last_sales_addition', 0);
			nlapiSetFieldValue('custbody_inv_cdd_temp_field', '');
		}

		// remove all the line items
		if (type == 'copy') {
			nlapiSetFieldValue('custbody_layout_type', '');
			var lineCount = nlapiGetLineItemCount('item');
			for (var linenum = lineCount; linenum >= 1; linenum--) {
				nlapiRemoveLineItem('item', linenum);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'beforeLoad', err);
	}
}

function beforeSubmit(type) {
	try {

		if (type == 'create' || type == 'copy') {
			var customer = nlapiGetFieldValue('entity');

			if (customer == '4297') {

				if (nlapiGetFieldValue('custbody_inv_is_discount_added') == 'T') {
					nlapiSetFieldValue('custbody_layout_type', '14');
				}
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'afterSubmit', err);
	}
}

function afterSubmit(type) {
	try {
		var customerDiscountRecord = nlapiGetFieldValue('custbody_inv_cdd_temp_field');

		if (customerDiscountRecord) {
			var recordId = nlapiGetRecordId();
			nlapiSubmitField('customrecord_customer_discount_details',
			        customerDiscountRecord, 'custrecord_cdd_invoice', recordId);
			nlapiLogExecution('debug', 'Customer Discount Record Updated',
			        customerDiscountRecord);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'afterSubmit', err);
	}
}