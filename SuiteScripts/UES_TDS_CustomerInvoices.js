// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:  UES TDS CustomerInvoices
	Author:       Supriya J
	Company:      Aashna cloudtech Pvt Ltd.
	Date:         18 feb 2014
	Version:      0.1
	Description:  User will edit the Invoice and check the TDS check box and select the TDS % from the list and save the Invoice.
	              On submit button Credit memo will be created at backend and reference for the credit memo is display on the invoice.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
    22 sep 2014            Supriya                         Kalpana                       Normal Account Validation
	20 july 2015           Nikhil                          satish                        Add a tds round up functionality
	30 July 2015           Nikhil                          sachin k                      add a reneweal process
Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY
    if (type != 'delete') 
	{
	
		var currentContext = nlapiGetContext();
		
		// IF EXECUTION CONTEXT IS WORKFLOW ====
		if (currentContext.getExecutionContext() != 'suitelet') 
		{
			var a_subisidiary = new Array();
			var s_pass_code;
			var i_invoiceId = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_invoiceId->' + i_invoiceId)
			
			if (i_invoiceId != null && i_invoiceId != undefined && i_invoiceId != '') 
			{
				var o_invoiceRec = nlapiLoadRecord('invoice', i_invoiceId)
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_invoiceId->' + i_invoiceId)
				
				var i_subsidiary = o_invoiceRec.getFieldValue('subsidiary')
				nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_subsidiary->' + i_subsidiary)
				
				
				//==== CALL A GLOBAL PARAMETER FUNCTION ======
				var i_AitGlobalRecId = SearchGlobalParameter();
				nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
				
				//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
				if (i_AitGlobalRecId != 0) 
				{
					var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
					
					var a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
					nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
					
					var s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
					nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
					
					var tdsRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_tds_roundoff');
					nlapiLogExecution('DEBUG', 'Bill ', "tdsRoundMethod->" + tdsRoundMethod);
					
					var Flag = 0;
					
					Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary)
					/*
				 if(a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '' )
				 {
				 for(var y=0 ;y<a_subisidiary.length;y++)
				 {
				 
				 //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
				 if (a_subisidiary[y] == i_subsidiary)
				 {
				 Flag = 1;
				 break;
				 }// END if (a_subisidiary[y] == i_subsidiary)
				 }
				 }
				 else
				 {
				 
				 //==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
				 if (a_subisidiary[0] == i_subsidiary)
				 {
				 Flag = 1;
				 // break;
				 }// END if (a_subisidiary[0] == i_subsidiary)
				 }
				 
				 //===== FOR SINGLE SUBSIDARY ACCOUNT  =======
				 var context = nlapiGetContext();
				 var i_subcontext = context.getFeature('SUBSIDIARIES');
				 if(i_subcontext == false)
				 {
				 Flag = 1;
				 }
				 */
					if (Flag == 1) 
					{
					
						// == CODE TO GET THE VALUES FROM THE INVOICE ==
						var i_invoicenumber = o_invoiceRec.getFieldValue('tranid')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_invoicenumber->' + i_invoicenumber)
						
						var i_customer = o_invoiceRec.getFieldValue('entity')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_customer->' + i_customer)
						
						var i_location = o_invoiceRec.getFieldValue('location')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_location->' + i_location)
						
						var i_ApplyTds = o_invoiceRec.getFieldValue('custbody_apply_tds')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_location->' + i_location)
						
						var i_ApplyTdsTypeMasterId = o_invoiceRec.getFieldValue('custbody_tds_rate')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_ApplyTdsTypeMasterId->' + i_ApplyTdsTypeMasterId)
						
						var i_InvoicedAmount = o_invoiceRec.getFieldValue('subtotal')//total
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_InvoicedAmount->' + i_InvoicedAmount)
						
						var i_CreatedCreditMemo = o_invoiceRec.getFieldValue('custbody_tds_creditmemoreference')
						nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_CreatedCreditMemo->' + i_CreatedCreditMemo)
						
						var i_department = o_invoiceRec.getFieldValue('department')
						var i_location = o_invoiceRec.getFieldValue('location')
						var i_class = o_invoiceRec.getFieldValue('class')
						
						
						// == CODE TO GET THE VALUES FROM THE INDIAN GLOBAL PARAMETER CUSTOM RECORD ==
						
						var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
						nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_vatCode->" + i_vatCode);
						
						if (i_ApplyTds == 'T') 
						{
						
							// == CODE TO LOAD TDS MASTER RECORD ==
							if (i_ApplyTdsTypeMasterId != null && i_ApplyTdsTypeMasterId != undefined && i_ApplyTdsTypeMasterId != '') {
								var o_TdsMasterRecord = nlapiLoadRecord('customrecord_tdsmaster', i_ApplyTdsTypeMasterId)
								
								var i_item = o_TdsMasterRecord.getFieldValue('custrecord_tdsrecievable_item');
								nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_item->" + i_item);
								
								var i_ApplyTdsRate = o_TdsMasterRecord.getFieldValue('custrecord_netper');
								nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_ApplyTdsRate->" + i_ApplyTdsRate);
								
								i_InvoicedAmount = validate_value(i_InvoicedAmount)
								i_ApplyTdsRate = validate_value(i_ApplyTdsRate)
								
								
								var i_InvoicedAmounttobeset = parseFloat(i_InvoicedAmount) * (parseFloat(i_ApplyTdsRate) / parseFloat(100))
								i_InvoicedAmounttobeset = i_InvoicedAmounttobeset.toFixed(2)
								
								
								//---------------------Begin function to round tds amount--------------------------//
								
								i_InvoicedAmounttobeset = applyTdsRoundMethod(tdsRoundMethod, i_InvoicedAmounttobeset)
								
								//---------------------End function to round tds amount--------------------------//
								
								i_InvoicedAmounttobeset = validate_value(i_InvoicedAmounttobeset)
								
								nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'i_InvoicedAmounttobeset->' + i_InvoicedAmounttobeset)
								
								// == CODE TO CREATE CREDIT MEMO RECORD ===
								
								try {
								
								
									if (i_CreatedCreditMemo != null && i_CreatedCreditMemo != 'undefined' && i_CreatedCreditMemo != '') {
									
										nlapiLogExecution('DEBUG', 'afterSubmitRecord', '************Inside Credit Memo Updation*************')
										var o_creditMemoRec = nlapiLoadRecord('creditmemo', i_CreatedCreditMemo)
										
										
										var i_ApplyLineCount = o_creditMemoRec.getLineItemCount('apply')
										nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_ApplyLineCount->" + i_ApplyLineCount);
										
										nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_InvoicedAmounttobeset->" + i_InvoicedAmounttobeset);
										
										o_creditMemoRec.setFieldValue('entity', i_customer)
										o_creditMemoRec.setFieldValue('location', i_location)
										
										o_creditMemoRec.setFieldValue('unapplied', 0)
										o_creditMemoRec.setFieldValue('applied', i_InvoicedAmounttobeset)
										
										
										
										// o_creditMemoRec.setLineItemValue('item','item',1,i_item)
										o_creditMemoRec.setLineItemValue('item', 'quantity', 1, 1)
										o_creditMemoRec.setLineItemValue('item', 'price', 1, parseFloat(-1))
										o_creditMemoRec.setLineItemValue('item', 'rate', 1, i_InvoicedAmounttobeset)
										o_creditMemoRec.setLineItemValue('item', 'amount', 1, i_InvoicedAmounttobeset)
										o_creditMemoRec.setLineItemValue('item', 'taxcode', 1, i_vatCode)
										o_creditMemoRec.setLineItemValue('item', 'department', 1, i_department)
										o_creditMemoRec.setLineItemValue('item', 'location', 1, i_location)
										
										for (var j = 1; j <= i_ApplyLineCount; j++) {
											var appliedinvoice = o_creditMemoRec.getLineItemValue('apply', 'refnum', j)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "appliedinvoice->" + appliedinvoice);
											nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_invoicenumber->" + i_invoicenumber);
											nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_InvoicedAmounttobeset->" + i_InvoicedAmounttobeset);
											if (i_invoicenumber == appliedinvoice) {
												o_creditMemoRec.setLineItemValue('apply', 'apply', j, 'T')
												o_creditMemoRec.setLineItemValue('apply', 'amount', j, i_InvoicedAmounttobeset)
												
											}
											
										}
										var i_createdCreditMemo = nlapiSubmitRecord(o_creditMemoRec, true, true)
										nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_createdCreditMemo->" + i_createdCreditMemo);
										
									}
									else {
									
										nlapiLogExecution('DEBUG', 'afterSubmitRecord', '************Inside New Credit Memo creation*************')
										var o_creditMemoRec = nlapiTransformRecord('invoice', i_invoiceId, 'creditmemo', {
											recordmode: 'dynamic'
										});
										
										var i_ItemlineCount = o_creditMemoRec.getLineItemCount('item')
										nlapiLogExecution('DEBUG', 'Update credit Memo ', 'i_ItemlineCount-->' + i_ItemlineCount)
										
										
										// === CODE TO REMOVE THE LINE ITEMS =====
										for (var k = i_ItemlineCount; k >= 1; k--) {
											{
												nlapiLogExecution('DEBUG', 'Update credit Memo ', 'IN IF Flag REMOVE-->')
												o_creditMemoRec.removeLineItem('item', k);
											}
										}
										// ==== CODE TO APPLY RESPECTIVE INVOICE ON THE CREDIT MEMO ==
										var i_ApplyLineCount = o_creditMemoRec.getLineItemCount('apply')
										nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_ApplyLineCount->" + i_ApplyLineCount);
										
										for (var j = 1; j <= i_ApplyLineCount; j++) {
											var appliedinvoice = o_creditMemoRec.getLineItemValue('apply', 'refnum', j)
											
											if (i_invoicenumber == appliedinvoice) {
												o_creditMemoRec.setLineItemValue('apply', 'apply', j, 'T')
												o_creditMemoRec.setLineItemValue('apply', 'amount', j, i_InvoicedAmounttobeset)
											}
											else {
												o_creditMemoRec.setLineItemValue('apply', 'apply', j, 'F')
											}
										}
										
										var i_LineCount = o_creditMemoRec.getLineItemCount('item')
										nlapiLogExecution('DEBUG', 'aftr remove ', "Item i_LineCount->" + i_LineCount);
										
										
										// ==== CODE TO SET THE LINE ITEM VALUES ==
										o_creditMemoRec.selectNewLineItem('item')
										o_creditMemoRec.setCurrentLineItemValue('item', 'item', i_item)
										o_creditMemoRec.setCurrentLineItemValue('item', 'quantity', 1)
										o_creditMemoRec.setCurrentLineItemValue('item', 'price', parseFloat(-1))
										o_creditMemoRec.setCurrentLineItemValue('item', 'rate', i_InvoicedAmounttobeset)
										o_creditMemoRec.setCurrentLineItemValue('item', 'amount', i_InvoicedAmounttobeset)
										o_creditMemoRec.setCurrentLineItemValue('item', 'taxcode', i_vatCode)
										o_creditMemoRec.setCurrentLineItemValue('item', 'department', i_department)
										o_creditMemoRec.setCurrentLineItemValue('item', 'location', i_location)
										o_creditMemoRec.setCurrentLineItemValue('item', 'class', i_class)
										o_creditMemoRec.commitLineItem('item')
										// ==== CODE TO APPLY RESPECTIVE INVOICE ON THE CREDIT MEMO ==
										
										
										
										var i_createdCreditMemo = nlapiSubmitRecord(o_creditMemoRec, true, true)
										nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_createdCreditMemo->" + i_createdCreditMemo);
										
										if (i_createdCreditMemo != null && i_createdCreditMemo != undefined && i_createdCreditMemo != '') {
										
											var o_invoiceRec1 = nlapiLoadRecord('invoice', i_invoiceId)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord', 'o_invoiceRec1->' + o_invoiceRec1)
											
											o_invoiceRec1.setFieldValue('custbody_tds_creditmemoreference', i_createdCreditMemo)
											
											var i_UpdatedInvoicerec = nlapiSubmitRecord(o_invoiceRec1, true, true)
											nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_UpdatedInvoicerec->" + i_UpdatedInvoicerec);
											
											
										}// END  if(i_createdCreditMemo!= null && i_createdCreditMemo != undefined && i_createdCreditMemo!= '')
									}// ELSE END
								} 
								catch (exception) {
									nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "Message->" + exception.getDetails());
									
								}// END   catch(exception)
							}// END if(i_ApplyTdsTypeMasterId != null && i_ApplyTdsTypeMasterId != undefined && i_ApplyTdsTypeMasterId != '')
						}// End if(i_ApplyTds == 'T')
					}// End if(Flag == 1)
				}// END if(i_AitGlobalRecId != 0 )
			}// if( i_invoiceId!= null && i_invoiceId != undefined && i_invoiceId != '')
		}
	}



	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
function SearchGlobalParameter()
{

	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;

    a_column.push(new nlobjSearchColumn('internalid'));

	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)

	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length; i++)
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);

		}

	}


	return i_globalRecId;
}
function validate_value(value)
	{
        var return_value;

        if (value == '' || value == null || value == 'undefined' || value.toString() == undefined || value == 'undefined' || value.toString() == 'undefined') {
            return_value = 0
        }
        else
		{
            return_value = value
        }

        return return_value;
    }

function applyTdsRoundMethod(tdsRoundMethod,tdsAmount)
{
	var roundedtdsAmount = tdsAmount;
	
	if(tdsRoundMethod == 2)
	{
		roundedtdsAmount = Math.round(tdsAmount)
	}
	if(tdsRoundMethod == 3)
	{
		roundedtdsAmount = Math.round(tdsAmount/10)*10;
	}
	if(tdsRoundMethod == 4)
	{
		roundedtdsAmount = Math.round(tdsAmount/100)*100;
	}
	
	return roundedtdsAmount;
}
}
// END FUNCTION =====================================================
