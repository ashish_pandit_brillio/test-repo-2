/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 Apr 17 2015 Nitish Mishra
 * 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *        type Operation types: create, edit, delete, xedit, approve, cancel,
 *        reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only)
 *        dropship, specialorder, orderitems (PO only) paybills (vendor
 *        payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	try {

		// check if the joining mail is to be send or not
		var sendJoiningMail = isTrue(nlapiGetFieldValue('custentity_emp_send_joining_email'));

		nlapiLogExecution('debug', 'type', type);
		nlapiLogExecution('debug', 'sendJoiningMail', sendJoiningMail);

		// if false, no need to send email
		if (sendJoiningMail) {

			// if type is create, send as per the field value
			if (type == 'create') {

			}
			// if edit, check if the value has been changed and is marked now
			else if (type == 'edit') {
				var oldRecord = nlapiGetOldRecord();

				if (isTrue(oldRecord.getFieldValue('custentity_emp_send_joining_email'))) {
					sendJoiningMail = false;
				}
			}

			nlapiLogExecution('debug', 'sendJoiningMail', sendJoiningMail);
			if (sendJoiningMail) {

				// get the person type
				var employeeId = nlapiGetRecordId();
				var mailContent = null;
				var employeeDetails = nlapiLookupField('employee', employeeId, [
					'firstname', 'email', 'custentity_persontype'
				]);

				// if email id contains "@brillio.com" send the salaried mail,
				// else send the contigent worker mail

				// get mail content as per the person type
				if (doesEmailContainsBrillio(employeeDetails.email)) {
					mailContent = getSalariedMailTemplate(employeeDetails);
				}
				else {
					mailContent = getContigentWorkerMailTemplate(employeeDetails);
				}

				// send email to the new joinee
				if (isNotEmpty(mailContent)) {

					nlapiSendEmail(constant.Mail_Author.InformationSystems, employeeDetails.email,
							mailContent.Subject, mailContent.Body, null, null, {
								entity : employeeId
							});
                                        sendNewJoineeEmail(employeeId);
				}
				else {
					throw "Invalid Person Type";
				}
			}
		}
	}
	catch (err) {
		nlapiLogExecution('error', 'userEventAfterSubmit', err);
		throw err;
	}
}

function doesEmailContainsBrillio(email) {

	email = email.toLowerCase();
	return email.indexOf("@brillio.com") > -1;
}

function getSalariedMailTemplate(employeeDetails) {

	var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';
	htmlText += '<p>Hi ' + employeeDetails.firstname + ',<p>';
	htmlText +=
			'<p>Your employee record has been created in NetSuite.</p>'
					+ '<p>Please follow the below the instructions to login to NetSuite.</p>'

					+ '<p>1. Login to Brillio SharePoint - <a href="https://brillioonline.sharepoint.com">https://brillioonline.sharepoint.com</a><br/>'
					+ '2. Proceed with windows credentials<br/>'
					+ '3. Click on <b>NetSuite</b> icon on the top-right corner of the screen.</p>'
					+ '<p>Login mail id : '
					+ "<a href='"
					+ employeeDetails.email
					+ "'>"
					+ employeeDetails.email
					+ "</a>"
					+ "<br/>"
					+ 'Password : Your outlook mail password</p>'
					+ '<p>If you have any queries, please inform  <a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a>.</p>'
					+ '<p>Regards,<br/>' + 'Information Systems</p>';

	return {
		Subject : 'Your account has been created in the NetSuite',
		Body : addMailTemplate(htmlText)
	};
}

function getContigentWorkerMailTemplate(employeeDetails) {

	var htmlText = '<style> p,li {font-size : 11; font-family : "verdana"}</style>';
	htmlText += '<p>Hi ' + employeeDetails.firstname + ',<p>';
	htmlText +=
			'<p>Your employee record has been created in NetSuite.</p>'
					+ '<p>Please follow the below the instructions to login to NetSuite.</p>'
					+ '<p>Login to NetSuite - <a href="https://system.na1.netsuite.com/pages/login.jsp">https://system.na1.netsuite.com/pages/login.jsp</a></p>'
					+ '<p>Login mail id : '
					+ "<a href='"
					+ employeeDetails.email
					+ "'>"
					+ employeeDetails.email
					+ "</a>"
					+ "<br/>"
					+ 'Password : Abcd123456</p>'
					+ '<p style="color:red">Note - You need to change your password after log in for the first time.</p>'
					+ '<p>If you have any queries, please inform  <a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a>.</p>'
					+ '<p>Regards,<br/>' + 'Information Systems</p>';


	return {
		Subject : 'Your account has been created in the NetSuite',
		Body : addMailTemplate(htmlText)
	};
}