function scheduled(type) {
	nlapiLogExecution('DEBUG', 'START');
	try {
		var context = nlapiGetContext();
		var provisionMasterId = getRunningProvisionMasterId();

clearErrorLogs(provisionMasterId);

		// delete existing entries related to this master
		var provisionEntrySearch = searchRecord('customrecord_provision_entry',
		        null, [ new nlobjSearchFilter('custrecord_pe_provision_master',
		                null, 'anyof', provisionMasterId) ]);

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'Deleting Old Provision Entries', '0%' ]);

		if (provisionEntrySearch) {
			var count = provisionEntrySearch.length;

			for (var i = 0; i < count; i++) {
				var provisionEntry = nlapiDeleteRecord(
				        'customrecord_provision_entry', provisionEntrySearch[i]
				                .getId());
				yieldScript(context);
			}
		}

		nlapiLogExecution('DEBUG', 'All Old Entries Deleted');

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent' ], [
		        'Creating New Provision Entries', '0%' ]);

		var provisionMaster = nlapiLoadRecord('customrecord_provision_master',
		        provisionMasterId);

		var startDate = provisionMaster
		        .getFieldValue('custrecord_pm_from_date');
		var endDate = provisionMaster.getFieldValue('custrecord_pm_to_date');
		var subsidiary = provisionMaster
		        .getFieldValue('custrecord_pm_subsidiary');

		var d_startDate = nlapiStringToDate(startDate);
		var d_endDate = nlapiStringToDate(endDate);

		// month wise date break up
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);

		// new_start_date = new_start_date < d_startDate ? new_start_date
		// : d_startDate;

		dateBreakUp.push({
		    Start : d_startDate,
		    End : ''
		});

		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);

			if (new_date > d_endDate) {
				break;
			}

			dateBreakUp.push({
			    Start : new_date,
			    End : ''
			});
		}

		for (var i = 0; i < dateBreakUp.length - 1; i++) {
			dateBreakUp[i].End = nlapiAddDays(dateBreakUp[i + 1].Start, -1);
		}

		dateBreakUp[dateBreakUp.length - 1].End = d_endDate;

		nlapiLogExecution('debug', 'dateBreakUp', JSON.stringify(dateBreakUp));

		// create provision entries
		for (var i = 0; i < dateBreakUp.length; i++) {

			// search all the allocation active during the specified period and
			// belonging to the subsidiary
			var filter1 = new nlobjSearchFilter('formuladate', null,
			        'notafter', dateBreakUp[i].End);
			filter1.setFormula('{startdate}');

			var filter2 = new nlobjSearchFilter('formuladate', null,
			        'notbefore', dateBreakUp[i].Start);
			filter2.setFormula('{enddate}');

			var allocationSearch = searchRecord('resourceallocation', null,
			        [
			                // new nlobjSearchFilter('resource', null, 'anyof',
			                // [
			                // '3233', '2115', '3192' ]),
			                new nlobjSearchFilter('jobbillingtype', 'job',
			                        'anyof', 'TM'),
			                filter1,
			                filter2,
			                new nlobjSearchFilter('subsidiary', 'job', 'anyof',
			                        subsidiary),
			                new nlobjSearchFilter('custeventrbillable', null,
			                        'is', 'T') ]);

			if (allocationSearch) {
				var count = allocationSearch.length;
				nlapiLogExecution('debug', 'allocation count', count);

				for (var j = 0; j < count; j++) {
					var provisionEntry = nlapiCreateRecord('customrecord_provision_entry');
					provisionEntry.setFieldValue(
					        'custrecord_pe_resource_allocation',
					        allocationSearch[j].getId());

					provisionEntry
					        .setFieldValue('custrecord_pe_provision_master',
					                provisionMasterId);

					provisionEntry.setFieldValue(
					        'custrecord_pe_provision_from_date',
					        nlapiDateToString(dateBreakUp[i].Start, 'date'));

					provisionEntry.setFieldValue(
					        'custrecord_pe_provision_to_date',
					        nlapiDateToString(dateBreakUp[i].End, 'date'));

					var id = nlapiSubmitRecord(provisionEntry);
					nlapiLogExecution('DEBUG', 'Provision Entry Created', id);
					yieldScript(context);
				}
			}
		}

		nlapiSubmitField('customrecord_provision_master', provisionMasterId, [
		        'custrecord_pm_status', 'custrecord_pm_percent',
		        'custrecord_pm_provision_creation_done' ], [
		        'Creating New Provision Entries', '100%', 'T' ]);
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}

	nlapiLogExecution('DEBUG', 'END');
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function getRunningProvisionMasterId() {
	try {
		// get the currently active monthly provision
		var provisionMasterSearch = nlapiSearchRecord(
		        'customrecord_provision_master', null, [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pm_is_currently_active', null,
		                        'is', 'T') ]);

		var provisionMasterId = null;
		if (provisionMasterSearch) {
			provisionMasterId = provisionMasterSearch[0].getId();
		}

		return provisionMasterId;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getRunningProvisionMasterId', err);
		throw err;
	}
}

function clearErrorLogs(provisionMasterId) {
	try {
		nlapiLogExecution('DEBUG', 'Clearing Error Log', 'Start');
		var logSearch = searchRecord('customrecord_provision_error_log', null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter(
		                        'custrecord_pel_provision_master', null,
		                        'anyof', provisionMasterId) ]);

		if (logSearch) {

			for (var i = 0; i < logSearch.length; i++) {
				nlapiSubmitField('customrecord_provision_error_log',
				        logSearch[i].getId(), 'isinactive', 'T');
			}
		}

		delete logSearch;
		delete i;
		nlapiLogExecution('DEBUG', 'Clearing Error Log', 'End');
	} catch (err) {
		nlapiLogExecution('ERROR', 'clearErrorLogs', err);
		throw err;
	}
}