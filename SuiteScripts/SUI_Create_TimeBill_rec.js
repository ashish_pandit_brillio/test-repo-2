/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Nov 2014     Swati
 * 2.00		  09 APR 2020	  Praveena		replaced searched record types as timebill and searches id 
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function time_bill_rec_create(request, response)
{//fun start

	try
	{
	
	 if (request.getMethod() == 'GET') 
	  {	//if get start
			nlapiLogExecution('DEBUG','working');
			var i_context = nlapiGetContext();
			var i_percent_complete='';
			//--------------------------fetching pass data----------------------------------------------
		
			 var i_usage_begin = i_context.getRemainingUsage();
			 //nlapiLogExecution('DEBUG', 'suiteletFunction',' Usage Begin -->' + i_usage_begin);	
						 			
			 var i_current_date =  request.getParameter('custscript_current_date');
			 var i_from_date =  request.getParameter('custscript_from_date');
			 var i_to_date =  request.getParameter('custscript_to_date');
			 var i_unbilled_type =   request.getParameter('custscript_unbilled_type');
			 var i_unbilled_receivable_GL =  request.getParameter('custscript_unbilled_receivable_gl');
			 var i_unbilled_revenue_GL =  request.getParameter('custscript_unbilled_revenue_gl');
			 var i_reverse_date =  request.getParameter('custscript_reverse_date');
			 var i_criteria =  request.getParameter('custscript_provision_creation_criteria');
			 var i_user_role =  request.getParameter('custscript_user_role');
			 var i_user_subsidiary =  request.getParameter('custscript_user_subsidiary');
			 var d_created_script_date =  request.getParameter('custscript_current_date');
			 var i_month =  request.getParameter('custscript_month');
			 var i_year =  request.getParameter('custscript_year');
			 
			
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Current Date -->' + i_current_date);	
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' From Date -->' + i_from_date);	
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' To Date -->' + i_to_date);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_unbilled_type -->'+i_unbilled_type);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_unbilled_receivable_GL -->'+i_unbilled_receivable_GL);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_unbilled_revenue_GL -->'+i_unbilled_revenue_GL);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_reverse_date -->'+i_reverse_date);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_criteria -->'+i_criteria);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_user_role -->'+i_user_role);
			 nlapiLogExecution('DEBUG','suiteletFunction','d_created_script_date -->'+i_user_subsidiary);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_user_subsidiary -->'+d_created_script_date);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_month -->'+i_month);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_year -->'+i_year);
			 //-----------------------------------------------------------------------------------------------------
			 var f_form = nlapiCreateForm("Time Bill Record Creation Status");
			 
			  
				var i_unbilled_type_f = f_form.addField('custpage_unbilled_type', 'text', 'Unbilled Type').setDisplayType('hidden');
				i_unbilled_type_f.setDefaultValue(i_unbilled_type)
				
	           //-----------------------------------------------------------------------------------------------------------------------------			
				var i_current_date_f =  f_form.addField('custpage_current_date', 'text', 'Current Date').setDisplayType('hidden');
				i_current_date_f.setDefaultValue(i_current_date);
				
				var i_from_date_f =  f_form.addField('custpage_from_date', 'text', 'From Date').setDisplayType('hidden');
				i_from_date_f.setDefaultValue(i_from_date);
				
				var i_to_date_f =  f_form.addField('custpage_to_date', 'text', 'To Date').setDisplayType('hidden');
				i_to_date_f.setDefaultValue(i_to_date);
				
				//---------------------------------------------------------------------------------------------------------------------------						
				var i_unbilled_receivable_GL_f = f_form.addField('custpage_unbilled_receivable_gl', 'text', 'Unbilled Rec GL').setDisplayType('hidden');
				i_unbilled_receivable_GL_f.setDefaultValue(i_unbilled_receivable_GL) ; 
				
				var i_unbilled_revenue_GL_f = f_form.addField('custpage_unbilled_revenue_gl', 'text', 'Unbilled ReEv GL').setDisplayType('hidden');
				i_unbilled_revenue_GL_f.setDefaultValue(i_unbilled_revenue_GL);
				
				var i_reverse_date_f =  f_form.addField('custpage_reverse_date', 'text', 'Reverse Date').setDisplayType('hidden');
				i_reverse_date_f.setDefaultValue(i_reverse_date);
				
				var i_criteria_fld = f_form.addField('custpage_criteria_f', 'longtext', 'Criteria').setDisplayType('hidden');			
				i_criteria_fld.setDefaultValue(i_criteria);
				
				var i_user_subsidiary_f = f_form.addField('custpage_user_subsidiary', 'textarea', 'Subsidiary').setDisplayType('hidden');				
				i_user_subsidiary_f.setDefaultValue(i_user_subsidiary);	
				
				var i_month_f = f_form.addField('custpage_month', 'textarea', 'Month').setDisplayType('hidden');				
				i_month_f.setDefaultValue(i_month);
				
				var i_year_f = f_form.addField('custpage_year', 'textarea', 'Year').setDisplayType('hidden');				
				i_year_f.setDefaultValue(i_year);
				//-------------------------------------------------------------------------------------------------
				 nlapiLogExecution('DEBUG','suiteletFunction','i_criteria -->'+i_criteria);
				if(i_criteria == 3)
			     {		
					 nlapiLogExecution('DEBUG','suiteletFunction','criteria 1');
				     var filter = new Array();
				    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
					filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
					var i_search_results = searchRecord('timebill','customsearch_notsubmitted_new_timesheet_',filter,null);//Repalced record type and searched id by praveena//286 for sandbox
					//nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
					
					var filter1 = new Array();
				    //filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
				    //filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
					filter1[0] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
					filter1[1] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
				    filter1[2] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
				    filter1[3] = new nlobjSearchFilter('custrecord_processed',null, 'is','T');
				    //filter1[4] = new nlobjSearchFilter('custrecord_billable',null, 'is','T');
				    var search_time_bill_count = searchRecord('customrecord_time_bill_rec',null,filter1,null);
				    
				    
			     }
				else if(i_criteria == 1)
					{
					nlapiLogExecution('DEBUG','suiteletFunction','criteria 1');
					var filter = new Array();
				    filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
					filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
					var i_search_results = searchRecord('timebill','customsearch_unbilled_approved_new_tb',filter,null);////Repalced record type and searched id by praveena
					nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
					
					var filter1 = new Array();
				    //filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
				    //filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
					filter1[0] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
					filter1[1] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
				    filter1[2] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
				    filter1[3] = new nlobjSearchFilter('custrecord_processed',null, 'is','T');
				    //filter1[4] = new nlobjSearchFilter('custrecord_billable',null, 'is','T');
				    var search_time_bill_count = searchRecord('customrecord_time_bill_rec',null,filter1,null);
				    
					
					}
				else if(i_criteria == 2)
				{
					nlapiLogExecution('DEBUG','suiteletFunction','criteria 3');
					 var filter = new Array();
					 filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,i_to_date);	
					 filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is', parseInt(i_user_subsidiary));
					var i_search_results = searchRecord('timebill','customsearch_submitted_not_appr_new_tb',filter,null);//Repalced record type and searched id by praveena
				//	nlapiLogExecution('DEBUG', 'suiteletFunction',' Not submitted - Allocated -->' + i_search_results.length);
						
						
					var filter1 = new Array();
				   // filter1[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
				   // filter1[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
					filter1[0] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
					filter1[1] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
				    filter1[2] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
				    filter1[3] = new nlobjSearchFilter('custrecord_processed',null, 'is','T');
				    //filter1[4] = new nlobjSearchFilter('custrecord_billable',null, 'is','T');
				    var search_time_bill_count = searchRecord('customrecord_time_bill_rec',null,filter1,null);
				    
					
				}
					//var search_time_bill_count=searchRecord('customrecord_time_bill_rec',null,filter1,null);
					if(i_search_results == null)
						{
						var final_count=0;
						}
					else
						{
						var final_count=i_search_results .length;
						}
					
				//var final_count=parseInt(i_search_results.length)-parseInt(time_rec_count);
				
				var i_count = f_form.addField('custpage_count', 'text', 'Count').setDisplayType('inline');				
				i_count.setDefaultValue(parseInt(final_count));	
			 //------------------------------------get schedule script status----------------------------------------
			 if(_logValidation(d_created_script_date))
				{//if start									
					var column = new Array();
					var filters = new Array();
					filters.push(new nlobjSearchFilter('status', null, 'anyof', ['PROCESSING', 'PENDING','COMPLETE']));
							
					filters.push(new nlobjSearchFilter('internalid','script','is','334'));//250 sand box id
					
					column.push(new nlobjSearchColumn('name', 'script'));
					column.push(new nlobjSearchColumn('internalid', 'scriptdeployment'));
					column.push(new nlobjSearchColumn('datecreated'));
					column.push(new nlobjSearchColumn('status'));
					column.push(new nlobjSearchColumn('startdate'));
					column.push(new nlobjSearchColumn('enddate'));
					column.push(new nlobjSearchColumn('queue'));
					column.push(new nlobjSearchColumn('percentcomplete'));
					column.push(new nlobjSearchColumn('queueposition'));
					column.push(new nlobjSearchColumn('percentcomplete'));
			
					var a_search_results =  searchRecord('scheduledscriptinstance', null, filters, column);
					//nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
					if(_logValidation(a_search_results)) 
				    {
				    	nlapiLogExecution('DEBUG','suiteletFunction','Search Results Length -->'+a_search_results.length);
				    	
						for(var i=0;i<a_search_results.length;i++)
				    	{
				    		var s_script  = a_search_results[i].getValue('name', 'script');
							//nlapiLogExecution('DEBUG','suiteletFunction','Script -->'+s_script);
			
							d_date_created  = a_search_results[i].getValue('datecreated');
							//nlapiLogExecution('DEBUG','suiteletFunction','Date Created -->'+d_date_created);
			    
				           	var i_percent_complete  = a_search_results[i].getValue('percentcomplete');
							//nlapiLogExecution('DEBUG','suiteletFunction','Percent Complete -->'+i_percent_complete);
			
							s_script_status  = a_search_results[i].getValue('status');
							nlapiLogExecution('DEBUG','suiteletFunction','Script Status -->'+s_script_status);
						
						    i_percent_complete = i_percent_complete;              
						
				    	}
				    }
				
				}//if close
			 
			 //---------------------------------------------------------------------------------------
			 if(i_percent_complete =='' || i_percent_complete == null || i_percent_complete == undefined)
			 {
			 	s_script_status = 'Not Started';
				i_percent_complete = '0%';
			 }
			 //---------------------------------------------------------------------------------------
				
				f_form.setScript('customscriptcli_time_bill_suitelet_ref_p');
				var i_percent_complete_f = f_form.addField('custpage_percent_complete', 'text', 'Percent Complete').setDisplayType('inline');
				i_percent_complete_f.setDefaultValue(i_percent_complete);
				
				var i_status_f = f_form.addField('custpage_status', 'text', 'Status').setDisplayType('inline');
				i_status_f.setDefaultValue(s_script_status);
			 //---------------------------call schedule script------------------------------------------
				 
				// params_1['custscript_customer'] = i_customer 	
					 
			//------------------------------------------------------------------------------------------
		     if(s_script_status == 'Complete')
		    {//if start
		    	 i_percent_complete_f.setDefaultValue('100%');
		    	 i_status_f.setDefaultValue(s_script_status);
		    	 f_form.addSubmitButton('LOAD DATA');
		    	 nlapiLogExecution('DEBUG', 'inside if status complete');
		        
		         
			}//if close
				 f_form.addButton('custpage_refresh','REFRESH','refresh()');
				
				
			     response.writePage(f_form);
		     //-----------------------------------------------------------------------------------------
	  }//if get close
	 else
	  {//post start
		 
		   var i_subsidiary =  request.getParameter('custpage_user_subsidiary');
			//var i_role =   request.getParameter('custpage_user_role');
			var i_current_date =  request.getParameter('custpage_current_date');
			//--------------------------------------------------------------------------
			var i_from_date =  request.getParameter('custpage_from_date');
			var i_to_date =  request.getParameter('custpage_to_date');
			//--------------------------------------------------------------------------
			var i_unbilled_type =   request.getParameter('custpage_unbilled_type');
			var i_unbilled_receivable_GL =  request.getParameter('custpage_unbilled_receivable_gl');
			var i_unbilled_revenue_GL =  request.getParameter('custpage_unbilled_revenue_gl');
			var i_reverse_date =  request.getParameter('custpage_reverse_date');
			var i_criteria =  request.getParameter('custpage_criteria_f');
			
			var i_month =  request.getParameter('custpage_month');
			var i_year =  request.getParameter('custpage_year');
			//var i_customer =  request.getParameter('custpage_customer');
			
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' Current Date -->' + i_current_date);	
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' From Date -->' + i_from_date);	
			 nlapiLogExecution('DEBUG', 'suiteletFunction',' To Date -->' + i_to_date);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_unbilled_type -->'+i_unbilled_type);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_unbilled_receivable_GL -->'+i_unbilled_receivable_GL);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_unbilled_revenue_GL -->'+i_unbilled_revenue_GL);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_reverse_date -->'+i_reverse_date);
			 nlapiLogExecution('DEBUG','suiteletFunction','i_criteria -->'+i_criteria);
			 
			 nlapiLogExecution('DEBUG','suiteletFunction','i_user_subsidiary -->'+i_subsidiary);
			
		 var params_1=new Array();
		 params_1['custscript_current_date'] = i_current_date
			
		 params_1['custscript_from_date'] = i_from_date
		 params_1['custscript_to_date'] = i_to_date
			
		 params_1['custscript_unbilled_type'] = i_unbilled_type
		 params_1['custscript_unbilled_receivable_gl'] = i_unbilled_receivable_GL
		 params_1['custscript_unbilled_revenue_gl'] = i_unbilled_revenue_GL
		 params_1['custscript_reverse_date'] = i_reverse_date
		 params_1['custscript_provision_creation_criteria'] = i_criteria 	
		// params_1['custscript_user_role'] = i_user_role;
		 params_1['custscript_user_subsidiary'] = i_subsidiary 
		 params_1['custscript_year'] = i_month 
		 params_1['custscript_month'] = i_year 
		 nlapiSetRedirectURL('suitelet','customscript_provision_unbilled_journal', 'customdeploy1', null,params_1);
		 
	  }//post close
	}//try close
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
					
	}//CATCH
}//fun close
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
