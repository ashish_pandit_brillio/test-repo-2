//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=368 1950
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Dec 2014     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function sourceColumnsFields(type){
	
	
	try{
	
	var record = nlapiLoadRecord('invoice', 1649704);
	var i_billable_time_count = record.getLineItemCount('time');	
	
	
	for (var i = 1; i <= i_billable_time_count; i++)
	{
		var emp_full_name = '';
		var misc_practice = '';
		var core_practice = '';
		var isApply = record.getLineItemValue('time','apply',i);
		
		var item	=	record.getLineItemValue('time','item',i);
		var employeeId = record.getLineItemValue('time', 'employee', i);
		var s_date = record.getLineItemValue('time', 'billeddate', i);
			
		//if(isApply == 'T' && (item == '2221' || item == '2222' || item == '2425' || item == '2633'))
		if(isApply == 'F')
			{
				
				
			//	if(employeeId == 160062 && nlapiStringToDate(s_date) >= nlapiStringToDate('09/08/2019') && nlapiStringToDate(s_date) <= nlapiStringToDate('09/14/2019'))
			//	record.setLineItemValue('time', 'apply', i, 'T'); // Set Employee
			
			if(employeeId == 174988 && nlapiStringToDate(s_date) >= nlapiStringToDate('09/08/2019') && nlapiStringToDate(s_date) <= nlapiStringToDate('09/14/2019')){
				record.setLineItemValue('time', 'apply', i, 'T'); // Set Employee
				record.setLineItemValue('time', 'taxcode', i, 2601);			
			}
			}
	}
	
	
	var submitted_id = nlapiSubmitRecord(record, {disabletriggers : true, enablesourcing : true}); 
	nlapiLogExecution('DEBUG','submitted_id',submitted_id);
}

catch(e){
nlapiLogExecution('DEBUG','Process Error',e);
}
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}