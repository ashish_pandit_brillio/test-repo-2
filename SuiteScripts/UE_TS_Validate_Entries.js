/**
 * Module Description
 * 
 * Add a custom checkbox field that will be used to track if the submit button was clicked or not.
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 Feb 2015     Nitish Mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord timesheet
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	form.addField('custpage_is_submit_clicked', 'checkbox', 'Is Submit Clicked').setDisplayType(
			'hidden');
}

function disableTimeCard(form) {

	var role = nlapiGetRole();

	// if (role == constant.Role.Administrator) {

	if (canPermitTimeEntry()) {
		form.removeButton('submit');
		form.removeButton('save');
	}
	// }
}

function canPermitTimeEntry() {
	var i_no_of_weeks = 8;
	var i_no_of_days = i_no_of_weeks * 7;
	var startDate = nlapiStringToDate(nlapiGetFieldValue('startdate'));
	var previous_week_start_date = nlapiAddDays(startDate, i_no_of_days);
	var next_week_start_date = nlapiAddDays(startDate, i_no_of_days + 7);
	var current_date = new Date();
	var allowTimeSheet = false;

	if (previous_week_start_date < current_date && current_date < next_week_start_date) {
		allowTimeSheet = true;
	}

	return allowTimeSheet;
}