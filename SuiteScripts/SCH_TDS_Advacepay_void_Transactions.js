// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SCH AIT advance pay to vendor void
	Author:Nikhil jain
	Company:Aashna cloudtech Pvt. Ltd.
	Date:22-12-2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_advance_void(type)
{
	/*  On scheduled function:
	 - PURPOSE
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SCHEDULED FUNCTION CODE BODY
	
	try 
	{
		
		var filters = new Array();
		
		var column = new Array();
		column.push(new nlobjSearchColumn('internalid'));
		column.push(new nlobjSearchColumn('custbody_tds_billcredit_ref'));
		column.push(new nlobjSearchColumn('subsidiary'));
		
		var results = nlapiSearchRecord('check', 'customsearch_void_advance_pay', filters, column);
		nlapiLogExecution('DEBUG', 'void vendor credit', 'results->' + results)
		
		if (results != null && results != '' && results != undefined) 
		{
			
			var i_AitGlobalRecId = SearchGlobalParameter();
			nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
			if (i_AitGlobalRecId != 0) 
			{
				var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
				var a_subisidiary = new Array()
				a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
				nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			
			}// end if(i_AitGlobalRecId != 0 )
		
			nlapiLogExecution('DEBUG', 'void vendor credit', 'result length->' + results.length)
			for (var i = 0; i < results.length; i++) 
			{
				var internal_id = results[i].getValue('internalid')
				nlapiLogExecution('DEBUG', 'void vendor credit', 'internal_id->' + internal_id)
				
				var bill_credit_id = results[i].getValue('custbody_tds_billcredit_ref')
				nlapiLogExecution('DEBUG', 'void vendor credit', 'bill_credit_id->' + bill_credit_id)
				
				if (bill_credit_id != '' && bill_credit_id != null && bill_credit_id != '') 
				{
					var chk_subsidiary = results[i].getValue('subsidiary')
					nlapiLogExecution('DEBUG', 'void vendor credit', 'chk_subsidiary->' + chk_subsidiary)
					
					var Flag = 0;
					
					Flag = isIndia_subsidiary(a_subisidiary, chk_subsidiary)
					
					if (Flag == 1) 
					{
						var accountpref_config = nlapiLoadConfiguration('accountingpreferences')
						var b_reversalvoiding = accountpref_config.getFieldValue('REVERSALVOIDING');
						if (b_reversalvoiding == 'F') 
						{
							//nlapiLogExecution('DEBUG', 'disable vendor credit void button', " Voided before api")
							nlapiVoidTransaction('vendorcredit', bill_credit_id)
							nlapiLogExecution('DEBUG', 'void the record', " Voided record")
						}
						else 
						{
							//nlapiVoidTransaction('vendorcredit', bill_credit_id)
							nlapiLogExecution('DEBUG', 'disable vendor credit void button', " Voided through zero")
							var o_billcredit_obj = nlapiLoadRecord('vendorcredit', bill_credit_id)
							nlapiLogExecution('DEBUG', 'disable vendor credit void button', " o_billcredit_obj " + o_billcredit_obj)
							
							if (o_billcredit_obj != '' && o_billcredit_obj != '' && o_billcredit_obj != null) 
							{
								var exp_linecount = o_billcredit_obj.getLineItemCount('expense');
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "expense line count" + exp_linecount)
								
								for (var n = 1; n <= exp_linecount; n++) 
								{
									o_billcredit_obj.setLineItemValue('expense', 'amount', n, parseInt(0))
								}
								
								var item_linecount = o_billcredit_obj.getLineItemCount('item');
								nlapiLogExecution('DEBUG', 'afterSubmitRecord check', "item line count" + item_linecount)
								
								for (var l = 1; l <= item_linecount; l++) 
								{
									o_billcredit_obj.setLineItemValue('item', 'amount', l, parseInt(0))
								}
								
								var updated_billcredit = nlapiSubmitRecord(o_billcredit_obj)
								nlapiLogExecution('DEBUG', 'disable vendor credit void button', "updated_billcredit " + updated_billcredit)
							}
							
							
						}
						
						var check_obj= nlapiLoadRecord('check',internal_id)
						check_obj.setFieldValue('custbody_tds_void','T')
						var updated_check = nlapiSubmitRecord(check_obj)
						nlapiLogExecution('DEBUG', 'disable vendor credit void button', "updated_check " + updated_check)
						
						
					}//endif (Flag == 1)
				}//End if(Bill credit != null)
			}
		}// End if(results != null)
	}
catch(e)
	{
		 nlapiLogExecution('DEBUG', 'Vendor credit void record ', "Exception Thrown" + e.getDetails());
	}

}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
{
//==== CODE TO GET THE GLOBAL PARAMETER =====
function SearchGlobalParameter()
{
	
	
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length && s_serchResult != null; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	
	
	return i_globalRecId;
}

function isIndia_subsidiary(a_subisidiary,chk_subsidiary)
{
	var Flag = 0;
				
	if (a_subisidiary[1] != undefined && a_subisidiary[1] != null && a_subisidiary[1] != '') 
	{
		for (var y = 0; y < a_subisidiary.length; y++) 
		{
			var i_subsidiary = chk_subsidiary;
						
			//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
			if (a_subisidiary[y] == i_subsidiary)
			{
				Flag = 1;
				break;
			}// END if (a_subisidiary[y] == i_subsidiary)
		}// END for (var y = 0; y < a_subisidiary.length; y++)
	}// END  if (a_subsidiary1.indexOf(',') > -1)
	else 
	{
		// === IF COMMA NOT FOUND ===
		var i_subsidiary = chk_subsidiary;
					
		//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
		if (a_subisidiary[0] == i_subsidiary)
		{
				Flag = 1;
		}// END  if (a_subsidiary1 == i_subsidiary)
	}// END  if (a_subsidiary1 == i_subsidiary)
				
	//===== FOR SINGLE SUBSIDARY ACCOUNT  =======
				
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
				
	if (i_subcontext == false) 
	{
		Flag = 1;
	}
return Flag;
	
}
// END FUNCTION =====================================================


}
// END FUNCTION =====================================================
