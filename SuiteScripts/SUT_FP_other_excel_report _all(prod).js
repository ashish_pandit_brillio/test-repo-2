/**
 * @author Prabhat
 */
function suiteletFunction_FP_RevRec_report(request, response)
{
	try {
		var method = request.getMethod();
		if (method == 'GET') {
			suiteletFunction_FP_RevRec_Dashboard(request);
		}else {
			postFormAppoved(request);
		}
	} catch (err) {
		throw  err;
		nlapiLogExecution('Error', 'Rev Rec Page ERROR',err);
	}
}
function suiteletFunction_FP_RevRec_Dashboard(request)
{
	var a_project_list_m = new Array();
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var a_project_list = new Array();
		var a_practice_list=new Array();
		var date_proj=new Date();
		var a_project_search_results = '';
		
		/*
		for testing purpose prabhat gupta NIS-1757 12/10/2020
		for testing purpose
		if(i_user_logegdIn_id == "140512"){
			i_user_logegdIn_id = "127354";
		}
		*/
		
		nlapiLogExecution('DEBUG','USER',i_user_logegdIn_id);
		if (parseInt(i_user_logegdIn_id) == parseInt(41571)||
			parseInt(i_user_logegdIn_id) == parseInt(62082)|| parseInt(i_user_logegdIn_id) == parseInt(7905)
					|| parseInt(i_user_logegdIn_id) == parseInt(35819)|| 
					parseInt(i_user_logegdIn_id) == parseInt(90037)|| parseInt(i_user_logegdIn_id) == parseInt(72055)
					|| parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165)
					|| parseInt(i_user_logegdIn_id) == parseInt(2301) || parseInt(i_user_logegdIn_id) == parseInt(97260))
		{
			var o_form_obj = nlapiCreateForm('Project Forecast report generation will take few moments, please continue with other tasks as you will receive this report in your mail box soon....! ');
				nlapiLogExecution('error', 'loop', i_user_logegdIn_id);
				 status=nlapiScheduleScript('customscript1554', 'customdeploy1');
		}
		else{
    if(parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(30099) )
	 i_user_logegdIn_id = 1605;
		// design form which will be displayed to user
		var pract_fil=new Array();
           pract_fil = [[ ['custrecord_practicehead','anyof', i_user_logegdIn_id ],
		            	'or',
						['custrecord_practice_head_2','anyof', i_user_logegdIn_id ],
		            	'and',
		            	['isinactive','is', 'F' ]]];
		//pract_fil[0]=new nlobjSearchFilter('custrecord_practicehead',null,'anyof',i_user_logegdIn_id);
		//pract_fil[1]=new nlobjSearchFilter('isinactive',null,'is','F');
		var pract_col=new nlobjSearchColumn('internalid');
		var pract_search=nlapiSearchRecord('department',null,pract_fil,pract_col);
		if(pract_search)
		{
			for(var i_prac=0;i_prac<pract_search.length;i_prac++)
			{
				a_practice_list.push(pract_search[i_prac].getValue('internalid'));
			}
		}
          if(_logValidation(a_practice_list))
          {
			var o_form_obj = nlapiCreateForm('Project Forecast report generation will take few moments, please continue with other tasks as you will receive this report in your mail box soon....! ');
				nlapiLogExecution('error', 'loop', i_user_logegdIn_id);
				 status=nlapiScheduleScript('customscript1552', 'customdeploy1');
		}
		var o_form_obj = nlapiCreateForm("Executing Practice forecast view");
		//mile starts
		var a_pro_li_m= new Array();
		var a_get_logged_in_user_exsiting_revenue_cap_m = '';
		if(_logValidation(a_practice_list)){
			var a_revenue_cap_filter_m = [[['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_revenue_share_project.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_revenue_share_project.custentity_practice','anyof',a_practice_list],'or',
								['custrecord_revenue_share_cust.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
								'and',
								['custrecord_revenue_share_approval_status','anyof',3], 
								'and',
								['custrecord_revenue_share_cust.status', 'noneof', "1"]// prabhat gupta NIS-1757 closed project will not come
								
								];
			}
		else
		{
			var a_revenue_cap_filter_m = [[['custrecord_revenue_share_project.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
							['custrecord_revenue_share_project.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
							['custrecord_revenue_share_project.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
							['custrecord_revenue_share_cust.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
							'and',
							['custrecord_revenue_share_approval_status','anyof',3],
								'and',
								['custrecord_revenue_share_cust.status', 'noneof', "1"]// prabhat gupta NIS-1757 closed project will not come
							
							];
		}
		var a_columns_existing_cap_srch_m = new Array();
		a_columns_existing_cap_srch_m[0] = new nlobjSearchColumn('custrecord_revenue_share_project');
		a_columns_existing_cap_srch_m[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
		a_columns_existing_cap_srch_m[2] = new nlobjSearchColumn('created').setSort(true);
		a_columns_existing_cap_srch_m[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');//(parseInt(i_user_logegdIn_id) == parseInt(41571) ||
		if (parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165))
		{
			var a_project_cap_filter_m = [['custrecord_revenue_share_approval_status','anyof',3],'and',
					['custrecord_revenue_share_project.enddate','onorafter',date_proj],
					'and',
								['custrecord_revenue_share_cust.status', 'noneof', "1"]// prabhat gupta NIS-1757 closed project will not come
					];
													
			a_get_logged_in_user_exsiting_revenue_cap_m = searchRecord('customrecord_revenue_share', null, a_project_cap_filter_m, a_columns_existing_cap_srch_m);
		 }
		else//
			 a_get_logged_in_user_exsiting_revenue_cap_m = searchRecord('customrecord_revenue_share', null, a_revenue_cap_filter_m, a_columns_existing_cap_srch_m);
			if(a_get_logged_in_user_exsiting_revenue_cap_m)
			{
             	nlapiLogExecution('DEBUG','total',a_get_logged_in_user_exsiting_revenue_cap_m.length);
				for(var i=0;i< a_get_logged_in_user_exsiting_revenue_cap_m.length;i++)
				{
					var pid_m=a_get_logged_in_user_exsiting_revenue_cap_m[i].getValue('custrecord_revenue_share_project');
					nlapiLogExecution('DEBUG','Projectid',pid_m);
					if(pid_m)
					{
						
						/*var a_project_filter_m = [[['custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]], 'and',
								['status', 'noneof', 1], 'and',
								//['internalid', 'noneof', pid_m], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM']];//, 'and'
								//['custentity_fp_rev_rec_type', 'anyof', 1]];
							//	['custentity_fp_rev_rec_type', 'noneof', '@NONE@']*/	
				/*			
						var a_project_filter_m =new Array();
					    a_project_filter_m.push(new nlobjSearchFilter('custentity_projectmanager', null, 'anyof', i_user_logegdIn_id));
						a_project_filter_m.push(new nlobjSearchFilter('custentity_deliverymanager', null, 'anyof', i_user_logegdIn_id));
						a_project_filter_m.push(new nlobjSearchFilter('custentity_clientpartner', null, 'anyof', i_user_logegdIn_id));
						a_project_filter_m.push(new nlobjSearchFilter('custentity_clientpartner', 'customer', 'anyof', i_user_logegdIn_id));
						a_project_filter_m.push(new nlobjSearchFilter('status', null, 'noneof', 1));
						a_project_filter_m.push(new nlobjSearchFilter('jobtype', null, 'anyof', 2));
						a_project_filter_m.push(new nlobjSearchFilter('jobbillingtype', null, 'anyof','FBM'));
						a_project_filter_m.push(new nlobjSearchFilter('custentity_fp_rev_rec_type', null, 'anyof', 1));
						var a_columns_proj_srch_m = new Array();
						a_columns_proj_srch_m[0] = new nlobjSearchColumn('custentity_projectvalue');
						a_columns_proj_srch_m[1] = new nlobjSearchColumn('customer');
						a_columns_proj_srch_m[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
						a_columns_proj_srch_m[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
				
						if(parseInt(i_user_logegdIn_id) == parseInt(41571)|| parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(90037) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(2301)|| parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165))
						{
							var a_project_filter_m = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1],'and',
                               ['enddate','onorbefore',date_proj],'and',
								['internalid','anyof',pid_m]];
								
							a_project_search_results_m = searchRecord('job', null, a_project_filter_m, a_columns_proj_srch_m);
						}
						else
						{
							a_project_search_results_m = searchRecord('job', null, a_project_filter_m, a_columns_proj_srch_m);
						}
				*/		
						var a_JSON_m = {};
						var s_create_update_mode_m = '';
			
						var proj_m=nlapiLoadRecord('job',pid_m);
					//	for(var i_pro_index = 0; i_pro_index < a_project_search_results_m.length; i_pro_index++)
						{
							var i_project_internal_id_m = proj_m.getId();
							var f_project_existing_revenue_m = 0;
							var i_existing_revenue_share_rcrd_id_m = 0;
							var b_is_proj_value_chngd_flag_m = 'F';
							var s_existing_revenue_share_status_m = '';
							
				
							a_JSON_m = {
									project_cutomer: proj_m.getFieldValue('customer'),
									project_id: proj_m.getId(),
									project_value:proj_m.getFieldValue('custentity_projectvalue'),
									revenue_share_status: 'Approved',
								//	project_existing_value: f_project_existing_revenue_m,
									project_rev_rec_type: proj_m.getFieldValue('custentity_fp_rev_rec_type'),
							
								};
				
							a_project_list_m.push(a_JSON_m);
							a_pro_li_m.push(a_JSON_m);
						}
					}
				
				}
		
				
				}
		
		
		
		
		
		
		//mile ends
			
		var a_get_logged_in_user_exsiting_revenue_cap = '';
		if(_logValidation(a_practice_list)){
			var a_revenue_cap_filter = [[['custrecord_fp_rev_rec_others_projec.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_practice','anyof',a_practice_list],'or',
								['custrecord_fp_rev_rec_others_customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
								'and',
								['custrecord_revenue_other_approval_status','anyof',2,3],
								'and',
								['custrecord_fp_rev_rec_others_projec.status', 'noneof', "1"]// prabhat gupta NIS-1757 closed project will not come
								];
			}
			else
			{
				var a_revenue_cap_filter = [[['custrecord_fp_rev_rec_others_projec.custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_projec.custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custrecord_fp_rev_rec_others_customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]],
								'and',
								['custrecord_revenue_other_approval_status','anyof',[2,3]],
								'and',
								['custrecord_fp_rev_rec_others_projec.status', 'noneof', "1"]// prabhat gupta NIS-1757 closed project will not come
								
								];
			}
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_total_rev');
			a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
			a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');//(parseInt(i_user_logegdIn_id) == parseInt(41571) ||
			if (parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(72055) ||  parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165))
			{
				var a_project_cap_filter =[ ['custrecord_revenue_other_approval_status','anyof',[2,3]],
   'and',                         ['custrecord_fp_rev_rec_others_projec.enddate','onorbefore',date_proj],
								'and',
								['custrecord_fp_rev_rec_others_projec.status', 'noneof', "1"]// prabhat gupta NIS-1757 closed project will not come
   
   ];
														
				a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_fp_rev_rec_others_parent', null, a_project_cap_filter, a_columns_existing_cap_srch);
			 }
			else
			 a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if(_logValidation(a_get_logged_in_user_exsiting_revenue_cap))
			{
              	nlapiLogExecution('DEBUG','total',a_get_logged_in_user_exsiting_revenue_cap.length);
				for(var i=0;i< a_get_logged_in_user_exsiting_revenue_cap.length;i++)
				{
					var pid=a_get_logged_in_user_exsiting_revenue_cap[i].getValue('custrecord_fp_rev_rec_others_projec');
					nlapiLogExecution('DEBUG','Projectid',pid);
					if(pid)
					{
						
					/*	
						var a_project_filter = [[['custentity_projectmanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_deliverymanager', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_clientpartner', 'anyof', i_user_logegdIn_id], 'or',
								['custentity_practice.custrecord_practicehead','anyof', i_user_logegdIn_id],'or',
								['customer.custentity_clientpartner', 'anyof', i_user_logegdIn_id]], 'and',
								['status', 'noneof', 1], 'and',
								//['internalid', 'noneof', pid], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', [2,4]]];//bid
								//['custentity_fp_rev_rec_type', 'noneof', '@NONE@']
						var a_columns_proj_srch = new Array();
						a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
						a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
						a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
						a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
				
						if(parseInt(i_user_logegdIn_id) == parseInt(41571)|| parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(90037) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(2301)|| parseInt(i_user_logegdIn_id) == parseInt(30484)|| parseInt(i_user_logegdIn_id) == parseInt(3165))
						{
							var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', [2,4]],'and',//bid
								['internalid','anyof',pid]];
								
							a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
						}
						else
						{
							a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
						}
					*/	
						var a_JSON = {};
						var s_create_update_mode = '';
			
						var proj=nlapiLoadRecord('job',pid);
					//	for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
						{
							var i_project_internal_id = proj.getId();
							var f_project_existing_revenue = 0;
							var i_existing_revenue_share_rcrd_id = 0;
							var b_is_proj_value_chngd_flag = 'F';
							var s_existing_revenue_share_status = '';
							
				
							a_JSON = {
									project_cutomer: proj.getFieldValue('customer'),
									project_id: proj.getId(),
									project_value:proj.getFieldValue('custentity_projectvalue'),
									revenue_share_status: 'Approved',
								//	project_existing_value: f_project_existing_revenue,
									project_rev_rec_type: proj.getFieldValue('custentity_fp_rev_rec_type'),
							
								};
				
							a_project_list.push(a_JSON);
						}
					}
				
				}
		
				
			
			//create sublist for project
				var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
				f_form_sublist.addField('project_cutomer','select','Customer','customer').setDisplayType('inline');
				f_form_sublist.addField('project_id','select','Project','job').setDisplayType('inline');
				f_form_sublist.addField('project_value','currency','Project Value');
				f_form_sublist.addField('revenue_share_status','text','Status');
				//f_form_sublist.addField('project_existing_value','currency','Captured Revenue Share');
				f_form_sublist.addField('project_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
				var total_fp_mi =new Array();
				total_fp_mi = a_project_list.concat(a_project_list_m);
				f_form_sublist.setLineItemValues(total_fp_mi);
		o_form_obj.addSubmitButton('Generate Project Forecast');
			}
		else
		{
           //throw "No Records Found";
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
                    
		}
		}
		response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','function suiteletFunction_FP_RevRec_Dashboard','ERROR MESSAGE :- '+err);
	}
}
function postFormAppoved(request) {
	try {
								
			var form = nlapiCreateForm('Project Forecast report generation will take few moments, please continue with other tasks as you will receive this report in your mail box soon....! ');
			//var param=[];
		//	param['custscript5']=request.getParameter('custpage_field1' );
			var user = nlapiGetUser();
			//if(parseInt(user) == parseInt(41571) || parseInt(user) == parseInt(7905))
			//user = 3169;
		/*
		for testing purpose prabhat gupta NIS-1757 12/10/2020
		if(user == "140512"){
			user = "127354";
		}
		*/
			var status = '';
			 if (parseInt(user) == parseInt(41571)
					|| parseInt(user) == parseInt(62082)
					|| parseInt(user) == parseInt(7905)
					|| parseInt(user) == parseInt(35819)
					|| parseInt(user) == parseInt(90037)
					|| parseInt(user) == parseInt(72055)
					|| parseInt(user) == parseInt(30484)
                    || parseInt(user) == parseInt(3165)
					|| parseInt(user) == parseInt(2301)) {
				nlapiLogExecution('error', 'loop', user);
				 status=nlapiScheduleScript('customscript1554', 'customdeploy1');
			}else{
				status=nlapiScheduleScript('customscript1552', 'customdeploy1');
			}
			
			//nlapiLogExecution('DEBUG','STATUS',status);
			response.writePage(form);
		
		
		
	}     catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		
	}
	
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }