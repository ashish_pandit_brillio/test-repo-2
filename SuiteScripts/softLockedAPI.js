/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Jan 2019     Aazamali Khan
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function softLockEmp(dataIn) {
    try {
        var requestBody = dataIn;
        nlapiLogExecution("AUDIT","requestBody",JSON.stringify(requestBody));
        var frfId, bodyJSON, empId,user;
        if (requestBody) {
            bodyJSON = requestBody;
            frfId = bodyJSON.frfId;
            empId = bodyJSON.empId;
 			user = bodyJSON.user;         
        }
        if (frfId && empId) {
			nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_selected_emp", parseInt(empId));
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_status_flag", "3");
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_open_close_status", "3");
			var date = new Date();
            var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
          	var d_today = moment(date).format('MM/DD/YYYY h:mm:ss a');
            nlapiLogExecution("DEBUG","date : ",d_today);
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_softlock_date", date);
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecordfrf_details_created_time", time);
            var employeeSearch = getEmployeeRec(user);
			if(employeeSearch){
				nlapiSubmitField("customrecord_frf_details",parseInt(frfId),"custrecord_frf_details_softlocked_by",employeeSearch[0].getId());
			}
            return {
            "code" : "frf has been processed",  
            "message": "successful"
        	}
        } else if (frfId && !empId) {
			nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_selected_emp", "");
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_status_flag", "1");
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_open_close_status", "1");
	//-----------------------------------------------------------------------------------------------------------------------//prabhat gupta FUEL-786 07/07/2020	capturing system date as de-softlock date when requested	
			var date = new Date();
		    nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_desoftlock_date", date); 
	//---------------------------------------------------------------------------------------------------------------------		 
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecord_frf_details_softlock_date", "");
            nlapiSubmitField("customrecord_frf_details", parseInt(frfId), "custrecordfrf_details_created_time", "");
            var employeeSearch = getEmployeeRec(user);
			if(employeeSearch){
				nlapiSubmitField("customrecord_frf_details",parseInt(frfId),"custrecord_frf_details_softlocked_by",employeeSearch[0].getId());
			}
            return {
            "code" : "frf has been processed",  
            "message": "successful"
        	}
        }
		else {
            var jsonObj = {
                "code" : "frfid is not present in netsuite",
                "message": "failed"
            }
        }
    } catch (e) {
        nlapiLogExecution("DEBUG", "Error in Script : ", e);
		
      if(e.code == "RCRD_DSNT_EXIST"){
			return {
            "code": e.code,
            "message": "FRF number does not exists in database!"
        }	
		}else{
        return {
            "code": "error",
            "message": e
        }
		}
    }
}
//*******Get Employee Search/
function getEmployeeRec(personalEmail) {
		var employeeSearch = nlapiSearchRecord("employee", null,
			[
				["email", "is", personalEmail]
			],
			[]);
		return employeeSearch;
}
//
/*********************************************************/
function getsoftLockedEmployee(softLockedEmp) {
    // Get softlocked employees
    if (softLockedEmp) {
        var empId = softLockedEmp;
        nlapiLogExecution("AUDIT", "empId", empId);
        s_visa = GetVisa(empId);
        if (s_visa) {
            s_visa = s_visa;
        } else {
            s_visa = "NA";
        }
		nlapiLogExecution('Debug','empId ',empId);
        empRecObj = nlapiLoadRecord('employee', parseInt(empId));
		nlapiLogExecution('Debug','empRecObj ',empRecObj);
        s_employeeName = empRecObj.getFieldValue("entityid"); //nlapiLookupField("employee",empId,"entityid");
        s_practice = empRecObj.getFieldText("department"); //;nlapiLookupField("employee",empId,"department");
        i_practice = empRecObj.getFieldValue("department"); //nlapiLookupField("employee",empId,"department",true);
        s_empLevel = empRecObj.getFieldValue("employeestatus");
        s_role = empRecObj.getFieldValue("title");
        s_location = empRecObj.getFieldText("location");
        i_location = empRecObj.getFieldValue("location");
		nlapiLogExecution('Debug','i_location ',i_location);
        // Get below information from employee records itself 
        var searchResult = getEmployeeSkillDetails(softLockedEmp);
		nlapiLogExecution('Debug','searchResult ',searchResult);
        if (searchResult) {
            for (var int = 0; int < searchResult.length; int++) {
                s_skillFamily = searchResult[int].getText("custrecord_primary_updated");
                a_skillFamily = searchResult[int].getValue("custrecord_primary_updated"); //frfDetailsObj.getFieldValue("custrecord_frf_details_primary_skills");
                i_family = searchResult[int].getValue("custrecord_family_selected");
                s_family = searchResult[int].getText("custrecord_family_selected");
            }
        }
        var o_searchResult = getAllocationEndDate(empId);
        if (o_searchResult) {
            s_avaliablity = "Avaliable From : " + o_searchResult[0].getValue("enddate");
            i_project = o_searchResult[0].getValue("project");
            i_customer = i_project ? nlapiLookupField("job", i_project, "custentity_customerlob") : "";
            s_customer = i_project ? nlapiLookupField("job", i_project, "custentity_customerid") : "";
        } else {
            s_avaliablity = "Available";
        }
        s_reportingManager = empRecObj.getFieldText("custentity_reportingmanager");
        jsonObj1 = {};
        jsonObj1 = {
            "preferred": {
                "id": true,
                "name": true
            },
            "visa": {
                "name": s_visa
            },
            "employee": {
                "name": s_employeeName,
                "id": empId
            },
            "practice": {
                "name": s_practice,
                "id": i_practice
            },
            "emplevel": {
                "name": s_empLevel
            },
            "skillfamily": {
                "name": s_family,
                "id": i_family
            },
            "primaryskill": {
                "name": s_skillFamily,
                "id": a_skillFamily
            },
            "role": {
                "name": s_role
            },
            "location": {
                "name": s_location,
                "id": i_location
            },
            "customer": {
                "name": s_customer,
                "id": i_customer
            },
            "availablefrom": {
                "name": s_avaliablity
            },
            "mandatorySkills": {
                "name": "Good Match"
            },
            "reportingManager": {
                "name": GetManagerName(s_reportingManager)
            }
        };
        return jsonObj1;
    }
    //End 
    /*********************************************************/
}

function GetManagerName(tempString) {
    var s_manager = "";
    //var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
    temp = tempString.indexOf("-");
    if (temp > 0) {
        var s_manager = tempString.split("-")[1];
    } else {
        var s_manager = tempString;
    }
    return s_manager;
}

function GetVisa(empId) {
    var customrecord_empvisadetailsSearch = nlapiSearchRecord("customrecord_empvisadetails", null,
        [
            ["custrecord_visaid", "anyof", empId],
            "AND",
            ["custrecord_validtill", "notbefore", "today"]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_visa"),
            new nlobjSearchColumn("custrecord27"),
            new nlobjSearchColumn("custrecord_validtill"),
            new nlobjSearchColumn("custrecord28")
        ]
    );
    return customrecord_empvisadetailsSearch;
}

function getAllocationEndDate(empId) {
    var resourceallocationSearch = nlapiSearchRecord("resourceallocation", null,
        [
            ["startdate", "onorbefore", "today"],
            "AND",
            ["enddate", "onorafter", "today"],
            "AND",
            ["resource", "anyof", empId]
        ],
        [
            new nlobjSearchColumn("enddate"),
            new nlobjSearchColumn("project")
        ]
    );
    return resourceallocationSearch;
}

function getEmployeeSkillDetails(empId) {
    var customrecord_employee_master_skill_dataSearch = nlapiSearchRecord("customrecord_employee_master_skill_data", null,
        [
            ["custrecord_employee_skill_updated", "anyof", empId]
        ],
        [
            new nlobjSearchColumn("scriptid").setSort(false),
            new nlobjSearchColumn("custrecord_employee_skill_updated"),
            new nlobjSearchColumn("custrecord_primary_updated"),
            new nlobjSearchColumn("custrecord_secondry_updated"),
            new nlobjSearchColumn("custrecord_skill_status"),
            new nlobjSearchColumn("custrecord_family_selected"),
            new nlobjSearchColumn("custrecord_employee_approver"),
            new nlobjSearchColumn("custrecord_new_skill_acquired")
        ]
    );
    return customrecord_employee_master_skill_dataSearch;
}