/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name : SUT_BLLC_T_M_Group_Invoice_Print.js Author : Shweta Chopde
	 * Date : 3 June 2014 Description : Create a Invoice Layout
	 * 
	 * 
	 * Script Modification Log: -- Date -- -- Modified By -- --Requested By-- --
	 * Description -- 26 June 2014 Shweta Chopde Shekar Modified IDs as per the
	 * sandbox accounts
	 * 
	 * 7 July 2014 Shweta Chopde Shekar Added logic as per the ST OT
	 * Calculations 27boct 2014 Swati Kurariya Shekar Change column sequence 04
	 * nov 2014 Swati Kurariya Shekar solve date issue 07 nov 2014 Swati
	 * Kurariya Shekar add discount item (to correct invoice amount)
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * SUITELET - suiteletFunction(request, response)
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization: - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN SUITELET ==================================================

function suiteletFunction(request, response) {
	var a_data_array = new Array();
	var a_hours_array = new Array();
	var a_employee_array = new Array();
	var a_line_array = new Array();
	var i_inc_cnt = 0;
	var i_cnt = 0;
	var i_hours_per_day;
	var i_hours_per_week;
	var i_days_for_month;
	var i_OT_applicable;
	var i_OT_rate_ST;
	var i_week_days;
	var a_location_data_array = new Array();
	var i_rate = 0;
	var otrVar;
	var ctrVar;
	var i_flag = 0;
	var i_project;
	var i_project_text;
	var i_total_amount_INV = 0;
	var a_split_f_array = new Array();
	var i_cnt = 0;
	var i_S_No_cnt = 0;
	var strVar = "";
	var a_employee_array = new Array();
	var a_employee_name_array = new Array();
	var i_cnt = 0;
	var linenum = '17';
	var pagening = 4;
	var pagecount = 0;

	var s_remittance_details = "Accounts Receivable, Brillio LLC;\n100 Town Square Place;\nSuite 308, Jersey City\n NJ 07310"

	var flag_name = '';

	var s_brillio_address = " 100 Town Square Place;\nSuite 308, Jersey City\n NJ 07310"

	s_remittance_details = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_remittance_details));
	s_brillio_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_brillio_address));

	try {
		var i_recordID = request.getParameter('i_recordID');
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' Record ID -->'
		        + i_recordID);

		if (_logValidation(i_recordID)) {
			var o_recordOBJ = nlapiLoadRecord('invoice', i_recordID);

			if (_logValidation(o_recordOBJ)) {
				var i_customer = o_recordOBJ.getFieldValue('entity');
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer -->'
				        + i_customer);

				if (_logValidation(i_customer)) {
					var o_customerOBJ = nlapiLoadRecord('customer', i_customer);

					if (_logValidation(o_customerOBJ)) {
						i_customer_name = o_customerOBJ
						        .getFieldValue('altname')
						// nlapiLogExecution('DEBUG', 'suiteletFunction','
						// Customer Name -->' + i_customer_name);

					}// Customer OBJ
				}// Customer

				var i_customer_txt = o_recordOBJ.getFieldText('entity');
				// nlapiLogExecution('DEBUG', 'suiteletFunction',' Customer Text
				// -->' + i_customer_txt);

				var i_tran_date = o_recordOBJ.getFieldValue('trandate')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tran Date
				// --> ' + i_tran_date);

				var i_tran_id = o_recordOBJ.getFieldValue('tranid')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Invoice
				// Number --> ' + i_tran_id);

				if (!_logValidation(i_tran_id)) {
					i_tran_id = ''
				}

				var i_bill_address = nlapiEscapeXML(o_recordOBJ
				        .getFieldValue('billaddress'))
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill Address
				// --> ' + i_bill_address);
				// i_bill_address =
				// formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_bill_address));

				if (!_logValidation(i_bill_address)) {
					i_bill_address = ''
				}

				var i_PO_ID = o_recordOBJ.getFieldValue('otherrefnum')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO # --> ' +
				// i_PO_ID);

				if (!_logValidation(i_PO_ID)) {
					i_PO_ID = ''
				}
				var i_terms = o_recordOBJ.getFieldText('terms')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Terms --> '
				// + i_terms);

				if (!_logValidation(i_terms)) {
					i_terms = ''
				}
				var s_memo = o_recordOBJ.getFieldValue('memo')
                s_memo = nlapiEscapeXML(s_memo);
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Memo --> ' +
				// s_memo);

				if (!_logValidation(s_memo)) {
					s_memo = ''
				}

				var i_discount = o_recordOBJ.getFieldValue('discounttotal')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Discount -->
				// ' + i_discount);

				if (!_logValidation(i_discount)) {
					i_discount = 0
				}

				var i_bill_to = o_recordOBJ.getFieldValue('custbody_billto')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill To -->
				// ' + i_bill_to);

				if (!_logValidation(i_bill_to)) {
					i_bill_to = ''
				}
				var i_bill_from = o_recordOBJ
				        .getFieldValue('custbody_billfrom')
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill From --> ' + i_bill_from);

				if (!_logValidation(i_bill_from)) {
					i_bill_from = ''
				}
				var i_billing_description = o_recordOBJ
				        .getFieldValue('custbody_billingdescription');
				 nlapiLogExecution('DEBUG', 'suiteletFunction', ' Bill Description --> ' + i_billing_description);

				if (!_logValidation(i_billing_description)) {
					i_billing_description = ''
				}

				var i_service_tax_category = o_recordOBJ
				        .getFieldText('custbody_servicetaxcategory')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Service Tax
				// Category --> ' + i_service_tax_category);

				if (!_logValidation(i_service_tax_category)) {
					i_service_tax_category = ''
				}
				var i_ST_Classification = o_recordOBJ
				        .getFieldText(' custbody_stclassificationus')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' ST
				// Classification U/S --> ' + i_ST_Classification);

				if (!_logValidation(i_ST_Classification)) {
					i_ST_Classification = ''
				}
				var i_tax = o_recordOBJ.getFieldValue('taxtotal')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax --> ' +
				// i_tax);

				if (!_logValidation(i_tax)) {
					i_tax = 0
				}

				var i_discount_percent = parseFloat(i_discount) / 100;

				var i_tax_percent = parseFloat(i_tax) / 100;

				i_discount_percent = parseFloat(i_discount_percent).toFixed(2);
				i_tax_percent = parseFloat(i_tax_percent).toFixed(2);

				var i_billable_time_count = o_recordOBJ
				        .getLineItemCount('time')

				var i_phone_no = '';
				var i_customer_address = '';
				var i_attention = '';
				var i_service_tax_reg_no = '';
				var i_CIN_No = '';
				var i_phone_no = '';
				var i_addr1 = '';
				var i_addr2 = '';
				var i_city = '';
				var i_zip = '';
				var i_country = '';
				var i_state = '';

				var a_customer_array = get_customer_details(i_customer)
				var a_split_array = new Array();
				if (_logValidation(a_customer_array)) {
					a_split_array = a_customer_array[0].split('######')

					i_customer_address = a_split_array[0];
					i_attention = a_split_array[1];
					i_service_tax_reg_no = a_split_array[2];
					i_CIN_No = a_split_array[3];
					i_phone_no = a_split_array[4];
					i_addr1 = a_split_array[5];
					i_addr2 = a_split_array[6];
					i_city = a_split_array[7];
					i_zip = a_split_array[8];
					i_country = a_split_array[9];
					i_state = a_split_array[10];

				}// Customer Array

				if (i_customer_address.toString() == 'null'
				        || i_customer_address.toString() == null
				        || i_customer_address == null
				        || i_customer_address == ''
				        || i_customer_address == undefined) {
					i_customer_address = '';
				}// Customer Address

				if (i_phone_no.toString() == 'null'
				        || i_phone_no.toString() == null || i_phone_no == null
				        || i_phone_no == '' || i_phone_no == undefined) {
					i_phone_no = '';
				}// Phone No

				if (!_logValidation(i_addr1)) {
					i_addr1 = ''
				}
				if (!_logValidation(i_addr2)) {
					i_addr2 = ''
				}
				if (!_logValidation(i_city)) {
					i_city = ''
				}
				if (!_logValidation(i_state)) {
					i_state = ''
				}
				if (!_logValidation(i_zip)) {
					i_zip = ''
				}
				if (!_logValidation(i_country)) {
					i_country = ''
				}

				var i_address_u = '';
				// var i_address_u =
				// i_addr1+'\n'+i_addr2+'\n'+i_city+','+i_state+'\n'+i_zip+'\n'+i_country;

				if (_logValidation(i_addr1)) {
					i_address_u = i_addr1 + '\n'
				}
				if (_logValidation(i_addr2)) {
					i_address_u += i_addr2 + '\n'
				}
				if (_logValidation(i_city)) {
					i_address_u += i_city + '\n'
				}
				if (_logValidation(i_state)) {
					i_address_u += i_state + '\n'
				}
				if (_logValidation(i_zip)) {
					i_address_u += i_zip + '\n'
				}
				if (_logValidation(i_country)) {
					i_address_u += i_country + '\n'
				}
				i_address_u = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address_u));

				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Customer
				// Address--> ' + i_customer_address);
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Phone No.
				// --> ' + i_phone_no);

				i_customer_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_customer_address));

				var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary')

				var i_subsidiary_text = o_recordOBJ.getFieldText('subsidiary')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Subsidiary
				// --> ' + i_subsidiary_text);
				
				if(nlapiStringToDate(i_tran_date) >= nlapiStringToDate('11/1/2017')){
				var i_address_rem = "Brillio LLC"
				i_address_rem = i_address_rem +"\n399 Thornall St\n1st Floor\nEdison NJ 08837";
				var i_adress_rem_ = "\n399 Thornall St\n1st Floor\nEdison NJ 08837";
				i_address_rem = nlapiEscapeXML(i_address_rem);
				var s_header_address = nlapiEscapeXML(i_adress_rem_);
				}
				else{
				var i_address_rem = nlapiEscapeXML(get_address_rem(i_subsidiary))
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address -->
				// ' + i_address_rem);
				var i_address = get_address(i_subsidiary)
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Address -->
				// ' + i_address);
				var s_header_address = header_adress(i_address,
				        i_subsidiary_text)
				}
				
				

				
				
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Header
				// Address --> ' + s_header_address);

				i_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_header_address));

				var i_billable_time_count = o_recordOBJ
				        .getLineItemCount('time')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item Count
				// --> ' + i_billable_time_count);
				var i_project_name;
				var i_project = o_recordOBJ.getFieldValue('job')

				if (_logValidation(i_project)) {
					var o_projectOBJ = nlapiLoadRecord('job', i_project);

					if (_logValidation(o_projectOBJ)) {
						i_project_name = o_projectOBJ
						        .getFieldValue('companyname')
						// nlapiLogExecution('DEBUG', 'suiteletFunction','
						// Project Name -->' + i_project_name);

					}// Customer OBJ
				}// Customer

				if (!_logValidation(i_project_name)) {
					i_project_name = '';
				}
				var i_currency = o_recordOBJ.getFieldText('currency')
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Currency -->
				// ' + i_currency);

				// var discount =
				// o_recordOBJ.getLineItemValue('item','description',1);
				// nlapiLogExecution('DEBUG', 'suiteletFunction', ' Currency -->
				// ' + i_currency);
				var discount_count = o_recordOBJ.getLineItemCount('item');
				var discount = o_recordOBJ.getLineItemValue('item',
				        'description', 1);
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' discount --> '
				        + discount);				
				var discount_amt = o_recordOBJ.getLineItemValue('item',
				        'amount', 1);
				nlapiLogExecution('DEBUG', 'suiteletFunction',
				        ' discount_amt --> ' + discount_amt);

				if (!_logValidation(i_currency)) {
					i_currency = '';
				}
				if (i_currency == 'USD') {
					s_currency_symbol = '$'
				}
				if (i_currency == 'INR') {
					s_currency_symbol = 'Rs.'
				}
				if (i_currency == 'GBP') {
					s_currency_symbol = '�'
				}
				if (i_currency == 'SGD') {
					s_currency_symbol = 'S$'
				}
				if (i_currency == 'Peso') {
					s_currency_symbol = 'P'
				}
               if(i_currency == 'CAD'){
                   s_currency_symbol ='c$'
                }
				// =================================== PDF Generation Code
				// ==================================

				var image_URL = constant.Images.CompanyLogo;
				image_URL = nlapiEscapeXML(image_URL)

				/*
				 * strVar += "<table border=\"0\" align=\"center\"
				 * width=\"100%\">";
				 * 
				 * strVar += "<tr>"; strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\" colspan=\"2\"><b>"+i_subsidiary_text+"<\/b><br/>"+i_address+"</td>";
				 * 
				 * if (image_URL != '' && image_URL != null && image_URL != '' &&
				 * image_URL != 'undefined' && image_URL != ' ' && image_URL !=
				 * '&nbsp') { strVar += " <td width=\"50%\" colspan=\"2\"><img
				 * width=\"146\" height=\"75\" align=\"right\" src=\"" +
				 * image_URL + "\"><\/img><\/td>"; } strVar += "</tr>";
				 * 
				 * 
				 * strVar += "</table>";
				 */

				strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";

				strVar += "<tr>";
				strVar += "<td border-bottom=\"0.3\" font-family=\"Helvetica\" style=\"color:#00CC00;font-size:35\" font-size=\"25\" width=\"100%\" colspan=\"2\"><b>Invoice<\/b></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
				strVar += "</tr>";

				i_tran_date = nlapiStringToDate(i_tran_date);

				var d_inv_date = get_date(i_tran_date);

				strVar += "<tr>"
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Invoice No. :&nbsp;<\/b>"
				        + i_tran_id + "</td>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Date :&nbsp;<\/b>"
				        + d_inv_date + "</td>";
				strVar += "</tr>";

				strVar += "<tr>"
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
				strVar += "</tr>";
				strVar += "</table>";

				strVar += "<table border=\"0\"  align=\"center\" width=\"100%\">";

				strVar += "<tr>";
				strVar += "<td height=\"70\" colspan=\"2\" border-top=\"0\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" width=\"100%\">"
				strVar += "<table border=\"0\" width=\"100%\">";

				strVar += "<tr>";
				strVar += "<td colspan=\"1\" border-top=\"0.3\" border-bottom=\"0.3\" border-left=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\">"

				strVar += "<table border=\"0\"  width=\"100%\">";
				strVar += "<tr>";
				strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b>Bill To :&nbsp;<\/b></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><PRE font-family=\"Helvetica\" font-size=\"9\">"
				        + i_bill_address + "</PRE></td>";
				strVar += "</tr>";

				var i_address_u = i_addr1 + '\n' + i_addr2 + '\n' + i_city
				        + ',' + i_state + '\n' + i_zip + '\n' + i_country;
				i_address_u = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address_u));

				/*
				 * if (_logValidation(i_customer_name)) { strVar += "<tr>"
				 * strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\">"+nlapiEscapeXML(i_customer_name)+"</td>";
				 * strVar += "</tr>"; }
				 * 
				 * if (_logValidation(i_attention)) { strVar += "<tr>"; strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(i_attention)+"</td>";
				 * strVar += "</tr>"; }
				 * 
				 * 
				 * if (_logValidation(i_addr1)) { strVar += "<tr>" strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_addr1)+"</td>";
				 * strVar += "</tr>"; } if (_logValidation(i_addr2)) { strVar += "<tr>"
				 * strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_addr2)+"</td>";
				 * strVar += "</tr>"; } if (_logValidation(i_city)) { strVar += "<tr>"
				 * strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_city)+"</td>";
				 * strVar += "</tr>"; } if (_logValidation(i_state)) { strVar += "<tr>"
				 * strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_state)+"</td>";
				 * strVar += "</tr>"; } if (_logValidation(i_zip)) { strVar += "<tr>"
				 * strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_zip)+"</td>";
				 * strVar += "</tr>"; } if (_logValidation(i_country)) {
				 * strVar += "<tr>" strVar += "<td border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b><\/b>"+nlapiEscapeXML(i_country)+"</td>";
				 * strVar += "</tr>"; } if (_logValidation(i_phone_no)) {
				 * strVar += "<tr>"; strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\">"+nlapiEscapeXML(i_phone_no)+"</td>";
				 * strVar += "</tr>"; }
				 */

				strVar += "</table>";

				strVar += "</td>";

				strVar += "<td height=\"70\" colspan=\"1\" border-top=\"0.3\" border-bottom=\"0.3\" border-left=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\">"
				strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";

				strVar += "<tr>";
				strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"><b>Remittance Details : <\/b></td>";
				strVar += "</tr>";

				/*
				 * if (_logValidation(s_remittance_details)) { strVar += "<tr>";
				 * strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">"+s_remittance_details+"</td>";
				 * strVar += "</tr>"; }
				 */

				if (_logValidation(i_address_rem)) {
					strVar += "<tr>";
					strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><PRE font-family=\"Helvetica\" font-size=\"9\">"
					        + i_address_rem + "</PRE></td>";
					strVar += "</tr>";
				}

				strVar += "</table>";

				/*
				 * strVar += "<table border=\"0\" align=\"center\"
				 * width=\"100%\">"; strVar += "<tr>"; strVar += "<td border-left=\"0\" border-top=\"0\" border-right=\"0\" font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Remittance
				 * Details : <\/b>"+s_remittance_details+"</td>"; strVar += "</tr>";
				 * 
				 * strVar += "</table>";
				 */
				strVar += "</td>";
				strVar += "</tr>";

				strVar += "</table>";
				strVar += "</td>"
				strVar += "</tr>";
				strVar += "</table>";

				strVar += "<table border=\"0\"  align=\"center\" width=\"100%\">";
				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Project Name :&nbsp;<\/b>"
				        + nlapiEscapeXML(i_project_name) + "</td>";
				strVar += "</tr>";

				strVar += "<tr>"
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>PO Number :&nbsp;<\/b>"
				        + nlapiEscapeXML(i_PO_ID) + "</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Terms of Payment :&nbsp;<\/b>"
				        + nlapiEscapeXML(i_terms) + "</td>";
				strVar += "</tr>";
				/*
				 * 
				 * strVar += "<tr>"; strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Billing
				 * Description:&nbsp;<\/b>"+nlapiEscapeXML(i_billing_description)+"</td>";
				 * strVar += "</tr>";
				 * 
				 */
				i_bill_from = nlapiStringToDate(i_bill_from);

				i_bill_from = get_date(i_bill_from);

				i_bill_to = nlapiStringToDate(i_bill_to);

				i_bill_to = get_date(i_bill_to);

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Billing Period:&nbsp;<\/b>&nbsp;Beginning&nbsp;"
				        + i_bill_from
				        + "&nbsp;thru&nbsp;"
				        + i_bill_to
				        + "</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Notes:<\/b>&nbsp;"
				        + s_memo + "</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>"
				strVar += "</tr>";

				strVar += "</table>";

				strVar += "<table border=\"0\"  align=\"center\" width=\"100%\">";

				strVar += "<tr>";
				// strVar += "<td colspan=\"1\" border-left=\"0\"
				// border-top=\"0.3\" border-bottom=\"0\"
				// font-family=\"Helvetica\" align=\"center\"
				// font-size=\"9\"></td>";
				strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				// strVar += "<td colspan=\"1\" border-left=\"0\"
				// border-top=\"0\" border-bottom=\"0\"
				// font-family=\"Helvetica\" align=\"center\"
				// font-size=\"9\"></td>";
				strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount("
				        + s_currency_symbol + ")<\/b></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				// strVar += "<td colspan=\"1\" border-left=\"0\"
				// border-top=\"0\" border-bottom=\"0.3\" align=\"center\"
				// font-family=\"Helvetica\" font-size=\"9\"><b>SL#<\/b></td>";
				strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "</tr>";

				if (_logValidation(i_billable_time_count)) {
					for (var i = 1; i <= i_billable_time_count; i++) {
						var i_date = o_recordOBJ.getLineItemValue('time',
						        'billeddate', i)

						var i_employee = o_recordOBJ.getLineItemValue('time',
						        'employeedisp', i)

						var i_project = o_recordOBJ.getLineItemValue('time',
						        'job', i)

						var i_item = o_recordOBJ.getLineItemValue('time',
						        'item', i)

						var i_rate = o_recordOBJ.getLineItemValue('time',
						        'rate', i)

						var i_hours = o_recordOBJ.getLineItemValue('time',
						        'quantity', i)

						var i_marked = o_recordOBJ.getLineItemValue('time',
						        'apply', i)

						/*
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Date
						 * --> ' + i_date);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', '
						 * Employee --> ' + i_employee);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', '
						 * Project --> ' + i_project);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item
						 * --> ' + i_item);
						 * 
						 * nlapiLogExecution('DEBUG', 'suiteletFunction', ' Rate
						 * --> ' + i_rate);
						 */

						// nlapiLogExecution('DEBUG', 'suiteletFunction', '
						// Hours --> ' + i_hours);
						if (i_marked == 'T') {
							var d_start_of_week = get_start_of_week(i_date)
							var d_end_of_week = get_end_of_week(i_date)

							var d_SOW_date = nlapiStringToDate(d_start_of_week);
							var d_EOW_date = nlapiStringToDate(d_end_of_week);

							d_SOW_date = get_date(d_SOW_date)

							d_EOW_date = get_date(d_EOW_date)

							// nlapiLogExecution('DEBUG', 'suiteletFunction', '
							// d_SOW_date --> ' + d_SOW_date);
							// nlapiLogExecution('DEBUG', 'suiteletFunction', '
							// d_EOW_date --> ' + d_EOW_date);

							if (i_item == 2222 || i_item == 2425
							        || i_item == 2221) // Amol: added 2221(FP)
							// item
							{
								a_employee_array[i_cnt++] = i_employee + '&&&'
								        + d_EOW_date + '&&&' + d_SOW_date
								        + '&&&' + i_rate;
								a_employee_name_array.push(i_employee);
							}// Time & Material

							// nlapiLogExecution('DEBUG', 'suiteletFunction', '
							// *******************Start & End Of Week
							// *************** --> [' + d_SOW_date + ' , ' +
							// d_EOW_date + ']');
						}

					}// Billable Loop
				}// Billable Line Count Loop

				a_employee_array = removearrayduplicate(a_employee_array)
				a_employee_array = a_employee_array.sort();
				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// ******************* Employee Array *************** -->'
				// +a_employee_array);

				a_employee_name_array = removearrayduplicate(a_employee_name_array)
				a_employee_name_array = a_employee_name_array.sort();
				// nlapiLogExecution('DEBUG', 'suiteletFunction', '
				// ******************* Employee Name Array *************** -->'
				// +a_employee_array);

				var i_employee_arr_n;
				var vb = 0;
				for (var k = 0; k < a_employee_array.length; k++) {
					var i_ST_rate = 0;
					var i_OT_rate = 0;
					var i_ST_units = 0;
					var i_OT_units = 0;
					var i_hours = 0
					var i_minutes = 0;
					var i_hours_ST = 0;
					var i_minutes_ST = 0;
					var i_hours_OT = 0;
					var i_minutes_OT = 0;
					var flag = 0;

					a_split_array = a_employee_array[k].split('&&&')

					var i_employee_arr = a_split_array[0]

					var i_end_of_week_arr = a_split_array[1]

					var i_start_of_week_arr = a_split_array[2]

					var i_rate_emp = a_split_array[3]

					for (var lt = 1; lt <= i_billable_time_count; lt++) {
						var i_date_new = o_recordOBJ.getLineItemValue('time',
						        'billeddate', lt)

						var i_employee_new = o_recordOBJ.getLineItemValue(
						        'time', 'employeedisp', lt)

						var i_project_new = o_recordOBJ.getLineItemValue(
						        'time', 'job', lt)

						var i_item_new = o_recordOBJ.getLineItemValue('time',
						        'item', lt)

						var i_rate_new = o_recordOBJ.getLineItemValue('time',
						        'rate', lt)

						var i_hours_new = o_recordOBJ.getLineItemValue('time',
						        'quantity', lt)

						var i_marked = o_recordOBJ.getLineItemValue('time',
						        'apply', lt)

						if (i_marked == 'T') {
							if ((i_employee_arr == i_employee_new)) {
								var d_start_of_week_new = get_start_of_week(i_date_new)
								var d_end_of_week_new = get_end_of_week(i_date_new)

								/*
								 * nlapiLogExecution('DEBUG',
								 * '-----------------------------------------------------');
								 * nlapiLogExecution('DEBUG', 'i_date_new', ' 1
								 * --> ' + i_date_new);
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 1 --> ' +
								 * d_SOW_date_B); nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 2 --> ' +
								 * d_EOW_date_B); nlapiLogExecution('DEBUG',
								 * 'i_hours_new', ' 2 --> ' + i_hours_new);
								 * nlapiLogExecution('DEBUG',
								 * '-----------------------------------------------------');
								 */

								var d_SOW_date_B = nlapiStringToDate(d_start_of_week_new);
								var d_EOW_date_B = nlapiStringToDate(d_end_of_week_new);

								/*
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', '3 --> ' + d_SOW_date_B);
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 4 --> ' +
								 * d_EOW_date_B);
								 * 
								 */
								d_SOW_date_B = get_date(d_SOW_date_B)
								d_EOW_date_B = get_date(d_EOW_date_B)

								/*
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction',' 5 --> ' + d_SOW_date_B);
								 * nlapiLogExecution('DEBUG',
								 * 'suiteletFunction', ' 6--> ' + d_EOW_date_B);
								 */

								if ((d_SOW_date_B == i_start_of_week_arr)
								        && (d_EOW_date_B == i_end_of_week_arr)
								        && (i_rate_emp == i_rate_new)) {

									/*
									 * nlapiLogExecution('DEBUG',
									 * '-----------------------------------------------------');
									 * nlapiLogExecution('DEBUG', 'i_date_new', '
									 * 1 --> ' + i_date_new);
									 * nlapiLogExecution('DEBUG',
									 * 'suiteletFunction', ' 1 --> ' +
									 * d_SOW_date_B); nlapiLogExecution('DEBUG',
									 * 'suiteletFunction', ' 2 --> ' +
									 * d_EOW_date_B); nlapiLogExecution('DEBUG',
									 * 'i_hours_new', ' 2 --> ' + i_hours_new);
									 * nlapiLogExecution('DEBUG', 'i_item_new', '
									 * 2 --> ' + i_item_new);
									 * nlapiLogExecution('DEBUG',
									 * '-----------------------------------------------------');
									 */

									if (flag != i_employee_arr) {
										i_employee_arr_n = i_employee_arr;
									} else {
										i_employee_arr_n = '';
									}
									var a_split_time_array = new Array();
									a_split_time_array = i_hours_new.split(':')
									var hours = a_split_time_array[0]
									var minutes = a_split_time_array[1]

									if (minutes == '0.00' || minutes == '0'
									        || minutes == 0) {
									} else {
										minutes = parseFloat(minutes) / 60;
										minutes = parseFloat(minutes)
										        .toFixed(2);
										// nlapiLogExecution('DEBUG', 'minutes',
										// ' ------ --> ' + minutes);
									}

									if (i_item_new == 2222
									        || i_item_new == 2221) // Amol:
									// added
									// 2221(FP)
									// item
									{
										i_hours_ST = parseFloat(i_hours_ST)
										        + parseFloat(hours)
										// i_minutes_ST =
										// parseFloat(i_minutes_ST) +
										// parseFloat(minutes);
										if (minutes == '0.75'
										        || minutes == '0.25') {
											// nlapiLogExecution('DEBUG',
											// 'i_minutes_ST', ' ------ --> ' +
											// i_minutes_ST);
											// nlapiLogExecution('DEBUG',
											// 'i_hours_ST', ' ------ --> ' +
											// i_hours_ST);
										}

										// i_minutes_ST=parseFloat(i_minutes_ST).toFixed(2);

										// i_minutes_ST=(parseFloat(i_minutes_ST)/60).toFixed(2);
										// nlapiLogExecution('DEBUG',
										// 'i_minutes_ST', ' ------ --> ' +
										// i_minutes_ST);

										i_hours_ST = parseFloat(i_hours_ST)
										        + parseFloat(minutes);
										i_hours_ST = parseFloat(i_hours_ST)
										        .toFixed(2);

										i_ST_rate = parseFloat(i_hours_ST)
										        * parseFloat(i_rate_new)
										i_ST_units = parseFloat(i_rate_new)
										        .toFixed(2);

									}// Time & Material

									if (i_item_new == 2425) {
										i_hours_OT = parseFloat(i_hours_OT)
										        + parseFloat(hours)
										// i_minutes_OT =
										// parseFloat(i_minutes_OT) +
										// parseFloat(minutes);

										// i_minutes_OT=parseFloat(i_minutes_OT).toFixed(2);

										// i_minutes_OT=(parseFloat(i_minutes_OT)/60).toFixed(2);

										i_hours_OT = parseFloat(i_hours_OT)
										        + parseFloat(minutes);

										i_hours_OT = parseFloat(i_hours_OT)
										        .toFixed(2);
										i_OT_rate = parseFloat(i_hours_OT);
										i_OT_units = parseFloat(i_rate_new)
										        .toFixed(2);

										/*
										 * nlapiLogExecution('DEBUG',
										 * '---------------------------------------------------');
										 * nlapiLogExecution('DEBUG', 'hours', ' -
										 * --> ' + hours);
										 * nlapiLogExecution('DEBUG',
										 * 'd_SOW_date_B', ' - --> ' +
										 * d_SOW_date_B);
										 * nlapiLogExecution('DEBUG',
										 * 'd_EOW_date_B', ' - --> ' +
										 * d_EOW_date_B);
										 * nlapiLogExecution('DEBUG',
										 * '---------------------------------------------------');
										 */

									}// OT Item

									// nlapiLogExecution('DEBUG',
									// 'suiteletFunction', ' 7 --> ' +
									// d_SOW_date_B);

								}// Start & End Date Validation
							}// Employee / Project / Item

						}// Marked T

					}// Billable Count

					i_amount = (parseFloat(i_hours_ST) * parseFloat(i_ST_units))
					        + (parseFloat(i_hours_OT) * parseFloat(i_OT_units));
					i_amount = parseFloat(i_amount);
					i_amount = i_amount.toFixed(2);

					i_total_amount_INV = parseFloat(i_total_amount_INV)
					        + parseFloat(i_amount);
					i_total_amount_INV = parseFloat(i_total_amount_INV);
					i_total_amount_INV = i_total_amount_INV.toFixed(2);

					var s_employee_addr_txt = '';
					s_employee_addr_txt = escape_special_chars(i_employee_arr);

					strVar += "			<tr>";

					if (flag == 0 && flag_name != i_employee_arr) {
						if (a_employee_name_array.indexOf(i_employee_arr) != -1) {
							vb++;
							// strVar += " <td border-bottom=\"0\"
							// border-left=\"0\" font-family=\"Helvetica\"
							// align=\"left\"
							// font-size=\"8\">"+parseInt(vb)+"<\/td>";

							strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
							        + s_employee_addr_txt + "<\/td>";
							flag = 1;
							flag_name = i_employee_arr
						}

					} else {
						// strVar += " <td border-bottom=\"0\" border-left=\"0\"
						// font-family=\"Helvetica\" align=\"left\"
						// font-size=\"8\"><\/td>";
						strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
					}

					// nlapiLogExecution('DEBUG', 'i_hours_OT', ' - --> ' +
					// i_hours_OT);

					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
					        + i_end_of_week_arr + "<\/td>";
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_ST_units) + "<\/td>";
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_hours_ST) + "<\/td>";
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_OT_units) + "<\/td>";// i_hours_OT//i_OT_rate
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">"
					        + parseFloat(i_hours_OT) + "<\/td>";// i_OT_units
					strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">"
					        + i_amount + "<\/td>";
					strVar += "			<\/tr>";

					pagecount++;

					if (pagecount == linenum && a_employee_array.length != '17')// if(pagecount
					// ==
					// '17'
					// &&
					// a_employee_array.length!=pagecount)
					{
						pagecount = 0;
						linenum = 40;
						strVar += "<\/table>";
						strVar += " <p style=\"page-break-after: always\"><\/p>";

						strVar += "<table border=\"0\" width=\"100%\">";
						// ----------------------------------------------------------

						strVar += "<tr>";
						// strVar += "<td colspan=\"1\" border-left=\"0\"
						// border-top=\"0.3\" border-bottom=\"0\"
						// font-family=\"Helvetica\" align=\"center\"
						// font-size=\"9\"></td>";
						strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount("
						        + s_currency_symbol + ")<\/b></td>";
						strVar += "</tr>";

						strVar += "<tr>";
						// strVar += "<td colspan=\"1\" border-left=\"0\"
						// border-top=\"0\" border-bottom=\"0.3\"
						// align=\"center\" font-family=\"Helvetica\"
						// font-size=\"9\"><b>SL#<\/b></td>";
						strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
						strVar += "</tr>";
						// ----------------------------------------------------------
					}
				}// Loop

				var all_tot_val = i_total_amount_INV;

				/*
				 * //----------------------------added by
				 * swati-----------------------------------------------------------------------
				 * 
				 * strVar += " <tr>"; strVar += "
				 * <td colspan=\"2\" border-bottom=\"0\" border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">'Total'<\/td>";
				 * strVar += "
				 * <td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
				 * strVar += "
				 * <td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
				 * strVar += "
				 * <td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
				 * strVar += "
				 * <td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";//i_hours_OT//i_OT_rate
				 * strVar += "
				 * <td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";//i_OT_units
				 * strVar += "
				 * <td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">"+all_tot_val+"<\/td>";
				 * strVar += " <\/tr>";
				 * //-----------------------------------------------------------------------------------------------------------
				 */
				// ----------------------------added by
				// swati-----------------------------------------------------------------------
				if (discount == null || discount == 'null') {
					discount = '';

				}
				if (discount_amt == null || discount_amt == 'null') {
					discount_amt = '';

				}
				for(var dis_ind=1;dis_ind<= discount_count;dis_ind++)
				{
				strVar += "			<tr>";
				strVar += "				<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">"
				        + o_recordOBJ.getLineItemValue('item',
				        'description', dis_ind) + "<\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_hours_OT//i_OT_rate
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_OT_units
				strVar += "				<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">"
				        +o_recordOBJ.getLineItemValue('item',
				        'amount',dis_ind ) + "<\/td>";
				strVar += "			<\/tr>";
				// -----------------------------------------------------------------------------------------------------------
				}
				if (_logValidation(i_discount_percent)
				        && i_discount_percent != 0) {
					strVar += "<tr>";
					// strVar += "<td colspan=\"1\" border-left=\"0\"
					// border-top=\"0\" border-bottom=\"0\" align=\"left\"
					// font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Less: Discount @"
					        + i_discount_percent + "</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "</tr>";

				}
				if (_logValidation(i_tax_percent) && i_tax_percent != 0) {
					strVar += "<tr>";
					// strVar += "<td colspan=\"1\" border-left=\"0\"
					// border-top=\"0\" border-bottom=\"0\" align=\"left\"
					// font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Add: Sales Tax PA state @"
					        + i_tax_percent + "</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "</tr>";

				}

				i_total_amount_INV = i_total_amount_INV
				        - parseInt(-discount_amt);
				i_total_amount_INV = i_total_amount_INV.toFixed(2);

				strVar += "<tr>";
				// strVar += "<td colspan=\"1\" border-left=\"0\"
				// border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\"
				// font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
				strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b> <\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total :<\/b></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
				strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>"
				        + s_currency_symbol
				        + "&nbsp;"
				        + formatDollar(parseFloat(i_total_amount_INV)
				                .toFixed(2)) + "<\/b></td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td border-right=\"0\" border-left=\"0\"  font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>"
				        + i_currency + "</td>";
				strVar += "</tr>";

				strVar += "<tr>";
				strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Amount In Words&nbsp;:&nbsp;<\/b>"
				        + toWordsFunc(
				                parseFloat(i_total_amount_INV).toFixed(2),
				                i_currency) + "</td>";
				strVar += "</tr>";
								

				strVar += "</table>";
								
				
	

			}// Record OBJ

		}// Record ID

		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
		xml += "<pdf>";
		xml += "<head>";
		xml += "  <macrolist>";
		xml += " <macro id=\"myfooter\">";
		// xml += "<i font-size=\"7\">";
		xml += " <table width=\"100%\">"
		xml += "<tr>"
		xml += "<td border-top=\"0.3\" font-size=\"9\"  font-family=\"Helvetica\" width=\"100%\" align=\"center\"> Brillio LLC. certifies that this invoice is correct, that payment has not been received and that it is presented with the understanding that<\/td>"
		xml += "<\/tr> "
		xml += "<tr>"
		xml += "<td border-bottom=\"0.3\" font-size=\"9\"  font-family=\"Helvetica\" width=\"100%\" align=\"center\">the amount to be paid thereunder can be audited to verify.<\/td>"
		xml += "<\/tr> "

		xml += "<tr width=\"100%\">";
		xml += "<td font-size='10' font-family='Times' align=\"right\" width=\"100%\"> Page <pagenumber size='2'/> of <totalpages size='2'/></td>";
		xml += "</tr>";

		xml += "<\/table> "
		// xml += "<\/i>";
		xml += "    <\/macro>";

		// --------------header repeat------added by swati---------------
		xml += " <macro id=\"myHead\">";
		xml += "<table border=\"0\" align=\"center\" width=\"100%\">";

		xml += "<tr>";
		xml += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\" colspan=\"2\"><b>"
		        + i_subsidiary_text + "<\/b><br/>" + i_address + "</td>";

		if (image_URL != '' && image_URL != null && image_URL != ''
		        && image_URL != 'undefined' && image_URL != ' '
		        && image_URL != '&nbsp') {
			// strVar += " <td width=\"50%\" colspan=\"2\"><img width=\"100\"
			// height=\"40\" align=\"right\" src=\"" + image_URL +
			// "\"><\/img><\/td>";//146,75
			xml += " <td width=\"50%\" colspan=\"2\"><img width=\"110\" height=\"45\" align=\"right\" src=\""
			        + image_URL + "\"><\/img><\/td>";// 146,75
		}

		// xml+="<td font-family=\"Helvetica\" font-size=\"9\"> Page <pagenumber
		// size=\"2\"/>";

		xml += "</tr>";
		xml += "</table>";
		xml += "    <\/macro>";

		// --------------------------------------------------------------
		xml += "  <\/macrolist>";
		xml += "<\/head>";

		xml += "<body margin-top=\"0pt\"  header=\"myHead\" header-height=\"30mm\" footer=\"myfooter\" footer-height=\"2em\">";
		xml += strVar;
		xml += "</body>\n</pdf>";

		// run the BFO library to convert the xml document to a PDF
		var file = nlapiXMLToPDF(xml);
		// Create PDF File with TimeStamp in File Name

		var d_currentTime = new Date();
		var timestamp = d_currentTime.getTime();
		// Create PDF File with TimeStamp in File Name

		var fileObj = nlapiCreateFile('BLLC T&M Group' + '.pdf', 'PDF', file
		        .getValue());
		fileObj.setFolder(62);// Folder PDF File is to be stored
		var fileId = nlapiSubmitFile(fileObj);
		// nlapiLogExecution('DEBUG', 'BLLC T&M Group', '************* PDF ID
		// *********** -->' + fileId);
		response.setContentType('PDF', 'BLLC T&M Group.pdf', 'inline');
		response.write(file.getValue());

	} catch (exception) {
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}

}

// END SUITELET ====================================================

// BEGIN OBJECT CALLED/INVOKING FUNCTION
// ===================================================

// return an array of date objects for start (monday)
// and end (sunday) of week based on supplied
// date object or current date
function get_start_of_week(date) {

	// nlapiLogExecution('DEBUG','date',' date -->'+date);
	// If no date object supplied, use current date
	// Copy date so don't modify supplied date
	var now = nlapiStringToDate(date)

	// set time to some convenient value
	now.setHours(0, 0, 0, 0);

	if (now.getDay() == '0') {

		// Get the previous Monday
		var monday = new Date(now);
		monday.setDate(monday.getDate() - 7 + 1);
		monday = nlapiDateToString(monday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> '
		// + monday);

		// Get next Sunday
		var sunday = new Date(now);
		sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
		sunday = nlapiDateToString(sunday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> '
		// + sunday);
		// Return array of date objects
		// nlapiLogExecution('DEBUG','date',' date -->'+date);
		// nlapiLogExecution('DEBUG','monday',' monday -->'+monday);

	} else {

		// Get the previous Monday
		var monday = new Date(now);
		monday.setDate(monday.getDate() - monday.getDay() + 1);
		monday = nlapiDateToString(monday)
		// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> '
		// + monday);

		// Get next Sunday
		var sunday = new Date(now);
		sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
		sunday = nlapiDateToString(sunday)

	}
	// nlapiLogExecution('DEBUG','date',' date -->'+date);
	// nlapiLogExecution('DEBUG','now.getDay()',now.getDay());
	return monday;
}

function get_end_of_week(date) {
	// If no date object supplied, use current date
	// Copy date so don't modify supplied date
	var now = nlapiStringToDate(date)

	// set time to some convenient value
	now.setHours(0, 0, 0, 0);
	var monday = new Date(now);
	var lastday = new Date(new Date(now).getFullYear(), new Date(now)
	        .getMonth() + 1, 0)
	// nlapiLogExecution('DEBUG','lastday',lastday.getDate());
	// nlapiLogExecution('DEBUG','monday.getDate()',monday.getDate());
	var f = parseInt(monday.getDate());
	var f1 = parseInt(lastday.getDate());
	// ----------------------------------------------------------------------
	if (f == f1) {
		var sunday = date;

		// nlapiLogExecution('DEBUG','inside if');
	} else {
		// nlapiLogExecution('DEBUG','now.getDay()', now.getDay());
		if (now.getDay() == '0') {
			// Get the previous Monday

			monday.setDate(monday.getDate() - monday.getDay() + 1);
			monday = nlapiDateToString(monday)

			// Get next Sunday
			var sunday = new Date(now);
			sunday.setDate(sunday.getDate() - 7 + 7);
			sunday = nlapiDateToString(sunday)

		} else {
			// Get the previous Monday
			// var monday = new Date(now);
			monday.setDate(monday.getDate() - monday.getDay() + 1);
			monday = nlapiDateToString(monday)
			// nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday
			// --> ' + monday);

			// Get next Sunday
			var sunday = new Date(now);
			sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
			// nlapiLogExecution('DEBUG','sunday', ' sunday --> ' + sunday);
			// ------------------------------------------------------------------
			if (sunday > lastday) {
				// nlapiLogExecution('DEBUG','inside second if');
				sunday = lastday;

			}
			// -----------------------------------------------------------------
			sunday = nlapiDateToString(sunday)

		}
		// --------------------------------------------------------------------------------------

	}
	return sunday;
}

/**
 * 
 * @param {Object}
 *            value
 * 
 * Description --> If the value is blank /null/undefined returns false else
 * returns true
 */
function _logValidation(value) {
	if (value != 'null' && value != null && value.toString() != null
	        && value != '' && value != undefined
	        && value.toString() != undefined && value != 'undefined'
	        && value.toString() != 'undefined' && value.toString() != 'NaN'
	        && value != NaN) {
		return true;
	} else {
		return false;
	}
}
function get_customer_details(i_customer) {
	var i_address;
	var i_attention;
	var i_service_tax_reg_no;
	var i_CIN_No;
	var i_phone = '';
	var a_return_array = new Array();
	if (_logValidation(i_customer)) {
		var o_customerOBJ = nlapiLoadRecord('customer', i_customer)

		if (_logValidation(o_customerOBJ)) {
			i_address = o_customerOBJ.getFieldValue('defaultaddress')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Address -->'
			// + i_address);

			i_service_tax_reg_no = o_customerOBJ.getFieldValue('vatregnumber')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Service Tax
			// Reg No. -->' + i_service_tax_reg_no);

			i_CIN_No = o_customerOBJ.getFieldValue('custentity_cinnumber')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' CIN No .
			// -->' + i_CIN_No);

			i_phone = o_customerOBJ.getFieldValue('phone')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Phone No .
			// -->' + i_phone);

			var i_addr_count = o_customerOBJ.getLineItemCount('addressbook')
			// nlapiLogExecution('DEBUG', 'get_customer_address', ' Adress Count
			// -->' + i_addr_count);

			if (_logValidation(i_addr_count)) {
				for (var r = 1; r <= i_addr_count; r++) {
					var i_default_billing = o_customerOBJ.getLineItemValue(
					        'addressbook', 'defaultbilling', r)
					// nlapiLogExecution('DEBUG', 'get_customer_address', '
					// Adress Count -->' + i_addr_count);

					if (i_default_billing == 'T') {
						i_attention = o_customerOBJ.getLineItemValue(
						        'addressbook', 'attention', r);
						nlapiLogExecution('DEBUG', 'beforeSubmitRecord',
						        ' Attention -->' + i_attention);

						i_addr1 = o_customerOBJ.getLineItemValue('addressbook',
						        'addr1', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Addr1 -->' + i_addr1);

						i_addr2 = o_customerOBJ.getLineItemValue('addressbook',
						        'addr2', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Addr2 -->' + i_addr2);

						i_city = o_customerOBJ.getLineItemValue('addressbook',
						        'city', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// City -->' + i_city);

						i_zip = o_customerOBJ.getLineItemValue('addressbook',
						        'zip', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Zip -->' + i_zip);

						i_country = o_customerOBJ.getLineItemText(
						        'addressbook', 'country', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// Country -->' + i_country);

						i_state = o_customerOBJ.getLineItemValue('addressbook',
						        'displaystate', r);
						// nlapiLogExecution('DEBUG', 'beforeSubmitRecord', '
						// State -->' + i_state);

						break;
					}// Default Billing
				}// Address Loop
			}// Address Count
			a_return_array[0] = i_address + '######' + i_attention + '######'
			        + i_service_tax_reg_no + '######' + i_CIN_No + '######'
			        + i_phone + '######' + i_addr1 + '######' + i_addr2
			        + '######' + i_city + '######' + i_zip + '######'
			        + i_country + '######' + i_state

		}

	}

	return a_return_array;
}
function formatAndReplaceSpacesofMessage(messgaeToBeSendPara) {
	if (messgaeToBeSendPara != null && messgaeToBeSendPara != ''
	        && messgaeToBeSendPara != undefined) {
		messgaeToBeSendPara = messgaeToBeSendPara.toString();

		messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g, "<br/>");// / /g
		return messgaeToBeSendPara;
	}

}

function get_address(i_subsidiary) {
	var i_address;
	if (_logValidation(i_subsidiary)) {
		var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)

		if (_logValidation(o_subsidiaryOBJ)) {
			i_address = o_subsidiaryOBJ.getFieldValue('addrtext')

		}
	}

	return i_address;
}

function get_address_rem(i_subsidiary) {
	var i_address;
	if (_logValidation(i_subsidiary)) {
		var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary)

		if (_logValidation(o_subsidiaryOBJ)) {
			i_address_rem = o_subsidiaryOBJ.getFieldValue('mainaddress_text')
		}
	}
	return i_address_rem;
}

function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}
function get_employeeID(i_employee) {
	var i_internal_id;
	if (_logValidation(i_employee)) {
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employee);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');

		var a_search_results = nlapiSearchRecord('employee', null, filter,
		        columns);

		if (a_search_results != null && a_search_results != ''
		        && a_search_results != undefined) {
			i_internal_id = a_search_results[0].getValue('internalid');

		}

	}// Employee
	return i_internal_id;
}
// --------------------------------Function Begin - Amount in
// words----------------------------------------------------------------//
function toWordsFunc(s, i_currency) {
	var str = '';
	if (i_currency == 'USD') {
		str = 'Dollar' + ' '
	}
	if (i_currency == 'INR') {
		str = 'Rs.' + ' '
	}
	if (i_currency == 'GBP') {
		str = '�' + ' '
	}
	if (i_currency == 'SGD') {
		str = 'S$' + ' '
	}
	if (i_currency == 'Peso') {
		str = 'P' + ' '
	}

	return str + toWords(s);
	var th = new Array('Billion ', 'Million ', 'Thousand ', 'Hundred ');
	var dg = new Array('1000000000', '1000000', '1000', '100');
	var dem = s.substr(s.lastIndexOf('.') + 1)
	s = parseInt(s)
	var d
	var n1, n2
	while (s >= 100) {
		for (var k = 0; k < 4; k++) {
			d = parseInt(s / dg[k])
			if (d > 0) {
				if (d >= 20) {
					n1 = parseInt(d / 10)
					n2 = d % 10
					printnum2(n1)
					printnum1(n2)
				} else
					printnum1(d)
				str = str + th[k]
			}
			s = s % dg[k]
		}
	}
	if (s >= 20) {
		n1 = parseInt(s / 10)
		n2 = s % 10
	} else {
		n1 = 0
		n2 = s
	}

	printnum2(n1)
	printnum1(n2)
	if (dem > 0) {
		decprint(dem)
	}
	return str

	function decprint(nm) {
		if (nm >= 20) {
			n1 = parseInt(nm / 10)
			n2 = nm % 10
		} else {
			n1 = 0
			n2 = parseInt(nm)
		}
		str = str + 'And '
		printnum2(n1)
		printnum1(n2)
	}

	function printnum1(num1) {
		switch (num1) {
			case 1:
				str = str + 'One '
			break;
			case 2:
				str = str + 'Two '
			break;
			case 3:
				str = str + 'Three '
			break;
			case 4:
				str = str + 'Four '
			break;
			case 5:
				str = str + 'Five '
			break;
			case 6:
				str = str + 'Six '
			break;
			case 7:
				str = str + 'Seven '
			break;
			case 8:
				str = str + 'Eight '
			break;
			case 9:
				str = str + 'Nine '
			break;
			case 10:
				str = str + 'Ten '
			break;
			case 11:
				str = str + 'Eleven '
			break;
			case 12:
				str = str + 'Twelve '
			break;
			case 13:
				str = str + 'Thirteen '
			break;
			case 14:
				str = str + 'Fourteen '
			break;
			case 15:
				str = str + 'Fifteen '
			break;
			case 16:
				str = str + 'Sixteen '
			break;
			case 17:
				str = str + 'Seventeen '
			break;
			case 18:
				str = str + 'Eighteen '
			break;
			case 19:
				str = str + 'Nineteen '
			break;
		}
	}

	function printnum2(num2) {
		switch (num2) {
			case 2:
				str = str + 'Twenty '
			break;
			case 3:
				str = str + 'Thirty '
			break;
			case 4:
				str = str + 'Forty '
			break;
			case 5:
				str = str + 'Fifty '
			break;
			case 6:
				str = str + 'Sixty '
			break;
			case 7:
				str = str + 'Seventy '
			break;
			case 8:
				str = str + 'Eighty '
			break;
			case 9:
				str = str + 'Ninety '
			break;
		}

	}
}
function toWords(s) {

	var th = [ '', 'thousand', 'million', 'billion', 'trillion' ];
	var dg = [ 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
	        'eight', 'nine' ];
	var tn = [ 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen',
	        'sixteen', 'seventeen', 'eighteen', 'nineteen' ];
	var tw = [ 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
	        'eighty', 'ninety' ];

	s = s.toString();
	s = s.replace(/[\, ]/g, '');
	if (s != parseFloat(s))
		return 'not a number';
	var x = s.indexOf('.');
	if (x == -1)
		x = s.length;
	if (x > 15)
		return 'too big';
	var n = s.split('');
	var str = '';
	var sk = 0;
	for (var i = 0; i < x; i++) {
		if ((x - i) % 3 == 2) {
			if (n[i] == '1') {
				str += tn[Number(n[i + 1])] + ' ';
				i++;
				sk = 1;
			} else if (n[i] != 0) {
				str += tw[n[i] - 2] + ' ';
				sk = 1;
			}
		} else if (n[i] != 0) {
			str += dg[n[i]] + ' ';
			if ((x - i) % 3 == 0)
				str += 'hundred ';
			sk = 1;
		}

		if ((x - i) % 3 == 1) {
			if (sk)
				str += th[(x - i - 1) / 3] + ' ';
			sk = 0;
		}
	}
	if (x != s.length) {
		var y = s.length;
		str += 'point ';
		for (var i = x + 1; i < y; i++)
			str += dg[n[i]] + ' ';
	}
	return str.replace(/\s+/g, ' ');
}

// --------------------------------------------Function End - Amount in
// Words----------------------------------//

function formatDollar(somenum) {
	var split_arr = new Array()
	var i_no_before_comma;
	var i_no_before_comma_length;

	if (somenum != null && somenum != '' && somenum != undefined) {
		split_arr = somenum.toString().split('.')
		i_no_before_comma = split_arr[0]
		i_no_before_comma = Math.abs(i_no_before_comma)

		if (i_no_before_comma.toString().length <= 3) {
			return somenum;
		} else {
			var p = somenum.toString().split(".");

			if (p[1] != null && p[1] != '' && p[1] != undefined) {
				p[1] = p[1]
			} else {
				p[1] = '00'
			}
			return p[0].split("").reverse().reduce(
			        function(acc, somenum, i, orig) {
				        return somenum + (i && !(i % 3) ? "," : "") + acc;
			        }, "")
			        + "." + p[1];
		}
	}
}
function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function get_date(date1) {
	var today;
	// ============================= Todays Date
	// ==========================================

	var offsetIST = 5.5;

	// To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime()
	        + (date1.getTimezoneOffset() * 60000));

	// Then cinver the UTS date to the required time zone offset like back to
	// 5.5 for IST
	//var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	var istdate = date1;
	var day = istdate.getDate();

	var month = istdate.getMonth() + 1;

	var year = istdate.getFullYear();

	var date_format = checkDateFormat();
	// nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format =='
	// +date_format);

	if (month == 1) {
		month = '01'
	} else if (month == 2) {
		month = '02'
	} else if (month == 3) {
		month = '03'
	}

	else if (month == 4) {
		month = '04'
	} else if (month == 5) {
		month = '05'
	} else if (month == 6) {
		month = '06'
	} else if (month == 7) {
		month = '07'
	} else if (month == 8) {
		month = '08'
	} else if (month == 9) {
		month = '09'
	}
	if (day == 1) {
		day = '01'
	} else if (day == 2) {
		day = '02'
	} else if (day == 3) {
		day = '03'
	}

	else if (day == 4) {
		day = '04'
	} else if (day == 5) {
		day = '05'
	} else if (day == 6) {
		day = '06'
	} else if (day == 7) {
		day = '07'
	} else if (day == 8) {
		day = '08'
	} else if (day == 9) {
		day = '09'
	}
	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {   //M/D/YYYY
		today = month + '/' + day + '/' + year;
	}
	if (date_format == 'M/D/YYYY') {   //M/D/YYYY
		today = month + '/' + day + '/' + year;
	}
	return today;
}
function header_adress(text, subsidiary) {
	var iChars = subsidiary;
	var text_new = ''

	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			text_new += text[y]
			if (text_new == subsidiary) {
				text_new = '';
			}// Break
		}// Loop
	}// Text Validation

	nlapiLogExecution('DEBUG', 'header_adress', ' text_new -->' + text_new);
	return text_new;
}// Header Address

function escape_special_chars(text) {
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.1234567890";
	var text_new = ''

	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			if (iChars.indexOf(text[y]) == -1) {
				text_new += text[y]
			}

		}// Loop
	}// Text Validation
	return text_new;
}
// END OBJECT CALLED/INVOKING FUNCTION
// =====================================================
