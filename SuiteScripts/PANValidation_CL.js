// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

/*
 Script Name: Client Script for PAN validaton.
 Author: Prashant Lokhande.
 Company: Inspirria Cloud Tech Pvt. ltd.
 Date:02-05-2017
 Description:This Client Script fetches PAN ID entered by user & Validate it.


 Script Modification Log:

 -- Date --      -- Modified By --       --Requested By--        -- Description --
 02-MAY-2017        Prashant Lokhande           --                  Created



 Below is a summary of the process controls enforced by this script file.  The control logic is described
 more fully, below, in the appropriate function headers and code blocks.

 */
// BEGIN PAGE INIT ==================================================
// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...
	var operationType;
	var a_subisidiary = new Array;
	var i_vatCode;
	var i_taxcode;
	var s_pass_code;
	var ad_GST_Liable = 'F';
	var b_Liable_Check = 0;
	var ad_GST_Number;

}

function pageInit_Set_GST_Liable(type) {
	/*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

	 */

	//  LOCAL VARIABLES

	//  PAGE INIT CODE BODY
	operationType = type;

	var i_AitGlobalRecId = SearchGlobalParameter();
	nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);

	//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
	if (i_AitGlobalRecId != 0) {
		var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);

		a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
		nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);

		i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
		nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);

		i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
		nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);

		s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
	} // END if(i_AitGlobalRecId != 0 )
	//  PAGE INIT CODE BODY

	var Flag = 0;
	Flag = isindia_subsidiary(a_subisidiary, s_pass_code);
 	if (type == 'create')
    {
	  nlapiDisableField('custentity_iit_is_nri',true);
    } 
	if (Flag == 1 && type == 'edit') {
		var b_GST_Liable = nlapiGetFieldValue('custentity_iit_gst_liable');
		if (_logValidation(b_GST_Liable) && b_GST_Liable == 'T')
		{
			nlapiSetFieldValue('custentity_iit_gst_liable', 'T');
		} 
		else if (_logValidation(b_GST_Liable) && b_GST_Liable == 'F')
		{
			nlapiSetFieldValue('custentity_iit_gst_liable', 'F');           
		}
	}

}

// END PAGE INIT ====================================================

//Begin Save Record ====================================================

function saveRecord_GST_Liable_Check() {
	/*  On save record:

	- PURPOSE

	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


	 */

	//  SAVE RECORD CODE BODY
	var Flag = 0;
	var subrecord;
	var addr_Country;
	Flag = isindia_subsidiary(a_subisidiary, s_pass_code);
	//	alert(Flag);
	//var vendorUrl = window.location.href;
	//	alert(vendorUrl);

	//==== IF SUBSIDIARY IS INDIAN SUBSIDIARY ====
	if (Flag == 1) {
		var b_GST_Liable = nlapiGetFieldValue('custentity_iit_gst_liable');
		var lineCount = nlapiGetLineItemCount('addressbook');

		var typeVendor = nlapiGetFieldValue('custentity_iit_gst_type_of_entity');
		var panId = nlapiGetFieldValue('custentity_vendor_panno');
		var isNRIVendorCheck = nlapiGetFieldValue('custentity_iit_is_nri');
		if (typeVendor == 1 && isNRIVendorCheck == 'F') {
			if (!_logValidation(panId)) {
				alert('Please Enter PAN');
				return false;
			}
		}
		//alert('lineCount'+lineCount);
		if (lineCount > 0) {
			for (var i = 1; i <= lineCount; i++) {

				subrecord = nlapiViewLineItemSubrecord('addressbook','addressbookaddress', i);
				if(_logValidation(subrecord))
				{
					ad_GST_Number = subrecord.getFieldValue('custrecord_iit_address_gstn_uid');
					addr_Country = subrecord.getFieldValue('country');
					//alert('SaveRecord'+addr_Country);
				}

				if (_logValidation(ad_GST_Number) && addr_Country =='IN') {
					//var customrecord_taxcompanyinfoSearch = nlapiSearchRecord("customrecord_taxcompanyinfo",null,null,[new nlobjSearchColumn("custrecord_permanent_accountno",null,null)]);
					//panNumber = customrecord_taxcompanyinfoSearch[0].getValue('custrecord_permanent_accountno');
					b_PAN_Id = nlapiGetFieldValue('custentity_vendor_panno');

					//i_State_Number = ad_GST_Number.substring(0, 2);

					var i_Pan_subString = ad_GST_Number.substring(2, 12);

					if (_logValidation(b_PAN_Id) && i_Pan_subString != b_PAN_Id)
					{
						alert('please check PAN is not matched with GSTIN');
						return false;
					}
				}

				if (b_GST_Liable == 'T') 
				{
					subrecord = nlapiViewLineItemSubrecord('addressbook','addressbookaddress', i);
					if(_logValidation(subrecord))
					{
						ad_GST_Liable = subrecord.getFieldValue('custrecord_iit_gst_liable_address');

						b_Liable_Check = 1;
						if (ad_GST_Liable == 'T') 
						{
							b_Liable_Check = 1;
							break;
						} 
						else if (ad_GST_Liable == 'F') 
						{
							b_Liable_Check = 0;
						}
					}
					else
					{
						alert('Please enter atleast One Address');
						return false;
					}
				} //end if
				else
				{
					subrecord = nlapiViewLineItemSubrecord('addressbook','addressbookaddress', i);
					if(!_logValidation(subrecord))
					{
						alert('Please enter atleast One Address');
						return false;
					}
				}
			}//end for

			//if(_logValidation(ad_GST_Liable) && ad_GST_Liable == 'F' && b_Liable_Check == 2)
			if (b_GST_Liable == 'T' && b_Liable_Check == 0) 
			{
				alert('Please enter atleast One Address as GST is Liable');
				return false;
			}
		} 
		else if (lineCount == 0) 
		{
			alert('Please enter atleast One Address');
			return false;

		} else {
			return true;
		}

	}// end Subsidiary Flag	

	return true;
}

// END SAVE RECORD ==================================================

/*FieldChanged Event
 - ValidatePAN()
 */

function ValidatePAN_FieldChange(type, name) {

	//Script Block For Vendor Record Field Changed	

	if (name === 'custentity_iit_gst_type_of_entity') {
		var panId = nlapiGetFieldValue('custentity_vendor_panno');
		var typeVendor = nlapiGetFieldValue('custentity_iit_gst_type_of_entity');
		if (typeVendor == 1) {
			if (!_logValidation(panId)) {
				alert('Please Enter PAN');				
			}
			nlapiDisableField('custentity_iit_is_nri',false);
		}
		else
		{
			nlapiDisableField('custentity_iit_is_nri',true);
		}
	}

	if (name === 'custentity_vendor_panno')
	{
		var panId = nlapiGetFieldValue('custentity_vendor_panno');

		if (_logValidation(panId)) 
		{
			var panPat = /^([A-Z]{5})(\d{4})([A-Z]{1})$/;
			if (panId.search(panPat) == -1) {
				//	alert("This is Invalid Pan No. Please Enter Correct Pan No");
				alert('This is Invalid Pan No. Please Enter Correct Pan No ,\n * PAN Card Number Length 10 Character \n * First Five Character from A to Z \n * Four Numeric 0 to 9 \n * Last One Character from A to Z');

				nlapiSetFieldValue('custentity_vendor_panno', '', false, false);
			}
		}
	}

}
/*if(name == 'custentity_iit_gst_liable')
{
	 var b_GST_Liable = nlapiGetFieldValue('custentity_iit_gst_liable');
	 if(_logValidation(b_GST_Liable) && b_GST_Liable == 'T')
	 {
		  var subrecord = nlapiCreateCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		  subrecord.setFieldValue('custrecord_iit_gst_liable_address', 'T');
	 }  
	 else if(_logValidation(b_GST_Liable) && b_GST_Liable == 'F')
	 {
		  var subrecord = nlapiCreateCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		  subrecord.setFieldValue('custrecord_iit_gst_liable_address', 'F');
	 }
}*/

//END Field Change ================================================

//Begin VALIDATE LINE ================================================
function AddressValidateLine(type)
{
	var subrecord;

	if (type == 'addressbook') 
	{
		//var flag=0
		var b_GST_Liable = nlapiGetFieldValue('custentity_iit_gst_liable');
		var isNRIVendorCheck = nlapiGetFieldValue('custentity_iit_is_nri');

		var lineCount = nlapiGetLineItemCount('addressbook');

		subrecord = nlapiViewCurrentLineItemSubrecord('addressbook','addressbookaddress');
		if(isNRIVendorCheck != 'T')
		{
			if(_logValidation(subrecord))
			{
				ad_GST_Number = subrecord.getFieldValue('custrecord_iit_address_gstn_uid');
				addr_Country = subrecord.getFieldValue('country');
				//alert('ValidateLine'+addr_Country);
			}
			if (_logValidation(ad_GST_Number) && addr_Country =='IN') 
			{
				//var customrecord_taxcompanyinfoSearch = nlapiSearchRecord("customrecord_taxcompanyinfo",null,null,[new nlobjSearchColumn("custrecord_permanent_accountno",null,null)]);
				//panNumber = customrecord_taxcompanyinfoSearch[0].getValue('custrecord_permanent_accountno');
				b_PAN_Id = nlapiGetFieldValue('custentity_vendor_panno');

				var i_Pan_subString = ad_GST_Number.substring(2, 12);

				if (_logValidation(b_PAN_Id) && i_Pan_subString != b_PAN_Id)
				{
					alert('please check PAN is not matched with GSTIN');
					return false;
				}
			}

			if (lineCount > 0)
			{
				for (var i = 1; i <= lineCount; i++)
				{
					if (b_GST_Liable == 'T') 
					{
						subrecord = nlapiViewLineItemSubrecord('addressbook','addressbookaddress', i);
						if(_logValidation(subrecord))
						{
							ad_GST_Liable = subrecord.getFieldValue('custrecord_iit_gst_liable_address');
						}

						if (_logValidation(ad_GST_Liable) && ad_GST_Liable == 'T')
						{
							b_Liable_Check = 1;
							break;
						} 
						else if (_logValidation(ad_GST_Liable)&& ad_GST_Liable == 'F') 
						{
							b_Liable_Check = 0;
						}
					} //end if
				}//end for (var i = 1; i <= lineCount; i++)

			//if(_logValidation(ad_GST_Liable) && ad_GST_Liable == 'F' && b_Liable_Check == 2)
				if (b_GST_Liable == 'T' && b_Liable_Check == 0) 
				{
					alert('You Need to select GSTIN Liable CheckBox on AddressForm');
					return false;
				}
			}// end if (lineCount > 0)
		}// end if(isNRIVendorCheck != 'T')
   	}// end if (type == 'addressbook')
	return true;
}

// END VALIDATE LINE ================================================

function _logValidation(value) {
	if (value != 'null' && value != null && value != null && value != ''&& value != undefined && value != undefined && value != 'undefined'&& value != 'undefined' && value != 'NaN' && value != NaN) 
	{
		return true;
	} 
	else 
	{
		return false;
	}
}

function SearchGlobalParameter() {

	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;

	a_column.push(new nlobjSearchColumn('internalid'));

	var s_serchResult = nlapiSearchRecord(
			'customrecord_indian_tax_glo_parameter', null, null, a_column)

	if (s_serchResult != null && s_serchResult != undefined&& s_serchResult != '') {
		for (var i = 0; i < s_serchResult.length; i++) 
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);

		}

	}
	return i_globalRecId;
}
/*

 function ValidatePAN_CL(type,name) 
 { 
 //alert("Client Script Called..");
 var record = nlapiGetRecordType();

 //Script Block For Customer Record
 if(record==='customer')
 {
 if(name==='custentity_iit_customer_pan_no')
 {
 var panId = nlapiGetFieldValue('custentity_iit_customer_pan_no');
 alert(panId);
 if (panId != "") 
 {           
 var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
 if (panId.search(panPat) == -1)
 {
 alert("Invalid Pan No");
 }
 else
 {
 alert("Correct Pan No");
 }
 }
 }
 }//if End

 //Script Block For Vendor Record
 else
 {
 if(name==='custentity_vendor_panno')
 {
 var panId = nlapiGetFieldValue('custentity_vendor_panno');
 alert(panId);
 if (panId != "") 
 {           
 var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
 if (panId.search(panPat) == -1)
 {
 alert("Invalid Pan No");
 }
 else
 {
 alert("Correct Pan No");
 }
 }
 }
 }//End Else
 } */