/** 
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */

// Load two standard modules.
define ( ['N/record', 'N/ui/serverWidget'] ,
    
    // Add the callback function.
    function(record, serverWidget) {
        // In the beforeLoad function, disable the Notes field.
        function myBeforeLoad (context) {
           //if (context.type !== context.UserEventType.EDIT)
                //   return;
            var form = context.form;
              var rateField = form.getField({
                   id: 'exchangerate'
            });
       /*   var customer = form.getValue({
            fieldId : 'entity'
          });
          if(customer !== 7899 ||customer !==153017 )
            {
              rateField.updateDisplayType({
                displayType: serverWidget.FieldDisplayType.DISABLED
            });
          }*/
        }
        
        // In the beforeSubmit function, add test to the Notes field.
        function myBeforeSubmit(context) {
            
        }
        
        // In the afterSubmit function, begin creating a task record.
        function myAfterSubmit(context) {
          
        }
        
        // Add the return statement that identifies the entry point funtions.
        return {
            beforeLoad: myBeforeLoad,
            //beforeSubmit: myBeforeSubmit,
            //afterSubmit: myAfterSubmit
        };   
    });

/******** Log Validation Function********/
function _logValidation(value){
    if (value != 'null' && value != '' && value != undefined && value != 'NaN') {
        return true;
    }
    else {
        return false;
    }
}