
var config  = {
  "legalEntity": {
    "Brillio Technologies Private Limited": "3",
    "Brillio LLC": "2",
    "Brillio Technologies Private Limited UK" : "7",
    "BRILLIO UK LIMITED": "7",
    "Brillio Canada Inc." : "10"	    
  }

}


function encryptSalary(type){
	
	        if(type == "delete"){
				return;
			}
	
		    var recId = nlapiGetRecordId();
			var recType = nlapiGetRecordType();
			var recordObject = nlapiLoadRecord(recType,recId);
			
			var fusion_id = recordObject.getFieldValue('custrecord_cost_p_fte_fusion_id');
			
			nlapiLogExecution('Debug','fusion ',fusion_id);
			var subsidiary_hidden = recordObject.getFieldValue('custrecord_subsidiary_hidden');
			
			nlapiLogExecution('Debug','sub hidden ',subsidiary_hidden);
			
			var subsidiary_id = config["legalEntity"][subsidiary_hidden];
			
			nlapiLogExecution('Debug','subsidiary ',subsidiary_id);
			recordObject.setFieldValue('custrecord_cost_p_fte_legal_entity',subsidiary_id);
			
			
			
			var employee_search = nlapiSearchRecord("employee",null,
									[["custentity_employee_inactive", "is", "F"],
									  "AND",
									 ["custentity_implementationteam", "is", "F"],
									  "AND",
									  ["custentity_fusion_empid", "is", fusion_id],
									  "AND",
									  ["subsidiary", "is", subsidiary_id],
									 ],
									  [ new nlobjSearchColumn("subsidiarynohierarchy"), new nlobjSearchColumn("custentity_fusion_empid"), new nlobjSearchColumn("internalid","subsidiary",null)]);
            var empId = "";
			
			if(employee_search){
			
            for(var i = 0; i < employee_search.length; i++){
						
						if(fusion_id == employee_search[i].getValue('custentity_fusion_empid') && subsidiary_id == employee_search[i].getValue("subsidiarynohierarchy")){
							
							empId = employee_search[i].getId();
							break;
						}
						
					}
            }
					
					nlapiLogExecution('Debug','Emp ',empId);
					
					recordObject.setFieldValue('custrecord_cost_p_fte_employee',empId);									  
			
			var ctc = recordObject.getFieldValue('custrecord_cost_p_fte_ctc');
			
			var ctc_encrypt = encrypt(ctc);
			recordObject.setFieldValue('custrecord_cost_p_fte_ctc_encrypted',ctc_encrypt);
			
			var salaryAmount = recordObject.getFieldValue('custrecord_cost_p_fte_salary_amount');
			
			var salary_encrypt = encrypt(salaryAmount);			
			recordObject.setFieldValue('custrecord_cost_p_fte_salary_encrypted',salary_encrypt);
					
			var recObj = nlapiSubmitRecord(recordObject);
			
			nlapiLogExecution('Debug','record encrpted ',recObj);
		
		
		

	
	
}

function encrypt(data){
	
	var encrypted_data = nlapiEncrypt(data, "aes", "7442A2900DA9A2C7E537BA94F939F191");//48656C6C6F0B0B0B0B0B0B0B0B0B0B0B  // 
	nlapiLogExecution('debug', 'encrypted data: ', encrypted_data);                    
	return encrypted_data;
	
}