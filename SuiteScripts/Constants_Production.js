/**
 * Constant file for Production
 * 
 * Version Date Author Remarks 1.00 11 Feb 2015 nitish.mishra
 * 
 */

var constant = {

    Mail_Author : {
        InformationSystem : '442',
        InformationSystems : '442',
        TalentAcquisition : 37276
    },

    Folder : {
	    WorkOrderExtension : '87701'
    },

    Role : {
        Administrator : 3,
        TimeSheet : 1068,
        ResourceManager_RMG : 1047,
        FullAccess : 18,
      businessOps: 1142
    },

    Employee : {
        InformationSystem : '442',
        FinanceHead : '1538', // Maneesh Agarwal
    },

    Practice : {
	    Human_Resource : 410
    },

    Subsidiary : {
        US : '2',
      //Comity :'9',
        IN : '3'
    },

    Finance_Approver : {
        US : [ 5764 // 108984-Govind S Kharayat
        , 100325
        // 115833-Bhavik Jayeshkumar Pandya
        ],
        IN : [ 7074 // 108786-Santosh Banadawar
        , 1615
        // 102300-Lakshmi Narayanan N
        ]
    },

    Account : {
	    TradeDiscount : 515
    },

    EmailId : {
	    TimeSheet : 'timesheet@brillio.com'
    },

    ExpenseReport : {
        Employee : 'entity',
        FirstLevelApprover : 'custbody1stlevelapprover',
        SecondLevelApprover : 'custbody_expenseapprover'
    },

    Suitelet : {
        Script : {
	        Employee_Inactive : 'customscript_st_emp_inactive_mapping'
        },
        Deployement : {
	        Employee_Inactive : 'customdeploy_st_emp_inactive_mapping'
        }
    },

    SavedSearch : {
        Allocation_Report : 747,
        BO_Open_Case_Search : 1048
    },

    PurchareRequestStatus : {
        Cancelled : '22',
        Rejected : '6'
    },

    Images : {
	    CompanyLogo : 'https://system.na1.netsuite.com/core/media/media.nl?id=3462&c=3883006&h=4bf8d19bbed50156330c&whence=' ,
      ComityLogo : 'https://system.na1.netsuite.com/core/media/media.nl?id=756130&c=3883006&h=6433627add7d8648e1f9'
    },

    Item : {
        FixedFee : '2522',
        TenureDiscount : '2524',
        VolumeDiscount : '2523'
    },

    None : '@NONE@'
};