/**
 * Menu RESTlet
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Jan 2020     Deepak
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	
	nlapiLogExecution('debug', 'datain', JSON.stringify(dataIn));
	nlapiLogExecution('debug', 'Email id', dataIn.EmailId);
	var response = new Response();
	try {
		var s_email =  dataIn.EmailId;
		
		var employeeId = getUserUsingEmailId(s_email);
		nlapiLogExecution('DEBUG','employeeId',employeeId);
		if (_logValidation(employeeId) && _logValidation(s_email)) {
			
			response.Data = getApproverList(employeeId);
			response.Status = true;
		} else {
			throw "No Employee Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}
function getApproverList(id){
	
	var searchRec = nlapiSearchRecord('employee',null,['internalid','anyof',id],
	[new nlobjSearchColumn('custentity_reportingmanager'),
	new nlobjSearchColumn('approver'),
	new nlobjSearchColumn('timeapprover'),
    new nlobjSearchColumn('custentity_emp_hrbp')]//addedd HRBP column on 15-01-2021
	);
	var dataRow = [];
	var data = {};
	if(searchRec){
		/*var rm_Email , timeApp_email, expApp_email;
		if(_logValidation(searchRec[0].getValue("custentity_reportingmanager")))
			rm_Email = nlapiLookupField('employee',searchRec[0].getValue("custentity_reportingmanager"),'email');
		if(_logValidation(searchRec[0].getValue("timeapprover")))
			timeApp_email = nlapiLookupField('employee',searchRec[0].getValue("timeapprover"),'email');
		if(_logValidation(searchRec[0].getValue("approver")))
			expApp_email = nlapiLookupField('employee',searchRec[0].getValue("approver"),'email');
		
		data.reportingManager = {"name":searchRec[0].getText("custentity_reportingmanager"),
		"email": rm_Email };
		data.expenseApprover = {"name":searchRec[0].getText("approver"),
		"email": expApp_email };
		data.timeApprover = {"name":searchRec[0].getText("timeapprover"),
		"email": timeApp_email};
		dataRow.push(data); */
		dataRow.push({
			'ReportingManager':searchRec[0].getText("custentity_reportingmanager"),
			'TimeApprover': searchRec[0].getText("timeapprover"),
			'ExpenseApprover': searchRec[0].getText("approver"),
          'EmployeeHRBP': searchRec[0].getText("custentity_emp_hrbp")//addedd HRBP column on 15-01-2021
		});
	}
	return dataRow;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

