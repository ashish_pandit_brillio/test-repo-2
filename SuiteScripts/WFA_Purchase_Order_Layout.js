/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Purchase_Order_Layout.js
	Author      : Shweta Chopde
	Date        : 22 Aug 2014
	Description : Generate the PO Layout


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================
var i_global_subsidiary = '';
function suiteletFunction()
{
	var i_user_first_name = '';
	var i_user_middle_name = '';
	var i_user_last_name = '';
	var i_user_contact_no = '';
	var i_user_email_id = '';
	var a_tax_code_array = new Array();
	var a_tax_amount_array = new Array();
	var i_final_total = 0;
	var i_vendor_address = '';	
	var i_CST_No = '';
	var i_TIN_No = '';
	var s_bill_to = ''
	var	i_vendor_gstin = '';
	// ======================== Address =====================
	
	//var s_bill_to = 'Brillio Technologies Pvt. Ltd. \n JP Nagar';
	//s_bill_to = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_bill_to));
			
	
	try
	{		
	// var i_user = nlapiGetUser();	
	// nlapiLogExecution('DEBUG','suiteletFunction',' User -->'+i_user); 
		
	 var i_recordID = nlapiGetRecordId();//request.getParameter('i_recordID');	
	 nlapiLogExecution('DEBUG','suiteletFunction',' Record ID -->'+i_recordID);	
	
	 if(_logValidation(i_recordID))
	 {
	 	var o_recordOBJ = nlapiLoadRecord('purchaseorder',i_recordID)
		
	 	i_vendor_gstin = o_recordOBJ.getFieldValue('custbody_iit_transaction_gstin_uid');
	 	nlapiLogExecution('DEBUG', 'i_vendor_gstin', ' i_vendor_gstin -->' + i_user);
	 	
	 	if(!_logValidation(i_vendor_gstin))
		{
	 		i_vendor_gstin = '';
		}
	 	
		 if (_logValidation(o_recordOBJ)) 
		 {		 	
		   var i_user = o_recordOBJ.getFieldValue('custbody_created_by_user')
           nlapiLogExecution('DEBUG', 'suiteletFunction', ' User -->' + i_user);
		 	
		  if(_logValidation(i_user))
		  {
		  	var o_userOBJ = nlapiLoadRecord('employee',i_user)
			
		  	if(_logValidation(o_userOBJ))
			{
				i_user_first_name =  o_userOBJ.getFieldValue('firstname');
				
				i_user_middle_name =  o_userOBJ.getFieldValue('middlename');
				
				i_user_last_name =  o_userOBJ.getFieldValue('lastname');
				
				i_user_contact_no =  o_userOBJ.getFieldValue('officephone');
				
				i_user_email_id =  o_userOBJ.getFieldValue('email');
				
				
							
				if(!_logValidation(i_user_first_name))
				{
					i_user_first_name = '';
				}
				if(!_logValidation(i_user_middle_name))
				{
					i_user_middle_name = '';
				}
				if(!_logValidation(i_user_last_name))
				{
					i_user_last_name = '';
				}
				if(!_logValidation(i_user_contact_no))
				{
					i_user_contact_no = '';
				}
				if(!_logValidation(i_user_email_id))
				{
					i_user_email_id = '';
				}
				 nlapiLogExecution('DEBUG','suiteletFunction',' User First Name -->'+i_user_first_name);
				 nlapiLogExecution('DEBUG','suiteletFunction',' User Contact No. -->'+i_user_contact_no);
				 nlapiLogExecution('DEBUG','suiteletFunction',' User Email ID -->'+i_user_email_id);				
			}	 
			
		  }//User			
					 
		   var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary')
           nlapiLogExecution('DEBUG', 'suiteletFunction', ' Subsidiary -->' + i_subsidiary);		
		    i_global_subsidiary = i_subsidiary;
		   var i_location = o_recordOBJ.getFieldValue('location')
           nlapiLogExecution('DEBUG', 'suiteletFunction', ' Location -->' + i_location);		 
			var i_approvalstatus = o_recordOBJ.getFieldValue('approvalstatus');	
           var i_brillio_gstin='';
	   		if (_logValidation(i_location)) 
			{
				var o_locationOBJ = nlapiLoadRecord('location',i_location)
				
				if (_logValidation(o_locationOBJ)) 
				{
					i_CST_No = o_locationOBJ.getFieldValue('custrecord_cstnumber')
					
					i_TIN_No = o_locationOBJ.getFieldValue('custrecord_tinnumber')
					
					s_bill_to = o_locationOBJ.getFieldValue('addrtext')
					
					i_brillio_gstin = o_locationOBJ.getFieldValue('custrecord_iit_location_gstn_uid');
					
				}//Location OBJ				
			}//Location			
			
			s_bill_to = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_bill_to));
			
			
	          
			 
           var i_vendor = o_recordOBJ.getFieldValue('entity')
		   nlapiLogExecution('DEBUG', 'suiteletFunction', ' Vendor -->' + i_vendor);
		   
		   var a_vendor_array = get_vendor_details(i_vendor);
		   nlapiLogExecution('DEBUG', 'suiteletFunction', ' Vendor Array -->' + a_vendor_array);
 					  
			var a_split_array = new Array();
			if (_logValidation(a_vendor_array)) 
			{
				a_split_array = a_vendor_array[0].split('######')
				
				i_vendor_name = a_split_array[0];
				i_vendor_code = a_split_array[1];
				i_vendor_address = a_split_array[2];
				s_vendor_email=a_split_array[3];
			}				
			if (!_logValidation(i_vendor_name)) 
			{
				i_vendor_name = '';
			}	
			if (!_logValidation(i_vendor_code)) 
			{
				i_vendor_code = '';
			}
		    if (!_logValidation(i_vendor_address)) 
			{
				i_vendor_address = '';
			}
			if (!_logValidation(s_vendor_email))
			{
				s_vendor_email='';
			}
			  i_vendor_name = nlapiEscapeXML(i_vendor_name)
			  i_vendor_code = nlapiEscapeXML(i_vendor_code)
			
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Vendor Name -->' + i_vendor_name);
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Vendor Code-->' + i_vendor_code);
			i_vendor_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_vendor_address));
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Vendor Address-->' + i_vendor_address);
										
			var i_PO_ID = o_recordOBJ.getFieldValue('tranid')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO# -->' + i_PO_ID);
		
			if (!_logValidation(i_PO_ID)) 
			{
				i_PO_ID = '';
			}	
			
			var i_PO_Date = o_recordOBJ.getFieldValue('trandate')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' PO Date -->' + i_PO_Date);
			
			if (!_logValidation(i_PO_Date)) 
			{
				i_PO_Date = '';
			}			
			var i_currency = o_recordOBJ.getFieldText('currency')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Currency -->' + i_currency);
			
			if (!_logValidation(i_currency)) 
			{
				i_currency = '';
			}			
			var i_ship_address = o_recordOBJ.getFieldValue('shipaddress')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Ship Address -->' + i_ship_address);
			
			if (!_logValidation(i_ship_address)) 
			{
				i_ship_address = '';
			}			
			i_ship_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_ship_address));
					
			var i_delivery_by = o_recordOBJ.getFieldValue('duedate')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Delivery By -->' + i_delivery_by);
			
			if (!_logValidation(i_delivery_by)) 
			{
				i_delivery_by = '';
			}			
			var i_warranty = o_recordOBJ.getFieldText('custbody_warranty')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Warranty -->' + i_warranty);
			
			if (!_logValidation(i_warranty)) 
			{
				i_warranty = '';
			}
			  i_warranty = nlapiEscapeXML(i_warranty)			
			var i_pricing = o_recordOBJ.getFieldText('custbody_pricing')
			nlapiLogExecution('DEBUG', 'suiteletFunction', 'Prices -->' + i_pricing);
			
			if (!_logValidation(i_pricing)) 
			{
				i_pricing = '';
			}			
			
			  i_pricing = nlapiEscapeXML(i_pricing)
			var i_payment = o_recordOBJ.getFieldText('terms')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Payment-->' + i_payment);			
			
			if (!_logValidation(i_payment)) 
			{
				i_payment = '';
			}	
			 i_payment = nlapiEscapeXML(i_payment)		
			var i_terms_n_conditions = o_recordOBJ.getFieldValue('custbody_tersconditions')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Terms  & Conditions -->' + i_terms_n_conditions);
						
			if (!_logValidation(i_terms_n_conditions)) 
			{
				i_terms_n_conditions = '';
			}	
		 i_terms_n_conditions = nlapiEscapeXML(i_terms_n_conditions)	
		 
	// ================== Geneartion Of PDF =================================
	
//if(parseInt(i_subsidiary) == parseInt(3))
{
	var image_URL = constant.Images.CompanyLogo;
	}
	if(parseInt(i_subsidiary) == parseInt(9)){
	var image_URL = constant.Images.ComityLogo;
	}	 
    image_URL = nlapiEscapeXML(image_URL)
		 
	var strVar="";
	strVar += "";
	
	
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	//strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"50%\">&nbsp;<\/td>";
	
	if (image_URL != '' && image_URL != null && image_URL != '' && image_URL != 'undefined' && image_URL != ' ' && image_URL != '&nbsp') 
	{
		//strVar += " <td width=\"60%\"><img width=\"150\" height=\"60\" src=\"" + image_URL + "\"><\/img><\/td>";
		//if(parseInt(i_global_subsidiary) == parseInt(3))
	    strVar += " <td width=\"50%\"><img width=\"133\" height=\"56\" src=\"" + image_URL + "\"><\/img><\/td>";
	   //if(parseInt(i_global_subsidiary) == parseInt(9))
		//strVar += " <td width=\"60%\"><img width=\"198\" height=\"45\" src=\"" + image_URL + "\"><\/img><\/td>";
	
	  	//strVar += " <td width=\"50%\"><img width=\"150\" height=\"75\" src=\"" + image_URL + "\"><\/img><\/td>";
    }
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"50%\">&nbsp;<\/td>";
	if(parseInt(i_approvalstatus) == parseInt(1)){
	strVar += "		<td font-family=\"Helvetica\" style=\"color:#FF0000;font-size:12\" font-size=\"12\"  width=\"50%\">Draft Purchase Order - Pending Approval<\/td>";
	}
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"8\"  width=\"60%\">&nbsp;<\/td>";
	strVar += "		<td border-bottom=\"0.3\" align=\"left\" style=\"color:#00CC00;font-size:25\" font-size=\"25\" font-family=\"Helvetica\" width=\"40%\">Purchase Order<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"50%\">&nbsp;<\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"50%\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
		
	strVar += "<\/table>";
			

strVar += "<table border=\"0\" width=\"100%\">";		
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"50%\">"
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" ><b>Order To.<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\" >"+i_vendor_address+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>Vendor Code.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+nlapiEscapeXML(i_vendor_code)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>Vendor GSTIN ID.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+nlapiEscapeXML(i_vendor_gstin)+"<\/td>";
	strVar += "	<\/tr>";	
	strVar += "<\/table>";	
	strVar += "	<\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"50%\">"
    strVar += "<table border=\"0\" width=\"100%\">";
	
    strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>PO No.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+nlapiEscapeXML(i_PO_ID)+"<\/td>";
	strVar += "	<\/tr>";	
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>PO Date<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+(i_PO_Date)+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>Contact Name<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+(i_user_first_name)+"<\/td>";
	strVar += "	<\/tr>";
		
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>Contact Number<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+(i_user_contact_no)+"<\/td>";
	strVar += "	<\/tr>";
	
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"15%\"><b>Contact Email ID<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  width=\"35%\">"+(i_user_email_id)+"<\/td>";
	strVar += "	<\/tr>";
		
	strVar += "<\/table>";	
    strVar += "	<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "<\/table>";

strVar += "";
	strVar += "<table border=\"0\" width=\"100%\">";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"2\"><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"2\"><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"2\">Dear Sir\/Madam,<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"2\">We are please to place this order for the supply of ";
	strVar += "		goods \/services as per the terms and conditions setforth herewith<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td colspan=\"2\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
    strVar += "<\/table>";
	//Brillio 
    strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" ><b>Ship To.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" ><b>Bill To<\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" >"+i_ship_address+"<\/td>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" >"+s_bill_to+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";			 
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><b> GSTIN ID.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><b> GSTIN ID.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >"+i_brillio_gstin+"<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >"+i_brillio_gstin+"<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "<\/table>";
	//Comity Table;
	
	strVar += "<table border=\"0\" width=\"100%\" align=\"right\">";
	if(parseInt(i_global_subsidiary) == parseInt(9)){
	strVar += "	<tr>";
	//if(parseInt(i_global_subsidiary) == parseInt(19))
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><b> Contact Person/Number<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><b> Contact Person/Number<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//if(parseInt(i_global_subsidiary) == parseInt(19))
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >Rupali Jain<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >Rupali Jain<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//if(parseInt(i_global_subsidiary) == parseInt(19))
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >9028042784/9028042798<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >9028042784/9028042798<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//if(parseInt(i_global_subsidiary) == parseInt(19))
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";}
	strVar += "<\/table>"; 
	
	/*//Comity:
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" ><b>Ship To.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" ><b>Bill To<\/b><\/td>";
	
	strVar += "	<\/tr>";


	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" >"+i_ship_address+"<\/td>";
	strVar += "		<td font-family=\"Helvetica\" width=\"50%\" font-size=\"8\" >"+s_bill_to+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "<\/table>";

					 
	strVar += "<table border=\"0\" width=\"100%\">";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><b>Comity GSTIN ID.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><b>Comity GSTIN ID.<\/b><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >"+i_brillio_gstin+"<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" >"+i_brillio_gstin+"<\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"35%\" font-size=\"8\" ><\/td>";
	strVar += "		<td font-family=\"Helvetica\"  width=\"15%\" font-size=\"8\" ><\/td>";
	strVar += "	<\/tr>";
	
	
	strVar += "<\/table>"; */
	

	//================= Line Items =============
	
	strVar += "<table border=\"0\" width=\"100%\">";
	strVar += "	<tr>";
	strVar += "		<th width=\"5%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>SL&nbps;&nbsp;#&nbps;<\/b><\/th>";
	strVar += "		<th width=\"20%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Item&nbps;&nbsp;Description&nbps;<\/b><\/th>";
	strVar += "		<th width=\"5%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Quantity&nbps;<\/b><\/th>";
	strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Unit&nbps;&nbsp;of&nbps;<br/>Measure&nbps;<\/b><\/th>";
//	strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Currency&nbps;<\/b><\/th>";
	strVar += "		<th width=\"5%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Rate&nbps;<br/>("+i_currency+")<\/b><\/th>";
	strVar += "		<th width=\"5%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Amount&nbps;<br/>("+i_currency+")<\/b><\/th>";
	strVar += "		<th width=\"20%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>&nbps;Tax<br/>Code&nbps;<\/b><\/th>";
	strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>&nbps;Tax<br/>Rate&nbps;<\/b><\/th>";
	strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>&nbps;Tax<br/>Amt&nbps;<\/b><\/th>";
	strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>&nbps;Gross<br/>&nbps;Amt&nbps;<\/b><\/th>";
	//strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Tax&nbps; Rate&nbps;<\/b><\/th>";
	//strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Tax&nbps;&nbsp;Amount&nbps;<\/b><\/th>";
	//strVar += "		<th width=\"10%\" border-bottom=\"0.3\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><b>Line&nbps;&nbsp;Total&nbps;<br/>("+i_currency+")<\/b><\/th>";
	strVar += "	<\/tr>";
	
	
 	var i_line_count = o_recordOBJ.getLineItemCount('item');
	
	 if(_logValidation(i_line_count))
	 {	 	
		var i_line_amount_total = 0 ;
		var i_amount_total = 0 ;
		var i_tax_amount_total_l = 0 ;
			
	 	for(var i=1;i<=i_line_count;i++)
		{
			var i_item_description = o_recordOBJ.getLineItemValue('item','description',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item Description -->' + i_item_description);
				
			var i_quantity = o_recordOBJ.getLineItemValue('item','quantity',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Quantity -->' + i_quantity);
			
			var i_UOM = o_recordOBJ.getLineItemText('item','custcol_units',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' UOM -->' + i_UOM);
			
			var i_rate = o_recordOBJ.getLineItemValue('item','rate',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Rate -->' + i_rate);
			
			var i_amount = o_recordOBJ.getLineItemValue('item','amount',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Amount -->' + i_amount);
			
			var i_tax_rate = o_recordOBJ.getLineItemValue('item','taxcode_display',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Rate -->' + i_tax_rate);
			
			var i_tax_amount = o_recordOBJ.getLineItemValue('item','tax1amt',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Amount -->' + i_tax_amount);
			
			var i_line_total = o_recordOBJ.getLineItemValue('item','amount',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Line Total -->' + i_line_total);
						
			var i_tax_code =  o_recordOBJ.getLineItemValue('item','taxcode',i)
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Code --> ' + i_tax_code);
			var i_tax_rate_d='';	
			
			var i_gross_amt =  o_recordOBJ.getLineItemValue('item','grossamt',i)          // taxinclusive or gross amt fetching from PO
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Gross Amt --> ' + i_gross_amt);
			
			var total = o_recordOBJ.getFieldValue('total'); // fetching the total from PO
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Total****** -->' + total);
		    if (!_logValidation(total)) 
			{
		    	total = '';
			}	
			
			
		
			var taxgrouprecord = nlapiLoadRecord('taxgroup',i_tax_code); /// modified by poobalan (start)
	   		var lineCount = taxgrouprecord.getLineItemCount('taxitem');
	   		
	   		var s_item_desc = '';
			var a_data_str = new Array();
			 var a_item_desc_split_arr = new Array();
			 if (_logValidation(i_item_description)) 
			 {
			 	for(var idt = 0;idt<i_item_description.toString().length;idt++)
				{
					a_item_desc_split_arr = i_item_description.split(' ')
					
					var i_item_desc_1 = a_item_desc_split_arr[idt];
				//	nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item Desc .... --> ' + i_item_desc_1);
					
					if (_logValidation(i_item_desc_1)) 
					{
						a_data_str.push(i_item_desc_1);
					}
		
				}
			 	
			
				for(var bt=0;bt<a_data_str.length;bt++)
				{
					
                    //if(a_data_str[bt] =='&')
					{
						a_data_str[bt] = nlapiEscapeXML(a_data_str[bt])
					}

                   nlapiLogExecution('DEBUG', 'suiteletFunction', ' a_data_str[bt] .... --> ' + a_data_str[bt]);

					
					
					//s_item_desc = (s_item_desc)+'&nbps;'+'&nbsp;'+(a_data_str[bt])+'&nbps;'
                   s_item_desc = (s_item_desc)+'&nbps;'+'&nbsp;'+(a_data_str[bt])+'&nbps;'
								
				}
				if (_logValidation(s_item_desc)) 
				{
					s_item_desc = (s_item_desc)
				}
				else
				{
					//strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+(s_item_desc+'&nbps;'+'&nbsp;'+a_data_str[bt]+'&nbps;')+"<\/td>";	
					//strVar += "		<td width=\"10%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";	
				}
			 }
	
	     	nlapiLogExecution('DEBUG', 'suiteletFunction', ' Item Desc .... --> ' + s_item_desc);
	   		
	   		
	   		strVar += "	<tr>";
			strVar += "		<td width=\"5%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" >"+i+"<\/td>"; 
	   		
			strVar += "		<td width=\"20%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" >"+nlapiEscapeXML(i_item_description)+"<\/td>";
			strVar += "		<td width=\"5%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+i_quantity+"<\/td>";
			strVar += "		<td width=\"10%\"  align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+i_UOM+"<\/td>";
		//	strVar += "		<td width=\"10%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" >"+i_currency+"<\/td>";
			strVar += "		<td width=\"5%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+i_rate+"<\/td>";
			strVar += "		<td width=\"5%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+i_amount+"<\/td>";
	   		
			var frst_row = 0;
				
	   		for(var k=1;k<=lineCount;k++)
	   				{   	
	   			   var taxgroupname = taxgrouprecord.getLineItemText('taxitem','taxname',k);
	   			   var taxgrouprate = taxgrouprecord.getLineItemValue('taxitem','rate',k);
	   				nlapiLogExecution('DEBUG', ' taxgroupCount',  lineCount);
	   				nlapiLogExecution('DEBUG', ' taxgroupname',  taxgroupname);
	   				nlapiLogExecution('DEBUG', ' taxgrouprate',  taxgrouprate);
	   				
	   				var i_total_amt = parseFloat(parseFloat(i_line_total)*parseFloat(taxgrouprate)/100); // (End)

	   				if(frst_row != 0)
   					{
	   					strVar += "<tr>";
	   					
   						strVar += "		<td width=\"5%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
		   		   		
		   				strVar += "		<td width=\"20%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";	
		   				strVar += "		<td width=\"5%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
		   				strVar += "		<td width=\"10%\"  align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
		   				strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
		   				strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
   					
	   				}
	   				
			strVar += "		<td width=\"20%\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(taxgroupname)+"<\/td>";			
			strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+taxgrouprate+"<\/td>";
			strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+parseFloat(i_total_amt).toFixed(1)+"<\/td>";
			
			if(frst_row == 0)
				strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+parseFloat(i_gross_amt).toFixed(2)+"<\/td>";
					
		//	strVar += "		<td width=\"10%\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" >"+i_amount+"<\/td>";
				
		
			var a_tax_split_arr = new Array();
			
			
			 if (_logValidation(i_tax_rate)) 
			 {
			 	if(i_tax_rate.indexOf(':')>-1)
				{
					a_tax_split_arr = i_tax_rate.split(':')
					
					var tax_rate_1 = a_tax_split_arr[0];
					var tax_rate_2 = a_tax_split_arr[1];
			
					if (!_logValidation(tax_rate_2)) 
					{
						i_tax_rate_d = '';
					}
					else
					{
						i_tax_rate_d = tax_rate_2;
					}
				
				} 
				else
				{
					if (!_logValidation(i_tax_rate)) 
					{
						i_tax_rate_d = '';
					}
					else
					{
						i_tax_rate_d = i_tax_rate;
					}
					
				}	
			 	
			 }
			
			if (!_logValidation(i_tax_amount)) 
			{
				i_tax_amount = '';
			}
			
			if (!_logValidation(i_line_total)) 
			{
				i_line_total = '';
			}		
			//strVar += "		<td width=\"10%\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" >"+i_line_total+"<\/td>";
			
			if(frst_row != 0)
				strVar += "	<\/tr>";
			
			if(frst_row == 0)
			{
				nlapiLogExecution('audit', 'inside tr close frst');
				strVar += "	<\/tr>";
				frst_row = 1;
			}
			
								
            a_tax_code_array.push(i_tax_code+'******##******'+i_tax_rate_d);	
						 
			a_tax_amount_array.push(i_tax_code+'^^^^^****^^^^^'+i_amount);
									
			i_amount_total = parseFloat(i_amount_total)+parseFloat(i_amount);
			i_amount_total = parseFloat(i_amount_total)
			i_amount_total = i_amount_total.toFixed(2);
		
			i_line_amount_total = parseFloat(i_line_amount_total)+parseFloat(i_line_total);
			i_line_amount_total = parseFloat(i_line_amount_total)
			i_line_amount_total = Math.round(i_line_amount_total).toFixed(2);	
			
	   	//}
		//}//Loop	
				
			   
					 
	   a_tax_code_array = removearrayduplicate(a_tax_code_array);
	   nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Code Array --> ' + a_tax_code_array);
	  	   
	   nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Amount Array --> ' + a_tax_amount_array);
			   		
		var a_tax_data_array='';	
		var a_split_tax_array = new Array();	
		var a_split_tax_code_name_array = new Array();
		var a_tax_name_values_1 = '';
		var a_tax_values_1 = '';	  
		var a_tax_name_values = new Array();
		var a_tax_values = new Array();	  
	

	   	 if (_logValidation(a_tax_code_array)) 
		 {		 
		 	for(var a=0;a<a_tax_code_array.length;a++)
			{
				var i_tax_amount_total = 0;
				a_split_tax_code_name_array = a_tax_code_array[a].split('******##******') 
					
				var i_tax_code_ID = a_split_tax_code_name_array[0];
				
				var i_tax_code_name = a_split_tax_code_name_array[1];
			
				for(var b=0;b<a_tax_amount_array.length;b++)
				{
					a_split_tax_array = a_tax_amount_array[b].split('^^^^^****^^^^^') 
					
					var i_tax_code_in = a_split_tax_array[0];
					
					var i_tax_amount_in = a_split_tax_array[1];
					
					if(i_tax_code_ID==i_tax_code_in)
					{
						i_tax_amount_total = parseFloat(i_tax_amount_total)+parseFloat(i_tax_amount_in);
						
					}
					
				}//Loop Tax Amount	
				
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Total Tax Amount --> ' + i_tax_amount_total +' '+'for'+' '+i_tax_code_ID);
	   			a_tax_data_array = tax_group_calculation(i_tax_code_ID,i_tax_amount_total)	
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Amount Return --> ' + a_tax_data_array);
	            nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Amount Return Length --> ' + a_tax_data_array.length);
	            
	          	            
								
			var a_split_data_taxes = new Array();
			
			 if (_logValidation(a_tax_data_array))
			 {
			 	a_split_data_taxes = a_tax_data_array[0].split('!!!!!@@@@@@!!!!!')
				
				var a_tax_return_array = a_split_data_taxes[0];
				
				var a_tax_name_array = a_split_data_taxes[1];
							 	
				nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Return Array --> ' + a_tax_return_array);
	   
	            nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Name Array --> ' + a_tax_name_array);
	   	   
		        nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Return Array Length--> ' + a_tax_return_array.length);
	   
	            nlapiLogExecution('DEBUG', 'suiteletFunction', ' Tax Name Array Length--> ' + a_tax_name_array.length);
	   	   				
				  for(var po=0;po<a_tax_return_array.length;po++)
				  {
					 	a_tax_values = a_tax_return_array.split(',')
					    break;				
				  }
				  
				   nlapiLogExecution('DEBUG', 'suiteletFunction', ' a_tax_values-> ' + a_tax_values);
	  		 
                 if(a_tax_values_1 =='')
				 {
				 	a_tax_values_1+= a_tax_values;				 	
					nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values ,,,,,,,,,,,,,,,,-->'+a_tax_values_1)
				 }			
                  else
				 {
				 	a_tax_values_1 = a_tax_values_1+','+a_tax_values;
					nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values ,,,,,,,,,,,,,,, -->'+a_tax_values_1)
				 }


				 
				 nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values-->'+a_tax_values)	
				 nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values Length-->'+a_tax_values.length)		
					
					
				 nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values Concat -->'+a_tax_values_1)	
				 nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values Length Concat-->'+a_tax_values_1.length)		
				
	     	    nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values XXXXXXXXXX-->'+a_tax_values_1)	
				nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values Length XXXXXXXXXXX -->'+a_tax_values_1.length)		
										   
				  for(var ko=0;ko<a_tax_name_array.length;ko++)
				  {
					 	a_tax_name_values = a_tax_name_array.split(',')
					    break;				
				  }
				
				a_tax_name_values = removearrayduplicate(a_tax_name_values);
						   
			     
                 if (a_tax_name_values_1 == '')
				 {
				 	 a_tax_name_values_1 += a_tax_name_values;				 	 
					 nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values @@@@@@@@@@@@@@@@ IF -->'+a_tax_name_values_1)	
				 } 				
                 else
				 {
				   a_tax_name_values_1 = a_tax_name_values_1+','+a_tax_name_values;	
				   nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values @@@@@@@@@@@@@@@@ ELSE-->'+a_tax_name_values_1)					
				 }


			   
			    nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values-->'+a_tax_name_values)	
				nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values Length-->'+a_tax_name_values.length)		
				
				nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values Conact YYYYY -->'+a_tax_name_values_1)	
				nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values Length Conact YYYYY-->'+a_tax_name_values_1.length)		
															
						
			 }//a_tax_data_array			 
			 else
			 {		
			    var i_tax_rate_x = '';
			    
				if(i_tax_code_ID != 'null')
					{
						i_tax_rate_x = nlapiLookupField('salestaxitem', parseInt(i_tax_code_ID), 'rate');//escape_special_chars(i_tax_code_name)
					}
				
				if (!_logValidation(i_tax_code_name)) 
					{
						i_tax_code_name = '';
					}					
                    if(i_tax_rate_x.indexOf('-')>-1)
					{						
						var a_split_tax_no = new Array();
						
						a_split_tax_no = i_tax_rate_x.split('-')
						
						var a_tax_1 = a_split_tax_no[0];
						
						var a_tax_2 = a_split_tax_no[1];
										
						i_tax_rate_x = parseFloat(a_tax_2)*parseFloat(-1)+'%';						

					}

					var i_tax_amount_net = (parseFloat(i_tax_amount_total)*parseFloat(i_tax_rate_x))/100;				
				    i_tax_amount_net = parseFloat(i_tax_amount_net).toFixed(2);
						 
			 	 	
			 
            /*    strVar += "<tr>";  commented   
 		        strVar += "		<td width=\"10%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
			    strVar += "		<td width=\"50%\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\" >"+i_tax_code_name+"<\/td>";			
				strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+i_tax_rate_x+"<\/td>";
				strVar += "		<td width=\"10%\"  align=\"left\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
				strVar += "		<td width=\"10%\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
			    strVar += "		<td width=\"10%\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" >"+parseFloat(i_tax_amount_net).toFixed(2)+"<\/td>";
				strVar += "	<\/tr>";*/
					
					
				i_amount_total = parseFloat(i_amount_total)+parseFloat(i_tax_amount_total);
				i_amount_total = parseFloat(i_amount_total)
			
				i_line_amount_total = parseFloat(i_line_amount_total)+parseFloat(i_tax_amount_net);
				i_line_amount_total = parseFloat(i_line_amount_total)
			 	
			 }
			 
			
			}//Loop Tax Code			
			
		 }//Tax Code Array		
	   	 
	   	 
	
		 
			    nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values )))))))))))))))) -->'+a_tax_name_values_1)	
				nlapiLogExecution('DEBUG','suiteletFunction',' Tax Name Values Length )))))))))) -->'+a_tax_name_values_1.length)		
				
				
		          for(var ko=0;ko<a_tax_name_values_1.length;ko++)
				  {				  	
					 	a_tax_name_values_1 = a_tax_name_values_1.split(',')
					    break;				
				  }

				a_tax_name_values_1 = removearrayduplicate(a_tax_name_values_1);
			   
			    nlapiLogExecution('DEBUG','suiteletFunction',' tyi Tax Name Values )))))))))))))))) -->'+a_tax_name_values_1)	
				nlapiLogExecution('DEBUG','suiteletFunction',' tyi Tax Name Values Length )))))))))) -->'+a_tax_name_values_1.length)		
												
		
		         for(var ko=0;ko<a_tax_values_1.length;ko++)
				  {
					 	a_tax_values_1 = a_tax_values_1.split(',')
					    break;				
				  }
				
				a_tax_values_1 = removearrayduplicate(a_tax_values_1);
			   
			    nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values &&&&&&&&&&&&&&& -->'+a_tax_values_1)	
				nlapiLogExecution('DEBUG','suiteletFunction',' Tax Values Length &&&&&&&&&&&&&&&&&& -->'+a_tax_values_1.length)		
												
		
		       for(var bt=0;bt<a_tax_name_values_1.length;bt++)
			   {
			   	var i_tax_rate_x = '';
				 var i_tax_amt_total = 0;
				 var i_tax_basis = 0;
				  i_tax_rate_x = escape_special_chars(a_tax_name_values_1[bt])
				  nlapiLogExecution('DEBUG','suiteletFunction',' a_tax_name_values_1[bt] -->'+a_tax_name_values_1[bt])
		   	     
				  	 if (_logValidation(a_tax_code_array)) 
					 {
					 	for (var a = 0; a < a_tax_code_array.length; a++) 
						{
					 		var i_tax_amount_total = 0;
					 		a_split_tax_code_name_array = a_tax_code_array[a].split('******##******')
					 		
					 		var i_tax_code_ID = a_split_tax_code_name_array[0];
					 		
					 		var i_tax_code_name = a_split_tax_code_name_array[1];
							
							  nlapiLogExecution('DEBUG','suiteletFunction',' vvvvvvvvvvvv i_tax_code_name-->'+i_tax_code_name)
		   	    
					 		
							if(a_tax_name_values_1[bt] == i_tax_code_name)
							{	
							 nlapiLogExecution('DEBUG','suiteletFunction',' vvvvvvvvvvvv innnnnnnnn-->')
								
						 		for (var b = 0; b < a_tax_amount_array.length; b++) 
								{
						 			a_split_tax_array = a_tax_amount_array[b].split('^^^^^****^^^^^')
						 			
						 			var i_tax_code_in = a_split_tax_array[0];
						 			
						 			var i_tax_amount_in = a_split_tax_array[1];
						 			
						 			if (i_tax_code_ID == i_tax_code_in) 
									{
						 				i_tax_amount_total = parseFloat(i_tax_amount_total) + parseFloat(i_tax_amount_in);
						 				
						 			}
						 			
						 		}//Loop Tax Amount	
								 nlapiLogExecution('DEBUG','suiteletFunction',' vvvvvvvvvvvv innnnnnnnn i_tax_amount_total-->'+i_tax_amount_total)
							
							}							
						
							}
						} 
				 
				  var f_rate	=	0.0;
				  
				  for(var z=0;z<a_tax_values_1.length;z++)
				  {
				  	a_split_tax_array = a_tax_values_1[z].split('*****###*****') 
				  
				    var i_tax_name = a_split_tax_array[0];
                    var i_amount =	a_split_tax_array[1];
                    var i_basis = a_split_tax_array[2];
					var f_rate_temp	= a_split_tax_array[3];
                    
					nlapiLogExecution('DEBUG','suiteletFunction',' i_tax_name -->'+i_tax_name)
					
					if(a_tax_name_values_1[bt] == i_tax_name)
					{				
						
						 nlapiLogExecution('DEBUG','suiteletFunction',' iiiinnnnnnnn i_tax_name -->'+i_tax_name)
						 
						 nlapiLogExecution('DEBUG','suiteletFunction',' Amount -->'+i_amount)	
						 i_tax_amt_total = parseFloat(i_tax_amt_total)+parseFloat(i_amount);
						 i_tax_basis = i_basis;
						 f_rate	=	f_rate_temp;
						 nlapiLogExecution('DEBUG','suiteletFunction',' TAX Amount -->'+i_tax_amt_total)	
					}//Tax Name
				  	
				  }//Loop Tax Return Array	
				
				
					i_final_total = parseFloat(i_final_total)+parseFloat(i_tax_amt_total);
	
	                var tax_name = a_tax_name_values_1[bt]
	            
				    if (!_logValidation(tax_name)) 
					{
						tax_name = '';
					}					
                    if(i_tax_rate_x.indexOf('-')>-1)
					{						
						var a_split_tax_no = new Array();
						
						a_split_tax_no = i_tax_rate_x.split('-')
						
						var a_tax_1 = a_split_tax_no[0];
						
						var a_tax_2 = a_split_tax_no[1];
										
						i_tax_rate_x = parseFloat(a_tax_2)*parseFloat(-1)+'%';						
						
						 nlapiLogExecution('DEBUG','i_tax_rate_x',' i_tax_rate_x -->'+i_tax_rate_x);
						 nlapiLogExecution('DEBUG','a_tax_1',' a_tax_1 -->'+a_tax_1);
						 nlapiLogExecution('DEBUG','a_tax_2',' a_tax_2 -->'+a_tax_2);
						 	
					}

                   nlapiLogExecution('DEBUG','suiteletFunction',' TAX Amount XXXXXXXXx CCCCCCCCCCCc -->'+i_tax_amt_total)	

					var i_tax_amount_net = (parseFloat(i_tax_amt_total)*parseFloat(f_rate)*parseFloat(i_tax_basis))/(100*100);
					i_tax_amount_net = parseFloat(i_tax_amount_net);					
				    i_tax_amount_net = i_tax_amount_net.toFixed(2)
				    
				 /*commented   
				
				strVar += "	<tr>";
		        strVar += "		<td width=\"10%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
			    strVar += "		<td width=\"50%\" align=\"left\" font-family=\"Helvetica\" font-size=\"8\" >"+tax_name+"<\/td>";			
				strVar += "		<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >"+i_tax_rate_x+"<\/td>";
				strVar += "		<td width=\"10%\"  align=\"left\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
				strVar += "		<td width=\"10%\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" ><\/td>";
			    strVar += "		<td width=\"10%\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" >"+parseFloat(i_tax_amount_net)+"<\/td>";
				strVar += "	<\/tr>";
				*/
				i_amount_total = parseFloat(i_amount_total)+parseFloat(i_tax_amt_total);
				i_amount_total = parseFloat(i_amount_total)
			
				i_line_amount_total = parseFloat(i_line_amount_total)+parseFloat(i_tax_amount_net);
				i_line_amount_total = parseFloat(i_line_amount_total)	
				
			   }
		
		
               

		
		if (!_logValidation(i_amount_total)) 
		{
			i_amount_total = '';
		}
		else
		{
		 i_amount_total = i_amount_total.toFixed(2)	
		}

		if (!_logValidation(i_line_amount_total)) 
		{
			i_line_amount_total = '';
		}
		else
		{
		 i_line_amount_total = Math.round(i_line_amount_total).toFixed(2)	
		}
		
		
		
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_amount_total .... --> ' + i_amount_total);
	
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_tax_amount_total_l.... --> ' + i_tax_amount_total_l);
	
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' i_line_amount_total .... --> ' + i_line_amount_total);
	

		var i_line_amount_total_f = ''
		var i_tax_amount_total_l_f = ''
		var i_amount_total_f = ''
		
		
		if (_logValidation(i_amount_total)) 
		{
			i_amount_total_f = formatDollar(i_amount_total)
		}
		else
		{
			i_amount_total_f = '';
		}
		if (_logValidation(i_tax_amount_total_l)) 
		{
			i_tax_amount_total_l_f = formatDollar(i_tax_amount_total_l)
		}
		else
		{
			i_tax_amount_total_l_f = '';
		}
		if (_logValidation(i_line_amount_total)) 
		{
			i_line_amount_total_f = formatDollar(i_line_amount_total)
		}
		else
		{
			i_line_amount_total_f = '';
		}
		
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' ********** i_amount_total .... --> ' + i_amount_total_f);
	
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' ********** i_tax_amount_total_l.... --> ' + i_tax_amount_total_l_f);
	
		nlapiLogExecution('DEBUG', 'suiteletFunction', ' ********** i_line_amount_total .... --> ' + i_line_amount_total_f);
	

	 } //for loop of taxgroup lineitem count
	   		
	   		strVar += "	<tr>";
			strVar += "	<td width=\"10%\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
			strVar += "	<\/tr>";
			
	 }//for loop of taxgroup lineitem count
	 	
	 	nlapiLogExecution('audit', 'last tr');
   		strVar += "	<tr>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"50%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" ><b>Total&nbps;<\/b><\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"8\" ><b>"+parseFloat(total).toFixed(2)+"<\/b><\/td>"; // printing the total value from the PO
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "		<td width=\"10%\" border-top=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"8\" >&nbsp;<\/td>";
		strVar += "	<\/tr>";
		
	 }//Line Count
	 strVar += "<\/table>";
 }//Record OBJ		
}
//Record ID 




strVar += "<table border=\"0\" width=\"100%\">";
    if (_logValidation(total)) 
	{
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\"><b>Amount&nbps;&nbsp;in&nbps;&nbsp;Words&nbps;&nbsp;:&nbps;&nbsp;"+toWordsFunc((total),i_currency)+"&nbsp;only<\/b><\/td>";
	strVar += "	<\/tr>";
	}
	else
	{
	 strVar += "	<tr>";
	strVar += "		<td width=\"100%\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\"><b>Amount&nbps;&nbsp;in&nbps;&nbsp;Words&nbps;&nbsp;:&nbps;&nbsp;"+toWordsFunc((total),'')+"&nbsp;<\/b><\/td>";
	strVar += "	<\/tr>";		
	}	
	
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\"><u><b>Specific&nbps;&nbsp;Terms&nbps;&nbsp;&amp;&nbps;&nbsp;Conditions&nbps;&nbsp;<\/b><\/u><\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Delivery&nbps;&nbsp;By&nbps;&nbsp;:&nbsp;"+nlapiEscapeXML(i_delivery_by)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Prices&nbps;&nbsp;:&nbsp; "+nlapiEscapeXML(i_pricing)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Warranty&nbps;&nbsp;:&nbsp;"+nlapiEscapeXML(i_warranty)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Billing&nbps;&nbsp;: Ensure digitally signed invoices should reach along apindia@brillio.com  with the supporting’s within 7 days of delivery of services / product at our premises.<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Payment&nbps;&nbsp;:&nbsp;"+nlapiEscapeXML(i_payment)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">TDS&nbps;&nbsp;: TDS will be deducted if and as applicable. For non ";
	strVar += "		deduction or lower tax deduction, please provide the tax exemption\/lower ";
	strVar += "		tax deduction certificate <\/td>";
	strVar += "	<\/tr>";
	
	if(_logValidation(i_terms_n_conditions))
	{
	 strVar += "	<tr>";
	 strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Note&nbps;&nbsp;:&nbsp;"+nlapiEscapeXML(i_terms_n_conditions)+"<\/td>";
	 strVar += "	<\/tr>"
	}
	
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\"><u><b>General Terms &amp; Conditions<\/b><\/u><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Please refer to section 'Additional Terms' <\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td width=\"100%\" font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">Please send us an order acceptance or proforma invoice ";
	strVar += "		as a token of order acceptance<\/td>";
	strVar += "	<\/tr>";
	
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	
	/*strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";*/
	strVar += "	<tr>";
	strVar += "		<td colspan=\"10\" font-family=\"Helvetica\" font-size=\"8\">This is a system generated PO, no signature required.<\/td>";
	strVar += "		<\/tr>"; 
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//strVar += "		<td colspan=\"10\" font-family=\"Helvetica\" font-size=\"8\">This is a system generated PO , no signature required.<\/td>";
	strVar += "		<\/tr>"; 
	

	strVar += "<\/table>";


    //strVar += " <p style=\"page-break-after: always\"><\/p>";

    var adrVtr = additional_terms()
    strVar += adrVtr;
	 
 	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    xml += "<pdf>";
  
    xml += "<head>";
    xml += "  <macrolist>";
    xml += " <macro id=\"myfooter\">";
    xml += "<i font-size=\"8\">";

	//xml += " <table width=\"100%\">"	
	/*
	xml += "	<tr>";
	
	if(parseInt(i_global_subsidiary) == parseInt(3))
	xml += "<td border-top=\"0.3\" align=\"justify\"  font-family=\"Helvetica\"  font-size=\"8\" width=\"100%\"  border-bottom=\"0\"  border-left=\"0\" border-right=\"0\">Registered Office : Brillio Technologies Pvt. Ltd., #58, 1st Main Road, Mini Forest, JP Nagar 3rd Phase,Bangalore,Karnataka - 560078<\/td>";
	if(parseInt(i_global_subsidiary) == parseInt(9))
	xml += "		<td border-top=\"0.3\" align=\"justify\"  font-family=\"Helvetica\"  font-size=\"8\" width=\"100%\"  border-bottom=\"0\"  border-left=\"0\" border-right=\"0\"><b>Registered Office</b> : Comity Designs Private Limited, Unit no.601,Anant Exquisite,Piramal Nagar Road,Goregaon West, Goregaon, Mumbai,Maharashtra 400062<\/td>";
	
	xml += "	<\/tr>";
	*/
/*
	xml += "	<tr>";
	xml += "		<td border-top=\"0.3\" align=\"justify\"  font-family=\"Helvetica\"  font-size=\"8\" width=\"100%\"  border-bottom=\"0\"  border-left=\"0\" border-right=\"0\">Bangalore, Karnataka - 560078<\/td>";
	xml += "	<\/tr>";
*/
  //  xml += "<\/table> "
	
    xml += " <table width=\"100%\">"	
	xml += "	<tr>";
	xml += "<td align=\"left\"  font-family=\"Helvetica\"  font-size=\"8\" width=\"100%\" border-top=\"0\" border-bottom=\"0\"  border-left=\"0\" border-right=\"0\"> <\/td>";
	//xml += "		<td align=\"right\"  font-family=\"Helvetica\"  font-size=\"8\" width=\"50%\" border-top=\"0\" border-bottom=\"0\"  border-left=\"0\" border-right=\"0\">CIN U22190KA1997FTC022250<\/td>";
	xml += "	<\/tr>";
    xml += "<\/table> "

   
    xml += "<\/i>";
    xml += "    <\/macro>";
    xml += "  <\/macrolist>";
    xml += "<\/head>";
 
    xml += "<body margin-top=\"0pt\"  footer=\"myfooter\" footer-height=\"5em\">";
	
	
    xml += strVar;
    xml += "</body>\n</pdf>";
    
    // run the BFO library to convert the xml document to a PDF
    var file = nlapiXMLToPDF(xml);
    //Create PDF File with TimeStamp in File Name
    
    var d_currentTime = new Date();
    var timestamp = d_currentTime.getTime();
    //Create PDF File with TimeStamp in File Name
    
    var fileObj = nlapiCreateFile('Purchase Order' + '.pdf', 'PDF', file.getValue());
	var i_recipient=s_vendor_email?i_vendor:i_user;
	//var s_body='The Vendor PO Number'+o_recordOBJ.getFieldValue('tranid') +'for Vendor'+i_vendor_code+' - and Subsidiary -'+o_recordOBJ.getFieldValue('subsidiary')+'has been raised.'; 
	var s_body="The VB";
			nlapiSendEmail(constant.Mail_Author.InformationSystems,
			        i_recipient,'Purchase Order Email',
			        s_body, null, null,
				        null,fileObj
			        );
	
    //fileObj.setFolder(52);// Folder PDF File is to be stored
    //var fileId = nlapiSubmitFile(fileObj);
    //nlapiLogExecution('DEBUG', 'Purchase Order', '************* PDF ID *********** -->' + fileId);
    //response.setContentType('PDF', 'Purchase Order.pdf', 'inline');
    //response.write(file.getValue());
    }
	catch(exception)
	{		
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);
	}
}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

function additional_terms()
{	
	var strVar="";
	strVar += "<table border=\"0\" width=\"100%\" style = \"padding-top:-5px;\">";
	
	strVar += "	<tr>";
	strVar += "		<td align=\"center\" font-family=\"Helvetica\" font-size=\"10\" ><b><u>ADDITIONAL TERMS<\/u><\/b><\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >1)	PRICE BASIS:<\/td>";
	strVar += "	<\/tr>";
	

	var str1 = 'As per purchase order. If not other wise specified, price basis is to be considered as for door delivery of the goods ordered or services provided.' 
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str1)+"<\/td>";
	strVar += "	<\/tr>";
	
/** New Terms added on 21 June 2021 by Shravan ***/
    var str2_h = '2) GOODS & SERVICE TAX (GST)';
		var s_Tax_Term1 = 'Goods & Service Tax (GST): as applicable ';
	var s_Tax_Term2 = 'Other duties & levies, as applicable subject to prior intimation and our confirmation.'
	var s_Tax_Term3 = '2.1 Vendor represents, warrants and undertakes to comply with the following:'
	var s_Tax_Term4 = '(i)Vendor shall raise a valid, correct and complete tax invoice as prescribed under the applicable laws. Brillio shall not be obliged to pay GST charged by the Vendor in its invoice until such time it appears in GSTR-2A of Brillio. Brillio shall be absolved from its obligation to pay GST charged by the Vendor in such invoice, if such GST does not appear as input credit in GSTR-2A of Brillio, in the Subsequent month from the date of invoice.'
	var s_Tax_Term5 = '(ii)	Vendor shall ensure that all invoices/credit note/debit note/supplementary invoice/receipt voucher/ refund voucher/ other related documents, as the case maybe, pertaining to the transactions under this PO (each, a "Payment Document" and collectively, the "Payment Documents"), are received by Brillio within 7 (seven) days from the date of such Supply of Goods/Services.'
	var s_Tax_Term6 = '(iii)	Vendor shall comply with all the provisions and compliance requirements under applicable laws, including without limitation, timely registration (including any amendments/additions/deletions thereto), filing of all necessary information and returns/ forms, furnishing of all Payment Documents Including e-Invoicing, payment of applicable taxes, etc.'
	var s_Tax_Term7 = '(iv)	Vendor shall maintain 100% GST Compliance, or such other parameters/ benchmarks as prescribed by the relevant authorities, from time to time.'
	var s_Tax_Term8 =  '2.2 In case of any breach/ failure by the Vendor of any of the terms set forth in  above, due to which: (a) tax credit is denied/ disallowed to Brillio, or Brillio is unable to avail tax credit on payments made to the Vendor under this Agreement; or (b) the relevant authorities seek to recover any such tax amounts from Brillio, or impose any interest, penalty or other charges/ liabilities on Brillio ("Tax Liabilities"); then, in such case, until such breach is rectified by the Vendor and all supporting documents/ proof in respect thereof is provided to Brillio; and Brillio receives the tax credit that was originally denied/ unavailable to it; Brillio shall have the option, at its discretion, not to pay the Vendor the amount of taxes charged on any further Payment Documents under the Agreement, and/ or withhold, adjust, set-off or otherwise recover any such Tax Liabilities, together with any legal and other out-of-pocket costs, expenses, charges and amounts incurred by Brillio, from and against any other amounts deposited with Brillio, or that are payable to the Vendor, whether under the Agreement or otherwise. Without prejudice to the foregoing, the vendor shall also indemnify and keep indemnified, Brillio, from and against any disputes, claims, liabilities and other legal proceedings against Brillio, arising out of the breach of Clause.'
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str2_h)+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term1)+"<\/td>"
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term2)+"<\/td>"
	strVar += "	<\/tr>";
	
	
	strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term3)+"<\/td>"
	strVar += "	<\/tr>";
	
	 strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term4)+"<\/td>"
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term5)+"<\/td>"
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term6)+"<\/td>"
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term7)+"<\/td>"
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
		strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(s_Tax_Term8)+"<\/td>"
	strVar += "	<\/tr>";

	/** New Terms added on 21 June 2021 by Shravan ***/

	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >3)	PACKING<\/td>";
	strVar += "	<\/tr>";
	
	var str3 = 'Suitable packing to be ensured to see that material supplied is not damaged or lost in transit.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str3)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >4)	INSURANCE:<\/td>";
	strVar += "	<\/tr>";
	var str4 = 'As specified. If not specified, supplier to arrange for comprehensive insurance at his cost and expenses covering value of goods from the place of dispatch to delivery at our works.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str4)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >5)	FREIGHT:<\/td>";
	strVar += "	<\/tr>";
	var str5 = 'As specified. If not specified, price as per order is to be treated as inclusive of freight charges for delivery at our works.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str5)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >6)	PAYMENT TERMS:<\/td>";
	strVar += "	<\/tr>";
	var str6 = 'As specified in the order. If not specified, within 90 days from the date of acceptance of goods/Services.';
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str6)+"<\/td>";
	strVar += "	<\/tr>";
	var str7_h = '7)	PLACE & MODE OF DELIVERY:'	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str7_h)+"<\/td>";
	strVar += "	<\/tr>";
	var str7 = 'As specified in the order. If not specified, goods ordered or services to be provided is to be delivered/ or provided at our works as per supply schedule.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str7)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >8)	GUARANTEE\/WARRANTY:<\/td>";
	strVar += "	<\/tr>";
	
	var str8 = 'Goods/Services should be guaranteed as free from all defects and should be of quality and specifications defined and agreed to.'
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str8)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >9)	EXCESS SUPPLY\/SUPPLY AHEAD OF SCHEDULE:<\/td>";
	strVar += "	<\/tr>";
	
	var str9 = 'Materials excess supplied/supplied ahead of schedule will be liable for rejection & returned at your cost, risk & expenses.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str9)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >10)	SHORT SUPPLY:<\/td>";
	strVar += "	<\/tr>";
	
	var str10 = 'In the event of short supply of materials ordered, the order to the extent of short supply is to be treated as cancelled, without any further intimation.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str10)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >11)	DELAYED SUPPLY:<\/td>";
	strVar += "	<\/tr>";
	
	var str11 = 'In the event of delayed supply of the materials ordered, the material supplied shall be liable for rejection and returned at your cost, risk & expenses. Otherwise if goods are accepted, Penalty will be charged @ 0.5% per week, or maximum 5% of the total value of the purchase order.'
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str11)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >12)	CONFIDENTIALITY:<\/td>";
	strVar += "	<\/tr>";
	
	var str12 = 'The design, drawings, technical specifications, process sheets, video slides and presentation materials and soft copy of theforegoing shall be the exclusive property of BRILLIO unless otherwise specified in the SOW and shall be returned along with the goods manufactured/ordered. The manufacturer/supplier shall not use, disclose, copy reveal any and all information made available to him to any  third party without taking prior permission in writing from BRILLIO, In the event of breach of these conditions,BRILLIOwill initiate action for recovery of damages, breach of trust against the supplier.'
	
	var str122 = 'The design, drawings, technical specifications, process sheets, video slides and presentation materials and soft copy of theforegoing shall be the exclusive property of COMITY unless otherwise specified in the SOW and shall be returned along with the goods manufactured/ordered. The manufacturer/supplier shall not use, disclose, copy reveal any and all information made available to him to any  third party without taking prior permission in writing from COMITY, In the event of breach of these conditions,COMITY will initiate action for recovery of damages, breach of trust against the supplier.'
	strVar += "	<tr>";
	//if(parseInt(i_global_subsidiary) == parseInt(3))
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str12)+"<\/td>";
   // if(parseInt(i_global_subsidiary) == parseInt(9))
	//strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str122)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >13)	ACCEPTANCE OF ORDER:<\/td>";
	strVar += "	<\/tr>";
	
	var str13 = 'Acceptance of order by supplier includes acceptance of all terms and conditions specified above and will be binding on the supplier.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str13)+"<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >14)	GENERAL:<\/td>";
	strVar += "	<\/tr>";
	var str14 = 'All the terms and conditions agreed / specified in your quotation, catalogue, leaflets, letters and minutes of the discussion held during negotiations, technical specifications furnished during the course of negotiations performance and other guarantees specified, rights, duties and responsibilities as stated/committed and understood by both the parties will be treated as part and parcel of this order.'
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str14)+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"8\"  colspan=\"10\">&nbsp;<\/td>";
	strVar += "	<\/tr>";
	strVar += "	<tr>";
	//strVar += "		<td colspan=\"10\" font-family=\"Helvetica\" font-size=\"8\">This is a system generated PO , no signature required.<\/td>";
	strVar += "		<\/tr>"; 
	
	strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >15)	JURISDICTION:<\/td>";
	strVar += "	<\/tr>";
	
	var str15 = 'All the disputes arising out of and in connection with this purchase order is subjected to Bangalore Jurisdiction only.'
    strVar += "	<tr>";
	strVar += "		<td font-family=\"Helvetica\" font-size=\"9\" >"+nlapiEscapeXML(str15)+"<\/td>";
	strVar += "	<\/tr>";
	
	strVar += "	<tr>";
	strVar += "		<td  font-family=\"Helvetica\" font-size=\"9\"><\/td>";
	strVar += "		<\/tr>"; 
	
	strVar += "	<tr>";
	strVar += "		<td  font-family=\"Helvetica\" font-size=\"9\"><\/td>";
	strVar += "		<\/tr>"; 
	
		strVar += "	<tr>";
	strVar += "		<td  font-family=\"Helvetica\" font-size=\"9\"><\/td>";
	strVar += "		<\/tr>"; 
	
	strVar += "	<tr>";
	strVar += "		<td  font-family=\"Helvetica\" font-size=\"9\"><\/td>";
	strVar += "		<\/tr>"; 
	
	/*
strVar += "	<tr>";
	strVar += "		<td  font-family=\"Helvetica\" font-size=\"9\">This is a system generated PO , no signature required.<\/td>";
	strVar += "		<\/tr>"; 
*/

		
	strVar += "<\/table>";

	return strVar;	
}

/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!='null' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function formatAndReplaceSpacesofMessage(messgaeToBeSendPara)
{
 if(messgaeToBeSendPara!=null && messgaeToBeSendPara!=''&& messgaeToBeSendPara!=undefined)
 {
   messgaeToBeSendPara = messgaeToBeSendPara.toString();   
   messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g,"<br/>");/// /g  
  return messgaeToBeSendPara;
 }

}


function get_vendor_details(i_vendor)
{
	var i_vendor_name = '';
	var i_vendor_code = '';
	var i_vendor_address = '';
	var a_return_array = new Array();
		
 	if (_logValidation(i_vendor)) 
	{
		/*
var filter = new Array();
		filter[0] = new nlobjSearchFilter('internalid', null, 'is', i_vendor);
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('altname')
		columns[1] = new nlobjSearchColumn('entityid')
		columns[2] = new nlobjSearchColumn('defaultaddress')
*/
		
		try
		{
			var o_vendorOBJ = nlapiLoadRecord('vendor',i_vendor)
		}
		catch(er)
		{
			var o_vendorOBJ = nlapiLoadRecord('customer',i_vendor)
		}
		
		
	//	var a_search_results = nlapiSearchRecord('entity', null, filter, columns);
		
		if (o_vendorOBJ != null && o_vendorOBJ != '' && o_vendorOBJ != undefined) 
		{
	    i_vendor_name = o_vendorOBJ.getFieldValue('altname');
		nlapiLogExecution('DEBUG', 'get_vendor_details', ' Vendor Name -->' + i_vendor_name);
		
		i_vendor_code =  o_vendorOBJ.getFieldValue('entityid');
		nlapiLogExecution('DEBUG', 'get_vendor_details', ' Vendor Code -->' + i_vendor_code);
		
		i_vendor_address =  o_vendorOBJ.getFieldValue('defaultaddress');
		nlapiLogExecution('DEBUG', 'get_vendor_details', ' Vendor Address -->' + i_vendor_address);	
		i_vendor_email =  o_vendorOBJ.getFieldValue('email');
		   
				
				/*
i_vendor_name = a_search_results[0].getValue('altname');
				nlapiLogExecution('DEBUG', 'get_vendor_details', ' Vendor Name -->' + i_vendor_name);
				
				i_vendor_code = a_search_results[0].getValue('entityid');
				nlapiLogExecution('DEBUG', 'get_vendor_details', ' Vendor Code -->' + i_vendor_code);
				
				i_vendor_address = a_search_results[0].getValue('defaultaddress');
				nlapiLogExecution('DEBUG', 'get_vendor_details', ' Vendor Address -->' + i_vendor_address);
*/
		
		}
	}	
	
	a_return_array[0] = i_vendor_name+'######'+i_vendor_code+'######'+i_vendor_address+'######'+i_vendor_email;
	
	return a_return_array;
}





//--------------------------------Function Begin -  Amount in words----------------------------------------------------------------//
function toWordsFunc(s,i_currency)
{ 
    var str = '';
   if(i_currency =='USD')
   {
   	  str='USD'+' '
   }
   if(i_currency =='INR')
   {
     str='INR'+' '
   }
   if(i_currency =='GBP')
   {
     str='GBP'+' '
   }
   if(i_currency =='SGD')
   {
     str='SGD'+' '
   }
   if(i_currency =='Peso')
   {
     str='Peso'+' '
   }

    
    var th  = new Array ('Crore ','Lakhs ','Thousand ','Hundred ');
	var dg = new Array ('10000000','100000','1000','100');
	var dem=s.substr(s.lastIndexOf('.')+1)
    s=parseInt(s)
    var d
    var n1,n2
    while(s>=100)
    {
       for(var k=0;k<4;k++)
        {
            d=parseInt(s/dg[k])
            if(d>0)
            {
                if(d>=20)
                {
	                n1=parseInt(d/10)
	                n2=d%10
	                printnum2(n1)
	                printnum1(n2)
                }
     			 else
      			printnum1(d)
     			str=str+th[k]
   			 }
    		s=s%dg[k]
        }
    }
	 if(s>=20)
	 {
	            n1=parseInt(s/10)
	            n2=s%10
	 }
	 else
	 {
	            n1=0
	            n2=s
	 }

	printnum2(n1)
	printnum1(n2)
	if(dem>0)
	{
		decprint(dem)
	}
	return str

	function decprint(nm)
	{
         if(nm>=20)
	     {
            n1=parseInt(nm/10)
            n2=nm%10
	     }
		  else
		  {
		              n1=0
		              n2=parseInt(nm)
		  }
		  str=str+'And '
		  printnum2(n1)
	 	  printnum1(n2)
	}

	function printnum1(num1)
	{
        switch(num1)
        {
			  case 1:str=str+'One '
			         break;
			  case 2:str=str+'Two '
			         break;
			  case 3:str=str+'Three '
			        break;
			  case 4:str=str+'Four '
			         break;
			  case 5:str=str+'Five '
			         break;
			  case 6:str=str+'Six '
			         break;
			  case 7:str=str+'Seven '
			         break;
			  case 8:str=str+'Eight '
			         break;
			  case 9:str=str+'Nine '
			         break;
			  case 10:str=str+'Ten '
			         break;
			  case 11:str=str+'Eleven '
			        break;
			  case 12:str=str+'Twelve '
			         break;
			  case 13:str=str+'Thirteen '
			         break;
			  case 14:str=str+'Fourteen '
			         break;
			  case 15:str=str+'Fifteen '
			         break;
			  case 16:str=str+'Sixteen '
			         break;
			  case 17:str=str+'Seventeen '
			         break;
			  case 18:str=str+'Eighteen '
			         break;
			  case 19:str=str+'Nineteen '
			         break;
		}
	}

	function printnum2(num2)
	{
	    switch(num2)
		{
				  case 2:str=str+'Twenty '
				         break;
				  case 3:str=str+'Thirty '
				        break;
				  case 4:str=str+'Forty '
				         break;
				  case 5:str=str+'Fifty '
				         break;
				  case 6:str=str+'Sixty '
				         break;
				  case 7:str=str+'Seventy '
				         break;
				  case 8:str=str+'Eighty '
				         break;
				  case 9:str=str+'Ninety '
				         break;
        }
    
	}
}

//--------------------------------------------Function End - Amount in Words----------------------------------//


function formatDollar(somenum) 
{
    var split_arr = new Array()
	var i_no_before_comma;
	var i_no_before_comma_length;	
	if (somenum != null && somenum != '' && somenum != undefined) 
	{  
	    split_arr = somenum.toString().split('.')
		i_no_before_comma = split_arr[0]
		i_no_before_comma = Math.abs(i_no_before_comma)	
		
		if(i_no_before_comma.toString().length<=3)
		{
			return  somenum ;		
		}
		else
		{		
		var p = somenum.toString().split(".");
		
		if(p[1]!=null && p[1]!='' && p[1]!=undefined)
		{
			p[1]=p[1]
		}
		else
		{
			p[1]='00'
		}		
    return p[0].split("").reverse().reduce(function(acc, somenum, i, orig) 
	{
        return  somenum + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
		}
	}	
}

function removearrayduplicate(array)
{
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) 
	{
        for (var j = 0; j < array.length; j++) 
		{
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}
function tax_group_calculation(i_tax_code,i_amount)
{
	var a_return_array = new Array();
	var a_tax_return_array = new Array();
	var a_final_array = new Array();
	
	 if(_logValidation(i_tax_code))
	 {
	 	try
		{
			var o_tax_groupOBJ = nlapiLoadRecord('taxgroup', i_tax_code)
			
			if(_logValidation(o_tax_groupOBJ))
			{ 
			  var i_tax_line_count = o_tax_groupOBJ.getLineItemCount('taxitem');
	          nlapiLogExecution('DEBUG', 'tax_group_calculation', 'Tax Line Count -->' + i_tax_line_count);
			  
			  if(_logValidation(i_tax_line_count))
			  {
			  	 for (var j = 1; j <= i_tax_line_count; j++) 
				 {
				 	var i_tax_rate = o_tax_groupOBJ.getLineItemValue('taxitem', 'rate', j);
	                nlapiLogExecution('DEBUG', 'tax_group_calculation', 'TAX Rate -->' + i_tax_rate);
	                
					var i_tax_name = o_tax_groupOBJ.getLineItemValue('taxitem', 'taxname2', j);
	                nlapiLogExecution('DEBUG', 'tax_group_calculation', ' TAX Name -->' + i_tax_name);
					
	                var i_tax_basis = o_tax_groupOBJ.getLineItemValue('taxitem', 'basis', j);
					//var i_amount = parseFloat(parseFloat(parseFloat(i_amount) * parseFloat(i_tax_rate)) / 100)
	                //nlapiLogExecution('DEBUG', 'tax_group_calculation', ' TAX Amount -->' + i_amount);
									
					a_return_array.push(i_tax_name+'*****###*****'+i_amount+'*****###*****'+i_tax_basis+'*****###*****'+i_tax_rate);
					a_tax_return_array.push(i_tax_name);
				 }//Loop Tax Line Count  	
				
			  }//Tax Code	 	
				
			 }//Tax Group
		}
		catch(exception1)
		{
		 nlapiLogExecution('DEBUG', 'tax_group_calculation', ' Exception Caught -->' + exception1);		
		 
		    if(exception1== 'RCRD_DSNT_EXIST')
			{
			  a_return_array = ''
			  a_tax_return_array = ''
				
			}//RCRD_DSNT_EXIST  	
		}
	  		 	
	 }
	 
	if(_logValidation(a_return_array)&&_logValidation(a_tax_return_array))
	{
		a_final_array[0] = a_return_array+'!!!!!@@@@@@!!!!!'+a_tax_return_array;
	} 
	else
	{
		a_final_array[0] = '';
	}
	
	return a_final_array;
}//Tax Group Calculation

function escape_special_chars(text)
{
	var iChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var text_new =''
		
	if(text!=null && text!='' && text!=undefined)
	{
		for(var y=0; y<text.toString().length; y++)
		{	
		  if(iChars.indexOf(text[y])== -1)
		  {		  
			text_new +=text[y]
		  }		
			
		}//Loop			
		
	}//Text Validation	
	
	return text_new;
}

function escape_special_chars_text(text)
{
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.";
	var text_new =''
		
	if(text!=null && text!='' && text!=undefined)
	{
		for(var y=0; y<text.toString().length; y++)
		{	
		  if(iChars.indexOf(text[y])== -1)
		  {		  
			text_new = true;
		  }
		  else 
		  {
		  	text_new = false;
		  }		
			
		}//Loop				
	}//Text Validation	
	return text_new;
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
