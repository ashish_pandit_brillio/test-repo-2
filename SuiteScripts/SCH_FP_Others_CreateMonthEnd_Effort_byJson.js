/**
 * @author Deepak MS Date:20 Feb 2017
 */

function scheduled_fp_others_create_effort_plan(type)
{
	try
	{
		nlapiLogExecution('DEBUG','test','test');
		var i_fp_others_plan_parent = '';
		var i_context = nlapiGetContext();
		var i_mnth_end_json_id = i_context.getSetting('SCRIPT', 'custscript_month_end_other_json');
		var i_rev_rec_id=i_context.getSetting('SCRIPT','custscript_other_rev_rec_id');
      		//var i_mnth_end_json_id=2263;
			//var i_rev_rec_id=173;
		nlapiLogExecution('audit','project id:- ','revenue share id:- '+i_rev_rec_id);
      nlapiLogExecution('audit','project id:- ','revenue share id:- '+i_mnth_end_json_id);
	
		if(i_rev_rec_id)
		{
			var rev_par_filter=[['internalid','anyof',i_rev_rec_id]];
			var column=new Array();
			column[0] = new nlobjSearchColumn('created').setSort(true);
			column[1]=new nlobjSearchColumn('custrecord_fp_rev_rec_pro_st_date');
			column[2]=new nlobjSearchColumn('custrecord_fp_rev_rec_proj_end_date');
			column[3]=new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
			var a_get_revenue_cap=nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, rev_par_filter, column);
		
			if(i_mnth_end_json_id)
			{
				var a_revenue_cap_filter = [['internalid', 'anyof', i_mnth_end_json_id]];
					
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_fld_json1');
				a_column[2] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_fld_json2');
				a_column[3] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_fld_json3');
				a_column[4] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_fld_json4');
				a_column[5] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_fld_json5');
				var a_get_Json_Search = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, a_revenue_cap_filter, a_column);
		
			}
		}

		if (a_get_revenue_cap)
		{
			for(var i_index=0; i_index<a_get_revenue_cap.length; i_index++)
			{
				var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_pro_st_date')),
			        nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_proj_end_date')));
					
				i_fp_others_plan_parent = a_get_revenue_cap[i_index].getId();
				var i_revenue_share_project = a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_others_projec');
				var projectWiseRevenue_previous_effrt = [];
				var o_mnth_end_parent = nlapiCreateRecord('customrecord_fp_rev_rec_others_mont_end');
				o_mnth_end_parent.setFieldValue('custrecord_fp_rev_rec_month_end_parent_l', i_fp_others_plan_parent);
				var i_mnth_end_parent_rcrd_id = nlapiSubmitRecord(o_mnth_end_parent, true, true);
				nlapiLogExecution('audit','mnth end rcrd id:- ',i_mnth_end_parent_rcrd_id);
				for(var i_ind=0;i_ind <a_get_Json_Search.length;i_ind++)
				{
				var s_effrt_json_1_prev_mnth = a_get_Json_Search[i_ind].getValue('custrecord_fp_others_mnth_end_fld_json1');
				var s_effrt_json_2_prev_mnth = a_get_Json_Search[i_ind].getValue('custrecord_fp_others_mnth_end_fld_json2');
				var s_effrt_json_3_prev_mnth = a_get_Json_Search[i_ind].getValue('custrecord_fp_others_mnth_end_fld_json3');
				var s_effrt_json_4_prev_mnth = a_get_Json_Search[i_ind].getValue('custrecord_fp_others_mnth_end_fld_json4');
				var s_effrt_json_5_prev_mnth = a_get_Json_Search[i_ind].getValue('custrecord_fp_others_mnth_end_fld_json5');
				if(s_effrt_json_1_prev_mnth)
				{
					//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
					
					{
						var i_practice_previous = 0;
						var f_total_revenue_for_tenure = 0;
						//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
						var s_entire_json_clubed = JSON.parse(s_effrt_json_1_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
							var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
							for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
							{
								var i_practice = a_row_json_data[i_row_json_index].prac;
								var s_practice = a_row_json_data[i_row_json_index].prac_text;
								var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
								var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
								var i_role = a_row_json_data[i_row_json_index].role;
								var s_role = a_row_json_data[i_row_json_index].role_text;
								var i_level = a_row_json_data[i_row_json_index].level;
								var s_level = a_row_json_data[i_row_json_index].level_text;
								var i_location = a_row_json_data[i_row_json_index].loc;
								var s_location = a_row_json_data[i_row_json_index].loc_text;
								var f_revenue = a_row_json_data[i_row_json_index].cost;
								if (!f_revenue) 
									f_revenue = 0;
								
								var f_revenue_share = a_row_json_data[i_row_json_index].share;
								if (!f_revenue_share) 
									f_revenue_share = 0;
									
								var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
								var s_mnth_strt_date = '1/31/2017';
								var s_mnth_end_date = '31/31/2017';
								var s_mnth = a_row_json_data[i_row_json_index].mnth;
								var s_year = a_row_json_data[i_row_json_index].year;		
								//var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);							
							
							}
						}
					}
				}
				
				create_record(i_mnth_end_parent_rcrd_id,s_entire_json_clubed,monthBreakUp,i_revenue_share_project);

				if(s_effrt_json_2_prev_mnth)
				{
					//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
					
					{
						var i_practice_previous = 0;
						var f_total_revenue_for_tenure = 0;
						//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
						var s_entire_json_clubed = JSON.parse(s_effrt_json_2_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
							var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
							for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
							{
								var i_practice = a_row_json_data[i_row_json_index].prac;
								var s_practice = a_row_json_data[i_row_json_index].prac_text;
								var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
								var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
								var i_role = a_row_json_data[i_row_json_index].role;
								var s_role = a_row_json_data[i_row_json_index].role_text;
								var i_level = a_row_json_data[i_row_json_index].level;
								var s_level = a_row_json_data[i_row_json_index].level_text;
								var i_location = a_row_json_data[i_row_json_index].loc;
								var s_location = a_row_json_data[i_row_json_index].loc_text;
								var f_revenue = a_row_json_data[i_row_json_index].cost;
								if (!f_revenue) 
									f_revenue = 0;
								
								var f_revenue_share = a_row_json_data[i_row_json_index].share;
								if (!f_revenue_share) 
									f_revenue_share = 0;
									
								var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
								var s_mnth_strt_date = '1/31/2017';
								var s_mnth_end_date = '31/31/2017';
								var s_mnth = a_row_json_data[i_row_json_index].mnth;
								var s_year = a_row_json_data[i_row_json_index].year;		
								//var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);							
							
							}
						}
					}
				}
				create_record(i_mnth_end_parent_rcrd_id,s_entire_json_clubed,monthBreakUp,i_revenue_share_project);
				if(s_effrt_json_3_prev_mnth)
				{
					//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
					
					{
						var i_practice_previous = 0;
						var f_total_revenue_for_tenure = 0;
						//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
						var s_entire_json_clubed = JSON.parse(s_effrt_json_3_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
							var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
							for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
							{
								var i_practice = a_row_json_data[i_row_json_index].prac;
								var s_practice = a_row_json_data[i_row_json_index].prac_text;
								var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
								var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
								var i_role = a_row_json_data[i_row_json_index].role;
								var s_role = a_row_json_data[i_row_json_index].role_text;
								var i_level = a_row_json_data[i_row_json_index].level;
								var s_level = a_row_json_data[i_row_json_index].level_text;
								var i_location = a_row_json_data[i_row_json_index].loc;
								var s_location = a_row_json_data[i_row_json_index].loc_text;
								var f_revenue = a_row_json_data[i_row_json_index].cost;
								if (!f_revenue) 
									f_revenue = 0;
								
								var f_revenue_share = a_row_json_data[i_row_json_index].share;
								if (!f_revenue_share) 
									f_revenue_share = 0;
									
								var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
								var s_mnth_strt_date = '1/31/2017';
								var s_mnth_end_date = '31/31/2017';
								var s_mnth = a_row_json_data[i_row_json_index].mnth;
								var s_year = a_row_json_data[i_row_json_index].year;		
								//var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);							
							
							}
						}
					}
				}
				create_record(i_mnth_end_parent_rcrd_id,s_entire_json_clubed,monthBreakUp,i_revenue_share_project);
				if(s_effrt_json_4_prev_mnth)
				{
					//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
					
					{
						var i_practice_previous = 0;
						var f_total_revenue_for_tenure = 0;
						//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
						var s_entire_json_clubed = JSON.parse(s_effrt_json_4_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
							var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
							for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
							{
								var i_practice = a_row_json_data[i_row_json_index].prac;
								var s_practice = a_row_json_data[i_row_json_index].prac_text;
								var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
								var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
								var i_role = a_row_json_data[i_row_json_index].role;
								var s_role = a_row_json_data[i_row_json_index].role_text;
								var i_level = a_row_json_data[i_row_json_index].level;
								var s_level = a_row_json_data[i_row_json_index].level_text;
								var i_location = a_row_json_data[i_row_json_index].loc;
								var s_location = a_row_json_data[i_row_json_index].loc_text;
								var f_revenue = a_row_json_data[i_row_json_index].cost;
								if (!f_revenue) 
									f_revenue = 0;
								
								var f_revenue_share = a_row_json_data[i_row_json_index].share;
								if (!f_revenue_share) 
									f_revenue_share = 0;
									
								var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
								var s_mnth_strt_date = '1/31/2017';
								var s_mnth_end_date = '31/31/2017';
								var s_mnth = a_row_json_data[i_row_json_index].mnth;
								var s_year = a_row_json_data[i_row_json_index].year;		
								//var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);							
							
							}
						}
					}
				}
				create_record(i_mnth_end_parent_rcrd_id,s_entire_json_clubed,monthBreakUp,i_revenue_share_project);
				if(s_effrt_json_5_prev_mnth)
				{
					//generate_previous_effrt_revenue(s_effrt_json_1_prev_mnth,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project);
					
					{
						var i_practice_previous = 0;
						var f_total_revenue_for_tenure = 0;
						//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
						var s_entire_json_clubed = JSON.parse(s_effrt_json_5_prev_mnth);
						for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
						{
							var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
							for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
							{
								var i_practice = a_row_json_data[i_row_json_index].prac;
								var s_practice = a_row_json_data[i_row_json_index].prac_text;
								var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
								var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
								var i_role = a_row_json_data[i_row_json_index].role;
								var s_role = a_row_json_data[i_row_json_index].role_text;
								var i_level = a_row_json_data[i_row_json_index].level;
								var s_level = a_row_json_data[i_row_json_index].level_text;
								var i_location = a_row_json_data[i_row_json_index].loc;
								var s_location = a_row_json_data[i_row_json_index].loc_text;
								var f_revenue = a_row_json_data[i_row_json_index].cost;
								if (!f_revenue) 
									f_revenue = 0;
								
								var f_revenue_share = a_row_json_data[i_row_json_index].share;
								if (!f_revenue_share) 
									f_revenue_share = 0;
									
								var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
								var s_mnth_strt_date = '1/31/2017';
								var s_mnth_end_date = '31/31/2017';
								var s_mnth = a_row_json_data[i_row_json_index].mnth;
								var s_year = a_row_json_data[i_row_json_index].year;		
								//var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);							
							
							}
						}
					}
				}
				create_record(i_mnth_end_parent_rcrd_id,s_entire_json_clubed,monthBreakUp,i_revenue_share_project);
			nlapiSubmitField('customrecord_fp_rev_rec_others_mont_end',parseInt(i_mnth_end_parent_rcrd_id),'custrecord_fp_created_by_json','T');
			}
			}
			
			nlapiLogExecution('audit','mail sent for approval');
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','scheduled_create_effort_plan','ERROR MESSAGE :- '+err);
	}
}



function create_record(i_mnth_end_parent_rcrd_id,s_entire_json_clubed,monthBreakUp,i_revenue_share_project)
{
	try
	{
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_get_exsiting_effort_plan = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				
			for (var i_effort_index = 0; i_effort_index < a_get_exsiting_effort_plan.length;i_effort_index++)
			{
				//for (var j = 0; j < monthBreakUp.length; j++)
				{
					if(nlapiGetContext().getRemainingUsage() <= 500)
				{
					nlapiYieldScript();
				}
					//var months = monthBreakUp[j];
					//var s_month_name = getMonthName(months.Start); // get month name
					//var month_strt = nlapiStringToDate(months.Start);
					//var month_end = nlapiStringToDate(months.End);
					//var i_month = month_strt.getMonth();
					//var i_year = month_strt.getFullYear();
					//i_year = parseInt(i_year);
					
				//var i_total_days_in_mnth = new Date(i_year, i_month + 1, 0).getDate();
					
					var parent_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].prac;
					var sub_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].subprac;
					var role_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].role;
					var level_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].level;
					var location_mnth_end_effrt = a_get_exsiting_effort_plan[i_effort_index].loc;
					var i_no_of_resources = a_get_exsiting_effort_plan[i_effort_index].allo;
					//var i_prcnt_allocated = a_get_exsiting_effort_plan[i_effort_index].cost;
					var s_month_name=a_get_exsiting_effort_plan[i_effort_index].mnth;
					var i_year=a_get_exsiting_effort_plan[i_effort_index].year;
					var f_cost_for_resource = a_get_exsiting_effort_plan[i_effort_index].cost;
					var mnthly_share=a_get_exsiting_effort_plan[i_effort_index].share;
					var st_date_end_date=get_current_month_end_date(s_month_name,i_year);
					var month_start=st_date_end_date[0].d_start_date;
					var month_end=st_date_end_date[0].d_end_date;
					
					var inactive_practice = nlapiLookupField('department',sub_practice_month_end_effrt,'isinactive');
                  if(inactive_practice == 'F')
                    {
					var o_actual_effrt_mnth_end = nlapiCreateRecord('customrecord_fp_rev_rec_othr_mnth_end_ef');
					o_actual_effrt_mnth_end.setFieldValue('custrecordfp_others_mnth_end_practice', parent_practice_month_end_effrt);
					o_actual_effrt_mnth_end.setFieldValue('custrecordfp_othr_month_end_sub_practice', sub_practice_month_end_effrt);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_month_end_role', role_month_end_effrt);
					o_actual_effrt_mnth_end.setFieldValue('custrecordfp_other_mnth_end_level', level_month_end_effrt);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others__location_mnth_end', location_mnth_end_effrt);				
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_year', s_month_name);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_year_y', i_year);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_st_date', nlapiStringToDate(month_start));
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnt_end_date', nlapiStringToDate(month_end));
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_end_alloc_per', i_no_of_resources);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others__resource_cost', f_cost_for_resource);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_othr__mnth_end_project_pla', i_revenue_share_project);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_fp_others_mnth_act_parent', i_mnth_end_parent_rcrd_id);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_split_share', mnthly_share);
					var i_month_end_activity = nlapiSubmitRecord(o_actual_effrt_mnth_end);
                    }
					
				}
				
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','create_record','ERROR MESSAGE :- '+err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}
function get_current_month_end_date(i_month,i_year)
{
	var d_start_date='';
	var d_end_date = '';
	var date_st_end=[];
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
				date_st_end.push({
				d_start_date :i_year + '-' + 1 + '-' + 1,	
			d_end_date : i_year + '-' + 1 + '-' + 31
				});
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
				date_st_end.push({
				d_start_date : 1 + '/' + 1 + '/' + i_year,
			d_end_date : 31 + '/' + 1 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date :1 + '/' + 1 + '/' + i_year,
			d_end_date : 1 + '/' + 31 + '/' + i_year
			});
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 2 + '-' + 1,	
			d_end_date : i_year + '-' + 2 + '-' + i_day
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 2 + '/' + i_year,
			d_end_date : i_day + '/' + 2 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:2 + '/' + 1 + '/' + i_year,
			d_end_date : 2 + '/' + i_day + '/' + i_year});
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 3 + '-' + 1,	
			d_end_date : i_year + '-' + 3 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 3 + '/' + i_year,
			d_end_date : 31 + '/' + 3 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:3 + '/' + 1 + '/' + i_year,
			d_end_date : 3 + '/' + 31 + '/' + i_year
			});
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 4 + '-' + 1,	
			d_end_date :i_year + '-' + 4 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 4 + '/' + i_year,
			d_end_date : 30 + '/' + 4 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date :4 + '/' + 1 + '/' + i_year,
			d_end_date : 4 + '/' + 30 + '/' + i_year
			});
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 5 + '-' + 1,		
			d_end_date : i_year + '-' + 5 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 5 + '/' + i_year,
			d_end_date : 31 + '/' + 5 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:5 + '/' + 1 + '/' + i_year,
			d_end_date : 5 + '/' + 31 + '/' + i_year
			});
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 6 + '-' + 1,		
			d_end_date : i_year + '-' + 6 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 6 + '/' + i_year,
			d_end_date : 30 + '/' + 6 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:6 + '/' + 1 + '/' + i_year,
			d_end_date : 6 + '/' + 30 + '/' + i_year
			});
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 7 + '-' + 1,		
			d_end_date : i_year + '-' + 7 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 7 + '/' + i_year,
			d_end_date : 31 + '/' + 7 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:7 + '/' + 1 + '/' + i_year,
			d_end_date : 7 + '/' + 31 + '/' + i_year
			});
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 8 + '-' + 1,		
			d_end_date : i_year + '-' + 8 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 8 + '/' + i_year,
			d_end_date : 31 + '/' + 8 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:8 + '/' + 1 + '/' + i_year,
			d_end_date : 8 + '/' + 31 + '/' + i_year
			});
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 9 + '-' + 1,	
			d_end_date : i_year + '-' + 9 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 9 + '/' + i_year,
			d_end_date : 30 + '/' + 9 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:9 + '/' + 1 + '/' + i_year,
			d_end_date : 9 + '/' + 30 + '/' + i_year
			});
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 10 + '-' + 1,		
			d_end_date : i_year + '-' + 10 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 10 + '/' + i_year,
			d_end_date : 31 + '/' + 10 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:10 + '/' + 1 + '/' + i_year,
			d_end_date : 10 + '/' + 31 + '/' + i_year
			});
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 11 + '-' + 1,		
			d_end_date : i_year + '-' + 11 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 11 + '/' + i_year,
			d_end_date : 30 + '/' + 11 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:11 + '/' + 1 + '/' + i_year,
			d_end_date : 11 + '/' + 30 + '/' + i_year
			});
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 12 + '-' + 1,		
			d_end_date : i_year + '-' + 12 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 12 + '/' + i_year,
			d_end_date : 31 + '/' + 12 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:12 + '/' + 1 + '/' + i_year,
			d_end_date : 12 + '/' + 31 + '/' + i_year
			});
	     }	 
			
	  }	
 }//Month & Year
 return date_st_end;
}
function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}