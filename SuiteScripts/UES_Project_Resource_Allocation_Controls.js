/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Project_Resource_Allocation_Controls.js
	Author      : Shweta Chopde
	Date        : 20 May 2014
	Description : Controls on Resource Allocation


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
   12-Mar-2020		Praveena madem						Deepak Ms					Changed iternal id of saved search and searched record type due to migration


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)


     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{
	//Added by Vinod
	var ProjType = form.addField('custpage_projtype', 'integer', 'Project Type');
	ProjType.setDisplayType('hidden');
	
	if(nlapiGetFieldValue('project') != null && nlapiGetFieldValue('project').length > 0)
	{
		ProjType.setDefaultValue(nlapiLookupField('job', nlapiGetFieldValue('project'), 'jobtype'));
	}
	else
	{
		ProjType.setDefaultValue(0);
	}
	
	if(nlapiGetFieldValue('custpage_projtype') == 1)
	{
		form.getField('custeventrbillable').setDisplayType('hidden');
		form.getField('custevent_otbillable').setDisplayType('hidden');
		form.getField('custevent_otpayable').setDisplayType('hidden');
		form.getField('custevent_otrate').setDisplayType('hidden');
		form.getField('custevent_otserviceitem').setDisplayType('hidden');
	}
	else
	{
		var OTSrvcItem = form.getField('custevent_otserviceitem');
		OTSrvcItem.getSelectOptions('OT', 'is');
	}

	if(type == 'create')
	{
		form.getField('custevent_otbillable').setDisplayType('disabled');
		form.getField('custevent_otpayable').setDisplayType('disabled');
		form.getField('custevent_otrate').setDisplayType('disabled');
		form.getField('custevent_otserviceitem').setDisplayType('disabled');
	}
	else if(type == 'edit')
	{
		if(nlapiGetFieldValue('custeventrbillable') == 'F')
		{
			form.getField('custevent_otbillable').setDisplayType('disabled');
			form.getField('custevent_otpayable').setDisplayType('disabled');
			form.getField('custevent_otrate').setDisplayType('disabled');
		}
	}

	return true;

}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmit_resource_controls(type) //
{
	if (type != 'delete') //
	{
		var i_recordID = nlapiGetRecordId();
		var i_project = nlapiGetFieldValue('project') //
		var d_start_date_new = nlapiGetFieldValue('startdate');
				
		var d_end_date_new = nlapiGetFieldValue('enddate');
		
		if (_logValidation(i_project)) //
		{
			var o_projectOBJ = nlapiLoadRecord('job', i_project);
			
			if (_logValidation(o_projectOBJ)) //
			{
				var i_project_manager = o_projectOBJ.getFieldValue('custentity_projectmanager')
				
				if (!_logValidation(i_project_manager)) //
				{
					throw 'You are not allowed to save the record as no Project Manager is assigned for the project .';
					return false;
				}//Project Manager
			}//Project OBJ		
			var f_is_task_present = get_project_task(i_project)
			//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' f_is_task_present  -->' + f_is_task_present);
			
			if (!_logValidation(f_is_task_present)) //
			{
				throw ' You are not allowed to save the record as no Project Task is assigned to the project .'
				return false;
			}
			
		}//Project
		var i_resource = nlapiGetFieldValue('allocationresource')
		//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Resource -->' + i_resource);
		
		var i_service_item = nlapiGetFieldValue('custevent1')
		//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Service Item -->' + i_service_item);
		
		var d_start_date = nlapiGetFieldValue('startdate')
		//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' RRR Start Date -->' + d_start_date);
		
		var d_end_date = nlapiGetFieldValue('enddate')
		//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' RRR End Date -->' + d_end_date);
		
		if (type == 'create') //
		{
			var a_resource_allocated = get_resource_allocation_details(i_resource, i_project, i_recordID, type)
			nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Resource Allocated  Array -->' + a_resource_allocated);
			
			var a_split_array = new Array();
			
			if (_logValidation(a_resource_allocated)) //
			{
				for (var b = 0; b < a_resource_allocated.length; b++) //
				{
					a_split_array = a_resource_allocated[b].split('&&&');
					
					var d_start_date_1 = a_split_array[0];
					
					var d_end_date_1 = a_split_array[1];
					
					d_start_date_1 = nlapiStringToDate(d_start_date_1);
					d_end_date_1 = nlapiStringToDate(d_end_date_1);
					d_start_date = nlapiStringToDate(d_start_date);
					d_end_date = nlapiStringToDate(d_end_date);
					
					//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Start Date -->' + d_start_date_1);
					//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' End Date -->' + d_end_date_1);
					
					if ((d_start_date_1 <= d_end_date) && (d_end_date_1 >= d_start_date)) //
					{
						if (d_end_date_1 > d_start_date || d_start_date_1 < d_end_date || d_end_date_1 == d_end_date || d_start_date_1 == d_start_date) //
						{
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Initially Allocated .........');
							throw ' This resource is already booked for these dates .\n Please book for other dates .'
							return false;
						}
					}
				}//Loop
			}//Resource Allocated
			var a_resource_allocated_book_array = get_resource_allocation_booking_details(i_resource, i_project, i_service_item,d_start_date,d_end_date)
			//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Resource Allocated Booking Array -->' + a_resource_allocated_book_array);
			
			var a_split_array_2 = new Array();
			
			if (_logValidation(a_resource_allocated_book_array)) //
			{
				//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' a_resource_allocated_book_array.indexOf(i_service_item)-->' + a_resource_allocated_book_array.indexOf(i_service_item));
				
				if (a_resource_allocated_book_array.indexOf(i_service_item) == -1) //
				{
					//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Different Serivice Item.................. ');
					
					throw 'The Resource is already allocated with other service item, kindly select the same service item to save the record .'
					return false;
				}
			}//Loop
		}//Create
				
		if (!_logValidation(i_service_item)) //
		{
			if (_logValidation(i_project)) //
			{
				var o_projectOBJ = nlapiLoadRecord('job', i_project);
				
				if (_logValidation(o_projectOBJ)) //
				{
					var i_billing_type = o_projectOBJ.getFieldValue('jobbillingtype')
					
					if (i_billing_type == 'TM') //
					{
						if (!_logValidation(i_service_item)) //
						{
							throw ' Please enter service item .';
							return false;
						}
						
					}//Time & Material
				}
			}
		}
		
		if (type == 'edit') //
		{
			var o_resource_oldOBJ = nlapiGetOldRecord();
			
			if (_logValidation(o_resource_oldOBJ)) //
			{
				var d_start_date_old = o_resource_oldOBJ.getFieldValue('startdate');
				
				var d_end_date_old = o_resource_oldOBJ.getFieldValue('enddate');
				
				var d_billing_start_date_old = o_resource_oldOBJ.getFieldValue('custeventbstartdate');
				
				var d_billing_end_date_old = o_resource_oldOBJ.getFieldValue('custeventbenddate');
				
				var d_start_date_new = nlapiGetFieldValue('startdate');
				
				var d_end_date_new = nlapiGetFieldValue('enddate');
				
				var d_billing_start_date_new = nlapiGetFieldValue('custeventbstartdate');
				
				var d_billing_end_date_new = nlapiGetFieldValue('custeventbenddate');
				
				
				d_start_date_old = nlapiStringToDate(d_start_date_old);
				d_end_date_old = nlapiStringToDate(d_end_date_old);
				d_billing_start_date_old = nlapiStringToDate(d_billing_start_date_old);
				d_billing_end_date_old = nlapiStringToDate(d_billing_end_date_old);
				
				
				d_start_date_new = nlapiStringToDate(d_start_date_new);
				d_end_date_new = nlapiStringToDate(d_end_date_new);
				d_billing_start_date_new = nlapiStringToDate(d_billing_start_date_new);
				d_billing_end_date_new = nlapiStringToDate(d_billing_end_date_new);
				
				
				//===================== START DATE =============================
				
				
				var offsetIST = 5.5;
				
				//To convert to UTC datetime by subtracting the current Timezone offset
				var utcdate = new Date(d_start_date_old.getTime() + (d_start_date_old.getTimezoneOffset() * 60000));
				
				//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
				var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
				
				var start_day_old = istdate.getDate();
				
				var start_month_old = istdate.getMonth() + 1;
				
				var start_year_old = istdate.getFullYear();
				
				var offsetIST = 5.5;
				
				//To convert to UTC datetime by subtracting the current Timezone offset
				var utcdate = new Date(d_start_date_new.getTime() + (d_start_date_new.getTimezoneOffset() * 60000));
				
				//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
				var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
				
				var start_day_new = istdate.getDate();
				
				var start_month_new = istdate.getMonth() + 1;
				
				var start_year_new = istdate.getFullYear();
				
				
				if (_logValidation(start_day_new) && _logValidation(start_month_new) && _logValidation(start_year_new)) //
				{
					if (_logValidation(start_day_old) && _logValidation(start_month_old) && _logValidation(start_year_old)) //
					{
						if ((start_day_new != start_day_old) || (start_month_new != start_month_old) || (start_year_new != start_year_old)) //
						{
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' d_end_date .........' + d_end_date);
							
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' d_start_date .........' + d_start_date);
							
							var a_resource_allocated = get_resource_allocation_details(i_resource, i_project, i_recordID, type)
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' a_resource_allocated .........' + a_resource_allocated);
							
							var a_split_array = new Array();
							
							if (_logValidation(a_resource_allocated)) //
							{
								for (var b = 0; b < a_resource_allocated.length; b++) //
								{
									a_split_array = a_resource_allocated[b].split('&&&');
									
									var d_start_date_1 = a_split_array[0];
									
									var d_end_date_1 = a_split_array[1];
									d_start_date_1 = nlapiStringToDate(d_start_date_1);
									d_end_date_1 = nlapiStringToDate(d_end_date_1);
									d_start_date = nlapiStringToDate(d_start_date);
									d_end_date = nlapiStringToDate(d_end_date);
									if (d_start_date_1 <= d_end_date) //
									{
										if (d_start_date_1 < d_start_date || d_end_date_1 == d_end_date || d_start_date_1 == d_start_date) {
											//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' First Allocated .........');
											throw ' This resource is already booked for these dates .\n Please book for other dates .'
											return false;
										}
									}
								}//Loop
							}//Resource Allocated
						}
					}
				}
				
				//===================== END DATE =============================
				
				var offsetIST = 5.5;
				
				//To convert to UTC datetime by subtracting the current Timezone offset
				var utcdate = new Date(d_end_date_old.getTime() + (d_end_date_old.getTimezoneOffset() * 60000));
				
				//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
				var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
				
				var end_day_old = istdate.getDate();
				
				var end_month_old = istdate.getMonth() + 1;
				
				var end_year_old = istdate.getFullYear();
				
				var offsetIST = 5.5;
				
				//To convert to UTC datetime by subtracting the current Timezone offset
				var utcdate = new Date(d_end_date_new.getTime() + (d_end_date_new.getTimezoneOffset() * 60000));
				
				//Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
				var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
				
				var end_day_new = istdate.getDate();
				
				var end_month_new = istdate.getMonth() + 1;
				
				var end_year_new = istdate.getFullYear();
				
				
				if (_logValidation(end_day_new) && _logValidation(end_month_new) && _logValidation(end_year_new)) //
				{
					if (_logValidation(end_day_old) && _logValidation(end_month_old) && _logValidation(end_year_old)) //
					{
						if ((end_day_new != end_day_old) || (end_month_new != end_month_old) || (end_year_new != end_year_old)) //
						{
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' d_end_date .........' + d_end_date);
							
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' d_start_date .........' + d_start_date);
							
							var a_resource_allocated = get_resource_allocation_details(i_resource, i_project, i_recordID, type)
							//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' a_resource_allocated .........' + a_resource_allocated);
							
							var a_split_array = new Array();
							
							if (_logValidation(a_resource_allocated)) //
							{
								for (var b = 0; b < a_resource_allocated.length; b++) //
								{
									a_split_array = a_resource_allocated[b].split('&&&');
									
									var d_start_date_1 = a_split_array[0];
									
									var d_end_date_1 = a_split_array[1];
									
									
									d_start_date_1 = nlapiStringToDate(d_start_date_1);
									d_end_date_1 = nlapiStringToDate(d_end_date_1);
									d_start_date = nlapiStringToDate(d_start_date);
									d_end_date = nlapiStringToDate(d_end_date);
									
									if (d_start_date_1 <= d_end_date) //
									{
										if (d_end_date_1 > d_start_date || d_end_date_1 == d_end_date || d_start_date_1 == d_start_date) //
										{
											//nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' Second Allocated .........');
											throw ' This resource is already booked for these dates .\n Please book for other dates .'
											return false;
										}
									}
								}//Loop
							}//Resource Allocated
						}
					}
				}
				
			}//OLD Object
		}// EDIT
		
		
		// Section added by Vikrant for validation of time sheets entered for the given time span
		
		if (type == 'edit') //
		{
			//get start and end date from the form
			var d_Start_Date_TS = nlapiGetFieldValue('startdate');
			var d_End_Date_TS = nlapiGetFieldValue('enddate');
            nlapiLogExecution('DEBUG', 'Time sheet submit val', 'd_Start_Date_TS : ' + d_Start_Date_TS + ', d_End_Date_TS : ' + d_End_Date_TS);
			
			// get employee id from the form
			var s_emp_id_TS = nlapiGetFieldValue('allocationresource');
			nlapiLogExecution('DEBUG', 'Time sheet submit val', 's_emp_id_TS : ' + s_emp_id_TS);
			
			var s_project_ID_TS = nlapiGetFieldValue('project');
			nlapiLogExecution('DEBUG', 'Time sheet submit val', 's_project_ID_TS : ' + s_project_ID_TS);
			
			// put the dates and employee ID into saved search as filter and get the expected result
			var a_Filters_TS = new Array();
            a_Filters_TS[a_Filters_TS.length] = new nlobjSearchFilter('date', null, 'onorafter', d_Start_Date_TS);
            //a_Filters_TS[a_Filters_TS.length] = new nlobjSearchFilter('date', null, 'onorbefore', d_End_Date_TS)
			a_Filters_TS[a_Filters_TS.length] = new nlobjSearchFilter('customer', null, 'anyof', s_project_ID_TS)
			a_Filters_TS[a_Filters_TS.length] = new nlobjSearchFilter('employee', null, 'anyof', s_emp_id_TS);
			
			//var s_Search_Result_TS = nlapiSearchRecord('timeentry', 'customsearch_val_timesheet_on_resource', a_Filters_TS, null);//Commented by praveena on 12-03-2020
			var s_Search_Result_TS = nlapiSearchRecord('timebill', 'customsearch_val_timebill_on_resource', a_Filters_TS, null);//Added by praveena on 12-03-2020
			
			if(_is_Valid(s_Search_Result_TS)) //
			{
				if (s_Search_Result_TS.length > 0) //
				{
					var all_Columns_TS = s_Search_Result_TS[0].getAllColumns();
					
					var d_Last_Timesheet_Date = s_Search_Result_TS[0].getValue(all_Columns_TS[0]);
					d_Last_Timesheet_Date = nlapiStringToDate(d_Last_Timesheet_Date);
					nlapiLogExecution('DEBUG', 'Time sheet submit val', 'd_Last_Timesheet_Date : ' + d_Last_Timesheet_Date);
					
					var d_End_Date_TS = nlapiGetFieldValue('enddate');
					d_End_Date_TS = nlapiStringToDate(d_End_Date_TS);
					nlapiLogExecution('DEBUG', 'Time sheet submit val', 'd_End_Date_TS : ' + d_End_Date_TS);
					
					if (d_Last_Timesheet_Date > d_End_Date_TS) // if last date of time sheet submitted is greater than end date selected in resource allocation
					{
						d_Last_Timesheet_Date = nlapiDateToString(d_Last_Timesheet_Date);
						nlapiLogExecution('DEBUG', 'Time sheet submit val', 's_Search_Result_TS.length : ' + s_Search_Result_TS.length);
                        throw 'Timesheet(s) for this employee has been already "Entered" till "' + d_Last_Timesheet_Date + '", \nTherefore you cannot modify this Resource Allocation.';
						return false;
					}
				}
			}
		}
		
		// Section end added by Vikrant for validation of time sheets entered
		

		if (type == 'create' || type == 'edit') //
		{
			//try //
			{
				// Section for validating over allocation of Resource
				// Added by Vikrant
				
				nlapiLogExecution('DEBUG', 'over_Allocation', '*** Into Section ***');
				
				// Get allocation start and end date, percentage of allocation, project id from the form.
				
				var d_Start_Date = nlapiGetFieldValue('startdate');
				var d_End_Date = nlapiGetFieldValue('enddate');
				var s_allocation_percentage = nlapiGetFieldValue('percentoftime');
				var s_Project_ID = nlapiGetFieldValue('project');
				var i_per_of_time = nlapiGetFieldValue('percentoftime'); 
				var s_allocationunit = nlapiGetFieldValue('allocationunit');
				var i_allocated_Hours = nlapiGetFieldValue('numberhours');
				var i_allocation_Amount = nlapiGetFieldValue('allocationamount');
				var s_resource_id = nlapiGetFieldValue('allocationresource');
				
				//nlapiLogExecution('DEBUG', 'over_Allocation', 'i_allocation_Amount : ' + i_allocation_Amount);
				
				if (_is_Valid(s_allocationunit)) //
				{
					if (s_allocationunit == 'H') //
					{
						var i_days_diff = calcBusinessDays(nlapiStringToDate(d_Start_Date), nlapiStringToDate(d_End_Date));
						//nlapiLogExecution('DEBUG', 'over_Allocation', 'i_days_diff : ' + i_days_diff);
						//nlapiLogExecution('DEBUG', 'over_Allocation', 'i_allocated_Hours : ' + i_allocated_Hours);
						
                        i_per_of_time = parseFloat((i_allocation_Amount / (i_days_diff * 8)) * 100);
						//nlapiLogExecution('DEBUG', 'over_Allocation', '1 i_per_of_time : ' + i_per_of_time);
					}
					else //
					{
						i_per_of_time = i_allocation_Amount;
					}
				}
                
                if (i_per_of_time > 100) //
                {
                    throw 'Resource is over allocated, you can only allocate "' + parseFloat(i_days_diff * 8) + '" hours as per date range selected.'
                }
				
				//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_Start_Date : ' + d_Start_Date + ', d_End_Date : ' + d_End_Date);
				//nlapiLogExecution('DEBUG', 'over_Allocation', 's_allocation_percentage : ' + s_allocation_percentage + ', s_Project_ID : ' + s_Project_ID);
                //nlapiLogExecution('DEBUG', 'over_Allocation', 'i_per_of_time : ' + i_per_of_time);
				//nlapiLogExecution('DEBUG', 'over_Allocation', 's_allocationunit : ' + s_allocationunit + ', i_allocated_Hours : ' + i_allocated_Hours);
				
				// Search for resource allocation records whose end date is greater than current Start date.
				var a_Filter = new Array();
				var a_Columns = new Array();
				
				a_Filter[a_Filter.length] = nlobjSearchFilter('enddate', null, 'onorafter', d_Start_Date);
				a_Filter[a_Filter.length] = nlobjSearchFilter('resource', null, 'anyof', s_resource_id);
				
				//nlapiLogExecution('DEBUG', 'over_Allocation', 'a_Filter : ' + a_Filter.length);
				//nlapiLogExecution('DEBUG', 'over_Allocation', 's_resource_id : ' + s_resource_id);
				
				var s_Search_Result = nlapiSearchRecord('resourceallocation', 'customsearch_res_over_allocation_1', a_Filter, null);
				
				if (_is_Valid(s_Search_Result)) //
				{
					//nlapiLogExecution('DEBUG', 'over_Allocation', 's_Search_Result : ' + s_Search_Result.length);
					// Extract their start date, end date, allocated by, percentage of time or hours and put all info into array.
					var all_Columns = s_Search_Result[0].getAllColumns();
					var d_current_Date = nlapiStringToDate(d_Start_Date);
					d_current_Date = nlapiDateToString(d_current_Date);
					
					//var d_current_Date_CHK = nlapiDateToString(d_Start_Date);
					
					do //
					{
						var i_total_percentage = 0;
						//nlapiLogExecution('DEBUG', 'over_Allocation', '*** i_total_percentage : ' + i_total_percentage);
						
						for (var counter_I = 0; counter_I < s_Search_Result.length; counter_I++) //
						{
							// retrieve end date of allocation
							
							//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_current_Date : ' + d_current_Date);
							//nlapiLogExecution('DEBUG', 'over_Allocation', 'counter_I : ' + counter_I);
							
							var d_Start_Date_Of_Old_alloc = s_Search_Result[counter_I].getValue(all_Columns[3]);
							d_Start_Date_Of_Old_alloc = nlapiStringToDate(d_Start_Date_Of_Old_alloc);
							d_Start_Date_Of_Old_alloc = nlapiDateToString(d_Start_Date_Of_Old_alloc);
							//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_Start_Date_Of_Old_alloc : ' + d_Start_Date_Of_Old_alloc);
							
							var d_End_Date_Of_Old_alloc = s_Search_Result[counter_I].getValue(all_Columns[4]);
							d_End_Date_Of_Old_alloc = nlapiStringToDate(d_End_Date_Of_Old_alloc);
							d_End_Date_Of_Old_alloc = nlapiDateToString(d_End_Date_Of_Old_alloc);
							//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_End_Date_Of_Old_alloc : ' + d_End_Date_Of_Old_alloc);
							
							var s_percentage = s_Search_Result[counter_I].getValue(all_Columns[6]);
							if (!_is_Valid(s_percentage)) //
							{
								s_percentage = 0;
							}
							s_percentage = s_percentage.replace('%', '');
							//nlapiLogExecution('DEBUG', 'over_Allocation', 's_percentage : ' + s_percentage);
							
							var s_project_id_1 = s_Search_Result[counter_I].getValue(all_Columns[2]);
							//nlapiLogExecution('DEBUG', 'over_Allocation', 's_project_id_1 : ' + s_project_id_1);
							
							var c_date = nlapiStringToDate(d_current_Date);
							var s_date = nlapiStringToDate(d_Start_Date_Of_Old_alloc);
							var e_date = nlapiStringToDate(d_End_Date_Of_Old_alloc);
							
                            if (type == 'edit') //
							{
								var current_Rec_ID = nlapiGetRecordId();
								var search_Rec_Id = s_Search_Result[counter_I].getValue(all_Columns[0]);
								
								//nlapiLogExecution('DEBUG', 'over allocation', 'current_Rec_ID : ' + current_Rec_ID);
								//nlapiLogExecution('DEBUG', 'over allocation', 'search_Rec_Id : ' + search_Rec_Id);
								
								if (current_Rec_ID == search_Rec_Id) // if record is getting edited then 
								{
									continue;
									//s_percentage = parseFloat(0) - parseFloat(s_percentage);
									//nlapiLogExecution('DEBUG', 'over_Allocation', 'Already Present : s_percentage : ' + s_percentage);
								}
							}
							else //
							{
								if (s_project_id_1 == s_Project_ID) //
								{
									throw 'Resource is already allocated to this project for given time frame !!! Kindly check previous allocations to proceed.';
									return false;
								}
							}
							
							//if (d_current_Date >= d_Start_Date_Of_Old_alloc && d_current_Date <= d_End_Date_Of_Old_alloc) //
							if (c_date >= s_date && c_date <= e_date) //
							{
								//nlapiLogExecution('DEBUG', 'over_Allocation', '*** Into Addition ***');
								i_total_percentage = parseFloat(i_total_percentage) + parseFloat(s_percentage);
								//nlapiLogExecution('DEBUG', 'over_Allocation', 'i_total_percentage : ' + i_total_percentage);
								
							}
							else
							{
								//nlapiLogExecution('DEBUG', 'over_Allocation', '*** Not into Addition ***');
								//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_Start_Date_Of_Old_alloc : '+ d_Start_Date_Of_Old_alloc);
								//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_current_Date : '+ d_current_Date);
								//nlapiLogExecution('DEBUG', 'over_Allocation', 'd_End_Date_Of_Old_alloc : '+ d_End_Date_Of_Old_alloc);
							}
						}
						
						//nlapiLogExecution('DEBUG', 'over_Allocation', 'Before i_total_percentage : ' + i_total_percentage);
						i_total_percentage = parseFloat(i_total_percentage) + parseFloat(i_per_of_time);
						//nlapiLogExecution('DEBUG', 'over_Allocation', 'final i_per_of_time : ' + i_per_of_time);
						//nlapiLogExecution('DEBUG', 'over_Allocation', 'final i_total_percentage : ' + i_total_percentage);
						
						if (i_total_percentage > 100) //
						{
							//var d_current_Date = new Date();
							//d_current_Date = nlapiDateToString(d_current_Date);
							throw 'Resource is already 100% allocated for given time frame(On date : ' + d_current_Date + ') !!! Kindly check previous allocations to proceed.';
							return false;
						}
						else //
						{
							d_current_Date = nlapiStringToDate(d_current_Date);
							d_current_Date = nlapiAddDays(d_current_Date, 1);
							d_current_Date = nlapiDateToString(d_current_Date);
							
							var e_date_1 = nlapiStringToDate(d_End_Date);
							var d_current_Date_1 = nlapiStringToDate(d_current_Date);
							
							nlapiLogExecution('DEBUG', 'over_Allocation', 'e_date_1 : ' + e_date_1);
							nlapiLogExecution('DEBUG', 'over_Allocation', 'd_current_Date_1 : ' + d_current_Date_1);
							
							if (d_current_Date_1 > e_date_1) // check if current date of checking is greater than allocation end date
							{
								break;
							}
						}
                        //nlapiLogExecution('DEBUG', 'over_Allocation', 'd_current_Date : ' + d_current_Date + ', d_end_date : ' + d_end_date);
						
						//var c_date_1 = nlapiStringToDate(d_current_Date);
						
					}
					while (d_current_Date != d_end_date) //
					
				}
				else //
				{
					nlapiLogExecution('ERROR', 'over_Allocation', '*** Resource is not allocated for this time frame !!! ***');
				}
				
				// run a loop for all records check the percentage of time allocated to resource and add current percentage to it.
				// if it goes pass to 100 % then restrict user from saving this record.
				// if allocation is OK then allow user to save record.
			
				// End of Section for validating over allocation of Resource
			
			} 
			//catch (e) //
			{
				//nlapiLogExecution('ERROR', 'over_Allocation', 'Exception : \n' + e + ', \nMessage : ' + e.message);
			}
		}
		else //
		{
		
		}
		
		if (type == 'edit') //
		{
			var i_project = nlapiGetFieldValue('project')
			
			if (_logValidation(i_project)) //
			{
				var o_projectOBJ = nlapiLoadRecord('job', i_project);
				
				if (_logValidation(o_projectOBJ)) //
				{
					var d_project_start_date = o_projectOBJ.getFieldValue('startdate')
					
					var d_project_end_date = o_projectOBJ.getFieldValue('enddate')
					
					var d_start_date = nlapiGetFieldValue('startdate');
					
					var d_end_date = nlapiGetFieldValue('enddate');
					
					d_start_date = nlapiStringToDate(d_start_date);
					d_end_date = nlapiStringToDate(d_end_date);
					d_project_end_date = nlapiStringToDate(d_project_end_date);
					d_project_start_date = nlapiStringToDate(d_project_start_date);
					
					if (_logValidation(d_start_date) && _logValidation(d_project_start_date)) //
					{
						if (d_start_date < d_project_start_date) //
						{
							throw ' Start Date of the project should be greater than / equal to Project Start Date';
							return false;
						}
					}
					if (_logValidation(d_start_date) && _logValidation(d_project_end_date)) //
					{
						if (d_start_date > d_project_end_date && d_start_date > d_project_start_date) //
						{
							throw ' Start Date of the project should be less than / equal to Project End Date';
							return false;
						}
					}
					if (_logValidation(d_end_date) && _logValidation(d_project_end_date)) //
					{
						if (d_end_date > d_project_end_date) //
						{
							throw ' End Date of the project should be less than /equal to Project End Date .';
							return false;
						}
					}
					if (_logValidation(d_end_date) && _logValidation(d_project_start_date) && _logValidation(d_project_end_date)) //
					{
						if (d_end_date < d_project_start_date && d_end_date < d_project_end_date) //
						{
							throw ' End Date of the project should be greater than /equal to Project Start Date .';
							return false;
						}
					}
					
					if (_logValidation(d_end_date) && _logValidation(d_start_date)) //
					{
						if (d_end_date < d_start_date) //
						{
							throw ' Start Date should be less than End Date . ';
							return false;
						}
					}
				}
			}
			var d_start_date = nlapiGetFieldValue('startdate')
			
			var d_end_date = nlapiGetFieldValue('enddate')
			
			var d_billing_start_date = nlapiGetFieldValue('custeventbstartdate');
			
			var d_billing_end_date = nlapiGetFieldValue('custeventbenddate');
			
			d_start_date = nlapiStringToDate(d_start_date);
			d_end_date = nlapiStringToDate(d_end_date);
			d_billing_start_date = nlapiStringToDate(d_billing_start_date);
			d_billing_end_date = nlapiStringToDate(d_billing_end_date);
			
			if (_logValidation(d_start_date) && _logValidation(d_billing_start_date)) //
			{
				if (d_billing_start_date < d_start_date) //
				{
					throw ' Billing Start Date of the project should be greater than / equal to Start Date';
					return false;
				}
			}
			if (_logValidation(d_start_date) && _logValidation(d_billing_end_date)) //
			{
				if (d_billing_start_date > d_end_date && d_billing_start_date > d_start_date) //
				{
					throw ' Billing Start Date of the project should be less than / equal to End Date';
					return false;
				}
			}
			if (_logValidation(d_end_date) && _logValidation(d_billing_end_date) && _logValidation(d_billing_start_date)) //
			{
				if (d_billing_end_date > d_start_date && d_billing_end_date > d_end_date) //
				{
					throw ' Billing End Date of the project should be less than /equal to End Date .';
					return false;
				}
			}
			if (_logValidation(d_end_date) && _logValidation(d_billing_start_date) && _logValidation(d_billing_end_date)) //
			{
				if (d_billing_end_date < d_start_date && d_billing_end_date < d_end_date) //
				{
					throw ' Billing End Date of the project should be greater than /equal to Start Date .';
					return false;
				}
			}
			if (_logValidation(d_billing_end_date) && _logValidation(d_billing_start_date)) //
			{
				if (d_billing_end_date < d_billing_start_date) //
				{
					throw ' Billing Start Date should be less than Billing End Date . ';
					return false;
				}
			}
		}//Billing Start Date
	}//Type
	
	return true;
}

// END BEFORE SUBMIT ==================================================


// BEGIN AFTER SUBMIT =============================================

function afterSubmit_allocate_assignee(type) //
{

	try //
	{
		var i_recordID = nlapiGetRecordId();
		var s_record_type = nlapiGetRecordType();
		//nlapiLogExecution('DEBUG', 's_record_type', s_record_type);
		
		if (_logValidation(i_recordID) && _logValidation(s_record_type)) //
		{
			var o_recordOBJ = nlapiLoadRecord(s_record_type, i_recordID);
			
			if (_logValidation(o_recordOBJ)) //
			{
				var i_project = o_recordOBJ.getFieldValue('project')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Project -->'+i_project);	
				nlapiLogExecution('ERROR', 'afterSubmit_allocate_assignee', 'Project -->'+i_project);
				var i_resource = o_recordOBJ.getFieldValue('allocationresource')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Resource -->'+i_resource);	
				
				var i_service_item = o_recordOBJ.getFieldValue('custevent1')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Service Item -->'+i_service_item);	
				
				var i_service_item_ID = get_item_ID(i_service_item)
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Service Item ID -->'+i_service_item_ID);	
				
				var i_allocated_time = o_recordOBJ.getFieldValue('allocationamount')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Allocated Time -->'+i_allocated_time);	
				
				var i_unit_cost = o_recordOBJ.getFieldValue('custevent2')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'Unit Cost -->'+i_unit_cost);	
				
				var i_bill_rate = o_recordOBJ.getFieldValue('custevent3')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Bill Rate -->'+i_bill_rate);	
				
				//Added by Vinod
				var i_ot_billable = o_recordOBJ.getFieldValue('custevent_otbillable')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'OT Billable -->' + i_ot_billable);
				
				var i_ot_payable = o_recordOBJ.getFieldValue('custevent_otpayable')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' OT Payable -->' + i_ot_payable);
				
				var i_service_otitem = o_recordOBJ.getFieldValue('custevent_otserviceitem')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Service Item -->' + i_service_otitem);
				
				var i_service_otrate = o_recordOBJ.getFieldValue('custevent_otrate')
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' OT Rate -->' + i_service_otrate);
				
				// Added by Vikrant
				var i_Holiday_Item_ID = o_recordOBJ.getFieldValue('custevent_holiday_service_item');
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'i_Holiday_Item_ID -->'+i_Holiday_Item_ID);
				
				var i_Leave_Item_ID = o_recordOBJ.getFieldValue('custevent_leave_service_item');
				//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', 'i_Leave_Item_ID -->'+i_Leave_Item_ID);
				
				// End of Added by Vikrant
				
				
				//Added by Vinod
				
				if (_logValidation(i_project)) //
				{
					
					//Added by Praveena to optimize code (considered the particula project tasks)
					var activityNames = [ "Project Activites", "Project activites",
                            "Project activities", "project activities",
                            "Bench Task","Bench" ,"Project Activities-Offsite",
                            "Project Activities-Onsite", "Operations",
                            "Project Activities", "Project activity",
                            "project activity", "Project Activity","Internal Project","Bench Activities","OT","Holiday","Leave","Floating Holiday"];
						var filter = [];
						
						var addOr = false;
						activityNames.forEach(function(name) {

							if (addOr) {
								filter.push('or');
							}

							filter.push([ 'title', 'is', name ]);

							addOr = true;
						});
					
					
						var a_search_results = nlapiSearchRecord('projecttask', null, [ filter,
						'and', 
						[ 'company', 'anyof', i_project ],
						'and', 
						[ 'ismilestone', 'is', 'F' ] ],
						[
						new nlobjSearchColumn("internalid"), 
						new nlobjSearchColumn("title")
						]);
					
				/*	
					var filter = new Array();
					filter[0] = new nlobjSearchFilter('company', null, 'is', i_project);

					// added by Nitish
					filter[1] = new nlobjSearchFilter('ismilestone', null, 'is', 'F');
				
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('internalid');
					columns[1] = new nlobjSearchColumn('title');//Added by Vinod
					var a_search_results = nlapiSearchRecord('projectTask', null, filter, columns);
					*/
					
					
					if (_logValidation(a_search_results)) //
					{
						nlapiLogExecution('ERROR', 'afterSubmit_allocate_assignee', ' Search Results Length -->' + a_search_results.length);
						
						for (var yt = 0; yt < a_search_results.length; yt++) //
						{
							var is_assignee_present = false;
							//Added by Vinod
							var AllocateTask = 'F';
							
							if (a_search_results[yt].getValue('title') == 'OT' && i_ot_billable == 'F' && i_ot_payable == 'F') //
							{
								AllocateTask = 'F';
							}
							else //
							{
								AllocateTask = 'T';
							}							
                            
                             if (a_search_results[yt].getValue('title') == 'Floating Holiday') // if Task is floating holiday then do not allocate employee to it
                             {
                             AllocateTask = 'F';
                             }
                             							
							//Added by Vinod
							
							//nlapiLogExecution('DEBUG', 'Allocate Task', AllocateTask);
							if (AllocateTask == 'T') //Added by Vinod
							{
								var i_internalID = a_search_results[yt].getValue('internalid')
								//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Internal ID *************-->' + i_internalID);
								
								if (_logValidation(i_internalID)) //
								{
									var o_project_taskOBJ = nlapiLoadRecord('projectTask', i_internalID)
									
									if (_logValidation(o_project_taskOBJ)) //
									{
										var i_estimated_work = o_project_taskOBJ.getFieldValue('estimatedwork')
										
										//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Estimated Work-->' + i_estimated_work);
										
										var i_line_count_assignee = o_project_taskOBJ.getLineItemCount('assignee')
										
										if (_logValidation(i_line_count_assignee)) //
										{
											for (var ht = 1; ht <= i_line_count_assignee; ht++) //
											{
												var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)
												
												if (i_resource_pr == i_resource) //
												{
													is_assignee_present = true;
													break;
												}
											}//Loop							
										}//Line Count Assignee
										if (_logValidation(i_estimated_work) && i_estimated_work != 0) //
										{
											if (is_assignee_present == false) //
											{
												nlapiLogExecution('audit','assignee created for resource :: '+i_resource);
												o_project_taskOBJ.selectNewLineItem('assignee')
												o_project_taskOBJ.setCurrentLineItemValue('assignee', 'resource', i_resource);
												o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitcost', i_unit_cost);
												//Added by Vinod
												if (o_project_taskOBJ.getFieldValue('title') == 'OT') //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_service_otitem);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', i_service_otrate);
												}
												else //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_service_item_ID);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', i_bill_rate);
												}
												
												if(o_project_taskOBJ.getFieldValue('title') == 'Holiday') //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_Holiday_Item_ID);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', 0);
												}
												
												if(o_project_taskOBJ.getFieldValue('title') == 'Leave') //
												{
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'serviceitem', i_Leave_Item_ID);
													o_project_taskOBJ.setCurrentLineItemValue('assignee', 'unitprice', 0);
												}
												
												//Added by Vinod
												o_project_taskOBJ.setCurrentLineItemValue('assignee', 'estimatedwork', i_allocated_time);
												o_project_taskOBJ.commitLineItem('assignee');
											}//False
											
											if (is_assignee_present == true) //
											{
												if (_logValidation(i_line_count_assignee)) //
												{
													for (var ht = 1; ht <= i_line_count_assignee; ht++) //
													{
														var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)
														
														if (i_resource_pr == i_resource) //
														{
															o_project_taskOBJ.setLineItemValue('assignee', 'estimatedwork', ht, i_allocated_time)
															o_project_taskOBJ.setLineItemValue('assignee', 'unitcost', ht, i_unit_cost);
															//Added by Vinod
															if (o_project_taskOBJ.getFieldValue('title') == 'OT') //
															{
																o_project_taskOBJ.setLineItemValue('assignee', 'unitprice', ht, i_service_otrate)
															}
															else //
															{
																o_project_taskOBJ.setLineItemValue('assignee', 'unitprice', ht, i_bill_rate)
															}
															//Added by Vinod
														}
													}//Loop							
												}//Line Count Assignee
											}
										}
										else {
												nlapiSendEmail('442', 'nitish.mishra@brillio.com', 
														'task assignee script issue', 'Estimated work 0 for allocation  ' +
														nlapiGetRecordId() + "<br/>" + 'Is Milestone : ' + (o_project_taskOBJ.getFieldValue('ismilestone') == 'T'));
										}
										var i_submitID = nlapiSubmitRecord(o_project_taskOBJ, true, true);
										//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' ***************** Project Task Submit ID *************** -->' + i_submitID);
										
									}//Project TAsk OBJ
								}//Project Task ID
							}
							else //
							{
								//Added by Vinod
								//Remove assignee from OT Task
								var is_assignee_present = false;
								
								var i_internalID = a_search_results[yt].getValue('internalid')
								//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' Internal ID *************-->' + i_internalID);
								
								var o_project_taskOBJ = nlapiLoadRecord('projectTask', i_internalID)
								for (var ht = 1; ht <= i_line_count_assignee; ht++) //
								{
									var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)
									if (i_resource_pr == i_resource) //
									{
										is_assignee_present = true;
										break;
									}
								}//Loop							
								if (is_assignee_present == true) //
								{
									for (var ht = 1; ht <= i_line_count_assignee; ht++) //
									{
										var i_resource_pr = o_project_taskOBJ.getLineItemValue('assignee', 'resource', ht)
										
										if (i_resource_pr == i_resource) //
										{
											if (o_project_taskOBJ.getFieldValue('title') == 'OT') //
											{
												//Remove the assignee from OT task
												o_project_taskOBJ.removeLineItem('assignee', ht);
											}
										}
									}
									var i_submitID = nlapiSubmitRecord(o_project_taskOBJ, true, true);
									//nlapiLogExecution('DEBUG', 'afterSubmit_allocate_assignee', ' ***************** Project Task Submit ID *************** -->' + i_submitID);
								}
								//Added by Vinod
							}
						}//Results
					}//Search Results
				}//Employee
			}//Record OBJ		
		}//Record ID & Record Type		
	} 
	catch (exception) //
	{
		nlapiLogExecution('ERROR', 'ERROR', 'Exception -->' + exception);
		
		nlapiSendEmail('442', 'nitish.mishra@brillio.com', 
				'task assignee script error', exception);
	}
	return true;
	
}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


function get_project_task(i_project)
{
  var i_project_task;	
  var i_result;
	
  if(_logValidation(i_project))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('company',null,'is',i_project);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	
	 var a_search_results = nlapiSearchRecord('projectTask',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	  //nlapiLogExecution('DEBUG', 'get_project_task', ' Search Results Length -->' + a_search_results.length);
		  i_result = true ;	
	 }//Search Results
	 else
	 {
	 	i_result = false;
	 }
	  //nlapiLogExecution('DEBUG', 'get_project_task', ' Result -->' + i_result);
  }//Employee
  return i_result;
}





function get_resource_allocation_details(i_resource,i_project,i_recordID,type)
{  
	var i_cnt = 0;
	var i_resource_1;
	var i_hours;
	var d_start_date;	
	var d_end_date;
	var i_work_location;
	var a_resource_array =  new Array();		

   
	
	if(type == 'create')
	{
	  var filter = new Array();
      filter[0] = new nlobjSearchFilter('project', null, 'is', i_project);
	  filter[1] = new nlobjSearchFilter('resource', null, 'anyof', i_resource);//ADDED pRAVEENA
	  nlapiLogExecution('DEBUG', 'i_project', 'i_project-->' + i_project);
	  nlapiLogExecution('DEBUG', 'i_resource', 'i_resource-->' + i_resource);
	}
	if(type != 'create')
	{
		var filter = new Array();
	    filter[0] = new nlobjSearchFilter('project', null, 'is', i_project);
		filter[1] = new nlobjSearchFilter('internalid', null, 'noneof', i_recordID);
		filter[2] = new nlobjSearchFilter('resource', null, 'anyof', i_resource);	//ADDED pRAVEENA	
	}
			
	var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
	 nlapiLogExecution('DEBUG', 'i_search_results', 'i_search_results-->' + i_search_results);
	if (_logValidation(i_search_results)) 
	{
		//nlapiLogExecution('DEBUG', 'get_resource_allocation_details', ' Search Results Length  -->' + i_search_results.length);
		
		for (var c = 0; c < i_search_results.length; c++) 
		{
			var a_search_transaction_result = i_search_results[c];
			var columns = a_search_transaction_result.getAllColumns();
			var columnLen = columns.length;
			
			for (var hg = 0; hg < columnLen; hg++) 
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
				
			     if(label =='Resource')
				 {
				 	i_resource_1 = value;
				 }
				 if(label =='Hours')
				 {
				 	i_hours = value;
				 }
				 if(label =='Start Date')
				 {
				 	d_start_date = value;
				 }
				 if(label =='End Date')
				 {
				 	d_end_date = value;
				 }
				 if(label =='Work Loctaion')
				 {
				 	i_work_location = value;
				 }				
			}			
			if(i_resource == i_resource_1)
			{
				a_resource_array[i_cnt++] = d_start_date+'&&&'+d_end_date;				
			}						
		}
	}
	return a_resource_array;
}



function get_resource_allocation_booking_details(i_resource,i_project,i_service_item,startDate,endDate)
{  
	var i_cnt = 0;
	var i_resource_1;
	var i_hours;
	var d_start_date;	
	var d_end_date;
	var i_work_location;
	var i_service_item;
	var i_service_item_1;
	var internalID;
	var a_resource_array =  new Array();		
	
	var timesheetData = getTimesheets(i_resource, i_project, startDate, endDate)

    var filter = new Array();
    filter[0] = new nlobjSearchFilter('project', null, 'is', i_project);
	
	filter[1] = new nlobjSearchFilter('resource', null, 'anyof', i_resource);//Added by Praveena - 10-12-2020
	
	var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
	 
	if (_logValidation(i_search_results)) 
	{
		//nlapiLogExecution('DEBUG', 'get_resource_allocation_booking_details', ' Search Results Length  -->' + i_search_results.length);
		
		for (var c = 0; c < i_search_results.length; c++) 
		{
			var a_search_transaction_result = i_search_results[c];
			var columns = a_search_transaction_result.getAllColumns();
			var columnLen = columns.length;
			
			for (var hg = 0; hg < columnLen; hg++) 
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
				
				 if(label =='Internal ID')
				 {
				 	internalID = value;
				 }
			     if(label =='Resource')
				 {
				 	i_resource_1 = value;
				 }
				 if(label =='Hours')
				 {
				 	i_hours = value;
				 }
				 if(label =='Start Date')
				 {
				 	d_start_date = value;
				 }
				 if(label =='End Date')
				 {
				 	d_end_date = value;
				 }
				 if(label =='Work Loctaion')
				 {
				 	i_work_location = value;
				 }	
				  if(label =='Service Item')
				 {
				 	i_service_item_1 = value;
				 }			
			}	
			//nlapiLogExecution('DEBUG', 'get_resource_allocation_booking_details', 'Internal ID-->' + internalID);
		    //nlapiLogExecution('DEBUG', 'get_resource_allocation_booking_details', 'i_service_item_1-->' + i_service_item_1);
		    //nlapiLogExecution('DEBUG', 'get_resource_allocation_booking_details', 'i_service_item-->' + i_service_item);
		
			if(timesheetData.length > 0){
					
			if((i_resource == i_resource_1))
			{
				if (_logValidation(i_service_item_1)&&_logValidation(i_service_item)) 
				{
				  //  if((parseInt(i_service_item_1) == parseInt(i_service_item)))
					{
						a_resource_array[i_cnt++] = i_service_item_1;
					}
							
				}//Service Item
						
				}
			}			
			//nlapiLogExecution('DEBUG', 'get_resource_allocation_booking_details', 'a_resource_array ->' + a_resource_array);
							
		}
	}
	a_resource_array = removearrayduplicate(a_resource_array)
	return a_resource_array;
}


function removearrayduplicate(array)
{
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) 
	{
        for (var j = 0; j < array.length; j++) 
		{
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}


function get_employee_labour_cost(i_resource)
{
  var i_labour_cost = 0;
  if(_logValidation(i_resource))
  {
  	var o_employeeOBJ = nlapiLoadRecord('employee',i_resource);
	
	if(_logValidation(o_employeeOBJ))
	{
		i_labour_cost = o_employeeOBJ.getFieldValue('laborcost')
		
		 if(!_logValidation(i_labour_cost))
		 {
		 	i_labour_cost = 0
		 }
		
	}//Employee OBJ	
  }//Resource	
  return i_labour_cost;
}//Employee Labour Cost


function get_project_bill_rate(i_project,i_currency,i_service_item)
{
	var i_bill_rate = 0;
	if(_logValidation(i_project)&&_logValidation(i_currency)&&_logValidation(i_service_item))
	{
	var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_proj',null,'is',i_project);
	filter[1] = new nlobjSearchFilter('custrecord_currency1',null,'is',i_currency);
	filter[2] = new nlobjSearchFilter('custrecord_item1',null,'is',i_service_item);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_rate');
	
	 var a_search_results = nlapiSearchRecord('customrecord_customraterecord',null,filter,columns);
	 
	 if (_logValidation(a_search_results)) 
	 {	 	
	 	for (var t = 0; t < a_search_results.length; t++) 
		{
	 	  i_bill_rate = a_search_results[t].getValue('custrecord_rate');		  
		  
		 if(!_logValidation(i_bill_rate))
		 {
		 	i_bill_rate = 0
		 }
		
	 		
	  	}
	 }
	}//Project
	
	return i_bill_rate;
}

function get_project_currency(i_project)
{
  var i_currency;
  if(_logValidation(i_project))
  {
  	var o_projectOBJ = nlapiLoadRecord('job',i_project);
	
	if(_logValidation(o_projectOBJ))
	{
		i_currency = o_projectOBJ.getFieldValue('currency')
		
	}//Project OBJ	
  }//Project
  
  return i_currency;	
}//Project Currency


function get_item_ID(i_service_item)
{
	
	 //nlapiLogExecution('DEBUG', 'get_item_ID', 'i_service_item-->' + i_service_item);
		
	var i_item_ID ;
 if(_logValidation(i_service_item))
 {
 	var filter = new Array();
    filter[0] = new nlobjSearchFilter('itemid',null,'is',i_service_item);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('internalid');
	
	 var a_search_results = nlapiSearchRecord('item',null,filter,columns);
	 
	 if (_logValidation(a_search_results)) 
	 {	 
	 	  i_item_ID = a_search_results[0].getValue('internalid');		  
		  //nlapiLogExecution('DEBUG', 'get_item_ID', 'i_item_ID-->' + i_item_ID);
		
	 }
	
 }	
	return i_item_ID;
}

function _is_Valid(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

function calcBusinessDays(dDate1, dDate2) //
{ // input given as Date objects
    var iWeeks, iDateDiff, iAdjust = 0;
    if (dDate2 < dDate1) 
        return -1; // error code if dates transposed
        
		
    var iWeekday1 = dDate1.getDay(); // day of week
    var iWeekday2 = dDate2.getDay();
	
    iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
    iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
    
	if ((iWeekday1 > 5) && (iWeekday2 > 5)) 
        iAdjust = 1; // adjustment if both days on weekend
    
	iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1; // only count weekdays
    iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;
    
    // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
    iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)
    
    if (iWeekday1 <= iWeekday2) {
        iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
    }
    else {
        iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
    }
    
    iDateDiff -= iAdjust // take into account both days on weekend
    return (iDateDiff + 1); // add 1 because dates are inclusive
}

// END FUNCTION =====================================================

function getTimesheets(employee, project, startDate, endDate) {
	try {
	
		var d_start_date = nlapiGetFieldValue('startdate')
		nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' getTimesheets Start Date -->' + d_start_date);
		
		var d_end_date = nlapiGetFieldValue('enddate')
		nlapiLogExecution('DEBUG', 'beforeSubmit_resource_controls', ' getTimesheets End Date -->' + d_end_date);
		d_start_date = nlapiStringToDate(d_start_date);
		d_end_date = nlapiStringToDate(d_end_date); 
		//var timeEntrySearch = nlapiSearchRecord('timeentry', null, [
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('customer', null, 'anyof', project),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', d_start_date,
		                d_end_date) ], [
		        new nlobjSearchColumn('approvalstatus', 'timesheet', 'group'),
		        new nlobjSearchColumn('startdate', 'timesheet', 'group')
		                .setSort(),
		        new nlobjSearchColumn('enddate', 'timesheet', 'group') ]);

		var timesheetData = "";
		var dataRow = [];
		var JSON  = {};
		if (timeEntrySearch) {
			var rows = "";

			timeEntrySearch.forEach(function(timesheet) {
			
			JSON = {
				StartDate: timesheet.getValue('startdate', 'timesheet', 'group'),
				EndDate: timesheet.getValue('enddate', 'timesheet', 'group'),
				Status: timesheet.getText('approvalstatus', 'timesheet',
				        'group')
			};
				/*rows += "<tr>";
				rows += "<td>";
				rows += timesheet.getValue('startdate', 'timesheet', 'group');
				rows += "</td>";
				rows += "<td>";
				rows += timesheet.getValue('enddate', 'timesheet', 'group');
				rows += "</td>";
				rows += "<td>";
				rows += timesheet.getText('approvalstatus', 'timesheet',
				        'group');
				rows += "</td>";
				rows += "</tr>"; */
				dataRow.push(JSON);
			});

			/*if (rows) {
				timesheetData += "<table border=1>";
				timesheetData += "<tr style='background:#E3E1E0'>";
				timesheetData += "<td>";
				timesheetData += "Week Start Date";
				timesheetData += "</td>";
				timesheetData += "<td>";
				timesheetData += "Week End Date";
				timesheetData += "</td>";
				timesheetData += "<td>";
				timesheetData += "Status";
				timesheetData += "</td>";
				timesheetData += "</tr>";
				timesheetData += rows;
				timesheetData += "</table>";
			} */
		}

		return dataRow;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTimesheets', err);
		throw err;
	}
}