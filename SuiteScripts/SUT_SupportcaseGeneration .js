// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_SupportcaseGeneration.js
	Author:Sachin Khairnar
	Company:Aashnacloudtech Pvt Ltd.
	Date:23-March-2015
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function SupportcaseGenaration(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
	if ( request.getMethod() == 'GET' )
	{
 		var form = nlapiCreateForm('Create Case');
  		//SC:Add fields in the newly created form
  		//form.setScript('customscript_cli_tds_master');
  		var date=form.addField('create_date','date','Date')
  		var sysdate=new Date
		var date1=nlapiDateToString(sysdate)
		date.setDefaultValue(date1)

		var user=nlapiGetUser()
  		var employee=form.addField('employee','select', 'Employee','employee');
  		employee.setMandatory(true)
  		employee.setDefaultValue(user)

		var emailId=nlapiLookupField('employee',user,'email')
		var mobilephone=nlapiLookupField('employee',user,'mobilephone')
  		var email=form.addField('email','email', ' Email');
  		email.setMandatory(true)
		email.setDefaultValue(emailId)
  		var subject=form.addField('subject','text','Subject');
  		subject.setMandatory(true)



  		var section=form.addField('question','textarea', 'Question/Description');
  		section.setMandatory(true)

        var fileField = form.addField('file', 'file', 'Attach File');
        //fileField.setMandatory(true)

        var phone=form.addField('phone','phone', 'Phone no');
  		phone.setDefaultValue(mobilephone)

		form.addSubmitButton('Submit');

		response.writePage( form );
	}
  else if(request.getMethod() == 'POST')
   	{
		 var create_date = request.getParameter("create_date")
		 var employee = request.getParameter("employee")
		 var email = request.getParameter("email")
		 var subject = request.getParameter("subject")
		 var question = request.getParameter("question")
		 var phone = request.getParameter("phone")
		 var file = request.getFile("file")

		 var firstname=nlapiLookupField('employee',employee,'firstname')
		 nlapiLogExecution('DEBUG','user', 'create_date '+create_date)
		 nlapiLogExecution('DEBUG','user', 'employee '+employee)
		 nlapiLogExecution('DEBUG','user', 'email '+email)
		 nlapiLogExecution('DEBUG','user', 'subject'+subject)
		 nlapiLogExecution('DEBUG','user', 'question '+question)
		 nlapiLogExecution('DEBUG','user', 'file '+file)

         var caseString='Hi \n Case is raised by:- '+firstname +" Contact No:-"+phone +'\n'+'--------------------------------------------------------------\n'
         question=caseString + question
		 var records = new Object();
         records['employee'] = employee;
		 nlapiSendEmail(employee, 'aitsupport@aashnacloudtech.com', subject, question, null, null, records, file);
		 response.write('<html><head></head><body>Hi ,<br>' +'Thank you for contacting AIT Customer Support.<br>'
		 		+'Support team will contact you soon.<br>'
		 		+'Click <a href="/app/center/card.nl?sc=-29&whence=">here</a> to go to Home.'
		 		+'</body></html>');
   	}





}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
