/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Approve_Expense.js
	Author      : Shweta Chopde
	Date        : 23 May 2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
  var a_split_data_array = new Array();	
  var i_ref_no;
  
  try
  {
  	  var i_context = nlapiGetContext();
	  
	  var d_to_date = i_context.getSetting('SCRIPT','custscript_date_till_now_e');
	  var i_employee = i_context.getSetting('SCRIPT','custscript_employee_e');
	  var a_data_array = i_context.getSetting('SCRIPT','custscript_data_array_approve_e');
	  var i_approve = i_context.getSetting('SCRIPT','custscript_approve_e');
	  var i_reject = i_context.getSetting('SCRIPT','custscript_reject_e');
	  var i_current_user = i_context.getSetting('SCRIPT','custscript_current_user_e');
	       
	  i_current_user = parseInt(i_current_user);   
	  var i_current_user_name = get_employee_name((i_current_user))
	   
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' To Date -->' + d_to_date);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Employee -->' + i_employee);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Data Array  -->' + a_data_array);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Approve -->' + i_approve);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Reject -->' + i_reject);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Current User -->' + i_current_user);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' Current User Name -->' + i_current_user_name);
	 
	 if(_logValidation(a_data_array))
	 {
	 var a_TR_array_values = new Array()	
	 var i_data_TR = new Array()
	 i_data_TR =  a_data_array;
 
      for(var dt=0;dt<i_data_TR.length;dt++)
	  {
		 	a_TR_array_values = i_data_TR.split(',')
		    break;				
	  }	

	 nlapiLogExecution('DEBUG', 'schedulerFunction','TR Array Values-->' +a_TR_array_values);
	 nlapiLogExecution('DEBUG', 'schedulerFunction',' TR Array Values Length-->' + a_TR_array_values.length);
	
		for(var yt=0;yt<a_TR_array_values.length;yt++)
		{
			a_split_data_array = a_TR_array_values[yt].split('#####')			
			
			i_employee = a_split_data_array[0];
            var i_internal_id = a_split_data_array[1];
            i_customer = a_split_data_array[2];
            i_amount = a_split_data_array[3];
			var i_ref_no = a_split_data_array[4];
			
			 if(_logValidation(i_approve))
			 {
			 	if(i_approve.indexOf('T')>-1)
				{				
                    approve_expense_report(i_internal_id,i_ref_no,i_current_user_name);
					nlapiLogExecution('DEBUG', 'schedulerFunction',' ************ Approve ******** -->' + i_internal_id);
	
				}//Approve
			 }//Approve
			 
			 if(_logValidation(i_reject))
			 {
			 	if(i_reject.indexOf('T')>-1)
				{
					reject_expense_report(i_internal_id,i_ref_no,i_current_user_name,i_current_user)
					nlapiLogExecution('DEBUG', 'schedulerFunction',' ************ Reject ******** -->' + i_internal_id);
	
				}//Reject
			 }//Reject
					
		}//TR Loop		
	
	 }//Data Array
  }//TRY
  catch(exception)
  {
  	nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
  }//CATCH
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================



/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function approve_timesheet(i_internal_id)
{
  if(_logValidation(i_internal_id))
  {
  	var o_recordOBJ = nlapiLoadRecord('timebill',i_internal_id);
	
	if(_logValidation(o_recordOBJ))
	{
	 o_recordOBJ.setFieldValue('custcol_approvalstatus',1)
     o_recordOBJ.setFieldValue('approvalstatus',1)
	 o_recordOBJ.setFieldValue('supervisorapproval','T')
	 
	 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	 nlapiLogExecution('DEBUG', 'approve_timesheet',' ************ Time Submit ID *********** -->' + i_submitID);
		
	}//Record OBJ	
  }//Record ID	
}//Approve


function reject_timesheet(i_internal_id)
{	
  if(_logValidation(i_internal_id))
  {
  	var o_recordOBJ = nlapiLoadRecord('timebill',i_internal_id);
	
	if(_logValidation(o_recordOBJ))
	{
	  o_recordOBJ.setFieldValue('custcol_approvalstatus',2)
	  o_recordOBJ.setFieldValue('supervisorapproval','F')
	  
	 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	 nlapiLogExecution('DEBUG', 'reject_timesheet',' ************ Time Submit ID *********** -->' + i_submitID);
	
	}//Record OBJ	
  }//Record ID		
}//Reject


function approve_expense_report(i_internal_id,i_ref_no,i_employee_name)
{
  var a_approve_array = new Array();
  var a_reject_array = new Array();
  var recipient = '';
  var flag = 0;
  var i_check_status;
  	
  var i_current_user = nlapiGetUser();
  if(_logValidation(i_internal_id))
  {
  	var o_recordOBJ = nlapiLoadRecord('expensereport',i_internal_id);
	
	if(_logValidation(o_recordOBJ))
	{
	 var i_status = o_recordOBJ.getFieldValue('status');
	  nlapiLogExecution('DEBUG', 'approve_expense_report',' Status -->' + i_status);
	
	 i_check_status = i_status
	 
	 var i_expense_status = o_recordOBJ.getLineItemValue('expense','custcol_approvalstatus',i_ref_no);
	 nlapiLogExecution('DEBUG', 'approve_expense_report',' Expense Status -->' + i_expense_status);
	  	     
	 var i_employee_name = 	o_recordOBJ.getFieldText('entity');
	 nlapiLogExecution('DEBUG', 'approve_expense_report',' Employee Name -->' + i_employee_name);
	  	     	 	 
	 var i_finance_approver = o_recordOBJ.getFieldValue('custbody_expenseapprover');
	 
	 var i_supervisor = o_recordOBJ.getFieldValue('custbody_reportingmanager');	
	
	 var i_line_count = o_recordOBJ.getLineItemCount('expense')
	 
	 if(_logValidation(i_line_count))
	 {
	 	for(var yt=1;yt<=i_line_count;yt++)
		{
			var i_approval_status = o_recordOBJ.getLineItemValue('expense','custcol_approvalstatus',yt);
			nlapiLogExecution('DEBUG', 'approve_expense_report',' Approval Status -->' + i_approval_status);
			
			var i_ref_no_e = o_recordOBJ.getLineItemValue('expense','refnumber',yt);
			nlapiLogExecution('DEBUG', 'approve_expense_report',' Ref No.  -->' + i_ref_no_e);
	  	     
			a_approve_array.push(i_ref_no_e+'%%%'+i_approval_status+'%%%'+yt);
		
		}//Loop
		a_approve_array = removearrayduplicate(a_approve_array);	 	
		
	 }//Line Count
	
	nlapiLogExecution('DEBUG', 'approve_expense_report',' Approve Array -->'+a_approve_array);
	nlapiLogExecution('DEBUG', 'approve_expense_report',' Approve Array Length-->'+a_approve_array.length);
	
	///var a_approve_array = new Array();
	var a_split_array_e = new Array();
	
	if(a_approve_array!=null && a_approve_array!='' && a_approve_array != 'undefined')
	{
	nlapiLogExecution('DEBUG', 'approve_expense_report',' ,,,,,,,,,,,,,,');
	
	  for(var ft =0;ft<a_approve_array.length;ft++)
	  {
	  	nlapiLogExecution('DEBUG', 'approve_expense_report',' ft -->'+ft);
	
	  	a_split_array_e = a_approve_array[ft].split('%%%')
		
		var i_ref_no_y = a_split_array_e[0]
		var i_app_status =a_split_array_e[1]
		var i_line_no =a_split_array_e[2]
		
		nlapiLogExecution('DEBUG', 'approve_expense_report',' i_ref_no_y -->'+i_ref_no_y);
		nlapiLogExecution('DEBUG', 'approve_expense_report',' i_app_status -->'+i_app_status);
		nlapiLogExecution('DEBUG', 'approve_expense_report',' Line No -->'+i_line_no);
		
		if(i_ref_no == i_ref_no_y)
		{
		 if(i_status == 'Pending Supervisor Approval')
		 {
		 	nlapiLogExecution('DEBUG', 'approve_expense_report',' i_line_count -->'+i_line_count);
		 	if(i_line_count == 1)
			{
			  if(i_app_status == 1)
			  {		  	
			    nlapiLogExecution('DEBUG', 'approve_expense_report',' i_ref_no Block 1-->'+i_ref_no);
		  		o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_line_no,2)
				o_recordOBJ.setFieldValue('supervisorapproval','T')	
			  }	
			}
		 	
		 }		 
		  if(i_line_count > 1)
		  {
		  if(i_status == 'Pending Supervisor Approval')
		  {		  	
				nlapiLogExecution('DEBUG', 'approve_expense_report',' first i_app_status-->'+i_app_status);
				
				if(i_app_status == 1)
				{
					nlapiLogExecution('DEBUG', 'approve_expense_report',' inside first i_app_status-->'+i_app_status);
								
					o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_line_no,2)
				}
				
			}
		/*
  if(i_status == 'Pending Supervisor Approval' && i_expense_status == 1)
		  {		  	
				nlapiLogExecution('DEBUG', 'approve_expense_report',' i_app_status-->'+i_app_status);
				
				if(i_app_status == 1)
				{				
					o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
				}
				
			}	
*/	  
			
		  }		
		   if(i_line_count == 1)
	       {
			   if(i_status == 'Pending Accounting Approval')
			   {
			   		  if(i_app_status == 1)
					  {
				  		o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_line_no,2)
					  }	
				}
		   				
		   }//Accounting
		    if(i_line_count > 1)
			{ 
	         if(i_status == 'Pending Accounting Approval')
		     {		  	
				if(i_app_status == 1)
				{
					o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_line_no,2)
				}				
			 }			
		  } //Accounting			
		}
		
	  }//Loop		  
	   var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	   nlapiLogExecution('DEBUG', 'approve_timesheet',' ************ Expense Report Submit ID 1 *********** -->' + i_submitID);
	 
	    if(_logValidation(i_submitID))
		{
			var a_app_array = new Array();
			var o_recordOBJ = nlapiLoadRecord('expensereport',i_submitID);
			
			if(_logValidation(o_recordOBJ))
			{			
			  var i_status = o_recordOBJ.getFieldValue('status');
			  nlapiLogExecution('DEBUG', 'approve_expense_report',' Status -->' + i_status);
						 
			 var i_expense_status = o_recordOBJ.getLineItemValue('expense','custcol_approvalstatus',i_ref_no);
			 nlapiLogExecution('DEBUG', 'approve_expense_report',' Expense Status -->' + i_expense_status);		
			
			 var i_line_count = o_recordOBJ.getLineItemCount('expense')
	 
			 if(_logValidation(i_line_count))
			 {
			 	for(var yt=1;yt<=i_line_count;yt++)
				{
					var i_approval_status = o_recordOBJ.getLineItemValue('expense','custcol_approvalstatus',yt);
					nlapiLogExecution('DEBUG', 'approve_expense_report',' Approval Status -->' + i_approval_status);
													  	     
					a_app_array.push(i_approval_status);
				
				}//Loop
				a_app_array = removearrayduplicate(a_app_array);	
				
				if(a_app_array.length == 1)
				{
					if(a_app_array.indexOf('2')>-1)
					{
						 if(i_check_status == 'Pending Supervisor Approval')
						{
						o_recordOBJ.setFieldValue('supervisorapproval','T')
						
						 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	                     nlapiLogExecution('DEBUG', 'approve_timesheet',' ************ Supervisor Expense Report Submit ID *********** -->' + i_submitID);
	 	
		                 if(_logValidation(i_submitID))
						 {
						 	 send_mail_approve_expense(i_employee_name,i_current_user,i_finance_approver,i_submitID)
						 }		
		
		                 flag = 1
		 				}						
					}
					if(flag!=1)
					{
					  if(i_check_status == 'Pending Accounting Approval' )
					  {					  	
						o_recordOBJ.setFieldValue('accountingapproval','T')	 
						
						var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	                     nlapiLogExecution('DEBUG', 'approve_timesheet',' ************ Accounting Expense Report Submit ID *********** -->' + i_submitID);
	 		
					  }
					}				  
					
				} 	
				
			 }//Line Count
				
				
			}//o_recordOBJ
			
			
		}//i_submitID
	 
	 
	}//a_approve_array
	
	 /*
if(i_status == 'Pending Supervisor Approval' && i_expense_status == 3)
	 {		
		if(i_line_count == 1)
		{
		  if(a_approve_array.indexOf('3')>-1)
		  {		  	
	  		o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
			o_recordOBJ.setFieldValue('supervisorapproval','T')	
		  }	
		}
		if(i_line_count>1 && a_approve_array.length == 1)
		{
		  if(a_approve_array.indexOf('1')>-1)
		  {
		  	o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
			o_recordOBJ.setFieldValue('supervisorapproval','T')
		  }	
		 
		}//More than 1	
		if(i_line_count>1 && a_approve_array.length > 1)
		{
			if(a_approve_array.indexOf('3')>-1)
			{
				o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
				o_recordOBJ.setFieldValue('supervisorapproval','T')
			}
			
		}	
		
		recipient = i_finance_approver ;
	 }	
*/
	/*
 if(i_status == 'Pending Accounting Approval' && i_expense_status == 1)
	 {
	 	o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
        o_recordOBJ.setFieldValue('accountingapproval','T')	 	
		
		if(i_line_count == 1)
		{
		  if(a_approve_array.indexOf('3')>-1)
		  {
	  		o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
			o_recordOBJ.setFieldValue('supervisorapproval','T')	
		  }	
		}
		if(i_line_count>1 && a_approve_array.length == 1)
		{
		  if(a_approve_array.indexOf('1')>-1)
		  {
		  	o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
			o_recordOBJ.setFieldValue('supervisorapproval','T')
		  }	
		  else
		  {
		  	o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',i_ref_no,1)
		  }
		}//More than 1	
	 }	
*/	
	
	
	
		
	}//Record OBJ	
  }//Record ID	
}//Approve


function reject_expense_report(i_internal_id,i_ref_no,i_current_user_name,i_current_user)
{	
  if(_logValidation(i_internal_id))
  {
  //var i_current_user =  nlapiGetUser();
  	
  	var o_recordOBJ = nlapiLoadRecord('expensereport',i_internal_id);
	
	if(_logValidation(o_recordOBJ))
	{	
	 var i_expense_no = o_recordOBJ.getFieldValue('tranid');	 
	 var i_employee_name = o_recordOBJ.getFieldText('entity');
	 var i_employee = o_recordOBJ.getFieldValue('entity');
	 
	  var i_line_count = o_recordOBJ.getLineItemCount('expense')
	 
	 if (_logValidation(i_line_count)) 
	 {
	 	for (var yt = 1; yt <= i_line_count; yt++) 
		{	 	
		   o_recordOBJ.setLineItemValue('expense','custcol_approvalstatus',yt,3);
	 		
	 	}//Loop
	 }
	 
	 	 
   
	 
	 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
	 nlapiLogExecution('DEBUG', 'reject_timesheet',' ************ Expense Report Submit ID *********** -->' + i_submitID);
		
	send_mail_reject_expense(i_expense_no,i_employee_name,i_current_user,i_employee,i_internal_id,i_current_user_name)
	
	}//Record OBJ	
  }//Record ID		
}//Reject



/**
 * 
 * @param {Object} array
 * 
 * Description --> Returns the unique elements of the array
 */
function removearrayduplicate(array)
{
   var newArray = new Array();
   label:for(var i=0; i<array.length;i++ )
     {  
     for(var j=0; j<array.length;j++ )
      {
      if(newArray[j]==array[i]) 
      continue label;
     }
     newArray[newArray.length] = array[i];
      }
   return newArray;
}
function send_mail_reject_expense(i_expense_no,i_employee_name,i_current_user,i_employee,i_internal_id,i_current_user_name)
{
    var i_author = i_current_user
	
    var i_receipient_mail = get_emailID(i_employee)
		
	var i_recipient = i_receipient_mail
	
	var s_email_body = 'Hello'+' '+' '+ i_employee_name+'\n\n'
	
	s_email_body+='Expenses raised by you has been rejected by '+i_current_user_name+'\n\n'
	
	s_email_body+='Regards\n'
	s_email_body+='Information Systems'
	   
	var s_email_subject = ' Expenses - '+i_expense_no+' raised by you has been rejected. '
	
	var records = new Object();
    records['transaction'] = i_internal_id	
	
	if(_logValidation(i_author)&&_logValidation(i_recipient))
	{
	  nlapiSendEmail(i_author, i_recipient, s_email_subject, s_email_body, null , null, records, null);
	  nlapiLogExecution('DEBUG', 'send_mail_reject_expense', ' Email Sent ......');	
					
	}//Author / Recipient	
}
function send_mail_approve_expense(i_employee_name,i_current_user,i_employee,i_internal_id)
{
	var i_author = i_current_user
	
    var i_receipient_mail = get_emailID(i_employee)
		
	var i_recipient = i_receipient_mail
	
	var s_email_body = 'Hello,\n\n'
	s_email_body+=i_employee_name+' '+' '+ 'has raised expenses for your approval , please login to the Netsuite to Approve / Reject the Expense Report .\n\n'
 	s_email_body+='Regards\n'
	s_email_body+='Information Systems'
	   
	var s_email_subject = i_employee_name+ 'has raised expenses for your approval'
	
	var records = new Object();
    records['transaction'] = i_internal_id	
	
	if(_logValidation(i_author)&&_logValidation(i_recipient))
	{
	  nlapiSendEmail(i_author, i_recipient, s_email_subject, s_email_body, null , null, records, null);
	  nlapiLogExecution('DEBUG', 'send_mail_reject_expense', ' Email Sent ......');	
					
	}//Author / Recipient
}
function get_emailID(i_employee)
{
	var s_email;
	if(_logValidation(i_employee))
	{
		var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
		
		if(_logValidation(o_employeeOBJ))
		{
			s_email = o_employeeOBJ.getFieldValue('email')
			
		}//Employee OBJ
		
	}//Employee	
	return s_email;
}//Email

function get_employee_name(i_employee)
{
	var s_name;
	if(_logValidation(i_employee))
	{
		var o_employeeOBJ = nlapiLoadRecord('employee',i_employee);
		
		if(_logValidation(o_employeeOBJ))
		{
			var s_first_name = o_employeeOBJ.getFieldValue('firstname')
			var s_middle_name = o_employeeOBJ.getFieldValue('middlename')
			var s_last_name = o_employeeOBJ.getFieldValue('lastname')
			
			if(!_logValidation(s_first_name))
			{
				s_first_name = ''
			}
			if(!_logValidation(s_middle_name))
			{
				s_middle_name = ''
			}
			if(!_logValidation(s_last_name))
			{
				s_last_name = ''
			}
			
			s_name = s_first_name+''+s_middle_name+''+s_last_name;
			
		}//Employee OBJ
		
	}//Employee	
	return s_name;
	
}





// END FUNCTION =====================================================
