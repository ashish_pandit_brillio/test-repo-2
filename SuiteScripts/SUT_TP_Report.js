// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_TP_Report.js
	Author:Sachin k
	Company:Aashna
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY
    	if(request.getMethod() == 'GET')
		{
			var form = nlapiCreateForm('Transfer Price Report');
	        form.setScript('customscript_cli_tp_reportdisplay');
			form.addButton('printreport', 'Print Report','reportprint()');
			response.writePage(form);
		}



}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
