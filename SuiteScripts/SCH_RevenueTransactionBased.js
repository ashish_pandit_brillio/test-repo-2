/**
 * @author Jayesh
 */

function scheduled(type) {
	try {
		var context = nlapiGetContext();
		var usd_inr_rate = context.getSetting('SCRIPT', 'custscript_usd_inr_rate_today');
		var usd_gbp_rate = context.getSetting('SCRIPT', 'custscript_usd_gbp_rate_today');
		var s_proj_end_date_after = context.getSetting('SCRIPT', 'custscript_proj_end_to_use');
		nlapiLogExecution('audit','usd_inr_rate :- '+usd_inr_rate,'usd_gbp_rate :- '+usd_gbp_rate);
		nlapiLogExecution('audit','proj end date after :- ',s_proj_end_date_after);
		//return;
		/*var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_monthly_project', null, 'is', 'F');
		filters[1] = new nlobjSearchFilter('custrecord_fb_project', null, 'is', 'F');
		var searchResult = searchRecord('customrecord_revenue_projection_tm', null, null, null);
		for (var i = 0; i < searchResult.length; i++) {
			nlapiLogExecution('audit','i:- '+i);
			var rcrd = nlapiLoadRecord('customrecord_revenue_projection_tm',searchResult[i].getId());
			nlapiSubmitRecord(rcrd,true,true);
			yieldScript(context);
		}
		return;*/
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_monthly_project', null, 'is', 'F');
		filters[1] = new nlobjSearchFilter('custrecord_fb_project', null, 'is', 'F');
		var searchResult = searchRecord('customrecord_revenue_projection_tm', null, null, null);
		
		for (var i = 0; i < searchResult.length; i++) {
			nlapiDeleteRecord('customrecord_revenue_projection_tm',searchResult[i].getId());
			yieldScript(context);
		}
		
		delete searchResult;
		
		nlapiYieldScript();
		
		var year_start = new Date(s_proj_end_date_after);
		var yyyy = year_start.getFullYear();
		var year_start_date = '1/1/'+yyyy;
		var year_end_date = '12/31/'+yyyy
		var newYear = nlapiStringToDate(year_start_date);
		var projectList = [];
		
		//searchRecord('job', '1540').forEach(function(project) {
			
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'TM');
			filters[1] = new nlobjSearchFilter('enddate', null, 'after', s_proj_end_date_after);
			filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
			//filters[2] = new nlobjSearchFilter('internalid', null, 'anyof', 4400);
			
			var column_proj = new Array();	
			column_proj[0] = new nlobjSearchColumn('entityid');
			column_proj[1] = new nlobjSearchColumn('startdate');
			column_proj[2] = new nlobjSearchColumn('enddate');
			column_proj[3] = new nlobjSearchColumn('customer');
			column_proj[4] = new nlobjSearchColumn('custentity_region');
			column_proj[5] = new nlobjSearchColumn('custentity_practice');
			column_proj[6] = new nlobjSearchColumn('custentity_projectvalue');
			
			var searchResult = searchRecord('job', null, filters, column_proj);
			if (searchResult) {
				for (var i = 0; i < searchResult.length; i++) {
					
					var pro_id = searchResult[i].getId();
					var pro_entity_id = searchResult[i].getValue('entityid');
					var pro_strtDate = searchResult[i].getValue('startdate');
					var pro_endDate = searchResult[i].getValue('enddate');
					var pro_cust = searchResult[i].getValue('customer');
					var pro_cust_name = searchResult[i].getText('customer');
					var pro_region = searchResult[i].getValue('custentity_region');
					var pro_practice = searchResult[i].getText('custentity_practice');
					var project_value = searchResult[i].getValue('custentity_projectvalue');
					
					var project_name = pro_entity_id+' '+pro_cust_name;
					
					var filters_search_transaction = new Array();
					filters_search_transaction[0] = new nlobjSearchFilter('custcolprj_name', null, 'startswith', pro_entity_id);					
					filters_search_transaction[1] = new nlobjSearchFilter('account', null, 'anyof', ['644', '647' ]);
					
					var transaction_srch = searchRecord('transaction', null, filters_search_transaction, [
			        new nlobjSearchColumn('fxamount', null, 'sum'),
					new nlobjSearchColumn('custcol_employeenamecolumn', null, 'group') ]);
					
					if (_logValidation(transaction_srch)) {
						for (var index = 0; index < transaction_srch.length; index++) {
							
							//var currency = transaction_srch[index].getText('currency', null, 'group');
							var amount = transaction_srch[index].getValue('fxamount', null, 'sum');
							//var practice = transaction_srch[index].getText('department', null, 'group');
							//var practice_id = transaction_srch[index].getValue('department', null, 'group');
							var emp_name = transaction_srch[index].getText('custcol_employeenamecolumn', null, 'group');
							var emp_id = transaction_srch[index].getValue('custcol_employeenamecolumn', null, 'group');
							var startDate = nlapiStringToDate(pro_strtDate);
														
							if (startDate < newYear) {
								startDate = newYear;
							}
							
							projectList.push({
								proj_id: pro_id,
								proj_name: project_name,
								pro_region: pro_region,
								pro_practice: pro_practice,
								pro_cust: pro_cust,
								resource: emp_id,
								resource_prac: '',
								resource_practice_id: '',
								StartDate: nlapiDateToString(startDate, 'date'),
								EndDate: pro_endDate,
								proj_actual_strt_date: pro_strtDate,
								proj_actual_end_date: pro_endDate,
								allocation_strt_date: '',
								allocation_end_date: '',
								emp_bill_rate: '',
								prcnt_of_time: '',
							});
						}
					}
					
				}
			}

		//nlapiLogExecution('debug', 'project count', projectList.length);
		
		for (var i = 0; i < projectList.length; i++) {
			var project = projectList[i];
			var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(project.StartDate),
			        nlapiStringToDate(project.EndDate));

			for (var j = 0; j < monthBreakUp.length; j++) {
				var months = monthBreakUp[j];
				try {
					var rec = nlapiCreateRecord(
					        'customrecord_revenue_projection_tm', {
						        recordmode : 'dynamic'
					        });
					rec.setFieldValue('custrecord_project_name_tm_transaction', project.proj_id);
					rec.setFieldValue('custrecord_strt_date_tm_transaction',months.Start);
					//rec.setFieldValue('custrecord_region_revenue',project.pro_region);
					rec.setFieldValue('custrecord_practice_tm_transaction',project.pro_practice);
					//rec.setFieldValue('custrecord_practice_id_tm_transaction',project.resource_practice_id);
					rec.setFieldValue('custrecord_customer_tm_transaction',project.pro_cust);
					rec.setFieldValue('custrecord_resource_tm_transaction',project.resource);
					//rec.setFieldValue('custrecord_emp_practice_tm_transaction',project.resource_prac);
					rec.setFieldValue('custrecord_pro_strt_date_tm_transaction',project.proj_actual_strt_date);
					rec.setFieldValue('custrecord_pro_end_date_tm_transaction',project.proj_actual_end_date);
					rec.setFieldValue('custrecord_usd_inr_rate_conversion',usd_inr_rate);
					rec.setFieldValue('custrecord_usd_gbp_conversion',usd_gbp_rate);
					
					var recId = nlapiSubmitRecord(rec, true, true);
					nlapiLogExecution('debug', 'record created', recId);
					yieldScript(context);
				} catch (err) {
					nlapiLogExecution('ERROR', 'Failed To Create Break-Up For '
					        + project.proj_id + " for " + months.Start, err);
				}
			}
			nlapiLogExecution('debug', 'pro id:- ', project.proj_id);
		}
		
		var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
		
		strVar += '<p>T&M revenue projection report can be downloaded now.</p>';
		strVar += '<a href="https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1019&deploy=1">Click to download</a>';
		
		strVar += '</body>';
		strVar += '</html>';
			
		nlapiSendEmail(442, 'bhavanishankar.t@brillio.com', 'Please download T&M revenue report', strVar);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	var dateBreakUp = [];
	var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
	        .getMonth(), 1);

	dateBreakUp.push({
	    Start : nlapiDateToString(new_start_date, 'date'),
	    End : ''
	});

	for (var i = 1;; i++) {
		var new_date = nlapiAddMonths(new_start_date, i);

		if (new_date > d_endDate) {
			break;
		}

		dateBreakUp.push({
		    Start : nlapiDateToString(new_date, 'date'),
		    End : ''
		});
	}

	return dateBreakUp;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
