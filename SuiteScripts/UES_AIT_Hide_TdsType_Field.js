// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_hideTDStype(type){

	/*  On before load:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	//  BEFORE LOAD CODE BODY
	
	var o_getid = form.getSubList('expense');
	
	if (o_getid != null && o_getid != '' && o_getid != undefined) 
	{
	
		var o_exp_tdsType_field = o_getid.getField('custcol_tdstype');
		
		if (o_exp_tdsType_field != null && o_exp_tdsType_field != '' && o_exp_tdsType_field != undefined) {
			o_exp_tdsType_field.setDisplayType('hidden')
		}
		var o_exp_advance_field = o_getid.getField('custcol_ait_advance_applied');
		
		if (o_exp_advance_field != null && o_exp_advance_field != '' && o_exp_advance_field != undefined) {
			o_exp_advance_field.setDisplayType('hidden')
		}
	}
	var o_itemgetid = form.getSubList('item');
	
	if (o_itemgetid != null && o_itemgetid != '' && o_itemgetid != undefined) 
	{
		var o_item_tdsType_field = o_itemgetid.getField('custcol_tdstype');
	
		if (o_item_tdsType_field != null && o_item_tdsType_field != '' && o_item_tdsType_field != undefined) 
		{
			o_item_tdsType_field.setDisplayType('hidden');
			
		}
		var o_item_advance_field = o_itemgetid.getField('custcol_ait_advance_applied');
	
		if (o_item_advance_field != null && o_item_advance_field != '' && o_item_advance_field != undefined) 
		{
			o_item_advance_field.setDisplayType('hidden');
			
		}
	}
	
	return true;
}
// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES
	

	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
