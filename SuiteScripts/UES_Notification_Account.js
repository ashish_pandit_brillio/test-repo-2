/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Oct 2019     Aazamali Khan
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function uesNotificationAccount(type) {
	try {
		// create account into mariaDb
		if (type == "create") {
			var method = "POST";
			var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var obj = {};
			obj.internalId = nlapiGetRecordId();
			obj.type = recObj.getFieldValue("stage");
			obj.name = recObj.getFieldValue("altname");
			obj.regionName = recObj.getFieldText("custentity_region");
			obj.clientPartnerEmailId = nlapiLookupField("employee",recObj.getFieldValue("custentity_clientpartner"),"email");
          obj.cpEmployeeInternalId = nlapiLookupField(nlapiGetRecordType(),nlapiGetRecordId(),'custentity_clientpartner',false);
			obj = JSON.stringify(obj);
			var url = "https://fuelnode1.azurewebsites.net/account";
			var response = call_node(url, obj, method);
			nlapiLogExecution("DEBUG", "response : create :  ", JSON.stringify(response));
		}
		// update account into mariaDb
		if (type == "edit") {
			var method = "POST";
			var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var oldRec = nlapiGetOldRecord();
			var clientPartner = recObj.getFieldValue("custentity_clientpartner");
			var oldclientPartner = oldRec.getFieldValue("custentity_clientpartner");
			if (clientPartner != oldclientPartner) {
				var obj = {};
				obj.internalId = nlapiGetRecordId();
				obj.type = recObj.getFieldValue("stage");
				obj.name = recObj.getFieldValue("altname");
				obj.regionName = recObj.getFieldText("custentity_region");
				obj.clientPartnerEmailId = nlapiLookupField("employee",clientPartner,"email"); //recObj.getFieldValue("custentity_clientpartner");
               obj.cpEmployeeInternalId = nlapiLookupField(nlapiGetRecordType(),nlapiGetRecordId(),'custentity_clientpartner',false);
				obj = JSON.stringify(obj);
				var url = "https://fuelnode1.azurewebsites.net/account";
				var response = call_node(url, obj, method);
				nlapiLogExecution("DEBUG", "response : create :  ", JSON.stringify(response));
			}
		}
	} catch (e) {
		// TODO: handle exception
		nlapiLogExecution("DEBUG", "Error in script : ", e);
	}
}

function beforeSubmitDelete(type) {
	// delete account into mariaDb
	if (type == 'delete') {
		var body = {};
		var method = "DELETE";
		var url = "https://fuelnode1.azurewebsites.net/account/internal_id/"+nlapiGetRecordId();
		var response = call_node(url, body, method);
		nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
	}
}