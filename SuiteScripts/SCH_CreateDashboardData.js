//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
     Script Name	: SCH_CreateDashboardData
     Author			: Ashish Pandit
     Company		: Inspirria CloudTech Pvt Ltd
     Date			: 19/12/2018
     
	 
	 Script Modification Log:
     
	 -- Date --               -- Modified By --                  --Requested By--                   -- Description --
     

	 Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SCHEDULED FUNCTION
     - RFQbody_SCH_main()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
	 */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================
 
function scheduled_CreateDashboardData() 
{
	try
	{
		/*Array to push Dashboard data*/
		var a_DBdataArray = new Array();
		
		/*Search to get Dashboard data records*/
		var customrecord_fulfillment_dashboard_dataSearch = searchRecord("customrecord_fulfillment_dashboard_data",null,
		[
		], 
		[
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_project"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_account"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_opp_id"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_start_date"), 
		   new nlobjSearchColumn("custrecord_fulfill_dashboard_practice")
		]
		);
		if(customrecord_fulfillment_dashboard_dataSearch)
		{
			nlapiLogExecution('debug','customrecord_fulfillment_dashboard_dataSearch ',customrecord_fulfillment_dashboard_dataSearch.length);
			for(i=0;i<customrecord_fulfillment_dashboard_dataSearch.length;i++)
			{
				var i_opp_id = customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_opp_id')
				if(!i_opp_id)
				{
					a_DBdataArray.push(customrecord_fulfillment_dashboard_dataSearch[i].getValue('custrecord_fulfill_dashboard_project'));
				}
			}
			//nlapiLogExecution('debug','a_DBdataArray ',a_DBdataArray);
		}
		 
		/*Search to get Projects*/ 
		var jobSearch = nlapiSearchRecord("job",null,
		[
		   ["isinactive","is","F"], 
		   "AND", 
		   ["status","anyof","4","2"],  // Active/Pendig Projects 
		   "AND", 
		   ["enddate","onorafter","startofthismonth"],
           "AND",
           ["jobtype","anyof","2"],
           "AND",
           ["custentity_project_allocation_category","anyof","1"]
		], 
		[
		   new nlobjSearchColumn("altname"), 
		   new nlobjSearchColumn("jobtype"), 
		   new nlobjSearchColumn("startdate"), 
		   new nlobjSearchColumn("custentity_projectmanager"), 
		   new nlobjSearchColumn("custentity_location"), 
		   new nlobjSearchColumn("custentity_executingpractice"), 
		   new nlobjSearchColumn("custentity_practice"), 
		   new nlobjSearchColumn("custentity_enddate"),
		   new nlobjSearchColumn("customer")
		   
		]
		);
		if(jobSearch)
		{
			nlapiLogExecution('Debug','jobSearch.length ',jobSearch.length);
			for(i=0;i<jobSearch.length;i++) 
			{
				var i_projectID = jobSearch[i].getId();
				/*If Dashboard data is not found then create the same*/
				if(a_DBdataArray.indexOf(i_projectID)==-1)
				{
					nlapiLogExecution('debug','In Loop Matching '+i_projectID,a_DBdataArray.indexOf(i_projectID)==-1);
					var i_project = jobSearch[i].getId();
					var i_account = jobSearch[i].getValue('customer');
					var i_sales_status = "Booked";
					var s_winability_per = "ONGOING";
					var i_projectManager = jobSearch[i].getText('custentity_projectmanager');
					var i_practice = jobSearch[i].getValue('custentity_practice');
					var i_team_size = GetTeamSize(i_project);
					var currentDate = sysDate();
					var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
					var currentDateAndTime = currentDate + ' ' + currentTime;
					var i_startDate = jobSearch[i].getValue('startdate');
					var n_frf = "0";
					var n_rrf = "0";
					
					if(i_team_size)
						i_team_size = i_team_size.toString();
					var i_emp_exiting = GetEmployeeExist(i_project);
					if(i_emp_exiting)
						i_emp_exiting = i_emp_exiting.toString();
					
					/*Create Dashboard custom records*/
					try
					{
						var recordObj = nlapiCreateRecord('customrecord_fulfillment_dashboard_data');
						//recordObj.setFieldValue('custrecord_fulfill_dashboard_status',);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_project',i_project);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_account',i_account);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_reve_confid',i_sales_status);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_rev_sta_pro',s_winability_per);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_manager',GetManagerName(i_projectManager));
						recordObj.setFieldValue('custrecord_fulfill_dashboard_practice',i_practice);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_pro_team',i_team_size);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_exiting',i_emp_exiting);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_frf',n_frf);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_rrf',n_rrf);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_last_update',currentDateAndTime);
						recordObj.setFieldValue('custrecord_fulfill_dashboard_start_date',i_startDate);
						var rec = nlapiSubmitRecord(recordObj);
						nlapiLogExecution('Debug','rec ',rec);
					}
					catch(error)
					{
						nlapiLogExecution('debug','Exception Occured du',error)
					}
				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug','Exception ',e.message);
	}
}

//===================================== END OF FUNCTION ================================================

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

/*----Function to count Team Size-----*/
function GetTeamSize(i_project) {
	var jobSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["project","anyof",i_project]
			 ], 
			[
			 // new nlobjSearchColumn("resource","resourceAllocation","group")
			  new nlobjSearchColumn("resource",null,"GROUP")
			]
	);
	if (jobSearch) {
	//var resourceCheck = jobSearch[0].getValue("resource",null,"GROUP");
	
		return jobSearch.length;
	}else{
		return "0";
	}
}
/*----Function to count employee exiting in 30 days-----*/
function GetEmployeeExist(i_project) {
	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}
	var date = new Date();
	date.addDays(30);
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
			[
			 ["job.internalid","anyof",i_project], //5515 project internal ID
			 "AND", 
			 ["employee.custentity_future_term_date","onorafter","today"] // 01/05/2019 calculated dynamically using current date and adding 30 days to it. 
			 ], 
			 [
			   new nlobjSearchColumn("resource",null,"GROUP")
			 ]
	);
	if (resourceallocationSearch) {
		return resourceallocationSearch.length;
	}else{
      return "";
    }
}
/*----Function to get FRF and RRF count-----*/
/*Function to get FRF details*/
function GetFRF(i_oppID) {
	nlapiLogExecution('AUDIT', 'project ID ', i_oppID);
	var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
	[
	   ["custrecord_frf_details_opp_id","anyof",i_oppID]
	], 
	[
		new nlobjSearchColumn("custrecord_frf_details_external_hire")
	]
	);
	return customrecord_frf_detailsSearch;
}


//Get current date
function sysDate() {
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
	}
//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
	    meridian += "pm";
	} else {
	    meridian += "am";
	}
	if (hours > 12) {

	    hours = hours - 12;
	}
	if (minutes < 10) {
	    minutes = "0" + minutes;
	}
	if (seconds < 10) {
	    seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes +" ";
	return str + meridian;
	}
	function GetManagerName(tempString) {
	var s_manager = "";
	//var tempString = searchResult[int].getText("custentity_projectmanager","custrecord_project_internal_id_sfdc",null);
	temp = tempString.indexOf("-");
	if(temp>0)
	{
		var s_manager = tempString.split("-")[1];
	}
	else{
		var s_manager = tempString;
	}
	return s_manager;
}

//Search function for results more than 1000
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}


function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
