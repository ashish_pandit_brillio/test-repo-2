//12,209,659.11

function update_Invoice_service_tax()
{
	try
	{		
		var record = nlapiLoadRecord('invoice',318070);
		
		var billingFrom = record.getFieldValue('custbody_billfrom');
		var billingTo = record.getFieldValue('custbody_billto');
		
		billingFrom = nlapiStringToDate(billingFrom);
		billingTo = nlapiStringToDate(billingTo);
		
		var i_billable_time_count = record.getLineItemCount('item');
		
		for (var i = 1; i <= i_billable_time_count; i++)
		{
			//var billedDate = nlapiStringToDate(record.getLineItemValue('time', 'billeddate', i));
			//var is_applied = record.getLineItemValue('time', 'apply', i);
			//if (billingFrom <= billedDate && billingTo >= billedDate)
			//if(is_applied == 'T')
			{
				nlapiLogExecution('audit','checked');
				//record.setLineItemValue('time','apply',i,'T');
				record.setLineItemValue('item', 'taxcode', i, 2552);
			}
		}
		nlapiLogExecution('audit','checked complete');
		var id = nlapiSubmitRecord(record,true,true);
		nlapiLogExecution('audit','invoice id:- ',id);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

function yieldScript(currentContext) {

		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}