function onSaveRecord()
{
	try
	{
		var projecttype = nlapiGetFieldValue('jobtype');
		var customer = nlapiGetFieldValue('customer');
		if(customer != 5205 && projecttype == 2)
		{
			var onsite_hours_per_day = nlapiGetFieldValue('custentity_onsite_hours_per_day');
			var onsite_hours_per_week = nlapiGetFieldValue('custentity_onsite_hours_per_week');
			var offshore_hours_per_day = nlapiGetFieldValue('custentity_hoursperday');
			var offshore_hours_per_week = nlapiGetFieldValue('custentity_hoursperweek');
			if(!_logValidation(onsite_hours_per_day))	
			{
				alert ('Please Enter the value for ONSITE HOURS PER DAY field');
			}
			if(!_logValidation(onsite_hours_per_week))	
			{
				alert ('Please Enter the value for ONSITE HOURS PER WEEK field');
			}
			/*if(!_logValidation(offshore_hours_per_day))	
			{
				alert ('Please Enter the value for OFFSHORE HOURS PER DAY field');
			}
			if(!_logValidation(offshore_hours_per_week))	
			{
				alert ('Please Enter the value for OFFSHORE HOURS PER WEEK field');
			}*/
		
			if(onsite_hours_per_day != '' && onsite_hours_per_week != '')
			{
				return true;
			}
		}
		else if(customer == 5205 && projecttype == 2)
		{
			alert ('For Brillio Customer the project cannot be tagged as External');
		}
		else
			return true;
		
		
	}
	catch(err)
	{
		nlapiLogExecution('debug','Error in the on save function',err);
		throw err;
	}
}

function _logValidation(value) {
    if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
        return true;
    } else {
        return false;
    }
}