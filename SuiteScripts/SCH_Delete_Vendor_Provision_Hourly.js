// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Delete_Vendor_Provision.js
	Author      : Jayesh Dinde
	Date        : 26 April 2016
    Description : Delete sal upload entries created for particular month and JE if exeist.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type)
{
	try
	{
		var i_context = nlapiGetContext();
		var sal_upload_month = i_context.getSetting('SCRIPT', 'custscript_month_to_del');
		var sal_upload_year = i_context.getSetting('SCRIPT', 'custscript_year_to_del');
		var rcrd_id = i_context.getSetting('SCRIPT', 'custscriptprovision_rcrd_id');
		rcrd_id = rcrd_id.trim();
		var vendor_provision_subsidiary = i_context.getSetting('SCRIPT', 'custscript_subsidiary_to_del');
		vendor_provision_subsidiary = parseInt(vendor_provision_subsidiary);
		
		var filters_sal_upload = new Array();
		filters_sal_upload[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',sal_upload_month.trim());
		filters_sal_upload[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',sal_upload_year.trim());
		filters_sal_upload[2] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null,'is',3);
		filters_sal_upload[3] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_provision_subsidiary));
		
		var sal_upload_search_results = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_sal_upload, null);
		
		if(_logValidation(sal_upload_search_results))
		{
			for(var i=0; i< sal_upload_search_results.length; i++)
				nlapiDeleteRecord('customrecord_salary_upload_file',sal_upload_search_results[i].getId());
		}
		
		var vendr_pro_rcrd = nlapiLoadRecord('customrecord_salary_upload_process_btpl',rcrd_id);
		var JE_array_list = vendr_pro_rcrd.getFieldValues('custrecord_journal_entries_created_btpl');
		if(_logValidation(JE_array_list))
		{
			for(var j=0; j< JE_array_list.length; j++)
				nlapiDeleteRecord('journalentry',JE_array_list[j]);
		}
		
		var filters_last_mnth_data = new Array();
		filters_last_mnth_data[0] = new nlobjSearchFilter('custrecord_current_month', null, 'is', sal_upload_month.trim());
		filters_last_mnth_data[1] = new nlobjSearchFilter('custrecord_current_year', null, 'is', sal_upload_year.trim());
		filters_last_mnth_data[2] = new nlobjSearchFilter('custrecord_rate_type_lst_mnth', null, 'is', 3);
		filters_last_mnth_data[3] = new nlobjSearchFilter('custrecord_subsidiary_processed', null, 'is', parseInt(vendor_provision_subsidiary));
		
		var i_search_results = nlapiSearchRecord('customrecord_vendor_pro_lst_mnth_data', null, filters_last_mnth_data, null);
		if(_logValidation(i_search_results))
		{
			for(var i=0; i< i_search_results.length; i++)
				nlapiDeleteRecord('customrecord_vendor_pro_lst_mnth_data',i_search_results[i].getId());
		}
		
		var filters_error_logs = new Array();
		filters_error_logs[0] = new nlobjSearchFilter('custrecord_month_ven_pro', null, 'is', sal_upload_month.trim());
		filters_error_logs[1] = new nlobjSearchFilter('custrecord_year_ven_pro', null, 'is', sal_upload_year.trim());
		filters_error_logs[2] = new nlobjSearchFilter('custrecord_rate_type_error_log', null, 'is', 3);
		filters_error_logs[3] = new nlobjSearchFilter('custrecord_subsidiary_ven_pro', null, 'is', parseInt(vendor_provision_subsidiary));
		
		var search_results = nlapiSearchRecord('customrecord_error_logs_vendor_pro', null, filters_error_logs, null);
		
		if(_logValidation(search_results))
		{
			for(var i=0; i< search_results.length; i++)
				nlapiDeleteRecord('customrecord_error_logs_vendor_pro',search_results[i].getId());
		}
		
		nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_status_btpl',8);
		nlapiSubmitField('customrecord_salary_upload_process_btpl',rcrd_id,'custrecord_notes_btpl','Data for vendor provision deleted, please process from start again!');
	} 
	catch (err)
	{
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
	}
}
// END SCHEDULED FUNCTION =============================================

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function schedule_script_after_usage_exceeded(i, i_month, i_year, i_account_debit, i_account_credit, i_recordID, vendor_provision_subsidiary, i_flag)
{
	////Define all parameters to schedule the script for voucher generation.
	 nlapiLogExecution('audit','Before Scheduling','i -->'+ i);
	 i = i++;
	 var params=new Array();
	 params['status']='scheduled';
 	 params['runasadmin']='T';
	 params['custscript_counter_btpl']=i;
	 params['custscript_sal_upload_date_month']=i_month;
	 params['custscript_sal_upload_date_year']=i_year;
	 params['custscript_account_debit_s_u_btpl']=i_account_debit;
	 params['custscript_account_credit_s_u_btpl']=i_account_credit;
	 params['custscriptrecord_id_s_u_btpl']=i_recordID;
	 params['custscript_vendor_pro_subsidiary']=vendor_provision_subsidiary;
	 params['custscript_flag_btpl']=i_flag;
		 
	 var startDate = new Date();
 	 params['startdate']=startDate.toUTCString();
	
	 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
	 //nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status -->'+ status);
	 
	 ////If script is scheduled then successfuly then check for if status=queued
	 if (status == 'QUEUED') 
 	 {
		//nlapiLogExecution('DEBUG', ' SCHEDULED', ' Script Is Re - Scheduled for -->'+i);
 	 }
}