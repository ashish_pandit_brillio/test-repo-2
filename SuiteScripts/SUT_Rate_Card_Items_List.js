/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_Rate_Card_Items_List.js
	Author      : Shweta Chopde
	Date        : 20 May 2014
	Description : Rate Card Item List


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{
  try
  {
  	if (request.getMethod() == 'GET')
	{
		var i_project = request.getParameter('custscript_project');
		nlapiLogExecution('DEBUG', 'get_project_task', ' Project-->' + i_project);
		
		var i_resource = request.getParameter('custscript_resource');
		nlapiLogExecution('DEBUG', 'get_project_task', ' Resource-->' + i_resource);
		
		
		
		
		var f_form = nlapiCreateForm(' Service Item List ')
		f_form.setScript('customscript_cli_rate_card_item_validati')
		
		var i_project_fld = f_form.addField('custpage_project', 'text', 'Project');
	     i_project_fld.setDefaultValue(i_project)
		 i_project_fld.setDisplayType('hidden')	
		
		
		var i_resource_fld = f_form.addField('custpage_resource', 'text', 'Resource');
	     i_resource_fld.setDefaultValue(i_resource)
		 i_resource_fld.setDisplayType('hidden')	
		
		
		
		var i_selected_item_fld = f_form.addField('custpage_selected_item', 'select', 'Item','item');
	     i_selected_item_fld.setDisplayType('hidden')	
		var i_item_fld = f_form.addField('custpage_item', 'select', 'Item');
		//	i_project = 2148
		  if(_logValidation(i_project))
		  {
		  	var filter = new Array();
		    filter[0] = new nlobjSearchFilter('custrecord_proj',null,'is',i_project);
			
			var columns = new Array();
		    columns[0] = new nlobjSearchColumn('custrecord_item1');
			
			 var a_search_results = nlapiSearchRecord('customrecord_customraterecord',null,filter,columns);
			 
			 if(_logValidation(a_search_results))
			 {
			 	  nlapiLogExecution('DEBUG', 'get_project_task', ' Search Results Length -->' + a_search_results.length);
				 i_item_fld.addSelectOption('','', false);
				  for(var t=0;t<a_search_results.length;t++)
				  {
				  	var i_service_item = a_search_results[t].getValue('custrecord_item1')
					
					var i_service_item_text = a_search_results[t].getText('custrecord_item1')
					
					i_item_fld.addSelectOption(i_service_item_text,i_service_item_text, false);
				  }//Loop	
				  				 	
			 }//Search Results
				
		  }//Employee
		
		f_form.addSubmitButton();
		response.writePage(f_form);
		
	}//GET
	else if (request.getMethod() == 'POST')
	{
	   var arr;
		var i_item =  request.getParameter('custpage_selected_item');
	    nlapiLogExecution('DEBUG','suiteletFunction',' Post Item -->'+i_item)
		
		var i_resource =  request.getParameter('custpage_resource');
	    nlapiLogExecution('DEBUG','suiteletFunction',' Post Resource -->'+i_resource)
			
		var i_project =  request.getParameter('custpage_project');
	    nlapiLogExecution('DEBUG','suiteletFunction',' Post Project -->'+i_project)
		
		arr = i_item+'##'+i_project+'##'+i_resource		
				
		response.write('<html><head><script>window.opener.add_item("' + arr + '");self.close();</script></head><body></body></html>');
	}//POST
  }
  catch(exception)
  {
  	 nlapiLogExecution('DEBUG', 'ERROR','Exception Caught -->' + exception);
  }//CATCH
	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================

function get_item_list(i_project)
{
  var a_item_array = new Array();		
  if(_logValidation(i_project))
  {
  	var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_proj',null,'is',i_project);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_item1');
	
	 var a_search_results = nlapiSearchRecord('customrecord_customraterecord',null,filter,columns);
	 
	 if(_logValidation(a_search_results))
	 {
	 	  nlapiLogExecution('DEBUG', 'get_project_task', ' Search Results Length -->' + a_search_results.length);
		 	
		  for(var t=0;t<a_search_results.length;t++)
		  {
		  	var i_service_item = a_search_results[t].getValue('custrecord_item1')
			a_item_array.push(i_service_item);
		  }//Loop	
		  nlapiLogExecution('DEBUG', 'get_project_task', ' Item Array  -->' + a_item_array);
		 	
	 }//Search Results
		
  }//Employee
  return a_item_array;
}


function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}


// END OBJECT CALLED/INVOKING FUNCTION =====================================================
