/**
 * @author Jayesh
 */

function scheduled(type) {
	try {
		
		var context = nlapiGetContext();
		var usd_inr_rate = context.getSetting('SCRIPT', 'custscript_usd_inr_rate');
		var usd_gbp_rate = context.getSetting('SCRIPT', 'custscript_usd_gbp_rate');
		var s_proj_end_date_after = context.getSetting('SCRIPT', 'custscript_proj_end_date_aftr');
		nlapiLogExecution('audit','usd_inr_rate :- '+usd_inr_rate,'usd_gbp_rate :- '+usd_gbp_rate);
		nlapiLogExecution('audit','proj end date after :- ',s_proj_end_date_after);
		//return;
		/*var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_fb_project', null, 'is', 'T');
		//filters[1] = new nlobjSearchFilter('custrecord_lst_participating_practice', null, 'is', 'T');
		var searchResult = searchRecord('customrecord_practice_revenue_empwise', null, filters, null);
		
		for (var i = 0; i < searchResult.length; i++) {
			nlapiLogExecution('audit','i:- '+i);
			var rcrd = nlapiLoadRecord('customrecord_practice_revenue_empwise',searchResult[i].getId());
			nlapiSubmitRecord(rcrd,true,true);
			yieldScript(context);
		}
		return;*/
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_fb_project', null, 'is', 'T');
		var searchResult = searchRecord('customrecord_practice_revenue_empwise', null, filters, null);
		
		for (var i = 0; i < searchResult.length; i++) {
			nlapiLogExecution('audit','counter:- '+i);
			nlapiDeleteRecord('customrecord_practice_revenue_empwise',searchResult[i].getId());
			if (context.getRemainingUsage() <= 1000)
				nlapiYieldScript();
		}
		
		delete searchResult;
		
		//return;
		
      	nlapiYieldScript();
      
	  	var year_start = new Date(s_proj_end_date_after);
		var yyyy = year_start.getFullYear();
		var year_start_date = '1/1/'+yyyy;
		var year_end_date = '12/31/'+yyyy
		var newYear = nlapiStringToDate(year_start_date);
		var end_year = nlapiStringToDate(year_end_date);
		var projectList = [];
		
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'FBM');
		filters[1] = new nlobjSearchFilter('enddate', null, 'onorafter', s_proj_end_date_after);
		//filters[2] = new nlobjSearchFilter('status', null, 'anyof', 2);
		filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		
		var column_proj = new Array();	
		column_proj[0] = new nlobjSearchColumn('entityid');
		column_proj[1] = new nlobjSearchColumn('startdate');
		column_proj[2] = new nlobjSearchColumn('enddate');
		column_proj[3] = new nlobjSearchColumn('customer');
		column_proj[4] = new nlobjSearchColumn('custentity_region');
		column_proj[5] = new nlobjSearchColumn('custentity_practice');
		column_proj[6] = new nlobjSearchColumn('custentity_projectvalue');
		column_proj[7] = new nlobjSearchColumn('subsidiary');
		
		var searchResult = searchRecord('job', null, filters, column_proj);
		
		if (searchResult)
		{	
			for (var i = 0; i < searchResult.length; i++)
			{
				var pro_id = searchResult[i].getId();
				var pro_entity_id = searchResult[i].getValue('entityid');
				var pro_strtDate = searchResult[i].getValue('startdate');
				var pro_endDate = searchResult[i].getValue('enddate');
				var pro_cust = searchResult[i].getValue('customer');
				var pro_cust_name = searchResult[i].getText('customer');
				var pro_region = searchResult[i].getValue('custentity_region');
				var pro_practice = searchResult[i].getText('custentity_practice');
				var project_value = searchResult[i].getValue('custentity_projectvalue');
				var proj_subsidiary = searchResult[i].getValue('subsidiary');
				
				var project_name = pro_entity_id+' '+pro_cust_name;
				
				var filters_transaction = new Array();
				filters_transaction[0] = new nlobjSearchFilter('custcolprj_name', null, 'startswith', pro_entity_id);
				if (proj_subsidiary == 2)
				{
					filters_transaction[1] = new nlobjSearchFilter('account', null, 'anyof',[ '773' ]);
				}
				else
				{ // BTPL
					filters_transaction[1] = new nlobjSearchFilter('account', null, 'anyof', ['773', '732' ]);
				}
				//filters_transaction[1] = new nlobjSearchFilter('account', null, 'anyof', ['773', '732' ]);
				filters_transaction[2] = new nlobjSearchFilter('type', null, 'noneof','SalesOrd');
				
				var transaction_srch = nlapiSearchRecord('transaction', null, filters_transaction, [
		        new nlobjSearchColumn('fxamount', null, 'sum'),
		        new nlobjSearchColumn('currency', null, 'group'),
				new nlobjSearchColumn('department', null, 'group') ]);
				
				if (_logValidation(transaction_srch)) {
					var practice_marked_checked_flag = 0;
					for (var index = 0; index < transaction_srch.length; index++)
					{
						var currency = transaction_srch[index].getText('currency', null, 'group');
						var amount = transaction_srch[index].getValue('fxamount', null, 'sum');
						var practice = transaction_srch[index].getText('department', null, 'group');
						var practice_id = transaction_srch[index].getValue('department', null, 'group');
						var startDate = nlapiStringToDate(pro_strtDate);
						if (startDate < newYear) {
							startDate = newYear;
						}
						
						var ENdDate = nlapiStringToDate(pro_endDate);
						if (ENdDate < end_year) {
							ENdDate = end_year;
						}
												
						var temp_transaction_len = parseInt(transaction_srch.length) - parseInt(1);
						if(parseInt(index) == 0 || parseInt(index) == 1)
						{
							if(practice_id)
							{
								if(practice_marked_checked_flag != 1)
								{
									practice_marked_checked_flag = 1;
									projectList.push({
										proj_id: pro_id,
										proj_name: project_name,
										pro_region: pro_region,
										pro_practice: pro_practice,
										pr_practice_id: practice_id,
										pro_cust: pro_cust,
										resource: '',
										resource_prac: '',
										StartDateActual: pro_strtDate,
										EndDateActual: pro_endDate,
										StartDate: nlapiDateToString(startDate, 'date'),
										EndDate: nlapiDateToString(ENdDate, 'date'),
										Practice: practice,
										project_value: project_value,
										last_practice: 1
									});
								}
								else
								{
									projectList.push({
										proj_id: pro_id,
										proj_name: project_name,
										pro_region: pro_region,
										pro_practice: pro_practice,
										pr_practice_id: practice_id,
										pro_cust: pro_cust,
										resource: '',
										resource_prac: '',
										StartDateActual: pro_strtDate,
										EndDateActual: pro_endDate,
										StartDate: nlapiDateToString(startDate, 'date'),
										EndDate: nlapiDateToString(ENdDate, 'date'),
										Practice: practice,
										project_value: project_value,
										last_practice: 0
									});
								}
								
							}
							else
							{
								projectList.push({
										proj_id: pro_id,
										proj_name: project_name,
										pro_region: pro_region,
										pro_practice: pro_practice,
										pr_practice_id: practice_id,
										pro_cust: pro_cust,
										resource: '',
										resource_prac: '',
										StartDateActual: pro_strtDate,
										EndDateActual: pro_endDate,
										StartDate: nlapiDateToString(startDate, 'date'),
										EndDate: nlapiDateToString(ENdDate, 'date'),
										Practice: practice,
										project_value: project_value,
										last_practice: 0
									});
							}
						}
						else
						{
							projectList.push({
								proj_id: pro_id,
								proj_name: project_name,
								pro_region: pro_region,
								pro_practice: pro_practice,
								pr_practice_id: practice_id,
								pro_cust: pro_cust,
								resource: '',
								resource_prac: '',
								StartDateActual: pro_strtDate,
								EndDateActual: pro_endDate,
								StartDate: nlapiDateToString(startDate, 'date'),
								EndDate: nlapiDateToString(ENdDate, 'date'),
								Practice: practice,
								project_value: project_value,
								last_practice: 0
							});
						}
					}
				}
				
			}
		}

		for (var i = 0; i < projectList.length; i++) {
			var project = projectList[i];
			
			var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(project.StartDate),
			        nlapiStringToDate(project.EndDate));

			for (var j = 0; j < monthBreakUp.length; j++) {
				var months = monthBreakUp[j];
				try {
					var rec = nlapiCreateRecord(
					        'customrecord_practice_revenue_empwise', {
						        recordmode : 'dynamic'
					        });
					rec.setFieldValue('custrecord_project_name_revenue', project.proj_id);
					rec.setFieldValue('custrecord_strt_date_revenue',months.Start);
					//rec.setFieldValue('custrecord_region_revenue',project.pro_region);
					rec.setFieldValue('custrecord_practice_revenue',project.pro_practice);
					rec.setFieldValue('custrecord_cust_revenue',project.pro_cust);
					rec.setFieldValue('custrecord_resource_revenue',project.resource);
					rec.setFieldValue('custrecord_emp_practice_revenue',project.resource_prac);
					rec.setFieldValue('custrecord_fb_project','T');
					rec.setFieldValue('custrecord_pro_strt_date',project.StartDateActual);
					rec.setFieldValue('custrecord_pro_end_date',project.EndDateActual);
					rec.setFieldValue('custrecord_practice_id',project.pr_practice_id);
					rec.setFieldValue('custrecord_fb_practice',project.Practice);
					rec.setFieldValue('custrecord_usd_inr_rate_to_use',usd_inr_rate);
					rec.setFieldValue('custrecord_usd_gbp_rate_to_use',usd_gbp_rate);
					
					var last_practice_found = project.last_practice;
					if(parseInt(last_practice_found) == parseInt(1))
					{
						rec.setFieldValue('custrecord_lst_participating_practice','T');
						rec.setFieldValue('custrecord_fb_project_value',project.project_value);
					}
					
					var recId = nlapiSubmitRecord(rec, true, true);
					nlapiLogExecution('debug', 'record created', recId);
					yieldScript(context);
				} catch (err) {
					nlapiLogExecution('ERROR', 'Failed To Create Break-Up For '
					        + project.proj_id + " for " + months.Start, err);
				}
			}
			nlapiLogExecution('debug', 'pro id:- ', project.proj_id);
		}
		
		var strVar = '';
		strVar += '<html>';
		strVar += '<body>';
		
		strVar += '<p>FP revenue projection report can be downloaded now.</p>';
		strVar += '<a href="https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1012&deploy=1">Click to download</a>';
		
		strVar += '</body>';
		strVar += '</html>';
			
		nlapiSendEmail(442, 'bhavanishankar.t@brillio.com', 'Please download FP revenue report', strVar);
		
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	var dateBreakUp = [];
	var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
	        .getMonth(), 1);

	dateBreakUp.push({
	    Start : nlapiDateToString(new_start_date, 'date'),
	    End : ''
	});

	for (var i = 1;; i++) {
		var new_date = nlapiAddMonths(new_start_date, i);

		if (new_date > d_endDate) {
			break;
		}

		dateBreakUp.push({
		    Start : nlapiDateToString(new_date, 'date'),
		    End : ''
		});
	}

	return dateBreakUp;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
