/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       14 Apr 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	
	// Load the record
	var rec	=	nlapiLoadRecord(recType, recId);
	
	// Get line item count
	var i_item_count = rec.getLineItemCount('item');
	
	// Check if practice or vertical is blank
	var is_to_be_updated	=	false;
	
	for(var i = 1; i <= i_item_count; i++)
		{
			var i_practice	=	rec.getLineItemValue('item', 'department', i);
			var i_vertical	=	rec.getLineItemValue('item', 'class', i);
			
			if(i_practice == null || i_vertical == null)
			{
				is_to_be_updated	=	true;
			}
		}
	if(is_to_be_updated)
		{
			var i_project = rec.getFieldValue('job');

			var a_project_values = nlapiLookupField('job', i_project, ['custentity_practice', 'custentity_vertical', 'jobbillingtype']);
			
			// Update for FP projects
			if(a_project_values.jobbillingtype == 'FBM')
			{
				for(var i = 1; i <= i_item_count; i++)
				{
					var i_practice	=	rec.getLineItemValue('item', 'department', i);
					var i_vertical	=	rec.getLineItemValue('item', 'class', i);
				
					if(i_practice == null)
					{
						rec.setLineItemValue('item', 'department', i, a_project_values.custentity_practice);
					}
				
					if(i_vertical == null)
					{
						rec.setLineItemValue('item', 'class', i, a_project_values.custentity_vertical);
					}
				}

				try
				{
					nlapiSubmitRecord(rec);
				}
				catch(e)
				{
					nlapiLogExecution('ERROR', 'Error: ', e.message);
				}
			}	
		}
}
