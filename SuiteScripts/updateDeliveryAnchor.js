function updateDeliveryAnchor() {
	try {
		var context = nlapiGetContext();
		var i_practice = context.getSetting('SCRIPT', 'custscript_practice_internal_id');
		var s_practice = nlapiLookupField("department", i_practice, "name");
		nlapiLogExecution("ERROR", "i_practice", i_practice);
		var i_customer = context.getSetting('SCRIPT', 'custscript_customer_internal_id')
		nlapiLogExecution("ERROR", "i_customer : ", i_customer);
		var searchResult = getSFDCOppRecords(i_practice, i_customer, s_practice);
		if (searchResult) {
			for (var i = 0; i < searchResult.length; i++) {
				CheckMetering();
				var oppRecId = searchResult[i].getValue("internalid", null, "GROUP");
				var customrecord_fulfillment_plan_sfdcSearch = nlapiSearchRecord("customrecord_fulfillment_plan_sfdc", null,
					[
						["custrecord_fulfill_plan_opp_id", "anyof", oppRecId]
					],
					[]);
				if (customrecord_fulfillment_plan_sfdcSearch) {
					var recId = customrecord_fulfillment_plan_sfdcSearch[0].getId();
					var fulfillmentRec = nlapiLoadRecord("customrecord_fulfillment_plan_sfdc", recId);
					var fulfillmentId = nlapiSubmitRecord(fulfillmentRec);
					nlapiLogExecution("DEBUG", "fulfillment plan record Id : ", fulfillmentId);
				}
			}
		}
	} catch (e) {
		nlapiLogExecution("DEBUG", "Error in script : ", e);
	}
}

function _logValidation(value) {
	if (value != null && value != 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}

function CheckMetering() {
	var CTX = nlapiGetContext();
	var START_TIME = new Date().getTime();
	//	want to try to only check metering every 15 seconds
	var remainingUsage = CTX.getRemainingUsage();
	var currentTime = new Date().getTime();
	var timeDifference = currentTime - START_TIME;
	//	changing to 15 minutes, should cause little if any impact, but willmake runaway scripts faster to kill
	if (remainingUsage < 800 || timeDifference > 900000) {
		START_TIME = new Date().getTime();
		var status = nlapiYieldScript();
		nlapiLogExecution('AUDIT', 'STATUS = ', JSON.stringify(status));
	}
}

function getSFDCOppRecords(i_practice, i_customer, s_practice) {
	var customrecord_sfdc_opportunity_recordSearch = nlapiSearchRecord("customrecord_sfdc_opportunity_record", null,
		[
			["custrecord_stage_sfdc", "noneof", "8", "9"], "AND",
			[
				["custrecord_practice_internal_id_sfdc", "anyof", i_practice], "OR", ["custrecord_fulfill_plan_opp_id.custrecord_fulfill_plan_practice", "is", s_practice]
			], "AND",
			["custrecord_customer_internal_id_sfdc", "anyof", i_customer]
		],
		[
			new nlobjSearchColumn("internalid", null, "GROUP")
		]);
	return customrecord_sfdc_opportunity_recordSearch;
}