// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
/*
	Date		: 24/11/2021
	Author		: Raghav Gupta
	Remarks		: Suitelet to generate Invoice PDF for all layout types.
	Script Name	: SUT_Invoice_PDF.js
	Script Type	: Suitelet Script (1.0)

	Script Modification Log:s
	-- Date --			-- Modified By --				--Requested By--				-- Description --

	Function used and their Descriptions:

	suiteletFunction FUNCTION		: Main function to generate the PDF.
	_logValidation FUNCTION			: Function to check null, empty or undefined values.
	get_customer_details FUNCTION	: Function to get customer Details by loading Customer Record.
	formatAndReplaceSpacesofMessage : Function to replace ' /\n/g ' with ' "<br/>" '.
	get_address FUNCTION			: Function to get address of subsidary by loading Subsidary Record.
	get_address_rem FUNCTION		: Function to get address of subsidary by loading Subsidary Record.
	toWordsFunc	FUNCTION			: Function to convert currency to currency sign and calls toWords function.
	toWords	FUNCTION				: Function to convert amount in numbers to words.
	formatDollar FUNCTION			: Function to add commas in amount.
	checkDateFormat	FUNCTION		: Function to get Date format.
	get_date FUNCTION				: Function to get today's date and change it in appropriate format based on date format.
	get_start_of_week FUNCTION		: Function to get start date of week.
	get_end_of_week FUNCTION		: Function to get end date of week.
	removearrayduplicate FUNCTION	: Function to remove duplicate values from the array.
	escape_special_chars FUNCTION	: Function to remove special character from string.
	get_dateFormat	FUNCTION 		: Function to change date format for UK FP layout.
*/
// END SCRIPT DESCRIPTION BLOCK ==================================

//BEGIN suiteletFunction
function suiteletFunction(request, response) {
	try {
		nlapiLogExecution('debug', 'Script Started');
		var strVar = "";
		var i_total = 0;
		var s_currency_symbol = '';
		var i_cnt = 0;
		var pagecount = 0;
		var linenum = '17';
		var a_employee_array = new Array();
		var a_employee_name_array = new Array();
		var flag_name = '';

		var i_recordID = request.getParameter('i_recordID');
		nlapiLogExecution('DEBUG', 'Suitelet Function', 'Record ID: ' + i_recordID);

		if (_logValidation(i_recordID)) {
			var o_recordOBJ = nlapiLoadRecord('invoice', i_recordID);
			if (_logValidation(o_recordOBJ)) {
				var i_customer = o_recordOBJ.getFieldValue('entity');
				var i_layout_type = o_recordOBJ.getFieldValue('custbody_layout_type');
				nlapiLogExecution('DEBUG', 'Suitelet Function', 'Layout_Type ID: ' + i_layout_type);
				var s_layout_type = InvoiceConstant[i_layout_type]["type"];
				var s_layout_tax_details = InvoiceConstant[i_layout_type]["Tax Details"];
				var s_layout_name = InvoiceConstant[i_layout_type]["layoutname"];
				var i_tran_date = o_recordOBJ.getFieldValue('trandate');
				var i_tran_id = o_recordOBJ.getFieldValue('tranid');
				if (!_logValidation(i_tran_id)) {
					i_tran_id = '';
				}
				var i_bill_address = nlapiEscapeXML(o_recordOBJ.getFieldValue('billaddress'));
				if (!_logValidation(i_bill_address)) {
					i_bill_address = '';
				}
				var i_PO_ID = o_recordOBJ.getFieldValue('otherrefnum');
				if (!_logValidation(i_PO_ID)) {
					i_PO_ID = '';
				}
				var s_memo = o_recordOBJ.getFieldValue('memo');
				if (!_logValidation(s_memo)) {
					s_memo = '';
				}
				var i_terms = o_recordOBJ.getFieldText('terms');
				if (!_logValidation(i_terms)) {
					i_terms = '';
				}
				var i_bill_to = o_recordOBJ.getFieldValue('custbody_billto');
				if (!_logValidation(i_bill_to)) {
					i_bill_to = '';
				}
				var i_bill_from = o_recordOBJ.getFieldValue('custbody_billfrom');
				if (!_logValidation(i_bill_from)) {
					i_bill_from = '';
				}

				i_bill_to = nlapiStringToDate(i_bill_to);
				i_bill_to = get_date(i_bill_to);
				i_bill_from = nlapiStringToDate(i_bill_from);
				i_bill_from = get_date(i_bill_from);

				var i_billable_time_count = o_recordOBJ.getLineItemCount('time');
				var i_expense_billable_item_count = o_recordOBJ.getLineItemCount('expcost');
				var discount_count = o_recordOBJ.getLineItemCount('item');
				var discount = o_recordOBJ.getLineItemValue('item', 'description', 1);
				var discount_amt = o_recordOBJ.getLineItemValue('item', 'amount', 1);
				var i_discount = o_recordOBJ.getFieldValue('discounttotal');
				if (!_logValidation(i_discount)) {
					i_discount = 0;
				}
				var i_tax = o_recordOBJ.getFieldValue('taxtotal');
				if (!_logValidation(i_tax)) {
					i_tax = 0;
				}
				var i_discount_percent = parseFloat(i_discount) / 100;
				i_discount_percent = parseFloat(i_discount_percent).toFixed(2);
				var i_tax_percent = parseFloat(i_tax) / 100;
				i_tax_percent = parseFloat(i_tax_percent).toFixed(2);

				var i_addr1 = '';
				var i_addr2 = '';
				var i_city = '';
				var i_zip = '';
				var i_country = '';
				var i_state = '';

				var a_customer_array = get_customer_details(i_customer);
				var a_split_array = new Array();

				if (_logValidation(a_customer_array)) {
					a_split_array = a_customer_array[0].split('######');
					i_addr1 = a_split_array[5];
					i_addr2 = a_split_array[6];
					i_city = a_split_array[7];
					i_zip = a_split_array[8];
					i_country = a_split_array[9];
					i_state = a_split_array[10];
				}

				var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary');
				var i_address = get_address(i_subsidiary);
				i_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address));
				var i_address_rem = nlapiEscapeXML(get_address_rem(i_subsidiary));

				if (i_addr1.toString() == 'null' || i_addr1.toString() == null || i_addr1 == null || i_addr1 == '' || i_addr1 == undefined) {
					i_addr1 = '';
				}
				if (i_addr2.toString() == 'null' || i_addr2.toString() == null || i_addr2 == null || i_addr2 == '' || i_addr2 == undefined) {
					i_addr2 = '';
				}
				if (i_city.toString() == 'null' || i_city.toString() == null || i_city == null || i_city == '' || i_city == undefined) {
					i_city = '';
				}
				if (i_zip.toString() == 'null' || i_zip.toString() == null || i_zip == null || i_zip == '' || i_zip == undefined) {
					i_zip = '';
				}
				if (i_country.toString() == 'null' || i_country.toString() == null || i_country == null || i_country == '' || i_country == undefined) {
					i_country = '';
				}
				if (i_state.toString() == 'null' || i_state.toString() == null || i_state == null || i_state == '' || i_state == undefined) {
					i_state = '';
				}

				var i_address_u = i_addr1 + '\n' + i_addr2 + '\n' + i_city + ',' + i_state + '\n' + i_zip + '\n' + i_country;
				i_address_u = formatAndReplaceSpacesofMessage(nlapiEscapeXML(i_address_u));

				var i_subsidiary = o_recordOBJ.getFieldValue('subsidiary');

				var i_subsidiary_text = o_recordOBJ.getFieldText('subsidiary');
				if (i_layout_type == 17) {
					i_subsidiary_text = "Brillio UK Ltd";
				}

				// nlapiLogExecution('DEBUG', 'Suitelet Function', 'Subsidiary --> ' + i_subsidiary_text);

				if (nlapiStringToDate(i_tran_date) >= nlapiStringToDate('11/1/2017')) {
					var s_address_rem = "";
					for (index = 1; index < InvoiceConstant[i_layout_type]["address"].length; index++) {
						s_address_rem += "\n";
						s_address_rem += InvoiceConstant[i_layout_type]["address"][index];
					}

					var i_address_rem = InvoiceConstant[i_layout_type]["address"][0];
					i_address_rem = i_address_rem + s_address_rem;
					i_address_rem = nlapiEscapeXML(i_address_rem);
					var i_adress_rem_ = s_address_rem;
					var s_header_address = nlapiEscapeXML(i_adress_rem_);
				}
				else {
					var i_address_rem = nlapiEscapeXML(get_address_rem(i_subsidiary));
					var i_address = get_address(i_subsidiary);
					var s_header_address = header_adress(i_address, i_subsidiary_text);
				}
				i_address = formatAndReplaceSpacesofMessage(nlapiEscapeXML(s_header_address));
				var i_currency = o_recordOBJ.getFieldText('currency');
				var i_project_name;
				var i_project = o_recordOBJ.getFieldValue('job');

				if (_logValidation(i_project)) {
					var o_projectOBJ = nlapiLoadRecord('job', i_project);
					if (_logValidation(o_projectOBJ)) {
						i_project_name = o_projectOBJ.getFieldValue('companyname');
					}
				}
				if (!_logValidation(i_project_name)) {
					i_project_name = '';
				}

				d_currency_symbol = {
					'USD': '$',
					'INR': 'Rs.',
					'GBP': '£',
					'SGD': 'S$',
					'Peso': 'P',
					'EUR': '€',
					'CAD': 'c$'
				}

				if (!_logValidation(i_currency)) {
					i_currency = '';
				}
				else {
					s_currency_symbol = d_currency_symbol[i_currency];
				}

				var i_item_count = o_recordOBJ.getLineItemCount('item');

				var image_URL = constant.Images.CompanyLogo;
				image_URL = nlapiEscapeXML(image_URL);
				i_tran_date = nlapiStringToDate(i_tran_date);
				var d_inv_date = get_date(i_tran_date);

				{//Begin 1st Table from invoice heading till the end of address
					strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
					{//Begin 1st row keyword Invoice
						strVar += "<tr>";
						if (s_layout_name == "UK FP" || s_layout_name == "UK T&M") {
							strVar += "<td border-bottom=\"0.3\" font-family=\"Helvetica\" style=\"color:#000000; font-size:35\" font-size=\"25\" width=\"100%\" colspan=\"2\"><b>Invoice<\/b></td>";
						}
						else {
							strVar += "<td border-bottom=\"0.3\" font-family=\"Helvetica\" style=\"color:#00CC00; font-size:35\" font-size=\"25\" width=\"100%\" colspan=\"2\"><b>Invoice<\/b></td>";
						}
						strVar += "</tr>";
					}//End 1st row keyword Invoice 

					{//End 2nd row line break
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
						strVar += "</tr>";
					}//End 2nd row line break

					{//Begin 3rd row Invoice no. and Date.
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Invoice No. :<\/b>&nbsp;" + i_tran_id + "</td>";
						if (s_layout_name == "UK FP") {
							strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Date :<\/b>&nbsp;" + get_dateFormat(d_inv_date) + "</td>";
						}
						else {
							strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\"><b>Date :<\/b>&nbsp;" + d_inv_date + "</td>";
						}
						strVar += "</tr>";
					}//End 3rd row Invoice no. and Date.

					{//Begin 4th row line break
						strVar += "<tr>"
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
						strVar += "</tr>";
					}//End 4th row line break

					{//Begin 5th row address table
						strVar += "<tr>";
						strVar += "<td height=\"70\" border-top=\"0.3\" border-left=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"1\">";
						strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
						strVar += "<tr>";
						strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><b>Bill To :<\/b>&nbsp;</td>";
						strVar += "</tr>";

						strVar += "<tr>";
						strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><PRE font-family=\"Helvetica\" font-size=\"9\">" + i_bill_address + "</PRE></td>";
						strVar += "</tr>";
						strVar += "</table>";
						strVar += "</td>";

						strVar += "<td height=\"70\" border-top=\"0.3\" border-left=\"0.3\" border-bottom=\"0.3\" border-right=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"1\">";
						strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
						strVar += "<tr>";
						strVar += "<td border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"><b>Remittance Details :<\/b>&nbsp;</td>";
						strVar += "</tr>";

						if (_logValidation(i_address_rem)) {
							strVar += "<tr>";
							strVar += "<td border-top=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" ><PRE font-family=\"Helvetica\" font-size=\"9\">" + i_address_rem + "</PRE></td>";
							strVar += "</tr>";
						}
						strVar += "</table>";
						strVar += "</td>";
						strVar += "</tr>";
					}//End 5th row address table		
					strVar += "</table>";
				}//End 1st Table from invoice heading till the end of address

				{//Begin 2nd Table from project names till notes
					strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
					{//Begin 1st row line break
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
						strVar += "</tr>";
					}//End 1st row

					{//Begin 2nd row Project Name
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Project Name :<\/b>&nbsp;" + nlapiEscapeXML(i_project_name) + "</td>";
						strVar += "</tr>";
					}//End 2nd row Project Name

					{//Begin 3rd PO Number
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>PO Number :<\/b>&nbsp;" + i_PO_ID + "</td>";
						strVar += "</tr>";
					}//End 3rd PO Number

					{//Begin 4th row line break
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
						strVar += "</tr>";
					}//End 4th row line break

					{//Begin 5th row Terms of Payment
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Terms of Payment :<\/b>&nbsp;" + nlapiEscapeXML(i_terms) + "</td>";
						strVar += "</tr>";
					}//End 5th row Terms of Payment

					{//Begin 6th row Billing Period
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Billing Period :<\/b>&nbsp;Beginning&nbsp;" + i_bill_from + "&nbsp;thru&nbsp;" + i_bill_to + "</td>";
						strVar += "</tr>";
					}//End 6th row Billing Period

					{//Begin 7th row line break
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
						strVar += "</tr>";
					}//End 7th row line break

					if (s_layout_name != "UK FP") {
						{//Begin 8th row notes
							strVar += "<tr>";
							strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"><b>Notes:<\/b>&nbsp;" + s_memo + "</td>";
							strVar += "</tr>";
						}//End 8th row notes
					}

					{//Begin 9th line break
						strVar += "<tr>";
						strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\">&nbsp;</td>";
						strVar += "</tr>";
					}//End 9th line break
					strVar += "</table>";
				}//End 2nd Table from project names till notes

				//Start of FP layout
				if (s_layout_type == "FP") {
					strVar += "<table border=\"0\" width=\"100%\">";
					strVar += "<tr>";
					strVar += "<td border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b><\/b></td>";
					strVar += "<td border-top=\"0.3\" border-bottom=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b><\/b></td>";
					strVar += "</tr>";
					strVar += "<tr>";
					if (s_layout_name == "UK FP") {
						strVar += "<td border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b>Project Details / Services Details<\/b></td>";
						strVar += "<td border-top=\"0\" border-bottom=\"0.3\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b>Base Amount(" + s_currency_symbol + ")<\/b></td>";
					}
					else {
						strVar += "<td border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b>Description<\/b></td>";
						strVar += "<td border-top=\"0\" border-bottom=\"0.3\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b>Amount(" + s_currency_symbol + ")<\/b></td>";
					}
					strVar += "</tr>";

					if (_logValidation(i_item_count)) {
						for (var kt = 1; kt <= i_item_count; kt++) {
							var i_item = o_recordOBJ.getLineItemValue('item', 'item', kt);
							if (i_item != 2219) {
								var i_description = o_recordOBJ.getLineItemValue('item', 'description', kt);
								var i_amount = o_recordOBJ.getLineItemValue('item', 'amount', kt);
								if (!_logValidation(i_description)) {
									i_description = '';
								}
								if (!_logValidation(i_amount)) {
									i_amount = 0;
								}


								if (s_layout_name == "UK FP") {
									if (i_currency == 'GBP') {
										var i_vat = o_recordOBJ.getLineItemValue('item', 'tax1amt', kt);
										i_total = parseFloat(i_total) + parseFloat(i_amount) + parseFloat(i_vat);
									}
									else {
										i_total = parseFloat(i_total) + parseFloat(i_amount);
									}
								}
								else {
									i_total = parseFloat(i_total) + parseFloat(i_amount);
								}

								strVar += "<tr>";
								strVar += "<td align=\"left\" border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
								strVar += "<td align=\"right\" border-left=\"0\" border-right=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
								strVar += "</tr>";

								strVar += "<tr>";
								strVar += "<td align=\"left\" border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">" + nlapiEscapeXML(i_description) + "</td>";
								strVar += "<td align=\"right\" border-left=\"0\" border-right=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">" + nlapiEscapeXML(i_amount) + "</td>";
								strVar += "</tr>";

								if (s_layout_name == "UK FP") {
									if (i_currency == 'GBP') {
										strVar += "<tr>";
										strVar += "</tr>";
										strVar += "<tr>";
										strVar += "<td colspan=\"2\" border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>VAT&nbps;&nbsp;@20%:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/b>" + "&nbsp;" + nlapiEscapeXML(i_vat) + "</td>";
										strVar += "</tr>";
									}
								}

								strVar += "<tr>";
								strVar += "<td align=\"left\" border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
								strVar += "<td align=\"right\" border-left=\"0\" border-right=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
								strVar += "</tr>";

								strVar += "<tr>";
								strVar += "<td align=\"left\" border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
								strVar += "<td align=\"right\" border-left=\"0\" border-right=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
								strVar += "</tr>";
							}
						}
					}
					strVar += "<tr>";
					if (s_layout_name == "UK FP") {
						strVar += "<td colspan=\"2\" border-right=\"0\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>Total&nbps;&nbsp;Amount:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + s_currency_symbol + "&nbsp;" + formatDollar(parseFloat(i_total).toFixed(2)) + "<\/b></td>";
					}
					else {
						strVar += "<td colspan=\"2\" border-right=\"0\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + s_currency_symbol + "&nbsp;" + formatDollar(parseFloat(i_total).toFixed(2)) + "<\/b></td>";
					}

					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>" + i_currency + "</td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Amount In Words&nbsp;:&nbsp;<\/b>" + toWordsFunc(parseFloat(i_total).toFixed(2), i_currency) + "</td>";
					strVar += "</tr>";

					strVar += "</table>";
				}//End of FP layout

				//Start of T&M Group layout
				else if (s_layout_type == "T&M Group") {
					strVar += "<table border=\"0\" align=\"center\" width=\"100%\">";
					strVar += "<tr>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount(" + s_currency_symbol + ")<\/b></td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
					strVar += "</tr>";
					if (_logValidation(i_billable_time_count)) {
						for (var i = 1; i <= i_billable_time_count; i++) {
							var i_date = o_recordOBJ.getLineItemValue('time', 'billeddate', i);
							var i_employee = o_recordOBJ.getLineItemValue('time', 'employeedisp', i);
							var i_project = o_recordOBJ.getLineItemValue('time', 'job', i);
							var i_item = o_recordOBJ.getLineItemValue('time', 'item', i);
							var i_rate = o_recordOBJ.getLineItemValue('time', 'rate', i);
							var i_hours = o_recordOBJ.getLineItemValue('time', 'quantity', i);
							var i_marked = o_recordOBJ.getLineItemValue('time', 'apply', i);
							if (i_marked == 'T') {
								var d_start_of_week = get_start_of_week(i_date);
								var d_end_of_week = get_end_of_week(i_date);
								var d_SOW_date = nlapiStringToDate(d_start_of_week);
								var d_EOW_date = nlapiStringToDate(d_end_of_week);
								d_SOW_date = get_date(d_SOW_date);
								d_EOW_date = get_date(d_EOW_date);
								if (i_item == 2222 || i_item == 2425 || i_item == 2221) {
									a_employee_array[i_cnt++] = i_employee + '&&&' + d_EOW_date + '&&&' + d_SOW_date + '&&&' + i_rate;
									a_employee_name_array.push(i_employee);
								}
							}
						}
					}
					a_employee_array = removearrayduplicate(a_employee_array);
					a_employee_array = a_employee_array.sort();
					a_employee_name_array = removearrayduplicate(a_employee_name_array);
					a_employee_name_array = a_employee_name_array.sort();
					var i_employee_arr_n;
					var vb = 0;
					for (var k = 0; k < a_employee_array.length; k++) {
						var i_ST_rate = 0;
						var i_OT_rate = 0;
						var i_ST_units = 0;
						var i_OT_units = 0;
						var i_hours = 0;
						var i_minutes = 0;
						var i_hours_ST = 0;
						var i_minutes_ST = 0;
						var i_hours_OT = 0;
						var i_minutes_OT = 0;
						var flag = 0;

						a_split_array = a_employee_array[k].split('&&&');
						var i_employee_arr = a_split_array[0];
						var i_end_of_week_arr = a_split_array[1];
						var i_start_of_week_arr = a_split_array[2];
						var i_rate_emp = a_split_array[3];
						for (var lt = 1; lt <= i_billable_time_count; lt++) {
							var i_date_new = o_recordOBJ.getLineItemValue('time', 'billeddate', lt);
							var i_employee_new = o_recordOBJ.getLineItemValue('time', 'employeedisp', lt);
							var i_project_new = o_recordOBJ.getLineItemValue('time', 'job', lt);
							var i_item_new = o_recordOBJ.getLineItemValue('time', 'item', lt);
							var i_rate_new = o_recordOBJ.getLineItemValue('time', 'rate', lt);
							var i_hours_new = o_recordOBJ.getLineItemValue('time', 'quantity', lt);
							var i_marked = o_recordOBJ.getLineItemValue('time', 'apply', lt);
							if (i_marked == 'T') {
								if ((i_employee_arr == i_employee_new)) {
									var d_start_of_week_new = get_start_of_week(i_date_new);
									var d_end_of_week_new = get_end_of_week(i_date_new);
									var d_SOW_date_B = nlapiStringToDate(d_start_of_week_new);
									var d_EOW_date_B = nlapiStringToDate(d_end_of_week_new);
									d_SOW_date_B = get_date(d_SOW_date_B);
									d_EOW_date_B = get_date(d_EOW_date_B);
									if ((d_SOW_date_B == i_start_of_week_arr) && (d_EOW_date_B == i_end_of_week_arr) && (i_rate_emp == i_rate_new)) {
										if (flag != i_employee_arr) {
											i_employee_arr_n = i_employee_arr;
										}
										else {
											i_employee_arr_n = '';
										}
										var a_split_time_array = new Array();
										a_split_time_array = i_hours_new.split(':');
										var hours = a_split_time_array[0];
										var minutes = a_split_time_array[1];

										if (minutes == '0.00' || minutes == '0' || minutes == 0) {
										}
										else {
											minutes = parseFloat(minutes) / 60;
											minutes = parseFloat(minutes).toFixed(2);
										}
										if (i_item_new == 2222 || i_item_new == 2221) {
											i_hours_ST = parseFloat(i_hours_ST) + parseFloat(hours);
											i_hours_ST = parseFloat(i_hours_ST) + parseFloat(minutes);
											i_hours_ST = parseFloat(i_hours_ST).toFixed(2);
											i_ST_rate = parseFloat(i_hours_ST) * parseFloat(i_rate_new);
											i_ST_units = parseFloat(i_rate_new).toFixed(2);
										}

										if (i_item_new == 2425) {
											i_hours_OT = parseFloat(i_hours_OT) + parseFloat(hours);
											i_hours_OT = parseFloat(i_hours_OT) + parseFloat(minutes);
											i_hours_OT = parseFloat(i_hours_OT).toFixed(2);
											i_OT_rate = parseFloat(i_hours_OT);
											i_OT_units = parseFloat(i_rate_new).toFixed(2);
										}
									}
								}
							}
						}
						i_amount = (parseFloat(i_hours_ST) * parseFloat(i_ST_units)) + (parseFloat(i_hours_OT) * parseFloat(i_OT_units));
						i_amount = parseFloat(i_amount);
						i_amount = i_amount.toFixed(2);
						i_total = parseFloat(i_total) + parseFloat(i_amount);
						i_total = parseFloat(i_total);
						i_total = i_total.toFixed(2);
						var s_employee_addr_txt = '';
						s_employee_addr_txt = escape_special_chars(i_employee_arr);
						strVar += "<tr>";
						if (flag == 0 && flag_name != i_employee_arr) {
							if (a_employee_name_array.indexOf(i_employee_arr) != -1) {
								vb++;
								strVar += "<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">" + s_employee_addr_txt + "<\/td>";
								flag = 1;
								flag_name = i_employee_arr;
							}
						}
						else {
							strVar += "<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
						}
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">" + i_end_of_week_arr + "<\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_ST_units) + "<\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_hours_ST) + "<\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_OT_units) + "<\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\">" + parseFloat(i_hours_OT) + "<\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">" + i_amount + "<\/td>";
						strVar += "<\/tr>";
						pagecount++;

						if (pagecount == linenum && a_employee_array.length != '17') {
							pagecount = 0;
							linenum = 40;
							strVar += "<\/table>";
							strVar += "<p style=\"page-break-after: always\"><\/p>";
							strVar += "<table border=\"0\" width=\"100%\">";
							strVar += "<tr>";
							strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Name<\/b></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>Week End<\/b></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Rate<\/b></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>ST Units<\/b></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Rate<\/b></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"9\"><b>OT Units<\/b></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Amount(" + s_currency_symbol + ")<\/b></td>";
							strVar += "</tr>";
							strVar += "<tr>";
							strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hourly)</td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\">(Hours)</td>";
							strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
							strVar += "</tr>";
						}
					}
					if (discount == null || discount == 'null') {
						discount = '';
					}
					if (discount_amt == null || discount_amt == 'null') {
						discount_amt = '';
					}
					for (var dis_ind = 1; dis_ind <= discount_count; dis_ind++) {
						strVar += "<tr>";
						strVar += "<td colspan=\"2\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\">" + o_recordOBJ.getLineItemValue('item', 'description', dis_ind) + "<\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"8\"><\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_hours_OT//i_OT_rate
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" font-family=\"Helvetica\" align=\"center\" font-size=\"8\"><\/td>";// i_OT_units
						strVar += "<td colspan=\"1\" border-bottom=\"0\" border-left=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"8\">" + o_recordOBJ.getLineItemValue('item', 'amount', dis_ind) + "<\/td>";
						strVar += "<\/tr>";
					}
					if (_logValidation(i_discount_percent) && i_discount_percent != 0) {
						strVar += "<tr>";
						strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Less: Discount @" + i_discount_percent + "</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "</tr>";
					}
					if (_logValidation(i_tax_percent) && i_tax_percent != 0) {
						strVar += "<tr>";
						strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\">Add: Sales Tax PA state @" + i_tax_percent + "</td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0\" border-bottom=\"0\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
						strVar += "</tr>";
					}

					i_total = i_total - parseInt(-discount_amt);
					i_total = i_total.toFixed(2);

					strVar += "<tr>";
					strVar += "<td colspan=\"2\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"left\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b> <\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"><b><\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"center\" font-family=\"Helvetica\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total :<\/b></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"></td>";
					strVar += "<td colspan=\"1\" border-left=\"0\" border-right=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" align=\"right\" font-family=\"Helvetica\" font-size=\"9\"><b>" + s_currency_symbol + "&nbsp;" + formatDollar(parseFloat(i_total).toFixed(2)) + "<\/b></td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>" + i_currency + "</td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"8\"><b>Amount In Words&nbsp;:&nbsp;<\/b>" + toWordsFunc(parseFloat(i_total).toFixed(2), i_currency) + "</td>";
					strVar += "</tr>";
					strVar += "</table>";
				}//END of T&M Group layout

				//Start of EXP layout
				else if (s_layout_type == "EXP") {
					strVar += "<table border=\"0\" width=\"100%\">";
					strVar += "<tr>";
					strVar += "<td border-left=\"0\" border-top=\"0.3\" border-bottom=\"0\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b><\/b></td>";
					strVar += "<td border-top=\"0.3\" border-bottom=\"0\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b><\/b></td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"left\" font-size=\"10\"><b>Description<\/b></td>";
					strVar += "<td border-top=\"0\" border-bottom=\"0.3\" border-right=\"0\" font-family=\"Helvetica\" align=\"right\" font-size=\"10\"><b>Amount(" + s_currency_symbol + ")<\/b></td>";
					strVar += "</tr>";

					var a_category_array = new Array();
					if (_logValidation(i_expense_billable_item_count)) {
						for (var et = 1; et <= i_expense_billable_item_count; et++) {
							var i_category = o_recordOBJ.getLineItemValue('expcost', 'memo', et);
							var i_apply = o_recordOBJ.getLineItemValue('expcost', 'apply', et);
							if (i_apply == 'T') {
								a_category_array.push(i_category);
							}
						}
					}
					a_category_array = removearrayduplicate(a_category_array);
					if (_logValidation(a_category_array) && _logValidation(i_expense_billable_item_count)) {
						for (var ct = 0; ct < a_category_array.length; ct++) {
							strVar += "<tr>";
							strVar += "<td align=\"left\" border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"><b>" + nlapiEscapeXML(a_category_array[ct]) + ": <\/b></td>";
							strVar += "<td align=\"right\" border-left=\"0\" border-right=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\"></td>";
							strVar += "</tr>";

							for (var kt = 1; kt <= i_expense_billable_item_count; kt++) {
								var i_description = o_recordOBJ.getLineItemValue('expcost', 'memo', kt);
								var i_amount = o_recordOBJ.getLineItemValue('expcost', 'amount', kt);
								var i_employee = o_recordOBJ.getLineItemValue('expcost', 'employeedisp', kt);
								var i_apply = o_recordOBJ.getLineItemValue('expcost', 'apply', kt);

								if (a_category_array[ct] == i_description) {
									if (i_apply == 'T') {
										if (!_logValidation(i_description)) {
											i_description = '';
										}
										if (!_logValidation(i_amount)) {
											i_amount = 0;
										}
										var s_employee_addr_txt = '';
										s_employee_addr_txt = escape_special_chars(i_employee);
										strVar += "<tr>";
										strVar += "<td align=\"left\" border-left=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">" + s_employee_addr_txt + "</td>";
										strVar += "<td align=\"right\" border-left=\"0\" border-right=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\">" + i_amount + "</td>";
										strVar += "</tr>";
										i_total = parseFloat(i_total) + parseFloat(i_amount);
									}
								}
							}
						}
					}
					strVar += "<tr>";
					strVar += "<td colspan=\"2\" border-right=\"0\" border-left=\"0\" border-top=\"0.3\" border-bottom=\"0.3\" font-family=\"Helvetica\" align=\"right\" font-size=\"9\"><b>Invoice&nbps;&nbsp;Total:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + s_currency_symbol + "&nbsp;" + formatDollar(parseFloat(i_total).toFixed(2)) + "<\/b></td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Billing Currency&nbsp;&nbsp;&nbsp;:&nbsp;<\/b>" + i_currency + "</td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0.3\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Amount In Words&nbsp;:&nbsp;<\/b>" + toWordsFunc(parseFloat(i_total).toFixed(2), i_currency) + "</td>";
					strVar += "</tr>";
					strVar += "</table>";
				}//END of EXP layout

				if (s_layout_tax_details == "Yes") {
					strVar += "<table border=\"0\" width=\"100%\">";

					strVar += "<tr>";
					strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\" align=\"left\"><b>VAT Registration No:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;319 2933 90</b></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"9\" colspan=\"2\"><b>Payment Instruction:		</b></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Bank Details:		</b></td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"100%\" colspan=\"2\"></td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Name of the Account:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSBC UK BANK PLC</td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Currency:</b>&nbps;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;GBP</td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Sort Code:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;40&nbsp;03&nbsp;04</td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Account Number:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;72327252</td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>IBAN Number:</b>&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GB76HBUK40030472327252</td>";
					strVar += "</tr>";

					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Swift ID / BIC:</b> &nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HBUKGB4B</td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\"><b>Bank Name and Address</b>:&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HSBC UK BANK PLC, The Helicon 1</td>";
					strVar += "</tr>";
					strVar += "<tr>";
					strVar += "<td border-right=\"0\" border-left=\"0\" border-top=\"0\" border-bottom=\"0\" font-family=\"Helvetica\" font-size=\"8\" colspan=\"2\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbps;&nbps;&nbps;&nbps;South Place, London, EC2M 2UP.</td>";
					strVar += "</tr>";
					strVar += "</table>";
				}

			}
		}

		{// Begin header and Footer
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
			xml += "<pdf>";
			xml += "<head>";
			xml += "<macrolist>";
			xml += "<macro id=\"myfooter\">";
			xml += "<table width=\"100%\">";
			xml += "<tr>";
			xml += "<td border-top=\"0.3\" font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" align=\"center\">";
			xml += InvoiceConstant[i_layout_type]["address"][0];
			xml += " certifies that this invoice is correct, that payment has not been received and that it is presented with the understanding that <\/td>";
			xml += "<\/tr>";
			xml += "<tr>";
			xml += "<td border-bottom=\"0.3\" font-size=\"8\" font-family=\"Helvetica\" width=\"100%\" align=\"center\">the amount to be paid thereunder can be audited to verify.<\/td>";
			xml += "<\/tr>";
			xml += "<tr width=\"100%\">";
			xml += "<td font-size='10' font-family='Times' align=\"right\" width=\"100%\"> Page <pagenumber size='2'/> of <totalpages size='2'/></td>";
			xml += "</tr>";
			xml += "<\/table>";
			xml += "<\/macro>";
			xml += "<macro id=\"myHead\">";
			xml += "<table border=\"0\" align=\"center\" width=\"100%\">";
			xml += "<tr>";
			xml += "<td font-family=\"Helvetica\" font-size=\"9\" width=\"50%\" colspan=\"2\"><b>" + i_subsidiary_text + "<\/b><br/>" + i_address + "</td>";
			if (image_URL != '' && image_URL != null && image_URL != '' && image_URL != 'undefined' && image_URL != ' ' && image_URL != '&nbsp') {
				xml += " <td width=\"50%\" colspan=\"2\"><img width=\"110\" height=\"45\" align=\"right\" src=\"" + image_URL + "\"><\/img><\/td>";
			}
			xml += "</tr>";
			xml += "</table>";
			xml += "<\/macro>";
			xml += "<\/macrolist>";
			xml += "<\/head>";
		}// End header and Footer

		xml += "<body margin-top=\"0pt\" header=\"myHead\" header-height=\"30mm\" footer=\"myfooter\" footer-height=\"2em\">";
		xml += strVar;
		xml += "</body>\n</pdf>";


		var file = nlapiXMLToPDF(xml);
		var filename = InvoiceConstant[i_layout_type]["layoutname"] + '.pdf';
		var fileObj = nlapiCreateFile(filename, 'PDF', file.getValue());
		fileObj.setFolder(62);
		response.setContentType('PDF', filename, 'inline');
		response.write(file.getValue());

		nlapiLogExecution('DEBUG', 'Script Ended');
	}
	catch (error) {
		nlapiLogExecution('DEBUG', 'ERROR', error);
	}

}//END suiteletFunction

//BEGIN _logValidation
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}
//END _logValidation

//BEGIN get_customer_details
function get_customer_details(i_customer) {
	var i_address;
	var i_attention;
	var i_service_tax_reg_no;
	var i_CIN_No;
	var i_phone = '';
	var a_return_array = new Array();
	if (_logValidation(i_customer)) {
		var o_customerOBJ = nlapiLoadRecord('customer', i_customer)

		if (_logValidation(o_customerOBJ)) {
			i_address = o_customerOBJ.getFieldValue('defaultaddress');
			i_service_tax_reg_no = o_customerOBJ.getFieldValue('vatregnumber');
			i_CIN_No = o_customerOBJ.getFieldValue('custentity_cinnumber');
			i_phone = o_customerOBJ.getFieldValue('phone');
			var i_addr_count = o_customerOBJ.getLineItemCount('addressbook');
			if (_logValidation(i_addr_count)) {
				for (var r = 1; r <= i_addr_count; r++) {
					var i_default_billing = o_customerOBJ.getLineItemValue('addressbook', 'defaultbilling', r);
					if (i_default_billing == 'T') {
						var i_attention = o_customerOBJ.getLineItemValue('addressbook', 'attention', r);
						var i_addr1 = o_customerOBJ.getLineItemValue('addressbook', 'addr1', r);
						var i_addr2 = o_customerOBJ.getLineItemValue('addressbook', 'addr2', r);
						var i_city = o_customerOBJ.getLineItemValue('addressbook', 'city', r);
						var i_zip = o_customerOBJ.getLineItemValue('addressbook', 'zip', r);
						var i_country = o_customerOBJ.getLineItemText('addressbook', 'country', r);
						var i_state = o_customerOBJ.getLineItemValue('addressbook', 'displaystate', r);
						break;
					}
				}
			}
			a_return_array[0] = i_address + '######' + i_attention + '######' + i_service_tax_reg_no + '######' + i_CIN_No + '######' + i_phone + '######' + i_addr1 + '######' + i_addr2 + '######' + i_city + '######' + i_zip + '######' + i_country + '######' + i_state;
		}
	}
	return a_return_array;
}
//END get_customer_details

//BEGIN formatAndReplaceSpacesofMessage
function formatAndReplaceSpacesofMessage(messgaeToBeSendPara) {
	if (messgaeToBeSendPara != null && messgaeToBeSendPara != '' && messgaeToBeSendPara != undefined) {
		messgaeToBeSendPara = messgaeToBeSendPara.toString();
		messgaeToBeSendPara = messgaeToBeSendPara.replace(/\n/g, "<br/>");
		return messgaeToBeSendPara;
	}
}
//END formatAndReplaceSpacesofMessage

//BEGIN get_address
function get_address(i_subsidiary) {
	var i_address;
	if (_logValidation(i_subsidiary)) {
		var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary);
		if (_logValidation(o_subsidiaryOBJ)) {
			i_address = o_subsidiaryOBJ.getFieldValue('addrtext');
		}
	}
	return i_address;
}
//END get_address

//BEGIN get_address_rem
function get_address_rem(i_subsidiary) {
	if (_logValidation(i_subsidiary)) {
		var i_address_rem;
		var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary', i_subsidiary);
		if (_logValidation(o_subsidiaryOBJ)) {
			i_address_rem = o_subsidiaryOBJ.getFieldValue('mainaddress_text');
		}
	}
	return i_address_rem;
}
//END get_address_rem

//BEGIN toWordsFunc
function toWordsFunc(s, i_currency) {
	var str = '';
	var d_currency = {
		'USD': 'Dollar',
		'INR': 'Rs.',
		'GBP': '£',
		'SGD': 'S$',
		'Peso': 'P',
		'EUR': '€',
		'CAD': 'c$'
	}
	str = d_currency[i_currency] + ' ';
	return str + toWords(s);
}
//END toWordsFunc

//BEGIN toWords
function toWords(s) {
	var th = ['', 'thousand', 'million', 'billion', 'trillion'];
	var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
	var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
	var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
	s = s.toString();
	s = s.replace(/[\, ]/g, '');
	if (s != parseFloat(s)) return 'not a number';
	var x = s.indexOf('.');
	if (x == -1) x = s.length;
	if (x > 15) return 'too big';
	var n = s.split('');
	var str = '';
	var sk = 0;
	for (var i = 0; i < x; i++) {
		if ((x - i) % 3 == 2) {
			if (n[i] == '1') {
				str += tn[Number(n[i + 1])] + ' ';
				i++;
				sk = 1;
			}
			else if (n[i] != 0) {
				str += tw[n[i] - 2] + ' ';
				sk = 1;
			}
		}
		else if (n[i] != 0) {
			str += dg[n[i]] + ' ';
			if ((x - i) % 3 == 0) str += 'hundred ';
			sk = 1;
		}

		if ((x - i) % 3 == 1) {
			if (sk) str += th[(x - i - 1) / 3] + ' ';
			sk = 0;
		}
	}
	if (x != s.length) {
		var y = s.length;
		str += 'point ';
		for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
	}
	str = str.charAt(0).toUpperCase() + str.slice(1);
	return str.replace(/\s+/g, ' ');
}
//END toWords

//BEGIN formatDollar
function formatDollar(somenum) {
	var split_arr = new Array();
	var i_no_before_comma;
	if (somenum != null && somenum != '' && somenum != undefined) {
		split_arr = somenum.toString().split('.');
		i_no_before_comma = split_arr[0];
		i_no_before_comma = Math.abs(i_no_before_comma);

		if (i_no_before_comma.toString().length <= 3) {
			return somenum;
		}
		else {
			var p = somenum.toString().split(".");

			if (p[1] != null && p[1] != '' && p[1] != undefined) {
				p[1] = p[1];
			}
			else {
				p[1] = '00';
			}
			return p[0].split("").reverse().reduce(function (acc, somenum, i, orig) {
				return somenum + (i && !(i % 3) ? "," : "") + acc;
			}, "") + "." + p[1];
		}
	}
}
//END formatDollar

//BEGIN checkDateFormat
function checkDateFormat() {
	var context = nlapiGetContext();
	var dateFormatPref = context.getPreference('dateformat');
	return dateFormatPref;
}
//END checkDateFormat

//BEGIN get_date
function get_date(date1) {
	var date_format = checkDateFormat();
	var today;
	var offsetIST = 5.5;
	//To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime() + (date1.getTimezoneOffset() * 60000));
	//Then convert the UTC date to the required time zone offset like back to 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	var day = date1.getDate();
	var month = date1.getMonth() + 1;
	var year = date1.getFullYear();

	var a_month = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09']
	if (month <= 9) {
		month = a_month[month];
	}

	var a_day = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09']
	if (day <= 9) {
		day = a_day[day];
	}

	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {
		today = month + '/' + day + '/' + year;
	}
	return today;
}
//END get_date

//BEGIN header_adress
function header_adress(text, subsidiary) {
	var text_new = '';
	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			text_new += text[y];
			if (text_new == subsidiary) {
				text_new = '';
			}
		}
	}
	return text_new;
}
//END header_adress

//BEGIN get_start_of_week
function get_start_of_week(date) {
	var now = nlapiStringToDate(date);
	now.setHours(0, 0, 0, 0);
	var monday = new Date(now);
	monday.setDate(monday.getDate() - monday.getDay() + 1);
	monday = nlapiDateToString(monday);
	var sunday = new Date(now);
	sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
	sunday = nlapiDateToString(sunday);
	return monday;
}
//END get_start_of_week

//BEGIN get_end_of_week
function get_end_of_week(date) {
	var now = nlapiStringToDate(date);
	now.setHours(0, 0, 0, 0);
	var monday = new Date(now);
	monday.setDate(monday.getDate() - monday.getDay() + 1);
	monday = nlapiDateToString(monday);
	var sunday = new Date(now);
	sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
	sunday = nlapiDateToString(sunday);
	return sunday;
}
//END get_end_of_week

//BEGIN removearrayduplicate
function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}
//END removearrayduplicate

//BEGIN escape_special_chars
function escape_special_chars(text) {
	var iChars = ".@()~`!#$%^&*+=-[]\\\';,/{}|\":<>?.1234567890";
	var text_new = '';

	if (text != null && text != '' && text != undefined) {
		for (var y = 0; y < text.toString().length; y++) {
			if (iChars.indexOf(text[y]) == -1) {
				text_new += text[y];
			}
		}
	}
	return text_new;
}
//END escape_special_chars

//BEGIN get_dateFormat
function get_dateFormat(d_inv_date) {
	try {
		//Date format for UK FP
		var dateSplit = d_inv_date.split('/');
		var newFormat = dateSplit[1] + "-" + dateSplit[0] + "-" + dateSplit[2];
		return newFormat;
	}
	catch (e) {
		nlapiLogExecution('DEBUG', 'Error in Date conversion function', e);
	}
}
//END get_dateFormat