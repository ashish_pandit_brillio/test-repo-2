/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Time_Sheet_Resource_Controls.js
	Author      : Shweta Chopde
	Date        : 21 May 2014
    Description : Time Sheet Controls


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_resource_controls(type)
{	
	 nlapiDisableLineItemField('timeitem','custcol_approvalstatus',true)
	

	var i_project;
	
	var i_time_count = nlapiGetLineItemCount('timeitem')
//	alert(' Time Count-->' + i_time_count);
	
	
 	if(_logValidation(i_time_count))
	{
		for (var i = 1; i <= i_time_count; i++) 
		{
		i_project = nlapiGetLineItemValue('timeitem', 'customer', i)
		//alert(' Project -->' + i_project);

          var i_resource = nlapiGetFieldValue('employee')
  
		  var i_tran_date = nlapiGetFieldValue('trandate')  
		  
		  var d_start_week = get_start_of_week(i_tran_date) 
		  var d_end_week =  get_end_of_week(i_tran_date)
		
		  var a_resource_allocated = get_resource_allocation_details(i_resource,i_project,d_start_week,d_end_week)
		  
		  var a_split_array =  new Array();
		  
		  if (_logValidation(a_resource_allocated)) 
		  {
		  	for (var b = 0; b < a_resource_allocated.length; b++) 
			{
		  		a_split_array = a_resource_allocated[b].split('&&&');
		  		
		  		var d_start_date_1 = a_split_array[0];
		  		
		  		var d_end_date_1 = a_split_array[1];		  		
				
				
			    if ((d_start_date_1 < d_end_week) && (d_end_date_1 > d_start_week || d_end_date_1 == d_end_week)) 
				{		
					nlapiSetLineItemValue('timeitem','isbillable',i,'T')
				}
				else
				{
					nlapiSetLineItemValue('timeitem','isbillable',i,'F')	
				}		
			
			}
		  }			
		
		}
		
	}
  		
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_controls(type, name, linenum)
{
  nlapiDisableLineItemField('timeitem','custcol_approvalstatus',true)
	 
	 
	 
	 
   if(name =='isbillable')
   {
   	 var is_billable = nlapiGetCurrentLineItemValue('timeitem','isbillable');
	 
	 // if(is_billable == 'T')
	 {	 		
	 var i_project = nlapiGetCurrentLineItemValue('timeitem', 'customer')
	
	  var i_resource = nlapiGetFieldValue('employee')
	  
	  var i_tran_date = nlapiGetFieldValue('trandate')  
	  
	  var d_start_week = get_start_of_week(i_tran_date) 
	  var d_end_week =  get_end_of_week(i_tran_date)
	
	  var a_resource_allocated = get_resource_allocation_details(i_resource,i_project,d_start_week,d_end_week)
	  
	  var a_split_array =  new Array();
	  
	  if (_logValidation(a_resource_allocated)) 
	  {
	  	for (var b = 0; b < a_resource_allocated.length; b++) 
		{
	  		a_split_array = a_resource_allocated[b].split('&&&');
	  		
	  		var d_start_date_1 = a_split_array[0];
	  		
	  		var d_end_date_1 = a_split_array[1];
			
			var is_billable_1 = a_split_array[2];
			
			if ((d_start_date_1 < d_end_week) && (d_end_date_1 > d_start_week || d_end_date_1 == d_end_week)) 
			{					
				nlapiSetCurrentLineItemValue('timeitem','isbillable',is_billable_1,false,false)
			}
			else
			{				
				nlapiSetCurrentLineItemValue('timeitem','isbillable',is_billable_1,false,false)
				
				if(is_billable_1 == 'F')
				{
					alert('Please Check Billing Start date and End Date of the Resource');
					return false;
				}
			}	
		
		}
	  }
	 }		 
    }//Is Billable
	
	
	
   if(name =='customer')
   {
   	 var is_billable = nlapiGetCurrentLineItemValue('timeitem','isbillable');
	
	// if(is_billable == 'T')
	 {	 		
	 var i_project = nlapiGetCurrentLineItemValue('timeitem', 'customer')
	
	  var i_resource = nlapiGetFieldValue('employee')
	  
	  var i_tran_date = nlapiGetFieldValue('trandate')  
	  
	  var d_start_week = get_start_of_week(i_tran_date) 
	  var d_end_week =  get_end_of_week(i_tran_date)
	
	  var a_resource_allocated = get_resource_allocation_details(i_resource,i_project,d_start_week,d_end_week)
	
	  var a_split_array =  new Array();
	  
	  if (_logValidation(a_resource_allocated)) 
	  {
	  	for (var b = 0; b < a_resource_allocated.length; b++) 
		{
	  		a_split_array = a_resource_allocated[b].split('&&&');
	  		
	  		var d_start_date_1 = a_split_array[0];
	  		
	  		var d_end_date_1 = a_split_array[1];
			
			var is_billable_1 = a_split_array[2];
							  		
		    if ((d_start_date_1 < d_end_week) && (d_end_date_1 > d_start_week || d_end_date_1 == d_end_week)) 
			{					
				nlapiSetCurrentLineItemValue('timeitem','isbillable',is_billable_1,false,false)				
			}
			else
			{				
				nlapiSetCurrentLineItemValue('timeitem','isbillable',is_billable_1,false,false)
				
				if(is_billable_1 == 'F')
				{
				//	alert('Please Check Billing Start date and End Date of the Resource');
					return false;
				}
			}

		
		}
	  }
	 }		 
    }//Is Billable
	
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_billable(type)
{
  if(type == 'timeitem')
  { 
   	 var is_billable = nlapiGetCurrentLineItemValue('timeitem','isbillable');
	 
	  var i_hours0 = nlapiGetCurrentLineItemValue('timeitem','hours0');
	  
	  var i_hours1 = nlapiGetCurrentLineItemValue('timeitem','hours1');
	   
	  var i_hours2 = nlapiGetCurrentLineItemValue('timeitem','hours2');
	  
	  var i_hours3 = nlapiGetCurrentLineItemValue('timeitem','hours3');
	   
	  var i_hours4 = nlapiGetCurrentLineItemValue('timeitem','hours4');
	  
	  var i_hours5 = nlapiGetCurrentLineItemValue('timeitem','hours5');
	   
	  var i_hours6 = nlapiGetCurrentLineItemValue('timeitem','hours6');
	  
	 
	// if(is_billable == 'T')
	{	 		
	 var i_project = nlapiGetCurrentLineItemValue('timeitem', 'customer')
	
	  var i_resource = nlapiGetFieldValue('employee')
	  
	  var i_tran_date = nlapiGetFieldValue('trandate')  
	  
	 /*
 var d_start_week = get_start_of_week(i_tran_date) 
	  var d_end_week =  get_end_of_week(i_tran_date)
	  
	  d_start_week = nlapiStringToDate(d_start_week)
	  d_end_week = nlapiStringToDate(d_end_week)
	  
	  
	  var i_hours0_date_SOW = nlapiAddDays(d_start_week,1)
	  
	  var i_hours1_date_SOW = nlapiAddDays(d_start_week,2)
	   
	  var i_hours2_date_SOW = nlapiAddDays(d_start_week,3)
	  
	  var i_hours3_date_SOW = nlapiAddDays(d_start_week,4)
	   
	  var i_hours4_date_SOW = nlapiAddDays(d_start_week,5)
	  
	  var i_hours5_date_SOW = nlapiAddDays(d_start_week,6)
	   
	  var i_hours6_date_SOW = nlapiAddDays(d_start_week,7)
*/
	 
	  var a = new Array();
      a['User-Agent-x'] = 'SuiteScript-Call';
	  
	  var resposeObject = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=212&deploy=1&custscript_project_ts_r=' + i_project+'&custscript_resource_ts_r=' + i_resource+'&custscript_hours0=' + i_hours0+'&custscript_hours1=' + i_hours1+'&custscript_hours2=' + i_hours2+'&custscript_hours3=' + i_hours3+'&custscript_hours4=' + i_hours4+'&custscript_hours5=' + i_hours5+'&custscript_hours6=' + i_hours6+'&custscript_tran_date_ts_r='+i_tran_date, null, a);
  	                    
	  var result_response = resposeObject.getBody();
	//  alert('result_response'+result_response)
	  
	  
	  if(result_response!=null && result_response!=undefined && result_response!='')
	  {
	  	  if(result_response == 'Resource is not allocated for this period .')
		  {
		  	alert('Resource is not allocated for this period .');
			return false;
		  }
	  	
	  }
	  
	  
/*
	  alert('a_resource_allocated_ts'+a_resource_allocated_ts)
	  alert('a_resource_allocated_ts length'+a_resource_allocated_ts.length)
	 
	 
	 var a_resource_allocated ;
	 var i_data_po = new Array()
	 i_data_po =  a_resource_allocated_ts;
 
      for(var dt=0;dt<i_data_po.length;dt++)
	  {
		 	a_resource_allocated = i_data_po.split(',')
		    break;				
	  }
	 alert(' PO Array -->'+a_resource_allocated)	
	 alert(' PO Array  Length-->'+a_resource_allocated.length)		
	
	 	 	  	
//    var a_resource_allocated = get_resource_allocation_details_other(i_resource,i_project,d_start_week,d_end_week)
	  
	  var a_split_array =  new Array();
	  
	  if (_logValidation(a_resource_allocated)) 
	  {
	  	for (var b = 0; b < a_resource_allocated.length; b++) 
		{
	  		a_split_array = a_resource_allocated[b].split('&&&');
	  		
	  		var d_start_date_1 = a_split_array[0];
	  		
	  		var d_end_date_1 = a_split_array[1];
			
			var is_billable_1 = a_split_array[2];
			
			
			d_start_date_1 = nlapiStringToDate(d_start_date_1);
			
			d_end_date_1 = nlapiStringToDate(d_end_date_1);
			     
	   if (_logValidation(i_hours0))
	   {
		   	if(d_start_date_1>i_hours0_date_SOW && d_end_date_1>i_hours0_date_SOW)
		   {
		   	alert('Resource is not allocated for this period .');
			return false;	
			
		   }	   	
	   }//i_hours0
	   if (_logValidation(i_hours1))
	   {
	   	 if(d_start_date_1>i_hours1_date_SOW && d_end_date_1>i_hours1_date_SOW)
	   {
	   	alert('Resource is not allocated for this period .');
		return false;	
		
	   }
		  
	   }//i_hours1
	   if (_logValidation(i_hours2))
	   {
	   	  if(d_start_date_1>i_hours2_date_SOW && d_end_date_1>i_hours2_date_SOW)
	   {
	   	alert('Resource is not allocated for this period .');
		return false;	
		
	   } 
		  
	   }//i_hours2
	   if (_logValidation(i_hours3))
	   {
	   	 if(d_start_date_1>i_hours3_date_SOW && d_end_date_1>i_hours3_date_SOW)
	   {
	   	alert('Resource is not allocated for this period .');
		return false;	
		
	   } 
		  
	   }//i_hours3
	   if (_logValidation(i_hours4))
	   {
	   	   if(d_start_date_1>i_hours4_date_SOW && d_end_date_1>i_hours4_date_SOW)
	   {
	   	alert('Resource is not allocated for this period .');
		return false;	
		
	   } 
		  
	   }//i_hours4
	     if (_logValidation(i_hours5))
	   {
	   	  
        if(d_start_date_1>i_hours5_date_SOW && d_end_date_1>i_hours5_date_SOW)
	   {
	   	alert('Resource is not allocated for this period .');
		return false;	
		
	   }  
		  
	   }//i_hours5
	   if (_logValidation(i_hours6))
	   {
	   	if(d_start_date_1>i_hours6_date_SOW && d_end_date_1>i_hours6_date_SOW)
	   {
	   	alert('Resource is not allocated for this period .');
		return false;	
		
	   }  
		  
	   }//i_hours6
	
		}
	  }
*/
	 }   
  }//Time Item

	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================





function get_resource_allocation_details_other(i_resource,i_project,d_start_week,d_end_week)
{  
	var i_cnt = 0;
	var i_resource_1;
	var i_hours;
	var d_start_date;	
	var d_end_date;
	var i_work_location;
	var d_billing_start_date;
	var d_billing_end_date;
	var is_billable;
	var a_resource_array =  new Array();
	var i_project;		

    if(_logValidation(i_project))
	{		
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('project', null, 'is', i_project);
	/*
filter[1] = new nlobjSearchFilter('custeventbstartdate', null, 'onorafter', d_start_week);	
	filter[2] = new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_end_week);
	filter[3] = new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_start_week);
	filter[4] = new nlobjSearchFilter('custeventbenddate', null, 'onorbefore', d_end_week);
		
*/
//	var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
	 
	 var i_search_results = nlapiSearchRecord('resourceallocation',null,filter,null);
	 alert('i_search_results'+i_search_results)
	 
	if (_logValidation(i_search_results)) 
	{	 
		for (var c = 0; c < i_search_results.length; c++) 
		{
			var a_search_transaction_result = i_search_results[c];
			var columns = a_search_transaction_result.getAllColumns();
			var columnLen = columns.length;
			
			for (var hg = 0; hg < columnLen; hg++) 
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
				
				
				 if(label =='project')
				 {
				 	i_project = value;
				 }
				
			     if(label =='Resource')
				 {
				 	i_resource_1 = value;
				 }
				 if(label =='Hours')
				 {
				 	i_hours = value;
				 }
				 if(label =='Start Date')
				 {
				 	d_start_date = value;
				 }
				 if(label =='End Date')
				 {
				 	d_end_date = value;
				 }
				 if(label =='Work Loctaion')
				 {
				 	i_work_location = value;
				 }	
				  if(label =='Billing Start Date')
				 {
				 	d_billing_start_date = value;
				 }
				 if(label =='Billing End Date')
				 {
				 	d_billing_end_date = value;
				 }	
				 if(label =='Billable')
				 {
				 	is_billable = value;
				 }				
			}			
			if(i_resource == i_resource_1)
			{
				a_resource_array[i_cnt++] = d_start_date+'&&&'+d_end_date+'&&&'+is_billable;				
			}	
						
		}
	}
	}//Project
  

	return a_resource_array;
}



function get_resource_allocation_details(i_resource,i_project,d_start_week,d_end_week)
{  
	var i_cnt = 0;
	var i_resource_1;
	var i_hours;
	var d_start_date;	
	var d_end_date;
	var i_work_location;
	var d_billing_start_date;
	var d_billing_end_date;
	var is_billable;
	var a_resource_array =  new Array();
	var i_project;		

    if(_logValidation(i_project))
	{		
    var filter = new Array();
    filter[0] = new nlobjSearchFilter('project', null, 'is', i_project);
	/*
filter[1] = new nlobjSearchFilter('custeventbstartdate', null, 'onorafter', d_start_week);	
	filter[2] = new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_end_week);
	filter[3] = new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_start_week);
	filter[4] = new nlobjSearchFilter('custeventbenddate', null, 'onorbefore', d_end_week);
		
*/
	var i_search_results = nlapiSearchRecord('resourceallocation','customsearch_resource_allocation_searc_2',filter,null);
	 
	if (_logValidation(i_search_results)) 
	{	 
		for (var c = 0; c < i_search_results.length; c++) 
		{
			var a_search_transaction_result = i_search_results[c];
			var columns = a_search_transaction_result.getAllColumns();
			var columnLen = columns.length;
			
			for (var hg = 0; hg < columnLen; hg++) 
			{
				var column = columns[hg];
				var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
				
				
				 if(label =='project')
				 {
				 	i_project = value;
				 }
				
			     if(label =='Resource')
				 {
				 	i_resource_1 = value;
				 }
				 if(label =='Hours')
				 {
				 	i_hours = value;
				 }
				 if(label =='Start Date')
				 {
				 	d_start_date = value;
				 }
				 if(label =='End Date')
				 {
				 	d_end_date = value;
				 }
				 if(label =='Work Loctaion')
				 {
				 	i_work_location = value;
				 }	
				  if(label =='Billing Start Date')
				 {
				 	d_billing_start_date = value;
				 }
				 if(label =='Billing End Date')
				 {
				 	d_billing_end_date = value;
				 }	
				 if(label =='Billable')
				 {
				 	is_billable = value;
				 }				
			}			
			if(i_resource == i_resource_1)
			{
				a_resource_array[i_cnt++] = d_billing_start_date+'&&&'+d_billing_end_date+'&&&'+is_billable;				
			}	
						
		}
	}
	}//Project
  

	return a_resource_array;
}


function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// return an array of date objects for start (monday)
// and end (sunday) of week based on supplied 
// date object or current date
function get_start_of_week(date) 
{
  // If no date object supplied, use current date
  // Copy date so don't modify supplied date
 var now  = nlapiStringToDate(date)

  // set time to some convenient value
  now.setHours(0,0,0,0);

  // Get the previous Monday
  var monday = new Date(now);
  monday.setDate(monday.getDate() - monday.getDay());
  monday = nlapiDateToString(monday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> ' + monday);
  
  // Get next Sunday
  var sunday = new Date(now);
  sunday.setDate(sunday.getDate() - sunday.getDay() +7);
  sunday = nlapiDateToString(sunday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> ' + sunday);
  // Return array of date objects
  return monday;
}

function get_end_of_week(date) 
{
  // If no date object supplied, use current date
  // Copy date so don't modify supplied date
 var now  = nlapiStringToDate(date)

  // set time to some convenient value
  now.setHours(0,0,0,0);

  // Get the previous Monday
  var monday = new Date(now);
  monday.setDate(monday.getDate() - monday.getDay());
  monday = nlapiDateToString(monday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Monday --> ' + monday);
  
  // Get next Sunday
  var sunday = new Date(now);
  sunday.setDate(sunday.getDate() - sunday.getDay() +7);
  sunday = nlapiDateToString(sunday)
 // nlapiLogExecution('DEBUG','afterSubmit_calculate_OT', ' Sunday --> ' + sunday);
  // Return array of date objects
  return sunday;
}


// END FUNCTION =====================================================
