//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
   	Script Name : FUEL | UES Delivery Anchor Notification 
	Author      : Ashish Pandit
	Date        : 09 Oct 2019
    Description : User Event to update MariaDB on Field change of Delivery Anchor   


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

	 */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================

function afterSubmitCheckFields(type)
{
	nlapiLogExecution('Debug','type ',type);
	if(type == 'create')
	{
		try
		{
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var employeeInternalId = recordObject.getFieldValue('allocationresource');
			var projectInternalId = recordObject.getFieldValue('project');
			var pmInternalId = nlapiLookupField('job',recordObject.getFieldValue('project'),'custentity_projectmanager');
			var accountInternalId = nlapiLookupField('job',recordObject.getFieldValue('project'),'parent');
			var employeeLocation =  nlapiLookupField('employee',recordObject.getFieldValue('allocationresource'),'location');
			var employeeAllocationStartDate = recordObject.getFieldValue('startdate');
			var employeeAllocationEndDate = recordObject.getFieldValue('enddate');
			
			var percentAllocation = recordObject.getFieldValue('percentoftime');

			var body = {};
			var method;
			method = "POST";
			body.employeeInternalId = employeeInternalId;
			body.projectInternalId = projectInternalId;
			body.pmInternalId = pmInternalId;
			body.accountInternalId = accountInternalId;
			body.employeeLocation = employeeLocation;
			body.employeeAllocationStartDate = employeeAllocationStartDate;
			body.employeeAllocationEndDate = employeeAllocationEndDate;
			
			body.allocationPercentage = parseInt(percentAllocation);

			//var url = "https://fuelnode1.azurewebsites.net/employee_allocation";
			
			var url = "https://fuelnodesandbox.azurewebsites.net/employee_allocation";
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
	if(type == 'edit' || type == 'xedit')
	{
		try
		{
			var oldRecord = nlapiGetOldRecord();

			var oldRecord_employeeInternalId = recordObject.getFieldValue('allocationresource');
			var oldRecord_projectInternalId = recordObject.getFieldValue('project');
			var oldRecord_pmInternalId = nlapiLookupField('job',recordObject.getFieldValue('project'),'custentity_projectmanager');
			var oldRecord_accountInternalId = nlapiLookupField('job',recordObject.getFieldValue('project'),'parent');
			var oldRecord_employeeLocation =  nlapiLookupField('employee',recordObject.getFieldValue('allocationresource'),'location');
			var oldRecord_employeeAllocationStartDate = recordObject.getFieldValue('startdate');
			var oldRecord_employeeAllocationEndDate = recordObject.getFieldValue('enddate');

			
			var recordObject = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var employeeInternalId = recordObject.getFieldValue('allocationresource');
			var projectInternalId = recordObject.getFieldValue('project');
			var pmInternalId = nlapiLookupField('job',recordObject.getFieldValue('project'),'custentity_projectmanager');
			var accountInternalId = nlapiLookupField('job',recordObject.getFieldValue('project'),'parent');
			var employeeLocation =  nlapiLookupField('employee',recordObject.getFieldValue('allocationresource'),'location');
			var employeeAllocationStartDate = recordObject.getFieldValue('startdate');
			var employeeAllocationEndDate = recordObject.getFieldValue('enddate');
			
			var percentAllocation = recordObject.getFieldValue('percentoftime');                           ////NIS-1231  prabhat Gupta 27/02/2020

			if(oldRecord_employeeInternalId != employeeInternalId || oldRecord_projectInternalId != projectInternalId || oldRecord_pmInternalId!=pmInternalId || oldRecord_accountInternalId!=accountInternalId || oldRecord_employeeLocation!=employeeLocation || oldRecord_employeeAllocationStartDate!=employeeAllocationStartDate || oldRecord_employeeAllocationEndDate != employeeAllocationEndDate)
			{
				var body = {};
				var method;
				method = "POST";
				body.employeeInternalId = employeeInternalId;
				body.projectInternalId = projectInternalId;
				body.pmInternalId = pmInternalId;
				body.accountInternalId = accountInternalId;
				body.employeeLocation = employeeLocation;
				body.employeeAllocationStartDate = employeeAllocationStartDate;
				body.employeeAllocationEndDate = employeeAllocationEndDate;

                body.allocationPercentage = parseInt(percentAllocation);                                 ////NIS-1231  prabhat Gupta 27/02/2020
				
			//	var url = " https://fuelnode1.azurewebsites.net/employee_allocation";
			
			   var url = "https://fuelnodesandbox.azurewebsites.net/employee_allocation";
				nlapiLogExecution('debug','body  ',JSON.stringify(body));
				body = JSON.stringify(body);
				var response = call_node(url,body,method);
				nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
			}
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
function beforeSubmitCheckFields(type)
{
	if(type == 'delete')
	{
		try
		{
			var body = {};
			var method = "DELETE";
		
		//	var url = "https://fuelnode1.azurewebsites.net/employee_allocation/employee_internal_id/"+nlapiGetRecordId();
		
		var url = "https://fuelnodesandbox.azurewebsites.net/employee_allocation/employee_internal_id/"+nlapiGetRecordId();
			//body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution('debug','body  ',JSON.stringify(body));
			nlapiLogExecution("DEBUG", "response Delete Mode: ", JSON.stringify(response));
		}
		catch(error)
		{
			nlapiLogExecution('Debug','Error ',error);
		}
	}
}
//END BEFORE LOAD ====================================================


function _logValidation(value) 
{
	if(value != null && value!= 'null' && value != '' && value != undefined && value != undefined && value != 'undefined' && value.toString() != 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}
//END FUNCTION =====================================================

/************************************************/
function getEmpIds(s_employee)
{
	var resultArray = new Array();
	if(_logValidation(s_employee))
	{
		nlapiLogExecution('Debug','s_employee in function',s_employee);
		//var temp = s_employee.split(',');
		for(var i=0; i<s_employee.length;i++)
		{
			resultArray.push(s_employee[i]);
		}
	}
	return resultArray;
}