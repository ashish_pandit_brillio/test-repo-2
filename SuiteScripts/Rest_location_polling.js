/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 */
 define(["N/record", "N/search", "N/file", "N/format", "./_get_time_lib.js","N/https"], function (record, search, file, format, utility,https) {
    function _post(context) {
        try {
            var datainObj = context;
            var filterArr = []
            log.debug({
                title: 'datainObj',
                details: datainObj
            });
            var currentDateAndTime = sysDate()
            log.debug({
                title: 'currentDateAndTime',
                details: currentDateAndTime
            });
            var receivedDate = datainObj.lastUpdatedTimestamp;
            log.debug({
                title: 'currentDateAndTime',
                details: currentDateAndTime
            });
            if(receivedDate){
                filterArr.push(search.createFilter({
                    "name": "lastmodified",
                    "operator": search.Operator.WITHIN,
                    "values": [receivedDate, currentDateAndTime]
                  }));
                  filterArr.push(search.createFilter({
                    "name": "isinactive",
                    "operator": search.Operator.IS,
                    "values": ['F']
                  }));
            }
            else{
                filterArr.push(search.createFilter({
                    "name": "name",
                    "operator": search.Operator.ISNOTEMPTY,
                    "values": [" "]
                  }));
            }
            log.debug({
                title: 'filterAr',
                details: filterArr
            })
            var customrecord_taleo_location_masterSearchObj = search.create({
                type: "customrecord_taleo_location_master",
                filters:filterArr,
                columns:
                    [
                        search.createColumn({
                            name: "name",
                            sort: search.Sort.ASC,
                            label: "Name"
                        }),
                        search.createColumn({ name: "custrecord_taleo_location_city", label: "City" }),
                        search.createColumn({ name: "custrecord_taleo_location_state", label: "State" }),
                        search.createColumn({ name: "custrecord_taleo_location_country", label: "Country" }),
                        search.createColumn({ name: "custrecord_taleo_location_code", label: "Location Code" }),
                        search.createColumn({name: "isinactive", label: "Inactive"})
                    ]
            });
            var searchResultCount = customrecord_taleo_location_masterSearchObj.runPaged().count;
            var dataRow = [];
            var id;
            var name;
            var city;
            var state;
            var country;
            var locationCode;
            var inactive;
            log.debug("customrecord_taleo_location_masterSearchObj result count", searchResultCount);
            customrecord_taleo_location_masterSearchObj.run().each(function (result) {
                var data = {};
                id =  result.id
                data.internalId = {
                    "name":id
                }
                name = result.getValue({
                    name: 'name'
                })
                data.name = {
                    "name": name
                }
                city = result.getValue({
                    name: 'custrecord_taleo_location_city'
                })
                data.city = {
                    "name": city
                }
                state = result.getValue({
                    name: 'custrecord_taleo_location_state'
                })
                data.state = {
                    "name":state
                }
                country = result.getValue({
                    name: 'custrecord_taleo_location_country'
                })
                data.country = {
                    "name": country
                }
                locationCode = result.getValue({
                    name: 'custrecord_taleo_location_code'
                })
                data.locationCode = {
                    "name": locationCode
                }
                inactive = result.getValue({
                    name: 'isinactive'
                })
                data.inactive = {
                    "name": inactive
                }
                dataRow.push(data)
                return true;
            });
            return {
                timeStamp :currentDateAndTime,
                data :dataRow,
                status: true
            };
        } catch (error) {
            log.debug({
                title: error.name,
                details:error.message
            })
            return {
                timeStamp : '',
                data :error.name,
                status : false
            };
        }
    }
    return {
        post: _post,
    }
});
