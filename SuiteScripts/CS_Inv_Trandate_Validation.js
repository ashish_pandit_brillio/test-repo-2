/**
 * Validate the transaction date being entered by the user
 * 
 * Version Date Author Remarks 1.00 26 Aug 2015 Nitish Mishra
 * 
 */

function pageInit(type) {

	if (nlapiGetFieldValue('subsidiary') == '2') {

		if (type == 'create' || type == 'copy') {
			nlapiLogExecution('debug', 'hide tran');
			nlapiSetFieldDisplay('tranid', false);
		}
	}
}

function clientSaveRecord() {
	return validateTransactionDate();
}

function clientValidateField(type, name, linenum) {

	if (nlapiGetFieldValue('subsidiary') == '2') {
		if (name == 'trandate') {
			return validateTransactionDate();
		}
	}
}

function clientFieldChanged(type, name, linenum) {

	if (nlapiGetFieldValue('subsidiary') == '2') {

		if (name == 'trandate') {
			return validateTransactionDate();
		}
	}
}

// check if the new transaction date lies within the same month as the old
// transaction date
function validateTransactionDate() {
	try {
		var tranDate = nlapiStringToDate(nlapiGetFieldValue('trandate'));
		var currentMonth = new Date().getMonth();
		var tranDateMonth = tranDate.getMonth();
		var currentYear = new Date().getFullYear();
		var tranDateYear = tranDate.getFullYear();

		if (currentMonth == tranDateMonth && currentYear == tranDateYear) {
			return true;
		} else {
			alert("Transaction Date not valid.\nTransaction Date should be of the current month.");
			nlapiSetFieldValue('trandate', '', false, false);
			return false;
		}
	} catch (err) {
		alert(err.message);
		return false;
	}
}