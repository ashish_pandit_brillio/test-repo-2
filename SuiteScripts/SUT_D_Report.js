//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name:SUT_Report_NotSubmittedfromTS_All_Res.js
     Author:
     Company:Aashna Cloud tech
     Date:20-08-2014
     Description:
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     20-08-2014
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     SUITELET
     - suiteletFunction(request, response )
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
     */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================



//END GLOBAL VARIABLE BLOCK  =======================================


//BEGIN SUITELET ==================================================
var d_start_wk_1 = null;
var d_start_wk_2 = null;
var d_start_wk_3 = null;
var d_start_wk_4 = null;

var d_end_wk_1 = null;
var d_end_wk_2 = null;
var d_end_wk_3 = null;
var d_end_wk_4 = null;
			
function GenerateNotSubmittedReport_new_1(request, response) //
{
    try //
    {
        var a_employeearray = new Array();
        
        if (request.getMethod() == 'GET') //
        {
            //nlapiLogExecution('DEBUG', 'In GET Method', '')
            
            
            var s_period = '';
            var form = nlapiCreateForm('Not Submitted - All Resources');
            var oldvalues = false;
            var flag = 0;
            form.setScript('customscriptcli_notsubmitted')
            
            var contextObj = nlapiGetContext();
            //nlapiLogExecution('DEBUG', 'GET', ' contextObj===>' + contextObj)
            
            var beginUsage = contextObj.getRemainingUsage();
            //nlapiLogExecution('DEBUG', 'Search Results', 'beginUsage :' + beginUsage);
            
            //var fromdate = form.addField('custpage_fromdate', 'Date', 'Start Date :');
            var fromdate = form.addField('custpage_fromdate', 'Date', 'Period End Date :');
            //var Todate = form.addField('custpage_todate', 'Date', 'End Date :');
            
            var s_startdate = request.getParameter('custpage_fromdate');
            
            //nlapiLogExecution('DEBUG', 'GET Search results ', ' s_startdate =' + s_startdate)
            
            //var s_enddate = request.getParameter('custpage_todate');
            //nlapiLogExecution('DEBUG', 'GET Search results ', ' s_enddate =' + s_enddate)
            var sublist1 = form.addSubList('record', 'list', 'Not Submitted Time Sheet Report');
            var id = request.getParameter('id');
            //nlapiLogExecution('DEBUG', 'GET results ', ' id=' + id)
            
            if (id == 'Export') //
            {
                var param = request.getParameter('param');
                //nlapiLogExecution('DEBUG', 'GET results ', ' param=' + param)
                
                var datesplit = param.toString().split('@')
                //nlapiLogExecution('DEBUG', 'GET results ', ' datesplit=' + datesplit)
                
                s_startdate = datesplit[0]
                //nlapiLogExecution('DEBUG', 'GET results ', ' s_startdate=' + s_startdate)
                //s_enddate = datesplit[1]
                //nlapiLogExecution('DEBUG', 'GET results ', ' s_enddate=' + s_enddate)
            }
            
            if (s_startdate == null) //
            {
                oldvalues = false;
            }
            else //
            {
                oldvalues = true;
            }
            
            //nlapiLogExecution('DEBUG', 'GET', ' oldvalues===>' + oldvalues)
            /////////////////////////////////////POST ON SUBMIT ////////////////////////////
            if (oldvalues) // Means start date is selected
            {
                // Add Export Button to form and set on click function to it
                form.addButton('custombutton', 'Export As CSV', 'fxn_generatePDF_ALL_RES(\'' + s_startdate + '\');');
                
                var id = request.getParameter('id');
                //nlapiLogExecution('DEBUG', 'GET results ', ' id=' + id)
                if (id == 'Export') //
                {
                    var param = request.getParameter('param');
                    //nlapiLogExecution('DEBUG', 'GET results ', ' param=' + param)
                    
                    var datesplit = param.toString().split('@')
                    //nlapiLogExecution('DEBUG', 'GET results ', ' datesplit=' + datesplit)
                    
                    s_startdate = datesplit[0]
                    //s_enddate = datesplit[1]
                }
                
                var s_startdate1 = s_startdate
                //nlapiLogExecution('DEBUG', 'GET results selected date', ' s_startdate1=' + s_startdate1)
                s_startdate1 = nlapiStringToDate(s_startdate1)
                //nlapiLogExecution('DEBUG', 'GET results ', ' s_startdate1=' + s_startdate1)
                
                //var enddate1 = nlapiAddMonths(s_startdate1, -1)
                var enddate1 = nlapiAddDays(s_startdate1, -27)
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate1=' + enddate1)
                
                s_startdate1 = nlapiDateToString(s_startdate1)
                enddate1 = nlapiDateToString(enddate1)
                
                d_start_wk_1 = enddate1;
                d_end_wk_1 = s_startdate1;
                
                var calculateSundays_new = calculateSundays(enddate1, s_startdate1)
                
                //nlapiLogExecution('DEBUG', 'GET results ', ' calculateSundays_new=' + calculateSundays_new)
                //nlapiLogExecution('DEBUG', 'GET results ', ' calculateSundays_new=' + calculateSundays_new.length)
                
                //var calculate = calculateSundays(s_startdate, s_enddate)
                //nlapiLogExecution('DEBUG', 'GET results ', ' calculate=' + calculate.length)
                
                var enddate_new = calculateSundays_new[0]
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate_new=' + enddate_new)
                
                var SearchEmployeeData1 = new Array()
                SearchEmployeeData1 = SearchEmployeeData(enddate1, s_startdate1)
                //nlapiLogExecution('DEBUG', 'GET results ', ' enddate1=' + enddate1 + '===' + s_startdate1)
                nlapiLogExecution('DEBUG', 'GET results ', ' SearchEmployeeData1=' + SearchEmployeeData1.length)
                
                // Set Week Start Date
                d_start_wk_1 = nlapiStringToDate(enddate1);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_1=' + d_start_wk_1);
                
                d_start_wk_2 = nlapiAddDays(d_start_wk_1, 7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_2=' + d_start_wk_2);
                
                d_start_wk_3 = nlapiAddDays(d_start_wk_2, 7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_3=' + d_start_wk_3);
                
                d_start_wk_4 = nlapiAddDays(d_start_wk_3, 7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_start_wk_4=' + d_start_wk_4);
                
                // Set Week End dates
                d_end_wk_4 = nlapiStringToDate(s_startdate1);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_4=' + d_end_wk_4);
                
                d_end_wk_3 = nlapiAddDays(d_end_wk_4, -7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_3=' + d_end_wk_3);
                
                d_end_wk_2 = nlapiAddDays(d_end_wk_3, -7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_2=' + d_end_wk_2);
                
                d_end_wk_1 = nlapiAddDays(d_end_wk_2, -7);
                //nlapiLogExecution('DEBUG', 'GET results', 'd_end_wk_1=' + d_end_wk_1);
                
                
                d_start_wk_1 = nlapiDateToString(d_start_wk_1);
                d_start_wk_2 = nlapiDateToString(d_start_wk_2);
                d_start_wk_3 = nlapiDateToString(d_start_wk_3);
                d_start_wk_4 = nlapiDateToString(d_start_wk_4);
                
                d_end_wk_1 = nlapiDateToString(d_end_wk_1);
                d_end_wk_2 = nlapiDateToString(d_end_wk_2);
                d_end_wk_3 = nlapiDateToString(d_end_wk_3);
                d_end_wk_4 = nlapiDateToString(d_end_wk_4);
                
                var timesheetdata_WK_1 = SearchTimeData_New(d_start_wk_1, d_end_wk_1);
                //nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_1=' + timesheetdata_WK_1.length)
                
                var timesheetdata_WK_2 = SearchTimeData_New(d_start_wk_2, d_end_wk_2);
                //nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_2=' + timesheetdata_WK_2.length)
                
                var timesheetdata_WK_3 = SearchTimeData_New(d_start_wk_3, d_end_wk_3);
                //nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_3=' + timesheetdata_WK_3.length)
                
                var timesheetdata_WK_4 = SearchTimeData_New(d_start_wk_4, d_end_wk_4);
                //nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_4=' + timesheetdata_WK_4.length)
                
                //var setvalue = setValuesublist(sublist1, SearchEmployeeData1, timesheetdata, enddate_new, s_startdate1, calculateSundays_new, enddate1)
                
                var setvalue = setValuesublist_New(sublist1, SearchEmployeeData1, timesheetdata_WK_1, timesheetdata_WK_2, timesheetdata_WK_3, timesheetdata_WK_4, enddate_new, s_startdate1, calculateSundays_new, enddate1);
            }
            
            if (oldvalues) //
            {
                fromdate.setDefaultValue(s_startdate);
                //Todate.setDefaultValue(s_enddate);
            }
            //nlapiLogExecution('DEBUG', 'GET results ', 'id=' + id)
            
            if (id != 'Export') //
            {
                form.addSubmitButton('Submit');
                response.writePage(form);
            }
            else //
            {
                Callsuitelet(sublist1, param);
            }
        }
        else // If method of request is Post
        {
            var date = request.getParameter('custpage_fromdate');
            //nlapiLogExecution('DEBUG', 'Post results ', ' date =' + date)
            //var fromDate = request.getParameter('custpage_todate');
            //nlapiLogExecution('DEBUG', 'Post results ', ' fromDate =' + fromDate)
            //date = nlapiDateToString(date);
            //fromDate = nlapiDateToString(fromDate);
            
            var params = new Array();
            params['custpage_fromdate'] = date;
            //params['custpage_todate'] = fromDate;
            //nlapiSetRedirectURL('SUITELET', 'customscriptnotsubmittedtsrptfromt', 'customdeploynotsubmittedtsrptfrom', false, params);
            nlapiSetRedirectURL('SUITELET', 'customscript_non_submitted_all_resource', 'customdeploy1', false, params);
        }
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'Try Catch Error ', 'Exception = ' + e)
    }
}

function setValuesublist_New(sublist1, SearchEmployeeData, timesheetdata_WK_1, timesheetdata_WK_2, timesheetdata_WK_3, timesheetdata_WK_4, s_enddate, s_startdate, calculate, enddate1) //
{
	get_Manager_Email();
    //nlapiLogExecution('DEBUG', 'setValuesublist', 'In Function setValuesublist' + SearchEmployeeData.length);
    var s_GCIID = sublist1.addField('custevent_gciid', 'text', 'GCI ID');
    var s_FusionId = sublist1.addField('custevent_fusid', 'text', 'Fusion Id');
    
    var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
    var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
    
    var s_phone = sublist1.addField('custevent_phone', 'text', 'Phone');
    var s_Email = sublist1.addField('custevent_email', 'text', 'Email');
    
    var s_function = sublist1.addField('custevent_function', 'text', 'Function');
    //var s_Week1 = sublist1.addField('custevent_wk1', 'text', 'Week 1');
	var s_Week1 = sublist1.addField('custevent_wk1', 'text', d_end_wk_1);
    
    //var s_Week2 = sublist1.addField('custevent_wk2', 'text', 'Week 2');
	var s_Week2 = sublist1.addField('custevent_wk2', 'text', d_end_wk_2);
	
    //var s_Week3 = sublist1.addField('custevent_wk3', 'text', 'Week 3');
	var s_Week3 = sublist1.addField('custevent_wk3', 'text', d_end_wk_3);
    
    //var s_Week4 = sublist1.addField('custevent_wk4', 'text', 'Week 4');
	var s_Week4 = sublist1.addField('custevent_wk4', 'text', d_end_wk_4);
	
    var s_project = sublist1.addField('custevent_project', 'text', 'Project');
    
    var s_manager_nm = sublist1.addField('custevent_manager_nm', 'text', 'Manager Name');
    var s_manager_eml = sublist1.addField('custevent_manager_eml', 'text', 'Manager Email');
    
    var s_ts_approver = sublist1.addField('custevent_ts_approver_name', 'text', 'Timesheet Approver');
    
    var s_ProjectState = sublist1.addField('custevent_projectstate', 'text', 'Project State');
    var s_dept1 = sublist1.addField('custevent_dept', 'text', 'Department');
    
    var s_customer = sublist1.addField('custevent_customer', 'text', 'Client');
    var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Employee_Type');
    
    var s_Employee_ID = sublist1.addField('custevent_employeeid', 'text', 'Employee_ID');
    var s_Person_Type = sublist1.addField('custevent_persontype', 'text', 'person_type');
	
	var s_term_date = sublist1.addField('custevent_term_date', 'text', 'term_date');
    
	var s_hire_date = sublist1.addField('custevent_hire_date', 'text', 'hire_date');
	var i_ST =    sublist1.addField('custevent_ST''text', 'ST');
	var i_OT =  sublist1.addField('custevent_OT''text', 'OT');
	var i_DT =  sublist1.addField('custevent_DT''text', 'DT');
	var i_Diff_ST = sublist1.addField('custevent_Diff_ST''text', 'Diff_ST');
	var i_Diff_OT = sublist1.addField('custevent_Diff_OT''text', 'Diff_OT');
	
	
    s_Employee_ID.setDisplayType('hidden');
    
    var temp = 0;
    var s_id = ''
    var linenumber = 1;
    var tempInc = ''
    var currentLinenumber = 0;
    var tempdate = '';
    var middlecounteroftime = 0;
    var preveprojtocome = '';
    var preveprojtocomeline = 0;
    var prevcalposarray = new Array();
    var previcalenderweek = new Array();
    var a_lineItemarr = new Array();
    var a_result_array = new Array();
    var i_totalTime = 0;
    var Time_Bothweeksundays = new Array()
    var g_wk1 = 0;
    var g_wk2 = 0;
    var g_wk3 = 0;
    var g_wk4 = 0;
    
    //nlapiLogExecution('DEBUG', 'setValuesublist', ' SearchEmployeeData.length =' + SearchEmployeeData.length)
    //var formatDate = formatdate()
    if (SearchEmployeeData.length != '' && SearchEmployeeData.length != null && SearchEmployeeData.length != 'undefined') //
    {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        var i_empfound = '0';
        var temp_emp = '';
        
        for (var c = 0; c < SearchEmployeeData.length; c++) //for (var c = 0; c < 600; c++) 
        {
            var calculateposition = Sundayswithweekposition(enddate1, s_startdate);
            var calculatepositiontemp = Sundayswithweekposition(enddate1, s_startdate);
            
            //nlapiLogExecution('DEBUG', 'GET results ', ' calculateposition=' + calculateposition.length)
            //nlapiLogExecution('DEBUG', 'setValuesublist', ' calculateposition =' + calculateposition)
            
            var i_Calendar_Week = ''
            var s_FusionID = ''
            var i_Duration = ''
            var i_Phone = '';
            var i_Duration_decimal = 0;
            var i_First_Name = '';
            var i_Last_Name = '';
            var i_Inc_Id = '';
            var Employee_Type = ''
            var Person_Type = ''
            var Employment_Category = ''
            var e_Project = '';
            var e_Client = '';
            var i_Email = ''
            var s_ProjectState = ''
            var s_dept = ''
            var s_Customer = ''
            var i_week1 = ''
            var i_week2 = ''
            var i_week3 = ''
            var i_week4 = ''
            var i_empid = ''
            var i_function = ''
			var i_ST =''
			var i_OT =''
			var i_DT =''
			var i_Diff_ST =''
			var i_Diff_OT =''
			
            var i_project = ''
            var s_manager_name = '';
            var s_manager_email = '';
            var s_ts_approver_name ='';
			var d_term_date = '';
			var d_hire_date = '';
            //nlapiLogExecution('DEBUG', 'c', 'c : ' + c);
            var a_search_employee = SearchEmployeeData[c];
            var columns = a_search_employee.getAllColumns();
            var columnLen = columns.length;
            temp = 0;///for every employee record looping counetr isset 
            //nlapiLogExecution('DEBUG', 'setValuesublist', ' columnLen =' + columnLen)
            
            for (var hg = 0; hg < columnLen; hg++) //
            {
                //nlapiLogExecution('DEBUG', 'setValuesublist', ' hg =' + hg)
                var column = columns[hg];
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                
                var label = column.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
                
                var value = a_search_employee.getValue(column)
                var text = a_search_employee.getText(column)
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                
                if (label == 'Timesheet Date') {
                    i_Calendar_Week = value
                }
                if (label == 'Fusion ID') {
                    s_FusionID = value;
                }
                if (label == 'Phone') {
                    i_Phone = value;
                }
                if (label == 'First Name') {
                    i_First_Name = value;
                }
                if (label == 'Last Name') {
                    i_Last_Name = value;
                }
                if (label == 'IncID') {
                    i_Inc_Id = value;
                }
                if (label == 'Emp. Type') {
                    Employment_Category = value;
                }
                if (label == 'Email') {
                    i_Email = value
                }
                if (label == 'Project') {
                    e_Project = value
                }
                
                if (label == 'Person Type') {
                    Person_Type = text
                }
                
                if (label == 'Department') {
                    s_Department = text;
                }
                if (label == 'Project State') {
                    s_ProjectState = text;
                    
                }
                if (label == 'Dept') {
                    s_dept = text;
                }
                
                if (label == 'Client') {
                    s_Customer = text;
                    e_Client = value;/////5205
                }
                if (label == 'Total hrs') {
                    i_Duration = value;
                }
                if (label == 'Fusion ID') {
                    s_FusionID = value;
                }
                if (label == 'EmpID') {
                    i_empid = value;
                }
                if (label == 'Function') {
                    i_function = text;
                }
				
				if (label == 'ST') {
                    i_ST = text;
                }
				
				if (label == 'OT') {
                    i_OT = text
                }
				
				if (label == 'DT') {
                    i_DT = text;
                }
				
				if (label == 'Diff_ST') {
                    Diff_ST = text;
                }
				
				if (label == 'Diff_OT') {
                    Diff_OT = text;
                }
				
                if (label == 'Project') {
                    i_project = text;
                }
				
				if (label == 'term_date') {
                    d_term_date = value;
                }
				if (label == 'hire_date') {
					d_hire_date = value;
				}
				if (label == 'manager_name') {
                    s_manager_name = text;
                }
				if (label == 'manager_email') {
                    s_manager_email = text;
                }
				if (label == 'ts_approver') {
					s_ts_approver_name = text;
                }
                //nlapiLogExecution('DEBUG', 'test', 'd_term_date : ' + d_term_date);
				if (d_term_date != '' && d_term_date != null && d_term_date != 'undefined' && d_term_date != '- None -') {
                    d_term_date = d_term_date
                }
                else {
                    d_term_date = ''
                }
				
				if (d_hire_date != '' && d_hire_date != null && d_hire_date != 'undefined' && d_hire_date != '- None -') {
                    d_hire_date = d_hire_date
                }
                else {
                    d_hire_date = ''
                }
				
				if (s_ts_approver_name != '' && s_ts_approver_name != null && s_ts_approver_name != 'undefined' && s_ts_approver_name != '- None -') {
					s_ts_approver_name = s_ts_approver_name
                }
                else {
                	s_ts_approver_name = ''
                }
				
				if (s_manager_name != '' && s_manager_name != null && s_manager_name != 'undefined' && s_manager_name != '- None -') {
                    s_manager_name = s_manager_name
                }
                else {
                    s_manager_name = ''
                }
				
				if (s_manager_email != '' && s_manager_email != null && s_manager_email != 'undefined' && s_manager_email != '- None -') {
                    s_manager_email = s_manager_email
                }
                else {
                    s_manager_email = ''
                }
				
                if (s_FusionID != '' && s_FusionID != null && s_FusionID != 'undefined' && s_FusionID != '- None -') {
                    s_FusionID = s_FusionID
                }
                else {
                    s_FusionID = ''
                }
                if (i_function != '' && i_function != null && i_function != 'undefined' && i_function != '- None -') {
                    i_function = i_function
                }
                else {
                    i_function = ''
                }
				
				if (i_ST != '' && i_ST != null && i_ST != 'undefined' && i_ST != '- None -') {
                    i_ST = i_ST
                }
                else {
                    i_ST = ''
                }
				
				if (i_OT != '' && i_OT != null && i_OT != 'undefined' && i_OT != '- None -') {
                    i_OT = i_OT
                }
                else {
                    i_OT = ''
                }
				
				if (i_DT != '' && i_DT != null && i_DT != 'undefined' && i_DT != '- None -') {
                    i_DT = i_DT
                }
                else {
                    i_DT = ''
                }
				
				if (i_Diff_ST != '' && i_Diff_ST != null && i_Diff_ST != 'undefined' && i_Diff_ST != '- None -') {
                    i_Diff_ST = Diff_ST
                }
                else {
                    i_Diff_ST = ''
                }
				
				f (i_Diff_OT != '' && i_Diff_OT != null && i_Diff_OT != 'undefined' && i_Diff_OT != '- None -') {
                    i_Diff_OT = Diff_OT
                }
                else {
                    i_Diff_OT = ''
                }
				
              
                if (i_Phone != '' && i_Phone != null && i_Phone != 'undefined' && i_Phone != '- None -') {
                    i_Phone = i_Phone
                }
                else {
                    i_Phone = ''
                }
                
                if (i_First_Name != '' && i_First_Name != null && i_First_Name != 'undefined' && i_First_Name != '- None -') {
                    i_First_Name = i_First_Name
                }
                else {
                    i_First_Name = ''
                }
                
                if (s_dept != '' && s_dept != null && s_dept != 'undefined' && s_dept != '- None -') {
                    s_dept = s_dept
                }
                else {
                    s_dept = ''
                }
                if (i_Inc_Id != '' && i_Inc_Id != null && i_Inc_Id != 'undefined' && i_Inc_Id != '- None -') {
                    i_Inc_Id = i_Inc_Id
                }
                else {
                    i_Inc_Id = ''
                }
                
                if (i_Email != '' && i_Email != null && i_Email != 'undefined' && i_Email != '- None -') {
                    i_Email = i_Email
                }
                else {
                    i_Email = ''
                }
                
                if (_logValidation(Employment_Category)) {
                    Employment_Category = Employment_Category
                }
                else {
                    Employment_Category = ''
                }
                
                if (s_ProjectState != '' && s_ProjectState != null && s_ProjectState != 'undefined' && s_ProjectState != '- None -') {
                    s_ProjectState = s_ProjectState
                }
                else {
                    s_ProjectState = ''
                }
                if (s_Customer != '' && s_Customer != null && s_Customer != 'undefined' && s_Customer != '- None -') {
                    s_Customer = s_Customer
                }
                else {
                    s_Customer = ''
                }
                if (i_week1 != '' && i_week1 != null && i_week1 != 'undefined' && i_week1 != '- None -') {
                    i_week1 = i_week1
                }
                else {
                    i_week1 = ''
                }
                if (i_week2 != '' && i_week2 != null && i_week2 != 'undefined' && i_week2 != '- None -') {
                    i_week2 = i_week2
                }
                else {
                    i_week2 = ''
                }
                if (i_week3 != '' && i_week3 != null && i_week3 != 'undefined') {
                    i_week3 = i_week3
                }
                else {
                    i_week3 = ''
                }
                if (i_week4 != '' && i_week4 != null && i_week4 != 'undefined') {
                    i_week4 = i_week4
                }
                else {
                    i_week4 = ''
                }
                
            } /////for loop of columns of employee search......
            //nlapiLogExecution('DEBUG', 'i_empid', 'i_empid : ' + i_empid);
            
            //continue;
            
            if (temp_emp == i_empid) // if employee is already entered in report
            {
                continue;
            }
            
            if (c == 0) //
            {
                temp_emp = i_empid;
            }
            
            if (timesheetdata_WK_1 != null && timesheetdata_WK_1 != '' && timesheetdata_WK_1 != 'undefined') //
            {
                var all_columns = timesheetdata_WK_1[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk1 : ' + g_wk1);
                
                for (var wk1 = g_wk1; wk1 < timesheetdata_WK_1.length; wk1++) //
                {
                    var result_wk1 = timesheetdata_WK_1[wk1];
                    //var all_columns = result_wk1.getAllColumns();
                    var wk1_emp = result_wk1.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk1_emp : ' + wk1_emp);
                    
                    //if (i_empid >= wk1_emp) //
                    {
                        if (i_empid == wk1_emp) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk1_emp : ' + wk1_emp);
                            i_week1 = 'Filled';
                            i_project = result_wk1.getValue(all_columns[3]);
                            //s_manager_name = result_wk1.getValue(all_columns[4]);
                            //s_manager_email = result_wk1.getValue(all_columns[5]);
                            g_wk1 = wk1;
                            break;
                        }
                    }
                    if (parseInt(wk1_emp) > parseInt(i_empid)) //
                    {
                        //g_wk1 = wk1;
                        break;
                    }
                }
            }
            
            if (timesheetdata_WK_2 != null && timesheetdata_WK_2 != '' && timesheetdata_WK_2 != 'undefined') //
            {
                var all_columns = timesheetdata_WK_2[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk2 : ' + g_wk2);
                
                for (var wk2 = g_wk2; wk2 < timesheetdata_WK_2.length; wk2++) //
                {
                    var result_wk2 = timesheetdata_WK_2[wk2];
                    //var all_columns = result_wk2.getAllColumns();
                    var wk2_emp = result_wk2.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk2_emp : ' + wk2_emp);
                    
                    //if (i_empid >= wk2_emp) //
                    {
                        if (i_empid == wk2_emp) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk2_emp : ' + wk2_emp);
                            i_week2 = 'Filled';
                            i_project = result_wk2.getValue(all_columns[3]);
                            //s_manager_name = result_wk2.getValue(all_columns[4]);
                            //s_manager_email = result_wk2.getValue(all_columns[5]);
                            g_wk2 = wk2;
                            break;
                        }
                    }
                    if (parseInt(wk2_emp) > parseInt(i_empid)) //
                    {
                        //g_wk2 = wk2;
                        break;
                    }
                }
            }
            
            if (timesheetdata_WK_3 != null && timesheetdata_WK_3 != '' && timesheetdata_WK_3 != 'undefined') //
            {
                var all_columns = timesheetdata_WK_3[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk3 : ' + g_wk3);
                
                for (var wk3 = g_wk3; wk3 < timesheetdata_WK_3.length; wk3++) //
                {
                    var result_wk3 = timesheetdata_WK_3[wk3];
                    
                    var wk3_emp = result_wk3.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk3_emp : ' + wk3_emp);
                    
                    //if (i_empid >= wk3_emp) //
                    {
                        if (i_empid == wk3_emp) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk3_emp : ' + wk3_emp);
                            i_week3 = 'Filled';
                            i_project = result_wk3.getValue(all_columns[3]);
                            //s_manager_name = result_wk3.getValue(all_columns[4]);
                            //s_manager_email = result_wk3.getValue(all_columns[5]);
                            g_wk3 = wk3;
                            break;
                        }
                    }
                    if (parseInt(wk3_emp) > parseInt(i_empid)) //
                    {
                        //g_wk3 = wk3;
                        break;
                    }
                }
            }
            
            if (timesheetdata_WK_4 != null && timesheetdata_WK_4 != '' && timesheetdata_WK_4 != 'undefined') //
            {
                var all_columns = timesheetdata_WK_4[0].getAllColumns();
                //nlapiLogExecution('DEBUG', 'i_empid', 'g_wk4 : ' + g_wk4);
                
                for (var wk4 = g_wk4; wk4 < timesheetdata_WK_4.length; wk4++) //
                {
                    var result_wk4 = timesheetdata_WK_4[wk4];
                    var wk4_emp = result_wk4.getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk4_emp : ' + wk4_emp);
                    
                    //if (i_empid >= wk4_emp) //
                    {
                        if (i_empid == wk4_emp) //
                        {
                            //nlapiLogExecution('DEBUG', 'wk4_emp', 'wk4_emp : ' + wk4_emp);
                            i_week4 = 'Filled';
                            i_project = result_wk4.getValue(all_columns[3]);
                            //s_manager_name = result_wk4.getValue(all_columns[4]);
                            //s_manager_email = result_wk4.getValue(all_columns[5]);
                            g_wk4 = wk4;
                            break;
                        }
                    }
                    if (parseInt(wk4_emp) > parseInt(i_empid)) //
                    {
                        //g_wk4 = wk4;
                        break;
                    }
                }
            }
            
            if (i_week1 == 'Filled' && i_week2 == 'Filled' && i_week3 == 'Filled' && i_week4 == 'Filled') // if employee has entered time in all weeks then skip it from report
            {
                continue;
            }
            
            sublist1.setLineItemValue('custevent_gciid', linenumber, i_Inc_Id);
            sublist1.setLineItemValue('custevent_fusid', linenumber, s_FusionID);
            //nlapiLogExecution('DEBUG', 'Test', 's_FusionID : ' + s_FusionID);
            
            sublist1.setLineItemValue('custevent_employeeid', linenumber, i_empid);
            //nlapiLogExecution('DEBUG', 'Test', 'i_empid : ' + i_empid);
            
            sublist1.setLineItemValue('custevent_persontype', linenumber, Person_Type);
            
            sublist1.setLineItemValue('custevent_firstname', linenumber, i_First_Name);
            //nlapiLogExecution('DEBUG', 'Test', 'i_First_Name : ' + i_First_Name);
            
            sublist1.setLineItemValue('custevent_lastname', linenumber, i_Last_Name);
            sublist1.setLineItemValue('custevent_phone', linenumber, i_Phone);
            sublist1.setLineItemValue('custevent_email', linenumber, i_Email);
            sublist1.setLineItemValue('custevent_function', linenumber, i_function);
            
			sublist1.setLineItemValue('custevent_ST', linenumber, i_ST);
			sublist1.setLineItemValue('custevent_OT', linenumber, i_OT);
			sublist1.setLineItemValue('custevent_DT', linenumber, i_DT);
			sublist1.setLineItemValue('custevent_TO', linenumber, i_TO);
			sublist1.setLineItemValue('custevent_Diff_ST', linenumber, i_Diff_ST);
			sublist1.setLineItemValue('custevent_Diff_OT', linenumber, i_Diff_OT);
			
			
			
			
			
            sublist1.setLineItemValue('custevent_wk1', linenumber, i_week1);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week1 : ' + i_week1);
            
            sublist1.setLineItemValue('custevent_wk2', linenumber, i_week2);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week2 : ' + i_week2);
            
            sublist1.setLineItemValue('custevent_wk3', linenumber, i_week3);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week3 : ' + i_week3);
            
            sublist1.setLineItemValue('custevent_wk4', linenumber, i_week4);
            //nlapiLogExecution('DEBUG', 'Test', 'i_week4 : ' + i_week4);
            
            sublist1.setLineItemValue('custevent_project', linenumber, i_project);
			
			for(var ii=0; ii< manager_email_Array.length; ii++) //
			{
				var str= manager_email_Array[ii];
				
				if(str.indexOf(s_manager_name) >= 0) //
				{
					s_manager_email = str.split("##")[1];
				}
			}
			
            sublist1.setLineItemValue('custevent_manager_nm', linenumber, s_manager_name);
            sublist1.setLineItemValue('custevent_manager_eml', linenumber, s_manager_email);
            sublist1.setLineItemValue('custevent_ts_approver_name', linenumber, s_ts_approver_name);
            sublist1.setLineItemValue('custevent_projectstate', linenumber, s_ProjectState);
            sublist1.setLineItemValue('custevent_dept', linenumber, s_dept);
            sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
            sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
			sublist1.setLineItemValue('custevent_term_date', linenumber, d_term_date);
			sublist1.setLineItemValue('custevent_hire_date', linenumber, d_hire_date);
			
			
			
			
			
            linenumber++;
            
            temp_emp = i_empid;
            //return;
            
            continue;
            
            
            
            
            
            
            var notsubmittedweek = new Array();
            var notsubmittedweek1 = new Array();
            
            /////time sheet data loop starts
            if (timesheetdata != null && timesheetdata != '' && timesheetdata != 'undefined') //
            {
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'timesheetdata  -->' + timesheetdata.length);
                ///for (var d = middlecounteroftime; d < 100; d++) 
                for (var d = middlecounteroftime; d < timesheetdata.length; d++) //
                {
                    //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'd  -->' + d);
                    
                    var i_EmployeeID = ''
                    var i_TimeCalendarWeek = ''
                    var i_Duration = ''
                    var i_EmpIntID = ''
                    var i_ProjectName = ''
                    var s_manager_name = '';
                    var s_manager_email = '';
                    var s_ts_approver_name = '';
                    
                    var a_search_timedata = timesheetdata[d];
                    var columns = a_search_timedata.getAllColumns();
                    
                    //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + a_search_timedata.getValue('internalid', 'employee', 'group'));
                    
                    var columntimeLen = columns.length;
                    for (var k = 0; k < columntimeLen; k++) //
                    {
                        var column_time = columns[k];
                        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                        var label = column_time.getLabel();
                        //nlapiLogExecution('DEBUG', 'search', '**name*=' + column_time.getName() + '**join*=' + column_time.getJoin());
                        
                        var value = a_search_timedata.getValue(column_time)
                        var text = a_search_timedata.getText(column_time)
                        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                        if (label == 'TimeCalendarWeek') {
                            i_TimeCalendarWeek = value
                        }
                        if (label == 'EmployeeID') {
                            i_EmployeeID = text;
                        }
                        if (label == 'Duration') {
                            i_Duration = value;
                        }
                        if (label == 'EmpIntID') {
                            i_EmpIntID = value;
                        }
                        if (label == 'Project Name') {
                            i_ProjectName = value;
                        }
                        if (label == 'Manager Name') {
                            s_manager_name = text;
                        }
                        if (label == 'Manager Email') {
                            s_manager_email = value;
                        }
                        if (label == 'Timesheet Approver') {
                            s_ts_approver_name = text;
                        }
                        
                    }//////////////////////end of for loop
                    //nlapiLogExecution('DEBUG', 'SearchData', '==e_empid==' + i_empid + 'i_EmpIntID==' + i_EmpIntID);
                    
                    //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'i_empid  -->' + i_empid + '  i_EmpIntID  -->' + i_EmpIntID + '===e_Project=' + e_Project + '==i_ProjectName==' + i_ProjectName);
                    if (parseInt(i_empid) > parseInt(i_EmpIntID))////if the employee id is ahead 34 of time employeeid 33 then start from next time entry
                    {
                        middlecounteroftime = parseInt(d + 1);
                        ////break;
                    }
                    else //////
                    {
                        if (i_empid == i_EmpIntID) //if employee time sheet id and project record time sheet found then go ahead
                        {
                        
                            //nlapiLogExecution('DEBUG', 'timesheetdata', 'a_search_timedata= ' + a_search_timedata);
                            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'i_TimeCalendarWeek  -->' + i_TimeCalendarWeek);
                            var flag = calculateposition.toString().indexOf(i_TimeCalendarWeek)
                            
                            ///get matched week time sheet data and week.
                            var i_timesheetdate = Cal_Timesheetdate(enddate1, i_TimeCalendarWeek)
                            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'i_timesheetdate  -->' + i_timesheetdate);
                            if (e_Project == i_ProjectName)///if its same project for same employee ,collect all sundays for all project
                            {
                                i_TimeCalendarWeek = nlapiStringToDate(i_TimeCalendarWeek)
                                
                                i_nextweekdate = nlapiAddDays(i_TimeCalendarWeek, 7); //to get next sunday as previous week text shown in search
                                i_nextweekdate = nlapiDateToString(i_nextweekdate)
                                
                                Time_Bothweeksundays.push(i_nextweekdate);
                            }
                            else {
                                ///Time_Bothweeksundays=new Array();
                                middlecounteroftime = parseInt(d);
                                break;
                            }
                            
                            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'Time_Bothweeksundays  -->' + Time_Bothweeksundays);
                        
                        
                        }
                        else {
                        
                            ///*********************************************************************************************************
                            //nlapiLogExecution('DEBUG', 'SearchData', 'Time_Bothweeksundays=' + Time_Bothweeksundays);
                            ///*********************************************************************************************************
                            for (var temp = 0; temp < calculatepositiontemp.length; temp++) {
                                var sunday = calculatepositiontemp[temp].toString().split('##');
                                //nlapiLogExecution('DEBUG', 'SearchData', 'valueofconst=' + calculatepositiontemp[temp] + 'searching..' + Time_Bothweeksundays.toString().search(sunday[0]));
                                if (Time_Bothweeksundays != null && Time_Bothweeksundays != '' && Time_Bothweeksundays != undefined) {
                                
                                    if (Time_Bothweeksundays.toString().search(sunday[0]) != -1)///if sunday found in array then remove
                                    {
                                        //nlapiLogExecution('DEBUG', 'SearchData', 'calculatepositiontemp[c]=' + calculatepositiontemp[temp]);
                                        calculatepositionTimeweek(sunday[0], calculateposition);
                                    }
                                }
                                
                            }
                            //nlapiLogExecution('DEBUG', 'SearchData', 'calculateposition=' + calculateposition);
                            var s_weekcolumn = '';
                            
                            ///if we have non submitted dates of sundays then 
                            if (i_empid != sublist1.getLineItemValue('custevent_employeeid', linenumber - 1)) {
                            
                            
                                //nlapiLogExecution('DEBUG', 'SearchData', 'calculateposition=' + calculateposition);
                                for (var weeksinmon = 0; weeksinmon < calculateposition.length; weeksinmon++) {
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'calculateposition=');
                                    notsubmittedweek.push(calculateposition[weeksinmon]);
                                }
                                //nlapiLogExecution('DEBUG', 'SearchData', 'else block notsubmittedweek=' + notsubmittedweek + 'Length==' + notsubmittedweek.length);
                                
                                if (notsubmittedweek != null && notsubmittedweek != undefined && notsubmittedweek != '') ////&& notsubmittedweek.length==4
                                {
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'Create line and print weeks');
                                    sublist1.setLineItemValue('custevent_gciid', linenumber, i_Inc_Id);
                                    sublist1.setLineItemValue('custevent_fusid', linenumber, s_FusionID);
                                    sublist1.setLineItemValue('custevent_employeeid', linenumber, i_empid);
                                    sublist1.setLineItemValue('custevent_persontype', linenumber, Person_Type);
                                    
                                    sublist1.setLineItemValue('custevent_firstname', linenumber, i_First_Name);
                                    sublist1.setLineItemValue('custevent_lastname', linenumber, i_Last_Name);
                                    sublist1.setLineItemValue('custevent_phone', linenumber, i_Phone);
                                    sublist1.setLineItemValue('custevent_email', linenumber, i_Email);
                                    sublist1.setLineItemValue('custevent_function', linenumber, i_function);
									
									sublist1.setLineItemValue('custevent_ST', linenumber, i_ST);
									sublist1.setLineItemValue('custevent_OT', linenumber, i_OT);
									sublist1.setLineItemValue('custevent_DT', linenumber, i_DT);
									sublist1.setLineItemValue('custevent_Diff_ST',linenumber, i_Diff_ST);
									sublist1.setLineItemValue('custevent_Diff_OT',linenumber, i_Diff_OT);
									
									
									
									
									
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'length=' + notsubmittedweek.length);
                                    
                                    
                                    for (t = 0; t < notsubmittedweek.length; t++) {
                                        var notsubmitteddates = notsubmittedweek[t].toString().split('##');
                                        if (notsubmitteddates[1] == '1') {
                                            s_weekcolumn = 'custevent_wk1'
                                        }
                                        if (notsubmitteddates[1] == '2') {
                                            s_weekcolumn = 'custevent_wk2'
                                        }
                                        if (notsubmitteddates[1] == '3') {
                                            s_weekcolumn = 'custevent_wk3'
                                        }
                                        if (notsubmitteddates[1] == '4') {
                                            s_weekcolumn = 'custevent_wk4'
                                        }
                                        //nlapiLogExecution('DEBUG', 'SearchData', 's_weekcolumn=' + s_weekcolumn + '====' + linenumber);
                                        sublist1.setLineItemValue(s_weekcolumn, linenumber, notsubmitteddates[0]);
                                        
                                    }///////for loop of not submitted week array and seeting weeks
                                    sublist1.setLineItemValue('custevent_project', linenumber, i_project);
                                    sublist1.setLineItemValue('custevent_manager_nm', linenumber, s_manager_name);
                                    sublist1.setLineItemValue('custevent_manager_eml', linenumber, s_manager_email);
                                    sublist1.setLineItemValue('custevent_ts_approver_name', linenumber, s_ts_approver_name);                                    
                                    sublist1.setLineItemValue('custevent_projectstate', linenumber, s_ProjectState);
                                    sublist1.setLineItemValue('custevent_dept', linenumber, s_dept);
                                    sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
                                    sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                                    linenumber++;
                                }
                            }///if next project is not for same employee and no time sheet for prev for any of weeks.thn only put on emplo one project entry
                            Time_Bothweeksundays = new Array();
                            middlecounteroftime = parseInt(d);
                            //}
                            
                            //nlapiLogExecution('DEBUG', 'SearchData', 'middlecounteroftime===' + middlecounteroftime)
                            break;
                        }////end of if employee time sheet id and project record matches
                    }///if else if emplioyee id is equal or less
                }////For loop of time sheet data array
            }//////end of if time sheet data found
            else {
                ////middlecounteroftime++;
                
                sublist1.setLineItemValue('custevent_gciid', linenumber, i_Inc_Id);
                sublist1.setLineItemValue('custevent_fusid', linenumber, s_FusionID);
                sublist1.setLineItemValue('custevent_employeeid', linenumber, i_empid); //
                sublist1.setLineItemValue('custevent_persontype', linenumber, Person_Type);
                sublist1.setLineItemValue('custevent_firstname', linenumber, i_First_Name);
                sublist1.setLineItemValue('custevent_lastname', linenumber, i_Last_Name);
                sublist1.setLineItemValue('custevent_phone', linenumber, i_Phone);
                sublist1.setLineItemValue('custevent_email', linenumber, i_Email);
                sublist1.setLineItemValue('custevent_function', linenumber, i_function);
				sublist1.setLineItemValue('custevent_ST', linenumber, i_ST);
			sublist1.setLineItemValue('custevent_OT', linenumber, i_OT);
			sublist1.setLineItemValue('custevent_DT', linenumber, i_DT);
			sublist1.setLineItemValue('custevent_Diff_ST',linenumber, i_Diff_ST);
			sublist1.setLineItemValue('custevent_Diff_OT',linenumber, i_Diff_OT);
				
				
				
				
				
                for (t = 0; t < calculate.length; t++) {
                    sublist1.setLineItemValue('custevent_wk' + (t + 1), linenumber, calculate[t]);
                    
                }
                
                sublist1.setLineItemValue('custevent_project', linenumber, i_project)
                sublist1.setLineItemValue('custevent_ts_approver_name', linenumber, s_ts_approver_name);
                //sublist1.setLineItemValue('custevent_manager_nm', linenumber, s_manager_nm);
                //sublist1.setLineItemValue('custevent_manager_eml', linenumber, s_manager_eml);
                sublist1.setLineItemValue('custevent_projectstate', linenumber, s_ProjectState);
                sublist1.setLineItemValue('custevent_dept', linenumber, s_dept);
                sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
                sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                linenumber++;
            }////end of else if time sheet data nt found at all....
        }////end of for loop of employee
    }///end of null values check for employee array
    nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array.length)
    return a_result_array;
}/////End of function

//not being used anywhere
function setValuesublist(sublist1, SearchEmployeeData, timesheetdata, s_enddate, s_startdate, calculate, enddate1) //
{
    //nlapiLogExecution('DEBUG', 'setValuesublist', 'In Function setValuesublist' + SearchEmployeeData.length);
    var s_GCIID = sublist1.addField('custevent_gciid', 'text', 'GCI ID');
    var s_FusionId = sublist1.addField('custevent_fusid', 'text', 'Fusion Id');
    
    var s_firstName = sublist1.addField('custevent_firstname', 'text', 'First Name');
    var s_lastName = sublist1.addField('custevent_lastname', 'text', 'Last Name');
    var s_phone = sublist1.addField('custevent_phone', 'text', 'Phone');
    var s_Email = sublist1.addField('custevent_email', 'text', 'Email');
    var s_function = sublist1.addField('custevent_function', 'text', 'Function');
    var s_Week1 = sublist1.addField('custevent_wk1', 'text', 'Week 1');
    var s_Week2 = sublist1.addField('custevent_wk2', 'text', 'Week 2');
    var s_Week3 = sublist1.addField('custevent_wk3', 'text', 'Week 3');
    var s_Week4 = sublist1.addField('custevent_wk4', 'text', 'Week 4');
    var s_project = sublist1.addField('custevent_project', 'text', 'Project');
    var s_manager_nm = sublist1.addField('custevent_manager_nm', 'text', 'Manager Name');
    var s_manager_eml = sublist1.addField('custevent_manager_eml', 'text', 'Manager Email');
    var s_ProjectState = sublist1.addField('custevent_projectstate', 'text', 'Project State');
    var s_dept1 = sublist1.addField('custevent_dept', 'text', 'Department');
    var s_customer = sublist1.addField('custevent_customer', 'text', 'Client');
    var s_Employee_Type = sublist1.addField('custevent_employeetype', 'text', 'Employee_Type');
    var s_Employee_ID = sublist1.addField('custevent_employeeid', 'text', 'Employee_ID');
    var s_Person_Type = sublist1.addField('custevent_persontype', 'text', 'person_type');
    
    s_Employee_ID.setDisplayType('hidden');
    
    
    var temp = 0;
    var s_id = ''
    var linenumber = 1;
    var tempInc = ''
    var currentLinenumber = 0;
    var tempdate = '';
    var middlecounteroftime = 0;
    var preveprojtocome = '';
    var preveprojtocomeline = 0;
    var prevcalposarray = new Array();
    var previcalenderweek = new Array();
    var a_lineItemarr = new Array();
    var a_result_array = new Array();
    var i_totalTime = 0;
    var Time_Bothweeksundays = new Array()
    
    //nlapiLogExecution('DEBUG', 'setValuesublist', ' SearchEmployeeData.length =' + SearchEmployeeData.length)
    //var formatDate = formatdate()
    if (SearchEmployeeData.length != '' && SearchEmployeeData.length != null && SearchEmployeeData.length != 'undefined') {
        /////////Loop through rows of search/////////////////////////////////////////////////////
        var i_empfound = '0';
        for (var c = 0; c < SearchEmployeeData.length; c++) //for (var c = 0; c < 600; c++) 
        {
        
        
            var calculateposition = Sundayswithweekposition(enddate1, s_startdate);
            var calculatepositiontemp = Sundayswithweekposition(enddate1, s_startdate);
            
            //nlapiLogExecution('DEBUG', 'GET results ', ' calculateposition=' + calculateposition.length)
            //nlapiLogExecution('DEBUG', 'setValuesublist', ' calculateposition =' + calculateposition)
            var i_Calendar_Week = ''
            var s_FusionID = ''
            var i_Duration = ''
            var i_Phone = '';
            var i_Duration_decimal = 0;
            var i_First_Name = '';
            var i_Last_Name = '';
            var i_Inc_Id = '';
            var Employee_Type = ''
            var Person_Type = ''
            var Employment_Category = ''
            var e_Project = '';
            var e_Client = '';
            var i_Email = ''
            var s_ProjectState = ''
            var s_dept = ''
            var s_Customer = ''
            var i_week1 = ''
            var i_week2 = ''
            var i_week3 = ''
            var i_week4 = ''
            var i_empid = ''
            var i_function = ''
			var i_ST = ''
			var i_OT = ''
			var i_DT = ''
			var i_Diff_ST = ''
			var i_Diff_OT = ''
			
            var i_project = ''
            //var s_manager_name = '';
            //var s_manager_email = '';
            var a_search_employee = SearchEmployeeData[c];
            var columns = a_search_employee.getAllColumns();
            
            var columnLen = columns.length;
            temp = 0;///for every employee record looping counetr isset 
            //nlapiLogExecution('DEBUG', 'setValuesublist', ' columnLen =' + columnLen)
            for (var hg = 0; hg < columnLen; hg++) {
                //nlapiLogExecution('DEBUG', 'setValuesublist', ' hg =' + hg)
                var column = columns[hg];
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                var label = column.getLabel();
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', '*************************label********************  -->' + label);
                
                var value = a_search_employee.getValue(column)
                var text = a_search_employee.getText(column)
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                if (label == 'Timesheet Date') {
                    i_Calendar_Week = value
                }
                if (label == 'Fusion ID') {
                    s_FusionID = value;
                }
                if (label == 'Phone') {
                    i_Phone = value;
                }
                if (label == 'First Name') {
                    i_First_Name = value;
                }
                if (label == 'Last Name') {
                    i_Last_Name = value;
                }
                if (label == 'IncID') {
                    i_Inc_Id = value;
                }
                if (label == 'Emp. Type') {
                    Employment_Category = value;
                }
                if (label == 'Email') {
                    i_Email = value
                }
                if (label == 'Project') {
                    e_Project = value
                }
                
                if (label == 'Person Type') {
                    Person_Type = text
                }
                
                if (label == 'Department') {
                    s_Department = text;
                }
                if (label == 'Project State') {
                    s_ProjectState = text;
                    
                }
                if (label == 'Dept') {
                    s_dept = text;
                }
                
                if (label == 'Client') {
                    s_Customer = text;
                    e_Client = value;/////5205
                }
                if (label == 'Total hrs') {
                    i_Duration = value
                }
                if (label == 'Fusion ID') {
                    s_FusionID = value
                }
                if (label == 'EmpID') {
                    i_empid = value;
                }
                if (label == 'Function') {
                    i_function = text
                }
                if (label == 'Project') {
                    i_project = text;
                }
                if (s_FusionID != '' && s_FusionID != null && s_FusionID != 'undefined' && s_FusionID != '- None -') {
                    s_FusionID = s_FusionID
                }
                else {
                    s_FusionID = ''
                }
                if (i_function != '' && i_function != null && i_function != 'undefined' && i_function != '- None -') {
                    i_function = i_function
                }
                else {
                    i_function = ''
                }
                
				if (i_ST != '' && i_ST != null && i_ST != 'undefined' && i_ST != '- None -') {
                    i_function = i_function
                }
                else {
                    i_ST = ''
                }
				
				if (i_OT != '' && i_OT != null && i_OT != 'undefined' && i_OT != '- None -') {
                    i_OT = i_OT
                }
                else {
                    i_OT = ''
                }
				
				if (i_DT != '' && i_DT != null && i_DT != 'undefined' && i_DT != '- None -') {
                    i_DT = i_DT
                }
                else {
                    i_DT = ''
                }
				
				if (i_Diff_ST != '' && i_Diff_ST != null && i_Diff_ST != 'undefined' && i_Diff_ST != '- None -') {
                    i_Diff_ST = i_Diff_ST
                }
                else {
                    i_Diff_ST = ''
                }
				
				if (i_Diff_OT != '' && i_Diff_OT != null && i_Diff_OT != 'undefined' && i_Diff_OT != '- None -') {
                    i_Diff_OT = i_Diff_OT
                }
                else {
                    i_Diff_OT = ''
                }
		
				
				
                if (i_Phone != '' && i_Phone != null && i_Phone != 'undefined' && i_Phone != '- None -') {
                    i_Phone = i_Phone
                }
                else {
                    i_Phone = ''
                }
                
                if (i_First_Name != '' && i_First_Name != null && i_First_Name != 'undefined' && i_First_Name != '- None -') {
                    i_First_Name = i_First_Name
                }
                else {
                    i_First_Name = ''
                }
                
                if (s_dept != '' && s_dept != null && s_dept != 'undefined' && s_dept != '- None -') {
                    s_dept = s_dept
                }
                else {
                    s_dept = ''
                }
                if (i_Inc_Id != '' && i_Inc_Id != null && i_Inc_Id != 'undefined' && i_Inc_Id != '- None -') {
                    i_Inc_Id = i_Inc_Id
                }
                else {
                    i_Inc_Id = ''
                }
                
                if (i_Email != '' && i_Email != null && i_Email != 'undefined' && i_Email != '- None -') {
                    i_Email = i_Email
                }
                else {
                    i_Email = ''
                }
                
                if (_logValidation(Employment_Category)) {
                    Employment_Category = Employment_Category
                }
                else {
                    Employment_Category = ''
                }
                
                if (s_ProjectState != '' && s_ProjectState != null && s_ProjectState != 'undefined' && s_ProjectState != '- None -') {
                    s_ProjectState = s_ProjectState
                }
                else {
                    s_ProjectState = ''
                }
                if (s_Customer != '' && s_Customer != null && s_Customer != 'undefined' && s_Customer != '- None -') {
                    s_Customer = s_Customer
                }
                else {
                    s_Customer = ''
                }
                if (i_week1 != '' && i_week1 != null && i_week1 != 'undefined' && i_week1 != '- None -') {
                    i_week1 = i_week1
                }
                else {
                    i_week1 = ''
                }
                if (i_week2 != '' && i_week2 != null && i_week2 != 'undefined' && i_week2 != '- None -') {
                    i_week2 = i_week2
                }
                else {
                    i_week2 = ''
                }
                if (i_week3 != '' && i_week3 != null && i_week3 != 'undefined') {
                    i_week3 = i_week3
                }
                else {
                    i_week3 = ''
                }
                if (i_week4 != '' && i_week4 != null && i_week4 != 'undefined') {
                    i_week4 = i_week4
                }
                else {
                    i_week4 = ''
                }
                
            }/////for loop of columns of employee search......
            var notsubmittedweek = new Array();
            var notsubmittedweek1 = new Array();
            
            /////time sheet data loop starts
            if (timesheetdata != null && timesheetdata != '' && timesheetdata != 'undefined') {
                //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'timesheetdata  -->' + timesheetdata.length);
                ///for (var d = middlecounteroftime; d < 100; d++) 
                for (var d = middlecounteroftime; d < timesheetdata.length; d++) {
                    //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'd  -->' + d);
                    
                    var i_EmployeeID = ''
                    var i_TimeCalendarWeek = ''
                    var i_Duration = ''
                    var i_EmpIntID = ''
                    var i_ProjectName = ''
                    var s_manager_name = '';
                    var s_manager_email = '';
                    
                    var a_search_timedata = timesheetdata[d];
                    var columns = a_search_timedata.getAllColumns();
                    
                    //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + a_search_timedata.getValue('internalid', 'employee', 'group'));
                    
                    var columntimeLen = columns.length;
                    for (var k = 0; k < columntimeLen; k++) {
                    
                        var column_time = columns[k];
                        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'column  -->' + column);
                        var label = column_time.getLabel();
                        //nlapiLogExecution('DEBUG', 'search', '**name*=' + column_time.getName() + '**join*=' + column_time.getJoin());
                        
                        var value = a_search_timedata.getValue(column_time)
                        var text = a_search_timedata.getText(column_time)
                        //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'value  -->' + value);
                        if (label == 'TimeCalendarWeek') {
                            i_TimeCalendarWeek = value
                        }
                        if (label == 'EmployeeID') {
                            i_EmployeeID = text;
                        }
                        if (label == 'Duration') {
                            i_Duration = value;
                        }
                        if (label == 'EmpIntID') {
                            i_EmpIntID = value;
                        }
                        if (label == 'Project Name') {
                            i_ProjectName = value;
                        }
                        if (label == 'Manager Name') {
                            s_manager_name = text;
                        }
                        if (label == 'Manager Email') {
                            s_manager_email = value;
                        }
                        
                    }//////////////////////end of for loop
                    //nlapiLogExecution('DEBUG', 'SearchData', '==e_empid==' + i_empid + 'i_EmpIntID==' + i_EmpIntID);
                    
                    //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'i_empid  -->' + i_empid + '  i_EmpIntID  -->' + i_EmpIntID + '===e_Project=' + e_Project + '==i_ProjectName==' + i_ProjectName);
                    if (parseInt(i_empid) > parseInt(i_EmpIntID))////if the employee id is ahead 34 of time employeeid 33 then start from next time entry
                    {
                        middlecounteroftime = parseInt(d + 1);
                        ////break;
                    }
                    else //////
                    {
                        if (i_empid == i_EmpIntID) //if employee time sheet id and project record time sheet found then go ahead
                        {
                        
                            //nlapiLogExecution('DEBUG', 'timesheetdata', 'a_search_timedata= ' + a_search_timedata);
                            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'i_TimeCalendarWeek  -->' + i_TimeCalendarWeek);
                            var flag = calculateposition.toString().indexOf(i_TimeCalendarWeek)
                            
                            ///get matched week time sheet data and week.
                            var i_timesheetdate = Cal_Timesheetdate(enddate1, i_TimeCalendarWeek)
                            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'i_timesheetdate  -->' + i_timesheetdate);
                            if (e_Project == i_ProjectName)///if its same project for same employee ,collect all sundays for all project
                            {
                                i_TimeCalendarWeek = nlapiStringToDate(i_TimeCalendarWeek)
                                
                                i_nextweekdate = nlapiAddDays(i_TimeCalendarWeek, 7); //to get next sunday as previous week text shown in search
                                i_nextweekdate = nlapiDateToString(i_nextweekdate)
                                
                                Time_Bothweeksundays.push(i_nextweekdate);
                            }
                            else {
                                ///Time_Bothweeksundays=new Array();
                                middlecounteroftime = parseInt(d);
                                break;
                            }
                            
                            //nlapiLogExecution('DEBUG', 'search_ComplaintCategory', 'Time_Bothweeksundays  -->' + Time_Bothweeksundays);
                        
                        
                        }
                        else {
                        
                            ///*********************************************************************************************************
                            //nlapiLogExecution('DEBUG', 'SearchData', 'Time_Bothweeksundays=' + Time_Bothweeksundays);
                            ///*********************************************************************************************************
                            for (var temp = 0; temp < calculatepositiontemp.length; temp++) {
                                var sunday = calculatepositiontemp[temp].toString().split('##');
                                //nlapiLogExecution('DEBUG', 'SearchData', 'valueofconst=' + calculatepositiontemp[temp] + 'searching..' + Time_Bothweeksundays.toString().search(sunday[0]));
                                if (Time_Bothweeksundays != null && Time_Bothweeksundays != '' && Time_Bothweeksundays != undefined) {
                                
                                    if (Time_Bothweeksundays.toString().search(sunday[0]) != -1)///if sunday found in array then remove
                                    {
                                        //nlapiLogExecution('DEBUG', 'SearchData', 'calculatepositiontemp[c]=' + calculatepositiontemp[temp]);
                                        calculatepositionTimeweek(sunday[0], calculateposition);
                                    }
                                }
                                
                            }
                            //nlapiLogExecution('DEBUG', 'SearchData', 'calculateposition=' + calculateposition);
                            var s_weekcolumn = '';
                            
                            ///if we have non submitted dates of sundays then 
                            if (i_empid != sublist1.getLineItemValue('custevent_employeeid', linenumber - 1)) {
                            
                            
                                //nlapiLogExecution('DEBUG', 'SearchData', 'calculateposition=' + calculateposition);
                                for (var weeksinmon = 0; weeksinmon < calculateposition.length; weeksinmon++) {
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'calculateposition=');
                                    notsubmittedweek.push(calculateposition[weeksinmon]);
                                }
                                //nlapiLogExecution('DEBUG', 'SearchData', 'else block notsubmittedweek=' + notsubmittedweek + 'Length==' + notsubmittedweek.length);
                                
                                if (notsubmittedweek != null && notsubmittedweek != undefined && notsubmittedweek != '') ////&& notsubmittedweek.length==4
                                {
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'Create line and print weeks');
                                    sublist1.setLineItemValue('custevent_gciid', linenumber, i_Inc_Id);
                                    sublist1.setLineItemValue('custevent_fusid', linenumber, s_FusionID);
                                    sublist1.setLineItemValue('custevent_employeeid', linenumber, i_empid);
                                    sublist1.setLineItemValue('custevent_persontype', linenumber, Person_Type);
                                    
                                    sublist1.setLineItemValue('custevent_firstname', linenumber, i_First_Name);
                                    sublist1.setLineItemValue('custevent_lastname', linenumber, i_Last_Name);
                                    sublist1.setLineItemValue('custevent_phone', linenumber, i_Phone);
                                    sublist1.setLineItemValue('custevent_email', linenumber, i_Email);
                                    sublist1.setLineItemValue('custevent_function', linenumber, i_function);
									
									sublist1.setLineItemValue('custevent_ST', linenumber, i_ST);
									sublist1.setLineItemValue('custevent_OT', linenumber, i_OT);
									sublist1.setLineItemValue('custevent_DT', linenumber, i_DT);
									sublist1.setLineItemValue('custevent_Diff_ST', linenumber, i_Diff_ST);
									sublist1.setLineItemValue('custevent_Diff_OT', linenumber, i_Diff_OT);
									
									
									
									
                                    //nlapiLogExecution('DEBUG', 'SearchData', 'length=' + notsubmittedweek.length);
                                    
                                    
                                    for (t = 0; t < notsubmittedweek.length; t++) {
                                        var notsubmitteddates = notsubmittedweek[t].toString().split('##');
                                        if (notsubmitteddates[1] == '1') {
                                            s_weekcolumn = 'custevent_wk1'
                                        }
                                        if (notsubmitteddates[1] == '2') {
                                            s_weekcolumn = 'custevent_wk2'
                                        }
                                        if (notsubmitteddates[1] == '3') {
                                            s_weekcolumn = 'custevent_wk3'
                                        }
                                        if (notsubmitteddates[1] == '4') {
                                            s_weekcolumn = 'custevent_wk4'
                                        }
                                        //nlapiLogExecution('DEBUG', 'SearchData', 's_weekcolumn=' + s_weekcolumn + '====' + linenumber);
                                        sublist1.setLineItemValue(s_weekcolumn, linenumber, notsubmitteddates[0]);
                                        
                                    }///////for loop of not submitted week array and seeting weeks
                                    sublist1.setLineItemValue('custevent_project', linenumber, i_project);
                                    sublist1.setLineItemValue('custevent_manager_nm', linenumber, s_manager_name);
                                    sublist1.setLineItemValue('custevent_manager_eml', linenumber, s_manager_email);
                                    sublist1.setLineItemValue('custevent_projectstate', linenumber, s_ProjectState);
                                    sublist1.setLineItemValue('custevent_dept', linenumber, s_dept);
                                    sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
                                    sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                                    linenumber++;
                                }
                            }///if next project is not for same employee and no time sheet for prev for any of weeks.thn only put on emplo one project entry
                            Time_Bothweeksundays = new Array();
                            middlecounteroftime = parseInt(d);
                            //}
                            
                            //nlapiLogExecution('DEBUG', 'SearchData', 'middlecounteroftime===' + middlecounteroftime)
                            break;
                        }////end of if employee time sheet id and project record matches
                    }///if else if emplioyee id is equal or less
                }////For loop of time sheet data array
            }//////end of if time sheet data found
            else {
                ////middlecounteroftime++;
                
                sublist1.setLineItemValue('custevent_gciid', linenumber, i_Inc_Id);
                sublist1.setLineItemValue('custevent_fusid', linenumber, s_FusionID);
                sublist1.setLineItemValue('custevent_employeeid', linenumber, i_empid); //
                sublist1.setLineItemValue('custevent_persontype', linenumber, Person_Type);
                sublist1.setLineItemValue('custevent_firstname', linenumber, i_First_Name);
                sublist1.setLineItemValue('custevent_lastname', linenumber, i_Last_Name);
                sublist1.setLineItemValue('custevent_phone', linenumber, i_Phone);
                sublist1.setLineItemValue('custevent_email', linenumber, i_Email);
                sublist1.setLineItemValue('custevent_function', linenumber, i_function);
				sublist1.setLineItemValue('custevent_ST', linenumber, i_ST);
				sublist1.setLineItemValue('custevent_OT', linenumber, i_OT);
				sublist1.setLineItemValue('custevent_DT', linenumber, i_DT);
				sublist1.setLineItemValue('custevent_Diff_ST', linenumber, i_Diff_ST);
				sublist1.setLineItemValue('custevent_Diff_OT', linenumber, i_Diff_OT);
				

				
                for (t = 0; t < calculate.length; t++) {
                    sublist1.setLineItemValue('custevent_wk' + (t + 1), linenumber, calculate[t]);
                    
                }
                
                sublist1.setLineItemValue('custevent_project', linenumber, i_project)
                //sublist1.setLineItemValue('custevent_manager_nm', linenumber, s_manager_nm);
                //sublist1.setLineItemValue('custevent_manager_eml', linenumber, s_manager_eml);
                sublist1.setLineItemValue('custevent_projectstate', linenumber, s_ProjectState);
                sublist1.setLineItemValue('custevent_dept', linenumber, s_dept);
                sublist1.setLineItemValue('custevent_customer', linenumber, s_Customer);
                sublist1.setLineItemValue('custevent_employeetype', linenumber, Employment_Category);
                linenumber++;
            }////end of else if time sheet data nt found at all....
        }////end of for loop of employee
    }///end of null values check for employee array
    //nlapiLogExecution('DEBUG', 'SearchData', 'a_result_array===' + a_result_array.length)
    return a_result_array;
}/////End of function

function calculatepositionTimeweek(i_TimeCalendarWeek, calculateposition){
    //i_TimeCalendarWeek = i_TimeCalendarWeek.toString();
    //declare flag
    //check if date is present in array
    //if equal to then set flag=1
    //check flag =1 return 0
    
    var i_nextweekdate = ''
    //i_TimeCalendarWeek = nlapiStringToDate(i_TimeCalendarWeek)
    //i_nextweekdate = nlapiAddDays(i_TimeCalendarWeek, 7)
    //i_nextweekdate = nlapiDateToString(i_nextweekdate)
    
    for (b = 0; calculateposition != null && b < calculateposition.length; b++) {
        var SplitPOSArray = new Array();
        SplitPOSArray = calculateposition[b].split('##');
        //nlapiLogExecution('DEBUG', 'SearchData', 'SplitPOSArray[0]===' + SplitPOSArray[0] + '==b==' + b)
        if (SplitPOSArray[0] == i_TimeCalendarWeek) {
            calculateposition.splice(b, 1);
            //return SplitPOSArray[1];
            //break;
        }
        else {
            //return null;
            //break;
        }
    }
    //nlapiLogExecution('DEBUG', 'calculatepositionTimeweek', 'calculateposition ===' + calculateposition)
}

function SearchEmployeeData(s_startdate, s_enddate) //
{
    var e_column = new Array();
    var e_filters = new Array();
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        //e_filters[0] = new nlobjSearchFilter('startdate', 'resourceallocation', 'notafter', s_enddate); ///is not after 9/28.14
        //e_filters[1] = new nlobjSearchFilter('enddate', 'resourceallocation', 'notbefore', s_startdate); ////is not before 1sept
        //e_filters[2] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept
		
		e_filters[0] = new nlobjSearchFilter('startdate', null, 'notafter', s_enddate); ///is not after 9/28.14
        e_filters[1] = new nlobjSearchFilter('enddate', null, 'notbefore', s_startdate); ////is not before 1sept
		e_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T); ////is not before 1sept

        //searchresult_T = nlapiSearchRecord('employee', 'customsearch_not_submitted_all_employe_2', e_filters, null);
		searchresult_T = nlapiSearchRecord('resourceallocation', 'customsearch_non_submitted_ts_by_alloc_2', e_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            var temp_emp = '';
            var emp = '';
            //nlapiLogExecution('DEBUG', 'researchcodes', 'searchresult_T==' + searchresult_T.length);
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[0]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        //temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal.push(searchresult[i]);
                //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult[searchresult.length - 1].getValue('internalid', null, 'group');
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastis==' + LastfetchedInternalid_T);
        }
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '')
    {
    
    }
    return searchresultFinal_T;
}

function SearchTimeData(s_startdate, s_enddate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        var a_filters = new Array();
        a_filters[0] = new nlobjSearchFilter('date', null, 'onorbefore', s_enddate);
        a_filters[1] = new nlobjSearchFilter('date', null, 'onorafter', s_startdate);
        a_filters[2] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all_2', a_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'researchcodes', 'searchresult_T == ' + searchresult_T.length);
            var temp_emp = '';
            var emp = '';
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[2]);
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal_T.push(searchresult_T[i]);
                //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
            //nlapiLogExecution('DEBUG', 'researchcodes', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
        //var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all', a_filters, null);
        //return searchresult_T
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '')
    {
    
    }
    return searchresultFinal_T;
    
}

function SearchTimeData_New(s_startdate, s_enddate) //
{
    var LastfetchedInternalid_T = 0;
    var searchresultFinal_T = new Array();
    var searchresult_T = new Array();
    
    do //
    {
        var a_filters = new Array();
        
        a_filters[a_filters.length] = new nlobjSearchFilter('date', null, 'onorafter', s_startdate);
        a_filters[a_filters.length] = new nlobjSearchFilter('date', null, 'onorbefore', s_enddate);
        a_filters[a_filters.length] = new nlobjSearchFilter('approvalstatus', null, 'anyof', ['2', '3']);
        a_filters[a_filters.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', LastfetchedInternalid_T);////is not before 1sept
        //nlapiLogExecution('DEBUG', 'researchcodes', 'LastfetchedInternalid_T == ' + LastfetchedInternalid_T);
        
        var searchresult_T = nlapiSearchRecord('timeentry', 'customsearch_salnonstdrptts_3_all_2_te_2', a_filters, null);
        
        if (searchresult_T != null && searchresult_T != undefined && searchresult_T != '') //
        {
            //nlapiLogExecution('DEBUG', 'researchcodes', 'searchresult_T == ' + searchresult_T.length);
            var temp_emp = '';
            var emp = '';
            
            for (var i = 0; i < searchresult_T.length; i++) //
            {
                var a_search_empdata = searchresult_T[i];
                var all_columns = a_search_empdata.getAllColumns();
                emp = a_search_empdata.getValue(all_columns[1]); // fetch emp id from search result
                //nlapiLogExecution('DEBUG', 'SearchTimeData', 'i : ' + i + ' emp ==' + emp);
                
                if (emp != null) //
                {
                    if (i == 0) //
                    {
                        temp_emp = emp;
                    }
                    
                    LastfetchedInternalid_T = temp_emp;
                    //nlapiLogExecution('DEBUG', 'SearchData', 'i : ' + i + ' temp_emp ==' + temp_emp);
                    if (emp == temp_emp) //if employee id is same then add to final array
                    {
                        searchresultFinal_T.push(searchresult_T[i]);
                    }
                    else //else employee id does nt match
                    {
                        //nlapiLogExecution('DEBUG', 'SearchData', 'Emp change row number : ' + i);
                        if (i > 950)//
                        {
                            //nlapiLogExecution('DEBUG', 'SearchData', 'i==' + i);
                            //nlapiLogExecution('DEBUG', 'SearchData', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
                            break;
                        }
                        else //
                        {
                            searchresultFinal_T.push(searchresult_T[i]);
                            temp_emp = emp;
                            LastfetchedInternalid_T = temp_emp;
                        }
                    }
                }
                //searchresultFinal_T.push(searchresult_T[i]);
                //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            }
            //nlapiLogExecution('DEBUG', 'researchcodes', 'lastid==' + a_search_empdata.getValue('internalid', null, 'group'));
            //LastfetchedInternalid_T = searchresult_T[searchresult_T.length - 1].getValue('internalid', 'employee', 'group');
            //nlapiLogExecution('DEBUG', 'researchcodes', 'LastfetchedInternalid_T==' + LastfetchedInternalid_T);
        }
        //var searchresult_T = nlapiSearchRecord('timebill', 'customsearch_salnonstdrptts_3_all', a_filters, null);
        //return searchresult_T
    }
    while (searchresult_T != null && searchresult_T != undefined && searchresult_T != '')
    {
    
    }
    return searchresultFinal_T;
    
}

var manager_email_Array = new Array();

function get_Manager_Email() //
{
	var search_result = nlapiSearchRecord('job', 'customsearch_manager_with_email', null, null);
	
	if (_logValidation(search_result)) //
	{
		for (var i = 0; i < search_result.length; i++) //
		{
			var result = search_result[i];
			var all_columns = result.getAllColumns();
			
			manager_email_Array[i] = result.getValue(all_columns[1]) + "##" + result.getValue(all_columns[2]);
		}
		nlapiLogExecution('DEBUG', 'get_Manager_Email', 'manager_email_Array count : ' + manager_email_Array.length);
	}
}

/*
 Function name :-getWeekNumber()
 Parameter :- date
 return type :- returns week number of given year
 */
function getWeekNumber(d){
    //Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    //Set to nearest Thursday: current date + 4 - current day number
    //Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    //Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    //Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'weekNo=' + weekNo);
    //Return array of year and week number
    return weekNo;
}

function Callsuitelet(sublist1, param){
    var count = sublist1.getLineItemCount();
    //alert(count);
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'param' + param);
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'count' + count);
    //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'linenumber' + linenumber);
    var globalArray = new Array();
    var arr = new Array();
    
    for (var i = 1; i <= count; i++) {
        var gciid = sublist1.getLineItemValue('custevent_gciid', i);
        var fusid = sublist1.getLineItemValue('custevent_fusid', i);
        var firstname = sublist1.getLineItemValue('custevent_firstname', i);
        var lastname = sublist1.getLineItemValue('custevent_lastname', i);
        var phone = sublist1.getLineItemValue('custevent_phone', i);
        var email = sublist1.getLineItemValue('custevent_email', i);
        var function1 = sublist1.getLineItemValue('custevent_function', i);
        var week1 = sublist1.getLineItemValue('custevent_wk1', i);
        var week2 = sublist1.getLineItemValue('custevent_wk2', i);
        var week3 = sublist1.getLineItemValue('custevent_wk3', i);
        var week4 = sublist1.getLineItemValue('custevent_wk4', i);
        var project = sublist1.getLineItemValue('custevent_project', i);
		var manager_name = sublist1.getLineItemValue('custevent_manager_nm', i);
		var manager_email = sublist1.getLineItemValue('custevent_manager_eml', i);
		var ts_approver = sublist1.getLineItemValue('custevent_ts_approver_name', i);
        var projectstate = sublist1.getLineItemValue('custevent_projectstate', i);
        var employeeType = sublist1.getLineItemValue('custevent_employeetype', i);
        var personType = sublist1.getLineItemValue('custevent_persontype', i);
        var dept = sublist1.getLineItemValue('custevent_dept', i);
        var customer = sublist1.getLineItemValue('custevent_customer', i);
		var term_date = sublist1.getLineItemValue('custevent_term_date', i);
		var hire_date = sublist1.getLineItemValue('custevent_hire_date', i);
		
		var ST = sublist1.getLineItemValue('custevent_ST', i);
		var OT = sublist1.getLineItemValue('custevent_OT', i);
		var  DT= sublist1.getLineItemValue('custevent_DT', i);
		var Diff_ST = sublist1.getLineItemValue('custevent_Diff_ST', i);
		var Diff_OT = sublist1.getLineItemValue('custevent_Diff_OT', i);
		
		
		
        customer = customer.toString().replace(/[,]/g, " ")
        if (gciid != null && gciid != '' && gciid != 'undefined') {
            gciid = gciid
        }
        else {
            gciid = ''
        }
        if (project != null && project != '' && project != 'undefined') {
            project = project
        }
        else {
            project = ''
        }
        if (function1 != null && function1 != '' && function1 != 'undefined') {
            function1 = function1
        }
        else {
            function1 = ''
        }
		
		if (ST != null && ST != '' && ST != 'undefined') {
            ST = ST
        }
        else {
            ST = ''
        }
		
			
		if (OT != null && OT != '' && OT != 'undefined') {
            OT = OT
        }
        else {
            OT = ''
        }
		
			
		if (DT != null && DT != '' && DT != 'undefined') {
            DT = DT
        }
        else {
            DT = ''
        }
		
			
		if (Diff_ST!= null && Diff_ST != '' && Diff_ST != 'undefined') {
            Diff_ST = Diff_ST
        }
        else {
            Diff_ST = ''
        }
		
		if (Diff_OT!= null && Diff_OT != '' && Diff_OT != 'undefined') {
            Diff_OT = Diff_OT
        }
        else {
            Diff_OT = ''
        }
	
		
        if (fusid != null && fusid != '' && fusid != 'undefined') {
            fusid = fusid
        }
        else {
            fusid = ''
        }
        if (firstname != null && firstname != '' && firstname != 'undefined') {
            firstname = firstname
        }
        else {
            firstname = ''
        }
        if (lastname != null && lastname != '' && lastname != 'undefined') {
            lastname = lastname
        }
        else {
            lastname = ''
        }
        if (phone != null && phone != '' && phone != 'undefined') {
            phone = phone
        }
        else {
            phone = ''
        }
        if (email != null && email != '' && email != 'undefined') {
            email = email
        }
        else {
            email = ''
        }
        if (week1 != null && week1 != '' && week1 != 'undefined') {
            week1 = week1
        }
        else {
            week1 = ''
        }
        if (week2 != null && week2 != '' && week2 != 'undefined') {
            week2 = week2
        }
        else {
            week2 = ''
        }
        if (week3 != null && week3 != '' && week3 != 'undefined') {
            week3 = week3
        }
        else {
            week3 = ''
        }
        if (week4 != null && week4 != '' && week4 != 'undefined') {
            week4 = week4
        }
        else {
            week4 = ''
        }
        if (projectstate != null && projectstate != '' && projectstate != 'undefined') {
            projectstate = projectstate
        }
        else {
            projectstate = ''
        }
        if (dept != null && dept != '' && dept != 'undefined') {
            dept = dept
        }
        else {
            dept = ''
        }
        if (customer != null && customer != '' && customer != 'undefined') {
            customer = customer
        }
        else {
            customer = ''
        }
        if (employeeType != null && employeeType != '' && employeeType != 'undefined') {
            employeeType = employeeType
        }
        else {
            employeeType = ''
        }
        if (personType != null && personType != '' && personType != 'undefined') {
            personType = personType
        }
        else {
            personType = ''
        }
		if (term_date != null && term_date != '' && term_date != 'undefined') {
            term_date = term_date
        }
        else {
            term_date = ''
        }
		if (hire_date != null && hire_date != '' && hire_date != 'undefined') {
            hire_date = hire_date
        }
        else {
            hire_date = ''
        }
        if (i == 1) //
		{
			//arr[i] = "GCI ID,Fusion Id,First Name,Last Name,Phone,Email,Function,Week1,Week2,Week3,Week4,Project,Manager Name,Manager Email,Project State,Department,Client,Employee Type,Person Type,Termination Date" + "\n" + gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," + week3 + "," + week4 + "," + project + "," + projectstate + "," + dept + "," + customer + "," + employeeType + "," + personType + "," + term_date;
			arr[i] = "GCI ID,Fusion Id,First Name,Last Name,Phone,Email,Function," + d_end_wk_1 + "," + d_end_wk_2 + "," + d_end_wk_3 + "," + d_end_wk_4 + ",Project,Manager Name,Manager Email,Timesheet Approver,Project State,Department,Client,Employee Type,Person Type,Termination Date,Hire Date,ST,OT,DT,Diff_ST,Diff_OT" + "\n" + gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," + week3 + "," + week4 + "," + project + "," + manager_name + "," + manager_email + "," + projectstate + "," + dept + "," + customer + "," + employeeType + "," + personType + "," + term_date + "," + hire_date +","+ ST +","+ OT +","+ DT +","+ Diff_ST +","+ Diff_OT;
		}
		else //
		{
			arr[i] = "\n" + gciid + "," + fusid + "," + firstname + "," + lastname + "," + phone + "," + email + "," + function1 + "," + week1 + "," + week2 + "," + week3 + "," + week4 + "," + project + "," + manager_name + "," + manager_email + "," + ts_approver + "," + projectstate + "," + dept + "," + customer + "," + employeeType + "," + personType + "," + term_date + "," + hire_date +","+ ST +","+ OT +","+ DT +","+ Diff_ST +","+ Diff_OT;
		}
        //nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr[i]);
        
        globalArray[i - 1] = (arr[i]);
    }
    
    var fileName = 'Not Submitted TS rpt- From TS'
    var Datetime = new Date();
    var CSVName = fileName + " - " + Datetime + '.csv';
    var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
    
    response.setContentType('CSV', CSVName);
    response.write(file.getValue());
    
}

function calculateSundays(s_startdate, s_enddate){
    var sundayarray = new Array();
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    //////s_startdate= s_enddate
    var date = nlapiStringToDate(s_startdate)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' date-->' + date);
    var day = date.getDay()
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var addday = getdays(day)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' addday-->' + addday);
    
    for (var i = 0; i < 4; i++) {
        s_startdate = nlapiStringToDate(s_startdate)
        if (i == 0) {
        
            s_startdate = nlapiAddDays(s_startdate, addday)
            s_startdate = nlapiDateToString(s_startdate)
            sundayarray.push(s_startdate)
        }
        else {
            s_startdate = nlapiAddDays(s_startdate, 7)
            s_startdate = nlapiDateToString(s_startdate)
            sundayarray.push(s_startdate)
        }
    }
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' sundayarray-->' + sundayarray);
    return sundayarray;
}

function Cal_Timesheetdate(s_startdate, i_calenderweek){
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' i_calenderweek-->' + i_calenderweek);
    var i_timsheetdate = ''
    i_calenderweek = nlapiStringToDate(i_calenderweek)
    i_timsheetdate = nlapiAddDays(i_calenderweek, 7)
    i_timsheetdate = nlapiDateToString(i_timsheetdate)
    return i_timsheetdate;
    
    
}

function Sundayswithweekposition(s_startdate, s_enddate){
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' s_startdate-->' + s_startdate);
    var position = new Array();
    
    var date = nlapiStringToDate(s_startdate)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' date-->' + date);
    var day = date.getDay()
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var addday = getdays(day)
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' addday-->' + addday);
    for (var i = 0; i < 4; i++) {
        s_startdate = nlapiStringToDate(s_startdate)
        if (i == 0) {
        
            s_startdate = nlapiAddDays(s_startdate, addday)
            s_startdate = nlapiDateToString(s_startdate)
            
            position.push(s_startdate + "##" + (i + 1))
            //position.push(s_startdate + "##" + (4 - i))
        }
        else {
            s_startdate = nlapiAddDays(s_startdate, 7)
            s_startdate = nlapiDateToString(s_startdate)
            position.push(s_startdate + "##" + (i + 1))
            //position.push(s_startdate + "##" + (4 - i))
        }
    }
    
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' position-->' + position);
    return position;
}

function getdays(day){
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' day-->' + day);
    var x = '';
    switch (day) {
        case 0:
            x = "7";
            break;
        case 1:
            x = "6";
            break;
        case 2:
            x = "5";
            break;
        case 3:
            x = "4";
            break;
        case 4:
            x = "3";
            break;
        case 5:
            x = "2";
            break;
        case 6:
            x = "1";
            break;
            
    }
    //nlapiLogExecution('DEBUG', 'suitlet_Po_ClosedStatus', ' x-->' + x);
    return x;
}

function EmployeeSearch(){


    var a_filters = new Array();
    
    
    
    var searchresult = nlapiSearchRecord('employee', 'customsearch1281', a_filters, null);
    return searchresult
    
}

function _logValidation(value){
    if (value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN) {
        return true;
    }
    else {
        return false;
    }
}
