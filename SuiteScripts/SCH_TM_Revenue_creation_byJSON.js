/**
 * @author Deepak MS Date:20 Feb 2017
 */

function scheduled_tm_create_forecast(type)
{
	try
	{
		nlapiLogExecution('DEBUG','test','test');
		var i_context = nlapiGetContext();
		
		var i_tm_json_id = i_context.getSetting('SCRIPT', 'custscript_tm_json_id');
		//var i_tm_json_id = 246;
		nlapiLogExecution('DEBUG','JSOn_search',i_tm_json_id);
		var JSOn_search= nlapiLoadRecord('customrecord_tm_month_total_json',i_tm_json_id);
		var data_array=[];
		var data_json_tm = [];
		//var monthBreakUp = getMonthsBreakup(
			//     nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_pro_st_date')),
			 //    nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_fp_rev_rec_proj_end_date')));
		var s_rampup_json_1_mnth = JSOn_search.getFieldValue('custrecord_rampupjson1');
		var s_rampup_json_2_mnth = JSOn_search.getFieldValue('custrecord_rampupjson2');
		var s_rampup_json_3_mnth = JSOn_search.getFieldValue('custrecord_rampupjson3');
		var s_rampup_json_4_mnth = JSOn_search.getFieldValue('custrecord_rampupjson4');
		var project_id = JSOn_search.getFieldValue('custrecord_project_json');
		var forecast_master_dataSearch = nlapiSearchRecord("customrecord_forecast_master_data", null, 
			[["custrecord_forecast_project_name", "anyof", project_id]], [
                    new nlobjSearchColumn("custrecord_forecast_department", null, "GROUP").setSort(false),
                    new nlobjSearchColumn("custrecord_forecast_month_startdate", null, "GROUP").setSort(false),
                    new nlobjSearchColumn("custrecord_forecast_amount", null, "SUM")
                ]);
		for(var search_indx = 0; search_indx < forecast_master_dataSearch.length; search_indx++)
		{
			var forecast_practice = forecast_master_dataSearch[search_indx].getValue("custrecord_forecast_department", null, "GROUP");
			var forecast_practice_text = forecast_master_dataSearch[search_indx].getText("custrecord_forecast_department", null, "GROUP");
			var forecast_date = forecast_master_dataSearch[search_indx].getValue("custrecord_forecast_month_startdate", null, "GROUP");
			var forecast_amount = forecast_master_dataSearch[search_indx].getValue("custrecord_forecast_amount", null, "SUM");
			data_json_tm.push({
					forecast_practice : forecast_practice_text,
					forecast_date : forecast_date,
					forecast_amount : forecast_amount
				});	
		}
		//data_array.push(data_json_tm);
		if(s_rampup_json_1_mnth)
		{
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
			var s_entire_json_clubed = JSON.parse(s_rampup_json_1_mnth);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					var rampList = a_row_json_data[i_row_json_index].rampList;
					var s_subprac_text = a_row_json_data[i_row_json_index].subprac_text;
					var i_project = a_row_json_data[i_row_json_index].project;
					var s_note = a_row_json_data[i_row_json_index].note;
					var f_revenue = a_row_json_data[i_row_json_index].allo;
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var i_year = a_row_json_data[i_row_json_index].year;
					if (!f_revenue) 
					f_revenue = 0;
				
					if (!rampList) 
						rampList = 'Total';
				}
			}
		}
		if(_logValidation(s_rampup_json_1_mnth))
		{
			create_record(s_entire_json_clubed,i_tm_json_id,project_id,data_json_tm);
		}
		if(s_rampup_json_2_mnth)
		{
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
						
			var s_entire_json_clubed = JSON.parse(s_rampup_json_2_mnth);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					var rampList = a_row_json_data[i_row_json_index].rampList;
					var s_subprac_text = a_row_json_data[i_row_json_index].subprac_text;
					var i_project = a_row_json_data[i_row_json_index].project;
					var s_note = a_row_json_data[i_row_json_index].note;
					var f_revenue = a_row_json_data[i_row_json_index].allo;
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var i_year = a_row_json_data[i_row_json_index].year;
					if (!f_revenue) 
					f_revenue = 0;
				
					if (!rampList) 
						rampList = 'Total';				
				}
			}
		}
		if(_logValidation(s_rampup_json_2_mnth))
		{
		create_record(s_entire_json_clubed,i_tm_json_id,project_id,data_json_tm);
		}
		if(s_rampup_json_3_mnth)
		{
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
			var s_entire_json_clubed = JSON.parse(s_rampup_json_3_mnth);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					var rampList = a_row_json_data[i_row_json_index].rampList;
					var s_subprac_text = a_row_json_data[i_row_json_index].subprac_text;
					var i_project = a_row_json_data[i_row_json_index].project;
					var s_note = a_row_json_data[i_row_json_index].note;
					var f_revenue = a_row_json_data[i_row_json_index].allo;
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var i_year = a_row_json_data[i_row_json_index].year;
					if (!f_revenue) 
					f_revenue = 0;
				
					if (!rampList) 
						rampList = 'Total';
				}
			}
		}
		if(_logValidation(s_rampup_json_3_mnth))
		{
		create_record(s_entire_json_clubed,i_tm_json_id,project_id,data_json_tm);
		}
		if(s_rampup_json_4_mnth)
		{
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
			var s_entire_json_clubed = JSON.parse(s_rampup_json_4_mnth);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					var rampList = a_row_json_data[i_row_json_index].rampList;
					var s_subprac_text = a_row_json_data[i_row_json_index].subprac_text;
					var i_project = a_row_json_data[i_row_json_index].project;
					var s_note = a_row_json_data[i_row_json_index].note;
					var f_revenue = a_row_json_data[i_row_json_index].allo;
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var i_year = a_row_json_data[i_row_json_index].year;
					if (!f_revenue) 
					f_revenue = 0;
				
					if (!rampList) 
						rampList = 'Total';
				}
			}
		}
		if(_logValidation(s_rampup_json_4_mnth))
		{
		create_record(s_entire_json_clubed,i_tm_json_id,project_id,data_json_tm);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','scheduled_create_effort_plan','ERROR MESSAGE :- '+err);
	}
}



function create_record(s_entire_json_clubed,i_tm_json_id,project_id,data_json_tm)
{
	try
	{
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_get_exsiting_effort_plan = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				
			for (var i_effort_index = 0; i_effort_index < a_get_exsiting_effort_plan.length;i_effort_index++)
			{
				//for (var j = 0; j < monthBreakUp.length; j++)
				{
					var sub_practice_share='';
					var dta_rw=[];
					var dta_arr =[];
					var rampList = a_get_exsiting_effort_plan[i_effort_index].rampList;
					var subprac_text = a_get_exsiting_effort_plan[i_effort_index].subprac_text;
					var project = a_get_exsiting_effort_plan[i_effort_index].project;
					var note = a_get_exsiting_effort_plan[i_effort_index].note;
					var allo=a_get_exsiting_effort_plan[i_effort_index].allo;
					var mnth = a_get_exsiting_effort_plan[i_effort_index].mnth;
					var i_year=a_get_exsiting_effort_plan[i_effort_index].year;		
					if(!_logValidation(rampList))
					{
						rampList = 'Total';
					}
					if(!_logValidation(subprac_text))
					{
						subprac_text = '';
						for(sub_ind = 0; sub_ind < data_json_tm.length; sub_ind++)
						{
							var practice = data_json_tm[sub_ind].forecast_practice;
							var month = data_json_tm[sub_ind].forecast_date;
							var strng_date = nlapiStringToDate(month);
                         	var pr_year= strng_date. getFullYear();
							month = getMonthName(month);
							if((month == mnth) &&(pr_year == i_year))
							{
								sub_practice_share = data_json_tm[sub_ind].forecast_amount;
								dta_rw.push({
									practice : practice,
									share : sub_practice_share
								});
							}
						}
						//dta_arr.push(dta_rw);
					}
					/*else
					{
						for(sub_ind = 0; sub_ind < data_json_tm.length; sub_ind++)
						{
							var practice = data_json_tm[sub_ind].forecast_practice;
							if(practice == subprac_text)
							{
								var month = data_json_tm[sub_ind].forecast_date;
								month = getMonthName(month);
								if(month == mnth)
								{
									sub_practice_share= data_json_tm[sub_ind].forecast_amount;
								}
							}
						}
					}*/
					if(!_logValidation(note))
					{
						note = '';
					}
					nlapiLogExecution('DEBUG','Practice share',dta_rw);
					
					var o_actual_effrt_mnth_end = nlapiCreateRecord('customrecord_forecast_revenue_tm');
					o_actual_effrt_mnth_end.setFieldValue('custrecord_rampup_type', rampList);
					o_actual_effrt_mnth_end.setFieldText('custrecord_tm_sub_practice', subprac_text.replace( /[\r\n]+/gm, "" ));
					o_actual_effrt_mnth_end.setFieldValue('custrecord_tm_data_project', project_id);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_tm_notes', note);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_tm_amount', allo);				
					o_actual_effrt_mnth_end.setFieldValue('custrecord_tm_month', mnth);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_tm_year', i_year);
					o_actual_effrt_mnth_end.setFieldValue('custrecord_tm_json_parent', i_tm_json_id);
					//Set bllank array for Blank practice in total sub practice revenue share
					if(_logValidation(o_actual_effrt_mnth_end.getFieldValue('custrecord_rampup_type')) && _nullValidation(subprac_text)){
					o_actual_effrt_mnth_end.setFieldValue('custrecord_total_share',"[]");
					}else
					{
					o_actual_effrt_mnth_end.setFieldValue('custrecord_total_share', JSON.stringify(dta_rw));
					}
					var i_month_end_activity = nlapiSubmitRecord(o_actual_effrt_mnth_end);
					
				}
				
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','create_record','ERROR MESSAGE :- '+err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}
function get_current_month_end_date(i_month,i_year)
{
	var d_start_date='';
	var d_end_date = '';
	var date_st_end=[];
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
				date_st_end.push({
				d_start_date :i_year + '-' + 1 + '-' + 1,	
			d_end_date : i_year + '-' + 1 + '-' + 31
				});
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
				date_st_end.push({
				d_start_date : 1 + '/' + 1 + '/' + i_year,
			d_end_date : 31 + '/' + 1 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date :1 + '/' + 1 + '/' + i_year,
			d_end_date : 1 + '/' + 31 + '/' + i_year
			});
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 2 + '-' + 1,	
			d_end_date : i_year + '-' + 2 + '-' + i_day
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 2 + '/' + i_year,
			d_end_date : i_day + '/' + 2 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:2 + '/' + 1 + '/' + i_year,
			d_end_date : 2 + '/' + i_day + '/' + i_year});
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 3 + '-' + 1,	
			d_end_date : i_year + '-' + 3 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 3 + '/' + i_year,
			d_end_date : 31 + '/' + 3 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:3 + '/' + 1 + '/' + i_year,
			d_end_date : 3 + '/' + 31 + '/' + i_year
			});
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 4 + '-' + 1,	
			d_end_date :i_year + '-' + 4 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 4 + '/' + i_year,
			d_end_date : 30 + '/' + 4 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date :4 + '/' + 1 + '/' + i_year,
			d_end_date : 4 + '/' + 30 + '/' + i_year
			});
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 5 + '-' + 1,		
			d_end_date : i_year + '-' + 5 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 5 + '/' + i_year,
			d_end_date : 31 + '/' + 5 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:5 + '/' + 1 + '/' + i_year,
			d_end_date : 5 + '/' + 31 + '/' + i_year
			});
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 6 + '-' + 1,		
			d_end_date : i_year + '-' + 6 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 6 + '/' + i_year,
			d_end_date : 30 + '/' + 6 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:6 + '/' + 1 + '/' + i_year,
			d_end_date : 6 + '/' + 30 + '/' + i_year
			});
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 7 + '-' + 1,		
			d_end_date : i_year + '-' + 7 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 7 + '/' + i_year,
			d_end_date : 31 + '/' + 7 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:7 + '/' + 1 + '/' + i_year,
			d_end_date : 7 + '/' + 31 + '/' + i_year
			});
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 8 + '-' + 1,		
			d_end_date : i_year + '-' + 8 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 8 + '/' + i_year,
			d_end_date : 31 + '/' + 8 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:8 + '/' + 1 + '/' + i_year,
			d_end_date : 8 + '/' + 31 + '/' + i_year
			});
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 9 + '-' + 1,	
			d_end_date : i_year + '-' + 9 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 9 + '/' + i_year,
			d_end_date : 30 + '/' + 9 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:9 + '/' + 1 + '/' + i_year,
			d_end_date : 9 + '/' + 30 + '/' + i_year
			});
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 10 + '-' + 1,		
			d_end_date : i_year + '-' + 10 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 10 + '/' + i_year,
			d_end_date : 31 + '/' + 10 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:10 + '/' + 1 + '/' + i_year,
			d_end_date : 10 + '/' + 31 + '/' + i_year
			});
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 11 + '-' + 1,		
			d_end_date : i_year + '-' + 11 + '-' + 30
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 11 + '/' + i_year,
			d_end_date : 30 + '/' + 11 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:11 + '/' + 1 + '/' + i_year,
			d_end_date : 11 + '/' + 30 + '/' + i_year
			});
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	date_st_end.push({
			d_start_date:i_year + '-' + 12 + '-' + 1,		
			d_end_date : i_year + '-' + 12 + '-' + 31
			});		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	date_st_end.push({
			d_start_date:1 + '/' + 12 + '/' + i_year,
			d_end_date : 31 + '/' + 12 + '/' + i_year
			});
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	date_st_end.push({
			d_start_date:12 + '/' + 1 + '/' + i_year,
			d_end_date : 12 + '/' + 31 + '/' + i_year
			});
	     }	 
			
	  }	
 }//Month & Year
 return date_st_end;
}
function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
///NULLVALIDATION FUNCTION///
function _nullValidation(value)
{
    if (value == null || value == 'NULL' || value == 'NaN' || value == "" || value == '' || value == undefined || value == '&nbsp;')
    {
        return true;
    }
    else
    {
        return false;
    }
}
