function TimeSheet_Notify_V()//
{
    nlapiLogExecution('ERROR', 'TimeSheet_Notify', '******* Execution Started *******');
    
    var context = nlapiGetContext();
    var usageRemaining = context.getRemainingUsage();
    
    var counter = context.getSetting('SCRIPT', 'custscript_counter_tm_rmndr1');
    nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'counter : ' + counter);
    
    var employee_number = 1;
    var initial_ID = 0;
    var today_date = getDate();
    
    try //
    {
        do //
        {
            if (validate(counter)) //
            {
                if (counter > 0) //
                {
                    initial_ID = counter;
                }
            }
            
            nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'before searching......');
            
            var filters = new Array();
            filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', initial_ID);
            nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'initial_ID : ' + initial_ID);
            
            var search_Result = nlapiSearchRecord('employee', 'customsearch_all_active_with_access_emp', filters, null);
            
            if (validate(search_Result)) //
            //if (validate(EmpList)) //
            {
                nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'search_Result.length : ' + search_Result.length);
                
                //nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'after searching......');
                
                //var emp_List_N = new Array();
                
                for (j = 0; j < search_Result.length; j++) //
                {
                    nlapiLogExecution('DEBUG', 'TimeSheet_Notify', '*******' + employee_number + '*******');
                    
                    var all_columns = search_Result[j].getAllColumns();
                    
                    var first_name = search_Result[j].getValue(all_columns[0]); // + ';' + search_Result[j].getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'first_name : ' + first_name);
                    
                    var email_ID = search_Result[j].getValue(all_columns[1]);
                    //nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'email_ID : ' + email_ID);
                    
                    var internal_ID = search_Result[j].getValue(all_columns[2]);
                    //nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'internal_ID : ' + internal_ID);
                    
                    if (validate(first_name) == false) //
                    {
                        first_name = ''
                    }
                    
                    var htmltext = '<table border="0" width="100%">';
                    htmltext += '<tr>';
                    htmltext += '<td colspan="4" valign="top">';
                    htmltext += '<p>Hi ' + first_name + ',</p>';
                    htmltext += '<p></p>';
                    htmltext += '<p>Please fill in time sheet for week ending ' + today_date + ' in NetSuite system.</p>';
                    htmltext += '<p></p>';
                    htmltext += '<p>If you have already filled, please ignore this mail.</p>';
                    htmltext += '<p></p>';
                    htmltext += '<p>For any system(NetSuite) related issues, Please write to <a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a></p>';
                    htmltext += '</td></tr>';
                    htmltext += '<tr></tr>';
                    htmltext += '<tr></tr>';
                    htmltext += '<tr></tr>';
                    htmltext += '<p>Regards,</p>';
                    htmltext += '<p>Information Systems</p>';
                    htmltext += '</table>';
                    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
                    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                    htmltext += '<tr>';
                    htmltext += '<td align="right">';
                    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
                    htmltext += '</td>';
                    htmltext += '</tr>';
                    htmltext += '</table>';
                    htmltext += '</body>';
                    htmltext += '</html>';
                    
                    //nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'htmltext : ' + htmltext);
                    
                    try //
                    {
                        var subject_line = 'Please fill your time sheet in NetSuite System';
                        //nlapiSendEmail(author, recipient, subject, body, cc, bcc, records, attachments, notifySenderOnBounce, internalOnly)
                        //nlapiSendEmail(442, 'vinod.n@brillio.com', subject_line, htmltext);
                        //nlapiSendEmail(442, 'vikrant@aashnacloudtech.com', subject_line, htmltext);
                        
                        nlapiSendEmail(442, email_ID, subject_line, htmltext);
                        
                        initial_ID = internal_ID; // this variable is used to send for new call of script on usage limit exceeds
						counter = internal_ID; // this variable will 
						
                        employee_number++;
                        
                        nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'Receipants Email ID : ' + email_ID);
                        
                        usageRemaining = context.getRemainingUsage();
                        nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' usageRemaining -->' + usageRemaining);
                        
                        if (usageRemaining < 60) // use this at time of sending to all...
                        //if (i == 10) // for testing purpose
                        {
                            // Define SYSTEM parameters to schedule the script to re-schedule.
                            var params = new Array();
                            params['status'] = 'scheduled';
                            params['runasadmin'] = 'T';
                            var startDate = new Date();
                            params['startdate'] = startDate.toUTCString();
                            
                            // Define CUSTOM parameters to schedule the script to re-schedule.
                            params['custscript_counter_tm_rmndr1'] = initial_ID;
                            
                            nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status --> Before schedule...');
                            
                            var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(), params);
                            nlapiLogExecution('DEBUG', 'TimeSheet_Notify ', ' Script Status -->' + status);
                            
                            ////If script is scheduled then successfully then check for if status=queued
                            if (status == 'QUEUED') //
                            {
                                nlapiLogExecution('DEBUG', ' TimeSheet_Notify', ' Script is rescheduled ....................');
                            }
                            
                            return;
                        }
                        
                        // COMMMENT BELOW LINE WHILE EXECUTING IN PRODUCTION 
                        //return;
                    } 
                    catch (ex)//
                    {
                        nlapiLogExecution('Error', 'TimeSheet_Notify', 'Not sent ID : ' + splitValues[1] + ' ex : ' + ex);
                        //nlapiLogExecution('Error', 'Sent E-Mail id', 'ex : ' + ex);
                    }
                }
            }
            else //
            {
                nlapiLogExecution('DEBUG', 'TimeSheet_Notify', 'search_Result is invalid : ' + search_Result);
            }
            
            //return;
        } //
        while (search_Result != null) //
        {
            nlapiLogExecution('ERROR', 'TimeSheet_Notify', '******* Execution Completed *******');
            return;
        }
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'Exception', 'Ex : ' + e);
        nlapiLogExecution('ERROR', 'Exception', 'Ex message : ' + e.message);
    }
}

function validate(value) //
{
    if (value != null && value != '' && value != 'undefined') // 
    {
        return true;
    }
    return false;
}

function getDate() //
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10)//
    {
        dd = '0' + dd
    }
    if (mm < 10)//
    {
        mm = getMonth(mm);
    }
    
    var today = mm + ' ' + dd + ',' + yyyy;
    nlapiLogExecution('DEBUG', 'getDate', 'today : ' + today);
    return today;
}

function getMonth(mm) //
{
    if (mm == 1)//
    {
        return 'January';
    }
    if (mm == 2)//
    {
        return 'February';
    }
    if (mm == 3)//
    {
        return 'March';
    }
    if (mm == 4)//
    {
        return 'April';
    }
    if (mm == 5)//
    {
        return 'May';
    }
    if (mm == 6)//
    {
        return 'June';
    }
    if (mm == 7)//
    {
        return 'July';
    }
    if (mm == 8)//
    {
        return 'August';
    }
    if (mm == 9)//
    {
        return 'September';
    }
    if (mm == 10)//
    {
        return 'October';
    }
    if (mm == 11)//
    {
        return 'November';
    }
    if (mm == 12)//
    {
        return 'December';
    }
}
