// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_Invoice_button_show
	Author: Vikrant S. Adhav	
	Company: Aashna Cloudtech
	Date: 23-04-2014
	Description: Initial draft for creating buttons of Approve and Reject on Invoice.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord_Button(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_Button(type,form)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY
    
    if (type == 'view') //
    {
        var approver = nlapiGetFieldValue('custbody_draft_invoice_app');
		nlapiLogExecution('DEBUG', 'button show', 'approver : ' + approver);
		
        var current_user = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'button show', 'current_user : ' + current_user);
		
		var approval_status = nlapiGetFieldValue('custbody_invoicestatus');
		nlapiLogExecution('DEBUG', 'button show', 'approval_status : ' + approval_status);
		
        if (approver == current_user && approval_status==3) //
        {
            form.setScript('customscript_approval_button_click');
            
            form.addButton('custpage_approve', 'Approve', 'btn_approve_call(' + nlapiGetRecordId() + ');');
            form.addButton('custpage_reject', 'Reject', 'btn_reject_call(' + nlapiGetRecordId() + ');');
            //form.addButton('custpage_final_invoice', 'Final Invoice', 'btn_final_invoice_call(' + nlapiGetRecordId() + ')');
        }
    }
	
    return true;

}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
