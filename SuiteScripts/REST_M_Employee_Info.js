/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 19 Feb 2016 Nitish Mishra
 * 
 */

function postRESTlet(dataIn) {
	var response = new Response();
	try {
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var employeeId = getUserUsingEmailId(dataIn.EmailId);
		var personId = dataIn.Data.Person;
		var requestType = dataIn.RequestType;

		var allowCall = false;
		if (employeeId == personId) {
			allowCall = true;
		}

		if (!personId) {
			personId = employeeId;
		}

		switch (requestType) {

			case M_Constants.Request.Get:

				if (employeeId) {
					response.Data = getEmployeeDetails(personId, allowCall);
					response.Status = true;
				} else {
					response.Data = "No Employee Id Provided";
					response.Status = false;
				}

			break;

			case M_Constants.Request.GetAll:
				response.Data = getAllActiveEmployeeName();
				response.Status = true;
			break;
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}

function getEmployeeDetails(employeeId, allowCall) {
	try {
		var employeeValue = nlapiLookupField('employee', employeeId, [
		        'firstname', 'middlename', 'lastname', 'custentity_empid',
		        'title', 'birthdate', 'hiredate', 'custentity_fusion_empid',
		        'mobilephone', 'phone' ]);
		var employeeText = nlapiLookupField('employee', employeeId, [
		        'custentity_reportingmanager', 'approver', 'timeapprover',
		        'employeestatus', 'location' ], true);

		var employeeObj = {
		    FullName : employeeValue['firstname'] + " "
		            + employeeValue['middlename'] + " "
		            + employeeValue['lastname'],
		    EmployeeId : employeeValue['custentity_empid'],
		    Designation : employeeValue['title'],
		    ReportingManager : employeeText['custentity_reportingmanager'],
		    BirthDate : employeeValue['birthdate'],
		    ExpenseApprover : employeeText['approver'],
		    TimeApprover : employeeText['timeapprover'],
		    FusionId : employeeValue['custentity_fusion_empid'],
		    Location : employeeText['location'],
		    Level : employeeText['employeestatus'],
		    Address : '',
		    PhoneNumber : employeeValue['mobilephone'] ? employeeValue['mobilephone']
		            : employeeValue['phone'],
		    AllowCall : allowCall
		};

		return employeeObj;
	} catch (err) {
		nlapiLogExecution('error', 'getEmployeeDetails', err);
		throw err;
	}
}

function getAllActiveEmployeeName() {
	try {
		var employeeList = [];

		var employeeSearch = searchRecord('employee', null, [
		        new nlobjSearchFilter('custentity_employee_inactive', null,
		                'is', 'F'),
		        new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		        new nlobjSearchFilter('custentity_implementationteam', null,
		                'is', 'F') ], [ new nlobjSearchColumn('firstname'),
		        new nlobjSearchColumn('middlename'),
		        new nlobjSearchColumn('lastname') ]);

		if (employeeSearch) {

			employeeSearch.forEach(function(emp) {
				employeeList.push({
				    Text : emp.getValue('middlename') ? (emp
				            .getValue('firstname')
				            + " " + emp.getValue('middlename') + " " + emp
				            .getValue('lastname')) : (emp.getValue('firstname')
				            + " " + emp.getValue('lastname')),
				    Value : emp.getId()
				});
			});
		}

		return employeeList;
	} catch (err) {
		nlapiLogExecution('error', 'getAllActiveEmployeeName', err);
		throw err;
	}
}