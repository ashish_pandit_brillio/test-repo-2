/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Aug 2014     Swati
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{//fun start
 
	if(type == 'edit')
		{//if start
		
		var rec_Id=nlapiGetRecordId();
		var rec_Type=nlapiGetRecordType();
		
		var rec_load=nlapiLoadRecord(rec_Type,rec_Id);
		var select=rec_load.getFieldValue('custrecord_select');
		nlapiLogExecution('DEBUG','select',select);
		if(select == 'T')
			{
			throw nlobjError('E1500','This Record Can Not Be Editable');
			}
		
		}//if close
	
}//fun close
