/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Jan 2019     Aazam 
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */


function getFRFPlanRoles(request, response){
  nlapiLogExecution('DEBUG','Start','Started');
	var arrFRFPlans = [];
	var searchResult = GetFRFPlans();
  nlapiLogExecution('ERROR','searchResult : ',JSON.stringify(searchResult));
	for (var int = 0; int < searchResult.length; int++) {
        var dataOut = {};
		dataOut.role = {"name":searchResult[int].getValue('custrecord_frf_plan_details_project_role')};
		dataOut.emplevel ={"name" : searchResult[int].getText('custrecord_frf_plan_details_emp_lvl'),"id" :searchResult[int].getValue('custrecord_frf_plan_details_emp_lvl')} ;
		dataOut.location ={"name" : searchResult[int].getText('custrecord_frf_plan_details_location'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_location')} ;
		dataOut.project = {"name" : searchResult[int].getText('custrecord_frf_plan_details_project'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_project')};
		dataOut.account = {"name" : searchResult[int].getText('custrecord_frf_plan_details_account'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_account')};
		dataOut.practice ={"name" : searchResult[int].getText('custrecord_frf_plan_details_practice'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_practice')};
		dataOut.billrate ={"name" : searchResult[int].getValue('custrecord_frf_plan_details_bill_rate')};
		dataOut.family = {"name" : searchResult[int].getText('custrecord_frf_plan_details_skill_family'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_skill_family')};
		dataOut.primaryskills ={"name" : searchResult[int].getText('custrecord_frf_plan_details_primary'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_primary')};
		dataOut.otherskills = {"name" : searchResult[int].getText('custrecord_frf_plan_details_other_skills'),"id" : searchResult[int].getValue('custrecord_frf_plan_details_other_skills')};
		var externalHire;
		if(searchResult[int].getValue('custrecord_frf_plan_details_externalhire')=='T')
		{
			externalHire = true;//'true';
		}else{
			externalHire = false;'false';
		}
		/*var dollarLoss;
		if(searchResult[int].getValue('custrecord_frf_plan_dollar_loss')=='T')
		{
			dollarLoss = 'true';
		}else{
			dollarLoss = 'false';
		}*/
		var billable;
		if (searchResult[int].getValue('custrecord_frf_plan_details_bill_role') =='T') {
			billable =true;//'true';
		}else{
			billable = false;//'false';
		}
		var criticalrole;
		if (searchResult[int].getValue('custrecord_frf_plan_details_criticalrole') =='T') {
			criticalrole =true;//'true';
		}else{
			criticalrole = false;//'false';
		}
		dataOut.externalhire = {"name" :externalHire };
		dataOut.dollarloss = {"name" : searchResult[int].getValue('custrecord_frf_plan_dollar_loss')};
		dataOut.billrole = {"name" : billable};
		dataOut.criticalrole = {"name" : criticalrole};
		arrFRFPlans.push(dataOut);
	}
	response.write(JSON.stringify(arrFRFPlans));
}
function GetFRFPlans() {
	var customrecord_frf_plan_detailsSearch = nlapiSearchRecord("customrecord_frf_plan_details",null,
			[
			], 
			[
			   new nlobjSearchColumn("scriptid"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_account"),
			   new nlobjSearchColumn("custrecord_frf_plan_details_criticalrole"),
			   new nlobjSearchColumn("custrecord_frf_plan_details_project"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_practice"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_emp_lvl"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_project_role"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_bill_role"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_positions"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_location"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_skill_family"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_other_skills"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_startdate"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_enddate"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_allocation"), 
			   new nlobjSearchColumn("custrecord_frf_plan_dollar_loss"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_bill_rate"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_externalhire"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_suggest_res"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_special_req"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_opp_id"), 
			   new nlobjSearchColumn("custrecord_frf_plan_details_confirmed"),
			   new nlobjSearchColumn("custrecord_frf_plan_details_primary")
			]
			);
	return customrecord_frf_plan_detailsSearch;
}