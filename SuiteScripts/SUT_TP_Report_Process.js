// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_TP_Report_Process.js
	Author:Sachin k
	Company:Aashana
	Date:9-Aug-2014
	Description:This script is used for Transfer price data processing criteria


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function createCriteriaForm(request, response)
{

	/*  Suitelet:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES



    //  SUITELET CODE BODY

		nlapiLogExecution('DEBUG','Request Method', " " + request.getMethod());
		if(request.getMethod() == 'GET')
		{
			var form = nlapiCreateForm("Transfer Price Data Processing");

		     var sysdate=new Date
			 var date1=nlapiDateToString(sysdate)

		    var fromdate = form.addField('fromdate', 'date', ' From Date');
			fromdate.setMandatory(true);
			fromdate.setDefaultValue(date1)

			var todate = form.addField('todate', 'date', ' To Date');
			todate.setMandatory(true);
			todate.setDefaultValue(date1)

			var fromsubsidiary = form.addField('fromsubsidiary', 'select', ' From Subsidiary','subsidiary');
			fromsubsidiary.setMandatory(true);
			//fromsubsidiary.setDefaultValue(date1)

			var tosubsidiary = form.addField('tosubsidiary', 'select', ' To Subsidiary','subsidiary');
			tosubsidiary.setMandatory(true);
			//tosubsidiary.setDefaultValue(date1)

			form.addSubmitButton('Data Process');
			response.writePage(form);
		}
		else if(request.getMethod() == 'POST')
		{
			var fromDate=request.getParameter('fromdate');
			var toDate=request.getParameter('todate');
			var fromsubsidiary=request.getParameter('fromsubsidiary');
			var tosubsidiary=request.getParameter('tosubsidiary');
			nlapiLogExecution('DEBUG','In Post','fromDate=='+fromDate);
			nlapiLogExecution('DEBUG','In Post','toDate=='+toDate);
			nlapiLogExecution('DEBUG','In Post','fromsubsidiary=='+fromsubsidiary);
			nlapiLogExecution('DEBUG','In Post','tosubsidiary=='+tosubsidiary);

			var report_Filters= new Array();
	        var report_Column  = new Array();

			report_Filters[0]=new nlobjSearchFilter('custrecord_tp_fromdate',null,'on',fromDate)
			report_Filters[1]=new nlobjSearchFilter('custrecord_tp_todate',null,'on',toDate)
			report_Filters[2]=new nlobjSearchFilter('custrecord_tp_fromsubsidiary',null,'is',fromsubsidiary)
			report_Filters[3]=new nlobjSearchFilter('custrecord_tp_tosubsidary',null,'is',tosubsidiary)
			//nlapiLogExecution('DEBUG','In report','after filter');
     		report_Column[0]= new nlobjSearchColumn('internalid');//
			//nlapiLogExecution('DEBUG','In report','after column');

			var reportResult = nlapiSearchRecord('customrecord_transferprice_report', null,report_Filters,report_Column);

			nlapiLogExecution('DEBUG','In report','search=='+reportResult);
			if (reportResult != null)
			{
				nlapiLogExecution('DEBUG','In report','search=='+reportResult.length);
				for (var j = 0; j < reportResult.length; j++)
				{
					var recId   = reportResult[0].getValue('internalid');
					nlapiLogExecution('DEBUG','In report','recId=='+recId);
					var reportRec=nlapiLoadRecord('customrecord_transferprice_report',recId)
					var id=nlapiSubmitRecord(reportRec,true,true)
					nlapiLogExecution('DEBUG','In report','updated id=='+id);
				}


			}
			else
			{
				var reportRec=nlapiCreateRecord('customrecord_transferprice_report')
				reportRec.setFieldValue('custrecord_tp_fromdate',fromDate)
				reportRec.setFieldValue('custrecord_tp_todate',toDate)
				reportRec.setFieldValue('custrecord_tp_fromsubsidiary',fromsubsidiary)
				reportRec.setFieldValue('custrecord_tp_tosubsidary',tosubsidiary)
				var id=nlapiSubmitRecord(reportRec,true,true)
				nlapiLogExecution('DEBUG','In report','updated id=='+id);
			}

			// response.write ('Your Data is processing. Please wait.')
			nlapiSetRedirectURL('SUITELET','customscript_sut_tp_report_processstatus', '1', false);

		}







}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
