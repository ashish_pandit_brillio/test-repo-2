// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_VA_Screen_in_edit.js
	Author:		 Rakesh K 
	Company:	 Brillio 
	Date:		 28-02-2017
	Version: 	 1.0
	Description: When user will click on edit button at that time all records will come.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================




var strVar= '';
var s_tablestring = '';
var l_link_URL = '';
// BEGIN SUITELET ==================================================

function suiteletFunction_edit_mode_ref(request, response){
	
	
	try{
		if (request.getMethod() == 'GET'){
		
			var objUser = nlapiGetContext();
			var o_values  = new Object();
			var i_employee_id = objUser.getUser();
			var i_roleID = objUser.getRole();
			var s_role	=	getRoleName(i_employee_id, objUser.getRole());
			var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
			//var o_json = getrequiterdata(i_employee_id);
			o_values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
			o_values['role']		=	s_role;
			
			
			var lastNextID = request.getParameter('lastnextrecordID');
			var last_pre_rec_id = request.getParameter('last_prev_id');
			var mode = request.getParameter('mode');
			var counter = request.getParameter('counter');
			
			if(lastNextID != null ){
				var o_json = getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,'next');
			}
			else if(last_pre_rec_id){
				var o_json = getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,'prev');
			}
			else{
				var o_json = getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,'diff');
			}
			
			 var recordid = '';
			 var s_vendor_name = '';
			 var  s_federal_id= '';
			 var s_sr_no = '';
			 var s_vendor_contracttype = '';
			 var s_vendor_emailaddress = '';
			 var s_vendor_contactperson = '';
			 var s_vendor_status = '';
			
			if(mode == '' || mode == null){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
				
			}
			if(mode == 'next'){
				var finalvalue = parseInt(1) + parseInt(counter);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' || counter =='' ||counter == null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			if(mode == 'prev' && counter !='' && counter != null){
				var finalvalue = parseInt(1);
				o_values['counter'] = finalvalue;
			}
			o_values['record_found'] = o_json.length;
			
			 	var s_tablestring="";
				s_tablestring += "<html>";
				
				s_tablestring += "<table class=\"table\">";
				s_tablestring += "	<thead>";
				s_tablestring += "	<tr>";
				s_tablestring += "	<th>SR.NO<\/th>";
				s_tablestring += "	<th>Vendor Name<\/th>";
				s_tablestring += "	<th>Federal ID<\/th>";
				s_tablestring += "<th style=\"display:none\">Record ID<\/th>";
				s_tablestring += "	<th>Contract Type<\/th>";
				s_tablestring += "	<th>Contact Person<\/th>";
				s_tablestring += "	<th>Email Address<\/th>";
				s_tablestring += "	<th>Status<\/th>";
				s_tablestring += "<th><\/th>";
				s_tablestring += "	<\/tr>";
				s_tablestring += "	<\/thead>";
			
				
			 for(var key in o_json){
				
				 if(key <= 10){
					 if(mode == 'prev'){
						 var newid = o_json[0].record_id;
						 nlapiLogExecution('debug','mode newid' , newid);
					 }else{
						   var newid = o_json[key].record_id;
						   nlapiLogExecution('debug','else newid' , newid);
						 } 
				 
					 var find_prev_id = o_json[0].record_id;
				 //	var newid = o_json[key].record_id;
					//var recordid = o_json[key].record_id ;
					 var s_vendor_name = o_json[key].vendor_name ;
					var s_federal_id = o_json[key].federal_id;
					var s_sr_no = o_json[key].sr_no ;
					var s_vendor_contracttype = o_json[key].vendor_contracttype ;
					var s_vendor_emailaddress = o_json[key].vendor_emailaddress ;
					var s_vendor_contactperson = o_json[key].vendor_contactperson ;
					var s_vendor_status = o_json[key].vendor_status ;
					var recid = nlapiEncrypt(newid, 'base64');
					l_link_URL = "<a href=https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1156&deploy=1&pid="+recid+"&edit=1>Edit<\/a><br>";
					
				 	s_tablestring += "	<tbody>";
					s_tablestring += "	<tr>";
					s_tablestring += "<td>"+s_sr_no+"<\/td>";
					s_tablestring += "	<td>"+s_vendor_name+"<\/td>";
					s_tablestring += "<td>"+s_federal_id+"<\/td>";
					s_tablestring += "	<td style=\"display:none\">"+recordid+"<\/td>";
					s_tablestring += "	<td>"+s_vendor_contracttype+"<\/td>";
					s_tablestring += "<td>"+s_vendor_contactperson+"<\/td>";
					s_tablestring += "<td>"+s_vendor_emailaddress+"<\/td>";
					s_tablestring += "	<td>"+s_vendor_status+"<\/td>";
					s_tablestring += "<td>"+l_link_URL+"<\/td>";
					s_tablestring += "<\/tr>";
					s_tablestring += "	<\/tbody>";
				/*var newid = o_json[key].record_id;
				recordid += o_json[key].record_id +'<br>';
				s_vendor_name += o_json[key].vendor_name +'<br>';
				s_federal_id += o_json[key].federal_id +'<br>';
				s_sr_no += o_json[key].sr_no +'<br>';
				s_vendor_contracttype += o_json[key].vendor_contracttype +'<br>';
				s_vendor_emailaddress += o_json[key].vendor_emailaddress +'<br>';
				s_vendor_contactperson += o_json[key].vendor_contactperson +'<br>';
				s_vendor_status += o_json[key].vendor_status +'<br>';*/
				//nlapiLogExecution('debug', 'record_id',recordid); 
				
				//l_link_URL+="<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1151&deploy=1&pid="+recid+"&edit=1>Update Vendor info<\/a><br>";
			 }
			 }
			 s_tablestring += "<\/table>";
			// l_link_URL+="<a href=https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1156&deploy=1&pid=&edit=1>Edit<\/a><br>";
			
			 
			 if(o_json.length <=1 || o_json.length == null ){
					
					o_values['btn_disable_next'] = 'disabled';
					o_values['s_href_next'] = '#';
					o_values['btn_disable_prev'] = 'disabled';
				
				}
				if(o_json.length <10){
					
					o_values['btn_disable_next'] = 'disabled';
					o_values['s_href_next'] = '#';
				}
				
				if(newid && o_json.length >=10 ){
					//nlapiLogExecution('debug', 'newid', newid);
				 o_values['s_href_next'] = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1195&deploy=1&lastnextrecordID="+newid+"&mode=next"+"&counter="+finalvalue;	
					
				}
				if(counter == 2){
					o_values['btn_disable_prev'] = 'disabled';
					o_values['s_href_prev'] = '#';
				}
				//&& mode == 'prev'
				// && mode
				if(find_prev_id  && mode && counter !=2){
					
					o_values['s_href_prev'] = "https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1195&deploy=1&last_prev_id="+find_prev_id+"&mode=prev"+"&counter="+finalvalue;
				}	
				if(mode == '' || mode == null){
					o_values['btn_disable_prev'] = 'disabled';
					o_values['s_href_prev'] = '#';
					
				}
					o_values['last_id'] = newid; 
					o_values['last_prev_id'] = newid; 
					o_values['s_table_content'] = strVar; 
					o_values['is_selected'] = 'selected';
					
					
			 nlapiLogExecution('debug', 'l_link_URL', l_link_URL);
			 o_values['employee-name'] = recordid;//o_json[key].record_id;
			 o_values['vendor-name'] = s_vendor_name;// o_json[key].vendor_name;
			 o_values['federal-id'] = s_federal_id;//o_json[key].federal_id;
			 o_values['sr-no'] = s_sr_no; //o_json[key].sr_no;
			 o_values['vendor-contracttype'] = s_vendor_contracttype;
			 o_values['vendor-emailaddress'] =s_vendor_emailaddress;
			 o_values['vendor-contactperson'] =s_vendor_contactperson;
			 o_values['vendor-status'] =s_vendor_status;
			 o_values['s_edit_button'] = strVar; 
			 o_values['is_selected'] = 'selected';
			 o_values['record_found'] = o_json.length;
			 
			 //var view_all = getrequiterdataAllEmployee();
			 //o_values['table_content'] = view_all;
			 
			 var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			 var s_my_requests_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
			 var s_new_home_url	=	nlapiResolveURL('SUITELET', 'customscript_vendor_contract_screen', 'customdeploy1');
			 var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
			 o_values['new_request_url']	=	s_new_request_url;
			 o_values['my_requests_url']	=	s_my_requests_url;
			 o_values['s_search_URL']	=	s_search_URL;
			 o_values['s_new_home_url']	=	s_new_home_url;
			 o_values['table_content'] = s_tablestring;
			 var file = nlapiLoadFile(2046231); 
			 var contents = file.getValue();    //get the contents
			 nlapiLogExecution('debug', 's_tablestring', s_tablestring);
			 contents = replaceValues(contents, o_values);
			 // nlapiLogExecution('debug', 'contents', contents);
			  response.write(contents);
		
		}
		
	}catch(e){
		
		nlapiLogExecution('debug', 'ERROR'+e);
	}




}
// get role name 
function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}
// END SUITELET ====================================================

function replaceValues(content, oValues)
{
	for(param in oValues)
		{
			// Replace null values with blank
			var s_value	=	(oValues[param] == null)?'':oValues[param];
			
			// Replace content
			content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		}
	
	return content;
}


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{

	
		

	// get custom record data for particular user 
	function getrequiterdata(i_employee_id,lastNextID,last_pre_rec_id,mode)	{
		
		var temp = 0;
			//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
			//nlapiLogExecution('debug', 'lastNextID', lastNextID);
		 	var filters =	new Array();
		 	var columns	=	new Array();
		 	
		 	//if(i_employee_id != null && i_employee_id != '')
		 	{
		 		filters[temp++]	=	new nlobjSearchFilter('custrecord_contract_type',null,'anyof',4);
		 		filters[temp++]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'noneof',3);
		 		filters[temp++]	=	new nlobjSearchFilter('custrecord_docusign_process',null,'is','F');
		 		//filters[temp++]	=	new nlobjSearchFilter('custrecord_netsuite_role',null,'anyof',i_employee_id);
		 	}
		 	if(_logValidation(lastNextID)){
		 		filters[temp++]	=	new nlobjSearchFilter('internalidnumber',null,'greaterthan',parseInt(lastNextID));
		 	}
		 	if(_logValidation(last_pre_rec_id)){
		 		filters[temp++]	=	new nlobjSearchFilter('internalidnumber',null,'lessthan',parseInt(last_pre_rec_id));
		 	}
		 	if(mode == 'next' || mode == 'diff'){
				//nlapiLogExecution('audit','inside if sort');
				columns[0]	=	new nlobjSearchColumn('internalid').setSort(false);
			}else if(mode == 'prev'){
				
				//nlapiLogExecution('audit','inside else sort');
				columns[0]	=	new nlobjSearchColumn('internalid').setSort(true);
			}
			
			
			columns[1]	=	new nlobjSearchColumn('custrecord_vendor_name');
			columns[2]	=	new nlobjSearchColumn('custrecord_federal_id');
			columns[3]	=	new nlobjSearchColumn('custrecord_email_for_administration'); // email for admin 
			columns[4]	=	new nlobjSearchColumn('custrecord_contract_type'); // contract type
			columns[5]	=	new nlobjSearchColumn('custrecord_contact_person'); // contact person 
			columns[6]	=	new nlobjSearchColumn('custrecord_approval_status_recruiters'); // status 
			
			
			var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
			//nlapiLogExecution('debug', 'searchResults', searchResults.length);
			var a_data	=	new Array();
			//va s_tablestring="";
			var k=1;
			if(searchResults){
				
				for(var i = 0; i < searchResults.length; i++){
					
					var s_record_id	=	searchResults[i].getValue(columns[0]);
					var s_vendor_name	=	searchResults[i].getValue(columns[1]);
					var s_federal_id	=	searchResults[i].getValue(columns[2]);
					var s_emailaddress = searchResults[i].getValue(columns[3]);
					var s_contracttype = searchResults[i].getText(columns[4]);
					var s_contactperson =searchResults[i].getValue(columns[5]);
					var s_status = searchResults[i].getText(columns[6]);
					a_data.push({ 'record_id': s_record_id, 'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': k,'vendor_contracttype':s_contracttype,'vendor_emailaddress':s_emailaddress,'vendor_contactperson':s_contactperson,'vendor_status':s_status});
					k++;
					}
			}
			
			
			

			return a_data;
		}
	
/*function getrequiterdata(i_employee_id)	{
	 	
		//nlapiLogExecution('debug', 'getrequiterdata', i_employee_id);
	 	var filters =	new Array();
	 	var columns	=	new Array();
	 	//if(i_employee_id != null && i_employee_id != '')
	 	{
	 		
	 	}
	 	filters[0]	=	new nlobjSearchFilter('custrecord_approval_status_recruiters',null,'anyof',2);
		columns[0]	=	new nlobjSearchColumn('internalid');
		columns[1]	=	new nlobjSearchColumn('custrecord_vendor_name');
		columns[2]	=	new nlobjSearchColumn('custrecord_federal_id');
		columns[3]	=	new nlobjSearchColumn('custrecord_email_for_administration'); // email for admin 
		columns[4]	=	new nlobjSearchColumn('custrecord_contract_type'); // contract type
		columns[5]	=	new nlobjSearchColumn('custrecord_contact_person'); // contact person 
		columns[6]	=	new nlobjSearchColumn('custrecord_approval_status_recruiters'); // status 
		
		var searchResults	=	searchRecord('customrecord_recruiters_details', null, filters, columns);
		//nlapiLogExecution('debug', 'searchResults', searchResults);
		var a_data	=	new Array();
		
		var k =1;
		for(var i = 0; i < searchResults.length; i++){
				
				var s_record_id	=	searchResults[i].getValue(columns[0]);
				var s_vendor_name	=	searchResults[i].getValue(columns[1]);
				var s_federal_id	=	searchResults[i].getValue(columns[2]);
				var s_emailaddress = searchResults[i].getValue(columns[3]);
				var s_contracttype = searchResults[i].getText(columns[4]);
				var s_contactperson =searchResults[i].getValue(columns[5]);
				var s_status = searchResults[i].getText(columns[6]);
						
				a_data.push({ 'record_id': s_record_id, 'vendor_name': s_vendor_name, 'federal_id': s_federal_id, 'sr_no': k,'vendor_contracttype':s_contracttype,'vendor_emailaddress':s_emailaddress,'vendor_contactperson':s_contactperson,'vendor_status':s_status});
					
				
				k++;

			}
			

		return a_data;
	}*/


}


// search code 
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function _logValidation(value)  
{
	if(value != null && value != '' && value != undefined && value.toString() != 'NaN' && value != NaN)
	{
		return true;
	}
	else
	{
		return false;
	}
}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================