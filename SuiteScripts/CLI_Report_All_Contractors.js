// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     Script Name: CLI_Report_All_Contractors
     Author: Vikrant
     Company: Aashna
     Date: 14-10-2014
     Description: Script to export the report for time sheet entered for Contractors
     Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
     Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     PAGE INIT
     - pageInit(type)
     SAVE RECORD
     - saveRecord()
     VALIDATE FIELD
     - validateField(type, name, linenum)
     FIELD CHANGED
     - fieldChanged(type, name, linenum)
     POST SOURCING
     - postSourcing(type, name)
     LINE INIT
     - lineInit(type)
     VALIDATE LINE
     - validateLine()
     RECALC
     - reCalc()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     */
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*

 */

// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

    //  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================


function export_CSV(param)//
{
    try//
    {
        //alert(param);
        //alert('s_start_date : ' + param.split('@')[0] + ', s_project : ' + param.split('@')[1]);
        
        var createPDFURL = nlapiResolveURL("SUITELET", 'customscript_sut_report_all_contractors', 'customdeploy1', false);
        
        //pass the internal id of the current record
        createPDFURL += '&id=' + 'Export' + "&param=" + param;
        // alert(createPDFURL);
        
        //show the PDF file 
        newWindow = window.open(createPDFURL);
        
		return;		
    } //
catch (ex) //
    {
        alert('Exception : ' + ex.message)
        nlapiLogExecution('ERROR', 'exception', ex);
        nlapiLogExecution('ERROR', 'exception', ex.message);
    }
}


// BEGIN PAGE INIT ==================================================

function pageInit(type){
   
    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord(){
    /*  On save record:
     - PURPOSE
     FIELDS USED:
     --Field Name--			--ID--		--Line Item Name--
     */
    //  LOCAL VARIABLES
    
    
    
    //  SAVE RECORD CODE BODY
    
    
    return true;
    
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum){

    /*  On validate field:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    
    //  VALIDATE FIELD CODE BODY
    
    
    return true;
    
}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum){
   
    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

    //alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name){
   
    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type){
  
    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type){

    /*  On validate line:
     - EXPLAIN THE PURPOSE OF THIS FUNCTION
     FIELDS USED:
     --Field Name--				--ID--
     */
    //  LOCAL VARIABLES
    
    
    //  VALIDATE LINE CODE BODY
    
    
    return true;
    
}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type){
   
    //  LOCAL VARIABLES


    //  RECALC CODE BODY


}

// END RECALC =======================================================
// BEGIN FUNCTION ===================================================
// END FUNCTION =====================================================
