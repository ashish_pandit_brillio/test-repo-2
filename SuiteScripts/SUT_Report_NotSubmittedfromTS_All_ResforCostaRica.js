/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2017     bidhi.raj
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var id = request.getParameter('id');
		var param = request.getParameter('param');
		if (id == 'Export') {
			
			var toDate = param;
			var date1 = nlapiStringToDate(toDate);
            var fromDate = nlapiAddDays(date1, -55);
            fromDate = nlapiDateToString(fromDate, 'mm/dd/yyyy');
            
			var arraytoCSV = _SearchData(fromDate, toDate);
			var creat_csv_text = create_CSV(arraytoCSV , fromDate);

			var fileName = 'CostaRica Non Submitted'+param + '.csv';
			var file = nlapiCreateFile(fileName, 'CSV', creat_csv_text);
			response.setContentType('CSV', fileName);
			response.write(file.getValue());

		}
		if (request.getMethod() == 'GET' && id !='Export') //
		{

			var form = nlapiCreateForm('Sal-Non-Submitted Rpt-From TS-Costa-Rica');
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :');
			var sublist1 = form.addSubList('record', 'list',
					'Costa-Rica-Report');
			form.setScript('customscript1390');

			form.addSubmitButton('Submit');
			response.writePage(form);

		} else {
			if(id !='Export'){
			var form = nlapiCreateForm('Sal-Non-Std Rpt-From TS-Costa-Rica');
			form.setScript('customscript1390');//change
			var select = form.addField('custpage_startdate', 'Date',
					'Period End Date :').setDefaultValue(
					request.getParameter('custpage_startdate'));
			var sublist1 = form.addSubList('record', 'list',
					'Costa-Rica-Report');
			form.setScript('customscript1390');
			var toDate = request.getParameter('custpage_startdate');
			var date1 = nlapiStringToDate(toDate);
            var fromDate = nlapiAddDays(date1, -55);
            fromDate = nlapiDateToString(fromDate, 'mm/dd/yyyy');
			var week1 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 6));
			var week2 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 13));
			var week3 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 20));
			var week4 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 27));
			var week5 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 34));
			var week6 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 41));
			var week7 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 48));
			var week8 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 55));
			sublist1.addField('custevent_employee', 'text', 'Employee');
			
			sublist1.addField('custevent_wk1', 'text', week1);

			sublist1.addField('custevent_wk2', 'text', week2);
			
			sublist1.addField('custevent_wk3', 'text', week3 );
			
			sublist1.addField('custevent_wk4', 'text', week4);
			
			sublist1.addField('custevent_wk5', 'text', week5);
			
			sublist1.addField('custevent_wk6', 'text', week6);
			
			sublist1.addField('custevent_wk7', 'text', week7 );
			
			sublist1.addField('custevent_wk8', 'text', week8);

			sublist1.addField('custevent_aprvr', 'text', 'Timesheet Approver');
			
			sublist1.addField('custevent_dptmt', 'text', 'Department');
			
			sublist1.addField('custevent_clnt', 'text', 'Customer');

			sublist1.addField('custevent_pm', 'text', 'Project Manager');

			sublist1.addField('custevent_hdt', 'text', 'Hire Date');
			
			sublist1.addField('custevent_tdt', 'text', 'Term Date');
			
			var array = _SearchData(fromDate, toDate);
			
			sublist1.setLineItemValues(array);
			
			form.addSubmitButton('Submit');
			
			form.addButton('custombutton', 'Export As CSV',
					'fxn_generatePDFForNnSubmtd();');
			
			response.writePage(form);
		}
		}
	} catch (e) {
		nlapiLogExecution('DEBUG', 'Search results exception ', +e)
	}
}

function _SearchData(fromDate, toDate) {
	    
	var st_wk1 ='';
	var st_wk2 ='';
	var st_wk3 ='';
	var st_wk4 ='';
	var st_wk5 ='';
	var st_wk6 ='';
	    var temp_fromDate = fromDate;
		var emp = new Array();
		var a_filters = new Array();
		a_filters[0] = new nlobjSearchFilter('date', null, 'within', [
				fromDate, toDate ]);

		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('employee');
		a_columns[1] = new nlobjSearchColumn('item');
		a_columns[2] = new nlobjSearchColumn('timesheet');
		a_columns[3] = new nlobjSearchColumn('startdate', 'timesheet');
		a_columns[4] = new nlobjSearchColumn('custrecordcustcol_temp_customer_ts');
		a_columns[5] = new nlobjSearchColumn('department');
		a_columns[6] = new nlobjSearchColumn('custrecord_ts_time_approver','timesheet');
		a_columns[7] = new nlobjSearchColumn('custrecord_projectmanager_ts');
		
		
		var w1_fdy = getfirstweekday(fromDate);
		var w2_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 7)));
		var w3_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 14)));
		var w4_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 21)));
		var w5_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 28)));
		var w6_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 35)));
		var w7_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 42)));
		var w8_fdy = getfirstweekday(nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 49)));

		var searchresult_T = searchRecord('timeentry',
				'customsearch_salnonstdrptte_4', a_filters, a_columns);

		var st = JSON.stringify(searchresult_T);
		searchresult_T = JSON.parse(st);
		var arywk = [];
		var j = 0;
		var m = 0;
		var status = '';
		var start = start || 0;
		var itemArray = new Array();
		

		for ( var i in searchresult_T) {
			
		try{

			var curr_col = searchresult_T[i].columns;

			if (i != 0) {
				var temp_col = searchresult_T[i - 1].columns;
				
				if (timesheet != curr_col.timesheet) {
					
					fromDate = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 7));
					
					status = '';
				
					
				}
			}
			
			if (i != 0) {
				if (temp_col.employee.name != curr_col.employee.name) {
					
					var emp_det = nlapiLookupField('employee', curr_col.employee.internalid , ['hiredate','custentity_future_termination_date']);
					
					itemArray[m] = {
						'custevent_employee' : temp_col.employee.name,
						
						'custevent_wk1' : arywk[0],
						
						'custevent_wk2' : arywk[1],
						
						'custevent_wk3' : arywk[2],
						
						'custevent_wk4' : arywk[3],
						
						'custevent_wk5' : arywk[4],
						
						'custevent_wk6' : arywk[5],
						
						'custevent_wk7' : arywk[6],
						
						'custevent_wk8' : arywk[7],
						
						'custevent_aprvr' :temp_col.custrecord_ts_time_approver.name,
						
						'custevent_dptmt' : temp_col.department.name,
						
						'custevent_clnt' : temp_col.custrecordcustcol_temp_customer_ts,
						
						'custevent_pm' : temp_col.custrecord_projectmanager_ts.name,
						
						'custevent_hdt' : emp_det.hiredate,
						
						'custevent_tdt': emp_det.custentity_future_termination_date
						
						
					};
					arywk = [];
					j =0;
					m = m+1;
					fromDate = temp_fromDate ;
					 st_wk1 ='';
					 st_wk2 ='';
					 st_wk3 ='';
				     st_wk4 ='';
				     st_wk5 ='';
					 st_wk6 ='';
				}
			}

			var employee = curr_col.employee;
			var timesheetStartdate = curr_col.startdate;
			var timesheet = curr_col.timesheet;
			
			
			
			var d_fromdt = nlapiStringToDate(fromDate);
		    var day = d_fromdt.getDay() - start;
		    var date = d_fromdt.getDate() - day;

		    var StartDate = new Date(d_fromdt.setDate(date));
		    var firstdayofweek = nlapiDateToString(StartDate, 'mm/dd/yyyy');
			if (timesheetStartdate == w1_fdy && status != 'checked') {

				arywk[j] = 'filled';
				status = 'checked';
				 
			}
			else if(timesheetStartdate == w2_fdy && status != 'checked') {
                  
				arywk[j+1] = 'filled';
				status = 'checked';
			}
			else if(timesheetStartdate == w3_fdy && status != 'checked') {

				arywk[j+2] = 'filled';
				status = 'checked';
			}
			else if(timesheetStartdate == w4_fdy && status != 'checked') {

				arywk[j+3] = 'filled';
				status = 'checked';
			}
			else if(timesheetStartdate == w5_fdy && status != 'checked') {

				arywk[j+4] = 'filled';
				status = 'checked';
			}
			else if(timesheetStartdate == w6_fdy && status != 'checked') {

				arywk[j+5] = 'filled';
				status = 'checked';
			}
			else if(timesheetStartdate == w7_fdy && status != 'checked') {

				arywk[j+6] = 'filled';
				status = 'checked';
			}
			else if(timesheetStartdate == w8_fdy && status != 'checked') {

				arywk[j+7] = 'filled';
				status = 'checked';
			}
		
		}
		catch (e) {
			nlapiLogExecution('DEBUG', 'Search loop exception ', +e)
		}
}
		return itemArray;
}
 function getfirstweekday(fromDate) {
	 var start = start || 0;
	var d_fromdt = nlapiStringToDate(fromDate);
	var day = d_fromdt.getDay() - start;
	var date = d_fromdt.getDate() - day;
	var StartDate = new Date(d_fromdt.setDate(date));
	var firstdayofweek = nlapiDateToString(StartDate, 'mm/dd/yyyy');
	return firstdayofweek;

}

function create_CSV(lists,fromDate) {
	        var week1 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 6));
			var week2 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 13));
			var week3 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 20));
			var week4 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 27));
			var week5 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 34));
			var week6 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 41));
			var week7 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 48));
			var week8 = nlapiDateToString(nlapiAddDays(nlapiStringToDate(fromDate), 55));

		var html = "";

		html += "Employee,";
		html += week1+',';
		html += week2+',';
		html += week3+',';
		html += week4+',';
		html += week5+',';
		html += week6+',';
		html += week7+',';
		html += week8+',';
		html += "Timesheet Approver,";
		html += "Department,";
		html += "Customer,";
		html += "Project Manager,";
		html += "Hire Date,";
		html += "Term Date,";
		
		html += "\r\n";
	
		for (var l = 0; l < lists.length; l++) {
			var list = lists[l];
			html += "\r\n";
			for ( var i in list) {
				if(list[i] == 'undefined'|| list[i] == null){
					list[i] = '';
				}
				

				html += list[i];//e
				html += ",";
			    
			}
		}
		
			return html;
		}


			
	

