// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:	CLI_PM_Changes_Notification
	Author:			
	Company:		
	Date:		
    Description:	This script will notify some users on update of PM on any Project.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit_CLI_PM_Changes_Notification(type)


     SAVE RECORD
		- saveRecord_CLI_PM_Changes_Notification()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...
	var i_project_manager_ID = '';
	//var i_new_project_manager_ID = '';
	
	var i_delivery_manager_ID = '';
	
	var i_project_manager_Name = '';
	//var i_new_project_manager_ID = '';
	
	var i_delivery_manager_Name = '';
	
	var user_name ='';
	
	var transaction_Type = '';
	//nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', '*******')

}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit_CLI_PM_Changes_Notification(type) //
{
	/*  On page init:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--				--ID--			--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	//  PAGE INIT CODE BODY
	
	nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', '*** Into Page init ***')
	
	
	i_project_manager_Name = nlapiGetFieldText('custentity_projectmanager');
	nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', 'i_project_manager_Name : ' + i_project_manager_Name)
	
	i_project_manager_ID = nlapiGetFieldValue('custentity_projectmanager');
	nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', 'i_project_manager_ID : ' + i_project_manager_ID)
	
	i_delivery_manager_Name = nlapiGetFieldText('custentity_deliverymanager');
	nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', 'i_delivery_manager_Name : ' + i_delivery_manager_Name)
	
	i_delivery_manager_ID = nlapiGetFieldValue('custentity_deliverymanager');
	nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', 'i_delivery_manager_ID : ' + i_delivery_manager_ID)
	
	user_name = nlapiGetContext().getName();
	nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', 'user_name : ' + user_name);
	
}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord_CLI_PM_Changes_Notification() //
{
	/*  On save record:
	 - PURPOSE
	 FIELDS USED:
	 --Field Name--			--ID--		--Line Item Name--
	 */
	//  LOCAL VARIABLES
	
	
	
	//  SAVE RECORD CODE BODY
	
	try // Notification when Project Manager changes
	{
		nlapiLogExecution('DEBUG', 'pageInit_CLI_PM_Changes_Notification', '*** Into On Save ***')
		
		i_new_project_manager_Name = nlapiGetFieldText('custentity_projectmanager');
		i_new_project_manager_ID = nlapiGetFieldValue('custentity_projectmanager');
		nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_new_project_manager_ID : ' + i_new_project_manager_ID);
		nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_new_project_manager_ID : ' + i_new_project_manager_Name);
		
		//i_project_manager_Name = nlapiGetFieldText('custentity_projectmanager');
		//i_project_manager_ID = nlapiGetFieldValue('custentity_projectmanager');
		//nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_project_manager_ID : ' + i_project_manager_ID);
		//nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_project_manager_ID : ' + i_project_manager_Name);
		
		if (_is_Valid(i_project_manager_ID)) //
		{
			if (i_project_manager_ID != i_new_project_manager_ID) //
			{
				// Check for Project Type is external or not
				
				var i_project_Type = '';
				
				i_project_Type = nlapiGetFieldValue('jobtype');
				nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_project_Type : ' + i_project_Type);
				
				if (i_project_Type == '2') // If it is external project.
				{
					nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'Project is external!!!');
					
					// Check for Time sheets pending for approval for earlier PM
					var a_Filters = new Array();
					a_Filters[a_Filters.length] = new nlobjSearchFilter('custentity_projectmanager', 'job', 'anyof', i_project_manager_ID);
					a_Filters[a_Filters.length] = new nlobjSearchFilter('internalid', 'job', 'anyof', nlapiGetRecordId());
					
					//var search_Result = nlapiSearchRecord(null, 'customsearch_timesheet_pending_approval_', a_Filters); // SB Search
					var search_Result = nlapiSearchRecord(null, 'customsearch_timesheets_pending_approval', a_Filters); // Production Search
					//nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'search_Result : ' + search_Result); 
					
					if (_is_Valid(search_Result)) //
					{
						if (search_Result.length > 0) //
						{
							alert('Timesheets are pending for approval, Please process the same and then change Project Manager for this Project!');
							nlapiSetFieldValue('custentity_projectmanager', i_project_manager_ID, false, false);
							
							return false;
						}
					}
				}
				else //
				{
					nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'Project is internal!!!');
				//return true;
				}
				
				var email_String = '<html>';
				email_String += '<table>';
				email_String += '<tr>';
				email_String += '<td>Hi All,</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>This is to inform you that, Project Manager for project "<strong>' + nlapiGetFieldValue('entityid') + ' ' + nlapiGetFieldValue('companyname') + '</strong>" has been altered.</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>Earlier Project Manager is "<strong>' + i_project_manager_Name + '</strong>", And Current Project Manager is "<strong>' + i_new_project_manager_Name + '</strong>".</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>This change is done by ' + user_name + '</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td></td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td></br></br>Regards,</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>Information Systems</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td align="right">';
				email_String += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
				email_String += '</td>';
				email_String += '</tr>';
				email_String += '</table>';
				
				nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'email_String : ' + email_String);
				
				var sender = '442'; // internal id of Information System
				var recepient = '442'; // internal id of Information System
				var CC = new Array();
				CC.push('sridhar.v@brillio.com'); // CCing Lisa
				CC.push('sridhar.v@brillio.com'); // CCing Business Ops
				CC.push('sridhar.v@brillio.com'); // CCing Business Ops
				var subject = 'Change in Project Manager !!!';
				
				nlapiSendEmail(sender, recepient, subject, email_String, CC);
				//nlapiSendEmail('442', 'mani@brilio.com', subject, email_String); //['mani@gmail.com', 'mani@yahoo.co.in']
				nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'PM Email Sent !!!');
				
				i_project_manager_ID = i_new_project_manager_ID;
				i_project_manager_Name = i_new_project_manager_Name;
				
			}
		}
	} 
	catch (ex) //
	{
		nlapiLogExecution('ERROR', 'saveRecord_CLI_PM_Changes_Notification', 'ex : ' + ex.message);
		return false;
	}
	
	try //  Notification when Delivery Manager changes
	{
		i_new_delivery_manager_ID = nlapiGetFieldValue('custentity_deliverymanager');
		nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_new_delivery_manager_ID : ' + i_new_delivery_manager_ID);
		nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_delivery_manager_ID : ' + i_delivery_manager_ID);
		
		i_new_delivery_manager_Name = nlapiGetFieldText('custentity_deliverymanager');
		nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_new_delivery_manager_ID : ' + i_new_delivery_manager_Name);
		nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_delivery_manager_ID : ' + i_delivery_manager_Name);
		
		if (_is_Valid(i_delivery_manager_ID)) //
		{
			if (i_delivery_manager_ID != i_new_delivery_manager_ID) //
			{
				var i_project_Type = '';
				
				i_project_Type = nlapiGetFieldValue('jobtype');
				nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'i_project_Type : ' + i_project_Type);
				
				if (i_project_Type == '2') // If it is external project then only check for the pending expenses
				{
					// Checking if any expense is pending for current DM
					var a_Filters = new Array();
					a_Filters[a_Filters.length] = new nlobjSearchFilter('custbody_reportingmanager', null, 'anyof', i_delivery_manager_ID);
					
					//var search_Result = nlapiSearchRecord(null, 'customsearch_pending_for_pm_approval_2', a_Filters); // SB Search
					var search_Result = nlapiSearchRecord(null, 'customsearch_timesheets_pending_approval', a_Filters); // Production Search
					if (_is_Valid(search_Result)) //
					{
						if (search_Result.length > 0) //
						{
							alert('Expense(s) are pending for approval, Please process the same and then you can change "Delivery Manager" for this Project!');
							nlapiSetFieldValue('custentity_deliverymanager', i_delivery_manager_ID, false, false);
							
							return false;
						}
					}
				}
				else //
				{
				//return true;
				}
				
				var email_String = '<html>';
				email_String += '<table>';
				email_String += '<tr>';
				email_String += '<td>Hi All,</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>This is to inform you that, Delivery Manager for project "<strong>' + nlapiGetFieldValue('entityid') + ' ' + nlapiGetFieldValue('companyname') + '</strong>" has been altered.</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>Earlier Delivery Manager is "<strong>' + i_delivery_manager_Name + '</strong>", And Current Delivery Manager is "<strong>' + i_new_delivery_manager_Name + '</strong>".</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>This change is done by ' + user_name + '</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td></td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td></br></br>Regards,</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td>Information Systems</td>';
				email_String += '</tr>';
				email_String += '<tr>';
				email_String += '<td align="right">';
				email_String += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
				email_String += '</td>';
				email_String += '</tr>';
				email_String += '</table>';
				
				nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'email_String : ' + email_String);
				
				var sender = '442'; // internal id of Information System
				var recipient = '442'; // internal id of Information System
				var CC = new Array();
				CC.push('sridhar.v@brillio.com'); // CCing Lisa
				CC.push('sridhar.v@brillio.com'); // CCing Business Ops
				CC.push('sridhar.v@brillio.com'); // CCing Business Ops
				var subject = 'Change in Delivery Manager !!!';
				
				nlapiSendEmail(sender, recipient, subject, email_String, CC);
				//nlapiSendEmail('442', 'mani@brillio.com', subject, email_String,['mani@gmail.com', 'vmaniv@yahoo.co.in']); //
				nlapiLogExecution('DEBUG', 'saveRecord_CLI_PM_Changes_Notification', 'DM Email Sent !!!');
				
				i_delivery_manager_ID = i_new_delivery_manager_ID;
				i_delivery_manager_Name = i_new_delivery_manager_Name;
			}
		}
	}
	catch (ex) //
	{
		nlapiLogExecution('ERROR', 'saveRecord_CLI_PM_Changes_Notification', 'ex : ' + ex.message);
		return false;
	}
	
	return true;
	
}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged(type, name, linenum)
{
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================





// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function _is_Valid(obj) //
{
	if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}


// END FUNCTION =====================================================
