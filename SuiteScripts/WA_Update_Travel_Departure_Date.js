/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Nov 2015     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	
	var newRecord	=	nlapiGetNewRecord();
	
	var departure_date	=	newRecord.getFieldValue('custrecord_trli_departure_date');
	
	var i_tr_id	=	newRecord.getFieldValue('custrecord_trli_travel_request');
	
	var current_departure_date	=	nlapiLookupField('customrecord_travel_request', i_tr_id, 'custrecord_tr_departure_date');
	
	if(current_departure_date == '' || (new Date(departure_date)) < (new Date(current_departure_date)))
		{
			nlapiSubmitField('customrecord_travel_request', i_tr_id, 'custrecord_tr_departure_date', departure_date);	
		}
	
}
