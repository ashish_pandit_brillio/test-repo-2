/**

 Approves Invoices which are pending under particular Login User 
 
 **/
/** Author Sai Saranya
	11 May 2017
 **/

function skillApprovalScreen(request, response)
{
	try {
		var method = request.getMethod();

		if (method == 'GET') {
		
			createApprovalForm(request);
		}else {
			postFormAppoved(request);
		}
	} catch (err) {
		nlapiLogExecution('Error', 'suitelet', err);
		nlapiLogExecution('Error', 'Skill Approval Error',err);
	}
}
function createApprovalForm(request)
{
	try{
	// create the form
		var form = nlapiCreateForm('Pending Skills ');
		var skill_data ;
		// add fields to the form
		//var log_user=nlapiGetUser();
		var log_user = nlapiGetUser();
		if(log_user==41571 || log_user==39108 || log_user == 62082 || log_user == 7905){
       // log_user = 3165; 
		}
		form.setScript('customscript_employee_skill_field_change');
		var approval_name = nlapiLookupField('employee', log_user,
		        'entityid');
		var user_name = form.addField('textfield','text');
		user_name.setDefaultValue(approval_name);
		user_name.setDisplayType('inline');
		skill_data=getPendingSkillRecordofUser(log_user);
		var skillSublist = form.addSubList('custpage_skill_list','list','Pending Skill list');
		skillSublist.addMarkAllButtons();
		skillSublist.addRefreshButton();
		// add fields to the sublist
		skillSublist.addField('select','checkbox', 'select');
		var id=skillSublist.addField('skillid','select', 'Employee#','employee').setDisplayType('inline');
		var id=parseInt(id);
		nlapiLogExecution('Debug','id',id);
		skillSublist.addField('internalid','text','InternalId').setDisplayType('hidden');
		var inv=skillSublist.addField('invoicelink','url','Skill Record');
		//var viewUrl = nlapiResolveURL('RECORD', 'invoice', id, false);							
		//inv.setDefaultValue(viewUrl);
		inv.setLinkText('View');
		skillSublist.addField('family','text', 'Family ');
		skillSublist.addField('primary_skill','text', 'Primary Skill');
		skillSublist.addField('secondary_skill','text', 'Secondary Skill');
		skillSublist.addField('approval_status','text', 'Approval Status');
		
		
     	skillSublist.addField('approver','text', 'Approver');
		//skillSublist.addField('from_date','date', 'From Date');
		//skillSublist.addField('to_date','date', 'to Date');
		//skillSublist.addField('amount','currency', 'Total Amount');
		skillSublist.setLineItemValues(skill_data);
		//form.addSubmitButton('Approve Skills');
         var url = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1664&deploy=1';
     
		form.addButton('custpage_btn_approve', 'Approve',  'approveInvoices(\'  ' +url+ '  \')');
		form.addButton('custpage_btn_reject', 'Reject', 'rejectInvoices(\'  ' +url+ '  \')');
		//form.addButton('custpage_btn_approve', 'Approve', 'approveInvoices');
		//form.addButton('custpage_btn_reject', 'Reject', 'rejectInvoices');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('Error', 'createApprovalForm', err);
		throw err;
	}
}
function getPendingSkillRecordofUser(log_user) {
	try {
		
			var skillSearch = nlapiSearchRecord(
		        'customrecord_employee_master_skill_data',null,[
		               new nlobjSearchFilter('custrecord_skill_status',null,'anyof','1'),
						new nlobjSearchFilter('custrecord_employee_approver',null,'anyof',log_user),new nlobjSearchFilter('isinactive',null,'is','F')],
						[ new nlobjSearchColumn('custrecord_employee_skill_updated'),
						new nlobjSearchColumn('custrecord_primary_updated'),
		                new nlobjSearchColumn('custrecord_secondry_updated'),
		                new nlobjSearchColumn('custrecord_skill_status'),
						new nlobjSearchColumn('custrecord_family_selected'),
		                new nlobjSearchColumn('custrecord_employee_approver'),
						new nlobjSearchColumn('internalid')
		                ]);

		var skill_data = [];

		if (skillSearch) {
		
			skillSearch
			        .forEach(function(result) {
				      for (var i = 0; i < skillSearch.length; i++) {   
						skill_data
						         .push({
						                skillid : result
						                            .getValue('custrecord_employee_skill_updated'),
										
										internalid: result.getValue('internalid'),
										invoicelink:  nlapiResolveURL('RECORD', 'customrecord_employee_master_skill_data', result.getValue('internalid'), false),
						                secondary_skill : result
						                            .getValue('custrecord_secondry_updated'),
						                approval_status : result
						                            .getText('custrecord_skill_status'),
						                primary_skill : result
						                           .getText('custrecord_primary_updated'),
						                
						                family : result
						                            .getText('custrecord_family_selected'),
                                  		 approver : result
						                            .getText('custrecord_employee_approver')							
						              
						                });
								 break;
					        }
				        });
			       }
	return skill_data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMappedSubtierRecords', err);
		throw err;
	}
}

function postFormAppoved(request) {
	try {
		var linecount=request.getLineItemCount('custpage_skill_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=request.getLineItemValue('custpage_skill_list','select',i);
			var invoice=request.getLineItemValue('custpage_skill_list','skillid',i);
			
			if(linevalue== 'T')
			{
				var recid = request.getLineItemValue('custpage_skill_list','internalid', i);
				var rec=nlapiSubmitField('customrecord_employee_master_skill_data',recid,['custrecord_skill_status'],['2']);
			//	rec.setFieldValue('custrecord_skill_status',1);
			//	rec.setFieldValue('approvalstatus','2');
			//	nlapiSubmitRecord(rec);
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
			}
					
			var form = nlapiCreateForm('Skill Approval - Result ( Do not refresh this page )');
			var invoiceList = form.addSubList('custpage_invoice_list', 'list',
			        'Skill Update');
			invoiceList.addField('invoice', 'select', 'Skills Approved','employee')
			        .setDisplayType('inline');
			invoiceList.setLineItemValues(resList);                                                                                                 
			response.writePage(form);
		
		
		}
	} catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		
	}
	
}

