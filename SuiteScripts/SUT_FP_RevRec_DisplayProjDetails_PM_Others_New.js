function suiteletFunction_FP_RevRec(request,rsponse)
{
	try
	{
		
		
			if(request.getMethod() =='GET')
			{
				var o_context = nlapiGetContext();
				var i_user_logegdIn_id = o_context.getUser();
		/*if(i_user_logegdIn_id==7905)
          i_user_logegdIn_id=8001;*/
				var i_projectId = request.getParameter('proj_id');
				var s_request_mode = request.getParameter('mode');
				var practices_involved=new Array(); 
				var i_existing_revenue_share_rcrd_id = request.getParameter('existing_rcrd_id');
				if(i_existing_revenue_share_rcrd_id)
					i_existing_revenue_share_rcrd_id = i_existing_revenue_share_rcrd_id.trim();
					
				var i_revenue_share_status = request.getParameter('revenue_share_status');
				if(i_revenue_share_status)
					i_revenue_share_status = parseInt(i_revenue_share_status);
					
			var o_rev_loc_sub=Search_revenue_Location_subsdidary();	
			var location_monthend=o_rev_loc_sub["Cost Location"];
				if(i_projectId) // check if project id is present
				{
				if(i_existing_revenue_share_rcrd_id)
					var s_reject_reason = nlapiLookupField('customrecord_revenue_share',i_existing_revenue_share_rcrd_id,'custrecord_rejection_reason');
				
				i_projectId = i_projectId.trim();
				
				var o_project_object = nlapiLoadRecord('job',i_projectId); // load project record object
				
				// get necessary information about project from project record
				var s_project_region = o_project_object.getFieldValue('custentity_region');
				var i_customer_name = o_project_object.getFieldValue('parent');
				var s_project_name = o_project_object.getFieldValue('companyname');
				var d_proj_start_date = o_project_object.getFieldValue('startdate');
				var d_proj_end_date = o_project_object.getFieldValue('enddate');
				var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
				var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
				var i_proj_manager_practice = nlapiLookupField('employee',i_proj_manager,'department');
				var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
				var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
				var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');//entityid
				var s_project_id = o_project_object.getFieldValue('entityid');
				var billing_sch=o_project_object.getFieldValue('billingschedule');
				var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
				// design form which will be displayed to user
				var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
				var d_proj_end_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				if(d_proj_strt_date_old_proj)
				{
					d_proj_start_date = d_proj_strt_date_old_proj;
				}
				if(d_proj_end_date_old_proj)
				{
					d_proj_end_date = d_proj_end_date_old_proj;
				}
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				var o_form_obj = nlapiCreateForm("FP (Others) Project Details Page");
				
				// call client script to perform validation check
				o_form_obj.setScript('customscript_cs_validation_fp_others');
				
				// create region field
				var fld_region_field = o_form_obj.addField('proj_region', 'select','Region','customrecord_region').setDisplayType('inline');
				fld_region_field.setDefaultValue(s_project_region);
				
				// create customer field
				var fld_customer_field = o_form_obj.addField('customer_selected', 'select','Customer','customer').setDisplayType('inline');
				fld_customer_field.setDefaultValue(i_customer_name);
				
				// create project field
				var fld_proj_field = o_form_obj.addField('proj_selected', 'select','Project','job').setDisplayType('inline');
				fld_proj_field.setDefaultValue(i_projectId);
				
				// create project sow value field
				var fld_proj_sow_value = o_form_obj.addField('proj_sow_value', 'text','Project SOW value').setDisplayType('inline');
				fld_proj_sow_value.setDefaultValue(i_project_sow_value);	
				var tst=o_form_obj.addField('custpage_testfield', 'inlinehtml', 'Note').setDisplayType('inline');
				var rev_msg='<html>';
				rev_msg='<p><font size="2" color="red">Monthly Split will be updated if practice split entered<br>System does not prompt if the difference is + $5 or -$5 between SOW value and total monthly revenue forecast split</p></html>';
				tst.setDefaultValue(rev_msg);
               
				// create field for project start date
				var fld_proj_start_date = o_form_obj.addField('proj_strt_date', 'date','Project Start Date').setDisplayType('inline');
				fld_proj_start_date.setDefaultValue(d_proj_start_date);
				
				// create field for projet end date
				var fld_proj_end_date = o_form_obj.addField('proj_end_date', 'date','Project End Date').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(d_proj_end_date);
				
				// create field for projet currency
				var fld_proj_end_date = o_form_obj.addField('proj_currency', 'text','Project Currency').setDisplayType('inline');
				fld_proj_end_date.setDefaultValue(s_proj_currency);
				
				// create field for project practice
				var fld_proj_practice = o_form_obj.addField('proj_practice', 'select','Project Executing Practice','department').setDisplayType('inline');
				fld_proj_practice.setDefaultValue(i_proj_executing_practice);
				
				// create field for poject manager practice field
				var fld_proj_manager_practice = o_form_obj.addField('proj_manager_practice', 'select','Project Manager Practice','department').setDisplayType('inline');
				fld_proj_manager_practice.setDefaultValue(i_proj_manager_practice);
				
				// create field for project revenue rec type
				var fld_proj_revRec_type = o_form_obj.addField('rev_rec_type', 'select','Revenue Recognition Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
				fld_proj_revRec_type.setDefaultValue(i_proj_revenue_rec_type);
				
				// create field for rejection reason
			//	var fld_proj_rejection_reason = o_form_obj.addField('rejection_reason', 'text','Rejection Reason').setDisplayType('inline');
				//fld_proj_rejection_reason.setDefaultValue(s_reject_reason);
				
				// create field for request type i.e create or update
				var fld_existing_revenue_share_status = o_form_obj.addField('share_type', 'text','Request Type').setDisplayType('inline');
				fld_existing_revenue_share_status.setDefaultValue(s_request_mode);
				
				//field to find is value changed or not
				var fld_existing_revenue_share_changed = o_form_obj.addField('change_triggered', 'checkbox','').setDisplayType('inline');
				fld_existing_revenue_share_changed.setDefaultValue('F');
				//Fld to capture amount change
				var fld_amt_captured = o_form_obj.addField('amt_captured', 'currency','Amount Captured').setDisplayType('inline');
				
				
				//Create Sublist for project setup
				
				if(s_request_mode == 'Update'){
					
							var d_today_date = new Date();
							var i_today_date = d_today_date.getDate();
							var i_today_month = d_today_date.getMonth();
							
							var o_freeze_date = nlapiLoadRecord('customrecord_fp_rev_rec_freeze_date',1);
							var s_freeze_date = o_freeze_date.getFieldValue('custrecord_freeze_date');
							var i_freeze_day = nlapiStringToDate(s_freeze_date).getDate();
							var i_freeze_month = nlapiStringToDate(s_freeze_date).getMonth();
							
							if(parseInt(i_freeze_month) != parseInt(i_today_month))
								i_freeze_day = 25;
							
							var o_Fp_Rev_Rec_admin_access = nlapiLoadRecord('customrecord_fp_rev_rec_admin_access',2);
							var a_Fp_Rev_Rec_admin_access_emp_list= o_Fp_Rev_Rec_admin_access.getFieldValues('custrecord36');
							var data_list = [];
							if(a_Fp_Rev_Rec_admin_access_emp_list){
							for(var list_index=0;list_index < a_Fp_Rev_Rec_admin_access_emp_list.length;list_index++ )
							{
								data_list.push(parseInt(a_Fp_Rev_Rec_admin_access_emp_list[list_index]));
							}
							}
							nlapiLogExecution('DEBUG','user',a_Fp_Rev_Rec_admin_access_emp_list);
							var fld_revenue_msg = o_form_obj.addField('rvenue_msg', 'inlinehtml',null,null);
							var test_user=a_Fp_Rev_Rec_admin_access_emp_list.indexOf(i_user_logegdIn_id);
                 // if(i_user_logegdIn_id !=parseInt(7905)){
							if((i_today_date >= i_freeze_day) && (data_list.indexOf(parseInt(i_user_logegdIn_id))<0))
							{
								var s_revenue_msg = '<html>';
								s_revenue_msg += '<p><font size="3" color="red">Month End Effort is locked for this month.</font></p></html>';
								fld_revenue_msg.setDefaultValue(s_revenue_msg);
                              var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', parseInt(i_projectId)]];
					
								var a_column = new Array();
								a_column[0] = new nlobjSearchColumn('created').setSort(true);
								a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_auto_numb');
								a_column[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
								
								var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
								if (a_get_logged_in_user_exsiting_revenue_cap)
								{
									var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
									var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_other_approval_status');
								}
								var a_effort_activity_mnth_end_filter = [['custrecord_fp_others_mnth_end_fp_parent', 'anyof', parseInt(i_revenue_share_id)]];
			
								var a_columns_mnth_end_effort_activity_srch = new Array();
								a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
								
								var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
								if (a_get_mnth_end_effrt_activity)
								{
									var i_mnth_end_json_id = a_get_mnth_end_effrt_activity[0].getId();
								}
								var a_sut_param = new Array();
								a_sut_param['proj_id'] = i_projectId;
								a_sut_param['mode'] = 'Submit';
								a_sut_param['mnth_end_activity_id'] = i_mnth_end_json_id;
								a_sut_param['revenue_share_id'] = i_revenue_share_id;
								a_sut_param['revenue_rcrd_status'] = i_revenue_share_status;
								nlapiSetRedirectURL('SUITELET', '1518', 'customdeploy_fp_othr_mnthend_screen', null, a_sut_param);
							}
               //   }
					else{
					//Get Month Breakup
					var monthBreakUp = getMonthsBreakup(
					        nlapiStringToDate(d_proj_start_date),
					        nlapiStringToDate(d_proj_end_date));
					var fld_existing_revenue_share_record_id = o_form_obj.addField('revenue_share_id', 'integer','Revenue Share ID').setDisplayType('hidden');
					fld_existing_revenue_share_record_id.setDefaultValue(i_existing_revenue_share_rcrd_id);
					// Add subtab for People Plan
					var tab_revenue_share = o_form_obj.addSubTab('people_plan', 'Allocation Details');
					//Create 2nd Subtab
					var fld_sublist_effort_plan_mnth_end = o_form_obj.addSubList('people_plan_sublist', 'inlineeditor', 'Effort Plan Month End', 'people_plan');
					var fld_is_allocation_row = fld_sublist_effort_plan_mnth_end.addField('is_resource_allo_row', 'text','Is Allocation Row').setDisplayType('inline').setDefaultValue('<html><body bgcolor="red"></body></html>');
					var fld_practice = fld_sublist_effort_plan_mnth_end.addField('parent_practice_month_end', 'select', 'Practice', 'department').setDisplayType('disabled');
					fld_sublist_effort_plan_mnth_end.addField('sub_practice_month_end', 'select', 'Sub Practice', 'department');
					fld_sublist_effort_plan_mnth_end.addField('role_month_end', 'select', 'Role','customlist_fp_rev_rec_roles');
					var fld_emp_level = fld_sublist_effort_plan_mnth_end.addField('level_month_end', 'select', 'Level'); //customlist_fp_rev_rec_level
					var fld_location_mnth_end = fld_sublist_effort_plan_mnth_end.addField('location_month_end', 'select', 'Location');
					fld_location_mnth_end.addSelectOption('', '');
					for(var key in location_monthend){ 	
					fld_location_mnth_end.addSelectOption(key,location_monthend[key]);                          	
					}
					
					fld_emp_level.addSelectOption('', '');
					fld_emp_level.addSelectOption('31', '0');
					fld_emp_level.addSelectOption('1', '1');
					fld_emp_level.addSelectOption('15', '1A');
					fld_emp_level.addSelectOption('16', '1B');
					fld_emp_level.addSelectOption('2', '2');
					fld_emp_level.addSelectOption('17', '2A');
					fld_emp_level.addSelectOption('18', '2B');
					fld_emp_level.addSelectOption('3', '3');
					fld_emp_level.addSelectOption('10', '3A');
					fld_emp_level.addSelectOption('19', '3B');
					fld_emp_level.addSelectOption('4', '4');
					fld_emp_level.addSelectOption('20', '4A');
					fld_emp_level.addSelectOption('21', '4B');
					fld_emp_level.addSelectOption('5', '5');
					fld_emp_level.addSelectOption('22', '5A');
					fld_emp_level.addSelectOption('23', '5B');
					fld_emp_level.addSelectOption('6', '6');
					fld_emp_level.addSelectOption('11', '6A');
					fld_emp_level.addSelectOption('14', '6B');
					fld_emp_level.addSelectOption('7', '7');
					fld_emp_level.addSelectOption('24', '7A');
					fld_emp_level.addSelectOption('25', '7B');
					fld_emp_level.addSelectOption('8', '8');
					fld_emp_level.addSelectOption('26', '8A');
					fld_emp_level.addSelectOption('27', '8B');
					fld_emp_level.addSelectOption('9', '9');
					fld_emp_level.addSelectOption('28', '9A');
					fld_emp_level.addSelectOption('29', '9B');
					fld_emp_level.addSelectOption('13', 'CW');
					fld_emp_level.addSelectOption('30', 'PT');
					fld_emp_level.addSelectOption('12', 'TS'); 
					fld_emp_level.addSelectOption('33', 'L'); 
					
					fld_sublist_effort_plan_mnth_end.addField('resource_cost_month_end', 'float', 'Resource Cost(USD)').setDisplayType('disabled');
					//fld_sublist_effort_plan_mnth_end.addField('current_month', 'float', 'Current Month').setDisplayType('disabled');
					for (var j = 0; j < monthBreakUp.length; j++)
					{
						var months = monthBreakUp[j];
						var s_month_name = getMonthName(months.Start); // get month name
						/*var month_strt = nlapiStringToDate(months.Start);
						var month_end = nlapiStringToDate(months.End);
						var i_month = month_strt.getMonth();
						var i_year = month_strt.getFullYear();
						s_month_name1 = s_month_name +'_'+ i_year;
						var d_today_date = new Date();
						
						if(month_strt < d_today_date)
						{*/
						var month_strt = nlapiStringToDate(months.Start);
						var month_name_yr = month_strt.getMonth();
						var month_end = nlapiStringToDate(months.End);
						var i_month = month_strt.getMonth();
						var i_year = month_strt.getFullYear();
						s_month_name = s_month_name +'_'+ i_year;
						var d_today_date = new Date();
						var today_month_yr = d_today_date.getMonth();
						var prev_year = d_today_date.getFullYear();
						//if(month_end <= d_today_date)
                      if(month_strt <= d_today_date)
                        //  if((month_name_yr < today_month_yr))
						//if((month_name_yr <= today_month_yr)&&(i_year <= prev_year))
						{
							fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name).setDisplayType('disabled');
						}
						else
						{
							fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name);
							
						}
						/*if(month_name_yr < today_month_yr)
						{
							fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name).setDisplayType('disabled');
						}
						else
						{
							fld_sublist_effort_plan_mnth_end.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name);
						}*/
					}
					
					//code to set month end plan per monthwise
					
					var a_effort_activity_mnth_end_filter = [['custrecord_fp_rev_rec_month_end_parent_l', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)],
																'and',
																['isinactive', 'is', 'F']
																];
			
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_others_mont_end', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						
						var obj_resource_location=o_rev_loc_sub["Location"];
						
						// Get resource allocation for current month
						var a_unique_sub_prac = new Array();
						var a_proj_prac_details = new Array();
						var a_current_pract = new Array();
						var a_current_pract_unique_list_sub_prac = new Array();
						var a_current_pract_unique_list_level = new Array();
						var a_current_pract_unique_list_role = new Array();
							
						var d_todays_date = nlapiStringToDate(nlapiDateToString(new Date()));
						var d_firstDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth(), 1);
						var d_lastDay_mnth = new Date(d_todays_date.getFullYear(), d_todays_date.getMonth()+1, 0);
						var i_days_in_mnth = getDatediffIndays(d_firstDay_mnth,d_lastDay_mnth);
						
						var s_year_current = d_todays_date.getFullYear();
						var i_month_current = d_todays_date.getMonth();
						var s_month_current = getMonthName_FromMonthNumber(i_month_current);
								
						var a_filters_allocation = [ [ 'startdate', 'onorbefore', d_lastDay_mnth ], 'and',
							[ 'enddate', 'onorafter', d_firstDay_mnth ], 'and',
							['project', 'anyof', i_projectId]
						  ];
						 
						var a_columns = new Array();
						a_columns[0] = new nlobjSearchColumn('custevent_practice',null,'group').setSort(false);
						a_columns[1] = new nlobjSearchColumn('custrecord_parent_practice','custevent_practice','group');
						a_columns[2] = new nlobjSearchColumn('percentoftime',null,'sum');
						a_columns[3] = new nlobjSearchColumn('employeestatus','employee','group');
						a_columns[4] = new nlobjSearchColumn('startdate',null,'group');
						a_columns[5] = new nlobjSearchColumn('enddate',null,'group');
						a_columns[6] = new nlobjSearchColumn('subsidiary','employee','group');
						a_columns[7] = new nlobjSearchColumn('location','employee','group');
						a_columns[8]=new nlobjSearchColumn('title','employee','group');
						var a_project_allocation_result = nlapiSearchRecord('resourceallocation', null, a_filters_allocation, a_columns);
						if (a_project_allocation_result)
						{
							for(var i_prac_count=0; i_prac_count<a_project_allocation_result.length; i_prac_count++)
							{
								var s_emp_level = a_project_allocation_result[i_prac_count].getText('employeestatus','employee','group');
								s_emp_level = s_emp_level.toString();
								var s_emp_level_sub_str = s_emp_level.substr(0,1);
								if(!isNaN(s_emp_level_sub_str))
								{
									s_emp_level = s_emp_level_sub_str;
								}
								else
								{
									s_emp_level = a_project_allocation_result[i_prac_count].getValue('employeestatus','employee','group');
								}
								var employee_role=a_project_allocation_result[i_prac_count].getText('title','employee','group');
								var d_allocation_strt_date = a_project_allocation_result[i_prac_count].getValue('startdate',null,'group');
								var d_allocation_end_date = a_project_allocation_result[i_prac_count].getValue('enddate',null,'group');
								d_allocation_strt_date = nlapiStringToDate(d_allocation_strt_date);
								d_allocation_end_date = nlapiStringToDate(d_allocation_end_date);
								
								var s_prcnt_allocated = a_project_allocation_result[i_prac_count].getValue('percentoftime',null,'sum');
								s_prcnt_allocated = s_prcnt_allocated.toString();
								s_prcnt_allocated = s_prcnt_allocated.split('.');
								s_prcnt_allocated = s_prcnt_allocated[0].toString().split('%');
								
								if(d_allocation_strt_date < d_firstDay_mnth)
								{
									d_allocation_strt_date = d_firstDay_mnth;
								}
								
								if(d_allocation_end_date > d_lastDay_mnth)
								{
									d_allocation_end_date = d_lastDay_mnth;
								}
								
								var i_days_diff = getDatediffIndays(d_allocation_strt_date,d_allocation_end_date);
								//nlapiLogExecution('audit','days allocated:- '+i_days_diff,'days in month:- '+i_days_in_mnth);
								if(i_days_diff >= i_days_in_mnth)
								{
									var f_final_prcnt_allocation = parseFloat(s_prcnt_allocated) / parseFloat(100);
								}
								else
								{
									var i_total_allocated_days = parseFloat(i_days_diff) / parseFloat(i_days_in_mnth);
									s_prcnt_allocated = parseFloat(s_prcnt_allocated) * parseFloat(i_total_allocated_days);
									var f_final_prcnt_allocation = parseFloat(s_prcnt_allocated) / parseFloat(100);
								}
								f_final_prcnt_allocation = parseFloat(f_final_prcnt_allocation).toFixed(2);
								
								a_unique_sub_prac.push(a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'));
								
								var i_resource_location = a_project_allocation_result[i_prac_count].getValue('location','employee','group');
								i_resource_location=obj_resource_location[i_resource_location]?parseInt(obj_resource_location[i_resource_location]):parseInt(i_resource_location);
								//nlapiLogExecution('audit','resource location:- ',i_resource_location);
								var i_is_practice_alreday_present = findAllocationPracticePresent(a_proj_prac_details, a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'), s_emp_level, a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'), i_resource_location);
								//nlapiLogExecution('audit','i_is_practice_alreday_present:- ',i_is_practice_alreday_present);
								
								if(i_is_practice_alreday_present >= 0)
								{
									var f_already_alloc_prcnt = a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated;
									f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt) + parseFloat(f_final_prcnt_allocation);
									f_already_alloc_prcnt = parseFloat(f_already_alloc_prcnt).toFixed(2);
									
									//nlapiLogExecution('audit','already present prcnt:- '+a_proj_prac_details[i_is_practice_alreday_present].f_prcnt_allocated,'f_already_alloc_prcnt:- '+f_already_alloc_prcnt);
									a_proj_prac_details[i_is_practice_alreday_present] = {
															'i_sub_practice': a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'),
															'i_practice': a_project_allocation_result[i_prac_count].getValue('custrecord_parent_practice','custevent_practice','group'),
															'f_prcnt_allocated': f_already_alloc_prcnt,
															's_emp_level': s_emp_level,
															'i_resource_location': i_resource_location,
															'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
															'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group')
														};
								}
								else
								{
									var i_prac_present_unique = find_uniq_prac_exist(a_proj_prac_details,a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'));
									if(i_prac_present_unique==0)
									{
										practices_involved.push(a_project_allocation_result[i_prac_count].getText('custevent_practice',null,'group'));
									}
									a_proj_prac_details.push(
														{
															'i_sub_practice': a_project_allocation_result[i_prac_count].getValue('custevent_practice',null,'group'),
															'i_practice': a_project_allocation_result[i_prac_count].getValue('custrecord_parent_practice','custevent_practice','group'),
															'f_prcnt_allocated': f_final_prcnt_allocation,
															's_emp_level': s_emp_level,
															'i_resource_location': i_resource_location,
															'subsidiary': a_project_allocation_result[i_prac_count].getValue('subsidiary','employee','group'),
															'emp_role':a_project_allocation_result[i_prac_count].getValue('title','employee','group')
														});
								}
							}
						}
						
						var i_mnth_end_effrt_activity_parent = a_get_mnth_end_effrt_activity[0].getId();
						nlapiLogExecution('audit','mnth end id:- ',i_mnth_end_effrt_activity_parent);
						
						var a_effort_plan_mnth_end_filter = [['custrecord_fp_others_mnth_act_parent', 'anyof', parseInt(i_mnth_end_effrt_activity_parent)]];
						
						var a_columns_mnth_end_effort_plan_srch = new Array();
						a_columns_mnth_end_effort_plan_srch[0] = new nlobjSearchColumn('custrecordfp_othr_month_end_sub_practice',null,'group').setSort(false);
						a_columns_mnth_end_effort_plan_srch[1] = new nlobjSearchColumn('custrecordfp_others_mnth_end_practice',null,'group');
						a_columns_mnth_end_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_fp_month_end_role',null,'group').setSort(false);
						a_columns_mnth_end_effort_plan_srch[3] = new nlobjSearchColumn('custrecordfp_other_mnth_end_level',null,'group').setSort(false);
						a_columns_mnth_end_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_fp_others__location_mnth_end',null,'group').setSort(false);
						a_columns_mnth_end_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_fp_others__resource_cost',null,'group');
						a_columns_mnth_end_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_alloc_per',null,'group');
						a_columns_mnth_end_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
						a_columns_mnth_end_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year_y',null,'group');
						//a_columns_mnth_end_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_revenue_share_mnth_end',null,'group');
						a_columns_mnth_end_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
						
						var a_get_mnth_end_effrt_plan_prac = searchRecord('customrecord_fp_rev_rec_othr_mnth_end_ef', null, a_effort_plan_mnth_end_filter, a_columns_mnth_end_effort_plan_srch);
						if (a_get_mnth_end_effrt_plan_prac)
						{
							nlapiLogExecution('audit','effrt length(CUSTOM TABLE):- '+a_get_mnth_end_effrt_plan_prac.length);
							
							var i_previous_sub_prac = 0;
							var i_previous_role = 0;
							var i_previous_level = 0;
							var i_previous_location = 0;
							var s_prev_month = '';
							var f_revised_allocation = 0;
							
							var i_line_no = 1;
							var a_contractor_avearge = new Array();
							var i_frst_contractor = 0;
							var i_total_contractor_row_flag = 0;
							var f_total_contractor_resource_cost = 0;
							for(var i_mnth_end_plan=0; i_mnth_end_plan<a_get_mnth_end_effrt_plan_prac.length; i_mnth_end_plan++)
							{
								if(i_previous_sub_prac == 0)
								{
									i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
									i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
									i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
									i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
								}
								
								if(i_previous_sub_prac != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'))
								{
									//i_current_sublist_length
									i_line_no++;
									i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
									i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
									i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
									i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
								}
								else if(i_previous_sub_prac == a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'))
								{
									if(i_previous_level != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'))
									{
										if (a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group')) {
											i_line_no++;
											i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
											i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
											i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
											i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
										}
									}
									else if(i_previous_role != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group'))
									{
										if (a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group')) {
											i_line_no++;
											i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
											i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
											i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
											i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
										}
									}
									else if(i_previous_location != a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'))
									{
										i_line_no++;
										i_previous_sub_prac = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group');
										i_previous_role = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group');
										i_previous_level = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group');
										i_previous_location = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group');
										
									}
								}
								
								var i_is_prac_present_already = find_prac_exist(a_current_pract,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'),a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'),a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'));
								var i_is_prac_present_unique = find_uniq_prac_exist(a_current_pract,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'));
								
								if(i_is_prac_present_already == 0)
								{
									if(i_is_prac_present_unique==0)
									{
										practices_involved.push(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getText('custrecordfp_othr_month_end_sub_practice',null,'group'));
									}
									a_current_pract.push({
											'i_sub_practice': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'),
											'i_level': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'),
											'i_location': a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'),
											'i_line_no': i_line_no
										});
								}
								
								var s_mnth_year_1 = '';
								var s_mnth_name = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year',null,'group');
								var s_year_name = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year_y',null,'group');
								s_year_name = s_year_name.split('.');
								s_year_name = s_year_name[0];
								s_mnth_year_1 = s_mnth_name+'_'+s_year_name;
								
								var s_mnth_year = s_month_current+'_'+s_year_current;
								
								if(s_mnth_year_1 == s_mnth_year)
								{
									var i_allocation_to_display = 0;
								}
								else
								{
									var i_allocation_to_display = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_alloc_per',null,'group');
								}
								
								i_allocation_to_display = parseFloat(i_allocation_to_display);
								
								if(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group') == 13)
								{
									if(i_frst_contractor == 0)
									{
										a_contractor_avearge.push(s_mnth_name);
										i_frst_contractor = 1;
									}
									
									if(a_contractor_avearge.indexOf(s_mnth_name) >= 0)
									{
										nlapiLogExecution('audit','resource cost actual:- '+a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__resource_cost',null,'group'));
										i_total_contractor_row_flag++;
										f_total_contractor_resource_cost = parseFloat(f_total_contractor_resource_cost) + parseFloat(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__resource_cost',null,'group'));
										nlapiLogExecution('audit','f_total_contractor_resource_cost:- '+f_total_contractor_resource_cost,'i_total_contractor_row_flag:- '+i_total_contractor_row_flag);
									}
									
									var f_total_contractor_avg_cost = parseFloat(f_total_contractor_resource_cost) / parseFloat(i_total_contractor_row_flag);
									fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,parseFloat(f_total_contractor_avg_cost).toFixed(0));
								}
								else
								{
									fld_sublist_effort_plan_mnth_end.setLineItemValue('resource_cost_month_end',i_line_no,parseFloat(a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__resource_cost',null,'group')).toFixed(0));
								}
								
								fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_others_mnth_end_practice',null,'group'));
								fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_othr_month_end_sub_practice',null,'group'));
								fld_sublist_effort_plan_mnth_end.setLineItemValue('role_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_month_end_role',null,'group'));
								fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecordfp_other_mnth_end_level',null,'group'));
								fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end',i_line_no,a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others__location_mnth_end',null,'group'));
								
								// set effort from existing effort plan
								var s_current_mnth = a_get_mnth_end_effrt_plan_prac[i_mnth_end_plan].getValue('custrecord_fp_others_mnth_end_year',null,'group');
								if(!s_prev_month)
								{
									s_prev_month = s_current_mnth;
								}
								
								//nlapiLogExecution('audit','s_current_mnth:- '+s_current_mnth,'s_prev_month:- '+s_prev_month);
								if(s_current_mnth == s_prev_month)
								{
									f_revised_allocation = parseFloat(f_revised_allocation) + parseFloat(i_allocation_to_display);
								}
								else
								{
									var f_revised_allocation = 0;
									s_prev_month = s_current_mnth;
								}
								
								nlapiLogExecution('audit','alloca prcnt:- '+i_allocation_to_display);
								fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year_1.toLowerCase(),i_line_no,parseFloat(i_allocation_to_display).toFixed(2));
								
							}
						}
						else
						{
							var s_nt_apprvd_msg = '<html>';
							s_nt_apprvd_msg += '<p><font size="3" color="red">Please wait system is generating month end effort as per budget plan, you will receive mail once process is completed.</font></p></html>';
							var fld_nt_apprvd_msg = o_form_obj.addField('s_nt_apprvd_msg', 'inlinehtml',null,null);
							fld_nt_apprvd_msg.setDefaultValue(s_nt_apprvd_msg);
						}
						
						nlapiLogExecution('audit','mnth end effrt total lines:- '+i_line_no);
						//fld_mnth_end_effrt_lines.setDefaultValue(i_line_no);
						
                      
                      //Added by praveena to check the content
                      //nlapiSendEmail ( "200580" , "praveena@inspirria.com" , "a_proj_prac_details" , JSON.stringify(a_proj_prac_details) , null , null , null , null );
                      // nlapiSendEmail ( "200580" , "praveena@inspirria.com" , "a_current_pract" , JSON.stringify(a_current_pract) , null , null , null , null );
                      
                      
						// code for practice which is not thr in effrt plan but exist in allocation
						for (var i_allocation_index = 0; i_allocation_index < a_proj_prac_details.length; i_allocation_index++)
						{
							var i_practice_available = 0;
							//nlapiLogExecution('audit','current prac len:- ',a_current_pract.length);
							for (var i_current_index = 0; i_current_index < a_current_pract.length; i_current_index++)
							{
								if (a_current_pract[i_current_index].i_sub_practice == a_proj_prac_details[i_allocation_index].i_sub_practice && a_current_pract[i_current_index].i_level == a_proj_prac_details[i_allocation_index].s_emp_level && a_current_pract[i_current_index].i_location == a_proj_prac_details[i_allocation_index].i_resource_location) //i_location
								{
									i_practice_available = 1;
									
                                  nlapiLogExecution('audit','Praveena a_current_pract[i_current_index].i_sub_practice',a_current_pract[i_current_index].i_sub_practice);
									
                                  
									nlapiLogExecution('audit','line no:- '+a_current_pract[i_current_index].i_line_no,'allocated percent:- '+a_proj_prac_details[i_allocation_index].f_prcnt_allocated);
									fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year.toLowerCase(),a_current_pract[i_current_index].i_line_no,parseFloat(a_proj_prac_details[i_allocation_index].f_prcnt_allocated).toFixed(2));
								}
							}
							/*var a_cost_setup_filter = [['custrecord_fp_cost_setup_practice', 'anyof', a_proj_prac_details[i_allocation_index].i_practice], 'and',
										['custrecord_fp_cost_setup_sub_practice', 'anyof', a_proj_prac_details[i_allocation_index].i_sub_practice], 'and',
										['custrecord_fp_cost_setup_level', 'anyof', a_proj_prac_details[i_allocation_index].s_emp_level]];
			
							//['custrecord_fp_cost_setup_role', 'anyof', i_mnth_end_role], 'and',
										
							var a_columns_cost_setup = new Array();
							a_columns_cost_setup[0] = new nlobjSearchColumn('custrecord_fp_cost_setup_india_cost');
							a_columns_cost_setup[1] = new nlobjSearchColumn('custrecord_fp_cost_setup_us_cost');
							a_columns_cost_setup[2] = new nlobjSearchColumn('custrecord_fp_cost_setup_europe_cost');
							
							var a_get_cost_setup_role_level = nlapiSearchRecord('customrecord_fp_cost_setup', null, a_cost_setup_filter, a_columns_cost_setup);
							if (a_get_cost_setup_role_level)
							{
								if(a_proj_prac_details[i_allocation_index].i_resource_location == 8) //India
								{
									var res_cost= a_get_cost_setup_role_level[0].getValue('custrecord_fp_cost_setup_india_cost');
									
								}
								else if(a_proj_prac_details[i_allocation_index].i_resource_location == 2) // UK
								{
									var res_cost= a_get_cost_setup_role_level[0].getValue('custrecord_fp_cost_setup_europe_cost');
									
								}
								else //US
								{
									var res_cost= a_get_cost_setup_role_level[0].getValue('custrecord_fp_cost_setup_us_cost');
								
								}
							}*/
							if(i_practice_available == 0)
							{
								i_line_no++;
								nlapiLogExecution('audit','level:- '+a_proj_prac_details[i_allocation_index].s_emp_level+':::i_line_no:- '+i_line_no,s_mnth_year);
								fld_sublist_effort_plan_mnth_end.setLineItemValue('parent_practice_month_end',i_line_no,a_proj_prac_details[i_allocation_index].i_practice);
								fld_sublist_effort_plan_mnth_end.setLineItemValue('sub_practice_month_end',i_line_no,a_proj_prac_details[i_allocation_index].i_sub_practice);
								fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_mnth_year.toLowerCase(),i_line_no,parseFloat(a_proj_prac_details[i_allocation_index].f_prcnt_allocated).toFixed(2));
								fld_sublist_effort_plan_mnth_end.setLineItemValue('level_month_end',i_line_no,a_proj_prac_details[i_allocation_index].s_emp_level);
								
								var subsidiary_monthend=o_rev_loc_sub["Subsidairy"][a_proj_prac_details[i_allocation_index].subsidiary];	
									subsidiary_monthend=subsidiary_monthend?	
									fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end', i_line_no, a_proj_prac_details[i_allocation_index].i_resource_location.toString())	
									:	
									fld_sublist_effort_plan_mnth_end.setLineItemValue('location_month_end', i_line_no, '9');
							}
						}
					}
					
					
					
					//------**********----------****************-----------------
					
					
					// call client script to perform validation check
					o_form_obj.setScript('customscript_cs_validation_fp_others');
				
							
							//fld_existing_revenue_share_status.setDefaultValue('MonthEnd');
				
				//if (i_revenue_share_status == 3)
				//{
					// Add subtab for month end activity effort plan
					var tab_effort_plan = o_form_obj.addSubTab('effort_plan_tab_month_end', 'Monthly Revenue');
					
					
					var url_proj_detail_url = 'https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=1055&deploy=1&mode=View&revenue_rcrd_updated=F&existing_rcrd_id='+i_existing_revenue_share_rcrd_id+'&proj_id='+i_projectId+'&revenue_rcrd_status=3&whence=';
							//var fld_proj_detail = o_form_obj.addField('proj_detail_url', 'url','').setLinkText('Project Detail');
							//fld_proj_detail.setDefaultValue(url_proj_detail_url);
							var month_line_no=1;
							// Add sublist on form to update effort plan
					var fld_sublist_effort_plan_mnth_end_ = o_form_obj.addSubList('revenue_sublist_month_end', 'inlineeditor', 'Effort Plan Month End', 'effort_plan_tab_month_end');
							//var fld_is_allocation_row = fld_sublist_effort_plan_mnth_end.addField('is_resource_allo_row', 'text','Is Allocation Row').setDisplayType('inline').setDefaultValue('<html><body bgcolor="red"></body></html>');
						//	var fld_practice = fld_sublist_effort_plan_mnth_end.addField('parent_practice_month_end', 'select', 'Practice', 'department');
						//	fld_sublist_effort_plan_mnth_end.addField('sub_practice_month_end', 'select', 'Sub Practice', 'department');
						//	fld_sublist_effort_plan_mnth_end.addField('role_month_end', 'select', 'Role','customlist_fp_rev_rec_roles');
						//	var fld_emp_level = fld_sublist_effort_plan_mnth_end.addField('level_month_end', 'select', 'Level'); //customlist_fp_rev_rec_level
				//	var fld_location_mnth_end = fld_sublist_effort_plan_mnth_end.addField('location_month_end', 'select', 'Location');
						//	fld_location_mnth_end.addSelectOption('', '');
						//	fld_location_mnth_end.addSelectOption('8', 'India');
						//	fld_location_mnth_end.addSelectOption('2', 'United Kingdom');
						// 	fld_location_mnth_end.addSelectOption('9', 'US');
						var mnth_reven_count=nlapiGetLineItemCount('people_plan_sublist');
						var mnth_rev=fld_sublist_effort_plan_mnth_end_.addField('monthly_split','text', 'Label').setDisplayType('disabled');
						nlapiLogExecution('debug','len',mnth_reven_count);
						fld_sublist_effort_plan_mnth_end_.setLineItemValue('monthly_split',month_line_no,'Monthly Split');
						
						var len_split = monthBreakUp.length;		
						for (var j = 0; j < monthBreakUp.length; j++)
							{
								var months = monthBreakUp[j];
								var s_month_name = getMonthName(months.Start); // get month name
								var month_strt = nlapiStringToDate(months.Start);
								var month_name_yr = month_strt.getMonth();
								var month_end = nlapiStringToDate(months.End);
								var i_month = month_strt.getMonth();
								var i_year = month_strt.getFullYear();
								s_month_name = s_month_name +'_'+ i_year;
								var d_today_date = new Date();
								var today_month_yr = d_today_date.getMonth();
								var prev_year = d_today_date.getFullYear();
								//if(month_end <= d_today_date)
								//if((month_name_yr < today_month_yr))
								if(month_end <= d_today_date)
								{
									fld_sublist_effort_plan_mnth_end_.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name).setDisplayType('disabled');
								}
								else
								{
									fld_sublist_effort_plan_mnth_end_.addField(''+s_month_name.toLowerCase(), 'float', ''+s_month_name);
								}
							}
							var project_split  = i_project_sow_value / len_split;
							nlapiLogExecution('debug','project_split',project_split);
							var  billid=0;
							for (var j = 0; j < monthBreakUp.length; j++)
							{
								//Get Transactions
								var months = monthBreakUp[j];
								var s_month_name = getMonthName(months.Start); // get month name
								var month_strt = nlapiStringToDate(months.Start);
								var month_name_yr = month_strt.getMonth();
								var month_end = nlapiStringToDate(months.End);
								var i_month = month_strt.getMonth();
								var i_year = month_strt.getFullYear();
								s_month_name = s_month_name +'_'+ i_year;
								var d_today_date = new Date();
								var today_month_yr = d_today_date.getMonth();
								var prev_year = d_today_date.getFullYear();
								var s_month_nam = getMonthName(months.Start);
								if(month_end < d_today_date)
								//if(month_name_yr < today_month_yr)
								//if((month_name_yr < today_month_yr)&&(i_year <= prev_year))
								{
									var f_amount_pp=search_ytd_rev(s_month_nam,i_year,i_projectId);
									fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),1,parseInt(f_amount_pp));
								}
								
								else
								{
									var mnth_end_json_fil = [['custrecord_fp_rev_rec_month_end_parent_l', 'anyof', parseInt(i_existing_revenue_share_rcrd_id)],'and',
																			['custrecord_fp_created_by_json','is','T']];
									var mnth_end_json_col = new Array();
									mnth_end_json_col[0] = new nlobjSearchColumn('created').setSort(true);
									var a_get_mnth_end_Json = nlapiSearchRecord('customrecord_fp_rev_rec_others_mont_end', null, mnth_end_json_fil, mnth_end_json_col);
									//nlapiLogExecution('DEBUG','Milestone JSon Id',a_get_mnth_end_Json);
									/*if(a_get_mnth_end_Json)
									{
										for(var i_indx_mnt=0;i_indx_mnt<1;i_indx_mnt++)
										{
											var mnth_end_id=a_get_mnth_end_Json[i_indx_mnt].getId();
										
										var effor_json_filt=['custrecord_fp_others_mnth_act_parent','anyof',parseInt(mnth_end_id)];
										var effor_json_col= new Array();
										effor_json_col[0] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
										//var effor_json_col[0]=new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
										effor_json_col[1]=new nlobjSearchColumn('custrecord_fp_others_mnth_end_year_y',null,'group');
										 effor_json_col[2]=new nlobjSearchColumn('custrecord_mnth_split_share',null,'group');
										 effor_plan_json=searchRecord('customrecord_fp_rev_rec_othr_mnth_end_ef',null,effor_json_filt,effor_json_col);
										if(effor_plan_json)
										{
											for(var effort_indx=0;effort_indx<effor_plan_json.length;effort_indx++)
											{
												var effort_mnth=effor_plan_json[effort_indx].getValue('custrecord_fp_others_mnth_end_year',null,'group');
												var effort_year=effor_plan_json[effort_indx].getValue('custrecord_fp_others_mnth_end_year_y',null,'group');
												var effort_split=effor_plan_json[effort_indx].getValue('custrecord_mnth_split_share',null,'group');
												var mnth_year=effort_mnth+'_'+parseInt(effort_year);
												if(s_month_name==mnth_year)
												{
													if(!_logValidation(effort_split))
													{
														effort_split=0;
													}
													fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),1,parseInt(effort_split));
												}
											}
										}
										
										}
									}
									else*/
									{
										var mnth_split_fil=[['custrecord_fp_rev_rec_other','anyof',parseInt(i_existing_revenue_share_rcrd_id)],'and',
										['custrecord_created_split','is','T'], 'and', ['isinactive','is','F']];
										var mnth_split_col=new Array();
										mnth_split_col[0]=new nlobjSearchColumn('custrecord_other_month');
										mnth_split_col[1] = new nlobjSearchColumn('created').setSort(true);
										var mnth_split_search=nlapiSearchRecord('customrecord_fp_other_monthly_rev',null,mnth_split_fil,mnth_split_col);
										if(mnth_split_search)
										{
											for(var i_mnth_split=0;i_mnth_split<1;i_mnth_split++)
											{
												var mnth_split_json=mnth_split_search[0].getValue('custrecord_other_month');
												var mnth_json_arr=JSON.parse(mnth_split_json);
												var mnth_json_len=mnth_json_arr[0].length;
												var monthSplitData = mnth_json_arr[0];
												for(var i_mnth_json=0;i_mnth_json<mnth_json_len;i_mnth_json++)
												{
													//Code udpated on 4th FEB
													var data_array = monthSplitData[i_mnth_json];
													var mnth_json = data_array.month;
													var amount_json = data_array.amount;
                                                
													var line_no = data_array.line_no;
													
													//End
													//var mnth_json=mnth_json_arr[0][i_mnth_json].month;
													//var amount_json=mnth_json_arr[0][i_mnth_json].amount;
                                                
													//var line_no=mnth_json_arr[0][i_mnth_json].line_no;
													if((s_month_name==mnth_json ))
													  if(_logValidation(amount_json))
                                                    {
                                                                      	fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),line_no,parseInt(amount_json));
                                            	    }
												   else{
													                  	fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),line_no,'');
												   }
												}
											}
										}
										else if(a_get_mnth_end_Json)
										{
											for(var i_indx_mnt=0;i_indx_mnt<1;i_indx_mnt++)
											{
												var mnth_end_id=a_get_mnth_end_Json[i_indx_mnt].getId();
												
												var effor_json_filt=['custrecord_fp_others_mnth_act_parent','anyof',parseInt(mnth_end_id)];
												var effor_json_col= new Array();
												effor_json_col[0] = new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
												//var effor_json_col[0]=new nlobjSearchColumn('custrecord_fp_others_mnth_end_year',null,'group');
												effor_json_col[1]=new nlobjSearchColumn('custrecord_fp_others_mnth_end_year_y',null,'group');
												effor_json_col[2]=new nlobjSearchColumn('custrecord_mnth_split_share',null,'group');
												effor_plan_json=searchRecord('customrecord_fp_rev_rec_othr_mnth_end_ef',null,effor_json_filt,effor_json_col);
												if(effor_plan_json)
												{
													for(var effort_indx=0;effort_indx<effor_plan_json.length;effort_indx++)
													{
														var effort_mnth=effor_plan_json[effort_indx].getValue('custrecord_fp_others_mnth_end_year',null,'group');
														var effort_year=effor_plan_json[effort_indx].getValue('custrecord_fp_others_mnth_end_year_y',null,'group');
														var effort_split=effor_plan_json[effort_indx].getValue('custrecord_mnth_split_share',null,'group');
														var mnth_year=effort_mnth+'_'+parseInt(effort_year);
														if(s_month_name==mnth_year)
														{
															if(!_logValidation(effort_split))
															{
																effort_split=0;
															}
															fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),1,parseInt(effort_split));
														}
													}
												}
												
											}
										}
										else
										{
											var mnth_split_fil=['custrecord_fp_rev_rec_other','anyof',parseInt(i_existing_revenue_share_rcrd_id)];
											var mnth_split_col=new Array();
											mnth_split_col[0]=new nlobjSearchColumn('custrecord_other_month');
											var mnth_split_search=nlapiSearchRecord('customrecord_fp_other_monthly_rev',null,mnth_split_fil,mnth_split_col);
											for(var i_mnth_split=0;i_mnth_split<mnth_split_search.length;i_mnth_split++)
											{
												var mnth_split_json=mnth_split_search[i_mnth_split].getValue('custrecord_other_month');
												var mnth_json_arr=JSON.parse(mnth_split_json);
												
												var mnth_json_len=mnth_json_arr[0].length;
												for(var i_mnth_json=0;i_mnth_json<mnth_json_len;i_mnth_json++)
												{
													var mnth_json=mnth_json_arr[0][i_mnth_json].month;
													var amount_json=mnth_json_arr[0][i_mnth_json].amount;
													if(s_month_name==mnth_json)
												fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),1,parseInt(amount_json));
												}
												
											}
										}
									}
									
								}
									
								
								//fld_sublist_effort_plan_mnth_end.setLineItemValue(''+s_month_name.toLowerCase(),1,parseInt(project_split));	
							}	
							var unique_list=remove_dulpi(practices_involved);
							var line_mon=month_line_no;
							for(var pract_indx=0;pract_indx<unique_list.length;pract_indx++)
							{
									line_mon++;
									fld_sublist_effort_plan_mnth_end_.setLineItemValue('monthly_split',line_mon,unique_list[pract_indx]);
							}
							var ytd_line=month_line_no;
							for(var pract_indx=0;pract_indx<unique_list.length;pract_indx++){
								ytd_line++;
								for (var j_mn = 0; j_mn < monthBreakUp.length; j_mn++)
								{
								//Get Transactions
								var months = monthBreakUp[j_mn];
								var s_month_name = getMonthName(months.Start); // get month name
								var month_strt = nlapiStringToDate(months.Start);
								var month_name_yr = month_strt.getMonth();
								var month_end = nlapiStringToDate(months.End);
								var i_month = month_strt.getMonth();
								var i_year = month_strt.getFullYear();
								s_month_name = s_month_name +'_'+ i_year;
								var d_today_date = new Date();
								var today_month_yr = d_today_date.getMonth();
								var prev_year = d_today_date.getFullYear();
								var s_month_nam = getMonthName(months.Start);
								if(month_end < d_today_date)
								//if(month_name_yr < today_month_yr)
								//if((month_name_yr < today_month_yr)&&(i_year <= prev_year))
								{
									var f_amount_pp=search_ytd_rev_pract(s_month_nam,i_year,i_projectId,unique_list[pract_indx]);
									fld_sublist_effort_plan_mnth_end_.setLineItemValue(''+s_month_name.toLowerCase(),ytd_line,parseInt(f_amount_pp));
								}
							}
						}
						// Add subtab for effort plan
							var tab_effort_plan = o_form_obj.addSubTab('effort_plan_tab', 'Plan Effort Budget');
							// Add sublist on form to update practice and revenue share
							var fld_form_sublist_effort_plan = o_form_obj.addSubList('effort_sublist', 'list', 'Effort Plan', 'effort_plan_tab');
							var fld_parent_prac_effort_sublist = fld_form_sublist_effort_plan.addField('parent_practice_effort_plan', 'select', 'Practice', 'department').setDisplayType('inline');
							var fld_sub_prac_effort_sublist = fld_form_sublist_effort_plan.addField('sub_practice_effort_plan', 'select', 'Sub Pratice').setDisplayType('inline');
							var f_role_field = fld_form_sublist_effort_plan.addField('role_effort_plan', 'select', 'Role','customlist_fp_rev_rec_roles').setDisplayType('inline');
							var level_fld_effrt_pln = fld_form_sublist_effort_plan.addField('level_effort_plan', 'select', 'Level').setDisplayType('disabled').setDisplayType('inline');
							fld_form_sublist_effort_plan.addField('no_resources_effort_plan', 'integer', '# of resources');
							fld_form_sublist_effort_plan.addField('allocation_strt_date_effort_plan', 'date', 'Allocation Start Date (MM/DD/YY)');
							fld_form_sublist_effort_plan.addField('allocation_end_date_effort_plan', 'date', 'Allocation End Date (MM/DD/YY)');
							fld_form_sublist_effort_plan.addField('prcnt_allocation_effort_plan', 'integer', '% Allocation');
							var fld_location = fld_form_sublist_effort_plan.addField('location_effort_plan', 'select', 'Location').setDisplayType('inline');
							fld_form_sublist_effort_plan.addField('cost_effort_plan', 'currency', 'Resource Cost(USD)');
							
							
							var a_filter_get_corporate_prac = [['custrecord_practice_type_ticker', 'anyof', 2], 'and',
													['isinactive', 'is', 'F']];
								
							var a_columns_get_corporate_prac = new Array();
							a_columns_get_corporate_prac[0] = new nlobjSearchColumn('name');
	
							var a_get_corporate_prac = nlapiSearchRecord('department', null, a_filter_get_corporate_prac, a_columns_get_corporate_prac);
							if (a_get_corporate_prac)
							{
								for(var i_prac_index=0; i_prac_index<a_get_corporate_prac.length; i_prac_index++)
								{
									fld_sub_prac_effort_sublist.addSelectOption(a_get_corporate_prac[i_prac_index].getId(), a_get_corporate_prac[i_prac_index].getValue('name'));
								}
							}
						
							level_fld_effrt_pln.addSelectOption('', '');
							level_fld_effrt_pln.addSelectOption('31', '0');
							level_fld_effrt_pln.addSelectOption('1', '1');
							level_fld_effrt_pln.addSelectOption('15', '1A');
							level_fld_effrt_pln.addSelectOption('16', '1B');
							level_fld_effrt_pln.addSelectOption('2', '2');
							level_fld_effrt_pln.addSelectOption('17', '2A');
							level_fld_effrt_pln.addSelectOption('18', '2B');
							level_fld_effrt_pln.addSelectOption('3', '3');
							level_fld_effrt_pln.addSelectOption('10', '3A');
							level_fld_effrt_pln.addSelectOption('19', '3B');
							level_fld_effrt_pln.addSelectOption('4', '4');
							level_fld_effrt_pln.addSelectOption('20', '4A');
							level_fld_effrt_pln.addSelectOption('21', '4B');
							level_fld_effrt_pln.addSelectOption('5', '5');
							level_fld_effrt_pln.addSelectOption('22', '5A');
							level_fld_effrt_pln.addSelectOption('23', '5B');
							level_fld_effrt_pln.addSelectOption('6', '6');
							level_fld_effrt_pln.addSelectOption('11', '6A');
							level_fld_effrt_pln.addSelectOption('14', '6B');
							level_fld_effrt_pln.addSelectOption('7', '7');
							level_fld_effrt_pln.addSelectOption('24', '7A');
							level_fld_effrt_pln.addSelectOption('25', '7B');
							level_fld_effrt_pln.addSelectOption('8', '8');
							level_fld_effrt_pln.addSelectOption('26', '8A');
							level_fld_effrt_pln.addSelectOption('27', '8B');
							level_fld_effrt_pln.addSelectOption('9', '9');
							level_fld_effrt_pln.addSelectOption('28', '9A');
							level_fld_effrt_pln.addSelectOption('29', '9B');
							level_fld_effrt_pln.addSelectOption('13', 'CW');
							level_fld_effrt_pln.addSelectOption('30', 'PT');
							level_fld_effrt_pln.addSelectOption('12', 'TS');
							level_fld_effrt_pln.addSelectOption('33', 'L');
							
							fld_location.addSelectOption('', '');
							fld_location.addSelectOption('', '');	
							for(var key in location_monthend){ 	
							fld_location.addSelectOption(key,location_monthend[key]);                          	
							}
						
							//fld_parent_prac_prac_sublist.addSelectOption('', '');
							//fld_parent_prac_effort_sublist.addSelectOption('', '');
							
							//fld_sub_prac_prac_sublist.addSelectOption('', '');
							//fld_sub_prac_effort_sublist.addSelectOption('', '');
							
							
							// Fetch existing effort entered
							var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', parseInt(i_projectId)]];
					
							var a_columns_existing_cap_srch = new Array();
							a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
							a_columns_existing_cap_srch[1] = new nlobjSearchColumn('created').setSort(true);
							a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
							
							a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
							if (a_get_logged_in_user_exsiting_revenue_cap)
							{
								var a_proj_effort_plan = new Array();
								var i_parent_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
								
								var a_budget_effrt_filter = [['custrecord_fp_rev_rec_others_parent_link', 'anyof', parseInt(i_parent_id)]];
					
								var a_columns_existing_effrt = new Array();
								a_columns_existing_effrt[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_practice').setSort(true);
								a_columns_existing_effrt[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_sub_practic');
								a_columns_existing_effrt[2] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_role');
								a_columns_existing_effrt[3] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_level');
								a_columns_existing_effrt[4] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_no_of_resou');
								a_columns_existing_effrt[5] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_alloc_st_da');
								a_columns_existing_effrt[6] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_alloc_end_d');
								a_columns_existing_effrt[7] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_percent_all');
								a_columns_existing_effrt[8] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_location');
								a_columns_existing_effrt[9] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_cost');
								a_columns_existing_effrt[10] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_project');
								
								var o_effort_entered = searchRecord('customrecord_fp_rev_rec_others_eff_plan', null, a_budget_effrt_filter, a_columns_existing_effrt);
								if (o_effort_entered)
								{
									for(var i_effort_index=0; i_effort_index<o_effort_entered.length; i_effort_index++)
									{
										var a_JSON_effort_plan = {
											sub_practice_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_sub_practic'),
											parent_practice_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_practice'),
											role_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_role'),
											level_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_level'),
											no_resources_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_no_of_resou'),
											allocation_strt_date_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_alloc_st_da'),
											allocation_end_date_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_alloc_end_d'),
											prcnt_allocation_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_percent_all'),
											location_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_location'),
											cost_effort_plan: o_effort_entered[i_effort_index].getValue('custrecord_fp_rev_rec_others_cost')
										}
										
										a_proj_effort_plan.push(a_JSON_effort_plan);
									}
								}
								
								fld_form_sublist_effort_plan.setLineItemValues(a_proj_effort_plan);
							}	
							
							o_form_obj.addSubmitButton('Generate Project Detail');
							o_form_obj.setScript('customscript_cli_fp_revrec_revenue_vali');// Client script for validation		
						}
				}
			else{
			throw 'No Project Selected/Existing';
			}
			} //get method
			  response.writePage(o_form_obj);
			}

			//try
			//}
			//display form on suitelet window
		      else
			  {
				  //else if(s_request_type == 'MonthEnd')
				{
					
					var i_projectId = request.getParameter('proj_selected');
				
				var s_request_type = request.getParameter('share_type');
				
				var b_revenue_rcrd_updated = request.getParameter('change_triggered');
				
				var b_proj_val_chngd = 'F';
				
				var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', parseInt(i_projectId)]];
					
				var a_column = new Array();
				a_column[0] = new nlobjSearchColumn('created').setSort(true);
				a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_auto_numb');
				a_column[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
				
				var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
				if (a_get_logged_in_user_exsiting_revenue_cap)
				{
					var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
					var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_other_approval_status');
				}
					var a_effort_activity_mnth_end_filter = [['custrecord_fp_others_mnth_end_fp_parent', 'anyof', parseInt(i_revenue_share_id)]];
			
					var a_columns_mnth_end_effort_activity_srch = new Array();
					a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
					
					var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
					if (a_get_mnth_end_effrt_activity)
					{
						var i_month_end_activity_id = a_get_mnth_end_effrt_activity[0].getId();
					}
							
					var a_url_parameters = new Array();
					a_url_parameters['proj_id'] = i_projectId;
					a_url_parameters['mode'] = 'View';
					a_url_parameters['mnth_end_activity_id'] = i_month_end_activity_id;
					a_url_parameters['revenue_rcrd_status'] = i_revenue_share_status;
					a_url_parameters['revenue_share_id'] = i_revenue_share_id;
					
					nlapiSetRedirectURL('SUITELET', '1518', 'customdeploy_fp_othr_mnthend_screen', null, a_url_parameters);
				}
			  }
	}
catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}	
	
}

function find_prac_exist(a_current_pract,i_prac_to_find,i_level_to_find)
{
	var i_prac_present = 0;
	for(var i_prac_find=0; i_prac_find<a_current_pract.length; i_prac_find++)
	{
		if(a_current_pract[i_prac_find].i_sub_practice == i_prac_to_find && a_current_pract[i_prac_find].i_level == i_level_to_find)
		{
			i_prac_present = 1;
		}
	}
	
	return i_prac_present;
}
function find_uniq_prac_exist(a_current_pract_un,i_prac_to_find)
{
	var i_prac_present_un = 0;
	for(var i_prac_find=0; i_prac_find<a_current_pract_un.length; i_prac_find++)
	{
		if(a_current_pract_un[i_prac_find].i_sub_practice == i_prac_to_find)
		{
			i_prac_present_un = 1;
		}
	}
	
	return i_prac_present_un;
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function generate_table_effort_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += '<p><font size="2" color="red">Recent Effort Plan</font></p>';
	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "Practice";
	html += "</td>";
	
	html += "<td>";
	html += "Sub Practice";
	html += "</td>";

	html += "<td>";
	html += "Role";
	html += "</td>";
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
		
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			
			html += "<tr>";
			html += "<td class='label-name'>";
			html += projectWiseRevenue[emp_internal_id].practice_name;
			html += "</td>";
				
			html += "<td class='label-name'>";
			html += projectWiseRevenue[emp_internal_id].sub_prac_name;
			html += "</td>";
		
			html += "<td class='label-name'>";
			html += projectWiseRevenue[emp_internal_id].role_name;
			html += "</td>";
				
			var i_sub_practice_internal_id = 0;
			for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
			{
				var a_MonthNames = MonthNames(i_year_project);
				var currentMonthPos = a_MonthNames.indexOf(month);
				
				if(currentMonthPos >= 0)
				{
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
				}
				
				if (i_project_mnth <= currentMonthPos && parseInt(s_current_mnth_yr) >= i_year_project) {
					
					html += "<td class='monthly-amount'>";
					html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
					html += "</td>";
				}
			}
			html += "</tr>";			
		}
	}

	html += "</table>";
	
	return html;
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
      var endDate = nlapiAddDays(nlapiAddMonths(new_start_date, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0
		};
	}
}
	
function searchForPractice(project)
{
	try
	{
		var a_JSON = {};
		var a_dataRows = [];
		var a_filters_project = new Array();
		a_filters_project.push(new nlobjSearchFilter('project',null,'anyof',project));
		
		var a_column_get_practice = new Array();
		a_column_get_practice.push(new nlobjSearchColumn('custevent_practice',null,'group'));
		a_column_get_practice.push(new nlobjSearchColumn('custrecord_parent_practice','custevent_practice','group'));
		
		var a_search_allocation = nlapiSearchRecord('resourceallocation',null,a_filters_project,a_column_get_practice);
		if(a_search_allocation){
			for(var i=0;i<a_search_allocation.length;i++){
				
				a_JSON = {
						sub_practice: a_search_allocation[i].getValue('custevent_practice',null,'group'),
						parent_practice: a_search_allocation[i].getValue('custrecord_parent_practice','custevent_practice','group')
				}
				a_dataRows.push(a_JSON);
			}
		}
		return a_dataRows;
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','searchForPractice :- ',err);
	}

}

function getMonthName_FromMonthNumber(month_number)
{
	var a_month_name = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
								
	return a_month_name[month_number];
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function searchJournal(d_strtdate,d_endDate,s_project_id){
	
	var f_amt_captured = 0;
	var a_filters = [];
	a_filters.push(new nlobjSearchFilter('type', null, 'anyof', 'Journal'));
	a_filters.push(new nlobjSearchFilter('trandate', null, 'within', d_strtdate,d_endDate));
	a_filters.push(new nlobjSearchFilter('custcol_project_entity_id', null, 'is', s_project_id));
	
								  
		var a_columns = new Array();
		a_columns[0] = new nlobjSearchColumn('custcol_project_entity_id',null,'group');
		a_columns[1] = new nlobjSearchColumn('fxamount',null,'sum');
		
		var tran_search = nlapiSearchRecord('transaction',null,a_filters,a_columns);
		if(tran_search){
			f_amt_captured = tran_search[0].getValue('fxamount',null,'sum');
		}
	return f_amt_captured;
} 

function searchMilestone(billing_sch)
{
                var billing_milestone_id=nlapiLoadRecord('billingschedule',billing_sch);
                var lineItemCount=billing_milestone_id.getLineItemCount('milestone');
                var isZeroAdded = false;
                var billing_JSON=[];
                var billing_amount=[];
                for (var linenum = 1; linenum <= lineItemCount; linenum++) {
                                                var currentMileStonePercent = billing_milestone_id.getLineItemValue('milestone',
                                                                                'milestoneamount', linenum);
 
                                                // remove the %age sign
                                                var currentMileStonePercentNumeric = currentMileStonePercent
                                                                                .split('%')[0].trim();
 
                                                if (currentMileStonePercentNumeric == 0) {
                                                                isZeroAdded = true;
                                                                break;
                                                }
 
                                                nlapiLogExecution('debug', 'currentMileStonePercentNumeric',
                                                                                currentMileStonePercentNumeric);
                               
                                                var currentMileStoneMont = billing_milestone_id.getLineItemValue('milestone',
                                                                                'milestonedate', linenum);
                                                                //            currentMilestoneMonth=getMonthName(currentMilestoneMonth);
                                                                var current_mnth=getMonthName(currentMileStoneMont);
                                                               
                                                                                var month_strt = nlapiStringToDate(currentMileStoneMont);
                                                                                var i_year = month_strt.getFullYear();
                                                                                nlapiLogExecution('debug', 'i_year',
                                                                                i_year);
                                                                                month_strt=current_mnth+'_'+i_year;
                                                                                nlapiLogExecution('debug', 'currentMileStoneMonth',
                                                                                month_strt);     
                                                // check if any invalid amount was entered in comments
                                                var currentLineAmountText = billing_milestone_id.getLineItemValue('milestone',
                                                                                'comments', linenum);
                                                                                                var currentLineAmountNumeric = extractNumberFromComment(currentLineAmountText);
                                                                nlapiLogExecution('debug', 'currentLineAmountNumeric',
                                                                                                currentLineAmountNumeric);
                                                                                                billing_JSON={
                                                                                                                month:month_strt,
                                                                                                                amount:currentLineAmountNumeric
                                                                                                };
                                                                                                billing_amount.push(billing_JSON);
                                                nlapiLogExecution('debug', 'currentLineAmountText',
                                                                                currentLineAmountText);
                                                if (!currentLineAmountText) {
                                                                throw "Amount entered in comments is not valid";
                                                }
                                }
                                if (isZeroAdded) {
                                                // get the project value
                                                var projectId = nlapiGetFieldValue('project');
                                                var projectValue = nlapiLookupField('job', projectId,
                                                                                'custentity_projectvalue');
                                                var sum = 0;
 
                                                // loop through line item and calculate percentage
                                                for (var linenum = 1; linenum < lineItemCount; linenum++) {
                                                                // read the amount from comments field
                                                                var currentLineAmountText = billing_milestone_id.getLineItemValue('milestone',
                                                                                                'comments', linenum);
 
                                                                // extract the amount (number) part from the currency text
                                                                var currentLineAmountNumeric = extractNumberFromComment(currentLineAmountText);
                                                                nlapiLogExecution('debug', 'currentLineAmountNumeric',
                                                                                                currentLineAmountNumeric);
 
                                                                // calculate the %age milestone using the project value
                                                                var percentage = (currentLineAmountNumeric / projectValue) * 100;
 
                                                                // set the new milestone %age
                                                               
 
                                                                sum += parseFloat(percentage.toFixed(4));
                                                }
 
                                                // to calculate the last line %age, subtract the sum from 100
                                                var lastLinePercentage = 100 - sum;
                                                nlapiSetLineItemValue('milestone', 'milestoneamount',
                                                                                lineItemCount, lastLinePercentage);
                                }
                return billing_amount;
 
}

function findAllocationPracticePresent(a_proj_prac_details, subpractice, level, subsi, i_resource_location)
{
	for(var i_index_arr_srchng=0; i_index_arr_srchng<a_proj_prac_details.length; i_index_arr_srchng++)
	{
		//if(a_proj_prac_details[i_srch_arr].i_practice == practice)
		{
			//nlapiLogExecution('audit','prac:- '+practice);
			if(parseFloat(a_proj_prac_details[i_index_arr_srchng].i_sub_practice) == parseFloat(subpractice))
			{
				if(subpractice == 324)
				{
					nlapiLogExecution('audit','sub prac:- '+subpractice);
					nlapiLogExecution('audit','level before match:- '+level);
				}
				
				if(parseFloat(a_proj_prac_details[i_index_arr_srchng].s_emp_level) == parseFloat(level))
				{
					//nlapiLogExecution('audit','level:- '+level);
					if(parseFloat(a_proj_prac_details[i_index_arr_srchng].subsidiary) == parseFloat(subsi))
					{
						//nlapiLogExecution('audit','subsi:- '+subsi);
						if(parseFloat(a_proj_prac_details[i_index_arr_srchng].i_resource_location) == parseFloat(i_resource_location))
						{
							//nlapiLogExecution('audit','loc:- '+i_resource_location);
							return i_index_arr_srchng;
						}
					}
				}
			}
		}
	}
	
	return -1;
}
 
function extractNumberFromComment(textAmount) {
                var index = textAmount.indexOf('$');
                var index2 = textAmount.indexOf('INR');
                var index3 = textAmount.indexOf('GBP');
                var index4 = textAmount.indexOf('Â£');
               
               
/*           nlapiLogExecution('debug', 'index', index);
                nlapiLogExecution('debug', 'index2', index2);
                nlapiLogExecution('debug', 'index3', index3);
                nlapiLogExecution('debug', 'index4', index4);*/
               
               
                if (index == -1 && index2 == -1 && index3 == -1 && index4 == -1) {
                                throw "Invalid Amount in comments - No $ or INR symbol found.";
                }
 
                if (index > 0 && index2 > 0 && index3 > 0 && index4> 0) {
                                throw "Invalid Amount Format - should start with $ or INR sign";
                }
 
                //var splitIndex = index != -1 ? '$' : 'INR';
               
                var splitIndex = '';
 
                if(index >= 0)
                {
                                splitIndex = '$';
                }
                else if(index2 >= 0)
                {
                                splitIndex = 'INR';
                }
                else if(index3 >= 0)
                {
                                splitIndex = 'GBP';
                }
                else
                {
                                splitIndex= 'Â£';
                }
                               
                if(splitIndex)
                {
                                var amount = textAmount.split(splitIndex)[1].trim();
                }
 
                nlapiLogExecution('debug', 'amount', amount);
 
                if (!isNumeric(amount)) {
                                throw "Invalid Amount - enter only numbers";
                }
 
                return amount;
}
function isNumeric(value) {
                return !isNaN(value);
}
function search_ytd_rev(s_currentMonthName,s_currentYear,s_project_id)
{
		var mnth_amount=0;
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', s_project_id],'and',
															['custrecord_month_to_recognize_amount', 'is', s_currentMonthName], 'and',
															['custrecord_year_to_recognize_amount', 'is', (s_currentYear).toString()]];
					
				var a_columns_get_ytd_revenue_recognized = new Array();
				a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
				a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
				a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
				//a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('custrecord_subtotal_amount');
				a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('customer','custrecord_project_to_recognize_amount');
				
				var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
				for(var i_rev_rec=0;i_rev_rec<a_get_ytd_revenue.length;i_rev_rec++)
				{
					var mnth_rec_amnt=a_get_ytd_revenue[i_rev_rec].getValue('custrecord_revenue_recognized');
					mnth_amount=parseFloat(mnth_amount)+parseFloat(mnth_rec_amnt);
				}
		}
		return mnth_amount;
}

function search_ytd_rev_pract(s_currentMonthName,s_currentYear,s_project_id,practice_line)
{
		var mnth_amount=0;
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', s_project_id],'and',
															['custrecord_month_to_recognize_amount', 'is', s_currentMonthName], 'and',
															['custrecord_year_to_recognize_amount', 'is', (s_currentYear).toString()]];
					
				var a_columns_get_ytd_revenue_recognized = new Array();
				a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
				a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
				a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
				a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
				//a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('custrecord_subtotal_amount');
				a_columns_get_ytd_revenue_recognized[8] = new nlobjSearchColumn('customer','custrecord_project_to_recognize_amount');
				
				var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
				for(var i_rev_rec=0;i_rev_rec<a_get_ytd_revenue.length;i_rev_rec++)
				{
					var sub_pra_ytd=a_get_ytd_revenue[i_rev_rec].getText('custrecord_subparctice_to_recognize_amnt');
					var mnth_rec_amnt=a_get_ytd_revenue[i_rev_rec].getValue('custrecord_revenue_recognized');
					if(sub_pra_ytd==practice_line)
					{
						mnth_amount=parseFloat(mnth_amount)+parseFloat(mnth_rec_amnt);
					}
				}
		}
		return mnth_amount;
}
function remove_dulpi(practices_involved)
{
	var unique_array=new Array();
	for(var uni_prac=0;uni_prac<practices_involved.length;uni_prac++)
	{
		if(unique_array.indexOf(practices_involved[uni_prac])<0)
		{
			unique_array.push(practices_involved[uni_prac]);
		}
	}
	return unique_array;
}