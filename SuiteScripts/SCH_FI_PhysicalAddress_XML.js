/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Oct 2016     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborpped
 * @returns {Void}
 */
//Search for the fusion Id from the XML file and match it with the existing employee records in NetSuite

function Main() {
    try {
        nlapiLogExecution("debug", 'started');

        var current_date = nlapiDateToString(new Date());
        //Log for current date
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
        //Native Javascript Date Object. 
        var d = new Date();
        // datetimetz will set the date and time to the date variable 
        var date = nlapiDateToString(d, 'datetimetz');

        var filters = new Array();
        filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', '135925'); //Physical Address Update Folder

        // get two columns so we can build a drop down list: file name and file internal ID
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('name', 'file');
        columns[1] = new nlobjSearchColumn('internalid', 'file');
        columns[2] = new nlobjSearchColumn('modified', 'file').setSort(true);
        var file_load;
        var searchResult = nlapiSearchRecord('folder', null, filters, columns);
        if (searchResult) {

            file_load = searchResult[0].getValue('internalid', 'file');

        }
        nlapiLogExecution('debug', 'searchResult.length', searchResult.length);
        var xmlFile = nlapiLoadFile(file_load);

        //var xmlData = nlapiStringToXML(nlapiGetFieldValue('custrecord_fi_fusion_xml_data'));
        var xmlDocument = xmlFile.getValue();

        var xmlDoc = nlapiStringToXML(xmlDocument);
        var xmlData = nlapiSelectNodes(xmlDoc, '//G_1');
        if (xmlData != 0 && xmlData.length > 0) {
            for (var i = 0; i < xmlData.length; i++) {
                var xmlValue = nlapiCreateRecord('customrecord_fusion_emp_physical_addr', { recordmode: 'dynamic' });
                xmlValue.setFieldValue('custrecord_fi_emp_phy_addr_type', 5);
                xmlValue.setFieldValue('custrecord_fi_emp_phy_addr_date', date);

                var regex = /<G_1>/gi, result, firstIndices = [];
                while ((result = regex.exec(xmlDocument))) {
                    firstIndices.push(result.index);
                }
                var regexs = /<\/G_1>/gi, results, lastIndices = [];
                while ((results = regexs.exec(xmlDocument))) {
                    lastIndices.push(results.index);
                }
                var res = xmlDocument.substring(firstIndices[i], lastIndices[i]);

                xmlValue.setFieldValue('custrecord_fi_xml_data', res + '</G_1>');
                var id = nlapiSubmitRecord(xmlValue, false, true);
             //   nlapiLogExecution('debug', 'XML Physical Address data', id);
            }
        }
    }
    catch (err) {
        nlapiLogExecution('error', 'main', err);
        Send_Exception_Mail(err);
       
    }
}
function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on FI Physical Address Update';
        
        var s_Body = 'This is to inform that FI Physical Address Update is having an issue and System is not able to send the details.';
        s_Body += '<br/>Issue: Code: '+err.code+' Message: '+err.message;
        s_Body += '<br/>Script: '+s_ScriptID;
        s_Body += '<br/>Deployment: '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​
