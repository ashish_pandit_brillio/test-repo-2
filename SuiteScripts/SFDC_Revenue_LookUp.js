/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Apr 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	
	if (type == "create" || type == "edit") {
		//********OpportunityID****************************************//
		var searchResOpp = null;
		var sfdcOppId = nlapiGetFieldValue("custrecord_sfdc_revenue_data_oppid");
		var sfdcProjectId = nlapiGetFieldValue("custrecord_sfdc_revenue_data_projectid");
		if(sfdcOppId){
			searchResOpp = getOppId(sfdcOppId);
		}
		if(searchResOpp){
			var oppNSId = searchResOpp[0].getId();
			nlapiSetFieldValue("custrecord_sfdc_revenue_data_opp_id", oppNSId);
		}
		//***************Project****************************************//
		if(sfdcProjectId)
		{
			var jobSearch = nlapiSearchRecord("job",null,
			[
			   ["entityid","is",regExReplacer(sfdcProjectId)]
			], 
			[
			   new nlobjSearchColumn("internalid")
			]
			);
			if(jobSearch)
			{
				nlapiSetFieldValue("custrecord_sfdc_revenue_data_proj_id_ns", jobSearch[0].getId());
			}
		}
		/************* Lookup Region *********************/
		var s_region = nlapiGetFieldValue('custrecord_sfdc_revenue_data_accregion');
		if(s_region)
		{
			var customrecord_regionSearch = nlapiSearchRecord("customrecord_region",null,
			[
			   ["name","contains",regExReplacer(s_region)]
			], 
			[
			   new nlobjSearchColumn("internalid")
			]
			);
			if(customrecord_regionSearch)
			{
				nlapiSetFieldValue('custrecord_sfdc_rev_data_ns_region',customrecord_regionSearch[0].getValue('internalid'));
			}
		}
	}
}
function getOppId(sfdcOppId) {
	var searchRes = nlapiSearchRecord("customrecord_sfdc_opportunity_record",null,
			[
			   ["custrecord_opportunity_id_sfdc","is",regExReplacer(sfdcOppId)]
			], 
			[
			]
			);
	return searchRes;
}
function regExReplacer(str)
{
	return str.replace(/\n|\r/g, "");
}