/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 20 Mar 2015 nitish.mishra
 * 
 */

function getAccessList(hasAccessField, listField) {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch =
				nlapiSearchRecord('customrecord_report_permission_list', null, [
					new nlobjSearchFilter('custrecord_rpl_employee', null, 'anyof', [
						currentEmployeeId
					]), new nlobjSearchFilter('isinactive', null, 'is', 'F'),
					new nlobjSearchFilter(hasAccessField, null, 'is', 'T')
				]);

		if (permissionSearch && isArrayNotEmpty(permissionSearch)) {

			var permission_record =
					nlapiLoadRecord('customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_value_list = permission_record.getFieldValues(listField);
			var a_text_list = permission_record.getFieldTexts(listField);
			var a_access_list = [];

			if (isArrayEmpty(a_value_list)) {
				throw "No Values Specified";
			}

			var count = a_value_list.length;

			if (count > 1) {
				a_access_list.push({
					Value : '',
					Text : '',
					IsSelected : true
				});
			}

			for (var i = 0; i < count; i++) {
				a_access_list.push({
					Value : a_value_list[i],
					Text : a_text_list[i],
					IsSelected : false
				});
			}

			// nlapiLogExecution('debug', 'C', JSON.stringify(a_access_list));

			if (count == 1) {
				a_access_list[0].IsSelected = true;
			}

			return a_access_list;
		}
		else {
			throw "You are not authorized to access this page";
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getAccessList', err);
		throw err;
	}
}

function getDepartmentList() {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch =
				nlapiSearchRecord('customrecord_report_permission_list', null, [
					new nlobjSearchFilter('custrecord_rpl_employee', null, 'anyof', [
						currentEmployeeId
					]), new nlobjSearchFilter('isinactive', null, 'is', 'F'),
					new nlobjSearchFilter('custrecord_rpl_has_dept_access', null, 'is', 'T')
				]);

		if (isArrayNotEmpty(permissionSearch)) {

			var permission_record =
					nlapiLoadRecord('customrecord_report_permission_list', permissionSearch[0]
							.getId());
			var a_department_value_list =
					permission_record.getFieldValues('custrecord_rpl_department');
			var a_department_text_list =
					permission_record.getFieldTexts('custrecord_rpl_department');
			var a_department_list = [];

			if (isArrayEmpty(a_department_value_list)) {
				throw "No department specified";
			}

			var count_department = a_department_value_list.length;

			if (count_department > 1) {
				a_department_list.push({
					Value : '',
					Text : '',
					IsSelected : true
				});
			}

			for (var i = 0; i < count_department; i++) {
				a_department_list.push({
					Value : a_department_value_list[i],
					Text : a_department_text_list[i],
					IsSelected : false
				});
			}

			if (count_department == 1) {
				a_department_list[0].IsSelected = true;
			}

			return a_department_list;
		}
		else {
			throw "You are not authorized to access this page";
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getDepartmentList', err);
		throw err;
	}
}


function getUserReportAccessList() {

	try {
		var currentEmployeeId = nlapiGetUser();
		var permissionSearch = nlapiSearchRecord('customrecord_report_permission_list', null, [
			new nlobjSearchFilter('custrecord_rpl_employee', null, 'anyof', [
				currentEmployeeId
			]), new nlobjSearchFilter('isinactive', null, 'is', 'F')
		]);

		if (permissionSearch && isArrayNotEmpty(permissionSearch)) {
			var report_access = {};

			var permission_record =
					nlapiLoadRecord('customrecord_report_permission_list', permissionSearch[0]
							.getId());

			// vertical list
			report_access.VerticalList =
					getReportAccessNode(permission_record, 'custrecord__rpl_has_vertical_access',
							'custrecord_rpl_vertical');

			// practice list
			report_access.PracticeList =
					getReportAccessNode(permission_record, 'custrecord_rpl_has_dept_access',
							'custrecord_rpl_department');

			// customer list
			report_access.CustomerList = [];

			try {
				var customer_count =
						permission_record
								.getLineItemCount('recmachcustrecord_rplc_report_permission_link');

				for (var linenum = 1; linenum <= customer_count; linenum++) {
					report_access.CustomerList.push({
						CustomerValue : permission_record.getLineItemValue(
								'recmachcustrecord_rplc_report_permission_link',
								'custrecord_rplc_customer', linenum),
						CustomerText : permission_record.getLineItemText(
								'recmachcustrecord_rplc_report_permission_link',
								'custrecord_rplc_customer', linenum),
						Practice : permission_record.getLineItemValues(
								'recmachcustrecord_rplc_report_permission_link',
								'custrecord_rplc_practice', linenum)
					});
				}
			}
			catch (e) {
				nlapiLogExecution('debug', 'error in getAccessList', e);
				report_access.CustomerList = [];
			}


			// report_access.CustomerList =
			// getReportAccessNode(permission_record,
			// 'custrecord_rpl_has_customer_access',
			// 'custrecord_rpl_customers');

			// project list
			report_access.ProjectList =
					getReportAccessNode(permission_record, 'custrecord_rpl_has_project_access',
							'custrecord_rpl_projects');

			return report_access;
		}
		else {
			throw "You are not authorized to access this page";
		}
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'getAccessList', err);
		throw err;
	}
}

function getReportAccessNode(record, accessField, listField) {

	return {
		Values : record.getFieldValues(listField),
		Texts : record.getFieldTexts(listField),
		HasAccess : isTrue(record.getFieldValue(accessField))
	};
}

function createDropdown(valueList, textList) {

	try {
		var count = valueList.length;
		var a_dropdown_list = [];

		if (count > 1) {
			a_dropdown_list.push({
				Value : '',
				Text : '',
				IsSelected : true
			});
		}

		for (var i = 0; i < count; i++) {
			a_dropdown_list.push({
				Value : valueList[i],
				Text : textList[i],
				IsSelected : false
			});
		}

		if (count == 1) {
			a_dropdown_list[0].IsSelected = true;
		}

		return a_dropdown_list;
	}
	catch (err) {
		nlapiLogExecution('error', 'createDropdown', err);
		throw err;
	}
}