/**
 * Report to show the allocation history of any employee under a PM / DM
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Jun 2015     Nitish Mishra
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response) {
	try {
		var mode = request.getParameter('mode');

		if (mode == "exportall") {
			exportAll();
		} else if (mode == "exportselected") {
			exportSelected(request);
		} else {
			createGetForm(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function createGetForm(request) {
	try {

		var form = nlapiCreateForm('Resource Allocation History');
		form.setScript('customscript_cs_emp_allocation_history');

		var context = nlapiGetContext();
		form.addField('custpage_scriptid', 'text', 'script').setDisplayType(
				'hidden').setDefaultValue(context.getScriptId());

		form.addField('custpage_deployid', 'text', 'deployment')
				.setDisplayType('hidden').setDefaultValue(
						context.getDeploymentId());

		// project list
		var project_list = form.addSubList('custpage_select_list_project',
				'staticlist', 'Select a project');
		project_list.addField('project', 'text', 'Project').setDisplayType(
				'inline');
		project_list.addButton('custpage_btn_export_project_all', "Export",
				"btnExportProjectAll");
		project_list.setLineItemValues(getProjectList());

		// employee list
		var emp_list = form.addSubList('custpage_select_list', 'staticlist',
				'Select an employee');
		emp_list.addField('employee', 'text', 'Employee').setDisplayType(
				'inline');
		emp_list.addButton('custpage_btn_export_all', "Export All",
				"btnExportAll");
		emp_list.setLineItemValues(getEmployeeList());

		// allocation history
		var allocation_history_list = form.addSubList(
				'custpage_allocation_history', 'staticlist',
				'Allocation History');
		allocation_history_list.addButton('custpage_btn_export_current',
				"Export Selected", "btnExportSelected");
		allocation_history_list.addField('employee', 'text', 'Employee')
				.setDisplayType('inline');
		allocation_history_list.addField('project', 'text', 'Project')
				.setDisplayType('inline');
		allocation_history_list.addField('allocationstartdate', 'text',
				'Start Date').setDisplayType('inline');
		allocation_history_list.addField('allocationenddate', 'text',
				'End Date').setDisplayType('inline');
		allocation_history_list.addField('percentallocation', 'text',
				'%age Allocation').setDisplayType('inline');
		allocation_history_list.addField('isbillable', 'text', 'Billable?')
				.setDisplayType('inline');
		allocation_history_list.addField('billingstartdate', 'text',
				'Billing Start Date').setDisplayType('inline');
		allocation_history_list.addField('billingenddate', 'text',
				'Billing End Date').setDisplayType('inline');
		allocation_history_list.addField('location', 'text', 'Location')
				.setDisplayType('inline');
		allocation_history_list.addField('site', 'text', 'Site')
				.setDisplayType('inline');
		allocation_history_list.addField('projectmanager', 'text',
				'Project Manager').setDisplayType('inline');
		allocation_history_list.addField('deliverymanager', 'text',
				'Delivery Manager').setDisplayType('inline');
		allocation_history_list.addField('projecttype', 'text', 'Type')
				.setDisplayType('inline');
		allocation_history_list.addField('practice', 'text', 'Practice')
				.setDisplayType('inline');

		var empId = request.getParameter('empId');
		if (empId) {
			form.addField('custpage_selected_emp', 'select', 'Employee',
					'employee').setDisplayType('hidden').setDefaultValue(empId);
			allocation_history_list
					.setLineItemValues(getEmployeeAllocationHistory(empId));
		}

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createGetForm', err);
		throw err;
	}
}

function getProjectList() {
	try {
		var currentuser = 1543;// nlapiGetUser();

		var projectSearch = nlapiSearchRecord('job', null,
				[

						[
								[ 'custentity_projectmanager', 'anyof',
										currentuser ],
								'or',
								[ 'custentity_deliverymanager', 'anyof',
										currentuser ] ], 'and',
						[ 'status', 'anyof', '2' ] ], [
						new nlobjSearchColumn('entityid'),
						new nlobjSearchColumn('altname') ]);

		var projects = [];
		var context = nlapiGetContext();

		var suiteletUrl = nlapiResolveURL('SUITELET', context.getScriptId(),
				context.getDeploymentId());

		if (projectSearch) {

			projectSearch.forEach(function(project) {
				projects.push({
					project : "<a href='" + suiteletUrl + "&unlayered=T&empId="
							+ project.getId() + "'>"
							+ project.getValue('entityid') + " "
							+ project.getValue('altname') + "</a>"
				});
			});
		}

		return projects;

	} catch (err) {
		nlapiLogExecution('ERROR', 'getProjectList', err);
		throw err;
	}
}

function getEmployeeList() {
	try {

		var currentuser = nlapiGetUser();

		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[

						[
								[ 'job.custentity_projectmanager', 'anyof',
										currentuser ],
								'or',
								[ 'job.custentity_deliverymanager', 'anyof',
										currentuser ] ],
						'and',
						[ 'enddate', 'notbefore', 'today' ],
						'and',
						[ 'employee.custentity_implementationteam', 'is', 'F' ],
						'and',
						[ 'employee.custentity_employee_inactive', 'is', 'F' ] ],
				[ new nlobjSearchColumn('resource') ]);

		var allocatedResources = [];
		var context = nlapiGetContext();

		var suiteletUrl = nlapiResolveURL('SUITELET', context.getScriptId(),
				context.getDeploymentId());

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				allocatedResources.push({
					employee : "<a href='" + suiteletUrl
							+ "&unlayered=T&empId="
							+ allocation.getValue('resource') + "'>"
							+ allocation.getText('resource') + "</a>"
				});
			});
		}

		return allocatedResources;

	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeList', err);
		throw err;
	}
}

function getEmployeeAllocationHistory(employeeId) {
	try {
		var currentuser = nlapiGetUser();
		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[
						[
								[ 'job.custentity_projectmanager', 'anyof',
										currentuser ],
								'or',
								[ 'job.custentity_deliverymanager', 'anyof',
										currentuser ] ], 'and',
						[ 'resource', 'anyof', employeeId ] ],
				[
						new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('project'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('enddate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custeventbstartdate'),
						new nlobjSearchColumn('custeventbenddate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_practice'),
						new nlobjSearchColumn('custentity_projectmanager',
								'job'),
						new nlobjSearchColumn('custentity_deliverymanager',
								'job'), new nlobjSearchColumn('jobtype', 'job') ]);

		var allocationHistory = [];

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				allocationHistory.push({
					employee : allocation.getText('resource'),
					project : allocation.getText('project'),
					allocationstartdate : allocation.getValue('startdate'),
					allocationenddate : allocation.getValue('enddate'),
					percentallocation : allocation.getValue('percentoftime'),
					billingstartdate : allocation
							.getValue('custeventbstartdate'),
					billingenddate : allocation.getValue('custeventbenddate'),
					location : allocation.getText('custeventwlocation'),
					site : allocation.getText('custevent4'),
					practice : allocation.getText('custevent_practice'),
					projectmanager : allocation.getText(
							'custentity_projectmanager', 'job'),
					deliverymanager : allocation.getText(
							'custentity_deliverymanager', 'job'),
					projecttype : allocation.getText('jobtype', 'job'),
					isbillable : isTrue(allocation
							.getValue('custeventrbillable')) ? 'Yes' : 'No'
				});
			});
		}

		return allocationHistory;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getEmployeeAllocationHistory', err);
		throw err;
	}
}

function btnExportAll() {
	try {
		var employeeCount = nlapiGetLineItemCount('custpage_select_list');

		if (employeeCount > 0) {
			var csvExportUrl = nlapiResolveURL('SUITELET',
					nlapiGetFieldValue('custpage_scriptid'),
					nlapiGetFieldValue('custpage_deployid'));

			var link = csvExportUrl + "&mode=exportall";

			var win = window.open(link);
			win.focus();
		} else {
			alert("No Employee Found");
		}
	} catch (err) {
		alert(err.message);
	}
}

function btnExportSelected() {
	try {
		var selectedEmployee = nlapiGetFieldValue('custpage_selected_emp');

		if (selectedEmployee) {
			var csvExportUrl = nlapiResolveURL('SUITELET',
					nlapiGetFieldValue('custpage_scriptid'),
					nlapiGetFieldValue('custpage_deployid'));

			var link = csvExportUrl + "&empId=" + selectedEmployee
					+ "&mode=exportselected";

			var win = window.open(link);
			win.focus();
		} else {
			alert("No Employee Selected");
		}
	} catch (err) {
		alert(err.message);
	}
}

function exportSelected(request) {
	try {

		var employeeId = request.getParameter('empId');

		if (!employeeId) {
			throw "No Employee Selected";
		}

		nlapiLogExecution('debug', 'employeeId', employeeId);

		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[ new nlobjSearchFilter('resource', null, 'anyof', employeeId) ],
				[
						new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('project'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('enddate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custeventbstartdate'),
						new nlobjSearchColumn('custeventbenddate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_practice'),
						new nlobjSearchColumn('custentity_projectmanager',
								'job'),
						new nlobjSearchColumn('custentity_deliverymanager',
								'job'), new nlobjSearchColumn('jobtype', 'job') ]);

		var csvText = "Employee,Project,Start Date,End Date,%age Allocation,Billable?,Billing Start Date, Billing End Date,";
		csvText += "Location, Site, Project Manager, Delivery Manager, Project Type, Practice,\n";

		if (allocationSearch) {

			allocationSearch
					.forEach(function(allocation) {
						csvText += allocation.getText('resource') + ",";
						csvText += allocation.getText('project') + ",";
						csvText += allocation.getValue('startdate') + ",";
						csvText += allocation.getValue('enddate') + ",";
						csvText += allocation.getValue('percentoftime') + ",";
						csvText += (isTrue(allocation
								.getValue('custeventrbillable')) ? 'Yes' : 'No')
								+ ",";
						csvText += allocation.getValue('custeventbstartdate')
								+ ",";
						csvText += allocation.getValue('custeventbenddate')
								+ ",";
						csvText += allocation.getText('custeventwlocation')
								+ ",";
						csvText += allocation.getText('custevent4') + ",";
						csvText += allocation.getText(
								'custentity_projectmanager', 'job')
								+ ",";
						csvText += allocation.getText(
								'custentity_deliverymanager', 'job')
								+ ",";
						csvText += allocation.getText('jobtype', 'job') + ",";
						csvText += allocation.getText('custevent_practice')
								+ ",";
						csvText += "\n";
					});
		}

		var empName = nlapiLookupField('employee', employeeId, 'entityid');
		var filename = "AllocationHistory_" + empName + ".csv";
		var file = nlapiCreateFile(filename, 'CSV', csvText);
		response.setContentType('CSV', filename);
		response.write(file.getValue());

	} catch (err) {
		nlapiLogExecution('ERROR', 'exportSelected', err);
		throw err;
	}
}

function exportAll() {
	try {

		var currentuser = nlapiGetUser();

		var employeeSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[

						[
								[ 'job.custentity_projectmanager', 'anyof',
										currentuser ],
								'or',
								[ 'job.custentity_deliverymanager', 'anyof',
										currentuser ] ],
						'and',
						[ 'enddate', 'notbefore', 'today' ],
						'and',
						[ 'employee.custentity_implementationteam', 'is', 'F' ],
						'and',
						[ 'employee.custentity_employee_inactive', 'is', 'F' ] ],
				[ new nlobjSearchColumn('resource') ]);

		var empList = [];
		if (employeeSearch) {

			employeeSearch.forEach(function(allocation) {
				empList.push(allocation.getValue('resource'));
			});
		}

		var allocationSearch = nlapiSearchRecord(
				'resourceallocation',
				null,
				[ new nlobjSearchFilter('resource', null, 'anyof', empList) ],
				[
						new nlobjSearchColumn('resource').setSort(),
						new nlobjSearchColumn('project'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('enddate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custeventbstartdate'),
						new nlobjSearchColumn('custeventbenddate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_practice'),
						new nlobjSearchColumn('custentity_projectmanager',
								'job'),
						new nlobjSearchColumn('custentity_deliverymanager',
								'job'), new nlobjSearchColumn('jobtype', 'job') ]);

		var csvText = "Employee,Project,Start Date,End Date,%age Allocation,Billable?,Billing Start Date, Billing End Date,";
		csvText += "Location, Site, Project Manager, Delivery Manager, Project Type, Practice,\n";

		if (allocationSearch) {

			allocationSearch
					.forEach(function(allocation) {
						csvText += allocation.getText('resource') + ",";
						csvText += allocation.getText('project') + ",";
						csvText += allocation.getValue('startdate') + ",";
						csvText += allocation.getValue('enddate') + ",";
						csvText += allocation.getValue('percentoftime') + ",";
						csvText += (isTrue(allocation
								.getValue('custeventrbillable')) ? 'Yes' : 'No')
								+ ",";
						csvText += allocation.getValue('custeventbstartdate')
								+ ",";
						csvText += allocation.getValue('custeventbenddate')
								+ ",";
						csvText += allocation.getText('custeventwlocation')
								+ ",";
						csvText += allocation.getText('custevent4') + ",";
						csvText += allocation.getText(
								'custentity_projectmanager', 'job')
								+ ",";
						csvText += allocation.getText(
								'custentity_deliverymanager', 'job')
								+ ",";
						csvText += allocation.getText('jobtype', 'job') + ",";
						csvText += allocation.getText('custevent_practice')
								+ ",";
						csvText += "\n";
					});
		}

		var filename = "AllocationHistory.csv";
		var file = nlapiCreateFile(filename, 'CSV', csvText);
		response.setContentType('CSV', filename);
		response.write(file.getValue());
	} catch (err) {
		nlapiLogExecution('ERROR', 'exportAll', err);
		throw err;
	}
}
