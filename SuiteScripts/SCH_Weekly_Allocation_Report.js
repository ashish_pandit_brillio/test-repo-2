function scheduled() {
	try {
		var currentDate = new Date();
		var currentDateString = nlapiDateToString(currentDate, 'date');
		var weekStartDate = nlapiAddDays(currentDate, -3);
		var weekStartDateString = nlapiDateToString(weekStartDate, 'date');
		var weekEndDate = nlapiAddDays(currentDate, 3);
		var weekEndDateString = nlapiDateToString(weekEndDate, 'date');
		var projectList = getActiveProjectList();
		nlapiLogExecution('debug', 'projectList.length', projectList.length);

		for (var i = 0; i < 3; i++) {
			var project = projectList[i];

			var allocationDetails = getAllocationDetails(project.getId());

			nlapiLogExecution('debug', 'done', JSON
					.stringify(allocationDetails));

			var projectName = allocationDetails[0].getText('project');

			var csvFile = createCsvFileForAllocation(allocationDetails,
					weekStartDateString, weekEndDateString);

			nlapiLogExecution('debug', 'done');

			var mailTemplate = weeklyAllocationMailTemplate(project.getValue(
					'firstname', 'custentity_projectmanager'), projectName,
					weekStartDateString, weekEndDateString, currentDateString);

			nlapiSendEmail(constant.Mail_Author.InformationSystems,
					'nitish.mishra@brillio.com', mailTemplate.Subject,
					mailTemplate.Body, null, null, null, [ csvFile ]);

			nlapiLogExecution('debug', 'email send');
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
	}
}

function suitelet(request, response) {
	try {
		var currentDate = new Date();
		var currentDateString = nlapiDateToString(currentDate, 'date');
		var weekStartDate = nlapiAddDays(currentDate, -3);
		var weekStartDateString = nlapiDateToString(weekStartDate, 'date');
		var weekEndDate = nlapiAddDays(currentDate, 3);
		var weekEndDateString = nlapiDateToString(weekEndDate, 'date');
		var projectId = request.getParameter('project');

		if (projectId) {
			// get the details of the PM and DM for the project
			var projectDetails = nlapiLookupField('job', projectId, [
					'custentity_projectmanager.firstname',
					'custentity_projectmanager.email',
					'custentity_deliverymanager.email', 'entityid',
					'companyname' ]);

			var allocationDetails = getAllocationDetails(projectId);
			var projectName = allocationDetails[0].getText('project');

			nlapiLogExecution('debug', 'done', JSON
					.stringify(allocationDetails));

			var csvFile = createCsvFileForAllocation(allocationDetails,
					weekStartDateString, weekEndDateString);

			nlapiLogExecution('debug', 'done');

			var mailTemplate = weeklyAllocationMailTemplate(
					projectDetails['custentity_projectmanager.firstname'],
					projectName, weekStartDateString, weekEndDateString,
					currentDateString);

			nlapiSendEmail(constant.Mail_Author.InformationSystems,
					'nitish.mishra@brillio.com', mailTemplate.Subject,
					mailTemplate.Body, null, null, null, [ csvFile ]);

			nlapiLogExecution('debug', 'email send');
		} else {
			throw "No Project Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'main', err);
	}
}

function getAllocationDetails(projectId) {
	try {
		nlapiLogExecution('debug', 'projectId', projectId);

		// get the allocations for this project for the week
		return nlapiSearchRecord('resourceallocation', null,
				[
						new nlobjSearchFilter('custentity_employee_inactive',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('custentity_implementationteam',
								'employee', 'is', 'F'),
						new nlobjSearchFilter('startdate', null, 'notafter',
								'thisweek'),
						new nlobjSearchFilter('enddate', null, 'notbefore',
								'startofthisweek'),
						new nlobjSearchFilter('project', null, 'anyof',
								projectId) ], [
						new nlobjSearchColumn('resource'),
						new nlobjSearchColumn('project'),
						new nlobjSearchColumn('startdate'),
						new nlobjSearchColumn('enddate'),
						new nlobjSearchColumn('custeventrbillable'),
						new nlobjSearchColumn('percentoftime'),
						new nlobjSearchColumn('custeventbstartdate'),
						new nlobjSearchColumn('custeventbenddate'),
						new nlobjSearchColumn('custeventwlocation'),
						new nlobjSearchColumn('custevent4'),
						new nlobjSearchColumn('custevent_practice'),
						new nlobjSearchColumn('custentity_projectmanager',
								'job'),
						new nlobjSearchColumn('custentity_deliverymanager',
								'job'),
						new nlobjSearchColumn('title', 'employee'),
						new nlobjSearchColumn('custentity_reportingmanager',
								'employee'),
						new nlobjSearchColumn('custevent3'),
						new nlobjSearchColumn('custevent_monthly_rate'),
						new nlobjSearchColumn('custevent_otrate'),
						new nlobjSearchColumn('numberhours') ]);
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocationDetails', err);
		throw err;
	}
}

function getActiveProjectList() {
	try {
		return searchRecord('job', null, [ new nlobjSearchFilter('status',
				null, 'anyof', [ '2' ]) ],
				[
						new nlobjSearchColumn('email',
								'custentity_projectmanager'),
						new nlobjSearchColumn('firstname',
								'custentity_projectmanager'),
						new nlobjSearchColumn('email',
								'custentity_deliverymanager'),
						new nlobjSearchColumn('firstname',
								'custentity_deliverymanager') ]);
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveProjectList', err);
		throw err;
	}
}

function createCsvFileForAllocation(searchResult, weekStartDateString,
		weekEndDateString) {
	try {
		var csv = "";

		csv += "Resource Name,";
		csv += "Designation,";
		csv += "Project Name,";
		csv += "Project Manager,";
		csv += "Delivery Manager,";
		csv += "Reporting Manager,";
		csv += "Bill Rate,";
		csv += "Monthly Rate,";
		csv += "OT Rate,";
		csv += "Number of Hours,";
		csv += "Location,";
		csv += "\n";

		searchResult
				.forEach(function(allocation) {
					csv += allocation.getText('resource') + ",";
					csv += allocation.getValue('title', 'employee') + ",";
					csv += allocation.getText('project') + ",";
					csv += allocation.getText('custentity_projectmanager',
							'job')
							+ ",";
					csv += allocation.getText('custentity_deliverymanager',
							'job')
							+ ",";
					csv += allocation.getText('custentity_reportingmanager',
							'employee')
							+ ",";
					csv += allocation.getValue('custevent3') + ",";
					csv += allocation.getValue('custevent_monthly_rate') + ",";
					csv += allocation.getValue('custevent_otrate') + ",";
					csv += allocation.getValue('numberhours') + ",";
					csv += allocation.getText('custeventwlocation') + ",";
					csv += "\n";
				});

		var projectName = searchResult[0].getText('project');
		var fileName = projectName + " ( " + weekStartDateString + " - "
				+ weekEndDateString + " ).csv";

		var csvFile = nlapiCreateFile(fileName, 'CSV', csv);
		return csvFile;
	} catch (err) {
		nlapiLogExecution('ERROR', 'createCsvForAllocation', err);
		throw err;
	}
}

function weeklyAllocationMailTemplate(firstname, projectName,
		weekStartDateString, weekEndDateString, currentDateString) {
	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + firstname + ',</p>';
	htmltext += '<p>Please find the attached resource list from NetSuite for <b>'
			+ projectName + '</b></p>';
	htmltext += '<p>Please confirm the billing details for the week '
			+ weekStartDateString + ' - ' + weekEndDateString
			+ ' , and let us know if any changes/updates by EOD( '
			+ currentDateString + ' ).</p>';
	htmltext += '<p></p>';
	htmltext += '</td></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<p><br/>Regards,<br/>';
	htmltext += 'Information Systems</p>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		Subject : 'Allocation Details - ' + projectName + " - "
				+ weekEndDateString,
		Body : htmltext
	};
}