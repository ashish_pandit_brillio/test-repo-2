/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 2 July 2020     prabhat gupta
 * 
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */



function scheduled(type) {

	try {
		var i_context = nlapiGetContext();
		//	var csvFileId = nlapiLoadFile('FUEL Back update/Fuel Funnel Report_without filter-with hire details1.csv');
		var csvFile = nlapiLoadFile('Fusion Cost per FTE Back Update/Cost per FTE Report.csv');
		
		if(csvFile.size <= 740){
			var currentdate = new Date(); 
            var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
			nlapiLogExecution('debug', 'empty file processed at', datetime);
			return -1;
			//throw new ERROR("File size is less then specified size");
		}
		
		var proccessedIds = [];	
		var csvContent = csvFile.getValue();
		var rows_data = csvContent.split(/\n/g);;
		
		nlapiLogExecution('debug', 'row data ', csvContent);
		nlapiLogExecution('debug', 'row data ', rows_data);
		nlapiLogExecution('debug', 'row data ', rows_data[0]);
		
/*		
		var subsidiarySearch = nlapiSearchRecord("subsidiary",null,
								[
								   ["isinactive","is","F"]
								], 
								[
								   new nlobjSearchColumn("name"), 
								   new nlobjSearchColumn("namenohierarchy"), 
								   new nlobjSearchColumn("internalid")
								]
								);
								
		var subsidiary = [];
           
          for(var i = 0; i < subsidiarySearch.length; i++){
			  
			  subsidiary[subsidiarySearch[i].getId()] = subsidiarySearch[i].getValue("namenohierarchy");
			  
		  }
*/
         var employee_search = searchRecord("employee",null,
					[["custentity_employee_inactive", "is", "F"],
				      "AND",
				     ["custentity_implementationteam", "is", "F"]],
					  [ new nlobjSearchColumn("subsidiarynohierarchy"), new nlobjSearchColumn("custentity_fusion_empid"), new nlobjSearchColumn("internalid","subsidiary",null)]);		  
		
		var emp_data = [];
		for(var i = 0; i < employee_search.length; i++){
			
			var emp_obj = {};
			emp_obj.empId =  employee_search[i].getId();
			emp_obj.subsidiary = employee_search[i].getText("subsidiarynohierarchy");//subsidiarynohierarchy
			emp_obj.subsidiaryId = employee_search[i].getValue("internalid","subsidiary",null);
			emp_obj.fusionId = employee_search[i].getValue("custentity_fusion_empid");
			
			emp_data.push(emp_obj);
			
			
		}
		
		
		
		
		for(var line_indx= 1; line_indx<=rows_data.length-1; line_indx++)
		{
			if(i_context.getRemainingUsage() <= 50)
			{
				nlapiYieldScript();
			}
			//Check for empyt string
			if(_logValidation(rows_data[line_indx]))
			{
			var col_data= rows_data[line_indx].split(',');
			var fusion_id= col_data[14];
			var start_Date = col_data[21];
			start_Date = FormatDate(start_Date);
			var rrf_number = '';
			
		//	var subsidiary_col_id = subsidiary.indexOf(col_data[30]);
		
	//	var sub_idx = col_data[30].toString();
		
		var sub_idx =  col_data[30].replace(/"/g,"");
		
		var subsidiary_col_id = config["legalEntity"][sub_idx];
		
		if(subsidiary_col_id){
			
			nlapiLogExecution('debug', 'Subsidiary Present internal id ', subsidiary_col_id);
		}else{
			
			var subject ='Legal Entity or subsidiary is missing from cost per fte integration.';	
			
			var body = 'Dear User, <br /><br />' +'Please map the config object in the cost per fte schedule script as per Fusion legal entity and NetSuite Subsidiary <br /><br /> Legal Entity =>'+ col_data[30];							
			
			nlapiSendEmail(140512, 442, subject, body,null,null,null,null);
			continue;
		}
			// here we searcing for particular employee based on fusion id
			var cost_per_fte_search = nlapiSearchRecord("customrecord_cost_per_fte",null,
					[["custrecord_cost_p_fte_fusion_id","is",fusion_id],"AND",
						["custrecord_cost_p_fte_legal_entity","is",subsidiary_col_id],"AND", 
                          ["custrecord_cost_p_fte_end_date","notbefore",start_Date]],
						[						   
						   new nlobjSearchColumn("custrecord_cost_p_fte_start_date"), 
						   new nlobjSearchColumn("custrecord_cost_p_fte_end_date").setSort(false), 
						   new nlobjSearchColumn("custrecord_cost_p_fte_salary_id"),
						   new nlobjSearchColumn("custrecord_cost_p_fte_action_name")
						]);
			if(_logValidation(cost_per_fte_search))
			{
				
				// here we are looping for each record stored for searched employee
				for(var i = 0; i < cost_per_fte_search.length; i++){
					
					var flag = false;
					var startDate = cost_per_fte_search[i].getValue("custrecord_cost_p_fte_start_date");
					var r_startDate = nlapiStringToDate(startDate);
					var endDate = cost_per_fte_search[i].getValue("custrecord_cost_p_fte_end_date");
					var r_endDate = nlapiStringToDate(endDate);
				
                // here we are validating start date and end date column is there in the csv report or not				
				if((col_data[21] !='NA' && _logValidation(col_data[21]))&&(col_data[22] !='NA' && _logValidation(col_data[22])))
				{
					var c_startDate = FormatDate(col_data[21]);
					var m_startDate = nlapiStringToDate(c_startDate);
					var c_endDate = FormatDate(col_data[22]);
					 var m_endDate = nlapiStringToDate(c_endDate);
					 // here we are comparing start date and end date of existing record and received .csv file to update the end date of existing record
					if(((m_startDate < r_endDate) && (r_startDate < m_startDate )) && (startDate != c_startDate)){					
												
						flag = true;
					}
					
				}
				
				var cost_per_fte_id= cost_per_fte_search[i].getId();
					if(proccessedIds.indexOf(cost_per_fte_id) == -1 ){
						var record_id =  nlapiLoadRecord('customrecord_cost_per_fte',cost_per_fte_id);
						var salary_id = record_id.getFieldValue("custrecord_cost_p_fte_salary_id");
						var ctc = record_id.getFieldValue("custrecord_cost_p_fte_ctc");
						var c_ctc = col_data[26]
						var c_salaryId = col_data[17];
						
						// if flag is true we will update the end date of existing record as -1 of received start date in csv report
						if(flag){
								
							var d_startDate = c_startDate; //FormatDate(col_data[21]);
							var d_endDate = endDateModify(d_startDate);
							nlapiLogExecution('debug', 'TheDate is: ', d_endDate);
							record_id.setFieldValue('custrecord_cost_p_fte_end_date',d_endDate);
							
							var recordId = nlapiSubmitRecord(record_id);
							 proccessedIds.push(recordId);
                             nlapiLogExecution('debug', 'updated record id', recordId);							 
                             nlapiLogExecution('debug', 'updated Fusion id record', fusion_id);								 
						}// here if both start date and end date are equal then we are updating the ctc and salary id of existing record, it will help us to handle the deleted cases in Fusion for same time frame
					else if((startDate == c_startDate) && (endDate == c_endDate)){ //&& (salary_id == c_salaryId) ){
							
							record_id.setFieldValue('custrecord_cost_p_fte_ctc',col_data[26]);
							
							record_id.setFieldValue('custrecord_cost_p_fte_salary_amount',col_data[23]); // prabhat gupta NIS-1396 29/06/2020
							
							record_id.setFieldValue('custrecord_cost_p_fte_salary_id',c_salaryId);
							
							var action_name = col_data[28];
							if(action_name){
							 action_name = col_data[28].replace(/"/g,"");
							}
							record_id.setFieldValue('custrecord_cost_p_fte_action_name',action_name);
							
							var salary_encrypt = encrypt(col_data[23]); // prabhat gupta
							record_id.setFieldValue('custrecord_cost_p_fte_salary_encrypted',salary_encrypt); // prabhat gupta NIS-1396 29/06/2020
							
							var ctc_encrypt = encrypt(col_data[26]);
							record_id.setFieldValue('custrecord_cost_p_fte_ctc_encrypted',ctc_encrypt);
							var recordId = nlapiSubmitRecord(record_id);
							proccessedIds.push(recordId);
						}// if in fusion they modified the end date of employee we will modify the end date in our system
						else if((startDate == c_startDate) && (salary_id == c_salaryId) && (ctc == c_ctc)){
							
							
							nlapiLogExecution('debug', 'The End Date is: ', c_endDate);
							record_id.setFieldValue('custrecord_cost_p_fte_end_date',c_endDate);
							
							var recordId = nlapiSubmitRecord(record_id);
							 proccessedIds.push(recordId);
							
						}// if in fusion they deleted the new proposed record and created the new one then we are deleting the existing record in our system
						else if(((r_endDate > m_endDate) || (endDate == c_endDate) ) && (salary_id != c_salaryId)){
							nlapiLogExecution('debug', 'record deleted id: ', cost_per_fte_search[i].getId());
							nlapiDeleteRecord(cost_per_fte_search[i].getRecordType(), cost_per_fte_search[i].getId());
							continue;
						}
                    
					
					//if 
						if((startDate != c_startDate) && (salary_id != c_salaryId)){
							var new_record_id = createCostFteRecord(emp_data, col_data);
							proccessedIds.push(new_record_id);
							nlapiLogExecution('debug', 'created new record id search', new_record_id);
							nlapiLogExecution('debug', 'new record search Fusion id', fusion_id);	
					    }
						
					}
					
										
			    }					
			}else{
				
				var new_record_id = createCostFteRecord(emp_data, col_data);
				//proccessedIds.push(new_record_id);			
				
				nlapiLogExecution('debug', 'created new record id', new_record_id);
				nlapiLogExecution('debug', 'created new record Fusion id', fusion_id);	
			}				
             
			 
				nlapiLogExecution('debug', 'Fusion id', fusion_id);	
		}
		}

	}
	catch(err)
	{
		nlapiLogExecution('ERROR', 'scheduled cost per fte:'+cost_per_fte_id, err);
	}

}
function FormatDate(dateis)
{
	// Netsuite Format is MM/DD/YYYY
	// Fusion format is YYYY/MM/DD
	var r  = dateis.split(" ");
	var sp = r[0].split("-");
	return sp[1]+"/"+sp[2].slice(0,2)+"/"+sp[0];

}
var month = [' ','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
function FormatDateWithMonth(dateis)
{
	// Netsuite Format is MM/DD/YYYY
	// Taleo format is DD/MM/YYYY
	
	var sp = r[0].split("-");
	var month_ind = sp[1];
	return monthName(month_ind)+"/"+sp[0]+"/"+sp[2].s;
//	return nlapiDateToString(new Date(sp[2],sp[0]-1,sp[1]));// YYYY/mm-1/dd
}
function _logValidation(value) 
{
	if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}

function monthName(name){
	var indx = '';
	if(name == 'Jan')
		indx = 01;
	if(name == 'Feb')
		indx = 02;
	if(name == 'Mar')
		indx = 03;
	if(name == 'Apr')
		indx = 04;
	if(name == 'May')
		indx = 05;
	if(name == 'Jun')
		indx = 06;
	if(name == 'Jul')
		indx = 07;
	if(name == 'Aug')
		indx = 08;
	if(name == 'Sep')
		indx = 09;
	if(name == 'Oct')
		indx = 10;
	if(name == 'Nov')
		indx = 11;
	if(name == 'Dec')
		indx = 12;
	
	return indx;
}

function createCostFteRecord(emp_data, column_data){
	var cost_per_fte = nlapiCreateRecord('customrecord_cost_per_fte');
	var sub_idx =  column_data[30].replace(/"/g,"");
	                var fusion_Id = column_data[14];
	                cost_per_fte.setFieldValue('custrecord_cost_p_fte_fusion_id',fusion_Id);
					
					//var subsidiary_id = subsidiary.indexOf(column_data[30]);
					
					var subsidiary_id = config["legalEntity"][sub_idx];
                     if(_logValidation(subsidiary_id)){					
					     cost_per_fte.setFieldValue('custrecord_cost_p_fte_legal_entity',subsidiary_id);
					}
					
					//var salaryId = (column_data[17]);
					
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_salary_id',column_data[17]);
					
					var salary_basis = column_data[18];
					if(salary_basis){
					 salary_basis = column_data[18].replace(/"/g,"");
					}
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_salary_basis',salary_basis);
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_currency_code',column_data[20]);	

                    if(column_data[21] !='NA' && _logValidation(column_data[21]))
					{
						var startDate = FormatDate(column_data[21]);
						
						nlapiLogExecution('debug', 'startDate is: ', startDate);
						cost_per_fte.setFieldValue('custrecord_cost_p_fte_start_date',startDate);
					}						
					
					if(column_data[22] !='NA' && _logValidation(column_data[22]))
					{
						var endDate = FormatDate(column_data[22]);
						
						nlapiLogExecution('debug', 'startDate is: ', endDate);
						cost_per_fte.setFieldValue('custrecord_cost_p_fte_end_date',endDate);
					}	
					
					
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_ctc',column_data[26]);
					
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_salary_amount',column_data[23]); // prabhat gupta NIS-1396 29/06/2020
					
					var action_name = column_data[28];
					if(action_name){
					 action_name = column_data[28].replace(/"/g,"");
					}
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_action_name',action_name);			
					
					var salary_encrypt = encrypt(column_data[23]);  // prabhat gupta
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_salary_encrypted',salary_encrypt); // prabhat gupta NIS-1396 29/06/2020
					
					var ctc_encrypt = encrypt(column_data[26]);
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_ctc_encrypted',ctc_encrypt);
					
					var emp_id = "";
					
					for(var i = 0; i < emp_data.length; i++){
						
						if(fusion_Id == emp_data[i].fusionId && subsidiary_id == emp_data[i].subsidiaryId){
							
							emp_id = emp_data[i].empId;
							break;
						}
						
					}
					
					cost_per_fte.setFieldValue('custrecord_cost_p_fte_employee',emp_id);
					
	  var record_id = nlapiSubmitRecord(cost_per_fte);	
  
     return record_id;  
	
}

function encrypt(data){
	
	var encrypted_data = nlapiEncrypt(data, "aes", "7442A2900DA9A2C7E537BA94F939F191");//48656C6C6F0B0B0B0B0B0B0B0B0B0B0B  // prabhat gupta NIS-1396 29/06/2020
	nlapiLogExecution('debug', 'encrypted data: ', encrypted_data);                    
	return encrypted_data;
	
}

function endDateModify(startDate){
	
	var end_date = nlapiStringToDate(startDate);
	
	return nlapiAddDays(end_date,-1);
}


function scientificToDecimal(num) {
    var nsign = Math.sign(num);
    //remove the sign
    num = Math.abs(parseInt(num));
    //if the number is in scientific notation remove it
    if (/\d+\.?\d*e[\+\-]*\d+/i.test(num)) {
        var zero = '0',
                parts = String(num).toLowerCase().split('e'), //split into coeff and exponent
                e = parts.pop(), //store the exponential part
                l = Math.abs(e), //get the number of zeros
                sign = e / l,
                coeff_array = parts[0].split('.');
        if (sign === -1) {
            l = l - coeff_array[0].length;
            if (l < 0) {
              num = coeff_array[0].slice(0, l) + '.' + coeff_array[0].slice(l) + (coeff_array.length === 2 ? coeff_array[1] : '');
            }
            else {
              num = zero + '.' + new Array(l + 1).join(zero) + coeff_array.join('');
            }
        }
        else {
            var dec = coeff_array[1];
            if (dec)
                l = l - dec.length;
            if (l < 0) {
              num = coeff_array[0] + dec.slice(0, l) + '.' + dec.slice(l);
            } else {
              num = coeff_array.join('') + new Array(l + 1).join(zero);
            }
        }
    }
    return nsign < 0 ? '-'+num : num;
};

function pos(string){
	
	var s = string.split();
	if(s == "-"){
		return -1;
	}else{
		return 1;
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
		filterExpression) {

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (savedSearch) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
					searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}