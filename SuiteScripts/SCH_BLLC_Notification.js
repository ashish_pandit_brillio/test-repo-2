/**
  * Date:    24 Jun 2015 
  * Author:  nitish.mishra
  * Remarks: Send mailer to BLLC Employees for updation of timesheet.
 */
 
 
 // BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
 /*
	 Script Name: SCH Send Confirmation Mail.js
	 Script Type: Scheduled Script (1.0)
 
 
	 Script Modification Log:
	 -- Date --            -- Modified By --                --Requested By--                -- Description --
	08 sep 2021				Koushalya Kamble												Added the exception notification
 
	 Function used and their Descriptions:
 
 
	scheduled FUNCTION                   : scheduled function where the search is getting called, and the rest 3 functions are getting called.
	notificationMailTemplate FUNCTION    : Function to create the email template.
	yieldScriptFUNCTION        			 :  Fuction to handle api usage .
	Send_Exception_Mail FUNCTION  		 : Function to send the exception mail, in case of any failure.
 */
 // END SCRIPT DESCRIPTION BLOCK  ==================================
/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	try {
		var context = nlapiGetContext();
		var a_table_data = new Array();
		a_table_data.push(["Employee ID", "Employee Name", "Error Code", "Error Message"])
		var b_err_flag = false;

		// get all employees
		var employeeSearch = searchRecord('employee', null, [
			new nlobjSearchFilter('subsidiary', null, 'anyof', ['2']),
			new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F'),
			new nlobjSearchFilter('custentity_implementationteam', null, 'is', 'F')],
			[
				new nlobjSearchColumn('firstname'),
				new nlobjSearchColumn('email'),
				new nlobjSearchColumn('entityid')
			]);

		if (isArrayNotEmpty(employeeSearch)) {
			nlapiLogExecution('debug', 'total emp', employeeSearch.length);
			for (var index = 0; index < employeeSearch.length; index++) {
				try {

					yieldScript(context);
					mailData = notificationMailTemplate(employeeSearch[index].getValue('firstname'));
					nlapiSendEmail('442', employeeSearch[index].getValue('email'), mailData.MailSubject, mailData.MailBody, null, null,
						{ entity: employeeSearch[index].getId() });

				}
				catch (err) {
					nlapiLogExecution('debug', 'Main for', err);
					b_err_flag = true;
					var a_row = new Array();
					a_row.push(employeeSearch[index].getId());
					a_row.push(employeeSearch[index].getValue('entityid'));
					a_row.push(err.code);
					a_row.push(err.message);
					a_table_data.push(a_row);
				}
			}
		}
		if (b_err_flag) {
			Send_Exception_Mail(a_table_data);
		}
	}
	catch (err) {
		nlapiLogExecution('error', 'scheduled', err);
	}
}


function notificationMailTemplate(firstName) {
	var htmltext = '';

	htmltext += '<table border="0" width="100%"><tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + firstName + ',</p>';

	htmltext += "<p style='color:red;background-color:yellow;'>"
		+ "** IMPORTANT REQUEST - KINDLY UPDATE YOUR TIMESHEETS BY 6/28/2015."
		+ " DUE TO A SHORTER PAYROLL WEEK, YOUR HOURS FOR DATES BETWEEN"
		+ " JUN 15 TO JUN 28, 2015 HAVE TO BE UPDATED BY JUN 28, 2015."
		+ " ANY HOURS UPDATED AFTER 6/28/2015 WILL NOT BE CONSIDERED FOR PAYROLL"
		+ " AND WILL BE PAID IN THE FOLLOWING CYCLE**" + "</p>";

	htmltext += "<p><b>Note:</b> If you have already filled the timesheets, please ignore this email.</p>";

	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">'
		+ 'Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		MailBody: htmltext,
		MailSubject: "Timesheet Reminder"
	};
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();
		nlapiLogExecution('DEBUG', 'state', state);

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason + ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
//begin Send_Exception_Mail function
function Send_Exception_Mail(a_table_data) {
	try {
		var context = nlapiGetContext();
		var s_DeploymentID = context.getDeploymentId();
		var s_ScriptID = context.getScriptId();
		var s_Subject = 'Issue on BLLC Notification Mail';

		var s_Body = 'This is to inform that BLLC notification is having an issue for employee and System is not able to send the email notification to employees.';
		s_Body += '<br/>Script: ' + s_ScriptID;
		s_Body += '<br/>Deployment: ' + s_DeploymentID;
		s_Body += '<br/><br/><table width="100%" border="1" cellspacing="1" cellpadding="1">';
		for (var index = 0; index < a_table_data.length; index++) {
			s_Body += '<tr>';
			s_Body += '<td>' + a_table_data[index][0] + '</td>';
			s_Body += '<td>' + a_table_data[index][1] + '</td>';
			s_Body += '<td>' + a_table_data[index][2] + '</td>';
			s_Body += '<td>' + a_table_data[index][3] + '</td>';
			s_Body += '</tr>';
		}
		s_Body += '</table>';
		s_Body += '<br/>Please have a look and do the needfull.';
		s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

		s_Body += '<br/><br/>Regards,';
		s_Body += '<br/>Information Systems';
		nlapiSendEmail('442', 'timesheet@brillio.com', s_Subject, s_Body, 'netsuite.support@brillio.com', null, null, null);
	}
	catch (err) {
		nlapiLogExecution('error', ' Send_Exception_Mail', err.message);

	}
}
//end of Send_Exception_Mail function