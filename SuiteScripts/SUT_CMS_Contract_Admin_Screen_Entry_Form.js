/**
 * Screen for the contract admin for entry
 * 
 * Version Date Author Remarks 1.00 14 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			createAdminEntryForm(request);
		} else {
			submitAdminEntryDetails(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Vendor Entry - Contract Admin");
	}
}

function createAdminEntryForm(request) {
	try {
		var vendorId = request.getParameter('vendor');

		if (vendorId) {
			var vendorRec = nlapiLoadRecord('customrecord_contract_management',
			        vendorId);
			var form = nlapiCreateForm('Vendor Entry - Contract Admin : '
			        + vendorRec.getFieldValue('name'));

			// recruiter filled fields
			form.addFieldGroup('custpage_grp_basic', 'Basic Info');
			form.addFieldGroup('custpage_grp_address', 'Address');
			form.addFieldGroup('custpage_grp_other', 'Other');

			form.addField('custpage_vendor_id', 'select', 'Vendor Name',
			        'customrecord_contract_management', 'custpage_grp_basic')
			        .setDisplayType('hidden').setDefaultValue(vendorId);

			form.addField('custpage_cms_vendor_name', 'text', 'Vendor Name',
			        null, 'custpage_grp_basic').setDisplayType('inline')
			        .setDefaultValue(vendorRec.getFieldValue('name'));
			form
			        .addField('custpage_cms_signing_authority', 'text',
			                'Signing Authority', null, 'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_signing_authority'));
			form
			        .addField('custpage_cms_federal_id', 'text', 'Federal Id',
			                null, 'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_federal_id'));
			form
			        .addField('custpage_cms_contact_person', 'text',
			                'Contact Person', null, 'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_contact_person'));
			form
			        .addField('custpage_cms_admin_email', 'email',
			                'Email For Administrator', null,
			                'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_admin_email'));
			form
			        .addField('custpage_cms_requirement_email', 'email',
			                'Email For Requirement', null, 'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_requirement_email'));
			form
			        .addField('custpage_cms_phone_number', 'text',
			                'Phone Number', null, 'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_phone_number'));
			form
			        .addField('custpage_cms_fax_number', 'text', 'Fax Number',
			                null, 'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_fax_number'));
			form
			        .addField('custpage_cms_contract_type', 'select',
			                'Contract Type', 'customlist_cms_contract_type',
			                'custpage_grp_basic')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_contract_type'));
			form
			        .addField('custpage_cms_addr_line_1', 'text', 'Line 1',
			                null, 'custpage_grp_address')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_addr_line_1'));
			form
			        .addField('custpage_cms_addr_line_2', 'text', 'Line 2',
			                null, 'custpage_grp_address')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_addr_line_2'));
			form
			        .addField('custpage_cms_addr_city', 'text', 'City', null,
			                'custpage_grp_address')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec.getFieldValue('custrecord_cms_addr_city'));
			form
			        .addField('custpage_cms_addr_state', 'text', 'State', null,
			                'custpage_grp_address')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_addr_state'));
			form
			        .addField('custpage_cms_addr_zip_code', 'text', 'Zip Code',
			                null, 'custpage_grp_address')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_addr_zip_code'));
			form
			        .addField('custpage_cms_client_name', 'select',
			                'Client Name', 'customer', 'custpage_grp_other')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_client_name'));
			form
			        .addField('custpage_cms_contracts_need_follow_up',
			                'checkbox', 'Does Need Follow Up By Contracts ?',
			                null, 'custpage_grp_other')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_contracts_need_follow_up'));
			form
			        .addField('custpage_cms_can_resc_work_california',
			                'checkbox', 'Can Resource Work In California ?',
			                null, 'custpage_grp_other')
			        .setDisplayType('inline')
			        .setDefaultValue(
			                vendorRec
			                        .getFieldValue('custrecord_cms_can_resc_work_california'));

			// contract admin fields
			form.addFieldGroup('custpage_contract_admin',
			        'Contract Admin Entry');
			form.addField('custpage_cms_category', 'select', 'Category', 'vendorcategory',
			        'custpage_contract_admin');
			form.addField('custpage_cms_subsidiary', 'select', 'Subsidiary',
			        'subsidiary', 'custpage_contract_admin');
			form.addField('custpage_cms_currency', 'select', 'Currency',
			        'currency', 'custpage_contract_admin');
			form.addField('custpage_cms_payment_term', 'select',
			        'Payment Term', 'term', 'custpage_contract_admin');
			form.addField('custpage_cms_default_payable_account', 'select',
			        'Default Payable Account', 'account',
			        'custpage_contract_admin');
			form.addField('custpage_cms_is_1099_eligible', 'checkbox',
			        '1099 Eligible - Vertical Head/Finance Approval', null,
			        'custpage_contract_admin');
			form.addField('custpage_cms_w9_status', 'select', 'W-9 Status',
			        'customlist_w9statuslist', 'custpage_contract_admin');
			form
			        .addField(
			                'custpage_cms_tax_classification',
			                'select',
			                'Tax Classification (If W-9 Status is Limited Liability Corporation)',
			                'customlist_cms_tax_classification',
			                'custpage_contract_admin');
			form.addField('custpage_cms_diversified_vendor', 'checkbox',
			        'Diversified Vendor', null, 'custpage_contract_admin');
			form
			        .addField('custpage_cms_minority_classification', 'select',
			                'Minority Classification',
			                'customlist_diversity_drop_down',
			                'custpage_contract_admin');
			form.addField('custpage_cms_minority_certify_agency', 'select',
			        'Minority Certifying Agency Name', null,
			        'custpage_contract_admin');

			form.addFieldGroup('custpage_ship_address', 'Shipping Address');
			form.addField('custpage_cms_ship_addr_line_1', 'text', 'Line 1',
			        null, 'custpage_ship_address');
			form.addField('custpage_cms_ship_addr_line_2', 'text', 'Line 2',
			        null, 'custpage_ship_address');
			form.addField('custpage_cms_ship_addr_city', 'text', 'City', null,
			        'custpage_ship_address');
			form.addField('custpage_cms_ship_addr_state', 'text', 'State',
			        null, 'custpage_ship_address');
			form.addField('custpage_cms_ship_addr_zip_code', 'text',
			        'Zip Code', null, 'custpage_ship_address');

			form.addFieldGroup('custpage_bill_address', 'Billing Address');
			form.addField('custpage_cms_bill_addr_line_1', 'text', 'Line 1',
			        null, 'custpage_bill_address');
			form.addField('custpage_cms_bill_addr_line_2', 'text', 'Line 2',
			        null, 'custpage_bill_address');
			form.addField('custpage_cms_bill_addr_city', 'text', 'City', null,
			        'custpage_bill_address');
			form.addField('custpage_cms_bill_addr_state', 'text', 'State',
			        null, 'custpage_bill_address');
			form.addField('custpage_cms_bill_addr_zip_code', 'text',
			        'Zip Code', null, 'custpage_bill_address');

			form.addResetButton('Reset');
			form.addSubmitButton('Submit');

			response.writePage(form);
		} else {
			throw "Vendor ID Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createAdminEntryForm', err);
		throw err;
	}
}

function submitAdminEntryDetails(request) {
	try {
		var vendorId = request.getParameter('custpage_vendor_id');

		if (vendorId) {

			// Create a new vendor record
			var vendorRec = nlapiLoadRecord('customrecord_contract_management',
			        vendorId);

			vendorRec.setFieldValue('custrecord_cms_category', request
			        .getParameter('custpage_cms_category'));
			vendorRec.setFieldValue('custrecord_cms_subsidiary', request
			        .getParameter('custpage_cms_subsidiary'));
			vendorRec.setFieldValue('custrecord_cms_currency', request
			        .getParameter('custpage_cms_currency'));
			vendorRec.setFieldValue('custrecord_cms_payment_term', request
			        .getParameter('custpage_cms_payment_term'));
			vendorRec
			        .setFieldValue(
			                'custrecord_cms_default_payable_account',
			                request
			                        .getParameter('custpage_cms_default_payable_account'));
			vendorRec.setFieldValue('custrecord_cms_1099_eligible', request
			        .getParameter('custpage_cms_is_1099_eligible'));
			vendorRec.setFieldValue('custrecord_cms_w9_status', request
			        .getParameter('custpage_cms_w9_status'));
			vendorRec.setFieldValue('custrecord_cms_tax_classification',
			        request.getParameter('custpage_cms_tax_classification'));

			vendorRec.setFieldValue('custrecord_cms_diversified_vendor',
			        request.getParameter('custpage_cms_diversified_vendor'));
			vendorRec
			        .setFieldValue(
			                'custrecord_cms_minority_classification',
			                request
			                        .getParameter('custpage_cms_minority_classification'));
			vendorRec
			        .setFieldValue(
			                'custrecord_cms_minority_certify_agency',
			                request
			                        .getParameter('custpage_cms_minority_certify_agency'));

			vendorRec.setFieldValue('custrecord_cms_ship_addr_line_1', request
			        .getParameter('custpage_cms_ship_addr_line_1'));
			vendorRec.setFieldValue('custrecord_cms_ship_addr_line_2', request
			        .getParameter('custpage_cms_ship_addr_line_2'));
			vendorRec.setFieldValue('custrecord_cms_ship_addr_city', request
			        .getParameter('custpage_cms_ship_addr_city'));
			vendorRec.setFieldValue('custrecord_cms_ship_addr_state', request
			        .getParameter('custpage_cms_ship_addr_state'));
			vendorRec.setFieldValue('custrecord_cms_ship_addr_zip_code',
			        request.getParameter('custpage_cms_ship_addr_zip_code'));

			vendorRec.setFieldValue('custrecord_cms_bill_addr_line_1', request
			        .getParameter('custpage_cms_bill_addr_line_1'));
			vendorRec.setFieldValue('custrecord_cms_bill_addr_line_2', request
			        .getParameter('custpage_cms_bill_addr_line_2'));
			vendorRec.setFieldValue('custrecord_cms_bill_addr_city', request
			        .getParameter('custpage_cms_bill_addr_city'));
			vendorRec.setFieldValue('custrecord_cms_bill_addr_state', request
			        .getParameter('custpage_cms_bill_addr_state'));
			vendorRec.setFieldValue('custrecord_cms_bill_addr_zip_code',
			        request.getParameter('custpage_cms_bill_addr_zip_code'));

			var vendorId = nlapiSubmitRecord(vendorRec);

			// send a notification mail to the contract admin

			// create a response form
			var form = nlapiCreateForm('Vendor Entry - Contract Admin : '
			        + vendorRec.getFieldValue('name'));
			form.addField("custpage_1", "inlinehtml", "").setDefaultValue(
			        "Your request has been submitted");
			response.writePage(form);
		} else {
			throw "Vendor ID Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitAdminEntryDetails', err);
		throw err;
	}
}
