/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 May 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	try {

		var old_record = nlapiGetOldRecord();
		var old_inactive_value = old_record
				.getFieldValue('custentity_employee_inactive');

		var new_inactive_value = nlapiGetFieldValue('custentity_employee_inactive');

		if (old_inactive_value != new_inactive_value
				&& new_inactive_value == 'T') {

			// check dependends
			var currentRecord = nlapiGetRecordId();
			var htmltext = "";

			// check for expense approver
			var filter = [ [ 'approver', 'anyof', currentRecord ], 'and',
					[ 'custentity_employee_inactive', 'is', 'F' ] ];

			var emp_search = nlapiSearchRecord('employee', null, filter,
					[ new nlobjSearchColumn('entityid') ]);

			if (emp_search) {
				htmltext += "<p><b><u>Expense Approver</u></b></p>";
				htmltext += "<ol>";
				for (var i = 0; i < emp_search.length; i++) {
					htmltext += "<li>" + emp_search[i].getValue('entityid')
							+ "</li>";
				}
				htmltext += "</ol>";
			}

			// check for TA
			filter = [ [ 'timeapprover', 'anyof', currentRecord ], 'and',
					[ 'custentity_employee_inactive', 'is', 'F' ] ];

			emp_search = nlapiSearchRecord('employee', null, filter,
					[ new nlobjSearchColumn('entityid') ]);

			if (emp_search) {
				htmltext += "<p><b><u>Time Approver</u></b></p>";
				htmltext += "<ol>";
				for (var i = 0; i < emp_search.length; i++) {
					htmltext += "<li>" + emp_search[i].getValue('entityid')
							+ "</li>";
				}
				htmltext += "</ol>";
			}

			// check PM
			filter = [ [ 'custentity_projectmanager', 'anyof', currentRecord ],
					'and', [ 'status', 'anyof', [ '2', '17', '4' ] ] ];

			var job_search = nlapiSearchRecord('job', null, filter,
					[ new nlobjSearchColumn('entityid') ]);

			if (job_search) {
				htmltext += "<p><b><u>Project Manager</u></b></p>";
				htmltext += "<ol>";
				for (var i = 0; i < job_search.length; i++) {
					htmltext += "<li>" + job_search[i].getValue('entityid')
							+ "</li>";
				}
				htmltext += "</ol>";
			}

			// check DM
			filter = [
					[ 'custentity_deliverymanager', 'anyof', currentRecord ],
					'and', [ 'status', 'anyof', [ '2', '17', '4' ] ] ];

			job_search = nlapiSearchRecord('job', null, filter,
					[ new nlobjSearchColumn('entityid') ]);

			if (job_search) {
				htmltext += "<p><b><u>Delivery Manager</u></b></p>";
				htmltext += "<ol>";
				for (var i = 0; i < job_search.length; i++) {
					htmltext += "<li>" + job_search[i].getValue('entityid')
							+ "</li>";
				}
				htmltext += "</ol>";
			}

			// send emails
			if (htmltext) {
				var mailSubject = "";
				var mailBody = "<p>Hi,</p>";
				mailBody += "<p>This is to inform you that <b>"
						+ nlapiGetFieldValue('entityid')
						+ "</b> was recently terminated <b>(Last working date : "
						+ nlapiGetFieldValue('custentity_lwd')
						+ ")</b>. Please assign new time approver(s), expense approver(s),"
						+ " project manager(s) and delivery manager(s) for the following :";

				mailBody += htmltext;
				mailBody += "<p>Regards,<br/>Information Systems</p>";

				nlapiSendEmail(constant.Mail_Author.InformationSystems,
						'teamis@brillio.com', mailSubject, mailBody);
			}
		}
	} catch (err) {
		nlapiLogExecution('error', 'userEventAfterSubmit', err);
	}
}
