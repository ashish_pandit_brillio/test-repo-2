 /**
 * Module Description
 * 
 * 
 * Version                      Date                                                Author                           Remarks
 * 1.00                            12th July, 2016               Manikandan V                               Buddy Questionnaire for New Joiners 
 *
 */


function feedbackSuitelet(request, response)
{
  var method	=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(212487);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);     //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call" );
  
  var feedback = new Object();
  feedback['Name'] = request.getParameter('TextBox1');
  feedback['EmployeeDepartment'] = request.getParameter('TextBox2');
  feedback['DateOfJoin'] = request.getParameter('TextBox3');
  feedback['Location'] = request.getParameter('TextBox4');
 //ddlViewBy
  feedback['Response1'] = request.getParameter('OptionsGroup3');
  feedback['Response2'] = request.getParameter('OptionsGroup4');
  feedback['Response3'] = request.getParameter('OptionsGroup5');
  //feedback['Response4'] = request.getParameter('OptionsGroup1');
  //feedback['Response5'] = request.getParameter('OptionsGroup2');
  feedback['Response6'] = request.getParameter('scripts');
  var value_1 = request.getParameter('scripts');
  nlapiLogExecution('DEBUG','Value',value_1);
  nlapiLogExecution('DEBUG','Value',value_1.value);

  
  feedback['Comments1'] = request.getParameter('txtComments1');
  //feedback['Comments2'] = request.getParameter('txtComments2');
  //feedback['Comments3'] = request.getParameter('txtComments3');
  //feedback['Comments4'] = request.getParameter('txtComments4');
  feedback['Comments5'] = request.getParameter('txtComments5');
 
 
  var status = saveRequest(feedback);
   //response.write("Thank You!");
   //response.write("Your survey responses have been recorded!");
    var thanks_note = nlapiLoadFile(50389);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
	try
	{
               var feedback_form = nlapiCreateRecord('customrecord362');
               feedback_form.setFieldValue('custrecord_employee_name_day1', feedback.Name);
	           feedback_form.setFieldValue('custrecord_department_day1', feedback.EmployeeDepartment);
			   feedback_form.setFieldValue('custrecord_date_of_join_day1', feedback.DateOfJoin);
			   feedback_form.setFieldValue('custrecord_location_day1', feedback.Location);
               
			   
			
			   feedback_form.setFieldValue('custrecord_ques_day_1',"I liked the concept of Buddy Program");
               feedback_form.setFieldValue('custrecord_ques_day_2', "I enjoyed being the Buddy to the new Brillian");
               feedback_form.setFieldValue('custrecord_ques_day_3', "The buddy program helped me groom my inter-personal skills");
			   
			   feedback_form.setFieldValue('custrecord_ques_day_4', "What are some of the critical/difficult activities in the buddy program?");
			   ////feedback_form.setFieldValue('custrecord_ques_day_5', "What according to you are the advantages & learnings from this program?");
               ////feedback_form.setFieldValue('custrecord_ques_day_6', "Did you face any problems (professional or personal) while being a buddy to the new Brillian? Suggestions for rectification for such problems");
			   
			   ////feedback_form.setFieldValue('custrecord_ques_day_7', "Would you like to become a Buddy again?");
			   ////feedback_form.setFieldValue('custrecord_ques_day_8', "Would you recommend your colleagues to be buddies to new Brillians");
               feedback_form.setFieldValue('custrecord_ques_day_9', "Rate the Buddy Program on a scale of 1-10, why?");
			   feedback_form.setFieldValue('custrecord_ques_day_10', "Please provide your suggestion to make this program even better");
			   
			   
			   
               feedback_form.setFieldValue('custrecord_ans_day_1', feedback.Response1);
               feedback_form.setFieldValue('custrecord_ans_day_2', feedback.Response2);
               feedback_form.setFieldValue('custrecord_ans_day_3', feedback.Response3);
			   ////feedback_form.setFieldValue('custrecord_ans_day_7', feedback.Response4);
			   ////feedback_form.setFieldValue('custrecord_ans_day_8', feedback.Response5);
           
               
			   
			    
                feedback_form.setFieldValue('custrecord_comments_day_1', feedback.Comments1);
			    ////feedback_form.setFieldValue('custrecord_comments_day_2', feedback.Comments2);
			    ////feedback_form.setFieldValue('custrecord_comments_day_3', feedback.Comments3);
			    feedback_form.setFieldValue('custrecord_ans_day_9', feedback.Response6);
				
				nlapiLogExecution('debug', 'Record Saved', feedback.Comments1);
				nlapiLogExecution('debug', 'Record Saved', feedback.Response6);
				
			    feedback_form.setFieldValue('custrecord_ans_day_10', feedback.Comments5);
               
     
              
               var id = nlapiSubmitRecord(feedback_form, false,true);
               nlapiLogExecution('debug', 'Record Saved', id);
			   
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log','');
}
}
 }