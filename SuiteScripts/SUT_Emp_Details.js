/**
 * Screen to display the employee details
 * 
 * Version Date Author Remarks 1.00 06 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var employeeId = request.getParameter('emp');
		var loggedUser = nlapiGetUser();
		var role = nlapiGetRole();

		if (!employeeId) {
			employeeId = loggedUser;
		}

		if (employeeId) {
			var employeeRecord = nlapiLoadRecord('employee', employeeId);

			// check if the user is authorised to access the project information
			if (loggedUser == employeeId
			        || loggedUser == employeeRecord
			                .getFieldValue('custentity_reportingmanager')
			        || loggedUser == employeeRecord
			                .getFieldValue('timeapprover')
			        || loggedUser == employeeRecord.getFieldValue('approver')
			        || role == 3) {
				var form = nlapiCreateForm(employeeRecord
				        .getFieldValue('entityid'));

				// Primary Information
				form.addFieldGroup('custpage_primary_info',
				        'Primary Information');

				form.addField('custpage_name', 'text', 'Name', null,
				        'custpage_primary_info').setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord.getFieldValue('entityid'));
								

				form.addField('custpage_title', 'text', 'Job Title', null,
				        'custpage_primary_info').setDisplayType('inline')
				        .setDefaultValue(employeeRecord.getFieldValue('title'));

				form
				        .addField('custpage_fusion_id', 'text', 'Fusion Id',
				                null, 'custpage_primary_info')
				        .setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord
				                        .getFieldValue('custentity_fusion_empid'));

				form
				        .addField('custpage_reporting_manager', 'text',
				                'Reporting Manager', 'employee',
				                'custpage_primary_info')
				        .setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord
				                        .getFieldText('custentity_reportingmanager'));

				form.addField('custpage_emp_id', 'text', 'Employee Id', null,
				        'custpage_primary_info').setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord
				                        .getFieldValue('custentity_empid'));

				// Contact Section
				form.addFieldGroup('custpage_contact_info',
				        'Contact Information');

				form.addField('custpage_mobile_phone', 'text', 'Mobile Phone',
				        null, 'custpage_contact_info').setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord.getFieldValue('mobilephone'));

				form.addField('custpage_phone', 'text', 'Phone', null,
				        'custpage_contact_info').setDisplayType('inline')
				        .setDefaultValue(employeeRecord.getFieldValue('phone'));

				form.addField('custpage_email', 'text', 'Email', null,
				        'custpage_contact_info').setDisplayType('inline')
				        .setDefaultValue(employeeRecord.getFieldValue('email'));

				// Approver Section
				form.addFieldGroup('custpage_approver_info',
				        'Approver Information');

				form.addField('custpage_expense_approver', 'text',
				        'Expense Approver', 'employee',
				        'custpage_approver_info').setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord.getFieldText('approver'));

				form.addField('custpage_time_approver', 'text',
				        'Time Approver', 'employee', 'custpage_approver_info')
				        .setDisplayType('inline').setDefaultValue(
				                employeeRecord.getFieldText('timeapprover'));

				// classification
				form.addFieldGroup('custpage_classification_info',
				        'Classification');

				form.addField('custpage_practice', 'text', 'Practice',
				        'department', 'custpage_classification_info')
				        .setDisplayType('inline').setDefaultValue(
				                employeeRecord.getFieldText('department'));

				form.addField('custpage_subsidiary', 'text', 'Subsidiary',
				        'subsidiary', 'custpage_classification_info')
				        .setDisplayType('inline').setDefaultValue(
				                employeeRecord.getFieldText('subsidiary'));

				form.addField('custpage_location', 'text', 'Location', null,
				        'custpage_classification_info')
				        .setDisplayType('inline').setDefaultValue(
				                employeeRecord.getFieldText('location'));

				// HR details
				form.addFieldGroup('custpage_hr_info', 'HR Details');

				form.addField('custpage_hire_date', 'date', 'Hire Date', null,
				        'custpage_hr_info').setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord.getFieldValue('hiredate'));

				form.addField('custpage_emp_level', 'text', 'Employee Level',
				        null, 'custpage_hr_info').setDisplayType('inline')
				        .setDefaultValue(
				                employeeRecord.getFieldText('employeestatus'));

				response.writePage(form);
			} else {
				throw "You are not authorised to access this information. Please contact your system adminstrator.";
			}
		} else {
			throw "Something went wrong. Please contact your system adminstrator.";
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err, "Employee Details");
	}
}
