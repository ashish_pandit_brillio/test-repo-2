function setParentPracticeHead() {
	try {

		/*try {
			// set the practice
			var project = nlapiGetFieldValue('custrecord_project');
			var customer = nlapiLookupField('job', project, 'customer');
			// nlapiLogExecution('debug', 'customer value', customer);
			var territory = nlapiLookupField('customer', customer, 'territory');
			// nlapiLogExecution('debug', 'territory value', territory);
			// nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(),
			// 'custrecord_prterritory', territory);
			// nlapiLogExecution('debug', 'territory set', nlapiGetRecordId());
		} catch (x) {
			nlapiLogExecution('ERROR', 'setting territory '
			        + nlapiGetRecordId(), x);
		}*/

		// get the parent practice head and set it
		var practice = nlapiGetFieldValue('custrecord_prpractices');
		var parentPractice = nlapiLookupField('department', practice,
		        'custrecord_parent_practice');

		if (_logValidation(parentPractice)) {
			var parentPracticeHead = nlapiLookupField('department',
			        parentPractice, 'custrecord_practicehead');

			if (parentPracticeHead) {
				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(),
				        'custrecord15', parentPracticeHead);
				nlapiLogExecution('debug',
				        'parent practice head value submitted');
			}
		} else {

		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'setParentPracticeHead', err);
	}
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}