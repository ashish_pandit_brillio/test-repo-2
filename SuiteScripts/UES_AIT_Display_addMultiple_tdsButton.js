// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:UES_Ait_Display_addMultiple_tdsButton.js
	Author:		Nikhil jain
	Company:	Aashna cloudtech Pvt. Ltd.
	Date:		19 Nov 2015
	Description:Display an add multiple button to map tds to the vendor master


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_addMultipleTDS(type, form){

	/*  On before load:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	//  BEFORE LOAD CODE BODY
	
	if (type == 'create' || type == 'edit') 
	{
		var o_sublistObj = form.getSubList('recmachcustrecord_vedname')
		
		if (o_sublistObj != null && o_sublistObj != '' && o_sublistObj != undefined) 
		{
			form.setScript('customscript_cli_vendor_tds');
			
			o_sublistObj.addButton('custpage_tdsbutton', 'Add Multiple', 'addmultipletdsline()');
			
			o_sublistObj.getField('name').setDisplayType('Hidden')
			
		}
	}
	
	return true;
	
}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmitRecord(type)
{
    /*  On after submit:

          - PURPOSE
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  AFTER SUBMIT CODE BODY

	return true;

}

// END AFTER SUBMIT ===============================================




// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
