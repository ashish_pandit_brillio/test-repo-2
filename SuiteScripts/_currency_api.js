/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
 define(['N/record', 'N/search', 'N/runtime'],
 function (obj_Record, obj_Search, obj_Runtime) {
     function Send_Currency_data(obj_Request) {
         try {
            var obj_Usage_Context = obj_Runtime.getCurrentUser();
            log.debug('obj_Usage_Context', obj_Usage_Context);
             log.debug('requestBody ==', JSON.stringify(obj_Request));
             var obj_Usage_Context = obj_Runtime.getCurrentScript();
             var s_Request_Type = obj_Request.RequestType;
             log.debug('s_Request_Type ==', s_Request_Type);
             var s_Request_Data = obj_Request.RequestData;
             log.debug('s_Request_Data ==', s_Request_Data);
             var obj_Response_Return = {};
             if (s_Request_Data == 'CURRENCY') {
                 obj_Response_Return = arr_Currency_json(obj_Search);
             }
         } 
         catch (s_Exception) {
             log.debug('s_Exception==', s_Exception);
         } //// 
         log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
         log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
         return obj_Response_Return;
     } ///// Function Send_Currency_data


     return {
         post: Send_Currency_data
     };

 }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function arr_Currency_json(obj_Search) {
 try {
    var currencySearchObj = obj_Search.create({
        type: "currency",
        filters:
        [
        ],
        columns:
        [
            obj_Search.createColumn({name: "internalid", label: "Internal ID"}),
            obj_Search.createColumn({
              name: "name",
              sort: obj_Search.Sort.ASC,
              label: "Name"
           })
        ]
     });
     var searchResultCount = currencySearchObj.runPaged().count;
     log.debug("currencySearchObj result count",searchResultCount);
     var myResults = getAllResults(currencySearchObj);
     var arr_Currency_json = [];
     myResults.forEach(function (result) {
         var obj_json_Container = {}
         obj_json_Container = {
             'Internal_ID': result.getValue({ name: "internalid", label: "Internal ID" }),
             'Name': result.getValue({ name: "name", sort: obj_Search.Sort.ASC })
         };
         arr_Currency_json.push(obj_json_Container);
         return true;
     });
     return arr_Currency_json;
 } //// End of try
 catch (s_Exception) {
     log.debug('arr_Currency_json s_Exception== ', s_Exception);
 } //// End of catch 
} ///// End of function arr_Currency_json()


function getAllResults(s) {
 var result = s.run();
 var searchResults = [];
 var searchid = 0;
 do {
     var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
     resultslice.forEach(function (slice) {
         searchResults.push(slice);
         searchid++;
     }
     );
 } while (resultslice.length >= 1000);
 return searchResults;
}