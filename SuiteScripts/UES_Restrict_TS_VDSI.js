/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/error', 'N/record', 'N/search'],
/**
 * @param {error} error
 * @param {record} record
 * @param {search} search
 */
function(error, record, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	
    	try{
    		if(scriptContext.type != scriptContext.UserEventType.DELETE){
    			return true;
    		}
    		var currentRecord = scriptContext.newRecord;
    		var i_employee = currentRecord.getValue({ fieldId: 'employee'}); 
    		var i_project = currentRecord.getValue({fieldId: 'project'});
    		var weekStartDate = currentRecord.getValue({fieldId: 'startdate'});
    		
    		var weekEndDate = nlapiAddDays(nlapiStringToDate(weekStartDate),7);
    		if(parseInt(i_project) == parseInt(23694)){
    	//function to check on allocation
    		var allocationResult = searchAllocation(i_employee,weekStartDate,weekEndDate,i_project);
    		if(allocationResult.length > 0){
    			throw 'Not allowed to submit timecard!!'
    		}
    		}
    		else{
    			return true;
    		}
    		//var employee = nlapiGetFieldValue('employee');
    		//var weekStartDate = nlapiGetFieldValue('startdate');
    	}
    	catch(err){
    		 var errMessage = err;         
    		 if(err instanceof nlobjError)         
    		 {
    			 errMessage = errMessage + ' ' + err.getDetails();        
    			 }        
    		 nlapiLogExecution('ERROR', 'Error', errMessage);    
    		 return err 
    	}
    	
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }

    return {
       
        beforeSubmit: beforeSubmit
       
    };
    
});

function searchAllocation(i_emp,stdate,endDate,i_project){
	try{
		var resourceallocationSearchObj = search.create({
			   type: "resourceallocation",
			   filters:
			   [
			      ["startdate","notafter",endDate], 
			      "AND", 
			      ["enddate","notbefore",stdate], 
			      "AND", 
			      ["project","anyof",i_project], 
			      "AND", 
			      ["resource","anyof",i_emp], 
			      "AND", 
			      ["custeventrbillable","is","T"]
			   ],
			   columns:
			   [
			    "project",
			    "custeventbstartdate",
			    "custeventbenddate",
			    "resource"
			   ]
			});
			var searchResultCount = resourceallocationSearchObj.runPaged().count;
			log.debug("resourceallocationSearchObj result count",searchResultCount);
			resourceallocationSearchObj.run().each(function(result){
			   // .run().each has a limit of 4,000 results
			   return result;
			});
	}
	catch(err){
		 var errMessage = err;         
		 if(err instanceof nlobjError)         
		 {
			 errMessage = errMessage + ' ' + err.getDetails();        
			 }        
		 nlapiLogExecution('ERROR', 'Error', errMessage);    
		 return err 
	}
}
