/**
 * @author Jayesh
 */
var i_mnth_end_parent_rcrd_id="";var s_project="";
function scheduled_create_effort_plan()
{
	try
	{
		
		var i_context = nlapiGetContext();
		var i_project_id = i_context.getSetting('SCRIPT', 'custscript_proj_id_to_send_mail');
		var i_revenue_share_id = i_context.getSetting('SCRIPT', 'custscript_revenue_share_id_to_process');
		//i_revenue_share_id = 251;
		//i_project_id = 93934;
		
		nlapiLogExecution('audit','project id:- '+i_project_id,'revenue share id:- '+i_revenue_share_id);
		
		var a_revenue_cap_filter = [['custrecord_revenue_share_approval_status', 'anyof', 3], 'and',
									['custrecord_is_mnth_end_effort_created', 'is', 'F'], 'and',
									['internalid', 'anyof', parseInt(i_revenue_share_id)]];
					
		var a_column = new Array();
		a_column[0] = new nlobjSearchColumn('created').setSort(true);
		a_column[1] = new nlobjSearchColumn('custrecord_proj_strt_date_revenue_cap');
		a_column[2] = new nlobjSearchColumn('custrecord_proj_end_date_revenue_cap');
		a_column[3] = new nlobjSearchColumn('custrecord_revenue_share_project');
		
		var a_get_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
		if (a_get_revenue_cap)
		{
			for(var i_index=0; i_index<a_get_revenue_cap.length; i_index++)
			{
				s_project=a_get_revenue_cap[i_index].getText('custrecord_revenue_share_project');
				var monthBreakUp = getMonthsBreakup(
			        nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_proj_strt_date_revenue_cap')),
			        nlapiStringToDate(a_get_revenue_cap[i_index].getValue('custrecord_proj_end_date_revenue_cap')));
					
				i_revenue_share_id = a_get_revenue_cap[i_index].getId();
				var i_revenue_share_project = a_get_revenue_cap[i_index].getValue('custrecord_revenue_share_project');
				
				var a_effort_plan_filter = [['custrecord_effort_plan_revenue_cap.internalid', 'anyof', parseInt(i_revenue_share_id)]];
			
				var a_columns_existing_effort_plan_srch = new Array();
				a_columns_existing_effort_plan_srch[0] = new nlobjSearchColumn('custrecord_effort_plan_practice');
				a_columns_existing_effort_plan_srch[1] = new nlobjSearchColumn('custrecord_effort_plan_sub_practice');
				a_columns_existing_effort_plan_srch[2] = new nlobjSearchColumn('custrecord_effort_plan_role');
				a_columns_existing_effort_plan_srch[3] = new nlobjSearchColumn('custrecord_effort_plan_level');
				a_columns_existing_effort_plan_srch[4] = new nlobjSearchColumn('custrecord_efort_plan_no_of_resources');
				a_columns_existing_effort_plan_srch[5] = new nlobjSearchColumn('custrecord_effort_plan_allo_strt_date');
				a_columns_existing_effort_plan_srch[6] = new nlobjSearchColumn('custrecord_effort_plan_allo_end_date');
				a_columns_existing_effort_plan_srch[7] = new nlobjSearchColumn('custrecord_effort_plan_percent_allocated');
				a_columns_existing_effort_plan_srch[8] = new nlobjSearchColumn('custrecord_effort_plan_location');
				a_columns_existing_effort_plan_srch[9] = new nlobjSearchColumn('custrecord_cost_for_resource');
				a_columns_existing_effort_plan_srch[10] = new nlobjSearchColumn('custrecord_revenue_share_effort_plan');
				
				var a_get_exsiting_effort_plan = nlapiSearchRecord('customrecord_fp_rev_rec_effort_plan', null, a_effort_plan_filter, a_columns_existing_effort_plan_srch);
				if (a_get_exsiting_effort_plan)
				{
					create_record(i_revenue_share_id,a_get_exsiting_effort_plan,monthBreakUp,i_revenue_share_project,a_get_revenue_cap[i_index].getText('custrecord_revenue_share_project'));
				}
				
				nlapiSubmitField('customrecord_revenue_share',i_revenue_share_id,'custrecord_is_mnth_end_effort_created','T');
			}
			
			//Commented the code to optimize the script as the below template data is in script
			/*var a_projectData = nlapiLookupField('job', i_project_id, [
				        'custentity_projectmanager', 'custentity_deliverymanager',
				        'customer', 'custentity_clientpartner', 'entityid', 'altname', 'custentity_region', 'startdate', 'enddate', 'custentity_practice']);
						
			var o_proj_rcrd = nlapiLoadRecord('job',i_project_id);
			var s_project_region = o_proj_rcrd.getFieldText('custentity_region');
			var s_project_cust = o_proj_rcrd.getFieldText('parent');
			var s_project_strt = o_proj_rcrd.getFieldValue('startdate');
			var s_project_end = o_proj_rcrd.getFieldValue('enddate');
			var s_project_prac = o_proj_rcrd.getFieldText('custentity_practice');
			
			var i_region = o_proj_rcrd.getFieldValue('custentity_region');
			var i_region_head = nlapiLookupField('customrecord_region',i_region,'custrecord_region_head');
			var s_region_head_mail = nlapiLookupField('employee',i_region_head,'email');
			
			var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
			var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
			var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
			var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
			var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
			
			var a_recipient_mail = new Array();
			var s_project_name = '';
			s_project_name += a_projectData.entityid+' ';
			s_project_name += a_projectData.altname;
			
			if(s_recipient_mail_pm)
			{
				a_recipient_mail.push(s_recipient_mail_pm);
			}
			if(s_recipient_mail_dm)
			{
				a_recipient_mail.push(s_recipient_mail_dm);
			}
			if(s_recipient_mail_cp)
			{
				a_recipient_mail.push(s_recipient_mail_cp);
			}
			if(s_recipient_mail_acp)
			{
				a_recipient_mail.push(s_recipient_mail_acp);
			}
			
			var a_participating_prac_mail_id = new Array();
			var a_practice_involved = new Array();
			
			var a_revenue_cap_filter = [['custrecord_revenue_share_per_practice_ca.internalid', 'anyof', parseInt(i_revenue_share_id)]];
			
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_pr');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_su');
		
			var a_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if (a_exsiting_revenue_cap)
			{
				for(var i_revenue_index = 0; i_revenue_index < a_exsiting_revenue_cap.length; i_revenue_index++)
				{
					var i_parent_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_pr');
					var i_sub_parctice_id = a_exsiting_revenue_cap[i_revenue_index].getValue('custrecord_revenue_share_per_practice_su');
					
					if(a_practice_involved.indexOf(i_parent_parctice_id) < 0)
					{
						a_practice_involved.push(i_parent_parctice_id);
					}
					if(a_practice_involved.indexOf(i_sub_parctice_id) < 0)
					{
						a_practice_involved.push(i_sub_parctice_id);
					}
				}
			}
			
			var a_filters_search_practice = [['internalid','anyof',a_practice_involved]
									  ];
									  
			var a_columns_practice = new Array();
			a_columns_practice[0] = new nlobjSearchColumn('email', 'custrecord_practicehead');
			
			var a_parctice_srch = nlapiSearchRecord('department', null, a_filters_search_practice, a_columns_practice);
			if (a_parctice_srch)
			{
				for(var i_practice_index = 0; i_practice_index < a_parctice_srch.length; i_practice_index++)
				{
					a_participating_prac_mail_id.push(a_parctice_srch[i_practice_index].getValue('email', 'custrecord_practicehead'));
				}
			}
			
			a_participating_prac_mail_id.push(s_region_head_mail);
			a_participating_prac_mail_id.push('billing@brillio.com');
			a_participating_prac_mail_id.push('vikash.garodia@brillio.com');
			a_participating_prac_mail_id.push('bhavanishankar.t@brillio.com');
			a_participating_prac_mail_id.push('team.fpa@brillio.com');
			
			var strVar = '';
			strVar += '<html>';
			strVar += '<body>';
			
			strVar += '<p>Hello PM,</p>';
			strVar += '<p>The Fixed price project with the following details has been setup and approved in Netsuite</p>';
			strVar += '<p>Please confirm planned project effort on a monthly basis to ensure accurate revenue recognition</p>';
			
			strVar += '<p>Region:- '+s_project_region+'<br>';
			strVar += 'Customer:- '+s_project_cust+'<br>';
			strVar += 'Project:- '+s_project_name+'<br>';
			strVar += 'Project Start '+s_project_strt+' and End Date '+s_project_end+'<br>';
			strVar += 'Executing Practice:- '+s_project_prac+'</p>';
		
			strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1139&deploy=1&compid=3883006&proj_id='+i_project_id+'&mode=Update&existing_rcrd_id='+i_revenue_share_id+'&revenue_share_status=3&proj_val_chngd=F>Link to update Monthly Effort plan</a>';
			
			strVar += '<p>Regards,</p>';
			strVar += '<p>Finance Team</p>';
				
			strVar += '</body>';
			strVar += '</html>';
			
			var a_emp_attachment = new Array();
			a_emp_attachment['entity'] = '39108';
			*/
		//	nlapiSendEmail(442, a_recipient_mail, 'FP Project Budget Plan “Approved”: '+s_project_name, strVar, a_participating_prac_mail_id, null, a_emp_attachment);
			nlapiLogExecution('audit','mail sent for approval');
		}
	}
	catch(err)
	{
		
		
		_send_failure_notification(442,["billing@brillio.com","information.systems@brillio.com"],context,err,s_project,i_revenue_share_id,i_month_end_activity);
		
		nlapiLogExecution('ERROR','scheduled_create_effort_plan','ERROR MESSAGE :- '+err);
		
		
	}
}



function create_record(i_revenue_share_id,a_get_exsiting_effort_plan,monthBreakUp,i_revenue_share_project,s_project)
{
	try
	{
		var o_mnth_end_parent = nlapiCreateRecord('customrecord_fp_revrec_month_end_process');
		o_mnth_end_parent.setFieldValue('custrecord_revenue_share_parent', i_revenue_share_id);
		i_mnth_end_parent_rcrd_id = nlapiSubmitRecord(o_mnth_end_parent, true, true);
		nlapiLogExecution('audit','mnth end rcrd id:- ',i_mnth_end_parent_rcrd_id);
		
		var a_total_row_clubbed_array = [];
		
		for (var i_effort_index = 0; i_effort_index < a_get_exsiting_effort_plan.length; i_effort_index++)
		{
			var a_per_row_array = new Array();
			
			for (var j = 0; j < monthBreakUp.length; j++)
			{
				var months = monthBreakUp[j];
				var s_month_name = getMonthName(months.Start); // get month name
				var month_strt = nlapiStringToDate(months.Start);
				var month_end = nlapiStringToDate(months.End);
				var i_month = month_strt.getMonth();
				var i_year = month_strt.getFullYear();
				i_year = parseInt(i_year);
				
				var d_today_date_server = new Date();
				var i_current_month = '';
				
				if(i_month == d_today_date_server.getMonth())
				{
					i_current_month = i_month;
				}
				
				var i_total_days_in_mnth = new Date(i_year, i_month + 1, 0).getDate();
				
				var parent_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_practice');
				var s_parent_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_practice');
				var sub_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_sub_practice');
				var s_sub_practice_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_sub_practice');
				var role_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_role');
				var s_role_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_role');
				var level_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_level');
				var s_level_month_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_level');
				var location_mnth_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_location');
				var s_location_mnth_end_effrt = a_get_exsiting_effort_plan[i_effort_index].getText('custrecord_effort_plan_location');
				var i_no_of_resources = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_efort_plan_no_of_resources');
				var i_prcnt_allocated = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_percent_allocated');
				var f_cost_for_resource = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_cost_for_resource');
				var s_allocation_strt_date = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_strt_date');
				var s_allocation_end_date = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_effort_plan_allo_end_date');
				var f_revenue_share_cost = a_get_exsiting_effort_plan[i_effort_index].getValue('custrecord_revenue_share_effort_plan');
				
				i_prcnt_allocated = (parseFloat(i_prcnt_allocated) * parseFloat(i_no_of_resources)) / 100;
				
				var d_allocation_strt_date = nlapiStringToDate(s_allocation_strt_date);
				var d_allocation_end_date = nlapiStringToDate(s_allocation_end_date);
				
				var i_days_diff = 0;
				if (month_strt >= d_allocation_strt_date || month_end >= d_allocation_strt_date) {
					if (d_allocation_strt_date <= month_strt && d_allocation_end_date > month_strt) {
						d_allocation_strt_date = month_strt;
					}
					else 
						if (d_allocation_end_date < month_strt) 
							d_allocation_strt_date = '';
					
					if (d_allocation_end_date >= month_end && d_allocation_end_date >= month_strt) {
						d_allocation_end_date = month_end;
					}
					else 
						if (d_allocation_end_date < month_strt) {
							d_allocation_end_date = '';
						}
					
					if (d_allocation_strt_date) {
						if (d_allocation_end_date) {
							i_days_diff = getDatediffIndays(d_allocation_strt_date, d_allocation_end_date);
						}
					}
				}
				
				var f_prcnt_mnth_allo = parseFloat(i_days_diff) / parseFloat(i_total_days_in_mnth);
				f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo) * parseFloat(i_prcnt_allocated);
				f_prcnt_mnth_allo = parseFloat(f_prcnt_mnth_allo).toFixed(1);
				
				var o_actual_effrt_mnth_end = nlapiCreateRecord('customrecord_fp_revrec_month_end_effort');
				o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_practice', parent_practice_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_sub_practice', sub_practice_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_role', role_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_level', level_month_end_effrt);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_location_mnth_end', location_mnth_end_effrt);				
				o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_month_name', s_month_name);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_year_name', i_year);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_month_start', months.Start);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_end_date', months.End);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_percent_allocated', f_prcnt_mnth_allo);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_resource_cost', f_cost_for_resource);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_mnth_end_project_plan', i_revenue_share_project);
				o_actual_effrt_mnth_end.setFieldValue('custrecord_month_end_parent', i_mnth_end_parent_rcrd_id);
				var i_month_end_activity = nlapiSubmitRecord(o_actual_effrt_mnth_end);
				
				a_per_row_array.push({
										'prac': parent_practice_month_end_effrt,
										'prac_text': s_parent_practice_month_end_effrt,
										'subprac': sub_practice_month_end_effrt,
										'subprac_text': s_sub_practice_month_end_effrt,
										'role': role_month_end_effrt,
										'role_text': s_role_month_end_effrt,
										'level': level_month_end_effrt,
										'level_text': s_level_month_end_effrt,
										'loc': location_mnth_end_effrt,
										'loc_text': s_location_mnth_end_effrt,
										'share': f_revenue_share_cost,
										'mnth': s_month_name,
										'year': i_year,
										'allo': f_prcnt_mnth_allo,
										'cost': f_cost_for_resource,
										'project': i_revenue_share_project
										});
			}
			
			a_total_row_clubbed_array.push(a_per_row_array);
			
			var i_total_effrt_len = parseInt(a_get_exsiting_effort_plan.length) - 1;
			if(parseInt(i_effort_index) == parseInt(3))
			{
				var o_mnth_end_effrt_json = nlapiCreateRecord('customrecord_fp_rev_rec_mnth_end_json');
				o_mnth_end_effrt_json.setFieldValue('custrecord_revenue_share_parent_json',i_revenue_share_id);
				o_mnth_end_effrt_json.setFieldValue('custrecord_current_mnth_no',parseInt(i_current_month));
				o_mnth_end_effrt_json.setFieldValue('custrecord_json_mnth_end_effrt',JSON.stringify(a_total_row_clubbed_array));
				
				var a_per_row_array = new Array();
				var a_total_row_clubbed_array = [];
			}
			else if((parseInt(i_effort_index) > 3 && parseInt(i_effort_index) == 7) || (i_effort_index == i_total_effrt_len && i_effort_index > 3 && i_effort_index <= 7))
			{
				o_mnth_end_effrt_json.setFieldValue('custrecord_json_mnth_end_effrt2',JSON.stringify(a_total_row_clubbed_array));
			
				var a_per_row_array = new Array();
				var a_total_row_clubbed_array = [];
			}
			else if((parseInt(i_effort_index) > 7 && parseInt(i_effort_index) == 11) || (i_effort_index == i_total_effrt_len && i_effort_index > 7 && i_effort_index <= 11))
			{
				o_mnth_end_effrt_json.setFieldValue('custrecord_json_mnth_end_effrt3',JSON.stringify(a_total_row_clubbed_array));
			
				var a_per_row_array = new Array();
				var a_total_row_clubbed_array = [];
			}
			else if((parseInt(i_effort_index) > 11 && parseInt(i_effort_index) == 15) || (i_effort_index == i_total_effrt_len && i_effort_index > 11))
			{
				o_mnth_end_effrt_json.setFieldValue('custrecord_json_mnth_end_effrt4',JSON.stringify(a_total_row_clubbed_array));
			
				var a_per_row_array = new Array();
				var a_total_row_clubbed_array = [];
			}
		}
		
		if(i_total_effrt_len < 3)
		{
			var o_mnth_end_effrt_json = nlapiCreateRecord('customrecord_fp_rev_rec_mnth_end_json');
			o_mnth_end_effrt_json.setFieldValue('custrecord_revenue_share_parent_json',i_revenue_share_id);
			o_mnth_end_effrt_json.setFieldValue('custrecord_current_mnth_no',parseInt(i_current_month));
			o_mnth_end_effrt_json.setFieldValue('custrecord_json_mnth_end_effrt',JSON.stringify(a_total_row_clubbed_array));	
		}
		
		var i_mnth_end_effrt_json = nlapiSubmitRecord(o_mnth_end_effrt_json,true,true);
		nlapiLogExecution('audit','json created for budget plan:- '+i_mnth_end_effrt_json);
	}
	catch(err)
	{
		var context=nlapiGetContext();
		_send_failure_notification(442,["billing@brillio.com","information.systems@brillio.com"],context,err,s_project,i_revenue_share_id,i_month_end_activity);
		nlapiLogExecution('ERROR','create_record','ERROR MESSAGE :- '+err);
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var endDate = nlapiAddDays(nlapiAddMonths(new_start_date, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	//var date1 = nlapiStringToDate(fromDate);

   //var date2 = nlapiStringToDate(toDate);
	
	var date1 = fromDate;
	var date2 = toDate;
	
    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function _send_failure_notification(from,to,msg,context,e,s_project,i_revenue_share_id,monthend_activity)
{
	var mailTemplate = "";
		mailTemplate += "<p> This is to inform that script has been failed .And It is having Error - <b>["+e.code+" : "+e.message +"]</b> and therefore, system is not able to the create the Fp Rev Rec Month End Effort Plan</p>";
		mailTemplate += "<p> Please look into the issue and resolved it.</p>";
		mailTemplate += "<p> Project : "+s_project+"</p>";
		if(i_revenue_share_id){mailTemplate += "<p> Revenue Share : "+i_revenue_share_id+"</p>";}
		if(monthend_activity){mailTemplate += "<p> Month End Activity : "+monthend_activity+"</p>";}
		mailTemplate += "<p><b> Script Id:- "+context.getScriptId() +"</b></p>";
		mailTemplate += "<p><b> Script Deployment Id:- "+context.getDeploymentId() +"</b></p>";
		mailTemplate += "<br/>"
		mailTemplate += "<p>Regards, <br/> Information Systems</p>";
		nlapiSendEmail(442,["billing@brillio.com","information.systems@brillio.com"],"Failure Notification on Creating the Month end effort",mailTemplate,null,null,null);
		
}