/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Jul 2021     shravan.k
 *
 */
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Jul 2021     shravan.k
 *
 */
function Send_ADO_Details(S_type)
{
  try
  {
	  nlapiLogExecution('DEBUG', 'S_type==', S_type);
	  if((S_type == 'edit')|| (S_type == 'create'))
	  {
		  var obj_ADO_Data = {}
		  var i_Internal_id = nlapiGetRecordId();
		  var s_Record_Type = nlapiGetRecordType();
		  nlapiLogExecution('DEBUG', 's_Record_Type==', s_Record_Type);
		  nlapiLogExecution('DEBUG', 'i_Internal_id==', i_Internal_id);
		  var obj_Ado_Record = nlapiLoadRecord(s_Record_Type,i_Internal_id);
		  if(obj_Ado_Record)
			  {
			  	obj_ADO_Data = {
			  			Customer : obj_Ado_Record.getFieldText('custrecord_ado_customer'),
			  			Customer_Id : obj_Ado_Record.getFieldValue('custrecord_customerid'),
			  			ADO : obj_Ado_Record.getFieldText('custrecord_account_delivery_owner')
			  	}
			  	 nlapiLogExecution("DEBUG", "obj_ADO_Data",JSON.stringify(obj_ADO_Data)); 
			  	
			  } //// if(obj_Ado_Record)
		 
		/* Auth Steps */  
		   var commonHeader = {
           		"Content-Type": "application/json",
          		    "accept": "application/json"
           };
		   
			var tokenURL = 'https://codexbackenduat.azurewebsites.net/codex-ns-mgt/authenticate';
			var tokenMethod = 'POST';
            var tokenBody = {
            		 "username":"NETSIUTE",
            		 "password":"NETSIUTE@123" 

            };
            
            var tokenResponse = nlapiRequestURL(tokenURL, JSON.stringify(tokenBody), commonHeader, null, tokenMethod);
            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
            var tken = JSON.parse(tokenResponse.body);
				//'https://onthego-uat-v2-performance-mgt.azurewebsites.net/api/otg-fusion-and-performance-management/leaves/get-access-and-refresh-tokens?userName=NSUser&password=OTG@NSIntegration'
	           /* var tokenResponse = nlapiRequestURL(tokenURL,null, commonHeader, null,'POST');
	            nlapiLogExecution("DEBUG", "tokenResponse Body :", (tokenResponse.body)); //JSON.stringify
	            var tken = JSON.parse(tokenResponse.body);*/
	            nlapiLogExecution("DEBUG", "tokenResponse accessToken :", (tken.token));
	            /* Auth Steps */ 
	            
	            var url = 'https://codexbackenduat.azurewebsites.net/codex-ns-mgt/ado-details'
	            	//'https://onthegouatnetsuitemanagement.azurewebsites.net/api/otg-netsuite-management/expense-management/push-expense?apitype=1003'
	                var method = "POST";
	                var headers = {
	                    	"Authorization": "Bearer "+tken.token,
	                        "x-api-key": "615679DD-ABEA-48CE-B916-28974A309F77",
	                        "x-api-customer-key": "98F78573-3172-4A8D-A521-3DC619AF2BE1",
	                        "x-api-token-key": "8D660F3B-7CA9-47D0-848A-E8D7BE1B2786",
	                        "x.api.user": "113056",
	                        "content-type": "application/json",
	                        "Accept": "application/json"

	                    };
	                var dataRow = obj_ADO_Data;
	                var JSONBody = JSON.stringify(dataRow, replacer);
	                var responseObj = nlapiRequestURL(url, JSONBody, headers, null, method);
	                nlapiLogExecution("DEBUG", "Response Body :", (responseObj.body)); //JSON.stringify
	                
	                if (!(parseInt(JSON.parse(responseObj.getCode())) == 200)) {
	                	var a_emp_attachment = new Array();
	            		a_emp_attachment['entity'] = 83134;
	                    var file = nlapiCreateFile("payload.json", "PLAINTEXT", JSONBody);
	                    var emailSub = "Error in ADO Notification #" + nlapiGetRecordId(); //onthego
	                    var emailBoody = "Team," + "\r\n" + "Error while Sending Push Notification of Practice Changes" + nlapiGetRecordId() + "\r\n";
	                    nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment, file);
	                }
	  }
  } //// End of Try
  catch(S_Exception)
  {
	   	var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 83134;
		 var emailSub = "Error in Codex ADO Sync - NS Script Error #" + nlapiGetRecordId();
		 var emailBoody = "Team," + "\r\n" + "Error while Sending Push Notification of Practice Changes" + nlapiGetRecordId() + "\r\n" + "Please find below NS error." + S_Exception + "\r\n";
        nlapiSendEmail(442, "onthego@BRILLIO.COM", emailSub, emailBoody, 'information.systems@brillio.com', null, a_emp_attachment);
        nlapiLogExecution('DEBUG', 'Codex- NS Notification Error - OTG', S_Exception);
  } /// End of catch
} ///// function Send_Prac_Data(type)
/// Supporting Function 
function replacer(key, value) {
    if (typeof value == "number" && !isFinite(value)) {
        return String(value);
    }
    return value;
}
/// Supporting Function 