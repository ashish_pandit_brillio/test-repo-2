/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Aug 2016     shruthi.l
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet_timesheet_creation(request, response){
	try
	{
		if (request.getMethod() == 'GET')
		{
			var form_obj = nlapiCreateForm("Timesheet Auto Creation");
			
			var csv_file = form_obj.addField('custpage_csv', 'file', 'CSV File');
			
			form_obj.addSubmitButton('Upload CSV');
			response.writePage(form_obj);
		}
		else
			{
			var form_obj = nlapiCreateForm("Timesheet Auto Creation");
			var csv_file_obj = request.getFile('custpage_csv');
			csv_file_obj.setFolder(105121);
			
			var csv_file_id = nlapiSubmitFile(csv_file_obj);
			
			var file_data = getFileContents(csv_file_id);
			
			if(_logValidation(file_data))
				{
					create_auto_timesheet(csv_file_id);
				}
			
			}
			
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}

function _logValidation(value)
{
    if (value != null && value != '' && value != undefined) 
	{
        return true;
    }
    else 
	{
        return false;
    }
}

function create_auto_timesheet(csv_file_id)
{
	nlapiLogExecution('debug', 'csv_file_id in sut', csv_file_id);
	var params=new Array();

 	params['custscript_csv_file_id_ts'] = csv_file_id;
	
	var status = nlapiScheduleScript('customscript_sch_timesheet_auto_creation',null,params);
	
}
