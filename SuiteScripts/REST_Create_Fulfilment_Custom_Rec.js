/**
* Module Description
* 
 * Version    Date            Author           Remarks
* 1.00       26 OCT 2018     deepak.srinivas
*
*/

/**
* @param {Object} dataIn Parameter object
* @returns {Object} Output object
*/
                  
function postRESTlet(dataIn) {
      try{
            var response = new Response();
            var current_date = nlapiDateToString(new Date());
            //Log for current date
            nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
            nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
            
      //    var employeeId =  getUserUsingEmailId(dataIn.EmailId);
            var requestType = dataIn.RequestType; 
            var idExists = dataIn.Data ? dataIn.Data[0].Account.NetSuite_Customer_ID__c : '';
            nlapiLogExecution('debug', 'idExists', idExists);         
                      
            
            
            switch (requestType) {

            case M_Constants.Request.Get:

                  if (idExists) 
                  {
                        response.Data = create_planStage(dataIn);
                        response.Status = true;
                  } else {
                        response.Data = "Some error with the data sent";
                        response.Status = false;
            }
                  break;
            }
      
      }
      catch (err) {
            nlapiLogExecution('ERROR', 'postRESTlet', err);
            response.Data = err;
            response.Status = false;
      }
      nlapiLogExecution('debug', 'response', JSON.stringify(response));
      return response;
      
}
function create_planStage(dataIn){
try{
      if(_logValidation(dataIn)){
      var o_dataObj = dataIn.Data[0]; //? dataIn.Data.WeekStartDate : '';
      var account_Obj = o_dataObj.Account.Account_ID__c;
      
      var account_type =o_dataObj.Account.Type;
      
    //  var Opp_subsidiary = o_dataObj.Account_Subsidiary__c;
      var tax_code = 'GST:GST @0%';
      nlapiLogExecution('debug', 'account_type', account_type);
      
      if(_logValidation(o_dataObj.Skills__c))
    	  var skills = o_dataObj.Skills__c.replace(';',',');
      else
    	  var skills = o_dataObj.Skills__c;
      
      if(_logValidation(o_dataObj.Fulfillment_Location__c))
    	  var s_location = o_dataObj.Fulfillment_Location__c.replace(';',',');
      else
    	  var s_location = o_dataObj.Fulfillment_Location__c;
            
      //Create Custom Record
      var record = nlapiCreateRecord('customrecord_sfdc_opportunity_record', {recordmode:'dynamic'});
     
      record.setFieldValue('custrecord_opportunity_id_sfdc',o_dataObj.Opportunity_ID__c);
      record.setFieldValue('custrecord_opportunity_name_sfdc',o_dataObj.Name);
      record.setFieldValue('custrecord_project_sfdc',o_dataObj.Project_Id__c);
      record.setFieldValue('custrecord_customer_sfdc',o_dataObj.account_Obj);
      record.setFieldValue('custrecord_start_date_sfdc',dateFormat(o_dataObj.Project_Start_Date__c));
      record.setFieldValue('custrecord_end_date_sfdc',dateFormat(o_dataObj.Project_End_Date__c));
      record.setFieldValue('custrecord_practice_sfdc',o_dataObj.Organization_Practice__c);
      record.setFieldValue('custrecord_position_sfdc',o_dataObj.Positions__c);
      record.setFieldValue('custrecord_skills_sfdc',skills);
      record.setFieldValue('custrecord_location_sfdc',s_location);
      record.setFieldValue('custrecord_projection_status_sfdc',o_dataObj.Projection_Status__c);
      record.setFieldValue('custrecord_account_type_sfdc',o_dataObj.Account.Type);
      record.setFieldValue('custrecord_stage_sfdc',o_dataObj.StageName);
      record.setFieldValue('custrecord_confidence_sfdc',o_dataObj.Confidence__c);
      record.setFieldValue('custrecord_close_date_sfdc',dateFormat(o_dataObj.Opportunity_ID__c));
      record.setFieldValue('customrecord_sfdc_opportunity_record',o_dataObj.FulfillmentPlanStatus__c);
      record.setFieldValue('custrecord_opp_service_line',o_dataObj.Service_Line__c);
      
   //   }
      var Opp_ID = nlapiSubmitRecord(record,true,true);
      nlapiLogExecution('DEBUG',' creating the Opportunity_ID','OPP_ID:'+Opp_ID);
      var JSON_ = {};
      var dataRow_ = [];
      if(Opp_ID){
            var lookUp_Expense = nlapiLookupField('customrecord_sfdc_opportunity_record',Opp_ID,['custrecord_opportunity_id_sfdc','internalid']);
            JSON_ ={
                        'REC_ID':lookUp_Expense.internalid,
                        'OPP_ID':lookUp_Expense.custrecord_opportunity_id_sfdc
            };
            dataRow_.push(JSON_);
      }
      
     
      return dataRow_;
}
}
      catch(e){         
            nlapiLogExecution('DEBUG','Erron in creating Opportunity',e);
            throw e;
            
      }
}
//validate blank entries
function _logValidation(value) 
{
if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
}
else 
 { 
  return false;
}
}
function dateFormat(date){
      var date_return = '';
      if(date){
            var dateObj = date.split('-');
            var month = dateObj[1];
            var year = dateObj[0];
            var date_d = dateObj[2];
            
            date_return = month+'/'+date_d+'/'+year;
      }
      return date_return;
}

function searchCustomer(account_id){
      try{
            var customer_id = '';
            var filters = [];
            var s_account_customer = '"'+account_id+'"';
            nlapiLogExecution('DEBUG','Customer Filter ',s_account_customer);
            //filters.push(new nlobjSearchFilter('custentity22',null,'is',account_id.toString())); //custentity_sfdc_account_id
            filters.push(new nlobjSearchFilter('custentity_sfdc_account_id',null,'is',account_id.toString()));
            
            var cols = [];
            cols.push(new nlobjSearchColumn('internalid').setSort(true));
            //cols.push(new nlobjSearchColumn('name'));
            
            var searchRecord = nlapiSearchRecord('customer',null,filters,cols);
            if(searchRecord)
                  customer_id = searchRecord[0].getValue('internalid');
            nlapiLogExecution('DEBUG','Customer Search ',customer_id);
            
            return customer_id;
      }
      catch(e){
            nlapiLogExecution('DEBUG','Customer Search Error',e);
            throw e;
      }
}

function searchProject(pro_id){
      try{
            var proj_id = '';
            var filters = [];
            var s_account_customer = '"'+pro_id+'"';
            nlapiLogExecution('DEBUG','Project Filter ',pro_id);
            filters.push(new nlobjSearchFilter('entityid',null,'startswith',pro_id));
            
            var cols = [];
            cols.push(new nlobjSearchColumn('internalid').setSort(true));
            //cols.push(new nlobjSearchColumn('name'));
            
            var searchRecord = nlapiSearchRecord('job',null,filters,cols);
            if(searchRecord)
                  proj_id = searchRecord[0].getValue('internalid');
            nlapiLogExecution('DEBUG','Project Search ',proj_id);
            
            return proj_id;
      }
      catch(e){
            nlapiLogExecution('DEBUG','Customer Search Error',e);
            throw e;
      }
}

