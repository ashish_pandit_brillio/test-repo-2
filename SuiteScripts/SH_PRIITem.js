/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Oct 2017     bidhi.raj
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
try{
	var PR_unique = new Array();
	
	var strtdt = nlapiGetContext().getSetting('SCRIPT', 'custscriptstart_date');
	var enddtae = nlapiGetContext().getSetting('SCRIPT', 'custscriptend_select');
	nlapiLogExecution('DEBUG','startdate',strtdt);
	nlapiLogExecution('DEBUG','enddate',enddtae);
	var html = "";
	var context = nlapiGetContext();
	
	var columns = new Array();
	var filt = new Array();
	columns.push(new nlobjSearchColumn('custrecord_prno1'));
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('custrecord_employee'));
	columns.push(new nlobjSearchColumn('custrecord_purchaserequest'));
	columns.push(new nlobjSearchColumn('date', 'systemnotes'));
	columns.push(new nlobjSearchColumn('name', 'systemnotes'));
	columns.push(new nlobjSearchColumn('oldvalue', 'systemnotes'));
	columns.push(new nlobjSearchColumn('record', 'systemnotes'));
	columns.push(new nlobjSearchColumn('newvalue', 'systemnotes'));
	columns.push(new nlobjSearchColumn('field', 'systemnotes'));
	columns.push(new nlobjSearchColumn('custrecord_created_po_id'));
	columns.push(new nlobjSearchColumn('created'));

	html += "PR # ,";
	html += "Date,";
	html += "Employee,";
	html += "Sr #,";
	html += "Item ,";
	html += "Description,";
	html += "Order Quantity ,";
	html += "Unit ,";
	html += "Delivery Location	,";
	html += "Project	,";
	html += "PR Status ,";
	html += "Sub Practice Head,";
	html += "Subpractice Head Approval Date,";
	html += "IT Approval,";
	html += "IT Approval Date,";
	html += "Procurement,";
	html += "Procurement Approval Date,";
	html += "Quotation Analysis Status,";
	html += "Practice Head ,";
	html += "Practice Head Approval- QA,";
	html += "Finance Head,";
	html += "Finance Head Approval - QA,";
	html += "PO No.,";
	html += "PO Date,";
	html += "PO Status,";
	html += "Item Receipt,";
	html += "Item Receipt Date,";
	html += "Bill No.,";
	html += "Bill Date,";
	html += "Remaining Quantity,";
	html += "\r\n";

	
	filt[0] = new nlobjSearchFilter('created', null, 'within', [strtdt , enddtae]);
  //filt[0] = new nlobjSearchFilter('created', null, 'within', ['11/1/2018' , '11/26/2018']);
	
	// var searchresult_T = nlapiSearchRecord('customrecord_pritem','customsearch2099', null, columns);
	 
	var searchresult_T = searchRecord('customrecord_pritem','customsearch2099', filt, columns); 
	
	if(searchresult_T){
	nlapiLogExecution('DEBUG','searchresult_T',searchresult_T.length);
	var st = JSON.stringify(searchresult_T);
	searchresult_T = JSON.parse(st);
	}
	var temp_pr_num = '';
	var bNum ='';
	var rNum ='';
    var bDate ='';
    var rDate ='';
	var pr_array = new Array();
	
	
	for ( var k in searchresult_T) {
		var colm_t = searchresult_T[k].columns;
		var pr_num_t = colm_t.custrecord_prno1;
		pr_array.push(pr_num_t);
	}
	pr_array = removearrayduplicate(pr_array);
	nlapiLogExecution('Debug','After removing duplicates ->', pr_array.length);
	var index = 0;
	for ( var r in pr_array) {
		
		bNum = ''; 
		bDate ='';
	    rNum ='';
	    rDate ='';
		var receipt_num = new Array();
		var receipt_date =new Array();
		var billNum =new Array();
		var billDate =new Array();
		var sr = new Array();
		var prnum = new Array();
		var date = new Array();
		var emp = new Array();
		var item = new Array();
		var item_cat =new Array();
		var description = new Array();
		var order_quantity = new Array();
		var unit = new Array();
		var del_location = new Array();
		var proj = new Array();
		var pr_status = new Array();
		var sub_practshd = new Array();
		var sub_practshd_apprvl_date = new Array();
		var PrtsHeadapp_QA = new Array();
		var FinHead = new Array();
		var FinHeadapp_QA = new Array();
		var poNum = new Array();
		var poDate = new Array();
		var poStatus = new Array();
		var sub_practshd_apprvl_date =new Array();
		var it_apprvl = new Array();
		var it_apprvl_date = new Array();
		var procurement = new Array();
		var procu_apprvl_date = new Array();
		var quotation_analys = new Array();
		var parent_practise_hd = new Array();
		var rem_quantity = new Array();
		var practse_head = new Array();
		var tempPO ='';
		 var count =0;
		yieldScript(context); 
		for (var i = index; i < searchresult_T.length; i++) {
		
		var usageEnd = context.getRemainingUsage();
			if (usageEnd < 1000) 
			{
				yieldScript(context);
			}
			yieldScript(context); 
			var colm = searchresult_T[i].columns;
			var pr_num = colm.custrecord_prno1;
			if (pr_array[r] == pr_num) {
				if (sr != null) 
				
				var srNum = colm.custrecord_prlineitemno;
				prnum[srNum] = colm.custrecord_prno1;
				sr[srNum] = srNum;
				
				if(_logValidation(colm.created)){
					date[srNum] = colm.created.split(' ')[0];
				}
				
				if(emp){
					emp[srNum] = colm.custrecord_employee.name;
				}
				
					item[srNum] = colm.custrecord_prmatgrpcategory.name;
				
				
				if (colm.custrecord_itemcategory) { ///comment
					 item_cat[srNum] = colm.custrecord_itemcategory.name;
				}

				description[srNum] = colm.custrecord_prmatldescription;
				description[srNum]  = description[srNum] .replace(/(\r\n|\n|\r)/gm," ");
                description[srNum] = description[srNum].replace(/\,/g," ");
				order_quantity[srNum] = colm.custrecord_quantity;
				if(colm.custrecord_unit){
					unit[srNum] = colm.custrecord_unit.name;
				}
					if(colm.custrecord_deliverylocation)
					del_location[srNum] = colm.custrecord_deliverylocation.name;
					
					if( colm.custrecord_project)
					proj[srNum] = colm.custrecord_project.name;
				
				
				if(colm.custrecord_prastatus){
				pr_status[srNum] = colm.custrecord_prastatus.name;
				pr_status[srNum] = pr_status[srNum].replace(/\,/g,"");
				}
				
				if(colm.custrecord_subpracticehead){
					sub_practshd[srNum] = colm.custrecord_subpracticehead.name;
				}
				

				if (colm.oldvalue == 'Pending Delivery Manager Approval'
						|| colm.oldvalue == 'PR Pending for Sub Practice Head Approval') {
						if(_logValidation(colm.date))
						sub_practshd_apprvl_date[srNum] = colm.date.split(' ')[0];
					// PrtsHeadapp_QA = colm.custrecord_prastatus.name;
				}
				if (colm.custrecord_itforpurchaserequest) {
					it_apprvl[srNum] = colm.custrecord_itforpurchaserequest.name;
				}

				if (colm.oldvalue == 'Pending for IT validation') {
					if(_logValidation(colm.date))
					{
					it_apprvl_date[srNum] = colm.date//.split(' ')[0];
					}
					else
					{
						it_apprvl_date[srNum] = " ";
					}
				}
				if (colm.custrecord_procurementteam) {
					procurement[srNum] = colm.custrecord_procurementteam.name;
				}

				if (colm.oldvalue == 'Pending for procurement team approval') {
					if(_logValidation(colm.date))
					procu_apprvl_date[srNum] = colm.date//.split(' ')[0];
				}

				if (colm.oldvalue == 'PR approved, Pending for Quotation analysis') {
					quotation_analys[srNum] = colm.custrecord_prastatus.name;
				}
				
				try{
					if(_logValidation(colm.custrecord15)){
					parent_practise_hd [srNum]= colm.custrecord15.name;
					}
					else
					{
					parent_practise_hd [srNum]	= "";
					}
					
				}catch (e) {
					nlapiLogExecution('error', 'Main Error In Practice Catch', e);
				}
				
				

				if (colm.custrecord_pri_finance_approver) {
					FinHead[srNum] = colm.custrecord_pri_finance_approver.name;
				}
				var filt_qtn = new Array();
				var col_qtn = new Array();
				col_qtn.push(new nlobjSearchColumn('custrecord_financehead'));
				col_qtn.push(new nlobjSearchColumn(
						'custrecord_verticalheadforquotation'));
				var s1 = (colm.internalid.internalid).toString();
				filt_qtn[0] = new nlobjSearchFilter('custrecord_qa_pr_item',
						null, 'anyof', [ s1 ]);
				var searchresult_QA = nlapiSearchRecord(
						'customrecord_quotationanalysis', null, filt_qtn,
						col_qtn);
				if(searchresult_QA){
					FinHeadapp_QA[srNum] = searchresult_QA[0].getText('custrecord_financehead');
					PrtsHeadapp_QA[srNum] = searchresult_QA[0].getText('custrecord_verticalheadforquotation');
	
				}
				
				poNum[srNum] = colm.custrecord_created_po_id;
				var leng =new Array();// for loop
				leng = colm.custrecord_created_po_id;
				var cou =0;
				for(var o in leng){
					if(leng[o]== leng.internalid){
						cou = cou +1;
					}
					
				}
				if(poNum[srNum] && cou < 1){
				//if(cou <1){
					var temArr = poNum[srNum] ;
					for(var n in temArr){
						poNum[srNum] =temArr[n];
						poNum[srNum] = poNum[srNum] + poNum[srNum].name;
		                  try{
		                	  if(poNum[srNum]!='undefined')
							var num = poNum[srNum].match(/\d+/g);
							poNum[srNum] = num[0];
		                  }catch (e) {
							nlapiLogExecution('error', 'match error', e+poNum[srNum]+' '+pr_num);
						}try{
							var _colz1 = new Array();
							var _filters2 = new Array();
							_filters2[0] = new nlobjSearchFilter('tranid', null, 'is',
									[ poNum[srNum] ]);
							_colz1.push(new nlobjSearchColumn('trandate'));
							_colz1.push(new nlobjSearchColumn('approvalstatus'));

							var purchaseOrder = nlapiSearchRecord('purchaseorder',
									'customsearch_po', _filters2, _colz1);
							
							var _filters = new Array();
							var intrn = temArr[n].internalid;
							_filters[0] = new nlobjSearchFilter("createdfrom", null,
									"anyof", [ intrn.toString() ]);
							var _colz = new Array();
							_colz.push(new nlobjSearchColumn('tranid'));
							_colz.push(new nlobjSearchColumn('trandate'));//
							_colz.push(new nlobjSearchColumn('createdfrom'));
							var receipt = nlapiSearchRecord('itemreceipt',
									'customsearchitem_rceipt', _filters, _colz);// createdfrom
							 if(purchaseOrder){
		                   	  poDate[srNum] =  purchaseOrder[0].getValue('trandate');
		     					poStatus[srNum] = purchaseOrder[0].getText('approvalstatus');
		                     }
							
							 if (receipt != null && rNum == '') {
									
								 for(var j in receipt){
								  if(tem_rNum !=receipt[j].getValue('tranid')){
									  rNum = rNum +receipt[j].getValue('tranid') +"  ";
									  rDate = rDate + receipt[j].getValue('trandate')+" ";
								  }
								  var tem_rNum = receipt[j].getValue('tranid');
								  }
								
								 }
							
                           
							
						}catch (e) {
							nlapiLogExecution('error', 'PurchaseOrder', e+poNum[srNum]);
						}
						 
						 
						 
							 if(poNum[srNum]){

							var a_filters = new Array();
							a_filters[0] = new nlobjSearchFilter('custbody3', null,
									'is', [ poNum[srNum] ]);// //custbody3= for po num
							var colz = new Array();
							colz.push(new nlobjSearchColumn('transactionnumber'));
							colz.push(new nlobjSearchColumn('trandate'));
							var bill = nlapiSearchRecord('vendorbill','customsearchbill_search', a_filters, colz);
							 }
							 if (bill != null && bNum == '') {
								
							 for(var j in bill){
							  if(tem_billNum !=bill[j].getValue('transactionnumber')){
								  bNum = bNum +bill[j].getValue('transactionnumber') +"  ";
								  bDate = bDate + bill[j].getValue('trandate')+" ";
							  }
							  var tem_billNum = bill[j].getValue('transactionnumber');
							  }
							
							 }
					
					 
                          if(colm.custrecord_qtyremain){
                            rem_quantity[srNum] = colm.custrecord_qtyremain;
                             }
					 if(colm.custrecord_created_po_id){
						 var col_id_coun = colm.custrecord_created_po_id.length;
					 }
					 else{
						 var col_id_coun = 0;
					 }
					 if(count < col_id_coun){
					 if(tempPO != poNum[srNum]){
						
                         count = count+1;
                         if(!poNum[l]){
                            poNum[l] ='';
                            poDate[l] ='';
                            poStatus[l] = '';
                            receipt_num[l] ='';
                            receipt_date[l] ='';
                        }
                        if(!receipt_num[l]){
                            receipt_num[l] ='';
                            receipt_date[l] ='';
                        }
                        if(!FinHead[l]){
                            FinHead[l] ='';
                        }
                        if(!quotation_analys[l]){
                            quotation_analys[l] ='';
                        }
                         if(!it_apprvl_date[l]){
                             it_apprvl_date[l] ='';
                         }
                         if(!sub_practshd_apprvl_date[l]){
                             sub_practshd_apprvl_date[l]='';
                         }
                         if(!procu_apprvl_date[l]){
                             procu_apprvl_date[l] ='';
                         }
                         if(!it_apprvl[l]){
                             it_apprvl[l] ='';
                         }
					    html += pr_num;// e
						html += ",";
						html += date[srNum];// e
						html += ",";
						html += emp[srNum];// e
						html += ",";
						html += sr[srNum];// e
						html += ",";
						html += _logValidation(item[srNum]) == false ? '' : item[srNum].replace(/[,.\t\n\r\f\v]/g, '');
						html += ",";// e
						/*html += ",";
						html += item_cat[l];// e
			*/			html += ",";
						html += _logValidation(description[srNum])== false ? '' : description[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += order_quantity[srNum];// e
						html += ",";
						html += unit[srNum];// e
						html += ",";
						html += del_location[srNum];// e
						html += ",";
						html += proj[srNum];// e
						html += ",";
						html += pr_status[srNum];// e
						html += ",";
						html += sub_practshd[srNum];// e
						html += ",";
						html += sub_practshd_apprvl_date[srNum];// e
						html += ",";
						html += it_apprvl[srNum];// e
						html += ",";
						html += it_apprvl_date[srNum];// e
						html += ",";
						html += procurement[srNum];// e
						html += ",";
						html += procu_apprvl_date[srNum];// e
						html += ",";
						html += quotation_analys[srNum];// e
						html += ",";
						html += _logValidation(parent_practise_hd[srNum]) == false ? '' : parent_practise_hd[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += _logValidation(PrtsHeadapp_QA[srNum])== false ? '' : PrtsHeadapp_QA[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += _logValidation(FinHead[srNum]) == false ? '' : FinHead[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += _logValidation(FinHeadapp_QA[srNum]) == false ? '' : FinHeadapp_QA[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += poNum[srNum];// e
						html += ",";
						html += poDate[srNum];// e
						html += ",";
						html += poStatus[srNum];// e
						html += ",";
						html += rNum;// e
						html += ",";
						html += rDate;// e
						html += ",";
						html += bNum;// e
						html += ",";
						html += bDate;// e
						html += ",";
						html += rem_quantity[srNum];// e
						html += ",";
						html += "\r\n";
						tempPO = poNum[srNum];
						 
					 }
					}
					}
			//	}
			}
			
				else{
					temp_pr_num = pr_num;
					
					if(poNum[srNum]){
					poNum[srNum] = poNum[srNum].name;
                  try{
                	  if(poNum[srNum]!='undefined')
					var num = poNum[srNum].match(/\d+/g);
					poNum[srNum] = num[0];
                  }catch (e) {
					nlapiLogExecution('error', 'match error', e+poNum[srNum]+' '+pr_num);
				}try{
					var _colz1 = new Array();
					var _filters2 = new Array();
					_filters2[0] = new nlobjSearchFilter('tranid', null, 'is',
							[ poNum[srNum] ]);
					_colz1.push(new nlobjSearchColumn('trandate'));
					_colz1.push(new nlobjSearchColumn('approvalstatus'));

					var purchaseOrder = nlapiSearchRecord('purchaseorder',
							'customsearch_po', _filters2, _colz1);
					
					var _filters = new Array();
					var intrn = colm.custrecord_created_po_id.internalid;
					_filters[0] = new nlobjSearchFilter("createdfrom", null,
							"anyof", [ intrn.toString() ]);
					var _colz = new Array();
					_colz.push(new nlobjSearchColumn('tranid'));
					_colz.push(new nlobjSearchColumn('trandate'));//
					_colz.push(new nlobjSearchColumn('createdfrom'));
					var receipt = nlapiSearchRecord('itemreceipt',
							'customsearchitem_rceipt', _filters, _colz);// createdfrom
					 if(purchaseOrder){
                   	  poDate[srNum] = purchaseOrder[0].getValue('trandate');
     					poStatus[srNum] = purchaseOrder[0].getText('approvalstatus');
                     }
					

					 if (receipt != null && rNum == '') {
							
						 for(var j in receipt){
						  if(tem_rNum !=receipt[j].getValue('tranid')){
							  rNum = rNum +receipt[j].getValue('tranid') +"  ";
							  rDate = rDate + receipt[j].getValue('trandate')+" ";
						  }
						  var tem_rNum = receipt[j].getValue('tranid');
						  }
						
						 }
					
					
				}catch (e) {
					nlapiLogExecution('error', 'PurchaseOrder', e+poNum[srNum]);
				}
                     
					 if(poNum[srNum]){

					var a_filters = new Array();
					a_filters[0] = new nlobjSearchFilter('custbody3', null,
							'is', [ poNum[srNum] ]);// //custbody3= for po num
					var colz = new Array();
					colz.push(new nlobjSearchColumn('transactionnumber'));
					colz.push(new nlobjSearchColumn('trandate'));
					var bill = nlapiSearchRecord('vendorbill','customsearchbill_search', a_filters, colz);
					 }
					 if (bill != null && bNum == '') {
						
					 for(var j in bill){
					  if(tem_billNum !=bill[j].getValue('transactionnumber')){
						  bNum = bNum +bill[j].getValue('transactionnumber') +"  ";
						  bDate = bDate + bill[j].getValue('trandate')+" ";
					  }
					  var tem_billNum = bill[j].getValue('transactionnumber');
					  }
					
					 }
					if(colm.custrecord_qtyremain){
					rem_quantity[srNum] = colm.custrecord_qtyremain;
					}
					 
				}
				
				else
				{
					if(PR_unique.indexOf(temp_pr_num) < 0)
					{
					PR_unique.push(temp_pr_num);
					poNum[srNum] = " ";
					poDate[srNum] = " ";
					poStatus[srNum] = " ";
					
				
				
				if(colm.custrecord_qtyremain){
					rem_quantity[srNum] = colm.custrecord_qtyremain;
                 }
				
				
				
				if (colm.oldvalue == 'Pending Delivery Manager Approval'
						|| colm.oldvalue == 'PR Pending for Sub Practice Head Approval') {
						if(_logValidation(colm.date))
						sub_practshd_apprvl_date[srNum] = colm.date.split(' ')[0];
					// PrtsHeadapp_QA = colm.custrecord_prastatus.name;
				}
				else
				{
					sub_practshd_apprvl_date[srNum] = " ";
				}
				
				if (colm.oldvalue == 'Pending for IT validation') {
					if(_logValidation(colm.date))
					it_apprvl_date[srNum] = colm.date;
				}
				else
				{
					it_apprvl_date[srNum] = "";
				}
				
				if (colm.oldvalue == 'Pending for procurement team approval') {
					if(_logValidation(colm.date))
					procu_apprvl_date[srNum] = colm.date;
				}
				else
				{
					procu_apprvl_date[srNum] = "";
				}
				
				if (colm.oldvalue == 'PR approved, Pending for Quotation analysis') {
					quotation_analys[srNum] = colm.custrecord_prastatus.name;
				}
				else
				{
					quotation_analys[srNum] = "";
				}
				
				if (colm.custrecord_pri_finance_approver) {
					FinHead[srNum] = colm.custrecord_pri_finance_approver.name;
				}
				else
				{
					FinHead[srNum] = "";
				}
				if (colm.custrecord_itforpurchaserequest) {
					it_apprvl[srNum] = colm.custrecord_itforpurchaserequest.name;
				}
				else
				{
					it_apprvl[srNum] = "";
				}
				
				
				
				html += pr_num;// e
						html += ",";
						html += date[srNum];// e
						html += ",";
						html += emp[srNum];// e
						html += ",";
						html += sr[srNum];// e
						html += ",";
						html += item[srNum];// e
						/*html += ",";
						html += item_cat[l];// e*/
						html += ",";
						html += _logValidation(description[srNum])== false ? '' : description[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += order_quantity[srNum];// e
						html += ",";
						html += unit[srNum];// e
						html += ",";
						html += del_location[srNum];// e
						html += ",";
						html += proj[srNum];// e
						html += ",";
						html += pr_status[srNum];// e
						html += ",";
						html += sub_practshd[srNum];// e
						html += ",";
						html += sub_practshd_apprvl_date[srNum];// e
						html += ",";
						html += it_apprvl[srNum];// e
						html += ",";
						html += it_apprvl_date[srNum];// e
						html += ",";
						html += procurement[srNum];// e
						html += ",";
						html += procu_apprvl_date[srNum];// e
						html += ",";
						html += quotation_analys[srNum];// e
						html += ",";
						html += _logValidation(parent_practise_hd[srNum]) == false ? '' : parent_practise_hd[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += _logValidation(PrtsHeadapp_QA[srNum])== false ? '' : PrtsHeadapp_QA[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += _logValidation(FinHead[srNum]) == false ? '' : FinHead[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += _logValidation(FinHeadapp_QA[srNum]) == false ? '' : FinHeadapp_QA[srNum].replace(/[,.\t\n\r\f\v]/g, '');// e
						html += ",";
						html += poNum[srNum];// e
						html += ",";
						html += poDate[srNum];// e
						html += ",";
						html += poStatus[srNum];// e
						html += ",";
						html += rNum;// e
						html += ",";
						html += rDate;// e
						html += ",";
						html += bNum;// e
						html += ",";
						html += bDate;// e
						html += ",";
						html += rem_quantity[srNum];// e
						html += ",";
						html += "\r\n";
						tempPO = poNum[srNum];
				}
				}
				}//end of if
				//end of else

				
			}//if loop
			//yieldScript(context);
			else
			{
				index = i;
				break;
			}
			
		}
	
		yieldScript(context); 
		
		for(var l=1;l<emp.length;l++){
		yieldScript(context); 
			if(!poNum[l]){
				poNum[l] ='';
				poDate[l] ='';
				poStatus[l] = '';
				receipt_num[l] ='';
				receipt_date[l] ='';
			}
			if(!receipt_num[l]){
				receipt_num[l] ='';
				receipt_date[l] ='';
			}
			if(!FinHead[l]){
				FinHead[l] ='';
			}
			if(!quotation_analys[l]){
				quotation_analys[l] ='';
			}
             if(!it_apprvl_date[l]){
            	 it_apprvl_date[l] ='';
             }
             if(!sub_practshd_apprvl_date[l]){
            	 sub_practshd_apprvl_date[l]='';
             }
             if(!procu_apprvl_date[l]){
            	 procu_apprvl_date[l] ='';
             }
             if(!it_apprvl[l]){
            	 it_apprvl[l] ='';
             }
			if( sr[l] && cou == 1){
			html += temp_pr_num;// e
			html += ",";
			html += date[l];// e
			html += ",";
			html += emp[l];// e
			html += ",";
			html += sr[l];// e
			html += ",";
			html += item[l];// e
			/*html += ",";
			html += item_cat[l];// e
*/			html += ",";
			
			html += _logValidation(description[l])== false ? '' : description[l].replace(/[,.\t\n\r\f\v]/g, '');// e
			html += ",";
			html += order_quantity[l];// e
			html += ",";
			html += unit[l];// e
			html += ",";
			html += del_location[l];// e
			html += ",";
			html += proj[l];// e
			html += ",";
			html += pr_status[l];// e
			html += ",";
			html += sub_practshd[l];// e
			html += ",";
			html += sub_practshd_apprvl_date[l];// e
			html += ",";
			html += it_apprvl[l];// e
			html += ",";
			html += it_apprvl_date[l];// e
			html += ",";
			html += procurement[l];// e
			html += ",";
			html += procu_apprvl_date[l];// e
			html += ",";
			html += quotation_analys[l];// e
			html += ",";
			html += _logValidation(parent_practise_hd[l]) == false ? '' : parent_practise_hd[l].replace(/[,.\t\n\r\f\v]/g, '');// e
			html += ",";
			html += _logValidation(PrtsHeadapp_QA[l])== false ? '' : PrtsHeadapp_QA[l].replace(/[,.\t\n\r\f\v]/g, '');// e
			html += ",";
			html += _logValidation(FinHead[l]) == false ? '' : FinHead[l].replace(/[,.\t\n\r\f\v]/g, '');// e
			html += ",";
			html += _logValidation(FinHeadapp_QA[l]) == false ? '' : FinHeadapp_QA[l].replace(/[,.\t\n\r\f\v]/g, '');// e
			html += ",";
			html += poNum[l];// e
			html += ",";
			html += poDate[l];// e
			html += ",";
			html += poStatus[l];// e
			html += ",";
			html += rNum;// e
			html += ",";
			html += rDate;// e
			html += ",";
			html += bNum;// e
			html += ",";
			html += bDate;// e
			html += ",";
			html += rem_quantity[l];// e
			html += ",";
			html += "\r\n";
          nlapiLogExecution('DEBUG','temp_pr_num',temp_pr_num);
			}
				}
				//yieldScript(context);
	}

  var fileName = 'PO History .csv';
//var fileName = 'PO History '+strtdt+'-'+enddtae+'.csv';
	var file = nlapiCreateFile(fileName, 'CSV', html);
	   var objUser = nlapiGetContext();
	    var i_employee_id = objUser.getUser();
	    var email = nlapiLookupField('employee',i_employee_id, [ 'email' ]);
	nlapiSendEmail('442', email.email, 'PR_Report', 'PR - Item Approval History Report', null, null, null, file, true, null, null);
  nlapiLogExecution('DEBUG','mail sent',email.email);
  
}catch (e) {
	nlapiLogExecution('error', 'Main', e);
}
}
function removearrayduplicate(cp) {
	var newArray = new Array();
	label: for (var i = 0; i < cp.length; i++) {
		for (var j = 0; j < cp.length; j++) {
			if (newArray[j] == cp[i])
				continue label;
		}
		newArray[newArray.length] = cp[i];
	}
	return newArray;
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 1000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}