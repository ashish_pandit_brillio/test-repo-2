/**
 * Module Description User Event script to handle employee record creation and
 * updation from the XML file generated from Fusion
 * 
 * Version Date Author Remarks
 * 
 * 1.00 11th June 2015 Anuradha Sinha This script runs when the New hire and
 * update XML file gets saved in custom records
 * 
 * 2.0 7/15/2016 Nitish Mishra Corrected the probation end date field part -
 * format issue, refactored for all date fields
 * 
 * 3.0 8/4/2016 Deepak MS Added new functionality of creating new subtier record
 */
// 4/24/2017 Sai Saranya Added _PM DM Inactivation trigger Script---60
// script to read XML from custom field
function beforeSubmit() {
    nlapiLogExecution('debug', 'Running');


    // Update the Termination File if the type of file is termination type
    var fu_attendance_Tracking = 'NO';
    var record_type = nlapiGetFieldValue('custrecord_fi_type');
    if (record_type == 3) {
        try {
            var xmlData = nlapiStringToXML(nlapiGetFieldValue('custrecord_fi_fusion_xml_data'));

            var fusionNumber = nlapiSelectValue(xmlData,
                '//Assignment/FusionID');


            if (fusionNumber != null) {
                var emp_search = nlapiSearchRecord('employee', null, [
                    new nlobjSearchFilter('custentity_fusion_empid', null,
                        'is', fusionNumber),
                    new nlobjSearchFilter('custentity_employee_inactive',
                        null, 'is', 'F')
                ], [new nlobjSearchColumn(
                    'internalid')]);

                // Load the employee record with the specified internal ID
                if (emp_search && emp_search.length == 1) {
                    var id = emp_search[0].getValue('internalid');
                    var employeeRecord = nlapiLoadRecord('employee', id);
                }

                // Uncheck project resource
                // employeeRecord.setFieldValue('isjobresource','F');

                // Make the employee custom inactive,remove access and role
                employeeRecord.setFieldValue('giveaccess', 'F');
                employeeRecord.setFieldValue('custentity_employee_inactive',
                    'T');

                // Update the last working date
                var last_working_date = nlapiSelectValue(xmlData,
                    '//Assignment/Termination_Date');
                var w = last_working_date.slice(0, 10).split('-');
                last_day = w[1] + '/' + w[2] + '/' + w[0];
                employeeRecord.setFieldValue('custentity_lwd', last_day);

                var ids = nlapiSubmitRecord(employeeRecord, false, true);
                if (ids) {
                    var param = new Array();
                    param['custscript2'] = ids;
                    nlapiScheduleScript('customscript1175', 'customdeploy1', param);
                    nlapiLogExecution('debug', 'TA EA Triggered', ids);
                }
                nlapiLogExecution('debug', 'Employee Record Terminated', ids);
                nlapiSetFieldValue('custrecord_fi_error_log', '');
                nlapiSetFieldValue('custrecord_fi_employee', ids);

                // // -- 6/2/2016 Nitish Start
                // update level change history
                updateEmployeeLevelHistory(ids, null, 'terminate');
                // -- 6/2/2016 END

                // Update Last working date in Resource Allocation
                // Get resource allocations associated to this employee,
                // having allocation end date after the termination date
                var resourceAllocationSearch = nlapiSearchRecord(
                    'resourceallocation', null, [
                        new nlobjSearchFilter('internalid', 'employee',
                            'anyof', ids),
                        new nlobjSearchFilter('enddate', null,
                            'onorafter', last_day)
                    ]);

                if (isArrayNotEmpty(resourceAllocationSearch)) {

                    for (var index = 0; index < resourceAllocationSearch.length; index++) {
                        var rescAllocRec = nlapiLoadRecord(
                            'resourceallocation',
                            resourceAllocationSearch[index].getId(), {
                                recordmode: 'dynamic'
                            });
                        rescAllocRec.setFieldValue('enddate', last_day);
                        rescAllocRec.setFieldValue('custeventbenddate',
                            last_day);
                        rescAllocRec.setFieldValue(
                            'custevent_allocation_status', '22');
                        rescAllocRec
                            .setFieldValue('notes',
                                'Employee terminated, hence allocation end date changed');
                        rescAllocRec.setFieldValue(
                            'custevent_ra_last_modified_by', 442);
                        rescAllocRec.setFieldValue(
                            'custevent_ra_last_modified_date',
                            nlapiDateToString(new Date(), 'datetimetz'));
                        nlapiSubmitRecord(rescAllocRec, false, true);
                        nlapiLogExecution('DEBUG', 'Allocation ended');
                    }
                }

                try {
                    // Update the subtier records
                    // Get all subtier records that end after the LWD
                    var subtierSearch = nlapiSearchRecord(
                        'customrecord_subtier_vendor_data', null, [
                            new nlobjSearchFilter(
                                'custrecord_stvd_contractor', null,
                                'anyof', ids),
                            new nlobjSearchFilter(
                                'custrecord_stvd_end_date', null,
                                'onorafter', last_day)
                        ]);

                    // change the end date of all the found subtier record
                    if (subtierSearch) {

                        for (var index = 0; index < subtierSearch.length; index++) {
                            var subTierRec = nlapiLoadRecord(
                                'customrecord_subtier_vendor_data',
                                subtierSearch[index].getId(), {
                                    recordmode: 'dynamic'
                                });

                            // if start date is after the termination date,
                            // change
                            // the start date
                            if (nlapiStringToDate(subTierRec
                                    .getFieldValue('custrecord_stvd_start_date')) > nlapiStringToDate(last_day)) {
                                subTierRec.setFieldValue('isinactive', 'T');
                                subTierRec.setFieldValue(
                                    'custrecord_stvd_start_date', last_day);
                            }

                            subTierRec.setFieldValue(
                                'custrecord_stvd_end_date', last_day);

                            nlapiSubmitRecord(subTierRec, false, true);
                        }
                    }
                } catch (erro) {
                    nlapiLogExecution('ERROR', 'While Updating subtier record',
                        erro);
                }

                nlapiSubmitField('employee', ids, 'isjobresource', 'F');
                nlapiLogExecution('DEBUG', 'Employee record updated',
                    'Project Resource : F');
            }
        } catch (err) {
            nlapiLogExecution('error', 'Record not terminated', err);
        }
    } else {

        // get all the fields in the custom record, so that we have a list of
        // fields to be read from the XML
        try {
            var field_search_hire = getAllFusionFieldMapping('New Hire');
            var field_search_update = getAllFusionFieldMapping('Update');

            // read the XML content from the record
            var xmlData = nlapiStringToXML(nlapiGetFieldValue('custrecord_fi_fusion_xml_data'));
            var vendor_Name = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName');
            var subsidiary_1 = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/LegalEntity');
            var personType = nlapiSelectValue(xmlData, '//Employee/PersonType');
            var assignemntCategory_new = nlapiSelectValue(xmlData, '//Assignment/AssignmentCategory');
            var assignemntCategory_old = nlapiSelectValue(xmlData, '//Assignment/AssignmentCategory_OLD');
            var ReferralVendor = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/ReferralVendor');
            // search and create a list of dropdown values
            var dropdown_search = getDropDownData();

            if (dropdown_search) {

                try {
                    var oldLevel = "";

                    // create a JSON array that can be used to map dropdown
                    // values
                    var dropdown = [];
                    for (var i = 0; i < dropdown_search.length; i++) {
                        var list = dropdown_search[i]
                            .getValue('custrecord_fi_dd_list_type');
                        var fusionId = dropdown_search[i]
                            .getValue('custrecord_fi_fusion_value');
                        var netsuiteId = dropdown_search[i]
                            .getValue('custrecord_fi_netsuite_id');

                        if (list == 'subsidiary' && subsidiary_1 == fusionId) {
                            var sub_ID = netsuiteId;
                            nlapiLogExecution('DEBUG', 'fusionId_sub_ID', fusionId + ' ' + sub_ID);
                        }
                        //Get vendor ID by comparing against dropdown mapping custom record
                        if (list == 'Assignment Category' && assignemntCategory_new == fusionId) {
                            var assigmentCategory_NEW_U = netsuiteId;
                            nlapiLogExecution('DEBUG', 'fusionId_Vendor_ID', fusionId + ' ' + assigmentCategory_NEW_U);
                        }
                        if (list == 'Assignment Category' && assignemntCategory_old == fusionId) {
                            var assigmentCategory_OLD_U = netsuiteId;
                            nlapiLogExecution('DEBUG', 'fusionId_Vendor_ID', fusionId + ' ' + assigmentCategory_OLD_U);
                        }

                        //Assignment Category
                        if (list == 'Vendors' && parseInt(vendor_Name) == parseInt(fusionId)) {
                            var Vendor_ID = netsuiteId;
                            nlapiLogExecution('DEBUG', 'fusionId_Vendor_ID', fusionId + ' ' + Vendor_ID);
                        }
                        if (list == 'Vendors' && parseInt(ReferralVendor) == parseInt(fusionId)) {
                            var referral_Vendor_ID = netsuiteId;
                            nlapiLogExecution('DEBUG', 'fusionId_referral_Vendor_ID', fusionId + ' ' + referral_Vendor_ID);
                        }

                        // check if the list is already added to the array
                        if (!dropdown[list]) {
                            dropdown[list] = [];
                        }

                        dropdown[list][fusionId] = netsuiteId;
                    }

                    // read the XML content from the record
                    //var xmlData = nlapiStringToXML(nlapiGetFieldValue('custrecord_fi_fusion_xml_data'));

                    // Create record only its a new record
                    var fieldData = nlapiSelectValue(xmlData,
                        '//AssignmentData/Assignment/Assignment_Action_Code');
                    var fusion_Number = nlapiSelectValue(xmlData, '//Assignment/PersonNumber');
                    var employeeRecord = null;
                    var empSearch = null;
                    //IF new hire and update happens on the same day
                    if (fusion_Number) {
                        empSearch = nlapiSearchRecord('employee', null, [
                            new nlobjSearchFilter('custentity_fusion_empid', null,
                                'is', fusion_Number),
                            new nlobjSearchFilter('custentity_employee_inactive',
                                null, 'is', 'F')
                        ], [new nlobjSearchColumn(
                            'internalid')]);
                    }

                    var field_search = null;
                    if (fieldData == "HIRE" || fieldData == "ADD_CWK" || empSearch == null) {
                        field_search = field_search_hire;
                        employeeRecord = nlapiCreateRecord('employee');

                        var personNumber = nlapiSelectValue(xmlData,
                            '//Employee/PersonNumber');
                        var Reporting_Mgr_fusion_id = nlapiSelectValue(xmlData,
                            '//Assignment/ManagerPersonNumber');
                        nlapiSetFieldValue('custrecord_fi_type', 1);

                        var Firstname = nlapiSelectValue(xmlData,
                            '//Employee/FirstName');
                        var Middlename = nlapiSelectValue(xmlData,
                            '//Employee/MiddleName');
                        var Lastname = nlapiSelectValue(xmlData,
                            '//Employee/LastName');
                        //Updated Code to capture address info, Date: 30-9-2016
                        var brillio_Location = nlapiSelectValue(xmlData,
                            '//Assignment/BaseLocation');
                        var brillio_address1 = nlapiSelectValue(xmlData,
                            '//Assignment/AsgAddressLine1');
                        var brillio_address2 = nlapiSelectValue(xmlData,
                            '//Assignment/AsgAddressLine2');
                        var brillio_city = nlapiSelectValue(xmlData,
                            '//Assignment/AsgCity');
                        var brillio_country = nlapiSelectValue(xmlData,
                            '//Assignment/AsgCountry');
                        var brillio_zipCode = nlapiSelectValue(xmlData,
                            '//Assignment/AsgZipCode');

                        var physicalWorkAddress = nlapiSelectValue(xmlData,
                            '//Assignment/PhysicalWorkAddress');
							//
							
							
			    var f_person_id = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/PersonID');
				var f_employer_id = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/EmployerId');
				var f_assignment_numb = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/AssignmentNumber');

                        employeeRecord.setFieldValue('autoname', 'F');

                        if (Firstname != null && Lastname != null) {
                            if (Middlename == null) {
                                employeeRecord.setFieldValue('entityid',
                                    personNumber + "-" + Firstname + " " +
                                    Lastname);
                            } else {
                                employeeRecord.setFieldValue('entityid',
                                    personNumber + "-" + Firstname + " " +
                                    Middlename + " " + Lastname);
                            }
                        }
                        Reporting_mgr_search = nlapiSearchRecord('employee',
                            null, [
                                new nlobjSearchFilter(
                                    'custentity_fusion_empid',
                                    null, 'is',
                                    Reporting_Mgr_fusion_id),
                                new nlobjSearchFilter(
                                    'custentity_employee_inactive',
                                    null, 'is', 'F')
                            ], [new nlobjSearchColumn('internalid')]);

                        // Add state and city
                        // var state=nlapiSelectValue(xmlData,
                        // '//Assignment/AsgState');
                        // if(state!=null)
                        // {
                        // employeeRecord.setFieldText('custentity_workstate',state);
                        // }

                        var city = nlapiSelectValue(xmlData, '//AsgCity');
                        if (city != null) {
                            employeeRecord.setFieldValue('custentity_workcity',
                                city);
                        }

                        // Add phone number
                        var phone = nlapiSelectValue(xmlData, '//PhoneNumber');
                        if (phone != null) {
                            employeeRecord.setFieldValue('phone', phone);
                        }

                        // Add employee gender
                        // var gender=nlapiSelectValue(xmlData,
                        // '//AssignmentHT/AssignmentData/Assignment/Gender');
                        // if(gender!=null)
                        // {
                        // employeeRecord.setFieldValue('gender',gender);
                        // }

                        // Add ST and OT rates
                        // var St_rate_type=nlapiSelectValue(xmlData,
                        // '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementName');
                        // if(St_rate_type!=0 && St_rate_type.length>0)
                        // {
                        // if(St_rate_type=="Hourly Salary")
                        // {
                        // employeeRecord.setFieldValue('custentity_stpayrate',St_rate);
                        // }
                        // }

                        // var ot_rate_type=nlapiSelectValue(xmlData,
                        // '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementName');
                        // if(ot_rate_type!=null && ot_rate_type=="OT Rate")
                        // {
                        // var Ot_rate=nlapiSelectValue(xmlData,
                        // '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementValue);
                        // employeeRecord.setFieldValue('custentity_otpayrate',Ot_rate);
                        // }
                        employeeRecord.setFieldValue('custentity_list_brillio_location_e',
                            brillio_Location);
                        employeeRecord.setFieldValue('custentity_brillio_address_1',
                            brillio_address1);
                        employeeRecord.setFieldValue('custentity_brillio_address_2',
                            brillio_address2);
                        employeeRecord.setFieldValue('custentity_brillio_country',
                            brillio_country);
                        employeeRecord.setFieldValue('custentity_brillio_zip_code',
                            brillio_zipCode);
                         //Fusion Assignment,Emp, PersonID
						   employeeRecord.setFieldValue('custentity_personid',
                            f_person_id);
							
							employeeRecord.setFieldValue('custentity_employer_id',
                            f_employer_id);
							employeeRecord.setFieldValue('custentity_assignment_num',
                            f_assignment_numb);


                        //employeeRecord.setFieldValue('custentity_brillio_address_1',
                        //	brillio_address1);

                        // Add Reporting manager

                        var mgr_id = Reporting_mgr_search[0]
                            .getValue('internalid');
                        employeeRecord.setFieldValue(
                            'custentity_reportingmanager', mgr_id);

                        var hire_date = nlapiSelectValue(xmlData,
                            '//Employee/HireDate');
                        // var d = hire_date.slice(0, 10).split('-');
                        // hire_day = d[1] + '/' + d[2] + '/' + d[0];
                        employeeRecord.setFieldValue('hiredate',
                            formatDate(hire_date));
                        employeeRecord.setFieldValue(
                            'custentity_actual_hire_date',
                            formatDate(hire_date));

                        var birth_date = nlapiSelectValue(xmlData,
                            '//Employee/DateofBirth');
                        // var b = birth_date.slice(0, 10).split('-');
                        // birth_day = b[1] + '/' + b[2] + '/' + b[0];
                        employeeRecord.setFieldValue('birthdate',
                            formatDate(birth_date));

                        var Identifier_type = nlapiSelectValue(xmlData,
                            '//NationalIDHT/NtionalIDData/NationalIDValues/NationalType');
                        if (Identifier_type == "SSN") {
                            var ssn = nlapiSelectValue(xmlData,
                                '//NationalIDValues/NationalIDNumber');
                            employeeRecord.setFieldValue(
                                'socialsecuritynumber', ssn);
                        } else {
                            var pan = nlapiSelectValue(xmlData,
                                '//NationalIDValues/NationalIDNumber');
                            employeeRecord.setFieldValue(
                                'custentity_permanent_account', pan);
                        }
                        employeeRecord.setFieldValue('isjobresource', 'T');

                        //Attendance Tracking Field 
                        var fu_subsidiary = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/LegalEntity');
                            fu_attendance_Tracking = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/AttendanceTracking');
                                if (fu_attendance_Tracking == 'YES' || fu_attendance_Tracking == 'Yes'){
                                    fu_attendance_Tracking = 1;
									}
									else{
									fu_attendance_Tracking = 2;
									}

                                if (_logValidation(fu_attendance_Tracking)) {
                                    if (fu_subsidiary != 'Brillio Technologies Private Limited') {
                                        employeeRecord.setFieldText('custentity_emp_attendance_tracking', 2); //NO
                                    } else {
                                        employeeRecord.setFieldValue('custentity_emp_attendance_tracking', fu_attendance_Tracking); //YES
                                    }
                                }

                                // nlapiSetFieldValue('entity', '294');

                                // var set_role=employeeRecord.SelectLineItem('role',1);
                                // var set_role=employeeRecord.getLineItemValue('role',
                                // 'select',1);
                                // set_role.insertLineItem('role', 1006);

                                // set_role.SetCurrentLineItemValue('role', 'select',
                                // 1006);

                                // set the role on the currently selected line

                                // employeeRecord.commitLineItem(role);

                                // employeeRecord.setFieldValue('giveaccess','T');
                                // employeeRecord.setFieldValue('custentity_emp_send_joining_email','T');
                            }
                            else {
                                // Update the change only extract
                                //	var current_date = nlapiDateToString(new Date());
                                //	nlapiLogExecution('DEBUG','Cureent Date','current_date:'+current_date);
                                //current_date = nlapiDateToString(current_date);
                                //	nlapiLogExecution('DEBUG','Cureent Date','String current_date:'+current_date);
                                field_search = field_search_update;
                                nlapiSetFieldValue('custrecord_fi_type', 2);
                                var fusionNumber = nlapiSelectValue(xmlData,
                                    '//Assignment/PersonNumber');
                                emp_search = nlapiSearchRecord('employee', null, [
                                    new nlobjSearchFilter(
                                        'custentity_fusion_empid', null, 'is',
                                        fusionNumber),
                                    new nlobjSearchFilter(
                                        'custentity_employee_inactive', null,
                                        'is', 'F')
                                ], [new nlobjSearchColumn(
                                    'internalid')]);

                                if (emp_search && emp_search.length == 1) {
                                    var id = emp_search[0].getValue('internalid');
                                    employeeRecord = nlapiLoadRecord('employee', id);
                                    oldLevel = employeeRecord
                                        .getFieldValue('employeestatus');

                                    var Reporting_Mgr_fusion_id = nlapiSelectValue(
                                        xmlData, '//Assignment/ManagerPersonNumber');

                                    //Attendance Tracking 
                                    fu_attendance_Tracking = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/AttendanceTracking');
                                        if (fu_attendance_Tracking == 'YES')
										{
                                            fu_attendance_Tracking = 1;
										}	
										else
										{	
											fu_attendance_Tracking = 2;
										}	

                                        if (_logValidation(fu_attendance_Tracking)) {
                                            if (parseInt(employeeRecord.getFieldValue('subsidiary')) != parseInt(3)) {
                                                employeeRecord.setFieldText('custentity_emp_attendance_tracking', 2); //NO
                                            } else {
                                                employeeRecord.setFieldValue('custentity_emp_attendance_tracking', fu_attendance_Tracking); //YES
                                            }
                                        }

                                        if (Reporting_Mgr_fusion_id != null) {
                                            var Reporting_mgr_search = nlapiSearchRecord(
                                                'employee',
                                                null, [
                                                    new nlobjSearchFilter(
                                                        'custentity_fusion_empid',
                                                        null, 'is',
                                                        Reporting_Mgr_fusion_id),
                                                    new nlobjSearchFilter(
                                                        'custentity_employee_inactive',
                                                        null, 'is', 'F')
                                                ], [new nlobjSearchColumn('internalid')]);
                                            var mgr_id = Reporting_mgr_search[0]
                                                .getValue('internalid');
                                            employeeRecord.setFieldValue(
                                                'custentity_reportingmanager', mgr_id);
                                        }

                                        var U_Firstname = nlapiSelectValue(xmlData,
                                            '//Employee/FirstName');
                                        var U_Middlename = nlapiSelectValue(xmlData,
                                            '//Employee/MiddleName');
                                        var U_Lastname = nlapiSelectValue(xmlData,
                                            '//Employee/LastName');
                                        var U_personNumber = nlapiSelectValue(xmlData,
                                            '//Employee/PersonNumber');

                                        //Updated for Brillio Location & Address
                                        var assignmentEffStartDate = nlapiSelectValue(xmlData,
                                            '//Assignment/AssignmentEffStartDate'); assignmentEffStartDate = formatDate(assignmentEffStartDate);

                                        var brillio_BaseLocation_OLD = nlapiSelectValue(xmlData,
                                            '//Assignment/BaseLocation_OLD');

                                        var brillio_Location_U = nlapiSelectValue(xmlData,
                                            '//Assignment/BaseLocation');

                                        var brillio_address1_U = nlapiSelectValue(xmlData,
                                            '//Assignment/AsgAddressLine1');

                                        var brillio_address2_U = nlapiSelectValue(xmlData,
                                            '//Assignment/AsgAddressLine2');

                                        var brillio_city_U = nlapiSelectValue(xmlData,
                                            '//Assignment/AsgCity');

                                        var brillio_country_U = nlapiSelectValue(xmlData,
                                            '//Assignment/AsgCountry');

                                        var brillio_zipCode_U = nlapiSelectValue(xmlData,
                                            '//Assignment/AsgZipCode');

                                        var brillio_city_U = nlapiSelectValue(xmlData,
                                            '//Assignment/AsgCity');
											
											//update :
											
				     var f_person_id = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/PersonID');
				      var f_employer_id = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/EmployerId');
				     var f_assignment_numb = nlapiSelectValue(xmlData, '/EmployeeData/AssignmentHT/AssignmentData/Assignment/AssignmentNumber');

                                        if (brillio_Location_U) {
                                            employeeRecord.setFieldValue('custentity_list_brillio_location_e',
                                                brillio_Location_U);
                                        }
                                        if (brillio_address1_U) {
                                            employeeRecord.setFieldValue('custentity_brillio_address_1',
                                                brillio_address1_U);
                                        }
                                        if (brillio_address2_U) {
                                            employeeRecord.setFieldValue('custentity_brillio_address_2',
                                                brillio_address2_U);
                                        }
                                        if (brillio_country_U) {
                                            employeeRecord.setFieldValue('custentity_brillio_country',
                                                brillio_country_U);
                                        }
                                        if (brillio_zipCode_U) {
                                            employeeRecord.setFieldValue('custentity_brillio_zip_code',
                                                brillio_zipCode_U);
                                        }
                                        if (brillio_city_U) {
                                            employeeRecord.setFieldValue('custentity_workcity',
                                                brillio_city_U);
                                        }
										
										 if (f_person_id) {
                                            employeeRecord.setFieldValue('custentity_personid',
                                                f_person_id);
                                        }
                                        if (f_employer_id) {
                                            employeeRecord.setFieldValue('custentity_employer_id',
                                                f_employer_id);
                                        }
                                        
                                        if (f_assignment_numb) {
                                            employeeRecord.setFieldValue('custentity_assignment_num',
                                                f_assignment_numb);
                                        }

                                        employeeRecord.setFieldValue('autoname', 'F');

                                        if (U_Firstname != null && U_Lastname == null) {
                                            U_Lastname = employeeRecord
                                                .getFieldValue('lastname');
                                            if (U_Middlename == null) {
                                                U_Middlename = employeeRecord
                                                    .getFieldValue('middlename');
                                                if (U_Middlename != null) {
                                                    employeeRecord.setFieldValue(
                                                        'entityid', U_personNumber +
                                                        "-" + U_Firstname +
                                                        " " + U_Middlename +
                                                        " " + U_Lastname);
                                                } else {
                                                    employeeRecord.setFieldValue(
                                                        'entityid', U_personNumber +
                                                        "-" + U_Firstname +
                                                        " " + U_Lastname);
                                                }
                                            } else {
                                                employeeRecord.setFieldValue('entityid',
                                                    U_personNumber + "-" + U_Firstname +
                                                    " " + U_Middlename + " " +
                                                    U_Lastname);
                                            }
                                        } else if (U_Firstname == null &&
                                            U_Lastname != null) {
                                            U_Firstname = employeeRecord
                                                .getFieldValue('firstname');
                                            if (U_Middlename == null) {
                                                U_Middlename = employeeRecord
                                                    .getFieldValue('middlename');
                                                if (U_Middlename != null) {
                                                    employeeRecord.setFieldValue(
                                                        'entityid', U_personNumber +
                                                        "-" + U_Firstname +
                                                        " " + U_Middlename +
                                                        " " + U_Lastname);
                                                } else {
                                                    employeeRecord.setFieldValue(
                                                        'entityid', U_personNumber +
                                                        "-" + U_Firstname +
                                                        " " + U_Lastname);
                                                }

                                            } else {
                                                employeeRecord.setFieldValue('entityid',
                                                    U_personNumber + "-" + U_Firstname +
                                                    " " + U_Middlename + " " +
                                                    U_Lastname);
                                            }
                                        }

                                        // update hire date
                                        var hire_date = nlapiSelectValue(xmlData,
                                            '//EmployeeData/HireDate');
                                        if (hire_date != null) {
                                            var d = hire_date.slice(0, 10).split('-');
                                            hire_day = d[1] + '/' + d[2] + '/' + d[0];
                                            employeeRecord.setFieldValue('hiredate',
                                                hire_day);
                                        }

                                        var birth_date = nlapiSelectValue(xmlData,
                                            '//EmployeeData/DateofBirth');
                                        if (birth_date != null) {
                                            var b = birth_date.slice(0, 10).split('-');
                                            birth_day = b[1] + '/' + b[2] + '/' + b[0];
                                            employeeRecord.setFieldValue('birthdate',
                                                birth_day);
                                        }

                                        var Identifier_type = nlapiSelectValue(xmlData,
                                            '//NationalIDHT/NtionalIDData/NationalIDValues/NationalType');
                                        if (Identifier_type != null) {
                                            if (Identifier_type == "SSN") {
                                                var ssn = nlapiSelectValue(xmlData,
                                                    '//NationalIDValues/NationalIDNumber');
                                                employeeRecord.setFieldValue(
                                                    'socialsecuritynumber', ssn);
                                            } else {
                                                var pan = nlapiSelectValue(xmlData,
                                                    '//NationalIDValues/NationalIDNumber');
                                                employeeRecord.setFieldValue(
                                                    'custentity_permanent_account', pan);
                                            }
                                        }
                                    }
                                    else {
                                        nlapiSetFieldValue('custrecord_fi_error_log',
                                            'Unique Fusion ID not found for updating record');
                                    }
                                }

                                field_search.forEach(function(field) {
                                    try {
                                        var xmlTagName = field
                                            .getValue('custrecord_fi_xml_tag_name');
                                        var netsuiteInternalId = field
                                            .getValue('custrecord_fi_netsuite_field_name');
                                        var fieldType = field
                                            .getValue('custrecord_fi_field_type');

                                        var fieldValue = nlapiSelectValue(xmlData,
                                            '/EmployeeData/' + xmlTagName);

                                        //AssignmentHT/AssignmentData/Assignment/LegalEntity

                                        var listName = field
                                            .getValue('custrecord_fi_list_type');

                                        // if a list name exists, pull the correct value from
                                        // the
                                        // dropdown array created above

                                        if (fieldType == 1) {
                                            fieldValue = formatDate(fieldValue);
                                        }



                                        // if (fieldType == 5) {

                                        if (listName) {
                                            fieldValue = dropdown[listName][fieldValue];
                                        }
                                        // } else

                                        nlapiLogExecution('debug', 'xml netsuite value',
                                            xmlTagName + " " + netsuiteInternalId + " " +
                                            fieldValue);

                                        if (fieldValue) {
                                            employeeRecord.setFieldValue(netsuiteInternalId,
                                                fieldValue);
                                        }
                                    } catch (e) {
                                        nlapiLogExecution('debug', 'Field Mapping Error', e);
                                    }
                                });

                                var newLevel = employeeRecord.getFieldValue('employeestatus');

                                var id = nlapiSubmitRecord(employeeRecord, false, true);
                                nlapiLogExecution('debug', 'employee record created', id);

                                var brillio_Location_Hire = nlapiSelectValue(xmlData,
                                    '//Assignment/BaseLocation');
                                var brillio_address1 = nlapiSelectValue(xmlData,
                                    '//Assignment/AsgAddressLine1');
                                var brillio_address2 = nlapiSelectValue(xmlData,
                                    '//Assignment/AsgAddressLine2');
                                var brillio_city = nlapiSelectValue(xmlData,
                                    '//Assignment/AsgCity');
                                var brillio_country = nlapiSelectValue(xmlData,
                                    '//Assignment/AsgCountry');
                                var brillio_zipCode = nlapiSelectValue(xmlData,
                                    '//Assignment/AsgZipCode');
                                var brillio_BaseLocation_OLD = nlapiSelectValue(xmlData,
                                    '//Assignment/BaseLocation_OLD');

                                //Update WorkAddress
                                if (brillio_Location_Hire && brillio_BaseLocation_OLD) {
                                    updateEmployeeWorkAddress(id, xmlData);
                                } else if (brillio_Location_Hire && brillio_BaseLocation_OLD == '') {
                                    createEmployeeWorkAddress(id, xmlData);
                                }


                                //Update Physical work address
                                var physicalAddress_NEW = nlapiSelectValue(xmlData,
                                    '//Assignment/PhysicalWorkAddress');
                                var physicalAddress_OLD = nlapiSelectValue(xmlData,
                                    '//Assignment/PhysicalWorkAddress_OLD');
                                var startDate = nlapiSelectValue(xmlData,
                                    '//Assignment/AssignmentEffStartDate');
                                startDate = formatDate(startDate);

                                /*if(physicalAddress_NEW && physicalAddress_OLD ){
                                	updatePhysicalAddress(id, xmlData);
                                }
                                else if(physicalAddress_NEW && physicalAddress_OLD == ''){
                                		createPhysicalCustomRecord(id, xmlData);
                                	
                                	}*/
                                //Update Part
                                //Probation Period
                                var probatiobPeriod_M = nlapiSelectValue(xmlData, '//Assignment/ProbationPeriod');
                                var probatiobPeriod_M_OLD = nlapiSelectValue(xmlData, '//Assignment/ProbationPeriod_OLD');
                                //	probatiobPeriod_M = formatDate(probatiobPeriod_M);
                                //probatiobPeriod_M_OLD = formatDate(probatiobPeriod_M_OLD);
                                if (probatiobPeriod_M && probatiobPeriod_M_OLD && (fieldData != "HIRE" || fieldData != "ADD_CWK")) {
                                    updateCustomRecordProbationPeriod(id, startDate, probatiobPeriod_M, probatiobPeriod_M_OLD);
                                } else if (probatiobPeriod_M && probatiobPeriod_M_OLD == '' && (fieldData == "HIRE" || fieldData == "ADD_CWK")) {
                                    createCustomRecordProbationPeriod(id, startDate, probatiobPeriod_M);
                                }
                                //Probation End Date 
                                var probatiobEnd_Date_M = nlapiSelectValue(xmlData, '//Assignment/ProbationEndDate');
                                var probatiobEnd_Date_M_OLD = nlapiSelectValue(xmlData, '//Assignment/ProbationEndDate_OLD');
                                probatiobEnd_Date_M = formatDate(probatiobEnd_Date_M);
                                probatiobEnd_Date_M_OLD = formatDate(probatiobEnd_Date_M_OLD);
                                if (probatiobEnd_Date_M && probatiobEnd_Date_M_OLD && (fieldData != "HIRE" || fieldData != "ADD_CWK")) {
                                    updateCustomRecordProbationEndDate(id, startDate, probatiobEnd_Date_M, probatiobEnd_Date_M_OLD);
                                } else if (probatiobEnd_Date_M && probatiobEnd_Date_M_OLD == '' && (fieldData == "HIRE" || fieldData == "ADD_CWK")) {
                                    createCustomRecordProbationEndDate(id, startDate, probatiobEnd_Date_M);
                                }
                                //Hourly Change
                                var HourlySalaried = nlapiSelectValue(xmlData, '//Assignment/HourlySalaried');
                                var HourlySalaried_OLD = nlapiSelectValue(xmlData, '//Assignment/HourlySalaried_OLD');
                                if (HourlySalaried && HourlySalaried_OLD && (fieldData != "HIRE" || fieldData != "ADD_CWK")) {
                                    updateCustomRecordHourlyChange(id, startDate, HourlySalaried, HourlySalaried_OLD);
                                } else if (HourlySalaried && HourlySalaried_OLD == '' && (fieldData == "HIRE" || fieldData == "ADD_CWK")) {
                                    createCustomRecordHourlyChange(id, startDate, HourlySalaried);
                                }
                                //Assigment Change
                                var AssignmentCategory = nlapiSelectValue(xmlData, '//Assignment/AssignmentCategory');
                                var AssignmentCategory_OLD = nlapiSelectValue(xmlData, '//Assignment/AssignmentCategory_OLD');
                                if (AssignmentCategory && AssignmentCategory_OLD && (fieldData != "HIRE" || fieldData != "ADD_CWK")) {
                                    updateCustomRecordAssignmentChange(id, startDate, assigmentCategory_NEW_U, assigmentCategory_OLD_U);
                                } else if (AssignmentCategory && AssignmentCategory_OLD == '' && (fieldData == "HIRE" || fieldData == "ADD_CWK")) {
                                    createCustomRecordAssignmentChange(id, startDate, assigmentCategory_NEW_U);
                                }
                                //Designation Change
                                var Desgination = nlapiSelectValue(xmlData, '//Assignment/Desgination');
                                var Desgination_OLD = nlapiSelectValue(xmlData, '//Assignment/Desgination_OLD');
                                //Grade Change
                                var Grade = nlapiSelectValue(xmlData, '//Assignment/Grade');
                                var Grade_OLD = nlapiSelectValue(xmlData, '//Assignment/Grade_OLD');
                                //Department Change
                                var Department = nlapiSelectValue(xmlData, '//Assignment/Department');
                                var Department_OLD = nlapiSelectValue(xmlData, '//Assignment/Department_OLD');
                                if (Department && Department_OLD && (fieldData != "HIRE" || fieldData != "ADD_CWK")) {
                                    updateCustomRecordDepartmentChange(id, startDate, Department, Department_OLD);
                                } else if (Department && Department_OLD == '' && (fieldData == "HIRE" || fieldData == "ADD_CWK")) {
                                    createCustomRecordDepartmentChange(id, startDate, Department);
                                }
                                //Function Change
                                var Function = nlapiSelectValue(xmlData, '//Assignment/Function');
                                var Function_OLD = nlapiSelectValue(xmlData, '//Assignment/Function_OLD');
                                if (Function && Function_OLD && (fieldData != "HIRE" || fieldData != "ADD_CWK")) {
                                    updateCustomRecordFucntionChange(id, startDate, Function, Function_OLD);
                                } else if (Function && Function_OLD == '' && (fieldData == "HIRE" || fieldData == "ADD_CWK")) {
                                    createCustomRecordFucntionChange(id, startDate, Function);
                                }

                                if (personType == 'CWK' && Vendor_ID) {
                                    //Create Subtier function Deepak MS, Start 4th Aug 2016
                                    createUpdateSubtierRecord(id, xmlData, Vendor_ID, referral_Vendor_ID);
                                    //End
                                }
                                if (personType == 'CWK' && referral_Vendor_ID) {
                                    //Create Subtier function Deepak MS, Start 4th Aug 2016
                                    createUpdateSubtierReferralRecord(id, xmlData, Vendor_ID, referral_Vendor_ID);
                                    //End
                                }
                                //Update subtier record if pay rate or vendor change
                                var oTRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementValue');
                                var oTRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementStartDate');
                                var sTRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Hourly Salary"]/ElementValue');
                                var sTRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementStartDate');
                                var payRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Pay Rate"]/ElementValue');
                                var payRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementStartDate');
                                var vendor_Name_U = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName');
                                var referral_payRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Referral Vendor Pay Rate"]/ElementValue');
                                var referral_payRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Referral Vendor Pay Rate"]/ElementStartDate');
                                var vendor_Name_OLD1_U = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName_OLD');
                                if (((oTRate_Value_U || sTRate_Value_U || payRate_Value_U) && personType != 'CWK') || vendor_Name_OLD1_U) {
                                    updateSubtierVendor_REC(xmlData, Vendor_ID);
                                }

                                // update level change history
                                if (!oldLevel) {
                                    updateEmployeeLevelHistory(id, 'create');
                                } else if (oldLevel != newLevel) {
                                    updateEmployeeLevelHistory(id, 'edit');
                                } else {
                                    nlapiLogExecution('debug', 'Level Change History',
                                        'not running ' + id);
                                }

                                nlapiSetFieldValue('custrecord_fi_error_log', '');
                                nlapiSetFieldValue('custrecord_fi_employee', id);
                            } catch (e) {
                                nlapiLogExecution('error', 'Inside', e);
                                nlapiSetFieldValue('custrecord_fi_error_log', e.message);
                            }
                        }
                        else {
                            nlapiSetFieldValue('custrecord_fi_error_log',
                                "No Fields Added In The Custom Record");
                        }
                    } catch (err) {
                        nlapiLogExecution('error', 'main', err);
                    }
                }
            }

            function createUpdateSubtierReferralRecord(employeeId, xmlData, vendorId, referral_Vendor_ID) {
                try {

                    var personType = nlapiSelectValues(xmlData, '//Employee/PersonType');
                    var referral_Vendor = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/ReferralVendor');
                    /*if(personType[0] !='CWK'){
                    	return;
                    }*/
                    var oTRate_Value = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementValue');
                    var sTRate_Value = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Hourly Salary"]/ElementValue');
                    var payRate_Value = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Pay Rate"]/ElementValue');

                    nlapiLogExecution('debug', 'Update Subtier Rec Vendor ID', 'Vendor ID:' + vendorId);
                    nlapiLogExecution('debug', 'Update Subtier Rec Emp ID', 'Emp ID:' + employeeId);
                    if (vendorId) {
                        var vendor_LookUp = nlapiLookupField('vendor', vendorId, ['terms']);
                        var payment_Terms = vendor_LookUp.terms;
                    }
                    if (employeeId) {
                        var emp_LookUp = nlapiLookupField('employee', employeeId, ['subsidiary']);
                        var emp_sub = emp_LookUp.subsidiary;
                    }

                    //Finalize

                    var subtierEntryName = nlapiSelectValues(xmlData,
                        '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementName');


                    var subtierEntryRate = nlapiSelectValues(xmlData,
                        '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementValue');
                    var subtierEntryStartDate = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementStartDate');
                    //var subtierEntryStartDate = '2016-08-04T00:00:00.000Z';
                    //subtierEntryStartDate = formatDate(subtierEntryStartDate);
                    var vendor_Name = nlapiSelectValues(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName');
                    var referral_Vendor_R = nlapiSelectValues(xmlData, '//AssignmentHT/AssignmentData/Assignment/ReferralVendor');


                    nlapiLogExecution('debug', 'Vendor Name & vendorId & personType:', vendor_Name[0] + ' ' + vendorId + ' ' + personType[0]);
                    //TEST DATA


                    //end subtier record
                    //endActiveSubtierRecord(employeeId,
                    //        formatDate(subtierEntryStartDate[i]));




                    //If referral Vendor Exists	
                    if (_logValidation(referral_Vendor_ID)) {
                        if (referral_Vendor_ID) {
                            var vendor_LookUp_R = nlapiLookupField('vendor', referral_Vendor_ID, ['terms']);
                            var payment_Terms_R = vendor_LookUp_R.terms;
                        }
                        var newSubtierRecord_R = nlapiCreateRecord('customrecord_subtier_vendor_data');
                        newSubtierRecord_R.setFieldValue('custrecord_stvd_contractor',
                            employeeId);
                        newSubtierRecord_R.setFieldValue('custrecord_stvd_vendor',
                            referral_Vendor_ID);


                        newSubtierRecord_R.setFieldValue('custrecord_stvd_start_date',
                            formatDate(subtierEntryStartDate));

                        newSubtierRecord_R.setFieldValue('custrecord_stvd_end_date',
                            '12/31/2018');

                        //For US

                        //Check for OT Rate

                        //st pay rate
                        newSubtierRecord_R.setFieldValue('custrecord_stvd_st_pay_rate',
                            parseFloat(0.0));
                        //ot pay rate


                        newSubtierRecord_R.setFieldValue('custrecord_stvd_ot_pay_rate',
                            parseFloat(0.0));




                        /*else{
                        	//ot pay rate
                        	newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                        	        parseFloat(subTier_Rate));
                        	newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                        	        parseFloat(0.0));
                        	}*/

                        newSubtierRecord_R.setFieldValue('custrecord_stvd_vendor_type', 2);


                        //Update 
                        newSubtierRecord_R.setFieldValue('custrecord_stvd_payment_terms',
                            payment_Terms_R);
                        newSubtierRecord_R.setFieldValue('custrecord_stvd_subsidiary',
                            nlapiLookupField('employee', employeeId, 'subsidiary'));

                        //Maonthly
                        if (emp_sub == 3) {
                            newSubtierRecord_R.setFieldValue('custrecord_stvd_rate_type',
                                3);

                        } //Hourly
                        else {
                            newSubtierRecord_R.setFieldValue('custrecord_stvd_rate_type',
                                1);
                        }

                        var newSubtierRecordId = nlapiSubmitRecord(newSubtierRecord_R, false, true);
                        nlapiLogExecution('DEBUG', 'newSubtierRecordId', newSubtierRecordId);
                    }

                } catch (err) {
                    nlapiLogExecution('ERROR', 'createUpdateSubtierReferralRecordRecord', err);
                }
            }

            function updateSubtierVendor_REC(xmlData, Vendor_ID) {
                try {
                    var oTRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementValue');
                    var oTRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementStartDate');
                    var sTRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Hourly Salary"]/ElementValue');
                    var sTRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Hourly Salary"]/ElementStartDate');

                    var payRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Pay Rate"]/ElementValue');
                    var payRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Pay Rate"]/ElementStartDate');

                    //var vendor_Name_U = nlapiSelectValue(xmlData,'//AssignmentHT/AssignmentData/Assignment/VendorName');
                    var fusion_Emp_Numb = nlapiSelectValue(xmlData, '//Assignment/PersonNumber');
                    var referral_Vendor = nlapiSelectValues(xmlData, '//AssignmentHT/AssignmentData/Assignment/ReferralVendor');
                    //Referral Vendor
                    var referral_payRate_Value_U = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Referral Vendor Pay Rate"]/ElementValue');
                    var referral_payRate_Value_U_St_date = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Referral Vendor Pay Rate"]/ElementStartDate');

                    //Vendor Change
                    var vendor_Name_OLD_U = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName_OLD');
                    var vendor_Name_U = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName');

                    if (vendor_Name_OLD_U) {
                        var vendor_LookUp = nlapiLookupField('vendor', Vendor_ID, ['terms']);
                        var payment_Terms = vendor_LookUp.terms;
                    }

                    var rateChange_date = null;
                    var rateChange_date_referral = null;
                    if (oTRate_Value_U_St_date) {
                        rateChange_date = formatDate(oTRate_Value_U_St_date);
                    } else if (sTRate_Value_U_St_date) {
                        rateChange_date = formatDate(sTRate_Value_U_St_date);
                    } else if (payRate_Value_U_St_date) {
                        rateChange_date = formatDate(payRate_Value_U_St_date);
                    }
                    rateChange_date = nlapiStringToDate(rateChange_date);
                    if (referral_payRate_Value_U_St_date) {
                        rateChange_date_referral = formatDate(referral_payRate_Value_U_St_date);
                    }
                    //var fusion_Number = nlapiSelectValue(xmlData,'//Assignment/PersonNumber');
                    //var employeeRecord = null;
                    var empSearch_U = null;
                    //IF new hire and update happens on the same day
                    if (fusion_Emp_Numb) {
                        empSearch_U = nlapiSearchRecord('employee', null, [
                            new nlobjSearchFilter('custentity_fusion_empid', null,
                                'is', fusion_Emp_Numb),
                            new nlobjSearchFilter('custentity_employee_inactive',
                                null, 'is', 'F')
                        ], [new nlobjSearchColumn(
                            'internalid')]);
                    }
                    if (empSearch_U) {
                        var emp_id = empSearch_U[0].getValue('internalid');
                    }
                    //For Subtier Vendor
                    if (emp_id) {
                        var cols = Array();
                        cols.push(new nlobjSearchColumn('internalid'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_vendor_type'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_vendor'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_start_date'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_end_date'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_st_pay_rate'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_payment_terms'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_ot_pay_rate'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_subsidiary'));
                        cols.push(new nlobjSearchColumn('custrecord_stvd_rate_type'));

                        var subtierSearch = nlapiSearchRecord(
                            'customrecord_subtier_vendor_data', null, [
                                new nlobjSearchFilter(
                                    'custrecord_stvd_contractor', null,
                                    'anyof', emp_id),
                                new nlobjSearchFilter(
                                    'custrecord_stvd_vendor_type', null,
                                    'anyof', 1),
                                new nlobjSearchFilter(
                                    'custrecord_stvd_end_date', null,
                                    'onorafter', rateChange_date)
                            ], cols);

                        // change the end date of all the found subtier record
                        if (subtierSearch) {

                            var subtier_id = subtierSearch[0].getValue('internalid');
                            var vendorType = subtierSearch[0].getValue('custrecord_stvd_vendor_type');
                            var vendorVal = subtierSearch[0].getValue('custrecord_stvd_vendor');
                            var vendor_st_rate = subtierSearch[0].getValue('custrecord_stvd_st_pay_rate');
                            var vendor_ot_rate = subtierSearch[0].getValue('custrecord_stvd_ot_pay_rate');
                            var vendor_subsidiary = subtierSearch[0].getValue('custrecord_stvd_subsidiary');
                            var vendor_rate_type = subtierSearch[0].getValue('custrecord_stvd_rate_type');
                            var vendor_payment_terms = subtierSearch[0].getValue('custrecord_stvd_payment_terms');
                            var vendor_start_date = subtierSearch[0].getValue('custrecord_stvd_start_date');
                        }
                        if (nlapiStringToDate(vendor_start_date) != rateChange_date) {
                            //IF both dates are not same then create
                            if (subtier_id) {
                                var end_DATE = nlapiAddDays(rateChange_date, -1);
                                var subTierRec = nlapiLoadRecord('customrecord_subtier_vendor_data', subtier_id);
                                subTierRec.setFieldValue('custrecord_stvd_end_date', nlapiDateToString(end_DATE));
                                nlapiSubmitRecord(subTierRec, false, true);
                            }
                            //Create New Subtier Record
                            if (subtier_id) {
                                var newSubtierRecord = nlapiCreateRecord('customrecord_subtier_vendor_data');
                                newSubtierRecord.setFieldValue('custrecord_stvd_contractor',
                                    emp_id);
                                if (vendor_Name_OLD_U) {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_vendor',
                                        Vendor_ID);
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_vendor',
                                        vendorVal);
                                }
                                if (rateChange_date) {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_start_date',
                                        nlapiDateToString(rateChange_date));
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_start_date',
                                        nlapiDateToString(vendor_start_date));
                                }


                                newSubtierRecord.setFieldValue('custrecord_stvd_end_date',
                                    '12/31/2018');

                                //OT Rate Update
                                if (oTRate_Value_U) {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                        parseFloat(oTRate_Value_U));
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                        parseFloat(vendor_ot_rate));
                                }
                                if (sTRate_Value_U) {
                                    //ST Rate
                                    newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                        parseFloat(sTRate_Value_U));
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                        parseFloat(vendor_st_rate));
                                }
                                if (payRate_Value_U) {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                        parseFloat(payRate_Value_U));
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                        parseFloat(vendor_st_rate));
                                }

                                /*else{
                                	//ot pay rate
                                	newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                	        parseFloat(subTier_Rate));
                                	newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                	        parseFloat(0.0));
                                	}*/

                                newSubtierRecord.setFieldValue('custrecord_stvd_vendor_type', vendorType);

                                //Update 
                                if (vendor_Name_OLD_U) {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_payment_terms',
                                        payment_Terms);
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_payment_terms',
                                        vendor_payment_terms);
                                }


                                newSubtierRecord.setFieldValue('custrecord_stvd_subsidiary', vendor_subsidiary);

                                //Maonthly

                                newSubtierRecord.setFieldValue('custrecord_stvd_rate_type',
                                    vendor_rate_type);



                                var newSubtierRecordId = nlapiSubmitRecord(newSubtierRecord, false, true);
                                nlapiLogExecution('DEBUG', 'newSubtierRecordId', newSubtierRecordId);
                            }
                        }
                        /*for (var index = 0; index < subtierSearch.length; index++) {
                        	var subTierRec = nlapiLoadRecord(
                        	        'customrecord_subtier_vendor_data',
                        	        subtierSearch[index].getId(), {
                        		        recordmode : 'dynamic'
                        	        });

                        	// if start date is after the termination date,
                        	// change
                        	// the start date
                        	if (nlapiStringToDate(subTierRec
                        	        .getFieldValue('custrecord_stvd_start_date')) > nlapiStringToDate(rateChange_date)) {
                        		subTierRec.setFieldValue('isinactive', 'T');
                        		subTierRec.setFieldValue(
                        		        'custrecord_stvd_start_date', last_day);
                        	}

                        	subTierRec.setFieldValue(
                        	        'custrecord_stvd_end_date', last_day);

                        	nlapiSubmitRecord(subTierRec, false, true);*/
                    }
                    //Referral Vendor Change
                    if (referral_payRate_Value_U) {
                        var cols_R = Array();
                        cols_R.push(new nlobjSearchColumn('internalid'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_vendor_type'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_vendor'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_start_date'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_end_date'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_st_pay_rate'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_payment_terms'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_ot_pay_rate'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_subsidiary'));
                        cols_R.push(new nlobjSearchColumn('custrecord_stvd_rate_type'));

                        var subtierSearch_R = nlapiSearchRecord(
                            'customrecord_subtier_vendor_data', null, [
                                new nlobjSearchFilter(
                                    'custrecord_stvd_contractor', null,
                                    'anyof', emp_id),
                                new nlobjSearchFilter(
                                    'custrecord_stvd_vendor_type', null,
                                    'anyof', 2),
                                new nlobjSearchFilter(
                                    'custrecord_stvd_end_date', null,
                                    'onorafter', rateChange_date)
                            ], cols_R);

                        // change the end date of all the found subtier record
                        if (subtierSearch_R) {

                            var subtier_id_R = subtierSearch_R[0].getValue('internalid');
                            var vendorType_R = subtierSearch_R[0].getValue('custrecord_stvd_vendor_type');
                            var vendorVal_R = subtierSearch_R[0].getValue('custrecord_stvd_vendor');
                            var vendor_st_rate_R = subtierSearch_R[0].getValue('custrecord_stvd_st_pay_rate');
                            var vendor_ot_rate_R = subtierSearch_R[0].getValue('custrecord_stvd_ot_pay_rate');
                            var vendor_subsidiary_R = subtierSearch_R[0].getValue('custrecord_stvd_subsidiary');
                            var vendor_rate_type_R = subtierSearch_R[0].getValue('custrecord_stvd_rate_type');
                            var vendor_payment_terms_R = subtierSearch_R[0].getValue('custrecord_stvd_payment_terms');
                            var vendor_start_date_R = subtierSearch[0].getValue('custrecord_stvd_start_date');
                        }
                        if (nlapiStringToDate(vendor_start_date_R) != rateChange_date) {
                            if (subtier_id_R) {
                                var end_DATE = nlapiAddDays(rateChange_date, -1);
                                var subTierRec_R = nlapiLoadRecord('customrecord_subtier_vendor_data', subtier_id_R);
                                subTierRec_R.setFieldValue('custrecord_stvd_end_date', nlapiDateToString(end_DATE));
                                nlapiSubmitRecord(subTierRec_R, false, true);
                            }
                            //Create New Subtier Record
                            if (subtier_id_R) {
                                var newSubtierRecord = nlapiCreateRecord('customrecord_subtier_vendor_data');
                                newSubtierRecord.setFieldValue('custrecord_stvd_contractor',
                                    emp_id);
                                newSubtierRecord.setFieldValue('custrecord_stvd_vendor',
                                    vendorVal_R);


                                newSubtierRecord.setFieldValue('custrecord_stvd_start_date',
                                    rateChange_date_referral);

                                newSubtierRecord.setFieldValue('custrecord_stvd_end_date',
                                    '12/31/2018');

                                //OT Rate Update

                                newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                    parseFloat(vendor_ot_rate_R));

                                if (referral_payRate_Value_U) {
                                    //ST Rate
                                    newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                        parseFloat(referral_payRate_Value_U));
                                } else {
                                    newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                        parseFloat(vendor_st_rate_R));
                                }

                                /*else{
                                	//ot pay rate
                                	newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                	        parseFloat(subTier_Rate));
                                	newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                	        parseFloat(0.0));
                                	}*/

                                newSubtierRecord.setFieldValue('custrecord_stvd_vendor_type', vendorType_R);

                                //Update 
                                newSubtierRecord.setFieldValue('custrecord_stvd_payment_terms',
                                    vendor_payment_terms_R);
                                newSubtierRecord.setFieldValue('custrecord_stvd_subsidiary', vendor_subsidiary_R);

                                //Maonthly

                                newSubtierRecord.setFieldValue('custrecord_stvd_rate_type',
                                    vendor_rate_type_R);



                                var newSubtierRecordId = nlapiSubmitRecord(newSubtierRecord, false, true);
                                nlapiLogExecution('DEBUG', 'newSubtierRecordId', newSubtierRecordId);
                            }
                        }
                        /*for (var index = 0; index < subtierSearch.length; index++) {
                        		var subTierRec = nlapiLoadRecord(
                        		        'customrecord_subtier_vendor_data',
                        		        subtierSearch[index].getId(), {
                        			        recordmode : 'dynamic'
                        		        });

                        		// if start date is after the termination date,
                        		// change
                        		// the start date
                        		if (nlapiStringToDate(subTierRec
                        		        .getFieldValue('custrecord_stvd_start_date')) > nlapiStringToDate(rateChange_date)) {
                        			subTierRec.setFieldValue('isinactive', 'T');
                        			subTierRec.setFieldValue(
                        			        'custrecord_stvd_start_date', last_day);
                        		}

                        		subTierRec.setFieldValue(
                        		        'custrecord_stvd_end_date', last_day);

                        		nlapiSubmitRecord(subTierRec, false, true);*/
                    }
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Updating Subtier Record Error', e);
                }
            }

            function createCustomRecordProbationEndDate(emp_id, startDate, probatiobEnd_Date_M) {
                try {
                    var createProbationPeriodEnd_Date_Rec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createProbationPeriodEnd_Date_Rec.setFieldValue('custrecord_field_name', 'Probation End Date');
                    createProbationPeriodEnd_Date_Rec.setFieldValue('custrecord_new_value_fu_update', probatiobEnd_Date_M);
                    createProbationPeriodEnd_Date_Rec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createProbationPeriodEnd_Date_Rec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createProbationPeriodEnd_Date_Rec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Error in Creating Probation End Date Fusion Custom in New Hire', e);
                }
            }

            function createCustomRecordProbationPeriod(emp_id, startDate, probatiobPeriod_M) {
                try {
                    var createProbationPeriodRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createProbationPeriodRec.setFieldValue('custrecord_field_name', 'Probation Period');
                    createProbationPeriodRec.setFieldValue('custrecord_new_value_fu_update', probatiobPeriod_M);
                    createProbationPeriodRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createProbationPeriodRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createProbationPeriodRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Error in Creating Probation period Fusion Custom in New Hire', e);
                }
            }

            function createCustomRecordFucntionChange(emp_id, startDate, Function) {
                try {
                    var createFunctionRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createFunctionRec.setFieldValue('custrecord_field_name', 'Function');
                    createFunctionRec.setFieldValue('custrecord_new_value_fu_update', Function);
                    createFunctionRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createFunctionRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createFunctionRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Error in Creating Function Fusion Custom in New Hire', e);
                }
            }

            function createCustomRecordDepartmentChange(emp_id, startDate, Department) {
                try {
                    var createPracticeRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createPracticeRec.setFieldValue('custrecord_field_name', 'Practice');
                    createPracticeRec.setFieldValue('custrecord_new_value_fu_update', Department);
                    createPracticeRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createPracticeRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createPracticeRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Error in Creating Practice Fusion Custom in New Hire', e);
                }
            }

            function createCustomRecordAssignmentChange(emp_id, startDate, assigmentCategory_NEW_U) {
                try {
                    var assignmentCategory_List = [' ', 'Full-time', 'Full-time regular', 'Full-time contract', 'Part Time-Regular', 'Part Time-Contract', 'Contractor', 'MS Full Time', 'MS InsVac Full-Time',
                        'MS InsVac Part-Time', 'MS Non BenVac Full-Time', 'MS Non BenVac Part Time', 'MS Part Time', ' ', 'Part Time', ' ', ' ', 'MS VAC – Full Time', 'MS VAC – Part Time', ' ', ' ', 'MS Nov Vac – Full time', 'MS Nov Vac – Part time'
                    ];
                    var createAssignmentRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createAssignmentRec.setFieldValue('custrecord_field_name', 'Assignment');
                    createAssignmentRec.setFieldValue('custrecord_new_value_fu_update', assignmentCategory_List[assigmentCategory_NEW_U]);
                    createAssignmentRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createAssignmentRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createAssignmentRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Error in Creating Assignment Fusion Custom in New Hire', e);
                }

            }

            function createCustomRecordHourlyChange(emp_id, startDate, HourlySalaried) {
                try {
                    var createHourlyRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createHourlyRec.setFieldValue('custrecord_field_name', 'Hourly');
                    createHourlyRec.setFieldValue('custrecord_new_value_fu_update', HourlySalaried);
                    createHourlyRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createHourlyRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var hourlyID = nlapiSubmitRecord(createHourlyRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Error in Creating Hourly Fusion Custom in New Hire', e);
                }
            }

            function createEmployeeWorkAddress(emp_id, xmlData) {
                try {
                    var assignmentEffStartDate = nlapiSelectValue(xmlData,
                        '//Assignment/AssignmentEffStartDate');
                    assignmentEffStartDate = formatDate(assignmentEffStartDate);

                    var brillio_BaseLocation_OLD = nlapiSelectValue(xmlData,
                        '//Assignment/BaseLocation_OLD');

                    var brillio_Location_N = nlapiSelectValue(xmlData,
                        '//Assignment/BaseLocation');

                    var brillio_address1_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgAddressLine1');

                    var brillio_address2_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgAddressLine2');

                    var brillio_city_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgCity');

                    var brillio_country_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgCountry');

                    var brillio_zipCode_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgZipCode');
                    var brillio_state_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgState');

                    //Create Work Adress Record
                    var createworkAddress_Rec = nlapiCreateRecord('customrecord_employee_work_address');
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_location', brillio_Location_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_1', brillio_address1_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_2', brillio_address2_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_city', brillio_city_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_state', brillio_state_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_country', brillio_country_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_zipcode', brillio_zipCode_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_employee', emp_id);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_st_date', assignmentEffStartDate);
                    var workAdd_ID = nlapiSubmitRecord(createworkAddress_Rec);
                    nlapiLogExecution('DEBUG', 'Work Address New Hire', workAdd_ID);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Creating WorkAddress Error for New Hire', e);
                }
            }

            function updateEmployeeWorkAddress(emp_id, xmlData) {
                try {
                    var assignmentEffStartDate = nlapiSelectValue(xmlData,
                        '//Assignment/AssignmentEffStartDate');
                    assignmentEffStartDate = formatDate(assignmentEffStartDate);

                    var brillio_BaseLocation_OLD = nlapiSelectValue(xmlData,
                        '//Assignment/BaseLocation_OLD');

                    var brillio_Location_N = nlapiSelectValue(xmlData,
                        '//Assignment/BaseLocation');

                    var brillio_address1_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgAddressLine1');

                    var brillio_address2_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgAddressLine2');

                    var brillio_city_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgCity');

                    var brillio_country_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgCountry');

                    var brillio_zipCode_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgZipCode');

                    var brillio_state_N = nlapiSelectValue(xmlData,
                        '//Assignment/AsgState');

                    var search = nlapiSearchRecord('customrecord_employee_work_address',
                        null, [new nlobjSearchFilter('custrecord_brillio_work_address_employee',
                            null, 'anyof', emp_id)], [new nlobjSearchColumn('custrecord_brillio_work_address_end_date').setSort(true),
                            new nlobjSearchColumn('custrecord_brillio_work_address_st_date')
                        ]);
                    if (search) {
                        var workAddressID = search[0].getId();
                        var workAddressStartDate = search[0].getValue('custrecord_brillio_work_address_st_date');
                        workAddressStartDate = nlapiStringToDate(workAddressStartDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(assignmentEffStartDate), -1);
                        if (workAddressStartDate > end_DATE) {
                            end_DATE = workAddressStartDate;
                        }
                        nlapiSubmitField('customrecord_employee_work_address', workAddressID,
                            'custrecord_brillio_work_address_end_date', nlapiDateToString(end_DATE));
                        //nlapiSubmitField('customrecord_employee_work_address', functionUpdateID,
                        //        'custrecord_old_value_fu_update',Function_OLD );
                    }
                    //Create Work Adress Record
                    var createworkAddress_Rec = nlapiCreateRecord('customrecord_employee_work_address');
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_location', brillio_Location_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_1', brillio_address1_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_2', brillio_address2_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_city', brillio_city_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_state', brillio_state_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_country', brillio_country_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_zipcode', brillio_zipCode_N);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_employee', emp_id);
                    createworkAddress_Rec.setFieldValue('custrecord_brillio_work_address_st_date', assignmentEffStartDate);
                    var workAdd_ID = nlapiSubmitRecord(createworkAddress_Rec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Updating Work Address Custom Record Error', e);
                }
            }

            function updateCustomRecordFucntionChange(emp_id, startDate, Function, Function_OLD) {
                try {
                    var search = nlapiSearchRecord('customrecord_fusion_update_track',
                        null, [
                            new nlobjSearchFilter('custrecord_field_name', null, 'is', 'Function'),
                            new nlobjSearchFilter('custrecord_employee_fu_update',
                                null, 'anyof', emp_id)
                        ], [new nlobjSearchColumn('custrecord_end_date_fu_update').setSort(true),
                            new nlobjSearchColumn('custrecord_start_date_fu_update')
                        ]);
                    if (search) {
                        var functionUpdateID = search[0].getId();
                        var updateFusionStDate = search[0].getValue('custrecord_start_date_fu_update');
                        updateFusionStDate = nlapiStringToDate(updateFusionStDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(startDate), -1);
                        if (updateFusionStDate > end_DATE) {
                            end_DATE = updateFusionStDate;
                        }

                        var loadRec = nlapiLoadRecord('customrecord_fusion_update_track', functionUpdateID);
                        loadRec.setFieldValue('custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        loadRec.setFieldValue('custrecord_new_value_fu_update', Function);
                        loadRec.setFieldValue('custrecord_old_value_fu_update', Function_OLD);
                        nlapiSubmitRecord(loadRec);
                        /*nlapiSubmitField('customrecord_fusion_update_track', functionUpdateID,
                                'custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        nlapiSubmitField('customrecord_fusion_update_track', functionUpdateID,
                                'custrecord_old_value_fu_update',Function_OLD );*/
                    }
                    var createFunctionRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createFunctionRec.setFieldValue('custrecord_field_name', 'Function');
                    createFunctionRec.setFieldValue('custrecord_new_value_fu_update', Function);
                    createFunctionRec.setFieldValue('custrecord_old_value_fu_update', Function_OLD);
                    createFunctionRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createFunctionRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createFunctionRec);

                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Assignment Change Error', e);
                }
            }

            function updateCustomRecordDepartmentChange(emp_id, startDate, Department, Department_OLD) {

                try {
                    var search = nlapiSearchRecord('customrecord_fusion_update_track',
                        null, [
                            new nlobjSearchFilter('custrecord_field_name', null, 'is', 'Practice'),
                            new nlobjSearchFilter('custrecord_employee_fu_update',
                                null, 'anyof', emp_id)
                        ], [new nlobjSearchColumn('custrecord_end_date_fu_update').setSort(true),
                            new nlobjSearchColumn('custrecord_start_date_fu_update')
                        ]);
                    if (search) {
                        var practiceUpdateID = search[0].getId();
                        var updateFusionStDate = search[0].getValue('custrecord_start_date_fu_update');
                        updateFusionStDate = nlapiStringToDate(updateFusionStDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(startDate), -1);
                        if (updateFusionStDate > end_DATE) {
                            end_DATE = updateFusionStDate;
                        }
                        var loadRec = nlapiLoadRecord('customrecord_fusion_update_track', practiceUpdateID);
                        loadRec.setFieldValue('custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        loadRec.setFieldValue('custrecord_new_value_fu_update', Department);
                        loadRec.setFieldValue('custrecord_old_value_fu_update', Department_OLD);
                        nlapiSubmitRecord(loadRec);
                        /*nlapiSubmitField('customrecord_fusion_update_track', practiceUpdateID,
                                'custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        nlapiSubmitField('customrecord_fusion_update_track', practiceUpdateID,
                                'custrecord_old_value_fu_update',Department_OLD );*/
                    }
                    var createPracticeRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createPracticeRec.setFieldValue('custrecord_field_name', 'Practice');
                    createPracticeRec.setFieldValue('custrecord_new_value_fu_update', Department);
                    createPracticeRec.setFieldValue('custrecord_old_value_fu_update', Department_OLD);
                    createPracticeRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createPracticeRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createPracticeRec);

                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Assignment Change Error', e);
                }
            }

            function updateCustomRecordProbationPeriod(emp_id, startDate, probatiobPeriod_M, probatiobPeriod_M_OLD) {

                try {
                    var search = nlapiSearchRecord('customrecord_fusion_update_track',
                        null, [
                            new nlobjSearchFilter('custrecord_field_name', null, 'is', 'Probation Period'),
                            new nlobjSearchFilter('custrecord_employee_fu_update',
                                null, 'anyof', emp_id)
                        ], [new nlobjSearchColumn('custrecord_end_date_fu_update').setSort(true),
                            new nlobjSearchColumn('custrecord_start_date_fu_update')
                        ]);
                    if (search) {
                        var probationPeriod_UpdateID = search[0].getId();
                        var updateFusionStDate = search[0].getValue('custrecord_start_date_fu_update');
                        updateFusionStDate = nlapiStringToDate(updateFusionStDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(startDate), -1);
                        if (updateFusionStDate > end_DATE) {
                            end_DATE = updateFusionStDate;
                        }
                        var loadRec = nlapiLoadRecord('customrecord_fusion_update_track', probationPeriod_UpdateID);
                        loadRec.setFieldValue('custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        loadRec.setFieldValue('custrecord_new_value_fu_update', probatiobPeriod_M);
                        loadRec.setFieldValue('custrecord_old_value_fu_update', probatiobPeriod_M_OLD);
                        nlapiSubmitRecord(loadRec);
                        /*nlapiSubmitField('customrecord_fusion_update_track', probationPeriod_UpdateID,
                                'custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        nlapiSubmitField('customrecord_fusion_update_track', probationPeriod_UpdateID,
                                'custrecord_old_value_fu_update',probatiobPeriod_M_OLD );*/
                    }
                    var createProbationPeriodRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createProbationPeriodRec.setFieldValue('custrecord_field_name', 'Probation Period');
                    createProbationPeriodRec.setFieldValue('custrecord_new_value_fu_update', probatiobPeriod_M);
                    createProbationPeriodRec.setFieldValue('custrecord_old_value_fu_update', probatiobPeriod_M_OLD);
                    createProbationPeriodRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createProbationPeriodRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createProbationPeriodRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Probation Period Change Error', e);
                }
            }

            function updateCustomRecordProbationEndDate(emp_id, startDate, probatiobEnd_Date_M, probatiobEnd_Date_M_OLD) {
                try {
                    var search = nlapiSearchRecord('customrecord_fusion_update_track',
                        null, [
                            new nlobjSearchFilter('custrecord_field_name', null, 'is', 'Probation End Date'),
                            new nlobjSearchFilter('custrecord_employee_fu_update',
                                null, 'anyof', emp_id)
                        ], [new nlobjSearchColumn('custrecord_end_date_fu_update').setSort(true),
                            new nlobjSearchColumn('custrecord_start_date_fu_update')
                        ]);
                    if (search) {
                        var probationEndDate_UpdateID = search[0].getId();
                        var updateFusionStDate = search[0].getValue('custrecord_start_date_fu_update');
                        updateFusionStDate = nlapiStringToDate(updateFusionStDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(startDate), -1);
                        if (updateFusionStDate > end_DATE) {
                            end_DATE = updateFusionStDate;
                        }
                        var loadRec = nlapiLoadRecord('customrecord_fusion_update_track', probationEndDate_UpdateID);
                        loadRec.setFieldValue('custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        loadRec.setFieldValue('custrecord_old_value_fu_update', probatiobEnd_Date_M_OLD);
                        loadRec.setFieldValue('custrecord_new_value_fu_update', probatiobEnd_Date_M);
                        nlapiSubmitRecord(loadRec);
                        /*nlapiSubmitField('customrecord_fusion_update_track', probationEndDate_UpdateID,
                                'custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        nlapiSubmitField('customrecord_fusion_update_track', probationEndDate_UpdateID,
                                'custrecord_old_value_fu_update',probatiobEnd_Date_M_OLD );*/
                    }
                    var createAssignmentRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createAssignmentRec.setFieldValue('custrecord_field_name', 'Probation End Date');
                    createAssignmentRec.setFieldValue('custrecord_new_value_fu_update', probatiobEnd_Date_M);
                    createAssignmentRec.setFieldValue('custrecord_old_value_fu_update', probatiobEnd_Date_M_OLD);
                    createAssignmentRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createAssignmentRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createAssignmentRec);
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Probation End Date Change Error', e);
                }
            }

            function updateCustomRecordAssignmentChange(emp_id, startDate, AssignmentCategory, AssignmentCategory_OLD) {
                try {
                    var assignmentCategory_List = [' ', 'Full-time', 'Full-time regular', 'Full-time contract', 'Part Time-Regular', 'Part Time-Contract', 'Contractor', 'MS Full Time', 'MS InsVac Full-Time',
                        'MS InsVac Part-Time', 'MS Non BenVac Full-Time', 'MS Non BenVac Part Time', 'MS Part Time', ' ', 'Part Time', ' ', ' ', 'MS VAC – Full Time', 'MS VAC – Part Time', ' ', ' ', 'MS Nov Vac – Full time', 'MS Nov Vac – Part time'
                    ];
                    var search = nlapiSearchRecord('customrecord_fusion_update_track',
                        null, [
                            new nlobjSearchFilter('custrecord_field_name', null, 'is', 'Assignment'),
                            new nlobjSearchFilter('custrecord_employee_fu_update',
                                null, 'anyof', emp_id)
                        ], [new nlobjSearchColumn('custrecord_end_date_fu_update').setSort(true),
                            new nlobjSearchColumn('custrecord_start_date_fu_update')
                        ]);
                    if (search) {
                        var assignmentUpdateID = search[0].getId();
                        var updateFusionStDate = search[0].getValue('custrecord_start_date_fu_update');
                        updateFusionStDate = nlapiStringToDate(updateFusionStDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(startDate), -1);
                        if (updateFusionStDate > end_DATE) {
                            end_DATE = updateFusionStDate;
                        }
                        var loadRec = nlapiLoadRecord('customrecord_fusion_update_track', assignmentUpdateID);
                        loadRec.setFieldValue('custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        loadRec.setFieldValue('custrecord_new_value_fu_update', assignmentCategory_List[AssignmentCategory]);
                        loadRec.setFieldValue('custrecord_old_value_fu_update', assignmentCategory_List[AssignmentCategory_OLD]);
                        nlapiSubmitRecord(loadRec);
                        /*nlapiSubmitField('customrecord_fusion_update_track', assignmentUpdateID,
                                'custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        nlapiSubmitField('customrecord_fusion_update_track', assignmentUpdateID,
                                'custrecord_old_value_fu_update',AssignmentCategory_OLD );*/
                    }
                    var createAssignmentRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createAssignmentRec.setFieldValue('custrecord_field_name', 'Assignment');
                    createAssignmentRec.setFieldValue('custrecord_new_value_fu_update', assignmentCategory_List[AssignmentCategory]);
                    createAssignmentRec.setFieldValue('custrecord_old_value_fu_update', assignmentCategory_List[AssignmentCategory_OLD]);
                    createAssignmentRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createAssignmentRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var assignmentID = nlapiSubmitRecord(createAssignmentRec);

                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Assignment Change Error', e);
                }
            }

            function updateCustomRecordHourlyChange(emp_id, startDate, HourlySalaried, HourlySalaried_OLD) {
                try {
                    var search = nlapiSearchRecord('customrecord_fusion_update_track',
                        null, [
                            new nlobjSearchFilter('custrecord_field_name', null, 'is', 'Hourly'),
                            new nlobjSearchFilter('custrecord_employee_fu_update',
                                null, 'anyof', emp_id)
                        ], [new nlobjSearchColumn('custrecord_end_date_fu_update').setSort(true),
                            new nlobjSearchColumn('custrecord_start_date_fu_update')
                        ]);
                    if (search) {
                        var hourlyUpdateID = search[0].getId();
                        var updateFusionStDate = search[0].getValue('custrecord_start_date_fu_update');
                        updateFusionStDate = nlapiStringToDate(updateFusionStDate);
                        var end_DATE = nlapiAddDays(nlapiStringToDate(startDate), -1);
                        if (updateFusionStDate > end_DATE) {
                            end_DATE = updateFusionStDate;
                        }
                        var loadRec = nlapiLoadRecord('customrecord_fusion_update_track', hourlyUpdateID);
                        loadRec.setFieldValue('custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        loadRec.setFieldValue('custrecord_new_value_fu_update', HourlySalaried);
                        loadRec.setFieldValue('custrecord_old_value_fu_update', HourlySalaried_OLD);
                        nlapiSubmitRecord(loadRec);
                        /*nlapiSubmitField('customrecord_fusion_update_track', hourlyUpdateID,  
                                'custrecord_end_date_fu_update', nlapiDateToString(end_DATE));
                        nlapiSubmitField('customrecord_fusion_update_track', hourlyUpdateID,
                                'custrecord_old_value_fu_update',HourlySalaried_OLD );*/
                    }
                    var createHourlyRec = nlapiCreateRecord('customrecord_fusion_update_track');
                    createHourlyRec.setFieldValue('custrecord_field_name', 'Hourly');
                    createHourlyRec.setFieldValue('custrecord_new_value_fu_update', HourlySalaried);
                    createHourlyRec.setFieldValue('custrecord_old_value_fu_update', HourlySalaried_OLD);
                    createHourlyRec.setFieldValue('custrecord_start_date_fu_update', startDate);
                    createHourlyRec.setFieldValue('custrecord_employee_fu_update', emp_id);
                    var hourlyID = nlapiSubmitRecord(createHourlyRec);

                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Hourly Error', e);
                }
            }

            function updatePhysicalAddress(emp_id, xmlData) {
                try {
                    var physicalAddressNEW = nlapiSelectValue(xmlData,
                        '//Assignment/PhysicalWorkAddress');
                    var physicalAddressOLD = nlapiSelectValue(xmlData,
                        '//Assignment/PhysicalWorkAddress_OLD');
                    var startDatE = nlapiSelectValue(xmlData,
                        '//Assignment/AssignmentEffStartDate');
                    startDatE = formatDate(startDatE);

                    //endActivePhysicalAddress(emp_id,startDatE);

                    var createPhysicalAddress_rec = nlapiCreateRecord('customrecord_empployee_physical_address');
                    createPhysicalAddress_rec.setFieldValue('custrecord_physical_address_1', physicalAddressNEW);
                    createPhysicalAddress_rec.setFieldValue('custrecord_physical_employee', emp_id);

                    var physicalAdd_ID = nlapiSubmitRecord(createPhysicalAddress_rec);
                    nlapiLogExecution('debug', 'Physical Address creation', 'physicalAdd_ID:' + physicalAdd_ID);

                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Update Physical Address Error', e);
                }
            }

            function endActivePhysicalAddress(emp_id, startDatE) {
                try {
                    var search = nlapiSearchRecord('customrecord_empployee_physical_address',
                        null, [
                            new nlobjSearchFilter('isinactive', null, 'is', 'F'),
                            new nlobjSearchFilter('custrecord_physical_employee',
                                null, 'anyof', emp_id)
                        ]);

                    if (search) {

                        // Only one subtier record is editted
                        var physicalAddressID = search[0].getId();
                        var endDate_U = nlapiAddDays(nlapiStringToDate(startDatE), -1);
                        nlapiSubmitField('customrecord_empployee_physical_address', physicalAddressID,
                            'custrecord_stvd_end_date', endDate_U);
                    }
                } catch (e) {
                    nlapiLogExecution('DEBUG', 'Ending Physical Address Errror', e);
                }
            }

            function createPhysicalCustomRecord(employeeID, xmlData) {
                try {

                    var physicalAddress = nlapiSelectValue(xmlData,
                        '//Assignment/PhysicalWorkAddress');
                    var start_date = nlapiSelectValue(xmlData,
                        '//Assignment/AssignmentEffStartDate');
                    start_date = formatDate(start_date);

                    var createPhysicalAddress_rec = nlapiCreateRecord('customrecord_empployee_physical_address');
                    createPhysicalAddress_rec.setFieldValue('custrecord_physical_address_1', physicalAddress);
                    createPhysicalAddress_rec.setFieldValue('custrecord_physical_employee', employeeID);

                    var physicalAdd_ID = nlapiSubmitRecord(createPhysicalAddress_rec);
                    nlapiLogExecution('debug', 'Physical Address creation', 'physicalAdd_ID:' + physicalAdd_ID);
                } catch (e) {
                    nlapiLogExecution('error', 'Error in Physical Address creation', err);
                }
            }
            // Create a new level change record and end the last one
            function updateEmployeeLevelHistory(employeeId, type) {
                try {
                    var newLevel = nlapiLookupField('employee', employeeId,
                        'employeestatus');

                    if (type == 'create') {
                        var hireDate = nlapiLookupField('employee', employeeId, 'hiredate');
                        var rec = nlapiCreateRecord('customrecord_level_change_history');
                        rec.setFieldValue('custrecord_lch_employee', employeeId);
                        rec.setFieldValue('custrecord_lch_level', newLevel);
                        rec.setFieldValue('custrecord_lch_from_date', hireDate);
                        nlapiSubmitRecord(rec);
                        nlapiLogExecution('debug', 'Level Change History',
                            'new level change history created ' + employeeId);
                    } else if (type == 'edit') {
                        // get the running level change record
                        var changeSearch = nlapiSearchRecord(
                            'customrecord_level_change_history', null, [
                                new nlobjSearchFilter('custrecord_lch_employee',
                                    null, 'anyof', employeeId),
                                new nlobjSearchFilter('custrecord_lch_to_date',
                                    null, 'isempty')
                            ]);

                        var currentDate = new Date();
                        if (changeSearch) {
                            // update the running record to date as yesterday
                            var yesterday = nlapiAddDays(currentDate, -1);
                            nlapiSubmitField('customrecord_level_change_history',
                                changeSearch[0].getId(), 'custrecord_lch_to_date',
                                nlapiDateToString(yesterday, 'date'));
                            nlapiLogExecution('debug', 'Level Change History',
                                'updated running entry to new date ' + employeeId);
                        } else {
                            nlapiLogExecution('debug', 'Level Change History',
                                'no previous entry ' + employeeId);
                        }

                        // create a new entry
                        var rec = nlapiCreateRecord('customrecord_level_change_history');
                        rec.setFieldValue('custrecord_lch_employee', employeeId);
                        rec.setFieldValue('custrecord_lch_level', newLevel);
                        rec.setFieldValue('custrecord_lch_from_date', nlapiDateToString(
                            currentDate, 'date'));
                        nlapiSubmitRecord(rec);
                        nlapiLogExecution('debug', 'Level Change History',
                            'new level change history created ' + employeeId);

                    } else if (type == 'terminate') {
                        var lwd = nlapiLookupField('employee', employeeId, 'custentity_lwd');

                        // get the running level change record
                        var changeSearch = nlapiSearchRecord(
                            'customrecord_level_change_history', null, [
                                new nlobjSearchFilter('custrecord_lch_employee',
                                    null, 'anyof', employeeId),
                                new nlobjSearchFilter('custrecord_lch_to_date',
                                    null, 'isempty')
                            ]);
                        if (changeSearch) {
                            // update the running record to date as LWD
                            nlapiSubmitField('customrecord_level_change_history',
                                changeSearch[0].getId(), 'custrecord_lch_to_date', lwd);
                            nlapiLogExecution('debug', 'Level Change History',
                                'updated running entry to LWD ' + employeeId);
                        } else {
                            nlapiLogExecution('debug', 'Level Change History',
                                'no previous entry ' + employeeId);
                        }
                    } else {
                        nlapiLogExecution('debug', 'Level Change History', 'wrong type ' +
                            employeeId + " " + type);
                    }
                } catch (err) {
                    nlapiLogExecution('error', 'updateEmployeeLevelHistory', err);
                }
            }

            //Create Subtier Vendor data
            function createUpdateSubtierRecord(employeeId, xmlData, vendorId, referral_Vendor_ID) {
                try {
                    var startDate = '';
                    var personType = nlapiSelectValues(xmlData, '//Employee/PersonType');
                    var referral_Vendor = nlapiSelectValue(xmlData, '//AssignmentHT/AssignmentData/Assignment/ReferralVendor');
                    /*if(personType[0] !='CWK'){
                    	return;
                    }*/
                    var oTRate_Value = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="OT Rate"]/ElementValue');
                    var sTRate_Value = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Hourly Salary"]/ElementValue');
                    var payRate_Value = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues[ElementName="Pay Rate"]/ElementValue');

                    nlapiLogExecution('debug', 'Update Subtier Rec Vendor ID', 'Vendor ID:' + vendorId);
                    nlapiLogExecution('debug', 'Update Subtier Rec Emp ID', 'Emp ID:' + employeeId);
                    if (vendorId) {
                        var vendor_LookUp = nlapiLookupField('vendor', vendorId, ['terms']);
                        var payment_Terms = vendor_LookUp.terms;
                    }
                    if (employeeId) {
                        var emp_LookUp = nlapiLookupField('employee', employeeId, ['subsidiary']);
                        var emp_sub = emp_LookUp.subsidiary;
                    }

                    var results = nlapiSearchRecord('customrecord_subtier_vendor_data', null, [
                        new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', employeeId),
                        new nlobjSearchFilter('custrecord_stvd_vendor', null, 'anyof', vendorId)
                    ], [
                        new nlobjSearchColumn('internalid'),
                        new nlobjSearchColumn('custrecord_stvd_start_date'),
                        new nlobjSearchColumn('custrecord_stvd_end_date')
                    ]);
                    if (_logValidation(results)) {
                        return;
                    }
                    //Finalize

                    var subtierEntryName = nlapiSelectValues(xmlData,
                        '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementName');


                    var subtierEntryRate = nlapiSelectValues(xmlData,
                        '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementValue');
                    var subtierEntryStartDate = nlapiSelectValue(xmlData, '//ElementEntriesHT/ElementEntriesData/ElementEntryValues/ElementStartDate');
                    var hire_date = nlapiSelectValue(xmlData, '//Employee/HireDate');
                    if (subtierEntryStartDate) {
                        startDate = subtierEntryStartDate;
                    } else {
                        startDate = hire_date;
                    }
                    startDate = formatDate(startDate);

                    //var subtierEntryStartDate = '2016-08-04T00:00:00.000Z';
                    //subtierEntryStartDate = formatDate(subtierEntryStartDate);
                    var vendor_Name = nlapiSelectValues(xmlData, '//AssignmentHT/AssignmentData/Assignment/VendorName');
                    //var referral_Vendor_R = nlapiSelectValues(xmlData,'//AssignmentHT/AssignmentData/Assignment/ReferralVendor');


                    nlapiLogExecution('debug', 'Vendor Name & vendorId & personType:', vendor_Name[0] + ' ' + vendorId + ' ' + personType[0]);
                    //TEST DATA


                    //end subtier record
                    //endActiveSubtierRecord(employeeId,
                    //        formatDate(subtierEntryStartDate[i]));


                    //Create Subtier Record
                    var newSubtierRecord = nlapiCreateRecord('customrecord_subtier_vendor_data');
                    newSubtierRecord.setFieldValue('custrecord_stvd_contractor',
                        employeeId);
                    newSubtierRecord.setFieldValue('custrecord_stvd_vendor',
                        vendorId);


                    newSubtierRecord.setFieldValue('custrecord_stvd_start_date',
                        startDate);



                    //For US
                    if (emp_sub == 2) {
                        //Check for OT Rate
                        for (var k = 0; k < subtierEntryName.length; k++) {
                            //Check for Rate Type and update pay rate respectively
                            if (subtierEntryName[k] == 'Hourly Salary') {
                                //st pay rate
                                newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                    parseFloat(sTRate_Value));
                                //ot pay rate

                            } else if (subtierEntryName[k] == 'OT Rate') {

                                newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                    parseFloat(oTRate_Value));
                            }
                        }
                    }

                    //FOR India
                    if (emp_sub == 3) {
                        for (var k = 0; k < subtierEntryName.length; k++) {
                            //Check for Rate Type and update pay rate respectively
                            if (subtierEntryName[k] == 'Pay Rate') {
                                //st pay rate
                                newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                                    parseFloat(payRate_Value));
                                //ot pay rate

                            } else if (subtierEntryName[k] == 'OT Rate') {

                                newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                                    parseFloat(oTRate_Value));
                            }
                        }

                    }
                    /*else{
                    	//ot pay rate
                    	newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
                    	        parseFloat(subTier_Rate));
                    	newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
                    	        parseFloat(0.0));
                    	}*/
                    if (_logValidation(referral_Vendor_ID)) {
                        newSubtierRecord.setFieldValue('custrecord_stvd_vendor_type', 2);
                    } else {
                        newSubtierRecord.setFieldValue('custrecord_stvd_vendor_type', 1);
                    }

                    //Update 
                    newSubtierRecord.setFieldValue('custrecord_stvd_payment_terms',
                        payment_Terms);
                    newSubtierRecord.setFieldValue('custrecord_stvd_subsidiary',
                        nlapiLookupField('employee', employeeId, 'subsidiary'));

                    //Maonthly
                    if (emp_sub == 3) {
                        newSubtierRecord.setFieldValue('custrecord_stvd_rate_type',
                            3);
                        startDate = nlapiStringToDate(startDate);
                        startDate = nlapiAddMonths(startDate, 6);
                        newSubtierRecord.setFieldValue('custrecord_stvd_end_date',
                            nlapiDateToString(startDate));

                    } //Hourly
                    else {
                        newSubtierRecord.setFieldValue('custrecord_stvd_rate_type',
                            1);
                        newSubtierRecord.setFieldValue('custrecord_stvd_end_date',
                            '12/31/2018');
                    }

                    var newSubtierRecordId = nlapiSubmitRecord(newSubtierRecord, false, true);
                    nlapiLogExecution('DEBUG', 'newSubtierRecordId', newSubtierRecordId);


                    /*	//If referral Vendor Exists	
		if(_logValidation(referral_Vendor_ID)){
			if(referral_Vendor_ID){
				var vendor_LookUp_R = nlapiLookupField('vendor',referral_Vendor_ID, ['terms']);
				var payment_Terms_R = vendor_LookUp_R.terms;
				}
			var newSubtierRecord_R = nlapiCreateRecord('customrecord_subtier_vendor_data');
			newSubtierRecord_R.setFieldValue('custrecord_stvd_contractor',
			        employeeId);
			newSubtierRecord_R.setFieldValue('custrecord_stvd_vendor',
					referral_Vendor_ID);
			
		
			newSubtierRecord_R.setFieldValue('custrecord_stvd_start_date',
			        formatDate(subtierEntryStartDate));
			
			newSubtierRecord_R.setFieldValue('custrecord_stvd_end_date',
	        '12/31/2017');
			
			//For US
			
			//Check for OT Rate
			
			//st pay rate
			newSubtierRecord_R.setFieldValue('custrecord_stvd_st_pay_rate',
	        parseFloat(0.0));
			//ot pay rate
			
			
			newSubtierRecord_R.setFieldValue('custrecord_stvd_ot_pay_rate',
			        parseFloat(0.0));
					
			
			
		
			else{
				//ot pay rate
				newSubtierRecord.setFieldValue('custrecord_stvd_ot_pay_rate',
				        parseFloat(subTier_Rate));
				newSubtierRecord.setFieldValue('custrecord_stvd_st_pay_rate',
				        parseFloat(0.0));
				}  
			
				newSubtierRecord_R.setFieldValue('custrecord_stvd_vendor_type',2);
			
			
			//Update 
			newSubtierRecord_R.setFieldValue('custrecord_stvd_payment_terms',
					payment_Terms_R);
			newSubtierRecord_R.setFieldValue('custrecord_stvd_subsidiary',
	        nlapiLookupField('employee', employeeId, 'subsidiary'));
			
			//Maonthly
			if(emp_sub == 3){
				newSubtierRecord_R.setFieldValue('custrecord_stvd_rate_type',
				        3);
				
			} //Hourly
			else{
				newSubtierRecord_R.setFieldValue('custrecord_stvd_rate_type',
				        1);
			}
			
			var newSubtierRecordId = nlapiSubmitRecord(newSubtierRecord_R,false,true);
			nlapiLogExecution('DEBUG', 'newSubtierRecordId', newSubtierRecordId); 
		} */

                } catch (err) {
                    nlapiLogExecution('ERROR', 'createUpdateSubtierRecord', err);
                }
            }

            function endActiveSubtierRecord(employeeId, newStartDate) {
                try {
                    var search = nlapiSearchRecord('customrecord_subtier_vendor_data',
                        null, [
                            new nlobjSearchFilter('custrecord_stvd_end_date', null,
                                'onorafter', newStartDate),
                            new nlobjSearchFilter('isinactive', null, 'is', 'F'),
                            new nlobjSearchFilter('custrecord_stvd_contractor',
                                null, 'anyof', employeeId)
                        ]);

                    if (search) {

                        // Only one subtier record is editted
                        var subtierId = search[0].getId();
                        nlapiSubmitField('customrecord_subtier_vendor_data', subtierId,
                            'custrecord_stvd_end_date', nlapiAddDays(
                                nlapiStringToDate(newStartDate), -1));
                    }
                } catch (err) {
                    nlapiLogExecution('ERROR', 'endActiveSubtierRecord', err);
                }
            }

            function getAllFusionFieldMapping(type) {

                var filters = Array();
                filters.push(new nlobjSearchFilter('custrecordcustrecord_fi_hire_update_type', null, 'is', type));
                //filters.push(new nlobjSearchFilter('isinactive',null, 'is', 'F'));
                return nlapiSearchRecord(
                    'customrecord_fusion_xml_to_field_name',
                    null, filters, [new nlobjSearchColumn('custrecord_fi_field_type'),
                        new nlobjSearchColumn('custrecord_fi_xml_tag_name'),
                        new nlobjSearchColumn('custrecord_fi_list_type'),
                        new nlobjSearchColumn('custrecord_fi_netsuite_field_name')
                    ]);
            }

            function getDropDownData() {
                return nlapiSearchRecord('customrecord_fi_drop_down_mapping', null, [new nlobjSearchFilter('isinactive', null, 'is', 'F')], [
                    new nlobjSearchColumn('custrecord_fi_fusion_value'),
                    new nlobjSearchColumn('custrecord_fi_dd_list_type'),
                    new nlobjSearchColumn('custrecord_fi_netsuite_id')
                ]);
            }

            function _logValidation(value) {
                if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
                    return true;
                } else {
                    return false;
                }
            }

            function formatDate(fusionDate) {

                if (fusionDate) {
                    // fusionDate = "2016-07-14T00:00:00.000Z";
                    var datePart = fusionDate.split('T')[0];
                    var dateSplit = datePart.split('-');


                    // MM/DD/ YYYY
                    var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
                        dateSplit[0];
                    return netsuiteDateString;
                }
                return "";
            }