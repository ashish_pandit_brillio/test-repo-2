// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_getSkills.js
	Author      : ASHISH PANDIT
	Date        : 08 JAN 2018
    Description : Suitelet to get skills according to Skill Family selected 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


function suiteletGetSkills(request, response){
	try
	{
		var i_skillFamily = request.getParameter('i_skill_family');   
		nlapiLogExecution('DEBUG','i_skillFamily',i_skillFamily);
		
		var arrSkills = [];
		var searchResult = getSkills(i_skillFamily);
		if(searchResult){
			for (var int = 0; int < searchResult.length; int++) {
				var dataOut = {};
				dataOut.id = searchResult[int].getValue('custrecord_emp_primary_skill');
				dataOut.name = searchResult[int].getText('custrecord_emp_primary_skill');
				arrSkills.push(dataOut);
			}
		}
		response.write(JSON.stringify(arrSkills));
	}
	catch(error)
	{
		nlapiLogExecution('Debug','error ',error);
	}
}

function getSkills(i_skillFamily) {
	try {
			var skill_filter = 
		        [
		            [ 'custrecord_employee_skil_family', 'is', i_skillFamily],               
		              'and',
					[ 'isinactive', 'is', 'F' ] 
				];

		var skill_search_results = searchRecord('customrecord_employee_skills', null, skill_filter,
		        [ 
					new nlobjSearchColumn("custrecord_emp_primary_skill"),
					new nlobjSearchColumn("internalid")
				]);

		nlapiLogExecution('debug', 'Results count',skill_search_results.length);

		if (skill_search_results.length == 0) {
			throw "You don't have any Primary Skills under Family Selected.";
		}
	
		return skill_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
}

/*******************************SEARCH RECORD CODE**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
/*********************************************************************************************/