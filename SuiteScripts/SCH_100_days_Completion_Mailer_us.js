/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         17th October 2017             Manikandan         Send 60 days completion mailer to the Employees scheduled at 9 a.m.
 */

function Main(){
try{

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');
columns[2]=new nlobjSearchColumn('email','custentity_reportingmanager');
//columns[3]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');

// search for employee with 100 days of service as of today

var emp_search = nlapiSearchRecord('employee', 'customsearch_60_days_completion_mailer_u', null,columns);
  //var emp_search = nlapiSearchRecord('employee', 'customsearch_60_days_completion_mailer_2', null,columns);

nlapiLogExecution('debug', 'emp_search.length',emp_search.length);

if(isNotEmpty(emp_search)){
	
	for(var i=0;i<emp_search.length;i++){
		
		// get the HR BP email id
		//var hr_bp_id = //emp_search[i].getValue('custrecord_hrbusinesspartner','department');
		//var hr_bp_email = null;
		
		//if(isNotEmpty(hr_bp_id)){
			//hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
			//hr_bp_firstname = nlapiLookupField('employee',hr_bp_id, 'firstname');
			//hr_bp_lastname = nlapiLookupField('employee',hr_bp_id, 'lastname');
		//}
		
		sendServiceMails(
			emp_search[i].getValue('firstname'),
			emp_search[i].getValue('email'),
			//hr_bp_email,
			emp_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, email, emp_id){
	
	try{
		var mailTemplate = serviceTemplate(firstName,emp_id);		
		nlapiLogExecution('debug', 'chekpoint',email);
      var employe=nlapiLoadRecord('employee',emp_id);
		employe.setFieldValue('custentity10','T');
		nlapiSubmitRecord(employe);
		nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,['paulina.b@BRILLIO.COM','unni.kondathil@brillio.com'],null, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendServiceMails', err);
throw err;
     }
}

function serviceTemplate(firstName,emp_id) {
	nlapiLogExecution('audit','emp_id',emp_id);  
var feedbackFormUrl='https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1480&deploy=1&compid=3883006&h=bc90624699eafe3d9662';
	feedbackFormUrl +='&custparam_employee_id='+emp_id;
	
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
//htmltext += '&nbsp';
 
htmltext += '<p>Congratulations on completion of 100 days with us.</p>';
//htmltext += &nbsp;

htmltext += '<p>Please click on <a href ="'+feedbackFormUrl+'" >Turning 100</a>to complete the survey.</p>';
htmltext += '<p>Request you to spare sometime from your busy schedule and provide us with your valuable feedback at the earliest.</p>';
htmltext += '<p>The motive of this survey is to understand your expectations better.</p>';

//htmltext += '<p>You can reach out to'+" "+ hr_bp_firstname+" "+hr_bp_lastname +" " +'at'+ hr_bp_email+'</p>';
htmltext += '<p>Here’s wishing you a very successful year ahead and all the best for your future endeavors and contributions to Brillio Technologies Pvt. Ltd.</p>';

htmltext += '<p>Thanks & Regards,</p>';
htmltext += '<p>Team HR</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Congratulations on completion of 100 days in Brillio!!"      
    };
}

