/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/runtime'],
    function (obj_Record, obj_Search, obj_Runtime) {
        function Send_headcount_data(obj_Request) {
            try {
                log.debug('requestBody ==', JSON.stringify(obj_Request));
                var obj_Usage_Context = obj_Runtime.getCurrentScript();
                var s_Request_Type = obj_Request.RequestType;
                log.debug('s_Request_Type ==', s_Request_Type);
                var s_Request_Data = obj_Request.RequestData;
                log.debug('s_Request_Data ==', s_Request_Data);
                var obj_Response_Return = {};
                if (s_Request_Data == 'DAILYHEADCOUNT') {
                    obj_Response_Return = get_dailyHeadCount_Data(obj_Search);
                }
            }
            catch (s_Exception) {
                log.debug('s_Exception==', s_Exception);
            } //// 
            log.debug('obj_Response_Return==', JSON.stringify(obj_Response_Return));
            log.debug('Final Remaining Usage  ==', obj_Usage_Context.getRemainingUsage());
            return obj_Response_Return;
        } ///// Function Send_headcount_data


        return {
            post: Send_headcount_data
        };

    }); //// End of Main Function
/****************** Supporting Functions ***********************************/
function get_dailyHeadCount_Data(obj_Search) {
    try {
        var resourceallocationSearchObj = obj_Search.create({
            type: "resourceallocation",
            filters:
                [
                    ["enddate", "notbefore", "today"],
                    "AND",
                    ["employee.custentity_implementationteam", "is", "F"],
                    "AND",
                    ["employee.custentity_employee_inactive", "is", "F"],
                    "AND",
                    ["startdate", "notafter", "today"]
                ],
            columns:
                [
                    obj_Search.createColumn({
                        name: "custentity_emp_function",
                        join: "employee",
                        label: "Function"
                    }),
                    obj_Search.createColumn({
                        name: "subsidiarynohierarchy",
                        join: "employee",
                        label: "Entity"
                    }),
                    obj_Search.createColumn({
                        name: "email",
                        join: "employee",
                        label: "Email"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_empid",
                        join: "employee",
                        label: "Solution ID"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_incid",
                        join: "employee",
                        label: "Inc ID"
                    }),
                    obj_Search.createColumn({ name: "resource", label: "Resource Name" }),
                    obj_Search.createColumn({
                        name: "custentity_otg_app",
                        join: "employee",
                        label: "OTG APP"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_fusion_empid",
                        join: "employee",
                        label: "Fusion ID"
                    }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "CASE WHEN INSTR({employee.department} , ' : ', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , ' : ', 1)) ELSE {employee.department} END",
                        label: "Employee Department"
                    }),
                    obj_Search.createColumn({
                        name: "departmentnohierarchy",
                        join: "employee",
                        label: "Employee Sub Department"
                    }),
                    obj_Search.createColumn({
                        name: "employeestatus",
                        join: "employee",
                        label: "Level"
                    }),
                    obj_Search.createColumn({
                        name: "title",
                        join: "employee",
                        label: "Designation"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_vertical",
                        join: "job",
                        label: "Customer Vertical"
                    }),
                    obj_Search.createColumn({
                        name: "altname",
                        join: "customer",
                        label: "Customer Name"
                    }),
                    obj_Search.createColumn({
                        name: "territory",
                        join: "customer",
                        label: "Customer Territory"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_endcustomer",
                        join: "job",
                        label: "End Customer"
                    }),
                    obj_Search.createColumn({
                        name: "entityid",
                        join: "job",
                        label: "Project ID"
                    }),
                    obj_Search.createColumn({
                        name: "altname",
                        join: "job",
                        label: "Project Name"
                    }),
                    obj_Search.createColumn({
                        name: "jobtype",
                        join: "job",
                        label: "Project Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_oldrefnumber",
                        join: "job",
                        label: "Old Project ID"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_practice",
                        join: "job",
                        label: "Executing Practice"
                    }),
                    obj_Search.createColumn({ name: "custeventrbillable", label: "Resource Billable" }),
                    obj_Search.createColumn({
                        name: "formulatext",
                        formula: "case when {custevent_ra_is_shadow} = 'T' then 'Shadow' else ' ' end",
                        label: "Shadow Resource"
                    }),
                    obj_Search.createColumn({ name: "custevent4", label: "Onsite/Offsite" }),
                    obj_Search.createColumn({ name: "startdate", label: "Allocation Start Date" }),
                    obj_Search.createColumn({ name: "enddate", label: "Allocation End Date" }),
                    obj_Search.createColumn({ name: "numberhours", label: "Number of Hours" }),
                    obj_Search.createColumn({ name: "percentoftime", label: "Percentage of Time" }),
                    obj_Search.createColumn({ name: "custeventbstartdate", label: "Billing Start Date" }),
                    obj_Search.createColumn({ name: "custeventbenddate", label: "Billing End Date" }),
                    obj_Search.createColumn({
                        name: "jobbillingtype",
                        join: "job",
                        label: "Billing Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_t_and_m_monthly",
                        join: "job",
                        label: "Is T&M Monthly"
                    }),
                    obj_Search.createColumn({ name: "custeventwlocation", label: "Work State" }),
                    obj_Search.createColumn({ name: "custevent_workcityra", label: "Work City" }),
                    obj_Search.createColumn({
                        name: "location",
                        join: "employee",
                        label: "Employee Location"
                    }),
                    obj_Search.createColumn({
                        name: "visatype",
                        join: "employee",
                        label: "Visa Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_projectmanager",
                        join: "job",
                        label: "Project Manager"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_deliverymanager",
                        join: "job",
                        label: "Delivery Manager"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_reportingmanager",
                        join: "employee",
                        label: "Reporting Manager"
                    }),
                    obj_Search.createColumn({
                        name: "employeetype",
                        join: "employee",
                        label: "Salaried/Hourly"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_employeetype",
                        join: "employee",
                        label: "Person Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_employeetype",
                        join: "employee",
                        label: "Employee Type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_emp_category",
                        join: "employee",
                        label: "Employment Category"
                    }),
                    obj_Search.createColumn({
                        name: "hiredate",
                        join: "employee",
                        label: "Hire Date"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_actual_hire_date",
                        join: "employee",
                        label: "Enterprise Hire Date"
                    }),
                    obj_Search.createColumn({
                        name: "approver",
                        join: "employee",
                        label: "Expense Approver"
                    }),
                    obj_Search.createColumn({
                        name: "timeapprover",
                        join: "employee",
                        label: "Time Approver"
                    }),
                    obj_Search.createColumn({ name: "custevent3", label: "Bill Rate" }),
                    obj_Search.createColumn({ name: "custevent_otrate", label: "OT Rate" }),
                    obj_Search.createColumn({ name: "custevent_monthly_rate", label: "Monthly Rate" }),
                    obj_Search.createColumn({ name: "notes", label: "Notes" }),
                    obj_Search.createColumn({ name: "custevent_allocation_status", label: "Allocation Status" }),
                    obj_Search.createColumn({ name: "custevent_ra_last_modified_by", label: "Last Modified By" }),
                    obj_Search.createColumn({ name: "custevent_ra_last_modified_date", label: "Last Modified Date" }),
                    obj_Search.createColumn({
                        name: "altemail",
                        join: "resource",
                        label: "Alt. Email"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_otheremailid",
                        join: "resource",
                        label: "Other Email ID"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_personalemailid",
                        join: "resource",
                        label: "Personal Email ID"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_employee_inactive",
                        join: "employee",
                        label: "Employee Inactive"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_lwd",
                        join: "employee",
                        label: "Last Working Date"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_project_allocation_category",
                        join: "job",
                        label: "Project Allocation Category"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_region",
                        join: "job",
                        label: "Project Region"
                    }),
                    obj_Search.createColumn({ name: "custevent_monthly_rate", label: "Monthly rate" }),
                    obj_Search.createColumn({
                        name: "custentity_employee_primary_skill",
                        join: "employee",
                        label: "Primary Skills"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_employee_secondary_skill",
                        join: "employee",
                        label: "Secondary skills"
                    }),
                    obj_Search.createColumn({
                        name: "internalid",
                        join: "employee",
                        label: "Employee Internal ID"
                    }),
                    obj_Search.createColumn({
                        name: "gender",
                        join: "employee",
                        label: "Gender"
                    }),
                    obj_Search.createColumn({ name: "custevent_gstworklocation", label: "GST location" }),
                    obj_Search.createColumn({
                        name: "custentity_region",
                        join: "customer",
                        label: "Region"
                    }),
                    obj_Search.createColumn({
                        name: "jobbillingtype",
                        join: "job",
                        label: "Billing type"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_account_delivery_owner",
                        join: "customer",
                        label: "Account Delivery Owner"
                    }),
                    obj_Search.createColumn({
                        name: "custentity_clientpartner",
                        join: "job",
                        label: "Client Partner"
                    })
                ]
        });
        var searchResultCount = resourceallocationSearchObj.runPaged().count;
        log.debug("resourceallocationSearchObj result count", searchResultCount);
        var myResults = getAllResults(resourceallocationSearchObj);
        var arr_headCount_json = [];
        var rate_temp = 0;
        var cols = resourceallocationSearchObj.columns;
        myResults.forEach(function (result) {
            rate_temp = result.getValue({ name: "custevent_otrate", label: "OT Rate" }),
            log.debug("rate_temp",rate_temp);
            //log.debug("rate_temp type",type(rate_temp));
            if(rate_temp>0){
               log.debug("rate_temp inside if");
               rate_temp = rate_temp
            }
            else{
               log.debug("rate_temp inside else");
               rate_temp = "0.00"
            }
            var obj_json_Container = {}
            obj_json_Container = {
                "Function":result.getText({
                    name: "custentity_emp_function",
                    join: "employee"
                }),
                "Subsidiary":result.getValue({
                    name: "subsidiarynohierarchy",
                    join: "employee"
                }),
                "Email":result.getValue({
                    name: "email",
                    join: "employee"
                }),
                "Solution_ID":result.getValue({
                    name: "custentity_empid",
                    join: "employee"
                }),
                "Inc_ID":result.getValue({
                    name: "custentity_incid",
                    join: "employee",
                }),
                "Resource_Name":result.getValue({ name: "resource", label: "Resource Name" }),
                "OTG_APP":result.getValue({
                    name: "custentity_otg_app",
                    join: "employee",
                    label: "OTG APP"
                }),
                "Fusion_ID":result.getValue({
                    name: "custentity_fusion_empid",
                    join: "employee",
                    label: "Fusion ID"
                }),
                "Employee_Practice":result.getValue(cols[8]),
                "Employee_Sub_Practice":result.getValue({
                    name: "departmentnohierarchy",
                    join: "employee",
                    label: "Employee Sub Department"
                }),
                "Level":result.getText({
                    name: "employeestatus",
                    join: "employee",
                    label: "Level"
                }),
                "Designation":result.getValue({
                    name: "title",
                    join: "employee",
                    label: "Designation"
                }),
                "Customer_Vertical":result.getText({
                    name: "custentity_vertical",
                    join: "job",
                    label: "Customer Vertical"
                }),
                "Customer_Name":result.getValue({
                    name: "altname",
                    join: "customer",
                    label: "Customer Name"
                }),
                "Customer_Territory":result.getText({
                    name: "territory",
                    join: "customer",
                    label: "Customer Territory"
                }),
                "End_Customer":result.getValue({
                    name: "custentity_endcustomer",
                    join: "job",
                    label: "End Customer"
                }),
                "Project_ID":result.getValue({
                    name: "entityid",
                    join: "job",
                    label: "Project ID"
                }),
                "Project_Name":result.getValue({
                    name: "altname",
                    join: "job",
                    label: "Project Name"
                }),
                "Project_Type":result.getText({
                    name: "jobtype",
                    join: "job",
                    label: "Project Type"
                }),
                "Old_Project_ID":result.getValue({
                    name: "custentity_oldrefnumber",
                    join: "job",
                    label: "Old Project ID"
                }),
                "Executing_Practice":result.getValue({
                    name: "custentity_practice",
                    join: "job",
                    label: "Executing Practice"
                }),
                "Resource Billable":result.getValue({ name: "custeventrbillable", label: "Resource Billable" }),
                "Shadow Resource":result.getValue({
                    name: "formulatext",
                    formula: "case when {custevent_ra_is_shadow} = 'T' then 'Shadow' else ' ' end",
                    label: "Shadow Resource"
                }),
                "Onsite/Offsite":result.getText({ name: "custevent4", label: "Onsite/Offsite" }),
                "Allocation_Start_Date":result.getValue({ name: "startdate", label: "Allocation Start Date" }),
                "Allocation_End_Date":result.getValue({ name: "enddate", label: "Allocation End Date" }),
                "Hours":result.getValue({ name: "numberhours", label: "Number of Hours" }),
                "Percentage_of_Time":result.getValue({ name: "percentoftime", label: "Percentage of Time" }),
                "Billing_Start_Date":result.getValue({ name: "custeventbstartdate", label: "Billing Start Date" }),
                "Billing_End_Date":result.getValue({ name: "custeventbenddate", label: "Billing End Date" }),
                "Billing_Type":result.getText({
                    name: "jobbillingtype",
                    join: "job",
                    label: "Billing Type"
                }),
                "Is_T&M_Monthly":result.getValue({
                    name: "custentity_t_and_m_monthly",
                    join: "job",
                    label: "Is T&M Monthly"
                }),
                "Work_State":result.getText({ name: "custeventwlocation", label: "Work State" }),
                "Work_City":result.getValue({ name: "custevent_workcityra", label: "Work City" }),
                "Employee_Location":result.getValue({
                    name: "location",
                    join: "employee",
                    label: "Employee Location"
                }),
                "Visa_Type":result.getValue({
                    name: "visatype",
                    join: "employee",
                    label: "Visa Type"
                }),
                "Project_Manager":result.getValue({
                    name: "custentity_projectmanager",
                    join: "job",
                    label: "Project Manager"
                }),
                "Delivery_Manager":result.getValue({
                    name: "custentity_deliverymanager",
                    join: "job",
                    label: "Delivery Manager"
                }),
                "Reporting_Manager":result.getValue({
                    name: "custentity_reportingmanager",
                    join: "employee",
                    label: "Reporting Manager"
                }),
                "Salaried/Hourly":result.getText({
                    name: "employeetype",
                    join: "employee",
                    label: "Salaried/Hourly"
                }),
                "Person_Type":result.getText({
                    name: "custentity_employeetype",
                    join: "employee",
                    label: "Person Type"
                }),
                "Employee_Type":result.getText({
                    name: "custentity_employeetype",
                    join: "employee",
                    label: "Employee Type"
                }),
                "Employment_Category":result.getText({
                    name: "custentity_emp_category",
                    join: "employee",
                    label: "Employment Category"
                }),
                "Hire_Date":result.getValue({
                    name: "hiredate",
                    join: "employee",
                    label: "Hire Date"
                }),
                "Enterprise_Hire_Date":result.getValue({
                    name: "custentity_actual_hire_date",
                    join: "employee",
                    label: "Enterprise Hire Date"
                }),
                "Expense_Approver":result.getValue({
                    name: "approver",
                    join: "employee",
                    label: "Expense Approver"
                }),
                "Time_Approver":result.getValue({
                    name: "timeapprover",
                    join: "employee",
                    label: "Time Approver"
                }),
                "Bill_Rate":result.getValue({ name: "custevent3", label: "Bill Rate" }),
                "OT_Rate":rate_temp,
                "Monthly_Rate":result.getValue({ name: "custevent_monthly_rate", label: "Monthly Rate" }),
                "Notes":result.getValue({ name: "notes", label: "Notes" }),
                "Allocation_Status":result.getText({ name: "custevent_allocation_status", label: "Allocation Status" }),
                "Last_Modified_By":result.getValue({ name: "custevent_ra_last_modified_by", label: "Last Modified By" }),
                "Last_Modified_Date":result.getValue({ name: "custevent_ra_last_modified_date", label: "Last Modified Date" }),
                "Alt_Email":result.getValue({
                    name: "altemail",
                    join: "resource",
                    label: "Alt. Email"
                }),
                "Other_Email_ID":result.getValue({
                    name: "custentity_otheremailid",
                    join: "resource",
                    label: "Other Email ID"
                }),
                "Personal_Email_ID":result.getValue({
                    name: "custentity_personalemailid",
                    join: "resource",
                    label: "Personal Email ID"
                }),
                "Employee_Inactive":result.getValue({
                    name: "custentity_employee_inactive",
                    join: "employee",
                    label: "Employee Inactive"
                }),
                "Last_Working_Date":result.getValue({
                    name: "custentity_lwd",
                    join: "employee",
                    label: "Last Working Date"
                }),
                "Project_Allocation_Category":result.getText({
                    name: "custentity_project_allocation_category",
                    join: "job",
                    label: "Project Allocation Category"
                }),
                "Project_Region":result.getValue({
                    name: "custentity_region",
                    join: "job",
                    label: "Project Region"
                }),
                "Monthly rate" :result.getValue({ name: "custevent_monthly_rate", label: "Monthly rate" }),
                "Primary_Skills":result.getValue({
                    name: "custentity_employee_primary_skill",
                    join: "employee",
                    label: "Primary Skills"
                }),
                "Secondary_Skills":result.getValue({
                    name: "custentity_employee_secondary_skill",
                    join: "employee",
                    label: "Secondary skills"
                }),
                "Employee_Internal_ID":result.getValue({
                    name: "internalid",
                    join: "employee",
                    label: "Employee Internal ID"
                }),
                "Gender":result.getText({
                    name: "gender",
                    join: "employee",
                    label: "Gender"
                }),
                "GST_location":result.getText({ name: "custevent_gstworklocation", label: "GST location" }),
                "Region":result.getValue({
                    name: "custentity_region",
                    join: "customer",
                    label: "Region"
                }),
                "Billing_type":result.getText({
                    name: "jobbillingtype",
                    join: "job",
                    label: "Billing type"
                }),
                "Account_Delivery_Owner":result.getValue({
                    name: "custentity_account_delivery_owner",
                    join: "customer",
                    label: "Account Delivery Owner"
                }),
                "Client_Partner":result.getValue({
                    name: "custentity_clientpartner",
                    join: "job",
                    label: "Client Partner"
                })
            };
            arr_headCount_json.push(obj_json_Container);
            return true;
        });
        return arr_headCount_json;
    } //// End of try
    catch (s_Exception) {
        log.debug('get_dailyHeadCount_Data s_Exception== ', s_Exception);
    } //// End of catch 
} ///// End of function get_dailyHeadCount_Data()


function getAllResults(s) {
    var result = s.run();
    var searchResults = [];
    var searchid = 0;
    do {
        var resultslice = result.getRange({ start: searchid, end: searchid + 1000 });
        resultslice.forEach(function (slice) {
            searchResults.push(slice);
            searchid++;
        }
        );
    } while (resultslice.length >= 1000);
    return searchResults;
}