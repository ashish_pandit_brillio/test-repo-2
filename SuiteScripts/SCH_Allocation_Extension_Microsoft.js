function update_allocation()
{
	try
	{
		var billing_from_date = nlapiStringToDate(nlapiDateToString(new Date('6/30/2018')));
		nlapiLogExecution('audit','billing_from_date:- '+billing_from_date);
		var filters_allocation = [ [ 'enddate', 'on', billing_from_date ], 'and',
									[ 'employee.custentity_employee_inactive', 'is', 'F'], 'and',
									[ 'project', 'anyof', parseInt(39750) ], 'and',
									[ 'employee.isinactive', 'is', 'F' ]
								  ];
									
		var a_columns_allocation = new Array();
		a_columns_allocation[0] = new nlobjSearchColumn('enddate','job');
		a_columns_allocation[1] = new nlobjSearchColumn('custeventbenddate');
									
		var a_allocatino_srch = nlapiSearchRecord('resourceallocation',null,filters_allocation,a_columns_allocation);
		if(a_allocatino_srch)
		{
			var s_project_end_date = a_allocatino_srch[0].getValue('enddate','job');
			
			/*var a_fld_id = new Array();
			a_fld_id[0] = 'enddate';
			a_fld_id[1] = 'custevent_allocation_status';
			a_fld_id[2] = 'custeventbenddate';
			
			var a_fld_values = new Array();
			a_fld_values[0] = s_project_end_date;
			a_fld_values[1] = 18;
			a_fld_values[2] = s_project_end_date;*/
			
			nlapiLogExecution('audit','project end date:- '+s_project_end_date, 'allocation len:- '+a_allocatino_srch.length);
			for(var i_srch_index=0; i_srch_index<a_allocatino_srch.length; i_srch_index++)
			{
				var i_allocation_id = a_allocatino_srch[i_srch_index].getId();
				if(i_allocation_id)
				{
					if(a_allocatino_srch[i_srch_index].getValue('custeventbenddate'))
					{
						nlapiSubmitField('resourceallocation',i_allocation_id,'custeventbenddate',s_project_end_date);
						nlapiLogExecution('audit','allocation id:- '+i_allocation_id,'line no:- '+i_srch_index);
					}	
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- '+err);
	}
}