/**
 * Module Description
 * Test
 * Version    Date            Author           Remarks
 * 1.00       02 Jan 2017     deepak.srinivas
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function validateDates_Subtier(){
try{

	var stDate = nlapiGetFieldValue('custrecord_stvd_start_date');
	var endDate = nlapiGetFieldValue('custrecord_stvd_end_date');
	var contractor = nlapiGetFieldValue('custrecord_stvd_contractor');
	stDate = nlapiStringToDate(stDate);
	endDate = nlapiStringToDate(endDate);
	
	
	//Search For the entry
	var filterExpression = Array();
	//Define search filter expression
	var filterExpression  = [
	            		[
	            			['custrecord_stvd_start_date', 'onorbefore', endDate],
	            			'OR' ,
	            			['custrecord_stvd_end_date', 'onorafter', stDate]
	            		],
						'AND',
	            		['isinactive', 'is', 'F'],
	            		'AND',
	            		['custrecord_stvd_vendor_type', 'anyof', parseInt(1)]
	            ];
	/*var filterExpression =   [ [ 'custrecord_stvd_start_date', 'within',stDate,endDate],
	               'or',
	               [ 'custrecord_stvd_end_date', 'within',stDate,endDate],
	               'and'
	               ['custrecord_stvd_contractor', 'is',contractor]
	               ] ;*/
	//filters.push(new nlobjSearchFilter('custrecord_stvd_start_date',null,'within',stDate,endDate));
	///filters.push(new nlobjSearchFilter('custrecord_stvd_contractor',null,'anyof',contractor));
	
	var cols =  Array();
	cols.push(new nlobjSearchColumn('internalid'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_start_date'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_end_date'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_contractor'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_vendor'));
	cols.push(new nlobjSearchColumn('custrecord_stvd_subsidiary'));
	
	var dataObj = {};
	var dataRow = [];
	var projectArray = [];
	var searchRes = nlapiSearchRecord('customrecord_subtier_vendor_data',null,filterExpression,cols);
	nlapiLogExecution('DEBUG','Subtier searchRes',searchRes.length);
	if(searchRes){
		for(var i=0;i<searchRes.length;i++){
			var id = searchRes[i].getValue('internalid');
			nlapiLogExecution('DEBUG','Subtier For Line',i);
			nlapiLogExecution('DEBUG','Subtier Rec ID',id);
			var st_date = searchRes[i].getValue('custrecord_stvd_start_date');
			nlapiLogExecution('DEBUG','Subtier st_date',st_date);
			var end_date = searchRes[i].getValue('custrecord_stvd_end_date');
			nlapiLogExecution('DEBUG','Subtier end_date',end_date);
			var s_contractor = searchRes[i].getText('custrecord_stvd_contractor');
			nlapiLogExecution('DEBUG','Subtier s_contractor',s_contractor);
			var s_vendor = searchRes[i].getText('custrecord_stvd_vendor');
			nlapiLogExecution('DEBUG','Subtier s_vendor',s_vendor);
		}
		alert('Please check the dates. There is already a record with same dates !!');
		return false;
	}

return true;
}   
catch(e){
	nlapiLogExecution('DEBUG','Validation Error',e);
	return false;
}
}

function fieldChange_Trigger(type, name, linenum)
{
try{
if(name == 'custrecord_stvd_end_date'){
var s_endDate = nlapiGetFieldValue('custrecord_stvd_end_date');
var d_endDate = nlapiStringToDate(s_endDate);
var s_startDate = nlapiGetFieldValue('custrecord_stvd_start_date');
var d_startDate = nlapiStringToDate(s_startDate);
if(d_endDate <= d_startDate && nlapiGetFieldValue('custrecord_stvd_end_date') != ''){
alert('End Date date cannot be lessthan Start Date');
nlapiSetFieldValue('custrecord_stvd_end_date', '')

}
}
return true;
}
catch(e){
nlapiLogExecution('DEBUG','Field Change Error',e);
	return false;
}
}