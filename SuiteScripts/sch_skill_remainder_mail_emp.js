/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Aug 2016     shruthi.l
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
	  try
	  {
	  	  //
		    /*
	   * Search Employees with creation date 3 days ago
	   * 
	   */
		  var emp_search = nlapiSearchRecord('employee',null,[
							["custentity_employee_inactive","is","F"],'AND', 
							["custentity_implementationteam","is","F"], 'AND', 
							["isinactive","is","F"],'AND', 
							["custentity_skill_updated","is","F"], 'AND', 
							["systemnotes.type","is","T"],'AND', 
							//["systemnotes.date","on","8/7/2018"]],
							["systemnotes.date","on","threedaysago"],'AND',
          			[["department","anyof","485","191"],'OR',["department.custrecord_parent_practice","anyof","492"]]], 
								[new nlobjSearchColumn("entityid").setSort(false),new nlobjSearchColumn("email"), 
								new nlobjSearchColumn("custentity_empid"), 
								new nlobjSearchColumn("custentity_reportingmanager"), 
								new nlobjSearchColumn('firstname'),
								new nlobjSearchColumn('email','custentity_reportingmanager'),
								new nlobjSearchColumn('internalid'),
								new nlobjSearchColumn("custentity_actual_hire_date")]);
		 
	
	   /*
	   * Search Employees created 5 days ago
	   * 
	   */
	  var emp_search2 = nlapiSearchRecord("employee",null,[
							["custentity_employee_inactive","is","F"],"AND", 
							["custentity_implementationteam","is","F"], "AND", 
							["isinactive","is","F"], "AND", 
							["custentity_skill_updated","is","F"], "AND", 
							["systemnotes.type","is","T"],"AND", 
							//["systemnotes.date","on","8/7/2018"]],
							["systemnotes.date","on","fivedaysago"],'AND',
     [["department","anyof","485","191"],"OR",
      ["department.custrecord_parent_practice","anyof","492"]]], 
								[new nlobjSearchColumn("entityid").setSort(false),new nlobjSearchColumn("email"), 
								new nlobjSearchColumn("custentity_empid"), 
								new nlobjSearchColumn("custentity_reportingmanager"), 
								new nlobjSearchColumn('firstname'),
								new nlobjSearchColumn('email','custentity_reportingmanager'),
								new nlobjSearchColumn('internalid'),
								new nlobjSearchColumn("custentity_actual_hire_date")]);

	 var rm_pendingSearch3 = nlapiSearchRecord("customrecord_employee_master_skill_data",null,[
							["systemnotes.type","is","T"], 
							"AND", 
							["systemnotes.date","on","threedaysago"], 
							"AND", 
							["custrecord_skill_status","anyof","1"]], 
							[new nlobjSearchColumn("entityid","custrecord_employee_skill_updated"), 
							new nlobjSearchColumn("internalid"),
							new nlobjSearchColumn("email","custrecord_employee_approver"), 
							new nlobjSearchColumn("custentity_reportingmanager","CUSTRECORD_EMPLOYEE_APPROVER",null)]);
	var rm_pendingSearch5 = nlapiSearchRecord("customrecord_employee_master_skill_data",null,[
							["systemnotes.type","is","T"], 
							"AND", 
							["systemnotes.date","on","fivedaysago"], 
							"AND", 
							["custrecord_skill_status","anyof","1"]], 
							[new nlobjSearchColumn("entityid","custrecord_employee_skill_updated"), 
							new nlobjSearchColumn("internalid"),
							new nlobjSearchColumn("email","custrecord_employee_approver"), 
							new nlobjSearchColumn("custentity_reportingmanager","CUSTRECORD_EMPLOYEE_APPROVER",null)]);
	  
	 if(isNotEmpty(emp_search))
	  {
		nlapiLogExecution('debug', 'emp_search.length',emp_search.length);
	  	for(var i=0;i<emp_search.length;i++)
	  	{
	  		var emp_email =	 emp_search[i].getValue('email');
	  		var rm_email = emp_search[i].getValue('email','custentity_reportingmanager');
	  	 		
	       sendServiceMails(
	  			emp_search[i].getValue('firstname'),
	  			rm_email,emp_email,
	  			emp_search[i].getValue('internalid'));
	  	}
	  }
	  
	  
	  if(isNotEmpty(emp_search2))
	  {
	  	nlapiLogExecution('debug', 'emp_search.length',emp_search2.length);
	  	for(var i=0;i<emp_search2.length;i++)
	  	{
			var emp_email =	 emp_search2[i].getValue('email');	 
			var rm_email = emp_search2[i].getValue('email','custentity_reportingmanager');
			sendServiceMails2(
	  			emp_search2[i].getValue('firstname'),
	  			rm_email,emp_email,  			
	  			emp_search2[i].getValue('internalid')
				);
	  	}
	  }
	   if(isNotEmpty(rm_pendingSearch3))
	  {
		nlapiLogExecution('debug', 'emp_search.length',rm_pendingSearch3.length);
	  	for(var i=0;i<rm_pendingSearch3.length;i++)
	  	{
	  		var rm_email = rm_pendingSearch3[i].getValue("email","custrecord_employee_approver");
			var emp_id = rm_pendingSearch3[i].getValue("entityid","custrecord_employee_skill_updated");
	  		var rm_reporting = rm_pendingSearch3[i].getValue("custentity_reportingmanager","CUSTRECORD_EMPLOYEE_APPROVER",null);
	  	 	var rm_reporting_mail = nlapiLookupField('employee',rm_reporting,'email');
			var rec_id= rm_pendingSearch3[i].getValue("internalid")
	       sendreportingMails(rm_email,rm_reporting_mail,rec_id,emp_id);
	  	}
	  }
	   if(isNotEmpty(rm_pendingSearch5))
	  {
		nlapiLogExecution('debug', 'emp_search.length',rm_pendingSearch5.length);
	  	for(var i=0;i<rm_pendingSearch5.length;i++)
	  	{
	  		var rm_email =	 rm_pendingSearch5[i].getValue("email","custrecord_employee_approver");
			var emp_id = rm_pendingSearch5[i].getValue("entityid","custrecord_employee_skill_updated");
	  		var rm_reporting = rm_pendingSearch5[i].getValue("custentity_reportingmanager","CUSTRECORD_EMPLOYEE_APPROVER",null);
	  	 	var rm_reporting_mail = nlapiLookupField('employee',rm_reporting,'email');
			var rec_id= rm_pendingSearch5[i].getValue("internalid")
	       sendreportingMails5(rm_email,rm_reporting_mail,rec_id,emp_id);
	  	}
	  }
	}
	  catch(err){
	  nlapiLogExecution('error', 'Main', err);
	  }
}

function sendServiceMails(firstName, reporting_mng_email,emp_mail, employee_internal_id)
{
	
	try{       
		var mailTemplate = serviceTemplate(firstName, reporting_mng_email, emp_mail, employee_internal_id);		
		nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(129799, emp_mail, mailTemplate.MailSubject, mailTemplate.MailBody,reporting_mng_email,null,{ entity : employee_internal_id},null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function sendServiceMails2(firstName,reporting_mng_email,emp_mail, employee_internal_id )
{
	
	try{       
		var mailTemplate = serviceTemplate2(firstName, reporting_mng_email, emp_mail, employee_internal_id);		
		nlapiLogExecution('debug', 'checkpoint',reporting_mng_email);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		var cc = new Array();
		cc.push(reporting_mng_email);
		cc.push('talentmanagement@brillio.com');
		cc.push('bhumika.sharma@brillio.com');
		nlapiSendEmail(129799, emp_mail, mailTemplate.MailSubject, mailTemplate.MailBody,cc,null,{ entity : employee_internal_id},null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}
function sendreportingMails(rm_email,rm_reporting_mail,rec_id,emp_id)
{
	
	try{       
		var mailTemplate = ReportingApprovalTemplate(rm_email, rm_reporting_mail, rec_id,emp_id);		
		nlapiLogExecution('debug', 'checkpoint',rm_reporting_mail);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		nlapiSendEmail(129799, rm_email, mailTemplate.MailSubject, mailTemplate.MailBody,rm_reporting_mail,null,{ customrecord_employee_master_skill_data : rec_id},null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}
function sendreportingMails5(rm_email,rm_reporting_mail,rec_id,emp_id)
{
	
	try{       
		var mailTemplate = ReportingApprovalTemplate(rm_email, rm_reporting_mail, rec_id,emp_id);		
		nlapiLogExecution('debug', 'checkpoint',rm_reporting_mail);
		nlapiLogExecution('debug', 'mailTemplate',mailTemplate.MailBody);
		var rec = new Array();
		rec['recordtype']='customrecord_employee_master_skill_data'; //script id of the custom record type
		rec['record'] = rec_id; 
		var cc = new Array();
		cc.push(rm_reporting_mail);
		cc.push('talentmanagement@brillio.com');
		cc.push('bhumika.sharma@brillio.com');
		nlapiSendEmail(129799, rm_email, mailTemplate.MailSubject, mailTemplate.MailBody,cc,null,rec,null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}
function serviceTemplate(firstName, reporting_mng_email, emp_mail,employee_internal_id) 
{
    
	var htmltext = ''; 
	
	htmltext += '<p>Dear Brillian,</p>'
	 + '<p><b> Request your help here to update the skill</b></p>'
	 + '<p>Please visit the below link to update your Skills and certification.</p>'
	 +'<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1657&deploy=1&empid=>Skills and certification</a>'
	 +  '<br/><p>Thanks and All The Best,<br/>' + 'Fulfillment Team</p>';
  

 
    return {
        MailBody : htmltext,
        MailSubject : "Gentle Reminder !!!!"      
    };
}


function serviceTemplate2(firstName, reporting_mng_email,emp_mail, employee_internal_id) 
{    
	var htmltext = ''; 

	htmltext += '<p>Dear Brillian,</p>'
	 + '<p><b> Request your help here to update the skill. These will help us to work closely on your next projects allocations. </b></p>'
	 + '<p>Please visit the below link to update your Skills and certification.</p>'
	 +'<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1657&deploy=1&empid=>Skills and certification</a>'
	 + '<br/><p>Thanks and All The Best,<br/>' + 'Fulfillment Team</p>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Gentle Reminder !!!!"      
    };
}
function ReportingApprovalTemplate(rm_email, rm_reporting_mail,rec_id,emp_id) 
{
    
	var strVar = ''; 
	strVar += '<p>Dear RM,</p>';
		strVar += '<p>'+emp_id+' who has recently joined your team has updated the skill field on the tool. Request you to kindly validate the same via accessing below mentioned link.';
		strVar += 'This activity will help in achieving following objectives:</p>';
			
		strVar += '<p>•	Determining Learning Plans for Brillians basis validated skills<br>';
		strVar += '<p>•	Redeployment of Brillian across Projects</p>';
	
			
		strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1664&deploy=1&empid=>Link to Approve Skill Record in Netsuite</a>';
			
		strVar += '<p>Regards,</p>';
		strVar += '<p>Fulfilment Team</p>';

 
    return {
        MailBody : strVar,
        MailSubject : "Gentle Reminder !!!!"      
    };
}