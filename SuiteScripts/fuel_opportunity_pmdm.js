/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Sep 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function uesNotificationOppPMDM(type){
	try {
		if (type == "create") {
			var method = "POST";
			var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var pmArray = recObj.getFieldValues("custrecord_sfdc_opp_pm");
			var dmArray = recObj.getFieldValues("custrecord_sfdc_opportunity_dm");
			var oppName = recObj.getFieldValue("custrecord_opportunity_name_sfdc");
			var oppId   = recObj.getFieldValue("custrecord_opportunity_id_sfdc");
			var accountInternalID = recObj.getFieldValue("custrecord_customer_internal_id_sfdc");
			var revenueStatus = recObj.getFieldText("custrecord_projection_status_sfdc");
			var pmEmails = [];
			var dmEmails = [];
			if(isArrayNotEmpty(pmArray)){
				for (var int = 0; int < pmArray.length; int++) {
					var array_element = pmArray[int];
					pmEmails.push(nlapiLookupField("employee", array_element, "email"))
				}
			}
			if(isArrayNotEmpty(dmArray)){
				for (var int = 0; int < dmArray.length; int++) {
					var array_element = dmArray[int];
					dmEmails.push(nlapiLookupField("employee", array_element, "email"))
				}
			}
          	var obj = {};
			obj.pmEmailId = pmEmails;
			obj.dmEmailId = dmEmails;
			obj.opportunityInternalId = nlapiGetRecordId();
			obj.name = oppName;
			obj.opportunityId = oppId;
			obj.accountInternalId = accountInternalID;
			obj.revenueStatus = revenueStatus;
			obj = JSON.stringify(obj);
          	var url = "https://fuelnode1.azurewebsites.net/opp_pm_dm";
          	var response = call_node(url,obj,method);
			nlapiLogExecution("DEBUG", "response : create :  ",  JSON.stringify(response));
		}
		if (type == "edit") {
			var method = "POST";
			var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			var oldRec = nlapiGetOldRecord();
			var oldpmArray = oldRec.getFieldValues("custrecord_sfdc_opp_pm");
			var olddmArray = oldRec.getFieldValues("custrecord_sfdc_opportunity_dm");
			var pmArray = recObj.getFieldValues("custrecord_sfdc_opp_pm");
			var dmArray = recObj.getFieldValues("custrecord_sfdc_opportunity_dm");
			var oppName = recObj.getFieldValue("custrecord_opportunity_name_sfdc");
			var oppId   = recObj.getFieldValue("custrecord_opportunity_id_sfdc");
			var accountInternalID = recObj.getFieldValue("custrecord_customer_internal_id_sfdc");
			var revenueStatus = recObj.getFieldText("custrecord_projection_status_sfdc");
			var pmEmails = [];
			var dmEmails = [];
			if(isArrayNotEmpty(pmArray)){
				for (var int = 0; int < pmArray.length; int++) {
					var array_element = pmArray[int];
					pmEmails.push(nlapiLookupField("employee", array_element, "email"))
				}
			}
			if(isArrayNotEmpty(dmArray)){
				for (var int = 0; int < dmArray.length; int++) {
					var array_element = dmArray[int];
					dmEmails.push(nlapiLookupField("employee", array_element, "email"))
				}
			}
			if(JSON.stringify(oldpmArray) != JSON.stringify(pmArray) || JSON.stringify(olddmArray) != JSON.stringify(dmArray)){
				var obj = {};
				obj.pmEmailId = pmEmails;
				obj.dmEmailId = dmEmails;
				obj.opportunityInternalId = nlapiGetRecordId();
				obj.name = oppName;
				obj.opportunityId = oppId;
				obj.accountInternalId = accountInternalID;
				obj.revenueStatus = revenueStatus;
              	obj = JSON.stringify(obj);
				var url = "https://fuelnode1.azurewebsites.net/opp_pm_dm";
                nlapiLogExecution("DEBUG", "obj : ", JSON.stringify(obj));
				var response = call_node(url,obj,method);
				nlapiLogExecution("DEBUG", "response : ", JSON.stringify(response));
			}
		}
	} catch (e) {
		// TODO: handle exception
		nlapiLogExecution("DEBUG", "Error in script : ", e);
	}
}
function beforeSubmitCheckFields(type)
{
	if (type == 'delete')
	{
		var body = {};
		var method = "DELETE";
		var url = "https://fuelnode1.azurewebsites.net/opp_pm_dm/opp_internal_id/"+nlapiGetRecordId();
		var response = call_node(url,body,method);
		nlapiLogExecution("DEBUG", "response Create Mode: ", JSON.stringify(response));
	}
}
