//Get post

function pending_allocation(request, response)
 {
	if (request.getMethod() == 'GET') 
	{
		
            var practice = 'department';
			
			//var import_practice;
			if(request.getParameter('import_practice')!=''&&(request.getParameter('import_practice')!= null ||request.getParameter('import_practice')!= undefined))
			{
				practice = request.getParameter('import_practice');
			}
		  //Create the form and add fields to it 
		  var form = nlapiCreateForm("Suitelet - Pending allocation from FUEL");		 
		 
		form.addField('custpage_practice','select','Practice','department').setDefaultValue(practice);
		 
		 var logged_in_mail = nlapiLookupField('employee', nlapiGetUser(), 'email');
		 
		
		 form.setScript('customscript_bo_pending_reason');
            var count = 0;
			
			var searchresults = nlapiSearchRecord("customrecord_frf_details",null,
			[
			   [[[["custrecord_frf_details_selected_emp","noneof","@NONE@"]],"AND",[["custrecord_frf_details_status_flag","anyof","3"],"OR",["custrecord_frf_details_open_close_status","anyof","3"]]],"OR",[[["custrecord_frf_details_external_hire","is","T"],"AND",["custrecord_frf_details_rrf_number.custrecord_taleo_ext_hire_status","is","Filled"]],"AND",[["custrecord_frf_details_status_flag","anyof","1"],"OR",["custrecord_frf_details_open_close_status","anyof","1"]]]], 
			   "AND", 
			   [["custrecord_frf_details_status","is","F"],"AND",["custrecord_frf_details_opp_id.custrecord_stage_sfdc","noneof","8","9"],"AND",["custrecord_frf_details_res_practice","anyof",practice]],
			   "AND",
			   ["custrecord_frf_details_bo_comments","noneof",7]
			], 
			[
			   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
			   new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
			   new nlobjSearchColumn("custrecord_frf_details_project"), 
			   new nlobjSearchColumn("custrecord_frf_details_account"),
				
			   new nlobjSearchColumn("custrecord_opportunity_id_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
			   
			   new nlobjSearchColumn("custrecord_frf_details_account"), 
			   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
			   new nlobjSearchColumn("custrecord_frf_details_res_location"),
			   new nlobjSearchColumn("custrecord_frf_details_bo_comments"),
			    new nlobjSearchColumn("custrecord_frf_details_assigned_to"),
			   new nlobjSearchColumn("custrecord_frf_details_operation_comment"),
			   new nlobjSearchColumn("custrecord_frf_details_softlock_date"),
			   new nlobjSearchColumn("custrecordfrf_details_created_time"),
			   new nlobjSearchColumn("internalid"),
			    new nlobjSearchColumn("custrecord_frf_details_status_flag"),
			   new nlobjSearchColumn("custrecord_taleo_external_selected_can","CUSTRECORD_FRF_DETAILS_RRF_NUMBER",null)
			]
			);
						
		if(searchresults != null && searchresults != undefined && searchresults != '')
		{
		count = searchresults.length;
		}else{
			count = 0;
		}
		

			var detail = form.addSubList('custpage_bo_pending_detail', 'list', 'Pending Allocation: '+ count, 'general');

			//add fields to the sublist
			detail.addField('custpage_check_box', 'checkbox', 'select');
			detail.addField('custpage_frf_no', 'text', 'FRF Number');
			detail.addField('custpage_softlock_emp', 'text', 'Softlock Emp');
			detail.addField('custpage_project_opp_id', 'text', 'Project/Opp ID');
			detail.addField('custpage_customer', 'text', 'Customer/Account');
			detail.addField('custpage_start_date', 'date', 'Start Date');
			detail.addField('custpage_end_date', 'date', 'End Date');
			detail.addField('custpage_bill_rate', 'text', 'Bill Rate');
			detail.addField('custpage_location', 'text', 'Location');
			detail.addField('custpage_reason', 'select', 'Reason','customlist_frf_bo_comments').setMandatory( true );
			detail.addField('custpage_comment', 'textarea', 'Comment').setDisplayType('entry');
			detail.addField('custpage_assigned_to', 'select', 'Assigned To','customlist_frf_bo_assigned_to');
			detail.addField('custpage_frf_internal_id', 'text', 'Internal Id').setDisplayType('hidden');
			detail.addField('custpage_frf_res_softlock_date', 'text', 'Resource SoftLock Date');
			detail.addField('custpage_frf_res_softlock_time', 'text', 'SoftLock Time');
			detail.addField('custpage_frf_status', 'text', 'Frf Status');
			detail.addField('custpage_rrf_selected_candidate', 'text', 'Selected Candidate');

			
			
			
			
			
       
		if (searchresults != null && searchresults != undefined && searchresults != '') //
        {    
		     var frf_data = [];
			 
			 var arr=[];
						var globalArray=[];
						
						arr[0] = "FRF Internal ID,FRF Number,Softlock Emp,Project/Opp Id,Customer,Start Date,End Date,Bill Rate,Location,BO Reason,BO Comment,Assigned To,SoftLock Date,SoftLock Time,FRF Status,Selected Candidate";
								//"\n" ;
								
						globalArray[0]=arr[0];
			 
			 for (var i = 0; i < searchresults.length; i++) 			 
             {
				 var project = searchresults[i].getText('custrecord_frf_details_project');
				 var opp_id = '';
				 
				 if(project== null || project == undefined || project == '')
				 {
					 opp_id = searchresults[i].getValue('custrecord_opportunity_id_sfdc','CUSTRECORD_FRF_DETAILS_OPP_ID',null);
					 opp_name = searchresults[i].getText('custrecord_frf_details_account');
					 
					 project = opp_id + " / "+opp_name;
				 }
                				
					var  data={
						   
								  'custpage_frf_no'           :  searchresults[i].getValue('custrecord_frf_details_frf_number'),
								  'custpage_softlock_emp'     :  searchresults[i].getText('custrecord_frf_details_selected_emp'),
								  'custpage_project_opp_id'   :  project,
								  'custpage_customer'	      :  searchresults[i].getText('custrecord_frf_details_account'),
								  'custpage_start_date'		  :  searchresults[i].getValue('custrecord_frf_details_start_date'),
								  'custpage_end_date'         :  searchresults[i].getValue('custrecord_frf_details_end_date'),
								  'custpage_bill_rate'        :  searchresults[i].getValue('custrecord_frf_details_bill_rate'),
								  'custpage_location'         :  searchresults[i].getText('custrecord_frf_details_res_location'),
								  'custpage_reason'           :  searchresults[i].getValue('custrecord_frf_details_bo_comments'),
								  'custpage_frf_internal_id'  :  searchresults[i].getText('internalid'),
								  'custpage_comment'          :  searchresults[i].getValue('custrecord_frf_details_operation_comment'),
								  'custpage_assigned_to'      :  searchresults[i].getValue('custrecord_frf_details_assigned_to'),
						  'custpage_frf_res_softlock_date'    :  searchresults[i].getValue('custrecord_frf_details_softlock_date'),
						  'custpage_frf_res_softlock_time'    :   searchresults[i].getValue('custrecordfrf_details_created_time'),
						  'custpage_frf_status'    			  :   searchresults[i].getText('custrecord_frf_details_status_flag'),
						  'custpage_rrf_selected_candidate'   :   searchresults[i].getValue('custrecord_taleo_external_selected_can','CUSTRECORD_FRF_DETAILS_RRF_NUMBER',null),
						 
						  
						};
						frf_data.push(data);
						
						
						if(request.getParameter('print')){
							
							
						 
						
							var frf_int_id = searchresults[i].getText('internalid');
							var frf_no = searchresults[i].getValue('custrecord_frf_details_frf_number');
							var softlock_emp =  searchresults[i].getText('custrecord_frf_details_selected_emp');
							var project_opp = project.replace(',',' ');
							var customer = searchresults[i].getText('custrecord_frf_details_account').replace(',',' ');
							var start_date = searchresults[i].getValue('custrecord_frf_details_start_date');
							var end_date = searchresults[i].getValue('custrecord_frf_details_end_date');
							var bill_rate = searchresults[i].getValue('custrecord_frf_details_bill_rate');
							var location = searchresults[i].getText('custrecord_frf_details_res_location').replace(',',' ');
							var reason = searchresults[i].getText('custrecord_frf_details_bo_comments');
							var comment = searchresults[i].getValue('custrecord_frf_details_operation_comment').replace(',',' ');
							var assigned_to = searchresults[i].getText('custrecord_frf_details_assigned_to');
							var softlock_date =  searchresults[i].getValue('custrecord_frf_details_softlock_date');
						    var softlock_time =  searchresults[i].getValue('custrecordfrf_details_created_time');
							var frf_status =  searchresults[i].getText('custrecord_frf_details_status_flag');
							var selected_emp_taleo =  searchresults[i].getValue('custrecord_taleo_external_selected_can','CUSTRECORD_FRF_DETAILS_RRF_NUMBER',null);

							if(selected_emp_taleo != null || selected_emp_taleo == undefined || selected_emp_taleo == ''){
							 selected_emp_taleo = searchresults[i].getValue('custrecord_taleo_external_selected_can','CUSTRECORD_FRF_DETAILS_RRF_NUMBER',null).replace(',',' ');
							}
					
							
							
							 
								arr[i+1] = "\n" + frf_int_id +
								"," +
								frf_no +               
								"," +
								softlock_emp +
								"," +
								project_opp +
								"," +
								customer +
								"," +
								start_date +
								"," +
								end_date +
								"," +
								bill_rate +
								"," +
								location +
								"," +
								reason +
								"," +
								comment +
								"," +
								assigned_to +
								"," +
								softlock_date +
								"," +
								softlock_time +
								"," +
								frf_status +
								"," +
								selected_emp_taleo;
								
								globalArray[i+1] = (arr[i+1]);
							   
						
							//nlapiLogExecution('DEBUG', 'beforeLoad_CalculateWeek', 'arr[i]=' + arr[i]);
											
							
						}
						
						
						
				} 

                    if(request.getParameter('print')){
						var fileName = 'BO pending allocation for FUEL'
						var Datetime = new Date();
						var CSVName = fileName + " - " + Datetime + '.csv';
						var file = nlapiCreateFile(CSVName, 'CSV', globalArray.toString());
						
						
						
					    response.setContentType('CSV', CSVName);
					    response.write(file.getValue());
						
						var newEmail = nlapiSendEmail(nlapiGetUser(), logged_in_mail, 'Report for Bo pending allocation from fuel',
				'Please see the attached file', null, null, null, file);
				
				
					}				
				
		      detail.setLineItemValues(frf_data);
			}
			
			
			
			
			
			
			
			
			
			
			
			
		
	
  
		// form.setScript('customscript_bo_pending_reason');
		  
		  
	 form.addButton('custpage_button_export_csv', 'Export','Bo_pending_allocation_csv()');
	 
	 
		  
//		form.setScript('customscript_bo_pending_reason');
			if(nlapiGetUser() == 139683 || nlapiGetUser() == 27789)
			{
				response.writePage(form);	
			}
			else
			{
				form.addSubmitButton('Submit');
				response.writePage(form);	
			}
	
			
	
    
	
 }
 //POST call
	else 
	{
		nlapiSetRedirectURL('SUITELET', '1929', 'customdeploy_bo_pending_alloc_frm_fuel');
	}
}




