/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Jan 2015     amol.sahijwani
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	// Get Employee ID
	var i_employee_id = nlapiGetNewRecord().getFieldValue('custrecord_recipient_employee');
	// Get Template ID
	var i_template_id = nlapiGetNewRecord().getFieldValue('custrecord_email_template');
	// Get CC
	var s_cc = nlapiGetNewRecord().getFieldValue('custrecord_send_email_queue_cc');
	var a_cc = s_cc.split(';');
	var emailId = nlapiLookupField('employee', i_employee_id, 'email');
	
	//var strText = nlapiMergeRecord(6, recType, recId);
	
	var template = nlapiLoadRecord('emailtemplate', i_template_id);

	if (template.getFieldValue('mediaitem') != null) {
			var media = template.getFieldValue('mediaitem');
			// do something with the media...
	} else {
			var strText = template.getFieldValue('content');
			// do something with the content...
			var rec = nlapiLoadRecord('employee', i_employee_id);
			
			var subject = template.getFieldValue('subject');
			
			var renderer = nlapiCreateTemplateRenderer();
			renderer.setTemplate(strText);
			renderer.addRecord('employee', rec);
			
			var body = renderer.renderToString();
			try
			{
				nlapiSendEmail(442, emailId, subject, body, a_cc);
			}
			catch(e)
			{
				nlapiLogExecution('ERROR','Error',e.message);
			}
			
			nlapiLogExecution('AUDIT', 'Email Sent ' + emailId + ', Employee ID: ' + i_employee_id, body);
	}
}
