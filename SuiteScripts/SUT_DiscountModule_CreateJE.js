/**
 * @author Jayesh
 */

function suiteletFunction(request, response)
{
	try
	{
		var i_recordID = request.getParameter('custscript_record_id');
		i_recordID = i_recordID.trim();
		nlapiLogExecution('audit','rcrd_id:-- ',i_recordID);
		
		var params=new Array();
		params['custscript_discount_module_id'] = i_recordID;
		
		var status = nlapiScheduleScript('customscript_sch_discountmodule_createje',null,params);
		
		nlapiSubmitField('customrecord_discount_module_for_cust', parseInt(i_recordID), 'custrecord_je_creation_status', 'Pending');
		
		nlapiSetRedirectURL('RECORD', 'customrecord_discount_module_for_cust', i_recordID, null,null);
	} 
	catch (err) {
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
	}
}