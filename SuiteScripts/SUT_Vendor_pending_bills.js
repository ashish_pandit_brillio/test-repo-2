/**

 Displays Contractor bills pending to create
 
 **/
/** Author Sai Saranya
	11 May 2017
 **/

function bills_to_create(request, response)
{
	try {
		var method = request.getMethod();

		if (method == 'GET') {
			bills_posting_period(request);
		}else {
			postcallbilldata(request);
		}
	} catch (err) {
		nlapiLogExecution('Error', 'suitelet', err);
		nlapiLogExecution('Error', 'Failed to generate the report',err);
	}
}
function bills_posting_period(request)
{
	try{
		var form = nlapiCreateForm('Pending Bills For Creation ');			
		var period = form.addField('stdate', 'date', 'Start Date');
		var yearselected = form.addField('enddate', 'date', 'End Date');	
	//	var month=form.addField('month_seleted','text','Month').setDisplayType('hidden');
	//	var year=form.addField('year_seleted','text','Year').setDisplayType('hidden');
		form.addSubmitButton('Load Details');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('Error', 'createApprovalForm', err);
		throw err;
	}
}
 
function postcallbilldata(period,year) {
	try {
			var stdate=request.getParameter('stdate');
			
			var enddate=request.getParameter('enddate');
		//	var stdate=get_previous_month_start_date(month,year);
			//var enddate=get_previous_month_end_date(month,year);
			nlapiLogExecution('DEBUG','StartDate',stdate);
			nlapiLogExecution('DEBUG','end date',enddate);
			var form = nlapiCreateForm('Pending Bills For Creation ');	
			var params=[];
			params['custscript_stdate']=stdate;
			params['custscript_end_date_bill']=enddate;
			nlapiScheduleScript('customscript_pending_vendor_bill', 'customdeploy1', params);
			var field = form.addField('dispalymessage', 'inlinehtml');
			var strVar="";
			strVar += "<html>";
			strVar += "<p>Email will be sent to you shortly.<\/p>";
			strVar += "<\/html>";
			field.setDefaultValue(strVar);
			response.writePage(form);
		
		
	}
	 catch (err) {
		nlapiLogExecution('ERROR', 'getMappedSubtierRecords', err);
		throw err;
	}
} 
