 /**
 * Module Description
 * User Event script to handle employee record creation and updation from the XML file generated from Fusion
 * 
 * Version                      Date                                                Author                           Remarks
 * 1.00                            18th September, 2015                Anuradha Sinha           This script saves the form data into Survey key Response Master custom record from the Survey.html 
 *
 */


function feedbackSuitelet(request, response)
{
  var method	=request.getMethod();
  if (method == 'GET' )
  {
    var file = nlapiLoadFile(50388);   //load the HTML file
    var contents = file.getValue();    //get the contents
    response.write(contents);     //render it on  suitlet
  
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call" );
  
  var feedback = new Object();
  feedback['Fusion_ID'] = request.getParameter('TextBox2');
  feedback['Name'] = request.getParameter('TextBox3');
  feedback['Email_ID'] = request.getParameter('TextBox5');
  feedback['Feedback_for'] = request.getParameter('TextBox4');
  feedback['Comments'] = request.getParameter('txtComments');
  feedback['Next_Level'] = request.getParameter('checkbox1');


  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Response3'] = request.getParameter('OptionsGroup3');
 
  var status = saveRequest(feedback);
   //response.write("Thank You!");
   //response.write("Your survey responses have been recorded!");
    var thanks_note = nlapiLoadFile(50389);   //load the HTML file
    var thanks_contents = thanks_note.getValue();    //get the contents
    response.write(thanks_contents);     //render it on  suitlet
   }

function saveRequest(feedback)
{
	try
	{
               var feedback_form = nlapiCreateRecord('customrecord_survey_response_key_master');
               feedback_form.setFieldValue('custrecord_fusion_id', feedback.Fusion_ID);
	       feedback_form.setFieldValue('custrecord_name', feedback.Name);
               feedback_form.setFieldValue('custrecord_email_identification', feedback.Email_ID);
               feedback_form.setFieldValue('custrecord_feedback_for', feedback.Feedback_for);

               feedback_form.setFieldValue('custrecord_question_1',"How would you rate the quality of resolution/clarification received for your query/concern?");
               feedback_form.setFieldValue('custrecord_question_2', "How satisfied are you with the response time in resolving your query?");
               feedback_form.setFieldValue('custrecord_question_3', "How would you rate the overall experience?");


               feedback_form.setFieldValue('custrecord_response1', feedback.Response1);
               feedback_form.setFieldValue('custrecord_response2', feedback.Response2);
               feedback_form.setFieldValue('custrecord_response3', feedback.Response3);
               
               feedback_form.setFieldValue('custrecord_suggestions', feedback.Comments);
              
               feedback_form.setFieldValue('custrecord_next_level', feedback.Next_Level);
               feedback_form.setFieldValue('custrecord_survey_type',4);
     
              
               var id = nlapiSubmitRecord(feedback_form, false,true);
               nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log','');
}
}
 }