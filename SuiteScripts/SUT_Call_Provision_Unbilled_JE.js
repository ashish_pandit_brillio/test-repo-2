/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:
	Author:
	Company:
	Date:
	Description:


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction(request, response)
{	
	var i_submitID;	
	
	try
	{
	 if(request.getMethod()=='GET')
	 {	 	
	   var a_data_process_array=request.getParameter('custscript_data_array_provision');
       nlapiLogExecution('DEBUG', ' suiteletFunction',' Data Process Array -->' + a_data_process_array);
	   nlapiLogExecution('DEBUG', ' suiteletFunction',' Data Process Array Length -->' + a_data_process_array.length);
	   
	   var i_current_user=request.getParameter('custscript_current_user');
       nlapiLogExecution('DEBUG', ' suiteletFunction',' Current User -->' + i_current_user);	   
	   
	  var o_processOBJ = nlapiCreateRecord('customrecord_provision_processed_records');
	  	
	  o_processOBJ.setFieldValue('custrecord_processed_records_provision',a_data_process_array)
	  o_processOBJ.setFieldValue('custrecord_employee_id_provision',i_current_user)	
	  
	  i_submitID = nlapiSubmitRecord(o_processOBJ,true,true)	
	  nlapiLogExecution('DEBUG', 'ERROR',' Custom Record ID -->' + i_submitID);	

	
	 }
		
	}
    catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH
	
	response.write(i_submitID);
	

}

// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
