function postRESTlet(dataIn) {
	try {

		/*
		test JSON
		
		var dataIn = []; json
		dataIn = {
  "DATA_DS": {
    "G_1": {
      "PAYROLL_ACTION_ID": "375154",
      "G_2": {
        "FILE_FRAGMENT": {
          "SR_ABS_Test": {
            "REP_CATEGORY_NAME": "Long_Leave_NS",
            "parameters": {
              "request_id": "300000198069156",
              "FLOW_NAME": "Testing2",
              "legislative_data_group_id": "",
              "effective_date": "2020-05-26",
              "start_date": "",
              "report_category_id": "300000173851733",
              "action_parameter_group_id": "",
              "Action_From_Date": "",
              "Changes_Only": "ATTRIBUTE"
            },
            "EmployeeData": {
              "OBJECT_ACTION_ID": "237419077",
              "Employee": {
                "PersonID": "100000000168911",
                "PersonNumber": "101908",
                "PersonType": "EMP",
                "HireDate": "2007-07-16T00:00:00.000Z",
                "WorkEmail": "Arun.Kareppagol@brillio.com",
                "FirstName": "Arun",
                "MiddleName": "M",
                "LastName": "Kareppagol"
              },
              "AbsenceHT": {
                "Absence": {
                  "AbsenceData": {
                    "AbsEntryID": "300000198069013",
                    "PersonID": "100000000168911",
                    "AbsenceType": "IN-LWP",
                    "StartDate": "2020-06-01T00:00:00.000Z",
                    "EndDate": "2020-07-02T00:00:00.000Z",
                    "AbsenceComments": "testing",
                    "Duration": "30",
                    "StartTime": "",
                    "EndTime": "23:59",
                    "AbsenceStatus": "ORA_WITHDRAWN",
                    "ApprovalStatus": "APPROVED",
                    "AbsenceReason": "",
                    "SubmittedDate": "2020-05-26T15:37:23.000Z",
                    "UOM": "C"
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}*/

		nlapiLogExecution('Debug', 'dataIn', JSON.stringify(dataIn));
		var Data_Array; //To store the JSON value
		var request_Type = 'GET';
		var response = new Response();
		switch (request_Type) {
			case M_Constants.Request.Get:
				if (!dataIn instanceof Array) {
					response.Data = "Some error with the data sent";
					response.Status = false;
				} else {
					Data_Array = new Array();
					for (var i = 0; i < dataIn.length; i++) {
						Data_Array[i] = dataIn[i];
						nlapiLogExecution('debug', 'Data_Array', dataIn[i]);
					}

					response.Data = leaveCreation(dataIn);
					response.Status = true;
				}
				break;
		}
		return response;
	} catch (error) {
		nlapiLogExecution('debug', 'postRESTlet', error);
		response.Data = error;
		response.status = false;
	}

	nlapiLogExecution('debug', 'response', JSON.stringify(response));

}

function leaveCreation(dataIn) {
	try {
		var o_data_employeeData = dataIn.DATA_DS.G_1.G_2.FILE_FRAGMENT.SR_ABS_Test.EmployeeData;
		if (_logValidation(o_data_employeeData)) {

			nlapiLogExecution('DEBUG', 'Checking Whether Object or Array');

			if (isArray(o_data_employeeData)) {
				nlapiLogExecution('debug', 'Length is ', o_data_employeeData.length);

				for (var count = 0; count < o_data_employeeData.length; count++) {


					var o_emp_datafields = o_data_employeeData[count].Employee;
					nlapiLogExecution('DEBUG', 'o_emp_datafields', o_emp_datafields);

					var s_personNumber = o_emp_datafields.PersonNumber;
					nlapiLogExecution('DEBUG', 's_personNumber', s_personNumber);

					var employeeSearch = nlapiSearchRecord("employee", null, [
						["custentity_fusion_empid", "is", s_personNumber]
					], [
						new nlobjSearchColumn("internalid")
					]);
					var emp_id = '';
					if (_logValidation(employeeSearch)) {
						emp_id = employeeSearch[0].getValue('internalid');
					}
					nlapiLogExecution('Debug', 'Emp InternalID', emp_id);

					var o_Data = o_data_employeeData[count].AbsenceHT;
					nlapiLogExecution('Debug', 'AbsenceHT', o_Data);
					if (_logValidation(o_Data)) {
						var o_absenceData = o_Data.Absence;
						nlapiLogExecution('Debug', 'Absence', o_absenceData);
						if (isArray(o_absenceData)) {
							if (_logValidation(o_absenceData)) {
								for (var i = 0; i < o_absenceData.length; i++) {

									var o_abs_data = o_absenceData[i].AbsenceData;

									if (_logValidation(o_abs_data)) { //prabhat gupta 13/8/2020 NIS-1683 validation for checking empty object

										var st_date = formatDate(o_absenceData[i].AbsenceData.StartDate);
										var end_date = formatDate(o_absenceData[i].AbsenceData.EndDate);

										var timeDiff = Math.abs((nlapiStringToDate(st_date).getTime()) - (nlapiStringToDate(end_date).getTime()))
										var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

										if (diffDays >= parseInt(29)) {
											if (_logValidation(o_abs_data)) {
												var absence_id = JSON.stringify(o_absenceData[i].AbsenceData.AbsEntryID);
												var absence_start_date = o_absenceData[i].AbsenceData.StartDate;
												var absence_end_date = o_absenceData[i].AbsenceData.EndDate;
												var approval_status = o_absenceData[i].AbsenceData.ApprovalStatus;
												var absence_status = o_absenceData[i].AbsenceData.AbsenceStatus;
												var absence_type = o_absenceData[i].AbsenceData.AbsenceType;


												nlapiLogExecution('Debug', 'abs id', absence_id);
												var leaveSearch = nlapiSearchRecord("customrecord_long_leave_data", null, [
													["custrecord_leave_id", "is", absence_id]
												], [
													new nlobjSearchColumn("internalid")
												]);

												if (_logValidation(leaveSearch)) {
													var id = leaveSearch[0].getValue('internalid');
													if (absence_status == 'ORA_WITHDRAWN' && approval_status == 'APPROVED') {
														nlapiDeleteRecord('customrecord_long_leave_data', id);
													} else {
														nlapiLogExecution('DEBUG', 'Leave Internal ID', id);
														var rec = nlapiLoadRecord('customrecord_long_leave_data', id);
														rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
														rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
														rec.setFieldValue('custrecord_leave_status', approval_status);
														rec.setFieldValue('custrecord_leave_emp_id', emp_id);
														rec.setFieldValue('custrecord_leave_type', absence_type);

														var submit_id = nlapiSubmitRecord(rec, false, true);
														nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id);
													}
												} else {
													nlapiLogExecution('DEBUG', 'Create new record - Array');
													var rec = nlapiCreateRecord('customrecord_long_leave_data', {
														recordmode: 'dynamic'
													});
													rec.setFieldValue('custrecord_leave_id', absence_id);
													rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
													rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
													rec.setFieldValue('custrecord_leave_status', approval_status);
													rec.setFieldValue('custrecord_leave_emp_id', emp_id);
													rec.setFieldValue('custrecord_leave_type', absence_type);

													var submit_id = nlapiSubmitRecord(rec, false, true);
													nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id);
												}
											}
										}
										//}//End if


									}

								} //End of absence array loop
							} //End of Condition
						} //End of array check condition
						else {
							if (_logValidation(o_absenceData)) {
								var o_abs_data = o_absenceData.AbsenceData;

								if (_logValidation(o_abs_data)) { //prabhat gupta 13/8/2020 NIS-1683 validation for checking empty object

									var st_date = formatDate(o_absenceData.AbsenceData.StartDate);
									var end_date = formatDate(o_absenceData.AbsenceData.EndDate);

									var timeDiff = Math.abs((nlapiStringToDate(st_date).getTime()) - (nlapiStringToDate(end_date).getTime()))
									var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

									if (diffDays >= parseInt(29)) {
										if (_logValidation(o_abs_data)) {
											var absence_id = JSON.stringify(o_absenceData.AbsenceData.AbsEntryID);
											var absence_start_date = o_absenceData.AbsenceData.StartDate;
											var absence_end_date = o_absenceData.AbsenceData.EndDate;
											var approval_status = o_absenceData.AbsenceData.ApprovalStatus;
											var absence_status = o_absenceData.AbsenceData.AbsenceStatus;
											var absence_type = o_absenceData.AbsenceData.AbsenceType;


											nlapiLogExecution('Debug', 'abs id', absence_id);
											var leaveSearch = nlapiSearchRecord("customrecord_long_leave_data", null, [
												["custrecord_leave_id", "is", absence_id]
											], [
												new nlobjSearchColumn("internalid")
											]);

											if (_logValidation(leaveSearch)) {
												var id = leaveSearch[0].getValue('internalid');
												if (absence_status == 'ORA_WITHDRAWN' && approval_status == 'APPROVED') {
													nlapiDeleteRecord('customrecord_long_leave_data', id);
												} else {
													nlapiLogExecution('DEBUG', 'Leave Internal ID', id);
													var rec = nlapiLoadRecord('customrecord_long_leave_data', id);
													rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
													rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
													rec.setFieldValue('custrecord_leave_status', approval_status);
													rec.setFieldValue('custrecord_leave_emp_id', emp_id);
													rec.setFieldValue('custrecord_leave_type', absence_type);

													var submit_id = nlapiSubmitRecord(rec, false, true);
													nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id);
												}
											} else {
												nlapiLogExecution('DEBUG', 'Create new record - Array');
												var rec = nlapiCreateRecord('customrecord_long_leave_data', {
													recordmode: 'dynamic'
												});
												rec.setFieldValue('custrecord_leave_id', absence_id);
												rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
												rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
												rec.setFieldValue('custrecord_leave_status', approval_status);
												rec.setFieldValue('custrecord_leave_emp_id', emp_id);
												rec.setFieldValue('custrecord_leave_type', absence_type);

												var submit_id = nlapiSubmitRecord(rec, false, true);
												nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id);
											}
										}
									}

								}
							}
						}


					}
				}
			} else {
				//object
				var o_emp_datafields = o_data_employeeData.Employee;
				nlapiLogExecution('Debug', 'o_emp_datafields', o_emp_datafields);
				var s_personNumber = o_emp_datafields.PersonNumber;

				var employeeSearch = nlapiSearchRecord("employee", null, [
					["custentity_fusion_empid", "is", s_personNumber]
				], [
					new nlobjSearchColumn("internalid")
				]);
				var emp_id = employeeSearch[0].getValue('internalid')
				var o_Data = o_data_employeeData.AbsenceHT
				nlapiLogExecution('Debug', 'o_Data', o_Data)
				if (_logValidation(o_Data)) {
					var o_absenceData = o_Data.Absence;


					nlapiLogExecution('Debug', 'Absence Length', o_absenceData.length);
					if (_logValidation(o_absenceData)) {
						var o_absDetails = o_absenceData.AbsenceData
						if (isArray(o_absDetails)) {
							//AbsenceData Array     

							if (_logValidation(o_absDetails)) { //prabhat gupta 13/8/2020 NIS-1683 validation for checking empty object

								var st_date = formatDate(o_absenceData[i].AbsenceData.StartDate);
								var end_date = formatDate(o_absenceData[i].AbsenceData.EndDate);
								var timeDiff = Math.abs((nlapiStringToDate(end_date).getTime()) - (nlapiStringToDate(st_date).getTime()))
								var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

								if (diffDays >= parseInt(29)) {
									for (var i = 0; i < o_absDetails.length; i++) {

										var absence_id = JSON.stringify(o_absenceData[i].AbsenceData.AbsEntryID);
										var absence_start_date = o_absenceData[i].AbsenceData.StartDate;
										var absence_end_date = o_absenceData[i].AbsenceData.EndDate;
										var approval_status = o_absenceData[i].AbsenceData.ApprovalStatus;
										var absence_status = o_absenceData[i].AbsenceData.AbsenceStatus;
										var absence_type = o_absenceData[i].AbsenceData.AbsenceType;


										var leaveSearch = nlapiSearchRecord("customrecord_long_leave_data", null, [
											["custrecord_leave_id", "is", absence_id]
										], [
											new nlobjSearchColumn("internalid")
										]);

										if (_logValidation(leaveSearch)) {

											var id = leaveSearch[i].getValue('internalid');
											if (absence_status == 'ORA_WITHDRAWN' && approval_status == 'APPROVED') {
												nlapiDeleteRecord('customrecord_long_leave_data', id);
											} else {
												nlapiLogExecution('DEBUG', 'Leave Internal ID', id);
												var rec = nlapiLoadRecord('customrecord_long_leave_data', id);

												rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
												rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
												rec.setFieldValue('custrecord_leave_status', approval_status);
												rec.setFieldValue('custrecord_leave_emp_id', emp_id);
												rec.setFieldValue('custrecord_leave_emp_id', absence_type);

												var submit_id = nlapiSubmitRecord(rec, false, true);
												nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id);
											}
										} else {
											var rec = nlapiCreateRecord('customrecord_long_leave_data', {
												recordmode: 'dynamic'
											});
											rec.setFieldValue('custrecord_leave_id', absence_id);
											rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
											rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
											rec.setFieldValue('custrecord_leave_status', approval_status);
											rec.setFieldValue('custrecord_leave_emp_id', emp_id);
											rec.setFieldValue('custrecord_leave_type', absence_type);

											var submit_id = nlapiSubmitRecord(rec, false, true);
											nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id)
										}
									}
								}
							}
						} else {
							//AbsenceData Object
							var o_absDetails = o_absenceData.AbsenceData
							if (_logValidation(o_absDetails)) { //prabhat gupta 13/8/2020 NIS-1683 validation for checking empty object

								var st_date = formatDate(o_absenceData.AbsenceData.StartDate);
								var end_date = formatDate(o_absenceData.AbsenceData.EndDate);
								var timeDiff = Math.abs((nlapiStringToDate(end_date).getTime()) - (nlapiStringToDate(st_date).getTime()))
								var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

								if (diffDays >= parseInt(29)) {
									var absence_id = JSON.stringify(o_absenceData.AbsenceData.AbsEntryID);
									var absence_start_date = o_absenceData.AbsenceData.StartDate;
									var absence_end_date = o_absenceData.AbsenceData.EndDate;
									var approval_status = o_absenceData.AbsenceData.ApprovalStatus;
									var absence_type = o_absenceData.AbsenceData.AbsenceType;
									var absence_status = o_absenceData.AbsenceData.AbsenceStatus;


									var leaveSearch = nlapiSearchRecord("customrecord_long_leave_data", null, [
										["custrecord_leave_id", "is", absence_id]
									], [
										new nlobjSearchColumn("internalid")
									]);

									if (_logValidation(leaveSearch)) {

										var id = leaveSearch[0].getValue('internalid');
										if (absence_status == 'ORA_WITHDRAWN' && approval_status == 'APPROVED') {
											nlapiDeleteRecord('customrecord_long_leave_data', id);
										} else {
											lapiLogExecution('DEBUG', 'Leave Internal ID', id);
											var rec = nlapiLoadRecord('customrecord_long_leave_data', id);
											rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
											rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
											rec.setFieldValue('custrecord_leave_status', approval_status);
											rec.setFieldValue('custrecord_leave_emp_id', emp_id);
											rec.setFieldValue('custrecord_leave_type', absence_type);

											var submit_id = nlapiSubmitRecord(rec, false, true);
											nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id);
										}
									} else {
										var rec = nlapiCreateRecord('customrecord_long_leave_data', {
											recordmode: 'dynamic'
										});
										rec.setFieldValue('custrecord_leave_id', absence_id);
										rec.setFieldValue('custrecord_leave_st_date', formatDate(absence_start_date));
										rec.setFieldValue('custrecord_leave_end_date', formatDate(absence_end_date));
										rec.setFieldValue('custrecord_leave_status', approval_status);
										rec.setFieldValue('custrecord_leave_emp_id', emp_id);
										rec.setFieldValue('custrecord_leave_type', absence_type);

										var submit_id = nlapiSubmitRecord(rec, false, true);
										nlapiLogExecution('debug', 'Leave data', 'Record id:' + submit_id)
									}

								}

							}
						}
					}
				}
			}
		}
	} catch (e) {
		nlapiLogExecution('Error', 'Process Error', e);
	}

}


function formatDate(fusionDate) {

	if (fusionDate) {
		var datePart = fusionDate.split('T')[0];
		var dateSplit = datePart.split('-');


		// MM/DD/ YYYY
		var netsuiteDateString = dateSplit[1] + "/" + dateSplit[2] + "/" +
			dateSplit[0];
		return netsuiteDateString;
	}
	return "";
}

function _logValidation(value) {
	if (value != null && value != '' && value != undefined && value.toString() != undefined && value.toString() != 'NaN' && value != NaN && value != 'undefined' && value != ' ') {
		return true;
	} else {
		return false;
	}
}