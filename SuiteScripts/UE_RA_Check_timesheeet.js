/**
 * Module Description
 * 
 * Version    Date            Author         Remarks:
 * 1.00       05 Dec 2017     bidhi.raj      This change was done to avoid saving of bill rate if timesheets already exist for employee
 *      
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	try {
		var startDate = nlapiGetFieldValue('startdate');
		var endDate = nlapiGetFieldValue('enddate');
		var employee = nlapiGetFieldValue('allocationresource');
        nlapiLogExecution('debug','startDate '+startDate+' endDate '+endDate+' employee '+employee);
		var employee_Lookup = nlapiLookupField('employee',employee,['hiredate']);
		var emp_hire_date = employee_Lookup.hiredate;
		emp_hire_date = nlapiStringToDate(emp_hire_date);
		var resAlloc_stDate = nlapiStringToDate(startDate);
		
		
		if (employee && startDate && endDate) {

			if (checkIfTimesheetExists(employee, startDate, endDate)) {
              if(nlapiGetUser()=="200580"){
                nlapiLogExecution('ERROR', 'inside if (checkIfTimesheetExists(employee, startDate, endDate))');
              }
           
				form.getField('custevent3').setDisplayType('inline');
              form.getField('custeventrbillable').setDisplayType('disabled');
              

			}
			
		}

		
	} catch (err) {
		nlapiLogExecution('ERROR', 'clientSaveRecordCheckTimesheet', err);
		alert(err.message);
	}
}

function checkIfTimesheetExists(employee, startDate, endDate) {
	try {
	//	var timeEntrySearch = nlapiSearchRecord('timeentry', null, [//commnented by praveena on 12-03-2020 due to migration
		var timeEntrySearch = nlapiSearchRecord('timebill', null, [//search record changed after migration. logic added by praveena on 12-03-2020  
		        new nlobjSearchFilter('employee', null, 'anyof', employee),
		        new nlobjSearchFilter('type', null, 'anyof', 'A'),
		        new nlobjSearchFilter('date', null, 'within', startDate,
						endDate ) ]);

		return timeEntrySearch != null;
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkIfTimesheetExists', err);
		throw err;
	}
}