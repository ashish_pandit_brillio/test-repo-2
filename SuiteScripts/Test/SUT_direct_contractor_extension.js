/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Aug 2016     shruthi.l
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response)
{
	try{
		
		var method=request.getMethod();
	var employee_id= request.getParameter('custparam_employee_id'); 
	nlapiLogExecution('debug', 'method==', method);
	nlapiLogExecution('debug', 'employee_id==', employee_id);
	// Get the employee details
  
	var filters = new Array();
	filters[0]  = new nlobjSearchFilter('internalid',null,'anyof',employee_id);
   
	var columns   = new Array();
	columns[0]    = new nlobjSearchColumn('firstname');
	columns[1]    = new nlobjSearchColumn('custentity_fusion_empid');
	columns[2]    = new nlobjSearchColumn('department');
	columns[3]    = new nlobjSearchColumn('title');
	columns[4]    = new nlobjSearchColumn('custentity_actual_hire_date');
	columns[5]    = new nlobjSearchColumn('custentity_probationenddate');
	columns[6]    = new nlobjSearchColumn('custentity_reportingmanager');
	columns[7]    = new nlobjSearchColumn('location');
	columns[8]    = new nlobjSearchColumn('middlename');
	columns[9]    = new nlobjSearchColumn('lastname');
	columns[10]	  = new nlobjSearchColumn('email', 'custentity_reportingmanager');
	//columns[11]   = new nlobjSearchColumn('custrecord_hrbusinesspartner','department');
	columns[11]   = new nlobjSearchColumn('custentity_emp_hrbp'); /// Added BY Shravan on  02-Sep
	columns[12]   = new nlobjSearchColumn('custentity_prior_confirmation');
    var emp_search = nlapiSearchRecord('employee',null,filters,columns);
   //var emp_search = nlapiSearchRecord('employee','customsearch_confirmation_trigger_single',null,columns);
   //var hr_bp_id = emp_search[0].getValue('custrecord_hrbusinesspartner','department');
   var hr_bp_id = null
   if(emp_search)
   {	
		hr_bp_id = emp_search[0].getValue('custentity_emp_hrbp');
   }	//// if(emp_search)
   var hr_bp_email = null;
   
   if(isNotEmpty(hr_bp_id)){
			hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
			hr_bp_firstname = nlapiLookupField('employee',hr_bp_id, 'firstname');
			hr_bp_lastname = nlapiLookupField('employee',hr_bp_id, 'lastname');
		}///if(isNotEmpty(hr_bp_id))
		else
		{
			hr_bp_email = 'hrbpteam@brillio.com' /// Added by Shravan Kulkarni
		} //// else of if(isNotEmpty(hr_bp_id))
	if (method == 'GET' )
	{	
		
  
		if(isNotEmpty(emp_search))
		{
			if(emp_search[0].getValue('custentity_prior_confirmation')=='F'){
				
				var file = nlapiLoadFile(446959);   //load the HTML file
				var contents = file.getValue();    //get the contents
				
			var objReplaceValues=new Object();
			objReplaceValues['employee_name']=emp_search[0].getValue('firstname');
            objReplaceValues['employee_number']=emp_search[0].getValue('custentity_fusion_empid');
            objReplaceValues['employee_pratcice']=emp_search[0].getText('department');
			objReplaceValues['employee_designation']=emp_search[0].getValue('title');
            objReplaceValues['date_of_joining']=emp_search[0].getValue('custentity_actual_hire_date');
            objReplaceValues['review_period']=emp_search[0].getValue('custentity_probationenddate');
            objReplaceValues['employee_reporting_manager']=emp_search[0].getText('custentity_reportingmanager');
            objReplaceValues['employee_location']=emp_search[0].getText('location');
            
			contents = replaceValues(contents, objReplaceValues);
			response.write(contents);          //render it on  suitlet
			}else{
				throw "Your feedback is filled";
			}
		 }
		
		
	}
	else
	{
		//var form = nlapiCreateForm("Suitelet - POST call");
		var feedback = new Object();
		feedback['employee_id'] =  request.getParameter('custparam_employee_id');
		feedback['feedback_record_id'] =request.getParameter('feedback_record_id');
		feedback['Response9'] = request.getParameter('OptionsGroup9');
		feedback['Response10'] = request.getParameter('OptionsGroup10');
		//feedback['Comments1'] = request.getParameter('txtComments1');
		feedback['Comments2'] = request.getParameter('txtComments2');
		feedback['actionperformed'] = request.getParameter('whatclicked');
		
		feedback['employeeNumber'] = emp_search[0].getValue('custentity_fusion_empid');
		feedback['employee_pratcice'] = emp_search[0].getText('department');
		feedback['actual_hire_date'] = emp_search[0].getValue('custentity_actual_hire_date');
		feedback['probation_end_date'] = emp_search[0].getValue('custentity_probationenddate');
		feedback['location'] = emp_search[0].getValue('location');
		feedback['reporting_manager'] = emp_search[0].getValue('custentity_reportingmanager');
		
	
		
		var status = saveRequest(feedback);
		
		if(isNotEmpty(emp_search))
		{
			var firstName = emp_search[0].getValue('firstname');
			var middleName = emp_search[0].getValue('middlename');
			var lastName = emp_search[0].getValue('lastname');
			var employee_pratcice = emp_search[0].getValue('department');
			
			var employeeNumber=emp_search[0].getValue('custentity_fusion_empid');
			var title=emp_search[0].getValue('title');
			var actual_hire_date=emp_search[0].getValue('custentity_actual_hire_date');
			var probation_end_date=emp_search[0].getValue('custentity_probationenddate');
			var overallRating = request.getParameter('OptionsGroup9');
			var salaryDetails=request.getParameter('OptionsGroup10');
			var reporting_manager_email = emp_search[0].getValue('email', 'custentity_reportingmanager');
			var employee_location=emp_search[0].getValue('location');
			nlapiLogExecution('Debug', "overallRating", overallRating);		
			if ( overallRating == '3 months'|| overallRating == '6 months' || overallRating == 'Short of Expectation')
			{
			sendEmail(firstName,employeeNumber, employee_id,reporting_manager_email,overallRating,salaryDetails,actual_hire_date,probation_end_date,hr_bp_email);
			}
			//generatePDF(firstName,middleName,lastName,title,employeeNumber,overallRating,actual_hire_date,probation_end_date);
		}
   
		var thanks_note = nlapiLoadFile(446961);   //load the HTML file
		var thanks_contents = thanks_note.getValue();    //get the contents
		response.write(thanks_contents);     //render it on  suitlet
	}	
	} /// End of try
	catch (s_Exp)
	{
			nlapiLogExecution('debug', 'Main Exception==', s_Exp);
	} //// End of catch
	
	
	
}


//Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
 {
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
     content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
 }
             
 return content;
}   

function saveRequest(feedback)
{
	try
 {
     
     var feedback_form = nlapiCreateRecord('customrecord_direct_contract');
	 feedback_form.setFieldValue('custrecord_employee_name_dc_', feedback.employee_id);
		
     //feedback_form.setFieldValue('custrecordcustrecord_new_question_test1',"Please provide the reason to extend");
	 feedback_form.setFieldValue('custrecord__new_question_test_2', "Probation extension months");
	// feedback_form.setFieldValue('custrecord__new_question_test_3', "Please click the below button to terminate");
	// feedback_form.setFieldValue('custrecord__new_question_test_3', "Please click the below button to terminate");
	 feedback_form.setFieldValue('custrecord_new_question_test_4', "Termination Reason");
	// feedback_form.setFieldValue('custrecord_new_comments_test_2', feedback.Response9);
		
	 feedback_form.setFieldValue('custrecord_employee_number_dc_', feedback.employeeNumber);
     feedback_form.setFieldText('custrecord_employee_department_dc_', feedback.employee_pratcice);
                             
     feedback_form.setFieldValue('custrecord_date_of_join_dc', feedback.actual_hire_date);
     feedback_form.setFieldValue('custrecord_review_period_dc', feedback.probation_end_date);
     //feedback_form.setFieldValue('custrecord_reporting_manager', feedback.reporting_manager);
     feedback_form.setFieldValue('custrecord_location_dc', feedback.location);
    nlapiLogExecution('Debug', "feedbackres", feedback.actionperformed); 
     if(feedback.actionperformed=="Extend"){
    	 feedback_form.setFieldValue('custrecord_extend_dc', 'T');
    	// feedback_form.setFieldValue('custrecord_extension_comments', feedback.Comments1);
    	 feedback_form.setFieldValue('custrecord_extend_mnths',feedback.Response9);
    	 if(feedback.Response9 =='3 months'){
    		 var extdate = new Date(feedback.probation_end_date);
    		 extdate=nlapiAddMonths(extdate, 3);
    		 var extdates= nlapiDateToString(extdate);
    		 nlapiLogExecution('Debug', "extdates", extdates);
    		 feedback_form.setFieldValue('custrecord_review_period_dc', extdates);
    		 nlapiSubmitField('employee',feedback.employee_id, 'custentity_probationenddate', extdates);
    		// nlapiSubmitField('employee',feedback.employee_id, 'custentity_notconfirmed', 'F');
    		 
    	 }
    	 if(feedback.Response9=='6 months'){
    		 var extdate = new Date(feedback.probation_end_date);
    		 nlapiLogExecution('Debug', "extdate", extdate);
    		 extdate=nlapiAddMonths(extdate, 6);
    		
    		 var extdates= nlapiDateToString(extdate);
    		 nlapiLogExecution('Debug', "extdates", extdates);
    		 feedback_form.setFieldValue('custrecord_review_period_dc', extdates);
    		 nlapiSubmitField('employee',feedback.employee_id, 'custentity_probationenddate', extdates);
    	 }
    	 
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_prior_confirmation', 'T');
    	 
     }else if(feedback.actionperformed=="Terminate"){
    	 feedback_form.setFieldValue('custrecord_close_dc', 'T');
    	 feedback_form.setFieldValue('customrecord_comments_dc', feedback.Comments2);
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_prior_confirmation', 'T');
    	 nlapiSubmitField('employee',feedback.employee_id, 'custentity_notconfirmed', 'T');
     }
     
	 feedback_form.setFieldValue('custrecord_employee_name_test', feedback.employee_id);
		
	var feedback_record_id = nlapiSubmitRecord(feedback_form, true,true);                                    
    
     nlapiLogExecution('debug', 'Record Saved', feedback_record_id);
	}
	catch(err)
	{
		nlapiLogExecution('error', 'Record not saved', err);
	}
}


function sendEmail(firstName,employeeNumber, emp_id,custentity_reportingmanager_email,overallRating,salaryDetails,actual_hire_date,probation_end_date,hr_bp_email)
{
	try
	{	
		var fileObj = nlapiLoadFile(175408);
		var email_to = emp_id;
		if((overallRating == 'Short of Expectation')||( salaryDetails== 'yes' ))
		{
			email_to = 'waseem.pasha@brillio.com';
		}
		
		
		var mailTemplate =serviceTemplate(firstName,employeeNumber,overallRating,salaryDetails,actual_hire_date,probation_end_date);
		nlapiLogExecution('Debug', 'mailTemplate', mailTemplate.MailBody);
		nlapiLogExecution('Debug','to',email_to);
		var atta=[];
			 atta['entity']='62082';
		nlapiSendEmail('10730', email_to, mailTemplate.MailSubject, mailTemplate.MailBody,[custentity_reportingmanager_email,hr_bp_email,'waseem.pasha@brillio.com'],null,atta);
		
	}
	catch(err)
	{
		nlapiLogExecution('error', 'sendEmail', err);
		throw err;
	}
}

function serviceTemplate(firstName,employeeNumber,overallRating,salaryDetails,custentity_actual_hire_date,probation_end_date)
{

var htmltext = '';
var subject ='';	
nlapiLogExecution('Debug', 'serviceTemplate', overallRating);
nlapiLogExecution('Debug', 'serviceTemplate', salaryDetails);
	//Search the rating value if rating is Outstanding,Exceed Expectations, Met Expectations Confirmation Form will be Generated

	if ((overallRating == '3 months'|| overallRating == '6 months')&& (salaryDetails =='no'))
	{
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Hi '+ firstName +'</p>';
		//htmltext += '&nbsp';
		
		//htmltext += &nbsp;
		htmltext += '<p> This is with reference to your Contract Agreement. This is to inform you that your contract period has been further extended for  '+overallRating+'  with effect from '+probation_end_date+'.</p>';
		htmltext +='<p>All other terms and conditions of your appointment with Brillio remain unchanged..</p>';
		htmltext += '<p>Warm Regards,</p>';
		htmltext += '<p>Team HR</p>';

		htmltext += '</td></tr>';
		htmltext += '</table>';
		subject +='Contract Extension';
	
	}
	else if((salaryDetails=="yes") && (overallRating == '3 months'|| overallRating == '6 months'))
	{
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Hi  </p>';
		//htmltext += &nbsp;
		htmltext += '<p> This is inform you that '+ firstName +'-'+employeeNumber+' manager has extended the contract for  '+ overallRating+' months with payment rate change in Brillio. Please take it forward with the further process..</p>';
					
					
		htmltext += '<p>Regards,</p>';
		htmltext += '<p>Team HR</p>';

		
	
		htmltext += '</td></tr>';
		htmltext += '</table>';
		subject +='Employee Payment Change';
	}

	else if (overallRating == 'Short of Expectation')
	{
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Hi  </p>';
		//htmltext += &nbsp;
		htmltext += '<p> This is inform you that '+ firstName +'-'+employeeNumber+' manager has wished to terminate his/her services in Brillio. Please take it forward with the further process..</p>';
      
      //Added her in template by Sitaram 30-03-2021 
					
					
		htmltext += '<p>Regards,</p>';
		htmltext += '<p>Team HR</p>';

		
	
		htmltext += '</td></tr>';
		htmltext += '</table>';
		subject +='Employee Termination';
		
	}
	
return {
     MailBody : htmltext,
     MailSubject : subject,    
 };

}