/**
 * P/L report for managers
 * 
 * Version Date Author Remarks
 * 
 * 1.00 10 Mar 2016 amol.sahijwani
 * 
 * 2.00 16 May 2016 Nitish Mishra - Refactored and gave access to client
 * partners and regional heads
 */

function update() {
	
		

		
		
		//Search for exchange Rate
		var f_rev_curr = 0;
		var f_cost_curr = 0;
		var filters = [];
	
		//var filters = [];
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
		//filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
		var a_dataVal = {};
		var dataRows =[];
		
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r');
		column[1] = new nlobjSearchColumn('custrecord_pl_currency_exchange_cost');
		column[2] = new nlobjSearchColumn('custrecord_pl_currency_exchange_year');
		column[3] = new nlobjSearchColumn('custrecord_pl_currency_exchange_month');
		column[4] = new nlobjSearchColumn('custrecord_pl_gbp_revenue');
		column[5] = new nlobjSearchColumn('custrecord_pl_gbp_cost_rate');
		column[6]=new nlobjSearchColumn('internalid');
		
		var currencySearch = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,null,column);
		if(currencySearch){
			for(var i_indx=0;i_indx < currencySearch.length;i_indx++){
			var id=currencySearch[i_indx].getValue('internalid');
			var mnth=currencySearch[i_indx].getValue('custrecord_pl_currency_exchange_month');
			var yr=currencySearch[i_indx].getText('custrecord_pl_currency_exchange_year');
			
			var mnt_lat_date= new Date(yr, mnth, 0);
			var rec=nlapiLoadRecord('customrecord_pl_currency_exchange_rates',id);
			nlapiLogExecution('audit','accnt name:- '+mnth);
			nlapiLogExecution('audit','year:- '+yr);
			mnt_lat_date=nlapiDateToString(mnt_lat_date);
			nlapiLogExecution('audit','year:- '+mnt_lat_date);
			var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
			nlapiLogExecution('audit','year:- '+rate);
				rec.setFieldValue('custrecord_pl_gbp_revenue',rate);
						nlapiSubmitRecord(rec);
						nlapiLogExecution('debug','id:- '+id);
					//gbp_rev_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_revenue'),
						//gbp_cost_rate : currencySearch[i_indx].getValue('custrecord_pl_gbp_cost_rate'),
						
						
				
				//if(_logValidation(a_dataVal))
				//dataRows.push(a_dataVal);
				
				
			}
		}

		
	
	
}	
	




	/*
			//nlapiLogExecution('audit','accnt name:- '+s_mont);
			//nlapiLogExecution('audit','accnt name:- '+s_year_);
			var mnt_lat_date=getLastDate(s_mont,s_year_);
			//var mnt_lat_date= new Date(s_year_, 1, 0);
			//mnt_lat_date=nlapiDateToString(mnt_lat_date);
			var rate=nlapiExchangeRate('GBP', 'INR', mnt_lat_date);
			nlapiLogExecution('audit','accnt name:- '+mnt_lat_date);
			//nlapiLogExecution('audit','accnt name:- '+rate);
			
		*/

function getLastDate(month,year)
{
	var date='';
	if(month == 'January')
		date = "1/31/"+year;
	if(month == 'February')
		date = "2/28/"+year;
	if(month == 'March')
		date = "3/31/"+year;
	if(month == 'April')
		date = "4/30/"+year;
	if(month == 'May')
		date = "5/31/"+year;
	if(month == 'June')
		date = "6/30/"+year;
	if(month == 'July')
		date = "7/31/"+year;
	if(month == 'August')
		date = "8/31/"+year;
	if(month == 'September')
		date = "9/30/"+year;
	if(month == 'October')
		date = "10/31/"+year;
	if(month == 'November')
		date = "11/30/"+year;
	if(month == 'December')
		date = "12/31/"+year;
	
	return date;
}

//Get the complete month name
function getMonthCompleteName(month){
	var s_mont_complt_name = '';
	if(month == 'Jan')
		s_mont_complt_name = 'January';
	if(month == 'Feb')
		s_mont_complt_name = 'February';
	if(month == 'Mar')
		s_mont_complt_name = 'March';
	if(month == 'Apr')
		s_mont_complt_name = 'April';
	if(month == 'May')
		s_mont_complt_name = 'May';
	if(month == 'Jun')
		s_mont_complt_name = 'June';
	if(month == 'Jul')
		s_mont_complt_name = 'July';
	if(month == 'Aug')
		s_mont_complt_name = 'August';
	if(month == 'Sep')
		s_mont_complt_name = 'September';
	if(month == 'Oct')
		s_mont_complt_name = 'October';
	if(month == 'Nov')
		s_mont_complt_name = 'November';
	if(month == 'Dec')
		s_mont_complt_name = 'December';
	
	return s_mont_complt_name;
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

