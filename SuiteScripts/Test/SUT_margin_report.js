/**

 Approves Invoices which are pending under particular Login User 
 
 **/
/** Author Sai Saranya
	11 May 2017
 **/

function margin_report_page(request, response)
{
	try {
		var method = request.getMethod();

		if (method == 'GET') {
			createApprovalForm(request);
		}else {
			postFormAppoved(request);
		}
	} catch (err) {
		throw  err;
		nlapiLogExecution('Error', 'P&L Page ERROR',err);
	}
}
function createApprovalForm(request)
{
	try{
	// create the form
		var form = nlapiCreateForm('P&L Report ');
		// add fields to the form
		//var log_user=nlapiGetUser();
		form.setScript('customscript1346');
		var log_user = nlapiGetUser();
		if(log_user==7905){
        log_user = 3169;
		}
		var approval_name = nlapiLookupField('employee', log_user,
		        'entityid');
		var user_name = form.addField('textfield','text');
		user_name.setDefaultValue(approval_name);
		user_name.setDisplayType('inline');		
		var practice_list = getTaggedPratice(log_user);
		var pract = form.addField('custpage_practice', 'select', 'Practice',null,null).setMandatory(true);
		pract.addSelectOption('','');
		for(var i=0;i<practice_list.length;++i){
        var n = practice_list[i].getValue('internalid');
	         pract.addSelectOption(n,practice_list[i].getValue('name'));
		}
		
		var employ = form.addField('custpage_field1', 'multiselect', 'Project',null,null).setMandatory(true);
		//employ.addSelectOption('','');
		
		form.addSubmitButton('Project Margin report');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('Error', 'createApprovalForm', err);
		throw err;
	}
}
function getTaggedPratice(log_user)
{
try{
var practice_filter = [
		        
		            [ 'custrecord_practicehead', 'anyOf', log_user ],
		                'and',
		                [ 'isinactive',
		                        'anyOf', 'F' ]
		         ];

		var practice_search_results = searchRecord('department', null, practice_filter,
		        [ new nlobjSearchColumn("name"),
		                new nlobjSearchColumn("custrecord_parent_practice"),
							new nlobjSearchColumn("internalid")]);


	return practice_search_results;
}catch(err){
	throw err;
	}
}
 

function postFormAppoved(request) {
	try {
								
			var form = nlapiCreateForm('Project margin list Triggers to mail  - Result ( Do not refresh this page )');
			var param=[];
			param['custscript5']=request.getParameter('custpage_field1' );
			var status=nlapiScheduleScript('customscript1339', 'customdeploy1',param);
			/*if(status == "INPROGRESS")
			{
				var status2=nlapiScheduleScript('customscript1339', 'customdeploy2',param);
				var scrpt=nlapiScheduleScript('customscript1339', 'customdeploy3',param);
				var scrpt=nlapiScheduleScript('customscript1339', 'customdeploy4',param);
				var scrpt=nlapiScheduleScript('customscript1339', 'customdeploy5',param);
				
			}*/
			nlapiLogExecution('DEBUG','STATUS',status);
			response.writePage(form);
		
		
		
	}     catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		
	}
	
}