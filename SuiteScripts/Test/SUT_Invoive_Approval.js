/**

 Approves Invoices which are pending under particular Login User 
 
 **/
/** Author Sai Saranya
	11 May 2017
 **/

function invoiceApprovalScreen(request, response)
{
	try {
		var method = request.getMethod();

		if (method == 'GET') {
		
			createApprovalForm(request);
		}else {
			postFormAppoved(request);
		}
	} catch (err) {
		nlapiLogExecution('Error', 'suitelet', err);
		nlapiLogExecution('Error', 'Invoice Approval Error',err);
	}
}
function createApprovalForm(request)
{
	try{
	// create the form
		var form = nlapiCreateForm('Pending Invoice ');
		var invoice_data ;
		// add fields to the form
		//var log_user=nlapiGetUser();
		var log_user = nlapiGetUser();
		if(log_user==5803 || log_user==133515 || log_user == 41571 || log_user == 7905 || log_user == 62082){
        log_user = 8177;
		}
		form.setScript('customscript_cs_invoice_approval');
		var approval_name = nlapiLookupField('employee', log_user,
		        'entityid');
		var user_name = form.addField('textfield','text');
		user_name.setDefaultValue(approval_name);
		user_name.setDisplayType('inline');
		var created_by = request.getParameter('created');
		if (created_by) {
			invoice_data=getPendingInvoiceReportofUser(log_user,created_by);
		}
		else{
			invoice_data = getPendingInvoiceReports(log_user);
			}
		var created_user=form.addField('created_user','select','Created By');
	//	var usr=form.addField('created_usr','text','USER').setDisplayType('inline');
		var user_fil=[];
		user_fil[0]=new nlobjSearchFilter('custrecord31',null,'anyof','1');
		var user_col=[];
		user_col[0]=new nlobjSearchColumn('custrecord30');
		user_col[1]=new nlobjSearchColumn('internalid');
		var user_seacrh=searchRecord('customrecord437',null,user_fil,user_col);
		created_user.addSelectOption('','');
		for(var n=0;n<user_seacrh.length;++n){
        var x = user_seacrh[n].getValue('internalid');
	         created_user.addSelectOption(x,user_seacrh[n].getText('custrecord30'));
		}
	//	nlapiLogExecution('DEBUG','user',usr);
	//	form.addButton('custpage_btn_approve','Load Date');
		var invoiceSublist = form.addSubList('custpage_invoce_list','list','Pending Invoice list');
		invoiceSublist.addMarkAllButtons();
		invoiceSublist.addRefreshButton();
		// add fields to the sublist
		invoiceSublist.addField('select','checkbox', 'select');
		var id_1=invoiceSublist.addField('invoiceid','text', 'Invoice#');
		//var id_1=parseInt(id_1);
		nlapiLogExecution('Debug','id_1',id_1);
		invoiceSublist.addField('internalid','text','InternalId').setDisplayType('hidden');
		var inv=invoiceSublist.addField('invoicelink','url','Invoice');
		//var viewUrl = nlapiResolveURL('RECORD', 'invoice', id, false);							
		//inv.setDefaultValue(viewUrl);
		inv.setLinkText('View');
		invoiceSublist.addField('posting_period','text', 'Posting Period');
		invoiceSublist.addField('invoice_status','text', 'Invoice Status');
		invoiceSublist.addField('customer','text', 'Customer');
		invoiceSublist.addField('due_date','date', 'Tran date');
		invoiceSublist.addField('project','text', 'Project Bill Type ');
     	invoiceSublist.addField('subsidiary','text', 'subsidiary ');
		invoiceSublist.addField('from_date','date', 'From Date');
		invoiceSublist.addField('to_date','date', 'to Date');
		invoiceSublist.addField('amount','currency', 'Total Amount');
		invoiceSublist.addField('creator','text', 'Created By');
		invoiceSublist.setLineItemValues(invoice_data);
		form.addSubmitButton('Approve Invoices');
		//form.addButton('custpage_btn_approve', 'Approve', 'approveInvoices');
		//form.addButton('custpage_btn_reject', 'Reject', 'rejectInvoices');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('Error', 'createApprovalForm', err);
		throw err;
	}
}
function getPendingInvoiceReportofUser(log_user,created_by) {
	try {
		
			var invoiceSearch = nlapiSearchRecord(
		        'invoice',null,[
		               new nlobjSearchFilter('custbody_invoicestatus',null,'anyof','3'),
						new nlobjSearchFilter('custbody_financemanager',null,'anyof',log_user),
						new nlobjSearchFilter('custbody_created_by_user',null,'anyof',created_by),
						new nlobjSearchFilter('mainline',null,'is','T')],
						[ new nlobjSearchColumn('tranid'),
						new nlobjSearchColumn('internalid'),
		                new nlobjSearchColumn('postingperiod'),
		                new nlobjSearchColumn('custbody_invoicestatus'),
						new nlobjSearchColumn('entity'),
		                new nlobjSearchColumn('trandate'),
                         new nlobjSearchColumn('subsidiary'),
		                new nlobjSearchColumn('altname','job'),
		                new nlobjSearchColumn('custbody_billfrom'),
						new nlobjSearchColumn('custbody_billto'),
		                new nlobjSearchColumn('approvalstatus'),
		                new nlobjSearchColumn('fxamount'),
						new nlobjSearchColumn('jobbillingtype','jobmain'),
						new nlobjSearchColumn('custbody_created_by_user')
		                ]);

		var invoice_data = [];

		if (invoiceSearch) {
		
			invoiceSearch
			        .forEach(function(result) {
				      for (var i = 0; i < invoiceSearch.length; i++) {   
						invoice_data
						         .push({
						                invoiceid : result
						                            .getValue('tranid'),
										
										internalid: result.getValue('internalid'),
										invoicelink:  nlapiResolveURL('RECORD', 'invoice', result.getValue('internalid'), false),
						                posting_period : result
						                            .getText('postingperiod'),
						                invoice_status : result
						                            .getText('custbody_invoicestatus'),
						                due_date : result
						                            .getValue('trandate'),
						                
						                project : result
						                            .getValue('jobbillingtype','jobmain'),
                                  		 subsidiary : result
						                            .getText('subsidiary'),
										customer : result
						                            .getText('entity'),
						                from_date : result
						                            .getValue('custbody_billfrom'),
										to_date : result
						                            .getValue('custbody_billto'),
						                approval_status : result
						                            .getValue('approvalstatus'),
						                amount : result
						                            .getValue('fxamount'),
										creator : result
													.getText('custbody_created_by_user')	
						               
						             });
									 
						        break;
					        }
				        });
			       }
	return invoice_data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMappedSubtierRecords', err);
		throw err;
	}
}
 
function getPendingInvoiceReports(log_user) {
	try {
		
			var invoiceSearch = nlapiSearchRecord(
		        'invoice',null,[
		               new nlobjSearchFilter('custbody_invoicestatus',null,'anyof','3'),
						new nlobjSearchFilter('custbody_financemanager',null,'anyof',log_user),
						new nlobjSearchFilter('mainline',null,'is','T')],
						[ new nlobjSearchColumn('tranid'),
						new nlobjSearchColumn('internalid'),
		                new nlobjSearchColumn('postingperiod'),
		                new nlobjSearchColumn('custbody_invoicestatus'),
						new nlobjSearchColumn('entity'),
		                new nlobjSearchColumn('trandate'),
                         new nlobjSearchColumn('subsidiary'),
		                new nlobjSearchColumn('altname','job'),
		                new nlobjSearchColumn('custbody_billfrom'),
						new nlobjSearchColumn('custbody_billto'),
		                new nlobjSearchColumn('approvalstatus'),
		                new nlobjSearchColumn('fxamount'),
						new nlobjSearchColumn('jobbillingtype','jobmain'),
						new nlobjSearchColumn('custbody2'),  //custbody_created_by_user
						new nlobjSearchColumn('custbody_created_by_user')	
					   ]);

		var invoice_data = [];

		if (invoiceSearch) {
		
			invoiceSearch
			        .forEach(function(result) {
				      for (var i = 0; i < invoiceSearch.length; i++) {   
						invoice_data
						         .push({
						                invoiceid : result
						                            .getValue('tranid'),
										
										internalid: result.getValue('internalid'),
										invoicelink:  nlapiResolveURL('RECORD', 'invoice', result.getValue('internalid'), false),
						                posting_period : result
						                            .getText('postingperiod'),
						                invoice_status : result
						                            .getText('custbody_invoicestatus'),
						                due_date : result
						                            .getValue('trandate'),
						                
						                project : result
						                            .getValue('jobbillingtype','jobmain'),
                                  		 subsidiary : result
						                            .getText('subsidiary'),
										customer : result
						                            .getText('entity'),
						                from_date : result
						                            .getValue('custbody_billfrom'),
										to_date : result
						                            .getValue('custbody_billto'),
						                approval_status : result
						                            .getValue('approvalstatus'),
						                amount : result
						                            .getValue('fxamount'),
										creator : result
													.getText('custbody_created_by_user')			
						               
						             });
									 
						        break;
					        }
				        });
			       }
	return invoice_data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMappedSubtierRecords', err);
		throw err;
	}
}
function postFormAppoved(request) {
	try {
		var linecount=request.getLineItemCount('custpage_invoce_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=request.getLineItemValue('custpage_invoce_list','select',i);
			var invoice=request.getLineItemValue('custpage_invoce_list','invoiceid',i);
			
			if(linevalue== 'T')
			{
				var recid = request.getLineItemValue('custpage_invoce_list','internalid', i);
				var rec=nlapiSubmitField('invoice',recid,['custbody_invoicestatus','approvalstatus'],['1','2']);
			//	rec.setFieldValue('custbody_invoicestatus',1);
			//	rec.setFieldValue('approvalstatus','2');
			//	nlapiSubmitRecord(rec);
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
				}
					
			var form = nlapiCreateForm('Invoice Approval - Result ( Do not refresh this page )');
			var invoiceList = form.addSubList('custpage_invoice_list', 'list',
			        'Invoice');
			invoiceList.addField('invoice', 'text', 'Invoice', 'invoice')
			        .setDisplayType('inline');
			invoiceList.setLineItemValues(resList);                                                                                                 
			response.writePage(form);
		
		
		}
	} catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		
	}
	
}

