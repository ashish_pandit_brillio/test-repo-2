/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Nov 2014     amol.sahijwani	Set account based on Expense Category or Item
 *Sourcing the Chart of accounts linked to Expenses & Items based on the selected category --- Request from Finance team Lisa & Raghavendra team ----
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
 
 function pageinit_setemp_type(type){
 if (type == 'copy' || type =='create' ||type == 'edit'){
// alert('Before Loading'+nlapiGetFieldValue('custbody_is_je_updated_for_emp_type'));
nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');  
 //  alert('After Loading'+nlapiGetFieldValue('custbody_is_je_updated_for_emp_type'));
 }
 }
function clientFieldChanged(type, name, linenum){
	if(type == 'line')
		{
			if(name == 'custcol_itemlistinje')
				{
					var itemId = nlapiGetCurrentLineItemValue(type, name);
					
					if(itemId != "")
						{
							var accountId = nlapiLookupField('item',itemId,'expenseaccount');
							
							nlapiSetCurrentLineItemValue(type, 'account', accountId);
						}
					
				}
			
			if(name == 'custcol1')
				{
					var expenseCategoryId = nlapiGetCurrentLineItemValue(type, name);
					
					if(expenseCategoryId != "")
						{
							var accountId = nlapiLookupField('expensecategory', expenseCategoryId, 'account');
							
							nlapiSetCurrentLineItemValue(type,'account',accountId);
						}
				}
			
		}
}
