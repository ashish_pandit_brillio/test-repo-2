/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 May 2017     bidhi.raj
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form){
 if(type=='view'){
	 var objUser = nlapiGetContext();
	 var i_role_id = objUser.getRole();
	 var userID = nlapiGetUser();
	// userID = parseFloat(userID);
	 var financeMngr = nlapiGetFieldValues('custbody_financemanager');
	 var invo_status = nlapiGetFieldValue('custbody_invoicestatus');
	 var created_user=nlapiGetFieldValue('custbody_created_by_user');
	 created_user = parseFloat(created_user);
	/* if(parseInt(invo_status)== 1)
	 {
		var rec=nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId())
		rec.setFieldText('status','Open');
		nlapiSubmitRecord(rec);
		form.removeButton('edit');
	 }
   if(parseInt(invo_status)== 2)
	 {
		var rec=nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId())
		rec.setFieldText('status','Rejected');
		nlapiSubmitRecord(rec);
		form.removeButton('edit');
	 }
    if(parseInt(invo_status)== 3)
	 {
		var rec=nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId())
		rec.setFieldText('status','Pending Approval');
		nlapiSubmitRecord(rec);
		
	 }*/
   
   
//   var dataRow = [];
   //for(var j=0;j<financeMngr.length;++j){
	//   dataRow.push(parseFloat(financeMngr[j]));
   //}
   //var value = dataRow.indexOf(userID);
    var filter=[];
   filter[0]=new nlobjSearchFilter('custrecord30',null,'anyof',userID);
   filter[1]=new nlobjSearchFilter('custrecord31',null,'is','1');
	var search=nlapiSearchRecord('customrecord437',null,filter,null);
   if(_logValidation(search))
   {
	   nlapiLogExecution('ERROR','userid',userID);
   }
   else if(parseInt(i_role_id)==3 )
   {
		nlapiLogExecution('ERROR','roleid',i_role_id);
   }
   else{
	   form.removeButton('edit');
	  
   }
  //var fnm= financeMngr.split('');
  /*  for(var j=0;j<fnm.length;++j){
      if(fnm[j]==userID){
        nlapiLogExecution('debug', 'fnm',fnm);
      nlapiLogExecution('debug', 'userID',userID);
         if(status == 'Pending Approval'){
       if(parseInt(userID) != parseInt(fnm[j])){
          form.removeButton('edit');
		 nlapiLogExecution('debug', 'status',status);
       }
		
	 }
      }
  } */
	var status=nlapiGetFieldValue('status');
	if(status != 'Pending Approval'){
     if(i_role_id != '3'){
       form.removeButton('edit');
	   
     }
     }
	 

   
	 /*if(i_role_id != '3' ||  parseInt(userID) != parseInt(financeMngr)){
		form.removeButton('edit');
		nlapiLogExecution('debug', 'iroleid',i_role_id);
  		
	 }*/
 }
}
//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

