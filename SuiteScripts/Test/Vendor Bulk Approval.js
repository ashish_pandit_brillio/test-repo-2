/**

 Approves Invoices which are pending under particular Login User 
 
 **/
/** Author Sai Saranya
	11 May 2017
 **/

function billApprovalScreen(request, response)
{
	try {
		var method = request.getMethod();

		if (method == 'GET') {
			createApprovalForm(request);
		}else {
			postFormAppoved(request);
		}
	} catch (err) {
		nlapiLogExecution('Error', 'suitelet', err);
		nlapiLogExecution('Error', 'Invoice Approval Error',err);
	}
}
function createApprovalForm(request)
{
	try{
	// create the form
		var form = nlapiCreateForm('Bill Approval ');
		// add fields to the form
		//var log_user=nlapiGetUser();
		var log_user = nlapiGetUser();
		if(log_user==7905){
        log_user = 5764;
		}
		var approval_name = nlapiLookupField('employee', log_user,
		        'entityid');
		var user_name = form.addField('textfield','text');
		user_name.setDefaultValue(approval_name);
		user_name.setDisplayType('inline');
		var invoice_data = getPendingBillReports(log_user);
		var invoiceSublist = form.addSubList('custpage_invoce_list','list','Pending Invoice list');
		invoiceSublist.addMarkAllButtons();
		invoiceSublist.addRefreshButton();
		// add fields to the sublist
		invoiceSublist.addField('select','checkbox', 'select');
		var id=invoiceSublist.addField('billid','text', 'Billid#');
	//	var id=parseInt(id);
		//nlapiLogExecution('Debug','id',id);
		invoiceSublist.addField('internalid','text','InternalId').setDisplayType('hidden');
		var inv=invoiceSublist.addField('invoicelink','url','Bill');
		//var viewUrl = nlapiResolveURL('RECORD', 'invoice', id, false);							
		//inv.setDefaultValue(viewUrl);
		inv.setLinkText('View');
		invoiceSublist.addField('posting_period','text', 'Posting Period');
		invoiceSublist.addField('approval_status','text', 'Bill Status');
		invoiceSublist.addField('customer','text', 'Customer');
		invoiceSublist.addField('due_date','date', 'Tran date');
		invoiceSublist.addField('project','text', 'Project Bill Type ');
     	invoiceSublist.addField('subsidiary','text', 'subsidiary ');
		//invoiceSublist.addField('from_date','date', 'From Date');
		//invoiceSublist.addField('to_date','date', 'to Date');
		invoiceSublist.addField('vendor_invnum','text', 'Vendor Invoice Number');
		invoiceSublist.addField('amount','currency', 'Total Amount');
		invoiceSublist.setLineItemValues(invoice_data);
		form.addSubmitButton('Approve Bills');
		//form.addButton('custpage_btn_approve', 'Approve', 'approveInvoices');
		//form.addButton('custpage_btn_reject', 'Reject', 'rejectInvoices');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('Error', 'createApprovalForm', err);
		throw err;
	}
}

 
function getPendingBillReports(log_user) {
	try {
			var invoiceSearch = nlapiSearchRecord(
		        'vendorbill',null,[
		               new nlobjSearchFilter('approvalstatus',null,'anyof','1'),
						new nlobjSearchFilter('custbody_financemanager',null,'anyof',log_user),
						new nlobjSearchFilter('entity',null,'anyof',['711','131037']),
						new nlobjSearchFilter('mainline',null,'is','T')],
						[ new nlobjSearchColumn('transactionnumber'),
						new nlobjSearchColumn('internalid'),
		                new nlobjSearchColumn('postingperiod'),
		                new nlobjSearchColumn('approvalstatus'),
						new nlobjSearchColumn('entity'),
		                new nlobjSearchColumn('trandate'),
                         new nlobjSearchColumn('subsidiary'),
		                new nlobjSearchColumn('altname','job'),
		                new nlobjSearchColumn('custbody_billfrom'),
						new nlobjSearchColumn('custbody_billto'),
		                new nlobjSearchColumn('custbody_invoicenumber'),
		                new nlobjSearchColumn('fxamount'),
						new nlobjSearchColumn('jobbillingtype','jobmain')
		                ]);

		var invoice_data = [];

		if (invoiceSearch) {
			invoiceSearch
			        .forEach(function(result) {
				      for (var i = 0; i < invoiceSearch.length; i++) {   
						invoice_data
						         .push({
						                billid : result
						                            .getValue('transactionnumber'),
										
										internalid: result.getValue('internalid'),
										invoicelink:  nlapiResolveURL('RECORD', 'vendorbill', result.getValue('internalid'), false),
						                posting_period : result
						                            .getText('postingperiod'),
						                approval_status : result
						                            .getText('approvalstatus'),
						                due_date : result
						                            .getValue('trandate'),
						                
						                project : result
						                            .getValue('jobbillingtype','jobmain'),
                                  		 subsidiary : result
						                            .getText('subsidiary'),
										customer : result
						                            .getText('entity'),
						                from_date : result
						                            .getValue('custbody_billfrom'),
										to_date : result
						                            .getValue('custbody_billto'),
						                vendor_invnum : result
						                            .getValue('custbody_invoicenumber'),
						                amount : result
						                            .getValue('fxamount')
						               
						             });
									 
						        break;
					        }
				        });
			       }
	return invoice_data;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getMappedSubtierRecords', err);
		throw err;
	}
}
function postFormAppoved(request) {
	try {
		var linecount=request.getLineItemCount('custpage_invoce_list');
		var resList=[];
		for(var i=1;i<=linecount;i++)
		{
			var linevalue=request.getLineItemValue('custpage_invoce_list','select',i);
			var invoice=request.getLineItemValue('custpage_invoce_list','billid',i);
			if(linevalue== 'T')
			{
				var recid = request.getLineItemValue('custpage_invoce_list','internalid', i);
				/*var rec=nlapiLoadRecord('invoice',recid);
				rec.setFieldValue('custbody_invoicestatus',1);
				rec.setFieldValue('approvalstatus','2');
				nlapiSubmitRecord(rec);*/ 
              if(_logValidation(recid))
                {
				var recid = nlapiSubmitField('vendorbill',recid,['approvalstatus'],['2']);
				nlapiLogExecution('debug', 'invoice', recid);
			
			resList.push({
					    invoice : invoice
					   
					});
				}
            }	
			var form = nlapiCreateForm('Bill Approval - Result ( Do not refresh this page )');
			var invoiceList = form.addSubList('custpage_invoice_list', 'list',
			        'Invoice');
			invoiceList.addField('invoice', 'text', 'Invoice', 'invoice')
			        .setDisplayType('inline');
			invoiceList.setLineItemValues(resList);
			response.writePage(form);
		
		
		}
	} catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		
	}
	
}
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
