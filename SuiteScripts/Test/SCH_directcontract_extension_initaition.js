/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jun 2017     Sai.Vannamareddy
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
 var arr_Email_cc = [];
 function contractExtension(type) {
	 try {
		 var a_table_data = new Array();
		 a_table_data.push(["Employee ID", "Employee Name", "Notification", "Error Code", "Error Message"])
		 var b_err_flag = false;

		 var columns = new Array();
		 columns[0] = new nlobjSearchColumn('firstname');
		 columns[1] = new nlobjSearchColumn('email');
		 columns[2] = new nlobjSearchColumn('email', 'custentity_reportingmanager');
		 columns[3] = new nlobjSearchColumn('firstname', 'custentity_reportingmanager');
		 columns[4] = new nlobjSearchColumn('internalid');
		 columns[5] = new nlobjSearchColumn('custentity_probationenddate');
		 columns[6] = new nlobjSearchColumn('custentity_reportingmanager');
		 columns[7] = new nlobjSearchColumn('custentity_future_termination_date', 'custentity_reportingmanager');
		 columns[8] = new nlobjSearchColumn('internalid', 'custentity_reportingmanager');
		 columns[9] = new nlobjSearchColumn('custentity_fusion_empid');
		 columns[10] = new nlobjSearchColumn('custentity_emp_hrbp');
		 columns[11] = new nlobjSearchColumn('custentity_reportingmanager', 'custentity_reportingmanager');
		 columns[12] = new nlobjSearchColumn('email', 'custentity_emp_hrbp');
		 
	 
		 //Search Employees with probation end date ends in 10 days
		  
		 var emp_search = nlapiSearchRecord('employee', 'customsearch_contract_extension10days', null, columns);
 
		 
		  // Search Employees with probation end date ends in 3 days
		  
		 var emp_search2 = nlapiSearchRecord('employee', 'customsearch_contract3days', null, columns);
		
		 nlapiLogExecution('debug', 'emp_search.length', emp_search.length);
		 if (isNotEmpty(emp_search)) {
			 
 
			 for (var i = 0; i < emp_search.length; i++) {
				 try {
					arr_Email_cc=[];
					arr_Email_cc.push('japnit.sethi@brillio.com');
					arr_Email_cc.push('waseem.pasha@brillio.com');
					 var rm_email = emp_search[i].getValue('email', 'custentity_reportingmanager');
					 var rm_firstname = emp_search[i].getValue('firstname', 'custentity_reportingmanager');
					 var i_Employee_Internal_id = emp_search[i].getId();
					 nlapiLogExecution('debug', 'i_Employee_Internal_id==', i_Employee_Internal_id);
					 nlapiSubmitField('employee', i_Employee_Internal_id, 'custentity_prior_confirmation', 'F');//// added on 18 may 2021
					 var s_Hrbp_email = emp_search[i].getValue('email', 'custentity_emp_hrbp');
					 if (s_Hrbp_email) {
						 arr_Email_cc.push(s_Hrbp_email);
					 }

					 //If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
					 if (emp_search[i].getValue('custentity_future_termination_date', 'custentity_reportingmanager')) {
						 var rm_rmRecord = nlapiLoadRecord('employee', emp_search[i].getValue('custentity_reportingmanager','custentity_reportingmanager'));
						 rm_email = rm_rmRecord.getValue('email');
						 rm_firstname = rm_email.getValue('firstname');
					 }
					 sendServiceMails(
						 emp_search[i].getValue('firstname'),
						 rm_email,
						 rm_firstname,
						 emp_search[i].getValue('internalid'),
						 emp_search[i].getValue('custentity_probationenddate'),
						 emp_search[i].getValue('custentity_reportingmanager'),
						 emp_search[i].getId());
				 }
				 catch (err) {
					 b_err_flag = true;
					 var a_row_data = new Array();
					 a_row_data.push(emp_search[i].getId());
					 a_row_data.push(emp_search[i].getValue('entityid'));
					 a_row_data.push("Prior_10days");
					 a_row_data.push(err.code);
					 a_row_data.push(err.message);
					 a_table_data.push(a_row_data);
 
				 }
			 }
		 }
 
		 if (isNotEmpty(emp_search2)) {
			 nlapiLogExecution('debug', 'emp_search.length', emp_search2.length);
			 for (var i = 0; i < emp_search2.length; i++) {
				 try {
					arr_Email_cc=[];
					arr_Email_cc.push('japnit.sethi@brillio.com');
					arr_Email_cc.push('waseem.pasha@brillio.com');
					 var rm_email = emp_search2[i].getValue('email', 'custentity_reportingmanager');
					 var rm_firstname = emp_search2[i].getValue('firstname', 'custentity_reportingmanager');
					 var i_Employee_Internal_id = emp_search2[i].getId();
					 nlapiLogExecution('debug', 'i_Employee_Internal_id==', i_Employee_Internal_id);
					 nlapiSubmitField('employee', i_Employee_Internal_id, 'custentity_prior_confirmation', 'F');//// added on 18 may 2021
					 var s_Hrbp_email = emp_search2[i].getValue('email', 'custentity_emp_hrbp');
					 if (s_Hrbp_email) {
						 arr_Email_cc.push(s_Hrbp_email);
 
					 }
	 
					 // If the employee's reporting manager is serving notice period then set rm_email to his reporting manager's email ID
					 if (emp_search2[i].getValue('custentity_future_termination_date', 'custentity_reportingmanager')) {
						 var rm_rmRecord=  nlapiLoadRecord('employee', emp_search2[i].getValue('custentity_reportingmanager', 'custentity_reportingmanager'));
						 nlapiLogExecution('debug', 'rmRecord', rm_rmRecord);
						 rm_email = rm_rmRecord.getValue('email');
						 rm_firstname = rm_email.getValue('firstname');
						 nlapiLogExecution('debug', 'insiderm_email', rm_email);
						 nlapiLogExecution('debug', 'insiderm_firstname', rm_firstname);
					 }
					 sendServiceMails2(
						 emp_search2[i].getValue('firstname'),
						 rm_email,
						 rm_firstname,
						 emp_search2[i].getValue('internalid'),
						 emp_search2[i].getValue('custentity_probationenddate'),
						 emp_search2[i].getValue('custentity_reportingmanager'));
 
				 }
				 catch (err) {
					 b_err_flag = true;
					 var a_row_data = new Array();
					 a_row_data.push(emp_search[i].getId());
					 a_row_data.push(emp_search[i].getValue('entityid'));
					 a_row_data.push("Prior_3days");
					 a_row_data.push(err.code);
					 a_row_data.push(err.message);
					 a_table_data.push(a_row_data);
				 }
			 }
		 }
		 if (b_err_flag) {
			 Send_Exception_Mail(a_table_data);
		 }
	 }
	 catch (err) {
		 nlapiLogExecution('error', 'Main', err);
	 }
 }
 
 function sendServiceMails(firstName, reporting_mng_, reporting_mng_first_name, employee_internal_id, probation_end_date, reporting_mng_id) {
 
	 try {
		 //PDF that needs to be attached
		 var mailTemplate = serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date);
		 nlapiLogExecution('debug', 'checkpoint', reporting_mng_email);
		// nlapiLogExecution('debug', 'mailTemplate', mailTemplate.MailBody);
		 nlapiLogExecution('debug', 'arr_Email_cc', JSON.stringify(arr_Email_cc));
		 nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody, arr_Email_cc, null, { entity: reporting_mng_id }, null, null);
 
	 }
	 catch (err) {
		 nlapiLogExecution('error', 'sendServiceMails', err);
		 throw err;
	 }
 }
 
 function sendServiceMails2(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, reporting_mng_id) {
 
	 try {
		 //PDF that needs to be attached
		 var mailTemplate = serviceTemplate2(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date);
		 nlapiLogExecution('debug', 'checkpoint', reporting_mng_email);
	//	 nlapiLogExecution('debug', 'mailTemplate', mailTemplate.MailBody);
		 nlapiLogExecution('debug', 'arr_Email_cc', JSON.stringify(arr_Email_cc));
		 nlapiSendEmail(10730, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody, arr_Email_cc, null, { entity: reporting_mng_id }, null, null);
 
	 }
	 catch (err) {
		 nlapiLogExecution('error', 'sendServiceMails', err);
		 throw err;
	 }
 }
 function serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date) {
 
	 var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1216&deploy=1&compid=3883006&h=e7869237e2e5e37359ae';
	 feedbackFormUrl += '&custparam_employee_id=' + employee_internal_id + '&feedback_record_id=';
	 nlapiLogExecution('debug', 'feedbackFormUrl', feedbackFormUrl);
 
	 var htmltext = '';
 
	 htmltext += '<table border="0" width="100%"><tr>';
	 htmltext += '<td colspan="4" valign="top">';
 
	 htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
	 //This is to bring to your notice that Preetha’s 
	 htmltext += '<p> This is to bring to your notice that ' + firstName + ' contract period is due for renewal on  ' + probation_end_date + '. Please click this <a href  ="' + feedbackFormUrl + '" >Link</a> and provide your inputs with the contract extension duration. The system will trigger a contract extension email to your reportee accordingly.</p>';
 
 
 
	 htmltext += '<p>Thanks & Regards,</p>';
	 htmltext += '<p>Team HR</p>';
 
 
	 htmltext += '</td></tr>';
	 htmltext += '</table>';
	 htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	 htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	 htmltext += '<tr>';
	 htmltext += '<td align="right">';
	 htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
	 htmltext += '</td>';
	 htmltext += '</tr>';
	 htmltext += '</table>';
	 htmltext += '</body>';
	 htmltext += '</html>';
 
	 return {
		 MailBody: htmltext,
		 MailSubject: "Direct Contract Extension Feedback Initiation"
	 };
 }
 
 
 function serviceTemplate2(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date) {
 
	 var feedbackFormUrl = 'https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1216&deploy=1&compid=3883006&h=e7869237e2e5e37359ae';
	 feedbackFormUrl += '&custparam_employee_id=' + employee_internal_id + '&feedback_record_id=';
	 nlapiLogExecution('debug', 'feedbackFormUrl', feedbackFormUrl);
 
	 var htmltext = '';
 
	 htmltext += '<table border="0" width="100%"><tr>';
	 htmltext += '<td colspan="4" valign="top">';
 
	 htmltext += '<p>Hi  ' + reporting_mng_first_name + ',</p>';
 
	 htmltext += '<p> This is to bring to your notice that ' + firstName + ' contract period is due for renewal on  ' + probation_end_date + '. Please click this <a href  ="' + feedbackFormUrl + '" >Link</a> and provide your inputs with the contract extension duration. The system will trigger a contract extension email to your reportee accordingly.</p>';
	 htmltext += '<p> Kindly ingnore if you already submitted the feedback.<p>';
	 htmltext += '<p>Thanks & Regards,</p>';
	 htmltext += '<p>Team HR</p>';
 
 
	 htmltext += '</td></tr>';
	 htmltext += '</table>';
	 htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	 htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	 htmltext += '<tr>';
	 htmltext += '<td align="right">';
	 htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
	 htmltext += '</td>';
	 htmltext += '</tr>';
	 htmltext += '</table>';
	 htmltext += '</body>';
	 htmltext += '</html>';
 
	 return {
		 MailBody: htmltext,
		 MailSubject: "Direct Contract Extension Feedback Initiation"
	 };
 }
 function Send_Exception_Mail(a_table_data) {
	 try {
		 var context = nlapiGetContext();
		 var s_DeploymentID = context.getDeploymentId();
		 var s_ScriptID = context.getScriptId();
		 var s_Subject = 'Issue on direct_Contract-extension_trigger';
		 var s_Body = 'This is to inform that direct_Contract-extension_trigger notification is having an issue and System is not able to send the email notification to employees.';
		 s_Body += '<br/>Issue: Code: ' + error.code + ' Message: ' + error.message;
		 s_Body += '<br/>Script: ' + s_ScriptID;
		 s_Body += '<br/>Deployment: ' + s_DeploymentID;
		 s_Body += '<br/><br/><table width="100%" border="1" cellspacing="1" cellpadding="1">';
		 for (var i = 0; i < a_table_data.length; i++) {
			 s_Body += '<tr>';
			 s_Body += '<td>' + a_table_data[i][0] + '</td>'
			 s_Body += '<td>' + a_table_data[i][1] + '</td>'
			 s_Body += '<td>' + a_table_data[i][2] + '</td>'
			 s_Body += '<td>' + a_table_data[i][3] + '</td>'
			 s_Body += '<td>' + a_table_data[i][4] + '</td>'
			 s_Body += '</tr>';
		 }
		 s_Body += '</table>';
		 s_Body += '<br/>Please have a look and do the needfull.';
		 s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';
 
		 s_Body += '<br/><br/>Regards,';
		 s_Body += '<br/>Digital Office';
		 nlapiSendEmail(442, "netsuite.support@brillio.com", s_Subject, s_Body, null, null, null, null);
	 }
	 catch (err) {
		 nlapiLogExecution('error', ' Send_Exception_Mail', err.message);
 
	 }
 }
 
 
 
 
 
 
 