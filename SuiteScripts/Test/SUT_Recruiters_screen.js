// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SUT_Recruiters_screen.js
	Author:		 Rakesh K 
	Company:	 Brillio
	Date:		 10-01-2017
	Version:	 1.0
	Description: Script is uses for create user interface for recruiters


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

var a_vendor_name = new Array();
function suiteletFunction_create_recruiters(request, response){

	
	try
	{
		
		//var form_obj = nlapiCreateForm("Recruiters Form");
		
		if (request.getMethod() == 'GET'){
			
			// Get logged-in user
			var objUser = nlapiGetContext();
			var o_values  = new Object();
			var values = new Object();
			var o_values_create = new Object();
			var i_employee_id = objUser.getUser();
			//nlapiLogExecution('debug', 'i_employee_id', i_employee_id);
			var s_role	=	getRoleName(i_employee_id, objUser.getRole());
			//nlapiLogExecution('debug', 'Role', objUser.getRole());
			var i_recID = request.getParameter('pid'); 
			var recmode = request.getParameter('edit'); 
			
			//nlapiLogExecution('debug', 'i_recID', i_recID);
			if(_logValidation(i_recID)){
				var recid = nlapiDecrypt (i_recID, 'base64');
				//nlapiLogExecution('debug', 'recid', recid);
				
			}
			
			var o_employee_data	=	nlapiLookupField('employee', i_employee_id, ['firstname', 'lastname', 'employeestatus']);
			var s_new_request_url	=	nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			var s_my_request_url = nlapiResolveURL('SUITELET', 'customscript_sut_my_request_form', 'customdeploy1');
			var s_search_URL = nlapiResolveURL('SUITELET', 'customscript_sut_searchvendorinformation', 'customdeploy1');
			var s_vendor_URL = nlapiResolveURL('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1');
			var today = getDate();
			values['s_search_URL'] = s_search_URL;
			
			var today = getDate();
			
			if(_logValidation(today)){
				values['d_date'] = today;
			}else{
				values['d_date']  = '';
			}
			
			var s_federal_id_alloted = getExistingFederalID();
			
			 values['federal_id'] = s_federal_id_alloted;
			 values['contact_person'] = '';
			 values['signing_authority'] = '';
			//values['phone_number'] = '';
			 values['fax_number'] = '';
			 values['contract_type'] ='';
			 //values['vendor_name'] = '';
			 values['status_recruiters'] = '';
			 values['netsuite_role'] = '';
			 values['vendor_usa'] = '';
			 values['need_follow_up_by_contracts'] = '';
			 values['zip_code'] = '';
			 values['_city'] = '';
			 values['state_name'] = '';
			 values['address_2'] = '';
			 values['address_line'] ='';
			 values['email_for_requirements']= '';
			 values['email_for_administration']= '';
			 values['initiator_recruiter_name'] = '';
					 //values['d_date'] = '';
			values['customer_options']  = '';
			values['username']	=	o_employee_data.firstname + ' ' + o_employee_data.lastname;
			values['role']		=	s_role;
			values['new_request_url']	=	s_new_request_url;
			values['my_requests_url']	=	s_my_request_url;
			values['script-id'] =1136;
			values['button_name']	=	o_values.button_name;
			values['v_name_list'] = a_vendor_name;
			values['initiator_recruiter_name'] = "'" +  o_employee_data.firstname + ' ' + o_employee_data.lastname + "'";
	    
			var o_json	=	getCustomerProjectJSON();
			//nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
			
			var o_json_state = getStateList();
			
			var strStateOptions= getStateDropdown(o_json_state);
			
			var strCustomerOptions	=	getCustomerDropdown(o_json);
			//o_values.button_name	=	'save_tr';
			
			var o_jsonvendor	=	getvendorJSON();
			//nlapiLogExecution('Debug', 'o_jsonvendor', JSON.stringify(o_jsonvendor));
			
			var strVendorOptions = getvendorDropdown(o_jsonvendor);
			//nlapiLogExecution('Debug', 'strVendorOptions', JSON.stringify(strVendorOptions));
			
			//nlapiLogExecution('Debug', 'strCustomerOptions', strCustomerOptions);
			values['customer-project-json']	=	JSON.stringify(o_json);
			values['customer_options'] = strCustomerOptions;
			values['vendor_name'] =strVendorOptions;
			values['state_name'] = strStateOptions;
			
			values['url_new_vendor_screen'] = s_vendor_URL;
		  //var file = nlapiLoadFile(328100);   //load the HTML file
			var file = nlapiLoadFile(328100); 
			//var file = nlapiLoadFile(335227);
			
			var contents = file.getValue();    //get the contents
		
			 //nlapiLogExecution('debug', 'if contents', contents);
			 contents = replaceValues(contents,values);
			 response.write(contents);
			
			  //form.setScript('customscript_cli_validate_rec_field');
			    
			
		}
		
		if (request.getMethod() == 'POST'){
		
			 var feedback = new Object();
			 
			/* var s_button_clicked = '';
			 if(request.getParameter('save_tr') != null){
					s_button_clicked	=	'SAVE_TRAVEL_REQUEST';
					//nlapiLogExecution('debug', 'contents', s_button_clicked);
				}
			else if(request.getParameter('edit_tr') != null && view_mode == false){
					s_button_clicked	=	'EDIT_TRAVEL_REQUEST';
				}		  
			  */
			 
			  feedback['Response1'] = request.getParameter('vendorname'); // vendor name 
			  feedback['Response2'] = request.getParameter('federalid'); //federalid
			  feedback['Response3'] = request.getParameter('contactperson'); //contactperson
			  feedback['Response4'] = request.getParameter('signingauthority');//signingauthority
			  feedback['Response5'] = request.getParameter('emailforadmin'); //emailforadmin
			  feedback['Response6'] = request.getParameter('emailforreq'); //emailforreq
			  feedback['Response7'] = request.getParameter('phonenumber'); //phonenumber
			  feedback['Response8'] = request.getParameter('faxnumber'); //faxnumber
			  feedback['Response9'] = request.getParameter('contracttype'); //contracttype
			  feedback['Response10'] = request.getParameter('address1'); //address1
			  feedback['Response11'] = request.getParameter('address2'); //address2
			  feedback['Response12'] = request.getParameter('statename'); //state
			  feedback['Response13'] = request.getParameter('city'); //city
			  feedback['Response14'] = request.getParameter('zipcode'); //zipcode
			 // feedback['Response15'] = request.getParameter('field15');
			  feedback['Response16'] = request.getParameter('field16');
			  feedback['Response17'] = request.getParameter('field17');
			  feedback['Response18'] = request.getParameter('initiator'); //initiator
			  feedback['Response19'] = request.getParameter('clientname_contact');// client 
			  feedback['Response20'] = request.getParameter('departure_date');
			  feedback['Response21'] = request.getParameter('clientnametext');
			  //feedback['Comments1'] = request.getParameter('txtComments1');
			 // var status = saveRequest(feedback);
			  //nlapiLogExecution('debug', 'feedbackResponse1', feedback['Response1']);
			 var o_status = saveTravelRequestDetails(request);
			 response.sendRedirect('SUITELET', 'customscript_sut_recruiters_screen', 'customdeploy1', false);
			 nlapiRequestURL('https://' + request.getHeader('Host'));
			
			}
	
		}
	catch(err){
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
	}

}
function saveTravelRequestDetails(request){
	
	//nlapiLogExecution('debug', 'contents', request);
	//if(s_button == 'SAVE_TRAVEL_REQUEST' || s_button == 'EDIT_TRAVEL_REQUEST')
	{
		 var s_federal_id = request.getParameter('federalid');
		 s_federal_id = s_federal_id.toString();
		 
		 var s_federal_substr = s_federal_id.substr(0,2);
		 s_federal_substr = s_federal_substr+'-'+s_federal_id.substr(2,s_federal_id.length);
			 
		  var feedback = new Object();
		  feedback['Response1'] = request.getParameter('vendorname'); // vendor name 
		  feedback['Response2'] = s_federal_substr; //federalid
		  feedback['Response3'] = request.getParameter('contactperson'); //contactperson
		  feedback['Response4'] = request.getParameter('signingauthority');//signingauthority
		  feedback['Response5'] = request.getParameter('emailforadmin'); //emailforadmin
		  feedback['Response6'] = request.getParameter('emailforreq'); //emailforreq
		  feedback['Response7'] = request.getParameter('phonenumber'); //phonenumber
		  feedback['Response8'] = request.getParameter('faxnumber'); //faxnumber
		  feedback['Response9'] = request.getParameter('contracttype'); //contracttype
		  feedback['Response10'] = request.getParameter('address1'); //address1
		  feedback['Response11'] = request.getParameter('address2'); //address2
		  feedback['Response12'] = request.getParameter('statename'); //state
		  feedback['Response13'] = request.getParameter('city'); //city
		  feedback['Response14'] = request.getParameter('zipcode'); //zipcode
		  //feedback['Response15'] = request.getParameter('departure_date');
		  feedback['Response16'] = request.getParameter('field16');
		  feedback['Response17'] = request.getParameter('field17');
		  feedback['Response18'] = request.getParameter('initiator'); //initiator
		  feedback['Response19'] = request.getParameter('clientname_contact');
		 // feedback['Response20'] = request.getParameter('departure_date');
		  feedback['Response21'] = request.getParameter('clientnametext');
		  nlapiLogExecution('debug', 'feedback.Response21', feedback['Response21']);
		 // nlapiLogExecution('debug', 'feedback.Response15', feedback['Response15']);
			//var s_save_tr_mode	=	s_button == 'SAVE_TRAVEL_REQUEST'?'NEW':'EDIT';
			
			var o_status = saveReqdetail(feedback);
		
			return o_status;
		}
}



// create NEW RECORD FOR REQ SCREEN 

function saveReqdetail(feedback)
{
	try
	{
		// Get logged-in user
		var objUser = nlapiGetContext();
		var today = getDate();
		var i_employee_id = objUser.getUser();
		//nlapiLogExecution('debug', 'i_employee_id', i_employee_id);
		var recTR	=	null;
		
		var feedback_form	=	nlapiCreateRecord('customrecord_recruiters_details');
		nlapiLogExecution('debug', 'feedback_form', feedback_form);
		feedback_form.setFieldValue('custrecord_vendor_name', feedback.Response1); // vendor name 
		feedback_form.setFieldValue('custrecord_federal_id', feedback.Response2);
		feedback_form.setFieldValue('custrecord_contact_person', feedback.Response3);
		feedback_form.setFieldValue('custrecord_signing_authority', feedback.Response4);
	    feedback_form.setFieldValue('custrecord_email_for_administration', feedback.Response5);
	    feedback_form.setFieldValue('custrecord_email_for_requirements', feedback.Response6);
	    feedback_form.setFieldValue('custrecord_phone_number', feedback.Response7);
	    feedback_form.setFieldValue('custrecord_fax_number', feedback.Response8);
	    feedback_form.setFieldValue('custrecord_netsuite_role',i_employee_id);
	    feedback_form.setFieldValue('custrecord_approval_status_recruiters',4);
		  
		  if(feedback.Response9 == 'onepersoncorp'){
			  feedback_form.setFieldValue('custrecord_contract_type', 1);
		  }
		  if(feedback.Response9 == 'singleclient'){
			  feedback_form.setFieldValue('custrecord_contract_type', 2);
		  }
		  if(feedback.Response9 == 'multipleclientcontract'){
			  feedback_form.setFieldValue('custrecord_contract_type', 3);
		  }
		  if(feedback.Response9 == 'referraldeal'){
			  feedback_form.setFieldValue('custrecord_contract_type', 4);
		  }
		  nlapiLogExecution('debug', 'state', feedback.Response12);
		  nlapiLogExecution('debug', 'phone number', feedback.Response7);
		 // nlapiLogExecution('debug', 'fax number', feedback.Response8);
		  feedback_form.setFieldValue('custrecord_address_line', feedback.Response10);
		  feedback_form.setFieldValue('custrecord_address_2', feedback.Response11);
		  feedback_form.setFieldValue('custrecord_state_name', feedback.Response12);
		  feedback_form.setFieldValue('custrecord_city', feedback.Response13);
		  feedback_form.setFieldValue('custrecord_zip_code', feedback.Response14);
		  if(feedback.Response16 == 'on'){
			  feedback_form.setFieldValue('custrecord_need_follow_up_by_contracts', 'T');
		  }
		  if(feedback.Response17 == 'on'){
			  feedback_form.setFieldValue('custrecord_vendor_usa', 'T');
			  //feedback_form.setFieldValue('custrecord_need_follow_up_by_contracts', 'T');
		  }
		  //nlapiLogExecution('debug', 'feedback.Response19', feedback.Response19);
		 
		  feedback_form.setFieldValue('custrecord_initiator_recruiter_name', feedback.Response18);
		  var str = feedback.Response21;
		  var a_client = new Array();
		  nlapiLogExecution('debug', 'feedback.Response21 length', feedback.Response21.length);
		  for(var j=0 ; j<feedback.Response21.length; j++){
			  
			  var res = str.split(",");
			  if(res[j] != null && res[j] != '' && res[j] !='null'){
				  a_client.push(res[j]);
			  }
			  
		  }
		  nlapiLogExecution('debug', 'JSON.stringify(arr);', JSON.stringify(a_client));
		  //feedback_form.setFieldValues('custrecord_vms_customer', feedback.Response21);
		  feedback_form.setFieldValues('custrecord_vms_customer', a_client);
		  feedback_form.setFieldValue('custrecord_vms_date',today);
		  
		  if(feedback.Response16 != 'on'){ // USA vendor check box
			  
			  var checkbox1 = 'No';
		  }else{
			  var checkbox1 = 'Yes';
		  }
		  if(feedback.Response17 != 'on'){ //Need follow up by contracts
			  
			  var checkbox2 = 'No';
		  }else{
			  var checkbox2 = 'Yes';
		  }
		 //return;
         
         var rec_id = nlapiSubmitRecord(feedback_form, false,true);
         nlapiLogExecution('debug', 'Record Saved', rec_id);
		 
		 if(rec_id)
		 {
		 	nlapiAttachRecord('file',343261,'customrecord_recruiters_details',rec_id);
		 }
		 
      //   return;
         //var recURL = nlapiResolveURL('RECORD', 'customrecord_recruiters_details', rec_id, 'VIEW');
         //nlapiLogExecution('debug', 'recURL', recURL);
		//var i_rec_id = nlapiSubmitRecord(recTR, true);	
         //recURL  = 'https://system.na1.netsuite.com'+recURL;
         var subject = "Please send "+" "+ cotractType +" documents for " + "("+ feedback.Response1+").";
		
		var strVar="";
		strVar += "<html>";
		strVar += "<body>";
		strVar += "<p style=font:small-caption>";
		strVar += "";
		strVar += "Hi,<br>";
		strVar += "I have added the details for vendor ("+feedback.Response1+") requested for contract type ("+cotractType+"), please verify vendor details and add it in PLACE.<br>";
		strVar += "CONTRACTS Follow-up - "+checkbox2+"<br>";
		strVar += "Expected that resource of this vendor will work in California State - "+checkbox1+"<br><br>";
		strVar += "";
		strVar += "Thank you.";
		strVar += "</p>";
		strVar += "";
		strVar += "";
		strVar += "<\/body>";
		strVar += "<\/html>";

        nlapiSendEmail(7905, 'rakeshk@inspirria.com', subject, strVar, null, null, null, null, null, null, null);
		
		return ({'status': 1, 'message': 'Vendor Record Saved', 'rec_id': rec_id});
	}
	catch(e){
		return ({'status': -1, 'message': 'Error: ' + e.message, 'tr_id': null});
	}
}

// get VIEW Mode 

function getViewMode(i_status, i_logged_in_user, i_tr_employee_id)
{
	if(i_status == 1 && i_logged_in_user == i_tr_employee_id){
			return false;
		}
	
	return true;
}


// function for get list of customer form NetSuite Database
function getCustomerList()
{
	var filters =	new Array();
	filters[0]	=	new nlobjSearchFilter('isinactive',null,'is',"F");
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('internalid');
	columns[1]	=	new nlobjSearchColumn('altname');
	
	var searchResults	=	searchRecord('customer', null, filters, columns);
	
	var a_data	=	new Array();
	
	for(var i = 0; i < searchResults.length; i++){
			
			var s_customer_id	=	searchResults[i].getValue(columns[0]);
			var s_customer_name	=	searchResults[i].getValue(columns[1]);
			
			a_data.push({ 'customer_id': s_customer_id, 'customer_name': s_customer_name});
		}
	
	return a_data;
}
//
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,  filterExpression){

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

// get state list 

function getStateList() {
	
	var o_vendor_rec = nlapiCreateRecord('vendor');
	
	var fld_state = o_vendor_rec.getField('custentity_vendor_state');
		
	var state_options = fld_state.getSelectOptions();
	var a_data = new Array(); 
	for(var i=0; i<state_options.length; i++){
		
		var s_stateID = state_options [i].getId();
		var s_state_name = state_options [i].getText() ;
		a_data.push({ 'state_id': s_stateID, 'state_name': s_state_name});
		//nlapiLogExecution('DEBUG', state_options [i].getId() + ',' + state_options [i].getText() ); 
	}

	//nlapiLogExecution('debug','state_options',state_options);
	//var i=0;
	return a_data;
}

// END SUITELET ====================================================
// create customer JSON 
function getCustomerProjectJSON()
{
	var a_data	=	getCustomerList();
	//nlapiLogExecution('debug', 'getCustomerProjectJSON', a_data);
	var o_json	=	new Object();
	
	for(var i = 0; i < a_data.length; i++){
			
			var o_customer	=	{'id':a_data[i].s_customer_id, 'value':a_data[i].s_customer_name};
			//nlapiLogExecution('Debug', 'o_customer', JSON.stringify(o_customer));
			if(o_json[a_data[i].customer_id] != undefined)
				{
					o_json[a_data[i].customer_id].list.push(o_customer);
				}
			else
			{
				o_json[a_data[i].customer_id]	=	{'name': a_data[i].customer_name};
			}
			
		}
	//nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
	//nlapiLogExecution('debug', 'getCustomerProjectJSON', a_data);
	return a_data;
	
}


// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{
	// function is used for replace the string code which is created for different-2 purpose to display content on user interface  
	function replaceValues(content, oValues)
	{
		for(param in oValues)
			{
				// Replace null values with blank
				var s_value	=	(oValues[param] == null)?'':oValues[param];
				
				// Replace content
				content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
			}
		
		return content;
	}



}


// END OBJECT CALLED/INVOKING FUNCTION =====================================================
// get ROLE name 

function getRoleName(i_user_id, i_role_id)
{
	var a_search_results	=	nlapiSearchRecord('employee', null, [new nlobjSearchFilter('internalid', null, 'anyof', i_user_id), new nlobjSearchFilter('role', null, 'anyof', i_role_id)], new nlobjSearchColumn('role'));
	
	if(a_search_results != null && a_search_results.length == 1)
		{
			return a_search_results[0].getText('role');
		}
	else
		{
			return '';
		}
}
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions)
{
	var strOptions	=	'';
	
	if(removeBlankOptions == true)
		{
		
		}
	else
		{
			strOptions = '<option value = "">' + (s_placeholder == undefined?'':s_placeholder) + '</option>';
		}

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += '<option value = "'+a_data[i].value+'" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}
//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}

// get state drop down list

function getStateDropdown(o_json){
	var list	=	new Array();
	var strOptions	=	'';
	var i_selected_value	=	'';
	
	for(var x in o_json){
		
		strOptions += '<option value = "'+o_json[x].state_id+'">' + o_json[x].state_name + '</option>';
			/*list.push({'display': o_json[x].s_customer_name, 'value': x});
			
			for(var i = 0; i < o_json[x].list.length; i++)
				{
					//if(o_json[x].list[i].id	==	i_project_id)
						{
							i_selected_value	=	x;
						}
				}*/
			
		}
	return strOptions;
	//return getOptions(list, i_selected_value);
}
// get client drop down list

function getCustomerDropdown(o_json){
	var list	=	new Array();
	var strOptions	=	'';
	var i_selected_value	=	'';
	
	for(var x in o_json){
		
		strOptions += '<option value = "'+o_json[x].customer_id+'">' + o_json[x].customer_name + '</option>';
			/*list.push({'display': o_json[x].s_customer_name, 'value': x});
			
			for(var i = 0; i < o_json[x].list.length; i++)
				{
					//if(o_json[x].list[i].id	==	i_project_id)
						{
							i_selected_value	=	x;
						}
				}*/
			
		}
	return strOptions;
	//return getOptions(list, i_selected_value);
}

function _logValidation(value)
{
	if (value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN') 
	{
	    return true;
	}
	else 
	{
	    return false;
	}
 }
 
function getExistingFederalID()
{
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('custrecord_federal_id');
	
	var s_fed_id_available	= '[';
	var searchResults	=	searchRecord('customrecord_recruiters_details', null, null, columns);
	for(var i = 0; i < searchResults.length; i++)
	{
		if(i == searchResults.length - 1)
		{
			s_fed_id_available += '"'+searchResults[i].getValue('custrecord_federal_id')+'"';
		}
		else
		{
			s_fed_id_available += '"'+searchResults[i].getValue('custrecord_federal_id')+'",';
		}
	}
	s_fed_id_available += ']';
	return s_fed_id_available;
}

// vendor string compare from netsuite database  
function getvendorDropdown(o_json)
{
	//var list	=	new Array();
	var strOptions	=	'';
	
	//var i_selected_value	=	'';
	strOptions += '[';
	for(var x in o_json)
		{
			if(x < o_json.length-1)
			{
				a_vendor_name.push(o_json[x].vendor_name);
				//nlapiLogExecution('debug', 'o_json[x].vendor_name', o_json[x].vendor_name);
				strOptions += '"'+o_json[x].vendor_name+'",';
							
			}else{
				a_vendor_name.push(o_json[x].vendor_name);
				strOptions += '"'+o_json[x].vendor_name+'"';
			}
		
		}
	strOptions += ']';
	return strOptions;
	//return getOptions(list, i_selected_value);
}

function getvendorJSON()
{
	var a_data	=	getvendorList();
	//nlapiLogExecution('debug', 'getvendorJSON', a_data);
	var o_json	=	new Object();
	
	for(var i = 0; i < a_data.length; i++){
			
			var o_vendor	=	{'id':a_data[i].s_vendor_id, 'value':a_data[i].s_vendor_name};
			//nlapiLogExecution('Debug', 'o_customer', JSON.stringify(o_customer));
			if(o_json[a_data[i].vendor_id] != undefined)
				{
					o_json[a_data[i].vendor_id].list.push(o_vendor);
				}
			else
			{
				o_json[a_data[i].vendor_id]	=	{'name': a_data[i].vendor_name};
			}
			
		}
	//nlapiLogExecution('Debug', 'o_json', JSON.stringify(o_json));
	//nlapiLogExecution('debug', 'getvendorJSON', a_data);
	return a_data;
	
}
function getvendorList()
{
	var filters =	new Array();
	//filters[0]	=	new nlobjSearchFilter('isinactive',null,'is',"F");
	filters[0]	=	new nlobjSearchFilter('category',null,'anyof',12);
	
	var columns	=	new Array();
	columns[0]	=	new nlobjSearchColumn('internalid');
	columns[1]	=	new nlobjSearchColumn('companyname');
	columns[2]	=	new nlobjSearchColumn('isinactive');
	
	var a_data	=	new Array();
	var searchResults	=	searchRecord('vendor', null, filters, columns);
	for(var i = 0; i < searchResults.length; i++)
	{
		
		var s_vendor_id	=	searchResults[i].getValue(columns[0]);
		var s_vendor_name	=	searchResults[i].getValue(columns[1]);
		var s_vendor_inactive	=	searchResults[i].getValue(columns[2]);
		//nlapiLogExecution('debug', 's_vendor_inactive', s_vendor_inactive);
		if(s_vendor_name != '' && s_vendor_name != null){
			
			a_data.push({ 'vendor_id': s_vendor_id, 'vendor_name': s_vendor_name,'isinactive':s_vendor_inactive});
		}
		
	}
	
	return a_data;
}
// get todays date 
function getDate(){
	
	//var view_mode = false;
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd;
	} 

	if(mm<10) {
	    mm='0'+mm;
	} 

	today = mm+'/'+dd+'/'+yyyy;
	//nlapiLogExecution('debug', 'today', today);
	
	return today;
}