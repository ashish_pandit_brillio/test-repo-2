/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 July 2017     sai.vannamareddy
 *
 */
 
 //https://debugger.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1240&deploy=1&project=34413&project=69245&project=73089&project=73610&fpna=T
 //&project=34413&project=69245&project=73089&project=73610&fpna=T

function projectReport(){
 
	 var objUser = nlapiGetContext();
	 recid=nlapiGetRecordId();
	 var projectlist = nlapiGetFieldValues('custrecord34');
	 var param=[];
	 var temp_param=[];
	// projectlist=JSON.stringify(projectlist);
	if(projectlist.length>=10)
	{
	
		throw "Due to performance issue we are restricting to select more than 10 projects at a time. Please select lessthan or equal of 10 projects";
	}
	else{
	var JSON = {};
	var temp = '';
	for(i=0;i<projectlist.length;i++)
	{
		param.push(projectlist[i]);
		var value = '&project='+projectlist[i];
		temp = temp + value;
		//temp_param['project']=projectlist[i];
		nlapiLogExecution('ERROR', 'projectlist', projectlist[i]);
		
		JSON  = {
			project: projectlist[i]
		};
		temp_param.push(JSON);
	}
	
		nlapiLogExecution('ERROR', 'projectlist', param);
		temp = temp +'&fpna=T';
	temp = temp.trim();
	temp_param['fpna']='T';
	var url = nlapiResolveURL('SUITELET','customscript1240','customdeploy1');
//	var script = "win = window.open('" + url + "', 'win', 'resizable=0,scrollbars=0,width=950,height=700');";
		var finalURL = 'https://debugger.na1.netsuite.com'+ url + temp;
	//window.open("/app/site/hosting/scriptlet.nl?script=1240&deploy=1"+temp);
	nlapiSetFieldValue('custrecord35',finalURL);
	document.loaction=finalURL;
return true;

	//nlapiSetRedirectURL('SUITELET','customscript1240', 'customdeploy1', false, temp);
	//nlapiLogExecution('ERROR', 'projectlist', temp_param);
	
	}
   
 
}


//validate blank entries
/*function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}*/

