/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00            mani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void}
 */
function inACtivationTAEATrigger() {
	try {
		var context = nlapiGetContext();
		var recorId = context.getSetting('SCRIPT', 'custscript2');
      nlapiLogExecution('debug', 'Record id',recorId);
		var emprec=nlapiLoadRecord('employee',
				        recorId);
      
		//var old_inactive_value = old_record
		//		.getFieldValue('custentity_employee_inactive');
		
		var new_inactive_value = emprec.getFieldValue('custentity_employee_inactive');
			nlapiLogExecution('error', 'Activation Trigger',recorId);
	//	if (old_inactive_value != new_inactive_value
			//	&& new_inactive_value == 'T') {

			// check dependends
			//var currentRecord = nlapiGetRecordId();
			var htmltext = "";
		
			var emp_practice = nlapiLookupField('employee',recorId, 'department');
		    var practice_head = nlapiLookupField('department', emp_practice,'custrecord_practicehead');
		
			// check for expense approver
			var filter = [ [ 'approver', 'anyof', recorId ], 'and',
					[ 'custentity_employee_inactive', 'is', 'F' ] ];

			var emp_search = nlapiSearchRecord('employee', null, filter,
					[ new nlobjSearchColumn('entityid') ]);

			if (emp_search) {
				htmltext += "<p><b><u>Expense Approver for the below resource(s)</u></b></p>";
				
				htmltext += "<ol>";
				for (var i = 0; i < emp_search.length; i++) {
					htmltext += "<li>" + emp_search[i].getValue('entityid')
							+ "</li>";
							
							
				}
				htmltext += "<p><b>New Expense Approver:__________________</u></b></p>";
				htmltext += "</ol>";
			}

			// check for TA
			filter = [ [ 'timeapprover', 'anyof', recorId ], 'and',
					[ 'custentity_employee_inactive', 'is', 'F' ] ];

			emp_search = nlapiSearchRecord('employee', null, filter,
					[ new nlobjSearchColumn('entityid') ]);

			if (emp_search) {
			
				htmltext += "<p><b><u>Time Approver for the below resource(s)</u></b></p>";
				
				htmltext += "<ol>";
				for (var i = 0; i < emp_search.length; i++) {
					htmltext += "<li>" + emp_search[i].getValue('entityid')
							+ "</li>";
							
							
				}
				htmltext += "<p><b>New Time sheet Approver:__________________</u></b></p>";
				htmltext += "</ol>";
			}

			// check PM
			filter = [ [ 'custentity_projectmanager', 'anyof', recorId ],
					'and', [ 'status', 'noneOf', ['1' ] ] ];

			var job_search = nlapiSearchRecord('job', null, filter,
					[ new nlobjSearchColumn('entityid'),
					new nlobjSearchColumn('altname')]);
					
						
					

			if (job_search) {
				htmltext += "<p><b><u>Project Manager for the below Project</u></b></p>";
				htmltext += "<ol>";
				for (var i = 0; i < job_search.length; i++) {
					htmltext += "<li>" + job_search[i].getValue('entityid');
							htmltext += " " + job_search[i].getValue('altname')
							+ "</li>";
							
				}
				
				htmltext += "<p><b>New project Manager:_________________</u></b></p>";
				htmltext += "</ol>";
			}

			// check DM
			filter = [
					[ 'custentity_deliverymanager', 'anyof', recorId ],
					'and', [ 'status', 'noneOf', [ '1' ] ] ];

			job_search = nlapiSearchRecord('job', null, filter,
					[ new nlobjSearchColumn('entityid') ,
					new nlobjSearchColumn('altname')]);
					
					//job_search = nlapiSearchRecord('job', null, filter,
					//[ new nlobjSearchColumn('altname') ]);
					
					
					if (job_search) {
				htmltext += "<p><b><u>Delivery Manager for the below Project</u></b></p>";
				htmltext += "<ol>";
				for (var i = 0; i < job_search.length; i++) {
					htmltext += "<li>" + job_search[i].getValue('entityid');
							htmltext += " " + job_search[i].getValue('altname')
							+ "</li>";
							
				
							
				}
				htmltext += "<p><b>New Delivery Manager:___________________</u></b></p>";
				htmltext += "</ol>";
			}
					
					

				// check CP
				
				filter =[['custentity_clientpartner','anyof',recorId],'and',['status','noneOf',['1']]];
                job_search = nlapiSearchRecord('job',null,filter,[new nlobjSearchColumn('entityid'),
				                                                 new nlobjSearchColumn('altname')]);

				
				if (job_search){
					
					htmltext += "<p><b><u>Client Partner for the below Project</u></b></p>";
					htmltext += "<ol>";
					for (var i = 0; i < job_search.length; i++) {
					htmltext += "<li>" + job_search[i].getValue('entityid');
					htmltext += " " + job_search[i].getValue('altname')	
					+ "</li>";
							
				}
				htmltext += "<p><b>New Client Partner:________________</u></b></p>";
				htmltext += "</ol>";
			}
			

			// send emails
			if (htmltext) {
				var mailSubject = 'PM/DM/EA/TA/CP Terminated';
				var mailBody = "<p>Hi,</p>";
				mailBody += "<p>This is to inform you that <b>"
						+ emprec.getFieldValue('entityid')
						+ "</b> was terminated <b>(Last working date : "
						+ emprec.getFieldValue('custentity_lwd')
						+ ")</b>. Please inform business ops team to make the below changes in System :"
                  mailBody += "<p>Currently he/she has been tagged as below:</p>";
				mailBody += htmltext;
				mailBody += "<p>Regards,<br/>Information Systems</p>";
				
				//nlapiSendEmail(sender, recepient, subject, email_String, CC);
				
				//nlapiSendEmail('442', 'mani.v@brillio.com', subject, email_String,CC); //['mani.007@gmail.com', 'mani@yahoo.co.in']

                                nlapiLogExecution('audit', 'snd email');
				var a_emp_attachment = new Array();
    			a_emp_attachment['entity'] = '62082'; 
				nlapiSendEmail(5803,practice_head, mailSubject, mailBody,'business.ops@brillio.com','waseem.pasha@brillio.com',a_emp_attachment);
			}
		}
	//}
	catch (err) {
		nlapiLogExecution('error', 'userEventAfterSubmit', err);
	}
}
