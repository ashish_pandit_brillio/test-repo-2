/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 May 2017     sai.vannamareddy
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form){
 if(type=='view'){
	 var objUser = nlapiGetContext();
	 var i_role_id = objUser.getRole();
	 var userID = nlapiGetUser();
	 recid=nlapiGetRecordId();
	 //userID = parseFloat(userID);
	 var financeMngr = nlapiGetFieldValues('custbody_financemanager');
	 var appro_status = nlapiGetFieldValue('approvalstatus');
	 var created_user=nlapiGetFieldValue('custbody_created_by_user');
	 /*created_user = parseFloat(created_user); 
	var dataRow = [];
   for(var j=0;j<financeMngr.length;++j){
	   dataRow.push(parseFloat(financeMngr[j]));
   }
   var value = dataRow.indexOf(userID); */
    var filter=[];
   filter[0]=new nlobjSearchFilter('custrecord30',null,'anyof',userID);
   filter[1]=new nlobjSearchFilter('custrecord31',null,'is','2');
	var search=nlapiSearchRecord('customrecord437',null,filter,null);
	
   //if(((value >= 0)&&(parseInt(appro_status)==1))||((userID==created_user)&& (parseInt(appro_status)== 1))){
   if(_logValidation(search))
   {
	   nlapiLogExecution('ERROR','userid',userID);
	   //form.addButton('custpageresubmit','ReSubmit for Approval');
	  // nlapiTriggerWorkflow('vendorbill', recid , 'customworkflow_vendorbillapproval', //'workflowaction559');
   }
   else if(parseInt(i_role_id)==3 )
   {
		nlapiLogExecution('ERROR','roleid',i_role_id);
   }
   else{
	   form.removeButton('edit');
	   form.removeButton('custpageworkflow1067');
	   nlapiLogExecution('ERROR','invalid user',i_role_id);
   }
  //var fnm= financeMngr.split('');
  /*  for(var j=0;j<fnm.length;++j){
      if(fnm[j]==userID){
        nlapiLogExecution('debug', 'fnm',fnm);
      nlapiLogExecution('debug', 'userID',userID);
         if(status == 'Pending Approval'){
       if(parseInt(userID) != parseInt(fnm[j])){
          form.removeButton('edit');
		 nlapiLogExecution('debug', 'status',status);
       }
		
	 }
      }
  } */
	var status=nlapiGetFieldValue('status');
	if(status == 'Open'){
     if(i_role_id != '3'){
       form.removeButton('edit');
     }
     }
	 

   
	 /*if(i_role_id != '3' ||  parseInt(userID) != parseInt(financeMngr)){
		form.removeButton('edit');
		nlapiLogExecution('debug', 'iroleid',i_role_id);
  		
	 }*/
 }
}

//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

