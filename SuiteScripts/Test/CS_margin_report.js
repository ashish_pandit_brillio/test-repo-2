function myFieldChange(type, name)
{
	if(name == 'custpage_practice'){
	//nlapiSetFieldValue('custpage_field1','');
	var listVal = nlapiGetFieldValues('custpage_field1');
	var user=nlapiGetUser();
	
	nlapiRemoveSelectOption('custpage_field1');
	
	var pract_selected=nlapiGetFieldValue('custpage_practice');
	var valu=nlapiGetFieldValues('custpage_field1');	
	var project_list=getProjects(pract_selected);
	for(var i=0; i< project_list.length;i++)
	{
		//nlapiSetFieldValue('custpage_field1',project_list[i].getValue('internalid'));
	nlapiInsertSelectOption('custpage_field1', project_list[i].getValue('internalid'), project_list[i].getValue('companyname'), false);
	}
	}
	if(name =='custpage_field1'){
		var selected=nlapiGetFieldValues('custpage_field1');
		var arr=[];
		
		nlapiSetFieldValue('custpage_projects',selected);
		
	}
}
 function getProjects(pract_selected) {
	try {
			var project_filter = 
		        [
		            [ 'custentity_practice', 'anyOf', pract_selected ],               
		              'and',
		        [ 'status', 'anyOf', 2 ] ];

		var project_search_results = searchRecord('job', null, project_filter,
		        [ new nlobjSearchColumn("entityid"),
		                new nlobjSearchColumn("companyname"),
							new nlobjSearchColumn("internalid")]);

		nlapiLogExecution('debug', 'project count',
		        project_search_results.length);

		if (project_search_results.length == 0) {
			throw "You don't have any projects under you.";
		}
	
		return project_search_results;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
		throw err;
	}
		
	
}