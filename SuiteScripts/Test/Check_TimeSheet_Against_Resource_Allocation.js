//massUpdate('timesheet', 33267);
/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Dec 2014     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
//massUpdate('timesheet', 10561);
function massUpdateCheckData(recType, recId) {
	// Load the Time Sheet Record
	var recTS = nlapiLoadRecord(recType, recId, {recordmode: 'dynamic'});
	var recJson = JSON.stringify(recTS);
	var recNewTS = nlapiCreateRecord(recType, {recordmode: 'dynamic'});
	var old_value = 0.0;
	var new_value = 0.0;
	var changedData = "";
	try
	{
		o_TimeSheet_values = getTSFieldValues(recTS);
		for(var s_field_name in o_TimeSheet_values)
		{
			recNewTS.setFieldValue(s_field_name,o_TimeSheet_values[s_field_name]);
		}
	}
	catch(exTS)
	{
		nlapiLogExecution('ERROR', 'Timesheet: ' + recId, exTS.message);
		return;
	}
	nlapiLogExecution('AUDIT', 'Timesheet: ' + recId, 'Started');		
	var isRecordChanged = false;
			
	// Array of Subrecord names
	var a_days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
			
	var i_employee_id = recTS.getFieldValue('employee');
					
	// Get Start Date
	var d_start_date	=	new Date(recTS.getFieldValue('startdate'));
			
	// Get End Date
	var d_end_date		=	new Date(recTS.getFieldValue('enddate'));
			
	// Get Line Count
	var i_line_count = recTS.getLineItemCount('timegrid');//recTS.getLineItemCount('timegrid');
	nlapiLogExecution('DEBUG', 'Number of Lines', i_line_count);		
	var o_projects = new Object();
	
	var a_resource_allocations = getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date);
	nlapiLogExecution('DEBUG','Test', JSON.stringify(a_resource_allocations));
	var isLineItemChanged = false;
	var isRecordToBeChanged = false;
	// Loop through Line Items
	for(var i_line_indx = 1; i_line_indx <= i_line_count; i_line_indx++)
		{
			
			//nlapiLogExecution('AUDIT', 'Test', JSON.stringify(a_days_data));		
						
			recTS.selectLineItem('timegrid',i_line_indx);
			recNewTS.selectNewLineItem('timegrid');			
			for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var o_sub_record_view = recTS.viewCurrentLineItemSubrecord('timegrid', sub_record_name);
					if(o_sub_record_view)
						{
							var d_current_date = nlapiStringToDate(o_sub_record_view.getFieldValue('day'), 'date');
							var i_project_id = o_sub_record_view.getFieldValue('customer');
							var isCurrentlyBillable = o_sub_record_view.getFieldValue('isbillable');
							var currentRate = o_sub_record_view.getFieldValue('rate');
							if(currentRate == '' || currentRate == null)
								{
									currentRate = 0.0;
								}
							
							var i_item_id = o_sub_record_view.getFieldValue('item');
							var i_approval_status = o_sub_record_view.getFieldValue('approvalstatus');
							var old_hours = _correct_time(o_sub_record_view.getFieldValue('hours'));
							if(isCurrentlyBillable == 'T')
								{
									if(currentRate != '' && !isNaN(currentRate))
										{
											old_value = old_value + parseFloat(currentRate) * old_hours;
										} 
								}
							//var isBilled = o_sub_record_view.getFieldValue('isbilled');
							nlapiLogExecution('DEBUG', 'Test', 'Project: ' + i_project_id + ', Item: ' + i_item_id);			
							// Check resource allocation
							if(i_project_id != null)
								{
									// Initialise to billable:'F' by default
									o_day_data = {billable:'F', rate:0};
												
									if(a_resource_allocations != null)
										{
											for(var i_ra_indx = 0; i_ra_indx < a_resource_allocations.length; i_ra_indx++)
												{
													var o_resource_allocation = a_resource_allocations[i_ra_indx];
																
													//var d_current_date = nlapiAddDays(d_start_date, i_day_indx);
													if(o_resource_allocation.project_id == i_project_id && o_resource_allocation.is_billable == 'T')
														{
															if(d_current_date >= o_resource_allocation.start_date 
																&& d_current_date <= o_resource_allocation.end_date)
																{
																	var i_rate = 0;
																	if(i_item_id == '2222' || i_item_id == '2221')
																		{
																			i_rate = o_resource_allocation.st_rate;
																		}
																	else if(i_item_id == '2425')
																		{
																			i_rate = o_resource_allocation.ot_rate;
																		}
																	o_day_data = {'billable':'T', 'rate': i_rate};
																}
														}
															
												}
										}
									
									nlapiLogExecution('DEBUG', 'data', JSON.stringify(o_day_data));
									try
									{
										if(o_day_data.rate == '' || o_day_data == null)
											{
												o_day_data.rate = 0.0;
											}
										if(o_day_data.billable == 'T')
											{
												if(o_day_data.rate != '' && !isNaN(o_day_data.rate))
													{
														var new_hours = _correct_time(o_sub_record_view.getFieldValue('hours'));
														new_value = new_value + parseFloat(o_day_data.rate) * new_hours;
													} 
											}
										var isSubRecordToBeChanged = true;
										if(o_day_data.billable == isCurrentlyBillable && o_day_data.rate == currentRate)
										{
											isSubRecordToBeChanged = false;
										}
										if(!isNaN(o_day_data.rate) && !isNaN(currentRate))
										{
											if(o_day_data.billable == isCurrentlyBillable && parseFloat(o_day_data.rate) == parseFloat(currentRate))
											{
												isSubRecordToBeChanged = false;
											}
										}
										if(old_hours == 0.0)
											{
												isSubRecordToBeChanged = false;
											}
										
										if(i_item_id == '2479' || i_item_id == '2480' || i_item_id == '2481')
										{
										    if(isCurrentlyBillable == 'F')
										    {
										        isSubRecordToBeChanged = false;
										    }
										}
										if(isRecordToBeChanged == false && isSubRecordToBeChanged == true)
											{
												isRecordToBeChanged = true;
											}
										if(isSubRecordToBeChanged == true)
											{
												changedData += i_item_id + ': ' + isCurrentlyBillable + ' => ' + o_day_data.billable + ', ' + currentRate + ' => ' + o_day_data.rate + '<br/>';
											}
											var o_sub_record = o_sub_record_view;
											//var o_sub_record_new = recNewTS.createCurrentLineItemSubrecord('timegrid', sub_record_name);
											var time_entry_fields = new Array(
																		'day',
																		'subsidiary',
																		'employee',
																		'projecttaskassignment',							
																		'customer',
																		'casetaskevent',
																		//'item',
																		'location',
																		'hours',
																		'memo',
																		'isbillable',
																		'approvalstatus',
																		'payrollitem',
																		'paidexternally',
																		'price',
																		'rate',
																		//'overriderate',
																		'department',
																		'class',
																		'billingclass',
																		'createddate',
																		'externalid',
																		'isexempt',
																		'isproductive',
																		'isutilized',
																		'timetype',
																		'custrecord_approvalstatus_ts',
																		'custrecord_is_provision_created_tr_ts',
																		'custrecord_projectmanager_ts',
																		'custrecord_projecttype_ts',
																		'custrecord_subtierpay_ts',
																		'custrecordcustcol_temp_customer_ts',
																		'custrecordprj_name_ts',
																		'custrecord_employeenamecolumn_ts');
											var o_new_values = new Object();
											for(var i_field = 0; i_field < time_entry_fields.length; i_field++)
												{
													var s_field_name = time_entry_fields[i_field];
													if(s_field_name == 'isbillable')
														{
															if(o_day_data.billable == 'F')
															{
																o_new_values['isbillable'] = o_day_data.billable;
															}
															else if(i_item_id != '2479' && i_item_id != '2480' && i_item_id != '2481')
															{
																o_new_values['isbillable'] = o_day_data.billable;
															}
														}
													else if(s_field_name == 'price')
														{
															o_new_values['price'] = -1;
														}
													else if (s_field_name == 'rate') 
														{
															o_new_values['rate'] = o_day_data.rate;
														}
													else
														{
															//o_new_values[s_field_name] = o_sub_record.getFieldValue(s_field_name);
														}
													
												}
											
											for(str_field_name in o_new_values)
												{
													//o_sub_record_new.setFieldValue(str_field_name,o_new_values[str_field_name]);
												}
											//o_sub_record_new.setFieldValue('approvalstatus', o_sub_record)
											//o_sub_record_new.setFieldValue('price', -1);
											//o_sub_record_new.setFieldValue('rate', o_day_data.rate);
											//o_sub_record_new.commit();
											isLineItemChanged = true;
											if(isSubRecordToBeChanged)
												{
													nlapiLogExecution('EMERGENCY', '----->Item: ' + i_item_id + 'TimesheetId: ' + recId + ', Date: ' + d_current_date.toDateString(), isCurrentlyBillable + ' - ' + o_day_data.billable + ' , ' + currentRate + ' - ' + o_day_data.rate);
													nlapiLogExecution('DEBUG', 'Item: ' + i_item_id + 'TimesheetId: ' + recId + ', Date: ' + d_current_date.toDateString(), isCurrentlyBillable + ' - ' + o_day_data.billable + ' , ' + currentRate + ' - ' + o_day_data.rate);
												}
											else
												{
													nlapiLogExecution('AUDIT', 'Item: ' + i_item_id + ', TimesheetId: ' + recId + ', Date: ' + d_current_date.toDateString(), isCurrentlyBillable + ' - ' + o_day_data.billable + ' , ' + currentRate + ' - ' + o_day_data.rate);
												}

									}
									catch(ex1)
									{
										nlapiLogExecution('ERROR','TimesheetId: ' + recId, ex1.message);
										return;
									}
									
								}
							
							
							}
								
					}
							
					//recNewTS.commitLineItem('timegrid');
						
		}
		var i_new_record_id;
		var i_log_id = null;
		if(isRecordToBeChanged == true)
		{
			try
			{
				/*var recLog = nlapiCreateRecord('customrecord_time_sheet_update_log');
				recLog.setFieldValue('custrecord_tsul_old_time_sheet_id', recId);
				//recLog.setFieldValue('custrecord_tsul_new_timesheet_id', i_new_record_id);
				recLog.setFieldValue('custrecord_tsul_old_record_json', recJson);
				//recLog.setFieldValue('custrecord_tsul_new_record_json', JSON.stringify(recNewTsLoad));
				recLog.setFieldValue('custrecord_tsul_change', changedData);
				recLog.setFieldValue('custrecord_tsul_amount_difference', parseFloat(new_value) - parseFloat(old_value));
				i_log_id = nlapiSubmitRecord(recLog);
				nlapiLogExecution('AUDIT', 'Update Logged', i_log_id);
				
				//nlapiSubmitRecord(recTS, true, true);
				var isTsDeleted = false;
				
				nlapiDeleteRecord('timesheet', recId);
				isTsDeleted = true;
				
				if(isTsDeleted)
					{
						
						i_new_record_id = nlapiSubmitRecord(recNewTS, true, true);
					}
				nlapiLogExecution('AUDIT', 'TimesheetId: ' + recId, 'Record Submitted' + i_new_record_id);
				
				var recNewTsLoad = nlapiLoadRecord('timesheet', i_new_record_id, {recordmode: 'dynamic'});
				
				var recLog = nlapiLoadRecord('customrecord_time_sheet_update_log', i_log_id);
				//recLog.setFieldValue('custrecord_tsul_old_time_sheet_id', recId);
				recLog.setFieldValue('custrecord_tsul_new_timesheet_id', i_new_record_id);
				//recLog.setFieldValue('custrecord_tsul_old_record_json', recJson);
				recLog.setFieldValue('custrecord_tsul_new_record_json', JSON.stringify(recNewTsLoad));
				//recLog.setFieldValue('custrecord_tsul_amount_difference', parseFloat(new_value) - parseFloat(old_value));
				i_log_id = nlapiSubmitRecord(recLog);
				nlapiLogExecution('AUDIT', 'Update Logged', i_log_id);
				*/
			}
			catch(ex3)
			{
				if(i_log_id != null)
					{
						//nlapiSubmitField('customrecord_time_sheet_update_log', i_log_id, 'custrecord_tsul_error', ex3.message);
					}
				nlapiLogExecution('ERROR', 'TimesheetId: ' + recId, ex3.message);
				return;
			}
			
		}
		var context = nlapiGetContext();
		nlapiLogExecution('AUDIT', 'Timesheet: ' + recId, 'End, Remaining Usage: ' + context.getRemainingUsage());
}

function getResourceAllocationsForEmployee(i_employee_id, d_start_date, d_end_date)
{
	// Store resource allocations for project and employee
	var a_resource_allocations = new Array();
	
	// Get Resource allocations for this week
	var filters = new Array();
	filters[0]	=	new nlobjSearchFilter('resource', null, 'anyof', i_employee_id);
	//filters[1]	=	new nlobjSearchFilter('project', null, 'anyof', i_project_id);
	filters[1]	=	new nlobjSearchFilter('custeventbstartdate', null, 'onorbefore', d_end_date);
	filters[2]	=	new nlobjSearchFilter('custeventbenddate', null, 'onorafter', d_start_date);
	filters[3]	=	new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');

	var columns = new Array();
	columns[0]	= new nlobjSearchColumn('custeventbstartdate');
	columns[1]	= new nlobjSearchColumn('custeventbenddate');
	columns[2]	= new nlobjSearchColumn('custeventrbillable');
	columns[3]  = new nlobjSearchColumn('custevent3');
	columns[4]	= new nlobjSearchColumn('custevent_otrate');
	columns[5]  = new nlobjSearchColumn('project');

	var search_results = nlapiSearchRecord('resourceallocation', null, filters, columns);
	
	if(search_results != null && search_results.length > 0)
	{
		for(var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++)
			{
				var i_project_id = search_results[i_search_indx].getValue('project');
				var resource_allocation_start_date = new Date(search_results[i_search_indx].getValue('custeventbstartdate'));
				var resource_allocation_end_date   = new Date(search_results[i_search_indx].getValue('custeventbenddate'));
				var is_resource_billable	= search_results[i_search_indx].getValue('custeventrbillable');
				var stRate					= search_results[i_search_indx].getValue('custevent3');
				var otRate					= search_results[i_search_indx].getValue('custevent_otrate');
				a_resource_allocations[i_search_indx] = {
															project_id: i_project_id,
															start_date:resource_allocation_start_date,
															end_date:resource_allocation_end_date,
															is_billable: is_resource_billable,
															st_rate: stRate,
															ot_rate: otRate
														};
			}
	}
	else
	{
		a_resource_allocations = null;
	}
	
	return a_resource_allocations;
}

function getTSFieldValues(o_old_record)
{
	var o_field_values = new Object();
	o_field_values['approvalstatus']= o_old_record.getFieldValue('approvalstatus');
	o_field_values['customform'] 	= o_old_record.getFieldValue('customform');
	o_field_values['employee'] 		= o_old_record.getFieldValue('employee');
	o_field_values['enddate'] 		= o_old_record.getFieldValue('enddate');
	o_field_values['externalid']	= o_old_record.getFieldValue('externalid');
	o_field_values['iscomplete']	= o_old_record.getFieldValue('iscomplete');
	o_field_values['startdate']		= o_old_record.getFieldValue('startdate');
	o_field_values['subsidiary']		= o_old_record.getFieldValue('subsidiary');
	o_field_values['totalhours']		= o_old_record.getFieldValue('totalhours');
	
	return o_field_values;
}
function _is_Valid(obj) //
{
	if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
	{
		return true;
	}
	else //
	{
		return false;
	}
}
function _correct_time(t_time) //
{
	// function is used to correct the time 
	
	if (_is_Valid(t_time)) //
	{
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time : ' + t_time);
		
		var hrs = t_time.split(':')[0]; 
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		
		if (t_time.indexOf(':') > -1) //
		{
			var mins = t_time.split(':')[1]; 
			//nlapiLogExecution('DEBUG', '_correct_time', 'mins : ' + mins);
			
			if (_is_Valid(mins)) //
			{
				mins = parseFloat(mins) / parseFloat('60'); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after correct mins : ' + mins);
				
				hrs = parseFloat(hrs) + parseFloat(mins); 
				//nlapiLogExecution('DEBUG', '_correct_time', 'after adding mins to hrs : ' + hrs);
			}
		}
		//nlapiLogExecution('DEBUG', '_correct_time', 'hrs : ' + hrs);
		return hrs;
	}
	else //
	{
		return 0;
		//nlapiLogExecution('DEBUG', '_correct_time', 't_time is invalid : ' + t_time);
	}
}