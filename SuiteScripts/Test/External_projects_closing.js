/** Author Sai Saranya
	11 May 2017
 **/

function closeProject()
{
	try{
	// 
		var search = nlapiSearchRecord(
		        'job','customsearch_external_project_closing',null,
		 
						[ new nlobjSearchColumn('jobtype'),
						new nlobjSearchColumn('internalid'),
		                new nlobjSearchColumn('custentity_pro_closing_comments'),
		                new nlobjSearchColumn('entitystatus')
		            ]);
			if(search)
				{
				for(var i=0;i<search.length;i++)
				{
					var pro_recid=search[i].getValue('internalid');
					var job_type=search[i].getValue('jobtype');
					var status=search[i].getValue('entitystatus');
					var job_comments=search[i].getValue('custentity_pro_closing_comments');				
					var rec=nlapiLoadRecord('job',pro_recid);
					rec.setFieldValue('custentity_pro_closing_comments','Project status updated as closed as it has been more than 90 days past the end date of the project.');
					rec.setFieldValue('entitystatus','1');
				//	nlapiLogExecution('debug', 'id', status);
					nlapiLogExecution('debug', 'projectid', pro_recid);
				//	var submitted_id = nlapiSubmitRecord(rec, {disabletriggers : true, enablesourcing : true});
                  	var submitted_id = nlapiSubmitRecord(rec,  false, true);
                 // nlapiSubmitField('job',pro_recid,'custentity_pro_closing_comments','Project status updated as closed as it has been more than 90 days past the end date of the project');
                  // nlapiSubmitField('job',pro_recid,'entitystatus','1');
                 //  var smail=nlapiSendEmail(62082, 'sai.vannamareddy@brillio.com', 'Project closure Script executed','executes sucessfully');
				
				}
				}
			}
			catch(e)
			{
			nlapiLogExecution('error', 'Main', e);
			}
		}
					
			