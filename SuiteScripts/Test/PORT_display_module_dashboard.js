// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: PORT_display_module_dashboard.js
	Author:		Rakesh K
	Company:	Brillio 
	Date:		24-01-2017
	Version:	1.0
	Description: This script is uses to display VMS module on Dash board 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     PORTLET
		- portletFunction()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PORTLET ==================================================

//function portletFunction(){

	
	function demoRssPortlet(portlet)
	{
	   portlet.setTitle('Custom RSS Feed');
	   var feeds = getRssFeed();
	   if ( feeds != null && feeds.length > 0 )
	   {
	      for ( var i=0; i < feeds.length ; i++ )
	      {
	         portlet.addLine('#'+(i+1)+': '+feeds[i].title, feeds[i].url, 0);
	         portlet.addLine(feeds[i].description, null, 1);
	      }
	   }
	}
	function getRssFeed()
	{
	   var url = 'http://rss.slashdot.org/Slashdot/slashdot';
	   var response = nlapiRequestURL( url, null, null );
	   var responseXML = nlapiStringToXML( response.getBody() );
	   var rawfeeds = nlapiSelectNodes( responseXML, "//item" );
	   var feeds = new Array();
	   for (var i = 0; i < rawfeeds.length && i < 5 ; i++)
	   {
	     feeds[feeds.length++] = new rssfeed( nlapiSelectValue(rawfeeds[i], "title"),
	                    nlapiSelectValue(rawfeeds[i], "link"),
	                    nlapiSelectValue(rawfeeds[i], "description"));
	   }
	   return feeds;
	}
	 
	function rssfeed(title, url, description)
	{
	   this.title = title;
	   this.url = url;
	   this.description = description;
	}
//}

// END PORTLET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
{



}
// END OBJECT CALLED/INVOKING FUNCTION =====================================================
