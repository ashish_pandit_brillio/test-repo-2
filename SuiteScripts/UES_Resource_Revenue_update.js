function revenue_update_for_del(type)
{
	if(type =='delete')
	{
		var recid_del=nlapiGetRecordId();
			var res_fields_del=nlapiLookupField('resourceallocation',recid_del,
									['custeventbstartdate','custeventbenddate','project','custevent3','percentoftime']);
			var new_bill_st_date_del=res_fields_del.custeventbstartdate;
			var new_bill_end_date_del=res_fields_del.custeventbenddate;
			var project_del=res_fields_del.project;
			var bill_rate_del=res_fields_del.custevent3;
			var percent_time_del=res_fields_del.percentoftime;
			nlapiLogExecution('DEBUG','Percent',percent_time_del);
			nlapiLogExecution('DEBUG','project',project_del);
			var reven_cost_del=0;
			var pro_fields_del=nlapiLookupField('job',project_del,['jobbillingtype','subsidiary','custentity_resource_revenue_cost']);
			var bill_type_del=pro_fields_del.jobbillingtype;
			var subsi_del=pro_fields_del.subsidiary;
			var pro_rev_cost_del=pro_fields_del.custentity_resource_revenue_cost;
			nlapiLogExecution('DEBUG','old_pro_rev_cost',pro_rev_cost_del);
			if((bill_type_del == 'TM' )&&(_logValidation(new_bill_st_date_del))&&(_logValidation(new_bill_end_date_del)))
			{
				var rev_cost_del=getRevCost(new_bill_st_date_del,new_bill_end_date_del,project_del,bill_rate_del,subsi_del,percent_time_del);
				pro_rev_cost_del=parseFloat(pro_rev_cost_del)-parseFloat(rev_cost_del);
				nlapiLogExecution('DEBUG','new_pro_rev_cost',pro_rev_cost_del);
				nlapiSubmitField('job',project_del,'custentity_resource_revenue_cost',pro_rev_cost_del);
				
			}
	}
}
function resoureRevenue_AfterSubmit(type, form) {

	if(type =='create' || type == 'edit')
	{
		try {
			var recid=nlapiGetRecordId();
			var res_fields=nlapiLookupField('resourceallocation',recid,
									['custeventbstartdate','custeventbenddate','project','custevent3','percentoftime']);
			var new_bill_st_date=res_fields.custeventbstartdate;
			var new_bill_end_date=res_fields.custeventbenddate;
			var project=res_fields.project;
			var bill_rate=res_fields.custevent3;
			var percent_time=res_fields.percentoftime;
			nlapiLogExecution('DEBUG','Percent',percent_time);
			nlapiLogExecution('DEBUG','project',project);
			var reven_cost=0;
			var pro_fields=nlapiLookupField('job',project,['jobbillingtype','subsidiary','custentity_resource_revenue_cost']);
			var bill_type=pro_fields.jobbillingtype;
			var subsi=pro_fields.subsidiary;
			var pro_rev_cost=pro_fields.custentity_resource_revenue_cost;
			if(!_logValidation(pro_rev_cost))
			{
				pro_rev_cost=0;
			}
			
			nlapiLogExecution('DEBUG','pro_rev_cost',pro_rev_cost);
			if((bill_type == 'TM' )&&(_logValidation(new_bill_st_date))&&(_logValidation(new_bill_end_date)))
			{
				var rev_cost=getRevCost(new_bill_st_date,new_bill_end_date,project,bill_rate,subsi,percent_time);
				nlapiLogExecution('DEBUG','present_rev_cost',rev_cost);
				if(type !='create'){
				var old_rec=nlapiGetOldRecord();
				var old_bill_st_date=old_rec.getFieldValue('custeventbstartdate');
				var old_end_date=old_rec.getFieldValue('custeventbenddate');
				var old_pro=old_rec.getFieldValue('project');
				var old_bill_rate=old_rec.getFieldValue('custevent3');
				var old_percent_time=old_rec.getFieldValue('percentoftime');
				var old_Rev_cost=getRevCost(old_bill_st_date,old_end_date,old_pro,old_bill_rate,subsi,old_percent_time);
				
				nlapiLogExecution('DEBUG', 'old_Rev_cost', old_Rev_cost);
				if(!_logValidation(pro_rev_cost)){
				
				}
				else{
				pro_rev_cost=parseFloat(pro_rev_cost)-parseFloat(old_Rev_cost);
				}
				nlapiLogExecution('DEBUG','oldpro_rev_cost',pro_rev_cost);
				}
				var act_rev_cost=parseFloat(pro_rev_cost)+parseFloat(rev_cost);
				nlapiLogExecution('DEBUG','act_rev_cost',act_rev_cost);
				//nlapiLogExecution('DEBUG', 'reven_cost', reven_cost);
				
				nlapiSubmitField('job',project,'custentity_resource_revenue_cost',act_rev_cost);
			}
			nlapiLogExecution('DEBUG', 'bill_type', bill_type);
			
		}
		
	catch (err) {
			nlapiLogExecution('ERROR', 'beforeLoad', err);
		}
	}
}

function getWeekend(startDate, endDate) 
{
                  
                var startDate_1 = startDate
                var endDate_1 = endDate
                var weekends=0;
               startDate = nlapiStringToDate(startDate);
               endDate = nlapiStringToDate(endDate);
                nlapiLogExecution('DEBUG', 'Check Date', 'endDate : ' + endDate);
                /*   var i_count_day = startDate.getDate();        
                var i_count_last_day = endDate.getDate();
                
                i_month = startDate.getMonth() + 1;
                
                i_year = startDate.getFullYear();*/
                
                //nlapiLogExecution('DEBUG', 'Check Date', '2 startDate_1 : ' + startDate_1);
                //nlapiLogExecution('DEBUG', 'Check Date', '2 endDate_1 : ' + endDate_1);
                var sat = new Array(); //Declaring array for inserting Saturdays
                var sun = new Array(); //Declaring array for inserting Sundays
				while(startDate<endDate)
				{
						
					if(startDate.getDay() == 0 ||startDate.getDay() == 6)
					{
					weekends++;
					}
					 startDate=nlapiAddDays(startDate,1);
				
				}
                       
                return weekends;
}
function checkDateFormat()
{
    var context = nlapiGetContext();

                var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

                var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
function getRevCost(new_bill_st_date,new_bill_end_date,project,bill_rate,subsi,percent_time)
{

			
			var allocated_days=getDatediffIndays(new_bill_st_date,new_bill_end_date);
			var sun_sat=getWeekend(new_bill_st_date, new_bill_end_date);	
			var st_Date = nlapiStringToDate(new_bill_st_date);
           var end_Date = nlapiStringToDate(new_bill_end_date);
			var hol_fil=[];
			hol_fil[0]=new nlobjSearchFilter('custrecordsubsidiary',null,'anyof',subsi);
			hol_fil[1]=new nlobjSearchFilter('custrecord_date',null,'within',[st_Date,end_Date]);
			var holiday_search=nlapiSearchRecord('customrecord_holiday',null,hol_fil,null);
			if(_logValidation(holiday_search)){
			var tot_holday=holiday_search.length;
			var total_days=allocated_days-sun_sat-tot_holday;
			}
			else{
			var total_days=allocated_days-sun_sat;
			}
			var percentoftime=parseFloat(percent_time)/100;
			nlapiLogExecution('DEBUG','Percentage of time',percentoftime);
			var rev_cost=parseFloat(total_days)*parseFloat(bill_rate)*8*parseFloat(percentoftime);
			/*nlapiLogExecution('DEBUG', 'tot_holday', tot_holday);
			nlapiLogExecution('DEBUG', 'allocated_days', allocated_days);
			nlapiLogExecution('DEBUG', 'sun_sat', sun_sat);
			nlapiLogExecution('DEBUG', 'tot_days', total_days);*/
			return rev_cost;
				//nlapiSubmitField('job',project,'custentity_resource_revenue_cost',parseFloat(rev_cost));
		
			
}
