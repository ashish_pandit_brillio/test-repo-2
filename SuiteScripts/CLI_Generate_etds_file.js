// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:CLI generate etds
	Author:Nikhil jain
	Company:Aashnacloudetch Pvt. Ltd.
	Date:13/03/2015
    Description:validation in etds suiltlet screen


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_set_designation(type, name, linenum)
{
	
	
    /*  On field changed:

          - PURPOSE


          FIELDS USED:

			--Field Name--				--ID--


    */
	
	
			
	if(name == 'custpage_personname')
	{
	var employee = nlapiGetFieldValue('custpage_personname')
	
	if (employee != '' && employee != undefined && employee != null) 
	{
		
		var person_name = '';
		var per_designation='';
		
		var company_filters = new Array();
	
		var company_column = new Array();
		company_column.push( new nlobjSearchColumn('custrecord_personname'));    
		company_column.push( new nlobjSearchColumn('custrecord_designation'));    
		
		var company_results = nlapiSearchRecord('customrecord_taxcompanyinfo', null, company_filters, company_column);
		if (company_results != null && company_results != '' && company_results != undefined) 
		{
			person_name = company_results[0].getValue('custrecord_personname')
			nlapiLogExecution('DEBUG', 'company info record', 'person_name->' + person_name)
			
			per_designation = company_results[0].getValue('custrecord_designation')
			nlapiLogExecution('DEBUG', 'company info record', 'per_designation->' + per_designation)
		}
		
		if (person_name != employee) 
		{
			var job_title = nlapiLookupField('employee', employee, 'title')
			
			nlapiSetFieldValue('custpage_designation', job_title)
		}
		else
		{
			nlapiSetFieldValue('custpage_designation', per_designation)
		}
	}
	}

    //  LOCAL VARIABLES


    //  FIELD CHANGED CODE BODY

	//alert('Field Name='+name+'\nLine Item Type='+type+'\nField Value='+nlapiGetFieldValue(name)+'\nLine Item Value='+nlapiGetLineItemValue(type, name, 1));

}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine(type)
{

	/*  On validate line:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  VALIDATE LINE CODE BODY


	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================




// BEGIN FUNCTION ===================================================

function genrate_etds_file()
{
	//alert('hello')
	
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
		
	
	var email = nlapiGetFieldValue('custpage_email')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'email->' + email)
	
    //-----------------------------validation One world account-------------------------------------------------//
	if (i_subcontext != false) 
	{
		var subsidiary = nlapiGetFieldValue('subsidiary')
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'subsidiary->' + subsidiary)
	}
	
	var creation_date = nlapiGetFieldValue('pdate')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'creation_date->' + creation_date)
	
	var minor_head = nlapiGetFieldValue('custpage_minor_head')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'minor_head->' + minor_head)
	
	var financial_yr = nlapiGetFieldValue('custpage_year')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'financial yr->' + financial_yr)
	
	var personname = nlapiGetFieldValue('custpage_personname')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'person name->' + personname)
	
	var designation = nlapiGetFieldValue('custpage_designation')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'designation->' + designation)
	
	var deductor_type = nlapiGetFieldValue('custpage_typeof_deductor')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'deductor_type->' + deductor_type)
	
	var quarter = nlapiGetFieldValue('custpage_quarter')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'quarter->' + quarter)
	
	var book_entry = nlapiGetFieldValue('custpage_book_entry')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'book entry->' + book_entry)
	
	var return_type = nlapiGetFieldValue('custpage_return_type')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'return_type ->' + return_type)
	
	var location = nlapiGetFieldValue('custpage_location')
	nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'location->' + location)
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_financial_year', null, 'is', financial_yr));
	filters.push(new nlobjSearchFilter('custrecord_quater', null, 'is', quarter));
	
	if (i_subcontext != false) 
	{
			filters.push(new nlobjSearchFilter('custrecord_quarterlyreturn_subsidiary', null, 'is', subsidiary));
	}
	
	var column = new Array();
	column.push(new nlobjSearchColumn('internalid'));
	
	var results = nlapiSearchRecord('customrecord_quarterly_return_details', null, filters, column);
	if (results != null && results != '' && results != undefined) 
	{
		var i_internaid = results[0].getValue('internalid')
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'i_internaid->' + i_internaid)
		
		var o_quarterly_recordobj = nlapiLoadRecord('customrecord_quarterly_return_details', i_internaid)
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'load Quarterly detail record->' + o_quarterly_recordobj)
	}
	else 
	{
		var o_quarterly_recordobj = nlapiCreateRecord('customrecord_quarterly_return_details')
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'create Quarterly detail record->' + o_quarterly_recordobj)
	}
	
	if (o_quarterly_recordobj != null && o_quarterly_recordobj != '' && o_quarterly_recordobj != undefined) 
	{
		o_quarterly_recordobj.setFieldValue('custrecord_financial_year', financial_yr)
		
		o_quarterly_recordobj.setFieldValue('custrecord_minor_head', minor_head)
		
		o_quarterly_recordobj.setFieldValue('custrecord_person_resposible', personname)
		
		o_quarterly_recordobj.setFieldValue('custrecord_person_resp_designation', designation)
		
		o_quarterly_recordobj.setFieldValue('custrecord_deductee_type', deductor_type)
		
		o_quarterly_recordobj.setFieldValue('custrecord_quater', quarter)
		
		o_quarterly_recordobj.setFieldValue('custrecord_email_id',email)
			
		o_quarterly_recordobj.setFieldValue('custrecord_bookentry', book_entry)
		
		o_quarterly_recordobj.setFieldValue('custrecord_return_type', return_type)
		
		o_quarterly_recordobj.setFieldValue('custrecord_quaterlyreturn_location', location)
		
		if (i_subcontext != false) 
		{
				o_quarterly_recordobj.setFieldValue('custrecord_quarterlyreturn_subsidiary', subsidiary)
		}
		
		o_quarterly_recordobj.setFieldValue('custrecord_quarterlyreturn_filecreated', creation_date)
		
		o_quarterly_recordobj.setFieldValue('custrecord_tds_file_status', 'In-Progress')
		
		var submit_id = nlapiSubmitRecord(o_quarterly_recordobj);
		nlapiLogExecution('DEBUG', 'create_quartely_detailrecord', 'Quarterly record id->' + submit_id)
		
		if (submit_id != null && submit_id != '' && submit_id != undefined) 
		{
			var url = nlapiResolveURL('SUITELET', 'customscript_sut_genrate_etdsemail', '1')
			window.open(url + '&record_id=' + submit_id, "_self")
		}
	//	nlapiRequestURL(url+'&record_id='+submit_id)
		
	}

	

}
function refresh_quarterly_record()
{
	location.reload();
}


// END FUNCTION =====================================================
