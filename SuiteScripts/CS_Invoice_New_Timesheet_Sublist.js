/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 24 Feb 2016 Nitish Mishra
 * 
 */

//function userEventBeforeLoad(type, form, request) {
//	try {
//		form.addButton('custpage_auto_check', 'Auto Check', 'AutoCheck()')

//		form.addTab('custpage_tab_ts', 'Custom Timesheet');

//		var timesheetList = form.addSubList('custpage_timesheet',
//		        'inlineeditor', 'Custom Timesheet', 'custpage_tab_ts');
//		timesheetList.addButton('custpage_sync', 'Sync', 'sync()');
//		timesheetList.addMarkAllButton();

//		timesheetList.addField('id', 'text', 'Id').setDisplayType('hidden');
//		timesheetList.addField('check', 'checkbox', 'Check');
//		timesheetList.addField('date', 'text', 'Date').setDisplayType(
//		        'disabled');
//		timesheetList.addField('employeename', 'text', 'Employee')
//		        .setDisplayType('disabled');
//		timesheetList.addField('rate', 'text', 'Rate').setDisplayType(
//		        'disabled');
//		timesheetList.addField('time', 'text', 'Time').setDisplayType(
//		        'disabled');

//	} catch (err) {
//		nlapiLogExecution('error', 'userEventBeforeLoad', err);
//	}
//}

//function clientFieldChanged(type, name, linenum) {
//	try {
//		if (name == 'custbody_billfrom' || name == 'custbody_billto') {
//			var billFrom = nlapiGetFieldValue('custbody_billfrom');
//			var billTo = nlapiGetFieldValue('custbody_billto');

//			if (billFrom && billTo) {
//				// search time-entry from bill from and bill to date
//				var timeEntrySearch = nlapiSearchRecord('timeentry', 1127, [
//				        new nlobjSearchFilter('date', null, 'within', billFrom,
//				                billTo),
//				        new nlobjSearchFilter('customer', null, 'anyof',
//				                nlapiGetFieldValue('job')) ]);

//				if (timeEntrySearch) {
//					nlapiLogExecution('debug', 'timeentry count',
//					        timeEntrySearch.length);

//					timeEntrySearch.forEach(function(timeEntry) {
//						nlapiSelectNewLineItem('custpage_timesheet');
//						nlapiSetCurrentLineItemValue('custpage_timesheet',
//						        'id', timeEntry.getId());
//						nlapiSetCurrentLineItemValue('custpage_timesheet',
//						        'check', 'F');
//						nlapiSetCurrentLineItemValue('custpage_timesheet',
//						        'date', timeEntry.getValue('date'));
//						nlapiSetCurrentLineItemValue('custpage_timesheet',
//						        'employeename', timeEntry.getText('employee'));
//						nlapiSetCurrentLineItemValue('custpage_timesheet',
//						        'rate', timeEntry.getValue('rate'));
//						nlapiSetCurrentLineItemValue('custpage_timesheet',
//						        'time', timeEntry.getValue('hours'));
//						nlapiCommitLineItem('custpage_timesheet');
//					});
//				} else {
//					nlapiLogExecution('debug', 'timeentry count', '0');
//				}
//			}
//		}
//	} catch (err) {
//		nlapiLogExecution('ERROR', 'clientFieldChanged', err);
//	}
//}

//function sync() {
//	try {
//		var customTimesheetCount = nlapiGetLineItemCount('custpage_timesheet');
//		var datesChecked = [];

//		for (var linenum = 1; linenum <= customTimesheetCount; linenum++) {

//			if (nlapiGetLineItemValue('custpage_timesheet', 'check', linenum) == 'T') {
//				datesChecked.push(nlapiGetLineItemValue('custpage_timesheet',
//				        'date', linenum));
//			}
//		}

//		if (datesChecked.length > 0) {
//			var timesheetCount = nlapiGetLineItemCount('time');

//			for (var linenum = 1; linenum <= timesheetCount; linenum++) {

//				if (datesChecked.indexOf(nlapiGetLineItemValue('time',
//				        'billeddate', linenum)) != -1) {
//					nlapiSelectLineItem('time', linenum);
//					nlapiSetCurrentLineItemValue('time', 'apply', 'T');
//					nlapiSetCurrentLineItemValue('time', 'rate', 0);
//					nlapiSetCurrentLineItemValue('time', 'amount', 0);
//					nlapiCommitLineItem('time');
//				}
//			}
//		}
//	} catch (err) {
//		nlapiLogExecution('error', 'sync', err);
//	}
//}

//function AutoCheck() {
//	try {

//		var billFrom = nlapiGetFieldValue('custbody_billfrom');
//		var billTo = nlapiGetFieldValue('custbody_billto');

//		if (billFrom && billTo) {
//			var timesheetCount = nlapiGetLineItemCount('time');
//			billFrom = nlapiStringToDate(billFrom);
//			billTo = nlapiStringToDate(billTo);

//			for (var linenum = 1; linenum <= timesheetCount; linenum++) {
//				var currentDate = nlapiStringToDate(nlapiGetLineItemValue(
//				        'time', 'billeddate', linenum));

//				if (currentDate >= billFrom && currentDate <= billTo) {
//					nlapiSelectLineItem('time', linenum);
//					nlapiSetCurrentLineItemValue('time', 'apply', 'T');
//					nlapiCommitLineItem('time');
//				}
//			}
//		} else {
//			alert("Enter bill from and to date");
//		}
//	} catch (err) {
//		nlapiLogExecution('error', 'AutoCheck', err);
//	}
//}

function scheduled(type) {
    try {
        var projectSearch = nlapiSearchRecord(null, '1240');
        var currentContext = nlapiGetContext();
        if (projectSearch) {

            for (var i = 0; i < projectSearch.length; i++) {
                createZeroInvoice(projectSearch[i].getId());
                yieldScript(currentContext);
            }
        }
    } catch (err) {
        nlapiLogExecution('error', 'scheduled', err);
    }
}

function createZeroInvoice(projectId) {
    try {

        var customer = nlapiLookupField('job', projectId, 'customer');
        var invoice = nlapiCreateRecord('invoice', {
            recordmode: 'dynamic',
            entity: customer
        });

        var billFrom = '7/1/2013';
        var billTo = '12/31/2015';

        invoice.setFieldValue('job', projectId);
        invoice.setFieldValue('custbody_billfrom', billFrom);
        invoice.setFieldValue('custbody_billto', billTo);
        invoice.setFieldValue('location', 8);
        invoice.setFieldValue('custbody_layout_type', 2);

        billFrom = nlapiStringToDate(billFrom);
        billTo = nlapiStringToDate(billTo);

        var timesheetCount = invoice.getLineItemCount('time');

        for (var linenum = 1; linenum <= timesheetCount; linenum++) {
            var currentDate = nlapiStringToDate(invoice.getLineItemValue(
			        'time', 'billeddate', linenum));

            if (currentDate <= billTo) {
                invoice.selectLineItem('time', linenum);
                invoice.setCurrentLineItemValue('time', 'apply', 'T');
                invoice.setCurrentLineItemValue('time', 'rate', 0);
                invoice.setCurrentLineItemValue('time', 'amount', 0);
                invoice.commitLineItem('time');
            }
        }

        if (invoice.getFieldValue('total') > 0) {
            nlapiSendEmail(442, 'nitish.mishra@brillio.com', 'Amount greater than 0', "Amount Greater than 0\nProject : " + nlapiLookupField('job', projectId, 'entityid'));
            return;
        }
        else {
            var invoiceId = nlapiSubmitRecord(invoice, true, true);
            nlapiLogExecution('AUDIT', 'Invoice Id', invoiceId);

            var invoiceNumber = nlapiLookupField('invoice', invoiceId, 'tranid');

            nlapiSendEmail(442, 'nitish.mishra@brillio.com', 'New Invoice Created',
                'Invoice Id : ' + invoiceNumber + " \nProject : " + nlapiLookupField('job', projectId, 'entityid'));
        }
    } catch (err) {
        nlapiLogExecution('error', 'createZeroInvoice : ' + projectId, err);
        nlapiSendEmail(442, 'nitish.mishra@brillio.com', 'Some error in invoice creation',
               "Project : " + nlapiLookupField('job', projectId, 'entityid') + "\n Error : " + err);
    }
}

function yieldScript(currentContext) {

    if (currentContext.getRemainingUsage() <= 100) {
        nlapiLogExecution('AUDIT', 'API Limit Exceeded');
        var state = nlapiYieldScript();

        if (state.status == "FAILURE") {
            nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
            return false;
        } else if (state.status == "RESUME") {
            nlapiLogExecution('AUDIT', 'Script Resumed');
        }
    }
}