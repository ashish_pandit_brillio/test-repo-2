/**
 * Module Description
 * 
 * Version   Date                          Author                          Remarks  
 * 1.00                        Send Birthday mailers to the Employees scheduled at 8 a.m.
 */

function Main()
{
try
{
var deploymentId = nlapiGetContext().getDeploymentId();

var columns = new Array();
columns[0]=new nlobjSearchColumn('firstname');
columns[1]=new nlobjSearchColumn('email');
columns[2]=new nlobjSearchColumn('email','custentity_reportingmanager');
//columns[3]=new nlobjSearchColumn('custrecord_hrbusinesspartner','department');

// search for emp with birthday as of today
//load the search depending on subsidiary

//if(deploymentId == "customdeploy_sch_bday_mail_btpl")
//{
//var emp_bday_search = nlapiSearchRecord('employee', 'customsearch_birthday_search_btpl', null,columns);
//}
//else
//{
var emp_bday_search = nlapiSearchRecord('employee', 'customsearch_birthday_search_bllc', null,columns);

nlapiLogExecution('debug', 'emp_bday_search.length',emp_bday_search.length);

if(isNotEmpty(emp_bday_search))
{
	
	for(var i=0;i<emp_bday_search.length;i++)
	{
		
		// get the HR BP email id
		//var hr_bp_id = emp_bday_search[i].getValue('custrecord_hrbusinesspartner','department');
		//var hr_bp_email = null;
		
		//if(isNotEmpty(hr_bp_id)){
			//hr_bp_email = nlapiLookupField('employee',hr_bp_id, 'email');
		//}
		
		sendBdayMails(
			emp_bday_search[i].getValue('firstname'),
			emp_bday_search[i].getValue('email'),
			emp_bday_search[i].getValue('email','custentity_reportingmanager'),
			emp_bday_search[i].getId());
	}
}

}
catch(err){
nlapiLogExecution('error', 'Main', err);
}


function sendBdayMails(firstName, email, reporting_mng_email,emp_id){
	
	try{
		var mailTemplate = bdayTemplate(firstName);		
		nlapiLogExecution('debug', 'chekpoint',email);
		nlapiSendEmail('10730', email, mailTemplate.MailSubject, mailTemplate.MailBody,[reporting_mng_email, 'unni.kondathil@BRILLIO.COM'],null, {entity: emp_id});
	}
	catch(err){
nlapiLogExecution('error', 'sendBdayMails', err);
throw err;
     }
}

function bdayTemplate(firstName) {
    var htmltext = '';
    
    htmltext += '<table border="0" width="100%"><tr>';
    htmltext += '<td colspan="4" valign="top">';
    //htmltext += '<p>Hi ' + firstName + ',</p>';  
htmltext += '<p>Dear ' + firstName + ',</p>';
 
//htmltext += '<p>MANY HAPPY RETURNS OF THE DAY!</p>';
 
//htmltext += '<p>Wishing you a day filled with happiness and a life filled with joy</p>';
htmltext += '<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=23876&c=3883006&h=b4c21d3880ddf62470ed" alt="Right click to download image." /></p>';
htmltext += '<p>With Best Wishes,</p>';
htmltext += '<p>HR & MANAGEMENT TEAM @ BRILLIO</p>';
 
 
    htmltext += '</td></tr>';
    htmltext += '</table>';
    htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
    htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
    htmltext += '<tr>';
    htmltext += '<td align="right">';
    htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> One System. No Limits.</font>';
    htmltext += '</td>';
    htmltext += '</tr>';
    htmltext += '</table>';
    htmltext += '</body>';
    htmltext += '</html>';
 
    return {
        MailBody : htmltext,
        MailSubject : "Birthday Wishes!!!"      
    };
}
}