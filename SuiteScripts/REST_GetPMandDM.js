/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Apr 2019     Aazamali Khan
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet() {
    var projectManager = [];
    var deliveryManager = [];
    var employeeList = [];
    var employeeSearch = getEmployees();
    for (var int = 0; int < employeeSearch.length; int++) {
		var jsonObj = {};
		jsonObj.employeename = employeeSearch[int].getValue("entityid");
		jsonObj.employeeid = employeeSearch[int].getId();
		jsonObj.email = employeeSearch[int].getValue("email");
		jsonObj.designation = employeeSearch[int].getValue("title");
		employeeList.push(jsonObj);
	}
    /*var projectManagerResult = getProjectManager();
    var deliveryManagerResult = getDeliveryManager();
    if (projectManagerResult) {
        for (var int = 0; int < projectManagerResult.length; int++) {
            var jsonObj = {};
            jsonObj.employeename = projectManagerResult[int].getValue("entityid", "CUSTENTITY_PROJECTMANAGER", "GROUP");
            jsonObj.employeeid = projectManagerResult[int].getValue("internalid", "CUSTENTITY_PROJECTMANAGER", "GROUP");
            projectManager.push(jsonObj);
        }
    }
    if (deliveryManagerResult) {
        for (var int = 0; int < deliveryManagerResult.length; int++) {
            var jsonObj = {};
            jsonObj.employeename = deliveryManagerResult[int].getValue("entityid", "CUSTENTITY_DELIVERYMANAGER", "GROUP");
            jsonObj.employeeid = deliveryManagerResult[int].getValue("internalid", "CUSTENTITY_DELIVERYMANAGER", "GROUP");
            deliveryManager.push(jsonObj);
        }
    }*/
    return {
        "dataOut": {
            "employeeList": employeeList
        }
    };
}
function postRESTlet(dataIn) {
	// use dataIn to store into database
}
function getProjectManager() {
    var jobSearch = nlapiSearchRecord("job", null,
        [
            ["status", "anyof", "2"]
        ],
        [
            new nlobjSearchColumn("entityid", "CUSTENTITY_PROJECTMANAGER", "GROUP"),
            new nlobjSearchColumn("internalid", "CUSTENTITY_PROJECTMANAGER", "GROUP")
        ]
    );
    return jobSearch;
}

function getDeliveryManager() {
    var jobSearch = nlapiSearchRecord("job", null,
        [
            ["status", "anyof", "2"]
        ],
        [
            new nlobjSearchColumn("entityid", "CUSTENTITY_DELIVERYMANAGER", "GROUP"),
            new nlobjSearchColumn("internalid", "CUSTENTITY_DELIVERYMANAGER", "GROUP")
        ]
    );
    return jobSearch
}
function getEmployees() {
	var employeeSearch = searchRecord("employee",null,
			[
			    ["isinactive","is","F"],"AND",
      			["custentity_implementationteam","is","F"],"AND",
      			["custentity_employee_inactive","is","F"]
			], 
			[
			   new nlobjSearchColumn("entityid").setSort(false), 
			   new nlobjSearchColumn("email"), 
			   new nlobjSearchColumn("title")
			]
			);
	return employeeSearch;
}