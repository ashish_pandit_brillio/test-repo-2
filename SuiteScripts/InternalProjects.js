function create_InternalProjectTasks()
{
	//var InternalID = [6950,6951,6952,6953,6954,6955,6956,6957,6958,6959,6960,6961,6962,6963,6964,6965,6966,6967];
	var InternalID = [6952,6953,6954,6955,6956,6957,6958,6959,6960,6961,6962,6963,6964,6965,6966,6967];
	for(var i=0; i< InternalID.length; i++)
	{
		nlapiLogExecution('DEBUG', 'InternalID', InternalID[i]);
		for(var j=0; j< 4; j++)
		{
			var task = nlapiCreateRecord('projecttask');
			task.setFieldValue('company', InternalID[i]);
			switch(j)
			{
				case 0:
					task.setFieldValue('title', 'Operations');		
					break;
					
				case 1:
					task.setFieldValue('title', 'Initiatives');
					break;
				
				case 2:
					task.setFieldValue('title', 'Holiday');
					break;
				
				case 3:
					task.setFieldValue('title', 'Leave');
					break;
					
				default:
					break;
			}
			
			task.setFieldValue('estimatedwork', 100);
			task.setFieldValue('status', 'PROGRESS');
			var taskId = nlapiSubmitRecord(task);
			nlapiLogExecution('DEBUG', 'Task Id', taskId);
		}
	}
}

function OT_TaskCreation()
{
	var ExtProjId = [6821,6831,6832,6833,6834,6835,6836,6837,6838,6839,6840,6943,6944,6945,6946,6947,6948,6949,7051];

	for(var i=0; i< ExtProjId.length; i++)
	{
		nlapiLogExecution('DEBUG', 'InternalID', ExtProjId[i]);
		var task = nlapiCreateRecord('projecttask');
		task.setFieldValue('company', ExtProjId[i]);
		task.setFieldValue('title', 'OT');
		task.setFieldValue('estimatedwork', 100);
		task.setFieldValue('status', 'PROGRESS');
		var taskId = nlapiSubmitRecord(task);
		nlapiLogExecution('DEBUG', 'Task Id', taskId);
	}
}

function email_notification()
{
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('email');
	cols[1] = new nlobjSearchColumn('firstname');
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('email', null, 'isnotempty');
	//filters[1] = new nlobjSearchFilter('internalid', null, 'anyof', 1643); //1643
	filters[1] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', 1524); //1643
	filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	
	var searchresult = nlapiSearchRecord('employee', null, filters, cols);
	nlapiLogExecution('DEBUG', 'TOTAL CNT', searchresult.length);
	for(var i=0; searchresult != null && i < searchresult.length; i++)
	{
		var str = '<html><head><title>User Access Notification</title>';
		str += '</head>';
		str += '<body>';
		str += '<table width="100%" border="0" cellspacing="2" cellpadding="2">';
		str += '<tr>';
		str += '<td colspan="2" valign="top">';
		str += '<p>';
		str += '<p>Hi ' + searchresult[i].getValue('firstname') +  ',</p>';
		str += '<b>Starting 01<sup>st</sup> July 2014, expenses claims and procurement requests are to be raised in NetSuite.</b></p>';
		str += '<p>';
		str += '</p>';
		str += '<p>';
		str += '<b><span style="color:#2E75B6">Expenses</span></b>';
		str += '<span style="color:#2E75B6"> </span>';
		str += '&#8211; Currently a manual process exists across the organization to raise expense claims to finance.&nbsp; System has a workflow built in that would guide the claims process. &nbsp;In the employee center, click &#8220;Claim Expenses&#8221; and raise your requests.<o:p></o:p>';
		str += '</p>';
		str += '<p></p>';
		str += '<p><b><span style="color:#2E75B6">Procurement</span></b>';
		str += '<span style="color:#2E75B6"> </span>';
		str += '&#8211; This process is moving from the current manual mode to a workflow based system in NetSuite.&nbsp;&nbsp; <b>All Level 6 (lead)</b> and above would have a link to raise procurement requests in the employee center of NetSuite.&nbsp; System has a workflow that would send the request for approval to the department head. Subsequent to approval, procurement team, IT team and finance team would be involved in various stages of requisition to purchase order and employees would be able to track the requests.<o:p></o:p>';
		str += '</p>';
		str += '<p></p>';
		str += '<p>For any issues on system, write to <a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a><p></p>';
		str += '</p>';
		str += '<p><b>User Manuals</b></p>';
		str += '</p>';
		str += '<p><b>For those will Brillio IDs</b>';
		str += '</p>';
		str += '<p><span style="font-family:Symbol><span>&middot;<span style="font:7.0pt Times New Roman">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>';
		str += '<span><b>Go to</b> <a href="https://brillioonline.sharepoint.com/IS/Pages/NetSuite-Help.aspx">https://brillioonline.sharepoint.com/IS/Pages/NetSuite-Help.aspx</a><p></p></span>';
		str += '</p>';
		str += '<p><b><span>For those who do not have Brillio IDs<p></p></span></b></p><p>';
		str += '<span style="font-family:Symbol"><span style="mso-list:Ignore">&middot;<span style="font:7.0pt "Times New Roman">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span>';
		str += '<span><b>Go to</b> <a href="http://is.brillio.com:8001/Netsuite.html">http://is.brillio.com:8001/Netsuite.html</a><p></p></span>';
		str += '</p>';
		str += '</tr>';
		str += '<tr></tr>';
		str += '</table>';
		str += '<hr width="100%" size="1" noshade color="#CCCCCC">';
		str += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		str += '<tr>';
		str += '<td align="right">';
		str += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
		str += '</td>';
		str += '</tr>';
		str += '</table>';
		str += '</body>';
		str += '</html>';
		
		//nlapiSendEmail(author, recipient, 			subject, 				   body, cc, bcc, records, attachments, notifySenderOnBounce, internalOnly)
		//nlapiSendEmail(442, 'vinod.n@brillio.com', 'New Release notification', str, 'information.systems@brillio.com');
		nlapiSendEmail(442, searchresult[i].getValue('email'), 'New Release notification', str);
		nlapiLogExecution('DEBUG', 'Last Id', searchresult[i].getId());
	}
}

function TimeSheet_Entry()
{
	var EmailSent = 0;
	var cols = new Array();
	cols[0] = new nlobjSearchColumn('email');
	cols[1] = new nlobjSearchColumn('firstname');
	cols[2] = new nlobjSearchColumn('internalid');
	cols[2].setSort();
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('email', null, 'isnotempty');
	//filters[1] = new nlobjSearchFilter('internalid', null, 'anyof', 1525); //1643 (Vinod)
	filters[1] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', 3228); //all users
	//filters[1] = new nlobjSearchFilter('internalidnumber', null, 'greaterthan', 2622); //all users
	filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	
	var searchresult = nlapiSearchRecord('employee', null, filters, cols);
	nlapiLogExecution('DEBUG', 'TOTAL CNT', searchresult.length);
	for(var i=0; searchresult != null && i < searchresult.length; i++)
	{
		var FirstName = searchresult[i].getValue('firstname');
		var Email = searchresult[i].getValue('email');

		var cols1 = new Array();
		cols1[cols1.length] = new nlobjSearchColumn('durationdecimal', null, 'sum');
		
		var filters1 = new Array();
		filters1[filters1.length] = new nlobjSearchFilter('employee', null, 'anyof', searchresult[i].getId());
		filters1[filters1.length] = new nlobjSearchFilter('date', null, 'within', '6/30/2014', '7/06/2014');
		filters1[filters1.length] = new nlobjSearchFilter('type', null, 'is', 'A');
		
		var searchresult1 = nlapiSearchRecord('timebill', null, filters1, cols1);
		if(searchresult1 != null)
		{
			for (var j=0; j < searchresult1.length; j++)
			{
				if(searchresult1[j].getValue('durationdecimal', null, 'sum') < 40)
				{
					TimeSheet_Emailer(FirstName, Email, 1)
					EmailSent++;
				}
			}
		}
		nlapiLogExecution('DEBUG', 'Last Id', searchresult[i].getId());
		//nlapiLogExecution('DEBUG', 'Last Actual Id', searchresult[i].getValue('internalid'));
	}
	nlapiLogExecution('DEBUG', 'EmailSent Count', EmailSent);
}


function TimeSheet_Emailer(FirstName, Email, Type)
{
	//Type -> 0 - General Notification, 1 -> Reminder to only employees
	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + FirstName +  ',</p>';
	htmltext += '<p></p>';
	htmltext += '<p>Please fill in timesheet for week ending July 05, 2014 in NetSuite system.</p>';
	htmltext += '<p></p>';
	if(Type == 1)
	{
		htmltext += '<p>If you have already filled the timesheet, please ignore this mail.</p>';
		htmltext += '<p></p>';
	}
	htmltext += '<p>For any issues on system, write to <a href="mailto:information.systems@brillio.com">information.systems@brillio.com</a></p>';
	htmltext += '</td></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<tr></tr>';
	htmltext += '<p>Regards,</p>';
	htmltext += '<p>Information Systems</p>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	//nlapiSendEmail(author, recipient, 			subject, 				   body, cc, bcc, records, attachments, notifySenderOnBounce, internalOnly)
	//nlapiSendEmail(442, 'vinod.n@brillio.com', 'Please fill your timesheet in NetSuite System', htmltext);
	if(Type == 0)
	{
		//nlapiSendEmail(442, Email, 'Please fill your time sheet in NetSuite System', htmltext);
	}
	else
	{
		nlapiSendEmail(442, Email, 'Reminder - Please fill your time sheet in NetSuite System', htmltext);
	}
}


function FH_TaskCreation()
{
	var ExtProjId = [4949,
4950,
4951,
4952,
4953,
4954,
4955,
4956,
4957,
4958,
5206,
5207,
5306,
5307,
5308,
5309,
5310,
5311,
5312,
5314,
5325,
5327,
5330,
5331,
5335,
5346,
5347,
5354,
5355,
5357,
5358,
5359,
5360,
5361,
5362,
5363,
5364,
5365,
5366,
5367,
5368,
5369,
5370,
5371,
5372,
5373,
5374,
5375,
5376,
5377,
5378,
5379,
5380,
5381,
5382,
5383,
5384,
5385,
5387,
5389,
5390,
5391,
5392,
5393,
5394,
5395,
5396,
5397,
5399,
5400,
5401,
5402,
5403,
5404,
5405,
5407,
5408,
5409,
5410,
5411,
5414,
5415,
5418,
5420,
5422,
5423,
5529,
5530,
5532,
5533,
5534,
5536,
5537,
5538,
5539,
5707,
5709,
5714,
5717,
5718,
5719,
5720,
5725,
5726,
5727,
5728,
5729,
5730,
5731,
5732,
5733,
5734,
5735,
5736,
5737,
5738,
5741,
5742,
5743,
5744,
5745,
5746,
5750,
5751,
5752,
5753,
5754,
5755,
5756,
5757,
5758,
5759,
5760,
5761,
5762,
6074,
6076,
6077,
6078,
6079,
6080,
6082,
6085,
6087,
6689,
6693,
6694,
6695,
6696,
6697,
6698,
6699,
6700,
6701,
6702,
6703,
6707,
6708,
6710,
6711,
6712,
6713,
6716,
6717,
6718,
6821,
6831,
6832,
6833,
6834,
6835,
6836,
6837,
6838,
6839,
6840,
6943,
6944,
6945,
6946,
6947,
6948,
7051,
7065,
7067,
7068,
7069,
7080,
7082,
7086,
7088,
7089,
7096,
7097,
7130,
7131,
7142,
7153,
7157,
7158,
7159,
7160,
7161,
7162,
7166,
7167,
7168,
7179,
7182,
7184,
7185,
7186,
7187,
7189,
7193,
7194,
7195,
7196,
7201,
7202,
7203,
7204,
7205,
7206,
7207,
7208,
7209,
7211,
7212,
7213,
7214,
7216,
7217,
7223,
7224,
7227,
7228,
7229,
7233,
7234,
7235,
7236,
7257,
7258,
7259,
7260,
7371,
7574,
7677,
7678,
7680,
7681,
7684,
7887,
7888,
7889,
7891,
7897,
7900,
7901,
7937,
7948,
7949,
7950
];

	for(var i=0; i< ExtProjId.length; i++)
	{
		nlapiLogExecution('DEBUG', 'Project InternalID', ExtProjId[i]);
		var task = nlapiCreateRecord('projecttask');
		task.setFieldValue('company', ExtProjId[i]);
		task.setFieldValue('title', 'Floating Holiday');
		task.setFieldValue('estimatedwork', 100);
		task.setFieldValue('status', 'PROGRESS');
		var taskId = nlapiSubmitRecord(task);
		nlapiLogExecution('DEBUG', 'Task Id', taskId);
	}
}
