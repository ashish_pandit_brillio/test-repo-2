	function scheduledReport()
	{
		try
		{
			var o_context = nlapiGetContext();
			var i_user_logegdIn_id = o_context.getUser();
			nlapiLogExecution('debug','i_user_logegdIn_id ',i_user_logegdIn_id);
			var i_customerId = o_context.getSetting('SCRIPT', 'custscript_customer');
			//nlapiLogExecution('debug','i_customerId ',i_customerId);
			if(i_customerId)
			{
				var jobSearch = searchRecord("job",null,
				[
				   ["jobbillingtype","is","TM"],
					"AND",
				   ["customer","anyof",i_customerId], 
				   "AND", 
				   ["isinactive","is","F"],
				   "AND",
				    ["status","noneof","1"], 
				   "AND", 
				   ["enddate","onorafter","startofthismonth"]
				], 
				[
				   new nlobjSearchColumn("entityid"), 
				   new nlobjSearchColumn("accountnumber"), 
				   new nlobjSearchColumn("customer"), 
				   new nlobjSearchColumn("altname"), 
				   new nlobjSearchColumn("startdate"), 
				   new nlobjSearchColumn("enddate"), 
				   new nlobjSearchColumn("custentity_enddate"), 
				   new nlobjSearchColumn("jobbillingtype"), 
				   new nlobjSearchColumn("custentity_practice")
				]);
			}
			else
			{
				var jobSearch = searchRecord("job",null,
				[
					["jobbillingtype","is","TM"],
					"AND",
					["isinactive","is","F"],
				    "AND",
				    ["status","noneof","1"], 
				    "AND", 
				    ["enddate","onorafter","startofthismonth"]
				], 
				[
				   new nlobjSearchColumn("entityid"), 
				   new nlobjSearchColumn("accountnumber"), 
				   new nlobjSearchColumn("customer"), 
				   new nlobjSearchColumn("altname"), 
				   new nlobjSearchColumn("startdate"), 
				   new nlobjSearchColumn("enddate"), 
				   new nlobjSearchColumn("custentity_enddate"), 
				   new nlobjSearchColumn("jobbillingtype"), 
				   new nlobjSearchColumn("custentity_practice")
				]);
			}
			if(jobSearch)
			{
				var strVar_excel = '';
				strVar_excel += '<table>';
				strVar_excel += '<tr>';
				strVar_excel += '<td width="100%">';
				strVar_excel += '<table width="100%" border="1">';
				strVar_excel += '	<tr><b>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Account</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Project</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Start Date</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">End Date</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Actual Revenue(YTD)</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Additional Revenue Forecast</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Project Type</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Executing Practice</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">#of HC(India) Allocated</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">#of HC(Other) Allocated</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Total #of allocated HC</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Revenue from Allocated HC</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Additional Revenue</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Total Revenue Forecast(from future allocation + revenue module rampup)</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">#of FRFs Raised - New</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">Revenue based on New FRF</td>';
				strVar_excel += ' <td width="6%" font-size="11" align="center">#of FRFs Raised - Atrrition/Replacement</td>';
				strVar_excel += '	</b></tr>';
				for(var ii=0;ii<jobSearch.length;ii++)
				{
					if(o_context.getRemainingUsage()<500)
					{
						nlapiYieldScript();
					}
					var i_project = jobSearch[ii].getId();
					var s_account = jobSearch[ii].getText('customer');
					var s_project_id = jobSearch[ii].getValue('entityid');
					var s_projectName = jobSearch[ii].getValue('altname');
					var s_project = s_project_id+" "+s_projectName;
					var d_startDate = jobSearch[ii].getValue('startdate');
					var d_endDate = jobSearch[ii].getValue('enddate');
					var s_billingType = jobSearch[ii].getValue('jobbillingtype');
					var s_exePractice = jobSearch[ii].getText('custentity_practice');
					nlapiLogExecution('debug','s_project ',s_project);
					
					var i_actualRevenue = getActualRevenue(s_project_id);
					var i_additionalRevenue = additionalRevenueForecast(i_project);
					var i_Revenue_from_HC = revenueFromAllocatedHC(i_project);
					if(!i_Revenue_from_HC)
						i_Revenue_from_HC = 0;
					var i_additionalRevenue_from_HC = i_additionalRevenue;//additionalRevenue_HeadCount(i_project);
					nlapiLogExecution('debug','i_Revenue_from_HC '+i_Revenue_from_HC,'i_additionalRevenue_from_HC '+i_additionalRevenue_from_HC);
					var i_totalRevenueForecast = parseFloat(i_Revenue_from_HC) + parseFloat(i_additionalRevenue_from_HC);
					//nlapiLogExecution('debug','i_totalRevenueForecast ',i_totalRevenueForecast);
					var allocationDetails = getAllocationDetails(i_project);
					var i_no_of_HC_India = 0;
					var i_no_of_HC_Other = 0;
					var i_Total_HC = 0;
					if(allocationDetails)
					{
						var i_no_of_HC_India = allocationDetails.indiahc;
						var i_no_of_HC_Other = allocationDetails.otherhc;
						var i_Total_HC = allocationDetails.totalhc;
					}
					var frfDetails = getFRFDetails(i_project);
					var i_noOfFRF = 0;
					var i_FRFrevenue = 0;
					var i_attritionFRF = 0;
					if(frfDetails)
					{
						i_noOfFRF = frfDetails.count;
						i_FRFrevenue = frfDetails.FRFRevenue;
						i_attritionFRF = frfDetails.attritionCount;
					}				
					strVar_excel += '	<tr>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +s_account+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +s_project+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +d_startDate+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +d_endDate+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_actualRevenue+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_additionalRevenue+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +s_billingType+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +s_exePractice+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_no_of_HC_India+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_no_of_HC_Other+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_Total_HC+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_Revenue_from_HC+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_additionalRevenue_from_HC+ '</td>'; 
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_totalRevenueForecast+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_noOfFRF+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_FRFrevenue+ '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' +i_attritionFRF+ '</td>';
					strVar_excel += '	</tr>';
				}
				strVar_excel += '</table>';
				strVar_excel += ' </td>';	
				strVar_excel += '	</tr>';
				strVar_excel += '</table>';
				var excel_file_obj = generate_excel(strVar_excel);
				var body = "Please find the attached  Revenue Vrs FRF revenue report for T&M Project";
				nlapiSendEmail(442, i_user_logegdIn_id, "Report", body, null, null, i_user_logegdIn_id, excel_file_obj, null, null, null);
				//response.writePage(excel_file_obj.getValue()); 
			}
		}
		catch(err)
		{
			nlapiLogExecution('ERROR','Job function','ERROR MESSAGE :- '+err);
		}
	}

	function getFRFDetails(i_project_id)
	{
		var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
		[
		   ["custrecord_frf_details_status_flag","anyof","1"], 
		   "AND", 
		   ["custrecord_frf_details_project","anyof",i_project_id]
		], 
		[
		   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
		   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
		   new nlobjSearchColumn("custrecord_frf_type")
		]
		);
		if(customrecord_frf_detailsSearch)
		{
			var FRFRevenue = 0;
			var attritionCount = 0;
			for(var i=0;i<customrecord_frf_detailsSearch.length;i++)
			{
				var startDate = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_start_date');
				var endDate = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_end_date');
				var billRate = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_bill_rate');
				var FRFtype = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_type');
				var noOfWorkingDays = getWorkingDays(startDate,endDate);
				var revenue = parseFloat(noOfWorkingDays) * parseFloat(billRate) * 8;
				FRFRevenue = FRFRevenue + revenue;
				if(FRFtype == 7)
				{
					attritionCount++;
				}
				//nlapiLogExecution('debug','FRFRevenue '+FRFRevenue,'attritionCount '+attritionCount);
			}
			var frf_Details = {};
			frf_Details["FRFRevenue"] = FRFRevenue;
			frf_Details["attritionCount"] = attritionCount;
			frf_Details["count"] = customrecord_frf_detailsSearch.length;
			return frf_Details;
		}
	}

	function getWorkingDays(startDate, endDate) {
		try {
			var d_startDate = nlapiStringToDate(startDate);
			var d_endDate = nlapiStringToDate(endDate);
			//nlapiLogExecution('debug','d_startDate '+d_startDate,'d_endDate '+d_endDate);
			var numberOfWorkingDays = 0;

			for (var i = 0;; i++) {
				var currentDate = nlapiAddDays(d_startDate, i);

				if (currentDate > d_endDate) {
					break;
				}

				if (currentDate.getDay() != 0 && currentDate.getDay() != 6) {
					numberOfWorkingDays += 1;
				}
			}

			return numberOfWorkingDays;
		} catch (err) {
			nlapiLogExecution('error', 'getWorkingDays', err);
			throw err;
		}
	}
	function getAllocationDetails(i_project_id)
	{
		var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
		[
		   ["project","anyof",i_project_id]
		], 
		[
		   new nlobjSearchColumn("custevent4"), 
		   new nlobjSearchColumn("custeventwlocation")
		]
		);
		if(resourceallocationSearch)
		{
			var i_India_HC = 0;
			var i_other_HC = 0;
			for(var i=0; i<resourceallocationSearch.length;i++)
			{
				var onsiteOffshore = resourceallocationSearch[i].getValue('custevent4');
				if(onsiteOffshore == 1)
				{
					i_other_HC++;
				}
				else
				{
					i_India_HC++;
				}
			}
			var HCDetails = {};
			HCDetails["indiahc"] = i_India_HC;
			HCDetails["otherhc"] = i_other_HC;
			HCDetails["totalhc"] = resourceallocationSearch.length;
			return HCDetails;
		}
	}

	function generate_excel(strVar_excel)
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
		'<meta name=ProgId content=Excel.Sheet/>'+
		'<meta name=Generator content="Microsoft Excel 11"/>'+
		'<!--[if gte mso 9]><xml>'+
		'<x:excelworkbook>'+
		'<x:excelworksheets>'+
		'<x:excelworksheet=sheet1>'+
		'<x:name>** ESTIMATE FILE**</x:name>'+
		'<x:worksheetoptions>'+
		'<x:selected></x:selected>'+
		'<x:freezepanes></x:freezepanes>'+
		'<x:frozennosplit></x:frozennosplit>'+
		'<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
		'<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
		'<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
		'<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		'<x:activepane>0</x:activepane>'+// 0
		'<x:panes>'+
		'<x:pane>'+
		'<x:number>3</x:number>'+
		'</x:pane>'+
		'<x:pane>'+
		'<x:number>1</x:number>'+
		'</x:pane>'+
		'<x:pane>'+
		'<x:number>2</x:number>'+
		'</x:pane>'+
		'<x:pane>'+
		'<x:number>0</x:number>'+//1
		'</x:pane>'+
		'</x:panes>'+
		'<x:protectcontents>False</x:protectcontents>'+
		'<x:protectobjects>False</x:protectobjects>'+
		'<x:protectscenarios>False</x:protectscenarios>'+
		'</x:worksheetoptions>'+
		'</x:excelworksheet>'+
		'</x:excelworksheets>'+
		'<x:protectstructure>False</x:protectstructure>'+
		'<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
		
		// '<style>'+
			
			//-------------------------------------
		'</x:excelworkbook>'+
		'</xml><![endif]-->'+
		 '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+
		'<!-- /* Style Definitions */'+
		
		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+
		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+
		'</style>'+
		'</head>'+
		'<body>'+
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = strVar_excel;
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Revenue Vrs FRF T&M Data.xls', 'XMLDOC', strVar1);
		//response.setContentType('XMLDOC','Revenue Vrs FRF Data.xls');
		//response.write( file.getValue() );
		return file;
	}

	function searchRecord(recordType, savedSearch, arrFilters, arrColumns, filterExpression)
	{
		try {
			var search = null;
			// if a saved search is provided, load it and add the filters and
			// columns
			if (isNotEmpty(savedSearch)) {
				search = nlapiLoadSearch(recordType, savedSearch);

				if (isArrayNotEmpty(arrFilters)) {
					search.addFilters(arrFilters);
				}

				if (isArrayNotEmpty(arrColumns)) {
					search.addColumns(arrColumns);
				}

				if (isArrayNotEmpty(filterExpression)) {
					search.setFilterExpression(filterExpression);
				}
			}
			// create a new search
			else {
				search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
			}

			// run search
			var resultSet = search.runSearch();

			// iterate through the search and get all data 1000 at a time
			var searchResultCount = 0;
			var resultSlice = null;
			var searchResult = [];

			do {
				resultSlice = resultSet.getResults(searchResultCount,
						searchResultCount + 1000);

				if (resultSlice) {

					resultSlice.forEach(function(result) {

						searchResult.push(result);
						searchResultCount++;
					});
				}
			} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

			return searchResult;
		} catch (err) {
			nlapiLogExecution('ERROR', 'searchRecord', err);
			throw err;
		}
	}

	function isEmpty(value) {

		return value == null || value == "" || typeof (value) == undefined;
	}

	function isNotEmpty(value) {

		return !isEmpty(value);
	}

	function isArrayEmpty(argArray) {

		return !isArrayNotEmpty(argArray);
	}

	function isArrayNotEmpty(argArray) {

		return (isNotEmpty(argArray) && argArray.length > 0);
	}

	function revenueFromAllocatedHC(i_project_id)
	{
		var customrecord_forecast_master_dataSearch = nlapiSearchRecord("customrecord_forecast_master_data",null,
		[
		   ["custrecord_forecast_project_name","anyof",i_project_id], 
		   "AND", 
		   ["custrecord_forecast_month_startdate","onorafter","startofthismonth"]
		], 
		[
		   new nlobjSearchColumn("custrecord_forecast_amount",null,"SUM")
		]
		);
		if(customrecord_forecast_master_dataSearch)
		{
				return customrecord_forecast_master_dataSearch[0].getValue("custrecord_forecast_amount",null,"SUM");
		}
	}
	//===========================================================================================================

	
function getActualRevenue(s_project_id) {
	try {
		
		var search = nlapiLoadSearch('transaction', 2209); 
		var filters = search.getFilters();
	  
		filters = filters.concat([new nlobjSearchFilter('custcol_project_entity_id', null,'is', s_project_id)]);

		var columns = search.getColumns();

		//filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
		//filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
		//filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
		//filters.push(new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
		//    'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
		//]));

		var search_results = searchRecord('transaction', 'customsearch2209', filters, [
			columns[2], columns[1], columns[0], columns[3], columns[4],
			columns[5], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], columns[16],
			columns[17], columns[18], columns[19], columns[20], columns[21],columns[22],columns[23]
		]);
		nlapiLogExecution('debug','search_results ',search_results.length);
		var totalAmount = 0;
		var a_dataVal = {};
		a_dataVal = PL_curencyexchange_rate();		
		
		for (var i = 0; search_results != null && i < search_results.length; i++) 
		{
			var period = search_results[i].getText(columns[0]);

			//Code updated by Deepak, Dated - 21 Mar 17
			var s_month_year = period.split(' ');
			var s_mont = s_month_year[0];
			s_mont = getMonthCompleteName(s_mont);
			var s_year_ = s_month_year[1];
			var f_revRate = 66.0;
			var f_costRate = 66.0;
			
			var data_indx=(s_mont+"_"+s_year_).toString();
			var transaction_type = search_results[i].getText(columns[8]);
			var transaction_date = search_results[i].getValue(columns[9]);
			var i_subsidiary = search_results[i].getValue(columns[10]);
			var amount = parseFloat(search_results[i].getValue(columns[1]));
			var category = search_results[i].getValue(columns[3]);
			var s_account_name = search_results[i].getValue(columns[11]);
			var currency = search_results[i].getValue(columns[12]);
			var exhangeRate = search_results[i].getValue(columns[13]);
			
			//check for mis practice
			var mis_sub_practice = '';
			var mis_parent_practice = '';
			var index_ = search_results[i].getValue(columns[23]).indexOf(':');
			if(parseInt(index_) > parseInt(0)){
				var a_mis_practice = search_results[i].getValue(columns[23]).split(':');
				mis_parent_practice = a_mis_practice[0];
				mis_sub_practice = search_results[i].getValue(columns[23]);
			}
			else{
			mis_parent_practice = search_results[i].getValue(columns[23]);
			mis_sub_practice = '';
			}
				
			//nlapiLogExecution('AUDIT','i_subsidiary ',i_subsidiary);	
			//nlapiLogExecution('AUDIT','s_account_name ',s_account_name);	
			//nlapiLogExecution('AUDIT','transaction_type ',transaction_type);
			//Hardcoded for GDS practice
			if(mis_parent_practice.trim() == 'Global Design Studio'){ 	
			mis_parent_practice = 'Design & Experience Studio';
			mis_sub_practice = 'Design & Experience Studio : Experience Design';
			}	

			if(mis_parent_practice.trim() == 'Advanced Technology Group'){ 	
						mis_parent_practice = 'Technology Advisory &Consulting';
						mis_sub_practice = '';
						}				
			
            //nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

            //Transaction blocking for discount GL's
            
            if(transaction_type != 'Journal' && (parseInt(s_account_name) == parseInt(515)|| parseInt(s_account_name) == parseInt(516))){
            	continue;
            }
				
	
			amount=_conversionrateIntoUSD(i_subsidiary, amount, category, exhangeRate, a_dataVal[data_indx]); //check
			if(amount=='NaN')
				amount = 0; 
			//nlapiLogExecution('AUDIT','amount '+amount,'category '+category);
			
			if (category != 'People Cost' && category != 'Discount' && category != 'Facility Cost' && category != 'Revenue') 
			{
               
            } else {	
            	if (parseInt(s_account_name) == parseInt(580)) { // Bad Debts
                    if (parseFloat(amount) < 0) {
                        amount = Math.abs(parseFloat(amount));
                    } else {
                        amount = '-' + amount;
                    }
                }
                //avoiding discounts from account						  
               // if (category == 'Revenue' && (i_subsidiary == parseInt(2) || i_subsidiary == parseInt(11) || i_subsidiary == parseInt(12) )) {
                if (category == 'Revenue' && (i_subsidiary == parseInt(2) || i_subsidiary == parseInt(11) || i_subsidiary == parseInt(12) || i_subsidiary == parseInt(7) || i_subsidiary == parseInt(3))) {
                    if (parseInt(s_account_name) != parseInt(732)) 
					{
                        totalAmount += parseFloat(amount);
                    }
                } else if (category == 'Revenue') 
				{
                    totalAmount += parseFloat(amount);
                }
	
				if (category == 'Discount') 
				{
					totalAmount -= parseFloat(amount);
                }
            }
		}
		return totalAmount;
	} catch (e) {
		nlapiLogExecution('DEBUG', 'Actual Data Flow Error', e);
		throw e;
	}
}
	//Get the complete month name
	function getMonthCompleteName(month) {
		var s_mont_complt_name = '';
		if (month == 'Jan')
			s_mont_complt_name = 'January';
		if (month == 'Feb')
			s_mont_complt_name = 'February';
		if (month == 'Mar')
			s_mont_complt_name = 'March';
		if (month == 'Apr')
			s_mont_complt_name = 'April';
		if (month == 'May')
			s_mont_complt_name = 'May';
		if (month == 'Jun')
			s_mont_complt_name = 'June';
		if (month == 'Jul')
			s_mont_complt_name = 'July';
		if (month == 'Aug')
			s_mont_complt_name = 'August';
		if (month == 'Sep')
			s_mont_complt_name = 'September';
		if (month == 'Oct')
			s_mont_complt_name = 'October';
		if (month == 'Nov')
			s_mont_complt_name = 'November';
		if (month == 'Dec')
			s_mont_complt_name = 'December';

		return s_mont_complt_name;
	}

	function additionalRevenueForecast(i_project_id)
	{
		//nlapiLogExecution('debug','additionalRevenueForecast  i_project_id** ',i_project_id);
		var jsonData = getTagged_Active_TM(i_project_id);
		
		var totlAmt = 0;
		//var jsonData = JSON.stringfy(jsonData);
		nlapiLogExecution('debug','jsonData  ** ',JSON.stringify(jsonData));
		if(jsonData)
		{
			for(var i=0; i<jsonData.length;i++)
			{
				for(var ii=0; ii<jsonData[i].length;ii++)
				{
					var amt = jsonData[i][ii].Amount;
					//nlapiLogExecution('debug','amt Amount ',amt);
					totlAmt = parseFloat(totlAmt) + parseFloat(amt);
				}
			}
			//nlapiLogExecution('debug','totlAmt  ',totlAmt);
		}
		return totlAmt;
	}
	
	
	function additionalRevenue_HeadCount(i_project_id)
	{
		var totalAmt = 0;
		var customrecord_tm_month_total_jsonSearch = nlapiSearchRecord("customrecord_tm_month_total_json",null,
		[
		   ["custrecord_project_json","anyof",i_project_id], 
		   "AND", 
		   ["custrecord_is_confirm","is","T"]
		], 
		[
		   new nlobjSearchColumn("name"), 
		   new nlobjSearchColumn("custrecord_project_json"), 
		   new nlobjSearchColumn("created").setSort(true)
		]
		);
		if(customrecord_tm_month_total_jsonSearch)
		{
			var jsonParent = customrecord_tm_month_total_jsonSearch[0].getId();
			var customrecord_forecast_revenue_tmSearch = nlapiSearchRecord("customrecord_forecast_revenue_tm",null,
			[
			   ["custrecord_tm_json_parent","anyof",jsonParent]
			], 
			[
			   new nlobjSearchColumn("custrecord_rampup_type"), 
			   new nlobjSearchColumn("custrecord_tm_sub_practice"), 
			   new nlobjSearchColumn("custrecord_tm_month"), 
			   new nlobjSearchColumn("custrecord_tm_year"), 
			   new nlobjSearchColumn("custrecord_tm_amount"), 
			   new nlobjSearchColumn("custrecord_tm_json_parent"), 
			   new nlobjSearchColumn("custrecord_total_share"), 
			   new nlobjSearchColumn("custrecord_tm_notes"), 
			   new nlobjSearchColumn("custrecord_tm_data_project")
			]
			);
			if(customrecord_forecast_revenue_tmSearch)
			{
				for(var i=0; i<customrecord_forecast_revenue_tmSearch.length;i++)
				{
					var amt = customrecord_forecast_revenue_tmSearch[i].getValue('custrecord_tm_amount');
					totalAmt = parseFloat(totalAmt) + parseFloat(amt);
				}
			}
		}
		return totalAmt;
	}