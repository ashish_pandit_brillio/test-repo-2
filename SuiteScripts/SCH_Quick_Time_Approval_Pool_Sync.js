/**
 * Sync timesheet and quick time approval pending approval records
 * 
 * Version Date Author Remarks 1.00 04 Apr 2016 Nitish Mishra
 * 
 */

function scheduled(type) {
	nlapiLogExecution('debug', 'START');
	var context = nlapiGetContext();
	try {
		// get all pending approval quick approval pool timesheet
		var pendingApprovalPoolSearch = searchRecord(
		        'customrecord_ts_quick_approval_pool', null, [
		                //new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_qap_approved', null,
		                        'is', 'F'),
		                new nlobjSearchFilter('custrecord_qap_rejected', null,
		                        'is', 'F'),
		                new nlobjSearchFilter('custrecord_qap_ts_updated',
		                        null, 'is', 'F') ], [ new nlobjSearchColumn(
		                'custrecord_qap_timesheet') ]);

		context.setPercentComplete(2);

		var poolTimesheet = [];
		pendingApprovalPoolSearch.forEach(function(pool) {
			poolTimesheet.push(pool.getValue('custrecord_qap_timesheet'));
		});

		context.setPercentComplete(5);

		nlapiLogExecution('debug', 'pool timesheet', poolTimesheet.length);

		// get all timesheets pending approval except which that are already in
		// pool
		var timesheetSearch = searchRecord('timesheet', null, [
		        new nlobjSearchFilter('timesheetdate', null, 'onorafter',
		                '7/1/2015'),
		        new nlobjSearchFilter('internalid', null, 'noneof',
		                poolTimesheet),
		        new nlobjSearchFilter('approvalstatusicon', null, 'anyof', 2) ]);

		nlapiLogExecution('debug', 'timesheet to be pushed',
		        timesheetSearch.length);

		context.setPercentComplete(10);

		//return;
		// create the required timesheets
		for (var i = 0; i < timesheetSearch.length; i++) {
			var percent = parseFloat((((((i / timesheetSearch.length) * 100) / 100) * 90) + 10)
			        .toFixed(2));
			nlapiLogExecution('debug', 'timesheet',
			        timesheetSearch[i].getId());

			context.setPercentComplete(percent);
			createQuickPoolRecord(timesheetSearch[i].getId());
			yieldScript(context);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
	nlapiLogExecution('debug', 'END');
}

function createQuickPoolRecord(timesheetId) {

	try {
		// check if the quick approval pool record exists for this timesheet
		var quickPoolSearch = nlapiSearchRecord(
		        'customrecord_ts_quick_approval_pool', null,
		        new nlobjSearchFilter('custrecord_qap_timesheet', null,
		                'anyof', timesheetId));

		var quickApprovalRecord = null;		

		if (quickPoolSearch) {
			nlapiLogExecution('debug', 'already in pool');
			return;
		} else {
			quickApprovalRecord = nlapiCreateRecord('customrecord_ts_quick_approval_pool');
		}
		
		var timesheetRecord = nlapiLoadRecord('timesheet', timesheetId);

		quickApprovalRecord.setFieldValue('custrecord_qap_timesheet',
		        timesheetRecord.getId());
		quickApprovalRecord.setFieldValue('custrecord_qap_employee',
		        timesheetRecord.getFieldValue('employee'));
		quickApprovalRecord.setFieldValue('custrecord_qap_week_start_date',
		        timesheetRecord.getFieldValue('startdate'));
		quickApprovalRecord.setFieldValue('custrecord_qap_week_end_date',
		        timesheetRecord.getFieldValue('enddate'));
		quickApprovalRecord.setFieldValue('custrecord_qap_total_hours',
		        timesheetRecord.getFieldValue('totalhours'));

		quickApprovalRecord.setFieldValue('custrecord_qap_approved', 'F');
		quickApprovalRecord.setFieldValue('custrecord_qap_rejected', 'F');
		quickApprovalRecord.setFieldValue('custrecord_qap_ts_updated', 'F');

		var recordId = nlapiSubmitRecord(quickApprovalRecord);

		nlapiInitiateWorkflowAsync('customrecord_ts_quick_approval_pool',
		        recordId, 'customworkflow_quick_approval_pool');

		nlapiLogExecution('debug', 'Quick Approval Pool TS Created / Updated',
		        recordId);
	} catch (err) {
		nlapiLogExecution('ERROR',
		        'TS Quick Approval Create : createQuickPoolRecord : '
		                + timesheetId, err);
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
