/**** Prospect Validation on SFDC Customer ID****/

function validate_acc_id(type){
try{
	
	if(type != 'delete' && type == 'create'){
	var s_sfdcAcc_id = nlapiGetFieldValue('custentity_sfdc_account_id');
	if(s_sfdcAcc_id){
	var a_searchRes = searchCustomer(s_sfdcAcc_id);
	if(a_searchRes){
	throw 'Duplicate SFDC Account ID!!';
	return false;
	}
	}
	}
	return true;
	
}
catch(e){
nlapiLogExecution('DEBUG','Validation Error',e);
throw e;
}
}

function searchCustomer(acc_id){
	return customerSearch = nlapiSearchRecord("customer",null,
[
   ["custentity_sfdc_account_id","is",acc_id], 
   "AND", 
   ["stage","anyof","CUSTOMER","PROSPECT"]
], 
[
   new nlobjSearchColumn("entityid").setSort(false), 
   new nlobjSearchColumn("altname"), 
   new nlobjSearchColumn("email"), 
   new nlobjSearchColumn("phone"), 
   new nlobjSearchColumn("internalid")
]
);
}