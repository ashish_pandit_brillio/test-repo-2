// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SUT_FulfillmentTeamDashboard.js
	Author      : ASHISH PANDIT
	Date        : 23 JAN 2019
    Description : Suitelet for Fulfillment team dashboard 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --
 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
//============================================================================================================
/*Image Links */ 
var calender = '<img src="https://system.netsuite.com/core/media/media.nl?id=858142&c=3883006_SB1&h=5dbf3c145cf776f760e6" class="calender_img">';
var team = '<img src="https://system.netsuite.com/core/media/media.nl?id=858152&c=3883006_SB1&h=fe7049c125b12a43a138">';
var exiting = '<img src="https://system.netsuite.com/core/media/media.nl?id=858146&c=3883006_SB1&h=e196781b2e0e4a1598aa">';
var rampup = '<img src="https://system.netsuite.com/core/media/media.nl?id=858137&c=3883006_SB1&h=f1348fbd7df81362ef3b">';
var star = '<img src="https://system.netsuite.com/core/media/media.nl?id=858143&c=3883006_SB1&h=8713920730d1e0e5c38d">';

var totalFRF = 0;
var totalRRF = 0;
var totalFRF_Booked = 0;
var totalFRF_MostLikely = 0;
var totalFRF_Stretached = 0;
var b_adminPerson = false;
/*Suitlet Main function*/
function CreateDashboard(request, response)
{
	try
	{
		var user = nlapiGetUser();
		var values = new Array();
		//var user = request.getParameter('custpage_user'); 
		nlapiLogExecution('Debug','Script Parameter user ',user);
		values['{myprojects}'] = nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
		values['{user}'] = nlapiGetContext().getName();
		values['{designation}'] = nlapiLookupField('employee',user,'title');
		
		/*Login user image*/
		if(nlapiLookupField('employee',user,'gender') == 'm')
		{
			values['{profilepic}'] = '/core/media/media.nl?id=860471&c=3883006_SB1&h=ba37c28ac5b42b0d12bc';
		}else{
			values['{profilepic}'] = '/core/media/media.nl?id=860472&c=3883006_SB1&h=f94f2ed7f683e8e277a4';
		}
		var customrecord_fuel_adminSearch = nlapiSearchRecord("customrecord_fuel_admin",null,
		[
		   ["custrecord_fuel_admin_name","anyof",user],
		   "AND",
		   ['isinactive','is', 'F'] 
		], 
		[
		   new nlobjSearchColumn("scriptid").setSort(false), 
		   new nlobjSearchColumn("custrecord_fuel_admin_name")
		]
		);
		if(customrecord_fuel_adminSearch)
		{
			values['{dashboardData}']  = getFrfDashboardData(null);
			b_adminPerson = true;
		}
		else
		{
			user= '1646';	
          	values['{dashboardData}']  = getFrfDashboardData(user);
		}
        values['{homeicon}']=nlapiResolveURL('SUITELET','customscript_fuel_sut_fulfillment_dash','customdeploy_fuel_sut_fulfillment_dash');
		values['{totalfrf}'] = totalFRF;
		values['{totalrrf}'] = totalRRF;
		values['{bookedfrf}'] = totalFRF_Booked;
		values['{mostlikely}'] = totalFRF_MostLikely;
		values['{stretched}'] = totalFRF_Stretached;
		var contents = nlapiLoadFile('1257138').getValue();
		contents = replaceValues(contents, values);
		//response.writeLine(JSON.stringify(arrayObj));
		response.write(contents);
	}
	catch(error)
	{
		nlapiLogExecution('Debug','Error ',error);
	}
}
/****Function to get FRF details ****/
function getFrfDashboardData(user)
{
	try
	{
		nlapiLogExecution('Debug','In Function getFrfDetails');
		var filters = [];
		filters.push(new nlobjSearchFilter("custrecord_frf_details_status",null,"is","F"));
		//filters.push(new nlobjSearchFilter("custrecord_frf_details_end_date",null,"onorafter","today"));
		filters.push(new nlobjSearchFilter("custrecord_frf_details_open_close_status",null,"noneof",2));
		if(user)
		{
			filters.push(new nlobjSearchFilter("custentity_projectmanager","CUSTRECORD_FRF_DETAILS_PROJECT","anyof",user));
		}
		var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null, filters,
		[
		   new nlobjSearchColumn("scriptid").setSort(false), 
		   new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
		   new nlobjSearchColumn("custrecord_frf_details_res_location"), 
		   new nlobjSearchColumn("custrecord_frf_details_res_practice"), 
		   new nlobjSearchColumn("custrecord_frf_details_emp_level"), 
		   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
		   new nlobjSearchColumn("custrecord_frf_details_role"), 
		   new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
		   new nlobjSearchColumn("custrecord_frf_details_critical_role"), 
		   new nlobjSearchColumn("custrecord_frf_details_skill_family"), 
		   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
		   new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"), 
		   new nlobjSearchColumn("custrecord_frf_details_primary_skills"), 
		   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
		   new nlobjSearchColumn("custrecord_frf_details_project"), 
		   new nlobjSearchColumn("custrecord_frf_details_suggestion"), 
		   new nlobjSearchColumn("custrecord_frf_type"), 
		   new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
		   new nlobjSearchColumn("custrecord_frf_details_billiable"), 
		   new nlobjSearchColumn("custrecord_frf_details_source"), 
		   new nlobjSearchColumn("custrecord_frf_details_secondary_skills"), 
		   new nlobjSearchColumn("custrecord_frf_details_allocation"), 
		   new nlobjSearchColumn("custrecord_frf_details_dollar_loss"), 
		   new nlobjSearchColumn("custrecord_frf_details_suggest_pref_mat"), 
		   new nlobjSearchColumn("custrecord_frf_details_special_req"), 
		   new nlobjSearchColumn("custrecord_frf_details_personal_email"), 
		   new nlobjSearchColumn("custrecord_frf_details_backup_require"), 
		   new nlobjSearchColumn("custrecord_frf_details_job_title"), 
		   new nlobjSearchColumn("custrecord_frf_details_edu_lvl"), 
		   new nlobjSearchColumn("custrecord_frf_details_edu_program"), 
		   new nlobjSearchColumn("custrecord_frf_details_emp_status"), 
		   new nlobjSearchColumn("custrecord_frf_details_first_interviewer"), 
		   new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"), 
		   new nlobjSearchColumn("custrecord_frf_details_cust_interview"), 
		   new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"), 
		   new nlobjSearchColumn("custrecord_frf_details_ta_manager"), 
		   new nlobjSearchColumn("custrecord_frf_details_req_type"), 
		   new nlobjSearchColumn("custrecord_frf_details_created_by"), 
		   new nlobjSearchColumn("custrecord_frf_details_parent"), 
		   new nlobjSearchColumn("custrecord_frf_details_account"), 
		   new nlobjSearchColumn("custrecord_frf_details_reason_external"),
		   new nlobjSearchColumn("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null), 
		   new nlobjSearchColumn("custrecord_customer_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null),
		   new nlobjSearchColumn("custrecord_projection_status_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)
		]
		);
		if(customrecord_frf_detailsSearch)
		{
			var dataArray = new Array();
			nlapiLogExecution('Debug','customrecord_frf_detailsSearch ',customrecord_frf_detailsSearch.length);
			for(var i=0; i<customrecord_frf_detailsSearch.length;i++)
			{
				totalFRF++;
				var b_billRoll = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_billiable");
				if(b_billRoll=='T')
				{
					b_billRoll=true;
				}
				else
				{
					b_billRoll = false;
				}
				
				var b_criticalRoll=customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_critical_role");
				if(b_criticalRoll=='T')
				{
					b_criticalRoll=true;
				}
				else
				{
					b_criticalRoll = false;
				}
				
				var b_externalHire =customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire");
				if(b_externalHire=='T')
				{
					b_externalHire=true;
				}
				else
				{
					b_externalHire = false;
				}
				var b_backupRequired = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_backup_require");
				if(b_backupRequired=='T')
				{
					b_backupRequired=true;
				}
				else
				{
					b_backupRequired=false;
				}
				var b_cust_interview = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_cust_interview");
				if(b_cust_interview=='T')
				{
					b_cust_interview=true;
				}
				else
				{
					b_cust_interview=false;
				}
				
				var s_account = customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account");
				if(!s_account)
				{
					s_account =  customrecord_frf_detailsSearch[i].getValue("custrecord_customer_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null);
					nlapiLogExecution('Debug','s_account ',s_account);
				}
				var s_project = customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project");
				if(!s_project)
				{
					s_project = customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null);
					nlapiLogExecution('Debug','s_project ',s_project);
				}
				var i_account = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account");
				
				var i_project = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project");
				var s_projection_status = customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null);
				var softlock = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp");
				nlapiLogExecution('Debug','s_projection_status ',s_projection_status);
				if(s_projection_status=="Booked" && (!softlock))
				{
					totalFRF_Booked++;
				}
				else if(s_projection_status=="Most Likely" && (!softlock))
				{
					totalFRF_MostLikely++;
				}
				else if(s_projection_status=="stretched" && (!softlock))
				{
					totalFRF_Stretached++;
				}
				var data = {};
				if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project"))
				{
					data.project = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_project"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_project")};	
				}
				else
				{
					data.project = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_opportunity_name_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)};
				}
				if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account"))
				{
					nlapiLogExecution('Debug','Account ',customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account"))
					data.account = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_account"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_account")};
				}
				else
				{
					data.account = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_customer_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)};
				}
				data.internalid = {"name":customrecord_frf_detailsSearch[i].getId()};
				data.frfnumber = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_frf_number")};
				data.role = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_role")};
				data.practice = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_practice"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_practice")};
				data.skillfamily = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_skill_family"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_skill_family")};
				data.primaryskills = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_primary_skills"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_primary_skills")};
				data.location = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_res_location"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_res_location")};
				data.startDate = {"name":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date")};
				var startDateCalculate = customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_start_date");
				var d_todaysDate = new Date();
				var d_startDate = new Date(startDateCalculate);
				data.frfage = {"name": datediff(d_startDate,d_todaysDate)};
				nlapiLogExecution('Debug','d_startDate '+d_startDate,'d_todaysDate '+d_todaysDate);
				nlapiLogExecution('Debug','Differnce Date ',datediff(d_startDate,d_todaysDate));
				data.softlockresource = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_frf_details_selected_emp"),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_selected_emp")};
				data.projectionstatus = {"name":customrecord_frf_detailsSearch[i].getText("custrecord_projection_status_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null),"id":customrecord_frf_detailsSearch[i].getValue("custrecord_projection_status_sfdc","CUSTRECORD_FRF_DETAILS_OPP_ID",null)};
				data.isadmin = {"name": b_adminPerson};
				if(customrecord_frf_detailsSearch[i].getValue("custrecord_frf_details_external_hire")=='T')
				{
					data.externalhire =  {"name":true};
					totalRRF++;
				}
				else
				{
					data.externalhire =  {"name":false};
				}
				dataArray.push(data);
			}
			
			nlapiLogExecution('Debug','dataArray$$$$$ ',JSON.stringify(dataArray));
			return JSON.stringify(dataArray);
		}
	}
	catch(error)
	{
		nlapiLogExecution('Error','Exception ',error);
	}
}
function replaceValues(content, oValues)
{
	for(param in oValues)
	{
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
		//content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
		content = content.replace(param, s_value);
	}

	return content;
}

/*********************************************Function to get Employee Level Values**********************************************/
function getEmpLevelValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn("employeestatus",null,"GROUP");
	
	var a_search_results = searchRecord(null, s_list_name, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getText("employeestatus",null,"GROUP"), 'value':a_search_results[i].getText("employeestatus",null,"GROUP")});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get List Values**********************************************/
function getListValues(s_list_name, strSelectedValue, s_placeholder, removeBlankOption)
{
	var columns = new Array();
	columns[0]	=	new nlobjSearchColumn('name');
	
	var a_search_results = searchRecord(s_list_name, null, null, columns);
	
	var list_values = new Array();
	
	for(var i = 0; a_search_results != null && i < a_search_results.length; i++)
		{
			list_values.push({'display':a_search_results[i].getValue('name'), 'value':a_search_results[i].id});
		}
	
	return getOptions(list_values, strSelectedValue, s_placeholder, removeBlankOption);
}

/*********************************************Function to get Search Results**********************************************/
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
/*********************************************Function for adding selected options**********************************************/
function getOptions(a_data, i_selected_value, s_placeholder, removeBlankOptions)
{
	var strOptions	=	'';
	
	if(removeBlankOptions == true)
		{
		
		}
	else
		{
			strOptions = '<option value = "">' + (s_placeholder == undefined?'':s_placeholder) + '</option>';
		}

	var strSelected = '';
	
	for(var i = 0; i < a_data.length; i++)
	{
		if(i_selected_value == a_data[i].value)
			{
				strSelected = 'selected';
			}
		else
			{
				strSelected = '';
			}
		strOptions += '<option value = "'+a_data[i].value+'" ' + strSelected + ' >' + a_data[i].display + '</option>';
	}
//nlapiLogExecution('Debug', 'stringsss', strOptions);
	return strOptions;
}

function datediff(first, second) 
{
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second-first)/(1000*60*60*24));
}