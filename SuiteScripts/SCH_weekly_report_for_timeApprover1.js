var Timesheet_Approval_Status = {
    Approved : '3',
    PendingApproval : '2',
    Rejected : '4',
    Open : '1'
};





function sch_weekly_timesheet_timeapprover(){
	

	try{
		//custentity_reportingmanager
		var context = nlapiGetContext();
		
		// list of all active employees
		var all_employee_list = [];
		searchRecord(
				'employee',
				null,
			   [ new nlobjSearchFilter('custentity_employee_inactive', null, 'is',
						'F'),new nlobjSearchFilter('custentity_implementationteam', null, 'is',
						'F')],null).forEach(function(employee) {

			all_employee_list.push(employee.getId());
		});
		
		var all_timeapprover_list = []; //,new nlobjSearchFilter('timeapprover', null, 'is','F')
						
		searchRecord(
				'employee',
				null,
			   [ new nlobjSearchFilter('custentity_employee_inactive', null, 'is',
						'F'),new nlobjSearchFilter('custentity_implementationteam', null, 'is',
						'F')],[ new nlobjSearchColumn('timeapprover')] ).forEach(function(employee){
							
					if(all_timeapprover_list.indexOf(employee.getValue('timeapprover')) == -1){
							all_timeapprover_list.push(employee.getValue('timeapprover'));
					}
		});
		
		
		var active_timeapprover_list = [];
		
		for(var i=0;i<all_employee_list.length;i++){
			
			if(all_employee_list.indexOf(all_timeapprover_list[i]) != -1){
							active_timeapprover_list.push(all_timeapprover_list[i]);
					}
			
		}	
		
		var excluded_practice = [];
		searchRecord(
				'customrecord_excluded_prj_lst_ta',
				null,
			   [ new nlobjSearchFilter('isinactive', null, 'is',
						'F')],[ new nlobjSearchColumn('custrecord_exclude_practice')] ).forEach(function(practice){
							
					if(excluded_practice.indexOf(practice.getValue('custrecord_exclude_practice')) == -1){
							excluded_practice.push(practice.getValue('custrecord_exclude_practice'));
					}
		});
		
		
		
		
			var d_week_end_date = new Date();
			d_week_end_date = nlapiAddDays(d_week_end_date, -3); //saturday
			var d_week_start_date = nlapiAddDays(d_week_end_date, -6); // sunday
			
			var s_week_start_date = nlapiDateToString(d_week_start_date, 'date');
			var s_week_end_date = nlapiDateToString(d_week_end_date, 'date');


		var globalarray = [];
		var arr=[];
		
		var str = [];
		var strVar = '';
		var j = 1;
		var n =0;
      
               var a_emp_attachment = new Array();
			        a_emp_attachment['entity'] =  '140512'; 
		
		//active_timeapprover_list = [86633];
		
		for(var i=0;i<active_timeapprover_list.length;i++){
			
			yieldScript(context);
			
			nlapiLogExecution('Debug','timeapprover -> '+i + ' -> ',active_timeapprover_list[i]);
			
			arr[0] = "Fusion Id,Brillian,Level,Role,Practice,Project,Project Manager,Allocation Percentage ,No. of Hours,Timesheet Id,Status";
									//"\n" ;
									
			globalarray[0]=arr[0];
			
			var SearchEmployeeData = search_all_resources(s_week_start_date,s_week_end_date,active_timeapprover_list[i]);
			nlapiLogExecution('DEBUG', 'GET results ', ' SearchEmployeeData=' + SearchEmployeeData.length)
		/*	
			if(SearchEmployeeData == 0){
					continue;
				}
		*/	
			var timesheetdata_WK_1 = search_timesheet_weekly(s_week_start_date,s_week_end_date,active_timeapprover_list[i]);
			nlapiLogExecution('DEBUG', 'GET results ', ' timesheetdata_WK_1=' + timesheetdata_WK_1.length)
			
			var result_emp = '';
			var emp_id = '';
			var project_id = '';
			var fusion_id = '';
			var level = '';
			var project_name = '';
			var emp = '';
			var allocation = '';
			var r_start_date = '';
			var r_end_date = '';
			
			var result_wk1 = '';
			var emp_wk1 = '';
			var project_id_wk1 = '';
			var approval_status = '';
			var total_hr = '';
			var timesheet_id = '';
			var role = '';
			var brillian = '';
			
			
			
			
			var m = 0;
			var n = 1;
			for(var j=0; j<SearchEmployeeData.length;j++){
				
				yieldScript(context);
				
				
				
				result_emp = SearchEmployeeData[j];
				emp_id = result_emp.split('##')[0];
				project_id = result_emp.split('##')[4];
				
				fusion_id = result_emp.split('##')[1];			
				emp = result_emp.split('##')[2];
				
				brillian = emp.split('-')[1];	
				
				if(brillian == null || brillian == undefined ){
					brillian = emp.split('-')[0];
				}			
				
				
				
				level = result_emp.split('##')[3];
				project_name = result_emp.split('##')[5];
				allocation = result_emp.split('##')[6];
				r_start_date = result_emp.split('##')[7];
				r_end_date = result_emp.split('##')[8];
				r_company = result_emp.split('##')[10];
				r_rept_manag = result_emp.split('##')[11];
				r_practice = result_emp.split('##')[12];
				
				if(timesheetdata_WK_1.length == 0 ||timesheetdata_WK_1 == 0 || timesheetdata_WK_1.length == 'null' && timesheetdata_WK_1.length == 'undefined'){
					
					approval_status = 'Not Submitted';
					
				}else{
				
						for(var k=0;k<timesheetdata_WK_1.length;k++){
							
							result_wk1 = timesheetdata_WK_1[k];
							emp_wk1 = result_wk1.split('##')[0];
							project_id_wk1 = result_wk1.split('##')[3];
							
							approval_status = result_wk1.split('##')[5];
							total_hr = result_wk1.split('##')[1];
							timesheet_id = result_wk1.split('##')[6];
							
							if (approval_status != '' && approval_status != null && approval_status != 'undefined') {
								approval_status = approval_status;
								 }
								else {
									approval_status = 'Not Submitted';
								}
						   
						   if (total_hr != '' && total_hr != null && total_hr != 'undefined') {
								total_hr = total_hr;
								 }
								else {
									total_hr = '';
								}
								
							if (timesheet_id != '' && timesheet_id != null && timesheet_id != 'undefined') {
							timesheet_id = timesheet_id;
							 }
							else {
								timesheet_id = '';
							}
							
							if(emp_id == emp_wk1 && project_id == project_id_wk1 ){
								total_hr = total_hr;
								approval_status = approval_status;
								timesheet_id = timesheet_id;
								break;
							}else{
								total_hr = '';
								approval_status = 'Not Submitted';
								timesheet_id = '';
							}
							
							
							
							
						}
				}
				
									strVar += "<tr>";
									strVar += "<td>" + n + "</td>";
									strVar += "<td>" + fusion_id+"</td>";
									strVar += "<td>" + brillian +"</td>";
									strVar += "<td>" + level +"</td>";
									strVar += "<td>" + role +"</td>";
									strVar += "<td>" + r_practice +"</td>";
									strVar += "<td>" + r_company + "</td>";
									strVar += "<td>" + r_rept_manag + "</td>";								
									strVar += "<td>" + allocation +"</td>";
								//	strVar += "<td>" + r_start_date +"</td>";
								//	strVar += "<td>" + r_end_date +"</td>";
									strVar += "<td>" + total_hr +"</td>";
									strVar += "<td>" + timesheet_id +"</td>";
									strVar += "<td>" + approval_status +"</td>";
									strVar += "</tr>";	
									
									
									str.push(strVar);   
									strVar = "";		
				
				
							
				
				
				arr[m+1] = "\n" + fusion_id +
									"," +
									brillian +               
									"," +
									level +
									"," +
									role +
									"," +
									r_practice +
									"," +
									r_company +
									"," +
									r_rept_manag +
									"," +
									allocation +
									"," +
							//		r_start_date +
							//		"," +
							//		r_end_date +
							//		"," +
									total_hr +
									"," +
									timesheet_id +
									"," +
									approval_status ;
									
									
									globalarray[m+1] = (arr[m+1]);
				
				//global_array
				
				m++;
				n++;
				
			}
			
			if(SearchEmployeeData.length <= 20){
				
				
				if(SearchEmployeeData.length == 0){
					strVar = '';
					str =[];
					
					 globalarray = [];
					 arr = [];
					
					continue;
				}
				
											var subject ='Timesheet summary for week of '+ s_week_start_date + ' to ' + s_week_end_date;	
										
												var timeapprover_emp_detail = '';
												
												for(var l=0; l<str.length; l++)
												{
													timeapprover_emp_detail += str[l];
												}
												
												strVar += "<table border=\"1\"  align=\"center\" width=\"100%\">"; 
												strVar += "<tr>";
												strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>S.No.</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"10%\" scope='col'><b>Fusion Id</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"10%\" scope='col'><b>Brillian<b/></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>Level</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>Role</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"20%\" scope='col'><b>Practice</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"30%\" scope='col'><b>Project</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"30%\" scope='col'><b>Reporting Manager</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>Allocation Percentage</b></th>";
										//		strVar += "<th border-bottom=\"0.3\" width=\"100%\" scope='col'><b>Start Date</b></th>";
										//		strVar += "<th border-bottom=\"0.3\" width=\"100%\" scope='col'><b>End Date</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"5%\" scope='col'><b>No. of Hours</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"20%\" scope='col'><b>Timesheet Id</b></th>";
												strVar += "<th border-bottom=\"0.3\" width=\"20%\" scope='col'><b>Status</b></th>";
												strVar += "</tr>";	
												strVar += timeapprover_emp_detail;
												strVar += "</table>";
												
												var timeapprover_name = nlapiLookupField('employee', active_timeapprover_list[i], 'entityid');
												
												   var timeapprover_full_name = timeapprover_name.split('-')[1];
												   
												   if(timeapprover_full_name == undefined || timeapprover_full_name == null)
													{
														var timeapprover_full_name = timeapprover_name.split('-')[0];
													}
												
												var timeapprover_mail = nlapiLookupField('employee', active_timeapprover_list[i], 'email');
												
												var body = 'Dear ' + timeapprover_full_name + ', <br /><br />' +  'Below is the summary of the timesheets submitted/not submitted by your team for the week of <b>'+ s_week_start_date + '</b>.<br /><br />'+ strVar; 
									//		timeapprover_mail = "prabhat.gupta@brillio.com"
											 nlapiSendEmail(442, timeapprover_mail, subject,body, null, null, a_emp_attachment, null);
											 nlapiLogExecution('Debug','Email Sent to ->',timeapprover_mail);
												
											
												strVar = '';
												str =[];
												
												 globalarray = [];
												 arr = [];
										/*		
												if(i==7){
													 break;
												 }
				
										*/
				
				
			}
			else{		
			
		//	if(timesheetdata_WK_1 != 0 || timesheetdata_WK_1.length == 'null'){
							 
							var fileName = 'Weekly Timesheet '+ s_week_start_date +' Report for Timeapprover';
							var Datetime = new Date();
							var CSVName = fileName + " - " + Datetime + '.csv';
							var file = nlapiCreateFile(CSVName, 'CSV', globalarray.toString());
							
							 var timeapprover_name = nlapiLookupField('employee', active_timeapprover_list[i], 'entityid');
												
							   var timeapprover_full_name = timeapprover_name.split('-')[1];
							   
							   if(timeapprover_full_name == undefined || timeapprover_full_name == null)
								{
									var timeapprover_full_name = timeapprover_name.split('-')[0];
								}
			
			
							var subject ='Timesheet summary for week of '+ s_week_start_date + ' to ' + s_week_end_date;	
							
							var body = 'Dear ' + timeapprover_full_name + ', <br /><br />' +'Please see the attached file for timesheets submitted/not submitted by your team for the week of <b>'+ s_week_start_date +'</b>.';
			
							
							
							
							var timeapprover_mail = nlapiLookupField('employee', active_timeapprover_list[i], 'email');
							
						  //  response.setContentType('CSV', CSVName);
						  //  response.write(file.getValue());
							//140512
							 nlapiSendEmail(442, timeapprover_mail, subject,body, null, null, a_emp_attachment, file);
					
							nlapiLogExecution('Debug','Email Sent to ->',timeapprover_mail);
							
											  strVar = '';
												str =[];
												
								 globalarray = [];
								 arr = [];
							/*	 
								 if(i==7){
									 break;
								 }
							*/	 
							   
			}			
			
		}
		
			
		
		}
		catch(e){
			nlapiLogExecution('Debug','process error',e);
		}
		
		function search_all_resources(start_date,end_date,time_approver){
		
		var resourceallocationSearch = searchRecord("resourceallocation",null,
														[
														   ["startdate","notafter",start_date], 
														   "AND", 
														   ["enddate","notbefore",end_date],
														   "AND", 
														   ["employee.timeapprover","anyof",time_approver]
														   
														], 
														[
														   new nlobjSearchColumn("internalid","employee",null), 
														   new nlobjSearchColumn("custentity_fusion_empid","employee",null), 
														   new nlobjSearchColumn("resource").setSort(false), 
														   new nlobjSearchColumn("employeestatus","employee",null), 
														   new nlobjSearchColumn("entityid","job",null).setSort(false), 
														   new nlobjSearchColumn("altname","job",null), 
														   new nlobjSearchColumn("percentoftime"), 
														   new nlobjSearchColumn("startdate"), 
														   new nlobjSearchColumn("enddate"), 
														   new nlobjSearchColumn("timeapprover","employee",null).setSort(false),
														   new nlobjSearchColumn("company"), 
														   new nlobjSearchColumn("custentity_reportingmanager","employee",null),
														   new nlobjSearchColumn("department","employee",null)
														]
													);
													
													
													
			if (resourceallocationSearch != null && resourceallocationSearch != undefined && resourceallocationSearch != '') //
			{				

						
													
							var emp_id='';
							var fusion_id = '';
							var resource = '';
							var level = '';
							var project_id = '';
							var project_name = '';
							var alloc_perc = '';
							var start_date = '';
							var end_date = '';
							var time_approver = '';
							var company_project = '';
							var report_manager = '';
							var practice = '';
							var practice_id = '';
							var flag = 0;
							 var resource_search_final = [];
							
							for(var i=0;i<resourceallocationSearch.length;i++){
								
								 emp_id      =     resourceallocationSearch[i].getValue("internalid","employee",null);
								 fusion_id   =     resourceallocationSearch[i].getValue("custentity_fusion_empid","employee",null);
								 resource    =     resourceallocationSearch[i].getText("resource");
								 level       =     resourceallocationSearch[i].getText("employeestatus","employee",null); 
								project_id   =	   resourceallocationSearch[i].getValue("entityid","job",null);
								project_name =	   resourceallocationSearch[i].getValue("altname","job",null) ;
								alloc_perc   =		resourceallocationSearch[i].getValue("percentoftime");
								start_date   =		resourceallocationSearch[i].getValue("startdate");
								end_date     =		resourceallocationSearch[i].getValue("enddate");							
							   time_approver =	   resourceallocationSearch[i].getValue("timeapprover","employee",null);
							   company_project =    resourceallocationSearch[i].getText("company");
							   report_manager =    resourceallocationSearch[i].getText("custentity_reportingmanager","employee",null);
							   practice =           resourceallocationSearch[i].getText("department","employee",null);
							   
							   practice_id = resourceallocationSearch[i].getValue("department","employee",null);
							   
							   for(var j=0;j<excluded_practice.length;j++){
								   
								   if(practice_id == excluded_practice[j]){
									   flag = 1;
									   break;
								   }else{
									   flag = 0;
								   }
								   
							   }
							   
							   if(flag==1){
								   continue;
							   }
							   
							   if(project_id == "VEDS01780"){								   
								    
								   continue;								   
								  
							   }
							   
							   resource_array_string = emp_id + '##' + fusion_id + '##' + resource + '##' + level + '##' + project_id + '##' + project_name + '##' + alloc_perc + '##' + start_date + '##' + end_date + '##' + time_approver + '##' + company_project + '##' + report_manager + '##' + practice;
							   
							   resource_search_final.push(resource_array_string);
							   
								
							}
					return resource_search_final;		
			}
			
			return resourceallocationSearch;			

	}
	
	
		function search_timesheet_weekly(start_date,end_date,time_approver){
		
		//   nlapiLogExecution('Debug','timeapprover -> '+i + ' -> ',time_approver);
		
		var timeentrySearch = searchRecord("timeentry",null,
												[
												   
												   ["type","anyof","A"], 
												   "AND", 											
												   ["date","within",start_date,end_date],
												   "AND", 
													["employee.timeapprover","anyof",time_approver]
												], 
												[
												   new nlobjSearchColumn("durationdecimal",null,"SUM"), 
												   new nlobjSearchColumn("internalid","employee","GROUP"), 
												   new nlobjSearchColumn("employee",null,"GROUP").setSort(false), 
												   new nlobjSearchColumn("entityid","job","GROUP").setSort(false), 
												   new nlobjSearchColumn("altname","job","GROUP"), 
												   new nlobjSearchColumn("approvalstatus",null,"GROUP"), 
												   new nlobjSearchColumn("custrecord_ts_time_approver","timeSheet","GROUP").setSort(false), 
												   new nlobjSearchColumn("id","timeSheet","GROUP")
												]
											);
											
			
					//   new nlobjSearchColumn("custrecord_ts_time_approver","timeSheet","GROUP").setSort(false), 
									
											
			if (timeentrySearch != null && timeentrySearch != undefined && timeentrySearch != '') //
			{					
					var t_emp_id = '';
					var t_total_hrs = '';
					var t_resource = '';
					var t_project_id = '';
					var t_project_name = '';
					var t_approval_status = '';
					var t_timesheet_id = '';
					var t_time_approver = '';
					var timesheet_search_final = [];
			
						for(var i=0;i<timeentrySearch.length;i++){
							
							var array_string = '';
							
								 t_emp_id            =     timeentrySearch[i].getValue("internalid","employee","GROUP");
								 t_total_hrs         =     timeentrySearch[i].getValue("durationdecimal",null,"SUM");
								 t_resource          =     timeentrySearch[i].getText("employee",null,"GROUP");
								  
								 t_project_id        =	   timeentrySearch[i].getValue("entityid","job","GROUP");
								 t_project_name      =	   timeentrySearch[i].getValue("altname","job","GROUP") ;
								 t_approval_status   =		timeentrySearch[i].getText("approvalstatus",null,"GROUP");
								 
								 t_timesheet_id      =	   timeentrySearch[i].getValue("id","timeSheet","GROUP");
								 t_time_approver     =	   timeentrySearch[i].getValue("custrecord_ts_time_approver","timeSheet","GROUP");
							
							timesheet_array_string = t_emp_id + '##' + t_total_hrs + '##' + t_resource + '##' + t_project_id + '##' + t_project_name + '##' + t_approval_status + '##' + t_timesheet_id + '##' + t_time_approver;
							
							timesheet_search_final.push(timesheet_array_string);
						}	

						return timesheet_search_final;
			}	
			
		return timeentrySearch;	
	}
		
		
		
		function yieldScript(currentContext) {

		if (currentContext.getRemainingUsage() <= 100) {
			nlapiLogExecution('AUDIT', 'API Limit Exceeded');
			var state = nlapiYieldScript();

			if (state.status == "FAILURE") {
				nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
						+ state.reason + ' / Size : ' + state.size);
				return false;
			} else if (state.status == "RESUME") {
				nlapiLogExecution('AUDIT', 'Script Resumed');
			}
		}
	}
}
