 /**
 * Module Description
 * 60 days questionnaire for new joiners 
 * 
 * Version                      Date                                                Author                           Remarks
 * 1.00                         17th Oct, 2017                                  Manikandan V          
 *
 */

function feedbackSuitelet(request, response)
{
  var method	=request.getMethod();
  
  if (method == 'GET' )
  {
var employee_id= request.getParameter('custparam_employee_id'); 
	
  nlapiLogExecution('Debug','emp_id in suitelet:- ',employee_id);
  var filters = new Array();
	filters[0]  = new nlobjSearchFilter('internalid',null,'is',parseInt(employee_id));
   
	var columns   = new Array();
	columns[0]    = new nlobjSearchColumn('entityid');
	columns[1]    = new nlobjSearchColumn('custentity_fusion_empid');
	columns[2]    = new nlobjSearchColumn('department');
	columns[3]    = new nlobjSearchColumn('title');
	columns[4]    = new nlobjSearchColumn('custentity_actual_hire_date');
	columns[5]    = new nlobjSearchColumn('custentity_probationenddate');
	columns[6]    = new nlobjSearchColumn('custentity_reportingmanager');
	columns[7]    = new nlobjSearchColumn('location');
	columns[8]    = new nlobjSearchColumn('middlename');
	columns[9]    = new nlobjSearchColumn('lastname');
	columns[10]	  = new nlobjSearchColumn('email', 'custentity_reportingmanager');
	
   var emp_search = nlapiSearchRecord('employee',null,filters,columns);
   nlapiLogExecution('debug','emp_search:- ',emp_search.length);
   
   var surveyFilter = new Array();
   filters[0]  = new nlobjSearchFilter('custrecordemp_name',null,'is',emp_search[0].getValue('entityid') );
   nlapiLogExecution('Debug','entity in suitelet:- ',emp_search[0].getValue('entityid'));
   
   var survey_search =  nlapiSearchRecord('customrecord_60_days_survey_us',null,surveyFilter,null);
   nlapiLogExecution('Debug','survey_search in suitelet:- ',survey_search );
   
   if(survey_search)
   {
	   var html="<html><body><h2>You have already submitted the Survey. Thank you !!!</h2></body></html>"
   }
   
    var file = nlapiLoadFile(578256);   //load the HTML file
    var contents = file.getValue();    //get the contents
   // response.write(contents);     //render it on  suitlet
	
	if(isNotEmpty(emp_search))
		{
			var objReplaceValues=new Object();
			objReplaceValues['employee_name']=emp_search[0].getValue('entityid');
            objReplaceValues['employee_number']=emp_search[0].getValue('custentity_fusion_empid');
            objReplaceValues['employee_pratcice']=emp_search[0].getText('department');
            objReplaceValues['date_of_joining']=emp_search[0].getValue('custentity_actual_hire_date');
            objReplaceValues['employee_location']=emp_search[0].getText('location');
            objReplaceValues['employee_id_hidden']=emp_search[0].getId();
			contents = replaceValues(contents, objReplaceValues);
            
			response.write(contents);          //render it on  suitlet
		}
	
  }
  else
  {
  //var form = nlapiCreateForm("Suitelet - POST call" );
	
 var feedback = new Object();
  
  feedback['employee_id'] =  request.getParameter('EmployeeID');
  nlapiLogExecution('DEBUG','employeeid',feedback.employee_id);
  feedback['feedback_record_id'] =request.getParameter('feedback_record_id');
  
  feedback['Name'] = request.getParameter('TextBox1');
  feedback['EmployeeDepartment'] = request.getParameter('TextBox2');
  feedback['DateOfJoin'] = request.getParameter('TextBox3');
  feedback['Location'] = request.getParameter('TextBox4');
 
  feedback['Response1'] = request.getParameter('OptionsGroup1');
  feedback['Response2'] = request.getParameter('OptionsGroup2');
  feedback['Response3'] = request.getParameter('OptionsGroup4');
  
  feedback['Response4'] = request.getParameter('OptionsGroup6');
  feedback['Response5'] = request.getParameter('OptionsGroup7');
  
  feedback['Response6'] = request.getParameter('OptionsGroup5');
  feedback['Response7'] = request.getParameter('OptionsGroup10');
  feedback['Response8'] = request.getParameter('OptionsGroup11');
  
  feedback['comments1'] = request.getParameter('txtcomments1');
  
  
  var filters = new Array();
	filters[0]  = new nlobjSearchFilter('internalid',null,'is',request.getParameter('EmployeeID'));
   
	var columns   = new Array();
	columns[0]    = new nlobjSearchColumn('entityid');
	columns[1]    = new nlobjSearchColumn('custentity_fusion_empid');
	columns[2]    = new nlobjSearchColumn('department');
	columns[3]    = new nlobjSearchColumn('title');
	columns[4]    = new nlobjSearchColumn('custentity_actual_hire_date');
	columns[5]    = new nlobjSearchColumn('custentity_probationenddate');
	columns[6]    = new nlobjSearchColumn('custentity_reportingmanager');
	columns[7]    = new nlobjSearchColumn('location');
	columns[8]    = new nlobjSearchColumn('middlename');
	columns[9]    = new nlobjSearchColumn('lastname');
	columns[10]	  = new nlobjSearchColumn('email', 'custentity_reportingmanager');
   
  var emp_search = nlapiSearchRecord('employee',null,filters,columns);
  
 feedback['employee_name']     = emp_search[0].getValue('entityid');
 feedback['employee_pratcice'] = emp_search[0].getText('department');
 feedback['actual_hire_date']  = emp_search[0].getValue('custentity_actual_hire_date');
 feedback['location']          = emp_search[0].getText('location');
 
 
  var status = saveRequest(feedback);
   //response.write("Thank You!");
   //response.write("Your survey responses have been recorded!");
 var thanks_note = nlapiLoadFile(588851);   //load the HTML file
  var thanks_contents = thanks_note.getValue();    //get the contents
  response.write(thanks_contents);     //render it on  suitlet
 
   }
   
   // Used to display the html, by replacing the placeholders
function replaceValues(content, oValues)
{
	for(param in oValues)
    {
		// Replace null values with blank
		var s_value	=	(oValues[param] == null)?'':oValues[param];

		// Replace content
        content = content.replace(new RegExp('{{' + param + '}}','gi'), s_value);
    }
                
    return content;
}   

  
function saveRequest(feedback)
{
	try
	{
               var feedback_form = nlapiCreateRecord('customrecord_60_days_survey_us');
			   var empId= feedback.employee_id;
			 
              feedback_form.setFieldValue('custrecord_employee_name_60', feedback.Name);
	          feedback_form.setFieldValue('custrecord_employee_dept_60', feedback.EmployeeDepartment);
			  feedback_form.setFieldValue('custrecord_employee_date_of_join_60', feedback.DateOfJoin);
			  feedback_form.setFieldValue('custrecord_employee_location', feedback.Location);
               
			 nlapiLogExecution('DEBUG','employee_id_post',feedback.employee_id);
			
			 feedback_form.setFieldValue('custrecord_query_1_60',"My organization communicates effectively and in a timely manner to its employees");
             feedback_form.setFieldValue('custrecord_query_2_60', "My manager/supervisor is giving me timely and regular feedback");
             feedback_form.setFieldValue('custrecord_query_3_60', "My suggestions and contributions are well recognized by my manager");
			 
			 feedback_form.setFieldValue('custrecord_query_4_60', "Team meetings are conducted regularly");
			 feedback_form.setFieldValue('custrecord_query_5_60', "Everyone in the team is professional and supportive");
			 feedback_form.setFieldValue('custrecord_query_6_60', "Individual differences are respected (Age, gender, caste, education, etc.)");
			 
			 feedback_form.setFieldValue('custrecord_query_7_60', "Adequate resources are provided to get the job done");
			 feedback_form.setFieldValue('custrecord_query_8_60', "I have flexible shift timings");
			 feedback_form.setFieldValue('custrecord_answer_9_60', "Nominate a Culture Champion who has any of the below mentioned qualities and explain why have you nominated him/her? 1.Inspired By Potential, 3. Insanely Competitive, 4. Responsive Beyond Imagination");
			  
			 
             
			  
             feedback_form.setFieldValue('custrecord_answer_1_60', feedback.Response1);
             feedback_form.setFieldValue('custrecord_answer_2_60', feedback.Response2);
             feedback_form.setFieldValue('custrecord_answer_3_60', feedback.Response3);
			 feedback_form.setFieldValue('custrecord_answer_4_60', feedback.Response4);
             feedback_form.setFieldValue('custrecord_answer_5_60', feedback.Response5);
			 feedback_form.setFieldValue('custrecord_answer_6_60', feedback.Response6);
             feedback_form.setFieldValue('custrecord_answer_7_60', feedback.Response7);
			 feedback_form.setFieldValue('custrecord_answer_8_60', feedback.Response8);
			 feedback_form.setFieldValue('custrecord_comments_9_60', feedback.comments1);
			
               
			  //Employee details
			  nlapiLogExecution('DEBUG','name,prac,hiredate,loc',feedback.employee_name+","+feedback.employee_pratcice+","+feedback.actual_hire_date+","+feedback.location);
              feedback_form.setFieldValue('custrecord_employee_name_60', empId);
			  feedback_form.setFieldText('custrecord_employee_dept_60', feedback.employee_pratcice)
			  
              feedback_form.setFieldValue('custrecord_employee_date_of_join_60', feedback.actual_hire_date);
			  feedback_form.setFieldText('custrecord_employee_location', feedback.location);
              
               var id = nlapiSubmitRecord(feedback_form, false,true);
               nlapiLogExecution('debug', 'Record Saved', id);
        }
catch(err)
{
nlapiLogExecution('error', 'Record not saved', err);
feedback_form.setFieldValue('custrecord_error_log','');
}
}
 }