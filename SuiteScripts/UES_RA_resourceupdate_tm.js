	/*	Script Name:    UES T&M Revenue Creation 
	 Author:        Sai Saranya
	 Company:		Brillio
	 Date:
	 Version:
	 
	 Description:	This script Call Schedule Script to Create a new record for revenue amount for T&M projects.
	 * Script Modification Log:
	 * 
	 -- Date --		-- Modified By --			--Requested By--				-- Description --
	 11 June 2018         Supriya                    Deepak                  
	 Below is a summary of the process controls enforced by this script file.  The control logic is described
	 more fully, below, in the appropriate function headers and code blocks.
	 
	 BEFORE LOAD
	 - beforeLoadRecord(type)
	 Not Used
	 
	 BEFORE SUBMIT
	 - beforeSubmitRecord(type)
	 Not Used
	 
	 AFTER SUBMIT
	 - afterSubmitRecord(type)
	 afterSubmitRecord(type)
	 
	 SUB-FUNCTIONS
	 - The following sub-functions are called by the above core functions in order to maintain code
	 modularization:
	
	 */
function beforeLoadRecord(type, form)	
{
	
	    return true;  
	    
}
	// END BEFORE LOAD ====================================================
	// BEGIN BEFORE SUBMIT ================================================
	
function beforeSubmitRecord_ait_adjustTDS_Amt(type){
		
		return true;
}
	
	// END BEFORE SUBMIT ==============================================
	
	// BEGIN AFTER SUBMIT =============================================
function afterSubmitRecord(type)	
{
	//if ((type == 'create') || (type == 'edit'))
	{
		var project_array = new Array()
		var id = nlapiGetRecordId();
		var project_id = nlapiGetFieldValue('project');
		project_array['custscript_tm_project_id'] = project_id ;
		var billable = nlapiGetFieldValue('custeventrbillable');
		var project_details = nlapiLookupField('job',project_id,['jobbillingtype','jobtype']);
		var project_type = project_details.jobbillingtype;
		var internal_exterbal = project_details.jobtype;
		if(billable == 'T' && project_type == 'TM')
		{
			var status = nlapiScheduleScript('customscript_resouce_alloc_update','customdeploy1',project_array);
		}
	      
	}
	
		
}// afterSubmitRecord(type)	
	
	