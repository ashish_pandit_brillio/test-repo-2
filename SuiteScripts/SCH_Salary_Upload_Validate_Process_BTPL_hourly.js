// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : SCH_Salary_Upload_Validate_Process_BTPL_hourly.js
	Author      : Jayesh Dinde
	Date        : 11 April 2016
    Description : Processing of data to check for errors


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

	24-07-2018			  Ashish Pandit					    Govind  					Script modified for BLLC 


Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction(type) //
{
	nlapiLogExecution('DEBUG', 'schedulerFunction', '*** into schedulerFunction hourly ***');
	
	var i_mnt = 0;
	var i_hnt = 0;
	var i_file_status = '';
	var f_is_monthly = 0;
	var f_is_hourly = 0;
	var i_recordID = '';
	 
	try //
	{
		var i_context = nlapiGetContext();
		var sal_upload_date_month = i_context.getSetting('SCRIPT', 'custscript_sal_upload_month_btpl');
		var sal_upload_date_year = i_context.getSetting('SCRIPT', 'custscript_sal_upload_year_btpl');
		var i_account_debit = i_context.getSetting('SCRIPT', 'custscript_account_debit_btpl');
		var i_account_credit = i_context.getSetting('SCRIPT', 'custscript_account_credit_btpl');
		i_recordID = i_context.getSetting('SCRIPT', 'custscript_record_id_btpl');
		var i_flag = i_context.getSetting('SCRIPT', 'custscript_flag_btpl');
		var vendor_provision_subsidiary = i_context.getSetting('SCRIPT', 'custscript_vendor_subsidiary');
		//nlapiLogExecution('DEBUG', 'schedulerFunction', '1 i_flag -->' + i_flag);
		
		
		if (i_flag == null || i_flag == '') //
		{
			i_flag = 0;
		}
		
		var i_usage_begin = i_context.getRemainingUsage();
		//nlapiLogExecution('DEBUG', 'schedulerFunction', ' Usage Begin -->' + i_usage_begin);
		
		var i_counter = i_context.getSetting('SCRIPT', 'custscript_counter_btpl');
		
		if (i_counter != null && i_counter != '' && i_counter != undefined) //
		{
			i_counter = i_counter;
		}//if
		else //
		{
			i_counter = 0;
		}
		
		var current_status = i_context.getSetting('SCRIPT', 'custscript_vendor_pro_status');
		nlapiLogExecution('audit','status:--',current_status);
		if (current_status == 2 || current_status == 7 || current_status == 5)
		{
			var filters_existing_err_logs = new Array();
			filters_existing_err_logs[0] = new nlobjSearchFilter('custrecord_month_ven_pro', null, 'is', sal_upload_date_month.trim());
			filters_existing_err_logs[1] = new nlobjSearchFilter('custrecord_year_ven_pro', null, 'is', sal_upload_date_year.trim());
			filters_existing_err_logs[2] = new nlobjSearchFilter('custrecord_rate_type_error_log', null, 'is', 3);
			filters_existing_err_logs[3] = new nlobjSearchFilter('custrecord_subsidiary_ven_pro', null, 'is', parseInt(vendor_provision_subsidiary));
			
			var i_search_results_existing_err_logs = nlapiSearchRecord('customrecord_error_logs_vendor_pro', null, filters_existing_err_logs, null);
			if(_logValidation(i_search_results_existing_err_logs))
			{
				for(var i=0; i< i_search_results_existing_err_logs.length; i++)
					nlapiDeleteRecord('customrecord_error_logs_vendor_pro',i_search_results_existing_err_logs[i].getId());
			}
		}
		//nlapiLogExecution('audit','i_counter:- ',i_counter);
		i_month = sal_upload_date_month;
		i_year = sal_upload_date_year;
		
		//var d_start_date = get_current_month_start_date(i_month, i_year);
		//var d_end_date = get_current_month_end_date(i_month, i_year);
				
		//var a_allocate_array = get_emp_list_for_salary_upload(d_start_date, d_end_date)
		var d_start_date = get_current_month_start_date(i_month, i_year);
		var d_end_date = get_current_month_end_date(i_month, i_year);
		
		i_file_status = monthly_employee_file_process_data(i_context, i_counter, i_recordID, i_account_debit, i_account_credit, i_flag, i_month, i_year, vendor_provision_subsidiary, d_start_date, d_end_date);	
				
		var error_logs_count = 0;
		if (i_file_status == 'ERROR') //
		{
			var s_notes = 'Error occured while processing vendor provisioning data, please refer error logs for more information.';
			if (_logValidation(i_recordID)) //
			{
				var filters_error_log = new Array();
				filters_error_log[0] = new nlobjSearchFilter('custrecord_month_ven_pro', null, 'is', sal_upload_date_month.trim());
				filters_error_log[1] = new nlobjSearchFilter('custrecord_year_ven_pro', null, 'is', sal_upload_date_year.trim());
				filters_error_log[2] = new nlobjSearchFilter('custrecord_rate_type_error_log', null, 'is', 3);
				filters_error_log[3] = new nlobjSearchFilter('custrecord_subsidiary_ven_pro', null, 'is', vendor_provision_subsidiary);
				
				var i_search_error_logs = nlapiSearchRecord('customrecord_error_logs_vendor_pro', null, filters_error_log, null);
				if (_logValidation(i_search_error_logs))
				{
					error_logs_count = i_search_error_logs.length;
				}
				
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process_btpl', i_recordID);
				
				if (_logValidation(o_recordOBJ)) //
				{
					o_recordOBJ.setFieldValue('custrecord_status_btpl', 2)
					o_recordOBJ.setFieldValue('custrecord_notes_btpl', s_notes)
					o_recordOBJ.setFieldValue('custrecord_not_processed', error_logs_count);
					var total_contractor_count = o_recordOBJ.getFieldValue('custrecord_contractor_count');
					var total_processed_count = Number(total_contractor_count) - Number(error_logs_count);
					if (total_processed_count > 0)
					{
						o_recordOBJ.setFieldValue('custrecord_total_processed', total_processed_count);
					}
					else
					{
						o_recordOBJ.setFieldValue('custrecord_total_processed', 0);
					}
					//o_recordOBJ.setFieldValue('custrecord_csv_file_id_no', i_CSV_File_ID)
								
					var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
					nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
					
				}//Record OBJ				
			}//Record ID		
			// ================= Call Schedule Script ================
			
			/*var params = new Array();
			params['custscript_file_id_delete'] = i_CSV_File_ID
			params['custscript_record_id_delete'] = i_recordID
			params['custscript_hourly_employee_delete'] = i_hourly_employee
			params['custscript_salaried_employee_delete'] = i_monthly_employee
			
			var status = nlapiScheduleScript('customscript_sch_salary_upload_delete_no', null, params);
			//nlapiLogExecution('DEBUG', 'schedulerFunction', ' Status -->' + status);*/
		
		}//ERROR
		if (i_file_status == 'SUCCESS') //
		{
			if (_logValidation(i_recordID)) //
			{
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process_btpl', i_recordID)
				
				if (_logValidation(o_recordOBJ)) //
				{
					var s_notes = 'Vendor data has been processed & its correct .<br> Please click on Create JE for further Journal Entry creation .';
					
					o_recordOBJ.setFieldValue('custrecord_notes_btpl', s_notes);
					o_recordOBJ.setFieldValue('custrecord_status_btpl', 1);
					o_recordOBJ.setFieldValue('custrecord_not_processed', 0);
					var total_rcrd_count = o_recordOBJ.getFieldValue('custrecord_contractor_count');
					o_recordOBJ.setFieldValue('custrecord_total_processed', total_rcrd_count);
					var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
					//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
				}
			}
		}
		//------------------added by swati-------------------------------------
		/*if (i_file_status == 'In Progress') //
		{
			if (_logValidation(i_recordID)) //
			{
				var o_recordOBJ = nlapiLoadRecord('customrecord_salary_upload_process', i_recordID)
				
				if (_logValidation(o_recordOBJ)) //
				{
				
					o_recordOBJ.setFieldValue('custrecord_file_status', 3);
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ, true, true);
					//nlapiLogExecution('DEBUG', 'schedulerFunction',' ------------ Submit ID ----------->' + i_submitID);
				}
			}
		}*/
		//--------------------------------------------------------------------
	}
	catch (exception) //
	{
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);
	}
	
	nlapiLogExecution('DEBUG', 'schedulerFunction', '*** Execution Complete ***');
}

// END SCHEDULED FUNCTION ===============================================





// BEGIN FUNCTION ===================================================
/**
 * 
 * @param {Object} value
 * 
 * Description --> If the value is blank /null/undefined returns false else returns true
 */
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function decode_base64(s)
{
	var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
	var n = [[65, 91], [97, 123], [48, 58], [47, 48], [43, 44]];
	
	for (z in n) 
	{
	    for (i = n[z][0]; i < n[z][1]; i++) 
		{
	        v.push(w(i));
	    }
	}
	for (i = 0; i < 64; i++) 
	{
	    e[v[i]] = i;
	}
	
	for (i = 0; i < s.length; i += 72) 
	{
	    var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
	    for (x = 0; x < o.length; x++) 
		{
	        c = e[o.charAt(x)];
	        b = (b << 6) + c;
	        l += 6;
	        while (l >= 8) 
			{
	            r += w((b >>> (l -= 8)) % 256);
	        }
	    }
	}
	return r;
}

//-------------------------------------------------------------------------------------------------------------------------

function search_employee(i_employeeID) //
{
	var i_employee_entity_ID = '';
	var i_subsidiary = '';
	var i_department = '';
	var i_location = '';
	var i_location_txt = '';
	var i_currency = '';
	var i_subsidiary_txt = '';
	var i_recordID = '';
	var i_employee_type = '';
	
	var a_return_array = new Array();
	
	if (_logValidation(i_employeeID)) //
	{
		i_employeeID = i_employeeID.trim();
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('entityid', null, 'is', i_employeeID);
		filter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('entityid');
		columns[1] = new nlobjSearchColumn('subsidiary');
		columns[2] = new nlobjSearchColumn('department');
		columns[3] = new nlobjSearchColumn('location');
		columns[4] = new nlobjSearchColumn('internalid');
		columns[5] = new nlobjSearchColumn('employeetype');
		
		var a_search_results = nlapiSearchRecord('employee', null, filter, columns);
		
		if (a_search_results != null && a_search_results != '' && a_search_results != undefined) //
		{
			i_employee_entity_ID = a_search_results[0].getValue('entityid');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee ID -->' + i_employee_entity_ID);
			
			i_recordID = a_search_results[0].getText('internalid');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Record ID-->' + i_recordID);
			
			i_subsidiary_txt = a_search_results[0].getText('subsidiary');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Subsidiary Txt-->' + i_subsidiary_txt);
			
			i_subsidiary = a_search_results[0].getValue('subsidiary');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Subsidiary -->' + i_subsidiary);
			
			i_department = a_search_results[0].getValue('department');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Department -->' + i_department);
			
			i_location = a_search_results[0].getValue('location');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Location -->' + i_location);
			
			i_employee_type = a_search_results[0].getValue('employeetype');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Type -->' + i_employee_type);
			
			i_location_txt = a_search_results[0].getText('location');
			//nlapiLogExecution('DEBUG', 'search_employee', ' Employee Location Txt-->' + i_location_txt);
		
		}//Search Results		
	}//Employee ID	
	a_return_array[0] = i_employee_entity_ID + '&&##&&' + i_subsidiary + '&&##&&' + i_department + '&&##&&' + i_location + '&&##&&' + i_location_txt + '&&##&&' + i_subsidiary_txt + '&&##&&' + i_recordID + '&&##&&' + i_employee_type
	
	return a_return_array;
}//Search Employee

function check_for_numeric_value(i_number) //
{
	var result = '';
	if (parseFloat(i_number) >= 0) //
	{
		result = isNaN(i_number);
		//nlapiLogExecution('DEBUG', 'check_no_of_days', ' No Of Days Number / Text -->' + result);
		//nlapiLogExecution('DEBUG', 'check_no_of_days', ' No Of Days Number / Number -->' + i_number);
		
		if (result == false) // False means it is a valid number 
		{
			//nlapiLogExecution('DEBUG', 'check_no_of_days', '*** Number is valid ***');
			
			if (parseFloat(i_number) <= 0) //
			{
				//nlapiLogExecution('DEBUG', 'check_no_of_days', '*** Number is less than 0 ***');
				result = true;
			}
		}
		
	}//Number Validation 
	//nlapiLogExecution('DEBUG', 'check_no_of_days',' No Of Days Number / result -->' + result);		
	return result;
}// Check if a number is a numeric or it contains text

function get_subsidiary_text(i_subsidiary)
{
  var s_subsidiary = '';	
 if(_logValidation(i_subsidiary))
 {
 	var o_subsidiaryOBJ = nlapiLoadRecord('subsidiary',i_subsidiary);
	
	if(_logValidation(o_subsidiaryOBJ))
	{
		s_subsidiary = o_subsidiaryOBJ.getFieldValue('name');
		//nlapiLogExecution('DEBUG', 'get_subsidiary_text',' Subsidiary -->' + s_subsidiary);	
		
	}//Subsidiary OBJ
 }//Subsidiary	
 return s_subsidiary;
}//Get Subsidiary

function get_Month(i_month) //
{
	if (i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY') {
		return '01';
	}
	else 
		if (i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY') {
			return '02';
		}
		else 
			if (i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH') {
				return '03';
			}
			else 
				if (i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL') {
					return '04';
				}
				else 
					if (i_month == 'May' || i_month == 'MAY') {
						return '05';
					}
					else 
						if (i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE') {
							return '06';
						}
						else 
							if (i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY') {
								return '07';
							}
							else 
								if (i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST') {
									return '08';
								}
								else 
									if (i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER') {
										return '09';
									}
									else 
										if (i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER') {
											return '10';
										}
										else 
											if (i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER') {
												return '11';
											}
											else 
												if (i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER') {
													return '12';
												}
}

function check_month_year_format(i_month_year)
{
	var is_month = false;
	var s_error_1 = '';
	var s_error_2 = '';
	var s_error = '';
	if(_logValidation(i_month_year))
	{
	  var a_split_m_y_array = new Array();
	  
	  if(i_month_year.indexOf('-')>-1)
	  {
		  a_split_m_y_array = i_month_year.split('-');
		  
		  var i_month = a_split_m_y_array[0];
		  
		  var i_year = a_split_m_y_array[1];
		  
		  var i_year_format = check_for_numeric_value(i_year);
		  //nlapiLogExecution('DEBUG', 'check_month_year_format',' Year Format -->' + i_year_format);			
			
		  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
		  {
		  	is_month = true ;
		  }	
		  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
		  {
		  	is_month = true ;		
		  }		
		  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'May' || i_month == 'MAY')
		  {
		  	is_month = true ;		
		  }	  
		  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
		  {
		 	is_month = true ;		
		  }	
		  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
		  {
		  	is_month = true ;		
		  }  
		  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
		  {
		 	is_month = true ;		
		  }	
		  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
		  {
		  	is_month = true ;		
		  }	
		  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
		  {
		  	is_month = true ;		
		  }	
					
			if(is_month == false)
			{
				s_error_1 = 'Invalid Year : Year is not in number format . \n' 			
			}
			if(i_year_format == true)
			{
				s_error_2 = ' Invalid Month : Month is not in MM format .\n'
			}			
		    s_error = s_error_1+''+s_error_2;		  	
	  }// Contains -
	  else
	  {	  	 
	   s_error+=' Invalid Date Format : Date is not in MM-YYYY Format . \n';
	   //nlapiLogExecution('DEBUG', 'check_month_year_format',' Error -->' + s_error);	
	  }		
	}//Month & Year	
	return s_error;
}//Month Year Format


function check_currency(i_currency) //
{
	var i_currencyID = '';
	if(_logValidation(i_currency)) //
	{
		var filters = new Array();
	    filters[0] = new nlobjSearchFilter('name', null, 'is',i_currency)
	    
	    var column= new Array();	
	    column[0]= new nlobjSearchColumn('internalid')
	 
	    var a_results = nlapiSearchRecord('currency',null,filters,column);
		
		if(_logValidation(a_results)) //
		{		
		i_currencyID = a_results[0].getValue('internalid');
		//nlapiLogExecution('DEBUG', 'check_currency', ' Currency ID -->' + i_currencyID);	
			
		}//		
	}//Currency V	
	return i_currencyID;
}//Currency

function monthly_employee_file_process_data(i_context, i_counter, i_recordID, i_account_debit, i_account_credit, i_flag, i_month, i_year, vendor_provision_subsidiary, d_start_date, d_end_date)
{
	var i_mnt = 0;
	var i_ent = 0;
	var i_file_status = 'SUCCESS';
	var a_currency_array = new Array();
	var i = i_counter;
	var employee_data_validated_flag = 0;
	
	d_start_date = nlapiStringToDate(d_start_date);
	d_end_date = nlapiStringToDate(d_end_date);
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('startdate',null,'onorbefore',d_end_date);
	filters[1] = new nlobjSearchFilter('enddate',null,'onorafter',d_start_date); 
	filters[2] = new nlobjSearchFilter('subsidiary','employee','is',parseInt(vendor_provision_subsidiary));
	
	var a_results = nlapiSearchRecord(null, 'customsearch_emplist_for_sal_upload_btpl', filters, null);
	if (_logValidation(a_results)) //
	{
		nlapiLogExecution('audit','emp process length:-- ',a_results.length);
		if(i_counter == a_results.length)
		{
			i_file_status = 'SUCCESS';
			return i_file_status;
		}
		nlapiSubmitField('customrecord_salary_upload_process_btpl',i_recordID,'custrecord_contractor_count',a_results.length);
		for (; i < a_results.length; i++) //
		{
			var s_error = '';
			var s_error_j = '';
			var s_error_d = '';
			var s_error_a = '';
			var s_error_allocate = '';
			var error_amount = '';
			var s_error_3 = '';
			var s_error_6 = '';
			var s_error = '';
			var result_C = a_results[0];
			var columns = result_C.getAllColumns();
			var columnLen = columns.length;
			
			for (var j = 0; j < columnLen; j++)
			{
				var column = columns[j];
				var label = column.getLabel();
					
				if (label == 'Employee Id')
				{
					var Emp_id = a_results[i].getValue(column);
				}
				else if (label == 'Employee')
				{
					var Emp_name = a_results[i].getText(column);
				}
				else if (label == 'Emp Location')
				{
					var Emp_location = a_results[i].getText(column);
				}
				else if (label == 'Emp Currency')
				{
					var Emp_currency = a_results[i].getText(column);
				}
				else if (label == 'Emp Monthly Rate')
				{
					var Emp_monthly_rate = a_results[i].getValue(column);
				}
				else if (label == 'Project')
				{
					var Emp_Project = a_results[i].getText(column);
					var proj_id = a_results[i].getValue(column);
					var proj_vertical = nlapiLookupField('job',proj_id,'custentity_vertical');
					if(_logValidation(proj_vertical))
						proj_vertical = '';
				}
				else if (label == 'Hours')
				{
					var Emp_proj_hours = a_results[i].getValue(column);
					Emp_proj_hours = ((daysInMonth(i_month,i_year)) * parseInt(8)).toFixed(0);
				}
				else if (label == 'Percent of Time')
				{
					var Emp_proj_percent_hours = a_results[i].getValue(column);
				}
				else if (label == 'Start Date')
				{
					var Emp_resource_strt_date = a_results[i].getValue(column);
				}
				else if (label == 'End Date')
				{
					var Emp_resource_end_date = a_results[i].getValue(column);
				}
				else //Project id
				{
					var Emp_proj_id = a_results[i].getText(column);
				}
			}
			
			var previous_month = get_previous_month(i_month);
			previous_month = previous_month.toString();
			if(previous_month == 'December')
			{
				i_year_temp = Number(i_year) - Number(1);
			}
			else
			{
				i_year_temp = i_year;
			}	
			nlapiLogExecution('debug','month previous:- ',previous_month);
			var temp_strt_date = get_previous_month_start_date(previous_month,i_year_temp);
			var temp_end_date = get_previous_month_end_date(previous_month,i_year_temp);
			
			temp_strt_date = nlapiStringToDate(temp_strt_date);
			temp_end_date = nlapiStringToDate(temp_end_date);
			//nlapiLogExecution('audit','last mnth temp_strt_date:- ',temp_strt_date);
			//nlapiLogExecution('audit','last mnth temp_end_date:- ',temp_end_date);
			
			var temp_last_mnth_duration = 0;
			b_provisonCreated = 'F';
			var filters_previous_mnth_vendor_bill = new Array();
			var columns__previous_mnth_vendor_bill = new Array();
			filters_previous_mnth_vendor_bill[0] = new nlobjSearchFilter('date','custrecord_time_entry_id','within',temp_strt_date,temp_end_date);
			filters_previous_mnth_vendor_bill[1] = new nlobjSearchFilter('employee', 'custrecord_time_entry_id','is', parseInt(Emp_id));
			
			columns__previous_mnth_vendor_bill[0] = new nlobjSearchColumn('custrecord_provision_created');
			
			var previous_mnth_vendor_bill = nlapiSearchRecord('customrecord_ts_extra_attributes', null, filters_previous_mnth_vendor_bill, columns__previous_mnth_vendor_bill);
			
				if(_logValidation(previous_mnth_vendor_bill))
				{
					b_provisonCreated = previous_mnth_vendor_bill[0].getValue('custrecord_provision_created');
				}
				//if(b_provisonCreated=='F')
				{
					var temp_get_sat_sun_strt_date = get_current_month_start_date(previous_month, i_year_temp);
					var temp_get_sat_sun_end_date = get_current_month_end_date(previous_month, i_year_temp);
						
					var filters_search_allocation = new Array();
					filters_search_allocation[0] = new nlobjSearchFilter('resource', null, 'anyof', parseInt(Emp_id));
					filters_search_allocation[1] = new nlobjSearchFilter('startdate', null, 'onorbefore', temp_get_sat_sun_end_date);
					filters_search_allocation[2] = new nlobjSearchFilter('enddate', null, 'onorafter', temp_get_sat_sun_strt_date);
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('resource');
					columns[1] = new nlobjSearchColumn('employeetype', 'employee');
					columns[2] = new nlobjSearchColumn('custentity_persontype', 'employee');
					columns[3] = new nlobjSearchColumn('subsidiary', 'employee');
					
					var project_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
					if(_logValidation(project_allocation_result))
					{
						//nlapiLogExecution('Debug','Allocation Found');
						var timsheet_full_day = new Array();
						var timesheet_half_day = new Array();
						
						var filters_previous_mnth_data = new Array();
						filters_previous_mnth_data[0] = new nlobjSearchFilter('employee', null, 'is', parseInt(Emp_id));
						filters_previous_mnth_data[1] = new nlobjSearchFilter('date', null, 'within', temp_strt_date, temp_end_date);
						filters_previous_mnth_data[2] = new nlobjSearchFilter('item', null, 'noneof', [2480, 2479]); // Leave and Holidays	
						//filters_previous_mnth_data[3] = new nlobjSearchFilter('custrecord_provision_created_new', null, 'is', 'F');
					
						var column_previous_mnth_data = new Array();
						column_previous_mnth_data[0] = new nlobjSearchColumn('durationdecimal');
						column_previous_mnth_data[1] = new nlobjSearchColumn('internalid', 'timesheet');
						column_previous_mnth_data[2] = new nlobjSearchColumn('date');
						
						var previous_mnth_data = nlapiSearchRecord(null, 'customsearch_vend_pro_last_m', filters_previous_mnth_data, column_previous_mnth_data);//customsearch_vend_pro_last_mnth_data
						if (_logValidation(previous_mnth_data)) 
						{
							for (var z = 0; z < previous_mnth_data.length; z++) 
							{
								var internalID = previous_mnth_data[z].getId();
								var time_sheet_entry_id = previous_mnth_data[z].getValue(column_previous_mnth_data[1]);
								
								var customrecord_ts_extra_attributesSearch = nlapiSearchRecord("customrecord_ts_extra_attributes",null,
								[
								   ["custrecord_time_entry_id","anyof",internalID]
								], 
								[
								   new nlobjSearchColumn("custrecord_provision_created")
								]
								);
								var isProCreated = '';
								if(customrecord_ts_extra_attributesSearch)
								{
									isProCreated = customrecord_ts_extra_attributesSearch[0].getValue('custrecord_provision_created');
								}
								
								if (_logValidation(time_sheet_entry_id)) 
								{
									
									if(customrecord_ts_extra_attributesSearch)
									{
										if(isProCreated=='T')
										{
										//nlapiLogExecution('Audit','Break Statment', isProCreated);
										continue;
										}
										//nlapiLogExecution('Audit','Next to Break  Statment', isProCreated);
									}
									var last_mnth_duration = previous_mnth_data[z].getValue('durationdecimal');
									temp_last_mnth_duration = parseFloat(last_mnth_duration) + parseFloat(temp_last_mnth_duration);
									//nlapiLogExecution('audit','Allocation Found temp_last_mnth_duration in loop ',temp_last_mnth_duration);
								}
							}
							//nlapiLogExecution('audit','Allocation Found temp_last_mnth_duration',temp_last_mnth_duration);
							var lst_mnth_data_rcrd = nlapiCreateRecord('customrecord_vendor_pro_lst_mnth_data');
							lst_mnth_data_rcrd.setFieldValue('custrecord_emp_name_lst_mnth', parseInt(Emp_id));
							lst_mnth_data_rcrd.setFieldValue('custrecord_tot_hours_nt_blld', temp_last_mnth_duration);
							lst_mnth_data_rcrd.setFieldValue('custrecord_current_month', i_month.toString());
							lst_mnth_data_rcrd.setFieldValue('custrecord_current_year', i_year.toString());
							lst_mnth_data_rcrd.setFieldValue('custrecord_rate_type_lst_mnth', 3);
							lst_mnth_data_rcrd.setFieldValue('custrecord_subsidiary_processed', vendor_provision_subsidiary);
							nlapiSubmitRecord(lst_mnth_data_rcrd);
						}
					}
				}
			
			var filters_emp_already_processed = new Array();
			filters_emp_already_processed[0] = new nlobjSearchFilter('custrecord_month_sal_up_ven',null,'is',i_month.toString());
			filters_emp_already_processed[1] = new nlobjSearchFilter('custrecord_year_sal_up_ven',null,'is',i_year.toString());
			filters_emp_already_processed[2] = new nlobjSearchFilter('custrecord_employee_rec_id_sal_up_ven', null,'is',Emp_id);
			filters_emp_already_processed[3] = new nlobjSearchFilter('custrecord_project_assigned', null,'is',Emp_Project.toString());
			filters_emp_already_processed[4] = new nlobjSearchFilter('custrecord_rate_type_sal_upload', null,'is',3);
			filters_emp_already_processed[5] = new nlobjSearchFilter('custrecord_subsidiary_sal_up_ven', null,'is',parseInt(vendor_provision_subsidiary));
			
			var emp_already_processed = nlapiSearchRecord('customrecord_salary_upload_file', null, filters_emp_already_processed, null);
			
			if (_logValidation(emp_already_processed))
			{
				
			}
			else
			{
			
				var d_start_date = get_current_month_start_date(i_month, i_year)
				var d_end_date = get_current_month_end_date(i_month, i_year)
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Start Date..... ' + d_start_date);
				
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....End Date..... ' + d_end_date); //employee_array_credit
				if (_logValidation(d_start_date)) //
				{
					d_start_date = nlapiStringToDate(d_start_date);
					d_start_date = nlapiDateToString(d_start_date);
				}
				if (_logValidation(d_end_date)) //
				{
					d_end_date = nlapiStringToDate(d_end_date);
					d_end_date = nlapiDateToString(d_end_date);
				}
				//======================================================================================
				
				// Check for employee over and under allocation
				// i_Salaried_OT, i_Salaried_OT_Diff, i_Hourly_Diff
				
				//nlapiLogExecution('DEBUG', 'check_emp_allocation', '*** Checking for resource validatoin ***');
				
				var check_emp_allocation = check_emp_over_or_under_allocated(Emp_id, d_start_date, d_end_date)
				//nlapiLogExecution('DEBUG', 'check_emp_allocation', check_emp_allocation);
				//=======================================================================================				
				
				var a_allocate_array = get_allocation_details(Emp_id, d_start_date, d_end_date)
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Allocate Array..... ' + a_allocate_array);
				
				var i_percent_time_total = 0;
				
				if (_logValidation(a_allocate_array)) //
				{
					var a_split_allocate_array = new Array();
					
					for (var a = 0; a < a_allocate_array.length; a++) //
					{
						if (_logValidation(a_allocate_array[a])) //
						{
							a_split_allocate_array = a_allocate_array[a].split('^^&&^^');
							
							var i_percent_time = a_split_allocate_array[0];
							
							var i_project = a_split_allocate_array[1];
							
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time..... ' + i_percent_time);
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Project..... ' + i_project);
							
							i_percent_time = parseInt(i_percent_time); //100.0% 
							//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA..... ' + i_percent_time);
							
							i_percent_time_total = parseInt(i_percent_time_total) + parseInt(i_percent_time)
						//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA Total ..... ' + i_percent_time_total);
						
						}//a_allocate_array[a]
					}//A		
				}//a_allocate_array		
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Percent Time AA Total ..... ' + i_percent_time_total);
				
				if (i_percent_time_total >= 100) //
				{
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....100 % Allocated .... ' + i_percent_time_total);
				}
				else //
				{
					//nlapiLogExecution('DEBUG', ' monthly_JE_creation', ' ....Not 100% Allocated ..... ' + i_percent_time_total);
					s_error_allocate = ' Employee not 100% Allocated  : Employee is not 100% allocated for the project.\n'
					s_error_allocate_fld = ' Employee 100 % Allocation'
				//     break;
				}
				//================================================================================================
				if (check_emp_allocation == 1) //
				{
					s_error_a = 'Employee Over Allocated  : Employee is Over allocated for the project.\n'
					s_error_a_fld = 'Employee Over Allocation'
				}
				else 
					if (check_emp_allocation == 2) //
					{
						s_error_a = 'Employee Under Allocated  : Employee is Under allocated for the project.\n'
						s_error_a_fld = 'Employee Under Allocation'
					}
				//===============================================================================================
				// End of  Check for employee over and under allocation
				
				
				var i_practice = nlapiLookupField('employee',Emp_id,'department');
				if (!_logValidation(i_practice)) //
				{
					s_error_3 = ' Practice not present : Employee does not have Practice .\n'
					s_error_3_fld = 'Employee Practice'
				}
				
				var i_location_e = nlapiLookupField('employee',Emp_id,'location');
				if (!_logValidation(i_location_e)) //
				{
					s_error_6 = ' Location not present  : Location is not present on Employee.\n'
					s_error_6_fld = 'Employee Location\n'
				}
				
				
				var break_up_resource_alloaction_flag = 0;
				var filters_subTier_info = new Array();
				filters_subTier_info[0] = new nlobjSearchFilter('custrecord_stvd_start_date',null,'onorbefore',d_end_date);
				filters_subTier_info[1] = new nlobjSearchFilter('custrecord_stvd_end_date',null,'onorafter',d_start_date);
				filters_subTier_info[2] = new nlobjSearchFilter('custrecord_stvd_contractor',null,'is',parseInt(Emp_id));
				filters_subTier_info[3] = new nlobjSearchFilter('custrecord_stvd_vendor_type',null,'is',1);
				filters_subTier_info[4] = new nlobjSearchFilter('custrecord_stvd_rate_type',null,'is',1);
				
				var column_subTier_info = new Array();
				column_subTier_info[0] = new nlobjSearchColumn('custrecord_stvd_start_date');
				column_subTier_info[1] = new nlobjSearchColumn('custrecord_stvd_end_date');
				column_subTier_info[2] = new nlobjSearchColumn('custrecord_stvd_st_pay_rate');
				var subTier_info_results = nlapiSearchRecord('customrecord_subtier_vendor_data', null, filters_subTier_info, column_subTier_info);
				
				if(_logValidation(subTier_info_results))
				{
					if(subTier_info_results.length >= 2)
					{
						break_up_resource_alloaction_flag = 1;
						for(var t=0; t<subTier_info_results.length; t++)
						{
							var st_rate_for_emp = subTier_info_results[t].getValue('custrecord_stvd_st_pay_rate');
							var sub_tier_strt_date = subTier_info_results[t].getValue('custrecord_stvd_start_date');
							var sub_tier_end_date = subTier_info_results[t].getValue('custrecord_stvd_end_date');	
							
							Emp_monthly_rate = st_rate_for_emp;
							
							if(new Date(sub_tier_strt_date) > new Date(d_start_date))
							{
								var temp_strt_date_sub_tier = sub_tier_strt_date;
							}
							else
							{
								var temp_strt_date_sub_tier = d_start_date;
							}
							
							if(new Date(d_end_date) > new Date(sub_tier_end_date))
							{
								var temp_end_date_sub_tier = sub_tier_end_date;
							}	
							else
							{
								var temp_end_date_sub_tier = d_end_date;
							}
							
							s_error = s_error_3 + '' + s_error_6 + '' + s_error_j + '' + s_error_d + '' + s_error_a + '' + s_error_allocate;
							//nlapiLogExecution('audit', 'inside subtier infor 2 entries:- ',s_error);
							
							// ===================== Create Salary Upload - Monthly Records ===================
					
							if (!_logValidation(s_error)) //
							{
								i_file_status = create_vendor_pro_sal_upload_rcrd(i_month, i_year, Emp_id, vendor_provision_subsidiary, Emp_location, Emp_currency, i_practice, Emp_monthly_rate, Emp_Project, Emp_proj_hours, Emp_proj_percent_hours, i_recordID, Emp_name, temp_strt_date_sub_tier, temp_end_date_sub_tier,proj_id,proj_vertical);
							}
						}
					}
					else
					{
						var st_rate_for_emp = subTier_info_results[0].getValue('custrecord_stvd_st_pay_rate');
						var sub_tier_strt_date = subTier_info_results[0].getValue('custrecord_stvd_start_date');
						var sub_tier_end_date = subTier_info_results[0].getValue('custrecord_stvd_end_date');
							
						Emp_monthly_rate = st_rate_for_emp;
						
						if(new Date(sub_tier_strt_date) > new Date(d_start_date))
						{
							var temp_strt_date_sub_tier = sub_tier_strt_date;
						}
						else
						{
							var temp_strt_date_sub_tier = d_start_date;
						}
						
						if(new Date(d_end_date) > new Date(sub_tier_end_date))
						{
							var temp_end_date_sub_tier = sub_tier_end_date;
						}	
						else
						{
							var temp_end_date_sub_tier = d_end_date;
						}
						
						s_error = s_error_3 + '' + s_error_6 + '' + s_error_j + '' + s_error_d + '' + s_error_a + '' + s_error_allocate;
						//nlapiLogExecution('audit', 'inside subtier infor:- ',s_error);	
							
						// ===================== Create Salary Upload - Monthly Records ===================
					
						if (!_logValidation(s_error)) //
						{
							i_file_status = create_vendor_pro_sal_upload_rcrd(i_month,i_year,Emp_id,vendor_provision_subsidiary,Emp_location,Emp_currency,i_practice,Emp_monthly_rate,Emp_Project,Emp_proj_hours,Emp_proj_percent_hours,i_recordID,Emp_name,temp_strt_date_sub_tier,temp_end_date_sub_tier,proj_id,proj_vertical);
						}//Non - Error Records	
					}
						
				}
				else
				{
					var filters_is_subTier_present = new Array();
					filters_is_subTier_present[0] = new nlobjSearchFilter('custrecord_stvd_start_date',null,'onorbefore',d_end_date);
					filters_is_subTier_present[1] = new nlobjSearchFilter('custrecord_stvd_end_date',null,'onorafter',d_start_date);
					filters_is_subTier_present[2] = new nlobjSearchFilter('custrecord_stvd_contractor',null,'is',parseInt(Emp_id));
					filters_is_subTier_present[3] = new nlobjSearchFilter('custrecord_stvd_vendor_type',null,'is',1);
				
					var is_subTier_info_present = nlapiSearchRecord('customrecord_subtier_vendor_data', null, filters_is_subTier_present, null);
					if (_logValidation(is_subTier_info_present))
					{
					
					}
					else
					{
						//nlapiLogExecution('audit','inside else for monthly rate',Emp_name);
						error_amount = "Hourly rate : Hourly rate for emp not defined";
						s_error = s_error_3 + '' + s_error_6 + '' + s_error_j + '' + s_error_d + '' + s_error_a + '' + s_error_allocate + '' + error_amount;
						//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Error -->' + s_error);
					}
				}
				
				/*if (Emp_monthly_rate == 0) {
				error_amount = "Monthly rate : Monthly rate for emp not defined";
				}*/
				
				if (_logValidation(s_error)) //
				{
					employee_data_validated_flag = 0;
					
					//nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* 2 Monthly Error Block ************************' + i_mnt);
					
					i_ent++;
					var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_vendor_pro');
					
					o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details', s_error);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_ven_pro', Emp_name);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_ven_pro', vendor_provision_subsidiary);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_month_ven_pro', i_month);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_year_ven_pro', i_year);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_vendor_pro_parent', i_recordID);
					o_salary_pr_errorOBJ.setFieldValue('custrecord_rate_type_error_log', 3);
					
					var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
					nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' Salary Upload - Error Logs Submit ID -->' + i_submit_err_ID);
					
					if (_logValidation(i_submit_err_ID)) //
					{
						//nlapiLogExecution('DEBUG', '_logValidation(s_error) i_file_status ' + i_file_status);
						i_file_status = 'ERROR'
						i_flag = 1;
					}
					
				}//Error Records
				if (i_flag == 1) //
				{
					i_file_status = 'ERROR'
				}
			
				var i_usage_end = i_context.getRemainingUsage();
				//nlapiLogExecution('DEBUG', 'schedulerFunction', 'Usage At End [' + i + '] -->' + i_usage_end);
				
				if (i_usage_end < 1000) //
				{
					i_file_status = 'In Progress';
					var ab = 0;
					nlapiYieldScript();
					//schedule_script_after_usage_exceeded(i, i_month, i_year, i_account_debit, i_account_credit, i_recordID, vendor_provision_subsidiary, i_flag);
				//break;
				}
			}
			
		}// to check employee data validation
	}
	return i_file_status;
}//monthly_employee_file_process_data

function create_vendor_pro_sal_upload_rcrd(i_month,i_year,Emp_id,vendor_provision_subsidiary,Emp_location,Emp_currency,i_practice,Emp_monthly_rate,Emp_Project,Emp_proj_hours,Emp_proj_percent_hours,i_recordID,Emp_name,temp_strt_date_sub_tier,temp_end_date_sub_tier,proj_id,proj_vertical)
{
	try
	{
		var i_file_status = 'SUCCESS';
		
		d_start_date = nlapiStringToDate(temp_strt_date_sub_tier);
		d_end_date = nlapiStringToDate(temp_end_date_sub_tier);
		
		try
		{
			var leave_duration_days = 0;
			var temp_leave_duration = 0;
			var leave_less_half_day = new Array();
			var leave_half_day = new Array();
			var leave_full_day = new Array();
			var filters_total_leaves = new Array();
			filters_total_leaves[0] = new nlobjSearchFilter('employee',null,'is',parseInt(Emp_id));
			filters_total_leaves[1] = new nlobjSearchFilter('internalid','job','is',proj_id);
			//filters_total_leaves[2] = new nlobjSearchFilter('approvalstatus',null,'is','Pending Approval');
			//filters_total_leaves[2] = new nlobjSearchFilter('title','projecttask','is','Leave');
			filters_total_leaves[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
			
			var column_leaves = new Array();
			column_leaves[0] = new nlobjSearchColumn('durationdecimal');
			column_leaves[1] = new nlobjSearchColumn('internalid','timesheet');
			column_leaves[2] = new nlobjSearchColumn('date');
			var not_approved_timeentry_leaves = nlapiSearchRecord('timebill', 'customsearch_not_appr_entry__2', filters_total_leaves, column_leaves);
			if (_logValidation(not_approved_timeentry_leaves))
			{
				var leave_duration = 0;
				for (var z = 0; z < not_approved_timeentry_leaves.length; z++) //
				{
					var time_sheet_entry_id = not_approved_timeentry_leaves[z].getValue(column_leaves[1]);
					if (_logValidation(time_sheet_entry_id))
					{
						leave_duration = not_approved_timeentry_leaves[z].getValue('durationdecimal');
						var leave_date = not_approved_timeentry_leaves[z].getValue('date');
						leave_date = leave_date.toString();
						if(leave_duration == 0)
						{
							leave_duration = parseFloat(8);
							leave_full_day.push(leave_date);
						}
						else if(leave_duration <= 4)
						{
							leave_duration = parseFloat(4);
							leave_half_day.push(leave_date);
						}
						else
						{
							leave_duration = parseFloat(8);
							leave_full_day.push(leave_date);
						}
					}
					temp_leave_duration = parseFloat(leave_duration) + parseFloat(temp_leave_duration);
				}
				leave_duration_days = parseFloat(temp_leave_duration);// / parseFloat(8);
			}
		}
		catch(e)
		{
			nlapiLogExecution('error','error message leaves taken loop:-- ',e);
		}
		
		try
		{
			var holidays = 0;
			var temp_holidays_duration = 0;
			var filters_total_leaves = new Array();
			filters_total_leaves[0] = new nlobjSearchFilter('employee',null,'is',parseInt(Emp_id));
			filters_total_leaves[1] = new nlobjSearchFilter('internalid','job','is',proj_id);
			filters_total_leaves[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
			
			var column_leaves = new Array();
			column_leaves[0] = new nlobjSearchColumn('durationdecimal');
			column_leaves[1] = new nlobjSearchColumn('internalid','timesheet');
			var not_approved_timeentry_holidays = nlapiSearchRecord('timebill', 'customsearch_not_appr_entry_', filters_total_leaves, column_leaves);
			if (_logValidation(not_approved_timeentry_holidays))
			{
				var holiday_duration = 0;
				for (var z = 0; z < not_approved_timeentry_holidays.length; z++) //
				{
					var time_sheet_entry_id = not_approved_timeentry_holidays[z].getValue(column_leaves[1]);
					if (_logValidation(time_sheet_entry_id))
					{
						holiday_duration = not_approved_timeentry_holidays[z].getValue('durationdecimal');
						if(holiday_duration >= 0)
							holiday_duration = parseFloat(8);
					}
					temp_holidays_duration = parseFloat(holiday_duration) + parseFloat(temp_holidays_duration);
				}
				holidays = parseFloat(temp_holidays_duration);// / parseFloat(8);
			}
		}
		catch(e)
		{
			nlapiLogExecution('error','error message holidays loop:-- ',e);
		}
		try
		{
			var not_approved_days = 0;
			var temp_not_approved_duration = 0;
			var filters_total_leaves = new Array();
			filters_total_leaves[0] = new nlobjSearchFilter('employee',null,'is',parseInt(Emp_id));
			filters_total_leaves[1] = new nlobjSearchFilter('internalid','job','is',proj_id);
			filters_total_leaves[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
			
			var column_leaves = new Array();
			column_leaves[0] = new nlobjSearchColumn('durationdecimal');
			column_leaves[1] = new nlobjSearchColumn('internalid','timesheet');
			column_leaves[2] = new nlobjSearchColumn('date');
			var not_approved_timeentry = nlapiSearchRecord('timebill', 'customsearchcustomsearch_not_appr_vendor', filters_total_leaves, column_leaves);
			if (_logValidation(not_approved_timeentry))
			{
				var not_approved_duration = 0;
				for (var z = 0; z < not_approved_timeentry.length; z++) //
				{
					var time_sheet_entry_id = not_approved_timeentry[z].getValue(column_leaves[1]);
					if (_logValidation(time_sheet_entry_id))
					{
						not_approved_duration = not_approved_timeentry[z].getValue('durationdecimal');
					}
					temp_not_approved_duration = parseFloat(not_approved_duration) + parseFloat(temp_not_approved_duration);
				}
				not_approved_days = parseFloat(temp_not_approved_duration);
			}
		}
		catch(e)
		{
			nlapiLogExecution('error','error message not approved entry:-- ',e);
		}

		try
		{
			var approved_days = 0;
			var temp_approved_duration = 0.0;
			var filters_total_leaves = new Array();
			filters_total_leaves[0] = new nlobjSearchFilter('employee',null,'is',parseInt(Emp_id));
			filters_total_leaves[1] = new nlobjSearchFilter('internalid','job','is',proj_id);
			filters_total_leaves[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
			
			var column_leaves = new Array();
			column_leaves[0] = new nlobjSearchColumn('durationdecimal');
			column_leaves[1] = new nlobjSearchColumn('internalid','timesheet');
			column_leaves[2] = new nlobjSearchColumn('date');
			var approved_timeentry = nlapiSearchRecord('timebill', 'customsearch_appr_vendor_pro', filters_total_leaves, column_leaves);
			if (_logValidation(approved_timeentry))
			{
				var approved_duration = 0;
				for (var z = 0; z < approved_timeentry.length; z++) //
				{
					var time_sheet_entry_id = approved_timeentry[z].getValue(column_leaves[1]);
					if (_logValidation(time_sheet_entry_id))
					{
						approved_duration = approved_timeentry[z].getValue('durationdecimal');	
					}
					temp_approved_duration = parseFloat(approved_duration) + parseFloat(temp_approved_duration);
				}
				approved_days = parseFloat(temp_approved_duration);
			}
		}
		catch(e)
		{
			nlapiLogExecution('error','error message approved entry:-- ',e);
		}
		
		try
		{
		/*	var not_submitted_days = 0;
			var temp_not_submitted_duration = 0.0;
			var filters_total_leaves = new Array();
			filters_total_leaves[0] = new nlobjSearchFilter('employee',null,'is',parseInt(Emp_id));
			filters_total_leaves[1] = new nlobjSearchFilter('internalid','job','is',proj_id);
			filters_total_leaves[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
			
			var not_submitted_timeentry = nlapiSearchRecord('timebill', 'customsearch_notsubmitted_ti', filters_total_leaves, null);
			if (_logValidation(not_submitted_timeentry))
			{
				var not_submitted_duration = 0;
				for (var z = 0; z < not_submitted_timeentry.length; z++) //
				{
					var result_C = not_submitted_timeentry[0];
					var columns = result_C.getAllColumns();
					var columnLen = columns.length;
					
					for (var j = 0; j < columnLen; j++)
					{
						var column = columns[j];
						var label = column.getLabel();
						
						/*if (label == 'Not Submitted') {
							not_submitted_duration = not_submitted_timeentry[z].getValue(column);
							nlapiLogExecution('debug', 'not_submitted_duration:-', not_submitted_duration);
						}*/
					/*	if(label == 'Allocated')
						{
							var allocated_tym = not_submitted_timeentry[z].getValue(column);
						}
					}
				}
				
				var temp_allocated_days = parseFloat(allocated_tym);
				not_submitted_days = parseFloat(approved_days) + parseFloat(not_approved_days);
				if(parseFloat(not_submitted_days) <= parseFloat(temp_allocated_days))
				{
					not_submitted_days = parseFloat(temp_allocated_days) - parseFloat(not_submitted_days);
					not_submitted_days = not_submitted_days - (parseFloat(leave_duration_days) + parseFloat(holidays));
				}
				else
					not_submitted_days = 0;
				nlapiLogExecution('audit','not_submitted_days Final',not_submitted_days);
			}*/
			var filters_total_leaves = new Array();
			filters_total_leaves[0] = new nlobjSearchFilter('employee',null,'is',parseInt(Emp_id));
			filters_total_leaves[1] = new nlobjSearchFilter('internalid','job','is',proj_id);
			filters_total_leaves[2] = new nlobjSearchFilter('date', null, 'within', d_start_date, d_end_date);
			
			var not_submitted_timeentry = nlapiSearchRecord('timebill', 'customsearch_notsubmitted_ti', filters_total_leaves, null);
			var actual_Val=0;
			var allocated_val=0;			
			if (not_submitted_timeentry)
			{
				var not_submitted_duration = 0;
				for (var z = 0; z < not_submitted_timeentry.length; z++) //
				{
					var result_C = not_submitted_timeentry[z];
					var columns = result_C.getAllColumns();
					var column = columns[6];
					var allocated_tym = not_submitted_timeentry[z].getValue(column);
					if(allocated_tym <=0)
					{
						actual_Val = Math.abs(allocated_tym) ;
					}
					else
					{
						allocated_val = allocated_tym;
					}
				}
			}

			var not_submited_hrs = (Number(allocated_val) - Number(actual_Val));

		}
		catch(e)
		{
			nlapiLogExecution('error','error message not submitted entry:-- ',e);
		}

		if(parseFloat(temp_approved_duration) != 0 || parseFloat(temp_not_approved_duration) != 0 || parseFloat(not_submited_hrs) != 0)
		{
			var d_emp_hireDate = nlapiLookupField('employee', Emp_id,'hiredate');
			var d_emp_releaseDate = nlapiLookupField('employee', Emp_id,'custentity_lwd');
			var o_salary_prOBJ = nlapiCreateRecord('customrecord_salary_upload_file');
			
			o_salary_prOBJ.setFieldValue('custrecord_month_sal_up_ven', i_month);
			o_salary_prOBJ.setFieldValue('custrecord_year_sal_up_ven', i_year);
			o_salary_prOBJ.setFieldValue('custrecord_emp_type_sal_up_ven', 3);
			o_salary_prOBJ.setFieldValue('custrecord_employee_rec_id_sal_up_ven', Emp_id);
		    nlapiLogExecution('Debug', 'Emp_id%%%%%%',Emp_id);
			o_salary_prOBJ.setFieldValue('custrecord_subsidiary_sal_up_ven', vendor_provision_subsidiary);
			o_salary_prOBJ.setFieldValue('custrecord_location_sla_up_ven', Emp_location);
			o_salary_prOBJ.setFieldValue('custrecord_currency_ven_pro', Emp_currency);
			o_salary_prOBJ.setFieldValue('custrecord_practice_sla_up_ven', i_practice);
			o_salary_prOBJ.setFieldValue('custrecord_cost_sal_up_ven', Emp_monthly_rate);
			nlapiLogExecution('Debug', 'Emp_monthly_rate%%%%%%',Emp_monthly_rate);
			o_salary_prOBJ.setFieldValue('custrecord_project_assigned', Emp_Project);
			o_salary_prOBJ.setFieldValue('custrecord_project_assigned_id', proj_id);
			o_salary_prOBJ.setFieldValue('custrecord_project_vertical_vendor_pro', proj_vertical);
			o_salary_prOBJ.setFieldValue('custrecord_proj_hours_allocated', Emp_proj_hours);
			o_salary_prOBJ.setFieldValue('custrecord_percent_of_time_allocated', Emp_proj_percent_hours);
			o_salary_prOBJ.setFieldValue('custrecord_no_of_holidays', holidays);// Line added for holiday 22/06/2018
			o_salary_prOBJ.setFieldValue('custrecord_no_of_leaves_taken', leave_duration_days); // Line added for holiday 22/06/2018
			o_salary_prOBJ.setFieldValue('custrecord_no_of_approved_days', parseFloat(temp_approved_duration));
			o_salary_prOBJ.setFieldValue('custrecord_no_of_sub_nt_approved_days', parseFloat(temp_not_approved_duration));
			o_salary_prOBJ.setFieldValue('custrecord_no_of_nt_submitted_days', not_submited_hrs);
			o_salary_prOBJ.setFieldValue('custrecord_vendor_pro_upload_id', i_recordID);
			o_salary_prOBJ.setFieldValue('custrecord_allocated_time', allocated_tym);
			o_salary_prOBJ.setFieldValue('custrecord_rate_type_sal_upload', 3);
			if(_logValidation(d_emp_hireDate))
				o_salary_prOBJ.setFieldValue('custrecord_hire_date',d_emp_hireDate);
			if(_logValidation(d_emp_releaseDate))
				o_salary_prOBJ.setFieldValue('custrecord_release_date', d_emp_releaseDate);
			var i_submitID = nlapiSubmitRecord(o_salary_prOBJ, true, true);
			nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload Monthly Submit ID *************** -->' + i_submitID);
		}
	} 
	catch (er) //
	{
		nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ************************* 1 Monthly Error Block ************************');
		
		var o_salary_pr_errorOBJ = nlapiCreateRecord('customrecord_error_logs_vendor_pro');
		
		o_salary_pr_errorOBJ.setFieldValue('custrecord_error_details', er);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_employee_id_ven_pro', Emp_name);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_subsidiary_ven_pro', vendor_provision_subsidiary);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_month_ven_pro', i_month);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_year_ven_pro', i_year);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_vendor_pro_parent', i_recordID);
		o_salary_pr_errorOBJ.setFieldValue('custrecord_rate_type_error_log', 3);
		
		var i_submit_err_ID = nlapiSubmitRecord(o_salary_pr_errorOBJ, true, true);
		nlapiLogExecution('DEBUG', 'monthly_employee_file_process_data', ' ***************** Salary Upload - Error Logs Submit ID *************** -->' + i_submit_err_ID);
		
		if (_logValidation(i_submit_err_ID)) //
		{
			i_flag = 1;
			i_file_status = 'ERROR'
		}
	}
	
	return i_file_status;
}





function schedule_script_after_usage_exceeded(i, i_month, i_year, i_account_debit, i_account_credit, i_recordID, vendor_provision_subsidiary, i_flag)
{
	////Define all parameters to schedule the script for voucher generation.
	 nlapiLogExecution('audit','Before Scheduling','i -->'+ i);
	 i = i++;
	 var params=new Array();
	 params['status']='scheduled';
 	 params['runasadmin']='T';
	 params['custscript_counter_btpl']=i;
	 params['custscript_sal_upload_date_month']=i_month;
	 params['custscript_sal_upload_date_year']=i_year;
	 params['custscript_account_debit_s_u_btpl']=i_account_debit;
	 params['custscript_account_credit_s_u_btpl']=i_account_credit;
	 params['custscriptrecord_id_s_u_btpl']=i_recordID;
	 params['custscript_vendor_pro_subsidiary']=vendor_provision_subsidiary;
	 params['custscript_flag_btpl']=i_flag;
		 
	 var startDate = new Date();
 	 params['startdate']=startDate.toUTCString();
	
	 var status=nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId(),params);
	 //nlapiLogExecution('DEBUG','After Scheduling','Script Scheduled Status -->'+ status);
	 
	 ////If script is scheduled then successfuly then check for if status=queued
	 if (status == 'QUEUED') 
 	 {
		//nlapiLogExecution('DEBUG', ' SCHEDULED', ' Script Is Re - Scheduled for -->'+i);
 	 }
}//fun close

function removearrayduplicate(array)
{
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) 
	{
        for (var j = 0; j < array.length; j++) 
		{
            if (newArray[j] == array[i]) 
                continue label;
        }
        newArray[newArray.length] = array[i];
    }
    return newArray;
}

function get_allocation_details(i_employee, d_start_date, d_end_date) //
{
	var a_allocate_array = new Array();
	var flag_1 = 0;
	var flag_2 = 0;
	var flag_3 = 0;
	
	if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('resource', null, 'is', i_employee)
		// filters[1] = new nlobjSearchFilter('startdate',null,'onorbefore',d_start_date)
		//filters[2] = new nlobjSearchFilter('startdate',null,'onorafter',d_start_date)
		//filters[3] = new nlobjSearchFilter('enddate',null,'onorbefore',d_end_date)
		//filters[2] = new nlobjSearchFilter('enddate',null,'onorafter',d_end_date)
		
		var column = new Array();
		column[0] = new nlobjSearchColumn('internalid')
		column[1] = new nlobjSearchColumn('startdate')
		column[2] = new nlobjSearchColumn('enddate')
		column[3] = new nlobjSearchColumn('percentoftime')
		column[4] = new nlobjSearchColumn('project')
		
		var a_results = nlapiSearchRecord('resourceallocation', null, filters, column);
		
		if (_logValidation(a_results)) //
		{
			//nlapiLogExecution('DEBUG', 'get_allocation_details', ' ************************** Resource Allocation Results Length **************************  -->' + a_results.length);
			
			for (var i = 0; i < a_results.length; i++) //
			{
				flag_1 = 0;
				flag_2 = 0;
				flag_3 = 0;
				//nlapiLogExecution('DEBUG', 'get_allocation_details', '  --------------------- No. --------------- -->' + (i + 1));
				
				var i_recordID = a_results[i].getValue('internalid');
				//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Record ID -->' + i_recordID);
				
				var d_start_date_s = a_results[i].getValue('startdate');
				//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Start Date -->' + d_start_date_s);
				
				var d_end_date_s = a_results[i].getValue('enddate');
				//nlapiLogExecution('DEBUG', 'get_allocation_details', 'End Date-->' + d_end_date_s);
				
				var i_percent_time = a_results[i].getValue('percentoftime');
				//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Percent Of Time -->' + i_percent_time);
				
				var i_project = a_results[i].getValue('project');
				//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Project -->' + i_project);
				//100.0%
				
				//nlapiLogExecution('DEBUG', 'get_allocation_details', ' Month Start Date -->' + d_start_date + '*****' + 'Month End Date -->' + d_end_date);
				
				
				if ((Date.parse(d_start_date_s) <= Date.parse(d_end_date)) && (Date.parse(d_start_date_s) >= Date.parse(d_start_date))) //
				{
					flag_1 = 1;
					//nlapiLogExecution('DEBUG', ' get_allocation_details', ' Start Date is present ......');
				}
				
				if ((Date.parse(d_end_date_s) >= Date.parse(d_start_date)) && (Date.parse(d_end_date_s) <= Date.parse(d_end_date))) //
				{
					flag_2 = 1;
					//nlapiLogExecution('DEBUG', ' get_allocation_details', ' End Date is present ......');
				}
				
				if ((d_start_date_s <= d_start_date && d_end_date <= d_end_date_s))	//  d_start_date_s <= d_start_date&& d_end_date <= d_end_date_s
				{
					flag_3 = 1;
					//nlapiLogExecution('DEBUG', ' flag_3', ' flag_3 ......');
				}
				
				if ((d_start_date_s >= d_start_date && d_end_date <= d_end_date_s))	//  d_start_date_s <= d_start_date&& d_end_date <= d_end_date_s
				{
					flag_3 = 1;
					//nlapiLogExecution('DEBUG', ' flag_3', ' flag_3 ......');
				}
				
				var d_end_date_s = new Date(d_end_date_s);
				var mm = d_end_date_s.getMonth();
				var dd = d_end_date_s.getDate();
				var yy = d_end_date_s.getFullYear();
				
				var date1 = dd + '/' + mm + '/' + yy;
				
				var d_end_date = new Date(d_end_date);
				var mm1 = d_end_date.getMonth();
				var dd1 = d_end_date.getDate();
				var yy1 = d_end_date.getFullYear();
				
				var date2 = dd1 + '/' + mm1 + '/' + yy1;
				
				var date3 = nlapiStringToDate(date1);
				var date4 = nlapiStringToDate(date2);
				
				if (date3 > date4 || yy > yy1) //
				{
					flag_3 = 1;
				}
				else 
					if (yy == yy1 && mm > mm1) //
					{
						flag_3 = 1;
					}
					else 
						if (yy == yy1 && mm == mm1 && (dd > dd1 || dd == dd1)) //
						{
							flag_3 = 1;
						}
				
				//nlapiLogExecution('DEBUG', ' get_allocation_details', ' ---------- Flag 1 -->' + flag_1 + ' ---------Flag 2 -->' + flag_2 + ' ---------Flag 3 -->' + flag_3);
				if ((flag_1 == 1) || (flag_2 == 1) || (flag_3 == 1)) //
				{
					a_allocate_array[i] = i_percent_time + '^^&&^^' + i_project
				}
				
				//a_allocate_array[i] = '';
			}
		}
	} //Validation
	return a_allocate_array;
} //Allocation


function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}
//--------------------------------------------------------------------------
function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}
//-----------------------------------------------------------------------

function getWeekend(startDate, endDate) //
{
	var i_no_of_sat = 0;
	var i_no_of_sun = 0;
	
	//nlapiLogExecution('DEBUG', 'Check Date', 'startDate : ' + startDate);
	//nlapiLogExecution('DEBUG', 'Check Date', 'endDate : ' + endDate);
	
	var date_format = checkDateFormat();
	
	var startDate_1 = startDate
	var endDate_1 = endDate
	
	startDate = nlapiStringToDate(startDate);
	endDate = nlapiStringToDate(endDate);
	
	//nlapiLogExecution('DEBUG', 'Check Date', '2 startDate : ' + startDate);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 endDate : ' + endDate);
	
	var i_count_day = startDate.getDate();
	
	var i_count_last_day = endDate.getDate();
	
	i_month = startDate.getMonth() + 1;
	
	i_year = startDate.getFullYear();
	
	//nlapiLogExecution('DEBUG', 'Check Date', '2 startDate_1 : ' + startDate_1);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 endDate_1 : ' + endDate_1);
	
	var d_f = new Date();
	var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month
	var sat = new Array(); //Declaring array for inserting Saturdays
	var sun = new Array(); //Declaring array for inserting Sundays
	
	for (var i = i_count_day; i <= i_count_last_day; i++) //
	{
		//looping through days in month
		if (date_format == 'YYYY-MM-DD') //
		{
			var newDate = i_year + '-' + i_month + '-' + i;
		}
		if (date_format == 'DD/MM/YYYY') //
		{
			var newDate = i + '/' + i_month + '/' + i_year;
		}
		if (date_format == 'MM/DD/YYYY') //
		{
			var newDate = i_month + '/' + i + '/' + i_year;
		}
		
		newDate = nlapiStringToDate(newDate);
		
		if (newDate.getDay() == 0) //
		{ //if Sunday
			sat.push(i);
			i_no_of_sat++;
		}
		if (newDate.getDay() == 6) //
		{ //if Saturday
			sun.push(i);
			i_no_of_sun++;
		}
	}
	
	var i_total_days_sat_sun = parseInt(i_no_of_sat) + parseInt(i_no_of_sun);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 i_total_days_sat_sun : ' + i_total_days_sat_sun);
	
	return i_total_days_sat_sun;
}

//=============================================================================================
function check_emp_over_or_under_allocated(i_employee, d_start_date, d_end_date) //
{
	var i_internal_ID;
	
	var i_hours_per_week;
	var i_project_name;
	var i_allocated_hours;
	var flag = 0;
	
	var month_start_date = d_start_date;
	var month_end_date = d_end_date;
	
	//--------------------------employee load to search hire date and termination date-------------
	var o_empOBJ = nlapiLoadRecord('employee', i_employee) //
	if (_logValidation(o_empOBJ)) //
	{//Employee OBJ
		var i_hire_date = o_empOBJ.getFieldValue('hiredate');
		//nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Hire Date  -->' + i_hire_date);
		
		//var i_resigned_date = o_empOBJ.getFieldValue('releasedate'); // This line is commented due to update in logic : we are now using custom field for validation.
		var i_resigned_date = o_empOBJ.getFieldValue('custentity_lwd');
		//nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' Resigned Date -->' + i_resigned_date);
	
	}//Employee OBJ
	
	var i_hire_date_dt_format = null;
	if(i_hire_date!= null) //
	{
		i_hire_date_dt_format = nlapiStringToDate(i_hire_date);
	}
	
	var i_resigned_date_dt_format = null;
	if(_logValidation(i_resigned_date)) //
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Parsing Date ***');
		i_resigned_date_dt_format = nlapiStringToDate(i_resigned_date);
	}
	
	var s_month_start_date_dt_format = nlapiStringToDate(month_start_date);
	var s_month_end_date_dt_format = nlapiStringToDate(month_end_date);
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	if (_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //i_month
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 1 ***');
		if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
		{
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is current month .....');
			
			d_start_date = i_hire_date
			d_end_date = d_end_date
		}
		else //
		{
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 1 Hire Date is not current month .....');
			d_start_date = d_start_date
			d_end_date = d_end_date
		}
		
	}//Hire Date is not blank & Termination Date is blank
	if (_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 2 ***');
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' both r present');
		if ((Date.parse(i_hire_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_hire_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
		{
			if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Hire Date is current month .....');
				d_start_date = i_hire_date;
				
				//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 Resigned Date is current month .....');
				d_end_date = i_resigned_date
			}
			else //
			{
				//	//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Hire Date is current month .....');
				d_start_date = i_hire_date;
				
				//	//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' 2 A Resigned Date is current month .....');
				d_end_date = d_end_date
			}
		}
		else 
			if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
			{
				d_start_date = d_start_date
				d_end_date = i_resigned_date;
			}
			else //
			{
				d_start_date = d_start_date
				d_end_date = d_end_date
			}
	}//Hire Date is not blank & Termination Date is not blank
	
	if (!_logValidation(i_hire_date_dt_format) && _logValidation(i_resigned_date_dt_format)) //
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 3 ***');
		if ((Date.parse(i_resigned_date_dt_format) <= Date.parse(s_month_end_date_dt_format) && Date.parse(i_resigned_date_dt_format) >= Date.parse(s_month_start_date_dt_format))) //
		{
			//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' no hire date but resigned present .....');
			
			d_start_date = d_start_date
			d_end_date = i_resigned_date
			
		}
		else //
		{
			d_start_date = d_start_date
			d_end_date = d_end_date
			
		}
		
	}//Hire Date is blank & Termination Date is not blank
	if (!_logValidation(i_hire_date_dt_format) && !_logValidation(i_resigned_date_dt_format)) //
	{
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation','*** Condition 4 ***');
		//nlapiLogExecution('DEBUG', ' monthly_JE_creation',' No Hire Date & Resigned Date .....');
		
		d_start_date = d_start_date;
		d_end_date = d_start_date;
		
	}//Hire Date is blank & Termination Date is blank
	if (_logValidation(d_start_date)) //
	{
		d_start_date = nlapiStringToDate(d_start_date);
		d_start_date = nlapiDateToString(d_start_date);
	}
	
	if (_logValidation(d_end_date)) //
	{
		d_end_date = nlapiStringToDate(d_end_date);
		d_end_date = nlapiDateToString(d_end_date)
		
	}
	
    //nlapiLogExecution('DEBUG', 'Test', 'd_start_date : ' + d_start_date);
    //nlapiLogExecution('DEBUG', 'Test', 'd_end_date : ' + d_end_date);
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
    var i_total_days = getDatediffIndays(d_start_date, d_end_date);
    //nlapiLogExecution('DEBUG', 'i_total_days', i_total_days);
    
    var i_total_sat_sun = getWeekend(d_start_date, d_end_date);
    //nlapiLogExecution('DEBUG', 'i_total_sat_sun', i_total_sat_sun);
    
    var tot_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
    //nlapiLogExecution('DEBUG', 'tot_hrs', tot_hrs); //i_total_days
	//---------------------------------------------------------------------------------------------
	
	if (_logValidation(i_employee) && _logValidation(d_start_date) && _logValidation(d_end_date)) //
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('employee', null, 'is', i_employee)
		//filters[1] = new nlobjSearchFilter('date', null, 'onorafter', month_start_date)
		//filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', month_end_date)
		
		filters[1] = new nlobjSearchFilter('date', null, 'onorafter', d_start_date);
		filters[2] = new nlobjSearchFilter('date', null, 'onorbefore', d_end_date);
		
		var i_search_results = nlapiSearchRecord('timebill', 'customsearch_salaried_new_ti_2', filters, null);
		// Nihal have confusion 
		if (_logValidation(i_search_results)) //
		{
			//if start
			//nlapiLogExecution('DEBUG', 'monthly_JE_creation', ' ----------- Salaried Time Search Length --------->' + i_search_results.length);
			
			var a_search_transaction_result = i_search_results[0];
			
			if (a_search_transaction_result == null || a_search_transaction_result == '' || a_search_transaction_result == undefined) //
			{
			
			}
			var columns = a_search_transaction_result.getAllColumns();
			
			var columnLen = columns.length;
			var i_duration = a_search_transaction_result.getValue(columns[2]);
		
			/*for (var hg = 0; hg < columnLen; hg++)
			 {
			 var column = columns[hg];
			 var label = column.getLabel();
			 var value = a_search_transaction_result.getValue(column)
			 var text = a_search_transaction_result.getText(column)
			 if (label == 'Duration')
			 {
			 var i_duration = value;
			 }
			 }*/
			nlapiLogExecution('DEBUG','i_duration',i_duration);
			
			if (parseFloat(i_duration) > parseFloat(tot_hrs)) //
			{
				flag = 1;
			}
			
			if (parseFloat(i_duration) < parseFloat(tot_hrs)) //
			{
				flag = 2;
			}
		}// if close
	}
	return flag;
} //Monthly Details
//=============================================================================================

function get_emp_list_for_salary_upload(d_start_date, d_end_date)
{
	var a_allocate_array = new Array();
	var flag_1 = 0;
	var flag_2 = 0;
	var flag_3 = 0;
	
	if (_logValidation(d_start_date) && _logValidation(d_end_date))
	{
		var filters = new Array();
		//filters[0] = new nlobjSearchFilter('startdate',null,'onorbefore',d_start_date);
		//filters[1] = new nlobjSearchFilter('enddate',null,'onorafter',d_end_date);
		filters[0] = new nlobjSearchFilter('subsidiary','employee','is',vendor_provision_subsidiary);
		
		var column = new Array();
		column[0] = new nlobjSearchColumn('internalid')
		column[1] = new nlobjSearchColumn('startdate')
		column[2] = new nlobjSearchColumn('enddate')
		column[3] = new nlobjSearchColumn('percentoftime')
		column[4] = new nlobjSearchColumn('project')
		column[5] = new nlobjSearchColumn('resource')
		
		var a_results = nlapiSearchRecord(null, 'customsearch_emplist_for_sal_upload_btpl', filters, null);
		
		if (_logValidation(a_results)) //
		{
			
			for (var i = 0; i < a_results.length; i++) //
			{
				var result_C = a_results[0];
				var columns = result_C.getAllColumns();
				var columnLen = columns.length;
				
				for (var j = 0; j < columnLen; j++)
				{
					var column = columns[j];
					var label = column.getLabel();
					
					if (label == 'Employee Id')
					{
						var Emp_id = a_results[i].getValue(column);
					}
					else if (label == 'Employee')
					{
						var Emp_name = a_results[i].getText(column);
					}
					else
					{
						var Emp_monthly_rate = a_results[i].getValue(column);
						if(!_logValidation(Emp_monthly_rate))
							Emp_monthly_rate = 0;
					}
					
					if(Emp_monthly_rate == 0)
					{
						var err_flag = 1;
						return "Emp Monthly rate is blank for emp:- "+Emp_name;
						break;
					}
				}
			}
			return a_results;
		}
	} //Validation
} //Allocation

function daysInMonth(i_month,year) {
	i_month = i_month.trim();
	
	if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	{	  	
		i_month = 1;		
	}	
	else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	{
		i_month = 2;	
	}		
	else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	{	  
		i_month = 3;
	}	
	else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	{
		i_month = 4;
	}	
	else if(i_month == 'May' || i_month == 'MAY')
	{	  
		i_month = 5;
	}	  
	else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	{	 
		i_month = 6;
	}	
	else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	{	  
		i_month = 7;
	}	
	else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	{	  
		i_month = 8;
	}  
	else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	{  	
		i_month = 9;
	}	
	else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	{	 	
		i_month = 10;
	}	
	else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	{	  
		i_month = 11;
	}	
	else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	{	 
		i_month = 12;
	}
	
	year = Number(year).toFixed(0);
    return new Date(year, i_month, 0).getDate();
}

function get_previous_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}

function get_previous_month(i_month)
{
	if (_logValidation(i_month)) 
 	{
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
	  		i_month = 'December';				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	i_month = 'January';
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     i_month = 'February';
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	i_month = 'March';
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		  i_month = 'April';
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 i_month = 'May';	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		i_month = 'June';
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 i_month = 'July';
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	      i_month = 'August';
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
			i_month = 'September';	
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		 i_month = 'October';
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		 i_month = 'November';
			
	  }	
 }//Month & Year
	
	return i_month;
}

function get_previous_month_end_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%4) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

// END FUNCTION =====================================================