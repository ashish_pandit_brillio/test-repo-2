/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Sep 2019     Aazamali Khan
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function checkRevisedStartDateEndDate(type) {
	if (type == "edit" || type == "xedit") {
		var parameter = [];
		
		var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		var oldRec = nlapiGetOldRecord();
		
		var reviseStartDate = recObj.getFieldValue("custrecord_sfdc_opp_rec_rev_startdate");
		//reviseStartDate = nlapiStringToDate(reviseStartDate, "date");
		
		//reviseStartDate = convertStringToDate(reviseStartDate);
		
		
		nlapiLogExecution("DEBUG", "datatype" + typeof(reviseStartDate), reviseStartDate)
		var reviseEndDate = recObj.getFieldValue("custrecord_sfdc_opp_rec_rev_enddate");
		//reviseEndDate = nlapiStringToDate(reviseEndDate, "date");
		var oldStartDate = oldRec.getFieldValue("custrecord_sfdc_opp_rec_rev_startdate");
		//oldStartDate = nlapiStringToDate(oldStartDate, "date");
		
		//oldStartDate = convertStringToDate(oldStartDate);
		
		var oldEndDate = oldRec.getFieldValue("custrecord_sfdc_opp_rec_rev_enddate");
		//oldEndDate = nlapiStringToDate(oldEndDate, "date");
		
		
		
		if (reviseStartDate != oldStartDate || reviseEndDate != oldEndDate) {
			// call schedule script to update the FRFs 
			parameter["custscript_fuel_sch_oppid"] = nlapiGetRecordId();
			nlapiScheduleScript("customscript_fuel_sch_update_frf_revised", "customdeploy_fuel_sch_update_frf_revised", parameter)
		}
	}
}

function _logValidation(value) {
	if (value != null && value != undefined && value != '' && value != 'undefined') {
		return true;
	} else {
		return false;
	}
}

function convertStringToDate(date){
	
	 return receive_date = new Date(date);
	
	
}