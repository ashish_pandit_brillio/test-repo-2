// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : USER_IDENTIFIER.js
	Author      : ASHISH PANDIT
	Date        : 11 APRIL 2018
    Description : Script to get Logged in User details. 
*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================

/*****************GET ADMIN*******************/
function GetAdmin(user) 
{
  	try
	{
		nlapiLogExecution('AUDIT', 'user GetAdmin ', user);
		var adminSearch = nlapiSearchRecord("customrecord_fuel_leadership",null,
				[
				 ["custrecord_fuel_leadership_employee","anyof",user],
				  "AND",
				 ["isinactive","is","F"]
				], 
				[
				  new nlobjSearchColumn("custrecord_fuel_leadership_employee")
				]
		);
		if(adminSearch)
		{
			var admin = adminSearch[0].getValue('custrecord_fuel_leadership_employee');
			return admin;
		}
		else
		{
			return '';
		}
	}
	catch(err)
	{
		return err;
	}
}


/*****************GET PRACTICE SPOC*******************/
function GetPracticeSPOC(user) 
{
  	try
	{
		nlapiLogExecution('AUDIT', 'user', user);
		var spocSearch = nlapiSearchRecord("customrecord_practice_spoc",null,
				[
				 ["custrecord_spoc","anyof",user],
				  "AND",
				 ["isinactive","is","F"]
				], 
				[
				  new nlobjSearchColumn("custrecord_practice")
				]
		);
		if(spocSearch)
		{
			var practice = spocSearch[0].getValue('custrecord_practice');
			return practice;
		}
		else
		{
			return '';
		}
		
	}
	catch(e)
	{
		return e; 
	}
	
}


/**************GET USER USING EMAILID *************/
function getUserUsingEmailId(emailId) 
{
	try 
	{
		var employeeSearch = nlapiSearchRecord('employee', null, [
		        new nlobjSearchFilter('email', null, 'is', emailId),
		        new nlobjSearchFilter('custentity_employee_inactive', null, 'is', 'F') ]);

		if (employeeSearch) 
		{
			return employeeSearch[0].getId();
		} 
		else 
		{
			throw "User does not exist";
		}
	} 
	catch (err) 
	{
		nlapiLogExecution('ERROR', 'getUserUsingEmailId', err);
		throw err;
	}
}

/****************GET PROJECT SEARCH OBJECT FOR PM/DM******************************/
function GetProjects(user)
{
	try
	{
		var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
		[
		   ["isinactive","is","F"], 
		   "AND", 
		   ["custrecord_frf_details_end_date","onorafter","today"], 
		   "AND", 
		   ["custrecord_frf_details_status_flag","noneof","2"], 
		   "AND", 
		   [["custrecord_frf_details_project.custentity_deliverymanager","anyof",user],"OR",["custrecord_frf_details_project.custentity_projectmanager","anyof",user]], 
		   "OR", 
		   [["custrecord_frf_details_opp_id.custrecord_sfdc_opportunity_dm","anyof",user],"OR",["custrecord_frf_details_opp_id.custrecord_sfdc_opp_pm","anyof",user]]
		], 
		[
		   new nlobjSearchColumn("scriptid").setSort(false), 
		   new nlobjSearchColumn("custrecord_frf_details_opp_id"), 
		   new nlobjSearchColumn("custrecord_frf_details_res_location"), 
		   new nlobjSearchColumn("custrecord_frf_details_res_practice"), 
		   new nlobjSearchColumn("custrecord_frf_details_emp_level"), 
		   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
		   new nlobjSearchColumn("custrecord_frf_details_role"), 
		   new nlobjSearchColumn("custrecord_frf_details_bill_rate"), 
		   new nlobjSearchColumn("custrecord_frf_details_critical_role"), 
		   new nlobjSearchColumn("custrecord_frf_details_skill_family"), 
		   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
		   new nlobjSearchColumn("custrecord_frf_details_internal_fulfill"), 
		   new nlobjSearchColumn("custrecord_frf_details_primary_skills"), 
		   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
		   new nlobjSearchColumn("custrecord_frf_details_project"), 
		   new nlobjSearchColumn("custrecord_frf_details_suggestion"), 
		   new nlobjSearchColumn("custrecord_frf_type"), 
		   new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
		   new nlobjSearchColumn("custrecord_frf_details_billiable"), 
		   new nlobjSearchColumn("custrecord_frf_details_account"), 
		   new nlobjSearchColumn("custrecord_frf_details_source"), 
		   new nlobjSearchColumn("custrecord_frf_details_secondary_skills"), 
		   new nlobjSearchColumn("custrecord_frf_details_allocation"), 
		   new nlobjSearchColumn("custrecord_frf_details_dollar_loss"), 
		   new nlobjSearchColumn("custrecord_frf_details_suggest_pref_mat"), 
		   new nlobjSearchColumn("custrecord_frf_details_special_req"), 
		   new nlobjSearchColumn("custrecord_frf_details_personal_email"), 
		   new nlobjSearchColumn("custrecord_frf_details_created_by"), 
		   new nlobjSearchColumn("custrecord_frf_details_parent"), 
		   new nlobjSearchColumn("custrecord_frf_details_status_flag"), 
		   new nlobjSearchColumn("custrecord_frf_details_availabledate"), 
		   new nlobjSearchColumn("custrecord_frf_details_sales_activity"), 
		   new nlobjSearchColumn("custrecord_frf_details_region"), 
		   new nlobjSearchColumn("custrecord_frf_details_allocated_emp"), 
		   new nlobjSearchColumn("custrecord_frf_details_allocated_date"), 
		   new nlobjSearchColumn("custrecord_frf_details_backup_require"), 
		   new nlobjSearchColumn("custrecord_frf_details_job_title"), 
		   new nlobjSearchColumn("custrecord_frf_details_edu_lvl"), 
		   new nlobjSearchColumn("custrecord_frf_details_edu_program"), 
		   new nlobjSearchColumn("custrecord_frf_details_emp_status"), 
		   new nlobjSearchColumn("custrecord_frf_details_first_interviewer"), 
		   new nlobjSearchColumn("custrecord_frf_details_sec_interviewer"), 
		   new nlobjSearchColumn("custrecord_frf_details_cust_interview"), 
		   new nlobjSearchColumn("custrecord_frf_details_cust_inter_email"), 
		   new nlobjSearchColumn("custrecord_frf_details_ta_manager"), 
		   new nlobjSearchColumn("custrecord_frf_details_req_type"), 
		   new nlobjSearchColumn("custrecord_frf_details_reason_external"), 
		   new nlobjSearchColumn("custrecord_frf_details_rrf_number")
		]
		);
		if(customrecord_frf_detailsSearch)
		{
			return customrecord_frf_detailsSearch;
		}
		else
		{
			return "NO DATA FOUND";
		}
	}
	catch(error)
	{
		return error;
	}
}
//*******************Search Record ********************************************//
function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isTrue(value) {

	return value == 'T';
}

function isFalse(value) {

	return value != 'T';
}

//****************************

function GetRegion(user) 
{
  	try
	{
		nlapiLogExecution('AUDIT', 'user', user);
		var regionSearch = nlapiSearchRecord("customrecord_region",null,
				[
				[["custrecord_region_head","anyof",user],"OR",["custrecord_region_head_2","anyof",user]],
				  "AND",
				 ["isinactive","is","F"]
				], 
				[
				  new nlobjSearchColumn("name")
				]
		);
		if(regionSearch)
		{
			var region = regionSearch[0].getValue('name');
			return region;
		}
		else
		{
			return '';
		}
		
	}
	catch(e)
	{
		return e; 
	}
	
}
//############### Code to create array from string array ##### 
function getSkillIds(s_skills)

{
	nlapiLogExecution("ERROR","skills Ids : ",typeof(s_skills));
    var resultArray = new Array();

    if(_logValidation(s_skills))

    {

        nlapiLogExecution('Debug','s_skills in function',s_skills);

        var temp = s_skills.split(',');

        for(var i=0; i<temp.length;i++)

        {

            resultArray.push(temp[i]);

        }

    }

    return resultArray;

}
//#########
//
function _logValidation(value){
	if (value != null && value != '' && value != undefined && value != 'NaN') {
		return true;
	}
	else {
		return false;
	}
}
