/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       05 Feb 2015     amol.sahijwani
 *
 */

/**
 * @param {String} recType Record type internal id
 * @param {Number} recId Record internal id
 * @returns {Void}
 */
function massUpdate(recType, recId) {
	var record = nlapiLoadRecord(recType, recId);
	var is_record_changed = false;
	var i_employee_id = record.getFieldValue('custbody_consultantname');
	
	var i_practice = null;
	
	var i_territory = null;
	
	var is_vendor_po = false;
	
	if(i_employee_id == null || i_employee_id == undefined || i_employee_id == '')
		{
			is_vendor_po = false;
			
			//i_practice = nlapiLookupField('employee', i_employee_id, 'department');
		}
	else
		{
			i_practice = nlapiLookupField('employee', i_employee_id, 'department');
		}
	
	var i_line_item_number = record.getLineItemCount('item');
	
	for(var i = 1; i <= i_line_item_number; i++)
		{
			var i_pr_item_id = record.getLineItemValue('item', 'custcol_po_pr_item', i);
			
			if(i_pr_item_id != '' && i_pr_item_id != null)
				{
					var rec_pr_item = nlapiLoadRecord('customrecord_pritem', i_pr_item_id);
				
					i_practice = rec_pr_item.getFieldValue('custrecord_prpractices');
				
					i_territory = rec_pr_item.getFieldValue('custrecord_prterritory');
				}
			else
				{
					var s_project_name = record.getLineItemValue('item', 'custcolprj_name', i);
					
					if(s_project_name != '' && s_project_name != null)
						{
						
							var strProjectId = s_project_name.substr(0, s_project_name.indexOf(' '));
						
							var i_project_id = searchProject(strProjectId);
							
							if(i_project_id > 0)
								{
									var i_end_customer = nlapiLookupField('job',i_project_id,'custentity_endcustomer');

									//var i_practice = nlapiLookupField('job', i_project_id, 'custentity_practice');
									
									var i_customer = nlapiLookupField('job',i_project_id,'customer');
									i_territory = nlapiLookupField('customer', i_customer, 'territory');
								}
							else
								{
									i_territory = null;
								}
						}
					
					if(i_practice == '' || i_practice == null)
						{
							var s_employee_name = record.getLineItemValue('item', 'custcol_employeenamecolumn', i);
							if(s_employee_name != '' && s_employee_name != null)
								{
									var i_employee_id = searchEmployee(s_employee_name);
								
									i_practice = nlapiLookupField('employee', i_employee_id, 'department');
								}	
						}
				}
			
			var i_current_practice = record.getLineItemValue('item','department',i);
			
			var i_current_territory = record.getLineItemValue('item','custcol_territory',i);
			
			if((i_practice != i_current_practice || i_territory != i_current_territory) && i_current_practice != null)
				{
					if(i_practice != null)
						{
							record.setLineItemValue('item', 'department', i, i_practice);
						}					
					
					record.setLineItemValue('item', 'custcol_territory', i, i_territory);
					nlapiLogExecution('AUDIT', 'Purchase Order: ' + recId + ', Line Number: ' + i, i_current_practice + ' -> ' + i_practice + ',' + i_current_territory + ' -> ' + i_territory);
					is_record_changed = true;
				}
		}
	
	var i_line_item_number = record.getLineItemCount('expense');
	
	for(var i = 1; i <= i_line_item_number; i++)
		{
			var i_pr_item_id = record.getLineItemValue('expense', 'custcol_po_pr_item', i);
			
			if(i_pr_item_id != '' && i_pr_item_id != null)
				{
					var rec_pr_item = nlapiLoadRecord('customrecord_pritem', i_pr_item_id);
				
					i_practice = rec_pr_item.getFieldValue('custrecord_prpractices');
				
					i_territory = rec_pr_item.getFieldValue('custrecord_prterritory');
				}
			else
				{
					var s_project_name = record.getLineItemValue('expense', 'custcolprj_name', i);
					
					if(s_project_name != '' && s_project_name != null)
						{
						
							var strProjectId = s_project_name.substr(0, s_project_name.indexOf(' '));
						
							var i_project_id = searchProject(strProjectId);
							
							if(i_project_id > 0)
								{
									var i_end_customer = nlapiLookupField('job',i_project_id,'custentity_endcustomer');

									//var i_practice = nlapiLookupField('job', i_project_id, 'custentity_practice');
									
									var i_customer = nlapiLookupField('job',i_project_id,'customer');
									i_territory = nlapiLookupField('customer', i_customer, 'territory');									
								}
							else
								{
									i_territory = null;
								}
						}
					
					if(i_practice == '' || i_practice == null)
					{
						var s_employee_name = record.getLineItemValue('expense', 'custcol_employeenamecolumn', i);
						if(s_employee_name != '' && s_employee_name != null)
							{
								var i_employee_id = searchEmployee(s_employee_name);
							
								i_practice = nlapiLookupField('employee', i_employee_id, 'department');
							}	
					}
				}
			
			var i_current_practice = record.getLineItemValue('expense','department',i);
			
			var i_current_territory = record.getLineItemValue('expense','custcol_territory',i);
			
			if((i_practice != i_current_practice || i_territory != i_current_territory) && i_current_practice != null && i_territory != null)
				{
					if(i_practice != null)
					{
						record.setLineItemValue('expense', 'department', i, i_practice);
					}
					
					record.setLineItemValue('expense', 'custcol_territory', i, i_territory);
					nlapiLogExecution('AUDIT', 'Purchase Order: ' + recId + ', Line Number: ' + i, i_current_practice + ' -> ' + i_practice + ',' + i_current_territory + ' -> ' + i_territory);
					is_record_changed = true;
				}
		}
	
	if(is_record_changed == true)
		{
			nlapiSubmitRecord(record);
			
			nlapiLogExecution('AUDIT', 'Purchase Order Updated: ', recId);
			
		}
}

function searchProject(strProjectId)
{
	var filters = [];
	filters[0] = new nlobjSearchFilter('entityid', null, 'is', strProjectId);
	
	var columns = [];
	columns[0] = new nlobjSearchColumn('internalid');
	
	var searchResult = nlapiSearchRecord('job', null, filters, columns);
	
	if(searchResult != null && searchResult.length == 1)
		{
			var i_project_id = searchResult[0].getValue('internalid');
			
			return i_project_id;
		}
	else
		{
			return -1;
		}
}

function searchEmployee(strEmployeeName)
{
	var s_employee_name = strEmployeeName.slice(0,6) + strEmployeeName.slice(7, strEmployeeName.length);
	var filters = [];
	filters[0] = new nlobjSearchFilter('entityid', null, 'is', s_employee_name);
	
	var columns = [];
	columns[0] = new nlobjSearchColumn('internalid');
	
	var searchResult = nlapiSearchRecord('employee', null, filters, columns);
	
	if(searchResult != null && searchResult.length == 1)
		{
			var i_project_id = searchResult[0].getValue('internalid');
			
			return i_project_id;
		}
	else
		{
			return -1;
		}
}