/**
 * @author Nikhil
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Mapping_Project_Values.js
	Author      :
	Date        :
	Description :


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord_check(type,form)
{



	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY






}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


    //  LOCAL VARIABLES


    //  BEFORE SUBMIT CODE BODY


	return true;

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_mapping_projectrecord_values(type)
{
	if(type=='create')
	{
			try 
			{
			var i_record = nlapiGetRecordId();
			var s_recordtype = nlapiGetRecordType();
			
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' Record ID -->' + i_record);
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' Record Type -->' + s_recordtype);
			
			var record_obj = nlapiLoadRecord(s_recordtype, i_record);
			
			if(record_obj!=null && record_obj!='' && record_obj!=undefined)
			{			
			var project_customer = record_obj.getFieldText('parent');
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' project_customer -->' + project_customer);
							
			var project_type = record_obj.getFieldText('jobtype');
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' project_type -->' + project_type);
			
			var project_name = record_obj.getFieldValue('entityid');
			nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' project_name -->' + project_name);
			
			if (project_type != 'Internal')
			{
				var sow_recordOBJ = search_sow(i_record);
				
				if (sow_recordOBJ != null && sow_recordOBJ != undefined && sow_recordOBJ != '') 
				{
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'we find sow');
					var customer = sow_recordOBJ.getFieldValue('entity');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' customer -->' + customer);

                     var work_location = sow_recordOBJ.getFieldValue('custbody_worklocation');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' work_location -->' + work_location);


					var Date = sow_recordOBJ.getFieldValue('trandate');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  date-->' + Date);
					var subsidiary = sow_recordOBJ.getFieldValue('subsidiary');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  subsidiary-->' + subsidiary);

					var Vertical = sow_recordOBJ.getFieldValue('class');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  vertical-->' + Vertical);

					var so_total = sow_recordOBJ.getFieldValue('total');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  total-->' + so_total);

					var Billing_Type = sow_recordOBJ.getFieldValue('custbody_billingtype');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  billing type-->' + Billing_Type);

					var currency = sow_recordOBJ.getFieldValue('currency');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' currency -->' + currency);

					var project_model = sow_recordOBJ.getFieldValue('custbody_projectmodel');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  projectmodel-->' + project_model);

					var client_partner = sow_recordOBJ.getFieldValue('custbody_accountmanager');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' client partner -->' + client_partner);

					var start_date = sow_recordOBJ.getFieldValue('custbody_projectstartdate');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', '  start date-->' + start_date);

					var end_date = sow_recordOBJ.getFieldValue('custbody_projectenddate');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' endate -->' + end_date);

                    var i_location = sow_recordOBJ.getFieldValue('location');
					nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' i_location -->' + i_location);

					var line_count = sow_recordOBJ.getLineItemCount('item');
					for (var i = 1; i <= line_count; i++)
					{
						if (sow_recordOBJ.getLineItemValue('item', 'custcol_sow_project', i) == i_record) 
						{							
							var project = sow_recordOBJ.getLineItemValue('item', 'custcol_sow_project', i);
							nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'project -->' + project);

							var rate = sow_recordOBJ.getLineItemValue('item', 'rate', i);
							nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'rate -->' + rate);

							var item = sow_recordOBJ.getLineItemValue('item', 'item', i);
							nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'item -->' + item);

							var quantity = sow_recordOBJ.getLineItemValue('item', 'quantity', i);
							nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'item -->' + quantity);
						}
					}					
                        if (validate_value(i_location)) 
						{
							record_obj.setFieldValue('custentity_location', i_location);
						}
						if (validate_value(customer)) 
						{
							record_obj.setFieldValue('parent', customer);
						}
						if (validate_value(customer)) 
						{
							record_obj.setFieldValue('parent', customer);
						}
						if (validate_value(Date)) 
						{
							record_obj.setFieldValue('custentity_sowdate', Date);
						}
						if (validate_value(subsidiary)) 
						{
							record_obj.setFieldValue('subsidiary', subsidiary);
						}
						if (validate_value(Vertical)) 
						{
							record_obj.setFieldValue('custentity_vertical', Vertical);
						}
						if( validate_value(so_total))
						 {
						 record_obj.setFieldValue('custentity_projectvalue',so_total);
						 }
						 if( _logValidation(work_location))
						 {
						 record_obj.setFieldValue('custentity_worklocation',work_location);
						 }			
						 
						
 
						 if (Billing_Type == 1) 
						 {
							if (validate_value(Billing_Type))
							{
								record_obj.setFieldValue('jobbillingtype', 'FBM');
							}
						}
						 if (Billing_Type == 2) 
						 {
							if (validate_value(Billing_Type))
							{
								record_obj.setFieldValue('jobbillingtype', 'FBI');
							}
						 }
						 if (Billing_Type == 3) 
						 {
							if (validate_value(Billing_Type))
							{
								record_obj.setFieldValue('jobbillingtype', 'TM');
							}
						 }

						if (validate_value(currency)) 
						{
							record_obj.setFieldValue('currency', currency);
						}
						var g_project_model;
						if(project_model == 1)
						{
							g_project_model = 2
						}
						if(project_model == 2)
						{
							g_project_model = 1
						}
						if(project_model == 3)
						{
							g_project_model = 3
						}
						if (validate_value(project_model)) 
						{
							record_obj.setFieldValue('custentity_deliverymodel', g_project_model);
						}
						if (validate_value(client_partner)) 
						{
							record_obj.setFieldValue('custentity_clientpartner', client_partner);
						}
						if (validate_value(start_date)) 
						{
							record_obj.setFieldValue('startdate', start_date);
						}
						if (validate_value(end_date)) 
						{
							record_obj.setFieldValue('enddate', end_date);
						}
					if(project_customer!='COIN-01 Collabera Inc')
					{						
					var cust_obj = nlapiLoadRecord('customer', customer);
					if (cust_obj != null && cust_obj != '' && cust_obj != undefined)
					{
						var customer_sector = cust_obj.getFieldValue('category');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' customer sector-->' + customer_sector);

						var customer_job = cust_obj.getFieldValue('custentity_customerlob');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' customer_job-->' + customer_job);

						var region = cust_obj.getFieldValue('custentity_entitygeo');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' geo/region-->' + region);

						var customer_id = cust_obj.getFieldValue('entityid');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' customer id-->' + customer_id);

						var hours_per_day = cust_obj.getFieldValue('custentity_hoursperday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' hours per day-->' + hours_per_day);

						var hours_per_week = cust_obj.getFieldValue('custentity_hoursperweek');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' hours per week-->' + hours_per_week);

						var days_for_month = cust_obj.getFieldValue('custentity_daysformonth');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' days for month-->' + days_for_month);

						var ot_applicable = cust_obj.getFieldValue('custentity_otapplicable');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'ot applicable-->' + ot_applicable);

						var ot_rate = cust_obj.getFieldValue('custentity3');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'ot rate -->' + ot_rate);

						var sunday = cust_obj.getFieldValue('custentity_sunday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' sunday-->' + sunday);

						var monday = cust_obj.getFieldValue('custentity_monday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' monday-->' + monday);

						var tuesday = cust_obj.getFieldValue('custentity_tuesday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'tuesday -->' + tuesday);

						var wednesday = cust_obj.getFieldValue('custentity_wednesday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'wednesday -->' + wednesday);

						var thrusday = cust_obj.getFieldValue('custentity_thursday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' thrusday-->' + thrusday);

						var friday = cust_obj.getFieldValue('custentity_fridaycustomerform');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', ' friday-->' + friday);

						var saturday = cust_obj.getFieldValue('custentity_saturday');
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'saturday-->' + saturday);
               
			            var i_item_pricing_cnt = cust_obj.getLineItemCount('itempricing')
						
						if(_logValidation(i_item_pricing_cnt))
						{
						  for(var yu=1;yu<=i_item_pricing_cnt;yu++)
						  {
						  	var i_item_cr = cust_obj.getLineItemValue('itempricing','item',yu)
							
							var i_level =cust_obj.getLineItemValue('itempricing','level',yu)
							
							var i_currency =cust_obj.getLineItemValue('itempricing','currency',yu)
							
							var i_price = cust_obj.getLineItemValue('itempricing','price',yu)
							
							nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'i_item_cr-->' + i_item_cr);
	                        nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'i_level-->' + i_level);               	
							nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'i_currency-->' + i_currency);
	                        nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'i_price-->' + i_price);
	               								
						var record = nlapiCreateRecord('customrecord_customraterecord');
						
						if (_logValidation(i_record)) 
						{
							record.setFieldValue('custrecord_proj', i_record);
						}
                        if (_logValidation(i_price)) 
						{
							record.setFieldValue('custrecord_rate', parseInt(i_price));
						}
						if (_logValidation(i_currency)) 
						{
							record.setFieldValue('custrecord_currency1', parseInt(i_currency));
						}						
						record.setFieldValue('custrecord_qty1', parseInt(0));
						
						if (_logValidation(i_item_cr)) 
						{
							record.setFieldValue('custrecord_item1', i_item_cr);
						}
						
						var r_id = nlapiSubmitRecord(record,true,true);
						nlapiLogExecution('DEBUG', 'afterSubmit_mapping', 'rate record id-->' + r_id);
													
						  }//Loop					
							
						}//Item Pricing
						
						if (validate_value(customer_sector)) 
						{
							record_obj.setFieldValue('category', customer_sector);
						}
						if (validate_value(customer_job)) 
						{
							record_obj.setFieldValue('custentity_customerlob', customer_job);
						}
						if (validate_value(customer_id)) 
						{
							record_obj.setFieldValue('custentity_customerid', customer_id);
						}
						if (validate_value(hours_per_day)) 
						{
							record_obj.setFieldValue('custentity_hoursperday', hours_per_day);
						}
						if (validate_value(hours_per_week)) 
						{
							record_obj.setFieldValue('custentity_hoursperweek', hours_per_week);
						}
						if (validate_value(days_for_month)) 
						{
							record_obj.setFieldValue('custentity_daysformonth', days_for_month);
						}
						if (validate_value(ot_applicable)) 
						{
							record_obj.setFieldValue('custentity_otapplicable', ot_applicable);
						}
						if (validate_value(ot_rate)) 
						{
							record_obj.setFieldValue('custentity3', ot_rate);
						}
						if (validate_value(region)) 
						{
							record_obj.setFieldValue('custentity_entitygeo', region);
						}

						record_obj.setFieldValue('custentity_sunday', sunday);
						record_obj.setFieldValue('custentity_monday', monday);
						record_obj.setFieldValue('custentity_tuesday', tuesday);
						record_obj.setFieldValue('custentity_wednesday', wednesday);
						record_obj.setFieldValue('custentity_thursday', thrusday);
						record_obj.setFieldValue('custentity_fridaycustomerform', friday);
						record_obj.setFieldValue('custentity_saturday', saturday);
                        						
                      
				
					}
					}//Coleebra Customer
					
				}
			}
				nlapiSubmitRecord(record_obj,true,true)
			}//Project Customer
		
		}
		  catch(exception)
	   {
	   	nlapiLogExecution('DEBUG', 'mapping_project values', " -->"+exception);

	   }

	}

  return true;
}


// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
function search_sow(i_record)
{
	nlapiLogExecution('DEBUG','Search SOW','i_record=='+i_record);
			
	var Filters = new Array();
	var Column  = new Array();

	Filters[0]=new nlobjSearchFilter('internalid','job','is',i_record)
	
	Column[0]= new nlobjSearchColumn('internalid');
	Column[1]= new nlobjSearchColumn('name');

	var result = nlapiSearchRecord('salesorder', null,Filters,Column);
	nlapiLogExecution('DEBUG','In getResourcerate','search=='+result);
	
	if(result!=null)
	{
		var internalid = result[0].getValue('internalid');
		nlapiLogExecution('DEBUG','In SO data','internalid=='+internalid);
		
		var name = result[0].getValue('name');
		nlapiLogExecution('DEBUG','In SO data','name=='+name);
		
		var sow_recordOBJ = nlapiLoadRecord('salesorder', internalid);
		return sow_recordOBJ;
	}
}//Search Result

function validate_value(value)
{
	var return_value;

	if (value == '' || value == null || value == undefined || value.toString() == undefined || value == 'undefined' || value.toString() == 'undefined') {
		return_value = '';
	}
	else {
		return_value = value;
	}

	return return_value;
}


function validate_number(value)
{
		var return_value;

		if (value == '' || value == null || value == undefined || value.toString() == undefined || value == 'undefined' || value.toString() == 'undefined') {
			return_value = 0;
		}
		else {
			return_value = value;
		}

		return return_value;
	}
}

function _logValidation(value)
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN)
 {
  return true;
 }
 else
 {
  return false;
 }
}
// END FUNCTION =====================================================
