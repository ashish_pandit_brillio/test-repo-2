/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 31 Aug 2015 nitish.mishra
 * 
 */

function suitelet(request, response) {
	try {
		var method = request.getMethod();

		if (method == 'GET') {
			createGetForm(request);
		} else {
			postForm(request);
		}
	} catch (err) {
		nlapiLogExecution('error', 'suitelet', err);
		displayErrorForm(err, "Quick Time Approval");
	}
}

function createGetForm(request) {
	try {
		var form = nlapiCreateForm('Quick Time Approval');
		form.setScript('customscript_cs_sut_ts_quick_approval');

		// add the timesheet sublist
		var timesheetSublist = form.addSubList('custpage_timesheets', 'list',
		        'Pending Timesheets');

		timesheetSublist.addMarkAllButtons();
		timesheetSublist.addRefreshButton();

		timesheetSublist.addField('internalid', 'integer', 'Internal Id')
		        .setDisplayType('hidden');
		timesheetSublist.addField('apply', 'checkbox', 'Apply');
		timesheetSublist.addField('view', 'text', 'View');
		timesheetSublist.addField('employee', 'text', 'Employee');
		timesheetSublist.addField('startdate', 'date', 'Start Date');
		timesheetSublist.addField('enddate', 'date', 'End Date');
		timesheetSublist.addField('sthours', 'text', 'ST');
		timesheetSublist.addField('othours', 'text', 'OT');
		timesheetSublist.addField('leave', 'text', 'Leave');
		timesheetSublist.addField('holiday', 'text', 'Holiday');
		timesheetSublist.addField('totalhours', 'text', 'Total');

		// set the line item value
		timesheetSublist
		        .setLineItemValues(getPendingTimesheets(nlapiGetUser()));

		form.addButton('custpage_btn_approve', 'Approve', 'approveTimeSheet');
		form.addButton('custpage_btn_reject', 'Reject', 'rejectTimeSheet');
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('Error', 'createGetForm', err);
		throw err;
	}
}

function getPendingTimesheets(timeApprover) {
	try {

               if(timeApprover == 9673) {
                     timeApprover = 1605;
                }

		var filters = [
		        [ 'custrecord_qap_approved', 'is', 'F' ],
		        'and',
		        [ 'custrecord_qap_rejected', 'is', 'F' ],
		        'and',
		        [ 'custrecord_qap_ts_updated', 'is', 'F' ],
		        'and',
		        [ 'custrecord_qap_employee.timeapprover', 'anyof', timeApprover ],
		        'and', [ 'isinactive', 'is', 'F' ] ];

		var columns = [ new nlobjSearchColumn('custrecord_qap_employee'),
		        new nlobjSearchColumn('custrecord_qap_week_start_date'),
		        new nlobjSearchColumn('custrecord_qap_week_end_date'),
		        new nlobjSearchColumn('custrecord_qap_timesheet'),
		        new nlobjSearchColumn('custrecord_qap_st_hours'),
		        new nlobjSearchColumn('custrecord_qap_ot_hours'),
		        new nlobjSearchColumn('custrecord_qap_leave_hours'),
		        new nlobjSearchColumn('custrecord_qap_holiday_hours'),
		        new nlobjSearchColumn('custrecord_qap_total_hours') ];

		var pendingTimesheetSearch = searchRecord(
		        'customrecord_ts_quick_approval_pool', null, filters, columns);

		var sublistData = [];

		for (var i = 0; i < pendingTimesheetSearch.length; i++) {
			var timesheet = pendingTimesheetSearch[i];
			sublistData
			        .push({
			            view : "<a target='_blank' href='/app/accounting/transactions/timesheet.nl?id="
			                    + timesheet
			                            .getValue('custrecord_qap_timesheet')
			                    + "'>view</a>",
			            timesheet : timesheet
			                    .getValue('custrecord_qap_timesheet'),
			            internalid : timesheet.getId(),
			            employee : timesheet.getText('custrecord_qap_employee'),
			            startdate : timesheet
			                    .getValue('custrecord_qap_week_start_date'),
			            enddate : timesheet
			                    .getValue('custrecord_qap_week_end_date'),
			            leave : timesheet
			                    .getValue('custrecord_qap_leave_hours'),
			            holiday : timesheet
			                    .getValue('custrecord_qap_holiday_hours'),
			            sthours : timesheet.getValue('custrecord_qap_st_hours'),
			            othours : timesheet.getValue('custrecord_qap_ot_hours'),
			            totalhours : timesheet
			                    .getValue('custrecord_qap_total_hours')
			        });

			if (i == 199) {
				break;
			}
		}

		return sublistData;
	} catch (err) {
		nlapiLogExecution('error', 'getPendingTimesheets', err);
		throw err;
	}
}

function postForm(request) {
	nlapiLogExecution('debug', 'start', new Date());
	var result = {
	    Status : false,
	    Message : ''
	};

	try {
		// nlapiLogExecution('debug', 'request', request
		// .getParameter('requesttype'));
		// nlapiLogExecution('debug', 'request',
		// request.getParameter('records'));

		var requestType = request.getParameter('requesttype');
		var records = request.getParameter('records');
		var statusField = "";

		if (requestType == 'approve') {
			statusField = 'custrecord_qap_approved';
		} else if (requestType == 'reject') {
			statusField = 'custrecord_qap_rejected';
		} else {
			result.Message = "Bad Request Type";
		}

		if (records && statusField) {
			var arrRecords = records.split(",");
			// nlapiLogExecution('debug', 'arrRecords', typeof arrRecords);
			// nlapiLogExecution('debug', 'arrRecords', arrRecords.length);
			var len = arrRecords.length;

			for (var i = 0; i < len; i++) {
				nlapiSubmitField('customrecord_ts_quick_approval_pool',
				        arrRecords[i], statusField, 'T');
			}

			result.Status = true;
			result.Message = "Timesheet Submitted Sucessfully";
		} else {
			result.Message = "No Timesheets Applied";
		}
	} catch (err) {
		nlapiLogExecution('error', 'postForm', err);
		result.Message = err.getDetails();
	}

	nlapiLogExecution('debug', 'end', new Date());
	response.write(JSON.stringify(result));
}