function beforeLoad_timesheet_restriction(type)
{
	try
	{  
		var counter ='';
		var user = nlapiGetUser();
		var currentContext = nlapiGetContext();
		var role = nlapiGetRole();
     
		//Execute the logic only when creating a sales order with the browser UI
		if(type == 'create'){
		if ( currentContext.getExecutionContext() == 'userinterface' )
		{
			var employee_search_fil=new Array();
			employee_search_fil[0] = new nlobjSearchFilter('custrecord_restricting_employee',null,'anyof',user);
          employee_search_fil[1] = new nlobjSearchFilter('isinactive',null,'is','F');
			var employee_search_col = new Array();
			employee_search_col[0] = new nlobjSearchColumn('custrecord_restricting_employee');
			var employee_restricted_rec = nlapiSearchRecord('customrecord_emp_timesheet_restriction',null,employee_search_fil,employee_search_col);
			var data_list = [];
			if(employee_restricted_rec){
				for(var list_index=0;list_index < employee_restricted_rec.length;list_index++ )
				{
					var employee_restricted_list= employee_restricted_rec[list_index].getValue('custrecord_restricting_employee');
					data_list.push(parseInt(employee_restricted_list));
				}
			}
			
			if(data_list.indexOf(parseInt(user))>=0 && role != 18)
			{
				nlapiLogExecution('ERROR','ERROR MESSAGE :- ',user);
				throw "Timesheets are blocked in Netsuite for this ID. Please Access OnTheGo to submit Timesheet!!";
				return false;
			}	
				else
				{
					nlapiLogExecution('ERROR','data_list :- ',data_list);
					nlapiLogExecution('ERROR','user :- ',user);
				}
		}
      }
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE :- ',err);
		throw err;
	}
}