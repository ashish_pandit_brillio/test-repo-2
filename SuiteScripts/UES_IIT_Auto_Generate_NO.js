// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: UES_IIT_Auto_Generate_NO.js
	Author: Chaitali Gaikwad
	Company: INSPIRRIA Cloudtech Pvt. Ltd.
	Date: 23/3/2017
	Description: To auto generate invoice number for transaction.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmitRecord(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmit_Auto_generateNO(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES

	var SeqNo = 0000;

	//  BEFORE SUBMIT CODE BODY
	if(type == 'create')
	{
		var i_recId = nlapiGetRecordId();
		var s_recType = nlapiGetRecordType();

		nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'i_recId = '+i_recId);
		nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 's_recType = '+s_recType);

		var i_Flagcheck = 0;
		var Flag = 0;
		var a_subisidiary = new Array();
		var s_pass_code;
		var i_Flag_Status = 0;
		var gstApplyDate;
		var context = nlapiGetContext();
	    var dateFormatPref = context.getPreference('dateformat');
	    nlapiLogExecution('DEBUG', 'beforeLoadRecord_ShowImportButton', 'dateFormatPref ->'+ dateFormatPref);
		
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		if (i_AitGlobalRecId != 0) 
		{
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			var i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
			
			var i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			s_pass_code = o_AitGloRec.getFieldValue('custrecord_encpwdstring');
			nlapiLogExecution('DEBUG', 'Bill ', "s_pass_code->" + s_pass_code);
			
			gstApplyDate = o_AitGloRec.getFieldValue('custrecord_ait_gst_apply_date');
			gstApplyDate = nlapiStringToDate(gstApplyDate, dateFormatPref);
			nlapiLogExecution('DEBUG', 'Bill ', "gstApplyDate->" + gstApplyDate);
			
			
		}// end if(i_AitGlobalRecId != 0 )
		
		var currentDate = nlapiGetFieldValue('trandate');
		currentDate = nlapiStringToDate(currentDate, dateFormatPref);
		nlapiLogExecution('DEBUG', 'create_IITSaleInvoiceRec_AfterSubmit', 'currentDate = '+currentDate);
		
		var i_subsidiary = nlapiGetFieldValue('subsidiary');
		
		Flag = isindia_subsidiary(a_subisidiary, s_pass_code, i_subsidiary);
		nlapiLogExecution('DEBUG', 'Bill ', "Flag = " + Flag);
		
		if(Flag == 1 && gstApplyDate <= currentDate)
		{
			
			
			//========Load record========
		//	var o_rec_obj = nlapiLoadRecord(s_recType,i_recId);
	
		//	var date = o_rec_obj.getFieldValue('trandate');
			var date = nlapiGetFieldValue('trandate');
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'date = '+date);
			var strtodate = nlapiStringToDate(date);
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'strtodate = '+strtodate);
			var year = strtodate.getFullYear();
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'strtodate = '+strtodate);
		//	var trType = o_rec_obj.getFieldValue('custbody_iit_transactiontype');
			var trType = nlapiGetFieldValue('custbody_iit_transactiontype');
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'trType = '+trType);
		//	var location = o_rec_obj.getFieldValue('location');
			var location = nlapiGetFieldValue('location');
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'location = '+location);
	
			var GSTIN_fromLocation = '';
			if(_logValidation(location))
			{
				GSTIN_fromLocation = nlapiLookupField('location', location, 'custrecord_iit_location_gstn_uid', false);
				nlapiLogExecution('DEBUG', 'create_IITSaleInvoiceRec_AfterSubmit', 'GSTIN_fromLocation = '+GSTIN_fromLocation);
			}	
			
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'date = '+date);
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'year = '+year);
	
			//================ Code to get Financial year =================================
	
			// Filter record for date is within financial year
			var Filters1 = new Array();
			Filters1.push(new nlobjSearchFilter('custrecord_fy_start_date', null, 'onorbefore', date));
			Filters1.push(new nlobjSearchFilter('custrecord_fy_end_date', null, 'onorafter', date));
	
			var Column1 = new Array();
			Column1.push(new nlobjSearchColumn('name'));
			Column1.push(new nlobjSearchColumn('internalid'));
			
			var searchFY= nlapiSearchRecord('customrecord_financial_year',null, Filters1, Column1);
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'searchFY = '+searchFY);
	
			var i_FY_id = ' ';
			if (searchFY != null)
			{
				FY = searchFY[0].getValue('name');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'FY = '+FY);
				
				i_FY_id = searchFY[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'Financial Year Internal ID', 'i_FY_id = '+i_FY_id);
			}
	
	
			//================ END : Code to get Financial year =================================
	
			
			//==================================================================================
			//Begin:Get the detail from custom record
			 var Filters = new Array();
			 var Filters_gstin = new Array();
			//============= Filters for Location Wise Record ============== 
			if(s_recType == 'transferorder')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 48)); //48 - Transfer Order
			}
			if(s_recType == 'invoice')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 7)); //7 - Invoice
			}
			if(s_recType == 'cashsale')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 5)); //5 - Cash Sale
			}
			if(s_recType == 'creditmemo')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 10)); //10 - Credit Memo
			}
			if(s_recType == 'vendorcredit')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 20)); //10 - Vendor Credit
			}
			if(s_recType == 'check')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 3)); //10 - Check
			}
			if(s_recType == 'customerpayment')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 9)); //10 - Customer Payment
			}
			
			Filters.push(new nlobjSearchFilter('custrecord_iit_finyear', null, 'is', i_FY_id)); //FY
			
			if(_logValidation(trType) && s_recType != 'creditmemo')
			{
				Filters.push(new nlobjSearchFilter('custrecord_iit_trtype', null, 'is', trType));
				
			}
			
			Filters.push(new nlobjSearchFilter('custrecord_iit_trlocation', null, 'is', location));
	
			//============= Filters for GSTIN Wise Record ============== 
			if(s_recType == 'transferorder')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 48)); //48 - Transfer Order
			}
			if(s_recType == 'invoice')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 7)); //7 - Invoice
			}
			if(s_recType == 'cashsale')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 5)); //5 - Cash Sale
			}
			if(s_recType == 'creditmemo')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 10)); //10 - Credit Memo
			}
			if(s_recType == 'vendorcredit')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 20)); //10 - Vendor Credit
			}
			if(s_recType == 'check')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 3)); //10 - Check
			}
			if(s_recType == 'customerpayment')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_record_type', null, 'is', 9)); //10 - Customer Payment
			}
			Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_finyear', null, 'is', i_FY_id)); //FY
			if(_logValidation(trType) && s_recType != 'creditmemo')
			{
				Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_trtype', null, 'is', trType));
			}
			Filters_gstin.push(new nlobjSearchFilter('custrecord_iit_gstin_auto_num', null, 'is', GSTIN_fromLocation));
	
			
			var Column = new Array();
			Column.push(new nlobjSearchColumn('custrecord_iit_auto_generated_no'));
			Column.push(new nlobjSearchColumn('custrecord_iit_prefix'));
			Column.push(new nlobjSearchColumn('custrecord_iit_suffix'));
			Column.push(new nlobjSearchColumn('custrecord_iit_locationprefix'));
			Column.push(new nlobjSearchColumn('custrecord_iit_gstin_prefix'));
			Column.push(new nlobjSearchColumn('custrecord_iit_minimum_digit'));
			Column.push(new nlobjSearchColumn('custrecord_iit_financial_year_prefix')); // For Financial Year Prefix 
			Column.push(new nlobjSearchColumn('internalid'));
	
	
			var searchSeqNo= nlapiSearchRecord('customrecord_iit_auto_inv_no',null, Filters, Column);
			
			if(_nullValidation(searchSeqNo))
			{
				var searchSeqNo= nlapiSearchRecord('customrecord_iit_auto_inv_no',null, Filters_gstin, Column);	
			}
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'searchSeqNo = '+searchSeqNo);
			if(_logValidation(searchSeqNo))
			{
				nlapiLogExecution('DEBUG', 'Length_Auto_generateNO_Records', 'searchSeqNo Length = '+searchSeqNo.length);
				
			}	
			var SeqNo = 1;
			var preFix = '';
			var locationPrefix = '';
			var FY_Prefix = '';
			var LocOrGSTIN_Prefix = '';
			if (searchSeqNo != null)
			{
				SeqNo = searchSeqNo[0].getValue('custrecord_iit_auto_generated_no');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'SeqNo = '+SeqNo);
				SeqNo = parseInt(SeqNo);
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'SeqNo = '+SeqNo);
				
				preFix = searchSeqNo[0].getValue('custrecord_iit_prefix');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'preFix = '+preFix);
				
				locationPrefix = searchSeqNo[0].getValue('custrecord_iit_locationprefix');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'locationPrefix = '+locationPrefix);
				
				FY_Prefix = searchSeqNo[0].getValue('custrecord_iit_financial_year_prefix');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'FY_Prefix = '+FY_Prefix);
				
				var internalid = searchSeqNo[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'internalid = '+internalid);
				
				var Min_Digit = searchSeqNo[0].getValue('custrecord_iit_minimum_digit');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'Min_Digit = '+Min_Digit);
				
				var GSTIN_Prefix = searchSeqNo[0].getValue('custrecord_iit_gstin_prefix');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'GSTIN_Prefix = '+GSTIN_Prefix);
	
//			}
	/*		else
			{
				preFix='IND'
				var o_custom_rec=nlapiCreateRecord('customrecord_iit_auto_inv_no');
				o_custom_rec.setFieldValue('custrecord_iit_minimum_digit',2);
				o_custom_rec.setFieldValue('custrecord_iit_record_type',7);
				o_custom_rec.setFieldValue('custrecord_iit_prefix','IND');
				o_custom_rec.setFieldValue('custrecord_iit_auto_generated_no',0);
				o_custom_rec.setFieldValue('custrecord_iit_trtype',trType);
				o_custom_rec.setFieldValue('custrecord_iit_finyear',FY);
				o_custom_rec.setFieldValue('custrecord_iit_trlocation', location);
				o_custom_rec.setFieldValue('custrecord_iit_locationprefix',' ')
				var internalid=nlapiSubmitRecord(o_custom_rec, true, true);
			//	var Min_Digit = nlapiLookupField('customrecord_iit_auto_inv_no', internalid, 'custrecord_iit_minimum_digit');
				var Min_Digit = parseInt(2);
			}
	*/
	
			if(SeqNo > 999999)
			{
				throw('Note for User : You can not generate number more than 999999');
				return false;
			}
			else
			{
				if(_logValidation(GSTIN_Prefix))
				{
					LocOrGSTIN_Prefix = GSTIN_Prefix;
				}
				else if(_logValidation(locationPrefix))
				{
					LocOrGSTIN_Prefix = locationPrefix;
				}
				var appendZero = SeqNo;
				//	 Min_Digit = --Min_Digit;
					 nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'Min_Digit = '+Min_Digit);
						
					 appendZero = AppendZero(SeqNo,appendZero, Min_Digit);
					 nlapiLogExecution('DEBUG', 'Get Value from function ', 'appendZero = '+appendZero);
						
			//		var AutoNO = FY+"/"+preFix+"/"+locationPrefix+"/"+appendZero;
			//		var AutoNO = preFix+"/"+LocOrGSTIN_Prefix+"/"+FY_Prefix+"/"+appendZero; //FY_Prefix
					var AutoNO = preFix+LocOrGSTIN_Prefix+FY_Prefix+appendZero; //FY_Prefix
					nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'AutoNO = '+AutoNO);
					
					nlapiLogExecution('DEBUG', 'Length for', 'AutoNO = '+AutoNO.length);
					
				//	AutoNO = 'FY 2017-18/IND/NAG/069';
					var checkDuplicats = SearchRecforSameidformed(AutoNO);
				    nlapiLogExecution('DEBUG', 'aftersubmit outside', 'checkDuplicats =' + checkDuplicats);
			        
				    if(checkDuplicats == 'T')
				    {
				    	appendZero = SeqNo;
				    	
				    	nlapiLogExecution('DEBUG', 'before adding 1', 'appendZero = '+appendZero);
				 	
				    	appendZero = parseInt(appendZero) + parseInt(1);
				    	nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
						
				    	var New_appendZero = AppendZero(SeqNo,appendZero, Min_Digit);
				    	
				    //	AutoNO = preFix+"/"+LocOrGSTIN_Prefix+"/"+FY_Prefix+"/"+New_appendZero;
				    	AutoNO = preFix+LocOrGSTIN_Prefix+FY_Prefix+New_appendZero;
				    	nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'AutoNO = '+AutoNO);
						
				   // 	o_rec_obj.setFieldValue('tranid',AutoNO);
                   //     nlapiSetFieldValue('generatetranidonsave', 'F');
				    	nlapiSetFieldValue('tranid', AutoNO);
				   //	var invId=nlapiSubmitRecord(o_rec_obj,true,true);
				   //	nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'invId = '+invId);
						
						SeqNo = parseInt(SeqNo) + parseInt(2); // Since we are incrementing auto number by 1 so this 1+1(for next initial no)
				    }
				    else
				    {
                   //     nlapiSetFieldValue('generatetranidonsave', 'F');
				    	nlapiSetFieldValue('tranid', AutoNO);
				    	
				    	/*
				    	o_rec_obj.setFieldValue('tranid',AutoNO);
						var invId=nlapiSubmitRecord(o_rec_obj,true,true);
						nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'invId = '+invId);
						*/
						SeqNo++;
				    }	
					
			
					var o_custom_rec=nlapiLoadRecord('customrecord_iit_auto_inv_no',internalid);// load custom record
					nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'o_custom_rec = '+o_custom_rec);
				//	SeqNo++;
					SeqNo = SeqNo.toString();
					nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'SeqNo = '+SeqNo);
			
					o_custom_rec.setFieldValue('custrecord_iit_auto_generated_no',SeqNo);
			
					var submit_customRec=nlapiSubmitRecord(o_custom_rec, true, true);
			
					nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'submit_customRec = '+submit_customRec);
					//End:Get the detail from custom record
					//==================================================================================
			}	
			 
		}
		}
	}
		return true;

	

}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_Auto_generateNO(type)
{}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
{
function AppendZero(SeqNo,appendZero, Min_Digit)
{
	nlapiLogExecution('DEBUG', 'Function - AppendZero', 'SeqNo = '+SeqNo);

	if(SeqNo <= 9 && Min_Digit <7)
	{
		for(var i = 0; i < Min_Digit-1 ; i++)
		{
			appendZero = '0'+appendZero;
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
		}	
		
	}
	else if(SeqNo <= 99 && Min_Digit <7)
	{
		for(var i = 0; i < Min_Digit-2 ; i++)
		{
			 appendZero = '0'+appendZero;
			 nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
		}	
		
	}
	else if(SeqNo <= 999 && Min_Digit <7)
	{
		for(var i = 0; i < Min_Digit-3 ; i++)
		{
			 appendZero = '0'+appendZero;
			 nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
		}	
		
	}
	else if(SeqNo <= 9999 && Min_Digit <7)
	{
		for(var i = 0; i < Min_Digit-4 ; i++)
		{
			 appendZero = '0'+appendZero;
			 nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
		}	
		
	}
	else if(SeqNo <= 99999 && Min_Digit <7)
	{
		for(var i = 0; i < Min_Digit-5 ; i++)
		{
			 appendZero = '0'+appendZero;
			 nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
		}	
		
	}

	else if(SeqNo <= 999999 && Min_Digit <7)
	{
		for(var i = 0; i < Min_Digit-6 ; i++)
		{
			 appendZero = '0'+appendZero;
			 nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'appendZero = '+appendZero);
		}	
		
	}
	return appendZero;
}

function SearchRecforSameidformed(s_noString){
    var Filterspo = new Array();
    var ColumnsPO = new Array();
    var Internalid = null;
    nlapiLogExecution('DEBUG', 'beforeSubmitRecord', 's_noString= ' + s_noString);
	
    Filterspo[0] = new nlobjSearchFilter('tranid', null, 'is', s_noString.toString());
	////Filterspo[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
    ColumnsPO[0] = new nlobjSearchColumn('internalid');
    var searchresults = nlapiSearchRecord('transaction',null, Filterspo, ColumnsPO);
    
    if (searchresults != null && searchresults != '' && searchresults != 'undefined') {
        //nlapiLogExecution('DEBUG', 'beforeSubmitRecord', 'length= ' + searchresults.length);
        
        for (var i = 0; i < searchresults.length; i++) {
            Internalid = searchresults[i].getValue('internalid');
            nlapiLogExecution('DEBUG', 'beforeSubmitRecord', 'id= ' + Internalid);
            if (parseInt(i) > 0) {
                Internalid += Internalid;
            }
        }
        return 'T'; ////if duplicate return true
    }
    else {
        return 'F';
    }
}

function SearchGlobalParameter()	
{
    var a_filters = new Array();
    var a_column = new Array();
    var i_globalRecId = 0;
    
    a_column.push(new nlobjSearchColumn('internalid'));
    
    var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter', null, null, a_column);
    
    if (s_serchResult != null && s_serchResult != undefined && s_serchResult != '') 
	{
        for (var i = 0; i < s_serchResult.length; i++) 
		{
            i_globalRecId = s_serchResult[0].getValue('internalid');
            nlapiLogExecution('DEBUG', 'Bill ', "i_globalRecId" + i_globalRecId);
            
        }
        
    }
    
    
    return i_globalRecId;
}
function _nullValidation(value){
	 if (value == null || value == undefined || value == '')
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
}
function _logValidation(value)
{
if(value != null && value != '' && value != undefined && value != 'undefined' && value != 'NaN' && value != ' ')
{
	return true;
}
else
{
	return false;
}
}
}
// END FUNCTION =====================================================