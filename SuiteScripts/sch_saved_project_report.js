/**
 * @author Jayesh
 */

function suiteletFunction_FP_RevRec_report(request,response)
{
	try
	{
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var a_project_list = new Array();
		var a_project_search_results = '';
		var email=nlapiLookupField('employee',i_user_logegdIn_id,'email');
		nlapiLogExecution('debug','mail',email);
		if(i_user_logegdIn_id == 5748) //Added user Deepak to check the functionality 
		   i_user_logegdIn_id =7905;
		// design form which will be displayed to user
		//var o_form_obj = nlapiCreateForm("FP Project Details Page");
		
		// call client script to open hyper link in same window instead of new tab
		//o_form_obj.setScript('customscript_cli_fp_revrec_share_samewin');
	

								//['custentity_fp_rev_rec_type', 'noneof', '@NONE@']
		var a_columns_proj_srch = new Array();
		a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
		a_columns_proj_srch[1] = new nlobjSearchColumn('customer');
		a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_columns_proj_srch[3] = new nlobjSearchColumn('custentity_ytd_rev_recognized');
		a_columns_proj_srch[4] = new nlobjSearchColumn('custentity_projectmanager');
		a_columns_proj_srch[5] = new nlobjSearchColumn('custentity_region');
		a_columns_proj_srch[6] = new nlobjSearchColumn('custentity_project_currency');
		a_columns_proj_srch[7] = new nlobjSearchColumn('entityid');
		a_columns_proj_srch[8] = new nlobjSearchColumn('altname');
		/*if(parseInt(i_user_logegdIn_id) == parseInt(41571)|| parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819)
		|| parseInt(i_user_logegdIn_id) == parseInt(90037) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(2301)||
		parseInt(i_user_logegdIn_id) == parseInt(30484) || parseInt(i_user_logegdIn_id) == parseInt(19967)|| parseInt(i_user_logegdIn_id) == parseInt(7981) || parseInt(i_user_logegdIn_id) == parseInt(19967)
		|| parseInt(i_user_logegdIn_id) == parseInt(9109)|| parseInt(i_user_logegdIn_id) == parseInt(94862) ) */
		{
			var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', [1,2,4]],'and',
								['enddate','onorafter','startofthismonth']];
								
			a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		}
		
		 
		if(a_project_search_results)
		{
			var a_get_logged_in_user_exsiting_revenue_cap = '';
			var a_rev_filter = ['isinactive','is','F'];
			
			var a_columns_existing_cap_srch = new Array();
			a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_revenue_share_project');
			a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
			a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);
			a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
			//if(parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(90037) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(2301)|| parseInt(i_user_logegdIn_id) == parseInt(30484)||parseInt(i_user_logegdIn_id) == parseInt(19967) || parseInt(i_user_logegdIn_id) == parseInt(94862))
			 a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_rev_filter, a_columns_existing_cap_srch);
			var a_existig_cap_projects = new Array();
			var a_existig_cap_projects_other = new Array();
			var a_existing_proj_cap_details = new Array();
			var i_index_existing_pro = 0;
			//else
			// a_get_logged_in_user_exsiting_revenue_cap = searchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_columns_existing_cap_srch);
			if(a_get_logged_in_user_exsiting_revenue_cap)
			{
				
				for(var i_existing_cap = 0; i_existing_cap < a_get_logged_in_user_exsiting_revenue_cap.length; i_existing_cap++)
				{
					if(a_existig_cap_projects.indexOf(a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project')) < 0)
					{
						//nlapiLogExecution('audit','existing proj index:- ',a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						
						a_existig_cap_projects.push(a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						a_existing_proj_cap_details[i_index_existing_pro] = {
																				'proj_intenal_id': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'),
																				'pro_revenue_share': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_total'),
																				'existing_rcrd_id': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getId(),
																				'existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_approval_status'),
																				's_existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getText('custrecord_revenue_share_approval_status'),
																				'type':'1'
																			};
						i_index_existing_pro++;
					}					
				}	
			}
			//
			{
				var a_get_logged_in_user_exsiting_revenue_cap_other = '';
				var a_revenue_cap_filter = [];
				var count=0;
				var a_existig_cap_projects_other=new Array();
				var a_columns_existing_cap_srch = new Array();
				var a_existing_proj_cap_details_other=new Array();
				a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
				//a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_revenue_share_total');
				a_columns_existing_cap_srch[1] = new nlobjSearchColumn('created').setSort(true);
				a_columns_existing_cap_srch[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
				if(parseInt(i_user_logegdIn_id) == parseInt(41571) || parseInt(i_user_logegdIn_id) == parseInt(7905)|| parseInt(i_user_logegdIn_id) == parseInt(35819) || parseInt(i_user_logegdIn_id) == parseInt(90037) || parseInt(i_user_logegdIn_id) == parseInt(72055) || parseInt(i_user_logegdIn_id) == parseInt(2301)||parseInt(i_user_logegdIn_id) == parseInt(30484) )
				a_get_logged_in_user_exsiting_revenue_cap_other = searchRecord('customrecord_fp_rev_rec_others_parent', null, null, a_columns_existing_cap_srch);
				if(a_get_logged_in_user_exsiting_revenue_cap_other)
				{
					for(var i_existing_cap = 0; i_existing_cap < a_get_logged_in_user_exsiting_revenue_cap_other.length; i_existing_cap++)
					{
						if((a_existig_cap_projects_other.indexOf(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec')) < 0) &&
						((a_existig_cap_projects.indexOf(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec')) < 0)))
						{
						//nlapiLogExecution('audit','existing proj index:- ',a_get_logged_in_user_exsiting_revenue_cap[i_existing_cap].getValue('custrecord_revenue_share_project'));
						
							a_existig_cap_projects_other.push(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'));
							a_existig_cap_projects.push(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'));
							
							
							a_existing_proj_cap_details[i_index_existing_pro] = {
																				'proj_intenal_id': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'),
																				//'pro_revenue_share': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_revenue_share_total'),
																				'existing_rcrd_id': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getId(),
																				'existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_revenue_other_approval_status'),
																				's_existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getText('custrecord_revenue_other_approval_status'),
																				'type':'4'
																			};
							i_index_existing_pro++;
						}	
						else
						{
							if((a_existig_cap_projects_other.indexOf(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec')) < 0) &&
							((a_existig_cap_projects.indexOf(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec')) > 0)))
							{
								var ind=a_existig_cap_projects.indexOf(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'));
								a_existig_cap_projects_other.push(a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'));
									a_existing_proj_cap_details[ind] = {
																				'proj_intenal_id': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_fp_rev_rec_others_projec'),
																				//'pro_revenue_share': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_revenue_share_total'),
																				'existing_rcrd_id': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getId(),
																				'existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getValue('custrecord_revenue_other_approval_status'),
																				's_existing_rcrd_status': a_get_logged_in_user_exsiting_revenue_cap_other[i_existing_cap].getText('custrecord_revenue_other_approval_status'),
																				'type':'4'
																			};
							}
								
							count++;
						}
						
					}	
				}
				
			}
			//
			
			var a_JSON = {};
			var s_create_update_mode = '';
						
			for(var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var i_project_internal_id = a_project_search_results[i_pro_index].getId();
				var f_project_existing_revenue = 0;
				var i_existing_revenue_share_rcrd_id = 0;
				var b_is_proj_value_chngd_flag = 'F';
				var s_existing_revenue_share_status = '';
				var type='';
				if((a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId()) >= 0))
				{
					//nlapiLogExecution('audit','a_existig_cap_projects '+a_project_search_results[i_pro_index].getId,a_existig_cap_projects.length);
						s_create_update_mode = 'Update';
					
						s_revenue_share_status = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].s_existing_rcrd_status;
						if(!s_revenue_share_status)
						{
							s_revenue_share_status = 'Saved';
						}
					
						f_project_existing_revenue = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].pro_revenue_share;
						i_existing_revenue_share_rcrd_id = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_id;
						s_existing_revenue_share_status = a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].existing_rcrd_status;
						type=a_existing_proj_cap_details[a_existig_cap_projects.indexOf(a_project_search_results[i_pro_index].getId())].type;
				
				}
				else
				{
					s_create_update_mode = 'Create';
					s_revenue_share_status = 'Not Submitted';
				}
				
				var i_project_value_ytd = a_project_search_results[i_pro_index].getValue('custentity_ytd_rev_recognized');
				
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				
				var f_proj_total_value = a_project_search_results[i_pro_index].getValue('custentity_projectvalue');
				f_proj_total_value = parseFloat(f_proj_total_value) - parseFloat(i_project_value_ytd);
				f_proj_total_value = parseFloat(f_proj_total_value).toFixed(2);
				if(type == parseInt(1))
				{
					if((parseFloat(f_project_existing_revenue) != parseFloat(f_proj_total_value)) && s_create_update_mode != 'Create')
					{
						s_existing_revenue_share_status = '';
						s_revenue_share_status = 'Saved';
						b_is_proj_value_chngd_flag = 'T';
					//nlapiSubmitField('customrecord_revenue_share',i_existing_revenue_share_rcrd_id,'custrecord_revenue_share_approval_status','');
					//nlapiSubmitField('customrecord_revenue_share',i_existing_revenue_share_rcrd_id,'custrecord_sow_value_change_status','Saved');
					}
				}
				var i_project_sow_value = a_project_search_results[i_pro_index].getValue('custentity_projectvalue');
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				
				if(s_revenue_share_status != 'Approved')
				{
					//var response = nlapiRequestURL('https://debugger.netsuite.com/app/site/hosting/scriptlet.nl?script=1608&deploy=1' + '&sowid='+a_project_search_results[i_pro_index].getId(), null, null);
					//var sow_id = response.getBody();
					var sow_id= searchSOW(a_project_search_results[i_pro_index].getId());
					a_JSON = {
							project_cutomer: a_project_search_results[i_pro_index].getText('customer'),
							project_id: a_project_search_results[i_pro_index].getValue('entityid'),
							project_name: a_project_search_results[i_pro_index].getValue('altname'),
							project_value: i_project_sow_value,
							revenue_share_status: s_revenue_share_status,
							project_manager:a_project_search_results[i_pro_index].getText('custentity_projectmanager'),
							region:a_project_search_results[i_pro_index].getText('custentity_region'),
							currency:a_project_search_results[i_pro_index].getText('custentity_project_currency'),
							sow_id:sow_id,
							//project_existing_value: f_project_existing_revenue,
							project_rev_rec_type: a_project_search_results[i_pro_index].getText('custentity_fp_rev_rec_type'),
						};
				
					a_project_list.push(a_JSON);
				}
			}
			
			//create sublist for project
			/*var f_form_sublist = o_form_obj.addSubList('project_sublist','list','Project List');
			f_form_sublist.addField('project_cutomer','select','Customer','customer').setDisplayType('inline');
			f_form_sublist.addField('project_id','select','Project','job').setDisplayType('inline');
			f_form_sublist.addField('project_value','currency','Project Value');
			f_form_sublist.addField('revenue_share_status','text','Project Setup Status');
			//f_form_sublist.addField('project_existing_value','currency','Captured Revenue Share');
			f_form_sublist.addField('project_rev_rec_type','select','Rev Rec Type','customlist_fp_rev_rec_project_type').setDisplayType('inline');
			f_form_sublist.addField('suitelet_url', 'url','').setLinkText('Update');
			f_form_sublist.setLineItemValues(a_project_list);*/
			down_excel_function(a_project_list,email);
			// o_form_obj.setScript('customscript_export_script');
			//o_form_obj.addButton('custpage_button','Schedule Script', "down_excel_function(a_project_list);");
			//response.writePage( o_form_obj );
					
			
		}
		/*else
		{
			var s_result_not_found = "<table>";
			s_result_not_found += "<tr><td>";
			s_result_not_found += "NO RESULTS FOUND";
			s_result_not_found += "</td></tr>";
			s_result_not_found += "</table>";
			
			var f_no_rslt_found = o_form_obj.addField('rcrd_nt_found', 'inlinehtml', 'No Record Found');
			f_no_rslt_found.setDefaultValue(s_result_not_found);
		}*/
		
		//response.writePage(o_form_obj);
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','function suiteletFunction_FP_RevRec_Dashboard','ERROR MESSAGE :- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}
function down_excel_function(a_project_list,email)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"100%\">";
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Customer</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project</td>";
		strVar2 += "<td Width=\"20%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Revenue</td>";
		strVar2 += "<td Width=\"10%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Setup Status</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Rev Rec Type</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Project manager</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Region</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>Currency</td>";
		strVar2 += "<td Width=\"50%\" style='text-align:left;font-weight:bold;' font-size=\"10\" \>SowId</td>";
		strVar2 += "<\/tr>";
	
		if ((a_project_list)) //
		{
			for (var i = 0; i < a_project_list.length; i++) //
			{
				var s_proj_cust = a_project_list[i].project_cutomer;
				var s_project = a_project_list[i].project_id;
				var s_project_name = a_project_list[i].project_name;
				var f_revenue_amount = a_project_list[i].project_value;
				var project_manager = a_project_list[i].project_manager;
				var region = a_project_list[i].region;
				var currency = a_project_list[i].currency;
				var setup_status = a_project_list[i].revenue_share_status;
				var s_type =a_project_list[i].project_rev_rec_type;
				var sow_id=a_project_list[i].sow_id;
				//nlapiLogExecution('audit','s_proj_entityid:-'+s_proj_entityid,'s_proj_name:-'+s_proj_name);
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_proj_cust + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + s_project + " " + s_project_name + "</td>";
				strVar2 += "<td Width=\"20%\" style='text-align:left;' font-size=\"10\" \>" + f_revenue_amount + "</td>";
				strVar2 += "<td Width=\"10%\" style='text-align:left;' font-size=\"10\" \>" + setup_status + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + s_type + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + project_manager + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + region + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + currency + "</td>";
				strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" + sow_id + "</td>";
          		// strVar2 += "<td Width=\"50%\" style='text-align:left;' font-size=\"10\" \>" +  + "</td>";   
				//strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('FP Project Report.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		//response.setContentType('XMLDOC','FP Project Report.xls');
		//response.write( file.getValue() );	
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = 62082;		
		nlapiSendEmail(7905, email, 'FP Project Report',
			'Please find the attachement of FP project report', null,
			'sai.vannamareddy@brillio.com', a_emp_attachment, file, true, null,
			null);
		
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}
function searchSOW(pro_id){
	try{
		var sow_id = '';
		var filters = [];
		var s_account_customer = '"'+pro_id+'"';
		//nlapiLogExecution('DEBUG','Customer Filter ',s_account_customer);
		filters.push(new nlobjSearchFilter('internalid','job','anyof',pro_id));
		//if(pro_stDate)
		//filters.push(new nlobjSearchFilter('custbody_projectstartdate',null,'onorafter',pro_stDate));
	//	if(pro_endDate)
		//	filters.push(new nlobjSearchFilter('custbody_projectenddate',null,'onorbefore',pro_endDate));
		
		var cols = [];
		cols.push(new nlobjSearchColumn('internalid'));
		cols.push(new nlobjSearchColumn('tranid').setSort());
		
		
		var searchRecords = nlapiSearchRecord('salesorder',null,filters,cols);
		if(searchRecords)
			sow_id = searchRecords[0].getValue('tranid');
		//nlapiLogExecution('DEBUG','SOW Search ',sow_id);
		
		return sow_id;
	}
	catch(e){
		nlapiLogExecution('DEBUG','Process Error',e);
		throw e;
	}
}