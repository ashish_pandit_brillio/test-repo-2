/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 June 2017     Jayesh Dinde
 *
 */

function after_submit(type)
{
	try
	{
		var o_context = nlapiGetContext();
		nlapiLogExecution('audit','ExecutionContext:- '+o_context.getExecutionContext());
		if(o_context.getExecutionContext() == 'userinterface' || o_context.getExecutionContext() == 'userevent' || o_context.getExecutionContext() == 'suitelet')
		{
			if (nlapiGetRecordType() == 'invoice')
			{
				if(nlapiGetFieldValue('subsidiary') == 2)
				{
					var i_project_selected = nlapiGetFieldValue('job');
					var i_proj_billing_type = nlapiLookupField('job',i_project_selected,'jobbillingtype');
					if(i_proj_billing_type == 'TM')
					{
						var i_old_rcrd_approval_status = 1;
						var o_invoice_old_rcrd = nlapiGetOldRecord();
						if(o_invoice_old_rcrd)
							i_old_rcrd_approval_status = o_invoice_old_rcrd.getFieldValue('custbody_invoicestatus');
						
						var i_approval_status = nlapiGetFieldValue('custbody_invoicestatus');
						nlapiLogExecution('audit','i_old_rcrd_approval_status:- '+i_old_rcrd_approval_status,'i_approval_status:- '+i_approval_status);
						
						if(i_old_rcrd_approval_status != 1 && i_approval_status == 1)
						{
							nlapiLogExecution('audit','Invoice approved.....');
							nlapiSubmitField('invoice',nlapiGetRecordId(),'custbody_auto_vendor_bill_invoice','T');
							
							// commented because solution changed for bulk approval process
							/*var params=new Array();
							params['custscript_invoice_to_process'] = nlapiGetRecordId();
								
							var status = nlapiScheduleScript('customscript_sch_auto_vendor_bill_bllc',null,params);*/
						}
					}
				}
			}
			
			if(o_context.getExecutionContext() == 'userinterface')
			{
              ///////////////////////////////
              // Filter record for date is within financial year
               var tran_date = nlapiGetFieldValue('trandate');
			
			var Filters1 = new Array();
			Filters1.push(new nlobjSearchFilter('custrecord_fy_start_date', null, 'onorbefore', tran_date));
			Filters1.push(new nlobjSearchFilter('custrecord_fy_end_date', null, 'onorafter', tran_date));
	
			var Column1 = new Array();
			Column1.push(new nlobjSearchColumn('name'));
			Column1.push(new nlobjSearchColumn('internalid'));
			
			var searchFY= nlapiSearchRecord('customrecord_financial_year',null, Filters1, Column1);
			nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'searchFY = '+searchFY);
	
			var i_FY_id = ' ';
			if (searchFY != null)
			{
				FY = searchFY[0].getValue('name');
				nlapiLogExecution('DEBUG', 'afterSubmit_Auto_generateNO', 'FY = '+FY);
				
				i_FY_id = searchFY[0].getValue('internalid');
				nlapiLogExecution('DEBUG', 'Financial Year Internal ID', 'i_FY_id = '+i_FY_id);
			}
              	
              //////////////////////////////
				var i_invoice_location = nlapiGetFieldValue('location');
				var i_invoice_sub_type = nlapiGetFieldValue('custbody_iit_transactiontype');
				
				var s_invoice_id = nlapiGetFieldValue('tranid');
				
				if (nlapiGetRecordType() == 'invoice')
				{
					var filters_auto_number = new Array();
					filters_auto_number[0] = new nlobjSearchFilter('custrecord_iit_trlocation', null, 'is', parseInt(i_invoice_location));
					filters_auto_number[1] = new nlobjSearchFilter('custrecord_iit_trtype', null, 'is', parseInt(i_invoice_sub_type));
                  filters_auto_number[2] = new nlobjSearchFilter('custrecord_iit_finyear', null, 'is', i_FY_id);
					
					var column_auto_number = new Array();
					column_auto_number[0] = new nlobjSearchColumn('custrecord_iit_prefix');
					column_auto_number[1] = new nlobjSearchColumn('custrecord_iit_locationprefix');
					column_auto_number[2] = new nlobjSearchColumn('custrecord_iit_financial_year_prefix');
					column_auto_number[3] = new nlobjSearchColumn('custrecord_iit_minimum_digit');
					column_auto_number[4] = new nlobjSearchColumn('custrecord_iit_record_type');
					
					var a_auto_number = nlapiSearchRecord('customrecord_iit_auto_inv_no', null, filters_auto_number, column_auto_number);
					if (a_auto_number)
					{
						for(var i_index=0; i_index<a_auto_number.length; i_index++)
						{
							var s_transaction_type = a_auto_number[i_index].getText('custrecord_iit_record_type');
							s_transaction_type = s_transaction_type.toLowerCase();
							if(s_transaction_type == 'invoice')
							{
								var s_iit_prefix = a_auto_number[i_index].getValue('custrecord_iit_prefix');
								var s_loc_prefix = a_auto_number[i_index].getValue('custrecord_iit_locationprefix');
								var s_financial_yr = a_auto_number[i_index].getValue('custrecord_iit_financial_year_prefix');
								var i_minimum_digit = a_auto_number[i_index].getValue('custrecord_iit_minimum_digit');
								
								var i_iit_prefix_len = s_iit_prefix.toString().length;
								var i_loc_prefix_len = s_loc_prefix.toString().length;
								if (i_loc_prefix_len > 2)
								{
									s_loc_prefix = s_loc_prefix.toString().substr(0, 2);
								}
								
								var i_yr_len = s_financial_yr.toString().length;
								
								s_invoice_id = s_invoice_id.toString().substr((parseInt(s_invoice_id.length) - parseInt(i_minimum_digit)), s_invoice_id.length);
								
								s_invoice_id = s_iit_prefix + '/' + s_loc_prefix + '/' + s_financial_yr + '/' + s_invoice_id;
								
							}
						}
					}
				}
				else if(nlapiGetRecordType() == 'creditmemo')
				{
					var filters_auto_number = new Array();
					filters_auto_number[0] = new nlobjSearchFilter('custrecord_iit_trlocation', null, 'is', parseInt(i_invoice_location));
					
					var column_auto_number = new Array();
					column_auto_number[0] = new nlobjSearchColumn('custrecord_iit_prefix');
					column_auto_number[1] = new nlobjSearchColumn('custrecord_iit_locationprefix');
					column_auto_number[2] = new nlobjSearchColumn('custrecord_iit_financial_year_prefix');
					column_auto_number[3] = new nlobjSearchColumn('custrecord_iit_minimum_digit');
					column_auto_number[4] = new nlobjSearchColumn('custrecord_iit_record_type');
					
					var a_auto_number = nlapiSearchRecord('customrecord_iit_auto_inv_no', null, filters_auto_number, column_auto_number);
					if (a_auto_number)
					{
						for (var i_index = 0; i_index < a_auto_number.length; i_index++)
						{
							var s_transaction_type = a_auto_number[i_index].getText('custrecord_iit_record_type');
							s_transaction_type = s_transaction_type.toLowerCase();
							if (s_transaction_type == 'credit memo')
							{
								var s_iit_prefix = a_auto_number[i_index].getValue('custrecord_iit_prefix');
								var s_loc_prefix = a_auto_number[i_index].getValue('custrecord_iit_locationprefix');
								var s_financial_yr = a_auto_number[i_index].getValue('custrecord_iit_financial_year_prefix');
								var i_minimum_digit = a_auto_number[i_index].getValue('custrecord_iit_minimum_digit');
								
								var i_iit_prefix_len = s_iit_prefix.toString().length;
								var i_loc_prefix_len = s_loc_prefix.toString().length;
								if (i_loc_prefix_len > 2)
								{
									s_loc_prefix = s_loc_prefix.toString().substr(0, 2);
								}
								
								var i_yr_len = s_financial_yr.toString().length;
								
								s_invoice_id = s_invoice_id.toString().substr((parseInt(s_invoice_id.length) - parseInt(i_minimum_digit)), s_invoice_id.length);
								
								s_invoice_id = s_iit_prefix + '/' + s_loc_prefix + '/' + s_financial_yr + '/' + s_invoice_id;
								
							}
						}
					}
				}
				
              var o_inv_load = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
             nlapiLogExecution('audit', 'o_inv_load =  ' + o_inv_load);
              o_inv_load.setFieldValue('tranid',s_invoice_id);
               var o_inv_submit =  nlapiSubmitRecord(o_inv_load,true,true);
              nlapiLogExecution('audit', 'o_inv_submit ' + o_inv_submit);
		//		nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'tranid', s_invoice_id);
				nlapiLogExecution('audit', 'updated tranid:- ' + s_invoice_id);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','after_submit','ERROR MESSAGE:- '+err);
	}
}
