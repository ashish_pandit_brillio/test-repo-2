/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Sep 2019     Ravishanker       This script is use to auto apply the timesheet in the Invocie record.
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */

function createInvoiceFromTimesheets(type) {
	
	try {
		nlapiLogExecution('debug', 'SCHEDULED INVOICE','Execution Start Here: '+ 'type:'+type);
		
		var currentContext  = 	nlapiGetContext();
		var i_InvocieId		=	currentContext.getSetting('SCRIPT', 'custscript_invoiceid');
		var dt_billFrom		=	currentContext.getSetting('SCRIPT', 'custscript_billingfromdate');
		var dt_billTo		=	currentContext.getSetting('SCRIPT', 'custscript_billingtodate');
		var i_taxCode		=	currentContext.getSetting('SCRIPT', 'custscript_taxcode');
		var i_fileId		=	currentContext.getSetting('SCRIPT', 'custscript_fileid');
		
		//if((type == 'ondemand') && (i_InvocieId != null && i_InvocieId !='') && (i_fileId != null && i_fileId !=''))
		{		
			var arrLines 	= 	CSV2JSON(i_fileId);
			//var content 	= 	arrLines[0];			                 
			//var dt_TimesheetDate 	= 	content['TimesheetDate'];
			/*for (var r = 1; r < arrLines.length; r++) {	
				
				nlapiLogExecution('debug', 'SCHEDULED INVOICE','dt_timesheetDate:'+arrLines[r].TimesheetDate);
			
			}	*/	
					
			var arr_TimesheetValue		= 	JSON.stringify(arrLines);			
			//nlapiLogExecution('debug', 'SCHEDULED INVOICE', 'arrLines: '+arr_TimesheetValue);
			//return;
			 /*for (var i = 0; i < arrLines.length - 1; i++) {
                 var content 				= arrLines[i];
                 var dt_timenetryDate		= content['Date'];                 	
			 }
			 
			 nlapiLogExecution('debug', 'SCHEDULED INVOICE', 'dt_timenetryDate:'+dt_timenetryDate);*/
			
			/*if((dt_billFrom != null || dt_billFrom != '') && (dt_billTo != null || dt_billTo != ''))
			{
				dt_billFrom =  nlapiStringToDate(dt_billFrom);
				dt_billTo   =  nlapiStringToDate(dt_billTo);
			}
				
			nlapiLogExecution('debug', 'SCHEDULED INVOICE','dt_billFrom:'+ dt_billFrom + ' || dt_billTo:'+dt_billTo);*/
			
			var obj_Invoice	=  nlapiLoadRecord('invoice', i_InvocieId);				
			var lineCount   =  obj_Invoice.getLineItemCount('time');			
			nlapiLogExecution('debug',  'SCHEDULED INVOICE', 'lineCount:'+lineCount);
			var i_LineAppliedCount = 0;
			
			
			for (var r = 1; r <= lineCount; r++) {				
				
				//nlapiLogExecution('debug', 'SCHEDULED INVOICE', 'Inside loop');				
				
				var dt_timesheetBilledDate	=	obj_Invoice.getLineItemValue('time', 'billeddate', r);
				var s_employeeName			=	obj_Invoice.getLineItemValue('time', 'employeedisp', r);
				var i_employeeName			=	obj_Invoice.getLineItemValue('time', 'employee', r);
				
				//nlapiLogExecution('debug', 'SCHEDULED INVOICE', 's_employeeName:'+s_employeeName);
			
				var dt_billedDate   		= 	nlapiStringToDate(dt_timesheetBilledDate);
				var i_BilledDateMatched 	= 	arr_TimesheetValue.indexOf(dt_timesheetBilledDate);	
				var i_BilledEmployeeMatched = 	arr_TimesheetValue.indexOf(s_employeeName);	
				var i_BilledEmployeeMatched_ = 	arr_TimesheetValue.indexOf(i_employeeName);	
				nlapiLogExecution('debug', 'SCHEDULED INVOICE', 'i_BilledDateMatched:'+i_BilledDateMatched + ', dt_timesheetBilledDate:'+dt_timesheetBilledDate);
				
				
				//arr_TimesheetValue.TimsheetDate[i_BilledDateMatched].TimesheetDate
				//if (dt_billFrom <= dt_billedDate && dt_billTo >= dt_billedDate)	
				if(i_BilledDateMatched != -1 &&  (i_BilledEmployeeMatched != -1 || i_BilledEmployeeMatched_ != -1))
				{	
					obj_Invoice.setLineItemValue('time', 'apply', r, 'T');
					obj_Invoice.setLineItemValue('time', 'taxcode', r, i_taxCode);
					i_LineAppliedCount++;
					nlapiLogExecution('debug', 'SCHEDULED INVOICE', 'CSV dt_timenetryDate:'+dt_timesheetBilledDate);
					
				}
				
				yieldScript(currentContext);
			}
			
			if(Number(i_LineAppliedCount) > 0)
			{
				for (var j=1; j <= obj_Invoice.getLineItemCount('item'); j++)
				{
					obj_Invoice.removeLineItem('item',j);
				}
				
				obj_Invoice.setFieldValue('custbody_auto_check_timesheet','F');
				var i_recordId =  nlapiSubmitRecord(obj_Invoice, {disabletriggers : true, enablesourcing : true});
             

				nlapiLogExecution('debug', 'SCHEDULED INVOICE','Auto check done for '+i_LineAppliedCount+ ' Timesheets For Invoice: '+i_recordId);
				
			}
		}		

		nlapiLogExecution('debug', 'SCHEDULED INVOICE','Execution END Here');
		
	} catch (err) {
		nlapiLogExecution('error', 'SCHEDULED INVOICE', err);
		Send_Exception_Mail(err);
	}	
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function CSVToArray(strData, strDelimiter) {
    strDelimiter = (strDelimiter || ",");
    var objPattern = new RegExp((
        // Delimiters.
        "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
        // Quoted fields.
        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
        // Standard fields.
        "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [
        []
    ];
    var arrMatches = null;
    while (arrMatches = objPattern.exec(strData)) {
        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[1];
        if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
            arrData.push([]);
        }
        if (arrMatches[2]) {
            var strMatchedValue = arrMatches[2].replace(
                new RegExp("\"\"", "g"), "\"");
        } else {
            // We found a non-quoted value.
            var strMatchedValue = arrMatches[3];
        }
        arrData[arrData.length - 1].push(strMatchedValue);        
    }
    // Return the parsed data.
    return (arrData);
}

function CSV2JSON(csvId) {
    var csv = nlapiLoadFile(csvId).getValue();
    var array = CSVToArray(csv);
    var objArray = [];
    /*for (var i = 1; i < array.length; i++) {
      objArray[i - 1] = array[i][1];         
    }*/

   // return ArraDataValue =  objArray.length === 0 ? "" : '"' + objArray.join('","') + '"';
    
   
    for (var i = 1; i < array.length; i++) {
        objArray[i - 1] = {};
        for (var k = 0; k < array[0].length && k < array[i].length; k++) {
            var key = array[0][k];
            objArray[i - 1][key] = array[i][k]
        }
    }
    objArray	= objArray.sort();
    var json 	= JSON.stringify(objArray);
    var str 	= json.replace(/},/g, "},\r\n");

    return JSON.parse(str);
}
function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Issue on upadate timesheet invoice';
        
        var s_Body = 'This is to inform that is upadate timesheet invoice having an issue and System is not able to send the details.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,null, null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​
