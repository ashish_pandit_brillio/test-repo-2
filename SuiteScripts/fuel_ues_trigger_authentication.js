/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Sep 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function authCode(type){
    if(type == "delete" ){
      return;
    }
	var body = {};
	var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	var empId = recObj.getFieldValue("custrecord_fuel_leadership_employee");
	nlapiLogExecution("DEBUG", "empId", empId);
	var method;
	if (type == "create") {
		method = "POST";
		body.emailId = nlapiLookupField("employee", empId, "email"); 
		var url = "https://fuelnode1.azurewebsites.net/leadership";
        body = JSON.stringify(body);
		var response = call_node(url,body,method);
		nlapiLogExecution("DEBUG", "response : ", JSON.stringify(response));
	}
	if (type == "xedit") {
		method = "DELETE";
		var b_status = recObj.getFieldValue("isinactive");
		if (b_status == "T") {
			//body.emailId = nlapiLookupField("employee", empId, "email"); 
			var url = "https://fuelnode1.azurewebsites.net/leadership/email_id/"+nlapiLookupField("employee", empId, "email");
			nlapiLogExecution("DEBUG", "xedit body delete: ", JSON.stringify(body));
            body = {};//JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response : ", JSON.stringify(response));
		}
		var oldRecObj = nlapiGetOldRecord();
		var b_olderStatus = oldRecObj.getFieldValue("isinactive");
		if(b_status == "F" && b_olderStatus == "T"){
			method = "POST";
			body.emailId = nlapiLookupField("employee", empId, "email"); 
			var url = "https://fuelnode1.azurewebsites.net/leadership";
			nlapiLogExecution("DEBUG", "xedit body create: ", JSON.stringify(body));
            body = JSON.stringify(body);
			var response = call_node(url,body,method);
			nlapiLogExecution("DEBUG", "response : ", JSON.stringify(response));
		}
	}
}
function deleteNotification(type){
	var body = {};
	var empId = nlapiGetFieldValue("custrecord_fuel_leadership_employee");
	var method;
	if (type == "delete") {
		method = "DELETE";
		//body.emailId = nlapiLookupField("employee", empId, "email"); 
		var email = nlapiLookupField("employee", empId, "email");
		var url = "https://fuelnode1.azurewebsites.net/leadership/email_id/"+email;
		nlapiLogExecution("DEBUG", "body : ", JSON.stringify(body));
        body = {};
		var response = call_node(url,body,method);
		nlapiLogExecution("DEBUG", "response : ", JSON.stringify(response));
	}
}