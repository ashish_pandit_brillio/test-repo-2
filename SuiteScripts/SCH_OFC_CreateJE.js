/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Sep 2016     deepak.srinivas
 *
 
 Script Modification Log:
     -- Date --			-- Modified By --				--Requested By--				-- Description --
    01/09/2020			Anukaran						   Deepak MS                   Add the logic of Dynamic Location and Subsidiary
																					   Create one custom record for the location and subisidary and attach as child record on "Exclude Facility Cost Against Customer" custom record.

*/
/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 * Objective: Creates the OFC Custom table based on employee location and creates the JE with Ofc cost
 */
function scheduled(type) {

	try{
		//Get script paramter value which 
		var i_context = nlapiGetContext();
		var exclude_ofc_cost_rcrd_id = i_context.getSetting('SCRIPT', 'custscript_exclude_ofc_cost_id');
		//var exclude_ofc_cost_rcrd_id = 53; //30;
		nlapiLogExecution('DEBUG','Exclude OFC Cost id',exclude_ofc_cost_rcrd_id);
		
		//Get the values of the record on which button is clicked 
		var loadRec = nlapiLoadRecord('customrecord_ofc_cost_against_customer',exclude_ofc_cost_rcrd_id);
		
		var customer_ofc_list = loadRec.getFieldValues('custrecord_customer_ofc');
		var project_ofc_list = loadRec.getFieldValues('custrecord_project_ofc');  
		var employee_ofc_list = loadRec.getFieldValues('custrecord_employeename'); // Code Added on 01/09/2020
		var month_ofc = loadRec.getFieldText('custrecord_month_ofc');
		var year_ofc = loadRec.getFieldText('custrecord_year_ofc');
		var i_debit_account = loadRec.getFieldValue('custrecord_ofc_cost_account'); //i_credit_account
		var i_credit_account = loadRec.getFieldValue('custrecord_ofc_credit_account');
		var exchangeRate = loadRec.getFieldValue('custrecord_ofc_cost_exchange_rate');
	//	var ofc_Cost_Bangalore = loadRec.getFieldValue('custrecord_ofc_cost_bangalore'); //Code comment on 01/09/2020
	//	var ofc_Cost_Trivandrum = loadRec.getFieldValue('custrecord_ofc_cost_trivandrum'); 
		
		//Updated for Pune Location
	//	var ofc_Cost_Pune = loadRec.getFieldValue('custrecord_ofc_cost_pune_location');//Code comment on 01/09/2020
		//Updated for Hyd Location
	//	var ofc_Cost_Hyd = loadRec.getFieldValue('custrecord_ofc_cost_india_hyd');  //Code comment on 01/09/2020
		
		var month_journal = loadRec.getFieldValue('custrecord_month_ofc');
		var year_journal = loadRec.getFieldValue('custrecord_year_ofc');
		
		var subsidiary_name = loadRec.getFieldValues('custrecord_ofc_cost_subsidiary'); // Code Added on 01/09/2020
		nlapiLogExecution('DEBUG','Subsidiary_Log','Subsidiary='+subsidiary_name); // Code Added on 01/09/2020
		
		//-------Start: Code Added on 01/09/2020-------------------------------------
			
			//Get child record location value from location sublist
			var itemlineCount = loadRec.getLineItemCount('recmachcustrecord_ref_location_facility_cost'); 
			nlapiLogExecution('DEBUG','Location_Log','Item Line Count='+itemlineCount);
			
			
			
			
			
			
           
           deleteOFC_Data(month_journal,year_journal,exclude_ofc_cost_rcrd_id);

			if(_logValidation(itemlineCount))
			{
				for(var i=1; i<=itemlineCount; i++)
				{
						var location_for_facility_cost = loadRec.getLineItemValue('recmachcustrecord_ref_location_facility_cost', 'custrecord_location_facility_cost', i);
						nlapiLogExecution('DEBUG','Location_Line_Level_Log','location_for_facility_cost ='+location_for_facility_cost); 
						var location_for_facility_cost_text = loadRec.getLineItemText('recmachcustrecord_ref_location_facility_cost', 'custrecord_location_facility_cost', i);
						nlapiLogExecution('DEBUG','Location_Line_Level_Log','location_for_facility_cost_text ='+location_for_facility_cost_text); 
						var ofc_cost_india= loadRec.getLineItemValue('recmachcustrecord_ref_location_facility_cost', 'custrecord_ofc_cost_india', i);
						nlapiLogExecution('DEBUG','Location_Line_Level_Log','ofc_cost_india='+ofc_cost_india);
						var ofc_exchange_rate_debit  = loadRec.getLineItemValue('recmachcustrecord_ref_location_facility_cost','custrecord_exchnage_rate_ofc', i);
						nlapiLogExecution('DEBUG','ofc_exchange_rate_debit','ofc_exchange_rate_debit='+ofc_exchange_rate_debit);
						
					
		//-------End: Code Added on 01/09/2020-------------------------------------
		
		
					var i_month = month_ofc;
					var i_year = year_ofc;
					
					//Gets the month start date
					var d_start_date = get_current_month_start_date(i_month, i_year);
					//Gets the month end date
					var d_end_date = get_current_month_end_date(i_month, i_year);
					
					d_start_date = nlapiStringToDate(d_start_date);
					d_end_date = nlapiStringToDate(d_end_date);
					//search resource Allocation record for the customer & project selected

					//var resourceAlloc_list = searchResourceAllocation(customer_ofc_list,project_ofc_list,d_start_date,d_end_date,i_month,i_year,ofc_Cost_Bangalore,ofc_Cost_Trivandrum,ofc_Cost_Pune,ofc_Cost_Hyd,exclude_ofc_cost_rcrd_id); //Code comment on 01/09/2020
					var resourceAlloc_list = searchResourceAllocation(customer_ofc_list,project_ofc_list, employee_ofc_list,d_start_date,d_end_date,i_month,i_year,subsidiary_name,location_for_facility_cost,ofc_cost_india,exclude_ofc_cost_rcrd_id,ofc_exchange_rate_debit); //Code Added on 01/09/2020
			
		
					//Deletes the cutom OFC Cost table for the month and year selected
					//deleteOFC_Data(month_journal,year_journal,exclude_ofc_cost_rcrd_id);
					//Creates the custom record entries for the resources list got from allocation list w.r.t location
					var createCustomTable = createOFC_CostRec(resourceAlloc_list,exclude_ofc_cost_rcrd_id);
					//If some error occured, then stop create JE
					if(createCustomTable == true)
					{
						nlapiLogExecution('DEBUG','Return Fucntion',createCustomTable);
						return;
					}
			}//End of For //Code Added on 01/09/2020
		}//End of If //Code Added on 01/09/2020
		//If no errors, then create JE for the entries made in OFC Custom table
		
		
		var journals  = createJournalEntry(month_journal,year_journal,i_debit_account,i_credit_account,exclude_ofc_cost_rcrd_id,exchangeRate,d_start_date,subsidiary_name);
	}
	catch(e){
		nlapiLogExecution('ERROR','Scheduled Script Error',e);
		return false;
	}

}

function deleteOFC_Data(month,year,exclude_ofc_cost_rcrd_id){
	try{
		var filter_D = Array();
		filter_D.push(new nlobjSearchFilter('custrecord_ofc_cost_month', null , 'anyof', parseInt(month)));
		filter_D.push(new nlobjSearchFilter('custrecord_ofc_cost_year', null , 'anyof', parseInt(year)));
      
        //added by sitaram - 06-July'21 to differentiate between credit and debit cost -- yet to test
        filter_D.push(new nlobjSearchFilter('custrecord_ofc_is_credit_cost', null , 'is', "F"));
      //
     //  ["custrecord_ofc_is_credit_cost","is","T"]
	//	filter_D.push(new nlobjSearchFilter('custrecord_ofc_cost', null , 'notequalto', parseInt(0)));
		
		var cols_D = Array();
		cols_D.push(new nlobjSearchColumn('internalid'));
		
	
		var ofc_cost_search = nlapiCreateSearch('customrecord_ofc_cost_custom_table',filter_D,cols_D);
		var resultset_ = ofc_cost_search.runSearch();
		var returnSearchResults_ = [];
		var searchid_ = 0;
		//var C_exchangeRate = parseFloat(exchangeRate);
		var currentContext_C = nlapiGetContext();
		var remaining_usage = currentContext_C.getRemainingUsage();
		//nlapiLogExecution('DEBUG','OFC Cost remaining_usage',remaining_usage);
		//If record count is >= 1000
		do {
		    var resultslice_ = resultset_.getResults(searchid_, searchid_ + 1000);
		    for ( var re in resultslice_) {
		    	if(currentContext_C.getRemainingUsage() <= 1000){
		    	nlapiYieldScript();
		    	}
		    	returnSearchResults_.push(resultslice_[re]);
		    	searchid_++;
		    }
		} while (resultslice_.length >= 1000);
		
		if(returnSearchResults_){
          nlapiLogExecution('DEBUG','deleteOFC_Data LENGTH',returnSearchResults_.length);
			
				
			for(var k=0;k<returnSearchResults_.length;k++){
              try {
			var internal_ID = 	returnSearchResults_[k].getValue('internalid');
			if(internal_ID){
              if(currentContext_C.getRemainingUsage() <= 1000){
		    	nlapiYieldScript();
		    	}
			nlapiDeleteRecord('customrecord_ofc_cost_custom_table',internal_ID);
			}
              }catch(e){
                nlapiLogExecution('DEBUG','deleteOFC_Data EXCEPTION',e);
				nlapiSubmitField('customrecord_ofc_cost_against_customer',exclude_ofc_cost_rcrd_id,'custrecord_ofc_cost_log',e.message);
				return;
			}
            nlapiLogExecution('DEBUG','deleteOFC_Data internal_ID',internal_ID);
			}
			
			
		
	}
	}
	catch(e){
		nlapiLogExecution('ERROR','Deleting OFC Custom Table Error',e);
		return false;
	}
	
}
function createJournalEntry(month,year,i_debit_account,i_credit_account,exclude_ofc_cost_rcrd_id,exchangeRate,d_start_date,subsidiary_name){
	try{
      
       var vendorList = { };

var default_vendor_mappingSearch = nlapiSearchRecord("customrecord_default_vendor_mapping",null,
[
], 
[
new nlobjSearchColumn("custrecord_subsidiary_je"), 
new nlobjSearchColumn("custrecord_default_vendor")
]
);

for (var i_index = 0; i_index < default_vendor_mappingSearch.length; i_index++)
{
    var subsidiary_je = default_vendor_mappingSearch[i_index].getValue('custrecord_subsidiary_je');
    var default_vendor_je = default_vendor_mappingSearch[i_index].getValue('custrecord_default_vendor');
    
    vendorList[subsidiary_je] = default_vendor_je;
                
}
      var JE_Arr = new Array();
	  nlapiLogExecution('DEBUG','subsidiary_name',subsidiary_name.length);
	  nlapiLogExecution('DEBUG','subsidiary_name',JSON.stringify(subsidiary_name));
      for(var ii=0;ii<subsidiary_name.length;ii++){
		  
	  nlapiLogExecution('DEBUG','subsidiary_name[ii]',subsidiary_name[ii]);
		var flag = false;
		var filter = Array();
		filter.push(new nlobjSearchFilter('custrecord_ofc_cost_month', null , 'anyof', parseInt(month)));
		filter.push(new nlobjSearchFilter('custrecord_ofc_cost_year', null , 'anyof', parseInt(year)));
		filter.push(new nlobjSearchFilter('custrecord_ofc_cost', null , 'notequalto', parseInt(0)));
		filter.push(new nlobjSearchFilter('custrecord_ofc_resource_subidiary_', null , 'anyof', subsidiary_name[ii]));
         //added by sitaram - 06-July'21 to create JE only for the cases where checkbox is unmarked -- yet to test
        filter.push(new nlobjSearchFilter('custrecord_ofc_is_credit_cost', null , 'is', "F"));
		
		var cols = Array();
		cols.push(new nlobjSearchColumn('custrecord_ofc_resource'));
		cols.push(new nlobjSearchColumn('custrecord_ofc_cost_project'));
		cols.push(new nlobjSearchColumn('custrecord_ofc_cost_customer'));
		cols.push(new nlobjSearchColumn('custrecord_ofc_cost_practice'));
		cols.push(new nlobjSearchColumn('custrecord_ofc_cost'));
		cols.push(new nlobjSearchColumn('isinactive','custrecord_ofc_cost_practice'));  //Added code on 1/09/2020
		cols.push(new nlobjSearchColumn('custrecord_exchnage_rate_'));
	
		//searches the custom record entries for the month and generates JE
		var ofc_cost_search = nlapiCreateSearch('customrecord_ofc_cost_custom_table',filter,cols);
		var resultset_ = ofc_cost_search.runSearch();
		var returnSearchResults_ = [];
		var searchid_ = 0;
		//var C_exchangeRate = parseFloat(exchangeRate);
		var currentContext_C = nlapiGetContext();
		var remaining_usage = currentContext_C.getRemainingUsage();
		//nlapiLogExecution('DEBUG','OFC Cost remaining_usage',remaining_usage);
		do {
		    var resultslice_ = resultset_.getResults(searchid_, searchid_ + 1000);
		    for ( var re in resultslice_) {
		    	if(currentContext_C.getRemainingUsage() <= 1000){
		    	nlapiYieldScript();
		    	}
		    	returnSearchResults_.push(resultslice_[re]);
		    	searchid_++;
		    }
		} while (resultslice_.length >= 1000);
		
		
		var sr_no_journal = 1;
		var total_credit_to_be_tagged = 0;
		nlapiLogExecution('DEBUG','OFC Cost Search Length',returnSearchResults_.length);
	
	var d_start_date_S = nlapiDateToString(d_start_date);
	nlapiLogExecution('DEBUG','d_start_date_S',d_start_date_S);
		if(returnSearchResults_ != null && returnSearchResults_.length > 0){
			
			var je_ofc_cost = nlapiCreateRecord('journalentry');
		//	je_discount_module.setFieldText('currency', 'USD'); returnSearchResults_.length
			je_ofc_cost.setFieldValue('subsidiary', parseInt(subsidiary_name[ii]));
			je_ofc_cost.setFieldValue('trandate',d_start_date_S);
			
			try{
			
			for(var k=0;k<returnSearchResults_.length;k++){
				
				var proj_rcrd = nlapiLoadRecord('job',returnSearchResults_[k].getValue('custrecord_ofc_cost_project'));
				var entity_id = proj_rcrd.getFieldValue('entityid');
				var alt_name = proj_rcrd.getFieldValue('altname');
				var custentity_vertical = proj_rcrd.getFieldValue('custentity_vertical');
				var proj_desrcption = entity_id +' '+alt_name;
				
				//lookup for Customer
				var customerDetails = nlapiLookupField('customer',returnSearchResults_[k].getValue('custrecord_ofc_cost_customer'),['territory']);
				var cust_territory = customerDetails.territory;
				
				var emp_amount_to_be_tagged = parseFloat(returnSearchResults_[k].getValue('custrecord_exchnage_rate_')) * returnSearchResults_[k].getValue('custrecord_ofc_cost');
				emp_amount_to_be_tagged = emp_amount_to_be_tagged.toString();
				if(emp_amount_to_be_tagged.indexOf(".")>0)
				{
					var temp_index = emp_amount_to_be_tagged.indexOf(".")+3;
					emp_amount_to_be_tagged = emp_amount_to_be_tagged.substr(0,temp_index);
				}
				emp_amount_to_be_tagged = parseFloat(emp_amount_to_be_tagged);
				
				//total_credit_to_be_tagged = parseFloat(total_credit_to_be_tagged) + parseFloat(emp_amount_to_be_tagged);
			//	total_credit_to_be_tagged = total_credit_to_be_tagged.toFixed(2);

				//Calculate Total
				total_credit_to_be_tagged = parseFloat(total_credit_to_be_tagged) + parseFloat(emp_amount_to_be_tagged);
				total_credit_to_be_tagged = total_credit_to_be_tagged.toFixed(2);
				
//Code comment on 01/09/2020 //nlapiLogExecution('DEBUG','OFC Cost remaining_usage ','Remaining usage:' + currentContext_C.getRemainingUsage() +'At line:'+k);
				je_ofc_cost.selectNewLineItem('line');
				
				if(currentContext_C.getRemainingUsage() <= 1000){
			    	nlapiYieldScript();
			    	}
				
				je_ofc_cost.setCurrentLineItemValue('line',  'account', i_debit_account);
				je_ofc_cost.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(k+1));
				sr_no_journal++;
				je_ofc_cost.setCurrentLineItemValue('line', 'debit', emp_amount_to_be_tagged);

				if(returnSearchResults_[k].getValue('isinactive','custrecord_ofc_cost_practice') == 'F')
				{
				je_ofc_cost.setCurrentLineItemValue('line', 'department', returnSearchResults_[k].getValue('custrecord_ofc_cost_practice'));
				}
				
				//je_ofc_cost.setCurrentLineItemValue('line', 'department', returnSearchResults_[k].getValue('custrecord_ofc_cost_practice'));
				je_ofc_cost.setCurrentLineItemValue('line', 'class', custentity_vertical);
				je_ofc_cost.setCurrentLineItemValue('line', 'custcol_project_name', proj_desrcption);
				je_ofc_cost.setCurrentLineItemValue('line', 'custcolprj_name', proj_desrcption);
				je_ofc_cost.setCurrentLineItemValue('line', 'custcol_sow_project', returnSearchResults_[k].getValue('custrecord_ofc_cost_project'));
				je_ofc_cost.setCurrentLineItemValue('line', 'custcol_territory', cust_territory);
				je_ofc_cost.setCurrentLineItemValue('line', 'custcol_employeenamecolumn', returnSearchResults_[k].getText('custrecord_ofc_resource'));
				je_ofc_cost.setCurrentLineItemValue('line', 'custcolcustcol_temp_customer', returnSearchResults_[k].getText('custrecord_ofc_cost_customer'));
                //Added by Sitaram - 
                //je_ofc_cost.setCurrentLineItemValue('line', 'entity', 296842);
                //Added by Sitaram - 
                if(_logValidation(vendorList[subsidiary_name[ii]])){
                je_ofc_cost.setCurrentLineItemValue('line', 'entity', vendorList[subsidiary_name[ii]]);											
                }
                
				je_ofc_cost.setCurrentLineItemValue('line', 'memo', 'OFC Cost JE');
				je_ofc_cost.commitLineItem('line');
			}
			je_ofc_cost.selectNewLineItem('line');
			je_ofc_cost.setCurrentLineItemValue('line', 'custcol_srnumber', parseInt(sr_no_journal));
			je_ofc_cost.setCurrentLineItemValue('line', 'account', i_credit_account);
			je_ofc_cost.setCurrentLineItemValue('line', 'credit', total_credit_to_be_tagged);
            //Added by Sitaram - 
            //je_ofc_cost.setCurrentLineItemValue('line', 'entity', 296842);
            //Added by Sitaram - 
                if(_logValidation(vendorList[subsidiary_name[ii]])){
                je_ofc_cost.setCurrentLineItemValue('line', 'entity', vendorList[subsidiary_name[ii]]);											
                }
            
			je_ofc_cost.commitLineItem('line');
			
			var je_id = nlapiSubmitRecord(je_ofc_cost,true,true);
			nlapiLogExecution('audit','je_id:- ',je_id);
			JE_Arr.push(je_id);
			}
			catch(e){
				nlapiSubmitField('customrecord_ofc_cost_against_customer',exclude_ofc_cost_rcrd_id,'custrecord_ofc_cost_log',e.message);
				return;
			}
		 }
			
		}
		if(JE_Arr.length>0){
			nlapiSubmitField('customrecord_ofc_cost_against_customer',exclude_ofc_cost_rcrd_id,'custrecord_journal_entry_ofc',JE_Arr);
		}
		
		
		
		
	}
	catch(e){
		nlapiLogExecution('ERROR','Create Journal Error',e);
		return false;
	}
	
	
}

function createOFC_CostRec(emp_list,exclude_ofc_cost_rcrd_id){
	try{
		var flag = false;
		var len  = emp_list.length;
		nlapiLogExecution('DEBUG','len','len:=>'+len);
		var currentContext_ = nlapiGetContext();
		if(emp_list){
			for(var j=0;j<emp_list.length;j++){
				
				//var ofc_cost_F = emp_list[j].ofc_cost;
				//ofc_cost_F = ofc_cost_F.toFixed(2);
				if(currentContext_.getRemainingUsage() <= 1000){
			    	nlapiYieldScript();
			    	}
				try{
                  //if(emp_list[j].resource == 108073 || emp_list[j].resource == '108073'){
                  //  nlapiLogExecution('DEBUG','emp_list[j].resource',JSON.stringify(emp_list[j]));
                  //}
				//nlapiLogExecution('DEBUG',' emp_list[j].practice','emp_practice'+ emp_list[j].practice);
				var createRecord = nlapiCreateRecord('customrecord_ofc_cost_custom_table');
				createRecord.setFieldValue('custrecord_ofc_resource', emp_list[j].resource);
				createRecord.setFieldValue('custrecord_ofc_cost_project', emp_list[j].project);
				createRecord.setFieldValue('custrecord_ofc_cost_customer', emp_list[j].customer);
				createRecord.setFieldValue('custrecord_ofc_percent_allocation', (emp_list[j].percent_of_time * 100));
				/*createRecord.setFieldText('custrecord_ofc_work_location', emp_list[j].workLocation);
				createRecord.setFieldText('custrecord_ofc_cost_work_city', emp_list[j].workCity);*/
				createRecord.setFieldValue('custrecord_ofc_cost_location', emp_list[j].emp_location);
				createRecord.setFieldValue('custrecord_ofc_cost_billing_start_date', emp_list[j].billingStartDate);
				createRecord.setFieldValue('custrecord_ofc_billion_end_date', emp_list[j].billingEndDate);
				if(emp_list[j].practice != 320) //Added on 01/09/2020  because 320 is practice is inactive insystem
				{
				createRecord.setFieldValue('custrecord_ofc_cost_practice', emp_list[j].practice); //Added on 01/09/2020
				}
				//createRecord.setFieldValue('custrecord_ofc_cost_practice', emp_list[j].practice); //commented on 01/09/2020
				
				createRecord.setFieldValue('custrecord_exchnage_rate_', emp_list[j].custrecord_exchnage_rate_);
				createRecord.setFieldValue('custrecord_ofc_resource_subidiary_', emp_list[j].custrecord_ofc_resource_subidiary_);
				
				nlapiLogExecution('DEBUG','Exchange Rate', emp_list[j].custrecord_exchnage_rate_);
				nlapiLogExecution('DEBUG','Resource Subsidiary', emp_list[j].custrecord_ofc_resource_subidiary_);
				
				createRecord.setFieldText('custrecord_ofc_cost_month', emp_list[j].month);
				createRecord.setFieldText('custrecord_ofc_cost_year', emp_list[j].year);
				createRecord.setFieldValue('custrecord_ofc_cost', emp_list[j].ofc_cost);
				
				var id = nlapiSubmitRecord(createRecord);
	//Code comment on 01/09/2020	//	nlapiLogExecution('DEBUG','OFC COST CUSTOM RECORD','OFC COST CUSTOM RECORD:'+id+'Length:'+j);
				}
				catch(e){
					//nlapiLogExecution('DEBUG','Catch Block','Inside Catch');
					nlapiSubmitField('customrecord_ofc_cost_against_customer',exclude_ofc_cost_rcrd_id,'custrecord_ofc_cost_log',e.message);
					flag = true;
				//	nlapiLogExecution('DEBUG','Catch Block flag','Inside Catch flag:'+flag);
					return flag;
				}
				
			}
		}
	}
	catch(e){
		nlapiLogExecution('ERROR','Error in creating the custom record for OFC',e);
		return false;
	}
}

//function searchResourceAllocation(custList,projList,d_start_date,d_end_date,month_ofc,year_ofc,ofc_Cost_Bangalore,ofc_Cost_Trivandrum,ofc_Cost_Pune,ofc_Cost_Hyd,exclude_ofc_cost_rcrd_id){ //Code comment on 01/09/2020
function searchResourceAllocation(custList,projList,employee_List,d_start_date,d_end_date,month_ofc,year_ofc,subsidiary_name,location_for_facility_cost,ofc_cost_india,exclude_ofc_cost_rcrd_id,ofc_exchange_rate_debit){ // Code added on 01/09/2020
	try{
		var filters = Array();
		var emp_detail_arr = new Array();
		nlapiLogExecution('DEBUG','Employee_List_Log','employee_List ='+employee_List);
		if(projList){
			filters.push(new nlobjSearchFilter('internalid', 'job', 'noneof', projList));
		}
		else if(custList){ // Code added on 01/09/2020
			filters.push(new nlobjSearchFilter('internalid', 'customer', 'noneof', custList));
		}else{
			filters.push(new nlobjSearchFilter('internalid', 'employee', 'noneof', employee_List));  // Code added on 01/09/2020
		}
		
		filters.push(new nlobjSearchFilter('startdate', null, 'onorbefore', d_end_date));
		filters.push(new nlobjSearchFilter('enddate', null, 'onorafter', d_start_date));
		filters.push(new nlobjSearchFilter('custentity_project_allocation_category', 'job', 'anyof', 1));//Revenue Category
		
		var columns = new Array();
		
		columns[0] = new nlobjSearchColumn('project');
		columns[1] = new nlobjSearchColumn('company');
		columns[2] = new nlobjSearchColumn('startdate');
		columns[3] = new nlobjSearchColumn('enddate');
		columns[4] = new nlobjSearchColumn('percentoftime');
		columns[5] = new nlobjSearchColumn('resource');
		columns[6] = new nlobjSearchColumn('subsidiary','employee');
		columns[7] = new nlobjSearchColumn('custevent_practice');
		columns[8] = new nlobjSearchColumn('custentity_vertical','job');
		columns[9] = new nlobjSearchColumn('territory','customer');
		columns[10] = new nlobjSearchColumn('subsidiary','customer');
		columns[11] = new nlobjSearchColumn('custeventwlocation');
		columns[12] = new nlobjSearchColumn('custevent_workcityra');
		columns[13] = new nlobjSearchColumn('custevent_workstatera');
		columns[14] = new nlobjSearchColumn('subsidiary','customer');
		columns[15] = new nlobjSearchColumn('customer'); 
		columns[16] = new nlobjSearchColumn('location','employee');
		
		try{
			
		
		var createResourceSearch = nlapiCreateSearch('resourceallocation', filters, columns);
		var resultset = createResourceSearch.runSearch();
		var returnSearchResults = [];
		var searchid = 0;
		var currentContext = nlapiGetContext();
		do {
		    var resultslice = resultset.getResults(searchid, searchid + 1000);
		    for ( var rs in resultslice) {
		    	if(currentContext.getRemainingUsage() <= 500){
		    	nlapiYieldScript();
		    	}
		        returnSearchResults.push(resultslice[rs]);
		        searchid++;
		    }
		} while (resultslice.length >= 1000);
		
		nlapiLogExecution('DEBUG','Resource Allocation d_start_date',d_start_date);
		nlapiLogExecution('DEBUG','Resource Allocation d_end_date',d_end_date);
		nlapiLogExecution('DEBUG','Resource Allocation Search Length',returnSearchResults.length);
		if(returnSearchResults){
			for(var index=0;index<returnSearchResults.length;index++){
				var resource = returnSearchResults[index].getValue('resource');
				var customer = returnSearchResults[index].getValue('customer');
				var project = returnSearchResults[index].getValue('project');
				var percent_of_time = returnSearchResults[index].getValue('percentoftime');
				
				var billingStartDate = returnSearchResults[index].getValue('startdate');
				var billingStartDate_OFC = returnSearchResults[index].getValue('startdate');
				billingStartDate = nlapiStringToDate(billingStartDate);
				
				var billingEndDate_OFC = returnSearchResults[index].getValue('enddate');
				var billingEndDate = returnSearchResults[index].getValue('enddate');
				 billingEndDate = nlapiStringToDate(billingEndDate);
				var practice = returnSearchResults[index].getValue('custevent_practice');
				var resource_name = returnSearchResults[index].getText('resource');
				var resource_subsidiary = returnSearchResults[index].getValue('subsidiary','employee');
				
			//	var emp_location = returnSearchResults[index].getText('location','employee'); //Code comment on 01/09/2020
				var emp_location = returnSearchResults[index].getValue('location','employee'); //Code added on 01/09/2020
				var customer_subsidiary = returnSearchResults[index].getValue('subsidiary','customer');
				
				percent_of_time = percent_of_time.toString();
				percent_of_time = percent_of_time.split('.');
				percent_of_time = percent_of_time[0].toString().split('%');
				var final_prcnt_allocation = parseFloat(percent_of_time) / parseFloat(100);
				final_prcnt_allocation = parseFloat(final_prcnt_allocation);
				var f_ofc_cost = 0;
               //if(resource == 108073 || resource == '108073'){
                 nlapiLogExecution('DEBUG','Employee details',resource_subsidiary+', '+subsidiary_name+', '+emp_location+', '+location_for_facility_cost);
               //}
				//d_start_date = nlapiDateToString(d_start_date);
				//d_end_date = nlapiDateToString(d_end_date);
				
				
				//if(parseInt(resource_subsidiary) == 3)	 //Code comment on 01/09/2020
				//nlapiLogExecution('DEBUG','Employee details','1st check'+resource+', '+parseInt(resource_subsidiary)+', '+subsidiary_name+', '+parseInt(emp_location)+', '+location_for_facility_cost);
				if(subsidiary_name.indexOf(resource_subsidiary) != -1)	 //Code added on 01/09/2020
				{
					//nlapiLogExecution('DEBUG','Employee details','first test passed '+index);
					//if(emp_location == 'India : Trivandrum') 	 //Code comment on 01/09/2020
				   if(parseInt(emp_location) == location_for_facility_cost)	 //Code added on 01/09/2020
					{
						//nlapiLogExecution('DEBUG','Employee details','second test passed '+index);
						//Check for Percent of Allocation
						//var ofc_cost_based_alloc = parseFloat(final_prcnt_allocation) * parseFloat(ofc_Cost_Trivandrum);  //Code comment on 01/09/2020
						var ofc_cost_based_alloc = parseFloat(final_prcnt_allocation) * parseFloat(ofc_cost_india); //Code added on 01/09/2020
						//nlapiLogExecution('DEBUG','ofc_cost_based_alloc',ofc_cost_based_alloc);
						
						var numberDays = getDatediffIndays(d_start_date,d_end_date);
						//nlapiLogExecution('DEBUG','numberDays',numberDays);
						//var numberDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(d_start_date); 
						
						var perDayOFC_Cost = ofc_cost_based_alloc / numberDays;
						//nlapiLogExecution('DEBUG','perDayOFC_Cost',perDayOFC_Cost);
						
					/*	if(billingEndDate >= d_end_date){
							f_ofc_cost = parseFloat(numberDays) * parseFloat(perDayOFC_Cost);
						}
						else {
							if()
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}*/
                      
                        if(resource == 108073){
                            nlapiLogExecution('DEBUG','If block executed ',ofc_cost_based_alloc+', '+numberDays+', '+perDayOFC_Cost);
                         }
						if(billingStartDate <= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate <= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
                      
                       if(resource == 108073 || resource == '108073'){
                            nlapiLogExecution('DEBUG','daysBtwMonth - f_ofc_cost',daysBtwMonth+', '+f_ofc_cost);
                         }
						
						//nlapiLogExecution('DEBUG','daysBtwMonth',daysBtwMonth);
						//nlapiLogExecution('DEBUG','f_ofc_cost',f_ofc_cost);
							
						//var dates_Diff = getDatediffIndays(,d_end_date)
						//var numberOfBillableDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(billingEndDate);
						
						
						//f_ofc_cost = parseFloat(numberOfBillableDays) * parseFloat(perDayOFC_Cost);
					}
					
			/* Code comment on 01/09/2020
					//Updated for pune location India : Hyderabad-Aqua
					if(emp_location == 'India : Pune : Indiqube')
					{
						
						//Check for Percent of Allocation
						var ofc_cost_based_alloc = parseFloat(final_prcnt_allocation) * parseFloat(ofc_Cost_Pune);
						
						var numberDays = getDatediffIndays(d_start_date,d_end_date);
						//var numberDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(d_start_date); 
						
						var perDayOFC_Cost = ofc_cost_based_alloc / numberDays;       Code comment on 01/09/2020 */
						
					/*	if(billingEndDate >= d_end_date){
							f_ofc_cost = parseFloat(numberDays) * parseFloat(perDayOFC_Cost);
						}
						else {
							if()
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}*/
						
				/* Code comment on 01/09/2020
						if(billingStartDate <= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate <= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						} 
							
						//var dates_Diff = getDatediffIndays(,d_end_date)
						//var numberOfBillableDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(billingEndDate);
						
						
						//f_ofc_cost = parseFloat(numberOfBillableDays) * parseFloat(perDayOFC_Cost);
						
					}  Code comment on 01/09/2020 */
					
			/* Code comment on 01/09/2020
					//Updated for hyderabad location India : Hyderabad-Aqua
					if(emp_location == 'India : Hyderabad-Aqua'){
						
						//Check for Percent of Allocation
						var ofc_cost_based_alloc = parseFloat(final_prcnt_allocation) * parseFloat(ofc_Cost_Hyd);
						
						var numberDays = getDatediffIndays(d_start_date,d_end_date);
						//var numberDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(d_start_date); 
						
						var perDayOFC_Cost = ofc_cost_based_alloc / numberDays; 
			
			Code comment on 01/09/2020 */
						
					/*	if(billingEndDate >= d_end_date){
							f_ofc_cost = parseFloat(numberDays) * parseFloat(perDayOFC_Cost);
						}
						else {
							if()
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}*/
			/* Code comment on 01/09/2020
						if(billingStartDate <= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate <= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
							
						//var dates_Diff = getDatediffIndays(,d_end_date)
						//var numberOfBillableDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(billingEndDate);
						
						
						//f_ofc_cost = parseFloat(numberOfBillableDays) * parseFloat(perDayOFC_Cost);
					}  Code comment on 01/09/2020 */
					
			/* Code comment on 01/09/2020		
					if(emp_location == 'India : Bangalore' || emp_location == 'India : Bangalore : Sapphire' || emp_location == 'India : Bangalore : Ecospace' || emp_location=='India : Bangalore : HSR')
					{
						
						var ofc_cost_based_alloc = parseFloat(final_prcnt_allocation) * parseFloat(ofc_Cost_Bangalore);
						var numberDays = getDatediffIndays(d_start_date,d_end_date);
						//var numberDays = nlapiStringToDate(d_end_date) - nlapiStringToDate(d_start_date); 
						
						var perDayOFC_Cost = ofc_cost_based_alloc / numberDays;
			Code comment on 01/09/2020 */
			
					/*	if(billingEndDate >= d_end_date){
							f_ofc_cost = parseFloat(numberDays) * parseFloat(perDayOFC_Cost);
						}
						else {
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}*/
						
			/* Code comment on 01/09/2020	
						if(billingStartDate <= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate <= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(d_start_date,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate >= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,d_end_date);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
						else if(billingStartDate >= d_start_date && billingEndDate <= d_end_date)
						{
							var daysBtwMonth = getDatediffIndays(billingStartDate,billingEndDate);
							f_ofc_cost = parseFloat(daysBtwMonth) * parseFloat(perDayOFC_Cost);
						}
					} 
				Code comment on 01/09/2020 */
					
				}
				else
				{
					if(resource == 108073 || resource == '108073'){
                       nlapiLogExecution('DEBUG','else block executed','index '+ index);
                     }
					f_ofc_cost = 0;
				}
				
				
				emp_detail_arr[index] = {
						'resource':resource,
						'resource_name':resource_name,
						'project':project,
						'customer':customer,
						'percent_of_time':final_prcnt_allocation,
						'emp_location':location_for_facility_cost,
						'billingStartDate':billingStartDate_OFC,
						'billingEndDate':billingEndDate_OFC,
						'practice':practice,
						'month':month_ofc,
						'year':year_ofc,
						'ofc_cost':parseFloat(f_ofc_cost),
						'customer_subsidiary':customer_subsidiary,
						'custrecord_exchnage_rate_': ofc_exchange_rate_debit,
						'custrecord_ofc_resource_subidiary_': resource_subsidiary 
						
					};
			}
			
		}
		return emp_detail_arr;
		}
		catch(e){
			nlapiSubmitField('customrecord_ofc_cost_against_customer',exclude_ofc_cost_rcrd_id,'custrecord_ofc_cost_log',e.message);
			throw nlapiCreateError('Error',e.message,true);
		}
	}
	catch(e){
		nlapiLogExecution('ERROR','Resource Allocation Search error',e);
		return false;
	}
}
//Get start date of the month
function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}

//Get End date of the month
function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

//checks for the blank value
function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

//script usage check and re-schedule 
function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate;
	var date1 = nlapiStringToDate(fromDate);
	var date2 = nlapiStringToDate(toDate);
	var date3 = Math.round((toDate - fromDate) / one_day);
	return (date3 + 1);
}

