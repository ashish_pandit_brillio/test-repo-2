/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Aug 2014     Swati
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord_Customer_Discount()
{
	var Current_Inv='';
    var Apply_Count=nlapiGetLineItemCount('apply');
	//alert('Apply_Count'+Apply_Count);
    
    if(Apply_Count !=0 || Apply_Count !=-1)
    {
    	for(var ss=1 ; ss<=Apply_Count ; ss++)
    		{
    			var Selected_Inv=nlapiGetLineItemValue('apply','apply',ss);
    			//alert('Selected_Inv'+Selected_Inv);
    			if(Selected_Inv == 'T')
    				{
    				
    				Current_Inv=nlapiGetLineItemValue('apply','doc',ss);
    				//alert('Current_Inv'+Current_Inv);
    				}
    		}
    }
    var o_Invoice_Load=nlapiLoadRecord('invoice', Current_Inv);
    var Location=nlapiGetFieldValue('location');
  //-------------------customer load------------------------------------------------
	var S_Customer_Id=nlapiGetFieldValue('customer');
	var o_Customer_Load=nlapiLoadRecord('customer', S_Customer_Id);
	var d_MSA_Start_Date=o_Customer_Load.getFieldValue('startdate');
	nlapiLogExecution('DEBUG','d_MSA_Start_Date',d_MSA_Start_Date);
	var d_MSA_End_Date=o_Customer_Load.getFieldValue('enddate');
	//nlapiLogExecution('DEBUG','d_MSA_End_Date',d_MSA_End_Date);
	//--------------------------------------------------------------------------------
	//-----------------------------project load--------------------------------------
	var o_Project_Id=o_Invoice_Load.getFieldValue('job');
	var o_Project_Load=nlapiLoadRecord('job',o_Project_Id);
	var d_Project_Start_Date=o_Project_Load.getFieldValue('startdate');
	nlapiLogExecution('DEBUG','d_Project_Start_Date',d_Project_Start_Date);
	var d_Project_End_Date=o_Project_Load.getFieldValue('enddate');
	//nlapiLogExecution('DEBUG','d_Project_End_Date',d_Project_End_Date);
	//--------------------------------------------------------------------------------
    
    //--------------------------------------------------------------------------------------
    var b_result = false;
	//alert('Fixed Free Discount Processing');
	var i_Fixed_Discount_Rate_Sum=0;
	var g_Fixed_Inv_Amt_Sum=0;
	var i_fixed_free_count=o_Customer_Load.getLineItemCount('recmachcustrecord_fpcustparent');
	//alert('i_fixed_free_count'+i_fixed_free_count);
		if(i_fixed_free_count == '-1' || i_fixed_free_count == 0)
		{//count check if start
		}// count check if close
		else
		{//count check else start
		    var i_Fixed_Apply_Method=parseFloat(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_applymethod123',1));
			//nlapiLogExecution('ERROR', 'i_Fixed_Apply_Method', i_Fixed_Apply_Method);
			var i_Fixed_Discount_Base_Amt=parseFloat(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fdbasedamount',1));
			//alert('i_Fixed_Discount_Base_Amt'+i_Fixed_Discount_Base_Amt);
			nlapiLogExecution('ERROR', 'i_Fixed_Discount_Base_Amt', i_Fixed_Discount_Base_Amt);
			if(i_Fixed_Discount_Base_Amt == '1')
				{
									/////include===
					var i_Subtotal_Val=parseFloat(nlapiGetFieldValue('total'));
					//alert('i_Subtotal_Val'+i_Subtotal_Val);
				}
			else{//excluding===
					var i_Subtotal_Val=parseFloat(nlapiGetFieldValue('subtotal'));
					//alert('i_Subtotal_Val'+i_Subtotal_Val);
				}
				//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if(i_Fixed_Apply_Method == '1')
			{//applied method if start	
			}//applied method if close
			else
			{
				var i_Fixed_Remittence=(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fpremittancemethod',1));
				nlapiLogExecution('DEBUG','i_Fixed_Remittence',i_Fixed_Remittence);
				
				if(i_Fixed_Remittence == '1')
					{
					  //-------search all invoice created in this year with same customer
					  var Credit_Memo_Filter=new Array();
					  Credit_Memo_Filter[0]=new nlobjSearchFilter('entity', null,'is',S_Customer_Id);
					  //Inv_Filter[1]=new nlobjSearchFilter('job', null,'anyof',project_rec_id);
					  var o_Credit_Memo_Search=nlapiSearchRecord('transaction',159,Credit_Memo_Filter,null);
					
						if(o_Credit_Memo_Search != null)
						{  // alert('o_Credit_Memo_Search.length===='+o_Credit_Memo_Search.length);
							b_result=true;
						}
					}
				else if(i_Fixed_Remittence == '2')
					{
						//-------search all invoice created in this quater with same customer
					  var Credit_Memo_Filter=new Array();
					  Credit_Memo_Filter[0]=new nlobjSearchFilter('entity', null,'is',S_Customer_Id);
					  //Inv_Filter[1]=new nlobjSearchFilter('job', null,'anyof',project_rec_id);
					  var o_Credit_Memo_Search=nlapiSearchRecord('transaction',160,Credit_Memo_Filter,null);
					
						if(o_Credit_Memo_Search != null)
						{ //alert('o_Credit_Memo_Search.length===='+o_Credit_Memo_Search.length);
							b_result=true;
						}
					}
				//alert('b_result'+b_result);
		            if(b_result== false)
		            	{
		            	//----------------------------------------------------------------------------------------------------------------------------------------
		            	
		        		//alert('Fixed Free Discount Processing');
		        		var i_Fixed_Discount_Rate_Sum=0;
		        		var g_Fixed_Inv_Amt_Sum=0;
		        		var i_fixed_free_count=o_Customer_Load.getLineItemCount('recmachcustrecord_fpcustparent');
		        		//alert('i_fixed_free_count'+i_fixed_free_count);
		        		if(i_fixed_free_count == '-1' || i_fixed_free_count == 0)
		        		{//count check if start
		        		}// count check if close
		        		else
		        		{//count check else start
		        			var i_Fixed_Discount_Base_Amt=parseFloat(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fdbasedamount',1));
		        			//nlapiLogExecution('DEBUG','i_Fixed_Discount_Base_Amt',i_Fixed_Discount_Base_Amt);
		        			if(i_Fixed_Discount_Base_Amt == '1')
		        				{
		        									/////include===
		        					var i_Subtotal_Val=parseFloat(nlapiGetFieldValue('total'));
		        					nlapiLogExecution('DEBUG','i_Subtotal_Val',i_Subtotal_Val);
		        				}
		        			else{//excluding===
		        					var i_Subtotal_Val=parseFloat(nlapiGetFieldValue('subtotal'));
		        					nlapiLogExecution('DEBUG','i_Subtotal_Val',i_Subtotal_Val);
		        				}
		        					var credit_memo_record = nlapiCreateRecord('creditmemo');
		        				for(var ss=1 ; ss<=i_fixed_free_count ; ss++)
		        				{
		        						var i_Fixed_Discount_Item=parseFloat(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fpdiscountitem',ss));
		        						nlapiLogExecution('DEBUG','i_Fixed_Discount_Item',i_Fixed_Discount_Item);
		        						if(i_Fixed_Discount_Item == '5')
		        							{
		        							i_Fixed_Discount_Item='2435';
		        							}
		        						else
		        							{
		        							i_Fixed_Discount_Item='1570';
		        							}
		        					
		        					
		        						var i_Fixed_Discount_Rate=parseFloat(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fpdiscountrate',ss));
		        						//nlapiLogExecution('DEBUG','i_Fixed_Discount_Rate',i_Fixed_Discount_Rate);
		        						//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		        						
		        							//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		        							var i_Fixed_Remittence=(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fpremittancemethod',ss));
		        							//nlapiLogExecution('DEBUG','i_Fixed_Remittence',i_Fixed_Remittence);
		        							
		        							/*if(i_Fixed_Remittence == '1')
		        								{
		        									b_result=true;
		        								}
		        							else if(i_Fixed_Remittence == '2')
		        								{
		        									b_result=true;
		        								}*/		
		        						
		        							var s_Fixed_Discount_validity=(o_Customer_Load.getLineItemValue('recmachcustrecord_fpcustparent','custrecord_fddiscountvalidity',ss));
		        							//nlapiLogExecution('DEBUG','s_Fixed_Discount_validity',s_Fixed_Discount_validity);	
		        							
		        							//i_Fixed_Discount_Rate_Sum=i_Fixed_Discount_Rate_Sum+parseFloat(i_Fixed_Discount_Rate);
		        							//nlapiLogExecution('DEBUG','i_Fixed_Discount_Rate_Sum',i_Fixed_Discount_Rate_Sum);
		        			
		        					if(i_Fixed_Remittence == '1' || i_Fixed_Remittence == '2')
		        						{
		        										
		        							if(s_Fixed_Discount_validity == '1')
		        							{//6 if start
		        								//---pick date from invoice record
		        								var d_Fixed_Inv_Date=nlapiGetFieldValue('trandate');
		        								//nlapiLogExecution('DEBUG','d_Fixed_Inv_Date',d_Fixed_Inv_Date);
		        								var d_Fixed_Date=d_Fixed_Inv_Date;
		        																
		        							}//6 if close
		        						 else
		        							{//6 else start
		        														
		        								var d_Project_Date=nlapiGetFieldValue('trandate');//d_Project_End_Date;//project_load.getFieldValue('enddate');
		        								//alert('d_Project_Date'+d_Project_Date);
		        								var d_Fixed_Date=d_Project_Date;
		        								//alert('d_date'+d_date);
		        							}//6 else close
		        									
		        								var o_Fixed_Inv_Date= new Date(d_Fixed_Date);
		        								var d_Fixed_Month=o_Fixed_Inv_Date.getMonth()+1;
		        								//nlapiLogExecution('DEBUG','d_Fixed_Month',d_Fixed_Month);
		        								//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
		        								if(i_Fixed_Remittence == '1')
		        								{
		        									var s_condition=d_Fixed_Month == '12';
		        								}
		        							else if(i_Fixed_Remittence == '2')
		        								{
		        												//alert('inside else of second method');
		        									var s_condition=d_Fixed_Month == '3' || d_Fixed_Month == '6' || d_Fixed_Month == '9' || d_Fixed_Month == '12';
		        								}
		        								//alert('s_condition'+s_condition);
		        								if(s_condition)
		        								{//check month if start		
		        									
		        										if(i_Fixed_Remittence == '1')
		        										{
		        											//-------search all invoice created in this year with same customer
		        											var Inv_Filter=new Array();
		        											Inv_Filter[0]=new nlobjSearchFilter('entity', null,'is',S_Customer_Id);
		        											//Inv_Filter[1]=new nlobjSearchFilter('job', null, 'is',project_rec_id);
		        											var o_Invoice_Search=nlapiSearchRecord('transaction',148,Inv_Filter, null);
		        											//nlapiLogExecution('DEBUG','o_Invoice_Search.length',o_Invoice_Search.length);
		        										}
		        								 else if( i_Fixed_Remittence == '2')
		        										{
		        											//-------search all invoice created in this quater year with same customer
		        											var Inv_Filter=new nlobjSearchFilter('entity', null,'is',S_Customer_Id);
		        											var o_Invoice_Search=nlapiSearchRecord('transaction',149,Inv_Filter, null);
		        											//nlapiLogExecution('DEBUG','o_Invoice_Search.length',o_Invoice_Search.length);
		        										}
		        									
		        										if(o_Invoice_Search != null)
		        											{//7 if start
		        														     //alert('inside if');
		        												for(var ks=0 ; ks<o_Invoice_Search.length ; ks++)//o_Invoice_Search.length
		        													{//for start
		        																					 
		        														var o_Inv_Result=o_Invoice_Search[ks];
		        														var columns=o_Inv_Result.getAllColumns();
		        																					   
		        														var i_Inv_Tot_Amt=o_Inv_Result.getValue(columns[0]);
		        														//nlapiLogExecution('DEBUG','i_Inv_Tot_Amt',i_Inv_Tot_Amt);
		        																					   
		        														g_Fixed_Inv_Amt_Sum=g_Fixed_Inv_Amt_Sum+parseFloat(i_Inv_Tot_Amt);
		        														//nlapiLogExecution('DEBUG','g_Fixed_Inv_Amt_Sum',g_Fixed_Inv_Amt_Sum);
		        																	//				   
		        													}//for close
		        																		   
		        											}//7 if close
		        											else
		        											{
		        												g_Fixed_Inv_Amt_Sum=i_Subtotal_Val;
		        											}
		        											//------------------------------------------------------------------								  
		        											var i_Fixed_Amt_After_Dsc_Cal=(g_Fixed_Inv_Amt_Sum)*(i_Fixed_Discount_Rate/100);
		        											//nlapiLogExecution('DEBUG','i_Fixed_Amt_After_Dsc_Cal',i_Fixed_Amt_After_Dsc_Cal);
		        																			
		        											//-----------add volumn discount in column level---------------------------------------------
		        											
		        											var credit_memo_record = nlapiCreateRecord('creditmemo');
		        											credit_memo_record.setFieldValue('entity', S_Customer_Id);
		    		        								credit_memo_record.setFieldValue('job',o_Project_Id);
		    		        								credit_memo_record.setFieldValue('location',Location);
		    		        								
		        											credit_memo_record.selectNewLineItem('item');  
		        											credit_memo_record.setCurrentLineItemValue('item','item',i_Fixed_Discount_Item);
		        											credit_memo_record.setCurrentLineItemValue('item','custcol_discouint_amt', i_Fixed_Amt_After_Dsc_Cal);
		        											credit_memo_record.setCurrentLineItemValue('item', 'rate',i_Fixed_Amt_After_Dsc_Cal);
		        											credit_memo_record.setCurrentLineItemValue('item','price_display','Custom');
		        											
		        											credit_memo_record.commitLineItem('item');	
		        											var credit_memo_id = nlapiSubmitRecord(credit_memo_record, false);
		        											//alert('credit_memo_id'+credit_memo_id);
		        					        				nlapiLogExecution('DEBUG','credit_memo_id',credit_memo_id);
		        											//--------------------------------------------------------------------------------------------				
		        								}
		        								else
		        								{
		        								alert('Discount Not Applied');
		        								}
		        						}
		        						else if(i_Fixed_Remittence == '3')
		        						{
		        							
		        							var d_Invoice_Date=new Date(nlapiGetFieldValue('trandate'));
		        							if(s_Fixed_Discount_validity == '1')
		        								{//6 if start
		        									//---pick date from invoice record
		        														
		        									var Start_Date_Tenure=new Date(d_MSA_Start_Date);
		        									var End_Date_Tenure=new Date(d_MSA_End_Date);
		        								}//6 if close
		        							else
		        								{//6 else start
		        														
		        									var Start_Date_Tenure=new Date(d_Project_Start_Date);
		        									var End_Date_Tenure= new Date(d_Project_End_Date);
		        								}//6 else close
		        								//alert('Start_Date_Tenure'+Start_Date_Tenure);
		        								//alert('End_Date_Tenure'+End_Date_Tenure);
		        								//alert('d_Invoice_Date'+d_Invoice_Date);
		        							if(Start_Date_Tenure<=d_Invoice_Date && d_Invoice_Date<=End_Date_Tenure)
		        							{    //nlapiLogExecution('DEBUG','ss',ss);
		        								var i_Amt_After_Dsc_Cal_Fixed=(i_Subtotal_Val)*(i_Fixed_Discount_Rate/100);
		        								nlapiLogExecution('DEBUG', 'i_Amt_After_Dsc_Cal_Fixed',i_Amt_After_Dsc_Cal_Fixed);
		        								//alert('i_Amt_After_Dsc_Cal_Fixed'+i_Amt_After_Dsc_Cal_Fixed);
		        							 //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		        								//var credit_memo_record = nlapiTransformRecord('invoice',o_Invoice_Rec_Id, 'creditmemo', null);
		        								credit_memo_record.setFieldValue('entity', S_Customer_Id);
		        								credit_memo_record.setFieldValue('job',o_Project_Id);
		        								credit_memo_record.setFieldValue('location',Location);
		        								
		        								credit_memo_record.selectNewLineItem('item');  
		        								credit_memo_record.setCurrentLineItemValue('item','item',i_Fixed_Discount_Item);
		        								credit_memo_record.setCurrentLineItemValue('item','custcol_discouint_amt', i_Amt_After_Dsc_Cal_Fixed);
		        								credit_memo_record.setCurrentLineItemValue('item', 'rate',i_Amt_After_Dsc_Cal_Fixed);
		        								//credit_memo_record.setCurrentLineItemValue('item', 'amount',i_Amt_After_Dsc_Cal_Fixed);
		        								credit_memo_record.setCurrentLineItemValue('item','price_display','Custom');
		        								var i_Credit_Memo_Count=credit_memo_record.getLineItemCount('apply');
		        								nlapiLogExecution('DEBUG','i_Credit_Memo_Count',i_Credit_Memo_Count);
		        								//alert('i_Credit_Memo_Count'+i_Credit_Memo_Count);
		        							
		        								credit_memo_record.commitLineItem('item');	
		        								//var credit_memo_id = nlapiSubmitRecord(credit_memo_record, false);
		        								//nlapiLogExecution('DEBUG','credit_memo_id',credit_memo_id);
		        							 //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		        							
		        							 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		        							}
		        							else
		        							{
		        								if(s_Fixed_Discount_validity == '1')
		        								{//6 if start
		        									//throw nlobjError('E1500','Invoice Date Is Not Between MSA Start Date And MSA End Date');
		        									alert('Invoice Date Is Not Between MSA Start Date And MSA End Date');
		        														
		        								}//6 if close
		        								else
		        								{//6 else start
		        													
		        									//throw nlobjError('E1500','Invoice Date Is Not Between Project Start Date And Project End Date');
		        									alert('Invoice Date Is Not Between Project Start Date And Project End Date');
		        									
		        								}//6 else close
		        							}
		        						}
		        				}
		        				var credit_memo_id = nlapiSubmitRecord(credit_memo_record, false);
		        				//alert('credit_memo_id'+credit_memo_id);
		        				nlapiLogExecution('DEBUG','credit_memo_id',credit_memo_id);
		        			
		        		}
		        	//--------------------------------------------------------------------------------------------------------------------------------	
		        		
		            	}//main dis if close
				
			}
		}//count check else close
    return true;
}
