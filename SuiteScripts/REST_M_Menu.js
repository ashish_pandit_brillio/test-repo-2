/**
 * Menu RESTlet
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Nov 2015     nitish.mishra
 *
 */

/**
 * @param {Object}
 *            dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	nlapiLogExecution('debug', 'datain', JSON.stringify(dataIn));
	nlapiLogExecution('debug', 'Email id', dataIn.EmailId);
	var response = new Response();
	try {
		var employeeId = getUserUsingEmailId(dataIn.EmailId);

		if (employeeId) {

			// Timesheet
			var pendingTimesheetSearch = nlapiSearchRecord(
			        'customrecord_ts_quick_approval_pool',
			        'customsearch_m_pending_timesheet',
			        [ new nlobjSearchFilter('timeapprover',
			                'custrecord_qap_employee', 'anyof', employeeId) ]);

			var pendingTimesheetCount = 0;
			if (pendingTimesheetSearch) {
				pendingTimesheetCount = pendingTimesheetSearch[0].getValue(
				        'internalid', null, 'count');
			}

			// Expenses
			var pendingExpenseSearch = nlapiSearchRecord('expensereport',
			        'customsearch_m_pending_expenses', [ new nlobjSearchFilter(
			                'custbody1stlevelapprover', null, 'anyof',
			                employeeId) ]);

			var pendingExpenseCount = 0;
			var totalExpensePendingAmount = 0;
			if (pendingExpenseSearch) {
				pendingExpenseCount = pendingExpenseSearch[0].getValue(
				        'internalid', null, 'count');
				totalExpensePendingAmount = pendingExpenseSearch[0].getValue(
				        'netamount', null, 'sum');
			}

			// PR Item
			/*
			 * var pendingPRSearch = nlapiSearchRecord('customrecord_pritem',
			 * 'customsearch_m_pending_pr', [ [ [ 'custrecord_prastatus',
			 * 'anyof', '2' ], 'and', [ 'custrecord_subpracticehead', 'anyof',
			 * employeeId ] ], 'or', [ [ 'custrecord_prastatus', 'anyof', '30' ],
			 * 'and', [ 'custrecord_pri_delivery_manager', 'anyof', employeeId ] ]
			 * ]);
			 * 
			 * var pendingPrCount = 0; if (pendingPRSearch) { pendingPrCount =
			 * pendingPRSearch[0].getValue('internalid', null, 'count'); }
			 */

			// travel
			var pendingTravelSearch = nlapiSearchRecord(
			        'customrecord_travel_request',
			        'customsearch_m_pending_travel', [ new nlobjSearchFilter(
			                'custrecord_tr_approving_manager_id', null, 'is',
			                employeeId) ]);
			var pendingTravelCount = 0;
			if (pendingTravelSearch) {
				pendingTravelCount = pendingTravelSearch[0].getValue(
				        'internalid', null, 'count');
			}

			// reportee count
			var reporteeSearch = nlapiSearchRecord('employee', null, [
			        new nlobjSearchFilter('custentity_implementationteam',
			                null, 'is', 'F'),
			        new nlobjSearchFilter('custentity_employee_inactive', null,
			                'is', 'F'),
			        new nlobjSearchFilter('custentity_reportingmanager', null,
			                'anyof', employeeId) ], [ new nlobjSearchColumn(
			        'internalid', null, 'count') ]);

			var reporteeCount = 0;
			if (reporteeSearch) {
				reporteeCount = reporteeSearch[0].getValue('internalid', null,
				        'count');
			}

			// get count of active allocation
			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('resource', null, 'anyof',
			                        employeeId),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        'today'),
			                new nlobjSearchFilter('startdate', null,
			                        'notafter', 'today') ],
			        [ new nlobjSearchColumn('internalid', null, 'count') ]);

			var allocationCount = 0;
			if (allocationSearch) {
				allocationCount = allocationSearch[0].getValue('internalid',
				        null, 'count');
			}

			// get project count
			var projectSearch = nlapiSearchRecord('job', null,
			        [
			                [ 'isinactive', 'is', 'F' ],
			                'and',
			                [ [ 'status', 'anyof', 2 ], 'or',
			                        [ 'status', 'anyof', 4 ] ],
			                'and',
			                [
			                        [ 'custentity_projectmanager', 'anyof',
			                                employeeId ],
			                        'or',
			                        [ 'custentity_deliverymanager', 'anyof',
			                                employeeId ] ] ],
			        [ new nlobjSearchColumn('internalid', null, 'count') ]);

			var projectCount = 0;
			var hasProjectListAccess = false;
			if (projectSearch) {
				projectSearch = projectSearch[0].getValue('internalid', null,
				        'count');
				hasProjectListAccess = true;
			}

			// Holiday Calendar (Get the next holiday)
			var employeeLocation = nlapiLookupField('employee', employeeId,
			        'location');

			var locationArray = [ employeeLocation ];
			var currentLocation = employeeLocation;

			while (currentLocation) {
				var locationRec = nlapiLoadRecord('location', currentLocation);
				currentLocation = locationRec.getFieldValue('parent');

				if (currentLocation) {
					locationArray.push(currentLocation);
				}
			}

			var searchHoliday = nlapiSearchRecord(
			        'customrecord_mobile_holiday_calendar',
			        null,
			        [
			                new nlobjSearchFilter('custrecord_mhc_location',
			                        null, 'anyof', locationArray),
			                new nlobjSearchFilter('custrecord_mhc_date', null,
			                        'onorafter', 'today'),
			                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
			        [ new nlobjSearchColumn('custrecord_mhc_date').setSort(),
			                new nlobjSearchColumn('custrecord_mhc_title') ]);

			var nextHoliday = "";
			var pendingHolidayCount = 0;
			if (searchHoliday) {
				pendingHolidayCount = searchHoliday.length;
				nextHoliday = searchHoliday[0].getValue('custrecord_mhc_date');
			}

			// response
			response.Data = [
			        new AccessAllowed(pendingTimesheetCount, true, "",
			                'img/timesheet-approval.png', 'Timesheet Approval',
			                'TimesheetApproval', 'Pending', '/timesheet'),
			        new AccessAllowed(pendingExpenseCount, true,
			                totalExpensePendingAmount,
			                'img/expense-approval.png', 'Expense Approval',
			                'ExpenseApproval', 'Pending', '/expense'),
			        // new AccessAllowed(pendingPrCount, true, "",
			        // 'img/Procurement-approval.png', 'PR Approval',
			        // 'ProcurementApproval', 'Pending', '/prapproval'),
			        new AccessAllowed(pendingTravelCount, true, "",
			                'img/travel-approval.png', 'Travel Approval',
			                'TravelApproval', 'Pending', '/trapproval'),
			        new AccessAllowed(reporteeCount, true, "",
			                'img/active-person.png', 'Hierarchy',
			                'EmployeeHierarchy', 'Reportee',
			                '/employeehierarchy'),
			        new AccessAllowed('', true, "",
			                'img/conferencecall-white.png', 'Dial-In Bridge',
			                'WebConference', '', '/conference'),

			        // April
			        new AccessAllowed(allocationCount, true, "",
			                'img/Allocation-details.png', 'My Allocation',
			                'Allocation', 'active', 'allocationHistory'),

			        new AccessAllowed(projectCount, hasProjectListAccess, "",
			                'img/project.png', 'Project Details',
			                'ProjectDetails', 'active', '/projectDetails'),

			        new AccessAllowed(pendingHolidayCount, true, "",
			                'img/holiday-calander.png', 'Holiday Calendar',
			                'HolidayCalender', nextHoliday, '/holidayCalender') ];

			/*
			 * ,
			 * 
			 * new AccessAllowed('', true, "", 'img/conferencecall-white.png',
			 * 'Change AD Password', 'WebConference', '', '/conference')
			 */
			response.Status = true;
		} else {
			throw "No Employee Id Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
	}
	return response;
}