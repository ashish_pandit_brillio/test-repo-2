/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Nov 2019     Nihal Mulani Cell: 9423591358
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) 
{
	nlapiLogExecution("debug","dataIn:",dataIn);
	var data =dataIn;// JSON.parse(dataIn);
	var employeeId = getUserUsingEmailId(data.userEmailId); // Call_Library: M_Utility.js
	nlapiLogExecution("debug","employeeId:",employeeId);
	var errorObj = [];
	var addList = data.addSkillList;
	var addSkillList = [];
	if(addList.length>0)
	{
		for(var i=0;i<addList.length;i++)
		{
			try {
				var empSkill = nlapiCreateRecord('customrecord_emp_skill_master_data');
				empSkill.setFieldValue('custrecord_skill_resource', employeeId);
				empSkill.setFieldValue('custrecord_resource_skillset', addList[i].skillId);
				empSkill.setFieldValue('custrecord_resource_proficency', addList[i].proficiencyLevel);
				var empSkillId = nlapiSubmitRecord(empSkill);
				nlapiLogExecution('debug', 'skill is created', empSkillId);
				addSkillList.push({"recordInternalId":empSkillId,"skillId":addList[i].skillId,"proficiencyLevel": addList[i].proficiencyLevel,"status":true});
			} catch (err) {
				nlapiLogExecution('ERROR', 'addList', err);
				addSkillList.push({"skillId":addList[i].skillId,"proficiencyLevel":addList[i].proficiencyLevel,"status":false});
			}
		}
	}
	var editList = data.editSkillList;
	var editSkillList = [];
	if(editList.length>0)
	{
		for(var i=0;i<editList.length;i++)
		{
			try {
				var skillId = editList[i].skillId;
				var proficiencyLevel = editList[i].proficiencyLevel;
				/*var editSkillListSearch = nlapiSearchRecord('customrecord_emp_skill_master_data', null, [
					new nlobjSearchFilter('custrecord_skill_resource', null, 'is', employeeId),
					new nlobjSearchFilter('custrecord_resource_skillset', null, 'is', skillId)]);

				var record_id = editSkillListSearch[0].getId();
				nlapiLogExecution('debug', 'Update record_id', record_id);
				if(record_id)
				{*/
					var eid = nlapiSubmitField('customrecord_emp_skill_master_data',editList[i].recordInternalId,['custrecord_resource_proficency','custrecord_resource_skillset'],[proficiencyLevel,skillId]);
					editSkillList.push({"recordInternalId":eid,"status":true});
				
			} catch (err) {
				nlapiLogExecution('ERROR', 'editList', err);
				editSkillList.push({"recordInternalId":editList[i].recordInternalId,"status":false});
			}
		}
	}
	var deleteList = data.deleteSkillList;
	var deleteSkillList = [];
	if(deleteList.length>0)
	{
		try{
			for(var i=0;i<deleteList.length;i++)
			{
				var skillId = deleteList[i].skillId;
				/*var deleteSkillListSearch = nlapiSearchRecord('customrecord_emp_skill_master_data', null, [
					new nlobjSearchFilter('custrecord_skill_resource', null, 'is', employeeId),
					new nlobjSearchFilter('custrecord_resource_skillset', null, 'is', skillId)]);

				var record_id = deleteSkillListSearch[0].getId();
				nlapiLogExecution('debug', 'deleteList record_id', record_id);
				if(record_id)
				{*/
				var deleteRec = nlapiDeleteRecord ('customrecord_emp_skill_master_data', deleteList[i].recordInternalId );
				nlapiLogExecution('ERROR', 'deleteRec', deleteRec);
				deleteSkillList.push({"recordInternalId":deleteList[i].recordInternalId,"status":true});
			}
		}
		catch(err)
		{
			nlapiLogExecution('ERROR', 'deleteList', err);
			deleteSkillList.push({"recordInternalId":deleteList[i].recordInternalId,"status":false});
		}
	}
	
	return {"addSkillList":addSkillList,"editSkillList":editSkillList,"deleteSkillList":deleteSkillList}
}
