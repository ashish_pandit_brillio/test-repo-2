//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
     Script Name: UES Disable MOVE Task
     Author: 
     Company: 
     Date: 
     Script Modification Log:
     -- Date --               -- Modified By --                  --Requested By--                   -- Description --
      16-03-2020				Praveena madem					  Deepak Ms						 Changed Intenalid of timesheet siblist ids and iternal id's of fields
	   
	   Below is a summary of the process controls enforced by this script file.  The control logic is described
     more fully, below, in the appropriate function headers and code blocks.
     BEFORELOAD FUNCTION
     - userEventBeforeLoad()
     SUB-FUNCTIONS
     - The following sub-functions are called by the above core functions in order to maintain code
     modularization:
     - NOT USED
	 */
}
//END SCRIPT DESCRIPTION BLOCK  ====================================

function userEventBeforeLoad(type,form){
try{
 nlapiLogExecution('debug','type',nlapiGetRole());
//var sublist = form.getSubList('timeitem');

if(nlapiGetRole() != 1094 && nlapiGetRole() != 3)
{
	form.getSubList('timeitem').getField('custcol_te_move_sub_task_tb').setDisplayType('disabled');
  form.getSubList('timeitem').getField('item').setDisplayType('disabled');
}
 }
 catch(e){
 nlapiLogExecution('debug','Disable Function Error',e);
 }
}