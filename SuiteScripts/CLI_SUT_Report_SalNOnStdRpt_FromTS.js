// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================

/*
		Script Name:	CLI_SUT_Report_SalNOnStdRpt_FromTS.js
		Author: 		Inspirria Cloud tech
		Created Date:	22-07-2020
		Description:	This script is used for Saturday Validation on Suitelet and Generating CSV file.
	
		
		Script Modification Log:
	
		-- Date --		-- Modified By --			--Requested By--				-- Description --
		
		

}*/
// END SCRIPT DESCRIPTION BLOCK  ====================================
function callGenerateExcel() {
    var head = 'GCI(INC) ID, Fusion ID ,First Name ,Last Name,WK1 ST ,WK1 OT,WK1 DT,WK1 TIMEOFF, WK1 HOLIDAY ,WK2 ST,WK2 OT, WK2 DT,WK2 TIMEOFF, WK2 HOLIDAY , OT Hours, Total Timeoff, Total DT, Float H(if in either wk), Assignment Category, Department, FLSA Code\n';
    var contents = '';
    var strvalues = '';
    var str = '';

    //Getting value from item line level
    var sublist_line_count = nlapiGetLineItemCount('record');
    nlapiLogExecution('DEBUG', 'Line_Level_Log', 'sublist_line_count = ' + sublist_line_count);
    var lineCount = sublist_line_count;
    //alert('lineCount'+lineCount);
    for (var i = 1; i <= lineCount; i++) {

        var employee_inc_id = nlapiGetLineItemValue('record', 'custevent_file', i);
        var employee_id = nlapiGetLineItemValue('record', 'custevent_employeeid', i);
        var employee_F_name = nlapiGetLineItemValue('record', 'custevent_firstname', i);
        var employee_L_name = nlapiGetLineItemValue('record', 'custevent_lastname', i);
        var wk1_st = nlapiGetLineItemValue('record', 'custevent_wkst1', i);
        var wl1_ot = nlapiGetLineItemValue('record', 'custevent_wkot1', i);
        var wk1_dt = nlapiGetLineItemValue('record', 'custevent_wkdt1', i);
        var wk1_timeoff = nlapiGetLineItemValue('record', 'custevent_wk1timeoff1', i);
        var wk1_holiday = nlapiGetLineItemValue('record', 'custevent_holidayweek1', i);
        var wk2_st = nlapiGetLineItemValue('record', 'custevent_wkst2', i);
        var wk2_ot = nlapiGetLineItemValue('record', 'custevent_wkot2', i);
        var wk2_dt = nlapiGetLineItemValue('record', 'custevent_wkdt2', i);
        var wk2_timeoff = nlapiGetLineItemValue('record', 'custevent_wk1timeoff2', i);
        var wk2_holiday = nlapiGetLineItemValue('record', 'custevent_holidayweek2', i);
        var ot_hours_total = nlapiGetLineItemValue('record', 'custevent_othours', i);
        var total_timeoff = nlapiGetLineItemValue('record', 'custevent_totaltimeoff', i);
        var total_dt = nlapiGetLineItemValue('record', 'custevent_totaldttime', i);
        var float_h = nlapiGetLineItemValue('record', 'custevent_floatingh', i);
        // var project_city = nlapiGetLineItemValue('record','custevent_projectcity',i);
        // var project_location = nlapiGetLineItemValue('record','custevent_projectloc',i);
        var employee_type = nlapiGetLineItemValue('record', 'custevent_employeetype', i);
        var e_department = nlapiGetLineItemValue('record', 'custevent_department', i);
        var flsa_code = nlapiGetLineItemValue('record', 'custevent_flsa_code', i);
        // var e_customer = nlapiGetLineItemValue('record','custevent_customer',i);

        //Dynamic Values
        strvalues = strvalues + employee_inc_id + ',' + employee_id + ',' + employee_F_name + ',' + employee_L_name + ',' + wk1_st + ',' + wl1_ot + ',' + wk1_dt + ',' + wk1_timeoff + ',' + wk1_holiday + ',' + wk2_st + ',' + wk2_ot + ',' + wk2_dt + ',' + wk2_timeoff + ',' + wk2_holiday + ',' + ot_hours_total + ',' + total_timeoff + ',' + total_dt + ',' + float_h + ',' + employee_type + ',' + e_department + ',' + flsa_code +'\n';
    }

    //alert('i am exist');
    if (strvalues.length > 0) {
        str = str + head + contents + strvalues;
    }

    str1 = encodeURIComponent(str);
    var uri = 'data:text/excel;charset=utf-8,\ufeff' + str1;
    var downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = "Sal-Non-Std Rpt-From-TS.csv";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}

//Only saturday user will select on suitelet
function fieldChanged(type, name, linenum) {
    if (name == 'custpage_startdate') {
        var s_period = nlapiGetFieldValue('custpage_startdate')
        //alert("S_period=======" + s_period)
        if (s_period != null && s_period != '' && s_period != 'undefined') {
            var date = nlapiStringToDate(s_period)
            //alert("date=======" + date)
            if (date != null && date != '' && date != 'undefined') {
                var day = date.getDay()
                //alert("day=======" + day)
                if (day != 6) {
                    alert("Please select enddate as Saturday.")
                    nlapiSetFieldValue('custpage_startdate', '')
                }
            }
        }
    }
}