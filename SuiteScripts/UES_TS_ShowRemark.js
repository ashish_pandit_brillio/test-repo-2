function beforeLoad(type, form) {
	try {
		var user_role = nlapiGetRole();

		// if user is admin or full access then show
		if (user_role == 3 || user_role == 18) {
			var first_approver = form.getField('custrecord_ts_remark');
			first_approver.setDisplayType('normal');

			nlapiLogExecution('debug', 'type', type);

			var personTypeField = form.addField('custpage_person_type',
			        'select', 'Employee Type', 'customlist_persontype')
			        .setDisplayType('inline');

			var employeeType = nlapiLookupField('employee',
			        nlapiGetFieldValue('employee'), 'custentity_persontype');
			personTypeField.setDefaultValue(employeeType);

		} else {
			var first_approver = form.getField('custrecord_ts_remark');
			first_approver.setDisplayType('hidden');
			form.removeButton('delete');
		}

		nlapiLogExecution('debug', 'center', nlapiGetContext().getRoleCenter());

		// check the center of the logged user
		if (nlapiGetContext().getRoleCenter() == 'EMPLOYEE') {
			form.getField('custrecord_ts_emp_category')
			        .setDisplayType('hidden');
		} else {

		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'beforeLoad', err);
	}
}

function setEmployeeType(type) {
	try {

		if (type == 'create') {
			var employeeType = nlapiLookupField('employee',
			        nlapiGetFieldValue('employee'), 'custentity_persontype');
			nlapiSetFieldValue('custrecord_ts_emp_category', employeeType);
			nlapiLogExecution('debug', 'emp category field set');
		}

	} catch (err) {
		nlapiLogExecution('ERROR', 'setEmployeeType', err);
	}
}