/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Jan 2017     deepak.srinivas
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type) {
	try {
		var context = nlapiGetContext();
		var fusionNumber = '';
		var counter = '';
		var currentDate = nlapiStringToDate(nlapiDateToString(new Date()));
		nlapiLogExecution('DEBUG', 'Execution Started', currentDate);

		var today = new Date();
		var date_yesterday = nlapiAddDays(today, -1);
		var dd = today.getDate();
		var mm = today.getMonth() + 1;
		var yyyy = today.getFullYear();
		var hours = today.getHours();
		var minutes = today.getMinutes();

		if (dd < 10) {
			dd = '0' + dd
		}
		if (mm < 10) {
			mm = '0' + mm
		}
		var today = dd + '/' + mm + '/' + yyyy;

		var strVar_excel = '';

		strVar_excel += '<html>';
		strVar_excel += '<body>';

		strVar_excel += '<p>Fusion Error Log ' + date_yesterday + '</p>';

		strVar_excel += '<table>';

		strVar_excel += '	<tr>';
		strVar_excel += ' <td width="100%">';
		strVar_excel += '<table width="100%" border="1">';

		strVar_excel += '	<tr>';
		strVar_excel += ' <td width="3%" font-size="11" align="center" style="color:blue;"><b>Sl No.</b></td>';
		strVar_excel += ' <td width="3%" font-size="11" align="center" style="color:blue;"><b>Record ID</b></td>';
		strVar_excel += ' <td width="3%" font-size="11" align="center" style="color:blue;"><b>Fusion ID</b></td>';
		strVar_excel += ' <td width="3%" font-size="11" align="center" style="color:blue;"><b>Date Of Extract</b></td>';
		strVar_excel += ' <td width="3%" font-size="11" align="center" style="color:blue;"><b>Type</b></td>';
		strVar_excel += ' <td width="6%" font-size="11" align="center" style="color:blue;"><b>Error Log Details</b></td>';
		strVar_excel += '	</tr>';

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');
		columns[1] = new nlobjSearchColumn('custrecord_fi_fusion_xml_data');
		columns[2] = new nlobjSearchColumn('custrecord_fi_date_of_fusion_extract');
		columns[3] = new nlobjSearchColumn('custrecord_fi_type');
		columns[4] = new nlobjSearchColumn('custrecord_fi_error_log');

		var fusionExtract_search = nlapiSearchRecord('customrecord_fi_employee_xml_master', 'customsearch1715', null, columns);
		if (fusionExtract_search) {
			for (var i = 0; i < fusionExtract_search.length; i++) {
					counter = parseInt(i) + 1;
					//var dataExtract = fusionExtract_search[i].getValue('custrecord_fi_fusion_xml_data');
					var date = fusionExtract_search[0].getValue('custrecord_fi_date_of_fusion_extract');
					var xmlData = nlapiStringToXML(fusionExtract_search[i].getValue('custrecord_fi_fusion_xml_data'));
					var record_type = fusionExtract_search[i].getValue('custrecord_fi_type');
					if (record_type == 3) {
						fusionNumber = nlapiSelectValue(xmlData, '//Assignment/FusionID');
					}
					else {
						fusionNumber = nlapiSelectValue(xmlData, '//Assignment/PersonNumber');
					}
					if (!_logValidation(fusionNumber)) {
						fusionNumber = '';
					}
					strVar_excel += '	<tr>';
					strVar_excel += ' <td width="3%" font-size="11" align="center">' + counter + '</td>';
					strVar_excel += ' <td width="3%" font-size="11" align="center">' + fusionExtract_search[i].getValue('internalid') + '</td>';
					strVar_excel += ' <td width="3%" font-size="11" align="center">' + fusionNumber + '</td>';
					strVar_excel += ' <td width="3%" font-size="11" align="center">' + fusionExtract_search[i].getValue('custrecord_fi_date_of_fusion_extract') + '</td>';
					strVar_excel += ' <td width="3%" font-size="11" align="center">' + fusionExtract_search[i].getText('custrecord_fi_type') + '</td>';
					strVar_excel += ' <td width="6%" font-size="11" align="center">' + fusionExtract_search[i].getValue('custrecord_fi_error_log') + '</td>';
					strVar_excel += '	</tr>';
			}


		}
		strVar_excel += '</table>';
		strVar_excel += ' </td>';
		strVar_excel += '	</tr>';

		strVar_excel += '</table>';
		strVar_excel += '<p>Thanks & Regards,</p>';
		strVar_excel += '<p>Team Digital Office</p>';

		strVar_excel += '</body>';
		strVar_excel += '</html>';


		//Send Email
		var author = 442;
		var recipient = new Array();
		recipient[0] = 'deepak.srinivas@brillio.com';
		recipient[1] ='sridhar.v@BRILLIO.COM';
		var cc = 'information.systems@brillio.com';
		var subject = 'Fusion Data Error Log' + ' - ' + date_yesterday;

		nlapiSendEmail(author, recipient, subject, strVar_excel, cc, null, null, null);
		nlapiLogExecution('debug', 'Mail Sent to User', 'USER ->' + recipient);
	}
	catch (err) {
		nlapiLogExecution('DEBUG', 'Processing Error', err);
		Send_Exception_Mail(err);
		
	}

}
function _logValidation(value) {
	if (value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
		return true;
	}
	else {
		return false;
	}
}

function Send_Exception_Mail(err)
{​​​​​
    try
    {​​​​​   
        var context = nlapiGetContext();
        var s_DeploymentID =context.getDeploymentId();
        var s_ScriptID =context.getScriptId();
        var s_Subject ='Failure Notification on Fusion Data - Error Log';
        
        var s_Body = 'This is to inform that Rev Projection Project Wise is having an issue and System is not able to send the email notification to employees.';
        s_Body += '<br/><b>Issue: Code:</b> '+err.code+' Message: '+err.message;
        s_Body += '<br/><b>Script: </b>'+s_ScriptID;
        s_Body += '<br/><b>Deployment:</b> '+s_DeploymentID;
        s_Body += '<br/>Please have a look and do the needfull.';
        s_Body += '<br/><br/>Note: This is system genarated email incase of any failure while sending the notification to employees';

        s_Body += '<br/><br/>Regards,';
        s_Body += '<br/>Digital Office';
        nlapiSendEmail(442,"netsuite.support@brillio.com",s_Subject,s_Body,["deepak.srinivas@brillio.com","sridhar.v@BRILLIO.COM"], null, null, null);
    }​​​​​
    catch(err)
    {​​​​​
        nlapiLogExecution('error',' Send_Exception_Mail', err.message); 
		
    }​​​​​
}​​​​​

