/**
 * @author Jayesh
 */

function scheduled_confirmation_reminder()
{
	try
	{
		var o_freeze_date = nlapiLoadRecord('customrecord_fp_rev_rec_freeze_date',1);
		if(o_freeze_date)
		{
			var s_freeze_date = o_freeze_date.getFieldValue('custrecord_freeze_date');
			var i_freeze_day = nlapiStringToDate(s_freeze_date).getDate();
			var i_freeze_month = nlapiStringToDate(s_freeze_date).getMonth();
			
			var d_today_date = new Date();
			var i_today_date = d_today_date.getDate();
			var i_today_month = d_today_date.getMonth();
			var i_today_year = d_today_date.getFullYear();
			
			if(parseInt(i_freeze_month) != parseInt(i_today_month))
				i_freeze_day = 25;
			
			i_today_month = parseInt(i_today_month) + parseInt(1);
			
			nlapiLogExecution('audit','i_freeze_day:- '+i_freeze_day,'i_today_date:- '+i_today_date);
			if(i_today_date < 20)
			{
				nlapiLogExecution('audit','freeze date trigger date yet to come');
				return;
			}
			
			if(i_today_date >= i_freeze_day)
			{
				nlapiLogExecution('audit','freeze date trigger crossed');
				return;
			}
		}
		var date = new Date();
		var firstDayOfCurrentMnth = new Date(date.getFullYear(), date.getMonth(), 1);
		firstDayOfCurrentMnth = nlapiDateToString(firstDayOfCurrentMnth, 'mm/dd/yyyy');
		var o_context = nlapiGetContext();
		var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', [1,4]], 'and' ,
								['enddate', 'onOrAfter', firstDayOfCurrentMnth], 'and' ,
								['internalid', 'noneof', 56606]];
		
		/*var a_project_filter = [['status', 'noneof', 1], 'and',
								['jobtype', 'anyof', 2], 'and',
								['jobbillingtype', 'anyof', 'FBM'], 'and',
								['custentity_fp_rev_rec_type', 'anyof', 1]];*/
		
		var a_columns_proj_srch = new Array();
		a_columns_proj_srch[0] = new nlobjSearchColumn('custentity_projectvalue');
		a_columns_proj_srch[1] = new nlobjSearchColumn('companyname');
		a_columns_proj_srch[2] = new nlobjSearchColumn('custentity_fp_rev_rec_type');
		a_columns_proj_srch[3] = new nlobjSearchColumn('custrecord_practicehead','custentity_practice');
		a_columns_proj_srch[4] = new nlobjSearchColumn('entityid');
		a_columns_proj_srch[5] = new nlobjSearchColumn('customer');
		a_columns_proj_srch[6] = new nlobjSearchColumn('custentity_region');
		a_columns_proj_srch[7] = new nlobjSearchColumn('enddate');
			
		var a_project_search_results = searchRecord('job', null, a_project_filter, a_columns_proj_srch);
		
		if (a_project_search_results)
		{
			for (var i_pro_index = 0; i_pro_index < a_project_search_results.length; i_pro_index++)
			{
				var pro_type=a_project_search_results[i_pro_index].getValue('custentity_fp_rev_rec_type');
				
				if(o_context.getRemainingUsage() <= 500)
				{
					nlapiYieldScript();
				}
				if(pro_type==1)
				{
					var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', a_project_search_results[i_pro_index].getId()]];
						
					var a_column = new Array();
					a_column[0] = new nlobjSearchColumn('created').setSort(true);
					a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
					a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
					var d = new Date();
					var n = d.getMonth();
					var firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
					var lastDay = new Date(d.getFullYear(), d.getMonth() + 1, 0);
					firstDay = nlapiDateToString(firstDay, 'mm/dd/yyyy');
					lastDay = nlapiDateToString(lastDay, 'mm/dd/yyyy');
					
					var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
					if (a_get_logged_in_user_exsiting_revenue_cap)
					{
						var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
						
						var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)],
																		  'and' ,
																 ['custrecord_is_mnth_effrt_confirmed', 'is','T'], 'and' ,['created', 'within',[firstDay , lastDay]],'and' ,['custrecord_current_mnth_no', 'equalto',n],];
						
						var a_columns_mnth_end_effort_activity_srch = new Array();
						a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
						a_columns_mnth_end_effort_activity_srch[1] = new nlobjSearchColumn('custrecord_is_mnth_effrt_confirmed');
						a_columns_mnth_end_effort_activity_srch[2] = new nlobjSearchColumn('custrecord_current_mnth_no');
						
						
						var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
						if (a_get_mnth_end_effrt_activity)
						{
						
						}
						else
						{
							{	
								var a_recipient_mail_id = new Array();
								
								var a_projectData = nlapiLookupField('job', a_project_search_results[i_pro_index].getId(), [
											'custentity_projectmanager', 'custentity_deliverymanager',
											'customer', 'custentity_clientpartner']);
											
								var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
								var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
								var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
								var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
								var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
								
								if(s_recipient_mail_pm)
								{
									a_recipient_mail_id.push(s_recipient_mail_pm);
								}
								if(s_recipient_mail_dm)
								{
									a_recipient_mail_id.push(s_recipient_mail_dm);
								}
								if(s_recipient_mail_cp)
								{
									a_recipient_mail_id.push(s_recipient_mail_cp);
								}
								if(s_recipient_mail_acp)
								{
									a_recipient_mail_id.push(s_recipient_mail_acp);
								}
				
								var a_participating_mail_id = new Array();
								a_participating_mail_id.push('billing@brillio.com');
								a_participating_mail_id.push('team.fpa@brillio.com');
				
								var strVar = '';
								strVar += '<html>';
								strVar += '<body>';
								
								strVar += '<p>Hello PM,</p>';
								strVar += '<p>Please confirm the effort in the Netsuite system for the following fixed price project before '+i_today_month+'/'+i_freeze_day+'/'+i_today_year+' to ensure revenue recognition.</p>';
								strVar += '<p>Please note that the module freeze date is '+i_today_month+'/'+i_freeze_day+'/'+i_today_year+', post which no efforts will be allowed to be submitted and hence no revenue will be recognised.<p>';
								strVar += '<p>Kindly ignore if already done.</p>';
								
								strVar += '<p>Region:- '+a_project_search_results[i_pro_index].getText('custentity_region')+'<br>';
								strVar += 'Customer:- '+a_project_search_results[i_pro_index].getText('customer')+'<br>';
								strVar += 'Project:- '+a_project_search_results[i_pro_index].getValue('entityid')+' '+a_project_search_results[i_pro_index].getValue('companyname')+'</p>';
								
								strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1138&deploy=1>Link to Netsuite to confirm Monthly Effort</a>';
								
								strVar += '<p>Thanks & Regards,</p>';
								strVar += '<p>Team IS</p>';
									
								strVar += '</body>';
								strVar += '</html>';
								
								nlapiSendEmail(442, a_recipient_mail_id, 'FP Project Monthly effort confirmation reminder: '+a_project_search_results[i_pro_index].getValue('entityid')+' '+a_project_search_results[i_pro_index].getValue('companyname'),strVar,a_participating_mail_id,'bidhi.raj@brillio.com');
								//nlapiSendEmail(442, 'bidhi.raj@brillio.com', 'FP Project Monthly effort confirmation reminder: '+a_project_search_results[i_pro_index].getValue('entityid')+' '+a_project_search_results[i_pro_index].getValue('companyname'), strVar, null, null, null, null, true, null, null);
								nlapiLogExecution('audit','email sent for month end plan confirmation:- ');
							}
						}
					}
				}
				if(pro_type==parseInt(4))
				{
					var a_revenue_cap_filter = [['custrecord_fp_rev_rec_others_projec', 'anyof', a_project_search_results[i_pro_index].getId()]];
						
					var a_column = new Array();
					a_column[0] = new nlobjSearchColumn('created').setSort(true);
					a_column[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_auto_numb');
					a_column[2] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
					var d = new Date();
					var n = d.getMonth();
					var firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
					var lastDay = new Date(d.getFullYear(), d.getMonth() + 1, 0);
					firstDay = nlapiDateToString(firstDay, 'mm/dd/yyyy');
					lastDay = nlapiDateToString(lastDay, 'mm/dd/yyyy');
					
					var a_get_logged_in_user_exsiting_revenue_cap_other = nlapiSearchRecord('customrecord_fp_rev_rec_others_parent', null, a_revenue_cap_filter, a_column);
					if (a_get_logged_in_user_exsiting_revenue_cap_other)
					{
						var i_revenue_share_id_other = a_get_logged_in_user_exsiting_revenue_cap_other[0].getId();
						
						var a_effort_activity_mnth_end_filter = [['custrecord_fp_others_mnth_end_fp_parent', 'anyof', parseInt(i_revenue_share_id_other)],
																		  'and' ,
																 ['custrecord_is_mnth_end_cfrmd', 'is','T'], 'and' ,['created', 'within',[firstDay , lastDay]],'and' ,['custrecord_other_current_mnth_no', 'equalto',n]];
						
						var a_columns_mnth_end_effort_activity_srch = new Array();
						a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
						a_columns_mnth_end_effort_activity_srch[1] = new nlobjSearchColumn('custrecord_is_mnth_end_cfrmd');
						a_columns_mnth_end_effort_activity_srch[2] = new nlobjSearchColumn('custrecord_other_current_mnth_no');
						
						
						var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_others_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
						if (a_get_mnth_end_effrt_activity)
						{
						
						}
						else
						{
							{	
								var a_recipient_mail_id = new Array();
								
								var a_projectData = nlapiLookupField('job', a_project_search_results[i_pro_index].getId(), [
											'custentity_projectmanager', 'custentity_deliverymanager',
											'customer', 'custentity_clientpartner']);
											
								var s_recipient_mail_pm = nlapiLookupField('employee',a_projectData.custentity_projectmanager,'email');
								var s_recipient_mail_dm = nlapiLookupField('employee',a_projectData.custentity_deliverymanager,'email');
								var s_recipient_mail_cp = nlapiLookupField('employee',a_projectData.custentity_clientpartner,'email');
								var i_client_cp = nlapiLookupField('customer',a_projectData.customer,'custentity_clientpartner');
								var s_recipient_mail_acp = nlapiLookupField('employee',i_client_cp,'email');
								
								if(s_recipient_mail_pm)
								{
									a_recipient_mail_id.push(s_recipient_mail_pm);
								}
								if(s_recipient_mail_dm)
								{
									a_recipient_mail_id.push(s_recipient_mail_dm);
								}
								if(s_recipient_mail_cp)
								{
									a_recipient_mail_id.push(s_recipient_mail_cp);
								}
								if(s_recipient_mail_acp)
								{
									a_recipient_mail_id.push(s_recipient_mail_acp);
								}
				
								var a_participating_mail_id = new Array();
								a_participating_mail_id.push('billing@brillio.com');
								a_participating_mail_id.push('team.fpa@brillio.com');
				
								var strVar = '';
								strVar += '<html>';
								strVar += '<body>';
								
								strVar += '<p>Hello PM,</p>';
								strVar += '<p>Please confirm the effort in the Netsuite system for the following fixed price project before '+i_today_month+'/'+i_freeze_day+'/'+i_today_year+' to ensure revenue recognition.</p>';
								strVar += '<p>Please note that the module freeze date is '+i_today_month+'/'+i_freeze_day+'/'+i_today_year+', post which no efforts will be allowed to be submitted and hence no revenue will be recognised.<p>';
								strVar += '<p>Kindly ignore if already done.</p>';
								
								strVar += '<p>Region:- '+a_project_search_results[i_pro_index].getText('custentity_region')+'<br>';
								strVar += 'Customer:- '+a_project_search_results[i_pro_index].getText('customer')+'<br>';
								strVar += 'Project:- '+a_project_search_results[i_pro_index].getValue('entityid')+' '+a_project_search_results[i_pro_index].getValue('companyname')+'</p>';
								
								strVar += '<a href=https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1514&deploy=1>Link to Netsuite to confirm Monthly Effort</a>';
								
								strVar += '<p>Thanks & Regards,</p>';
								strVar += '<p>Team IS</p>';
									
								strVar += '</body>';
								strVar += '</html>';
								
								nlapiSendEmail(442, a_recipient_mail_id, 'FP Project Monthly effort confirmation reminder: '+a_project_search_results[i_pro_index].getValue('entityid')+' '+a_project_search_results[i_pro_index].getValue('companyname'),strVar,a_participating_mail_id,'sai.vannamareddy@brillio.com');
								//nlapiSendEmail(442, 'sai.vannamareddy@brillio.com', 'FP Project Monthly effort confirmation reminder: '+a_project_search_results[i_pro_index].getValue('entityid')+' '+a_project_search_results[i_pro_index].getValue('companyname'), strVar, null, null, null, null, true, null, null);
								nlapiLogExecution('audit','email sent for month end plan confirmation:- ');
							}
						}
					}
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','scheduled_confirmation_reminder','ERROR MESSAGE:- '+err);
	}
}

function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
        filterExpression)
{

	try {
		var search = null;

		// if a saved search is provided, load it and add the filters and
		// columns
		if (isNotEmpty(savedSearch)) {
			search = nlapiLoadSearch(recordType, savedSearch);

			if (isArrayNotEmpty(arrFilters)) {
				search.addFilters(arrFilters);
			}

			if (isArrayNotEmpty(arrColumns)) {
				search.addColumns(arrColumns);
			}

			if (isArrayNotEmpty(filterExpression)) {
				search.setFilterExpression(filterExpression);
			}
		}
		// create a new search
		else {
			search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
		}

		// run search
		var resultSet = search.runSearch();

		// iterate through the search and get all data 1000 at a time
		var searchResultCount = 0;
		var resultSlice = null;
		var searchResult = [];

		do {
			resultSlice = resultSet.getResults(searchResultCount,
			        searchResultCount + 1000);

			if (resultSlice) {

				resultSlice.forEach(function(result) {

					searchResult.push(result);
					searchResultCount++;
				});
			}
		} while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

		return searchResult;
	} catch (err) {
		nlapiLogExecution('ERROR', 'searchRecord', err);
		throw err;
	}
}

function isEmpty(value) {

	return value == null || value == "" || typeof (value) == undefined;
}

function isNotEmpty(value) {

	return !isEmpty(value);
}

function isArrayEmpty(argArray) {

	return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

	return (isNotEmpty(argArray) && argArray.length > 0);
}