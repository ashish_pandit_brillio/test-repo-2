//https://system.na1.netsuite.com/app/common/scripting/script.nl?id=961

// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Set_Emp_type_Person_type_JE.js
	Author      : Jayesh Dinde
	Date        : 28 June 2016
	Description : 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmitRecord(type)


     AFTER SUBMIT
		- afterSubmit_Set_emp_type(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN BEFORE LOAD ==================================================

function beforeLoadRecord(type)
{

	/*  On before load:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION

		-


		FIELDS USED:

          --Field Name--				--ID--


	*/



	//  LOCAL VARIABLES

    //  BEFORE LOAD CODE BODY


	return true;



}

// END BEFORE LOAD ====================================================





// BEGIN BEFORE SUBMIT ================================================

function beforeSubmitRecord(type)
{
    /*  On before submit:

          - PURPOSE
		-

          FIELDS USED:

          --Field Name--				--ID--

    */

    //  LOCAL VARIABLES
	
	try{
	//obtain the context object
	var currentContext = nlapiGetContext();   
	if( (currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'copy'))
	{ 
	var i_recordID = nlapiGetRecordId();
	var s_record_type = nlapiGetRecordType();
			
	if(s_record_type == 'journalentry')
	{
	//Validation for expense lines
	var expense_lines = nlapiGetLineItemCount('line');
	
	for(var index = 1 ; index <= expense_lines ; index ++){
	var i_proj_id = ''; 
	var i_employee_id =	'';
	var i_practice = '';
	i_practice = nlapiGetLineItemValue('line', 'department',index);
	i_proj_id = nlapiGetLineItemValue('line', 'custcol_project_entity_id',index);
	//i_proj_id = i_proj_id.trim();
	if(_logValidation(i_proj_id)){
	var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'contains',i_proj_id);
						//filters_search_proj[1] = new nlobjSearchFilter('status',null,'anyof',['2','4']);
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('internalid');
												
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results)){
						//ProjectID is valid
						}
						else{
						throw 'Invalid Project ID at expense line - '+index;
						return false;
						}
						}
	//Employee Validation
	i_employee_id = nlapiGetLineItemValue('line', 'custcol_employee_entity_id',index);
	//if(i_employee_id)
	//	i_employee_id = i_employee_id.trim();
	if(_logValidation(i_employee_id)){
		var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',i_employee_id);
					filters_emp[1] = new nlobjSearchFilter('isinactive',null,'is','F');
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('internalid');
					
					
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp)){
					}
					else{
						throw 'Invalid Employee ID at expense line - '+index;
						return false;
						}					
				}	
	//Practice Validation
	
	if(_logValidation(i_practice)){
		var filters_practice = new Array();
					filters_practice[0] = new nlobjSearchFilter('internalid',null,'is',i_practice);
					filters_practice[1] = new nlobjSearchFilter('isinactive',null,'is','F');
					var column_practice = new Array();	
    				column_practice[0] = new nlobjSearchColumn('internalid');
					
					
					var a_results_emp = nlapiSearchRecord('department', null, filters_practice, column_practice);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp)){
					}
					else{
						throw 'Invalid Practice at expense line - '+index;
						return false;
						}					
				}
		nlapiSetFieldValue('custbody_is_je_updated_for_emp_type','F');		
	//var is_practice_active_e = nlapiLookupField('department',parseInt(emp_practice),['isinactive']);
										//var isinactive_Practice_e = is_practice_active_e.isinactive;
										//nlapiLogExecution('debug','isinactive_Practice_e',isinactive_Practice_e);
		
	
	}
}//End of Journal Validation

else if(s_record_type == 'creditmemo'){
	//Validation for item lines
	var item_lines = nlapiGetLineItemCount('item');
	
	for(var n = 1 ; n <= item_lines ; n ++){
	var i_proj_id_item = '';
	var i_employee_id_item = '';
	var i_practice_item = '';
	i_practice_item = nlapiGetLineItemValue('item', 'department',n);
	i_proj_id_item = nlapiGetLineItemValue('item', 'custcol_project_entity_id',n);
	//i_proj_id_item = i_proj_id_item.trim();
	if(_logValidation(i_proj_id_item)){
	var filters_search_proj = new Array();
						filters_search_proj[0] = new nlobjSearchFilter('entityid',null,'contains',i_proj_id_item);
						//filters_search_proj[1] = new nlobjSearchFilter('status',null,'anyof','2');
						var column_search_proj = new Array();
						column_search_proj[0] = new nlobjSearchColumn('internalid');
												
						var search_proj_results = nlapiSearchRecord('job', null, filters_search_proj, column_search_proj);
						if (_logValidation(search_proj_results)){
						//ProjectID is valid
						}
						else{
						throw 'Invalid Project ID at item line - '+n;
						return false;
						}
						}
	//Employee Validation
	i_employee_id_item = nlapiGetLineItemValue('item', 'custcol_employee_entity_id',n);
		//i_employee_id_item = i_employee_id_item.trim();
		if(_logValidation(i_employee_id_item)){
		var filters_emp = new Array();
					filters_emp[0] = new nlobjSearchFilter('entityid',null,'contains',i_employee_id_item);
					filters_emp[1] = new nlobjSearchFilter('isinactive',null,'is','F');
					var column_emp = new Array();	
    				column_emp[0] = new nlobjSearchColumn('internalid');
					
					
					var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp)){
					}
					else{
						throw 'Invalid Employee ID at item line - '+n;
						return false;
						}					
					}	
					
	//Practice Validation
				if(_logValidation(i_practice_item)){
				var filters_practice = new Array();
					filters_practice[0] = new nlobjSearchFilter('internalid',null,'is',i_practice_item);
					filters_practice[1] = new nlobjSearchFilter('isinactive',null,'is','F');
					var column_practice = new Array();	
    				column_practice[0] = new nlobjSearchColumn('internalid');
					
					
					var a_results_emp = nlapiSearchRecord('department', null, filters_practice, column_practice);
					nlapiLogExecution('audit','a_results_emp:-- ',a_results_emp);
					if (_logValidation(a_results_emp)){
					}
					else{
						throw 'Invalid Practice at item line - '+index;
						return false;
						}					
				}
	
	}
	}//End of Credit Memo Validation
	}
	
	}
	catch(e){
	nlapiLogExecution('debug','Before Submit Error',e);
	throw e;
	}


    //  BEFORE SUBMIT CODE BODY


	return true;
}

// END BEFORE SUBMIT ==================================================





// BEGIN AFTER SUBMIT =============================================

function afterSubmit_Set_emp_type(type)
{
	if (type == 'create' || type == 'edit')
	{
		try
		{
			var i_recordID = nlapiGetRecordId();
			var s_record_type = nlapiGetRecordType();
			
			if(s_record_type == 'journalentry')
			{
				var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);
			
				var is_je_already_processed = o_recordOBJ.getFieldValue('custbody_je_already_processed');
				
				if(is_je_already_processed == 'T')
				{
					return true;
				}
				
				var i_line_count = o_recordOBJ.getLineItemCount('line');
				for (var i = 1; i <= i_line_count; i++)
				{
					var emp_type = '';
					var person_type = '';
					var onsite_offsite = '';
					var proj_billing_type = '';
					var s_employee_name = o_recordOBJ.getLineItemValue('line','custcol_employeenamecolumn',i);
					if (_logValidation(s_employee_name))
					{
						var s_employee_name_id = s_employee_name.split('-');
						var s_employee_name_split = s_employee_name_id[0];
							
						var filters_emp = new Array();
						filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', s_employee_name_split);
						var column_emp = new Array();
						column_emp[0] = new nlobjSearchColumn('custentity_persontype');
						column_emp[1] = new nlobjSearchColumn('employeetype');
						column_emp[2] = new nlobjSearchColumn('subsidiary');
						
						var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
						nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
						if (_logValidation(a_results_emp))
						{
							var emp_id = a_results_emp[0].getId();
							var emp_type = a_results_emp[0].getText('employeetype');
							nlapiLogExecution('audit', 'emp_type:-- ', emp_type);
							if (!_logValidation(emp_type))
							{
								emp_type = '';
							}
							var person_type = a_results_emp[0].getText('custentity_persontype');
							nlapiLogExecution('audit', 'person_type:-- ', person_type);
							if (!_logValidation(person_type))
							{
								person_type = '';
							}
							var emp_subsidiary = a_results_emp[0].getValue('subsidiary');
							if (_logValidation(emp_subsidiary))
							{
								if(emp_subsidiary == 3)
								{
									onsite_offsite = 'Offsite';
								}
								else
								{
									onsite_offsite = 'Onsite';
								}	
							}
						}
					}	
				
					var proj_selected = o_recordOBJ.getLineItemValue('line','custcol_sow_project',i);
					if (_logValidation(proj_selected))
					{
						var proj_rcrd = nlapiLoadRecord('job',proj_selected);
						var proj_billing_type = proj_rcrd.getFieldText('jobbillingtype');
						if (!_logValidation(proj_billing_type))
							proj_billing_type = '';
					}
					else
					{
						var proj_desc = o_recordOBJ.getLineItemValue('line','custcolprj_name',i);
						if (_logValidation(proj_desc))
						{
							var s_proj_desc_id = proj_desc.split(' ');
							var s_proj_desc_split = s_proj_desc_id[0];
							
							var filters_proj = new Array();
							filters_proj[0] = new nlobjSearchFilter('entityid', null, 'contains', s_proj_desc_split);
							var column_proj = new Array();
							column_proj[0] = new nlobjSearchColumn('jobbillingtype');
					
							var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
							if (_logValidation(a_results_proj))
							{
								var proj_billing_type = a_results_proj[0].getText('jobbillingtype');
								if (!_logValidation(proj_billing_type))
									proj_billing_type = '';
							}
						}
						else
						{
							var s_proj_desc_id = proj_desc.split('-');
							var s_proj_desc_split = s_proj_desc_id[0];
							
							var filters_proj = new Array();
							filters_proj[0] = new nlobjSearchFilter('entityid', null, 'contains', s_proj_desc_split);
							var column_proj = new Array();
							column_proj[0] = new nlobjSearchColumn('jobbillingtype');
					
							var a_results_proj = nlapiSearchRecord('job', null, filters_proj, column_proj);
							if (_logValidation(a_results_proj))
							{
								var proj_billing_type = a_results_proj[0].getText('jobbillingtype');
								if (!_logValidation(proj_billing_type))
									proj_billing_type = '';
								
							}
						}
					}
				
					o_recordOBJ.setLineItemValue('line','custcol_onsite_offsite', i, onsite_offsite);
					o_recordOBJ.setLineItemValue('line','custcol_billing_type', i, proj_billing_type);
					o_recordOBJ.setLineItemValue('line','custcol_employee_type', i, emp_type);
					o_recordOBJ.setLineItemValue('line','custcol_person_type', i, person_type);
							
				}
			
				var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
				nlapiLogExecution('DEBUG',' ***************** Submit ID ****************-->' + i_submitID);
			}
			else if(s_record_type == 'creditmemo')
			{
				var o_recordOBJ = nlapiLoadRecord(s_record_type,i_recordID);
				var cust_slected = o_recordOBJ.getFieldValue('entity');
				var cust_name = o_recordOBJ.getFieldText('entity');
				var proj_selected = o_recordOBJ.getFieldValue('job');
				if (_logValidation(proj_selected))
				{
					var proj_rcrd = nlapiLoadRecord('job', proj_selected);
					
					var proj_entity_id = proj_rcrd.getFieldValue('entityid');
					
					var billing_type = proj_rcrd.getFieldText('jobbillingtype');
					if (!_logValidation(billing_type)) 
						billing_type = '';
					
					var onsite_offsite = proj_rcrd.getFieldText('custentity_deliverymodel');
					if (!_logValidation(onsite_offsite)) 
						onsite_offsite = '';
						
					if (_logValidation(cust_name))
					{
						var cust_id = cust_name.split(' ');
						cust_id = cust_id[0];
					}
					var region = nlapiLookupField('customer',cust_slected,'custentity_region');
					
					var i_line_count = o_recordOBJ.getLineItemCount('item');
					for (var i = 1; i <= i_line_count; i++)
					{
						o_recordOBJ.setLineItemValue('item', 'custcol_onsite_offsite', i, onsite_offsite);
						o_recordOBJ.setLineItemValue('item', 'custcol_billing_type', i, billing_type);
						o_recordOBJ.setLineItemValue('item', 'custcol_project_entity_id', i, proj_entity_id);
						o_recordOBJ.setLineItemValue('item', 'custcol_customer_entityid', i, cust_id);
						o_recordOBJ.setLineItemValue('item', 'custcol_region_master_setup', i, region);
					}
					
					var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true);
					nlapiLogExecution('DEBUG',' ***************** Submit ID ****************-->' + i_submitID);
				}
			}
				
		} 
		catch (err)
		{
			nlapiLogExecution('ERROR', 'ERROR MESSAGE:-- ', err);
		}
	} 	

	return true;

}

// END AFTER SUBMIT ===============================================





// BEGIN FUNCTION ===================================================
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// END FUNCTION =====================================================
