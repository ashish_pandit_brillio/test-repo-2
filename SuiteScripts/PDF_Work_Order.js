/**
 * Generate PDF for the vendor Work Order
 * 
 * Version Date Author Remarks
 * 
 * 1.00 17 May 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {
		var subTierId = request.getParameter('wo');

		if (subTierId) {
			var xmlContent = getWorkOrderTemplate(subTierId);
			response.renderPDF(xmlContent);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function getWorkOrderTemplate(subTierId) {
	try {
		// load the sub-tier record
		var subTierRecord = nlapiLoadRecord('customrecord_subtier_vendor_data',
		        subTierId);

		// get the consultant details
		var employeeDetails = nlapiLookupField('employee', subTierRecord
		        .getFieldValue('custrecord_stvd_contractor'), [ 'firstname',
		        'lastname' ]);
		var employeeTextDetails = nlapiLookupField('employee', subTierRecord
		        .getFieldValue('custrecord_stvd_contractor'), [
		        'locationnohierarchy', 'lastname' ], true);
		var consultantName = employeeDetails.firstname + " "
		        + employeeDetails.lastname;

		// get the vendor details
		var vendorDetails = nlapiLookupField('vendor', subTierRecord
		        .getFieldValue('custrecord_stvd_vendor'), [ 'altname',
		        'entityid', 'custentity_vendor_start_date',
		        'custentity_vendor_end_date' ]);

		var companyLogoUrl = nlapiEscapeXML(constant.Images.CompanyLogo);

		var stRate = subTierRecord.getFieldValue('custrecord_stvd_st_pay_rate');
		var payRate = stRate + " + Tax";
		var otRate = parseFloat(subTierRecord
		        .getFieldValue('custrecord_stvd_ot_pay_rate'));
		var overTimeRate = otRate == 0.00 ? "NA" : subTierRecord
		        .getFieldValue('custrecord_stvd_ot_pay_rate');
		var locationName = employeeTextDetails.locationnohierarchy;
		var workOrderNumber = subTierRecord
		        .getFieldValue('custrecord_stvd_wo_number');

		var originalStartDate = vendorDetails.custentity_vendor_start_date;
		var originalEndDate = vendorDetails.custentity_vendor_end_date;

		// get parent WO details
		//if (subTierRecord.getFieldValue('custrecord_stvd_original_subtier')) {
			//var parentSubtierDetails = nlapiLookupField(
			    //    'customrecord_subtier_vendor_data',
			     //   subTierRecord
			     //           .getFieldValue('custrecord_stvd_original_subtier'),
			     //   [ 'custrecord_stvd_new_start_date', 'custrecord_stvd_end_date' ]);

			// originalStartDate =
			// parentSubtierDetails.custrecord_stvd_start_date;
			// originalEndDate = parentSubtierDetails.custrecord_stvd_end_date;
		//}

		var htmlText = "";
		htmlText += "<table>";

		htmlText += "<tr>";
		htmlText += "<td>";

		htmlText += "<table width='100%'>";
		htmlText += "<tr>";
		htmlText += "<td style='border:0px solid black;'>";
		htmlText += "<img style='padding-top:20px;' class='companyLogo' src='"
		        + companyLogoUrl + "'/>";
		htmlText += "</td>";

		htmlText += "<td style='border:0px solid black;'>";
		htmlText += "<p class='go-right' style='margin-right:-180px;padding-top:20px;'>No 58, 1st Main road, Mini Forest ,<br/>"
		        + "JP Nagar 3rd Phase,<br/>"
		        + "Bengaluru, 560078<br/>"
		        + "www.brillio.com<br/></p>";
		htmlText += "</td>";
		htmlText += "</tr>";
		htmlText += "</table>";

		htmlText += "</td>";
		htmlText += "</tr>";

		htmlText += "<tr>";
		htmlText += "<td>";

		htmlText += "<table>";

		htmlText += "<tr>";
		htmlText += "<td style='width:70px;'>";
		htmlText += "</td>";

		htmlText += "<td>";
		htmlText += "<table>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td  style='padding-left:50px;'><span style='font-weight:bold; font-size:14px;'>EXHIBIT A - WORK ORDER Number : "
		        + nlapiEscapeXML(workOrderNumber) + "</span></td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>1. NAME OF CONSULTANT : </b>"
		        + nlapiEscapeXML(consultantName) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>2. NAME OF CONTRACTOR : </b>"
		        + nlapiEscapeXML(vendorDetails.altname) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>3. CONSULTING AGREEMENT REFERENCE : </b>"
		        + nlapiEscapeXML(vendorDetails.entityid);

		if (originalStartDate && originalEndDate) {
			htmlText += " ( " + formatVendorDate1(originalStartDate) + " to "
			        + formatVendorDate1(originalEndDate) + " )";
		}

		htmlText += "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>4. NATURE OF WORK TO BE PERFORMED : </b>"
		        + "Consulting" + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>5. WORK LOCATION : </b>"
		        + nlapiEscapeXML(locationName) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>6. START DATE : </b>"
		        + formatVendorDate2(subTierRecord
		                .getFieldValue('custrecord_stvd_new_start_date')) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>7. END DATE* : </b>"
		        + formatVendorDate2(subTierRecord
		                .getFieldValue('custrecord_stvd_end_date')) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td>*NOTE : This Work Order will commence on the Actual Start ";
		htmlText += "Date and shall remain in effect until the date specified above or ";
		htmlText += "until the work is completed to the satisfaction of Brillio or ";
		htmlText += "terminated pursuant to the Consulting Service Agreement.</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>8. PAY RATE AND FEES : </b>"
		        + nlapiEscapeXML(payRate) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>Billing Cycle : </b>"
		        + nlapiEscapeXML(subTierRecord
		                .getFieldText('custrecord_stvd_rate_type')) + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>Over Time : </b>" + overTimeRate + "</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>9. CONSULTANT TYPE : </b>Select Consultant's relationship ";
		htmlText += "with Contractor.</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td style='padding-left:20px;'>Employee : _________________      On Contract : __________________</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td>Contractor shall not delegate or subcontract any services to ";
		htmlText += "a third party and shall not utilize any other corporate or entity for ";
		htmlText += "providing Services to Brillio.</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>10. CRIMINAL BACKGROUND CHECK : </b> The charges towards ";
		htmlText += "all client required Background Checks and Drug Tests will be deducted ";
		htmlText += "from the next payment due to actuals : NA</td>";
		htmlText += "</tr>";
		htmlText += "<tr class='spaced-row'>";
		htmlText += "<td><b>11. UNIQUE TERMS AND CONDITIONS : </b>One leave allowed ";
		htmlText += "per month, cannot be carried forward.</td>";
		htmlText += "</tr>";
		htmlText += "<tr>";
		htmlText += "<td>";
		htmlText += "<table width='100%'>";
		htmlText += "<tr>";
		htmlText += "<td style='border:1px solid black'>Vendor Name :</td>";
		htmlText += "</tr>";
		htmlText += "<tr>";
		htmlText += "<td style='border:1px solid black'>Signature :</td>";
		htmlText += "</tr>";
		htmlText += "<tr>";
		htmlText += "<td style='border:1px solid black'>Title :</td>";
		htmlText += "</tr>";
		// htmlText += "<tr>";
		// htmlText += "<td style='border:1px solid black'>***** This is
		// electronically generated document hence does ";
		// htmlText += "not require a signature *****</td>";
		// htmlText += "</tr>";
		htmlText += "</table>";
		htmlText += "</td>";
		htmlText += "</tr>";
		htmlText += "</table>";
		htmlText += "</td>";

		htmlText += "<td style='width:80px;'>";
		htmlText += "</td>";
		htmlText += "</tr>";
		htmlText += "</table>";

		htmlText += "</td>";
		htmlText += "</tr>";
		htmlText += "</table>";

		// get the template for the work order pdf
		// var xmlString = getWorkOrderTemplate(subTierId);
		var companyLogoUrl = nlapiEscapeXML(constant.Images.CompanyLogo);

		var xmlContent = "";
		xmlContent += "<?xml version='1.0'?>";
		xmlContent += "<pdf><head>";

		var cssFileUrl = nlapiEscapeXML("https://system.na1.netsuite.com/core/media/media.nl?id=178865&c=3883006&h=bf0b5f48fb62b82609fc&mv=iofdfq0o&_xt=.css&whence=");
		xmlContent += "<link SRC='" + cssFileUrl + "' type='stylesheet'/>";
		xmlContent += "<meta name='title' value='Work Order'/>";
		xmlContent += "</head>";
		xmlContent += "<body>";
		xmlContent += htmlText;
		xmlContent += "</body>";
		xmlContent += "</pdf>";

		return xmlContent;

	} catch (err) {
		nlapiLogExecution('ERROR', 'getWorkOrderTemplate', err);
		throw err;
	}
}

function formatVendorDate1(dateString) {
	var d_date = nlapiStringToDate(dateString);
	return d_date.getDate() + " " + getShortMonthText(d_date.getMonth()) + " "
	        + d_date.getFullYear();
}

function formatVendorDate2(dateString) {
	var d_date = nlapiStringToDate(dateString);
	return getFullMonthText(d_date.getMonth()) + " " + d_date.getDate() + ", "
	        + d_date.getFullYear();
}

function getFullMonthText(monthInt) {
	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";

	return month[monthInt];
}

function getShortMonthText(monthInt) {
	return getFullMonthText(monthInt).substring(0, 3);
}
