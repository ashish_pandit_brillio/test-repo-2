/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Nov 2014     Swati
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve,
 *            cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF
 *            only) dropship, specialorder, orderitems (PO only) paybills
 *            (vendor payments)
 * @returns {Void} ..
 */
function userEventAfterSubmit(type) {// fun start

	nlapiLogExecution('debug', 'type', type)
	if (type == 'delete') {
		return;
	}

	var type = nlapiGetRecordType();
	var id = nlapiGetRecordId();
	var i_approved_hour = '';
	var i_leave_days;
	var i_billing_days;
	var i_floating_holidays_days;
	var i_holidays_days;
	var i_approved_days;
	var i_not_approved_days;
	var net_billable_days = '';
	var i_billing_amount = '';
	var i_approved_amount = '';
	var i_submitted_amount = '';
	var i_submitted_days = '';

	var rec_load = nlapiLoadRecord(type, id);
	var i_project = rec_load.getFieldValue('custrecord_project_id');
	var i_employee = rec_load.getFieldValue('custrecord_employee_id');
	var i_criteria = rec_load.getFieldValue('custrecord_criteria');
	var i_from_date = rec_load
	        .getFieldValue('custrecord_calculate_bill_start_date');
	var i_to_date = rec_load
	        .getFieldValue('custrecord_calculate_bill_end_date');// custrecord_subsidiary
	// var i_percent_allocation = parseFloat(rec_load.getFieldValue(
	// 'custrecord_percent_of_time').split("%")[0].trim());

	// -----------------------------------------------------------------------
	if (i_criteria == '3') {

		// ------------------------------------------------------------------
		// var rec_load=nlapiLoadRecord(type, id);
		var o_projectOBJ = nlapiLoadRecord('job', i_project)

		if (rec_load != null)
			{
			var i_vertical = o_projectOBJ.getFieldValue('custentity_vertical');
			var i_currency = o_projectOBJ.getFieldValue('currency');
			// nlapiLogExecution('DEBUG','i_currency swati test',i_currency);
			var i_exchange_rate = o_projectOBJ.getFieldValue('fxrate');
			// i_subsidiary = o_projectOBJ.getFieldValue('subsidiary');

		}// Search Results
		// rec_load.setFieldValue('custrecord_billable',b_Resource_Billable);
		// rec_load.setFieldValue('custrecord_billable_rate',i_Billable_Rate);

		rec_load.setFieldValue('custrecord_project_currency', i_currency);
		rec_load.setFieldValue('custrecord_project_vertical', i_vertical);
		rec_load.setFieldValue('custrecord_project_exchange_rate', i_exchange_rate);

		// ----------------------------------------------------------------------
		var allocated_billable_hrs = rec_load
		        .getFieldValue('custrecord_allocated_billable_hours');
		var Emp = rec_load.getFieldValue('custrecord_employee_id');
		var project = rec_load.getFieldValue('custrecord_project_id');
		var percentage_of_time = rec_load
		        .getFieldValue('custrecord_percent_of_time');

		var customer = rec_load.getFieldValue('custrecord_customer');

		var i_user_subsidiary = rec_load.getFieldValue('custrecord_subsidiary');//

		var i_billing_end_date = rec_load
		        .getFieldValue('custrecord_billing_end_date');//
		// nlapiLogExecution('DEBUG','i_billing_end_date',i_billing_end_date);

		var i_billing_start_date = rec_load
		        .getFieldValue('custrecord_billing_start_date');//
		// nlapiLogExecution('DEBUG','i_billing_start_date',i_billing_start_date);

		var i_from_date = rec_load.getFieldValue('custrecord_from_date');//
		// nlapiLogExecution('DEBUG','i_from_date',i_from_date);

		var i_to_date = rec_load.getFieldValue('custrecord_to_date');//
		// nlapiLogExecution('DEBUG','i_to_date',i_to_date);

		// -----------------------------------------------------------------------
			 var jsObj= GetOnsiteOffsite(i_from_date, i_to_date, Emp, project);
		// -----------------------------------------------------------------------

		if (_logValidation(i_billing_end_date)
		        && _logValidation(i_billing_start_date)) {// if start

			var timeDiff = Math.abs(new Date(i_billing_end_date).getTime()
			        - new Date(i_to_date).getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

			// var timeDiff_start = Math.abs(new Date(i_billing_start_date) -
			// new Date(i_from_date));
			// var diffDays_start = Math.ceil(timeDiff / (1000 * 3600 * 24));

			// nlapiLogExecution('DEBUG','diffDays_start',diffDays_start);
			// --------------------------------------------

			var context = nlapiGetContext();

			var date_format = context.getPreference('dateformat');
			if (date_format == 'YYYY-MM-DD') {

				var d_start_date3 = i_billing_start_date.split('/');
				var mm3 = parseInt(d_start_date3[1]);
				var dd3 = parseInt(d_start_date3[2]);
				var yy3 = parseInt(d_start_date3[0]);

				var d_start_date4 = i_from_date.split('/');
				var mm4 = parseInt(d_start_date4[1]);
				var dd4 = parseInt(d_start_date4[2]);
				var yy4 = parseInt(d_start_date4[0]);

				var d_start_date5 = i_billing_end_date.split('/');
				var mm5 = parseInt(d_start_date5[1]);
				var dd5 = parseInt(d_start_date5[2]);
				var yy5 = parseInt(d_start_date5[0]);

				var d_start_date6 = i_to_date.split('/');
				var mm6 = parseInt(d_start_date6[1]);
				var dd6 = parseInt(d_start_date6[2]);
				var yy6 = parseInt(d_start_date6[0]);

			}
			if (date_format == 'DD/MM/YYYY') {
				var d_start_date3 = i_billing_start_date.split('/');
				var mm3 = parseInt(d_start_date3[1]);
				var dd3 = parseInt(d_start_date3[0]);
				var yy3 = parseInt(d_start_date3[2]);

				var d_start_date4 = i_from_date.split('/');
				var mm4 = parseInt(d_start_date4[1]);
				var dd4 = parseInt(d_start_date4[0]);
				var yy4 = parseInt(d_start_date4[2]);

				var d_start_date5 = i_billing_end_date.split('/');
				var mm5 = parseInt(d_start_date5[1]);
				var dd5 = parseInt(d_start_date5[0]);
				var yy5 = parseInt(d_start_date5[2]);

				var d_start_date6 = i_to_date.split('/');
				var mm6 = parseInt(d_start_date6[1]);
				var dd6 = parseInt(d_start_date6[0]);
				var yy6 = parseInt(d_start_date6[2]);
			}
			if (date_format == 'MM/DD/YYYY') {
				var d_start_date3 = i_billing_start_date.split('/');
				var mm3 = parseInt(d_start_date3[0]);
				var dd3 = parseInt(d_start_date3[1]);
				var yy3 = parseInt(d_start_date3[2]);

				var d_start_date4 = i_from_date.split('/');
				var mm4 = parseInt(d_start_date4[0]);
				var dd4 = parseInt(d_start_date4[1]);
				var yy4 = parseInt(d_start_date4[2]);

				var d_start_date5 = i_billing_end_date.split('/');
				var mm5 = parseInt(d_start_date5[0]);
				var dd5 = parseInt(d_start_date5[1]);
				var yy5 = parseInt(d_start_date5[2]);

				var d_start_date6 = i_to_date.split('/');
				var mm6 = parseInt(d_start_date6[0]);
				var dd6 = parseInt(d_start_date6[1]);
				var yy6 = parseInt(d_start_date6[2]);
			}

			/*
			 * nlapiLogExecution('DEBUG', 'i_billing_start_date',
			 * i_billing_start_date); nlapiLogExecution('DEBUG', 'i_from_date',
			 * i_from_date); nlapiLogExecution('DEBUG', 'yy3', yy3);
			 * nlapiLogExecution('DEBUG', 'yy4', yy4);
			 * nlapiLogExecution('DEBUG', 'mm3', mm3);
			 * nlapiLogExecution('DEBUG', 'mm4', mm4);
			 * nlapiLogExecution('DEBUG', 'dd3', dd3);
			 * nlapiLogExecution('DEBUG', 'dd4', dd4);
			 */

			if (yy3 > yy4) {
				rec_load.setFieldValue('custrecord_calculate_bill_start_date',
				        i_billing_start_date);
				i_from_date = i_billing_start_date;

				var i_holiday = holiday_cal(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

				var i_non_billable_holiday_days = calculate_non_billable_holiday(
				        i_from_date, i_to_date, Emp, project, customer);
				// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

				var Concat_string = not_submitted_count_test(i_from_date,
				        i_to_date, Emp, project, customer);

				i_total_days = getDatediffIndays(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

				i_total_sat_sun = getWeekend(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

				net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -
				// parseInt(i_holiday));
				// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

				// net_billable_days=
				// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
				// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);
				if(jsObj.onsite_offsite == "1")
				{
					
						var hrs = jsObj.onsiteHrs;
						hrs = hrs?hrs:8;
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
						nlapiLogExecution('DEBUG', 'Onsite HRS set 235',hrs);
				}
				else if(jsObj.onsite_offsite == "2")
				{
						var hrs  = jsObj.offsiteHrs;
						hrs = hrs?hrs:8;
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
						nlapiLogExecution('DEBUG', 'Offsite HRS set 242',hrs);
				}
				else
				{
					allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
					Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
				}
				
				if (isNaN(Total_allocated_billable_hrs)) {
					Total_allocated_billable_hrs = 0;
				}

				allocated_billable_hrs = Total_allocated_billable_hrs;

				rec_load.setFieldValue('custrecord_net_billable_days',
				        net_billable_days);//
				rec_load.setFieldValue('custrecord_allocated_billable_hours',
				        Total_allocated_billable_hrs);

			} else if (yy3 == yy4) {
				if (mm3 > mm4) {
					rec_load.setFieldValue(
					        'custrecord_calculate_bill_start_date',
					        i_billing_start_date);
					i_from_date = i_billing_start_date;

					var i_holiday = holiday_cal(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

					var i_non_billable_holiday_days = calculate_non_billable_holiday(
					        i_from_date, i_to_date, Emp, project, customer);
					nlapiLogExecution('DEBUG',
					        'i_holidai_non_billable_holiday_daysy_days',
					        i_non_billable_holiday_days);

					var Concat_string = not_submitted_count_test(i_from_date,
					        i_to_date, Emp, project, customer);

					i_total_days = getDatediffIndays(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

					i_total_sat_sun = getWeekend(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

					net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -
					// parseInt(i_holiday));
					// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

					// net_billable_days=
					// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
					// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);
					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 298',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 305',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}

				// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
					if (isNaN(Total_allocated_billable_hrs)) {
						Total_allocated_billable_hrs = 0;
					}

					allocated_billable_hrs = Total_allocated_billable_hrs;

					rec_load.setFieldValue('custrecord_net_billable_days',
					        net_billable_days);//
					rec_load.setFieldValue(
					        'custrecord_allocated_billable_hours',
					        Total_allocated_billable_hrs);

				} else if (mm3 == mm4) {
					if (dd3 >= dd4) {
						rec_load.setFieldValue(
						        'custrecord_calculate_bill_start_date',
						        i_billing_start_date);
						i_from_date = i_billing_start_date;

						var i_holiday = holiday_cal(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

						var i_non_billable_holiday_days = calculate_non_billable_holiday(
						        i_from_date, i_to_date, Emp, project, customer);
						// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

						var Concat_string = not_submitted_count_test(
						        i_from_date, i_to_date, Emp, project, customer);

						i_total_days = getDatediffIndays(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

						i_total_sat_sun = getWeekend(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

						net_billable_days = (parseFloat(net_billable_days) - parseFloat(i_holidays_days))
						        * (parseFloat(percentage_of_time) / 100);
						// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);
					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 257',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 364',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}
						
						// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
						if (isNaN(Total_allocated_billable_hrs)) {
							Total_allocated_billable_hrs = 0;
						}

						allocated_billable_hrs = Total_allocated_billable_hrs;

						rec_load.setFieldValue('custrecord_net_billable_days',
						        net_billable_days);//
						rec_load.setFieldValue(
						        'custrecord_allocated_billable_hours',
						        Total_allocated_billable_hrs);

					}
				} else {
					rec_load
					        .setFieldValue(
					                'custrecord_calculate_bill_start_date',
					                i_from_date);
					i_total_days = getDatediffIndays(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

					var i_holiday = holiday_cal(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

					var i_non_billable_holiday_days = calculate_non_billable_holiday(
					        i_from_date, i_to_date, Emp, project, customer);
					// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

					var Concat_string = not_submitted_count_test(i_from_date,
					        i_to_date, Emp, project, customer);

					i_total_sat_sun = getWeekend(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 412',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 419',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}
				
					// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
					if (isNaN(Total_allocated_billable_hrs)) {
						Total_allocated_billable_hrs = 0;
					}

					allocated_billable_hrs = Total_allocated_billable_hrs;

					rec_load.setFieldValue('custrecord_net_billable_days',
					        net_billable_days);//
					rec_load.setFieldValue(
					        'custrecord_allocated_billable_hours',
					        Total_allocated_billable_hrs);

				}
			} else {
				rec_load.setFieldValue('custrecord_calculate_bill_start_date',
				        i_from_date);

				var i_holiday = holiday_cal(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

				i_total_days = getDatediffIndays(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

				// nlapiLogExecution('DEBUG','i_from_date',i_from_date);
				// nlapiLogExecution('DEBUG','i_to_date',i_to_date);

				var i_non_billable_holiday_days = calculate_non_billable_holiday(
				        i_from_date, i_to_date, Emp, project, customer);
				// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

				var Concat_string = not_submitted_count_test(i_from_date,
				        i_to_date, Emp, project, customer);

				i_total_sat_sun = getWeekend(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

				net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -parseInt(i_holiday));
				// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

				// net_billable_days=
				// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
				// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 476',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 483',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}
					
				// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
				if (isNaN(Total_allocated_billable_hrs)) {
					Total_allocated_billable_hrs = 0;
				}
				allocated_billable_hrs = Total_allocated_billable_hrs;

				rec_load.setFieldValue('custrecord_net_billable_days',
				        net_billable_days);//
				rec_load.setFieldValue('custrecord_allocated_billable_hours',
				        Total_allocated_billable_hrs);

			}

			// ----------------------------------------------------------------------------
			// =================================================================================

			if (yy5 > yy6) {// 1 if start
				rec_load.setFieldValue('custrecord_calculate_bill_end_date',
				        i_to_date);

				var i_holiday = holiday_cal(i_from_date, i_to_date);
				nlapiLogExecution('DEBUG', 'i_holiday', i_holiday);

				var i_non_billable_holiday_days = calculate_non_billable_holiday(
				        i_from_date, i_to_date, Emp, project, customer);
				// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

				var Concat_string = not_submitted_count_test(i_from_date,
				        i_to_date, Emp, project, customer);

				i_total_days = getDatediffIndays(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

				i_total_sat_sun = getWeekend(i_from_date, i_to_date);
				// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

				net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -parseInt(i_holiday));
				// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

				// net_billable_days=
				// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
				// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);
					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 538',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 545',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}

				// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
				if (isNaN(Total_allocated_billable_hrs)) {
					Total_allocated_billable_hrs = 0;
				}

				allocated_billable_hrs = Total_allocated_billable_hrs;

				rec_load.setFieldValue('custrecord_net_billable_days',
				        net_billable_days);//
				rec_load.setFieldValue('custrecord_allocated_billable_hours',
				        Total_allocated_billable_hrs);

			}// 1 end
			else if (yy5 == yy6) {// 2 start
				if (mm5 > mm6) {// 3 start
					rec_load.setFieldValue(
					        'custrecord_calculate_bill_end_date', i_to_date);

					var i_holiday = holiday_cal(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

					var i_non_billable_holiday_days = calculate_non_billable_holiday(
					        i_from_date, i_to_date, Emp, project, customer);
					// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

					i_total_days = getDatediffIndays(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

					i_total_sat_sun = getWeekend(i_from_date, i_to_date);
					// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

					net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -parseInt(i_holiday));
					// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

					var Concat_string = not_submitted_count_test(i_from_date,
					        i_to_date, Emp, project, customer);

					// net_billable_days=
					// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
					// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);
					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 598',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 605',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}
					// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
					if (isNaN(Total_allocated_billable_hrs)) {
						Total_allocated_billable_hrs = 0;
					}

					allocated_billable_hrs = Total_allocated_billable_hrs;

					rec_load.setFieldValue('custrecord_net_billable_days',
					        net_billable_days);//
					rec_load.setFieldValue(
					        'custrecord_allocated_billable_hours',
					        Total_allocated_billable_hrs);

				}// 3 close
				else if (mm5 == mm6) {// 4 strt
					if (dd5 >= dd6) {
						rec_load
						        .setFieldValue(
						                'custrecord_calculate_bill_end_date',
						                i_to_date);

						var i_holiday = holiday_cal(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

						var i_non_billable_holiday_days = calculate_non_billable_holiday(
						        i_from_date, i_to_date, Emp, project, customer);
						// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

						i_total_days = getDatediffIndays(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

						i_total_sat_sun = getWeekend(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

						net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -parseInt(i_holiday));
						// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

						var Concat_string = not_submitted_count_test(
						        i_from_date, i_to_date, Emp, project, customer);

						// net_billable_days=
						// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
						// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 661',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 668',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}

						// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
						if (isNaN(Total_allocated_billable_hrs)) {
							Total_allocated_billable_hrs = 0;
						}

						allocated_billable_hrs = Total_allocated_billable_hrs;

						rec_load.setFieldValue('custrecord_net_billable_days',
						        net_billable_days);//
						rec_load.setFieldValue(
						        'custrecord_allocated_billable_hours',
						        Total_allocated_billable_hrs);

					} else {
						i_to_date = i_billing_end_date;
						rec_load.setFieldValue(
						        'custrecord_calculate_bill_end_date',
						        i_billing_end_date);

						var i_holiday = holiday_cal(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_holiday',i_holiday);

						var i_non_billable_holiday_days = calculate_non_billable_holiday(
						        i_from_date, i_to_date, Emp, project, customer);
						// nlapiLogExecution('DEBUG','i_holidai_non_billable_holiday_daysy_days',i_non_billable_holiday_days);

						var Concat_string = not_submitted_count_test(
						        i_from_date, i_to_date, Emp, project, customer);

						i_total_days = getDatediffIndays(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_total_days',i_total_days);

						i_total_sat_sun = getWeekend(i_from_date, i_to_date);
						// nlapiLogExecution('DEBUG','i_total_sat_sun',i_total_sat_sun);

						net_billable_days = (parseInt(i_total_days) - parseInt(i_total_sat_sun));// -parseInt(i_holiday));
						// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

						// net_billable_days=
						// (parseFloat(net_billable_days)-(parseFloat(i_holidays_days)+parseFloat(i_leave_days)+parseFloat(i_billing_days)+parseFloat(i_floating_holidays_days)+parseFloat(i_approved_days)+parseFloat(i_not_approved_days)))*(parseFloat(percentage_of_time)/100);
						// nlapiLogExecution('DEBUG','net_billable_days',net_billable_days);

					if(jsObj.onsite_offsite == "1")
					{
							var hrs = jsObj.onsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Onsite HRS set 723',hrs);
					}
					else if(jsObj.onsite_offsite == "2")
					{
							var hrs  = jsObj.offsiteHrs;
							hrs = hrs?hrs:8;
							allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * Number(hrs)
							Total_allocated_billable_hrs = parseFloat(net_billable_days)* Number(hrs) * (parseFloat(percentage_of_time) / 100);
							nlapiLogExecution('DEBUG', 'Offsite HRS set 730',hrs);
					}
					else
					{
						allocated_billable_hrs = (parseInt(i_total_days) - parseInt(i_total_sat_sun)) * 8
						Total_allocated_billable_hrs = parseFloat(net_billable_days)* 8 * (parseFloat(percentage_of_time) / 100);
					}

						// nlapiLogExecution('DEBUG','Total_allocated_billable_hrs',Total_allocated_billable_hrs);
						if (isNaN(Total_allocated_billable_hrs)) {
							Total_allocated_billable_hrs = 0;
						}

						allocated_billable_hrs = Total_allocated_billable_hrs;

						rec_load.setFieldValue('custrecord_net_billable_days',
						        net_billable_days);//
						rec_load.setFieldValue(
						        'custrecord_allocated_billable_hours',
						        Total_allocated_billable_hrs);
					}
				}// 4 close

			}

		}// if close

		// ================================
		nlapiLogExecution('DEBUG', 'Concat_string', Concat_string);
		var i_leave_days, i_billing_days, i_floating_holidays_days, i_holidays_days, i_approved_days, i_not_approved_days, i_working_days;

		if (_logValidation(Concat_string)) 
		{
			/*
			 * var split_Concat_string=Concat_string.split('##');
			 * i_leave_days=split_Concat_string[0];
			 * i_billing_days=split_Concat_string[1];
			 * i_floating_holidays_days=split_Concat_string[2];
			 * i_holidays_days=split_Concat_string[3];
			 * i_approved_days=split_Concat_string[4];
			 * i_not_approved_days=split_Concat_string[5];
			 */

			i_billing_days = Concat_string.billed_hours;
			nlapiLogExecution('DEBUG', 'i_billing_days', i_billing_days);

			i_approved_days = Concat_string.approved_hours;
			nlapiLogExecution('DEBUG', 'i_approved_days', i_approved_days);

			i_submitted_days = Concat_string.submitted_hours;
			nlapiLogExecution('DEBUG', 'i_submitted_days', i_submitted_days);

			i_leave_days = Concat_string.leave_hours;
			nlapiLogExecution('DEBUG', 'i_leave_days', i_leave_days);

			i_holidays_days = Concat_string.holiday_hours;
			nlapiLogExecution('DEBUG', 'i_holidays_days', i_holidays_days);

			i_billing_amount = Concat_string.billed_amount;
			nlapiLogExecution('DEBUG', 'i_billing_amount', i_billing_amount);

			i_approved_amount = Concat_string.approved_amount;
			nlapiLogExecution('DEBUG', 'i_approved_amount', i_approved_amount);

			i_submitted_amount = Concat_string.submitted_amount;
			nlapiLogExecution('DEBUG', 'i_submitted_amount', i_submitted_amount);

			i_submitted_amount = Concat_string.submitted_amount;
			nlapiLogExecution('DEBUG', 'i_submitted_amount', i_submitted_amount);

			i_working_days = Concat_string.working_days_entered;
			nlapiLogExecution('DEBUG', 'i_working_days', i_working_days);
		}

		if (!_logValidation(i_leave_days)) {
			i_leave_days = 0;
		}
		if (!_logValidation(i_billing_days)) {
			i_billing_days = 0;
		}
		if (!_logValidation(i_floating_holidays_days)) {
			i_floating_holidays_days = 0;
		}
		if (!_logValidation(i_holidays_days)) {
			i_holidays_days = 0;
		}
		if (!_logValidation(i_approved_days)) {
			i_approved_days = 0;
		}
		if (!_logValidation(i_not_approved_days)) {
			i_not_approved_days = 0;
		}
		if (!_logValidation(i_working_days)) {
			i_working_days = 0;
		}

		// ================================

		// ----------------------------------------------------------------------

		rec_load.setFieldValue('custrecord_leave_days', i_leave_days);
		rec_load.setFieldValue('custrecord_billed_days', i_billing_days);
		rec_load.setFieldValue('custrecord_approved_days', i_approved_days);
		rec_load.setFieldValue('custrecord_not_approved_days',
		        i_not_approved_days);
		rec_load.setFieldValue('custrecord_holidays_days', i_holidays_days);
		rec_load.setFieldValue('custrecord_floating_holidays_days',
		        i_floating_holidays_days);
		// -----------------------------------------------------------------------
		var o_Load_Emp = nlapiLoadRecord('employee', Emp);

		var Employee_Hour = o_Load_Emp.getFieldValue('custentityhoursperday');

		if (!_logValidation) {
			Employee_Hour = 8;
		}
	nlapiLogExecution('DEBUG', 'Employee_Hour',Employee_Hour);
		// added by Nitish (9/14/2015)
		// added new logic to calculate the not-submitted hours based on the no.
		// of weeks submitted
		var i_Not_Submitted_Days = get_not_submitted_days(i_from_date,
		        i_to_date, Emp, project, customer);

		// var i_Not_Submitted_Days = (parseFloat(net_billable_days)
		// - parseFloat(i_working_days) - parseInt(i_non_billable_holiday_days))
		var i_Not_Submitted_Hour  = "";
		if(jsObj.onsite_offsite == "1")
		{
				Employee_Hour = jsObj.onsiteHrs;
				Employee_Hour = Employee_Hour?Employee_Hour:8;
			 i_Not_Submitted_Hour = parseFloat(i_Not_Submitted_Days)* parseFloat(Employee_Hour);
			 	nlapiLogExecution('DEBUG', 'Onsite HRS set',Employee_Hour);
		}
		else if(jsObj.onsite_offsite == "2")
		{
				Employee_Hour = jsObj.offsiteHrs;
				Employee_Hour = Employee_Hour?Employee_Hour:8;
			 i_Not_Submitted_Hour = parseFloat(i_Not_Submitted_Days)* parseFloat(Employee_Hour);
			 	nlapiLogExecution('DEBUG', 'Offsite HRS set',Employee_Hour);
		}
		else
		{
			 i_Not_Submitted_Hour = parseFloat(i_Not_Submitted_Days)* parseFloat(Employee_Hour);
			 nlapiLogExecution('DEBUG', 'Normal HRS set',Employee_Hour);
		}
		
		if (isNaN(i_non_billable_holiday_days)) {
			i_non_billable_holiday_days = 0;
		}
		if (isNaN(i_Not_Submitted_Days)) {
			i_Not_Submitted_Days = 0;
		}
		if (isNaN(i_Not_Submitted_Hour)) {
			i_Not_Submitted_Hour = 0;
		}

		rec_load.setFieldValue('custrecord_not_submitted_days',
		        i_Not_Submitted_Days);

		rec_load
		        .setFieldValue(
		                'custrecord_after_percent_applied_not_sub',
		                (parseFloat(i_Not_Submitted_Days) * (parseFloat(percentage_of_time) / 100)));
	if(jsObj.onsite_offsite == "1")
		{
				Employee_Hour = jsObj.onsiteHrs;
				Employee_Hour = Employee_Hour?Employee_Hour:8;
			rec_load.setFieldValue('custrecord_after_per_app_not_sub_hour',(parseFloat(i_Not_Submitted_Days) * (parseFloat(percentage_of_time) / 100)) * Number(Employee_Hour));
		nlapiLogExecution('DEBUG', 'Onsite HRS set 917',Employee_Hour);
		}
		else if(jsObj.onsite_offsite == "2")
		{
				Employee_Hour = jsObj.offsiteHrs;
				Employee_Hour = Employee_Hour?Employee_Hour:8;
				rec_load.setFieldValue('custrecord_after_per_app_not_sub_hour',(parseFloat(i_Not_Submitted_Days) * (parseFloat(percentage_of_time) / 100)) * Number(Employee_Hour));
			 	nlapiLogExecution('DEBUG', 'Offsite HRS set 924',Employee_Hour);
		}	
	
		rec_load.setFieldValue('custrecord_not_submitted_hours',
		        i_Not_Submitted_Hour);

		rec_load.setFieldValue('custrecord_non_billable_holiday',
		        i_non_billable_holiday_days);

		rec_load.setFieldValue('custrecord_submitted_days', i_working_days);

		// ----------------------------------------------------------------------

		nlapiSubmitRecord(rec_load);
	}
}// fun show
// ------------------------------------------------------------
function getDatediffIndays(startDate, endDate) {
	var one_day = 1000 * 60 * 60 * 24;
	var fromDate = startDate;
	var toDate = endDate

	var date1 = nlapiStringToDate(fromDate);

	var date2 = nlapiStringToDate(toDate);

	var date3 = Math.round((date2 - date1) / one_day);

	return (date3 + 1);
}
// -----------------------------------------------------------------------
function getWeekend(startDate, endDate) {
	var i_no_of_sat = 0;
	var i_no_of_sun = 0;

	/*
	 * nlapiLogExecution('DEBUG','startDate',startDate);
	 * nlapiLogExecution('DEBUG','endDate',endDate);
	 */
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	var date_format = checkDateFormat();

	var startDate_1 = startDate
	var endDate_1 = endDate

	startDate = nlapiStringToDate(startDate);
	endDate = nlapiStringToDate(endDate);

	var i_count_day = startDate.getDate();

	var i_count_last_day = endDate.getDate();

	i_month = startDate.getMonth() + 1;

	i_year = startDate.getFullYear();

	var d_f = new Date();
	var getTot = getDatediffIndays(startDate_1, endDate_1); // Get total days in
	// a month

	var sat = new Array(); // Declaring array for inserting Saturdays
	var sun = new Array(); // Declaring array for inserting Sundays

	for (var i = i_count_day; i <= i_count_last_day; i++) { // looping through
		// days in month

		if (date_format == 'YYYY-MM-DD') {
			var newDate = i_year + '-' + i_month + '-' + i;
		}
		if (date_format == 'DD/MM/YYYY') {
			var newDate = i + '/' + i_month + '/' + i_year;
		}
		if (date_format == 'MM/DD/YYYY') {
			var newDate = i_month + '/' + i + '/' + i_year;
		}

		newDate = nlapiStringToDate(newDate);

		if (newDate.getDay() == 0) { // if Sunday
			sat.push(i);
			i_no_of_sat++;
		}
		if (newDate.getDay() == 6) { // if Saturday
			sun.push(i);
			i_no_of_sun++;
		}

	}

	var i_total_days_sat_sun = parseInt(i_no_of_sat) + parseInt(i_no_of_sun);
	return i_total_days_sat_sun;
}

function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function _logValidation(value) {
	if (value != 'null' && value != null && value != null && value != ''
	        && value != undefined && value != undefined && value != 'undefined'
	        && value != 'undefined' && value != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}
// -----------------------------------------------------------
function holiday_cal(i_from_date, i_to_date) {// fun start

	var i_holiday = '';

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_date', null, 'within',
	        i_from_date, i_to_date);

	var i_search_results = nlapiSearchRecord('customrecord_holiday', null,
	        filter, null);
	if (i_search_results != null) {
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();
		i_holiday = parseInt(i_search_results.length);

	} else {
		i_holiday = 0;
	}
	return i_holiday
}// fun close
// ---------------------------------------------------------------------------------
function Field_calculate(i_from_date, i_to_date, i_user_subsidiary, Emp,
        project)
{// function start
	var i_leave_days;
	var i_billing_days;
	var i_floating_holidays_days;
	var i_holidays_days;
	var i_approved_days;
	var i_not_approved_days;
	var Con_String = '';
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,
	        i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is',
	        parseInt(i_user_subsidiary));
	filter[2] = new nlobjSearchFilter('employee', null, 'is', Emp);
	filter[3] = new nlobjSearchFilter('customer', null, 'is', project);
	// filter[3] = new nlobjSearchFilter('item',null,'anyof','Leave');
	var i_search_results = nlapiSearchRecord('timebill',
	        'customsearch_unbilled_approved_new_time6', filter, null);

	if (i_search_results != null) {
		// nlapiLogExecution('DEBUG','i_search_results.length',i_search_results.length);
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();

		i_leave_days = a_search_transaction_result.getValue(columns[10]);
		// nlapiLogExecution('DEBUG','i_leave_days',i_leave_days);

	} else {
		i_leave_days = 0;
	}

	// -------------------------billed----------------------------

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,
	        i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is',
	        parseInt(i_user_subsidiary));
	filter[2] = new nlobjSearchFilter('employee', null, 'is', Emp);
	filter[3] = new nlobjSearchFilter('customer', null, 'is', project);
	// filter[3] = new nlobjSearchFilter('item',null,'anyof','Leave');
	var i_search_results = nlapiSearchRecord('timebill',
	        'customsearch_unbilled_approved_new_time7', filter, null);
	if (i_search_results != null) {
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();
		i_billing_days = a_search_transaction_result.getValue(columns[9]);
		// nlapiLogExecution('DEBUG','i_billing_days',i_billing_days);
	} else {
		i_billing_days = 0;
	}
	// ---------------------floating
	// holidays----------------------------------------

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,
	        i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is',
	        parseInt(i_user_subsidiary));
	filter[2] = new nlobjSearchFilter('employee', null, 'is', Emp);
	filter[3] = new nlobjSearchFilter('customer', null, 'is', project);
	// filter[3] = new nlobjSearchFilter('item',null,'anyof','Leave');
	var i_search_results = nlapiSearchRecord('timebill',
	        'customsearch_unbilled_approved_new_time8', filter, null);
	if (i_search_results != null) {
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();
		i_floating_holidays_days = a_search_transaction_result
		        .getValue(columns[10]);
		// nlapiLogExecution('DEBUG','i_floating_holidays_days',i_floating_holidays_days);
	} else {
		i_floating_holidays_days = 0;
	}
	// -------------------------holidays in days---------------------------

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,
	        i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is',
	        parseInt(i_user_subsidiary));
	filter[2] = new nlobjSearchFilter('employee', null, 'is', Emp);
	filter[3] = new nlobjSearchFilter('customer', null, 'is', project);
	// filter[3] = new nlobjSearchFilter('item',null,'anyof','Leave');
	var i_search_results = nlapiSearchRecord('timebill',
	        'customsearch_unbilled_approved_new_time9', filter, null);
	if (i_search_results != null) {
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();
		i_holidays_days = a_search_transaction_result.getValue(columns[10]);
		// nlapiLogExecution('DEBUG','i_holidays_days',i_holidays_days);
	} else {
		i_holidays_days = 0;
	}
	// ---------------------------approved----------------------------------
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,
	        i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is',
	        parseInt(i_user_subsidiary));
	filter[2] = new nlobjSearchFilter('employee', null, 'is', Emp);
	filter[3] = new nlobjSearchFilter('customer', null, 'is', project);
	// filter[3] = new nlobjSearchFilter('item',null,'anyof','Leave');
	var i_search_results = nlapiSearchRecord('timebill',
	        'customsearch_unbilled_approved_new_tim10', filter, null);
	if (i_search_results != null) {
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();
		i_approved_days = a_search_transaction_result.getValue(columns[10]);
		// nlapiLogExecution('DEBUG','i_approved_days',i_approved_days);
	} else {
		i_approved_days = 0;
	}
	// ----------------------not
	// approved--------------------------------------------------
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('date', null, 'within', i_from_date,
	        i_to_date);
	filter[1] = new nlobjSearchFilter('subsidiary', 'customer', 'is',
	        parseInt(i_user_subsidiary));
	filter[2] = new nlobjSearchFilter('employee', null, 'is', Emp);
	filter[3] = new nlobjSearchFilter('customer', null, 'is', project);
	// filter[3] = new nlobjSearchFilter('item',null,'anyof','Leave');
	var i_search_results = nlapiSearchRecord('timebill',
	        'customsearch_submitted_not_appr_new_tim3', filter, null);
	if (i_search_results != null) {
		var a_search_transaction_result = i_search_results[0];
		var columns = a_search_transaction_result.getAllColumns();
		i_not_approved_days = a_search_transaction_result.getValue(columns[10]);
		nlapiLogExecution('DEBUG', 'i_not_approved_days', i_not_approved_days);
	} else {
		i_not_approved_days = 0;
	}

	return i_leave_days + '##' + i_billing_days + '##'
	        + i_floating_holidays_days + '##' + i_holidays_days + '##'
	        + i_approved_days + '##' + i_not_approved_days;

}// function close
// -------------------------------------------------------------------------------------------
function calculate_non_billable_holiday(d_Calculated_Start_Date,
        d_Calculated_End_Date, employee, project, customer)
{// fun start

	// nlapiLogExecution('DEBUG','d_Calculated_Start_Date',d_Calculated_Start_Date);
	// nlapiLogExecution('DEBUG','d_Calculated_Start_Date',d_Calculated_End_Date);
nlapiLogExecution('DEBUG','fun start calculate_non_billable_holiday',"calculate_non_billable_holiday");
	var Holiday_Array = new Array();
	var i_Project_Holiday;
	if (_logValidation(employee)) {
		o_Load_Emp = nlapiLoadRecord('employee', employee);
		i_Emp_Subsidiary = o_Load_Emp.getFieldValue('subsidiary');
		nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
		        'i_Emp_Subsidiary' + i_Emp_Subsidiary);
	}
	if (_logValidation(project)) {
		o_Load_Project = nlapiLoadRecord('job', project);
		i_Project_Holiday = o_Load_Project
		        .getFieldValue('custentityproject_holiday');
		nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
		        'i_Project_Holiday' + i_Project_Holiday);
	}
	// company holiday=1
	// customer holiday=2;
	if (_logValidation(i_Project_Holiday)) {// 1 if start
		if (i_Project_Holiday == 1) {// if start
			nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday','i_Project_Holiday==1');
			//nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Calculated_Start_Date',d_Calculated_Start_Date);
			//nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Calculated_End_Date',d_Calculated_End_Date);
			//nlapiLogExecution('DEBUG','calculate_non_billable_holiday','i_Emp_Subsidiary',i_Emp_Subsidiary);
			// ---------------load company holiday
			// record------------------------
			var com_filter = new Array();
			com_filter[0] = new nlobjSearchFilter('custrecord_date',null,'within',d_Calculated_Start_Date,d_Calculated_End_Date);
			com_filter[1] = new nlobjSearchFilter('custrecordsubsidiary',null,'anyof',i_Emp_Subsidiary);

			var com_column = new Array();
			com_column[0] = new nlobjSearchColumn('custrecord_date');
		  var o_Search_Company_Holiday="";
			o_Search_Company_Holiday = nlapiSearchRecord(
			        'customrecord_holiday',
			        'customsearch_company_holiday_search',com_filter,null);
			//nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Company_Holiday.length'+o_Search_Company_Holiday.length);

			if (_logValidation(o_Search_Company_Holiday)) {// if start
				nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Company_Holiday.length'+o_Search_Company_Holiday.length);
				for (var j = 0; j < o_Search_Company_Holiday.length; j++) {// for
					// start

					var a_search_transaction_result = o_Search_Company_Holiday[j];
					var columns = a_search_transaction_result.getAllColumns();

					var columnLen = columns.length;
					var d_Company_Holiday_Date = a_search_transaction_result.getValue(columns[0]);

				
					// var d_Company_Holiday_Date=
					// o_Search_Company_Holiday[j].getValue('custrecord_date');
					// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Company_Holiday_Date'+d_Company_Holiday_Date);
					Holiday_Array[j] = d_Company_Holiday_Date;
					

				}// for close

			}// if close
		}// if close
		else {// else start
			// ---------------load the customer holiday
			// record-------------------
			// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','customer'+customer);
			var cust_filter = new Array();
			cust_filter[0] = new nlobjSearchFilter('custrecordholidaydate',
			        null, 'within', d_Calculated_Start_Date,
			        d_Calculated_End_Date);
			cust_filter[1] = new nlobjSearchFilter(
			        'custrecordcustomersubsidiary', null, 'is',
			        i_Emp_Subsidiary);
			cust_filter[2] = new nlobjSearchFilter('custrecord13', null, 'is',
			        customer);

			var cust_column = new Array();
			//cust_column[0] = new nlobjSearchColumn('custrecordholidaydate');
			cust_column[0] = new nlobjSearchColumn('custrecordholidaydate',null,'group');

			var o_Search_Customer_Holiday = nlapiSearchRecord(
			        'customrecordcustomerholiday',
			        'customsearch_customer_holiday', cust_filter, null);

			if (_logValidation(o_Search_Customer_Holiday)) {// if start
				// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Customer_Holiday.length'+o_Search_Customer_Holiday.length);
				for (var j = 0; j < o_Search_Customer_Holiday.length; j++) {// for
					// start

					var a_search_transaction_result = o_Search_Customer_Holiday[j];
					var columns = a_search_transaction_result.getAllColumns();

					var columnLen = columns.length;
					var d_Customer_Holiday_Date = a_search_transaction_result
					        .getValue(columns[0]);

					// var d_Customer_Holiday_Date=
					// o_Search_Customer_Holiday[j].getValue('custrecordholidaydate');
					// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Customer_Holiday_Date'+d_Customer_Holiday_Date);
					Holiday_Array[j] = d_Customer_Holiday_Date;

				}// for close

			}// if close
			// ---------------------------------------------------------------
		}// else close
	}// 1 if close
	// --------------------------------------------------------------------------------
	var count = 0;
	if (_logValidation(Holiday_Array)) {// if start
		// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','Holiday_Array'+Holiday_Array);
		// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','Holiday_Array'+Holiday_Array.length);
		// ------------------------search time entry record-------------------
		for (var ss = 0; ss < Holiday_Array.length; ss++) {// fr start
			var time_filter = new Array();
			time_filter[0] = new nlobjSearchFilter('date', null, 'on',
			        Holiday_Array[ss]);
			time_filter[1] = new nlobjSearchFilter('employee', null, 'is',
			        employee);
			time_filter[2] = new nlobjSearchFilter('internalid', 'job', 'is',
			        project);
			// time_filter[2]=new nlobjSearchFilter('type',null,'is',employee);
			var o_Search_Time_Entry = nlapiSearchRecord('timebill',
			        'customsearch_non_billable_time_holiday', time_filter, null);

			if (_logValidation(o_Search_Time_Entry)) {// if start
				nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
				        'o_Search_Time_Entry' + o_Search_Time_Entry.length);
			}// if close
			else {
				count++;
			}
		}// fr close

	}// if close
	nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday', 'count'
	        + count);
	return count;

}// fun close
// ---------------------------------------------------------------------------

function get_not_submitted_days(d_start_date, d_end_date, employee, project,
        customer)
{
	d_start_date = nlapiStringToDate(d_start_date);
	d_end_date = nlapiStringToDate(d_end_date);

	nlapiLogExecution('debug', 'Not Submitted Started');

	nlapiLogExecution('debug', 'd_start_date', d_start_date);
	nlapiLogExecution('debug', 'd_end_date', d_end_date);
	nlapiLogExecution('debug', 'employee', employee);
	nlapiLogExecution('debug', 'project', project);
	nlapiLogExecution('debug', 'customer', customer);

	// 1. create a array containing all days from start to end
	var main_array = [];

	// 2. loop from start to end date and mark saturday sunday as submitted
	for (var i = 0;; i++) {
		var d_current_date = nlapiAddDays(d_start_date, i);

		// if the date exceeds the end date, exit the loop
		if (d_current_date > d_end_date) {
			break;
		}

		// check if saturday / sunday
		var day = d_current_date.getDay();
		if (day == 0 || day == 6) {
			main_array[i] = 3;
		} else {
			main_array[i] = 1;
		}
	}

	nlapiLogExecution('debug', 'data', JSON.stringify(main_array));
	nlapiLogExecution('debug', 'no. of days', main_array.length);

	// 3. mark holidays as submitted
	var holiday_list = get_holidays(d_start_date, d_end_date, employee,
	        project, customer);
	nlapiLogExecution('debug', 'no. of holidays', holiday_list.length);

	for (var i = 0; i < holiday_list.length; i++) {
		var d_holiday_date = nlapiStringToDate(holiday_list[i]);
		var n = getDatediffIndays(nlapiDateToString(d_start_date),
		        holiday_list[i]) - 1;
		nlapiLogExecution('debug', 'holiday : ' + n);
		main_array[n] = 3;
	}

	nlapiLogExecution('debug', 'data', JSON.stringify(main_array));

	// 4. mark the no. of allocated days
	var search_allocation = nlapiSearchRecord('resourceallocation', null,
	        [
	                new nlobjSearchFilter('resource', null, 'anyof', employee),
	                new nlobjSearchFilter('project', null, 'anyof', project),
	                new nlobjSearchFilter('startdate', null, 'notafter',
	                        d_end_date),
	                new nlobjSearchFilter('enddate', null, 'notbefore',
	                        d_start_date) ], [
	                new nlobjSearchColumn('startdate'),
	                new nlobjSearchColumn('enddate') ]);
					
					nlapiLogExecution('debug', 'no. of allocations', search_allocation.length);
	for (var i = 0; i < search_allocation.length; i++) {
		
		var allocation_start_date = nlapiStringToDate(search_allocation[i]
		        .getValue('startdate'));
		var allocation_end_date = nlapiStringToDate(search_allocation[i]
		        .getValue('enddate'));

		var start_date_point = allocation_start_date > d_start_date ? allocation_start_date
		        : d_start_date;

		for (var d = 0;; d++) {
			var d_current_date = nlapiAddDays(start_date_point, d);

			if (d_current_date > allocation_end_date
			        || d_current_date > d_end_date) {
				break;
			}

			var n = getDatediffIndays(nlapiDateToString(d_start_date),
			        nlapiDateToString(d_current_date)) - 1;

			if (main_array[n] == 1) {
				main_array[n] = 2;
			}
		}
	}

	nlapiLogExecution('debug', 'allocation data', JSON.stringify(main_array));

	// 5. get the timesheet between the start date and end date
	var search_timesheet = nlapiSearchRecord('timesheet', null, [
	        new nlobjSearchFilter('employee', null, 'anyof', employee),
	        new nlobjSearchFilter('approvalstatus', null, 'noneof', [ 1, 4 ]),
            new nlobjSearchFilter('totalhours', null, 'greaterthan', "0"),
	       // new nlobjSearchFilter('timesheetdate', null, 'within',
	        new nlobjSearchFilter('date', 'timebill', 'within',
	                d_start_date, d_end_date) ], [ new nlobjSearchColumn(
	        'startdate') ]);

	// 6. loop through the timesheets
	if (search_timesheet) {
		nlapiLogExecution('debug', 'no. of timesheets', search_timesheet.length);

		// 6a. if timesheet is submitted, mark entire week as submitted
		for (var i = 0; i < search_timesheet.length; i++) {
			var timesheet_date = search_timesheet[i].getValue('startdate');
			var n = getDatediffIndays(nlapiDateToString(d_start_date),
			        timesheet_date) - 1;

			if (n >= 0) {

				for (var j = n; j < n + 7; j++) {

					if (main_array[j] == 2) {
						main_array[j] = 3;
					}
				}
			} else {

				for (var j = 0; j < n + 7; j++) {

					if (main_array[j] == 2) {
						main_array[j] = 3;
					}
				}
			}
		}
	}

	nlapiLogExecution('debug', 'data', JSON.stringify(main_array));

	// 7. count the not submitted days in the main array
	var not_submitted_days = 0;
	var submitted = 0;

	for (var i = 0; i < main_array.length; i++) {

		if (main_array[i] == 2) {
			not_submitted_days = not_submitted_days + 1;
		} else {
			submitted = submitted + 1;
		}
	}

	nlapiLogExecution('debug', 'not_submitted_days', not_submitted_days);
	nlapiLogExecution('debug', 'submitted', submitted);
	nlapiLogExecution('debug', 'Not Submitted Ended');

	// 8. return the count
	return not_submitted_days;
}

function GetOnsiteOffsite(d_start_date, d_end_date, employee, project)
{
	var temp = [];
	var search_allocation = nlapiSearchRecord('resourceallocation', null,
	        [
	                new nlobjSearchFilter('resource', null, 'anyof', employee),
	                new nlobjSearchFilter('project', null, 'anyof', project),
	                new nlobjSearchFilter('startdate', null, 'notafter',d_end_date),
	                new nlobjSearchFilter('enddate', null, 'notbefore',d_start_date) 
			],
			[
	                new nlobjSearchColumn('custevent4'),
	                new nlobjSearchColumn('custentity_onsite_hours_per_day','job'),
					new nlobjSearchColumn('custentity_hoursperday','job')
			]);
			if(search_allocation){
			var onsite_offsite = search_allocation[0].getValue('custevent4');
			var onsiteHrs = search_allocation[0].getValue('custentity_onsite_hours_per_day','job');
			var offsiteHrs = search_allocation[0].getValue('custentity_hoursperday','job');
			var ob = {"onsite_offsite":onsite_offsite,"onsiteHrs":onsiteHrs,"offsiteHrs":offsiteHrs};
			//temp.push(ob);
			}
			else{
				var ob = {"onsite_offsite":"","onsiteHrs":"","offsiteHrs":""};
				nlapiLogExecution('DEBUG', 'JSON: IN else Blank Object');
			}
			nlapiLogExecution('DEBUG', 'JSON: ',JSON.stringify(ob));
			return ob;
}
function get_holidays(start_date, end_date, employee, project, customer) {

	var project_holiday = nlapiLookupField('job', project,
	        'custentityproject_holiday');
	var emp_subsidiary = nlapiLookupField('employee', employee, 'subsidiary');

	if (project_holiday == 1) {
		return get_company_holidays(start_date, end_date, emp_subsidiary);
	} else {
		return get_customer_holidays(start_date, end_date, emp_subsidiary,
		        customer);
	}
}

function get_company_holidays(start_date, end_date, subsidiary) {
	var holiday_list = [];

	start_date = nlapiDateToString(start_date, 'date');
	end_date = nlapiDateToString(end_date, 'date');

	nlapiLogExecution('debug', 'start_date', start_date);
	nlapiLogExecution('debug', 'end_date', end_date);
	nlapiLogExecution('debug', 'subsidiary', subsidiary);

	var search_company_holiday = nlapiSearchRecord('customrecord_holiday',
	        'customsearch_company_holiday_search', [
	                new nlobjSearchFilter('custrecord_date', null, 'within',
	                        start_date, end_date),
	                new nlobjSearchFilter('custrecordsubsidiary', null,
	                        'anyof', subsidiary) ], [ new nlobjSearchColumn(
	                'custrecord_date') ]);

	if (search_company_holiday) {

		for (var i = 0; i < search_company_holiday.length; i++) {
			holiday_list.push(search_company_holiday[i]
			        .getValue('custrecord_date'));
		}
	}

	return holiday_list;
}

function get_customer_holidays(start_date, end_date, subsidiary, customer) {
	var holiday_list = [];

	start_date = nlapiDateToString(start_date, 'date');
	end_date = nlapiDateToString(end_date, 'date');

	nlapiLogExecution('debug', 'start_date', start_date);
	nlapiLogExecution('debug', 'end_date', end_date);
	nlapiLogExecution('debug', 'subsidiary', subsidiary);
	nlapiLogExecution('debug', 'customer', customer);

	var search_customer_holiday = nlapiSearchRecord(
	        'customrecordcustomerholiday', 'customsearch_customer_holiday', [
	                new nlobjSearchFilter('custrecordholidaydate', null,
	                        'within', start_date, end_date),
	                new nlobjSearchFilter('custrecordcustomersubsidiary', null,
	                        'anyof', subsidiary),
	                new nlobjSearchFilter('custrecord13', null, 'anyof',
	                        customer) ], [ new nlobjSearchColumn(
	                'custrecordholidaydate', null, 'group') ]);

	if (search_customer_holiday) {

		for (var i = 0; i < search_customer_holiday.length; i++) {
			holiday_list.push(search_customer_holiday[i].getValue(
			        'custrecordholidaydate', null, 'group'));
		}
	}

	return holiday_list;
}

function not_submitted_count_test(d_Calculated_Start_Date,
        d_Calculated_End_Date, employee, project, customer)
{// function
	// start

	nlapiLogExecution('DEBUG',
	        '------------------------------------------------------');
	nlapiLogExecution('DEBUG', 'd_Calculated_Start_Date',
	        d_Calculated_Start_Date);
	nlapiLogExecution('DEBUG', 'd_Calculated_Start_Date', d_Calculated_End_Date);
	nlapiLogExecution('DEBUG', 'employee', employee);
	nlapiLogExecution('DEBUG', 'project', project);
	nlapiLogExecution('DEBUG',
	        '-----------------------------------------------------------');

	var Holiday_Array = new Array();
	var i_Project_Holiday;
	if (_logValidation(employee)) {
		nlapiLogExecution('DEBUG',
		        '-----------------------------------------------------------',
		        'inside if');
		o_Load_Emp = nlapiLoadRecord('employee', employee);
		i_Emp_Subsidiary = o_Load_Emp.getFieldValue('subsidiary');
		nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
		        'i_Emp_Subsidiary' + i_Emp_Subsidiary);
	}
	if (_logValidation(project)) {
		o_Load_Project = nlapiLoadRecord('job', project);
		i_Project_Holiday = o_Load_Project
		        .getFieldValue('custentityproject_holiday');
		nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
		        'i_Project_Holiday' + i_Project_Holiday);
	}
	// company holiday=1
	// customer holiday=2;
	if (_logValidation(i_Project_Holiday)) {// 1 if start
		if (i_Project_Holiday == 1) {// if start
		
			nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday','i_Project_Holiday==1');
			nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Calculated_Start_Date',d_Calculated_Start_Date);
			nlapiLogExecution('DEBUG','calculate_non_billable_holiday','d_Calculated_End_Date',d_Calculated_End_Date);
			//---------------load company holiday
			//record------------------------
			
			var com_filter = new Array();
			com_filter[1] = new nlobjSearchFilter('custrecordsubsidiary', null,'anyof', i_Emp_Subsidiary);
			com_filter[0] = new nlobjSearchFilter('custrecord_date', null,'within',d_Calculated_Start_Date,d_Calculated_End_Date);
			
			var com_column = new Array();
			com_column[0] = new nlobjSearchColumn('custrecord_date');

			var o_Search_Company_Holiday = nlapiSearchRecord('customrecord_holiday','customsearch_company_holiday_search',com_filter,null);
		//	nlapiLogExecution('DEBUG','calculate_non_billable_holiday','o_Search_Company_Holiday.length'+o_Search_Company_Holiday.length);

			if (_logValidation(o_Search_Company_Holiday)) {// if start
				nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
				        'o_Search_Company_Holiday.length'
				                + o_Search_Company_Holiday.length);
				for (var j = 0; j < o_Search_Company_Holiday.length; j++) {// for
					// start

					var a_search_transaction_result = o_Search_Company_Holiday[j];
					var columns = a_search_transaction_result.getAllColumns();

					var columnLen = columns.length;
					var d_Company_Holiday_Date = a_search_transaction_result
					        .getValue(columns[0]);

					// var d_Company_Holiday_Date=
					// o_Search_Company_Holiday[j].getValue('custrecord_date');
					nlapiLogExecution('DEBUG',
					        'calculate_non_billable_holiday',
					        'd_Company_Holiday_Date' + d_Company_Holiday_Date);
					Holiday_Array[j] = d_Company_Holiday_Date;

				}// for close

			}// if close
		}// if close
		else {// else start
			// ---------------load the customer holiday
			// record-------------------
			nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
			        'customer' + customer);
			var cust_filter = new Array();
			cust_filter[0] = new nlobjSearchFilter('custrecordholidaydate',
			        null, 'within', d_Calculated_Start_Date,
			        d_Calculated_End_Date);
			cust_filter[1] = new nlobjSearchFilter(
			        'custrecordcustomersubsidiary', null, 'is',
			        i_Emp_Subsidiary);
			cust_filter[2] = new nlobjSearchFilter('custrecord13', null, 'is',
			        customer);

			var cust_column = new Array();
			cust_column[0] = new nlobjSearchColumn('custrecordholidaydate');

			var o_Search_Customer_Holiday = nlapiSearchRecord(
			        'customrecordcustomerholiday',
			        'customsearch_customer_holiday', cust_filter, null);

			if (_logValidation(o_Search_Customer_Holiday)) {// if start
				nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
				        'o_Search_Customer_Holiday.length'
				                + o_Search_Customer_Holiday.length);
				for (var j = 0; j < o_Search_Customer_Holiday.length; j++) {// for
					// start

					var a_search_transaction_result = o_Search_Customer_Holiday[j];
					var columns = a_search_transaction_result.getAllColumns();

					var columnLen = columns.length;
					var d_Customer_Holiday_Date = a_search_transaction_result
					        .getValue(columns[0]);

					// var d_Customer_Holiday_Date=
					// o_Search_Customer_Holiday[j].getValue('custrecordholidaydate');
					nlapiLogExecution('DEBUG',
					        'calculate_non_billable_holiday',
					        'd_Customer_Holiday_Date' + d_Customer_Holiday_Date);
					Holiday_Array[j] = d_Customer_Holiday_Date;

				}// for close

			}// if close
			// ---------------------------------------------------------------
		}// else close
	}// 1 if close
	// --------------------------------------------------------------------------------
	var strExcludeHolidayEntries = '';
	if (_logValidation(Holiday_Array)) {// if start
		nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
		        'Holiday_Array' + Holiday_Array);
		nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
		        'Holiday_Array' + Holiday_Array.length);

		for (var ss = 0; ss < Holiday_Array.length; ss++) {// fr start

			nlapiLogExecution('DEBUG', 'inside for loop');
			var s_date = Holiday_Array[ss];
			strExcludeHolidayEntries += ' OR ({employee.subsidiary.id} = TO_NUMBER(\''
			        + i_Emp_Subsidiary
			        + '\') AND {date} = TO_DATE(\''
			        + s_date
			        + '\'))';

			// OR ({employee.subsidiary.id} = TO_NUMBER('3') AND {date} =
			// TO_DATE('24/2/2015'))

		}// for close
	}
	nlapiLogExecution('DEBUG', 'outside for loop');
	// +++++++++++++++++++++++++++++++++++++++++++++++
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('date', null, 'within',
	        d_Calculated_Start_Date, d_Calculated_End_Date);
	filters[1] = new nlobjSearchFilter('employee', null, 'is', employee);
	filters[2] = new nlobjSearchFilter('internalid', 'job', 'is', project);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('formuladate', null, 'count');
	columns[0]
	        .setFormula('CASE WHEN (TO_CHAR({date}, \'D\') = 1 OR TO_CHAR({date},\'D\') = 7) OR ({billable} = \'F\' AND {item.id} != \'Leave\' AND {item.id} != \'Holiday\' AND ({durationdecimal} != 0 OR {rate} = 0)) THEN NULL ELSE {date} END')//
	// columns[0].setFormula('CASE WHEN (TO_CHAR({date}, \'D\') = 1 OR
	// TO_CHAR({date},\'D\') = 7) OR ({billable} = \'F\' AND {item.id} !=
	// \'Leave\' AND {item.id} != \'Holiday\' AND ({durationdecimal} != 0 OR
	// {rate} = 0))'+ strExcludeHolidayEntries +'THEN NULL ELSE {date} END')//

	// CASE WHEN (TO_CHAR({date},'D') = 1 OR TO_CHAR({date},'D') = 7) OR
	// ({billable} = 'F' AND {item.id} != 'Leave' AND {item.id} != 'Holiday' AND
	// ({durationdecimal} != 0 OR {rate} = 0)) OR ({employee.subsidiary.id} =
	// TO_NUMBER('3') AND {date} = TO_DATE('24/2/2015')) THEN NULL ELSE {date}
	// END
	nlapiLogExecution('DEBUG', 'strExcludeHolidayEntries',
	        strExcludeHolidayEntries);
	nlapiLogExecution('DEBUG', 'before search');
	// Search Records
	var search_results = nlapiSearchRecord('timebill','customsearch_mon_pro_not_sub_count_test1', filters, columns);// replaced by praveena 0n 17-03-2020
	// nlapiLogExecution('DEBUG','calculate_non_billable_holiday','search_results'+search_results.length);

	var o_data = new Object();
	if (_logValidation(search_results)) {

		for (var i = 0; i < search_results.length; i++) {// for start

			columns = search_results[i].getAllColumns();

			o_data.employee_id = search_results[i].getValue(columns[1]);
			o_data.employee = search_results[i].getText(columns[1]);
			o_data.project_id = search_results[i].getValue(columns[2]);
			o_data.project = search_results[i].getText(columns[2]);
			o_data.billed_hours = search_results[i].getValue(columns[3]);
			o_data.approved_hours = search_results[i].getValue(columns[4]);
			o_data.submitted_hours = search_results[i].getValue(columns[5]);
			o_data.leave_hours = search_results[i].getValue(columns[6]);
			o_data.holiday_hours = search_results[i].getValue(columns[7]);
			o_data.billed_amount = search_results[i].getValue(columns[8]);
			o_data.approved_amount = search_results[i].getValue(columns[9]);
			o_data.submitted_amount = search_results[i].getValue(columns[10]);
			o_data.working_days_entered = parseInt(search_results[i]
			        .getValue(columns[0]));

			var working_days_entered = parseInt(search_results[i]
			        .getValue(columns[0]));
			nlapiLogExecution('DEBUG', 'calculate_non_billable_holiday',
			        'working_days_entered' + working_days_entered);

		}// for close
	}

	return o_data;

}// fun close