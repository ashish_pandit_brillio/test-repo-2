// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : UES_Invoice_Add_ST_Item_Bill_rate.js
	Author      : Jayesh Dinde
	Date        : 18 July 2016
	Description : Add ST item line to round off bill rate roundoff issue for billable timesheet.


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     BEFORE LOAD
		- beforeLoadRecord(type)



     BEFORE SUBMIT
		- beforeSubmit_invoice(type)


     AFTER SUBMIT
		- afterSubmit_Set_emp_type(type)



     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================

function beforeSubmit_invoice(type)
{
	try
	{
		if(type == 'delete')
			return;
			
		nlapiLogExecution('audit','user:- ',nlapiGetUser());	
		if(nlapiGetUser() != 8177)
			return;
			
		if(nlapiGetFieldValue('entity') != parseInt(1387))
			return;
		
		var emp_list = new Array();
		var a_emp_timesheet_checked = new Array();
		var index = 0;
		var final_amount_invoice = 0;
		var cust_name_with_id = ''; 
		var proj_full_name_with_id = '';
		var emp_full_name = '';
		var onsite_offsite = '';
		var employee_with_id = '';
		
		var project_selected = nlapiGetFieldValue('job');
		var vertical = nlapiLookupField('job',project_selected,'custentity_vertical');
		//Fetch the values from project record
		
		var project_Obj = nlapiLoadRecord('job',project_selected);
		var proj_entity_id = project_Obj.getFieldValue('entityid');
		var proj_name = project_Obj.getFieldValue('altname');
		var proj_category = project_Obj.getFieldText('custentity_project_allocation_category');
		var proj_billing_type = project_Obj.getFieldText('jobbillingtype');

		var customer_V = nlapiGetFieldValue('entity');
		var customer_Obj = nlapiLoadRecord('customer',customer_V);
		var cust_entity_id = customer_Obj.getFieldValue('entityid');
		var cust_name = customer_Obj.getFieldValue('companyname');
		var cust_territory = customer_Obj.getFieldValue('territory');
		var customer_region = customer_Obj.getFieldValue('custentity_region');

		if(cust_entity_id)
		cust_name_with_id = cust_entity_id +' '+ cust_name;
		
		if(proj_entity_id)
		proj_full_name_with_id = proj_entity_id +' '+ proj_name;

		var is_project_monthly_billing = nlapiLookupField('job',project_selected,'custentity_t_and_m_monthly');
		if (is_project_monthly_billing != 'T')
		{
			var round_off_item_count = nlapiGetLineItemCount('item');
			for(var item_count = round_off_item_count; item_count>0 ; item_count--)
			{
				var is_round_off_item = nlapiGetLineItemValue('item','custcol_is_billing_roundoff_item',item_count);
				if(is_round_off_item == 'T')
					nlapiRemoveLineItem('item',item_count);
			}
		
			var billable_time_line_count = nlapiGetLineItemCount('time');
			for(var i=1; i<=billable_time_line_count; i++)
			{
				var is_timesheet_applied = nlapiGetLineItemValue('time','apply',i);
				if(is_timesheet_applied == 'T')
				{
					var rate = nlapiGetLineItemValue('time','rate',i);
					var amount = nlapiGetLineItemValue('time','amount',i);
					var employeeId = nlapiGetLineItemValue('time', 'employee', i);
					var hours = nlapiGetLineItemValue('time','qty',i);
					var hourly_amount = parseFloat(hours) * parseFloat(rate);
					
					if(emp_list.indexOf(employeeId)>=0)
					{
						var emp_already_exist_index = emp_list.indexOf(employeeId);
						var emp_existing_amount_hourly = a_emp_timesheet_checked[emp_already_exist_index].hourly_amount;
						emp_existing_amount_hourly = parseFloat(emp_existing_amount_hourly) + parseFloat(hourly_amount);
						
						var existing_amount_for_emp = a_emp_timesheet_checked[emp_already_exist_index].amount;
						var amount_on_invoice_for_emp = parseFloat(existing_amount_for_emp) + parseFloat(amount);
						
						a_emp_timesheet_checked[emp_already_exist_index] = {
																'emp_id': employeeId,
																'amount':amount_on_invoice_for_emp,
																'rate':rate,
																'hours':hours,
																'hourly_amount':emp_existing_amount_hourly
															};
					}
					else
					{
						emp_list.push(employeeId);
						a_emp_timesheet_checked[index] = {
																'emp_id': employeeId,
																'amount':amount,
																'rate':rate,
																'hours':hours,
																'hourly_amount':hourly_amount
															};
						index++;
					}
				}
			}
			
			for (var j = 0; j < a_emp_timesheet_checked.length; j++)
			{
				var emp_id = a_emp_timesheet_checked[j].emp_id;
				var emp_amount_on_invoice = a_emp_timesheet_checked[j].amount;
				var exact_amount_for_emp = a_emp_timesheet_checked[j].hourly_amount;
				
				var emp_rcrd = nlapiLoadRecord('employee',emp_id);
				var practice = emp_rcrd.getFieldValue('department');
				var location_val = emp_rcrd.getFieldValue('location');
				var emp_name = emp_rcrd.getFieldValue('entityid');
				var person_type = emp_rcrd.getFieldText('custentity_persontype');
				var emp_type = emp_rcrd.getFieldText('employeetype');
				var emp_fusion_id = emp_rcrd.getFieldValue('custentity_fusion_empid');	
				var emp_subsidiary = emp_rcrd.getFieldValue('subsidiary');
				var emp_frst_name = emp_rcrd.getFieldValue('firstname');
				var emp_middl_name = emp_rcrd.getFieldValue('middlename');	
				var emp_lst_name = emp_rcrd.getFieldValue('lastname');

				if(emp_frst_name)
				emp_full_name = emp_frst_name;
											
				if(emp_middl_name)
				emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
				if(emp_lst_name)
				emp_full_name = emp_full_name + ' ' + emp_lst_name;

				if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
				
				if (emp_subsidiary)
										{
											if(emp_subsidiary == 3)
											{
												onsite_offsite = 'Offsite';
											}
											else
											{
												onsite_offsite = 'Onsite';
											}	
										}
				
				var final_amount_for_emp = Number(exact_amount_for_emp) - Number(emp_amount_on_invoice);
				var is_zero = 0;
				var is_final_amount_zero = final_amount_for_emp.toString();
				if(is_final_amount_zero.indexOf(".")>0)
				{
					var temp_index = is_final_amount_zero.indexOf(".")+1;
					var is_zero = is_final_amount_zero.substr(temp_index,2);
				}
				
				nlapiLogExecution('audit','final_amount_for_emp that shld b on invoice st item hourly:-- '+emp_name,final_amount_for_emp);
				
				if (parseFloat(is_zero) != 00)
				{
					nlapiSelectNewLineItem('item');
					nlapiSetCurrentLineItemValue('item', 'item', 2222);
					nlapiSetCurrentLineItemValue('item', 'amount', final_amount_for_emp);
					nlapiSetCurrentLineItemValue('item', 'custcol_employeenamecolumn', emp_name);
					nlapiSetCurrentLineItemValue('item', 'department', practice);
					nlapiSetCurrentLineItemValue('item', 'location', location_val);
					nlapiSetCurrentLineItemValue('item', 'class', vertical);
					nlapiSetCurrentLineItemValue('item', 'description', 'Round Off Item for Employee ' + emp_name);
					nlapiSetCurrentLineItemValue('item', 'custcol_is_billing_roundoff_item', 'T');
					//Income St Lines
							nlapiSetCurrentLineItemValue('item','custcol_onsite_offsite', onsite_offsite);
							nlapiSetCurrentLineItemValue('item','custcol_billing_type',  proj_billing_type);
							nlapiSetCurrentLineItemValue('item','custcol_employee_type',  emp_type);
							nlapiSetCurrentLineItemValue('item','custcol_person_type',  person_type);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_customer_entityid',  cust_entity_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report',  cust_name);
							nlapiSetCurrentLineItemValue('item', 'custcol_territory',  cust_territory);
							
							
								
							nlapiSetCurrentLineItemValue('item', 'custcolcustcol_temp_customer',  cust_name_with_id);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_project_entity_id',  proj_entity_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report',  proj_name);
							
							//if(region_id){
							//o_je_rcrd.setLineItemValue('item', 'custcol_region_master_setup',  region_id);
							//}
							//else if(customer_region){
							
							//}
							nlapiSetCurrentLineItemValue('item', 'custcol_region_master_setup',  customer_region);
							nlapiSetCurrentLineItemValue('item', 'custcol_proj_category_on_a_click',  proj_category);
							
							nlapiSetCurrentLineItemValue('item', 'custcolprj_name',  proj_full_name_with_id);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_employee_entity_id',  emp_fusion_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report',  emp_full_name);

					nlapiCommitLineItem('item');
				}
				
			}
			return;
		}
		
		var billing_from_date = nlapiGetFieldValue('custbody_billfrom');
		billing_from_date = nlapiStringToDate(billing_from_date);
		var billing_start_date = new Date(billing_from_date);
		var billing_to_date = nlapiGetFieldValue('custbody_billto');
		billing_to_date = nlapiStringToDate(billing_to_date);
			
		var round_off_item_count = nlapiGetLineItemCount('item');
		for(var item_count = round_off_item_count; item_count>0 ; item_count--)
		{
			var is_round_off_item = nlapiGetLineItemValue('item','custcol_is_billing_roundoff_item',item_count);
			if(is_round_off_item == 'T')
				nlapiRemoveLineItem('item',item_count);
		}
		
		var billable_time_line_count = nlapiGetLineItemCount('time');
		for(var i=1; i<=billable_time_line_count; i++)
		{
			var is_timesheet_applied = nlapiGetLineItemValue('time','apply',i);
			if(is_timesheet_applied == 'T')
			{
				var amount = nlapiGetLineItemValue('time','amount',i);
				var employeeId = nlapiGetLineItemValue('time', 'employee', i);
				var hours = nlapiGetLineItemValue('time','qty',i);
				var rate = nlapiGetLineItemValue('time','rate',i);
				var hourly_amount = parseFloat(hours) * parseFloat(rate);
				
				if(emp_list.indexOf(employeeId)>=0)
				{
					var emp_already_exist_index = emp_list.indexOf(employeeId);
					var emp_existing_amount = a_emp_timesheet_checked[emp_already_exist_index].amount;
					emp_existing_amount = parseFloat(emp_existing_amount) + parseFloat(amount);
					
					var emp_existing_hours = a_emp_timesheet_checked[emp_already_exist_index].hours;
					emp_existing_hours = parseFloat(emp_existing_hours) + parseFloat(hours);
					
					var emp_existing_total_entry = a_emp_timesheet_checked[emp_already_exist_index].emp_total_entries_selected;
					emp_existing_total_entry = parseFloat(emp_existing_total_entry) + parseFloat(1);
					
					a_emp_timesheet_checked[emp_already_exist_index] = {
															'emp_id': employeeId,
															'amount':emp_existing_amount,
															'emp_total_entries_selected':emp_existing_total_entry,
															'hours':emp_existing_hours
														};
				}
				else
				{
					emp_total_entries_selected = 1;
					emp_list.push(employeeId);
					a_emp_timesheet_checked[index] = {
															'emp_id': employeeId,
															'amount':amount,
															'emp_total_entries_selected':emp_total_entries_selected,
															'hours':hours
														};
					index++;
				}
			}
		}
		
		for(var j=0; j<a_emp_timesheet_checked.length; j++)
		{			
			var emp_id = a_emp_timesheet_checked[j].emp_id;
			var emp_amount_on_invoice = a_emp_timesheet_checked[j].amount;
			var emp_entry_selected = a_emp_timesheet_checked[j].emp_total_entries_selected;
			var total_hrs = a_emp_timesheet_checked[j].hours;
			var emp_rcrd = nlapiLoadRecord('employee',emp_id);
			var practice = emp_rcrd.getFieldValue('department');
			var location_val = emp_rcrd.getFieldValue('location');
			var emp_name = emp_rcrd.getFieldValue('entityid');
			var person_type = emp_rcrd.getFieldText('custentity_persontype');
				var emp_type = emp_rcrd.getFieldText('employeetype');
				var emp_fusion_id = emp_rcrd.getFieldValue('custentity_fusion_empid');	
				var emp_subsidiary = emp_rcrd.getFieldValue('subsidiary');
				var emp_frst_name = emp_rcrd.getFieldValue('firstname');
				var emp_middl_name = emp_rcrd.getFieldValue('middlename');	
				var emp_lst_name = emp_rcrd.getFieldValue('lastname');

				if(emp_frst_name)
				emp_full_name = emp_frst_name;
											
				if(emp_middl_name)
				emp_full_name = emp_full_name + ' ' + emp_middl_name;
											
				if(emp_lst_name)
				emp_full_name = emp_full_name + ' ' + emp_lst_name;

				if(emp_fusion_id)
								employee_with_id = emp_fusion_id +'-'+ emp_full_name;
				
				if (emp_subsidiary)
										{
											if(emp_subsidiary == 3)
											{
												onsite_offsite = 'Offsite';
											}
											else
											{
												onsite_offsite = 'Onsite';
											}	
										}
			
			var filters_search_allocation = new Array();
			filters_search_allocation[0] = new nlobjSearchFilter('project', null, 'anyof', project_selected);
			filters_search_allocation[1] =	new nlobjSearchFilter('startdate', null, 'onorbefore', billing_to_date);
			filters_search_allocation[2] =	new nlobjSearchFilter('enddate', null, 'onorafter', billing_from_date);
			filters_search_allocation[3] =	new nlobjSearchFilter('resource', null, 'anyof', parseInt(emp_id));
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('resource');
			columns[1] = new nlobjSearchColumn('custevent_monthly_rate');
			columns[2] = new nlobjSearchColumn('startdate');
			columns[3] = new nlobjSearchColumn('enddate');
				
			var emp_allocation_result = nlapiSearchRecord('resourceallocation', null, filters_search_allocation, columns);
			if (_logValidation(emp_allocation_result))
			{
				if(emp_allocation_result.length == 1)
				{
					var i_employee_monthly_rate = emp_allocation_result[0].getValue('custevent_monthly_rate');
					var working_days = getWorkingDays(billing_start_date);
					var daily_rate = parseFloat(i_employee_monthly_rate) / parseFloat(working_days);
					
					var startDate =	new Date(billing_from_date.getTime());
					var endDate	= new Date(billing_to_date.getTime());
					var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
					startDate.setHours(0,0,0,1);  // Start just after midnight
					endDate.setHours(23,59,59,999);  // End just before midnight
					var diff = endDate - startDate;  // Milliseconds between datetime objects
					var days = Math.round(diff / millisecondsPerDay);
					
					var final_amount_for_emp = parseFloat(daily_rate) * parseFloat(total_hrs);
					nlapiLogExecution('audit','final_amount_for_emp:- '+final_amount_for_emp,emp_amount_on_invoice);
					
					//final_amount_for_emp = Number(final_amount_for_emp) - Number(emp_amount_on_invoice);
					final_amount_for_emp = Number(i_employee_monthly_rate) - Number(emp_amount_on_invoice);
					
					nlapiSelectNewLineItem('item');
					nlapiSetCurrentLineItemValue('item','item',2222);
					nlapiSetCurrentLineItemValue('item','amount',final_amount_for_emp);
					nlapiSetCurrentLineItemValue('item','custcol_employeenamecolumn',emp_name);
					nlapiSetCurrentLineItemValue('item','department',practice);
					nlapiSetCurrentLineItemValue('item','location',location_val);
					nlapiSetCurrentLineItemValue('item','class',vertical);
					nlapiSetCurrentLineItemValue('item','description','Round Off Item for Employee '+emp_name);
					nlapiSetCurrentLineItemValue('item','custcol_is_billing_roundoff_item','T');

					//Income St Lines
							nlapiSetCurrentLineItemValue('item','custcol_onsite_offsite', onsite_offsite);
							nlapiSetCurrentLineItemValue('item','custcol_billing_type',  proj_billing_type);
							nlapiSetCurrentLineItemValue('item','custcol_employee_type',  emp_type);
							nlapiSetCurrentLineItemValue('item','custcol_person_type',  person_type);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_customer_entityid',  cust_entity_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report',  cust_name);
							nlapiSetCurrentLineItemValue('item', 'custcol_territory',  cust_territory);
							
							
								
							nlapiSetCurrentLineItemValue('item', 'custcolcustcol_temp_customer',  cust_name_with_id);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_project_entity_id',  proj_entity_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report',  proj_name);
							
							//if(region_id){
							//o_je_rcrd.setLineItemValue('item', 'custcol_region_master_setup',  region_id);
							//}
							//else if(customer_region){
							
							//}
							nlapiSetCurrentLineItemValue('item', 'custcol_region_master_setup',  customer_region);
							nlapiSetCurrentLineItemValue('item', 'custcol_proj_category_on_a_click',  proj_category);
							
							nlapiSetCurrentLineItemValue('item', 'custcolprj_name',  proj_full_name_with_id);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_employee_entity_id',  emp_fusion_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report',  emp_full_name);

					nlapiCommitLineItem('item');
					nlapiLogExecution('audit','final_amount_for_emp that shld b on invoice st item:-- '+emp_name,final_amount_for_emp);
				}
				else
				{
					var first_resource_flag = 0;
					var final_amount_for_emp_to_be_on_invoice = 0;
					for(var resource_count=0; resource_count<emp_allocation_result.length; resource_count++)
					{
						var no_of_entries_in_resource = 0;
						var resource_strt_date = emp_allocation_result[resource_count].getValue('startdate');
						resource_strt_date = nlapiStringToDate(resource_strt_date);
						var resource_end_date = emp_allocation_result[resource_count].getValue('enddate');
						resource_end_date = nlapiStringToDate(resource_end_date);
						
						for(var i=1; i<=billable_time_line_count; i++)
						{
							var is_timesheet_applied = nlapiGetLineItemValue('time','apply',i);
							if(is_timesheet_applied == 'T')
							{
								var employeeId_onInvoice = nlapiGetLineItemValue('time', 'employee', i);
									
								if(employeeId_onInvoice == emp_id)
								{
									var is_checked_date = nlapiGetLineItemValue('time', 'billeddate', i);
									
									var resource_strt_date_modi = new Date(resource_strt_date.getTime());
									var resource_end_date_modi = new Date(resource_end_date.getTime());
									var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
									resource_strt_date_modi.setHours(0,0,0,1);  // Start just after midnight
									resource_end_date_modi.setHours(23,59,59,999);  // End just before midnight
									
									var billing_from_date_modi = new Date(billing_from_date.getTime());
									billing_from_date_modi.setHours(0,0,0,1);
									if(resource_strt_date_modi <= billing_from_date)
									{
										is_checked_date = nlapiStringToDate(is_checked_date);
										var is_checked_date_modi = new Date(is_checked_date.getTime());
										is_checked_date_modi.setHours(0,0,0,1);
										if(is_checked_date_modi <= resource_end_date_modi)
										{
											first_resource_flag = 1;
											no_of_entries_in_resource++;
										}
									}
									else
									{
										is_checked_date = nlapiStringToDate(is_checked_date);
										var is_checked_date_modi = new Date(is_checked_date.getTime());
										is_checked_date_modi.setHours(0,0,0,1);
										if(is_checked_date_modi >= resource_strt_date_modi && is_checked_date_modi <= resource_end_date_modi)
										{
											first_resource_flag = 0;
											no_of_entries_in_resource++;
										}
									}
								}
							}
						}
						
						if(first_resource_flag == 1)
						{
							var resource_end_date_new = new Date(resource_end_date);
			
							var i_employee_monthly_rate = emp_allocation_result[resource_count].getValue('custevent_monthly_rate');
							var working_days = getWorkingDays(resource_end_date_new);
							nlapiLogExecution('audit','i_employee_monthly_rate:- '+i_employee_monthly_rate,working_days);
							var daily_rate = parseFloat(i_employee_monthly_rate) / parseFloat(working_days);
		
							var final_amount_for_emp = parseFloat(daily_rate) * parseFloat(no_of_entries_in_resource);
							nlapiLogExecution('audit','final_amount_for_emp :--'+resource_count,final_amount_for_emp);
							final_amount_for_emp_to_be_on_invoice = Number(final_amount_for_emp);
						}
						else
						{
							var resource_strt_date_new = new Date(resource_strt_date);
			
							var i_employee_monthly_rate = emp_allocation_result[resource_count].getValue('custevent_monthly_rate');
							var working_days = getWorkingDays(resource_strt_date_new);
							nlapiLogExecution('audit','i_employee_monthly_rate:- '+i_employee_monthly_rate,working_days);
							var daily_rate = parseFloat(i_employee_monthly_rate) / parseFloat(working_days);
							
							var final_amount_for_emp = parseFloat(daily_rate) * parseFloat(no_of_entries_in_resource);
							nlapiLogExecution('audit','final_amount_for_emp :--'+resource_count,final_amount_for_emp);
							final_amount_for_emp_to_be_on_invoice = Number(final_amount_for_emp_to_be_on_invoice) + Number(final_amount_for_emp);
						}
					}
					
					final_amount_for_emp_to_be_on_invoice = Number(final_amount_for_emp_to_be_on_invoice) - Number(emp_amount_on_invoice);
					nlapiLogExecution('audit','final amount on invoice for st item:- '+final_amount_for_emp_to_be_on_invoice);
					
					nlapiSelectNewLineItem('item');
					nlapiSetCurrentLineItemValue('item','item',2222);
					nlapiSetCurrentLineItemValue('item','amount',final_amount_for_emp_to_be_on_invoice);
					nlapiSetCurrentLineItemValue('item','custcol_employeenamecolumn',emp_name);
					nlapiSetCurrentLineItemValue('item','department',practice);
					nlapiSetCurrentLineItemValue('item','location',location_val);
					nlapiSetCurrentLineItemValue('item','class',vertical);
					nlapiSetCurrentLineItemValue('item','description','Round Off Item for Employee '+emp_name);
					nlapiSetCurrentLineItemValue('item','custcol_is_billing_roundoff_item','T');
					//Income St Lines
							nlapiSetCurrentLineItemValue('item','custcol_onsite_offsite', onsite_offsite);
							nlapiSetCurrentLineItemValue('item','custcol_billing_type',  proj_billing_type);
							nlapiSetCurrentLineItemValue('item','custcol_employee_type',  emp_type);
							nlapiSetCurrentLineItemValue('item','custcol_person_type',  person_type);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_customer_entityid',  cust_entity_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_cust_name_on_a_click_report',  cust_name);
							nlapiSetCurrentLineItemValue('item', 'custcol_territory',  cust_territory);
							
							
								
							nlapiSetCurrentLineItemValue('item', 'custcolcustcol_temp_customer',  cust_name_with_id);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_project_entity_id',  proj_entity_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_proj_name_on_a_click_report',  proj_name);
							
							//if(region_id){
							//o_je_rcrd.setLineItemValue('item', 'custcol_region_master_setup',  region_id);
							//}
							//else if(customer_region){
							
							//}
							nlapiSetCurrentLineItemValue('item', 'custcol_region_master_setup',  customer_region);
							nlapiSetCurrentLineItemValue('item', 'custcol_proj_category_on_a_click',  proj_category);
							
							nlapiSetCurrentLineItemValue('item', 'custcolprj_name',  proj_full_name_with_id);
							
							nlapiSetCurrentLineItemValue('item', 'custcol_employee_entity_id',  emp_fusion_id);
							nlapiSetCurrentLineItemValue('item', 'custcol_emp_name_on_a_click_report',  emp_full_name);

					nlapiCommitLineItem('item');
				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',e);
	}
}

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

// Get Working Days for the month
function getWorkingDays(d_date)
{
	var i_month	=	d_date.getMonth();
	
	var i_year	=	d_date.getFullYear();
	
	var firstDay = new Date(i_year, i_month, 1);
	var lastDay = new Date(i_year, i_month + 1, 0);
	
	return calcBusinessDays(firstDay, lastDay);// - calculate_non_billable_holiday(firstDay, lastDay, i_employee_id, i_project_id, i_customer_id);
}

function calcBusinessDays(d_startDate, d_endDate) { // input given as Date objects
    var startDate	=	new Date(d_startDate.getTime());
    var endDate		=	new Date(d_endDate.getTime());
	// Validate input
    if (endDate < startDate)
        return 0;
    
    // Calculate days between dates
    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
    startDate.setHours(0,0,0,1);  // Start just after midnight
    endDate.setHours(23,59,59,999);  // End just before midnight
    var diff = endDate - startDate;  // Milliseconds between datetime objects    
    var days = Math.round(diff / millisecondsPerDay);
    
    // Subtract two weekend days for every week in between
    var weeks = Math.floor(days / 7);
    var days = days - (weeks * 2);

    // Handle special cases
    var startDay = startDate.getDay();
    var endDay = endDate.getDay();
    
    // Remove weekend not previously removed.   
    if (startDay - endDay > 1)         
        days = days - 2;      
    
    // Remove start day if span starts on Sunday but ends before Saturday
    if (startDay == 0 && endDay != 6)
        days = days - 1  
            
    // Remove end day if span ends on Saturday but starts after Sunday
    if (endDay == 6 && startDay != 0)
        days = days - 1  
    
    return days;
     
    }
