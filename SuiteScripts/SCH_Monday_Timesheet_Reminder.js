/**
 * Send emails every Monday reminding employees to submit the timesheets for the
 * past week in case they have not submitted
 * 
 * Version Date Author Remarks 1.00 Apr 28 2015 Nitish Mishra
 * 1.1 Changes made by shravan on 23 March 2021
 */

var Timesheet_Approval_Status = {
	Approved: '3',
	PendingApproval: '2',
	Rejected: '4',
	Open: '1'
};

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */
function scheduled(type) {
	nlapiLogExecution('debug', 'started');
	var a_table_data = new Array();
	a_table_data.push(["Employee ID", "Employee Name", "Error Code", "Error Message"])
	var b_err_flag = false;
	var arr_Employee_list_Particular_Project = [];

	//* Refer Search 3605 in Prod for UI Help */
	var arr_Resrce_Allcn_Filter = [
		[[["job.type", "anyof", "2"], "AND", ["formulatext: {job.custentity_project_allocation_category}", "contains", "Practice Dedicated Resources"], "AND", ["job.status", "noneof", "1"]], "OR", [["job.type", "anyof", "2"], "AND", ["job.custentity_project_allocation_category", "anyof", "1"], "AND", ["job.status", "noneof", "1"]]],
		"AND",
		["job.internalid", "noneof", "157500"],
		"AND",
		["enddate", "onorafter", "today"]
	];
	var arr_Resrce_Allcn_col = [
		new nlobjSearchColumn("id").setSort(false),
		new nlobjSearchColumn("internalid", "resource", null)
	];

	var obj_Rsc_Allcn_Search = searchRecord('resourceallocation', null, arr_Resrce_Allcn_Filter, arr_Resrce_Allcn_col);
	if (obj_Rsc_Allcn_Search) {
		var i_Employee_Internal_id;
		nlapiLogExecution('DEBUG', 'obj_Rsc_Allcn_Search.length==', obj_Rsc_Allcn_Search.length);
		for (var i_Index_RA_Index = 0; i_Index_RA_Index < obj_Rsc_Allcn_Search.length; i_Index_RA_Index++) {
			i_Employee_Internal_id = obj_Rsc_Allcn_Search[i_Index_RA_Index].getValue("internalid", "resource", null);
			nlapiLogExecution('debug', 'intrenalId', i_Employee_Internal_id);
			if (arr_Employee_list_Particular_Project.indexOf(i_Employee_Internal_id) == -1) {
				arr_Employee_list_Particular_Project.push(i_Employee_Internal_id);
			}
		}
	}
	nlapiLogExecution('DEBUG', 'arr_Employee_list_Particular_Project.length==', arr_Employee_list_Particular_Project.length);

	var a_employee_id_submitted_list = [];
	// get all employees that have submitted the timesheet for the last week
	var search_employee_submitted_timesheet = searchRecord('timesheet', null, [
		new nlobjSearchFilter('approvalstatus', null, 'anyof', [
			Timesheet_Approval_Status.Approved,
			Timesheet_Approval_Status.Rejected,
			Timesheet_Approval_Status.PendingApproval]),
		new nlobjSearchFilter('timesheetdate', null, 'on', 'startoflastweek'),
		new nlobjSearchFilter('employee', null, 'anyof', arr_Employee_list_Particular_Project)], [
		new nlobjSearchColumn('internalid', 'employee', 'group').setSort(),
		new nlobjSearchColumn('firstname', 'employee', 'group'),
		new nlobjSearchColumn('email', 'employee', 'group')]);

	// create a list of the internal id of the employees that submitted

	search_employee_submitted_timesheet.forEach(function (employee) {

		a_employee_id_submitted_list.push(employee.getValue('internalid', 'employee', 'group'));
	});

	nlapiLogExecution('debug', 'no. of employees submitted the timesheet', a_employee_id_submitted_list.length);
	// get all active non-submitted employees list, alongwith the timesheet
	// approver
	var all_employee_list = [];
	var arr_Emp_Mail_Sending = [];

	for (var i_Array_index = 0; i_Array_index < arr_Employee_list_Particular_Project.length; i_Array_index++) {
		if (a_employee_id_submitted_list.indexOf(arr_Employee_list_Particular_Project[i_Array_index]) == -1) {
			arr_Emp_Mail_Sending.push(arr_Employee_list_Particular_Project[i_Array_index]);
		}
	}
	nlapiLogExecution('DEBUG', 'arr_Emp_Mail_Sending.length==', arr_Emp_Mail_Sending.length);
	var filters = [
		new nlobjSearchFilter('internalid', null, 'anyof', arr_Emp_Mail_Sending)
	];
	var all_employee_list = searchRecord('employee', null, filters, [
		new nlobjSearchColumn('email'),
		new nlobjSearchColumn('firstname'),
		new nlobjSearchColumn('timeapprover')
	]);

	nlapiLogExecution('debug', 'no. of employees NOT submitted the timesheet', all_employee_list.length);

	// week dates
	var d_week_end_date = new Date();
	d_week_end_date = nlapiAddDays(d_week_end_date, -2);
	var d_week_start_date = nlapiAddDays(d_week_end_date, -6);
	var s_week_end_date = nlapiDateToString(d_week_end_date, 'date');
	var s_week_start_date = nlapiDateToString(d_week_start_date, 'date');
	var i_time_approver, cc = [];
	var s_time_approver_email;
	var context = nlapiGetContext();

	// send email to all not-submitted employees
	for (var index = 0; index < all_employee_list.length; index++) {
		try {
			yieldScript(context);
			s_reporting_manager_email = null;
			s_time_approver_email = null;
			cc = [constant.EmailId.TimeSheet];

			// get the email Ids of timeapprover
			i_time_approver = all_employee_list[index].getValue('timeapprover');

			if (isNotEmpty(i_time_approver)) {
				s_time_approver_email = nlapiLookupField('employee', i_time_approver, 'email');
				cc.push(s_time_approver_email);
			}
			var mailContent = mondayTimesheetReminderTemplate(
				all_employee_list[index].getValue('firstname'),
				s_week_start_date, s_week_end_date);

			nlapiSendEmail(constant.Employee.InformationSystem,
				all_employee_list[index].getValue('email'),
				mailContent.Subject, mailContent.Body, cc, null, {
				'entity': all_employee_list[index].getId()
			});

			//	nlapiLogExecution('debug', 'send to', all_employee_list[index].getValue('email'));
		}
		catch (er) {
			b_err_flag = true;
			var a_row = new Array();
			a_row.push(all_employee_list[index].getId());
			a_row.push(all_employee_list[index].getValue('entityid'));
			a_row.push(err.code);
			a_row.push(err.message);
			a_table_data.push(a_row);
		}
	}
	if (b_err_flag) {
		Send_Exception_Mail(a_table_data);
	}
	nlapiLogExecution('debug', 'ended');
}

function mondayTimesheetReminderTemplate(first_name, week_start_date,week_end_date) {
	var htmltext = '<table border="0" width="100%">';
	htmltext += '<tr>';
	htmltext += '<td colspan="4" valign="top">';
	htmltext += '<p>Hi ' + first_name + ',</p>';
	htmltext += '<p></p>';

	htmltext += '<p>A gentle reminder!</p>';

	htmltext += '<p>You have not submitted the timesheet for the week <b>'
		+ week_start_date + " - " + week_end_date + '</b>. </p>';

	htmltext += '<p><b>Please enter the timesheet and make sure that you click on "Submit" button to route the timesheet for approval.</b></p>';
	htmltext += '<p>Note: All dates are in MM/DD/YYYY format. </p>';
	// htmltext +=
	// '<p>For any system related issues, please write to <a
	// href="mailto:information.systems@brillio.com">information.systems@brillio.com</a>.</p>';
	htmltext += '<p>Regards,<br/>';
	htmltext += 'Information Systems</p>';

	htmltext += '</td></tr>';
	htmltext += '</table>';
	htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
	htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
	htmltext += '<tr>';
	htmltext += '<td align="right">';
	htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; One System. No Limits.</font>';
	htmltext += '</td>';
	htmltext += '</tr>';
	htmltext += '</table>';
	htmltext += '</body>';
	htmltext += '</html>';

	return {
		Subject: 'Weekly Timesheet Reminder : ' + week_start_date + " - "
			+ week_end_date,
		Body: htmltext
	};
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
				+ state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}

function Send_Exception_Mail(a_table_data) {
	try {
		var context = nlapiGetContext();
		var s_DeploymentID = context.getDeploymentId();
		var i_ScriptID = context.getScriptId();
		var s_Subject = 'Issue on SCH Monday Timesheet Reminder';

		var s_Body = 'This is to inform that SCH Monday Timesheet Reminder is having an issue and System is not able to send the email notification to the employees.';
		s_Body += '<br/>Script: ' + i_ScriptID;
		s_Body += '<br/>Deployment: ' + s_DeploymentID;
		s_Body += '<br/><table width="100%" border="1" cellspacing="1" cellpadding="1">';
		for (var index = 0; index < a_table_data.length; index++) {
			s_Body += '<tr>';
			s_Body += '<td>' + a_table_data[index][0] + '</td>';
			s_Body += '<td>' + a_table_data[index][1] + '</td>';
			s_Body += '<td>' + a_table_data[index][2] + '</td>';
			s_Body += '<td>' + a_table_data[index][3] + '</td>';
			s_Body += '</tr>';
		}
		s_Body += '</table>';
		s_Body += '<br/>Please have a look and do the needfull.';
		s_Body += '<br/><br/>Note: This is system genarated email, incase of any failure while sending the notification to employee(s)';

		s_Body += '<br/><br/>Regards,';
		s_Body += '<br/>Information Systems';
		nlapiSendEmail('422', 'netsuite.support@brillio.com', s_Subject, s_Body,null, null, null, null);
	}
	catch (err) {
		nlapiLogExecution('error', ' Send_Exeception_Mail', err.message);
	}
}
// END Send_Exception_Mail Function
