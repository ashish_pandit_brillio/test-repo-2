dataIn = {
  "RequestType": "GET",
  "Data": [
    {
      "attributes": {
        "type": "Fulfillment_Plan__c",
        "url": "/services/data/v44.0/sobjects/Fulfillment_Plan__c/a2Xq00000007vmkEAA"
      },
      "Name": "FFP-0139",
      "FulFillment_Status__c": "Initiate",
      "practices__c": "Brillio Analytics",
      "Location__c": "US East",
      "Resource_Count__c": 10,
      "Skill__c": "Java/JEE",
      "Opportunity__c": "006q000000HSRQOAA5",
      "Id": "a2Xq00000007vmkEAA",
      "Opportunity__r": {
        "attributes": {
          "type": "Opportunity",
          "url": "/services/data/v44.0/sobjects/Opportunity/006q000000HSRQOAA5"
        },
        "Name": "Verizon",
        "Opportunity_ID__c": "Opp-5849",
        "Type": "New Business",
        "Projection_Status__c": "Stretch",
        "Confidence__c": "20",
        "Organization_Practice__c": "Brillio Analytics",
        "Service_Line__c": "Brillio Data",
        "Project_Start_Date__c": "2018-12-20",
        "Project_End_Date__c": "2019-01-16",
        "CloseDate": "2018-12-19",
        "StageName": "Opportunity Qualified",
        "AccountId": "001q000000kN71DAAS",
        "Id": "006q000000HSRQOAA5",
        "Account": {
          "attributes": {
            "type": "Account",
            "url": "/services/data/v44.0/sobjects/Account/001q000000kN71DAAS"
          },
          "Type": "Prospect",
          "NetSuite_Customer_ID__c": "45hyt677676iu788",
          "Id": "001q000000kN71DAAS"
        }
      }
    },
    {
      "attributes": {
        "type": "Fulfillment_Plan__c",
        "url": "/services/data/v44.0/sobjects/Fulfillment_Plan__c/a2Xq00000007vmlEAA"
      },
      "Name": "FFP-0140",
      "FulFillment_Status__c": "Initiate",
      "practices__c": "DEXA",
      "Location__c": "US West",
      "Resource_Count__c": 5,
      "Skill__c": "Java/JEE;.Net",
      "Opportunity__c": "006q000000HSRQOAA5",
      "Id": "a2Xq00000007vmlEAA",
      "Opportunity__r": {
        "attributes": {
          "type": "Opportunity",
          "url": "/services/data/v44.0/sobjects/Opportunity/006q000000HSRQOAA5"
        },
        "Name": "Verizon",
        "Opportunity_ID__c": "Opp-5849",
        "Type": "New Business",
        "Projection_Status__c": "Stretch",
        "Confidence__c": "20",
        "Organization_Practice__c": "Brillio Analytics",
        "Service_Line__c": "Brillio Data",
        "Project_Start_Date__c": "2018-12-20",
        "Project_End_Date__c": "2019-01-16",
        "CloseDate": "2018-12-19",
        "StageName": "Opportunity Qualified",
        "AccountId": "001q000000kN71DAAS",
        "Id": "006q000000HSRQOAA5",
        "Account": {
          "attributes": {
            "type": "Account",
            "url": "/services/data/v44.0/sobjects/Account/001q000000kN71DAAS"
          },
          "Type": "Prospect",
          "NetSuite_Customer_ID__c": "45hyt677676iu788",
          "Id": "001q000000kN71DAAS"
        }
      }
    }
  ]
}