/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 04 Sep 2015 nitish.mishra
 * 
 */

function clientFieldChanged(type, name, linenum) {
    // discountProportionate();
}

function beforeSubmit(type, form) {
    try {
        var sublist = form.getSubList('apply');
        sublist.addButton('custpage_btn_disc', 'Proportionate Discount', 'discountProportionate');
    } catch (err) {
        nlapiLogExecution('error', 'beforeSubmit', err);
    }
}

function discountProportionate() {
    try {
        var mainDiscount = nlapiGetFieldValue('custbody_payment_main_discount');

        if (mainDiscount) {
            mainDiscount = parseFloat(mainDiscount);

            if (mainDiscount > 0) {
                var totalAmount = getSumOfAllAppliedInvoices();
                nlapiLogExecution('debug', 'totalAmount', totalAmount);
                nlapiLogExecution('debug', 'mainDiscount', mainDiscount);
                proportionateDiscount(totalAmount, mainDiscount);
            } else {
                alert('No Discount Entered');
            }
        } else {
            alert('No Discount Entered');
        }
    } catch (err) {
        nlapiLogExecution('error', 'discountProportionate', err);
    }
}

function getSumOfAllAppliedInvoices() {
    var totalAmount = nlapiGetFieldValue('payment');

    if (!totalAmount)
        totalAmount = 0;

    return parseFloat(totalAmount);
}

function proportionateDiscount(totalAmount, totalDiscount) {
    var invoiceCount = nlapiGetLineItemCount('apply');

    for (var linenum = 1; linenum <= invoiceCount; linenum++) {

        if (nlapiGetLineItemValue('apply', 'apply', linenum) == 'T') {
            var amount = parseFloat(nlapiGetLineItemValue('apply', 'amount',
			        linenum));
            nlapiLogExecution('debug', 'amount', amount);
            var discount = parseFloat(((amount / totalAmount) * totalDiscount)
			        .toFixed(2));
            nlapiLogExecution('debug', 'discount', discount);
            nlapiSelectLineItem('apply', linenum);
            nlapiSetLineItemValue('apply', 'disc', linenum, discount);
        }
    }

    return totalAmount;
}