/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name: SCH_PR_Item_Escalation_Level_Approval.js
	Author     : Shweta Chopde
	Date       : 16 April 2014


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================


function schedulerFunction(type)
{
	var d_date ;
	var i_current_value;
	var i_set_by ;
	var i_PR_status;
	var i_item ;
	var i_item_category;	 
	var i_procurement_team;
	var i_IT_team;
	var i_practice_head;
	var i_no_of_escaltion_days ; 
	var i_supervisor;
	var i_internalID;
		
    try
	{
	   var filter = new Array();
	   //filter[0] = new nlobjSearchFilter('internalidnumber', null, 'greaterthanorequalto', i_counter);
		  			
	 	   
	   var a_search_results = nlapiSearchRecord('customrecord_pritem','customsearch_pr_item_pending_approval',  null, null);
		
	   if(_logValidation(a_search_results))
	   {
	   	 nlapiLogExecution('DEBUG','schedulerFunction',' No of PR - Items  -->  ' + a_search_results.length);
	   	
		 for(var i=0;i<a_search_results.length;i++)
		 {
		    var a_search_transaction_result = a_search_results[i];
	        var columns = a_search_transaction_result.getAllColumns();		
	        var columnLen = columns.length;
			
			for(var t = 0 ; t < columnLen ; t++ )
			{
				var column = columns[t];
		        var label = column.getLabel();
				var value = a_search_transaction_result.getValue(column)
				var text = a_search_transaction_result.getText(column)
				     
				 if(label =='Item')
				 {
				 	i_item = text;
				 }
				 if(label =='Item Category')
				 {
				 	i_item_category = value;
				 }		
				 if(label =='PR Status')
				 {
				 	i_PR_status = value;
				 }
				 if(label =='Date')
				 {
				 	d_date = value;
				 }						
				 if(label =='Current Value')
				 {
				 	i_current_value = value;
				 }				 
				 if(label =='Set By')
				 {
				 	i_set_by = value;
				 }
				  if(label =='Practice Head')
				 {
				 	i_practice_head = value;
				 }						
				 if(label =='IT Team')
				 {
				 	i_IT_team = value;
				 }				 
				 if(label =='Procurement Team')
				 {
				 	i_procurement_team = value;
				 }
				 if(label =='Internal ID')
				 {
				 	i_internalID = value;
				 }			
								
			}//Columns Length	
			 nlapiLogExecution('DEBUG','schedulerFunction',' Internal ID -->  ' + i_internalID);
			 
			 nlapiLogExecution('DEBUG','schedulerFunction',' PR Status  -->  ' + i_PR_status);
   	
			 nlapiLogExecution('DEBUG','schedulerFunction',' Date  -->  ' + d_date);
		   	
			 nlapiLogExecution('DEBUG','schedulerFunction',' Current Value  -->  ' + i_current_value);
		    	
			 nlapiLogExecution('DEBUG','schedulerFunction',' Set By  -->  ' + i_set_by);
			 
			 nlapiLogExecution('DEBUG','schedulerFunction',' Item  -->  ' + i_item);
		    	
			 nlapiLogExecution('DEBUG','schedulerFunction',' Item Category -->  ' + i_item_category);
			 
			 nlapiLogExecution('DEBUG','schedulerFunction',' Practice Head  -->  ' + i_practice_head);
			 
			 nlapiLogExecution('DEBUG','schedulerFunction',' IT Team  -->  ' + i_IT_team);
		    	
			 nlapiLogExecution('DEBUG','schedulerFunction',' Procurement Team -->  ' + i_procurement_team);
			 
			 var d_date_to_string = nlapiStringToDate(d_date)
			 d_date_to_string = get_converted_date(d_date_to_string)
			 nlapiLogExecution('DEBUG','schedulerFunction',' Date To String  -->  ' + d_date_to_string);
			 
			 var d_todays_date = get_todays_date();
			 nlapiLogExecution('DEBUG','schedulerFunction',' Todays Date  -->  ' + d_todays_date);
			 
		   	 var d_no_of_day = getDatediffIndays(d_date_to_string,d_todays_date)	
				 			
		     nlapiLogExecution('DEBUG','schedulerFunction',' *********** No. Of Days **********-->  ' + d_no_of_day);
		 	
         			
		     if(_logValidation(i_internalID))
			 {
			   var o_recordOBJ = nlapiLoadRecord('customrecord_pritem',i_internalID)	
			   
			   if(_logValidation(o_recordOBJ))
			   {			   	 
				 if(i_PR_status == 2)
				 {
				 	i_no_of_escaltion_days = get_escalation_days_sub_unit_head(i_item)
					nlapiLogExecution('DEBUG','schedulerFunction',' *********** Escalation Days Practice Head  **********-->  ' + i_no_of_escaltion_days);
		 	
					
					if((i_no_of_escaltion_days == d_no_of_day) && (i_no_of_escaltion_days!='NA'))
					{					
					 i_supervisor = get_supervisor(i_practice_head)
					 nlapiLogExecution('DEBUG','schedulerFunction',' Practice Head Supervisor  -->  ' + i_supervisor);
			 		 
					  o_recordOBJ.setFieldValue('custrecord_subpracticehead',i_supervisor)		
					 			
					}//Escalation Sub Unit			 	
					
				 }//Practice Head
				 if(i_PR_status == 3)
				 {
				 	i_no_of_escaltion_days = get_escalation_days_sub_unit_head(i_item)
					nlapiLogExecution('DEBUG','schedulerFunction',' *********** Escalation Days IT Team  **********-->  ' + i_no_of_escaltion_days);
		 	
					if((i_no_of_escaltion_days == d_no_of_day) && (i_no_of_escaltion_days!='NA'))
					{					
					 i_supervisor = get_supervisor(i_IT_team)
					 nlapiLogExecution('DEBUG','schedulerFunction',' IT Team Supervisor  -->  ' + i_supervisor);
			 		
					 o_recordOBJ.setFieldValue('custrecord_itforpurchaserequest',i_supervisor)	
					 			
					}//Escalation IT Team		
					
				 }//IT Validation
				 if(i_PR_status == 4)
				 {
				 	i_no_of_escaltion_days = get_escalation_days_sub_unit_head(i_item)
					nlapiLogExecution('DEBUG','schedulerFunction',' *********** Escalation Days Procurement Team  **********-->  ' + i_no_of_escaltion_days);
		 	
					if((i_no_of_escaltion_days == d_no_of_day) && (i_no_of_escaltion_days!='NA'))
					{					
					 i_supervisor = get_supervisor(i_procurement_team)
					 nlapiLogExecution('DEBUG','schedulerFunction',' Procurement Supervisor  -->  ' + i_supervisor);
			 		 
					  o_recordOBJ.setFieldValue('custrecord_procurementteam',i_supervisor)				
					}//Escalation Procurement		
									
				 }//Procurement
				 
				 var i_submitID = nlapiSubmitRecord(o_recordOBJ,true,true)
				 nlapiLogExecution('DEBUG','schedulerFunction',' ******************  PR Item Submit ID ******************   -->  ' + i_submitID);
			 		
				// nlapiLogExecution('DEBUG','schedulerFunction',' ******************  PR Item Submit ID ******************   -->  ' );
			 			
				 
			   }//Record OBJ			 	
				
			 }//Internal ID		  	 
		 
		 }//Loop		 		
		
	   }//Search Results
	   
	}//TRY
    catch(exception)
	{
	  nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
	}//CATCH	

}//Scheduler Function

// END SCHEDULED FUNCTION ===============================================




function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}



function get_todays_date()
{
	// ============================= Todays Date ==========================================
		
	 var date1 = new Date();
     
     var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
   
    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
    
     var day = istdate.getDate();
    
     var month = istdate.getMonth()+1;
     
	 var year=istdate.getFullYear();
	  
	 var today= day + '/' + month + '/' + year;
	 
	 return today;
	  
}




function get_converted_date(date1)
{
	// ============================= Todays Date ==========================================
			
      var offsetIST = 5.5;

    //To convert to UTC datetime by subtracting the current Timezone offset
     var utcdate =  new Date(date1.getTime() + (date1.getTimezoneOffset()*60000));
	      
    //Then cinver the UTS date to the required time zone offset like back to 5.5 for IST
     var istdate =  new Date(utcdate.getTime() - ((-offsetIST*60)*60000));
    
     var day = istdate.getDate();
    
     var month = istdate.getMonth()+1;
    
	 var year=istdate.getFullYear();
	  
	 var today= day + '/' + month + '/' + year;
	 
	 return today;	  
}



function getDatediffIndays(startDate, endDate)
{
     var one_day=1000*60*60*24;
     var fromDate = startDate;
    
	 var toDate=endDate
     
     var x=fromDate.split("/");
     var y=toDate.split("/");
     var date1=new Date(x[2],(x[1]-1),x[0]);
     var date2=new Date(y[2],(y[1]-1),y[0]);
     var month1=x[1]-1;
     var month2=y[1]-1;
     var Diff = Math.ceil((date2.getTime()-date1.getTime())/(one_day));
     nlapiLogExecution('DEBUG', 'getDatediffIndays', 'Date difference Return Value = '+Diff);

     return (Diff);
}

function get_supervisor(i_employee)
{
  var i_supervisor;
 
  if(_logValidation(i_employee))
  {
  	var o_empOBJ = nlapiLoadRecord('employee',i_employee)
	
	if(_logValidation(o_empOBJ))
	{
		i_supervisor = o_empOBJ.getFieldValue('supervisor')		
	}
	
  }//Employee	
	
  return i_supervisor;
}






function get_escalation_days_sub_unit_head(i_item)
{
	var i_no_of_days = 'NA';
	
	 if(i_item == 'LAPTOPS & DESKTOPS')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'SERVER & STORAGE')
	 {
	 	i_no_of_days = 2
	 }	
	 else if(i_item == 'SWITCH & ROUTER')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'Communication Equipment')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'NETWORK  EQUIPMENT')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'Computer Consumables & Peripherals')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'OTHER HARDWARE DEVICES')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'MOBILE PHONE')
	 {
	 	i_no_of_days = 1
	 }	
	 else if(i_item == 'Telephone')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'DATA CARD / CLIP')
	 {
	 	i_no_of_days = 1
	 }
	  else if(i_item == 'AUDIO/VIDEO EQUIPMENTS')
	 {
	 	i_no_of_days = 2
	 }	
	 else if(i_item == 'ELECTRONIC COMPONENTS')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'GIFT/AWARD  MATERIAL')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'ELECTRICAL SUPPLIES')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'AIR CONDITIONERS/ UPS/DG SET')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'PRINTING&STATIONERY')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'HOUSEKEEPING CONSUMABLES')
	 {
	 	i_no_of_days = 1
	 }	
	 else if(i_item == 'SOFTWARE')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'LINKS and TELECOMUNICATION')
	 {
	 	i_no_of_days = 2
	 }
	  else if(i_item == 'FURNITURE & FIXTURE')
	 {
	 	i_no_of_days = 2
	 }	
	 else if(i_item == 'MISCELLANEOUS')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'VEHICLES')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'PROFESSIONAL & CONSULTANCY SERVICES')
	 {
	 	i_no_of_days =  2
	 }
	 else if(i_item == 'ACCOUTING FEE')
	 {
	 	i_no_of_days = 'NA'
	 }	
	 else if(i_item == 'STAFF WELFARE')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'AMC (IT Equipment and Software )')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'AMC  (Non IT Equipment)')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'TRAINING')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'REPAIR & MAINTENANCE ')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'OFFICE MAINTENANCE')
	 {
	 	i_no_of_days = 2
	 }	
	 else if(i_item == 'SUBCONTRACTOR')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'RENTALS  (IT Equipment)')
	 {
	 	i_no_of_days = 2
	 }
	  else if(i_item == 'RENTALS  (Non IT Equipment)')
	 {
	 	i_no_of_days = 2
	 }	
	 else if(i_item == 'RENT')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'UTILITIES')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'COMMUNICATION EXP')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'CAB HIRE CHGS')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'INSURANCE -EMPLOYEE')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'INSURANCE -OTHERS')
	 {
	 	i_no_of_days = 'NA'
	 }
	
	 else if(i_item == 'CLEARING & FORWARDING CHGS')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'RECRUITMENT EXPENSES')
	 {
	 	i_no_of_days = 1
	 }
	  else if(i_item == 'MEMBERSHIP & SEMINAR FEE')
	 {
	 	i_no_of_days = 1
	 }	
	 else if(i_item == 'TRAVEL')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'VISA FEE')
	 {
	 	i_no_of_days = 'NA'
	 	
	 }
	 else if(i_item == 'MARKETING & SALES PROMOTION')
	 {
	 	i_no_of_days = 1
	 }
	  else if(i_item == 'RELOCATION EXPENSES')
	 {
	 	i_no_of_days = 'NA'
	 }	
	 else if(i_item == 'FINANCE COST')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'OTHERS')
	 {
	 	i_no_of_days = 1
	 }
	 
	return i_no_of_days;
}//Sub Unit Head No Of Days



function get_escalation_days_IT_Team(i_item)
{
	var i_no_of_days = 'NA';
	
	 if(i_item == 'LAPTOPS & DESKTOPS')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'SERVER & STORAGE')
	 {
	 	i_no_of_days = 1
	 }	
	 else if(i_item == 'SWITCH & ROUTER')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'Communication Equipment')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'NETWORK  EQUIPMENT')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'Computer Consumables & Peripherals')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'OTHER HARDWARE DEVICES')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'MOBILE PHONE')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'Telephone')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'DATA CARD / CLIP')
	 {
	 	i_no_of_days = 0
	 }
	  else if(i_item == 'AUDIO/VIDEO EQUIPMENTS')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'ELECTRONIC COMPONENTS')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'GIFT/AWARD  MATERIAL')
	 {
	 	i_no_of_days = 2
	 }
	 else if(i_item == 'ELECTRICAL SUPPLIES')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'AIR CONDITIONERS/ UPS/DG SET')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'PRINTING&STATIONERY')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'HOUSEKEEPING CONSUMABLES')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'SOFTWARE')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'LINKS and TELECOMUNICATION')
	 {
	 	i_no_of_days = 1
	 }
	  else if(i_item == 'FURNITURE & FIXTURE')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'MISCELLANEOUS')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'VEHICLES')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'PROFESSIONAL & CONSULTANCY SERVICES')
	 {
	 	i_no_of_days =  0
	 }
	 else if(i_item == 'ACCOUTING FEE')
	 {
	 	i_no_of_days = 'NA'
	 }	
	 else if(i_item == 'STAFF WELFARE')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'AMC (IT Equipment and Software )')
	 {
	 	i_no_of_days = 1
	 }
	 else if(i_item == 'AMC  (Non IT Equipment)')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'TRAINING')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'REPAIR & MAINTENANCE ')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'OFFICE MAINTENANCE')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'SUBCONTRACTOR')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'RENTALS  (IT Equipment)')
	 {
	 	i_no_of_days = 1
	 }
	  else if(i_item == 'RENTALS  (Non IT Equipment)')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'RENT')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'UTILITIES')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'COMMUNICATION EXP')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'CAB HIRE CHGS')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'INSURANCE -EMPLOYEE')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'INSURANCE -OTHERS')
	 {
	 	i_no_of_days = 'NA'
	 }
	
	 else if(i_item == 'CLEARING & FORWARDING CHGS')
	 {
	 	i_no_of_days = 0
	 }
	 else if(i_item == 'RECRUITMENT EXPENSES')
	 {
	 	i_no_of_days = 0
	 }
	  else if(i_item == 'MEMBERSHIP & SEMINAR FEE')
	 {
	 	i_no_of_days = 0
	 }	
	 else if(i_item == 'TRAVEL')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'VISA FEE')
	 {
	 	i_no_of_days = 'NA'
	 	
	 }
	 else if(i_item == 'MARKETING & SALES PROMOTION')
	 {
	 	i_no_of_days = 0
	 }
	  else if(i_item == 'RELOCATION EXPENSES')
	 {
	 	i_no_of_days = 'NA'
	 }	
	 else if(i_item == 'FINANCE COST')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'OTHERS')
	 {
	 	i_no_of_days = 0
	 }
	 
	return i_no_of_days;
}//IT Team No Of Days



function get_escalation_days_procurement(i_item)
{
	var i_no_of_days = 'NA';
	
	 if(i_item == 'LAPTOPS & DESKTOPS')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'SERVER & STORAGE')
	 {
	 	i_no_of_days = 7
	 }	
	 else if(i_item == 'SWITCH & ROUTER')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'Communication Equipment')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'NETWORK  EQUIPMENT')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'Computer Consumables & Peripherals')
	 {
	 	i_no_of_days = 3
	 }
	 else if(i_item == 'OTHER HARDWARE DEVICES')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'MOBILE PHONE')
	 {
	 	i_no_of_days = 3
	 }	
	 else if(i_item == 'Telephone')
	 {
	 	i_no_of_days = 3
	 }
	 else if(i_item == 'DATA CARD / CLIP')
	 {
	 	i_no_of_days = 3
	 }
	  else if(i_item == 'AUDIO/VIDEO EQUIPMENTS')
	 {
	 	i_no_of_days = 4
	 }	
	 else if(i_item == 'ELECTRONIC COMPONENTS')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'GIFT/AWARD  MATERIAL')
	 {
	 	i_no_of_days = 4
	 }
	 else if(i_item == 'ELECTRICAL SUPPLIES')
	 {
	 	i_no_of_days = 3
	 }
	 else if(i_item == 'AIR CONDITIONERS/ UPS/DG SET')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'PRINTING&STATIONERY')
	 {
	 	i_no_of_days = 3
	 }
	 else if(i_item == 'HOUSEKEEPING CONSUMABLES')
	 {
	 	i_no_of_days = 3
	 }	
	 else if(i_item == 'SOFTWARE')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'LINKS and TELECOMUNICATION')
	 {
	 	i_no_of_days = 7
	 }
	  else if(i_item == 'FURNITURE & FIXTURE')
	 {
	 	i_no_of_days = 7
	 }	
	 else if(i_item == 'MISCELLANEOUS')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'VEHICLES')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'PROFESSIONAL & CONSULTANCY SERVICES')
	 {
	 	i_no_of_days =  5
	 }
	 else if(i_item == 'ACCOUTING FEE')
	 {
	 	i_no_of_days = 'NA'
	 }	
	 else if(i_item == 'STAFF WELFARE')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'AMC (IT Equipment and Software )')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'AMC  (Non IT Equipment)')
	 {
	 	i_no_of_days = 7
	 }
	 else if(i_item == 'TRAINING')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'REPAIR & MAINTENANCE ')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'OFFICE MAINTENANCE')
	 {
	 	i_no_of_days = 5
	 }	
	 else if(i_item == 'SUBCONTRACTOR')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'RENTALS  (IT Equipment)')
	 {
	 	i_no_of_days = 5
	 }
	  else if(i_item == 'RENTALS  (Non IT Equipment)')
	 {
	 	i_no_of_days = 5
	 }	
	 else if(i_item == 'RENT')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'UTILITIES')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'COMMUNICATION EXP')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'CAB HIRE CHGS')
	 {
	 	i_no_of_days = 5
	 }
	 else if(i_item == 'INSURANCE -EMPLOYEE')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'INSURANCE -OTHERS')
	 {
	 	i_no_of_days = 'NA'
	 }
	
	 else if(i_item == 'CLEARING & FORWARDING CHGS')
	 {
	 	i_no_of_days = 3
	 }
	 else if(i_item == 'RECRUITMENT EXPENSES')
	 {
	 	i_no_of_days = 5
	 }
	  else if(i_item == 'MEMBERSHIP & SEMINAR FEE')
	 {
	 	i_no_of_days = 5
	 }	
	 else if(i_item == 'TRAVEL')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'VISA FEE')
	 {
	 	i_no_of_days = 'NA'
	 	
	 }
	 else if(i_item == 'MARKETING & SALES PROMOTION')
	 {
	 	i_no_of_days = 5
	 }
	  else if(i_item == 'RELOCATION EXPENSES')
	 {
	 	i_no_of_days = 'NA'
	 }	
	 else if(i_item == 'FINANCE COST')
	 {
	 	i_no_of_days = 'NA'
	 }
	 else if(i_item == 'OTHERS')
	 {
	 	i_no_of_days = 7
	 }
	 
	return i_no_of_days;
}//Procurement No Of Days




// BEGIN FUNCTION ===================================================

// END FUNCTION =====================================================
