/**
 * Module Description
 * 
 * Version      Date                          Author                          Remarks  
 * 1.00         24th  Feb 2016             Manikandan v          
 */
 
 function Main()
{
  try

{
var filters = new Array();
filters[0]  =new nlobjSearchFilter('custrecord_new_question_6',null,'isnotempty');
filters[1] =new nlobjSearchFilter('custrecord_pdf_file_sent', null,'is','F');
filters[2]=new nlobjSearchFilter('formulanumeric', null, 'equalto', '1');
filters[2].setFormula('case when TO_DATE({custrecord_review_period}) < TO_DATE((sysdate)) then 1 else 0 end')
var columns = new Array();
columns[0]  =new nlobjSearchColumn('firstname','custrecord_njcp_employee_name');
columns[1]  =new nlobjSearchColumn('email','custrecord_reporting_manager');
columns[2]  =new nlobjSearchColumn('firstname', 'custrecord_reporting_manager');
columns[3]  =new nlobjSearchColumn('internalid','custrecord_njcp_employee_name');
columns[4]  =new nlobjSearchColumn('custentity_probationenddate','custrecord_njcp_employee_name');
columns[5]  =new nlobjSearchColumn('internalid');
columns[6]    = new nlobjSearchColumn('middlename','custrecord_njcp_employee_name');
columns[7]    = new nlobjSearchColumn('lastname','custrecord_njcp_employee_name');
columns[8]     =new nlobjSearchColumn('custrecord_employee_designation');
columns[9]     = new nlobjSearchColumn('custrecord_new_answer_6');  
columns[10]	=new nlobjSearchColumn('custentity_fusion_empid', 'custrecord_njcp_employee_name');   
columns[11]	=new nlobjSearchColumn('custrecord_date_of_joining');
columns[12]=new nlobjSearchColumn('custrecord_review_period');

// search for employee's reporting Manager

var emp_search = nlapiSearchRecord('customrecord_confirmation_process', null,filters,columns);

if(isNotEmpty(emp_search))

{
	
	nlapiLogExecution('debug', 'emp_search.length',emp_search.length);
	
	for(var i=0;i<emp_search.length;i++)
	{
     
	 
		var firstName = emp_search[i].getValue('firstname','custrecord_njcp_employee_name');
		var middleName = emp_search[i].getValue('middlename','custrecord_njcp_employee_name');
		var lastName = emp_search[i].getValue('lastname','custrecord_njcp_employee_name');
		var employeeNumber=emp_search[i].getValue('custentity_fusion_empid', 'custrecord_njcp_employee_name');
		var title=emp_search[i].getValue('custrecord_employee_designation');
		var actual_hire_date=emp_search[i].getValue('custrecord_date_of_joining');
		var probation_end_date=emp_search[i].getValue('custrecord_review_period');
		var overallRating = emp_search[i].getValue('custrecord_new_answer_6');
		var reporting_manager_email = emp_search[i].getValue('email', 'custrecord_reporting_manager');
		var  employee_id =emp_search[i].getValue('internalid','custrecord_njcp_employee_name');
		//sendEmail(firstName, employee_id,reporting_manager_email);
	 
		generatePDF(firstName,middleName,lastName,title,employeeNumber,overallRating,actual_hire_date,probation_end_date,employee_id,reporting_manager_email);
		
		nlapiSubmitField('customrecord_confirmation_process', emp_search[i].getValue('internalid'), 'custrecord_pdf_file_sent', 'T');
	 
	 /*sendServiceMails(
			emp_search[i].getValue('firstname','custrecord_njcp_employee_name'),
			emp_search[i].getValue('email','custrecord_reporting_manager'),
			emp_search[i].getValue('firstname', 'custrecord_reporting_manager'),
			emp_search[i].getValue('internalid','custrecord_njcp_employee_name'),
			emp_search[i].getValue('custentity_probationenddate','custrecord_njcp_employee_name'),
			emp_search[i].getValue('internalid'));*/
	}

}

}
catch(err){
          nlapiLogExecution('error', 'Main', err);
}
}

function sendServiceMails(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date,feedback_record_id)
{
	
	try{
                //var AttachmentID=32680;
                //var fileObj = nlapiLoadFile();
		var mailTemplate = serviceTemplate(firstName, reporting_mng_email, reporting_mng_first_name, employee_internal_id, probation_end_date, feedback_record_id);		
		nlapiLogExecution('debug', 'chekpoint',reporting_mng_email);
		nlapiSendEmail(5803, reporting_mng_email, mailTemplate.MailSubject, mailTemplate.MailBody,['waseem.pasha@brillio.com'],null,null,null,null);
		
	}
catch(err){
          nlapiLogExecution('error', 'sendServiceMails', err);
          throw err;
     }
}

function generatePDF(firstName,middleName,lastName,title,employeeNumber,overallRating,custentity_actual_hire_date,probation_end_date,employee_id,reporting_manager_email)
{
	var htmltext = '';
	
	htmltext += '';
	//Search the rating value if rating is Outstanding,Exceed Expectations, Met Expectations Confirmation Form will be Generated

	if (overallRating == 'Outstanding'|| overallRating == 'Exceeded Expectation'|| overallRating == 'Met Expectation')
	{
		htmltext += '<p style="margin-right:380px;">Mr.'+ firstName +''+ middleName + ''+ lastName +'  </p>';
		htmltext += '<p style="margin-right:380px;"> '+title+'</p>';
		htmltext += '<p style="margin-left:380px;">Employee Number : '+employeeNumber+'</p>';
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Dear ' + firstName + ',</p>';
		//htmltext += '&nbsp';
		
		//htmltext += &nbsp;
		htmltext += '<p> Your performance for the period  ' + custentity_actual_hire_date + ' to ' + probation_end_date + '  has been assessed as "<b>'+overallRating+' </b>" Consequently, we are pleased to inform you that with effect from ' + probation_end_date + ' your employment with the Company is confirmed All other terms and conditions of your employment as stated in the Appointment Letter remain the same.We would like to express our sincere appreciation for your efforts and look forward to your continued contribution to the success of the company.</p>';
		htmltext +='<p>Wishing you the very best in the coming years</p>';
		
		htmltext += '<p>Yours sincerely</p>';
		htmltext += '<p>for Brillio Technologies Pvt. Ltd.,</p>';


		htmltext +='<p><b>Hemavathi J</b></p>';

		htmltext +='<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=131760&amp;c=3883006&amp;h=888054e2a84780788144"/></p>';
										  
		htmltext +='<p><b>Senior Manager - Human Resources</b></p>';


		htmltext += '</td></tr>';
		htmltext += '</table>';
	}
	else if (overallRating == 'Short of Expectation')
	{
		htmltext += '<p style="margin-right:380px;">Mr.'+ firstName +''+ middleName + ' '+ lastName +'  </p>';
		htmltext += '<p style="margin-right:380px;"> '+title+'</p>';
		htmltext += '<p style="margin-left:380px;">Employee Number : '+employeeNumber+'</p>';
		
		htmltext += '<table border="0" width="100%"><tr>';
		htmltext += '<td colspan="4" valign="top">';
		//htmltext += '<p>Hi ' + firstName + ',</p>';  
		htmltext += '<p>Dear '+ firstName +', </p>';
		//htmltext += &nbsp;
		htmltext += '<p> Your performance for the period  '+custentity_actual_hire_date+' to '+probation_end_date+' has been assessed as "<b>'+overallRating+'</b>" Consequently, your probation period has been extended for further Months with effect from '+probation_end_date+' </p>';
					
					
					
		htmltext += '<p>Yours sincerely</p>';
		htmltext += '<p>for Brillio Technologies Pvt. Ltd.,</p>';

		htmltext +='<p><b>Hemavathi J</b></p>';
		htmltext +='<p><img src="https://system.na1.netsuite.com/core/media/media.nl?id=131760&amp;c=3883006&amp;h=888054e2a84780788144"/></p>';
		htmltext +='<p><b>Senior Manager - Human Resources</b></p>';
	 
		htmltext += '</td></tr>';
		htmltext += '</table>';
	}
	
	htmltext += "";

	var filePDF = nlapiXMLToPDF('<pdf><body>' + htmltext + '</body></pdf>');
	filePDF.setName(firstName+'_'+lastName+'_'+(new Date()).toString() + '.pdf')
	filePDF.setFolder(65654);
	
	nlapiSubmitFile(filePDF);
	
	nlapiSendEmail(10730,employee_id,'Confirmation', htmltext,['waseem.pasha@brillio.com',reporting_manager_email],null,null,filePDF);
}









