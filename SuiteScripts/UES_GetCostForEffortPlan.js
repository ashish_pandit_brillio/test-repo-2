/**
 * @author Jayesh
 */

function afterSubmit_getEffortCost()
{
	try
	{
		var i_revenue_cap = nlapiGetFieldValue('custrecord_effort_plan_revenue_cap');
		var i_practice = nlapiGetFieldValue('custrecord_effort_plan_practice');
		var i_sub_practice = nlapiGetFieldValue('custrecord_effort_plan_sub_practice');
		var i_role_selected = nlapiGetFieldValue('custrecord_effort_plan_role');
		var i_level_selected = nlapiGetFieldValue('custrecord_effort_plan_level');
		var i_location_selected = nlapiGetFieldValue('custrecord_effort_plan_location');
		
		var a_cost_setup_filter = [['custrecord_fp_cost_setup_practice', 'anyof', i_practice], 'and',
									['custrecord_fp_cost_setup_sub_practice', 'anyof', i_sub_practice], 'and',
									['custrecord_fp_cost_setup_level', 'anyof', i_level_selected]];
			
		var a_fld_location_filters=['custrecord_revenue_sourcetype','anyof',3];
		var o_cost_location=Search_revenue_Location_subsdidary(a_fld_location_filters);
		o_cost_location=o_cost_location["Cost Reference"];
		var a_columns_cost_setup = new Array();
		for(var key in o_cost_location)
		{
			a_columns_cost_setup.push(new nlobjSearchColumn(o_cost_location[key]));
		}
		
		var a_get_cost_setup_role_level = nlapiSearchRecord('customrecord_fp_cost_setup', null, a_cost_setup_filter, a_columns_cost_setup);
		if (a_get_cost_setup_role_level)
		{
			var i_cost_location_cost=o_cost_location[i_location_selected]?o_cost_location[i_location_selected]:'custrecord_fp_cost_setup_us_cost';
			nlapiSubmitField(nlapiGetRecordType(),nlapiGetRecordId(),'custrecord_cost_for_resource',a_get_cost_setup_role_level[0].getValue(i_cost_location_cost));
		}
		
		var a_revenue_share_filter = [['custrecord_revenue_share_per_practice_ca', 'anyof', i_revenue_cap], 'and',
									['custrecord_revenue_share_per_practice_pr', 'anyof', i_practice], 'and',
									['custrecord_revenue_share_per_practice_su', 'anyof', i_sub_practice]];
			
		var a_columns_revenue_share = new Array();
		a_columns_revenue_share[0] = new nlobjSearchColumn('created').setSort(true);
		a_columns_revenue_share[1] = new nlobjSearchColumn('custrecord_revenue_share_per_practice_re');
		
		var a_get_revenue_share = nlapiSearchRecord('customrecord_revenue_share_per_practice', null, a_revenue_share_filter, a_columns_revenue_share);
		if (a_get_revenue_share)
		{
			var f_revenue_share = a_get_revenue_share[0].getValue('custrecord_revenue_share_per_practice_re');
			nlapiSubmitField(nlapiGetRecordType(),nlapiGetRecordId(),'custrecord_revenue_share_effort_plan',f_revenue_share);
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','afterSubmit_getCost','ERROR MESSAGE :- '+err);
	}
}