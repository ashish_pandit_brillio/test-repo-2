/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Nov 2016     deepak.srinivas
 *
 */

/**
 Scripts creates three-project_tasks when project is created for 1st time
 Also marks Holiday & Leave project_tasks as non-billable by default
 */

function userEventAfterSubmit(type){
  try{
	  //Create only if type is 'create'
	  
	  nlapiLogExecution('DEBUG','type',type);
	  nlapiLogExecution('DEBUG','Record ID',nlapiGetRecordId());
	  var context = nlapiGetContext();
	  var ex_context = context.getExecutionContext();
	  nlapiLogExecution('DEBUG','Record ex_context',ex_context);
	  if(type == 'create' || type == 'edit'){
		//Get the record id of Project  
	  var rec_id = nlapiGetRecordId();
	  if(rec_id){
		  var proj_rec = nlapiLoadRecord('job',rec_id);
		  var proj_type = proj_rec.getFieldValue('jobtype');
		  var proj_ID = rec_id;
	  
		  
	  		nlapiLogExecution('DEBUG', ' proj_ID InternalID', proj_ID);
	  		nlapiLogExecution('DEBUG', ' proj_type InternalID', proj_type);
	  		
	  		//Search For project Task
	  		var filters = Array();
	  		filters.push(new nlobjSearchFilter('company',null,'anyof',parseInt(proj_ID)));
	  		
	  		var cols = Array();
	  		cols.push(new nlobjSearchColumn('internalid'));
	  		cols.push(new nlobjSearchColumn('title'));
	  		
	  		var projetTask_search = nlapiSearchRecord('projecttask',null,filters,cols);
	  		if(projetTask_search){
	  			
	  		}
	  		else{
	  			
	  			//Create Project Activities
	  			if(parseInt(proj_type) == 2){ //External Project
						//Create three project_tasks by looping in for-loop
					var project_task = nlapiCreateRecord('projecttask'); 
					project_task.setFieldValue('company', proj_ID); //set the project into project_task
					project_task.setFieldValue('title', 'Project Activites');	
					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
	  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
	  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
	  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
				}
				else{
					var project_task = nlapiCreateRecord('projecttask'); 
					project_task.setFieldValue('company', proj_ID); //set the project into project_task
					project_task.setFieldValue('title', 'Project Activites');	
					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
	  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
	  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
	  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
				}
	  			
	  			//Create Holiday
	  			if(parseInt(proj_type) == 2){ //External Project
						//Create three project_tasks by looping in for-loop
					var project_task = nlapiCreateRecord('projecttask'); 
					project_task.setFieldValue('company', proj_ID); //set the project into project_task
					project_task.setFieldValue('title', 'Holiday');	
					project_task.setFieldValue('nonbillabletask', 'T');
					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
					project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
					var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
					nlapiLogExecution('DEBUG', 'Holiday Id', project_taskId);
	  				}
				else{
					var project_task = nlapiCreateRecord('projecttask'); 
					project_task.setFieldValue('company', proj_ID); //set the project into project_task
					project_task.setFieldValue('title', 'Holiday');	
					project_task.setFieldValue('nonbillabletask', 'T');
					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
					project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
					var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
					nlapiLogExecution('DEBUG', 'Holiday Id', project_taskId);
					}
	  			//Create Leave Task
	  			if(parseInt(proj_type) == 2){ //External Project
						//Create three project_tasks by looping in for-loop
						var project_task = nlapiCreateRecord('projecttask'); 
						project_task.setFieldValue('company', proj_ID); //set the project into project_task
						project_task.setFieldValue('title', 'Leave');	
						project_task.setFieldValue('nonbillabletask', 'T');
						project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
		  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
		  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
		  			nlapiLogExecution('DEBUG', 'Leave Activites Id', project_taskId);
					}
					else{
						var project_task = nlapiCreateRecord('projecttask'); 
						project_task.setFieldValue('company', proj_ID); //set the project into project_task
						project_task.setFieldValue('title', 'Leave');	
						project_task.setFieldValue('nonbillabletask', 'T');
						project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
		  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
		  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
		  			nlapiLogExecution('DEBUG', 'Leave Activites Id', project_taskId);
					}
	  			//Create OT Task
	  			if(parseInt(proj_type) == 2){ //External Project
						//Create three project_tasks by looping in for-loop
	  				var project_task = nlapiCreateRecord('projecttask'); 
	  				project_task.setFieldValue('company', proj_ID); //set the project into project_task
	  				project_task.setFieldValue('title', 'OT');	
	  				project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
	  				project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
	  				var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
	  				nlapiLogExecution('DEBUG', 'OT Activites Id', project_taskId);
	  				}
		  		}
	  			/*for(var i_index = 0 ;i_index <projetTask_search.length; i_index++){
	  				var s_title = projetTask_search[i_index].getvalue('title');
	  				if(s_title != 'Project Activites' )//|| s_title != 'Holiday' || s_title != 'Leave'
	  				{
	  					if(parseInt(proj_type) == 2){ //External Project
		  							//Create three project_tasks by looping in for-loop
		  					var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'Project Activites');	
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
		  				}
	  					else{
	  						var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'Project Activites');	
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
	  					}
	  				}
	  				if(s_title != 'Holiday' )//|| s_title != 'Holiday' || s_title != 'Leave'
	  				{
	  					if(parseInt(proj_type) == 2){ //External Project
		  							//Create three project_tasks by looping in for-loop
		  					var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'Holiday');	
		  					project_task.setFieldValue('nonbillabletask', 'T');
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
		  				}
	  					else{
	  						var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'Holiday');	
		  					project_task.setFieldValue('nonbillabletask', 'T');
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
	  					}
	  				}
	  				if(s_title != 'Leave' )//|| s_title != 'Holiday' || s_title != 'Leave'
	  				{
	  					if(parseInt(proj_type) == 2){ //External Project
		  							//Create three project_tasks by looping in for-loop
		  					var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'Leave');	
		  					project_task.setFieldValue('nonbillabletask', 'T');
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
		  				}
	  					else{
	  						var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'Leave');	
		  					project_task.setFieldValue('nonbillabletask', 'T');
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
	  					}
	  				}
	  				if(s_title != 'OT' )//|| s_title != 'Holiday' || s_title != 'Leave'
	  				{
	  					if(parseInt(proj_type) == 2){ //External Project
		  							//Create three project_tasks by looping in for-loop
		  					var project_task = nlapiCreateRecord('projecttask'); 
		  					project_task.setFieldValue('company', proj_ID); //set the project into project_task
		  					project_task.setFieldValue('title', 'OT');	
		  					project_task.setFieldValue('estimatedwork', 100); //Hard-coded estimated work to 100
				  			project_task.setFieldValue('status', 'PROGRESS'); //Hard-coded project_task status to 'In Progress'
				  			var project_taskId = nlapiSubmitRecord(project_task); //Submit the record and creates the project_task
				  			nlapiLogExecution('DEBUG', 'Project Activites Id', project_taskId);
		  				}
	  				
	  				}
	  			}*/
	  		          
	  	}
	  }
  }
	  catch(e){
		  nlapiLogExecution('DEBUG','Creating Project project_task Error',e);
	  }
	  
}
	  
