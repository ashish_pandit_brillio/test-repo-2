// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
    /*
     * Script Name: SCH_PM_Not_Submitted_Notification
     * Author: Vikrant Adhav
     * Company: Aashna CloudTech
     * Date: 18-11-2014
     * Script Modification Log: --
     * Date -- -- Modified By -- --Requested By-- -- Description --
     
     * Below is a summary of the process controls enforced by this script file. The control
     * logic is described more fully, below, in the appropriate function headers
     * and code blocks. SCHEDULED FUNCTION -
     * SCH_PM_Not_Submitted_Notification(type) SUB-FUNCTIONS - The following
     * sub-functions are called by the above core functions in order to maintain
     * code modularization: - NOT USED
     */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN SCHEDULED FUNCTION =============================================
function SCH_PM_Not_Submitted_Notification()//
{
    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'Into Scheduled script...');
    
    var context = nlapiGetContext();
    var usageRemaining = context.getRemainingUsage();
    
    var counter = context.getSetting('SCRIPT', 'custscript_counter_tm');
    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'counter : ' + counter);
    
    try //
    {
        // Fetch the list of all employees who has filled time sheet for last
        // week
        var a_submitted_Emp_List = new Array();
        
        var initialID_2 = 0;
        var a_filters_2 = new Array();
        
        do //
        {
            a_filters_2[a_filters_2.length] = new nlobjSearchFilter('internalidnumber', 'employee', 'greaterthan', initialID_2);
            
            var search_Emp_List = nlapiSearchRecord('timebill', 'customsearch_submitted_emp_list_last_wee', a_filters_2, null);
            
            if (_validate(search_Emp_List)) //
            {
                var a_all_columns_2 = search_Emp_List[0].getAllColumns();
                
                for (var counter_j = 0; counter_j < search_Emp_List.length; counter_j++) //
                {
                    a_submitted_Emp_List.push(search_Emp_List[counter_j].getValue(a_all_columns_2[0]));
                    initialID_2 = search_Emp_List[counter_j].getValue(a_all_columns_2[0]);
                }
            }
        }
        while (!_validate(search_Emp_List)) //
        {
        
        }
        nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'a_submitted_Emp_List : ' + a_submitted_Emp_List.length);
        
        // Fetch All project manager's list with active projects
        var a_project_managers = new Array();
        
        do //
        {
            var search_Pro_managers = null;
            var a_all_column_1 = null;
            var filters_1 = new Array();
            var initialID_1 = 0;
            
            filters_1[filters_1.length] = new nlobjSearchFilter('internalidnumber', 'custentity_projectmanager', 'greaterthan', initialID_1);
            search_Pro_managers = nlapiSearchRecord('job', 'customsearch_list_project_managers', null, null);
            
            if (_validate(search_Pro_managers)) //
            {
                nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'search_Pro_managers : ' + search_Pro_managers.length);
                
                a_all_column_1 = search_Pro_managers[0].getAllColumns();
                
                for (var counter_i = 0; counter_i < search_Pro_managers.length; counter_i++) //
                {
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', '****' +
                    counter_i +
                    '****');
                    a_project_managers.push(search_Pro_managers[counter_i].getValue(a_all_column_1[0]));
                    
                    initialID_1 = search_Pro_managers[counter_i].getValue(a_all_column_1[0]);
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'initialID_1 : ' + initialID_1);
                    
                    var s_manager_name = search_Pro_managers[counter_i].getValue(a_all_column_1[1]);
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 's_manager_name : ' + s_manager_name);
                    
                    var s_manager_email = search_Pro_managers[counter_i].getValue(a_all_column_1[2]);
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 's_manager_email : ' + s_manager_email);
                    
                    var a_projects = new Array(); // used to set as projects
                    // in next search
                    // For each project manager find the projects
                    var a_filters_3 = new Array();
                    a_filters_3[a_filters_3.length] = new nlobjSearchFilter('custentity_projectmanager', null, 'anyof', initialID_1);
                    
                    // var a_column_3 = new Array();
                    // a_column_3[a_column_3.length] = new
                    // nlobjSearchColumn('internalid')
                    
                    var search_Projects = nlapiSearchRecord('job', 'customsearch_project_for_manager', a_filters_3, null);
                    
                    if (_validate(search_Projects)) //
                    {
                        nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'search_Projects : ' + search_Projects.length);
                        var a_all_columns_3 = search_Projects[0].getAllColumns();
                        
                        for (var counter_k = 0; counter_k < search_Projects.length; counter_k++) //
                        {
                            a_projects.push(search_Projects[counter_k].getValue(a_all_columns_3[0]));
                        }
                    }
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'a_projects : ' + a_projects);
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'a_projects : ' + a_projects.length);
                    
                    // For all projects find the resources that are allocated
                    // till end of last week
                    
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'initialID_1 : ' + initialID_1);
                    var a_filters_4 = new Array();
                    a_filters_4[a_filters_4.length] = new nlobjSearchFilter('custentity_projectmanager', 'job', 'anyof', initialID_1.toString());
                    
                    var a_columns_4 = new Array();
                    var a_Emp_Allocated = new Array();
                    
                    //var search_emp_allocated = nlapiSearchRecord('resourceallocation', 'customsearch_list_of_emp_allocated_last', null, null);
					var search_emp_allocated = nlapiSearchRecord('resourceallocation', 'customsearch_last_week_emp', a_filters_4, null);
					
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'search_emp_allocated : ' + search_emp_allocated);
					// nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'search_emp_allocated : ' + search_emp_allocated.length);
                    
                    if (_validate(search_emp_allocated)) //
                    {
                        nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'search_emp_allocated : ' + search_emp_allocated.length);
                        
                        var a_all_column_4 = search_emp_allocated[0].getAllColumns();
                        
                        for (var counter_l = 0; counter_l < search_Projects.length; counter_l++) //
                        {
                            a_Emp_Allocated.push(search_Projects[counter_l].getValue(a_all_column_4[0]));
                        }
                    }
                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'a_Emp_Allocated : ' + a_Emp_Allocated.length);
                    
                    // Compare the list of employee from time sheet and the
                    // employees assigned to projects
                    
                    var a_emp_not_submitted = new Array();
                    
                    if (_validate(a_Emp_Allocated) && _validate(a_submitted_Emp_List)) //
                    {
                        for (var counter_m = 0; counter_m < a_Emp_Allocated; a_Emp_Allocated++) //
                        {
                            var submitted_flag = false;
                            var s_emp_allocated = a_Emp_Allocated[counter_m];
                            
                            for (var counter_n = 0; counter_n < a_submitted_Emp_List; a_Emp_Allocated++) //
                            {
                                var s_submitted_Emp = a_submitted_Emp_List[counter_n];
                                
                                if (s_emp_allocated == s_submitted_Emp) //
                                {
                                    submitted_flag = true;
                                    break;
                                }
                            }
                            
                            if (submitted_flag) //
                            {
                                continue;
                            }
                            else //
                            {
                                a_emp_not_submitted.push(s_emp_allocated);
                            }
                        }
                    }
                    
                    // If anyone is left then send mail to respective project
                    // manager.
                    
                    if (_validate(a_emp_not_submitted)) //
                    {
                        if (a_emp_not_submitted.length > 0) //
                        {
                            var htmltext = '<table border="0" width="100%">';
                            htmltext += '<tr>';
                            htmltext += '<td colspan="4" valign="top">';
                            
                            htmltext += '<p>Hi ' + s_manager_name + ',</p>';
                            htmltext += '<p></p>';
                            htmltext += '<p>Following is a list if employees who has not submitted thier time sheet for last week.</p>';
                            htmltext += '<p></p>';
                            htmltext += '<p>Kindly do followup with them ASAP.</p>';
                            htmltext += '<p></p>';
                            
                            htmltext += '<table>';
                            htmltext += '<tr>';
                            htmltext += '<td>SR. No.';
                            htmltext += '</td>';
                            htmltext += '<td>Employee Name';
                            htmltext += '</td>';
                            
                            for (var counter_o = 0; counter_o < a_emp_not_submitted.length; counter_o++) //
                            {
                                htmltext += '<td>' + counter_o + 1;
                                htmltext += '</td>';
                                htmltext += '<td>' +
                                a_emp_not_submitted[counter_o];
                                htmltext += '</td>';
                            }
                            
                            htmltext += '</tr>';
                            htmltext += '</table>';
                            
                            htmltext += '</td></tr>';
                            htmltext += '<tr></tr>';
                            htmltext += '<tr></tr>';
                            htmltext += '<tr></tr>';
                            htmltext += '<p>Regards,</p>';
                            htmltext += '<p>Information Systems</p>';
                            htmltext += '<p>This e-mail contains Privileged and Confidential Information intended solely for the use of the addressee(s). It shall not attach any liability on the sender or Brillio or its affiliates. Any views or opinions presented in this email are solely those of the sender and may not necessarily reflect the opinions of Brillio or its affiliates. If you are not the intended recipient, you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately and destroy all copies of this message and any attachments. WARNING: Computer viruses can be transmitted via email. While Brillio has taken reasonable precautions to minimize this risk, Brillio accepts no liability for any damage that may be caused to you in the event that there is any virus in this e-mail or any attachments attached hereto. It is the addresses(s) duty to check and scan this email and any attachments attached hereto for the presence of viruses prior to opening the email. ** Thank You **</p>';
                            htmltext += '</table>';
                            htmltext += '<hr width="100%" size="1" noshade color="#CCCCCC">';
                            htmltext += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                            htmltext += '<tr>';
                            htmltext += '<td align="right">';
                            htmltext += '<font style="font-size:9px; font-family:Verdana,Arial,Helvetica,sans-serif; color:#999999;">' +
                            +'Brillio is powered by <a href="http://www.netsuite.com/" style="color:#999999;">NetSuite</a> &#151; ' +
                            +'One System. No Limits.</font>';
                            htmltext += '</td>';
                            htmltext += '</tr>';
                            htmltext += '</table>';
                            htmltext += '</body>';
                            htmltext += '</html>';
                            nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'htmltext : ' + htmltext);
                            
                            if (_validate(s_manager_email)) //
                            {
                                try //
                                {
                                    var s_sender = 442;
                                    var s_recepient = s_manager_email;
                                    var s_subject = 'Time sheets not submitted in last week !!!';
                                    
                                    s_recepient = 'vikrant@aashnacloudtech.com';
                                    
                                    nlapiSendEmail(s_sender, s_recepient, s_subject, htmltext);
                                    nlapiLogExecution('DEBUG', 'SCH_PM_Not_Submitted_Notification', 'Email Sent To : ' +
                                    s_manager_email);
                                    
                                    // Remove return after testing
                                    return;
                                    
                                } 
                                catch (ex) //
                                {
                                    nlapiLogExecution('ERROR', 'SCH_PM_Not_Submitted_Notification', 'Email not sent to ' +
                                    s_manager_email +
                                    ', Ex : ' +
                                    ex)
                                    nlapiLogExecution('ERROR', 'SCH_PM_Not_Submitted_Notification', 'Message : ' + ex.message)
                                }
                            }
                        }
                    }
                }
            }
        }
        while (!_validate(search_Pro_managers)) //
        {
        
        }
    } 
    catch (e) //
    {
        nlapiLogExecution('ERROR', 'Exception', 'Ex : ' + e);
        nlapiLogExecution('ERROR', 'Exception', 'Ex Message : ' + e.message);
    }
}

// END SCHEDULED FUNCTION ===============================================

// BEGIN FUNCTION ===================================================
{
    function _validate(obj) //
    {
        if (obj != null && obj != '' && obj != 'undefined')//
        {
            return true;
        }
        else //
        {
            return false;
        }
    }
    
}
// END FUNCTION =====================================================

function validate(value) //
{
    if (value != null && value != '' && value != 'undefined') // 
    {
        return true;
    }
    return false;
}

function getDate() //
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; // January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10)//
    {
        dd = '0' + dd
    }
    if (mm < 10)//
    {
        mm = getMonth(mm);
    }
    
    var today = mm + ' ' + dd + ',' + yyyy;
    nlapiLogExecution('DEBUG', 'getDate', 'today : ' + today);
    return today;
}

function getMonth(mm) //
{
    if (mm == 1)//
    {
        return 'January';
    }
    if (mm == 2)//
    {
        return 'February';
    }
    if (mm == 3)//
    {
        return 'March';
    }
    if (mm == 4)//
    {
        return 'April';
    }
    if (mm == 5)//
    {
        return 'May';
    }
    if (mm == 6)//
    {
        return 'June';
    }
    if (mm == 7)//
    {
        return 'July';
    }
    if (mm == 8)//
    {
        return 'August';
    }
    if (mm == 9)//
    {
        return 'September';
    }
    if (mm == 10)//
    {
        return 'October';
    }
    if (mm == 11)//
    {
        return 'November';
    }
    if (mm == 12)//
    {
        return 'December';
    }
}
