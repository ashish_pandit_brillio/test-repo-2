/**
 * Screen for the vendor request initiator
 * 
 * Version Date Author Remarks 1.00 14 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			createVendorForm();
		} else {
			submitVendorDetails(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "New Vendor Request");
	}
}

function createVendorForm() {
	try {
		var form = nlapiCreateForm('New Vendor Request');

		form.addFieldGroup('custpage_grp_basic', 'Basic Info');
		form.addFieldGroup('custpage_grp_address', 'Address');
		form.addFieldGroup('custpage_grp_other', 'Other');

		form.addField('custpage_cms_vendor_name', 'text', 'Vendor Name', null,
		        'custpage_grp_basic').setMandatory(true);
		form.addField('custpage_cms_signing_authority', 'text',
		        'Signing Authority', null, 'custpage_grp_basic').setMandatory(
		        true);
		form.addField('custpage_cms_federal_id', 'text', 'Federal Id', null,
		        'custpage_grp_basic').setMandatory(true);
		form.addField('custpage_cms_contact_person', 'text', 'Contact Person',
		        null, 'custpage_grp_basic').setMandatory(true);
		form.addField('custpage_cms_admin_email', 'email',
		        'Email For Administrator', null, 'custpage_grp_basic')
		        .setMandatory(true);
		form.addField('custpage_cms_requirement_email', 'email',
		        'Email For Requirement', null, 'custpage_grp_basic')
		        .setMandatory(true);
		form.addField('custpage_cms_phone_number', 'text', 'Phone Number',
		        null, 'custpage_grp_basic').setMandatory(true);
		form.addField('custpage_cms_fax_number', 'text', 'Fax Number', null,
		        'custpage_grp_basic').setMandatory(true);
		form.addField('custpage_cms_contract_type', 'select', 'Contract Type',
		        'customlist_cms_contract_type', 'custpage_grp_basic')
		        .setMandatory(true);
		form.addField('custpage_cms_addr_line_1', 'text', 'Line 1', null,
		        'custpage_grp_address').setMandatory(true);
		form.addField('custpage_cms_addr_line_2', 'text', 'Line 2', null,
		        'custpage_grp_address').setMandatory(true);
		form.addField('custpage_cms_addr_city', 'text', 'City', null,
		        'custpage_grp_address').setMandatory(true);
		form.addField('custpage_cms_addr_state', 'text', 'State', null,
		        'custpage_grp_address').setMandatory(true);
		form.addField('custpage_cms_addr_zip_code', 'text', 'Zip Code', null,
		        'custpage_grp_address').setMandatory(true);
		form.addField('custpage_cms_client_name', 'select', 'Client Name',
		        'customer', 'custpage_grp_other').setMandatory(true);
		form.addField('custpage_cms_contracts_need_follow_up', 'checkbox',
		        'Does Need Follow Up By Contracts ?', null,
		        'custpage_grp_other').setMandatory(true);
		form
		        .addField('custpage_cms_can_resc_work_california', 'checkbox',
		                'Can Resource Work In California ?', null,
		                'custpage_grp_other').setMandatory(true);

		form.addResetButton('Reset');
		form.addSubmitButton('Submit');

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'createVendorForm', err);
		throw err;
	}
}

function submitVendorDetails(request) {
	try {
		// Create a new vendor record
		var vendorRec = nlapiCreateRecord('customrecord_contract_management');
		vendorRec.setFieldValue('name', request
		        .getParameter('custpage_cms_vendor_name'));
		vendorRec.setFieldValue('custrecord_cms_signing_authority', request
		        .getParameter('custpage_cms_signing_authority'));
		vendorRec.setFieldValue('custrecord_cms_federal_id', request
		        .getParameter('custpage_cms_federal_id'));
		vendorRec.setFieldValue('custrecord_cms_contact_person', request
		        .getParameter('custpage_cms_contact_person'));
		vendorRec.setFieldValue('custrecord_cms_admin_email', request
		        .getParameter('custpage_cms_admin_email'));
		vendorRec.setFieldValue('custrecord_cms_requirement_email', request
		        .getParameter('custpage_cms_requirement_email'));
		vendorRec.setFieldValue('custrecord_cms_phone_number', request
		        .getParameter('custpage_cms_phone_number'));
		vendorRec.setFieldValue('custrecord_cms_fax_number', request
		        .getParameter('custpage_cms_fax_number'));
		vendorRec.setFieldValue('custrecord_cms_contract_type', request
		        .getParameter('custpage_cms_contract_type'));
		vendorRec.setFieldValue('custrecord_cms_addr_line_1', request
		        .getParameter('custpage_cms_addr_line_1'));
		vendorRec.setFieldValue('custrecord_cms_addr_line_2', request
		        .getParameter('custpage_cms_addr_line_2'));
		vendorRec.setFieldValue('custrecord_cms_addr_city', request
		        .getParameter('custpage_cms_addr_city'));
		vendorRec.setFieldValue('custrecord_cms_addr_state', request
		        .getParameter('custpage_cms_addr_state'));
		vendorRec.setFieldValue('custrecord_cms_addr_zip_code', request
		        .getParameter('custpage_cms_addr_zip_code'));
		vendorRec.setFieldValue('custrecord_cms_client_name', request
		        .getParameter('custpage_cms_client_name'));
		vendorRec.setFieldValue('custrecord_cms_contracts_need_follow_up',
		        request.getParameter('custpage_cms_contracts_need_follow_up'));
		vendorRec.setFieldValue('custrecord_cms_can_resc_work_california',
		        request.getParameter('custpage_cms_can_resc_work_california'));
		var vendorId = nlapiSubmitRecord(vendorRec);

		// send a notification mail to the contract admin

		// create a response form
		var form = nlapiCreateForm("New Vendor Request");
		form.addField("custpage_1", "inlinehtml", "").setDefaultValue(
		        "Your request has been submitted");
		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitVendorDetails', err);
		throw err;
	}
}
