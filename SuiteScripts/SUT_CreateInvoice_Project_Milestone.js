/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Jul 2017     Jayesh Dinde
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function su_invoice_creation_milestoneprj(request, response)
{
	try
	{
		if(request.getMethod() == 'GET')
		{   
			var i_cust_id = request.getParameter('i_cust_id');
			if(!i_cust_id)
			{
				return true;
			}
			var i_proj_id = request.getParameter('i_proj_id');
			if(!i_proj_id)
			{
				return true;
			}
			var i_subsidiary_id = request.getParameter('i_subsidiary_id');
			if(!i_subsidiary_id)
			{
				return true;
			}
			var i_milestone_rcrd_id= request.getParameter('i_milestone_rcrd_id');
			if(!i_milestone_rcrd_id)
			{
				return true;
			}
			var i_proj_billing_schedule_id= request.getParameter('i_proj_billing_schedule_id');
			if(!i_proj_billing_schedule_id)
			{
				return true;
			}
			
			var o_billing_schdl_rcrd = nlapiLoadRecord('billingschedule',i_proj_billing_schedule_id);
			if(o_billing_schdl_rcrd)
			{
				var s_amount_after_split = '';
				var s_project_task_description = '';
				var i_billing_schedule_line_count = o_billing_schdl_rcrd.getLineItemCount('milestone');
				for (var i_line_index = 1; i_line_index <= i_billing_schedule_line_count; i_line_index++)
				{
					var i_billing_project_task = o_billing_schdl_rcrd.getLineItemValue('milestone', 'projecttask', i_line_index);
					if (i_billing_project_task == i_milestone_rcrd_id)
					{
						var s_amount_schdule = o_billing_schdl_rcrd.getLineItemValue('milestone', 'comments', i_line_index);
						
						var i_decimal_point_index = s_amount_schdule.indexOf('.');
						if(parseInt(i_decimal_point_index) > 0)
						{
							for (var i_chr_len = 0; i_chr_len < i_decimal_point_index; i_chr_len++)
							{
								if (!isNaN(s_amount_schdule[i_chr_len]))
								{
									s_amount_after_split += s_amount_schdule[i_chr_len];
								}
							}
							
							var s_str_after_decimal = s_amount_schdule.substr(i_decimal_point_index,s_amount_schdule.length);
							s_amount_after_split += s_str_after_decimal;
						}
						else
						{
							for (var i_chr_len = 0; i_chr_len < s_amount_schdule.length; i_chr_len++)
							{
								if (!isNaN(s_amount_schdule[i_chr_len]))
								{
									s_amount_after_split += s_amount_schdule[i_chr_len];
								}
							}
						}
						
						s_amount_after_split = parseFloat(s_amount_after_split);
						nlapiLogExecution('Debug', 's_amount_after_split', s_amount_after_split);
						
						s_project_task_description = o_billing_schdl_rcrd.getLineItemText('milestone', 'projecttask', i_line_index);
						nlapiLogExecution('Debug', 's_project_task_description ', s_project_task_description);
					}
				}
			}
			
			var s_milestone_end_date = nlapiLookupField('projecttask',i_milestone_rcrd_id,'enddate');
			
			var o_invoice_rcrd = nlapiCreateRecord('invoice'); 

			o_invoice_rcrd.setFieldValue('entity', i_cust_id);
			o_invoice_rcrd.setFieldValue('subsidiary', i_subsidiary_id);
			o_invoice_rcrd.setFieldValue('job', i_proj_id);
			o_invoice_rcrd.setFieldValue('location', 9);
			
			o_invoice_rcrd.setFieldValue('custbody_billto', s_milestone_end_date);
			o_invoice_rcrd.setFieldValue('custbody_layout_type', 3);

			o_invoice_rcrd.selectNewLineItem('item');
			o_invoice_rcrd.setCurrentLineItemValue('item', 'item', 2221);
			o_invoice_rcrd.setCurrentLineItemValue('item', 'amount', parseFloat(s_amount_after_split).toFixed(2));
			o_invoice_rcrd.setCurrentLineItemValue('item', 'description', s_project_task_description);
			o_invoice_rcrd.commitLineItem('item');
			
			var filters_search_invoice = new Array();
			filters_search_invoice[0] = new nlobjSearchFilter('internalid','job','is',i_proj_id);
			filters_search_invoice[1] = new nlobjSearchFilter('type',null,'is','CustInvc');
			
			var columns_search_invoice = new Array();
			columns_search_invoice[0] = new nlobjSearchColumn('datecreated').setSort(true);
			columns_search_invoice[1] = new nlobjSearchColumn('tranid');
			columns_search_invoice[2] = new nlobjSearchColumn('custbody_billto');
			
			var a_search_invoice = nlapiSearchRecord('transaction', null, filters_search_invoice, columns_search_invoice);
			if (a_search_invoice)
			{
				var s_prev_invoice_to_date = a_search_invoice[0].getValue('custbody_billto');
				s_prev_invoice_to_date = nlapiAddDays(nlapiStringToDate(s_prev_invoice_to_date),1);
				o_invoice_rcrd.setFieldValue('custbody_billfrom', nlapiDateToString(s_prev_invoice_to_date));
			}
			else
			{
				var s_milestone_strt_date = nlapiLookupField('projecttask',i_milestone_rcrd_id,'startdate');
				var s_prev_invoice_to_date = s_milestone_strt_date;
				s_prev_invoice_to_date = nlapiStringToDate(s_prev_invoice_to_date);
				o_invoice_rcrd.setFieldValue('custbody_billfrom', s_milestone_strt_date);
			}
			
			var s_billing_from_date = s_prev_invoice_to_date;
			
			var filters_search_sow = new Array();
			filters_search_sow[0] = new nlobjSearchFilter('internalid','job','is',i_proj_id);
			filters_search_sow[1] = new nlobjSearchFilter('type',null,'is','SalesOrd');
			
			var columns_search_sow = new Array();
			columns_search_sow[0] = new nlobjSearchColumn('datecreated').setSort(true);
			columns_search_sow[1] = new nlobjSearchColumn('tranid');
			columns_search_sow[2] = new nlobjSearchColumn('custbody_projectstartdate');
			columns_search_sow[3] = new nlobjSearchColumn('custbody_projectenddate');
			
			var a_search_sow = nlapiSearchRecord('transaction', null, filters_search_sow, columns_search_sow);
			if(a_search_sow)
			{
				var s_sow_proj_strt_date = a_search_sow[0].getValue('custbody_projectstartdate');
				s_sow_proj_strt_date = nlapiStringToDate(s_sow_proj_strt_date);
				if(s_sow_proj_strt_date <= s_billing_from_date)
				{
					//var s_sow_tranid = a_search_sow[0].getValue('tranid');
					var s_sow_po_no = nlapiLookupField('salesorder',a_search_sow[0].getId(),'otherrefnum');
					o_invoice_rcrd.setFieldValue('otherrefnum',s_sow_po_no);
				}
				else
				{
					for(var i_sow_index=1; i_sow_index<a_search_sow.length; i_sow_index++)
					{
						var s_sow_proj_strt_date = a_search_sow[i_sow_index].getValue('custbody_projectstartdate');
						s_sow_proj_strt_date = nlapiStringToDate(s_sow_proj_strt_date);
						if(s_sow_proj_strt_date <= s_billing_from_date)
						{
							//var s_sow_tranid = a_search_sow[i_sow_index].getValue('tranid');
							var s_sow_po_no = nlapiLookupField('salesorder',a_search_sow[i_sow_index].getId(),'otherrefnum');
							o_invoice_rcrd.setFieldValue('otherrefnum',s_sow_po_no);
							break;
						}
					}
				}
			}
			
			nlapiLogExecution('audit','invoice manadatory fields setuped');
			var i_invoice_created_id = nlapiSubmitRecord(o_invoice_rcrd,true,true);
			if(i_invoice_created_id)
			{
				nlapiLogExecution('audit','i_invoice_created_id:- '+i_invoice_created_id);
				
				var a_fld_id = new Array();
				a_fld_id[0] = 'custevent_auto_invoice_ref_link';
				a_fld_id[1] = 'status';
				
				var a_fld_val = new Array();
				a_fld_val[0] = i_invoice_created_id;
				a_fld_val[1] = 'COMPLETE';
				
				nlapiSubmitField('projecttask',i_milestone_rcrd_id,a_fld_id,a_fld_val);
				
				nlapiSetRedirectURL('RECORD', 'invoice', i_invoice_created_id, true);
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
	}
}