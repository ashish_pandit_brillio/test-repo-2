// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:SUT_AIT_Invoice_pay_with_tds.js
	Author:		Nikhil jain
	Company:	Aashna Cloudtech Pvt. Ltd.
	Date:		26 Nov 2015
	Description:Deduct the TDS from invoice at the time of payment 


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --



Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SUITELET
		- suiteletFunction(request, response)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{
	//  Initialize any Global Variables, in particular, debugging variables...




}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN SUITELET ==================================================

function suiteletFunction_mutiple_TDSinvoicePay(request, response){

	/*  Suitelet:
	 - EXPLAIN THE PURPOSE OF THIS FUNCTION
	 -
	 FIELDS USED:
	 --Field Name--				--ID--
	 */
	//  LOCAL VARIABLES
	
	
	//  SUITELET CODE BODY
	if (request.getMethod() == 'GET') {
		var tdsRoundMethod = '';
		var b_Mand_location;
		var b_Mand_Class;
		var b_Mand_department;
		var a_subisidiary = new Array();
		var i_PayTaxcode;
		var i_vatCode;
		var i_taxcode;
		
		// ===== GET THE VALUES OF GLOBAL SUBSIDIARY PARAMETERS ====
		var i_AitGlobalRecId = SearchGlobalParameter();
		nlapiLogExecution('DEBUG', 'Bill ', "i_AitGlobalRecId" + i_AitGlobalRecId);
		
		//==== CODE TO CHECK THE INDIAN SUBSIDIARY ====
		if (i_AitGlobalRecId != 0) {
			var o_AitGloRec = nlapiLoadRecord('customrecord_indian_tax_glo_parameter', i_AitGlobalRecId);
			
			a_subisidiary = o_AitGloRec.getFieldValues('custrecord_ait_subsidiary');
			//nlapiLogExecution('DEBUG', 'Bill ', "a_subisidiary->" + a_subisidiary);
			
			i_vatCode = o_AitGloRec.getFieldValue('custrecord_ait_vat_code');
			//nlapiLogExecution('DEBUG', 'Bill ', "i_vatCode->" + i_vatCode);
			
			i_taxcode = o_AitGloRec.getFieldValue('custrecord_ait_taxcode');
			//nlapiLogExecution('DEBUG', 'Bill ', "i_taxcode->" + i_taxcode);
			
			i_PayTaxcode = o_AitGloRec.getFieldValue('custrecord_pay_servicetax');
			//nlapiLogExecution('DEBUG', 'Bill ', "i_PayTaxcode->" + i_PayTaxcode);
			
			b_Mand_location = o_AitGloRec.getFieldValue('custrecord_ait_location');
			//nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_location->" + b_Mand_location);
			
			b_Mand_Class = o_AitGloRec.getFieldValue('custrecord_ait_class');
			//nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_Class->" + b_Mand_Class);
			
			b_Mand_department = o_AitGloRec.getFieldValue('custrecord_ait_department');
			//nlapiLogExecution('DEBUG', 'Bill ', "b_Mand_department->" + b_Mand_department);
			
			tdsRoundMethod = o_AitGloRec.getFieldValue('custrecord_ait_tds_roundoff');
		//nlapiLogExecution('DEBUG','Bill ', "tdsRoundMethod->"+tdsRoundMethod);
		}
		var form = nlapiCreateForm('Invoice TDS Deduction Form');
		
		form.setScript('customscript_ait_invpay_with_tds');
		
		var customer = request.getParameter('customer');
		var fromDate = request.getParameter('fromDate');
		var toDate = request.getParameter('toDate');
		var department = request.getParameter('department');
		var s_class = request.getParameter('s_class');
		var location = request.getParameter('location');
		var tdstype = request.getParameter('tdsrecivable');
		var tdspercent = request.getParameter('tdspercent');
		
		var vatcodeObj = form.addField('vatcode', 'text', 'Vatcode');
		vatcodeObj.setDisplayType('hidden')
		vatcodeObj.setDefaultValue(i_vatCode);
		
		
		var custFieldObj = form.addField('custpage_customer', 'select', 'Customer');
		custFieldObj.addSelectOption('', '--All--')
		populateCustomerName(request, response, custFieldObj, a_subisidiary)
		
		if (customer != null && customer != '' && customer != undefined) {
			custFieldObj.setDefaultValue(customer);
		}
		
		var from_dateObj = form.addField('custpage_fromdate', 'date', 'From Date');
		
		if (fromDate != null && fromDate != '' && fromDate != undefined) {
			from_dateObj.setDefaultValue(fromDate);
		}
		
		var to_dateObj = form.addField('custpage_todate', 'date', 'To Date');
		
		if (toDate != null && toDate != '' && toDate != undefined) {
			to_dateObj.setDefaultValue(toDate);
		}
		
		var total_amountField = form.addField('custpage_total_amt', 'currency', 'Total Base Amount');
		total_amountField.setDisplayType('inline')
		total_amountField.setDefaultValue(0)
		
		var total_TDSamountField = form.addField('custpage_total_tds_amt', 'currency', 'Total TDS Amount');
		total_TDSamountField.setDisplayType('inline')
		total_TDSamountField.setDefaultValue(0)
		
		var TDS_Recivable = form.addField('custpage_tdsrecivable', 'select', 'TDS Recivable', 'customrecord_tdsmaster');
		TDS_Recivable.setMandatory(true)
		if (tdstype != null && tdstype != '' && tdstype != undefined) {
			TDS_Recivable.setDefaultValue(tdstype);
		}
		
		var tdsround = form.addField('custpage_tdsround', 'text', 'TDS Round');
		tdsround.setDisplayType('hidden')
		tdsround.setDefaultValue(tdsRoundMethod);
		
		var TDS_percent = form.addField('custpage_tdspercent', 'percent', 'TDS %');
		TDS_percent.setDisplayType('inline');
		if (tdspercent != null && tdspercent != '' && tdspercent != undefined) {
			TDS_percent.setDefaultValue(tdspercent);
		}
		
		var locationObj = form.addField('location', 'select', 'Location', 'Location');
		if (b_Mand_location == 'T') {
			locationObj.setMandatory(true)
		}
		if (logvalidatevalue(location)) {
			locationObj.setDefaultValue(location);
		}
		var classObj = form.addField('class', 'select', 'Class', 'classification');
		if (b_Mand_Class == 'T') {
			classObj.setMandatory(true)
		}
		if (logvalidatevalue(s_class)) {
			classObj.setDefaultValue(s_class);
		}
		var departmentObj = form.addField('department', 'select', 'Department', 'Department');
		if (b_Mand_department == 'T') {
			departmentObj.setMandatory(true)
		}
		if (logvalidatevalue(department)) {
			departmentObj.setDefaultValue(department);
		}
		
		form.addSubmitButton('Deduct TDS');
		
		form.addButton('custpage_submit', 'Search', 'customerinvoicesearchcritera()');
		
		form.addButton('custpage_refresh', 'Refresh', 'customerInvoiceCriteriaRefersh()')
		//Code to Add The List
		//var attResult = searchopenBillToPay(request, response);
		var sublist1 = form.addSubList('invoice_line_items', 'list', 'Invoices', 'tab1');
		
		sublist1.addButton('custpage_markall', 'Mark All', 'bulkpaymarkall()');
		sublist1.addButton('custpage_unmarkall', 'UnMark All', 'bulkpayunmarkall()');
		
		searchopenInvoicesToPay(sublist1, request, response, a_subisidiary, customer, fromDate, toDate)
		
		response.writePage(form);
		
	}
	else 
		if (request.getMethod() == 'POST') {
			var context = nlapiGetContext();
			var usage = context.getRemainingUsage();
			nlapiLogExecution('DEBUG', 'Invoice payment with tds', 'Before process ---> ' + usage)
			processLineItem(request)
			var usage = context.getRemainingUsage();
			nlapiLogExecution('DEBUG', 'Invoice payment with tds', 'after process ---> ' + usage)
			nlapiSetRedirectURL('SUITELET', 'customscript_ait_invoice_pay_with_tds', '1', false);
			
		}
	
}
// END SUITELET ====================================================




// BEGIN OBJECT CALLED/INVOKING FUNCTION ===================================================
//Begin :function for  Populate Customer Name=================
function populateCustomerName(request, response, custFieldObj, a_subisidiary)
{
	var filchk1 = new Array();
	var columnsCheck1 = new Array();
	
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
	//nlapiLogExecution('DEBUG','Test ', 'i_subcontext --> '+ i_subcontext )
		
	if (i_subcontext != false) {

		if (a_subisidiary != null && a_subisidiary != undefined && a_subisidiary != '' && a_subisidiary != 'undefined') {
			filchk1.push(new nlobjSearchFilter('subsidiary', null, 'anyOf', a_subisidiary));
		}
	}
	columnsCheck1.push(new nlobjSearchColumn('entityid'));
	columnsCheck1.push(new nlobjSearchColumn('internalid'));
	
	var cust_search = nlapiCreateSearch('customer')
	
	cust_search.addFilters(filchk1);
	
	cust_search.addColumns(columnsCheck1);
	
	var resultset = cust_search.runSearch();
	
	var searchid = 0;
	//loop while the results returned is 1000
	do {
		var searchResultsCheck1 = resultset.getResults(searchid, searchid + 1000);
		
		if (searchResultsCheck1 != null && searchResultsCheck1 != '' && searchResultsCheck1 != undefined) {
			for (var i in searchResultsCheck1) {
				custFieldObj.addSelectOption(searchResultsCheck1[i].getValue('internalid'), searchResultsCheck1[i].getValue('entityid'), false);
				searchid++;
			}
		}
	}
	while (searchResultsCheck1.length >= 1000);
}
function SearchGlobalParameter()
{
	// == GET TDS GLOBAL PARAMETER ====
	var a_filters = new Array();
	var a_column = new Array();
	var i_globalRecId = 0;
	
    a_column.push(new nlobjSearchColumn('internalid'));
	
	var s_serchResult = nlapiSearchRecord('customrecord_indian_tax_glo_parameter',null,null,a_column)
	
	if(s_serchResult != null && s_serchResult != undefined && s_serchResult != '')
	{
		for (var i=0; i<s_serchResult.length && s_serchResult != null; i++) 	
		{
			i_globalRecId = s_serchResult[0].getValue('internalid');
			//nlapiLogExecution('DEBUG','Bill ', "i_globalRecId"+i_globalRecId);
		
		}
		
	}
	return i_globalRecId;
}
function searchopenInvoicesToPay(sublist1, request, response,a_subisidiary,customer,fromDate,toDate)
{
	var internal_id = sublist1.addField('internalid', 'text', 'Internal ID');
	internal_id.setDisplayType('hidden');
	
	sublist1.addField('invoiceapply', 'checkbox', 'Select');
	
	sublist1.addField('custline_srno', 'text', 'Sr. No.');
	
	var date = sublist1.addField('date', 'date', 'Date');
	
	sublist1.addField('custline_tranid', 'text', 'Invoice #');
	
	var vendor_obj = sublist1.addField('line_customer', 'select', 'Customer', 'Customer');
	vendor_obj.setDisplayType('inline');
	
	var status_obj = sublist1.addField('line_status', 'text', 'Status');
	
	var currency_obj = sublist1.addField('line_currency', 'select', 'Currency', 'currency');
	currency_obj.setDisplayType('inline');
	
	sublist1.addField('line_exchangerate', 'text', 'Exchange Rate');
	var context = nlapiGetContext();
	var i_subcontext = context.getFeature('SUBSIDIARIES');
	//nlapiLogExecution('DEBUG','Test ', 'i_subcontext --> '+ i_subcontext )
		
	if (i_subcontext != false) {
		var subsidairy_obj = sublist1.addField('line_subsidiary', 'select', 'Subsidiary', 'subsidiary');
		subsidairy_obj.setDisplayType('inline');
	}
	var debitaccObj = sublist1.addField('linecreditaccount', 'Select', 'Credit Account', 'account');
	debitaccObj.setDisplayType('inline');
	
	//sublist1.addField('invoiceamt', 'currency', 'Amount');

	sublist1.addField('amount', 'currency', 'Amount');
	
	sublist1.addField('taxamount', 'currency', 'Tax');
	
	sublist1.addField('baseamount', 'currency', 'Base Amount');
	
	var paymentObj = sublist1.addField('paymentbaseamount', 'currency', 'Payment Base Amount');
	paymentObj.setDisplayType('entry')
	
	var tdspaymentObj = sublist1.addField('tdsamount', 'currency', 'TDS Amount');
	tdspaymentObj.setDisplayType('entry')
	
	var filters = new Array();
	
	if (fromDate != null && fromDate != '' && fromDate != undefined) {
		filters.push(new nlobjSearchFilter('trandate', null, 'onorafter', fromDate))
	}
	if (toDate != null && toDate != '' && toDate != undefined) {
		filters.push(new nlobjSearchFilter('trandate', null, 'onorbefore', toDate))
	}

	if (i_subcontext != false) {
	
		if (a_subisidiary != null && a_subisidiary != '' && a_subisidiary != undefined) {
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyOf', a_subisidiary))
		}
	}
	if (customer != null && customer != '' && customer != undefined) {
		filters.push(new nlobjSearchFilter('entity', null, 'is', customer))
	}
	
	var column = new Array();
	
	column[0] = new nlobjSearchColumn('trandate')
	column[1] = new nlobjSearchColumn('entity')
	column[2] = new nlobjSearchColumn('status')
	column[3] = new nlobjSearchColumn('amount')
	column[4] = new nlobjSearchColumn('currency')
	column[5] = new nlobjSearchColumn('internalid')
	column[6] = new nlobjSearchColumn('amountremaining')
	column[7] = new nlobjSearchColumn('internalid')
	column[8] = new nlobjSearchColumn('amountpaid')
	column[9] = new nlobjSearchColumn('tranid')
	column[10] = new nlobjSearchColumn('account')
	column[11] = new nlobjSearchColumn('exchangerate')
	column[12] = new nlobjSearchColumn('fxamountpaid')
	column[13] = new nlobjSearchColumn('fxamount')
	column[14] = new nlobjSearchColumn('fxamountremaining')
	column[15] = new nlobjSearchColumn('formulacurrency').setFormula('{netamountnotax}/{exchangerate}')
	column[16] = new nlobjSearchColumn('formulacurrency').setFormula('{fxamount}-{netamountnotax}/{exchangerate}')
	if (i_subcontext != false) {
		column[17] = new nlobjSearchColumn('subsidiarynohierarchy')
	}
	var s_searchresult = nlapiSearchRecord('invoice', 'customsearch_open_invoice_pay_tds', filters, column)
	
	var k = 0;
	
	if (s_searchresult != null && s_searchresult != '' && s_searchresult != undefined) 
	{
		nlapiLogExecution('DEBUG', 'ven bill details ', 's_searchresult--->' + s_searchresult)
		
		for (var i = 0; i < s_searchresult.length; i++) 
		{
			k = parseInt(k) + parseInt(1);
			
			var internalid = s_searchresult[i].getValue('internalid');
			
			var date = s_searchresult[i].getValue('trandate');
			
			var custname = s_searchresult[i].getValue('entity');
			if (i_subcontext != false) {
			
				var subsidiary_text = s_searchresult[i].getText('subsidiarynohierarchy');
			}
			var transactionID = s_searchresult[i].getValue('tranid');
			
			var accountpayable = s_searchresult[i].getValue('account');
			
			var exchangerate = s_searchresult[i].getValue('exchangerate');
			
			var amount = s_searchresult[i].getValue('fxamount');
			
			var taxamount = s_searchresult[i].getValue(column[16]);
			
			var baseamount = s_searchresult[i].getValue(column[15]);
		
		if (i_subcontext != false) {
		
			var subsidiary = s_searchresult[i].getValue('subsidiarynohierarchy');
		}
			var status = s_searchresult[i].getValue('status');
			
			var currency = s_searchresult[i].getValue('currency');
			
			sublist1.setLineItemValue('custline_srno', k, k.toString());
			sublist1.setLineItemValue('internalid', k, internalid);
			sublist1.setLineItemValue('line_customer', k, custname);
			if (i_subcontext != false) {
				sublist1.setLineItemValue('line_subsidiary', k, subsidiary);
			}
			sublist1.setLineItemValue('amount', k, amount);
			
			if(taxamount != '' && taxamount != null && taxamount != undefined) 
			{
				taxamount = parseFloat(taxamount)
				taxamount = taxamount.toFixed(2);
				taxamount = nlapiFormatCurrency(taxamount)
				sublist1.setLineItemValue('taxamount', k,taxamount);
			}
			else
			{
				sublist1.setLineItemValue('taxamount', k, 0);
			}
			if(baseamount != null && baseamount != '' && baseamount != undefined) 
			{
				baseamount = parseFloat(baseamount)
				baseamount = baseamount.toFixed(2);
				baseamount = nlapiFormatCurrency(baseamount)
				sublist1.setLineItemValue('baseamount', k,baseamount);
				//sublist1.setLineItemValue('paymentbaseamount', k, baseamount);
			}
			else
			{
				sublist1.setLineItemValue('baseamount', k, amount);
			}
			sublist1.setLineItemValue('line_status', k, status);
			sublist1.setLineItemValue('date', k, date);
			sublist1.setLineItemValue('line_currency', k, currency);
			sublist1.setLineItemValue('custline_tranid', k, transactionID);
			sublist1.setLineItemValue('linecreditaccount', k, accountpayable);
			sublist1.setLineItemValue('line_exchangerate', k, exchangerate);
		}
	}
}
//END :function for  Populate Bank Account List
function processLineItem(request){
	var tdsType = request.getParameter('custpage_tdsrecivable');
	
	var i_class = request.getParameter('class');
	
	var i_location = request.getParameter('location');
	
	var i_department = request.getParameter('department');
	
	var i_vatCode = request.getParameter('vatcode');
	
	var o_TdsMasterRecord = nlapiLoadRecord('customrecord_tdsmaster', tdsType)
	
	var i_item = o_TdsMasterRecord.getFieldValue('custrecord_tdsrecievable_item');
	//nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_item->" + i_item);
	
	var i_ApplyTdsRate = o_TdsMasterRecord.getFieldValue('custrecord_netper');
	//nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_ApplyTdsRate->" + i_ApplyTdsRate);
	
	var i_line_count = request.getLineItemCount('invoice_line_items')
	//nlapiLogExecution('DEBUG', 'process line item', 'line count--->' + i_line_count)
	
	for (var q = 1; q <= i_line_count; q++) 
	{
		var apply = request.getLineItemValue('invoice_line_items', 'invoiceapply', q)
		//nlapiLogExecution('DEBUG', 'invoices', 'apply--->' + apply)
		
		if (apply == 'T') 
		{
			var i_invoiceId = request.getLineItemValue('invoice_line_items', 'internalid', q)
			//nlapiLogExecution('DEBUG', 'Invoice', 'Invoice ID--->' + i_invoiceId)
			
			var i_invoicenumber = request.getLineItemValue('invoice_line_items', 'custline_tranid', q)
			//nlapiLogExecution('DEBUG', 'Invoice', 'i_invoicenumber--->' + i_invoicenumber)
			
			var i_tdsamount = request.getLineItemValue('invoice_line_items', 'tdsamount', q)
			//nlapiLogExecution('DEBUG', 'Invoice ', 'i_tdsamount--->' + i_tdsamount)
			
			var amounttobepaid = request.getLineItemValue('invoice_line_items', 'amounttobepaid', q)
			//nlapiLogExecution('DEBUG', 'Invoice ', 'applied_amount--->' + applied_amount)
			
			var applied_amount = (parseFloat(amounttobepaid) - parseFloat(i_tdsamount))
			
			var o_creditMemoRec = nlapiTransformRecord('invoice', i_invoiceId, 'creditmemo', {
				recordmode: 'dynamic'
			});
			
			if (i_department != null && i_department != '' && i_department != undefined) {
				o_creditMemoRec.setFieldValue('department', i_department)
			}
			if (logvalidatevalue(i_location)) {
				o_creditMemoRec.setFieldValue('location', i_location)
			}
			if (logvalidatevalue(i_class)) {
				o_creditMemoRec.setFieldValue('class', i_class)
			}
			
			var i_LineCount = o_creditMemoRec.getLineItemCount('item')
			//nlapiLogExecution('DEBUG', 'aftr remove ', "Item i_LineCount->" + i_LineCount);
			
			// === CODE TO REMOVE THE LINE ITEMS =====
			for (var k = i_LineCount; k >= 1; k--) {
				{
					nlapiLogExecution('DEBUG', 'Update credit Memo ', 'IN IF Flag REMOVE-->')
					o_creditMemoRec.removeLineItem('item', k);
				}
			}
			// ==== CODE TO APPLY RESPECTIVE INVOICE ON THE CREDIT MEMO ==
			var i_ApplyLineCount = o_creditMemoRec.getLineItemCount('apply')
			//nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_ApplyLineCount->" + i_ApplyLineCount);
			
			for (var j = 1; j <= i_ApplyLineCount; j++) {
				var appliedinvoice = o_creditMemoRec.getLineItemValue('apply', 'refnum', j)
				
				if (i_invoicenumber == appliedinvoice) {
					o_creditMemoRec.setLineItemValue('apply', 'apply', j, 'T')
					o_creditMemoRec.setLineItemValue('apply', 'amount', j, i_tdsamount)
				}
				else {
					o_creditMemoRec.setLineItemValue('apply', 'apply', j, 'F')
				}
			}
			// ==== CODE TO SET THE LINE ITEM VALUES ==
			o_creditMemoRec.selectNewLineItem('item')
			o_creditMemoRec.setCurrentLineItemValue('item', 'item', i_item)
			o_creditMemoRec.setCurrentLineItemValue('item', 'quantity', 1)
			o_creditMemoRec.setCurrentLineItemValue('item', 'price', parseFloat(-1))
			o_creditMemoRec.setCurrentLineItemValue('item', 'rate', i_tdsamount)
			o_creditMemoRec.setCurrentLineItemValue('item', 'amount', i_tdsamount)
			o_creditMemoRec.setCurrentLineItemValue('item', 'taxcode', i_vatCode)
			if (logvalidatevalue(i_department)) {
				o_creditMemoRec.setCurrentLineItemValue('item', 'department', i_department)
			}
			if (logvalidatevalue(i_location)) {
				o_creditMemoRec.setCurrentLineItemValue('item', 'location', i_location)
			}
			if (logvalidatevalue(i_class)) {
				o_creditMemoRec.setCurrentLineItemValue('item', 'class', i_class)
			}
			o_creditMemoRec.commitLineItem('item')
			// ==== CODE TO APPLY RESPECTIVE INVOICE ON THE CREDIT MEMO ==
			
			var i_createdCreditMemo = nlapiSubmitRecord(o_creditMemoRec, true, true)
			nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_createdCreditMemo->" + i_createdCreditMemo);
			
			if (i_createdCreditMemo != null && i_createdCreditMemo != undefined && i_createdCreditMemo != '') {
				var fields = new Array();
				var values = new Array();
				
				fields[0] = 'custbody_apply_tds';
				fields[1] = 'custbody_tds_rate';
				fields[2] = 'custbody_tds_creditmemoreference';
				
				values[0] = 'T';
				values[1] = tdsType;
				values[2] = i_createdCreditMemo;
				
				var i_UpdatedInvoicerec = nlapiSubmitField('invoice', i_invoiceId, fields, values, false);
				nlapiLogExecution('DEBUG', 'afterSubmitRecord ', "i_UpdatedInvoicerec->" + i_UpdatedInvoicerec);
			}
		}
	}
}
function logvalidatevalue(value)
{
	if (value != '' && value != null && value != 'undefined') {
		return true;
	}
	else {
		return false;
	}
	
	
	
}

// END OBJECT CALLED/INVOKING FUNCTION =====================================================
