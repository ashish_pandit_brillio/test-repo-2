function main() {

	var csvFile = 20391;
	var file = nlapiLoadFile(csvFile);
	var value = file.getValue();
	var rows = value.split('\r\n');
	var numberOfRecords = rows.length - 1;
	nlapiLogExecution('debug', 'start');
	var currentContext = nlapiGetContext();

	for (var i = 1; i < rows.length; i++) {

		var columns = rows[i].split(",");
		var id = columns[0];
		try {
			yieldScript(currentContext);
			var dateModified = formatDate(columns[1]);
			var setBy = columns[2];
			var resc = nlapiLoadRecord('resourceallocation', id);
			resc.setFieldValue('custevent_ra_last_modified_date', dateModified);
			resc.setFieldText('custevent_ra_last_modified_by', setBy);
			nlapiSubmitRecord(resc, false, true);
			nlapiLogExecution('debug', 'done:', id);

			if (i % 20 == 0) {
				currentContext.setPercentComplete((i / numberOfRecords) * 100);
			}
		}
		catch (err) {
			nlapiLogExecution('error', 'failed:' + id, err);
		}
	}
	nlapiLogExecution('debug', 'end');
}

function formatDate(date1) {

	var dateSplit = date1.split(':');
	var dateSplit1 = dateSplit[0].split(' ');
	var H = parseInt(dateSplit1[1]);
	var h = H % 12;
	var tz = H < 12 ? 'AM' : 'PM';

	var newDate = dateSplit1[0] + " " + h + ":" + dateSplit[1] + ":00 " + tz;
	return newDate;
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == constants.K_YIELD_FAILURE) {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : ' + state.reason
					+ ' / Size : ' + state.size);
			return false;
		}
		else if (state.status == constants.K_YIELD_RESUME) {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}