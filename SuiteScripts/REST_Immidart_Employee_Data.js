/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Mar 2020     shamanth.k
 *
 */




/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	
    var adminEmployeeList  = getAdminEmployee(); //Added by Sitaram
    
   //return adminEmployeeList
	try{
		var response = new Response();
		//nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
		var current_date = nlapiDateToString(new Date());
		var currentDate = sysDate();
		var currentTime = timestamp(); // returns the time stamp in HH:MM:SS
	    var currentDateAndTime = currentDate + ' ' + currentTime;
		
		//Log for current date
		nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date);
	
		
		var receivedDate = dataIn.LastInstaceDate;
		nlapiLogExecution('DEBUG', 'Current Date', 'receivedDate...' + receivedDate);
		//var receivedDate = '3/16/2020 4:00 am';
		/*
		var filters = [];
		filters.push(new nlobjSearchFilter('lastmodifieddate',null,'within',receivedDate,currentDateAndTime));
		filters.push(new nlobjSearchFilter('custentity_employee_inactive',null,'anyof','F'));
		filters.push(new nlobjSearchFilter('custentity_implementationteam',null,'anyof','F'));
		filters.push(new nlobjSearchFilter('role','noneof','3'));
		*/
        var filters =	[
	                   ["lastmodifieddate",'within',receivedDate,currentDateAndTime], 
					   "AND",
	                   ["custentity_implementationteam","is","F"], 
					   "AND",
					    ["custentity_employeetype","anyof","1"]
					]
		
		
		var cols = [];
		cols.push(new nlobjSearchColumn("custentity_fusion_empid",null,"GROUP"));
		cols.push(new nlobjSearchColumn("entityid",null,"GROUP").setSort(false)); 
		cols.push(new nlobjSearchColumn("firstname",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("middlename",null,"GROUP")); 
		cols.push(new nlobjSearchColumn("lastname",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("custentity_actual_hire_date",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("phone",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("email",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("custentity_personalemailid",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("gender",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("title",null,"GROUP")); 
		cols.push(new nlobjSearchColumn("department",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("custentity_reportingmanager",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("custentity_fusion_empid","custentity_reportingmanager","GROUP")); 
		cols.push(new nlobjSearchColumn("email","custentity_reportingmanager","GROUP")); 
		cols.push(new nlobjSearchColumn("custrecord_parent_practice","department","GROUP"));  
		cols.push(new nlobjSearchColumn("departmentnohierarchy",null,"GROUP")); 
		cols.push(new nlobjSearchColumn("namenohierarchy","department","GROUP"));  
		cols.push(new nlobjSearchColumn("birthdate",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("employeestatus",null,"GROUP")); 
		cols.push(new nlobjSearchColumn("custentity_list_brillio_location_e",null,"GROUP"));  
		cols.push(new nlobjSearchColumn("custentity_employeetype",null,"GROUP")); 
		cols.push(new nlobjSearchColumn("date","systemNotes","MAX")); 
		cols.push(new nlobjSearchColumn("datecreated",null,"MAX"));
		cols.push(new nlobjSearchColumn("custentity_is_sent_to_immidart",null,"GROUP"));
		cols.push(new nlobjSearchColumn("internalid",null,"GROUP"));
      	cols.push(new nlobjSearchColumn("custentity_job_code",null,"GROUP"));
        cols.push(new nlobjSearchColumn("custentity_legal_entity_fusion",null,"GROUP"));
		 cols.push(new nlobjSearchColumn("subsidiary",null,"GROUP"));
		
		
		var searchResults = searchRecord('employee',null,filters,cols); //employee search
		var JSON_New = {};
		var JSON_Update = {};
		var dataRow_New = [];
		var dataRow_Update = [];
		var dataRow = [];
        var immidartFail = [];
		
	if(searchResults){
      
      nlapiLogExecution('DEBUG', 'searchResults ', searchResults.length);
     
      
       var strName="<head>";
		    strName+="<style>";
		    strName+="th{background-color: #3c8dbc; color:white;}";
		    strName+=".BorderTopLeft{border-top:solid;border-left:solid}";
		    strName+=".BorderBottomLeft{border-bottom:solid;border-left:solid}";
		    strName+=".BorderTopRight{border-top:solid;border-right:solid}";
		    strName+=".BorderBottomRight{border-bottom:solid;border-right:solid}";
		    strName+=".BorderTop{border-top:solid;}";
		    strName+=".BorderLeft{border-left:solid}";
		    strName+=".BoderAll{border-bottom:solid;border-right:solid;border-Top:solid;border-left:solid}";
		    strName+=".BorderRight{border-right:solid;}";
		    strName+=".BorderBottom{border-bottom:solid;}";
		    strName+=".BorderTopLeftRight{border-Top:solid;border-Right:solid;border-Left:solid;}"; 
		    strName+="body{font-family:sans-serif;font-size:8px;margin-top:0px;}";
		    strName+="</style>";
		    strName+="<macrolist>";
		    strName+="<macro id='myfooter'>";
		    strName+="<table  style='width: 100%;'><tr>";
		    strName+="<td align='right' ><pagenumber/> of <totalpages/></td>";
		    strName+="</tr></table>";
		    strName+="</macro>";
		    strName+="<macro id='myHead'>";
		    strName+="</macro>";
		    strName+="</macrolist>";
		    strName+="</head>";
		    
		    
		    strName += "<table width='1430px' border='1'>";
		    strName += "<tr>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Name</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>FirstName</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>MiddleName</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>LastName Type</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>DOJ</b></td>"; 
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>PrimaryContact</b></td>";
		    
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>OfficialEmail</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>PersonalEmail</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Gender</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Designation</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Department</b></td>";
            strName += "<td width='30px' style='align:left;' font-size='10px'><b>Sub-Department</b></td>";
            
            strName += "<td width='30px' style='align:left;' font-size='10px'><b>RMName</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>RmEmpId</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>RmEmail</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>BU</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>DOB</b></td>";
            strName += "<td width='30px' style='align:left;' font-size='10px'><b>Level</b></td>";
            
            strName += "<td width='30px' style='align:left;' font-size='10px'><b>BaseLocation</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>EmployeeType</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>JobCode</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>LegalEntity</b></td>";
		    strName += "<td width='30px' style='align:left;' font-size='10px'><b>Timestamp</b></td>";
	
		    strName += "</tr>";
			
		for(var i = 0;i < searchResults.length; i++)
		{ 
           
	            
	            var name = searchResults[i].getValue('entityid',null,'GROUP');
				
				var fullName = getFullName(name);
				
				var rmName = searchResults[i].getText('custentity_reportingmanager',null,'GROUP')
				
				var rmFullName = getFullName(rmName);
				
				var phone = searchResults[i].getValue('phone',null,'GROUP') == "- None -" ? "" : searchResults[i].getValue('phone',null,'GROUP');
				
				var gender = searchResults[i].getValue('gender',null,'GROUP') == "- None -" ? "" : searchResults[i].getValue('gender',null,'GROUP');
				var sub = searchResults[i].getText("subsidiary",null,"GROUP");
				var namesub = sub.split(":");
				var SubSid = namesub[namesub.length-1].trim()
			if(searchResults[i].getValue('custentity_is_sent_to_immidart',null,'GROUP') == 'F')
			{			
				
				
			JSON_New = {
				Person_Number: searchResults[i].getValue('custentity_fusion_empid',null,'GROUP') || "",
				Name: fullName || "",
				FirstName: searchResults[i].getValue('firstname',null,'GROUP') || "",
				MiddleName: "",//searchResults[i].getValue('middlename',null,'GROUP') || "",
				LastName: searchResults[i].getValue('lastname',null,'GROUP') || "",
				DOJ: searchResults[i].getValue('custentity_actual_hire_date',null,'GROUP') || "",
				PrimaryContact: phone,
				OfficialEmail: searchResults[i].getValue('email',null,'GROUP') || "",
				PersonalEmail: searchResults[i].getValue('custentity_personalemailid',null,'GROUP') || "",
				Gender: gender,
				Designation: searchResults[i].getValue('title',null,'GROUP') || "",
				Department: searchResults[i].getText('custrecord_parent_practice','department','GROUP') || "",
				SubDepartment: searchResults[i].getValue("departmentnohierarchy",null,"GROUP"),
				RMName: rmFullName  || "",
				RmEmpId: searchResults[i].getValue("custentity_fusion_empid","custentity_reportingmanager","GROUP") || "",
				RmEmail : searchResults[i].getValue("email","custentity_reportingmanager","GROUP") || "",
				BU: SubSid,//searchResults[i].getText("subsidiary",null,"GROUP") || "",
				DOB: searchResults[i].getValue('birthdate',null,'GROUP') || "",
				Level: searchResults[i].getText('employeestatus',null,'GROUP') || "",
				BaseLocation: searchResults[i].getValue('custentity_list_brillio_location_e',null,'GROUP') || "",
				EmployeeType: searchResults[i].getText('custentity_employeetype',null,'GROUP') || "",
              	JobCode : searchResults[i].getValue('custentity_job_code',null,'GROUP'),
              	LegalEntity : searchResults[i].getValue('custentity_legal_entity_fusion',null,'GROUP'),
				Timestamp: currentDateAndTime
				}
			/*	 var newAttachment = nlapiCreateFile('immidat.txt', 'PLAINTEXT', JSON.stringify(JSON_New));
	nlapiSendEmail(181189, 'nihal@inspirria.com', 'immidat new JSON' ,'Run time data', null, null, null, newAttachment);*/
			   
              if(adminEmployeeList.indexOf(searchResults[i].getValue('internalid',null,'GROUP'))!=-1){
                  //sendEmail
                  strName += addRowExcelData(JSON_New);
                  immidartFail.push(JSON_New);
                
                
              }
               else {
                nlapiSubmitField('employee',searchResults[i].getValue('internalid',null,'GROUP'),'custentity_is_sent_to_immidart','T')
                dataRow_New.push(JSON_New);
                
               }
			}
			else
			{
				JSON_Update = {
					/*DOJ: searchResults[i].getValue("custentity_actual_hire_date",null,"GROUP"),
					PrimaryContact: searchResults[i].getValue('phone',null,'GROUP'),
					Department: searchResults[i].getText('department',null,'GROUP'),
					RMName: searchResults[i].getText('custentity_reportingmanager',null,'GROUP'),
					BU: searchResults[i].getText('custrecord_parent_practice','department','GROUP'),
					Level: searchResults[i].getText('employeestatus',null,'GROUP'),
					BaseLocation: searchResults[i].getValue('custentity_list_brillio_location_e',null,'GROUP'),
					Person_Number: searchResults[i].getValue('custentity_fusion_empid',null,'GROUP'),
                    Timestamp: currentDateAndTime*/
                  
                Person_Number: searchResults[i].getValue('custentity_fusion_empid',null,'GROUP') || "",
				Name: fullName || "",
				FirstName: searchResults[i].getValue('firstname',null,'GROUP') || "",
				MiddleName: "",//searchResults[i].getValue('middlename',null,'GROUP') != "- None -" || "",
				LastName: searchResults[i].getValue('lastname',null,'GROUP') || "",
				DOJ: searchResults[i].getValue('custentity_actual_hire_date',null,'GROUP') || "",
				PrimaryContact: phone,
				OfficialEmail: searchResults[i].getValue('email',null,'GROUP') || "",
				PersonalEmail: searchResults[i].getValue('custentity_personalemailid',null,'GROUP') || "",
				Gender: gender,
				Designation: searchResults[i].getValue('title',null,'GROUP') || "",
				Department: searchResults[i].getText('custrecord_parent_practice','department','GROUP') || "",
				SubDepartment: searchResults[i].getValue("departmentnohierarchy",null,"GROUP"),
				RMName: rmFullName || "",
				RmEmpId: searchResults[i].getValue("custentity_fusion_empid","custentity_reportingmanager","GROUP") || "",
				RmEmail : searchResults[i].getValue("email","custentity_reportingmanager","GROUP") || "",
				BU: SubSid,//searchResults[i].getText("subsidiary",null,"GROUP") || "",
				DOB: searchResults[i].getValue('birthdate',null,'GROUP') || "",
				Level: searchResults[i].getText('employeestatus',null,'GROUP') || "",
				BaseLocation: searchResults[i].getValue('custentity_list_brillio_location_e',null,'GROUP') || "",
				EmployeeType: searchResults[i].getText('custentity_employeetype',null,'GROUP') || "",
                JobCode : searchResults[i].getValue('custentity_job_code',null,'GROUP'), 
				LegalEntity : searchResults[i].getValue('custentity_legal_entity_fusion',null,'GROUP'),
              	Timestamp: currentDateAndTime
				}
				dataRow_Update.push(JSON_Update);
			}
		}
        
        strName += "</table>";
        strName += "</body>";
      
       if(immidartFail.length>0) {
         var fromId = 442;
        var toEmail = 'mahesh.alavala@BRILLIO.COM';
        var sbj = 'Immidart Failure Records';
        var cc = ['FusionTaleoSupport@brillio.com','information.systems@brillio.com']
        var mailTemplate = "<p>Hi, </p>";
			 mailTemplate += "<p> Attached here is the report for Immidart failure records.  </p>";
			 mailTemplate += "<p> Kindly have a look into it  </p>";
			 
			 mailTemplate += "<p>Regards, <br/> Information Systems</p>";

    var fileObj = nlapiCreateFile('Admin_ImmidartEmployee.xls', 'EXCEL', nlapiEncrypt(strName, 'base64'));

    nlapiSendEmail(fromId, toEmail, sbj, mailTemplate, cc, null, null, fileObj);
       }

        
	}
		dataRow = {
			NewHire : dataRow_New,
			Update : dataRow_Update
		}
      	response.Status = true;
		response.Data = dataRow;
      	response.TimeStamp = currentDateAndTime;
		
	}
	catch (err) {
		nlapiLogExecution('ERROR', 'postRESTlet', err);
		response.Data = err;
		response.Status = false;
        response.TimeStamp = '';
	}
	
	return response;
	
}

//validate blank entries
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

//Get current date
function sysDate() 
{
	var date = new Date();
	var tdate = date.getDate();
	var month = date.getMonth() + 1; // jan = 0
	var year = date.getFullYear();
	return currentDate = month + '/' + tdate + '/' + year;
	
}

//Returns timestamp
function timestamp() {
	var str = "";

	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var meridian = "";
	if (hours > 12) {
	    meridian += "pm";
	} else {
	    meridian += "am";
	}
	if (hours > 12) {

	    hours = hours - 12;
	}
	if (minutes < 10) {
	    minutes = "0" + minutes;
	}
	if (seconds < 10) {
	    seconds = "0" + seconds;
	}
	//str += hours + ":" + minutes + ":" + seconds + " ";
	str += hours + ":" + minutes +" ";
	return str + meridian;
	}
	
	function getFullName(name){
		
		var fullName = name.split("-");
				
				if(fullName.length > 1){
					fullName = fullName[1];
				}else{
					fullName = fullName[0];
				}
				
				if(fullName == " None "){
					fullName = "";
				}
		
		return fullName;
	}

function getAdminEmployee(){
    var adminList = [];     
    var adminEmployee = nlapiSearchRecord("employee",null,
 [
   ["role","anyof","3"], 
   "AND", 
   ["custentity_implementationteam","is","F"], 
   "AND", 
   ["custentity_employeetype","anyof","1"]
 ], 
[
   new nlobjSearchColumn("entityid").setSort(true), 
   new nlobjSearchColumn("internalid")
]
);


if(adminEmployee){
    for (var res = 0; res < adminEmployee.length; res++) {
        //var entityID = adminEmployee[res].getValue('entityid')
        var internalid = adminEmployee[res].getValue('internalid')
        adminList.push(internalid);    
    
  }

}

 return adminList


}






function addRowExcelData(JSON_New) {

    var strName =  "<tr>";

			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.Name+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.FirstName+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.MiddleName+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.LastName+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.DOJ+"</td>";
			    
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.PrimaryContact+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.OfficialEmail+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.PersonalEmail+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.Gender+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.Designation+"</td>";
                strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.Department+"</td>";
                
                strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.SubDepartment+"</td>";
                strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.RMName+"</td>"; 
                strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.RmEmpId+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.RmEmail+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.BU+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.DOB+"</td>";
			    
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.Level+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.BaseLocation+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.EmployeeType+"</td>"; 
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.JobCode+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.LegalEntity+"</td>";
			    strName += "<td width='30px' style='align:left;' font-size='10px'>"+JSON_New.Timestamp+"</td>";
                strName += "</tr>";
                
                return strName;




}