/**
 * 
 * 
 * Version Date Author Remarks 1.00 06 May 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {

	try {
		var s_startDate = "3/1/2016";
		var s_endDate = "7/1/2016";
		var customer = "4295";
		var activeFilter = "T";

		var allocationDetails = getAllocations(s_startDate, s_endDate,
		        customer, activeFilter);

		tagVendorDetails(allocationDetails);

		// create a sublist to display the result
		var form = nlapiCreateForm('Employee Details');

		var htmlContent = generateHtmlContent(allocationDetails.AllocationList);
		form.addField('custpage_table', 'inlinehtml', '').setDefaultValue(
		        htmlContent);

		// var employeeList = form.addSubList('custpage_list', 'staticlist',
		// 'Employee');
		//
		// employeeList.addField('employee_text', 'text', 'Employee')
		// .setDisplayType('inline');
		// employeeList.addField('fusion_id', 'text', 'Fusion
		// Id').setDisplayType(
		// 'inline');
		// employeeList.addField('subsidiary_text', 'text', 'Subsidiary')
		// .setDisplayType('inline');
		// employeeList.addField('practice_text', 'text', 'Practice')
		// .setDisplayType('inline');
		// employeeList.addField('hire_date', 'text', 'Hire
		// Date').setDisplayType(
		// 'inline');
		// employeeList.addField('employee_type_text', 'text', 'Employee Type')
		// .setDisplayType('inline');
		// employeeList.addField('customer_text', 'text', 'Customer')
		// .setDisplayType('inline');
		// employeeList.addField('project_text', 'text', 'Project')
		// .setDisplayType('inline');
		// employeeList.addField('start_date', 'text', 'Start Date')
		// .setDisplayType('inline');
		// employeeList.addField('end_date', 'text', 'End Date').setDisplayType(
		// 'inline');
		// employeeList.addField('percent_allocation', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('is_billable', 'checkbox', 'Is Billable')
		// .setDisplayType('inline');
		// employeeList.addField('bill_rate', 'currency', 'Bill Rate')
		// .setDisplayType('inline');
		// employeeList.addField('ot_rate', 'currency', 'OT
		// Rate').setDisplayType(
		// 'inline');
		//
		// employeeList.addField('vendor_type', 'text', 'Vendor
		// Type').setDisplayType(
		// 'inline');
		// employeeList.addField('vendor', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('vendor_start_date', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('vendor_end_date', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('vendor_pay_rate', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('vendor_ot_rate', 'text',
		// '%age').setDisplayType(
		// 'inline');
		// employeeList.addField('vendor_payment_terms', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('vendor_subsidiary', 'text', '%age')
		// .setDisplayType('inline');
		// employeeList.addField('vendor_rate_type', 'text', '%age')
		// .setDisplayType('inline');
		//
		// employeeList.setLineItemValues(allocationDetails.AllocationList);

		response.writePage(form);
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
	}
}

function getAllocations(s_startDate, s_endDate, customer, activeFilter) {
	try {
		var finalList = [];
		var employeeIdList = [];

		// get all allocation between the specified time period
		var allocationFilters = [];
		allocationFilters.push(new nlobjSearchFilter('startdate', null,
		        'notafter', s_endDate));
		allocationFilters.push(new nlobjSearchFilter('enddate', null,
		        'notbefore', s_startDate));

		if (customer) {
			allocationFilters.push(new nlobjSearchFilter('customer', null,
			        'anyof', customer));
		}

		if (activeFilter == 'T') {
			allocationFilters.push(new nlobjSearchFilter(
			        'custentity_employee_inactive', 'employee', 'is', 'F'));
		}

		var allocationColumns = [ new nlobjSearchColumn('resource'),
		        new nlobjSearchColumn('startdate'),
		        new nlobjSearchColumn('enddate'),
		        new nlobjSearchColumn('company'),
		        new nlobjSearchColumn('customer'),
		        new nlobjSearchColumn('percentoftime'),
		        new nlobjSearchColumn('custeventrbillable'),
		        new nlobjSearchColumn('custevent3'),
		        new nlobjSearchColumn('custevent_otrate'),
		        new nlobjSearchColumn('entityid', 'employee'),
		        new nlobjSearchColumn('custentity_fusion_empid', 'employee'),
		        new nlobjSearchColumn('subsidiary', 'employee'),
		        new nlobjSearchColumn('department', 'employee'),
		        new nlobjSearchColumn('hiredate', 'employee'),
		        new nlobjSearchColumn('custentity_persontype', 'employee') ];

		var allocationSearch = searchRecord('resourceallocation', null,
		        allocationFilters, allocationColumns);

		if (allocationSearch) {

			allocationSearch.forEach(function(allocation) {
				employeeIdList.push(allocation.getValue('resource'));

				finalList
				        .push({
				            employee_id : allocation.getValue('resource'),
				            employee_text : allocation.getText('resource'),
				            fusion_id : allocation.getValue(
				                    'custentity_fusion_empid', 'employee'),
				            subsidiary_id : allocation.getValue('subsidiary',
				                    'employee'),
				            subsidiary_text : allocation.getText('subsidiary',
				                    'employee'),
				            practice_id : allocation.getValue('department',
				                    'employee'),
				            practice_text : allocation.getText('department',
				                    'employee'),
				            hire_date : allocation.getValue('hiredate',
				                    'employee'),
				            employee_type_text : allocation.getText(
				                    'custentity_persontype', 'employee'),
				            employee_type_id : allocation.getValue(
				                    'custentity_persontype', 'employee'),
				            customer_text : allocation.getText('customer'),
				            customer_id : allocation.getValue('customer'),
				            project_text : allocation.getText('company'),
				            project_id : allocation.getValue('company'),
				            start_date : allocation.getValue('startdate'),
				            end_date : allocation.getValue('enddate'),
				            percent_allocation : allocation
				                    .getValue('percentoftime'),
				            is_billable : allocation
				                    .getValue('custeventrbillable'),
				            bill_rate : allocation.getValue('custevent3'),
				            ot_rate : allocation.getValue('custevent_otrate'),
				            vendor_list : []
				        });
			});
		}

		return {
		    AllocationList : finalList,
		    ContractorList : _.uniq(employeeIdList)
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'getAllocations', err);
		throw err;
	}
}

function tagVendorDetails(allocationDetails) {
	try {
		var vendorDetailsSearch = searchRecord(
		        'customrecord_subtier_vendor_data',
		        null,
		        [
		                new nlobjSearchFilter('isinactive', null, 'is', 'F'),
		                new nlobjSearchFilter('custrecord_stvd_contractor',
		                        null, 'anyof', allocationDetails.ContractorList) ],
		        [ new nlobjSearchColumn('custrecord_stvd_contractor'),
		                new nlobjSearchColumn('custrecord_stvd_vendor_type'),
		                new nlobjSearchColumn('custrecord_stvd_vendor'),
		                new nlobjSearchColumn('custrecord_stvd_start_date'),
		                new nlobjSearchColumn('custrecord_stvd_end_date'),
		                new nlobjSearchColumn('custrecord_stvd_st_pay_rate'),
		                new nlobjSearchColumn('custrecord_stvd_ot_pay_rate'),
		                new nlobjSearchColumn('custrecord_stvd_payment_terms'),
		                new nlobjSearchColumn('custrecord_stvd_subsidiary'),
		                new nlobjSearchColumn('custrecord_stvd_rate_type') ]);

		if (vendorDetailsSearch) {

			allocationDetails.AllocationList
			        .forEach(function(allocation) {

				        vendorDetailsSearch
				                .forEach(function(vendor) {

					                if (vendor
					                        .getValue('custrecord_stvd_contractor') == allocation.employee_id) {
						                allocation.vendor_list
						                        .push({
						                            vendor_type : vendor
						                                    .getText('custrecord_stvd_vendor_type'),
						                            vendor : vendor
						                                    .getText('custrecord_stvd_vendor'),
						                            vendor_start_date : vendor
						                                    .getValue('custrecord_stvd_start_date'),
						                            vendor_end_date : vendor
						                                    .getValue('custrecord_stvd_end_date'),
						                            vendor_pay_rate : vendor
						                                    .getValue('custrecord_stvd_st_pay_rate'),
						                            vendor_ot_rate : vendor
						                                    .getValue('custrecord_stvd_ot_pay_rate'),
						                            vendor_payment_terms : vendor
						                                    .getText('custrecord_stvd_payment_terms'),
						                            vendor_subsidiary : vendor
						                                    .getText('custrecord_stvd_subsidiary'),
						                            vendor_rate_type : vendor
						                                    .getText('custrecord_stvd_rate_type')
						                        });
					                }
				                });
			        });
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'tagVendorDetails', err);
		throw err;
	}
}

function generateHtmlContent(allocationList) {

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=63885&c=3883006&h=1ab6e33882afec4c1d2b&_xt=.css";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";
	html += "<style> .timesheet-master-table { margin-left: 0px; !important}</style>";

	html += "<table class='timesheet-master-table'>";

	html += "<tr class='header-row'>";
	html += "<td colspan=6>Employee Details</td>";
	html += "<td colspan=8>Allocation Details</td>";
	html += "<td colspan=9>Vendor Details</td>";
	html += "</tr>";

	html += "<tr class='header-row'>";
	html += "<td>Employee</td>";
	html += "<td>Fusion Id</td>";
	html += "<td>Subsidiary</td>";
	html += "<td>Practice</td>";
	html += "<td>Hire Date</td>";
	html += "<td>Employee Type</td>";

	html += "<td>Customer</td>";
	html += "<td>Project</td>";
	html += "<td>Start Date</td>";
	html += "<td>End Date</td>";
	html += "<td>%age</td>";
	html += "<td>Is Billable</td>";
	html += "<td>Bill Rate</td>";
	html += "<td>OT Rate</td>";

	html += "<td>Type</td>";
	html += "<td>Name</td>";
	html += "<td>Start Date</td>";
	html += "<td>End Date</td>";
	html += "<td>Pay Rate</td>";
	html += "<td>OT Rate</td>";
	html += "<td>Payment Terms</td>";
	html += "<td>Subsidiary</td>";
	html += "<td>Rate Type</td>";
	html += "</tr>";

	allocationList.forEach(function(allocation) {
		html += "<tr class='ts-submitted'>";

		html += "<td>" + allocation.employee_text + "</td>";
		html += "<td>" + allocation.fusion_id + "</td>";
		html += "<td>" + allocation.subsidiary_text + "</td>";
		html += "<td>" + allocation.practice_text + "</td>";
		html += "<td>" + allocation.hire_date + "</td>";
		html += "<td>" + allocation.employee_type_text + "</td>";
		html += "<td>" + allocation.customer_text + "</td>";
		html += "<td>" + allocation.project_text + "</td>";
		html += "<td>" + allocation.start_date + "</td>";
		html += "<td>" + allocation.end_date + "</td>";
		html += "<td>" + allocation.percent_allocation + "</td>";
		html += "<td>" + (allocation.is_billable == 'T' ? 'Yes' : 'No')
		        + "</td>";
		html += "<td>" + allocation.bill_rate + "</td>";
		html += "<td>" + allocation.ot_rate + "</td>";

		if (allocation.vendor_list.length == 0) {
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "</tr>";
		} else if (allocation.vendor_list.length == 1) {

			allocation.vendor_list.forEach(function(vendor) {
				html += "<td>" + vendor.vendor_type + "</td>";
				html += "<td>" + vendor.vendor + "</td>";
				html += "<td>" + vendor.vendor_start_date + "</td>";
				html += "<td>" + vendor.vendor_end_date + "</td>";
				html += "<td>" + vendor.vendor_pay_rate + "</td>";
				html += "<td>" + vendor.vendor_ot_rate + "</td>";
				html += "<td>" + vendor.vendor_payment_terms + "</td>";
				html += "<td>" + vendor.vendor_subsidiary + "</td>";
				html += "<td>" + vendor.vendor_rate_type + "</td>";
				html += "</tr>";
			});
		} else {

			for (var i = 0; i < allocation.vendor_list.length; i++) {

				if (i > 1) {
					html += "<tr>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
					html += "<td></td>";
				}

				var vendor = allocation.vendor_list[i];
				html += "<td>" + vendor.vendor_type + "</td>";
				html += "<td>" + vendor.vendor + "</td>";
				html += "<td>" + vendor.vendor_start_date + "</td>";
				html += "<td>" + vendor.vendor_end_date + "</td>";
				html += "<td>" + vendor.vendor_pay_rate + "</td>";
				html += "<td>" + vendor.vendor_ot_rate + "</td>";
				html += "<td>" + vendor.vendor_payment_terms + "</td>";
				html += "<td>" + vendor.vendor_subsidiary + "</td>";
				html += "<td>" + vendor.vendor_rate_type + "</td>";
				html += "</tr>";
			}
		}
	});

	html += "</table>";
	return html;
}