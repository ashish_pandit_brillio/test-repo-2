/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Apr 2016     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
  
	var o_record	=	nlapiGetNewRecord();
	
	var i_project	=	o_record.getFieldValue('custrecord_project');
  
	var i_territory	=	nlapiLookupField('job', i_project, 'customer.territory');
  
nlapiLogExecution('AUDIT', nlapiGetRecordId(), i_territory);

	o_record.setFieldValue('custrecord_prterritory', i_territory);
	
}
