/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK ==================================
{
	/*
	 * Script Name : SUT_Create_Purchase_Order.js Author : Shweta Chopde Date :
	 * 13 April 2014 Description :
	 * 
	 * 
	 * Script Modification Log: -- Date -- -- Modified By -- --Requested By-- --
	 * Description --
	 * 
	 * 
	 * 
	 * Below is a summary of the process controls enforced by this script file.
	 * The control logic is described more fully, below, in the appropriate
	 * function headers and code blocks.
	 * 
	 * 
	 * SUITELET - suiteletFunction(request, response)
	 * 
	 * 
	 * SUB-FUNCTIONS - The following sub-functions are called by the above core
	 * functions in order to maintain code modularization: - NOT USED
	 * 
	 */
}
// END SCRIPT DESCRIPTION BLOCK ====================================

// BEGIN GLOBAL VARIABLE BLOCK =====================================
{
	// Initialize any Global Variables, in particular, debugging variables...

}
// END GLOBAL VARIABLE BLOCK =======================================

// BEGIN SUITELET ==================================================

function suiteletFunction(request, response) {

	var a_vendor_array = new Array();
	var a_PO_data_array = new Array();
	var i_cnt = 0
	var a_PO_ID_array = new Array();
	var s_status;
	var a_PI_array = new Array();
	var a_QI_array = new Array();
	var a_SNO_array = new Array();
	var i_pnt = 0;

	try {
		if (request.getMethod() == 'GET') {
			var i_recordID_PR = request
			        .getParameter('custscript_record_id_c_po')
			nlapiLogExecution('DEBUG', 'suiteletFunction', ' Record ID PR -->'
			        + i_recordID_PR);

			var a_create_PO_array = request
			        .getParameter('custscript_create_po_array')
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' Create PO Array -->' + a_create_PO_array);

			var a_quantity_array = request.getParameter('custscript_q_array')
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' Quantity Array -->' + a_quantity_array);

			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' Quantity Array Length-->' + a_quantity_array.length);

			var a_SNO_ID_array = a_create_PO_array;

			for (var so = 0; so < a_SNO_ID_array.length; so++) {
				a_SNO_array = a_SNO_ID_array.split(',')
				break;
			}

			var a_Q_ID_array = a_quantity_array;

			for (var po = 0; po < a_Q_ID_array.length; po++) {
				a_QI_array = a_Q_ID_array.split(',')
				break;
			}

			nlapiLogExecution('DEBUG', 'suiteletFunction', ' QI Array -->'
			        + a_QI_array);

			var a_sd_array = new Array();
			var i_snt = 0;

			if (_logValidation(a_QI_array) && _logValidation(a_SNO_array)) {
				for (var q = 0; q < a_QI_array.length; q++) {
					// for(var sn=0;sn<a_SNO_array.length;sn++)
					{
						i_snt++;
						a_sd_array.push(a_SNO_array[q] + '^^^^^^'
						        + a_QI_array[q]);

					}// SN
				}// Q

			}

			nlapiLogExecution('DEBUG', 'suiteletFunction', ' SD Array -->'
			        + a_sd_array);
			nlapiLogExecution('DEBUG', 'suiteletFunction',
			        ' SD Array Length-->' + a_sd_array.length);

			var i_quantity;
			if (_logValidation(i_recordID_PR)) {
				var o_recordOBJ = nlapiLoadRecord(
				        'customrecord__prpurchaserequest', i_recordID_PR)

				if (_logValidation(o_recordOBJ)) {
					var PR_No = o_recordOBJ.getFieldValue('custrecord_prno')

					var i_line_item_count = o_recordOBJ
					        .getLineItemCount('recmachcustrecord_purchaserequest')

					if (_logValidation(i_line_item_count)) {
						for (var i = 1; i <= i_line_item_count; i++) {
							var i_SR_No = o_recordOBJ.getLineItemValue(
							        'recmachcustrecord_purchaserequest',
							        'custrecord_prlineitemno', i)
							nlapiLogExecution('DEBUG',
							        'afterSubmit_Item_Data_Sourcing',
							        ' SR No -->' + i_SR_No);

							nlapiLogExecution('DEBUG', 'suiteletFunction',
							        ' a_SNO_ID_array indexOf(i_SR_No) -->'
							                + a_SNO_ID_array.indexOf(i_SR_No));

							if ((a_SNO_ID_array.indexOf(i_SR_No) > -1)) {
								nlapiLogExecution('DEBUG',
								        'afterSubmit_Item_Data_Sourcing',
								        ' ..............a_quantity_array[i] ......'
								                + a_QI_array[a_SNO_ID_array
								                        .indexOf(i_SR_No)]);

								var f_select_create_PO = o_recordOBJ
								        .getLineItemValue(
								                'recmachcustrecord_purchaserequest',
								                'custrecord_select_create_po',
								                i)
								nlapiLogExecution('DEBUG',
								        'afterSubmit_Item_Data_Sourcing',
								        ' Select Create PO -->'
								                + f_select_create_PO);

								var i_create_PO_ID = o_recordOBJ
								        .getLineItemValue(
								                'recmachcustrecord_purchaserequest',
								                'custrecord_created_po_id', i)
								nlapiLogExecution('DEBUG',
								        'afterSubmit_Item_Data_Sourcing',
								        ' Create PO ID -->' + i_create_PO_ID);

								if (_logValidation(i_create_PO_ID)) {
									var o_PO_OBJ = nlapiLoadRecord(
									        'purchaseorder', i_create_PO_ID)

									if (_logValidation(o_PO_OBJ)) {
										s_status = o_PO_OBJ
										        .getFieldValue('status')
									}// PO OBJ

								}// Created PO Object

								// if((i_create_PO_ID == null ||
								// i_create_PO_ID=='' ||
								// i_create_PO_ID==undefined) ||(s_status ==
								// 'Closed'))
								{
									var i_item = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_prmatgrpcategory',
									                i)
									nlapiLogExecution('DEBUG',
									        'afterSubmit_Item_Data_Sourcing',
									        ' Item-->' + i_item);

									var i_description = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_prmatldescription',
									                i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Description -->' + i_description);

									var i_currency = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_currencyforpr',
									                i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Currency -->' + i_currency);

									var i_quantity = a_QI_array[a_SNO_ID_array
									        .indexOf(i_SR_No)]
									nlapiLogExecution('DEBUG',
									        'afterSubmit_Item_Data_Sourcing',
									        ' Quantity -->' + i_quantity);

									var i_quantity_original = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_quantity', i)
									nlapiLogExecution('DEBUG',
									        'afterSubmit_Item_Data_Sourcing',
									        ' Quantity Original-->'
									                + i_quantity_original);

									var i_vendor_name = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_vendor_pr_item',
									                i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Description -->' + i_description);

									var i_project = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_project', i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Currency -->' + i_currency);

									var i_customer = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_customerforpritem',
									                i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Quantity -->' + i_quantity);

									var i_vertical = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_verticals', i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Description -->' + i_description);
									var i_territory = o_recordOBJ
							        		.getLineItemValue(
							        				'recmachcustrecord_purchaserequest',
							        				'custrecord_prterritory', i)
									
									var i_practice = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_prpractices', i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Currency -->' + i_currency);

									var i_subsidiary = o_recordOBJ
									        .getLineItemValue(
									                'recmachcustrecord_purchaserequest',
									                'custrecord_subsidiaryforpr',
									                i)
									// nlapiLogExecution('DEBUG',
									// 'afterSubmit_Item_Data_Sourcing','
									// Quantity -->' + i_quantity);

									// a_sd_array
									var a_split_y_arr = new Array();
									var i_SN;
									var i_qnt;
									for (var pj = 0; pj < a_sd_array.length; pj++) {
										a_split_y_arr = a_sd_array[pj]
										        .split('^^^^^^')

										i_SN = a_split_y_arr[0]

										i_qnt = a_split_y_arr[1]

										if (i_SN == i_SR_No) {
											i_quantity = i_qnt

											a_PO_data_array[i_cnt++] = i_SR_No
											        + '###' + i_item + '###'
											        + i_description + '###'
											        + i_currency + '###'
											        + i_quantity + '###'
											        + i_vendor_name + '###'
											        + i_project + '###'
											        + i_customer + '###'
											        + i_vertical + '###'
											        + i_practice + '###'
											        + i_subsidiary + '###'
											        + i_quantity_original + '###'
											        + i_territory

											a_vendor_array.push(i_vendor_name)
											break;
										}
									}

								}// Create PO = True

							}// Create PO Array

						}// Loop

						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' a_PO_data_array -->' + a_PO_data_array);

						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' a_vendor_array -->' + a_vendor_array);

						a_PO_ID_array = create_PO(a_PO_data_array,
						        a_vendor_array, i_recordID_PR, PR_No)

						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' ***************** a_PO_ID_array ***********-->'
						                + a_PO_ID_array);
						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' ***************** a_PO_ID_array length***********-->'
						                + a_PO_ID_array.length);

					}// Line Count

				}// Record Object

			}// Record ID PR

			/*
			 * for(var po=0;po<a_PO_ID_array.length;po++) {
			 * response.sendRedirect(
			 * 'RECORD','purchaseorder',a_PO_ID_array[po]) }
			 * 
			 */
			response.write(a_PO_ID_array.toString())

		} else if (request.getMethod() == 'POST') {

		}
	}// TRY
	catch (exception) {
		nlapiLogExecution('DEBUG', 'ERROR', ' Exception Caught -->' + exception);

		response.write(exception.getDetails())
	}// CATCH

}// Suitlet Function

// END SUITELET ====================================================

// BEGIN OBJECT CALLED/INVOKING FUNCTION
// ===================================================

function _logValidation(value) {
	if (value != null && value.toString() != null && value != ''
	        && value != undefined && value.toString() != undefined
	        && value != 'undefined' && value.toString() != 'undefined'
	        && value.toString() != 'NaN' && value != NaN) {
		return true;
	} else {
		return false;
	}
}

function create_PO(a_PO_data_array, a_vendor_array, i_recordID_PR, PR_No) {
	var a_PO_ID_array = new Array()
	var a_split_PO_array = new Array();
	var i_SR_No;
	var i_item;
	var i_description;
	var i_currency;
	var i_quantity;
	var i_vendor_name;
	var i_project;
	var i_customer;
	var i_vertical;
	var i_territory;
	var i_practice;
	var i_subsidiary;
	var a_repeated_vendor = new Array()
	var a_single_vendor = new Array()
	var i_quantity_original;

	var a_PR_array = new Array()

	var i_preferred_vendor;

	var i_vendor_PO;

	var i_rate_1;

	var i_payment_1;

	var i_rate_2;

	var i_payment_2;

	var i_rate_3;

	var i_payment_3;

	var i_vendor_1;

	var i_vendor_2;

	var i_vendor_3;

	var i_serial_no;

	var i_record_ID_PR_item;

	var a_PR_PO_array = new Array()

	var a_PI_array = new Array();

	var i_pnt = 0;

	var s_project_description;

	var i_customer_nv;

	a_repeated_vendor = get_single_element_array(a_vendor_array)

	a_single_vendor = removearrayduplicate(a_vendor_array)

	nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
	        ' a_repeated_vendor -->' + a_repeated_vendor);

	nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
	        ' a_single_vendor -->' + a_single_vendor);

	if (_logValidation(a_PO_data_array)) {
		if (_logValidation(a_single_vendor)) {
			for (var yt = 0; yt < a_single_vendor.length; yt++) {
				var i_sr_no = 1

				// ========================== Create PO
				// ==========================

				var o_PO_OBJ = nlapiCreateRecord('purchaseorder')

				var d_todays_date = get_todays_date()

				for (var t = 0; t < a_PO_data_array.length; t++) {
					a_split_PO_array = a_PO_data_array[t].split('###')

					i_SR_No = a_split_PO_array[0]

					i_serial_no = i_SR_No

					i_item = a_split_PO_array[1]

					i_description = a_split_PO_array[2]

					i_currency = a_split_PO_array[3]

					i_quantity = a_split_PO_array[4]

					i_vendor_name = a_split_PO_array[5]

					i_project = a_split_PO_array[6]

					i_customer = a_split_PO_array[7]

					i_vertical = a_split_PO_array[8]

					i_practice = a_split_PO_array[9]

					i_subsidiary = a_split_PO_array[10]

					i_quantity_original = a_split_PO_array[11]

					i_territory	= a_split_PO_array[12]
					
					if (_logValidation(i_project)) {
						var o_projectOBJ = nlapiLoadRecord('job', i_project);

						if (_logValidation(o_projectOBJ)) {
							var i_project_name_ID = o_projectOBJ
							        .getFieldValue('entityid');
							nlapiLogExecution('DEBUG',
							        'afterSubmit_cross_project',
							        ' Project Name ID -->' + i_project_name_ID);

							var i_project_name = o_projectOBJ
							        .getFieldValue('companyname');
							nlapiLogExecution('DEBUG',
							        'afterSubmit_cross_project',
							        ' Project Name -->' + i_project_name);

							s_project_description = i_project_name_ID + ' '
							        + i_project_name;
							nlapiLogExecution('DEBUG',
							        'afterSubmit_cross_project',
							        ' Project Description -->'
							                + s_project_description);

							i_customer_nv = o_projectOBJ.getFieldText('parent');
							nlapiLogExecution('DEBUG',
							        'afterSubmit_cross_project', ' Customer-->'
							                + i_customer);

						}// Project OBJ
					}

					if ((i_vendor_name == a_single_vendor[yt])) {
						if (!_logValidation(i_quantity)) {
							i_quantity = 0
						}

						o_PO_OBJ.setFieldValue('trandate', d_todays_date)
					 //o_PO_OBJ.setFieldValue('subsidiary', i_subsidiary)
						o_PO_OBJ.setFieldValue('entity', i_vendor_name)
						o_PO_OBJ.setFieldValue('currency', i_currency)
						o_PO_OBJ.setFieldValue('class', i_vertical)
						o_PO_OBJ.setFieldValue('department', i_practice)
						o_PO_OBJ.setFieldValue('custbody_prrefno',
						        i_recordID_PR)
						o_PO_OBJ.setFieldValue('custbody_pr_ref_no_2', PR_No)

						o_PO_OBJ.selectNewLineItem('item')
						o_PO_OBJ.setCurrentLineItemValue('item',
						        'custcol_srnumber', (parseInt(i_sr_no++))
						                .toFixed(0));
						o_PO_OBJ
						        .setCurrentLineItemValue('item', 'item', i_item);
						o_PO_OBJ.setCurrentLineItemValue('item', 'description',
						        i_description);
						o_PO_OBJ.setCurrentLineItemValue('item', 'quantity',
						        i_quantity);
						o_PO_OBJ.setCurrentLineItemValue('item', 'class',
						        i_vertical);
						o_PO_OBJ.setCurrentLineItemValue('item', 'custcol_territory', i_territory);
						o_PO_OBJ.setCurrentLineItemValue('item', 'department',
						        i_practice);
						o_PO_OBJ.setCurrentLineItemText('item', 'customer',
						        i_customer);
						o_PO_OBJ.setCurrentLineItemValue('item', 'vendorname',
						        i_vendor_name);
						o_PO_OBJ.setCurrentLineItemValue('item',
						        'custcolprj_name', s_project_description);
						o_PO_OBJ.setCurrentLineItemValue('item',
						        'custcolcustcol_temp_customer', i_customer_nv);
							
						var o_global_parameter = nlapiLoadRecord('customrecord_indian_tax_glo_parameter',1);
						o_PO_OBJ.setCurrentLineItemValue('item',
						        'taxcode', o_global_parameter.getFieldValue('custrecord_pay_servicetax'));

						var quant_remain = parseInt(i_quantity_original)
						        - parseInt(i_quantity)
						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' ***************** quant_remain ***********-->'
						                + quant_remain);

						var i_QA_recordID = search_quotation_analysis_recordID(
						        i_recordID_PR, i_serial_no, i_item)

						i_record_ID_PR_item = search_PR_Item_recordID(
						        i_recordID_PR, i_serial_no, i_item)

						nlapiLogExecution('DEBUG', 'QA Internal Id',
						        i_QA_recordID);
						nlapiLogExecution('DEBUG', 'PR Item Internal Id',
						        i_record_ID_PR_item);

						a_PR_array.push(i_record_ID_PR_item)

						if (_logValidation(i_QA_recordID)) {
							var o_QA_OBJ = nlapiLoadRecord(
							        'customrecord_quotationanalysis',
							        i_QA_recordID)

							if (_logValidation(o_QA_OBJ)) {
								i_preferred_vendor = o_QA_OBJ
								        .getFieldValue('custrecord_preferred_vendor')

								i_vendor_PO = o_QA_OBJ
								        .getFieldValue('custrecord_vendorforpo')

								i_rate_1 = o_QA_OBJ
								        .getFieldValue('custrecord_rate1')

								i_payment_1 = o_QA_OBJ
								        .getFieldValue('custrecord_paymentterms')

								i_rate_2 = o_QA_OBJ
								        .getFieldValue('custrecord_rate2')

								i_payment_2 = o_QA_OBJ
								        .getFieldValue('custrecord_paymenttermsv2')

								i_rate_3 = o_QA_OBJ
								        .getFieldValue('custrecord_rate3')

								i_payment_3 = o_QA_OBJ
								        .getFieldValue('custrecord_paymenttermsv3')

								i_vendor_1 = o_QA_OBJ
								        .getFieldValue('custrecord_vendor1name')

								i_vendor_2 = o_QA_OBJ
								        .getFieldValue('custrecord_vendor2name')

								i_vendor_3 = o_QA_OBJ
								        .getFieldValue('custrecord_vendor3name')

								o_PO_OBJ.setCurrentLineItemValue('item',
								        'custcol_po_pr_item',
								        i_record_ID_PR_item);

								if ((i_vendor_PO == i_vendor_1)
								        && (((i_vendor_1 != null
								                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 != null
								                && i_vendor_2 != undefined && i_vendor_2 != '') || (i_vendor_3 != null
								                && i_vendor_3 != undefined && i_vendor_3 != ''))))) {
									o_PO_OBJ.setCurrentLineItemValue('item',
									        'rate', i_rate_1);
									o_PO_OBJ
									        .setFieldValue('terms', i_payment_1)
								} else if ((i_vendor_PO == i_vendor_2)
								        && (((i_vendor_1 != null
								                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 != null
								                && i_vendor_2 != undefined && i_vendor_2 != '') || (i_vendor_3 != null
								                && i_vendor_3 != undefined && i_vendor_3 != ''))))) {
									o_PO_OBJ.setCurrentLineItemValue('item',
									        'rate', i_rate_2);
									o_PO_OBJ
									        .setFieldValue('terms', i_payment_2)
								} else if ((i_vendor_PO == i_vendor_3)
								        && ((i_vendor_1 != null
								                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 != null
								                && i_vendor_2 != undefined && i_vendor_2 != '') || (i_vendor_3 != null
								                && i_vendor_3 != undefined && i_vendor_3 != '')))) {
									o_PO_OBJ.setCurrentLineItemValue('item',
									        'rate', i_rate_3);
									o_PO_OBJ
									        .setFieldValue('terms', i_payment_3)
								} else if (((i_vendor_PO == i_vendor_1))
								        && ((i_vendor_1 != null
								                && i_vendor_1 != undefined && i_vendor_1 != '') && ((i_vendor_2 == null
								                || i_vendor_2 == undefined || i_vendor_2 == '') && (i_vendor_3 == null
								                || i_vendor_3 == undefined || i_vendor_3 == '')))) {
									o_PO_OBJ.setCurrentLineItemValue('item',
									        'rate', i_rate_1);
									o_PO_OBJ
									        .setFieldValue('terms', i_payment_1)
								}

							}// QA OBJ

						}
						o_PO_OBJ.commitLineItem('item');

						a_PI_array[i_pnt++] = i_record_ID_PR_item + '^^^^^'
						        + quant_remain;

						/*
						 * var o_PR_OBJ =
						 * nlapiLoadRecord('customrecord_pritem',i_record_ID_PR_item)
						 * 
						 * if (_logValidation(o_PR_OBJ)) {
						 * o_PR_OBJ.setFieldValue('custrecord_quantity',
						 * quant_remain) }
						 */

					}

					// var i_submitID_PR_item =
					// nlapiSubmitRecord(o_PR_OBJ,true,true)
					// nlapiLogExecution('DEBUG',
					// 'afterSubmit_Item_Data_Sourcing',' *****************
					// Submit ID PR Item***********-->' + i_submitID_PR_item);

				}// PO Data Array

				// =======================================================
				var i_submitID_PO = nlapiSubmitRecord(o_PO_OBJ, true, true)
				nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing',
				        ' ***************** Submit ID PO ***********-->'
				                + i_submitID_PO);

				// update the status of PR Item QA - Added by Nitish 2/9/2016
				if (i_QA_recordID) {
					nlapiSubmitField('customrecord_quotationanalysis',
					        i_QA_recordID,
					        'custrecord_approvalstatusforquotation', '8');
					nlapiLogExecution('DEBUG', 'QA status updated');
				}

				if (i_record_ID_PR_item) {
					nlapiSubmitField('customrecord_pritem',
					        i_record_ID_PR_item, 'custrecord_prastatus', '23');
					nlapiLogExecution('DEBUG', 'PR status updated');
				}
				// End

				a_PO_ID_array.push(i_submitID_PO)

			}// Loop Single Vendor

		}// Single Vendor Array

	}// Create PO Array

	/*
	 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
	 * *******a_PI_array*******-->' + a_PI_array);
	 * 
	 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
	 * *******a_PR_PO_array*******-->' + a_PR_PO_array);
	 * 
	 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
	 * *******a_PR_array*******-->' + a_PR_array);
	 * 
	 * nlapiLogExecution('DEBUG', 'afterSubmit_Item_Data_Sourcing','
	 * *******i_submitID_PO*******-->' + i_submitID_PO);
	 * 
	 */
	update_PO(a_PO_ID_array, a_PI_array)

	return a_PO_ID_array;
}

function update_PO(a_PO_ID_array, a_PI_array) {
	var i_create_po_1 = new Array()

	nlapiLogExecution('DEBUG', 'update_PO', ' a_PO_ID_array -->'
	        + a_PO_ID_array);

	var o_po_OBJ = nlapiLoadRecord('purchaseorder', a_PO_ID_array)
	if (o_po_OBJ != null && o_po_OBJ != '' && o_po_OBJ != undefined) {
		var i_line_cnt = o_po_OBJ.getLineItemCount('item')
		nlapiLogExecution('DEBUG', 'update_PO', ' i_line_cnt -->' + i_line_cnt);

		if (i_line_cnt != null && i_line_cnt != '' && i_line_cnt != undefined) {
			for (var i = 1; i <= i_line_cnt; i++) {
				var i_create_po = new Array()

				var i_item = o_po_OBJ.getLineItemValue('item',
				        'custcol_po_pr_item', i)
				nlapiLogExecution('DEBUG', 'update_PO', ' i_item -->' + i_item);

				var o_PR_OBJ = nlapiLoadRecord('customrecord_pritem', i_item)
				if (o_PR_OBJ != null && o_PR_OBJ != '' && o_PR_OBJ != undefined) {
					i_created_PO = o_PR_OBJ
					        .getFieldValues('custrecord_created_po_id')
					nlapiLogExecution('DEBUG',
					        'afterSubmit_Item_Data_Sourcing',
					        ' **i_created_PO****-->' + i_created_PO);

					if (_logValidation(i_created_PO)) {
						/*
						 * nlapiLogExecution('DEBUG',
						 * 'afterSubmit_Item_Data_Sourcing','
						 * **i_created_PO****[0]-->' + i_created_PO[0]);
						 * nlapiLogExecution('DEBUG',
						 * 'afterSubmit_Item_Data_Sourcing','
						 * **i_created_PO****[1]-->' + i_created_PO[1]);
						 * 
						 */
					}

					var i_created_PO_C = o_PR_OBJ
					        .getFieldValues('custrecord_created_po_id')
					nlapiLogExecution('DEBUG',
					        'afterSubmit_Item_Data_Sourcing',
					        ' **i_created_PO_C****-->' + i_created_PO_C);

					if (_logValidation(i_created_PO)) {
						i_create_po_1.push(a_PO_ID_array)
						var igg = i_created_PO.concat(i_create_po_1)
						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' ** IF igg cccccccccccccc****-->' + igg);
					} else {
						var igg = a_PO_ID_array
						nlapiLogExecution('DEBUG',
						        'afterSubmit_Item_Data_Sourcing',
						        ' **ELSE igg cccccccccccccc****-->' + igg);
					}
					nlapiLogExecution('DEBUG',
					        'afterSubmit_Item_Data_Sourcing',
					        ' **igg cccccccccccccc****-->' + igg);
					igg = removearrayduplicate(igg)

					o_PR_OBJ.setFieldValue('custrecord_created_po_id', igg);

					// Added by Nitish - 2/23/2016
					o_PR_OBJ.setFieldValue('custrecord_prastatus', '23');

					var quoteSearch = nlapiSearchRecord(
					        'customrecord_quotationanalysis', null, [
					                new nlobjSearchFilter('isinactive', null,
					                        'is', 'T'),
					                new nlobjSearchFilter(
					                        'custrecord_qa_pr_item', null,
					                        'anyof', o_PR_OBJ.getId()) ]);
					if (quoteSearch) {
						nlapiSubmitField('customrecord_quotationanalysis',
						        quoteSearch[0].getId(),
						        'custrecord_approvalstatusforquotation', '8');
						nlapiLogExecution('debug', 'QA status updated');
					}

					// End

					// ===============================

					var id = nlapiSubmitRecord(o_PR_OBJ, true, true)
					nlapiLogExecution('DEBUG', 'update_PO', ' id -->' + id);

				}

			}

		}

	}
	var a_split_array_PI = new Array();
	for (var pt = 0; pt < a_PI_array.length; pt++) {
		a_split_array_PI = a_PI_array[pt].split('^^^^^')

		var i_PI_ID = a_split_array_PI[0]

		var i_PI_Quantity = a_split_array_PI[1]

		nlapiLogExecution('DEBUG', 'update_PO', ' i_PI_ID -->' + i_PI_ID);
		nlapiLogExecution('DEBUG', 'update_PO', ' i_PI_Quantity -->'
		        + i_PI_Quantity);

		var o_PR_OBJ = nlapiLoadRecord('customrecord_pritem', i_PI_ID)

		if (_logValidation(o_PR_OBJ)) {
			o_PR_OBJ.setFieldValue('custrecord_quantity', i_PI_Quantity)

			// =======================================

			var i_submitID_PR_item = nlapiSubmitRecord(o_PR_OBJ, true, true)
			nlapiLogExecution('DEBUG', 'update_PO',
			        ' ***************** Submit ID PR Item***********-->'
			                + i_submitID_PR_item);

		}

	}// Loop

}

function get_single_element_array(array) {
	var newArray = new Array();
	var single_array = new Array();
	var duplicates_array = new Array();

	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i]) {
				duplicates_array.push(array[i])
				duplicates_array.push(newArray[j])
				continue label;
			}

		}
		newArray[newArray.length] = array[i];
	}

	return duplicates_array;
}

/**
 * 
 * @param {Object}
 *            array
 * 
 * Description : Remove the duplicates from an array
 */
function removearrayduplicate(array) {
	var newArray = new Array();
	label: for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < array.length; j++) {
			if (newArray[j] == array[i])
				continue label;
		}
		newArray[newArray.length] = array[i];
	}
	return newArray;
}

function get_todays_date() {
	var today;
	// ============================= Todays Date
	// ==========================================

	var date1 = new Date();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date ==' + date1);

	var offsetIST = 5.5;

	// To convert to UTC datetime by subtracting the current Timezone offset
	var utcdate = new Date(date1.getTime()
	        + (date1.getTimezoneOffset() * 60000));
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'UTC Date' + utcdate);

	// Then cinver the UTS date to the required time zone offset like back to
	// 5.5 for IST
	var istdate = new Date(utcdate.getTime() - ((-offsetIST * 60) * 60000));
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'IST Date ==' + istdate);

	var day = istdate.getDate();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Day ==' + day);

	var month = istdate.getMonth() + 1;
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Month ==' + month);

	var year = istdate.getFullYear();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Year ==' + year);

	var date_format = checkDateFormat();
	nlapiLogExecution('DEBUG', 'schedulerFunction', 'Date Format =='
	        + date_format);

	if (date_format == 'YYYY-MM-DD') {
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY') {
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY') {
		today = month + '/' + day + '/' + year;
	}
	return today;
}
function checkDateFormat() {
	var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

	return dateFormatPref;
}

function search_PR_Item_recordID(i_recordID, i_SR_No_QA, i_item_QA) {
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_recordID =='
	        + i_recordID);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_SR_No_QA =='
	        + i_SR_No_QA);
	nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', 'i_item_QA =='
	        + i_item_QA);

	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_purchaserequest', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_prlineitemno', null, 'is',
	        parseInt(i_SR_No_QA));
	filter[2] = new nlobjSearchFilter('custrecord_prmatgrpcategory', null,
	        'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_purchaserequest');
	columns[2] = new nlobjSearchColumn('custrecord_prlineitemno');
	columns[3] = new nlobjSearchColumn('custrecord_prmatgrpcategory');

	var a_seq_searchresults = nlapiSearchRecord('customrecord_pritem', null,
	        filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
		        'a_seq_searchresults ==' + a_seq_searchresults.length);

		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty]
			        .getValue('custrecord_purchaserequest');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty]
			        .getValue('custrecord_prlineitemno');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty]
			        .getValue('custrecord_prmatgrpcategory');
			nlapiLogExecution('DEBUG', 'search_PR_Item_recordID', ' i_item-->'
			        + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP

	}

	return i_internal_id;

}

function search_quotation_analysis_recordID(i_recordID, i_SR_No_QA, i_item_QA) {
	var i_internal_id;

	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_prquote', null, 'is',
	        i_recordID);
	filter[1] = new nlobjSearchFilter('custrecord_srno', null, 'is', i_SR_No_QA);
	filter[2] = new nlobjSearchFilter('custrecord_item', null, 'is', i_item_QA);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_item');
	columns[2] = new nlobjSearchColumn('custrecord_srno');
	columns[3] = new nlobjSearchColumn('custrecord_prquote');

	var a_seq_searchresults = nlapiSearchRecord(
	        'customrecord_quotationanalysis', null, filter, columns);

	if (a_seq_searchresults != null && a_seq_searchresults != ''
	        && a_seq_searchresults != undefined) {
		for (var ty = 0; ty < a_seq_searchresults.length; ty++) {
			i_internal_id = a_seq_searchresults[ty].getValue('internalid');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' Internal ID -->' + i_internal_id);

			var i_PR = a_seq_searchresults[ty].getValue('custrecord_prquote');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_PR ID -->' + i_PR);

			var i_line_no = a_seq_searchresults[ty].getValue('custrecord_srno');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_line_no -->' + i_line_no);

			var i_item = a_seq_searchresults[ty].getValue('custrecord_item');
			nlapiLogExecution('DEBUG', 'search_quotation_analysis_recordID',
			        ' i_item-->' + i_item);

			if ((i_PR == i_recordID) && (i_line_no == i_SR_No_QA)
			        && (i_item == i_item_QA)) {
				i_internal_id = i_internal_id
				break
			}

		}// LOOP
	}

	return i_internal_id;

}
// END OBJECT CALLED/INVOKING FUNCTION
// =====================================================
