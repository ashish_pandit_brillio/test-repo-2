//BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
	/*
	     Script Name	: SCH_CancelFRFforClosedOpp.js
	     Author			: Ashish Pandit
	     Company		: Inspirria CloudTech Pvt Ltd
	     Date			: 19 AUG 2019
	     Details        : Scheduled Script to Cancel FRF for the closed opportunities and send notification  

		 Script Modification Log:

		 -- Date --               -- Modified By --                  --Requested By--                   -- Description --


		 Below is a summary of the process controls enforced by this script file.  The control logic is described
	     more fully, below, in the appropriate function headers and code blocks.
	     SCHEDULED FUNCTION
	     - RFQbody_SCH_main()
	     SUB-FUNCTIONS
	     - The following sub-functions are called by the above core functions in order to maintain code
	     modularization:
	     - NOT USED
	 */
}

//END SCRIPT DESCRIPTION BLOCK  ====================================

function scheduledCancelFRFforClosedOpp() 
{
	try
	{
		var context = nlapiGetContext();
		var i_oppId = context.getSetting('SCRIPT', 'custscript_opportunity');
		var i_projectId = context.getSetting('SCRIPT', 'custscript_projectid');
		nlapiLogExecution('debug','i_oppId '+i_oppId,'i_projectId '+i_projectId);
		if(_logValidation(i_oppId))
		{
			var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
			[
			   ["custrecord_frf_details_opp_id","anyof",i_oppId]
			], 
			[
			   new nlobjSearchColumn("custrecord_frf_details_status"), 
			   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
			   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
			   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
			   new nlobjSearchColumn("custrecord_frf_details_project"), 
			   new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
			   new nlobjSearchColumn("custrecord_frf_details_account"), 
			   new nlobjSearchColumn("custrecord_frf_details_created_by"), 
			   new nlobjSearchColumn("custrecord_frf_details_created_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_status_flag"), 
			   new nlobjSearchColumn("custrecord_frf_details_allocated_emp"), 
			   new nlobjSearchColumn("custrecord_frf_details_open_close_status"),
			   new nlobjSearchColumn("custrecord_frf_details_open_close_status"),
			   new nlobjSearchColumn("custrecord_taleo_ext_hire_status","custrecord_frf_details_rrf_number",null),
			   new nlobjSearchColumn("custrecord_taleo_ext_offer_released","custrecord_frf_details_rrf_number",null),
			   new nlobjSearchColumn("custrecord_taleo_offer_accepted","custrecord_frf_details_rrf_number",null),
			   new nlobjSearchColumn("custrecord_taleo_offer_made_clear","custrecord_frf_details_rrf_number",null)
			]
			);
			if(customrecord_frf_detailsSearch)
			{
				nlapiLogExecution('debug','customrecord_frf_detailsSearch ',customrecord_frf_detailsSearch.length);
				for(var i=0; i<customrecord_frf_detailsSearch.length; i++)
				{
					var b_frfStatus = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_status');
					var i_frfId = customrecord_frf_detailsSearch[i].getId();
					nlapiLogExecution('debug','i_frfId ',i_frfId);
					var s_taleoStatus = customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_hire_status","custrecord_frf_details_rrf_number",null);
					var frfObj = nlapiLoadRecord('customrecord_frf_details',i_frfId);
					var n_oppId = frfObj.getFieldValue("custrecord_frf_details_opp_id");
					var recipient = frfObj.getFieldValue("custrecord_frf_details_created_by");
					var frfNumber = frfObj.getFieldValue("custrecord_frf_details_frf_number");
					var s_project = frfObj.getFieldText("custrecord_frf_details_project");
				
					var i_practice = frfObj.getFieldValue('custrecord_frf_details_res_practice');
					var i_Location = frfObj.getFieldValue('custrecord_frf_details_res_location');
					if(i_Location)
						var s_Country = nlapiLookupField('customrecord_taleo_location_master',i_Location,'custrecord_taleo_location_country')
					//==============================Taleo Status================================================================== 
					var b_cancleCheck = false;
					if(s_taleoStatus=="Sourcing")
					{
						var i_offerReleased =  customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_offer_released","custrecord_frf_details_rrf_number",null);
						var i_offerAccepted =  customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_offer_accepted","custrecord_frf_details_rrf_number",null);
						var i_offerMade =  customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_offer_made_clear","custrecord_frf_details_rrf_number",null);
						if(parseInt(i_offerAccepted)>0 || parseInt(i_offerReleased)>0)
						{
							b_cancleCheck = false;
						}
						else
						{
							b_cancleCheck = true;
						}
					}
					else if(s_taleoStatus=="Approved" || s_taleoStatus=="Draft" || s_taleoStatus== ""|| s_taleoStatus=="Rejected" ||s_taleoStatus=="Canceled"||s_taleoStatus=="Deleted")
					{
						b_cancleCheck = true;
					}
					else if(s_taleoStatus=="To Be Approved" ||s_taleoStatus=="On Hold"||s_taleoStatus=="Filled")
					{
						b_cancleCheck = false;
					}
					var ccArray = new Array();
					var i_pmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record',i_oppId,'custrecord_sfdc_opp_pm');
					var i_dmOpp = nlapiLookupField('customrecord_sfdc_opportunity_record',i_oppId,'custrecord_sfdc_opportunity_dm');
					if(i_pmOpp)
						ccArray.push(nlapiLookupField('employee',i_pmOpp,'email'));
					if(i_dmOpp)
						ccArray.push(nlapiLookupField('employee',i_dmOpp,'email'));
					ccArray.push('business.ops@brillio.com');//Business Operations Team
					ccArray.push('fuel.support@brillio.com');
					var taggedBenchProject = '';
					if(b_cancleCheck == false)
					{
						/*****Brillio Analytics***/
						if(i_practice == 322 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',8215); 
							taggedBenchProject = 8215;
						}
						else if(i_practice == 322 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167525); 
							taggedBenchProject = 167525;
						}
						/*****Digital Infrastucture***/
						else if(i_practice == 191 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',5514); 
							taggedBenchProject = 5514;
						}
						else if(i_practice == 191 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167526); 
							taggedBenchProject = 167526; 
						}
						/*****Product Enginnering***/
						else if(i_practice == 527 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',144844); 
							taggedBenchProject = 144844;
						}
						else if(i_practice == 527 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167530); 
							taggedBenchProject = 167530;
						}
						/*****Digital Infrastucture***/
						else if(i_practice == 530 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',111804); 
							taggedBenchProject = 111804;
						}
						else if(i_practice == 530 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167524); 
							taggedBenchProject = 167524;
						}
						/*****GDS***/
						else if(i_practice == 520)
						{
							frfObj.setFieldValue('custrecord_frf_details_project',144847); 
							taggedBenchProject = 144847;
						}
						/*****ATG***/
						else if(i_practice == 510 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',95174);
							taggedBenchProject	= 95174;					
						}
						else if(i_practice == 510 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167532); 
							taggedBenchProject = 167532;
						}
						/*****DFO***/
						else if(i_practice == 530 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',111804);
							taggedBenchProject	= 111804;					
						}
						else if(i_practice == 530 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167524); 
							taggedBenchProject = 167524;
						}
						frfObj.setFieldValue('custrecord_frf_details_bill_rate',''); //  Bill Rate 
						frfObj.setFieldValue('custrecord_frf_details_billiable','F'); //  Billable  
						var submitRecord = nlapiSubmitRecord(frfObj,{disabletriggers:true,enablesourcing:true});
						//var submitRecord = nlapiSubmitRecord(frfObj,{"disableTriggers" : true},false);
						sendEmailNotificationBenchProject(submitRecord,n_oppId,recipient,frfNumber,s_project,taggedBenchProject,ccArray); // Need to check Notification
					}
					else
					{
						frfObj.setFieldValue('custrecord_frf_details_status','T');
						frfObj.setFieldValue('custrecord_frf_details_status_flag',4);
						frfObj.setFieldValue('custrecord_frf_details_open_close_status',4);
						frfObj.setFieldValue('custrecord_frf_details_allocated_emp','');
						frfObj.setFieldValue('custrecord_frf_details_selected_emp','');
						frfObj.setFieldValue('custrecord_frf_details_cancellation_reas',1);
						//var submitRecord = nlapiSubmitRecord(frfObj,false,true); // Ignore mandatory 
						var submitRecord = nlapiSubmitRecord(frfObj,{disabletriggers:true,enablesourcing:true}); // Ignore mandatory 
						nlapiLogExecution('debug','submitRecord ',submitRecord);
						sendEmailNotification(submitRecord,n_oppId,recipient,frfNumber,s_project,ccArray);
					}
				}
			}					
		}
		else if(_logValidation(i_projectId))
		{
			var customrecord_frf_detailsSearch = nlapiSearchRecord("customrecord_frf_details",null,
			[
			   ["custrecord_frf_details_project","anyof",i_projectId]
			], 
			[
			   new nlobjSearchColumn("custrecord_frf_details_status"), 
			   new nlobjSearchColumn("custrecord_frf_details_external_hire"), 
			   new nlobjSearchColumn("custrecord_frf_details_start_date"), 
			   new nlobjSearchColumn("internalid"), 
			   new nlobjSearchColumn("custrecord_frf_details_end_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_selected_emp"), 
			   new nlobjSearchColumn("custrecord_frf_details_frf_number"), 
			   new nlobjSearchColumn("custrecord_frf_details_project"), 
			   new nlobjSearchColumn("custrecord_frf_emp_notice_rotation"), 
			   new nlobjSearchColumn("custrecord_frf_details_account"), 
			   new nlobjSearchColumn("custrecord_frf_details_created_by"), 
			   new nlobjSearchColumn("custrecord_frf_details_created_date"), 
			   new nlobjSearchColumn("custrecord_frf_details_status_flag"), 
			   new nlobjSearchColumn("custrecord_frf_details_allocated_emp"), 
			   new nlobjSearchColumn("custrecord_taleo_ext_hire_status","custrecord_frf_details_rrf_number",null),
			   new nlobjSearchColumn("custrecord_taleo_ext_offer_released","custrecord_frf_details_rrf_number",null),
			   new nlobjSearchColumn("custrecord_taleo_offer_accepted","custrecord_frf_details_rrf_number",null),
			   new nlobjSearchColumn("custrecord_taleo_offer_made_clear","custrecord_frf_details_rrf_number",null)			   
			]
			);
			if(customrecord_frf_detailsSearch)
			{
				nlapiLogExecution('debug','customrecord_frf_detailsSearch.length ',customrecord_frf_detailsSearch.length);
				for(var i=0; i<customrecord_frf_detailsSearch.length; i++)
				{
					var b_frfStatus = customrecord_frf_detailsSearch[i].getValue('custrecord_frf_details_status');
					
					var i_frfId = customrecord_frf_detailsSearch[i].getValue('internalid');
					nlapiLogExecution('debug','i_frfId  ',i_frfId);
					var s_taleoStatus = customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_hire_status","custrecord_frf_details_rrf_number",null);
					var frfObj = nlapiLoadRecord('customrecord_frf_details',i_frfId);
					var n_oppId = frfObj.getFieldValue("custrecord_frf_details_opp_id");
					var recipient = frfObj.getFieldValue("custrecord_frf_details_created_by");
					var frfNumber = frfObj.getFieldValue("custrecord_frf_details_frf_number");
					var s_project = frfObj.getFieldText("custrecord_frf_details_project");
					var i_practice = frfObj.getFieldValue('custrecord_frf_details_res_practice');
					var i_Location = frfObj.getFieldValue('custrecord_frf_details_res_location');
					if(i_Location)
						var s_Country = nlapiLookupField('customrecord_taleo_location_master',i_Location,'custrecord_taleo_location_country');
					var b_cancleCheck = false;
					if(s_taleoStatus=="Sourcing")
					{
						var i_offerReleased =  customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_ext_offer_released","custrecord_frf_details_rrf_number",null);
						var i_offerAccepted =  customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_offer_accepted","custrecord_frf_details_rrf_number",null);
						var i_offerMade =  customrecord_frf_detailsSearch[i].getValue("custrecord_taleo_offer_made_clear","custrecord_frf_details_rrf_number",null);
						if(parseInt(i_offerAccepted)>0 || parseInt(i_offerReleased)>0)
						{
							b_cancleCheck = false;
						}
						else
						{
							b_cancleCheck = true;
						}
					}
					else if(s_taleoStatus=="Approved" || s_taleoStatus=="Draft" || s_taleoStatus== ""|| s_taleoStatus=="Rejected" ||s_taleoStatus=="Canceled"||s_taleoStatus=="Deleted"){
						b_cancleCheck = true;
					}
					else if(s_taleoStatus=="To Be Approved" ||s_taleoStatus=="On Hold"||s_taleoStatus=="Filled")
					{
						b_cancleCheck = false;
					}

					var ccArray = new Array();
					var i_pm = nlapiLookupField('job',i_projectId,'custentity_projectmanager');
					var i_dm = nlapiLookupField('job',i_projectId,'custentity_deliverymanager');
					if(i_pm)
						ccArray.push(nlapiLookupField('employee',i_pm,'email'));
					if(i_dm)
						ccArray.push(nlapiLookupField('employee',i_dm,'email'));
					ccArray.push('business.ops@brillio.com');//Business Operations Team
					ccArray.push('fuel.support@brillio.com'); // FUEL Support
					
					var taggedBenchProject = '';
					if(b_cancleCheck == false)
					{
						/*****Brillio Analytics***/
						if(i_practice == 322 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',8215); 
							taggedBenchProject = 8215;
						}
						else if(i_practice == 322 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167525); 
							taggedBenchProject = 167525;
						}
						/*****Digital Infrastucture***/
						else if(i_practice == 191 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',5514); 
							taggedBenchProject = 5514;
						}
						else if(i_practice == 191 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167526); 
							taggedBenchProject = 167526; 
						}
						/*****Product Enginnering***/
						else if(i_practice == 527 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',144844); 
							taggedBenchProject = 144844;
						}
						else if(i_practice == 527 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167530); 
							taggedBenchProject = 167530;
						}
						/*****Digital Infrastucture***/
						else if(i_practice == 530 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',111804); 
							taggedBenchProject = 111804;
						}
						else if(i_practice == 530 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167524); 
							taggedBenchProject = 167524;
						}
						/*****GDS***/
						else if(i_practice == 520)
						{
							frfObj.setFieldValue('custrecord_frf_details_project',144847); 
							taggedBenchProject = 144847;
						}
						/*****ATG***/
						else if(i_practice == 510 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',95174);
							taggedBenchProject	= 95174;					
						}
						else if(i_practice == 510 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167532); 
							taggedBenchProject = 167532;
						}
						/*****DFO***/
						else if(i_practice == 530 && s_Country == "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',111804);
							taggedBenchProject	= 111804;					
						}
						else if(i_practice == 530 && s_Country != "India")
						{
							frfObj.setFieldValue('custrecord_frf_details_project',167524); 
							taggedBenchProject = 167524;
						}
						
						frfObj.setFieldValue('custrecord_frf_details_bill_rate',''); //  Bill Rate 
						frfObj.setFieldValue('custrecord_frf_details_billiable','F'); //  Billable  
						var submitRecord = nlapiSubmitRecord(frfObj,{disabletriggers:true,enablesourcing:true});
						//var submitRecord = nlapiSubmitRecord(frfObj,{"disableTriggers" : true},false);
						sendEmailNotificationBenchProject(submitRecord,n_oppId,recipient,frfNumber,s_project,taggedBenchProject,ccArray); // Need to check Notification
					}
					else
					{
						frfObj.setFieldValue('custrecord_frf_details_status','T');
						frfObj.setFieldValue('custrecord_frf_details_status_flag',4);
						frfObj.setFieldValue('custrecord_frf_details_open_close_status',4);
						frfObj.setFieldValue('custrecord_frf_details_allocated_emp','');
						frfObj.setFieldValue('custrecord_frf_details_selected_emp','');
						frfObj.setFieldValue('custrecord_frf_details_cancellation_reas',1);
						//var submitRecord = nlapiSubmitRecord(frfObj,false,true); // Ignore mandatory 
						var submitRecord = nlapiSubmitRecord(frfObj,{disabletriggers:true,enablesourcing:true}); // Ignore mandatory 
						nlapiLogExecution('debug','submitRecord ',submitRecord);
						sendEmailNotification(submitRecord,n_oppId,recipient,frfNumber,s_project,ccArray);
					}
				}
			}					
		}
		
	}
	catch(e)
	{
		nlapiLogExecution('debug','Error ',e);
	}
}

//========================================================================

function sendEmailNotification(frfId,n_oppId,recipient,frfNumber,s_project,ccArray)
{
	try
	{
		if(frfId)
		{
			if (n_oppId) 
			{
				var s_oppName = nlapiLookupField("customrecord_sfdc_opportunity_record", n_oppId, "custrecord_opportunity_name_sfdc");
			}
			var s_recipient = nlapiLookupField("employee", recipient, "firstname");
			var subject = frfNumber + ": FUEL Notification: FRF Cancelled";
			
			if(s_oppName)
			{
				finalProjectOpp = s_oppName; 
			}
			else
			{
				finalProjectOpp = s_project;
			}
			var body = "Dear " + s_recipient + " \nFRF# " + frfNumber + " has been cancelled as the opportunity " + finalProjectOpp + " against which it was raised is Closed/Shelved.\n\n\n"
			body += "Brillio-FUEL \n"
			body += "This email was sent from a notification only address.\n"
			body += "If you need further assistance, please write to fuel.support@brillio.com \n";
			if (recipient) 
			{
				nlapiSendEmail(154256, recipient, subject,body, ccArray, 'deepak.srinivas@brillio.com', null);
				//nlapiSendEmail(154256, recipient, subject, body);
				nlapiLogExecution("DEBUG", "Email Sent : ", body);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('error','Error in Sending Notice',e);
	}
}

function sendEmailNotificationBenchProject(frfId,n_oppId,recipient,frfNumber,s_project,taggedBenchProject,ccArray)
{
	try
	{
		if(frfId)
		{
			if(taggedBenchProject)
				var s_benchProject= nlapiLookupField('job',taggedBenchProject,'altname');
			if (n_oppId) 
			{
				var s_oppName = nlapiLookupField("customrecord_sfdc_opportunity_record", n_oppId, "custrecord_opportunity_name_sfdc");
			}
			
			var s_recipient = nlapiLookupField("employee", recipient, "firstname");
			var subject = frfNumber + ": FUEL Notification: FRF Cancelled";
			if(s_oppName){
				finalProjectOpp = s_oppName; 
			}else{
				finalProjectOpp = s_project;
			}
			var body = "Dear " + s_recipient + " \nFRF #" + frfNumber + " has been tagged to " + s_benchProject + " as the opportunity "+finalProjectOpp+" againest which it was raised has been closed/shelved and offer made for the external hire has been requested for this FRF.\n\n"
			body += "Brillio-FUEL \n"
			body += "This email was sent from a notification only address.\n"
			body += "If you need further assistance, please write to fuel.support@brillio.com \n";
			if (recipient) 
			{
				nlapiSendEmail(154256, recipient, subject,body, ccArray, 'deepak.srinivas@brillio.com', null);
				//nlapiSendEmail(154256, recipient, subject, body);
				nlapiLogExecution("DEBUG", "Email Sent : ", body);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('error','Error in Sending Notice',e)
	}
}
//================================================================================
function _logValidation(value) 
{
	if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
	{
		return true;
	}
	else 
	{ 
		return false;
	}
}

//function to get the subpractice tagged to parent 
function getSubPractices(i_practice){
	var temp = [];
	var departmentSearch = nlapiSearchRecord("department",null,
[
   ["isinactive","is","F"], 
   "AND", 
   ["custrecord_parent_practice","anyof",i_practice]
], 
[
]
);
if(departmentSearch){
	for(var i = 0 ; i < departmentSearch.length ;i++){
		temp.push(departmentSearch[i].getId());
	}
	return temp;
}else{
	return i_practice
}
}