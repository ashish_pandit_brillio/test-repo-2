// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script SCH_flash_mail_dm.js
	Author: Sai Saranya	
	Date:11-Nov-2018	
    Description: Scheduled script to send Flash in Email to DM for all tagged Projects


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --

	
	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

   
     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

		scheduled_SendEmailForApproval()



*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================
var f_total_cost_overall_prac = new Array();
var mnth_array=new Array();
mnth_array=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
function sch_flash_mail_to_DM() {
try{
		var context = nlapiGetContext();
	
		var date_proj = new Date();
		var sr_num = 0;
		var a_practice_list = new Array();
		var a_project_search_results = '';
		var d_today_date = new Date();
		var delivery_heads = new Array();
		//Conversion rates for INR and GBP
		 var inr_to_usd = 0;
		var gbp_to_usd = 0;
		var eur_to_usd = 0;
		var proj ='';
	//	var d_day = nlapiAddDays(d_today_date, -1);
		var d_month_start = nlapiAddDays(d_today_date, -1 * (d_today_date.getDate() - 1));
		var s_month_start = d_month_start.getMonth() + 1 + '/'
					        + d_month_start.getDate() + '/' + d_month_start.getFullYear();
		var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
		var s_month_end = d_month_end.getMonth() + 1 + '/'
					        + d_month_end.getDate() + '/' + d_month_end.getFullYear();
		var rate_month = d_today_date.getMonth()+1;
		var rate_year = d_today_date.getFullYear();
		var rate_filter = new Array();
		rate_filter =[["custrecord_pl_currency_exchange_month","anyof",rate_month],"AND",
						["formulatext: {custrecord_pl_currency_exchange_year}","is",rate_year]]
		var a_conversion_rate_table = nlapiSearchRecord('customrecord_pl_currency_exchange_rates',null,rate_filter,[new nlobjSearchColumn('custrecord_pl_currency_exchange_revnue_r'),
                                                                  new nlobjSearchColumn('custrecord_gbp_converstion_rate'),
                                                                  new nlobjSearchColumn('custrecord_eur_usd_conv_rate'),
                                                                  new nlobjSearchColumn('internalid').setSort(true)]);
		if(a_conversion_rate_table)
		{
			inr_to_usd = 1 / parseFloat(a_conversion_rate_table[0].getValue('custrecord_pl_currency_exchange_revnue_r'));
			gbp_to_usd = a_conversion_rate_table[0].getValue('custrecord_gbp_converstion_rate');
			eur_to_usd = a_conversion_rate_table[0].getValue('custrecord_eur_usd_conv_rate');
		 }
		 else
		 {
			 inr_to_usd = nlapiExchangeRate('INR','USD');
			 gbp_to_usd = nlapiExchangeRate('GBP','USD');
			 eur_to_usd = nlapiExchangeRate('EUR','USD');
							
		 }
        var o_total_array = [];
		//
			var a_project_filter = new Array();
		a_project_filter[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
		a_project_filter[1] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
		a_project_filter[2] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', ['FBM','TM']);
		//a_project_filter[3] = new nlobjSearchFilter('custentity_fp_rev_rec_type', null, 'anyof', [1,2,4]);
		a_project_filter[3] = new nlobjSearchFilter('custentity_discount_project',null,'is','F');
		a_project_filter[4] =new nlobjSearchFilter('enddate',null,'onorafter','startofthismonth');
		a_project_filter[5] = new nlobjSearchFilter('internalid', null, 'anyof',23694);
		var a_project_col = new Array();
		a_project_col[0] = new nlobjSearchColumn('custentity_deliverymanager');
		var a_project_search_results = searchRecord('job', null, a_project_filter, a_project_col);
		
		if (a_project_search_results)
		{
			nlapiLogExecution('audit','proj len:- '+a_project_search_results.length);
			for (var i_proj_index = 0; i_proj_index < a_project_search_results.length; i_proj_index++)
			{
				var o_context = nlapiGetContext();
				if(o_context.getRemainingUsage() <= 2000)
				{
					nlapiYieldScript();
				}
				var delivery_manager = a_project_search_results[i_proj_index].getValue('custentity_deliverymanager');
				if(delivery_heads.indexOf(delivery_manager) < 0)
				{
					delivery_heads.push(delivery_manager);
				}
			}
		}
		for(i_del_indx = 0; i_del_indx < delivery_heads.length ; i_del_indx++)
		{
			//var final_revenue_dat=new Array();
			var excel_data_res = [];
			var a_get_dm_projects = '';
			var a_project_filters = new Array();
			a_project_filters[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
			a_project_filters[1] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
			a_project_filters[2] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'FBM');
			a_project_filters[3] = new nlobjSearchFilter('custentity_fp_rev_rec_type', null, 'anyof', [1,2,4]);
			a_project_filters[4] = new nlobjSearchFilter('custentity_discount_project',null,'is','F');
			a_project_filters[5] = new nlobjSearchFilter('custentity_deliverymanager',null,'anyof',delivery_heads[i_del_indx]);
			a_project_filters[6] = new nlobjSearchFilter('enddate',null,'onorafter','startofthismonth');
			//a_project_filters[6] = new nlobjSearchFilter('internalid', null, 'anyof',136816);
			
			var a_project_search = searchRecord('job', null, a_project_filters, null);
			nlapiLogExecution('audit','proj len DM:- '+a_project_search.length);
			nlapiLogExecution('audit','proj  DM:- '+delivery_heads[i_del_indx]);
			var s_deliveryManager = nlapiLookupField('employee',delivery_heads[i_del_indx],'firstname');
			for(i_proj_ind =0 ; i_proj_ind < a_project_search.length ; i_proj_ind++)
			{
				var final_revenue_dat = {
					'excel_dat' : []
					};
				var o_project_object = nlapiLoadRecord('job', a_project_search[i_proj_ind].getId()); // load project record object
				// get necessary information about project from project record
				var s_project_region = o_project_object.getFieldValue('custentity_region');
				var i_customer_name = o_project_object.getFieldValue('parent');
				var s_customer_name = o_project_object.getFieldText('parent');
				var customer_lookup = nlapiLookupField('customer',parseInt(i_customer_name),'custentity_sow_validation_exception');
				var s_project_name = o_project_object.getFieldValue('companyname');
				var s_proj_entity_id = o_project_object.getFieldValue('entityid');
				s_project_name = s_proj_entity_id +' '+s_project_name;
				var d_proj_start_date = o_project_object.getFieldValue('startdate');
				var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
				var d_proj_enddate_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
				var d_proj_end_date = o_project_object.getFieldValue('enddate');
				var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
				var s_proj_executing_practice = o_project_object.getFieldText('custentity_practice');
				var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
				var i_del_manager = o_project_object.getFieldValue('custentity_deliverymanager');
				var i_proj_manager_practice = nlapiLookupField('employee', i_proj_manager, 'department');
				var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
				var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
				var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
				var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
				if(!i_project_value_ytd)
					i_project_value_ytd = 0;
				i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
				i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
				var s_projectManager = nlapiLookupField('employee',i_proj_manager,'firstname');
				
				var i_month = d_today_date.getMonth();
				nlapiLogExecution('DEBUG', 'i_month', i_month);
				var d_prev_lastday_mnth = new Date(d_today_date.getFullYear(), i_month , 0);
				if(i_proj_revenue_rec_type==1)
				{
					//
					if(d_proj_strt_date_old_proj)
					{
						d_proj_start_date = d_proj_strt_date_old_proj;
					}
					if(d_proj_enddate_old_proj)
					{
						d_proj_end_date = d_proj_enddate_old_proj;
					}	
					var a_revenue_cap_filter = [['custrecord_revenue_share_project', 'anyof', parseInt(a_project_search[i_proj_ind].getId())],'AND',
						['custrecord_revenue_share_project.enddate','onorafter','startofthismonth'],'AND',
						['custrecord_revenue_share_project.custentity_discount_project','is','F']];
					
					var a_column = new Array();
					a_column[0] = new nlobjSearchColumn('created').setSort(true);
					a_column[1] = new nlobjSearchColumn('custrecord_auto_number_counter');
					a_column[2] = new nlobjSearchColumn('custrecord_revenue_share_approval_status');
					a_column[3]= new nlobjSearchColumn('custrecord_sow_value_change_status');
					a_column[4]= new nlobjSearchColumn('custrecord_revenue_share_total');
					
					var a_get_logged_in_user_exsiting_revenue_cap = nlapiSearchRecord('customrecord_revenue_share', null, a_revenue_cap_filter, a_column);
					if (a_get_logged_in_user_exsiting_revenue_cap)
					{
						var i_revenue_share_id = a_get_logged_in_user_exsiting_revenue_cap[0].getId();
						var i_revenue_share_status = a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_approval_status');
						var i_saved_status=a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_sow_value_change_status');
						var pro_revenue_share= a_get_logged_in_user_exsiting_revenue_cap[0].getValue('custrecord_revenue_share_total');
						if (parseInt(i_revenue_share_status) == parseInt(3) || (i_saved_status=='Saved')) 
						{
							nlapiLogExecution('audit', 'revenue share found:- ' + i_revenue_share_id);
							if (parseInt(i_revenue_share_status) == parseInt(3))
							{
							var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)], 'and', ['created', 'onorbefore', d_month_end ],'and',['custrecord_is_mnth_effrt_confirmed','is','T']];
							var a_columns_mnth_end_effort_activity_srch = new Array();
							a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
							var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
							var i_mnth_end_json_id_m = a_get_mnth_end_effrt_activity[0].getId();
							
							}
							else if((i_saved_status=='Saved') )
							{
                              if(parseFloat(pro_revenue_share) <= parseFloat(i_project_sow_value))
                              {
								var a_effort_activity_mnth_end_filter = [['custrecord_revenue_share_parent_json', 'anyof', parseInt(i_revenue_share_id)], 'and', ['created', 'onorbefore', d_month_end ],'and',['custrecord_is_mnth_effrt_confirmed','is','T']];
								var a_columns_mnth_end_effort_activity_srch = new Array();
								a_columns_mnth_end_effort_activity_srch[0] = new nlobjSearchColumn('created').setSort(true);
								
								var a_get_mnth_end_effrt_activity = nlapiSearchRecord('customrecord_fp_rev_rec_mnth_end_json', null, a_effort_activity_mnth_end_filter, a_columns_mnth_end_effort_activity_srch);
								var i_mnth_end_json_id_m = a_get_mnth_end_effrt_activity[0].getId();
							 }
							}
							//
							var project_revenue= create_month_end_confirmation_milestone(a_project_search[i_proj_ind].getId(),i_revenue_share_id,i_mnth_end_json_id_m);
							for(var dataln=0;dataln<project_revenue.length;dataln++)
							{
								var revenue = 0;
								if(s_proj_currency != 'USD')
								{
									if(s_proj_currency == 'INR')
									{
										var amount = project_revenue[dataln].revenue;
										revenue =  amount * inr_to_usd;
									}
									else
									{
										var amount = project_revenue[dataln].revenue;
										revenue =  amount * gbp_to_usd;
									}
								}
								else
								{
									revenue =  project_revenue[dataln].revenue;
								}
								final_revenue_dat['excel_dat'].push({
									'project_id':s_project_name,
									'customer' :s_customer_name,
									'st_date' : d_proj_start_date ,
									'endDate' : d_proj_end_date ,
									'exec_practice' : s_proj_executing_practice,
									'sow_value' : i_project_sow_value ,
									'currency':s_proj_currency,
									'sub_practice':project_revenue[dataln].sub_pract,
									'revenue': revenue,
									'offset' : "NA",
									'NetRevenue' : revenue
												//'st_date':project_revenue[dataln].month,
												//'actual_rev':project_revenue[dataln].actual_rev,
									});
										//sr_num++;
							}
								excel_data_res.push(final_revenue_dat);
						}
					}
				}
				else
				{
					var a_revenue_cap_filter = [
						['custrecord_fp_rev_rec_others_projec','anyof', a_project_search[i_proj_ind].getId() ],'and',
						[ 'custrecord_revenue_other_approval_status', 'anyof', 2 ],'and',
						['custrecord_fp_rev_rec_others_projec.enddate','onorafter','startofthismonth'],'AND',
						['custrecord_fp_rev_rec_others_projec.custentity_discount_project','is','F']];
					var a_columns_existing_cap_srch = new Array();
					a_columns_existing_cap_srch[0] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_projec');
					a_columns_existing_cap_srch[1] = new nlobjSearchColumn('custrecord_fp_rev_rec_others_total_rev');
					a_columns_existing_cap_srch[2] = new nlobjSearchColumn('created').setSort(true);	
					a_columns_existing_cap_srch[3] = new nlobjSearchColumn('custrecord_revenue_other_approval_status');
					a_get_dm_projects = searchRecord('customrecord_fp_rev_rec_others_parent', null,a_revenue_cap_filter, a_columns_existing_cap_srch);
					if (_logValidation(a_get_dm_projects))
					{
						nlapiLogExecution('DEBUG','a_get_dm_projects.length=',a_get_dm_projects.length);
						flag_counter_arr=0;
						flag_counter=0;
						var a_participating_mail_id = new Array();		
						var customrecord_fp_others_mnth_end_jsonSearch = nlapiSearchRecord("customrecord_fp_others_mnth_end_json",null,
							[["custrecord_is_mnth_end_cfrmd","is","T"],"AND", 
							["custrecord_fp_others_mnth_end_fp_parent","anyof", a_get_dm_projects[0].getId()]], 
							[new nlobjSearchColumn("created",null,null).setSort(true),
							new nlobjSearchColumn("id",null,null), 
							new nlobjSearchColumn("custrecord_other_current_mnth_no",null,null)]);
						if(_logValidation(customrecord_fp_others_mnth_end_jsonSearch))
						{	
							nlapiLogExecution('Debug','customrecord_fp_others_mnth_end_jsonSearch.length',customrecord_fp_others_mnth_end_jsonSearch.length);
							var i_monthNo = customrecord_fp_others_mnth_end_jsonSearch[0].getValue('custrecord_other_current_mnth_no',null,null);
							var i_mnth_end_json_id = customrecord_fp_others_mnth_end_jsonSearch[0].getValue('id',null,null);
							nlapiLogExecution('debug','i_mnth_end_json_id=',i_mnth_end_json_id);
							var project_revenue= create_month_end_confirmation(a_project_search[i_proj_ind].getId(),a_get_dm_projects[0].getId(),i_mnth_end_json_id);
							for(var dataln=0;dataln<project_revenue.length;dataln++)
							{
								var revenue = 0;
								if(s_proj_currency != 'USD')
								{
									if(s_proj_currency == 'INR')
									{
										var amount = project_revenue[dataln].revenue;
										revenue =  amount * inr_to_usd;
									}
									else if(s_proj_currency == 'GBP')
									{
										var amount = project_revenue[dataln].revenue;
										revenue =  amount * gbp_to_usd;
									}
								}
								else
								{
									revenue =  project_revenue[dataln].revenue;
								}
								final_revenue_dat['excel_dat'].push({
									'project_id':s_project_name,
									'customer' :s_customer_name,
									'st_date' : d_proj_start_date ,
									'endDate' : d_proj_end_date ,
									'exec_practice' : s_proj_executing_practice,
									'sow_value' : i_project_sow_value ,
									'currency':s_proj_currency,
									'sub_practice':project_revenue[dataln].sub_pract,
									'revenue': revenue,
									'offset' : "NA",
									'NetRevenue' : revenue
												//'st_date':project_revenue[dataln].month,
												//'actual_rev':project_revenue[dataln].actual_rev,
									});
										//sr_num++;
							}
								excel_data_res.push(final_revenue_dat);
						}
						
					}
				}
			}
			var context = nlapiGetContext();
			if(context.getRemainingUsage() < 200)
			{
					var state = nlapiYieldScript();
			}
			//T&M FLASH Logic 
			var uniq_proj_array= new Array();
			var a_project_filters_tm = new Array();
			a_project_filters_tm[0] = new nlobjSearchFilter('status', null, 'noneof', 1);
			a_project_filters_tm[1] = new nlobjSearchFilter('jobtype', null, 'anyof', 2);
			a_project_filters_tm[2] = new nlobjSearchFilter('jobbillingtype', null, 'anyof', 'TM');
			a_project_filters_tm[3] = new nlobjSearchFilter('custentity_discount_project',null,'is','F');
			a_project_filters_tm[4] = new nlobjSearchFilter('custentity_deliverymanager',null,'anyof',delivery_heads[i_del_indx]);
			a_project_filters_tm[5] = new nlobjSearchFilter('enddate',null,'onorafter','startofthismonth');
			//a_project_filters[6] = new nlobjSearchFilter('internalid', null, 'anyof',136816);
			
			var a_project_search_tm = searchRecord('job', null, a_project_filters_tm, null);
			var dataRow_Proj_list= new Array();
			for(i_proj_tm =0 ; i_proj_tm < a_project_search_tm.length ; i_proj_tm++)
			{
				  dataRow_Proj_list.push(a_project_search_tm[i_proj_tm].getId());
			
			}
			if(dataRow_Proj_list.length > 0)
			{
			   // Store resource allocations for project and employee
				var a_resource_allocations = new Array();
				var filter = [];
				filter.push(new nlobjSearchFilter('custrecord_forecast_month_startdate', null, 'on', d_month_start));
				filter.push(new nlobjSearchFilter('custrecord_forecast_month_enddate', null, 'on', d_month_end));
				filter.push(new nlobjSearchFilter('custrecord_forecast_project_name', null, 'anyof', dataRow_Proj_list));
				//   filter.push(new nlobjSearchFilter('custrecord_forecast_project_name', null, 'anyof', 93521));

				var cols = [];
				cols.push(new nlobjSearchColumn('custrecord_forecast_month_startdate',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_month_enddate',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_vertical',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_department',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_parent_department',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_project_id',null,'group').setSort(false));
				cols.push(new nlobjSearchColumn('custrecord_forecast_project_name',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_customer',null,'group'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_amount',null,'sum'));
				cols.push(new nlobjSearchColumn('custrecord_forecast_project_currency',null,'group'));
				//cols.push(new nlobjSearchColumn('custrecord_forecast_employee',null,'group'));
				var final_revenue_dat = {
					'excel_dat' : []
					};
				var search_rec = searchRecord('customrecord_forecast_master_data', null, filter, cols);
			
				if (search_rec) {
			
					for (var indx = 0; indx < search_rec.length; indx++) {
						a_resource_allocations[indx] = {
							//'custpage_employee' : search_rec[indx].getValue('custrecord_forecast_employee',null,'group'),
							'custpage_vertical' : search_rec[indx].getText('custrecord_forecast_vertical',null,'group'),
							'custpage_employee_department' : search_rec[indx].getText('custrecord_forecast_department',null,'group'),
							'custpage_employee_department_value' : search_rec[indx].getValue('custrecord_forecast_department',null,'group'),
							'custpage_employee_parent_department' : search_rec[indx].getValue('custrecord_forecast_parent_department',null,'group'),
							'custpage_project_id' : search_rec[indx].getValue('custrecord_forecast_project_id',null,'group'),
							'custpage_project' : search_rec[indx].getText('custrecord_forecast_project_name',null,'group'),
							'custpage_project_internalid' : search_rec[indx].getValue('custrecord_forecast_project_name',null,'group'),
							'custpage_customer' : search_rec[indx].getText('custrecord_forecast_customer',null,'group'),
							'custpage_total_amount' : search_rec[indx].getValue('custrecord_forecast_amount',null,'sum'),
							'custpage_currency' : search_rec[indx].getText('custrecord_forecast_project_currency',null,'group')
						};
					}
				}
				var final_revenue_dat = {
					'excel_dat' : []
					};
				var offset_recognized_list =  new Array();
				var offsetvalue = 0;
				for( res_ind = 0; res_ind< a_resource_allocations.length;res_ind++)
				{
					var dep = a_resource_allocations[res_ind].custpage_employee_department_value;
					var proj_to_push = a_resource_allocations[res_ind].custpage_project_id+ '_' +dep;
					if(offset_recognized_list.indexOf(proj_to_push)< 0)
					{
						offsetvalue = offsetcalculation(a_resource_allocations[res_ind].custpage_project_id,a_resource_allocations[res_ind].custpage_project_internalid,a_resource_allocations[res_ind].custpage_currency,inr_to_usd,gbp_to_usd,dep);
						offset_recognized_list.push(proj_to_push);
					}
					if(uniq_proj_array.indexOf(a_resource_allocations[res_ind].custpage_project_id)<0)
					{
						if(final_revenue_dat.length != 0 && res_ind != 0)
						{
							excel_data_res.push(final_revenue_dat);
							uniq_proj_array.push(a_resource_allocations[res_ind].custpage_project_id);
						}
						final_revenue_dat = {
							'excel_dat' : []
						};
					}
					var revenue =0;
					var po_value=0;
					var proj_details = nlapiLoadRecord('job',a_resource_allocations[res_ind].custpage_project_internalid);
			
					/*if(a_resource_allocations[res_ind].custpage_currency != 'USD')
					{
						var amount= a_resource_allocations[res_ind].custpage_total_amount;
						if(a_resource_allocations[res_ind].custpage_currency == 'INR')
						{
							revenue = amount*inr_to_usd;
						}
						else if(a_resource_allocations[res_ind].custpage_currency =='GBP')
						{
							revenue = amount*gbp_to_usd;
						}
						
					}
					else*/
					{
						revenue = a_resource_allocations[res_ind].custpage_total_amount;
					}
					if(!_logValidation(proj_details.getFieldValue('custentity_projectvalue')))
					{
						po_value = 0;
					}
					final_revenue_dat['excel_dat'].push({
						'project_id':a_resource_allocations[res_ind].custpage_project,
						'customer' :a_resource_allocations[res_ind].custpage_customer,
						'st_date' : proj_details.getFieldValue('startdate') ,
						'endDate' : proj_details.getFieldValue('enddate') ,
						'exec_practice' : proj_details.getFieldText('custentity_practice'),
						'sow_value' : po_value,
						'currency':a_resource_allocations[res_ind].custpage_currency,
						'sub_practice':a_resource_allocations[res_ind].custpage_employee_department,
						'revenue': parseFloat(revenue),
						'offset' : parseFloat(offsetvalue),
						'NetRevenue': parseFloat(revenue)+ parseFloat(offsetvalue)
													
					});
					if(res_ind == a_resource_allocations.length -1)
					{
					excel_data_res.push(final_revenue_dat);
					}
				}
				
			}
	//
	if(excel_data_res.length >0)
	{
		var d_today = new Date();	
		var getCurrentMonth = d_today.getMonth() + 1;
		var getCurrentYear = d_today.getFullYear();
		var s_month_name = getMonthName(nlapiDateToString(d_today));
		var s_emailSub = 'Flash revenue for the month  '+s_month_name+' '+getCurrentYear;
		var s_emailBody = 'Dear '+s_deliveryManager +'<br> <p>Please review and confirm the revenue for your projects for the month '+s_month_name +'  '+ getCurrentYear +' before the freeze date .Should you fail to confirm, system will recognize the below revenue for the current month.</p><br><p><b>Disclaimer:</b> Discount and Exchange fluctuation in INR/GBP based invoices are ignored.';
			//
			s_emailBody += '<html>';
		s_emailBody += '<body>';
		//s_emailBody += '<p>Daily Ticker dated '+today+' '+hours+':'+minutes+' (24 Hour Format)</p>';
		//s_emailBody += '<table>';
		//s_emailBody += '	<tr>';
		//s_emailBody += ' <td width="100%">';
		s_emailBody += '<table width="100%" border="1">';
		s_emailBody += '	<tr style="background-color:#0B6FA4;">';
		s_emailBody += ' <td width="20%" font-size="11" align="center" style="color: #FFFFFF;">Project Name</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">Customer</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">Start Date</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">End Date</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">Executing Practice</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">PO Value</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;" >Currency</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">Sub Practice </td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">Revenue</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">Offset</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">NetRevenue</td>';
		s_emailBody += ' <td width="10%" font-size="11" align="center" style="color: #FFFFFF;">/Update</td>';
		//s_emailBody += ' <td width="10%" font-size="11" align="center">Offsite Unbilled Count</td>';
		//s_emailBody += ' <td width="10%" font-size="11" align="center">Total Unbilled Count</td>';
	//	s_emailBody += ' <td width="20%" font-size="11" align="center">Client Partner</td>';
		s_emailBody += '	</tr>';
		for(var proj_detail_index=0; proj_detail_index<excel_data_res.length; proj_detail_index++)
		{
			var proj_arr_ind=excel_data_res[proj_detail_index].excel_dat;
			//s_emailBody += '<tr>';
			for(var proj_data = 0;proj_data< proj_arr_ind.length; proj_data++)
			{
				s_emailBody += '<tr style="background-color:#D0E4F5;">';
				if(proj_data == 0)
				{
				s_emailBody += ' <td width="20%" font-size="11">' + excel_data_res[proj_detail_index].excel_dat[proj_data].project_id + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].customer + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].st_date + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].endDate + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].exec_practice + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].sow_value + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].currency+ '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].sub_practice + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].revenue + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].offset + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].NetRevenue + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">'+'<a href=https://debugger.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1706&deploy=1&user='+i_del_manager+'>Update</a>'+'</td>';
				//var href= debugger.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1706&deploy=1&proj_id='+excel_data_res;
				}
				else
				{
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="20%" font-size="11"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center"></td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].sub_practice + '</td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].revenue + '</td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].offset + '</td>';
				s_emailBody += ' <td style="background-color:#FDF5E6;" width="10%" font-size="11" align="center">' + excel_data_res[proj_detail_index].excel_dat[proj_data].NetRevenue + '</td>';
				s_emailBody += ' <td width="10%" font-size="11" align="center">'+'<a href=https://debugger.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=1706&deploy=1&user='+i_del_manager+'>Update</a>'+'</td>';
				}
					s_emailBody += '	</tr>';
			}
	
		//s_emailBody += ' <td width="10%" font-size="11" align="center">' + final_revenue_dat[proj_detail_index].unbillable_allo_offsite + '</td>';
		//s_emailBody += ' <td width="10%" font-size="11" align="center">' + final_revenue_dat[proj_detail_index].toFixed(2) + '</td>';
		//s_emailBody += ' <td width="20%" font-size="11">' + final_revenue_dat[proj_detail_index].customer_CP_name + '</td>';
		//s_emailBody += '	</tr>';
		
		}
		s_emailBody += '</table>';
	//	s_emailBody += ' </td>';	
	//	s_emailBody += '</tr>';
	//	s_emailBody += '</table>';	
					//strVar1 += '<p>NOTE: Management Count is the management team allocated to this Region.</p>';
		s_emailBody += '<p>**For any discrepancy in data, please reach out to nikhil.motiani@brillio.com </p>';
					
		s_emailBody += '<p>Thanks & Regards,</p>';
		s_emailBody += '<p>Team IS</p>';
		s_emailBody += '</body>';
		s_emailBody += '</html>';
			//
		//s_emailBody +=  '<br>If there are any discrepancy in number then pls get back to nikhil.motiani@brillio.com and sapan.shah@brillio.com immediately, before the freeze date.<br>Thanks & Regards,<br>Team IS'; 
		var a_emp_attachment = new Array();
		a_emp_attachment['entity'] = '62082';
		//nlapiSendEmail(442,i_projManagerEmail,s_emailSub,s_emailBody,a_participating_mail_id,['information.systems@brillio.com','deepak.srinivas@brillio.com'],recordarray,null,false,false,null);
		//nlapiSendEmail(442,'sai.vannamareddy@brillio.com',s_emailSub,s_emailBody,null,null,recordarray,);
		nlapiSendEmail(442, 'sai.vannamareddy@brillio.com', s_emailSub, s_emailBody,null,null,a_emp_attachment);
			
	}
			//
		}
		//
		
		
}	
catch(err)
{
	nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
}
}
function create_month_end_confirmation(i_projectId,parentRecId,i_month_end_activity_id)
{
	try
	{
		var sr_num = 0;
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var i_projectId = i_projectId;//request.getParameter('proj_id');
		var s_request_mode = '';//request.getParameter('mode');
		
		var i_revenue_share_id = parentRecId;//request.getParameter('revenue_share_id');
		
	
		var i_month_end_activity_id = i_month_end_activity_id;//request.getParameter('mnth_end_activity_id');
		
		if(!i_month_end_activity_id)
			return;
			
		var projectWiseRevenue = [];
		
		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', i_projectId]
													];
							
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
		
		var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
			nlapiLogExecution('audit','recognized revenue for project found');
			for(var i_revenue_index=0; i_revenue_index<a_get_ytd_revenue.length; i_revenue_index++)
			{
				//nlapiLogExecution('audit','recognized amnt:- ',a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'));
				a_revenue_recognized_for_project.push({
												'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
												'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
												'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
												'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
												'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
												'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
												'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
												});
			}
		}
			
		var o_project_object = nlapiLoadRecord('job', i_projectId); // load project record object
		// get necessary information about project from project record
		var s_project_region = o_project_object.getFieldValue('custentity_region');
		var i_customer_name = o_project_object.getFieldValue('parent');
		var customer_lookup = nlapiLookupField('customer',parseInt(i_customer_name),'custentity_sow_validation_exception');
		var s_project_name = o_project_object.getFieldValue('companyname');
		var d_proj_start_date = o_project_object.getFieldValue('startdate');
		var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
		var d_proj_end_date = o_project_object.getFieldValue('enddate');
		var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
		var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
		var i_proj_manager_practice = nlapiLookupField('employee', i_proj_manager, 'department');
		var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
		var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
		var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
		var d_proj_end_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
		var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
		if(!i_project_value_ytd)
			i_project_value_ytd = 0;
		i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
		i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
		if(d_proj_strt_date_old_proj)
		{
			d_proj_start_date = d_proj_strt_date_old_proj;
		}
		if(d_proj_end_date_old_proj)
		{
			d_proj_end_date = d_proj_end_date_old_proj;
		}
		var monthBreakUp = getMonthsBreakup(
		        nlapiStringToDate(d_proj_start_date),
		        nlapiStringToDate(d_proj_end_date));
				
		var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
		var i_year_project = d_pro_strtDate.getFullYear();
		
		var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
		var i_year_project_end = s_pro_endDate.getFullYear();
		
		var i_project_mnth = d_pro_strtDate.getMonth();
		var i_prject_end_mnth = s_pro_endDate.getMonth();
		
		var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
		var s_currency_symbol_usd = getCurrency_Symbol('USD');
		
		// get previous mnth effrt for true up and true down scenario
		var d_today_date = new Date();
		var i_current_mnth = d_today_date.getMonth();
		var i_current_year = d_today_date.getFullYear();
		{
			var a_total_effort_json = [];
			var o_mnth_end_effrt = nlapiLoadRecord('customrecord_fp_others_mnth_end_json',i_month_end_activity_id);
			var s_effrt_json = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json1');
			var s_effrt_json_2 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json2');
			var s_effrt_json_3 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json3');
			var s_effrt_json_4 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json4');
			var s_effrt_json_5 = o_mnth_end_effrt.getFieldValue('custrecord_fp_others_mnth_end_fld_json5');
			var a_duplicate_sub_prac_count = new Array();
			var a_unique_list_practice_sublist = new Array();
			
			var a_subprac_searched_once = new Array();
			var a_subprac_searched_once_month = new Array();
			var a_subprac_searched_once_year = new Array();
			
			if(s_effrt_json)
			{
				a_total_effort_json.push(s_effrt_json);
				
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				var s_entire_json_clubed = JSON.parse(s_effrt_json);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
						pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
					
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			if(s_effrt_json_2)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
						pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
														
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
										
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
											
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			if(s_effrt_json_3)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
							pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;	
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
											
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
						
						
						
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			if(s_effrt_json_4)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
							pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;	
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
									
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
										
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			if(s_effrt_json_5)
			{
				var i_practice_previous = 0;
				var f_total_revenue_for_tenure = 0;
				//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
				var s_entire_json_clubed = JSON.parse(s_effrt_json_5);
				for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
				{
					var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
					for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
					{
						var i_practice = a_row_json_data[i_row_json_index].prac;
						var s_practice = a_row_json_data[i_row_json_index].prac_text;
						var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
						var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
						var i_role = a_row_json_data[i_row_json_index].role;
						var s_role = a_row_json_data[i_row_json_index].role_text;
						var i_level = a_row_json_data[i_row_json_index].level;
						var s_level = a_row_json_data[i_row_json_index].level_text;
						var i_location = a_row_json_data[i_row_json_index].loc;
						var s_location = a_row_json_data[i_row_json_index].loc_text;
						var f_revenue = a_row_json_data[i_row_json_index].cost;
						if (!f_revenue) 
							f_revenue = 0;
						
						var f_revenue_share = a_row_json_data[i_row_json_index].share;
						if (!f_revenue_share) 
							f_revenue_share = 0;
						var pract_share_split=a_row_json_data[i_row_json_index].pract_share;
						if(!pract_share_split)
							pract_share_split=0;
						var mnt_flag=a_row_json_data[i_row_json_index].mnth_flag;	
						var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
						var s_mnth_strt_date = '1/31/2017';
						var s_mnth_end_date = '31/31/2017';
						var s_mnth = a_row_json_data[i_row_json_index].mnth;
						var s_year = a_row_json_data[i_row_json_index].year;
						
						var f_revenue_recognized = 0;
						
						if(a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac)<0)
						{
							a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
							a_duplicate_sub_prac_count.push({
														'sub_prac': a_row_json_data[i_row_json_index].subprac,
														'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
														'revenue_share': 50000,
														'no_count': 0
														});
									
						}
						
						f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
						
						if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
										
						var s_month_name = s_mnth + '_' + s_year;
						
						var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
						total_revenue = parseFloat(total_revenue).toFixed(2);
						
						if (i_practice_previous == 0) {
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
						
						if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
						}
					
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
						f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
						
						var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
						
						if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
							projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								//revenue_share: f_revenue_share,
								RevenueData: {}
							};
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
							
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].revenue_share = f_revenue_share;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].practice_share = pract_share_split;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].f_mnth_flag = mnt_flag;
						}
					}
				}
			}
			
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
			
				var f_total_cost_breakUp = generate_total_cost(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,2,a_duplicate_sub_prac_count[i_dupli].sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth);
				
			}
			f_current_month_actual_revenue_total_proj_level = 0;
			a_recognized_revenue_total_proj_level = new Array();
			var data_array = new Array();
			// Effort View table
			var h_tableHtml_effort_view = new Array();
			for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
			{
				
				var revenue_call =  generate_cost_view_other(projectWiseRevenue, monthBreakUp, i_year_project, i_project_mnth, 2, a_duplicate_sub_prac_count[i_dupli].sub_prac, s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth,f_total_cost_breakUp,i_project_sow_value,customer_lookup);
				revenue_call = revenue_call.split('##');
				var f_amount_to_recognize = revenue_call[0];
				
				data_array[sr_num]= {
					'sub_pract': a_duplicate_sub_prac_count[i_dupli].sub_prac_name,
					'revenue' : f_amount_to_recognize
				};
				sr_num++;
			}
			
			// Total revenue table
			
		
			return data_array ; 
			
		}
		
		nlapiLogExecution('audit','remaining usage:- ',nlapiGetContext().getRemainingUsage());
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','suitelet_month_end_confirmation','ERROR MESSAGE:- '+err);
	}
}
function create_month_end_confirmation_milestone(i_projectId,parentRecId,i_month_end_activity_id)
{
	try{
	
		var sr_num = 0;
		var o_context = nlapiGetContext();
		var i_user_logegdIn_id = o_context.getUser();
		var i_projectId = i_projectId;//request.getParameter('proj_id');
		var s_request_mode = '';//request.getParameter('mode');
		
		var i_revenue_share_id = parentRecId;//request.getParameter('revenue_share_id');
		
	
		var i_month_end_activity_id = i_month_end_activity_id;//request.getParameter('mnth_end_activity_id');
		
		if(!i_month_end_activity_id)
			return;
			var projectWiseRevenue = [];
			//
		var o_project_object = nlapiLoadRecord('job', i_projectId); // load project record object
		// get necessary information about project from project record
		var s_project_region = o_project_object.getFieldValue('custentity_region');
		var i_customer_name = o_project_object.getFieldValue('parent');
		var customer_lookup = nlapiLookupField('customer',parseInt(i_customer_name),'custentity_sow_validation_exception');
		var s_project_name = o_project_object.getFieldValue('companyname');
		var d_proj_start_date = o_project_object.getFieldValue('startdate');
		var d_proj_strt_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_strt_date');
		var d_proj_end_date = o_project_object.getFieldValue('enddate');
		var i_proj_executing_practice = o_project_object.getFieldValue('custentity_practice');
		var i_proj_manager = o_project_object.getFieldValue('custentity_projectmanager');
		var i_proj_manager_practice = nlapiLookupField('employee', i_proj_manager, 'department');
		var i_project_sow_value = o_project_object.getFieldValue('custentity_projectvalue');
		var i_proj_revenue_rec_type = o_project_object.getFieldValue('custentity_fp_rev_rec_type');
		var s_proj_currency = o_project_object.getFieldText('custentity_project_currency');
		var d_proj_end_date_old_proj = o_project_object.getFieldValue('custentity_rev_rec_new_end_date');
		var i_project_value_ytd = o_project_object.getFieldValue('custentity_ytd_rev_recognized');
		if(!i_project_value_ytd)
			i_project_value_ytd = 0;
		i_project_sow_value = parseFloat(i_project_sow_value) - parseFloat(i_project_value_ytd);
		i_project_sow_value = parseFloat(i_project_sow_value).toFixed(2);
		if(d_proj_strt_date_old_proj)
		{
			d_proj_start_date = d_proj_strt_date_old_proj;
		}
		if(d_proj_end_date_old_proj)
		{
			d_proj_end_date = d_proj_end_date_old_proj;
		}
		var monthBreakUp = getMonthsBreakup(nlapiStringToDate(d_proj_start_date), nlapiStringToDate(d_proj_end_date));
		var d_pro_strtDate = nlapiStringToDate(d_proj_start_date);
		var i_year_project = d_pro_strtDate.getFullYear();
		var s_pro_endDate = nlapiStringToDate(d_proj_end_date);
		var i_year_project_end = s_pro_endDate.getFullYear();
		var i_project_mnth = d_pro_strtDate.getMonth();
		var i_prject_end_mnth = s_pro_endDate.getMonth();
		var s_currency_symbol_proj = getCurrency_Symbol(s_proj_currency);
		var s_currency_symbol_usd = getCurrency_Symbol('USD');
		var a_revenue_recognized_for_project = new Array();
		var a_filter_get_ytd_revenue_recognized = [['custrecord_project_to_recognize_amount', 'anyof', i_projectId]];
		var a_columns_get_ytd_revenue_recognized = new Array();
		a_columns_get_ytd_revenue_recognized[0] = new nlobjSearchColumn('custrecord_project_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[1] = new nlobjSearchColumn('custrecord_practice_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[2] = new nlobjSearchColumn('custrecord_subparctice_to_recognize_amnt');
		a_columns_get_ytd_revenue_recognized[3] = new nlobjSearchColumn('custrecord_role_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[4] = new nlobjSearchColumn('custrecord_level_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[5] = new nlobjSearchColumn('custrecord_revenue_recognized');
		a_columns_get_ytd_revenue_recognized[6] = new nlobjSearchColumn('custrecord_month_to_recognize_amount');
		a_columns_get_ytd_revenue_recognized[7] = new nlobjSearchColumn('custrecord_year_to_recognize_amount');
		var a_get_ytd_revenue = nlapiSearchRecord('customrecord_fp_rev_rec_recognized_amnt', null, a_filter_get_ytd_revenue_recognized, a_columns_get_ytd_revenue_recognized);
		if (a_get_ytd_revenue)
		{
			for (var i_revenue_index = 0; i_revenue_index < a_get_ytd_revenue.length; i_revenue_index++)
			{
				a_revenue_recognized_for_project.push({
					'practice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_practice_to_recognize_amount'),
					'subpractice_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_subparctice_to_recognize_amnt'),
					'role_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_role_to_recognize_amount'),
					'level_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_level_to_recognize_amount'),
					'amount_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_revenue_recognized'),
					'month_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_month_to_recognize_amount'),
					'year_revenue_recognized': a_get_ytd_revenue[i_revenue_index].getValue('custrecord_year_to_recognize_amount')
				});
			}
		}
				
			//
								
		var s_effrt_json = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt');
		var s_effrt_json_2 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt2');
		var s_effrt_json_3 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt3');
		var s_effrt_json_4 = nlapiLookupField('customrecord_fp_rev_rec_mnth_end_json', i_month_end_activity_id, 'custrecord_json_mnth_end_effrt4');
		var a_duplicate_sub_prac_count = new Array();
		var a_unique_list_practice_sublist = new Array();
		var a_unique_list_level = new Array();
		var a_subprac_searched_once = new Array();
		var a_subprac_searched_once_month = new Array();
		var a_subprac_searched_once_year = new Array();
		if (s_effrt_json) {
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
			var s_entire_json_clubed = JSON.parse(s_effrt_json);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0)
					{
						a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
						a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
						});
					}
					var i_practice = a_row_json_data[i_row_json_index].prac;
					var s_practice = a_row_json_data[i_row_json_index].prac_text;
					var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
					var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
					var i_role = a_row_json_data[i_row_json_index].role;
					var s_role = a_row_json_data[i_row_json_index].role_text;
					var i_level = a_row_json_data[i_row_json_index].level;
					var s_level = a_row_json_data[i_row_json_index].level_text;
					var i_location = a_row_json_data[i_row_json_index].loc;
					var s_location = a_row_json_data[i_row_json_index].loc_text;
					var f_revenue = a_row_json_data[i_row_json_index].cost;
					if (!f_revenue) 
						f_revenue = 0;
					var f_revenue_share = a_row_json_data[i_row_json_index].share;
					if (!f_revenue_share) 
						f_revenue_share = 0;
					var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
					var s_mnth_strt_date = '1/31/2017';
					var s_mnth_end_date = '31/31/2017';
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var s_year = a_row_json_data[i_row_json_index].year;
					var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
					if (!f_revenue_recognized) 
						f_revenue_recognized = 0;
					var s_month_name = s_mnth + '_' + s_year;
					var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
					total_revenue = parseFloat(total_revenue).toFixed(2);
					if (i_practice_previous == 0) {
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
							f_total_revenue_for_tenure = 0;
							i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
											
					var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
											
					if (!projectWiseRevenue[i_prac_subPrac_role_level]) 
					{
						projectWiseRevenue[i_prac_subPrac_role_level] = {
										practice_name: s_practice,
										sub_prac_name: s_sub_practice,
										sub_practice: i_sub_practice,
										role_name: s_role,
										level_name: s_level,
										location_name: s_location,
										total_revenue_for_tenure: f_total_revenue_for_tenure,
										revenue_share: f_revenue_share,
										RevenueData: {}
						};
												
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
												
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					}
					else {
								projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
												
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					}
				}
			}
		}
		if (s_effrt_json_2) {
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
			var s_entire_json_clubed = JSON.parse(s_effrt_json_2);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0)
					{
						a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
												a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
						});
					}
											
					var i_practice = a_row_json_data[i_row_json_index].prac;
					var s_practice = a_row_json_data[i_row_json_index].prac_text;
					var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
					var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
					var i_role = a_row_json_data[i_row_json_index].role;
					var s_role = a_row_json_data[i_row_json_index].role_text;
					var i_level = a_row_json_data[i_row_json_index].level;
					var s_level = a_row_json_data[i_row_json_index].level_text;
					var i_location = a_row_json_data[i_row_json_index].loc;
					var s_location = a_row_json_data[i_row_json_index].loc_text;
					var f_revenue = a_row_json_data[i_row_json_index].cost;
					if (!f_revenue) 
							f_revenue = 0;
					var f_revenue_share = a_row_json_data[i_row_json_index].share;
					if (!f_revenue_share) 
							f_revenue_share = 0;
					//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
					var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
					var s_mnth_strt_date = '1/31/2017';
					var s_mnth_end_date = '31/31/2017';
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var s_year = a_row_json_data[i_row_json_index].year;
					var f_revenue_recognized = 0;
					var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
					if (!f_revenue_recognized) 
							f_revenue_recognized = 0;
					var s_month_name = s_mnth + '_' + s_year;
					var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
					total_revenue = parseFloat(total_revenue).toFixed(2);
					if (i_practice_previous == 0) {
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
								f_total_revenue_for_tenure = 0;
								i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
					var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
											
					if (!projectWiseRevenue[i_prac_subPrac_role_level]) {
						projectWiseRevenue[i_prac_subPrac_role_level] = {
										practice_name: s_practice,
										sub_prac_name: s_sub_practice,
										sub_practice: i_sub_practice,
										role_name: s_role,
										level_name: s_level,
										location_name: s_location,
										total_revenue_for_tenure: f_total_revenue_for_tenure,
										revenue_share: f_revenue_share,
										RevenueData: {}
										};
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
												
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
							projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
												
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
							projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
					}
				}
			}
								
		if (s_effrt_json_3) {
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
			var s_entire_json_clubed = JSON.parse(s_effrt_json_3);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0) 
					{
						a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
						a_duplicate_sub_prac_count.push({
													'prac': a_row_json_data[i_row_json_index].prac,
													'sub_prac': a_row_json_data[i_row_json_index].subprac,
													'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
													'emp_role': a_row_json_data[i_row_json_index].role,
													'emp_level': a_row_json_data[i_row_json_index].level,
													'emp_level_text': a_row_json_data[i_row_json_index].level_text,
													'emp_cost': a_row_json_data[i_row_json_index].cost,
													'revenue_share': 50000,
													'no_count': 0
							});
					}
											
					var i_practice = a_row_json_data[i_row_json_index].prac;
					var s_practice = a_row_json_data[i_row_json_index].prac_text;
					var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
					var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
					var i_role = a_row_json_data[i_row_json_index].role;
					var s_role = a_row_json_data[i_row_json_index].role_text;
					var i_level = a_row_json_data[i_row_json_index].level;
					var s_level = a_row_json_data[i_row_json_index].level_text;
					var i_location = a_row_json_data[i_row_json_index].loc;
					var s_location = a_row_json_data[i_row_json_index].loc_text;
					var f_revenue = a_row_json_data[i_row_json_index].cost;
					if (!f_revenue) 
						f_revenue = 0;
					var f_revenue_share = a_row_json_data[i_row_json_index].share;
					if (!f_revenue_share) 
						f_revenue_share = 0;
					//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
					var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
					var s_mnth_strt_date = '1/31/2017';
					var s_mnth_end_date = '31/31/2017';
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var s_year = a_row_json_data[i_row_json_index].year;
					var f_revenue_recognized = 0;
					var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
					if (!f_revenue_recognized) 
						f_revenue_recognized = 0;
					var s_month_name = s_mnth + '_' + s_year;
					var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
					total_revenue = parseFloat(total_revenue).toFixed(2);
					if (i_practice_previous == 0) {
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
						f_total_revenue_for_tenure = 0;
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
					var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
					if (!projectWiseRevenue[i_prac_subPrac_role_level])
					{
						projectWiseRevenue[i_prac_subPrac_role_level] = {
									practice_name: s_practice,
									sub_prac_name: s_sub_practice,
									sub_practice: i_sub_practice,
									role_name: s_role,
									level_name: s_level,
									location_name: s_location,
									total_revenue_for_tenure: f_total_revenue_for_tenure,
									revenue_share: f_revenue_share,
									RevenueData: {}
							};
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
													
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					}
					else {
						projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
					}
				}
			}
		}
								
		if (s_effrt_json_4) {
			var i_practice_previous = 0;
			var f_total_revenue_for_tenure = 0;
									//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
			var s_entire_json_clubed = JSON.parse(s_effrt_json_4);
			for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
			{
				var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
				for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
				{
					if (a_unique_list_practice_sublist.indexOf(a_row_json_data[i_row_json_index].subprac) < 0)
					{
						a_unique_list_practice_sublist.push(a_row_json_data[i_row_json_index].subprac);
						a_duplicate_sub_prac_count.push({
									'prac': a_row_json_data[i_row_json_index].prac,
									'sub_prac': a_row_json_data[i_row_json_index].subprac,
									'sub_prac_name': a_row_json_data[i_row_json_index].subprac_text,
									'emp_role': a_row_json_data[i_row_json_index].role,
									'emp_level': a_row_json_data[i_row_json_index].level,
									'emp_level_text': a_row_json_data[i_row_json_index].level_text,
									'emp_cost': a_row_json_data[i_row_json_index].cost,
									'revenue_share': 50000,
									'no_count': 0
							});
					}
					var i_practice = a_row_json_data[i_row_json_index].prac;
					var s_practice = a_row_json_data[i_row_json_index].prac_text;
					var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
					var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
					var i_role = a_row_json_data[i_row_json_index].role;
					var s_role = a_row_json_data[i_row_json_index].role_text;
					var i_level = a_row_json_data[i_row_json_index].level;
					var s_level = a_row_json_data[i_row_json_index].level_text;
					var i_location = a_row_json_data[i_row_json_index].loc;
					var s_location = a_row_json_data[i_row_json_index].loc_text;
					var f_revenue = a_row_json_data[i_row_json_index].cost;
					if (!f_revenue) 
						f_revenue = 0;
					var f_revenue_share = a_row_json_data[i_row_json_index].share;
					if (!f_revenue_share) 
						f_revenue_share = 0;
					//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
					var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
					var s_mnth_strt_date = '1/31/2017';
					var s_mnth_end_date = '31/31/2017';
					var s_mnth = a_row_json_data[i_row_json_index].mnth;
					var s_year = a_row_json_data[i_row_json_index].year;
					var f_revenue_recognized = 0;
					var f_revenue_recognized = find_recognized_revenue(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year);
					if (!f_revenue_recognized) 
						f_revenue_recognized = 0;
					var s_month_name = s_mnth + '_' + s_year;
					var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
					total_revenue = parseFloat(total_revenue).toFixed(2);
					if (i_practice_previous == 0) {
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
						f_total_revenue_for_tenure = 0;
						i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
					}
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
					f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
					var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
					if (!projectWiseRevenue[i_prac_subPrac_role_level])
					{
						projectWiseRevenue[i_prac_subPrac_role_level] = {
								practice_name: s_practice,
								sub_prac_name: s_sub_practice,
								sub_practice: i_sub_practice,
								role_name: s_role,
								level_name: s_level,
								location_name: s_location,
								total_revenue_for_tenure: f_total_revenue_for_tenure,
								revenue_share: f_revenue_share,
								RevenueData: {}
						};
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
						else {
						projectWiseRevenue[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
						projectWiseRevenue[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
						}
				}
			}
		}
	// Get Cost to be recognized for each sub practice
		var a_practice_nt_processed = new Array();
		var a_index_prac_nt_processed = new Array();
		//for(var i_allo_index=0; i_allo_index<a_allocation_details.length; i_allo_index++)
		var data_array= new Array();
		for (var i_dupli = 0; i_dupli < a_duplicate_sub_prac_count.length; i_dupli++)
		{
			if (o_context.getRemainingUsage() <= 1000) {
			nlapiYieldScript();
			}
			var f_actual_revenue_to_recognize = generate_cost_view(projectWiseRevenue, monthBreakUp, i_year_project, i_project_mnth, 2, a_duplicate_sub_prac_count[i_dupli].sub_prac, s_currency_symbol_proj, s_currency_symbol_usd, i_year_project_end, i_prject_end_mnth);
			nlapiLogExecution('audit', 'json to process:- ' + f_actual_revenue_to_recognize, 'level:-- ' + a_duplicate_sub_prac_count[i_dupli].emp_level_text);
			f_actual_revenue_to_recognize = f_actual_revenue_to_recognize.split('##');
			var f_amount_to_recognize = f_actual_revenue_to_recognize[0];
			var f_amount_sub_total = f_actual_revenue_to_recognize[1];
			data_array[sr_num]= {
					'sub_pract': a_duplicate_sub_prac_count[i_dupli].sub_prac_name,
					'revenue' : f_amount_to_recognize
				};
				sr_num++;
		}								
			return data_array ; 							
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','Sch_flad_dm_milestone_error',err);
	}
}
function generate_cost_view(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	var d_today_date = new Date();
	//var i_current_month = mnth_array.indexOf(s_currentMonthName);
	var i_current_month = d_today_date.getMonth();
	
	var i_current_year = d_today_date.getFullYear();
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{	
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var i_mnth_posi = parseFloat(i_current_month) - parseFloat(i_project_mnth);
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							//nlapiLogExecution('audit','monthly amount:- '+parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount));
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			//f_total_prev_mnths = parseFloat(f_total_prev_mnths) - parseFloat(1);
		}
		//if(i_amt < f_total_prev_mnths)
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
		
	}
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	nlapiLogExecution('audit','f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month,'i_total_revenue_recognized_ytd:- '+i_total_revenue_recognized_ytd)
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	
	//f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	
	f_current_month_actual_revenue = parseInt(f_current_month_actual_revenue);
	var json_to_process = f_current_month_actual_revenue+'##'+a_amount[f_total_prev_mnths];
	return json_to_process;
}
function generate_cost_view_other(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth,f_total_cost_breakUp,i_project_sow_value,customer_lookup)
{
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	var revenue_share_arr=new Array();
	var practice_share_arr=new Array();
	var mnth_flag_arr=new Array();
	var d_today_date = new Date();
//var i_current_month=mnth_array.indexOf(s_currentMonthName);
	
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
					}
				}
			}
			
			b_first_line_copied = 1;
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{	
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							var i_mnth_posi = parseFloat(i_current_month) - parseFloat(i_project_mnth);
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push amnt');
								a_amount.push(total_revenue_format);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								revenue_share_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share));
								practice_share_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].practice_share));
								mnth_flag_arr.push(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].f_mnth_flag));
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								revenue_share_arr[i_amt]=parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].revenue_share);
								practice_share_arr[i_amt]=(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].practice_share));
								mnth_flag_arr[i_amt]=(parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].f_mnth_flag));
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								//if(i_amt < i_mnth_posi-2)
									a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							//nlapiLogExecution('audit','monthly amount:- '+parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount));
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
					}
				}
				
				if (mode == 2)
				{
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	var f_tot_revenue_till_current_month=0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) /parseFloat(f_total_cost_breakUp[i_amt]);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if((mnth_flag_arr[i_amt]==1) && (a_amount[i_amt] != 0))
		{
			var f_revenue_amount = practice_share_arr[i_amt];
		}
		else
		{
			var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(revenue_share_arr[i_amt])) / 100;
		}
		//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
			
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
			//f_total_prev_mnths = parseFloat(f_total_prev_mnths) - parseFloat(1);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount);
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			
		}
		if(i_amt < f_total_prev_mnths)
		{
			f_tot_revenue_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_tot_revenue_till_current_month);
						
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
	}
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
		{
			var i_amount_map = 0;
			if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						var f_prcnt_revenue = parseFloat(a_amount[i_amount_map]) / parseFloat(i_total_row_revenue);
						f_prcnt_revenue = f_prcnt_revenue * 100;
						
						//var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
						var f_revenue_amount = (parseFloat(f_prcnt_revenue) * revenue_share_arr[i_amount_map]) / 100;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].total_revenue = f_revenue_amount;
						projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcent_complt = f_prcnt_revenue;
						i_amount_map++;
					}
				}	
			}
		}
	}
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
		
	}
	
	var i_total_revenue_recognized_ytd = 0;
	for(var i_revenue_index=0; i_revenue_index<a_recognized_revenue.length; i_revenue_index++)
	{
		i_total_revenue_recognized_ytd = parseFloat(i_total_revenue_recognized_ytd) + parseFloat(a_recognized_revenue[i_revenue_index]);
		
	}
	
	nlapiLogExecution('audit','f_revenue_amount_till_current_month:- '+f_revenue_amount_till_current_month,'i_total_revenue_recognized_ytd:- '+i_total_revenue_recognized_ytd)
	//var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month) - parseFloat(i_total_revenue_recognized_ytd);
	//var i_total_revenue_recognized_ytd = 0;
	var f_current_month_actual_revenue = parseFloat(f_revenue_amount_till_current_month);
	//f_current_month_actual_revenue_total_proj_level = parseFloat(f_current_month_actual_revenue_total_proj_level) + parseFloat(f_current_month_actual_revenue);
	if((parseInt(i_project_sow_value)==parseInt(f_tot_revenue_till_current_month))&& customer_lookup!= 'T')
	{
		f_current_month_actual_revenue=0;
	}
	f_current_month_actual_revenue = parseInt(f_current_month_actual_revenue);
	var json_to_process = f_current_month_actual_revenue+'##'+a_amount[f_total_prev_mnths];
	
	return json_to_process;
}
function format2(n) {
    n = parseFloat(n).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	n = n.toString().split('.');
	return n[0];
}

function getCurrency_Symbol(s_proj_currency)
{
	var s_currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'GBP': '£', // British Pound Sterling
    'INR': '₹', // Indian Rupee
	};
	
	if(s_currency_symbols[s_proj_currency]!==undefined) {
	    return s_currency_symbols[s_proj_currency];
	}
}

function getMonthsBreakup(d_startDate, d_endDate) {
	
	try
	{
		var dateBreakUp = [];
		var new_start_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth(), 1);
				
		var nxt_mnth_strt_date = new Date(d_startDate.getFullYear(), d_startDate
		        .getMonth()+1, 1);
		
		var endDate = new Date(nxt_mnth_strt_date - 1);
				
		//var endDate = nlapiAddDays(nlapiAddMonths(d_startDate, 1), -1);
		endDate = nlapiDateToString(endDate, 'date');
	
		dateBreakUp.push({
		    Start : nlapiDateToString(new_start_date, 'date'),
		    End : endDate
		});
	
		for (var i = 1;; i++) {
			var new_date = nlapiAddMonths(new_start_date, i);
	
			if (new_date > d_endDate) {
				break;
			}
			
			endDate = nlapiAddDays(nlapiAddMonths(new_date, 1), -1);
			endDate = nlapiDateToString(endDate, 'date');
					
			dateBreakUp.push({
			    Start : nlapiDateToString(new_date, 'date'),
			    End : endDate
			});
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','getMonthsBreakup','ERROR MESSAGE :- '+err);
	}

	return dateBreakUp;
}

function getMonthName(currentDate) {
	currentDate = nlapiStringToDate(currentDate)
	var monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
	        "SEP", "OCT", "NOV", "DEC" ];
	return monthNames[currentDate.getMonth()];
}

function MonthNames(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	return monthNames;
}

function yearData(yr) {
	var yr1 = Number(yr) + 1;
	
	var monthNames = [ "JAN_"+yr, "FEB_"+yr, "MAR_"+yr, "APR_"+yr, "MAY_"+yr, "JUN_"+yr, "JUL_"+yr, "AUG_"+yr,
	        "SEP_"+yr, "OCT_"+yr, "NOV_"+yr, "DEC_"+yr ];
	var month_nxt_yr = ["JAN_"+yr1, "FEB_"+yr1, "MAR_"+yr1, "APR_"+yr1, "MAY_"+yr1, "JUN_"+yr1, "JUL_"+yr1, "AUG_"+yr1,
	        "SEP_"+yr1, "OCT_"+yr1, "NOV_"+yr1, "DEC_"+yr1 ];
	monthNames = monthNames.concat(month_nxt_yr);
	
	for (var i = 0; i < monthNames.length; i++) {
		this[monthNames[i]] = {
		    Amount : 0,
			prcnt_allocated : 0,
			total_revenue : 0,
			recognized_revenue : 0,
			actual_revenue : 0,
			mnth_strt : 0,
			mnth_end : 0,
			prcent_complt : 0
		};
	}
}

function generate_previous_effrt_revenue(s_effrt_json,projectWiseRevenue_previous_effrt,a_revenue_recognized_for_project,i_year_project)
{
	if(s_effrt_json)
	{
		var i_practice_previous = 0;
		var f_total_revenue_for_tenure = 0;
		//for (var i_mnth_end_plan = 0; i_mnth_end_plan < a_get_mnth_end_effrt_plan.length; i_mnth_end_plan++)
		var s_entire_json_clubed = JSON.parse(s_effrt_json);
		for (var i_mnth_end_plan = 0; i_mnth_end_plan < s_entire_json_clubed.length; i_mnth_end_plan++)
		{
			var a_row_json_data = JSON.parse(JSON.stringify(s_entire_json_clubed[i_mnth_end_plan]));
			for (var i_row_json_index = 0; i_row_json_index < a_row_json_data.length; i_row_json_index++)
			{
				var i_practice = a_row_json_data[i_row_json_index].prac;
				var s_practice = a_row_json_data[i_row_json_index].prac_text;
				var i_sub_practice = a_row_json_data[i_row_json_index].subprac;
				var s_sub_practice = a_row_json_data[i_row_json_index].subprac_text;
				var i_role = a_row_json_data[i_row_json_index].role;
				var s_role = a_row_json_data[i_row_json_index].role_text;
				var i_level = a_row_json_data[i_row_json_index].level;
				var s_level = a_row_json_data[i_row_json_index].level_text;
				var i_location = a_row_json_data[i_row_json_index].loc;
				var s_location = a_row_json_data[i_row_json_index].loc_text;
				var f_revenue = a_row_json_data[i_row_json_index].cost;
				if (!f_revenue) 
					f_revenue = 0;
				
				var f_revenue_share = a_row_json_data[i_row_json_index].share;
				if (!f_revenue_share) 
					f_revenue_share = 0;
					
				var i_no_of_resources = a_row_json_data[i_row_json_index].allo;
				var s_mnth_strt_date = '1/31/2017';
				var s_mnth_end_date = '31/31/2017';
				var s_mnth = a_row_json_data[i_row_json_index].mnth;
				var s_year = a_row_json_data[i_row_json_index].year;
				
				var f_revenue_recognized = find_recognized_revenue_prev(a_revenue_recognized_for_project,i_practice,i_sub_practice,i_role,i_level,s_mnth,s_year,a_prev_subprac_searched_once);
				
				if (!f_revenue_recognized) 
					f_revenue_recognized = 0;
				
				//nlapiLogExecution('audit','f_revenue_recognized:- '+f_revenue_recognized);
				
				var s_month_name = s_mnth + '_' + s_year;
				
				var total_revenue = parseFloat(i_no_of_resources) * parseFloat(f_revenue);
				total_revenue = parseFloat(total_revenue).toFixed(2);
				
				if (i_practice_previous == 0) {
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
				
				if (i_practice_previous != a_row_json_data[i_row_json_index].prac_row) {
					f_total_revenue_for_tenure = 0;
					i_practice_previous = a_row_json_data[i_row_json_index].prac_row;
				}
			
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure) + parseFloat(total_revenue);
				f_total_revenue_for_tenure = parseFloat(f_total_revenue_for_tenure).toFixed(2);
				
				var i_prac_subPrac_role_level = i_practice + '_' + i_sub_practice + '_' + i_role + '_' + i_level + '_' + i_location;
				
				if (!projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level]) {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level] = {
						practice_name: s_practice,
						sub_prac_name: s_sub_practice,
						sub_practice: i_sub_practice,
						role_name: s_role,
						level_name: s_level,
						location_name: s_location,
						total_revenue_for_tenure: f_total_revenue_for_tenure,
						revenue_share: f_revenue_share,
						RevenueData: {}
					};
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice] = new yearData(i_year_project);
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
				else {
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].total_revenue_for_tenure = f_total_revenue_for_tenure;
					
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].Amount = total_revenue;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].prcnt_allocated = i_no_of_resources;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].recognized_revenue = f_revenue_recognized;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_strt = s_mnth_strt_date;
					projectWiseRevenue_previous_effrt[i_prac_subPrac_role_level].RevenueData[i_practice][s_month_name].mnth_end = s_mnth_end_date;
				}
			}
		}
	}
	
	//return projectWiseRevenue_previous_effrt;
}

function find_recognized_revenue_prev(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_prev_subprac_searched_once)
{
	var f_recognized_amount = 0;
	
	if(a_prev_subprac_searched_once.indexOf(subpractice) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice == a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{
								if(year == a_revenue_recognized_for_project[i_index_find].year_revenue_recognized)
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									a_prev_subprac_searched_once.push(subpractice);
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function find_recognized_revenue(a_revenue_recognized_for_project,practice,subpractice,role,level,month,year,a_subprac_searched_once,a_subprac_searched_once_month,a_subprac_searched_once_year)
{
	var f_recognized_amount = 0;
	
	if(func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year) < 0)
	{
		for(var i_index_find=0; i_index_find<a_revenue_recognized_for_project.length; i_index_find++)
		{				
			if(practice == a_revenue_recognized_for_project[i_index_find].practice_revenue_recognized)
			{
				if(subpractice ==  a_revenue_recognized_for_project[i_index_find].subpractice_revenue_recognized)
				{
					//if(role == a_revenue_recognized_for_project[i_index_find].role_revenue_recognized)
					{
						//if(level == a_revenue_recognized_for_project[i_index_find].level_revenue_recognized)
						{
							if(month == a_revenue_recognized_for_project[i_index_find].month_revenue_recognized)
							{	
								if(parseInt(year) == parseInt(a_revenue_recognized_for_project[i_index_find].year_revenue_recognized))
								{
									f_recognized_amount = a_revenue_recognized_for_project[i_index_find].amount_revenue_recognized;
									//nlapiLogExecution('audit','f_recognized_amount inside look for prev mnth amnt once pr sub prac:- ',f_recognized_amount);
									a_subprac_searched_once.push(
																{
																	'subpractice': subpractice,
																	'month': month,
																	'year': year
																});
									//a_subprac_searched_once_month.push(month);
									//a_subprac_searched_once_year.push(year);
									
									if(subpractice == 325)
									{
										nlapiLogExecution('audit','month:- '+month,'year:- '+year);
										nlapiLogExecution('audit','f_recognized_amount:- '+f_recognized_amount);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return f_recognized_amount;
	}
	else
	{
		return 0;
	}
}

function func_search_prac_processed_fr_mnth(a_subprac_searched_once,subpractice,month,year)
{
	var i_return_var = -1;
	for(var i_loop=0; i_loop<a_subprac_searched_once.length; i_loop++)
	{
		if(a_subprac_searched_once[i_loop].subpractice == subpractice)
		{
			if(a_subprac_searched_once[i_loop].month == month)
			{
				if(a_subprac_searched_once[i_loop].year == year)
				{
					i_return_var = i_loop;
					break;
				}
			}
		}
	}
	
	return i_return_var;
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
 }
 function getMonthCompleteIndex(month){
	var s_mont_indx = '';
	if(month == 'JAN')
		s_mont_indx = 1;
	if(month == 'FEB')
		s_mont_indx = 2;
	if(month == 'MAR')
		s_mont_indx = 3;
	if(month == 'APR')
		s_mont_indx = 4;
	if(month == 'MAY')
		s_mont_indx = 5;
	if(month == 'JUN')
		s_mont_indx = 6;
	if(month == 'JUL')
		s_mont_indx = 7;
	if(month == 'AUG')
		s_mont_indx = 8;
	if(month == 'SEP')
		s_mont_indx = 9;
	if(month == 'OCT')
		s_mont_indx = 10;
	if(month == 'NOV')
		s_mont_indx = 11;
	if(month == 'DEC')
		s_mont_indx = 12;
	
	return s_mont_indx;
}


function generate_total_cost(projectWiseRevenue,monthBreakUp,i_year_project,i_project_mnth,mode,sub_prac,s_currency_symbol_proj,s_currency_symbol_usd,i_year_project_end,i_prject_end_mnth)
{
	var css_file_url = "";

	var context = nlapiGetContext();
	if (context.getEnvironment() == "SANDBOX") {
		css_file_url = "https://system.sandbox.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	} else if (context.getEnvironment() == "PRODUCTION") {
		css_file_url = "https://system.na1.netsuite.com/core/media/media.nl?id=199133&c=3883006&h=64fa194295cd4ab64d58&mv=iq3kjr6g&_xt=.css&whence=";
	}

	var html = "";
	html += "<link href='" + css_file_url + "'  rel='stylesheet'>";

	html += "<table class='projection-table'>";

	// header
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "";
	html += "</td>";
	
	html += "<td width:15px>";
	html += "Practice";
	html += "</td>";
	
	html += "<td width:30px>";
	html += "Sub Practice";
	html += "</td>";
	
	html += "<td width=\"10%\">";
	html += "Role";
	html += "</td>";
	
	html += "<td width=\"10%\">";
	html += "Level";
	html += "</td>";
	
	html += "<td>";
	html += "Location";
	html += "</td>";
		
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	for (var j = 0; j < monthBreakUp.length; j++)
	{
		var months = monthBreakUp[j];
		var s_month_name = getMonthName(months.Start); // get month name
		var s_year = nlapiStringToDate(months.Start)
		s_year = s_year.getFullYear();
		s_month_name = s_month_name+'_'+s_year;
				
		html += "<td>";
		html += s_month_name;
		html += "</td>";
	}
		
	html += "<td>";
	html += "Total";
	html += "</td>";
			
	html += "</tr>";
	
	var a_sub_prac_already_displayed = new Array();
	var i_subpractice_id = 0;
	var a_amount = new Array();
	var a_recognized_revenue = new Array();
	var i_frst_row = 0;
	var i_diplay_frst_column = 0;
	var i_total_row_revenue = 0;
	var i_total_per_row = 0;
	var total_prcnt_aloocated = new Array();
	var b_first_line_copied = 0;
	var f_total_allocated_row = 0;
	
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			f_total_allocated_row = 0;
			for (var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData)
			{
				html += "<tr>";
				
				if(i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Effort View';
					html += "</td>";
				}
				else
				{
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}
				
				html += "<td class='label-name' width:15px>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
						
				html += "<td class='label-name' width:30px>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name' width=\"10%\">";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name' width=\"10%\">";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
				
				var i_index_mnth = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						//nlapiLogExecution('audit','currentMonthPos:- ',+currentMonthPos);
						if(b_first_line_copied == 1)
						{
							var f_allocated_prcnt = total_prcnt_aloocated[i_index_mnth];
							f_allocated_prcnt = parseFloat(f_allocated_prcnt) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
							total_prcnt_aloocated[i_index_mnth] = f_allocated_prcnt;
							i_index_mnth++;							
						}
						else
						{
							total_prcnt_aloocated.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated);
						}
						
						f_total_allocated_row = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated) + parseFloat(f_total_allocated_row);
						
						html += "<td class='monthly-amount'>";
						html += parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].prcnt_allocated).toFixed(2);
						html += "</td>";
					}
				}
			}
			
			b_first_line_copied = 1;
			
			html += "<td class='monthly-amount'>";
			html += parseFloat(f_total_allocated_row).toFixed(2);
			html += "</td>";
			
			html += "</tr>";
			
			var s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			var s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			
			i_diplay_frst_column = 1;
		}
	}
	i_diplay_frst_column = 0;
	
	html += "<tr>";
	html += "<td class='label-name'>";
	html += '<b>Sub Total';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_row_allocation = 0;
	for(var i_index_total_allocated=0; i_index_total_allocated<total_prcnt_aloocated.length; i_index_total_allocated++)
	{
		html += "<td class='monthly-amount'>";
		html += '<b>'+parseFloat(total_prcnt_aloocated[i_index_total_allocated]).toFixed(2);
		html += "</td>";
		
		f_total_row_allocation = parseFloat(f_total_row_allocation) + parseFloat(total_prcnt_aloocated[i_index_total_allocated]);
	}
	
	html += "<td class='monthly-amount'>";
	html += '<b>'+parseFloat(f_total_row_allocation).toFixed(2);
	html += "</td>";
		
	html += "</tr>";
	
	
	for ( var emp_internal_id in projectWiseRevenue) {
		
		for ( var project_internal_id in projectWiseRevenue[emp_internal_id].RevenueData) {
			
			if(projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
			{
				html += "<tr>";
				
				if (i_diplay_frst_column == 0)
				{
					html += "<td class='label-name'>";
					html += '<b>Cost View';
					html += "</td>";
				}
				else
				{
					html += "<td class='label-name'>";
					html += '<b>';
					html += "</td>";
				}
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].practice_name;
				html += "</td>";
						
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].sub_prac_name;
				html += "</td>";
			
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].role_name;
				html += "</td>";
					
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].level_name;
				html += "</td>";
				
				html += "<td class='label-name'>";
				html += projectWiseRevenue[emp_internal_id].location_name;
				html += "</td>";
					
				var i_sub_practice_internal_id = 0;
				var i_amt = 0;
				for (var month in projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id])
				{
					var a_MonthNames = MonthNames(i_year_project);
					var currentMonthPos = a_MonthNames.indexOf(month);
					var s_current_month = a_MonthNames[currentMonthPos];
					var s_current_mnth_yr = s_current_month.substr(4, 8);
					
					var i_nxt_yr_flg = 0;
					if(currentMonthPos >=12)
					{
						currentMonthPos = currentMonthPos - 12;
						i_nxt_yr_flg = 1;
					}
					
					var i_curr_yr_dis_flag = 0;
					if(parseInt(s_current_mnth_yr) < i_year_project_end && i_prject_end_mnth < currentMonthPos)
					{
						i_curr_yr_dis_flag = 1;
					}
					
					if (((i_project_mnth <= currentMonthPos && ((i_prject_end_mnth >= currentMonthPos || i_curr_yr_dis_flag == 1) && i_nxt_yr_flg == 0) && parseInt(s_current_mnth_yr) <= i_year_project) && parseInt(s_current_mnth_yr) <= i_year_project_end) || (i_prject_end_mnth >= currentMonthPos && parseInt(s_current_mnth_yr) <= i_year_project_end && i_nxt_yr_flg == 1))
					{
						if (mode == 2)
						{
							
							html += "<td class='monthly-amount'>";
							var total_revenue_format = parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount).toFixed(1);
							html += ''+s_currency_symbol_proj+' '+format2(total_revenue_format);
							
							if(i_frst_row == 0)
							{
								//nlapiLogExecution('audit','inside push recog amnt:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								a_amount.push(total_revenue_format);
								a_recognized_revenue.push(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
							}
							else
							{
								var amnt_mnth = a_amount[i_amt];
								amnt_mnth = parseFloat(amnt_mnth) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
								
								a_amount[i_amt] = amnt_mnth;
								
								var i_existing_recognised_amount = a_recognized_revenue[i_amt];
								//nlapiLogExecution('audit','month name:- '+month);
								//nlapiLogExecution('audit','index:- '+i_amt+'::existing rev:- '+i_existing_recognised_amount,'Rev REcg:- '+projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								i_existing_recognised_amount = parseFloat(i_existing_recognised_amount) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].recognized_revenue);
								
								a_recognized_revenue[i_amt] = i_existing_recognised_amount;
								
								i_amt++;
							}
							i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
							i_total_per_row = parseFloat(i_total_per_row) + parseFloat(projectWiseRevenue[emp_internal_id].RevenueData[project_internal_id][month].Amount);
						}
						
						html += "</td>";
					}
				}
				
				if (mode == 2)
				{
					html += "<td class='monthly-amount'>";
					//html += ''+s_currency_symbol_usd+' '+format2(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
					html += ''+s_currency_symbol_proj+' '+format2(i_total_per_row);
					html += "</td>";
					i_total_per_row = 0;
					//i_total_row_revenue = parseFloat(i_total_row_revenue) + parseFloat(projectWiseRevenue[emp_internal_id].total_revenue_for_tenure);
				}
				
				i_frst_row = 1;
	
				html += "</tr>";
				
				i_diplay_frst_column = 1;
				
				var f_revenue_share = projectWiseRevenue[emp_internal_id].revenue_share;
			}			
		}
	}
	
	var s_sub_prac_name = '';
	for (var emp_internal_id in projectWiseRevenue)
	{
		if (projectWiseRevenue[emp_internal_id].sub_practice == sub_prac)
		{
			s_sub_prac_name = projectWiseRevenue[emp_internal_id].sub_prac_name;
			s_practice_name = projectWiseRevenue[emp_internal_id].practice_name;
			//projectWiseRevenue[emp_internal_id].revenue_share_revised = i_total_row_revenue;
			//projectWiseRevenue[emp_internal_id].total_revenue_for_tenure = i_total_row_revenue;
		}
	}
	
	html += "</tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Sub total';
	html += "</td>";
				
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	if(flag_counter==0){
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		f_total_cost_overall_prac[i_amt] = 0;
		
	}
	}
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(a_amount[i_amt]);
		html += "</td>";
		f_total_cost_overall_prac[i_amt] = parseFloat(a_amount[i_amt]) + parseFloat(f_total_cost_overall_prac[i_amt]);
		flag_counter=1;
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+s_currency_symbol_proj+' '+format2(i_total_row_revenue);
	html += "</td>";
	
	html += "</tr>";
	
	// Percent row
	html += "</tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Monthly % Cost Completion';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var f_total_prcnt = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		if(!f_prcnt_revenue)
			f_prcnt_revenue = 0;
			
		f_total_prcnt = parseFloat(f_prcnt_revenue) + parseFloat(f_total_prcnt);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+parseFloat(f_prcnt_revenue).toFixed(1)+' %';
		html += "</td>";
	}
	
	html += "<td class='projected-amount'>";
	html += '<b>'+parseFloat(f_total_prcnt).toFixed(1)+' %';
	html += "</td>";
	
	html += "</tr>";
	
	//Total reveue recognized per month row
	html += "<tr>";
	
	html += "<td class='label-name'>";
	html += '<b>Revenue to be reconized/month';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '<b>'+s_practice_name;
	html += "</td>";
			
	html += "<td class='label-name'>";
	html += '<b>'+s_sub_prac_name;
	html += "</td>";

	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
		
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	html += "<td class='label-name'>";
	html += '';
	html += "</td>";
	
	var d_today_date = new Date();
	var i_current_month = d_today_date.getMonth();
	var i_current_year = d_today_date.getFullYear();
	
	var f_total_revenue = 0;
	var f_revenue_amount_till_current_month = 0;
	for (var i_amt = 0; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if (parseInt(i_year_project) != parseInt(i_current_year))
		{
			var i_total_project_tenure = 11 + parseInt(i_current_month);
			var f_total_prev_mnths = parseFloat(i_total_project_tenure) - parseFloat(i_project_mnth);
			f_total_prev_mnths = parseFloat(f_total_prev_mnths) + parseFloat(1);
		}
		else
		{
			var f_total_prev_mnths = parseFloat(i_current_month) - parseFloat(i_project_mnth);
		}
		
		if(i_amt <= f_total_prev_mnths)
		{
			//nlapiLogExecution('audit','f_revenue_amount:- '+f_revenue_amount,'a_amount.length:- '+a_amount.length);
			f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
		html += "</td>";
	}

	
	html += "</tr>";
	
	html += "</table>";
	
	html += "<table class='projection-table'>";
	
	html += "<tr class='header-row'>";
	
	html += "<td>";
	html += "";
	html += "</td>";
	
	html += "<td width:15px>";
	html += "Practice";
	html += "</td>";
	
	html += "<td width:30px>";
	html += "Sub Practice";
	html += "</td>";
	
	var today = new Date();
	var currentMonthName = getMonthName(nlapiDateToString(today));
	
	
	for (var i_amt = i_current_month+1; i_amt < a_amount.length; i_amt++)
	{
		var f_prcnt_revenue = parseFloat(a_amount[i_amt]) / parseFloat(i_total_row_revenue);
		f_prcnt_revenue = f_prcnt_revenue * 100;
		
		var f_revenue_amount = (parseFloat(f_prcnt_revenue) * parseFloat(f_revenue_share)) / 100;
		if(!f_revenue_amount)
			f_revenue_amount = 0;
		
		if(i_amt < i_current_month)
		{
			//nlapiLogExecution('audit','i_current_month:- '+i_current_month,'a_amount.length:- '+a_amount.length);
		//	f_revenue_amount_till_current_month = parseFloat(f_revenue_amount) + parseFloat(f_revenue_amount_till_current_month);
			if(!f_revenue_amount_till_current_month)
				f_revenue_amount_till_current_month = 0;
		}
		
		f_total_revenue = parseFloat(f_revenue_amount) + parseFloat(f_total_revenue);
		
		html += "<td class='projected-amount'>";
		html += '<b>'+s_currency_symbol_proj+' '+format2(f_revenue_amount);
		html += "</td>";
		
		//i_total_actual_revenue_recognized = parseFloat(i_total_actual_revenue_recognized) + parseFloat(f_revenue_amount);
	}
	
	
	html += "<td class='projected-amount'>";
	//html += '<b>'+s_currency_symbol_proj+' '+format2(i_total_actual_revenue_recognized);;
	html += "</td>";
	
	html += "</tr>";
	
	
	
	//if(i_total_revenue_recognized_ytd == 0)
		i_total_revenue_recognized_ytd = f_revenue_share;
	
	html += "<td class='monthly-amount'>";
	html += s_currency_symbol_proj+' '+format2(i_total_revenue_recognized_ytd);
	html += "</td>";
	
	html += "</tr>";
	
	html += "</table>";
	

	//a_recognized_revenue_total_proj_level = a_recognized_revenue;
	
	return f_total_cost_overall_prac;
}
function getResourceAllocations(d_start_date, d_end_date,proList)
{

      nlapiLogExecution('AUDIT', 'getResourceAllocation Parameters', d_start_date
              + ' - ' + d_end_date );
     
      var a_resource_allocations = new Array();

      // Get Resource allocations for this month
     // var  user =3169;//taking pro under hema
      var user = nlapiGetUser();
	  if(parseInt(user) == parseInt(41571))
			user = 3169;
     nlapiYieldScript();
     var a_practice_list = new Array();
     var pract_col = new Array();
     var filters = new Array();
      //
      filters[0]  = new nlobjSearchFilter('formuladate', null, 'notafter',d_end_date);
      filters[0].setFormula('{custeventbstartdate}');
      filters[1] = new nlobjSearchFilter('formuladate', null, 'notbefore',d_start_date);
      filters[1].setFormula('{custeventbenddate}');
      filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
      filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'anyof', 'TM');
      filters[4] = new nlobjSearchFilter('custentity_exclude_rev_forecast', 'job', 'is', 'F');
      filters[5] = new nlobjSearchFilter('project', null, 'anyof', proList);
    
      var columns = new Array();
      columns[0] = new nlobjSearchColumn('percentoftime', null, 'avg');
      columns[1] = new nlobjSearchColumn('custeventbstartdate', null, 'group');
      columns[2] = new nlobjSearchColumn('custeventbenddate', null, 'group');
      columns[3] = new nlobjSearchColumn('resource', null, 'group');
      columns[4] = new nlobjSearchColumn('project', null, 'group');
      columns[5] = new nlobjSearchColumn('custevent3', null, 'avg');
      columns[6] = new nlobjSearchColumn('custevent_monthly_rate', null, 'avg');
      columns[7] = new nlobjSearchColumn('custeventrbillable', null, 'group');
      columns[8] = new nlobjSearchColumn('departmentnohierarchy', 'employee',
    'group');
      columns[9] = new nlobjSearchColumn('subsidiary', 'employee', 'group');
      columns[10] = new nlobjSearchColumn('customer', 'job', 'group');
      columns[11] = new nlobjSearchColumn('custentity_project_currency', 'job',
    'group');
      columns[12] = new nlobjSearchColumn('custentity_vertical', 'job', 'group');
      columns[13] = new nlobjSearchColumn('custentity_t_and_m_monthly', 'job',
    'group');
      columns[14] = new nlobjSearchColumn('custevent_monthly_rate', null, 'group');
      
      columns[15] = new nlobjSearchColumn('formulatext', null, 'group');
      columns[15]
              .setFormula('CASE WHEN INSTR({employee.department} , \' : \', 1) > 0 THEN SUBSTR({employee.department}, 1 , INSTR({employee.department} , \' : \', 1)) ELSE {employee.department} END');
      columns[16] = new nlobjSearchColumn('custentity_endcustomer', 'job',
              'group');
      columns[17] = new nlobjSearchColumn('internalid', null, 'count');
     
      try {
    	  nlapiYieldScript();
            var search_results = searchRecord('resourceallocation', null, filters,
                    columns);
    	//  nlapiYieldScript();
    	 /* var search_results = searchRecord('resourceallocation', null, filters,
                  columns);*/
      } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
      }
      nlapiYieldScript();
      if (search_results != null && search_results.length > 0) {
    	//  nlapiYieldScript();
            for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
                  var i_project_id = search_results[i_search_indx]
                          .getValue(columns[4]);
                  var s_project_name = search_results[i_search_indx]
                          .getText(columns[4]);
                  
                  var s_project_strdte= search_results[i_search_indx]
                  .getText(columns[1]);
                  var s_project_enddate= search_results[i_search_indx]
                  .getText(columns[2]);
                  
                  var is_resource_billable = search_results[i_search_indx]
                          .getValue(columns[7]);
                  var stRate = search_results[i_search_indx].getValue(columns[5]);
                  stRate = stRate ? parseFloat(stRate) : 0;
                  var i_employee = search_results[i_search_indx].getValue(columns[3]);
                  var s_employee_name = search_results[i_search_indx]
                          .getText(columns[3]);
                  var i_percent_of_time = search_results[i_search_indx]
                          .getValue(columns[0]);
                  var i_department_id = search_results[i_search_indx]
                          .getValue(columns[8]);
                  var s_department = search_results[i_search_indx]
                          .getValue(columns[8]);
                  var i_subsidiary = search_results[i_search_indx]
                          .getValue(columns[9]);
            //    var i_working_days = search_results[i_search_indx].getValue(columns[8]);
                  var allocationStartDate = search_results[i_search_indx].getValue(columns[1]);
                  var allocationEndDate = search_results[i_search_indx].getValue(columns[2]);
                  
                  //var i_working_days = parseInt(calcBusinessDays(d_start_date, d_end_date));;
                  
                  var i_customer_id = search_results[i_search_indx]
                          .getValue(columns[10]);
                  var s_customer = search_results[i_search_indx].getText(columns[10]);
                  var i_count = search_results[i_search_indx].getValue(columns[17]);
                  var i_currency = search_results[i_search_indx].getText(columns[11]);
                  var i_vertical_id = search_results[i_search_indx]
                          .getValue(columns[12]);
                  var s_vertical = search_results[i_search_indx].getText(columns[12]);
                  var is_T_and_M_monthly = search_results[i_search_indx]
                          .getValue(columns[13]);
                  var f_monthly_rate = search_results[i_search_indx]
                          .getValue(columns[14]);
                  f_monthly_rate = f_monthly_rate ? parseFloat(f_monthly_rate) : 0;
                  var s_parent_department = search_results[i_search_indx]
                          .getValue(columns[15]);
                  var s_end_customer = search_results[i_search_indx]
                          .getText(columns[16]);
                  
                  // days calculation
                  
                  
                  var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
                  var d_allocationEndDate = nlapiStringToDate(allocationEndDate);
                  var d_provisionStartDate = d_start_date;
                  var d_provisionEndDate = d_end_date;
                  var provisionStartDate = nlapiDateToString(d_start_date);
                  var provisionEndDate = nlapiDateToString(d_end_date);

                  var provisionMonthStartDate = nlapiStringToDate((d_provisionStartDate
                          .getMonth() + 1)
                          + '/1/' + d_provisionStartDate.getFullYear());
                  var nextProvisionMonthStartDate = nlapiAddMonths(
                          provisionMonthStartDate, 1);
                  var provisionMonthEndDate = nlapiAddDays(
                          nextProvisionMonthStartDate, -1);
                  // nlapiLogExecution('debug', 'provisionMonthStartDate',
                  // provisionMonthStartDate);
                  // nlapiLogExecution('debug', 'provisionMonthEndDate',
                  // provisionMonthEndDate);

                  var startDate = d_allocationStartDate > d_provisionStartDate ? allocationStartDate
                          : provisionStartDate;
                  var endDate = d_allocationEndDate < d_provisionEndDate ? allocationEndDate
                          : provisionEndDate;
                  
                  // days calculation
                  var workingDays = parseFloat(getWorkingDays(
                              startDate, endDate));
                  var holidayDetailsInMonth = get_holidays(startDate,
                              endDate, i_employee, i_project_id, i_customer_id);
                  var holidayCountInMonth = holidayDetailsInMonth.length;

                  
                  
                  a_resource_allocations[i_search_indx] = {
                      'project_id' : i_project_id,
                      'project_name' : s_project_name,
                     ' is_billable' : is_resource_billable,
                     ' st_rate' : stRate,
                      'employee' : i_employee,
                      'employee_name' : s_employee_name,
                      'percentoftime' : i_percent_of_time,
                      'department_id' : i_department_id,
                      'department' : s_department,
                      'holidays' : holidayCountInMonth,
                      'workingdays' : workingDays,
                      'customer_id' : i_customer_id,
                      'customer' : s_customer,
                      'count' : i_count,
                      'currency' : i_currency,
                      'vertical_id' : i_vertical_id,
                      'vertical' : s_vertical,
                      'monthly_billing' : is_T_and_M_monthly,
                      'monthly_rate' : f_monthly_rate,
                      'end_date':endDate,
                      'start_date':startDate,
                      'parent_department' : s_parent_department,
                      'end_customer' : s_end_customer == '- None -' ? ''
                              : s_end_customer
                  };
            }
      }  else {
            a_resource_allocations = null;
      }

      return a_resource_allocations;
}
function offsetcalculation(proj_id,proj_int_id,pro_currency,inr_to_usd,gbp_to_usd,dep)
{
		var invoice_total = 0;
		var je_total = 0;
		var offset_value = 0;
		var currency_rate = 1;
		var proj_subsidiary = nlapiLookupField('job',proj_int_id,'subsidiary');
		if(pro_currency != 'USD')
		{
			if(pro_currency == 'INR')
			{
				currency_rate = inr_to_usd;
			}
			else if(pro_currency =='GBP')
			{
				currency_rate = gbp_to_usd;
			}
							
		}
					
			//Invoice Search raised for current month
	/*var invoiceSearch = nlapiSearchRecord("invoice",null,
											[["datecreated","within","12/01/2018 12:00 am","12/31/2018 11:59 pm"], "AND", 
											["account","anyof","644"], "AND", 
											["name","anyof",proj_int_id], "AND", 
											["type","anyof","CustInvc"]],  ["department","anyof","485"]
											[new nlobjSearchColumn("amount",null,"SUM")]);*/
	var invoiceSearch = nlapiSearchRecord("invoice",null,
											[["trandate","within","thismonth"],"AND", 
											 ["account","anyof","644"],"AND", 
											 ["type","anyof","CustInvc"],"AND", 
											 ["name","anyof",proj_int_id]], 
											[new nlobjSearchColumn("transactionnumber",null,"GROUP"), 
											 new nlobjSearchColumn("department",null,"GROUP"), 
											 new nlobjSearchColumn("amount",null,"SUM"), 
											 new nlobjSearchColumn("trandate",null,"GROUP").setSort(false)]);
	//Journals Search accounted for previous month
	/*var journalentrySearch = nlapiSearchRecord("journalentry",null,
							[["datecreated","within","11/01/2018 12:00 am","11/30/2018 11:59 pm"],"AND",
							["account","anyof","647"], "AND", 
							["custcol_project_entity_id","is","KPCO03528"], "AND", 
							["type","anyof","Journal"], "AND", 
							["posting","is","T"],"AND", 
							["mainline","is","T"]],
							[new nlobjSearchColumn("amount",null,"sum")]);*/
	if(proj_subsidiary == 8 || proj_subsidiary == 9)
	{
		var journalentrySearch = nlapiSearchRecord("journalentry",null,
							[["trandate","within","thismonth"], "AND", 
							["account","anyof","647","773"], "AND", 
							["type","anyof","Journal"],"AND", 
							["mainline","is","T"],"AND", 
							["posting","is","T"],"AND", 
							["custcol_project_entity_id","is",proj_id],"AND",
							["department","anyof",dep]], 
							[new nlobjSearchColumn("amount",null,"SUM")]);
	}
	else
	{
		var journalentrySearch = nlapiSearchRecord("journalentry",null,
							[["trandate","within","thismonth"], "AND", 
							["account","anyof","647"], "AND", 
							["type","anyof","Journal"],"AND", 
							["mainline","is","T"],"AND", 
							["posting","is","T"],"AND", 
							["custcol_project_entity_id","is",proj_id],"AND",
							["department","anyof",dep]], 
							[new nlobjSearchColumn("amount",null,"SUM")]);
	}
								
	if(invoiceSearch)
	{
		invoice_total = invoiceSearch[0].getValue("amount",null,"sum");
		
	}
	if(journalentrySearch)
	{
		je_total = journalentrySearch[0].getValue("amount",null,"sum");
		
	}
	offset_value = parseFloat(invoice_total) + parseFloat(je_total);

return offset_value;	
}