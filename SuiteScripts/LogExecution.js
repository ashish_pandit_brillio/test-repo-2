/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 Jan 2015     Amol
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function workflowAction() {
	nlapiLogExecution('AUDIT', 'Log', nlapiGetRecordId());
}
