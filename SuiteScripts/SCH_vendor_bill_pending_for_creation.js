function bills_pending(type)
{
	try
	{
		var bill_data_arr=[];
		var sr_no=0;
		var flag=0;
		var i_context = nlapiGetContext();
		var stdate = i_context.getSetting('SCRIPT', 'custscript_stdate');
		var enddate = i_context.getSetting('SCRIPT', 'custscript_end_date_bill');
		//var stdate='7/1/2017';
		//var enddate='7/31/2017';
		nlapiLogExecution('DEBUG','Stdate',stdate);
		nlapiLogExecution('DEBUG','Stdate',enddate);
		stdate=nlapiStringToDate(stdate);
		enddate=nlapiStringToDate(enddate);
		var emp_filter=[];
		emp_filter[0]=new nlobjSearchFilter('custentity_implementationteam',null,'is','F');
		emp_filter[1]=new nlobjSearchFilter('custentity_employee_inactive',null,'is','F');
		emp_filter[2]=new nlobjSearchFilter('isinactive',null,'is','F');
		emp_filter[3]=new nlobjSearchFilter('custentity_persontype',null,'anyof','1');
		emp_filter[4]=new nlobjSearchFilter('custentity_employeetype',null,'anyof','6');	
	//	emp_filter[5]=new nlobjSearchFilter('custentity_fusion_empid',null,'is','115232');	
		var emp_search=searchRecord('employee',null,emp_filter,[new nlobjSearchColumn('internalid'),
									new nlobjSearchColumn('custentity_fusion_empid'),
									new nlobjSearchColumn('entityid'),new nlobjSearchColumn('location')
								]);
		nlapiLogExecution('DEBUG','EMPLIST',emp_search.length);
		if(emp_search)
		{
			
			for(var emplen=0;emplen< emp_search.length;emplen++)
			{
				var empid=emp_search[emplen].getValue('custentity_fusion_empid');
				var emp_int_id=emp_search[emplen].getValue('internalid');
							
				
				var ts_filter=[];
				//ts_filter[0]=new nlobjSearchFilter('type',null,'anyof','VendBill');
				ts_filter[0]=new nlobjSearchFilter('custbody_billfrom',null,'onorafter',stdate);
				ts_filter[1]=new nlobjSearchFilter('custbody_billto',null,'onorbefore',enddate);
				ts_filter[2]=new nlobjSearchFilter('custcol_employee_entity_id',null,'is',empid);
				var bil_search=searchRecord('vendorbill',null,ts_filter,[new nlobjSearchColumn('transactionnumber')]);	
			//	nlapiLogExecution('DEBUG','length',ts_search.length);
				if(_logValidation(bil_search))
				{
					
					for(var i=0;i<bil_search.length;i++)
					{
						var bill_id=bil_search[i].getValue('transactionnumber');
						nlapiLogExecution('DEBUG','BILL-ID-'+bill_id,'empid'+empid);						
					}	
				}
				else{
				flag++;
				nlapiLogExecution('Debug','empid',empid);
					var time_duration=0;
					var holiday_dura=0;
					var total_leaves=0;
					var total_ot_dura=0;
					var br=0;
					var sub=0;
					var sub_rate_list={};
					var sub_rate_arr=[];
				
					var employee=emp_search[emplen].getValue('entityid');
					var location=emp_search[emplen].getText('location');
					var sub_tier_filt=[];
					sub_tier_filt[0]=new nlobjSearchFilter('custrecord_stvd_contractor',null,'anyof',emp_int_id);
					sub_tier_filt[1]=new nlobjSearchFilter('isinactive',null,'is','F');
					sub_tier_filt[2]=new nlobjSearchFilter('custrecord_stvd_end_date',null,'notonorbefore',stdate);
					//sub_tier_filt[3]=new nlobjSearchFilter('custrecord_stvd_start_date',null,'notonorafter',enddate);
					var sub_tier_search=searchRecord('customrecord_subtier_vendor_data',null,sub_tier_filt,
								[new nlobjSearchColumn('custrecord_stvd_st_pay_rate'),
								new nlobjSearchColumn('custrecord_stvd_ot_pay_rate'),
								new nlobjSearchColumn('custrecord_stvd_rate_type'),
								new nlobjSearchColumn('custrecord_stvd_start_date'),
								new nlobjSearchColumn('custrecord_stvd_end_date'),
								new nlobjSearchColumn('custrecord_subtier_currency'),
								new nlobjSearchColumn('custrecord_stvd_subsidiary')]);
					if(sub_tier_search)
					{
						for(var l=0;l<sub_tier_search.length;l++)
						{
					
							var sub_stdate=sub_tier_search[l].getValue('custrecord_stvd_start_date');
							var sub_enddate=sub_tier_search[l].getValue('custrecord_stvd_end_date');
							var strate=sub_tier_search[l].getValue('custrecord_stvd_st_pay_rate');
							var otrate=sub_tier_search[l].getValue('custrecord_stvd_ot_pay_rate');
							var rate_type=sub_tier_search[l].getText('custrecord_stvd_rate_type');
							var subsidiary=sub_tier_search[l].getText('custrecord_stvd_subsidiary');
							var currency=sub_tier_search[l].getValue('custrecord_subtier_currency');
					
							sub_rate_list={
							startdate: sub_stdate,
							enddate: sub_enddate,
							strate : strate,
							otrate: otrate,			
							};
							sub_rate_arr.push(sub_rate_list);	
						}
						
					}
					var time_fil=[];
					var id_list=[];
					time_fil[0]=new nlobjSearchFilter('employee',null,'anyof',emp_int_id);
					time_fil[1]=new nlobjSearchFilter('date',null,'within',[stdate,enddate]);
					time_fil[2]=new nlobjSearchFilter('approvalstatus',null,'anyof','3');
					var time_search=nlapiSearchRecord('timebill',null,time_fil,[new nlobjSearchColumn('employee'),
									new nlobjSearchColumn('customer').setSort(true),
									new nlobjSearchColumn('item'),
									new nlobjSearchColumn('internalid'),
									new nlobjSearchColumn('hours'),
									new nlobjSearchColumn('date'),
									//new nlobjSearchColumn('enddate','timesheet'),
									new nlobjSearchColumn('employee')
									]);
					if(time_search)
					//nlapiLogExecution('DEBUG','TIME',time_search.length);
					{
						var project_list=[];
						var project_name_arr=[];
						for(var n=0;n<time_search.length;n++)
						{
							var item=time_search[n].getValue('item');
							var project=time_search[n].getValue('customer');	
							var duration=time_search[n].getValue('hours');
							if(n==0){
							var date=time_search[n].getValue('date');
							}
							if(n<time_search.length-1){
							var ts_end_date=time_search[n].getValue('date');
							}
							nlapiLogExecution('DEBUG','item',item);
							var id=time_search[n].getValue('internalid');
							var cont=time_search[n].getValue('employee');
							id_list.push(id);
							if(project_list.indexOf(project)>=0)
							{
								if(item=='ST' || item=='FP' || item== 'Bench Project' || item=='Internal Projects')
								{
									time_duration=parseFloat(time_duration)+parseFloat(duration);
								}
								else if( item=='OT')
								{
									total_ot_dura=parseFloat(total_ot_dura)+parseFloat(duration);
								}
								else if(item=='Holiday')
								{
									holiday_dura=parseFloat(holiday_dura)+parseFloat(duration);
								}
								else{
									if(item=='Leave')
									{
										total_leaves=parseFloat(total_leaves)+parseFloat(duration);
									}
								}
								br++;
								var project_name=time_search[n].getText('customer');
							
							}
							else
							{
								if(br>0)
								{
							
									for(var ind=0;ind< sub_rate_arr.length;ind++)
									{
										bill_data_arr[sr_no] = {
										'employee': employee,
										'location' :location,
										'subsidiary':subsidiary,
										'project_name': project_name,
										'st_dura':time_duration,
										'total_leaves':total_leaves,
										'holiday_dura' :holiday_dura,
										'total_ot_dura' :total_ot_dura,
										'timesheet_date':date,
										'timesheet_enddate' :ts_end_date,
										'st_date':sub_rate_arr[ind].startdate,
										'end_date':sub_rate_arr[ind].enddate,
										'strate':sub_rate_arr[ind].strate ,
										'ot_rate':sub_rate_arr[ind].otrate,
										'rate_type':rate_type
										};
										sr_no++;
								
									
									}
									br=0;
								}
							
								total_leaves=0;
								total_ot_dura=0;
								holiday_dura=0;
								time_duration=0;
						
								if(item=='ST' || item=='FP')
								{
									time_duration=parseFloat(time_duration)+parseFloat(duration);
								}
								else if( item=='OT')
								{
									total_ot_dura=parseFloat(total_ot_dura)+parseFloat(duration);
								}
								else if(item=='Holiday')
								{
									holiday_dura=parseFloat(holiday_dura)+parseFloat(duration);
								}
								else{
									if(item=='Leave')
									{
										total_leaves=parseFloat(total_leaves)+parseFloat(duration);
									}
								}
						
							
							}
							if(project_list.indexOf(project)<0)
							{
						
								project_list.push(project);
								project_name_arr.push(project_name);
							}
						
						// nlapiLogExecution('DebUG','TimePEriod',time_duration);
					   
						
						}
						for(var ind=0;ind< sub_rate_arr.length;ind++)
						{
							bill_data_arr[sr_no] = {
								'employee': employee,
								'location' :location,
								'subsidiary':subsidiary,
								'project_name': project_name,
								'st_dura':time_duration,
								'total_leaves':total_leaves,
								'holiday_dura' :holiday_dura,
								'total_ot_dura' :total_ot_dura,
								'timesheet_date':date,
								'timesheet_enddate':ts_end_date,
								'st_date':sub_rate_arr[ind].startdate,
								'end_date':sub_rate_arr[ind].enddate,
								'strate':sub_rate_arr[ind].strate ,
								'ot_rate':sub_rate_arr[ind].otrate,
								'rate_type':rate_type
								//'ot_rate':JSON.stringify(sub_rate_arr)
								};
								sr_no++;
						}
				
					
			
					//	nlapiLogExecution('DEBUG','PROJECT',project_list.length);
				
					}	
				
				}
				
			
			}
			nlapiLogExecution('DEBUG','flag',flag);
			nlapiLogExecution('DEBUG','DAta1',bill_data_arr.length);
			down_excel_function(bill_data_arr); 

		
		}
	}
	catch(err)
	{
	
		
		nlapiLogExecution('ERROR','ERROR MESSAGE:-- ',err);
	}
}


function daysInMonth(month,year) {
	month = month.trim();
	
	if(month == 'Jan' || month == 'January' || month == 'JAN' || month == 'JANUARY')
	{	  	
		month = 1;		
	}	
	else if(month == 'Feb' || month == 'February' || month == 'FEB' || month == 'FEBRUARY')
	{
		month = 2;	
	}		
	else if(month == 'Mar' || month == 'March' || month == 'MAR' || month == 'MARCH')
	{	  
		month = 3;
	}	
	else if(month == 'Apr' || month == 'April' || month == 'APR' || month == 'APRIL')
	{
		month = 4;
	}	
	else if(month == 'May' || month == 'MAY')
	{	  
		month = 5;
	}	  
	else if(month == 'June' || month == 'Jun' || month == 'JUN' || month == 'JUNE')
	{	 
		month = 6;
	}	
	else if(month == 'Jul' || month == 'July' || month == 'JUL' || month == 'JULY')
	{	  
		month = 7;
	}	
	else if(month == 'August' || month == 'Aug' || month == 'AUG' || month == 'AUGUST')
	{	  
		month = 8;
	}  
	else if(month == 'Sep' || month == 'September' || month == 'SEP' || month == 'SEPTEMBER')
	{  	
		month = 9;
	}	
	else if(month == 'Oct' || month == 'October' || month == 'OCT' || month == 'OCTOBER')
	{	 	
		month = 10;
	}	
	else if(month == 'Nov' || month == 'November' || month == 'NOV' || month == 'NOVEMBER')
	{	  
		month = 11;
	}	
	else if(month == 'Dec' || month == 'December' || month == 'DEC' || month == 'DECEMBER')
	{	 
		month = 12;
	}
	
	year = Number(year).toFixed(0);
    return new Date(year, month, 0).getDate();
}

function get_todays_date()
{
	var today;
	
	var date1 = new Date();
	
	var day = date1.getDate();
	
	var month = date1.getMonth()+1;
	
	var year_sd =date1.getYear();
	
	var year=date1.getFullYear();
	
	var date_format = checkDateFormat();
	
	if (date_format == 'YYYY-MM-DD')
	{
		today = year + '-' + month + '-' + day;
	}
	if (date_format == 'DD/MM/YYYY')
	{
		today = day + '/' + month + '/' + year;
	}
	if (date_format == 'MM/DD/YYYY')
	{
		today = month + '/' + day + '/' + year;
	}
	return today;
}


function get_current_month_start_date(i_month,i_year)
{
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}


function get_current_month_end_date(i_month,i_year)
{
	var d_start_date = '';
	var d_end_date = '';
	
	var date_format = checkDateFormat();
	 	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }	 
	
		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}

function checkDateFormat()
{
    var context = nlapiGetContext();

	var dateFormatPref = context.getPreference('dateformat');

    return dateFormatPref;
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}

function getWeekend(startDate, endDate) //
{
	var i_no_of_sat = 0;
	var i_no_of_sun = 0;
	
	//nlapiLogExecution('DEBUG', 'Check Date', 'startDate : ' + startDate);
	//nlapiLogExecution('DEBUG', 'Check Date', 'endDate : ' + endDate);
	
	var date_format = checkDateFormat();
	
	var startDate_1 = startDate
	var endDate_1 = endDate
	
	startDate = nlapiStringToDate(startDate);
	endDate = nlapiStringToDate(endDate);
	
	//nlapiLogExecution('DEBUG', 'Check Date', '2 startDate : ' + startDate);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 endDate : ' + endDate);
	
	var i_count_day = startDate.getDate();
	
	var i_count_last_day = endDate.getDate();
	
	i_month = startDate.getMonth() + 1;
	
	i_year = startDate.getFullYear();
	
	//nlapiLogExecution('DEBUG', 'Check Date', '2 startDate_1 : ' + startDate_1);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 endDate_1 : ' + endDate_1);
	
	var d_f = new Date();
	var getTot = getDatediffIndays(startDate_1, endDate_1); //Get total days in a month
	var sat = new Array(); //Declaring array for inserting Saturdays
	var sun = new Array(); //Declaring array for inserting Sundays
	
	for (var i = i_count_day; i <= i_count_last_day; i++) //
	{
		//looping through days in month
		if (date_format == 'YYYY-MM-DD') //
		{
			var newDate = i_year + '-' + i_month + '-' + i;
		}
		if (date_format == 'DD/MM/YYYY') //
		{
			var newDate = i + '/' + i_month + '/' + i_year;
		}
		if (date_format == 'MM/DD/YYYY') //
		{
			var newDate = i_month + '/' + i + '/' + i_year;
		}
		
		newDate = nlapiStringToDate(newDate);
		
		if (newDate.getDay() == 0) //
		{ //if Sunday
			sat.push(i);
			i_no_of_sat++;
		}
		if (newDate.getDay() == 6) //
		{ //if Saturday
			sun.push(i);
			i_no_of_sun++;
		}
	}
	
	var i_total_days_sat_sun = parseInt(i_no_of_sat) + parseInt(i_no_of_sun);
	//nlapiLogExecution('DEBUG', 'Check Date', '2 i_total_days_sat_sun : ' + i_total_days_sat_sun);
	
	return i_total_days_sat_sun;
}

function getDatediffIndays(startDate, endDate)
{
    var one_day=1000*60*60*24;
    var fromDate = startDate;
    var toDate=endDate

	var date1 = nlapiStringToDate(fromDate);

    var date2 = nlapiStringToDate(toDate);

    var date3=Math.round((date2-date1)/one_day);

    return (date3+1);
}

function get_previous_month(i_month)
{
	if (_logValidation(i_month)) 
 	{
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
	  		i_month = 'December';				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	i_month = 'January';
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     i_month = 'February';
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	i_month = 'March';
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		  i_month = 'April';
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 i_month = 'May';	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		i_month = 'June';
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 i_month = 'July';
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	      i_month = 'August';
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
			i_month = 'September';	
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		 i_month = 'October';
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		 i_month = 'November';
			
	  }	
 }//Month & Year
	
	return i_month;
}

function get_previous_month_start_date(i_month,i_year)
{
	i_month = i_month.toString();
	i_year = i_year.toString();
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_start_date;
}

function get_previous_month_end_date(i_month,i_year)
{
	i_month = i_month.toString();
	i_year = i_year.toString();
   var d_start_date = '';
   var d_end_date = '';
	
   var date_format = checkDateFormat();	 
	
 if (_logValidation(i_month) && _logValidation(i_year)) 
 {
 	  if(i_month == 'Jan' || i_month == 'January' || i_month == 'JAN' || i_month == 'JANUARY')
	  {	  	
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 1 + '-' + 1;		
			d_end_date = i_year + '-' + 1 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 31 + '/' + 1 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 1 + '/' + 1 + '/' + i_year;
			d_end_date = 1 + '/' + 31 + '/' + i_year;
		 }	 
				
	  }	
	  else if(i_month == 'Feb' || i_month == 'February' || i_month == 'FEB' || i_month == 'FEBRUARY')
	  {
	  	if((i_year%2) == 0)
		{
			i_day = 29;
		}
		else
		{
			i_day = 28;
		}
			
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 2 + '-' + 1;		
			d_end_date = i_year + '-' + 2 + '-' + i_day;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 2 + '/' + i_year;
			d_end_date = i_day + '/' + 2 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 2 + '/' + 1 + '/' + i_year;
			d_end_date = 2 + '/' + i_day + '/' + i_year;
		 }	 
			
	  }		
	  else if(i_month == 'Mar' || i_month == 'March' || i_month == 'MAR' || i_month == 'MARCH')
	  {	  
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 3 + '-' + 1;		
			d_end_date = i_year + '-' + 3 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 3 + '/' + i_year;
			d_end_date = 31 + '/' + 3 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 3 + '/' + 1 + '/' + i_year;
			d_end_date = 3 + '/' + 31 + '/' + i_year;
		 }	 
	
	  }	
	  else if(i_month == 'Apr' || i_month == 'April' || i_month == 'APR' || i_month == 'APRIL')
	  {
	  	if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 4 + '-' + 1;		
			d_end_date = i_year + '-' + 4 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 4 + '/' + i_year;
			d_end_date = 30 + '/' + 4 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 4 + '/' + 1 + '/' + i_year;
			d_end_date = 4 + '/' + 30 + '/' + i_year;
	     }	 
	 
	
	  }	
	  else if(i_month == 'May' || i_month == 'MAY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 5 + '-' + 1;		
			d_end_date = i_year + '-' + 5 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 5 + '/' + i_year;
			d_end_date = 31 + '/' + 5 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 5 + '/' + 1 + '/' + i_year;
			d_end_date = 5 + '/' + 31 + '/' + i_year;
	     }	 
	
	  }	  
	  else if(i_month == 'June' || i_month == 'Jun' || i_month == 'JUN' || i_month == 'JUNE')
	  {	 
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 6 + '-' + 1;		
			d_end_date = i_year + '-' + 6 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 6 + '/' + i_year;
			d_end_date = 30 + '/' + 6 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 6 + '/' + 1 + '/' + i_year;
			d_end_date = 6 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Jul' || i_month == 'July' || i_month == 'JUL' || i_month == 'JULY')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 7 + '-' + 1;		
			d_end_date = i_year + '-' + 7 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 7 + '/' + i_year;
			d_end_date = 31 + '/' + 7 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 7 + '/' + 1 + '/' + i_year;
			d_end_date = 7 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }	
	  else if(i_month == 'August' || i_month == 'Aug' || i_month == 'AUG' || i_month == 'AUGUST')
	  {	  
		 if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 8 + '-' + 1;		
			d_end_date = i_year + '-' + 8 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 8 + '/' + i_year;
			d_end_date = 31 + '/' + 8 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 8 + '/' + 1 + '/' + i_year;
			d_end_date = 8 + '/' + 31 + '/' + i_year;
	     }	 
		
	  }  
	  else if(i_month == 'Sep' || i_month == 'September' || i_month == 'SEP' || i_month == 'SEPTEMBER')
	  {  	
	     if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 9 + '-' + 1;		
			d_end_date = i_year + '-' + 9 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 9 + '/' + i_year;
			d_end_date = 30 + '/' + 9 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 9 + '/' + 1 + '/' + i_year;
			d_end_date = 9 + '/' + 30 + '/' + i_year;
	     }	 
	
	  }	
	  else if(i_month == 'Oct' || i_month == 'October' || i_month == 'OCT' || i_month == 'OCTOBER')
	  {	 	
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 10 + '-' + 1;		
			d_end_date = i_year + '-' + 10 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 10 + '/' + i_year;
			d_end_date = 31 + '/' + 10 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 10 + '/' + 1 + '/' + i_year;
			d_end_date = 10 + '/' + 31 + '/' + i_year;
	     }		
	  }	
	  else if(i_month == 'Nov' || i_month == 'November' || i_month == 'NOV' || i_month == 'NOVEMBER')
	  {	  
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 11 + '-' + 1;		
			d_end_date = i_year + '-' + 11 + '-' + 30;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 11 + '/' + i_year;
			d_end_date = 30 + '/' + 11 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 11 + '/' + 1 + '/' + i_year;
			d_end_date = 11 + '/' + 30 + '/' + i_year;
	     }	 
			
	  }	
	  else if(i_month == 'Dec' || i_month == 'December' || i_month == 'DEC' || i_month == 'DECEMBER')
	  {	 
		if (date_format == 'YYYY-MM-DD')
		 {
		 	d_start_date = i_year + '-' + 12 + '-' + 1;		
			d_end_date = i_year + '-' + 12 + '-' + 31;		
		 } 
		 if (date_format == 'DD/MM/YYYY') 
		 {
		  	d_start_date = 1 + '/' + 12 + '/' + i_year;
			d_end_date = 31 + '/' + 12 + '/' + i_year;
		 }
		 if (date_format == 'MM/DD/YYYY')
		 {
		  	d_start_date = 12 + '/' + 1 + '/' + i_year;
			d_end_date = 12 + '/' + 31 + '/' + i_year;
	     }	 
			
	  }	
 }//Month & Year
 return d_end_date;
}
function getmonthname(month)
{
var mnth;

if(month==1)
{
	mnth='January';
}
if(month==2)
{
	mnth='February';
}
if(month==3)
{
	mnth='March';
}
if(month==4)
{
	mnth='April';
}
if(month==5)
{
	mnth='May';
}
if(month==6)
{
	mnth='June';
}
if(month==7)
{
	mnth='July';
}if(month==8)
{
	mnth='August';
}
if(month==9)
{
	mnth='September';
}
if(month==10)
{
	mnth='October';
}
if(month==11)
{
	mnth='November';
}
if(month==12)
{
	mnth='December';
}
return mnth;
}
function down_excel_function(bill_data_arr)
{
	try
	{
		var strVar1= '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'+
		'<head>'+
		'<meta http-equiv=Content-Type content="text/html; charset=windows-1252"/>'+
        '<meta name=ProgId content=Excel.Sheet/>'+
        '<meta name=Generator content="Microsoft Excel 11"/>'+
        '<!--[if gte mso 9]><xml>'+
        '<x:excelworkbook>'+
        '<x:excelworksheets>'+
        '<x:excelworksheet=sheet1>'+
        '<x:name>** ESTIMATE FILE**</x:name>'+
        '<x:worksheetoptions>'+
        '<x:selected></x:selected>'+
        '<x:freezepanes></x:freezepanes>'+
        '<x:frozennosplit></x:frozennosplit>'+
        '<x:splithorizontal>** FROZEN ROWS + 1 **</x:splithorizontal>'+
        '<x:toprowbottompane>** FROZEN ROWS + 1 **</x:toprowbottompane>'+
        '<x:splitvertical>** FROZEN COLUMNS + 1 **</x:splitvertical>'+
        '<x:leftcolumnrightpane>** FROZEN COLUMNS + 1**</x:leftcolumnrightpane>'+
		    '<x:activepane>0</x:activepane>'+// 0
		    '<x:panes>'+
			'<x:pane>'+
			'<x:number>3</x:number>'+
			'</x:pane>'+
			'<x:pane>'+
		    '<x:number>1</x:number>'+
		    '</x:pane>'+
		    '<x:pane>'+
			'<x:number>2</x:number>'+
        '</x:pane>'+
        '<x:pane>'+
        '<x:number>0</x:number>'+//1
        '</x:pane>'+
        '</x:panes>'+
        '<x:protectcontents>False</x:protectcontents>'+
        '<x:protectobjects>False</x:protectobjects>'+
        '<x:protectscenarios>False</x:protectscenarios>'+
        '</x:worksheetoptions>'+
        '</x:excelworksheet>'+
		'</x:excelworksheets>'+
        '<x:protectstructure>False</x:protectstructure>'+
        '<x:protectwindows>False</x:protectwindows>'+
		'</x:excelworkbook>'+
        
        // '<style>'+
			
			//-------------------------------------
        '</x:excelworkbook>'+
        '</xml><![endif]-->'+
         '<style>'+
		'p.MsoFooter, li.MsoFooter, div.MsoFooter'+
		'{ margin:0in; margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; font-size:12.0pt;}'+
		'<style>'+

		'<!-- /* Style Definitions */'+

		//'@page Section1'+
		//'{ size:8.5in 11.0in; margin:1.9cm 1.27cm 1.9cm 1.27cm; mso-header-margin:.5in; mso-footer-margin:.5in; mso-title-page:yes; mso-header: h1; mso-footer: f1; mso-first-header: fh1; mso-first-footer: ff1; mso-paper-source:0;}'+

		'div.Section1'+
		'{ page:Section1;}'+

		'table#hrdftrtbl'+
		'{ margin:0in 0in 0in 900in; width:1px; height:1px; overflow:hidden; font-family:Arial;}-->'+

		'</style>'+
		
		
		'</head>'+
		

		'<body>'+
		
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'
		
		var strVar2 = '';    
		strVar2 += "<table width=\"105%\">";
		 strVar2 += '<table width="100%" border="1">';
		strVar2 += "<tr>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Employee</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Location</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Project Type</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Subsidiary</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ST Hours</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Holiday</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Leaves Approved</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>OT Hours</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Timesheet Start Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Timesheet End Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Subtier Start date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Subtier Enddate</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>ST Rate</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>OT Rate</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Rate Type</td>";
		/*strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>TDS Account</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Partial Paid Amount</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Net Payable</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Memo</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>MSME Vendor</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Subsidiary</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Currency</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Billing From Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Billing To Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Vendor Invoice Date</td>";
		strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>Vendor Open Balance</td>";*/
		strVar2 += "<\/tr>";
		
		if (bill_data_arr)
		{
			nlapiLogExecution('audit','down_excel_function transcation length:- ',bill_data_arr.length);
			for (var jk = 0; jk < bill_data_arr.length; jk++)
			{
				//var age_in_days = getDatediffIndays(bill_data_arr[jk].transaction_date,nlapiDateToString(new Date()));
				//var tds_ = bill_data_arr[jk].tds_amount;
				//var tds_t = '';
				//if(parseFloat(tds_)>0)
				//	tds_t = bill_data_arr[jk].tds_account_name_t;
									
				//nlapiLogExecution('audit','down_excel_function vendor name:- ',bill_data_arr[jk].vendor_name);
				strVar2 += "<tr>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].employee+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].location+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].project_name
+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].subsidiary+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].st_dura
							+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].holiday_dura+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].total_leaves+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].total_ot_dura+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].timesheet_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].timesheet_enddate+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].st_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].end_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].strate+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].ot_rate+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].rate_type+ "</td>";
				/*strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +tds_t+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].finalPartialamount+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_total+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].transaction_memo+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].is_vendor_msme+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_subsidiary+ "</td>";
				
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_currency+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_from_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_to_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].bill_vendor_invoice_date+ "</td>";
				strVar2 += "<td Width=\"5%\" style='text-align:left;' font-size=\"10\" \>" +bill_data_arr[jk].vendor_open_balance+ "</td>";*/
				strVar2 += "<\/tr>";
			}
		}
		else
		{
			//NO RESULTS FOUND
		}
					
		strVar2 += "<\/table>";			
		strVar1 = strVar1 + strVar2;
		var file = nlapiCreateFile('Bills Pending for Creation.xls', 'XMLDOC', strVar1);
		nlapiLogExecution('debug','file:- ',file);
		//done for testing
		var curr_user = nlapiGetUser();
		var s_email = nlapiLookupField('employee', curr_user, 'email');
		//response.setContentType('XMLDOC','Vendor Bill Aging Report.xls');
		var a_emp_attachment = new Array();
		/*nlapiSendEmail(442, 'sai.vannamareddy@brillio.com', 'Bill Report', 'Please find the attachement of  bills pending for creation', null, null, null, file, true, null, null);*/
		//done for testing
		a_emp_attachment['entity'] = curr_user;
		nlapiSendEmail(442, s_email, 'Vendor Bill Aging Report', 'Please find the attachement of vendor bill aging report', null, 'sai.vannamareddy@brillio.com', a_emp_attachment, file, true, null, null);
		//response.write( file.getValue() );
		//window.close();
	}
	catch(err)
	{
		nlapiLogExecution('ERROR','ERROR MESSAGE:- ',err);
	}
}