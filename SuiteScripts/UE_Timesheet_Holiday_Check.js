/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 28 Sep 2015 nitish.mishra
 * 
 */

function userEventBeforeLoad(type, form, request) {
	try {

		// add a custom field for storing if the week has a holiday or not
		var weekHasHolidayField = form.addField('custpage_week_has_holiday',
		        'checkbox', 'Week Has Holiday');
		weekHasHolidayField.setDisplayType('hidden');

		// add a custom field to store the holidays
		var holidayListField = form.addField('custpage_holiday_dates',
		        'textarea', 'Holiday Dates');
		holidayListField.setDisplayType('hidden');

		var holidayDetails = checkWeekHasHoliday(
		        nlapiGetFieldValue('startdate'), nlapiGetFieldValue('enddate'),
		        nlapiGetFieldValue('employee'),
		        nlapiGetFieldValue('subsidiary'));

		weekHasHolidayField.setDefaultValue(holidayDetails.WasHolidayFound);
		holidayListField.setDefaultValue(JSON
		        .stringify(holidayDetails.HolidayList));

		// if (holidayDetails.WasHolidayFound == 'F') {

		// if (type == "create") {
		// form.addButton('custpage_btn_autofill', 'Autofill', 'autoFill');
		// }
		// }
	} catch (err) {
		nlapiLogExecution('ERROR', 'userEventBeforeLoad', err);
	}
}

function checkWeekHasHoliday(startDate, endDate, employeeId, empSubsidiary) {
	try {
		var doesHolidayExists = 'F';
		var holidayList = [];

		// search all brillio holiday within the week
		var brillioHolidaySearch = nlapiSearchRecord('customrecord_holiday',
		        null, [
		                new nlobjSearchFilter('custrecord_date', null,
		                        'within', startDate, endDate),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('custrecord_date'),
		                new nlobjSearchColumn('custrecordsubsidiary') ]);

		// search all customer holiday within the week
		var customerHolidaySearch = nlapiSearchRecord(
		        'customrecordcustomerholiday', null, [
		                new nlobjSearchFilter('custrecordholidaydate', null,
		                        'within', startDate, endDate),
		                new nlobjSearchFilter('isinactive', null, 'is', 'F') ],
		        [ new nlobjSearchColumn('custrecordholidaydate'),
		                new nlobjSearchColumn('custrecordcustomersubsidiary'),
		                new nlobjSearchColumn('custrecord13') ]);

		if (brillioHolidaySearch || customerHolidaySearch) {
			nlapiLogExecution('debug', 'holiday exists in the week');

			var allocationSearch = nlapiSearchRecord('resourceallocation',
			        null, [
			                new nlobjSearchFilter('startdate', null,
			                        'notafter', endDate),
			                new nlobjSearchFilter('enddate', null, 'notbefore',
			                        startDate),
			                new nlobjSearchFilter('resource', null, 'anyof',
			                        employeeId) ], [
			                new nlobjSearchColumn('custentityproject_holiday',
			                        'job'), new nlobjSearchColumn('customer'),
			                new nlobjSearchColumn('subsidiary', 'job') ]);

			if (allocationSearch) {
				nlapiLogExecution('debug', 'allocationSearch.length',
				        allocationSearch.length);

				for (var i = 0; i < allocationSearch.length; i++) {

					// Brillio Holiday
					if (allocationSearch[i].getValue(
					        'custentityproject_holiday', 'job') == '1') {

						if (brillioHolidaySearch) {
							nlapiLogExecution('debug',
							        'brillio holiday exists in the week');

							// check if a brillio holiday exists for the project
							// subsidiary
							for (var j = 0; j < brillioHolidaySearch.length; j++) {

								if (brillioHolidaySearch[j]
								        .getValue('custrecordsubsidiary') == allocationSearch[i]
								        .getValue('subsidiary', 'job')) {
									doesHolidayExists = 'T';
									holidayList.push(brillioHolidaySearch[j]
									        .getValue('custrecord_date'));
								}
							}
						}
					} else { // Customer Holiday

						if (customerHolidaySearch) {
							nlapiLogExecution('debug',
							        'customer holiday exists in the week');

							// check if the customer exists in the customer
							// holiday list
							for (var j = 0; j < customerHolidaySearch.length; j++) {

								// customer holiday found
								if (customerHolidaySearch[j]
								        .getValue('custrecord13') == allocationSearch[i]
								        .getValue('customer')
								        && customerHolidaySearch[j]
								                .getValue('custrecordcustomersubsidiary') == allocationSearch[i]
								                .getValue('subsidiary', 'job')) {
									doesHolidayExists = 'T';
									holidayList.push(customerHolidaySearch[j]
									        .getValue('custrecordholidaydate'));
								}
							}
						}
					}
				}
			}
		}

		nlapiLogExecution('debug', 'holiday added', doesHolidayExists);
		nlapiLogExecution('debug', 'holiday list', JSON.stringify(holidayList));
		return {
		    WasHolidayFound : doesHolidayExists,
		    HolidayList : holidayList
		};
	} catch (err) {
		nlapiLogExecution('ERROR', 'checkWeekHasHoliday', err);
		throw err;
	}
}

function getActiveAllocations(weekStartDate, weekEndDate, employee, subsidiary)
{
	try {
		var allocationSearch = nlapiSearchRecord('resourceallocation', null, [
		        new nlobjSearchFilter('startdate', null, 'notafter',
		                weekEndDate),
		        new nlobjSearchFilter('enddate', null, 'notbefore',
		                weekStartDate),
		        new nlobjSearchFilter('resource', null, 'anyof', employee) ], [
		        new nlobjSearchColumn('custentityproject_holiday', 'job'),
		        new nlobjSearchColumn('customer') ]);

		if (isNotEmpty(allocationSearch)) {
			var holiday_record_list = [];

			for (var i = 0; i < allocationSearch.length; i++) {
				var holiday_id = allocationSearch[i].getValue(
				        'custentityproject_holiday', 'job');

				if (holiday) {
					holiday_record_list.push(holiday_id);
				}
			}

			if (isArrayNotEmpty(holiday_record_list)) {

			} else {
				throw "No Holiday Assigned In Projects";
			}
		} else {
			throw "No active allocation";
		}

		return projects;
	} catch (err) {
		nlapiLogExecution('ERROR', 'getActiveAllocations', err);
		throw err;
	}
}
