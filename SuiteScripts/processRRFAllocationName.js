/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 May 2019     Aazamali Khan
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try {
		var recObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		var externalHire = recObj.getFieldValue("custrecord_frf_details_external_hire");
		if(externalHire == "T"){
		  	var taleoRecId = recObj.getFieldValue("custrecord_frf_details_rrf_number");
			var selectedEmployeeEmail = "";
			if(isNotEmpty(taleoRecId)){
				selectedEmployeeEmail = nlapiLookupFiled("customrecord_taleo_external_hire",taleoRecId,"custrecord_offered_emp_email");	//Changed the field from "custrecord_taleo_external_selected_can" to "custrecord_offered_emp_email" by Ashish
			}
			// write a search 
			if(isNotEmpty(selectedEmployeeEmail)){
				var searchResult = getEmployeeRec(selectedEmployeeEmail);
				if(searchResult){
					var empId = searchResult[0].getId();
					// Get the resource allocation 
					var allocationSearch = getAllocationSearchForExternal(empId); 
					if(allocationSearch){
						var resourceId = allocationSearch[0].getValue("resource");
						var startDate = allocationSearch[0].getValue("startdate");
						recObj.setFieldValue("custrecord_frf_details_allocated_emp",resourceId);
						recObj.setFieldValue("custrecord_frf_details_allocated_date",startDate);
						recObj.setFieldValue("custrecord_frf_details_open_close_status","2");
						recObj.setFieldValue("custrecord_frf_details_status_flag","2");
						nlapiSubmitRecord(recObj);
					}
				}
			}
		}
	} catch (e) {
		// TODO: handle exception
		nlapiLogExecution("DEBUG", "error in script : ", e);
	}
}
function getAllocationSearchForExternal(empId){
	var resourceallocationSearch = nlapiSearchRecord("resourceallocation",null,
[
   ["startdate","notafter","today"], 
   "AND", 
   ["enddate","notbefore","today"], 
   "AND", 
   ["resource","anyof",empId]
], 
[
   new nlobjSearchColumn("resource"), 
   new nlobjSearchColumn("startdate"), 
   new nlobjSearchColumn("company")
]
);
return resourceallocationSearch;
}
function getEmployeeRec(personalEmail){
	
	var employeeSearch = nlapiSearchRecord("employee",null,
[
   ["custentity_personalemailid","is",personalEmail]
], 
[
]
);
return employeeSearch;
}
