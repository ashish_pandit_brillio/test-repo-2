/**
 * @author Shweta
 */
// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name : CLI_Project_Assignee_Controls.js
	Author      : Shweta Chopde
	Date        : 20 May 2014
	Description : Assigne Validations



	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




	Below is a summary of the process controls enforced by this script file.  The control logic is described
	more fully, below, in the appropriate function headers and code blocks.

     PAGE INIT
		- pageInit(type)


     SAVE RECORD
		- saveRecord()


     VALIDATE FIELD
		- validateField(type, name, linenum)


     FIELD CHANGED
		- fieldChanged(type, name, linenum)


     POST SOURCING
		- postSourcing(type, name)


	LINE INIT
		- lineInit(type)


     VALIDATE LINE
		- validateLine()


     RECALC
		- reCalc()


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:





*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================



// BEGIN SCRIPT UPDATION BLOCK  ====================================
/*


*/
// END SCRIPT UPDATION BLOCK  ====================================




// BEGIN GLOBAL VARIABLE BLOCK  =====================================
{

	//  Initialize any Global Variables, in particular, debugging variables...


}
// END GLOBAL VARIABLE BLOCK  =======================================





// BEGIN PAGE INIT ==================================================

function pageInit(type)
{
    /*  On page init:

	- PURPOSE

		FIELDS USED:

		--Field Name--				--ID--			--Line Item Name--

    */

    //  LOCAL VARIABLES


    //  PAGE INIT CODE BODY


}

// END PAGE INIT ====================================================





// BEGIN SAVE RECORD ================================================

function saveRecord()
{
    /*  On save record:

	- PURPOSE



	  FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */


    //  LOCAL VARIABLES



    //  SAVE RECORD CODE BODY


	return true;

}

// END SAVE RECORD ==================================================





// BEGIN VALIDATE FIELD =============================================

function validateField(type, name, linenum)
{

	/*  On validate field:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  VALIDATE FIELD CODE BODY


	return true;

}

// END VALIDATE FIELD ===============================================





// BEGIN FIELD CHANGED ==============================================

function fieldChanged_assignee(type, name, linenum) //
{

	if (name == 'serviceitem') //
	{
		nlapiLogExecution('DEBUG', 'fieldChanged_assignee', '*** Into serviceitem ***');
		
		var i_project = nlapiGetFieldValue('company');
		var i_index = nlapiGetCurrentLineItemIndex('assignee')
		
		var i_resource = nlapiGetCurrentLineItemValue('assignee', 'resource');
		
		var i_service_item = nlapiGetCurrentLineItemValue('assignee', 'serviceitem');
		
		var i_currency = get_project_currency(i_project);
		
		var i_unit_cost = get_employee_labour_cost(i_resource);
        nlapiLogExecution('DEBUG', 'fieldChanged_assignee', '*** i_unit_cost *** : ' + i_unit_cost);
		
		nlapiSetCurrentLineItemValue('assignee', 'unitcost', i_unit_cost)
		
		var i_bill_rate = get_project_bill_rate(i_project, i_currency, i_service_item)
		nlapiSetCurrentLineItemValue('assignee', 'unitprice', i_bill_rate)
	}//Name
	
	if (name == 'resource') //
	{
		nlapiLogExecution('DEBUG', 'fieldChanged_assignee', '*** Into Resource ***');
		
		var i_resource = nlapiGetCurrentLineItemValue('assignee', 'resource');
		
		var i_service_item = nlapiGetCurrentLineItemValue('assignee', 'serviceitem');
		
		var i_project = nlapiGetFieldValue('company');
		
		var i_currency = get_project_currency(i_project);
		
		var i_unit_cost = get_employee_labour_cost(i_resource);
		
		nlapiSetCurrentLineItemValue('assignee', 'unitcost', i_unit_cost)
		
		var i_bill_rate = get_project_bill_rate(i_project, i_currency, i_service_item)
		nlapiSetCurrentLineItemValue('assignee', 'unitprice', i_bill_rate)
		
		// Code added to Automatically set Service item while selecting any resource for selected Task.
		
		// Take value of task name
		var s_Task_Name = nlapiGetFieldValue('title');
		
		// Search for the respective service item by their name.
		var a_Filters = new Array();
		var a_Columns = new Array();
		
		a_Columns[a_Columns.length] = new nlobjSearchColumn('internalid')
		
		if (s_Task_Name == 'Project Activities') //
		{
			a_Filters[a_Filters.length] = new nlobjSearchFilter('itemid', null, 'is', 'ST');
		}
		else //
 			if (s_Task_Name == 'Leave') //
			{
				a_Filters[a_Filters.length] = new nlobjSearchFilter('itemid', null, 'is', 'Leave');
			}
			else //
 				if (s_Task_Name == 'Holiday') //
				{
					a_Filters[a_Filters.length] = new nlobjSearchFilter('itemid', null, 'is', 'Holiday');
				}
		
		if (a_Filters.length > 0) //
		{
			try //
			{
				var search_Result = nlapiSearchRecord('item', null, a_Filters, a_Columns);
				
				if (_is_Valid(search_Result)) //
				{
					nlapiLogExecution('DEBUG', 'fieldChanged_assignee', 'search_Result : ' + search_Result.length);
					var all_Columns = search_Result[0].getAllColumns();
					
					for (var counter_I = 0; counter_I < search_Result.length; counter_I++) //
					{
						var service_Item_ID = search_Result[0].getValue(all_Columns[0]);
						nlapiLogExecution('DEBUG', 'fieldChanged_assignee', 'service_Item_ID : ' + service_Item_ID);
						
						nlapiSetCurrentLineItemValue('assignee', 'serviceitem', service_Item_ID, true, true);
						//nlapiDisableLineItemField('assignee', 'serviceitem', 'T');
					}
				}
			} 
			catch (exp) //
			{
				nlapiLogExecution('ERROR', 'fieldChanged_assignee', 'exception : ' + exp);
			}
		}
		
		// set internal id of service item on line level.
		// disable the field of service item.
	
		// End of Code added to Automatically set Service item while selecting any resource for selected Task.
	
	}//Resource
}

// END FIELD CHANGED ================================================





// BEGIN POST SOURCING ==============================================

function postSourcing(type, name)
{

    /*  On post sourcing:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  POST SOURCING CODE BODY



}

// END POST SOURCING ================================================



// BEGIN LINE INIT ==============================================

function lineInit(type)
{

    /*  On Line Init:

	- PURPOSE

          FIELDS USED:

		--Field Name--			--ID--		--Line Item Name--


    */

    //  LOCAL VARIABLES


    //  LINE INIT CODE BODY



}

// END LINE INIT ================================================


// BEGIN VALIDATE LINE ==============================================

function validateLine_assigne(type)
{ 
  if(type =='assignee')
  {
  	var i_project = nlapiGetFieldValue('company')
	var i_service_item = nlapiGetCurrentLineItemValue('assignee','serviceitem')	
	
		if (_logValidation(i_project)) 
		{
			var o_projectOBJ = nlapiLoadRecord('job', i_project);
			
			if (_logValidation(o_projectOBJ)) 
			{			
				var i_billing_type = o_projectOBJ.getFieldValue('jobbillingtype')
				
				if (i_billing_type == 'TM') 
				{
					if (!_logValidation(i_service_item)) 
					{
						alert(' Please enter Service Item');
					    return false;
					}
					
				}//Time & Material
			}
		}
  }//Assignee

	return true;

}

// END VALIDATE LINE ================================================




// BEGIN RECALC =====================================================

function recalc(type)
{

	/*  On recalc:

          - EXPLAIN THE PURPOSE OF THIS FUNCTION


          FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES


	//  RECALC CODE BODY


}

// END RECALC =======================================================





// BEGIN FUNCTION ===================================================

function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}



function get_employee_labour_cost(i_resource)
{
  var i_labour_cost = 0;
  if(_logValidation(i_resource))
  {
  	var o_employeeOBJ = nlapiLoadRecord('employee',i_resource);
	
	if(_logValidation(o_employeeOBJ))
	{
		i_labour_cost = o_employeeOBJ.getFieldValue('laborcost')
		
		 if(!_logValidation(i_labour_cost))
		 {
		 	i_labour_cost = 0
		 }
		
	}//Employee OBJ	
  }//Resource	
  return i_labour_cost;
}//Employee Labour Cost


function get_project_bill_rate(i_project,i_currency,i_service_item)
{
	var i_bill_rate = 0;
	if(_logValidation(i_project)&&_logValidation(i_currency)&&_logValidation(i_service_item))
	{
	var filter = new Array();
    filter[0] = new nlobjSearchFilter('custrecord_proj',null,'is',i_project);
	filter[1] = new nlobjSearchFilter('custrecord_currency1',null,'is',i_currency);
	filter[2] = new nlobjSearchFilter('custrecord_item1',null,'is',i_service_item);
	
	var columns = new Array();
    columns[0] = new nlobjSearchColumn('custrecord_rate');
	
	 var a_search_results = nlapiSearchRecord('customrecord_customraterecord',null,filter,columns);
	 
	 if (_logValidation(a_search_results)) 
	 {	 	
	 	for (var t = 0; t < a_search_results.length; t++) 
		{
	 	  i_bill_rate = a_search_results[t].getValue('custrecord_rate');		  
		  
		 if(!_logValidation(i_bill_rate))
		 {
		 	i_bill_rate = 0
		 }
		
	 		
	  	}
	 }
	}//Project
	
	return i_bill_rate;
}

function get_project_currency(i_project)
{
  var i_currency;
  if(_logValidation(i_project))
  {
  	var o_projectOBJ = nlapiLoadRecord('job',i_project);
	
	if(_logValidation(o_projectOBJ))
	{
		i_currency = o_projectOBJ.getFieldValue('currency')
		
	}//Project OBJ	
  }//Project
  
  return i_currency;	
}//Project Currency

function _is_Valid(obj) //
{
    if (obj != null && obj != '' && obj != 'undefined' && obj != undefined)//
    {
        return true;
    }
    else //
    {
        return false;
    }
}

// END FUNCTION =====================================================
