/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Nov 2017     deepak.srinivas
 *
 */
/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
    try {
        var response = new Response();
        var current_date = nlapiDateToString(new Date());

		/*var dataIn = ({"RequestType":"GET",
				"Date":"",
				"Month":"06",
				"Year":"2021"});
*/
        //var employeeId =  getUserUsingEmailId(dataIn.EmailId);
        //var requestType = dataIn.RequestType;  
        //var email = 'rahul.murali@brillio.com';
        //var employeeId =  getUserUsingEmailId(email);
        var requestType = 'GET';

        //Log for current date
        nlapiLogExecution('DEBUG', 'Current Date', 'current_date...' + current_date)
        nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));


        switch (requestType) {

            case M_Constants.Request.Get:

                if (requestType) {
                    response.Data = actual_data_flow(dataIn);
                    response.Status = true;
                } else {
                    response.Data = "Some error with the data sent";
                    response.Status = false;
                }
                break;
        }

    } catch (err) {
        nlapiLogExecution('ERROR', 'postRESTlet', err);
        response.Data = err;
        response.Status = false;
    }
    nlapiLogExecution('debug', 'response', JSON.stringify(response));
    return response;

}

function actual_data_flow(dataIn) {
    try {
        var a_project_list = new Array();

        var s_selected_project_name = '';

        var d_today = nlapiDateToString(new Date());
        //var d_today = '12/1/2017';
        d_today = nlapiStringToDate(d_today);


        var d_day = nlapiAddMonths(d_today, -1);
        nlapiLogExecution('debug', 'd_day', d_day);
        var d_month_start = nlapiAddDays(d_day, -1 * (d_day.getDate() - 1));
        var s_month_start = d_month_start.getMonth() + 1 + '/' +
            d_month_start.getDate() + '/' + d_month_start.getFullYear();
        var d_month_end = nlapiAddDays(nlapiAddMonths(d_month_start, 1), -1);
        var s_month_end = d_month_end.getMonth() + 1 + '/' +
            d_month_end.getDate() + '/' + d_month_end.getFullYear();


        var showAll = false;

        var s_from = '';
        var s_to = '';

        var project_search_results = '';
        var currentDate_Time = date_time();
        var receivedDate_Time = dataIn.Date;
        var s_d_flag = false;
        var filters = [];
       //	var receivedDate_Time = '01/07/2019 10:00 pm';
      //Actual Delta
        if (_logValidation(receivedDate_Time)) { 
            var d_receivedDate_Time = nlapiStringToDate(receivedDate_Time);

            var d_month_receivedDate_Time = d_receivedDate_Time.getMonth();
			//If year change
			if(parseInt(d_month_receivedDate_Time) == parseInt(0)){
			//var d_month_receivedDate_Time_s = nlapiAddDays(d_receivedDate_Time, -1 * (d_receivedDate_Time.getDate() - 1));
			//var month_S = d_month_receivedDate_Time_s.getMonth();
			d_month_receivedDate_Time = 12;
			}
            var d_pre_month = d_month_start.getMonth() + 1;
            nlapiLogExecution('DEBUG', 'd_month_receivedDate_Time', d_month_receivedDate_Time);
            nlapiLogExecution('DEBUG', 'd_pre_month', d_pre_month);

            if (parseInt(d_month_receivedDate_Time) != parseInt(d_pre_month)) {
                s_d_flag = true;
            }

        }
        if (_logValidation(receivedDate_Time) && s_d_flag == false) {
        	nlapiLogExecution('DEBUG','Inside Delta Func');
			
			var p_search = nlapiLoadSearch('transaction', 2347); 
            var filtrs = p_search.getFilters();
            var s_from = s_month_start;
            var s_to_ = s_month_end;
            filtrs = filtrs.concat([new nlobjSearchFilter('date', 'systemnotes', 'within', [receivedDate_Time, currentDate_Time])]);
				
            
           // filtrs.push(new nlobjSearchFilter('date', 'systemnotes', 'within', [receivedDate_Time, currentDate_Time]));

            var cols = [];
            cols.push(new nlobjSearchColumn('custcolprj_name', null, 'group'));
            cols.push(new nlobjSearchColumn('custcol_project_entity_id', null, 'group'));
            var search_projects = searchRecord('transaction', 2347, filtrs, cols);

            var s_project_options = '';
            var a_selected_project_list = '';
            var s_project_number = '';
            var s_project_name = '';
            for (var i = 0; search_projects != null && i < search_projects.length; i++) {
                var i_project_id = search_projects[i].getValue('internalid');
                s_project_number = search_projects[i]
                    .getValue('custcol_project_entity_id', null, 'group');
                s_project_name = search_projects[i].getValue('custcolprj_name', null, 'group');

                //	var s_selected_project = request.getParameter('project');

                if (_logValidation(s_project_number)) {
                    a_project_list.push(s_project_number);
                }



            }
            
            
        } else if(!_logValidation(dataIn.Month) && !_logValidation(dataIn.Year)){
            var project_search_results = getTaggedProjectList();
            var s_project_options = '';
            var a_selected_project_list = '';
            var s_project_number = '';
            var s_project_name = '';
            for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
                var i_project_id = project_search_results[i].getValue('internalid');
                s_project_number = project_search_results[i]
                    .getValue('entityid');
                s_project_name = project_search_results[i].getValue('companyname');



                if (_logValidation(s_project_number)) {
                    a_project_list.push(s_project_number);
                }



            }
            nlapiLogExecution('DEBUG','NOT Inside Delta Func');
            //Date filter needs to get all transaction within month
            var s_from = s_month_start;
            var s_to_ = s_month_end;
            
        }
        
        //Specific month actuals
        if(_logValidation(dataIn.Month) && _logValidation(dataIn.Year)){
        	
        	var s_month_start = '';
        	var s_month_end = '';
        	//Make time stamp as blank
        	currentDate_Time = '';
        	var project_search_results = getTaggedProjectList();
            var s_project_options = '';
            var a_selected_project_list = '';
            var s_project_number = '';
            var s_project_name = '';
            for (var i = 0; project_search_results != null && i < project_search_results.length; i++) {
                var i_project_id = project_search_results[i].getValue('internalid');
                s_project_number = project_search_results[i]
                    .getValue('entityid');
                s_project_name = project_search_results[i].getValue('companyname');



                if (_logValidation(s_project_number)) {
                    a_project_list.push(s_project_number);
                }



            }
        	
            s_month_start = dataIn.Month + '/' +
                1 + '/' + dataIn.Year;
            nlapiLogExecution('DEBUG','Specific month Run - Month Start Date',s_month_start);
            
            var nextMonthStartDate = nlapiAddMonths(nlapiStringToDate(s_month_start),1);
           //Logic to find out last date in month
            var last_date = nlapiAddDays(nextMonthStartDate, -1);
            
            s_month_end = dataIn.Month + '/' +
            last_date.getDate() + '/' + dataIn.Year;
            nlapiLogExecution('DEBUG','Specific month Run - Month ENd Date',s_month_end);
            var s_from = s_month_start;
            var s_to_ = s_month_end;
        }
       // var search = nlapiLoadSearch('transaction', 2209); // TO get Project List

        nlapiLogExecution('debug', 'a_project_list', JSON.stringify(a_project_list));
		a_project_list = removearrayduplicate(a_project_list);
		nlapiLogExecution('debug', 'a_project_list', a_project_list);
        //Search for exchange Rate
        var f_rev_curr = 0;
        var f_cost_curr = 0;
       

        //var filters = [];
        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_mont).setFormula('{custrecord_pl_currency_exchange_month}')); //NOTE : Capital 'Y' in Year
        //filters.push(new nlobjSearchFilter('formulatext', null, 'is', s_year_).setFormula('{custrecord_pl_currency_exchange_year}'));
			var a_dataVal = {};
			var a_dataVal =PL_curencyexchange_rate();

			



        var i_user_id = 1618;
        //nlapiLogExecution('debug', 'user', i_user_id);
        var project_search_results = '';
		
		
			var search = nlapiLoadSearch('transaction', 2209); 
            var filters = search.getFilters();
           
            filters = filters.concat([new nlobjSearchFilter('trandate', null,
                'within', s_from, s_to_)]);


        if (a_selected_project_list == null ||
            a_selected_project_list.length == 0) {
            if (a_project_list.length > 0) {
                a_selected_project_list = a_project_list;

                showAll = true;
            } else {
                a_selected_project_list = new Array();
            }
        }

        if (a_selected_project_list != null &&
            a_selected_project_list.length != 0) {
            var s_formula = '';

            for (var i = 0; i < a_selected_project_list.length; i++) {
                if (i != 0) {
                    s_formula += "OR";
                }
                var n = a_selected_project_list[i].length;
                /*s_formula += "{custcol_project_entity_id} = '" +
                a_selected_project_list[i] + "' ";*/
                if(parseInt(n)>parseInt(9)){
                s_formula += " SUBSTR({custcolprj_name}, 0,16) = '" +
                    a_selected_project_list[i] + "' ";
                }
                else{
                s_formula += " SUBSTR({custcolprj_name}, 0,9) = '" +
                a_selected_project_list[i] + "' ";
                }
               /*  s_formula += " SUBSTR({custcolprj_name}, 0,9)','startswith',"
                	 + a_selected_project_list[i] + "' ";*/
            }

            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("CASE WHEN " + s_formula +
                " THEN 1 ELSE 0 END");

            filters = filters.concat([projectFilter]);
        } else {
            var projectFilter = new nlobjSearchFilter('formulanumeric', null,
                'equalto', 1);

            projectFilter.setFormula("0");

            filters = filters.concat([projectFilter]);
        }



       //Date filter pushed to above 


        var columns = search.getColumns();

        //columns[0].setSort(false);
        //columns[3].setSort(true);
        //	columns[9].setSort(false);
        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'SalesOrd'));
        filters.push(new nlobjSearchFilter('transactionnumbernumber', null, 'isnotempty'));
        filters.push(new nlobjSearchFilter('type', null, 'noneof', 'PurchOrd'));
        
        // filters.push(new nlobjSearchFilter('internalid',null,'anyof',[1508707]));

        filters.push(new nlobjSearchFilter('status', null, 'noneof', ['ExpRept:A', 'ExpRept:B', 'ExpRept:C', 'ExpRept:D', 'ExpRept:E', 'ExpRept:H', 'Journal:A', 'VendBill:C',
            'VendBill:D', 'VendBill:E', , 'CustInvc:E', 'CustInvc:D'
        ]));

		
        var search_results = searchRecord('transaction', null, filters, [
            columns[2], columns[1], columns[0], columns[3], columns[4],
            columns[5], columns[8], columns[9], columns[10], columns[11], columns[12], columns[13], columns[14], columns[15], columns[16],
            columns[17], columns[18], columns[19], columns[20], columns[21],columns[22],columns[23]
        ]);
		
	/*	var filters_1= new Array();
		filters_1.push(new nlobjSearchFilter('internalid',null,'anyof','1870177'));
	   var search_results = nlapiSearchRecord('transaction',null,[["internalid","is","1870177"]],null);*/
        // Get the facility cost
nlapiLogExecution('debug',"search_results",search_results);
     
        var o_json = new Object();

        var o_data = {
            'TimeStamp': currentDate_Time,
            'Revenue': [],
            'Discount' : []
		    /*'People Cost' : [],
		    'Facility Cost' : [],
			'Other Cost - Travel':[],
			'Other Cost - Immigration': [],
			'Other Cost - Professional Fees' : [],
			'Other Cost - Others' : []*/

        };

        var new_object = null;

        var s_period = '';

        var a_period_list = [];

        var a_category_list = [];
        a_category_list[0] = '';
        var a_group = [];

        var a_income_group = [];
        var j_other_list = {};
        var other_list = [];

       nlapiLogExecution('debug',"search_results",search_results.length);

        for (var i = 0; search_results != null && i < search_results.length; i++) {
            var period = search_results[i].getText(columns[0]);

            //Code updated by Deepak, Dated - 21 Mar 17
            var s_month_year = period.split(' ');
            var s_mont = s_month_year[0];
            s_mont = getMonthCompleteName(s_mont);
            var s_year_ = s_month_year[1];
            var f_revRate = 66.0;
            var f_costRate = 66.0;
            var f_gbp_rev_rate, f_crc_cost_rate, f_nok_cost_rate,f_eur_conv_rate,f_aud_conv_rate;
            

			var data_indx=(s_mont+"_"+s_year_).toString();


            var transaction_type = search_results[i].getText(columns[8]);

            var transaction_date = search_results[i].getValue(columns[9]);

            var i_subsidiary = search_results[i].getValue(columns[10]);

            var amount = parseFloat(search_results[i].getValue(columns[1]));

            var category = search_results[i].getValue(columns[3]);

            var s_account_name = search_results[i].getValue(columns[11]);

            var currency = search_results[i].getValue(columns[12]);

            var exhangeRate = search_results[i].getValue(columns[13]);
			
			//check for mis practice
			var mis_sub_practice = '';
			var mis_parent_practice = '';
			var index_ = search_results[i].getValue(columns[23]).indexOf(':');
			if(parseInt(index_) > parseInt(0)){
				var a_mis_practice = search_results[i].getValue(columns[23]).split(':');
				mis_parent_practice = a_mis_practice[0];
				mis_sub_practice = search_results[i].getValue(columns[23]);
			}
			else{
			mis_parent_practice = search_results[i].getValue(columns[23]);
			mis_sub_practice = '';
			}
				
			nlapiLogExecution('AUDIT','mis_parent_practice',mis_parent_practice);	
			nlapiLogExecution('AUDIT','Project Description',search_results[i].getValue(columns[16]));
				//Hardcoded for GDS practice
			if(mis_parent_practice.trim() == 'Global Design Studio'){ 	
			mis_parent_practice = 'Design & Experience Studio';
			mis_sub_practice = 'Design & Experience Studio : Experience Design';
			}	

			if(mis_parent_practice.trim() == 'Advanced Technology Group'){ 	
						mis_parent_practice = 'Technology Advisory &Consulting';
						mis_sub_practice = '';
						}				
			
            //	nlapiLogExecution('audit','exhangeRate:- ',exhangeRate);

            //Transaction blocking for discount GL's
            
            if(transaction_type != 'Journal' && (parseInt(s_account_name) == parseInt(515)|| parseInt(s_account_name) == parseInt(516))){
            	continue;
            }
            
            var s_practice = search_results[i].getText(columns[14]);

            var s_project_description = search_results[i].getValue(columns[16]);
            var project_ID = '';
            var projectName = '';
            var s_subPractice = '';
            var s_parentPractice = '';
            var projectSearch = '';
            if (_logValidation(s_project_description)) {
                project_ID = s_project_description.split(' ')[0];
                projectName = s_project_description.split(' ')[1];
                s_project_number = search_results[i].getValue(columns[21]);
                //s_project_number = project_ID;
                //s_project_name =  projectName;
            }
			
			/*
            if (!_logValidation(s_practice) && _logValidation(project_ID)) { //&& _logValidation(project_ID)
                projectSearch = getProjectInternalID(project_ID);
                
                if (projectSearch) {
                    var projectRES = projectSearch[0];
                    nlapiLogExecution('DEBUG', 'Project ', projectRES);
                    var departmentLookup = nlapiLookupField('department', parseInt(projectRES.Department), ['name']);
                    s_subPractice = departmentLookup.name;
                    var departmentLookup_parent = nlapiLookupField('department', parseInt(projectRES.Department), ['custrecord_parent_practice'], true);
                    s_parentPractice = departmentLookup_parent.custrecord_parent_practice;
                    s_project_number = search_results[i].getValue(columns[21]);
                  
                }
            } else {
                s_subPractice = s_practice;
                s_parentPractice = search_results[i].getText(columns[15]);
            }
			
            
			if(!_logValidation(s_parentPractice)){
				s_parentPractice = s_subPractice;
			}
           */
			//Calculate the amount into USD based on conversion rate
			amount=_conversionrateIntoUSD(i_subsidiary, amount, category, exhangeRate, a_dataVal[data_indx]);
			
		  


            var isWithinDateRange = true;

            var d_transaction_date = nlapiStringToDate(transaction_date,
                'datetimetz');

            if (s_from != '') {
                var d_from = nlapiStringToDate(s_from, 'datetimetz');

                if (d_transaction_date < d_from) {
                    isWithinDateRange = false;
                }
            }

            if (s_to != '') {
                var d_to = nlapiStringToDate(s_to, 'datetimetz');

                if (d_transaction_date > d_to) {
                    isWithinDateRange = false;
                }
            }

            if (isWithinDateRange == true) {
                var i_index = a_period_list.indexOf(period);

                if (i_index == -1) {
                    if (_logValidation(period)) {
                        a_period_list.push(period);
                    } else {
                        //nlapiLogExecution('debug', 'error', '388');
                    }

                }

                i_index = a_period_list.indexOf(period);
            }
			if(!_logValidation(s_project_number)){
			s_project_number = search_results[i].getValue(columns[16]).split(' ')[0];
			}
            if (category != 'People Cost' && category != 'Discount' && category != 'Facility Cost' && category != 'Revenue') {
                var o_index = a_category_list.indexOf(s_account_name);
                if (o_index != -1) {
                    o_index = o_index - 1;
                    var acct = other_list[o_index].o_acct;
                    var typeofact = other_list[o_index].o_type;


                    if ((_logValidation(period)) && (_logValidation(amount)) && (_logValidation(search_results[i].getValue(columns[4]))) &&
                        (_logValidation(search_results[i].getValue(columns[5]))) && (_logValidation(transaction_type)) &&
                        (_logValidation(transaction_date)) && (_logValidation(isWithinDateRange)) && (_logValidation(s_account_name))) {} else {
                        //  nlapiLogExecution('debug','error','408');
                    }
                    /*if(typeofact == 'Other Direct Cost - Travel'){
                    o_data['Other Cost - Travel'].push({
                        'period' : period,
                        'amount' : amount,
                       'num' : search_results[i].getValue(columns[4]),
                        'memo' : search_results[i].getValue(columns[5]),
                        'type' : transaction_type,
                        'dt' : transaction_date,
                        'include' : isWithinDateRange,
                    	's_account_name' : s_account_name
                    });
                    }
                    else if(typeofact == 'Other Direct Cost - Immigration'){
                    o_data['Other Cost - Immigration'].push({
                        'period' : period,
                        'amount' : amount,
                        'num' : search_results[i].getValue(columns[4]),
                        'memo' : search_results[i].getValue(columns[5]),
                        'type' : transaction_type,
                        'dt' : transaction_date,
                        'include' : isWithinDateRange,
                    	's_account_name' : s_account_name
                    });
                    }*/
                    /*if(typeofact == 'Other Direct Cost - Professional Fees'){
                    o_data['Other Cost - Professional Fees'].push({
                        'period' : period,
                        'amount' : amount,
                        'num' : search_results[i].getValue(columns[4]),
                        'memo' : search_results[i].getValue(columns[5]),
                        'type' : transaction_type,
                        'dt' : transaction_date,
                        'include' : isWithinDateRange,
                    	's_account_name' : s_account_name
                    });
                    }
                    if(typeofact == 'Other Direct Cost - Others'){
                    o_data['Other Cost - Others'].push({
                        'period' : period,
                        'amount' : amount,
                       'num' : search_results[i].getValue(columns[4]),
                       'memo' : search_results[i].getValue(columns[5]),
                        'type' : transaction_type,
                        'dt' : transaction_date,
                        'include' : isWithinDateRange,
                    	's_account_name' : s_account_name
                    });
                    }*/

                }


            } else {
            	if (parseInt(s_account_name) == parseInt(580)) { // Bad Debts
                    if (parseFloat(amount) < 0) {
                        amount = Math.abs(parseFloat(amount));
                    } else {
                        amount = '-' + amount;
                    }
                }
                //avoiding discounts from account						  
               // if (category == 'Revenue' && (i_subsidiary == parseInt(2) || i_subsidiary == parseInt(11) || i_subsidiary == parseInt(12) )) {
                if (category == 'Revenue' && (i_subsidiary == parseInt(2) || i_subsidiary == parseInt(11) || i_subsidiary == parseInt(12) || i_subsidiary == parseInt(7) || i_subsidiary == parseInt(3) || i_subsidiary == parseInt(13) || i_subsidiary == parseInt(14))) {
                    if (parseInt(s_account_name) != parseInt(732)) {
                        o_data[category].push({
                            'project_id': s_project_number,
                          // 	'project_desc' : search_results[i].getValue(columns[16]),
                            'period': period,
                            'amount': amount,
                            'num': search_results[i].getValue(columns[4]),
                          // 	'customer' : search_results[i].getValue(columns[16]),
                            'sub_practice': mis_sub_practice,
                            'practice': mis_parent_practice,
                            'type': transaction_type,
                            'dt': transaction_date
						//	'mis_practice': search_results[i].getValue(columns[23]),
						//	'mis_sub': mis_sub_practice,
						//	'mis_parent': mis_parent_practice
                            //	'include' : isWithinDateRange,
                          //  	's_account_name' : search_results[i].getText(columns[11])
                        });
                       

                    }
                    
                } else if (category == 'Revenue') {
                    o_data[category].push({
                        'project_id': s_project_number,
                     //   	'project_desc' : search_results[i].getValue(columns[16]),
                        'period': period,
                        'amount': amount,

                        'num': search_results[i].getValue(columns[4]),
                        'sub_practice': mis_sub_practice,
                        'practice': mis_parent_practice,

                    //	'customer' : search_results[i].getValue(columns[16]),
                        'type': transaction_type,
                        'dt': transaction_date
					//	'mis_practice': search_results[i].getValue(columns[23]),
					//	'mis_sub': mis_sub_practice,
					//		'mis_parent': mis_parent_practice
                        //    'include' : isWithinDateRange,
                    //    's_account_name' : search_results[i].getText(columns[11])
                    });
                }
	
			if (category == 'Discount') {
                    o_data[category].push({
                        'project_id': s_project_number,
                    //    	'project_desc' : search_results[i].getValue(columns[16]),
                        'period': period,
                        'amount': amount,

                        'num': search_results[i].getValue(columns[4]),
                        'sub_practice': mis_sub_practice,
                        'practice': mis_parent_practice,

                   // 	'customer' : search_results[i].getValue(columns[16]),
                        'type': transaction_type,
                        'dt': transaction_date
					//	'mis_practice': search_results[i].getValue(columns[23]),
					//	'mis_sub': mis_sub_practice,
					//		'mis_parent': mis_parent_practice
                        //    'include' : isWithinDateRange,
                  //      's_account_name' : search_results[i].getText(columns[11])
                    });
                }
                //nlapiLogExecution('audit','cat:- '+category);
            }

        }
        return o_data;
    } catch (e) {
        nlapiLogExecution('DEBUG', 'Actual Data Flow Error', e);
        throw e;
    }
}
//Used to display the html, by replacing the placeholders
function replaceValues(content, oValues) {
    for (param in oValues) {
        // Replace null values with blank
        var s_value = (oValues[param] == null) ? '' : oValues[param];

        // Replace content
        content = content.replace(new RegExp('{{' + param + '}}', 'gi'),
            s_value);
    }

    return content;
}

function createPeriodList(startDate, endDate) {
    var d_startDate = nlapiStringToDate(startDate);
    var d_endDate = nlapiStringToDate(endDate);

    var arrPeriod = [];

    for (var i = 0;; i++) {
        var currentDate = nlapiAddMonths(d_startDate, i);
        if ((_logValidation(getMonthName(currentDate))) && (_logValidation(getMonthStartDate(currentDate))) && (_logValidation(getMonthEndDate(currentDate)))) {} else {
            nlapiLogExecution('debug', 'error', '909');
        }
        arrPeriod.push({
            Name: getMonthName(currentDate),
            StartDate: getMonthStartDate(currentDate),
            EndDate: getMonthEndDate(currentDate)
        });

        if (getMonthEndDate(currentDate) >= d_endDate) {
            break;
        }
    }

    var today = new Date();

    // remove the current month
    if (d_endDate.getMonth() >= today.getMonth()) {
        arrPeriod.pop();
    }

    return arrPeriod;
}

function getMonthEndDate(currentDate) {
    return nlapiAddDays(nlapiAddMonths(currentDate, 1), -1);
}

function getMonthStartDate(currentDate) {
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    var s_startDate = month + "/1/" + year;
    var d_startDate = nlapiStringToDate(s_startDate);
    return d_startDate;
}

function getMonthName(currentDate) {
    var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
        "SEP", "OCT", "NOV", "DEC"
    ];
    return monthNames[currentDate.getMonth()] + " " + currentDate.getFullYear();
}

function getLastDate(month, year) {
    var date = '';
    if (month == 'January')
        date = "1/31/" + year;
    if (month == 'February')
        date = "2/28/" + year;
    if (month == 'March')
        date = "3/31/" + year;
    if (month == 'April')
        date = "4/30/" + year;
    if (month == 'May')
        date = "5/31/" + year;
    if (month == 'June')
        date = "6/30/" + year;
    if (month == 'July')
        date = "7/31/" + year;
    if (month == 'August')
        date = "8/31/" + year;
    if (month == 'September')
        date = "9/30/" + year;
    if (month == 'October')
        date = "10/31/" + year;
    if (month == 'November')
        date = "11/30/" + year;
    if (month == 'December')
        date = "12/31/" + year;

    return date;
}

//Get the complete month name
function getMonthCompleteName(month) {
    var s_mont_complt_name = '';
    if (month == 'Jan')
        s_mont_complt_name = 'January';
    if (month == 'Feb')
        s_mont_complt_name = 'February';
    if (month == 'Mar')
        s_mont_complt_name = 'March';
    if (month == 'Apr')
        s_mont_complt_name = 'April';
    if (month == 'May')
        s_mont_complt_name = 'May';
    if (month == 'Jun')
        s_mont_complt_name = 'June';
    if (month == 'Jul')
        s_mont_complt_name = 'July';
    if (month == 'Aug')
        s_mont_complt_name = 'August';
    if (month == 'Sep')
        s_mont_complt_name = 'September';
    if (month == 'Oct')
        s_mont_complt_name = 'October';
    if (month == 'Nov')
        s_mont_complt_name = 'November';
    if (month == 'Dec')
        s_mont_complt_name = 'December';

    return s_mont_complt_name;
}

function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}

function getSelectedProjects(project) {
    try {



        // get the list of project the user has access to
        var project_filter = [
            [
                ['internalid',
                    'anyOf', project
                ]
            ]
        ];

        var project_search_results = searchRecord('job', null, project_filter, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("companyname"),
            new nlobjSearchColumn("internalid")
        ]);

        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        if (project_search_results.length == 0) {
            throw "You didn't selected any projects.";
        }

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}

function getTaggedProjectList() {
    try {


        // get the list of project the user has access to
        /*var project_filter = [
                [[ 'custbody_projecttype', 'anyOf', 2 ], 'and' ,
                ['startdate',  'notafter',enDate]), 'and',
                ['enddate',  'notbefore',stDate]), 'and',		       
                [ 'status', 'anyOf', 2 ] ];*/

        var project_search_results = searchRecord('job', 'customsearch2430', null, [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("altname"),
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("companyname")
        ]);



        if (project_search_results.length == 0) {
            throw "You don't have any projects under you.";
        }
        nlapiLogExecution('debug', 'project count',
            project_search_results.length);

        return project_search_results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}

function getProjectInternalID(pro_id) {
    try {
        nlapiLogExecution('ERROR', 'pro_id', pro_id);
        var project_search_results = searchRecord('job', null, [new nlobjSearchFilter('entityid', null, 'is', pro_id)], [new nlobjSearchColumn("entityid"),
            new nlobjSearchColumn("custentity_practice"),
            new nlobjSearchColumn("internalid"),
            new nlobjSearchColumn("companyname")
        ]);
        var results = [];
        if (_logValidation(project_search_results)) {
            results.push({
                'ID': project_search_results[0].getValue('internalid'),
                'Department': project_search_results[0].getValue('custentity_practice')
            });
        }
        return results;
    } catch (err) {
        nlapiLogExecution('ERROR', 'getTaggedProjectList', err);
        throw err;
    }
}

function date_time() {
    var date = new Date();
    var date_ = nlapiDateToString(date);
    var am_pm = "am";
    var hour = date.getHours();

    var currentDateTime = new Date();
    var companyTimeZone = nlapiLoadConfiguration('companyinformation').getFieldText('timezone');
    var timeZoneOffSet = (companyTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(companyTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(/:30/gi, '.5'));
    var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
    var companyDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);

    if (hour >= 12) {
        am_pm = "pm";
    }
    if (hour > 12) {
        hour = hour - 12;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }

    var minute = date.getMinutes();
    if (minute < 10) {
        minute = "0" + minute;
    }
    var sec = date.getSeconds();
    if (sec < 10) {
        sec = "0" + sec;
    }

    return date_ + ' ' + hour + ':' + minute + ' ' + am_pm;
}

function removearrayduplicate(array) {
    var newArray = new Array();
    label: for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array.length; j++) {
                if (newArray[j] == array[i])
                      continue label;
          }
          newArray[newArray.length] = array[i];
    }
    return newArray;
}