function Main() {
    try {
        // Store resource allocations employee and their corresponding start and end dates 
        //search id - 4153
        //getNotSubmitted(startDate, endDate, employeeList, getDayWise)

        
        var end_day = '';
        var request_to_date = new Date();
       // var startDateHrs = moment(request_to_date).utcOffset("+05:30").format();
        nlapiLogExecution('debug', 'request_to_date', request_to_date);
		
		var currentTimeStamp = moment(request_to_date).format("DD-MMM-YYYY HH:mm:ss");

      
        //Moment request - 
		var moment_date = moment(request_to_date).utcOffset("+05:30").format("MM/DD/YYYY");
        request_to_date = nlapiStringToDate(moment_date);
		nlapiLogExecution('debug', 'request_to_date', request_to_date);
      
        //return;
        if(request_to_date.getDay()==0){
            end_day = 8;
        } else if(request_to_date.getDay()==1){
            end_day = 9;   
         }
        else{
            end_day = request_to_date.getDay()+1;
        }

        //var end_day = request_to_date.getDay();
        //var sunday_of_end_week = nlapiAddDays(request_to_date, -end_day);
        var d_end_date = nlapiAddDays(request_to_date, -end_day);
        
		var endDate_Str = nlapiDateToString(d_end_date);
        //var d_end_date = nlapiAddDays(sunday_of_end_week, 6);
        
        nlapiLogExecution('debug', 'd_end_date', d_end_date);
      
      //Added -
       d_end_date = nlapiDateToString(d_end_date);
       d_end_date = nlapiStringToDate(d_end_date);
      //-----


        var request_from_date = nlapiAddDays(d_end_date, -97);
        //Added -
       request_from_date = nlapiDateToString(request_from_date);
       request_from_date = nlapiStringToDate(request_from_date);
      //-----
		var request_from_date_Str = nlapiDateToString(request_from_date);
        nlapiLogExecution('debug', 'request_from_date', request_from_date);

        var customerHoliday = CustomerHolidayss(request_from_date_Str,endDate_Str);
        var companyHolidays = HolidayCompany(request_from_date_Str,endDate_Str);
        

       // var startAllocation = nlapiDateToString(request_from_date)
       // var endAllocation = nlapiDateToString(d_end_date)


        //var a_resource_allocations = new Array();
        //var projectList = new Array();
        //projectList.push('161382');
        var filters = new Array();
        filters[0] = new nlobjSearchFilter('custeventbstartdate', null,
            'notafter', d_end_date);
        filters[1] = new nlobjSearchFilter('custeventbenddate', null, 'notbefore',
            request_from_date);
        filters[2] = new nlobjSearchFilter('custeventrbillable', null, 'is', 'T');
        filters[3] = new nlobjSearchFilter('jobbillingtype', 'job', 'anyof', 'TM');
        filters[4] = new nlobjSearchFilter('customer', null, 'noneof',
        '5766');
        //filters[5] = new nlobjSearchFilter('subsidiary', 'employee', 'anyof',['3','13','12']); //BTPL,Standav-India,Cognetik Srl
        
        //filters[5] = new nlobjSearchFilter('resource', null,'anyof', ["324480","310250","271732"]); 
        

        var columns = new Array();

        columns[0] = new nlobjSearchColumn('custeventbstartdate', null, 'group');
        columns[1] = new nlobjSearchColumn('custeventbenddate', null, 'group');
        columns[2] = new nlobjSearchColumn('resource', null, 'group');
        columns[3] = new nlobjSearchColumn('custevent4', null, 'group');
        columns[4] = new nlobjSearchColumn("custentity_hoursperday","job","GROUP");
        columns[5] = new nlobjSearchColumn("custentity_onsite_hours_per_day","job","GROUP");
        columns[6] = new nlobjSearchColumn("percentoftime",null,"avg");
        columns[7] =new nlobjSearchColumn("custentityproject_holiday","job","GROUP")

        columns[8] = new nlobjSearchColumn("customer",null,"GROUP");
        columns[9] = new nlobjSearchColumn("subsidiary","employee","GROUP");
		columns[10] = new nlobjSearchColumn("timeapprover","employee","GROUP");

        try {
            var search_results = searchRecord('resourceallocation', null, filters,
                columns);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }

        var a_data = new Object();

        nlapiLogExecution('debug', 'search_results.length', search_results.length);

        var employeeList = new Array();  //List to store all employee's who have the allocation
        var employeeNameWise = new Object();
        var employeeWeekHrs = new Object();
		var timeApproverList = new Object();

        if (search_results != null && search_results.length > 0) {
            for (var i_search_indx = 0; i_search_indx < search_results.length; i_search_indx++) {
				
                var currentContext_C = nlapiGetContext();
               // nlapiLogExecution('debug', 'usage', currentContext_C.getRemainingUsage());
                 if(currentContext_C.getRemainingUsage() <= 2000){
		    	yieldScript(currentContext_C);
		    	}

                var allocationStartDate = search_results[i_search_indx].getValue(columns[0]);
                var allocationEndDate = search_results[i_search_indx].getValue(columns[1]);
                var s_employee_name = search_results[i_search_indx].getValue(columns[2]);
                var s_employeeName = search_results[i_search_indx].getText(columns[2]);
                var onsite_offsite = search_results[i_search_indx].getValue(columns[3]);
                var offsiteHrs = search_results[i_search_indx].getValue(columns[4]);
                var onsiteHrs = search_results[i_search_indx].getValue(columns[5]);
                var percentTime = search_results[i_search_indx].getValue(columns[6]);
                var projectHoliday = search_results[i_search_indx].getValue(columns[7]);
                var customer = search_results[i_search_indx].getValue(columns[8]);
                var empSubsidiary = search_results[i_search_indx].getValue(columns[9]);
				var timeApprover = search_results[i_search_indx].getValue(columns[10]);
                
                var i_perday_hours = "";

if (onsite_offsite == "1")//Onsite
{
	i_perday_hours = onsiteHrs;
}

if (onsite_offsite == "2")//Offsite
{
	i_perday_hours = offsiteHrs;
}
i_perday_hours = i_perday_hours ? i_perday_hours : 8;

if(i_perday_hours == '- None -'){
    i_perday_hours = 8;

}



                if(!_logValidation(employeeNameWise[s_employee_name])){
                    employeeNameWise[s_employee_name] = s_employeeName;
					timeApproverList[s_employee_name] = timeApprover;
                }

                var d_allocationStartDate = nlapiStringToDate(allocationStartDate);
                var d_allocationEndDate = nlapiStringToDate(allocationEndDate);

                if (_logValidation(d_allocationStartDate) && _logValidation(d_allocationStartDate)) {
                  
                    ////Added to convert from date--string--date
                    request_from_date = nlapiDateToString(request_from_date);
                    request_from_date = nlapiStringToDate(request_from_date);
                  
                    d_end_date = nlapiDateToString(d_end_date);
                    d_end_date = nlapiStringToDate(d_end_date);
                   ///Added ----------------

                    var startDate = d_allocationStartDate > request_from_date ? d_allocationStartDate :
                        request_from_date;
                    var endDate = d_allocationEndDate < d_end_date ? d_allocationEndDate :
                        d_end_date;
                   
                    ////Added to convert from date--string--date
                    startDate = nlapiDateToString(startDate);
                    startDate = nlapiStringToDate(startDate);
                  
                    endDate = nlapiDateToString(endDate);
                    endDate = nlapiStringToDate(endDate);
                   ///Added ----------------
                   
                    var actualAllocationStart = startDate;
                    var actualAllocationEnd = endDate;
                   // nlapiLogExecution('debug', 's_employee_name', s_employee_name);
                   // nlapiLogExecution('debug', 'allocation startDate', startDate);
                    //nlapiLogExecution('debug', 'allocation endDate', endDate);


                   /* if(request_to_date.getDay()==0){
                        end_day = 8;
                    }else{
                        end_day = request_to_date.getDay()+1;
                    }

                    var d_end_date = nlapiAddDays(request_to_date, -end_day);
                    var request_from_date = nlapiAddDays(d_end_date, -69);*/
      
                    if(endDate.getDay()==0){
                        endDate = nlapiAddDays(endDate, -1);

                    }
                    end_day = endDate.getDay();
                    var sunday_of_end_week = nlapiAddDays(endDate, -end_day);

                    
                   // var sunday_of_end_week = nlapiAddDays(endDate, -end_day);
                    endDate = nlapiAddDays(sunday_of_end_week, 6);

                    //startDate = nlapiAddDays(endDate, -69);

                   var end_day1 = startDate.getDay();
				   startDate = nlapiAddDays(startDate, -end_day1);

                   // nlapiLogExecution('debug', 'startDate -- Sunday', startDate);
                   // nlapiLogExecution('debug', 'endDate  --Saturday', endDate);

                   if(_logValidation(employeeWeekHrs[s_employee_name])){
                    
                   // nlapiLogExecution('debug', 'onsite_offsite', onsite_offsite);
                   // nlapiLogExecution('debug', 's_employee_name', s_employee_name);
                  //  nlapiLogExecution('debug', 'JSON.stringify(employeeWeekHrs[s_employee_name])', JSON.stringify(employeeWeekHrs[s_employee_name]));




                   }
                  

                    var weekStartDateList = [];
                    if (a_data[s_employee_name]) {
                        var weekList = a_data[s_employee_name];
                        for (var i = 0;; i += 7) {
                            var d_current_date = nlapiAddDays(startDate, i);
                            var current_end_date = nlapiAddDays(startDate, i+6);
                           // nlapiLogExecution('debug', 'current_end_date', current_end_date);

                            if (d_current_date > endDate ) { //|| d_current_date < startDate
                                break;
                            }

                            var startDateCurrent = nlapiDateToString(d_current_date); //, 'date'

                             /////       -----      Logic for taking hours from allocation week wise ---------------///

                             var startDateHrs = actualAllocationStart > d_current_date ? actualAllocationStart :
                             d_current_date;

                             var endDateHrs = actualAllocationEnd > current_end_date ? current_end_date :
                             actualAllocationEnd;

                             startDateHrs = nlapiDateToString(startDateHrs); //, 'date'
                             //startDateHrs = moment(startDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");//commented for taking the date from NLAPI
                            // startDateHrs = new Date(startDateHrs); //commented for taking the date from NLAPI
                            startDateHrs = nlapiStringToDate(startDateHrs);

                             endDateHrs = nlapiDateToString(endDateHrs); //, 'date'
                             //endDateHrs = moment(endDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                             //endDateHrs = new Date(endDateHrs);
                             endDateHrs = nlapiStringToDate(endDateHrs);

                             var i_working_days = calcBusinessDays(startDateHrs, endDateHrs);
                            // nlapiLogExecution('debug', 'i_working_days', i_working_days);
                             var holidaysCount = 0;

                            // nlapiLogExecution('debug', 'projectHoliday', projectHoliday);

                            // nlapiLogExecution('debug', '---employeeWeekHrs----', JSON.stringify(employeeWeekHrs));

                             if(projectHoliday == 1){

                                 holidaysCount =  companyHolidayCount(startDateHrs,endDateHrs,companyHolidays,empSubsidiary)

                                // nlapiLogExecution('debug', 'Comp holidaysCount--0', holidaysCount);


                             } else{

                                 holidaysCount = CustomerHolidaysCount(startDateHrs,endDateHrs,customerHoliday,customer,empSubsidiary,s_employee_name);
                                // nlapiLogExecution('debug', 'Cust holidaysCount--0', holidaysCount);

                             }
                             // - parseFloat(holidaysCount)

                             //nlapiLogExecution('debug', 'hoursNotSub -- inside---', parseFloat(i_working_days) + ', '+ parseFloat(holidaysCount) + ', '+ parseInt(i_perday_hours) + parseFloat(percentTime));

                             var hoursNotSub = parseFloat(((parseFloat(i_working_days)) - parseFloat(holidaysCount)) * parseInt(i_perday_hours) * parseFloat(percentTime) / 100.0)
                            //nlapiLogExecution('debug', 'hoursNotSub -- inside 1', hoursNotSub);






                             
                             if(!_logValidation(employeeWeekHrs[s_employee_name])){

                                 employeeWeekHrs[s_employee_name] = new Object();
     
                                 if(!_logValidation(employeeWeekHrs[s_employee_name][startDateCurrent])){

                                    //nlapiLogExecution('debug', '1', '1');

     
                                     employeeWeekHrs[s_employee_name][startDateCurrent] = new Object();
                                     employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub
     
     
                                 } else{
                                     var previousHrs = employeeWeekHrs[s_employee_name][startDateCurrent];
                                     employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub + previousHrs;

                                    // nlapiLogExecution('debug', '2', '2');
     
                                 }
     
                             } else {
                                 if(!_logValidation(employeeWeekHrs[s_employee_name][startDateCurrent])){
                                     employeeWeekHrs[s_employee_name][startDateCurrent] = new Object();
                                     //employeeWeekHrs[s_employee_name][startDateCurrent] = previousHrs + hoursNotSub;
                                     employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub;

                                    // nlapiLogExecution('debug', '3', '3');
     
     
                                 } else{
                                     var previousHrs = employeeWeekHrs[s_employee_name][startDateCurrent];
                                     employeeWeekHrs[s_employee_name][startDateCurrent] = previousHrs + hoursNotSub;
                                     //employeeWeekHrs[s_employee_name][startDateCurrent] = hoursNotSub;

                                     //nlapiLogExecution('debug', '4', '4');
     
                                 }
     
                             }

                             //nlapiLogExecution('debug', '[hoursNotSub]', JSON.stringify(employeeWeekHrs));









                             /// --            Logic for taking hours from allocation week wise -------------////









                            if (weekList.indexOf(startDateCurrent) == -1) {  //&& d_current_date>=request_from_date && d_current_date<=d_end_date
                               
                                weekList.push(startDateCurrent); //For timeSheet weeks
                               
                               
                                
                                



                            } //indexof finished


                        }

                        a_data[s_employee_name] = weekList;


                    } else {
                        for (var i = 0;; i += 7) {
                            var d_current_date = nlapiAddDays(startDate, i);
                            var current_end_date = nlapiAddDays(startDate, i+6);
                            //nlapiLogExecution('debug', 'current_end_date', current_end_date);

                           // nlapiLogExecution('debug', 'd_current_date', d_current_date);
                           // nlapiLogExecution('debug', 'endDate', endDate);

                            if (d_current_date > endDate) { //|| d_current_date < startDate
                                break;
                            }

                         /// if(d_current_date>=request_from_date && d_current_date<=d_end_date){  removed if condition
                         var currentDateStr = nlapiDateToString(d_current_date, 'date');
                            weekStartDateList
                            .push(nlapiDateToString(d_current_date, 'date'));

                        //  }
                        
                        ////added to convert from date--string--date
                        d_current_date = nlapiDateToString(d_current_date);
                        d_current_date = nlapiStringToDate(d_current_date);
                        current_end_date = nlapiDateToString(current_end_date);
                        current_end_date = nlapiStringToDate(current_end_date);
                          
                        /////
                        
                        var startDateHrs = actualAllocationStart > d_current_date ? actualAllocationStart :
                        d_current_date;

                        var endDateHrs = actualAllocationEnd > current_end_date ? current_end_date :
                        actualAllocationEnd;

                        startDateHrs = nlapiDateToString(startDateHrs); //, 'date'
                             //startDateHrs = moment(startDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                            // startDateHrs = new Date(startDateHrs); commented for  taking date from nlapi
                             startDateHrs = nlapiStringToDate(startDateHrs)

                             endDateHrs = nlapiDateToString(endDateHrs);//, 'date'
                             //endDateHrs = moment(endDateHrs).utcOffset("+05:30").format("MM/DD/YYYY");
                             //endDateHrs = new Date(endDateHrs);
                             endDateHrs = nlapiStringToDate(endDateHrs)

                         //nlapiLogExecution('debug', 'startDateHrs + endDateHrs', startDateHrs + ', '+endDateHrs);

                        var i_working_days = calcBusinessDays(startDateHrs, endDateHrs);
                       // nlapiLogExecution('debug', 'i_working_days', i_working_days);
                        var holidaysCount = 0;

                        //nlapiLogExecution('debug', 'projectHoliday', projectHoliday);

                        if(projectHoliday == 1){

                            holidaysCount =  companyHolidayCount(startDateHrs,endDateHrs,companyHolidays,empSubsidiary)

                         //   nlapiLogExecution('debug', 'Comp holidaysCount--1', holidaysCount);


                        } else{

                            holidaysCount = CustomerHolidaysCount(startDateHrs,endDateHrs,customerHoliday,customer,empSubsidiary,s_employee_name);

                           // nlapiLogExecution('debug', 'Cust holidaysCount--1', holidaysCount);

                        }

                        //nlapiLogExecution('debug', 'Cust holidaysCount--1', parseFloat(i_working_days) +', '+parseFloat(holidaysCount)+', '+i_perday_hours+', '+parseFloat(percentTime));

                        var hoursNotSub = parseFloat(((parseFloat(i_working_days)) - parseFloat(holidaysCount)) * parseInt(i_perday_hours) * parseFloat(percentTime) / 100.0)
                        
                        
                        






                     
                        
                        if(!_logValidation(employeeWeekHrs[s_employee_name])){

                            employeeWeekHrs[s_employee_name] = new Object();

                            if(!_logValidation(employeeWeekHrs[s_employee_name][currentDateStr])){

                                employeeWeekHrs[s_employee_name][currentDateStr] = new Object();
                                employeeWeekHrs[s_employee_name][currentDateStr] = hoursNotSub

                                //nlapiLogExecution('debug', '00', '00');


                            } else{
                                var previousHrs = employeeWeekHrs[s_employee_name][currentDateStr];
                                employeeWeekHrs[s_employee_name][currentDateStr] = hoursNotSub + previousHrs;
                                //nlapiLogExecution('debug', '01', '01');

                            }

                        } else {
                            if(!_logValidation(employeeWeekHrs[s_employee_name][currentDateStr])){
                                employeeWeekHrs[s_employee_name][currentDateStr] = new Object();
                                //employeeWeekHrs[s_employee_name][startDateCurrent] = previousHrs + hoursNotSub;
                                employeeWeekHrs[s_employee_name][currentDateStr] = hoursNotSub;
                                //nlapiLogExecution('debug', '02', '02');


                            } else{
                                var previousHrs = employeeWeekHrs[s_employee_name][currentDateStr];
                                employeeWeekHrs[s_employee_name][startDateCurrent] = previousHrs + hoursNotSub;
                                //nlapiLogExecution('debug', '03', '03');
                                //employeeWeekHrs[s_employee_name][currentDateStr] =  hoursNotSub;

                            }

                        }

                        

                            
                        }

                    }


                    //nlapiLogExecution('debug', 'weekStartDateList', JSON.stringify(weekStartDateList));



                    if (a_data[s_employee_name] == undefined) {
                        // a_data[s_employee_name] = new Array();
                        a_data[s_employee_name] = weekStartDateList;

                    }

                    if (employeeList.indexOf(s_employee_name) == -1) {
                        employeeList.push(s_employee_name);

                    }

                }




            } // for loop ends here -- 

            nlapiLogExecution('debug', 'employeeWeekHrs', JSON.stringify(employeeWeekHrs));

            //nlapiSendEmail(442,320475, 'TS - notification - emp -test', JSON.stringify(a_data), null, null, null, null);

           // nlapiLogExecution('debug', 'a_data', JSON.stringify(a_data));



        }

        // nlapiLogExecution('debug', 'employeeWeekHrs', JSON.stringify(employeeWeekHrs));

        //employeeWeekHrs

        //employeeList -- list of all employees 
        //

        var filtersTS = new Array();
        filtersTS[0] = new nlobjSearchFilter('timesheetdate', null, 'within', request_from_date,
            d_end_date);
        filtersTS[1] = new nlobjSearchFilter('employee', null, 'anyof',
            employeeList);
        filtersTS[2] = new nlobjSearchFilter('approvalstatusicon', null, 'anyof',
            "2", "3");

        var columnsTS = new Array();

        columnsTS[0] = new nlobjSearchColumn('startdate', null, 'group');
        columnsTS[1] = new nlobjSearchColumn('enddate', null, 'group');
        columnsTS[2] = new nlobjSearchColumn('employee', null, 'group').setSort(true);

        try {
            var search_results_TS = searchRecord('timesheet', null, filtersTS,
                columnsTS);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }

        var prevEmpID = '';

        var empListSubmitted = new Array();
        var empListNS = new Object();
        var empSubmittedOnce = new Array();

        if (search_results_TS != null && search_results_TS.length > 0) {
            for (var i_search_indx = 0; i_search_indx < search_results_TS.length; i_search_indx++) {

                var currEmpID = search_results_TS[i_search_indx].getValue(columns[2]);

                
                if (i_search_indx == 0) {
                    prevEmpID = currEmpID;

                }
				
				var currentContext_C = nlapiGetContext();
                 if(currentContext_C.getRemainingUsage() <= 2000){
		    	yieldScript(currentContext_C);
		    	}
               // nlapiLogExecution('DEBUG', 'test',i_search_indx);
               // nlapiLogExecution('DEBUG', 'currEmpID + prevEmpID', currEmpID+' '+prevEmpID);
              //  nlapiLogExecution('DEBUG', 'prevEmpID---------', JSON.stringify(a_data[prevEmpID]));
              //   nlapiLogExecution('DEBUG', 'currEmpID--------', JSON.stringify(a_data[currEmpID]));
                if(currEmpID=='10393' || currEmpID==10393){
                  var test = search_results_TS[i_search_indx].getValue(columns[0]);
                //   nlapiLogExecution('DEBUG', 'test', test +', ' +i_search_indx);
                //   nlapiLogExecution('DEBUG', 'empListSubmitted', JSON.stringify(empListSubmitted));
                //   nlapiLogExecution('DEBUG', 'currEmpID + prevEmpID', currEmpID+' '+prevEmpID);
                }

                if(empSubmittedOnce.indexOf(currEmpID)==-1){
                    empSubmittedOnce.push(currEmpID);
                }
                var start = search_results_TS[i_search_indx].getValue(columns[0]);

                if(i_search_indx==25){
                  //  nlapiLogExecution('DEBUG', 'currEmpID + prevEmpID', currEmpID+' '+prevEmpID);
                  //  nlapiLogExecution('DEBUG', 'prevEmpID---------', JSON.stringify(a_data[prevEmpID]));
                  //  nlapiLogExecution('DEBUG', 'currEmpID--------', JSON.stringify(a_data[currEmpID]));

                }

               

                if (currEmpID != prevEmpID) {

                    if(prevEmpID=='10393' || prevEmpID==10393){
                        var test = search_results_TS[i_search_indx].getValue(columns[0]);
                       // nlapiLogExecution('DEBUG', 'test', test);
                       // nlapiLogExecution('DEBUG', 'a_data[prevEmpID]', JSON.stringify(a_data[prevEmpID]));
                       // nlapiLogExecution('DEBUG', 'currEmpID + prevEmpID', currEmpID+' '+prevEmpID);
                      }

                    if (a_data[prevEmpID]) {
                        var RAData = a_data[prevEmpID];
                        var empNotSubmitted = inAButNotInB(RAData, empListSubmitted);
                        if(empNotSubmitted.length > 0){
                            empListNS[prevEmpID] = empNotSubmitted;
                        }

                        if(prevEmpID=='10393' || prevEmpID==10393){
                          //  var test = search_results_TS[i_search_indx].getValue(columns[0]);
                           // nlapiLogExecution('DEBUG', 'empNotSubmitted', empNotSubmitted);
                          //  nlapiLogExecution('DEBUG', 'empListSubmitted', JSON.stringify(empListNS[prevEmpID]));
                          }


                        

                    }

                    empListSubmitted = [];
                    empListSubmitted.push(start);

                } else {

                    empListSubmitted.push(start);

                }

                prevEmpID = currEmpID;

                




            }
        }

            if(empListSubmitted.length>0){
                var empNotSubmittedd = inAButNotInB(a_data[currEmpID], empListSubmitted);
                if(empNotSubmittedd.length>0){
                    empListNS[prevEmpID] = empNotSubmittedd;
                }
                
                //empSubmittedOnce.push(prevEmpID);

            }

            var notSumbittedAtAll = inAButNotInB(employeeList, empSubmittedOnce);
            if(notSumbittedAtAll.length > 0){
                for(var nsub=0; nsub<notSumbittedAtAll.length;nsub++){
                    empListNS[notSumbittedAtAll[nsub]] = a_data[notSumbittedAtAll[nsub]];
                }
            }
        

        //nlapiSendEmail(442,320475, 'TS - notification - emp -Not Submitted- test', JSON.stringify(empListNS), null, null, null, null);

       // nlapiLogExecution('debug', 'empListNS', JSON.stringify(empListNS));

        sendNotificationNotSubmitted(empListNS,employeeNameWise,employeeWeekHrs,timeApproverList,currentTimeStamp);
		
		//var NSArray = getNotSubmitted(request_from_date_Str, endDate_Str, empSubmittedOnce)
       // nlapiLogExecution('debug', 'employeeWeekHrs', JSON.stringify(employeeWeekHrs));
       // nlapiSendEmail(442,320475, 'TS - not -employeeHrs', JSON.stringify(employeeWeekHrs), null, null, null, null);
        //nlapiSendEmail(442, 210240, 'TS - not -Submitted - Array', JSON.stringify(NSArray), null, null, null, null);
        


    } catch (err) {
        nlapiLogExecution('error', 'Main', err);
    }
}



function searchRecord(recordType, savedSearch, arrFilters, arrColumns,
    filterExpression) {

    try {
        var search = null;

        // if a saved search is provided, load it and add the filters and
        // columns
        if (isNotEmpty(savedSearch)) {
            search = nlapiLoadSearch(recordType, savedSearch);

            if (isArrayNotEmpty(arrFilters)) {
                search.addFilters(arrFilters);
            }

            if (isArrayNotEmpty(arrColumns)) {
                search.addColumns(arrColumns);
            }

            if (isArrayNotEmpty(filterExpression)) {
                search.setFilterExpression(filterExpression);
            }
        }
        // create a new search
        else {
            search = nlapiCreateSearch(recordType, arrFilters, arrColumns);
        }

        // run search
        var resultSet = search.runSearch();

        // iterate through the search and get all data 1000 at a time
        var searchResultCount = 0;
        var resultSlice = null;
        var searchResult = [];

        do {
            resultSlice = resultSet.getResults(searchResultCount,
                searchResultCount + 1000);

            if (resultSlice) {

                resultSlice.forEach(function(result) {

                    searchResult.push(result);
                    searchResultCount++;
                });
            }
        } while (isArrayNotEmpty(resultSlice) && resultSlice.length >= 1000);

        return searchResult;
    } catch (err) {
        nlapiLogExecution('ERROR', 'searchRecord', err);
        throw err;
    }
}

function isArrayEmpty(argArray) {

    return !isArrayNotEmpty(argArray);
}

function isArrayNotEmpty(argArray) {

    return (isNotEmpty(argArray) && argArray.length > 0);
}

function isEmpty(value) {

    return value == null || value == "" || typeof(value) == undefined;
}

function isNotEmpty(value) {

    return !isEmpty(value);
}

function isTrue(value) {

    return value == 'T';
}

function isFalse(value) {

    return value != 'T';
}


function _logValidation(value) {
    if (value != '- None -' && value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined' && value.toString() != 'NaN' && value != NaN) {
        return true;
    } else {
        return false;
    }
}


function inAButNotInB(A, B) {
    return _.filter(A, function(a) {
        return !_.contains(B, a);
    });
}

function sendNotificationNotSubmitted(empListNS,employeeNameWise,employeeWeekHrs,timeApproverList,currentTimeStamp){

var currentdate = new Date();
var currentYear = currentdate.getFullYear();


/*var employeeSubmission = searchRecord('timesheet','customsearch4171');
var employeesSLA = new Object();

var columns = new Array();

columns[0] = new nlobjSearchColumn("employee",null,"GROUP").setSort(true);
columns[1] = new nlobjSearchColumn("startdate",null,"GROUP");



if (employeeSubmission != null && employeeSubmission.length > 0) {
            for (var i_search_indx = 0; i_search_indx < employeeSubmission.length; i_search_indx++) {

                var s_employee_name = employeeSubmission[i_search_indx].getValue(columns[0]);
                var startDateCurrent = employeeSubmission[i_search_indx].getValue(columns[1]);
				
				if(!_logValidation(employeesSLA[s_employee_name])){

                employeesSLA[s_employee_name] = new Array();
				employeesSLA[s_employee_name].push(startDateCurrent);
     
                                 
     
                } else {
					
					employeesSLA[s_employee_name].push(startDateCurrent);
					
				} 
                
				
				}
}

nlapiLogExecution('debug', 'employeesSLA', JSON.stringify(employeesSLA));
*/


  




    /*var filtersTSFilterSLA = new Array();
    filtersTSFilterSLA[0] = new nlobjSearchFilter('custrecord_ts_outside_sla', null,
            'is', "T");
        
        var columnsTSSLA = new Array();

        columnsTSSLA[0] = new nlobjSearchColumn('employee', null, 'group');
        columnsTSSLA[1] = new nlobjSearchColumn('internalid', null, 'COUNT');
		
		try {
            var search_results_SLA = searchRecord('timesheet', null, filtersTSFilterSLA,
                columnsTSSLA);
        } catch (e) {
            nlapiLogExecution('ERROR', 'Error', e.message);
        }
		
		var empTSSLA = new Object();
		
		if (search_results_SLA != null && search_results_SLA.length > 0) {
            for (var i_search_indx = 0; i_search_indx < search_results_SLA.length; i_search_indx++) {

                var employeeSLA = search_results_SLA[i_search_indx].getValue(columnsTSSLA[0]);
                var countSLA = search_results_SLA[i_search_indx].getValue(columnsTSSLA[1]);
				
				if(!_logValidation(empTSSLA[employeeSLA])){
                    empTSSLA[employeeSLA] = countSLA
                }
				
			}
        }*/
        

     

    for(var emp in empListNS){
		
		var ccTime = null;
		
        var currentContext_C = nlapiGetContext();
        //nlapiLogExecution('debug', 'usage', currentContext_C.getRemainingUsage())
                 if(currentContext_C.getRemainingUsage() <= 2000){
		    	yieldScript(currentContext_C);
		    	}

        //var presentOutsideSLA = new Array();

        var EmployeeName = employeeNameWise[emp].split("-");
        if(EmployeeName.length>1){
            EmployeeName = EmployeeName[1];
        }

        var tableInitial = '';
        tableInitial +='<!DOCTYPE html>';
tableInitial +='<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">'
tableInitial +='<head>';
tableInitial +='<meta charset="utf-8">';
tableInitial +='<meta name="viewport" content="width=device-width,initial-scale=1">';
tableInitial +='<meta name="x-apple-disable-message-reformatting">';
tableInitial +='<title></title>';
tableInitial +='<!--[if mso]>';
tableInitial +='  <style>';
tableInitial +='    table {border-collapse:collapse;border-spacing:0;border:none;margin:0;}';
tableInitial +='    div, td {padding:0;}';
tableInitial +='    div {margin:0 !important;}';
tableInitial +='	</style>';
tableInitial +='  <noscript>';
tableInitial +='    <xml>';
tableInitial +='      <o:OfficeDocumentSettings>';
tableInitial +='        <o:PixelsPerInch>96</o:PixelsPerInch>';
tableInitial +='      </o:OfficeDocumentSettings>';
tableInitial +='    </xml>';
tableInitial +='  </noscript>';
tableInitial +='  <![endif]-->';
tableInitial +='  <style>';
tableInitial +='table, td, div, h1, p {';
tableInitial +='font-family: Calibri;';
tableInitial +='    }';
tableInitial +='    @media screen and (max-width: 530px) {';
tableInitial +='      .unsub {';
tableInitial +='        display: block;';   
tableInitial +='        padding: 8px;';
tableInitial +='        margin-top: 14px;';
tableInitial +='        border-radius: 6px;';
tableInitial +='        background-color: #555555;';
tableInitial +='       text-decoration: none !important;';
tableInitial +='        font-weight: bold;';
tableInitial +='      }';
tableInitial +='      .col-lge {';
tableInitial +='        max-width: 100% !important;';
tableInitial +='      }';
tableInitial +='    }';                                     
tableInitial +='    @media screen and (min-width: 531px) {';
tableInitial +='      .col-sml {';                  
tableInitial +='       max-width: 27% !important;';
tableInitial +='     }';
tableInitial +='      .col-lge {';
tableInitial +='        max-width: 73% !important;';
tableInitial +='      }';
tableInitial +='    }';
tableInitial +='<style>   .pushable {     background: hsl(340deg 100% 32%);     border-radius: 12px;     border: none;     padding: 0;     cursor: pointer;     outline-offset: 4px;   }   .front {     display: block;     padding: 12px 42px;     border-radius: 12px;     font-size: 1.25rem;     background: hsl(345deg 100% 47%);     color: white;     transform: translateY(-6px);   }    .pushable:active .front {     transform: translateY(-2px);   } </style>'
tableInitial +='  </style>';
tableInitial +='</head>';
tableInitial +='<body style="margin:0;padding:0;word-spacing:normal;">';                                                                                                    
tableInitial +='  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">';
tableInitial +='    <table role="presentation" style="width:100%;border:none;border-spacing:0;">';
tableInitial +='      <tr>';                                    
tableInitial +='        <td align="center" style="padding:0;">';
tableInitial +='          <!--[if mso]>';                                                   
tableInitial +='          <table role="presentation" align="center" style="width:600px;">';
tableInitial +='          <tr>';
tableInitial +='          <td>';
tableInitial +='          <![endif]-->';                                                                                                                                                                            
tableInitial +='          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Calibri;font-size:16px;line-height:22px;color:#363636;">';
tableInitial +='            <tr>';                                                                                          
tableInitial +='              <td style="padding:40px 30px 30px 30px;text-align:center;font-size:24px;font-weight:bold;">';
tableInitial +='              </td>';
tableInitial +='           </tr>';
tableInitial +='            <tr>';                                                  
tableInitial +='              <td style="padding:30px;background-color:#ffffff;">';                                                                                                                                                                                                         
tableInitial +='                <img src="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4760022&c=3883006_SB1&h=b_YK6us2YfICYXOfZtbEHKUDney9QG4KdyzLh6vj5TcW_Kso" width="100%" alt="Logo" style="max-width:100%;height:auto;border:none;text-decoration:none;color:#ffffff;">';
tableInitial +='                 <p><span style="font-family:"calibri";font-size:11pt">Dear <strong>'+EmployeeName+'</strong>,</span></p>';                                                       
tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">Submitting weekly timesheets on schedule is critical for our financial operations, including billing and revenue recognition, in addition to compliance with client contractual requirements.&nbsp; As a reminder, Timesheets are due each week on Fridays. If not completed by the following Monday end-of-day (PST), the timesheet will be considered late.&nbsp;</span></p>';
tableInitial +='  <p><span style="font-family:"calibri";font-size:11pt"><strong>Please find below the list of timesheet submissions that are showing as late as per our records.</strong>&nbsp;We request you review the list and, if appropriate, submit the missing timesheets immediately. If you need any assistance, <a href="https://brillioonline.sharepoint.com/sites/Digitaloffice/Shared Documents/Forms/AllItems.aspx?id=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support%2FTimesheet%20Management%2Epdf&parent=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support&p=true">click here for instructions on submitting your timesheet</a> or reach out to timesheet@brillio.com.</span></p> ';	
tableInitial +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://onthego.brillio.com/dashboard/timesheet/apply-timesheet">Click here to submit your timesheet(s) in OnTheGo</a></span></p> ';
tableInitial +='   <div style="overflow-x:auto;">';
tableInitial +='<table style="border:1px solid black;width:96%;margin: 0 auto;"><tbody><tr><th style="border:1px solid black;text-align:left"><b>Timesheet Week</b></th><th style="border:1px solid black ;text-align:center"><b>No. of Hours</b></th>'

        //tableInitial = '<p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">Dear <strong>'+EmployeeName+',</strong></span></p>'
        //tableInitial += '<p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">&nbsp;</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">Having timesheets filled in diligently, at the end of every week, is an important expectation from all our employees since timesheet data is critical for ensuring compliance with contractual requirements and to provide timely, accurate information to our client executives.&nbsp;Timesheet are also an important source of data for project management tasks including schedule and effort tracking. &nbsp;</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">&nbsp;</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">This is to bring to your attention that the timesheets for the below specified weeks have not been filled in. Timesheets are required to be filled in for every week, no later than end of business on the coming Monday.</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">&nbsp;</span></p> ';
      //tableInitial += '<style> table, th, td {   border:1px solid black; } </style> <table >   <tr>     <th><b>Sl.No</b></th>     <th><b>Timesheet Week</b></th> <th text-align:center;><b> No. of Hours</b></th>   </tr>'

        var missingArray = empListNS[emp];
        var sum_hours_NotSubmitted = 0;

        for(var index=0;index<missingArray.length;index++){

            var startDate = missingArray[index];
			 var dateStart = startDate.split("/");
            var dateStartWeek = dateStart[1]+'-'+dateStart[0]+'-'+dateStart[2];
            //var yearComparision = startDate.split('/')[2];
            //if(yearComparision==currentYear.toString()){
            //    presentOutsideSLA.push(startDate);
           // }
             startDated = nlapiStringToDate(startDate);
             var startDateFormatted = moment(startDated).format("DD-MMM-YYYY");
             var d_endDate = nlapiAddDays(startDated, 6);
             var endDateFormatted = moment(d_endDate).format("DD-MMM-YYYY");
             d_endDate = nlapiDateToString(d_endDate, 'date');
			 
			 var dateEnd = d_endDate.split("/");
             var dateEndWeek = dateEnd[1]+'-'+dateEnd[0]+'-'+dateEnd[2];

             var notSubmittedHr = employeeWeekHrs[emp][startDate];
             sum_hours_NotSubmitted+=parseInt(notSubmittedHr);
             
			 if(notSubmittedHr > 0){
				 tableInitial +='<tr><td style="border:1px solid black">'+startDateFormatted+' - '+ endDateFormatted+'</td><td style="border:1px solid black;text-align:center">'+notSubmittedHr+'</td></tr>';
               // tableInitial += '<tr> <td>'+parseInt(index+1)+'</td> <td> <a href="https://onthego.brillio.com/dashboard/timesheet/apply-timesheet" style="text-decoration: none;">'+startDate+' - '+ d_endDate+'</a></td> <td>'+notSubmittedHr+'</td> </tr>'
            }
            // tableInitial +='<li><span style="color: #000000;"><strong><span style="font-family: Calibri; font-size: 11pt;">'+startDate+' - '+ d_endDate+'&nbsp; &nbsp;- '+notSubmittedHr +' hrs</span></strong></span></li>'

             


        }
      
       tableInitial +='<tr><td style="border:1px solid black"><b> Total Hours</b></td><td style="border:1px solid black;text-align:center"><b>'+sum_hours_NotSubmitted+'</b></td></tr>';
       tableInitial +='</tbody></table>';
	   
	   //tableInitial +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://onthego.brillio.com/dashboard/timesheet/apply-timesheet">Click here to submit your timesheet(s) in OnTheGo</a></span></p> ';
      //tableInitial +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://brillioonline.sharepoint.com/sites/Digitaloffice/Shared Documents/Forms/AllItems.aspx?id=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support%2FTimesheet%20Management%2Epdf&parent=%2Fsites%2FDigitaloffice%2FShared%20Documents%2FOnTheGo%20Support&p=true">Click here for instructions on submitting your timesheet</a></span></p> ';
      //tableInitial +='<p><span style="font-family:"calibri";font-size:11pt"><a href="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4768027&c=3883006_SB1&h=f2ERYrjvjrAq8mJDuO1FDku8pW7Bi0JoE0fVqczU-BQosEZM&_xt=.pdf">Click here for instructions on submitting your timesheet</a></span></p> ';
	   //Click here to submit your timesheet in OnTheGo
       tableInitial +='</div>';

        var notSubmittedcount = 0;
        //var notSubmittedFromRecords = new Array();

        //if(_logValidation(employeesSLA[emp])){
        //    notSubmittedFromRecords = employeesSLA[emp];

       // }

       //var SLABreach =  _.union(notSubmittedFromRecords,presentOutsideSLA);
      // if(SLABreach.length>0){
      //  notSubmittedcount = SLABreach.length;
      // }



        //tableInitial += '<p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">&nbsp;</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">You are receiving this email because you have one or more incomplete timesheets. If you need any assistance or believe you have completed all required timesheet updates, please reach out to <em>timesheet@brillio.com</em> for assistance. &nbsp;</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;"></span></p><p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;"></span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">We look forward to your understanding and cooperation</span></p> <p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;">&nbsp;</span></p>';
        //tableInitial += '<p>&nbsp;</p><p><span style="color: #4472c4; font-family: Calibri; font-size: 11.0pt;"><strong>Timesheet Policy:</strong>&nbsp;Updating timesheet in the Brillio timesheet tracking system, on a weekly basis, is mandatory for all Brillians and is due on Friday of each week.&nbsp;Managers (Timesheet Approvers) are responsible to ensure that timesheet for their team is updated accurately and on time and the same is approved before the Thursday of the subsequent week.</span></p>';
        
//		tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">&nbsp;</span></p> ';
//tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">We request you submit the missing timesheets immediately. If you need any assistance or believe you have submitted all required timesheets, please reach out to <em>timesheet@brillio.com</em> for assistance. &nbsp;</span></p>';
//tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt"></span></p>';
tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt"></span></p> ';
tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">Thank you in advance for your cooperation.</span></p>';
tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">&nbsp;</span></p>';
//tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">Dated: '+currentTimeStamp+' PST</span></p>';
tableInitial +='   <p><span style="font-family:"calibri";font-size:11pt">Regards,</span></p>';
tableInitial +='  <p><strong><span style="font-family:"calibri";font-size:11pt">Brillio HR</span></strong></p>';
tableInitial +='  <p><img src="https://3883006-sb1.app.netsuite.com/core/media/media.nl?id=4776380&c=3883006_SB1&h=7VyKjfer-59HR7fdi1j-6Zm7AGXJ0yHs-OAhl3yEE74tv_pg" alt="Policy." width="100%">';
tableInitial +='</p>';
             
tableInitial +='              </td>';
tableInitial +='            </tr>';           
tableInitial +='            <!--tr>';
tableInitial +='              <td style="padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">';
tableInitial +='                <!--[if mso]>';
tableInitial +='                <table role="presentation" width="100%">';
tableInitial +='                <tr>';
tableInitial +='                <td style="width:145px;" align="left" valign="top">';
tableInitial +='               <![endif]-->';                
tableInitial +='                <!--[if mso]>';
tableInitial +='                </td>';
tableInitial +='                <td style="width:395px;padding-bottom:20px;" valign="top">';
tableInitial +='                <![endif]-->';
             
tableInitial +='               <!--[if mso]>';
tableInitial +='                </td>';
tableInitial +='                </tr>';
tableInitial +='                </table>';
tableInitial +='                <![endif]-->';
tableInitial +='              <!--/td>';
tableInitial +='            <!--/tr>';
tableInitial +='          </table>';
tableInitial +='          <!--[if mso]>';
tableInitial +='          </td>';
tableInitial +='          </tr>';
tableInitial +='          </table>';
tableInitial +='          <![endif]-->';
tableInitial +='        </td>';
tableInitial +='      </tr>';
tableInitial +='    </table>';
tableInitial +='  </div>';
tableInitial +='</body>';
tableInitial +='</html>';
    
       // var tableInitial = '<!DOCTYPE html> <html> <head> <style> table, th, td {   border: 1px solid white;   border-collapse: collapse; } th{   background-color: #C0C0C0; } </style> </head> <body>  <h2>Alert !!! Open Timesheet in your bucket </h2>  <p> Please act on the below Timesheet immediately - </p>';
       // tableInitial += '<p>This is to bring to your attention that the time sheets for the below specified weeks have not been filled in. Time sheets are required to be filled in for every week, no later than end of business on the coming Monday.</p>';   
       // tableInitial += '<table style="width:100%"> <tr>  <th>Start Date</th> <th>End Date</th> </tr>';
        
        var notSubmittedWeeks = empListNS[emp];
		if(_logValidation(timeApproverList[emp])){
			ccTime = timeApproverList[emp];
		}
       // nlapiLogExecution('DEBUG', 'emp', emp);
       // nlapiLogExecution('DEBUG', 'emp-Name', employeeNameWise[emp]);
       // nlapiLogExecution('DEBUG', 'notSubmittedWeeks', JSON.stringify(notSubmittedWeeks));
		// if(sum_hours_NotSubmitted >0){
        // nlapiSendEmail(442,320475, 'Missing Timesheet alert ', tableInitial, null, null, null, null);
		// }
        
        
            if(sum_hours_NotSubmitted > 0){
                try{
                nlapiSendEmail(442, parseInt(emp), 'Missing Timesheet alert ', tableInitial, ccTime, null, null, null);
            } catch(e){
                nlapiLogExecution('ERROR', 'Error', e.message);

            }
            }
        
		


    }

}


function calcBusinessDays(d_startDate, d_endDate) { // input given as Date

	// objects
	var startDate = new Date(d_startDate.getTime());
	var endDate = new Date(d_endDate.getTime());
	// Validate input
	if (endDate < startDate)
		return 0;

	// Calculate days between dates
	var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
	startDate.setHours(0, 0, 0, 1); // Start just after midnight
	endDate.setHours(23, 59, 59, 999); // End just before midnight
	var diff = endDate - startDate; // Milliseconds between datetime objects
	var days = Math.ceil(diff / millisecondsPerDay);

	// Subtract two weekend days for every week in between
	var weeks = Math.floor(days / 7);
	var days = days - (weeks * 2);

	// Handle special cases
	var startDay = startDate.getDay();
	var endDay = endDate.getDay();

	// Remove weekend not previously removed.
	if (startDay - endDay > 1)
		days = days - 2;

	// Remove start day if span starts on Sunday but ends before Saturday
	if (startDay == 0 && endDay != 6)
		days = days - 1

		// Remove end day if span ends on Saturday but starts after Sunday
	if (endDay == 6 && startDay != 0)
		days = days - 1
	
	if(days>5){
      days = 5;
    }

	return days;

}


function HolidayCompany(request_from_date_Str,endDate_Str){

    var filterHoliday = new Array();
            filterHoliday[0] = new nlobjSearchFilter('custrecord_date', null, 'within', request_from_date_Str,
                endDate_Str);
                
                var holidayMaster = new Object();
            
            
    
            var columnsHoliday = new Array();
    
            columnsHoliday[0] = new nlobjSearchColumn("custrecord_date");
            columnsHoliday[1] = new nlobjSearchColumn("custrecordsubsidiary").setSort(true);
            columnsHoliday[2] = new nlobjSearchColumn("custrecord_holiday_location");
            
    
            try {
                var search_results_Holiday = searchRecord('customrecord_holiday', null, filterHoliday,
                    columnsHoliday);
            } catch (e) {
                nlapiLogExecution('ERROR', 'Error', e.message);
            }
            
            //var holidayObj = 
            
            
             if (search_results_Holiday != null && search_results_Holiday.length > 0) {
                for (var i_search_indx = 0; i_search_indx < search_results_Holiday.length; i_search_indx++) {
    
                    var date = search_results_Holiday[i_search_indx].getValue(columnsHoliday[0]);
                    var subsidiary = search_results_Holiday[i_search_indx].getValue(columnsHoliday[1]);
                    var location = search_results_Holiday[i_search_indx].getValue(columnsHoliday[2]);
                    
                    if(!holidayMaster[subsidiary]){
                    holidayMaster[subsidiary] = new Array();
                    holidayMaster[subsidiary].push(date)
               
                    } else {
                    holidayMaster[subsidiary].push(date)
                    }
                    
                    }
                    }
      
      return holidayMaster;
                    
        }
    
    function CustomerHolidayss(start_date,end_date){
    
    var customerholidaySearch = nlapiSearchRecord("customrecordcustomerholiday",null,
    [
       ["custrecordholidaydate","within",start_date,end_date]
    ], 
    [
       new nlobjSearchColumn("custrecordholidaydate"), 
       new nlobjSearchColumn("custrecord13").setSort(true),
       new nlobjSearchColumn("custrecordcustomersubsidiary").setSort(true)
    ]
    );
    
    var customerMaster = new Object();
    
    if (customerholidaySearch) {
    
            for (var i = 0; i < customerholidaySearch.length; i++) {
                var holidayDate = customerholidaySearch[i].getValue('custrecordholidaydate');
                var customer = customerholidaySearch[i].getValue('custrecord13')
                var empSubsidiary = customerholidaySearch[i].getValue('custrecordcustomersubsidiary')
                
                if(!customerMaster[customer]){
                 customerMaster[customer] =new Object();
                 customerMaster[customer][empSubsidiary] = new Array();
                 customerMaster[customer][empSubsidiary].push(holidayDate);
                 
                 
                
                
                
                } else {
                 if(!customerMaster[customer][empSubsidiary]){
                 customerMaster[customer][empSubsidiary] = new Array();
                 customerMaster[customer][empSubsidiary].push(holidayDate)
                 
                 
                 } else {
                 
                 customerMaster[customer][empSubsidiary].push(holidayDate);
                 
                 }
                
                }
                
                
            }
        }
        
        return customerMaster;
        
        }


        function companyHolidayCount(startDateHrs,endDateHrs,companyHolidays,empSubsidiary)  {

           // nlapiLogExecution('debug', 'companyHolidays List', JSON.stringify(companyHolidays[empSubsidiary]));
           // nlapiLogExecution('debug', 'startDateHrs', startDateHrs);
           // nlapiLogExecution('debug', 'endDateHrs', endDateHrs);

            var count = 0;
            
            if(_logValidation(companyHolidays[empSubsidiary])){
            
            var cmpHolidays =  companyHolidays[empSubsidiary];
            
            for (var i = 0;; i++) {
               // nlapiLogExecution('debug', 'i', i);
                                      var d_current_date = nlapiAddDays(startDateHrs, i);
          
                                      if (d_current_date > endDateHrs ) { //|| d_current_date < startDate
                                          break;
                                      }
          
                                      var startDateCurrent = nlapiDateToString(d_current_date, 'date');
                                     
                                      if(cmpHolidays.indexOf(startDateCurrent)!=-1){
                                           count++;
                                      }
                                      
                                      }
            
            
            }
            
            return count;
          
          
          }
          
          function CustomerHolidaysCount(startDateHrs,endDateHrs,customerHoliday,customer,empSubsidiary,s_employee_name)  {

            //nlapiLogExecution('debug', 'customer Holiday', empSubsidiary +', '+ customer+', '+s_employee_name);
            var customerHolidayList = '';
            var count = 0;

            try{
                 customerHolidayList = customerHoliday[customer][empSubsidiary]
            } catch(e){
                //nlapiLogExecution('debug', 'e', e);

            }
          
            
            
            if(!_logValidation(customerHolidayList)){
                return count;
            }

            else {
                var custHolidays =  customerHoliday[customer][empSubsidiary];
            
            for (var i = 0;; i++) {
                                      var d_current_date = nlapiAddDays(startDateHrs, i);
          
                                      if (d_current_date > endDateHrs ) { //|| d_current_date < startDate
                                          break;
                                      }
          
                                      var startDateCurrent = nlapiDateToString(d_current_date, 'date');
                                      if(custHolidays.indexOf(startDateCurrent)!=-1){
                                           count++;
                                      }
                                      
                                      }

            }
            
            
            
            
            
            
            return count;
          
          
          }
		  
		  function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 2000) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}
        