// open and save all the utilization trend report records

function scheduled(type) {
	try {
		// var utilSearch =
		// searchRecord('customrecord_utilization_report_trend',
		// 1375);
		var utilSearch = searchRecord('customrecord_utilization_report_trend');
		nlapiLogExecution('debug', 'utilSearch', utilSearch.length);

		for (var j = 0; j < utilSearch.length; j++) {
			try {
				// nlapiDeleteRecord('customrecord_utilization_report_trend',
				// utilSearch[j].getId());
				var rec = nlapiLoadRecord(
				        'customrecord_utilization_report_trend', utilSearch[j]
				                .getId());
				nlapiSubmitRecord(rec);
				yieldScript(nlapiGetContext());
			} catch (k) {
				nlapiLogExecution('ERROR',
				        'project : ' + utilSearch[j].getId(), k);
			}
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'scheduled', err);
	}
}

function yieldScript(currentContext) {

	if (currentContext.getRemainingUsage() <= 100) {
		nlapiLogExecution('AUDIT', 'API Limit Exceeded');
		var state = nlapiYieldScript();

		if (state.status == "FAILURE") {
			nlapiLogExecution('ERROR', 'Yield Script Failed', 'Reason : '
			        + state.reason + ' / Size : ' + state.size);
			return false;
		} else if (state.status == "RESUME") {
			nlapiLogExecution('AUDIT', 'Script Resumed');
		}
	}
}