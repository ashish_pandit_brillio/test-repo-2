/**
 * Screen for the contract admin for file upload
 * 
 * Version Date Author Remarks 1.00 14 Apr 2016 Nitish Mishra
 * 
 */

function suitelet(request, response) {
	try {

		if (request.getMethod() == "GET") {
			createAdminFileUploadForm(request);
		} else {
			submitAdminFileUploadDetails(request);
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'suitelet', err);
		displayErrorForm(err, "Vendor File Upload - Contract Admin");
	}
}

function createAdminFileUploadForm(request) {
	try {
		var vendorId = request.getParameter('vendor');

		if (vendorId) {
			var vendorRec = nlapiLoadRecord('customrecord_contract_management',
			        vendorId);
			var form = nlapiCreateForm('Vendor Entry - Contract Admin : '
			        + vendorRec.getFieldValue('name'));

			form.addField('custpage_vendor_id', 'select', 'Vendor',
			        'customrecord_contract_management', null).setDisplayType(
			        'hidden').setDefaultValue(vendorId);

			form.addField('custpage_cms_file_csa', 'file',
			        'Consulting Service Agreement (CSA)', null, null);
			form.addField('custpage_cms_file_billing_guidelines', 'file',
			        'Billing Guidelines', null, null);
			form.addField('custpage_cms_file_addendum', 'file',
			        'Addendum to CSA', null, null);

			// form
			// .addField(
			// 'custpage_cms_file_page_1_incorporation',
			// 'file',
			// 'Page 1 of the Articles of Incorporation/Certificate of
			// Incorporation',
			// null, null);
			form.addField('custpage_cms_file_form_w_9', 'file', 'Form W 9',
			        null, null);
			form.addField('custpage_cms_file_minority_survey', 'file',
			        'Minority Survey Information', null, null);

			form.addField('custpage_csa_tax_form', 'select', 'CSA Tax Form',
			        'customrecord_csa_tax_forms', null);
			form.addField('custpage_cms_file_ca_tax_forms', 'file',
			        'CA Tax Forms', null, null);

			// form.addField('custpage_cms_file_insurance_certificat', 'file',
			// 'Certificate of Insurance', null, null);
			form.addField('custpage_cms_file_ach_form', 'file', 'ACH Form',
			        null, null);

			form.addResetButton('Reset');
			form.addSubmitButton('Submit');

			response.writePage(form);
		} else {
			throw "Vendor ID Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'createAdminFileUploadForm', err);
		throw err;
	}
}

function submitAdminFileUploadDetails(request) {
	try {
		var vendorId = request.getParameter('custpage_vendor_id');

		if (vendorId) {

			// Create a new vendor record
			var vendorRec = nlapiLoadRecord('customrecord_contract_management',
			        vendorId);

			var folderId = getFolderId(vendorId);

			vendorRec.setFieldValue('custrecord_cms_file_csa',
			        saveFileToCabinet(request.getFile('custpage_cms_file_csa'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_billing_guidelines',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_billing_guidelines'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_addendum',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_addendum'), folderId));
			vendorRec.setFieldValue('custrecord_cms_file_page_1_incorporation',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_page_1_incorporation'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_form_w_9',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_form_w_9'), folderId));
			vendorRec.setFieldValue('custrecord_cms_file_minority_survey',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_minority_survey'),
			                folderId));
			vendorRec.setFieldValue('custrecord_csa_tax_form', request
			        .getParameter('custpage_csa_tax_form'));
			// vendorRec.setFieldValue('custrecord_cms_file_ca_tax_forms',
			// saveFileToCabinet(request
			// .getFile('custpage_cms_file_ca_tax_forms'),folderId ));
			vendorRec.setFieldValue('custrecord_cms_file_insurance_certificat',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_insurance_certificat'),
			                folderId));
			vendorRec.setFieldValue('custrecord_cms_file_ach_form',
			        saveFileToCabinet(request
			                .getFile('custpage_cms_file_ach_form'), folderId));

			var vendorId = nlapiSubmitRecord(vendorRec);

			// send a notification mail to the contract admin

			// create a response form
			var form = nlapiCreateForm('Vendor File Upload - Contract Admin : '
			        + vendorRec.getFieldValue('name'));
			form.addField("custpage_1", "inlinehtml", "").setDefaultValue(
			        "Files Submitted");
			response.writePage(form);
		} else {
			throw "Vendor ID Not Provided";
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'submitAdminFileUploadDetails', err);
		throw err;
	}
}

function saveFileToCabinet(fileObj, folderId) {
	try {
		if (fileObj) {
			fileObj.setFolder(folderId);
			var fileId = nlapiSubmitFile(fileObj);
			return fileId;
		} else {
			return null;
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'saveFileToCabinet', err);
		throw err;
	}
}

function getFolderId(vendorId) {
	try {
		var contractMasterFolder = '72213';
		var folderName = "CMS#" + vendorId;

		// search if the folder already exists
		var folderSearch = nlapiSearchRecord('folder', null, [
		        new nlobjSearchFilter('name', null, 'is', folderName),
		        new nlobjSearchFilter('parent', null, 'anyof',
		                contractMasterFolder) ]);

		if (folderSearch) {
			return folderSearch[0].getId();
		} else {
			var folder = nlapiCreateRecord('folder');
			folder.setFieldValue('name', folderName);
			folder.setFieldValue('parent', contractMasterFolder);
			var folderId = nlapiSubmitRecord(folder);
			nlapiLogExecution('debug', 'New Folder Created', folderId);
			return folderId;
		}
	} catch (err) {
		nlapiLogExecution('ERROR', 'getFolderId', err);
		throw err;
	}
}