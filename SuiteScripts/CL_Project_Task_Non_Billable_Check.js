/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Mar 2015     nitish.mishra
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your
 * script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {

	var record_type = nlapiGetRecordType();
	nlapiLogExecution('debug', 'record_type', record_type);
	
	if (record_type == 'job') {
		var task_search = nlapiSearchRecord('projecttask', null, [
			[ 'company', 'anyof', nlapiGetRecordId() ], 'and', [ 'nonbillabletask', 'is', 'F' ],
			'and', [ [ 'title', 'is', 'leave' ], 'or', [ 'title', 'is', 'Holiday' ], 'or', [ 'title', 'is', 'Leaves' ]  ] ]);

		if (task_search) {
			alert('Leave and Holiday cannot billable');
			return false;
		}
	} else if (record_type == 'projectTask') {
		var task_title = nlapiGetFieldValue('title');
		var is_non_billable = nlapiGetFieldValue('nonbillabletask');

		nlapiLogExecution('debug', 'task_title', task_title);
		nlapiLogExecution('debug', 'is_non_billable', is_non_billable);

		if ((task_title == 'leave' || task_title == 'Holiday'  || task_title == 'Leaves' ) && is_non_billable == 'F') {
			alert('Leave and Holiday cannot billable');
			return false;
		}
	}
	return true;
}

function pageInit(){
	var record_type = nlapiGetRecordType();
	nlapiLogExecution('debug', 'record_type', record_type);
}
