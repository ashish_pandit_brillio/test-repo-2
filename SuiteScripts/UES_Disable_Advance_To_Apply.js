/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 May 2015     amol.sahijwani
 * Disable advance to apply field
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	var advanceField	=	form.getField('advance');
    nlapiLogExecution('Debug','a','aaa2');
	advanceField.setDisplayType('inline');
  nlapiLogExecution('Debug','a','aaa1');
}
