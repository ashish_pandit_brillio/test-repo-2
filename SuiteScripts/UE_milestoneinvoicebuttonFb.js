/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Jul 2017     Jayesh Dinde
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord projectTask
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form)
{
	try
	{
		if(type == "edit")
		{
			var i_milestone_rcrd_id = nlapiGetRecordId();
			var b_is_milestone_task = nlapiGetFieldValue('ismilestone');
			var s_milestone_status = nlapiGetFieldValue('status');
			nlapiLogExecution('DEBUG','b_is_milestone_task:- '+b_is_milestone_task,s_milestone_status);
			if(b_is_milestone_task == 'T' && s_milestone_status != 'COMPLETE')
			{
				var i_proj_id = nlapiGetFieldValue('company');
				if(!i_proj_id)
				{
					return true;
				}
				var o_proj_rcrd = nlapiLoadRecord('job',i_proj_id);
				if(!o_proj_rcrd)
				{
					return true;
				}
				var i_customer_id = o_proj_rcrd.getFieldValue('customer');
				var i_proj_susidiary = o_proj_rcrd.getFieldValue('subsidiary');
				if(i_proj_susidiary != 2)
				{
					return true;
				}
				var i_proj_billing_schedule_id = o_proj_rcrd.getFieldValue('billingschedule');
				if(!i_proj_billing_schedule_id)
				{
					return true;
				}
				
				var o_billing_schdl_rcrd = nlapiLoadRecord('billingschedule', i_proj_billing_schedule_id);
				if(!o_billing_schdl_rcrd)
				{
					return true;
				}
				
				var s_amount_after_split = '';
				var i_billing_schedule_line_count = o_billing_schdl_rcrd.getLineItemCount('milestone');
				for ( var i_line_index=1; i_line_index<=i_billing_schedule_line_count; i_line_index++)
				{
					var i_billing_project_task = o_billing_schdl_rcrd.getLineItemValue('milestone', 'projecttask',i_line_index);
					if(i_billing_project_task == i_milestone_rcrd_id)
					{
						var s_amount_schdule = o_billing_schdl_rcrd.getLineItemValue('milestone', 'comments', i_line_index);
						
						for(var i_chr_len=0; i_chr_len<s_amount_schdule.length; i_chr_len++)
						{
							if(!isNaN(s_amount_schdule[i_chr_len]))
							{
								s_amount_after_split += s_amount_schdule[i_chr_len];
							}
						}
						//var s_amount_after_split = s_amount_schdule.split(" ");
						//s_amount_after_split = s_amount_after_split[1].toString().trim();
						s_amount_after_split = parseFloat(s_amount_after_split);
						//var s_amount_after_split = s_amount_schdule.toString().substr(0,1);
						nlapiLogExecution('Debug','s_amount_after_split', s_amount_after_split);

						var s_project_task_description = o_billing_schdl_rcrd.getLineItemText('milestone', 'projecttask', i_line_index);
						nlapiLogExecution('Debug','s_project_task_description ', s_project_task_description);
						
						var s_suitelet_url_create_invoice = 'https://system.na1.netsuite.com'+nlapiResolveURL('SUITELET','customscript_su_invoice_creation_milesto','customdeploy_su_invoice_creation_milesto',false);
					
						s_suitelet_url_create_invoice = s_suitelet_url_create_invoice+'&i_cust_id=' +i_customer_id+ '&i_proj_id=' +i_proj_id+ '&i_subsidiary_id='+ i_proj_susidiary +'&i_milestone_rcrd_id='+i_milestone_rcrd_id+'&i_proj_billing_schedule_id='+i_proj_billing_schedule_id;
						nlapiLogExecution('debug','s_suitelet_url_create_invoice',s_suitelet_url_create_invoice);
						
						var s_invoke_suitelet = "window.location.href = '"+s_suitelet_url_create_invoice+"';";
						
						form.addButton('custpage_createinvoice_button', 'Create Invoice',s_invoke_suitelet);
					}
				}
			}	   
		}
	}
	catch(err)
	{
		nlapiLogExecution('ERROR', 'ERROR MESSAGE:- ', err);
	}
}