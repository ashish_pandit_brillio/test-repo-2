function beforeLoadTimeSheet(type,name)
{
	try
	{
		var a_days = ['sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday'];
		var employee_id = nlapiGetUser();
		var level = nlapiLookupField('employee',employee_id,'employeestatus');
		
		level = level.toString();
		 
		var level_sub_str =  level.substr(0,1);
		if(!isNaN(level_sub_str))
                {
					level = level_sub_str;
                } 
		
		var role_id = nlapiGetRole();
		var id = nlapiGetRecordId();
		var recType = nlapiGetRecordType();
		var rec = nlapiLoadRecord(recType, id, {recordmode: 'dynamic'});
		var count = rec.getLineItemCount('timegrid');

		if(type == 'view' || type == 'edit')
		{
			for(var i = 1 ; i <= count ; i++)
			{
				rec.selectLineItem('timegrid',i);
				for(var i_day_indx = 0; i_day_indx < 7; i_day_indx++)
				{
					var sub_record_name = a_days[i_day_indx];
					var obj_view = rec.viewCurrentLineItemSubrecord('timegrid',sub_record_name);
					//if(role_id == 1085 || role_id == 1115 || role_id == 1094 || role_id == 1005 || role_id == 1049 || role_id == 1004 || role_id == 1048 || role_id == 1120 || role_id == 1053 || role_id == 1054 || role_id == 1055 || role_id == 1056 || role_id == 1123)
					{
						if(parseInt(level) > parseInt(5))
						{
							var obj = rec.editCurrentLineItemSubrecord('timegrid', sub_record_name);
							if(_logValidation(obj))
							{
								var rate = obj.getField('rate');
								rate.setDisplayType('hidden');
							}
						}//level check end	
					}//role check end

				}//end FOR days count
			}//end FOR Line count
		}
		
	}
	catch(err)
	{
		nlapiLogExecution('Debug','We are in catch block',err);
		return err;
	}
}

function _logValidation(value) 
{
 if(value!='null' && value != null && value != null && value != '' && value != undefined && value != undefined && value != 'undefined' && value!= 'undefined'&& value!= 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
