/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Aug 2016     shruthi.l
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */

var timesheetId_Emp='';
var currentEmp = '';
var timesheetId = 0;

function scheduled(type) {
	
    try 
	{
    	var i_context = nlapiGetContext();
    	var csv_file_id = i_context.getSetting('SCRIPT', 'custscript_csv_file_id_ts');
    //  var  csv_file_id = 2439802; // Added for testing purpose by Nihal
    	var csvFileId = parseInt(csv_file_id);
    	
    	 if (csvFileId) {
    		 var csvFile = nlapiLoadFile(csvFileId);
    		// nlapiLogExecution('Debug','csvFile',csvFile);
    		//var csvtype = csvFile.getType();
    		//nlapiLogExecution('Debug','csvtype',csvtype);
    		 var csvContent = csvFile.getValue();
             var rows_data = csvContent.split('\r\n');
             
 			 var total_rows = parseInt(rows_data.length) - parseInt(1);
 			// nlapiLogExecution('debug', 'total_rows len', total_rows);
 			 var temp_total_rows = (parseInt(total_rows)-1)/7;
 			 
 			 nlapiLogExecution('debug', 'temp_total_rows len', temp_total_rows);
 		//if(csvtype!='CSV'){
 			
 			var status = 'status_';
 			var count = 1;
 			
 			 for (var i = 1; i <= temp_total_rows; i++)	//Get into each rows
 			{ 
 			//	nlapiLogExecution('debug', 'in for', i);
 				
 				var newTimesheet = nlapiCreateRecord('timesheet',{recordmode : 'dynamic'}); 
 				
 			var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday' ];
 				//var daysOfWeek = 	['timebill0','timebill1','timebill2','timebill3','timebill4','timebill5','timebill6'];
 				
 				var a_task_hours_array={};

 				for (var j = 0; j < 7; j++) {
 					
 					
 					 var each_day_data = rows_data[count++].split(',');
 				//	nlapiLogExecution('debug', 'each_day_data', each_day_data[1]);
 					
 					var currentEmp_proj = each_day_data[0];
 	 				 currentEmp = each_day_data[1];
 	 				var currentTask = each_day_data[2];
 	 				var curentServiceItem = each_day_data[3];
 	 				var currentday = each_day_data[4];
 	 				var weekStartDate = each_day_data[5];
 	 				var currentDayDuration = each_day_data[6];
 	 				var currentEmpApproval = each_day_data[7];
					
 	 				if(each_day_data[7]== "Approved")
 	 				{
 	 					currentEmpApproval = '3';
 	 					}
 	 				else if(each_day_data[7]== "Pending Approval")
 	 				{
 	 					currentEmpApproval = '2';
 	 				}else if(each_day_data[7] == "Open")
 	 				{
 	 					currentEmpApproval = '1';
 	 				}
 	 				if(currentDayDuration == 0 || currentDayDuration == "0")
 	 				{
 	 					continue;
 	 				}
					
					if(!a_task_hours_array[currentTask]){
					a_task_hours_array[currentTask]=[];
					}
					
					a_task_hours_array[currentTask].push({'currentEmp_proj':currentEmp_proj,
														'currentEmp':currentEmp,
														'curentServiceItem':curentServiceItem,
														'currentday':currentday,
														'weekStartDate':weekStartDate,
														'currentDayDuration':currentDayDuration,
														'currentEmpApproval':currentEmpApproval,
                                                         'currentTask':currentTask});
					var splitEmployee = currentEmp.split('-');
					var employee_split = splitEmployee[0];
                  nlapiLogExecution('audit', 'employee_split:-- ', employee_split);
					if(employee_split){
					var filters_emp = new Array();
						filters_emp[0] = new nlobjSearchFilter('entityid', null, 'contains', employee_split);
						var column_emp = new Array();
						column_emp[0] = new nlobjSearchColumn('internalid');
						column_emp[1] = new nlobjSearchColumn('employeetype');
						column_emp[2] = new nlobjSearchColumn('subsidiary');
						
						var a_results_emp = nlapiSearchRecord('employee', null, filters_emp, column_emp);
					nlapiLogExecution('audit', 'a_results_emp:-- ', a_results_emp);
						
						if(a_results_emp){
						var employee_internalID = a_results_emp[0].getValue('internalid');
						var employee_subsidiary = a_results_emp[0].getValue('subsidiary');
						}
						
						}
 	 				//nlapiLogExecution('Debug','currentTask',currentTask);
					newTimesheet.setFieldValue('employee', employee_internalID);
 	 				//newTimesheet.setFieldValue('approvalstatus', currentEmpApproval);
 	 				newTimesheet.setFieldValue('startdate', weekStartDate);	
					}
              
              
					var daysOfWeek = [ 'sunday', 'monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday' ];
					nlapiLogExecution('Debug','a_task_hours_array',JSON.stringify(a_task_hours_array));
              
               var is_timesheetexists=nlapiSearchRecord("timesheet", null,
					[	
						["employee.internalid", "anyof",employee_internalID],"AND",
						["timesheetdate","on",weekStartDate]
					],null);
					
					if(is_timesheetexists)
					{
						throw "Duplicate Timesheet for the employee" +currentEmp+" Start Date"+weekStartDate;
					}
              
					for(var key in a_task_hours_array)
					{
							newTimesheet.selectNewLineItem('timeitem');
							nlapiLogExecution('Debug','a_task_hours_array[key]',JSON.stringify(a_task_hours_array[key]));
							for(var e=0;a_task_hours_array[key].length>e;e++){
                             
							newTimesheet.setCurrentLineItemText('timeitem','customer',a_task_hours_array[key][e].currentEmp_proj);
							newTimesheet.setCurrentLineItemText('timeitem','casetaskevent',a_task_hours_array[key][e].currentTask);
							//newTimesheet.setCurrentLineItemValue('timeitem','hours'+j,a_task_hours_array[key].currentDayDuration);
							newTimesheet.setCurrentLineItemText('timeitem','item',a_task_hours_array[key][e].curentServiceItem);
							newTimesheet.setCurrentLineItemValue('timeitem','hours'+daysOfWeek.indexOf(a_task_hours_array[key][e].currentday),a_task_hours_array[key][e].currentDayDuration);
							}
							newTimesheet.commitLineItem('timeitem');
					}						
														
 							
						timesheetId = nlapiSubmitRecord(newTimesheet);
						 nlapiLogExecution('debug', 'timesheetId:',timesheetId);
						var timesheetSearch = nlapiSearchRecord("timesheet", null,
            [	
				["internalid", "anyof",timesheetId]
            ],
            [
                new nlobjSearchColumn("internalid", "timeBill", null)
            ]
        );
              	if(currentEmpApproval == "Approved")
 	 				{
 	 					currentEmpApproval = '3';
 	 					}
 	 				else if(currentEmpApproval == "Pending Approval")
 	 				{
 	 					currentEmpApproval = '2';
 	 				}else if(currentEmpApproval == "Open")
 	 				{
 	 					currentEmpApproval = '1';
 	 				}
        for (var j = 0;timesheetSearch!=null && timesheetSearch.length > j; j++) {
            var Obj_timebill_Rec = nlapiSubmitField('timebill', timesheetSearch[j].getValue("internalid", "timeBill"), ['approvalstatus'], [currentEmpApproval]);
     //  nlapiLogExecution('DEBUG', 'Obj_timebill_Rec-Submit', Obj_timebill_Rec);
	   }						
						timesheetId_Emp =timesheetId_Emp + currentEmp+' , ';
						nlapiLogExecution('debug', 'timesheetId + Date', timesheetId_Emp);
					
 			}
 			 
 			var custom_rec_id = i_context.getSetting('SCRIPT', 'custscript_custom_rec_id');
 	    	var values = 'All Timesheets were created successfully.';
 	        nlapiSubmitField('customrecord_timesheet_import_csv_log', custom_rec_id, 'custrecord_ts_upload_err_log', values);
 		/*}
 		else{
 			var custom_rec_id = i_context.getSetting('SCRIPT', 'custscript_custom_rec_id');
 			var values = 'Please check the file format';
 	        nlapiSubmitField('customrecord_timesheet_import_csv_log', custom_rec_id, 'custrecord_ts_upload_err_log', values);
 		}*/
    	 }
    	 
    }
    catch(err)
	{
        nlapiLogExecution('ERROR', 'scheduled', err);
        
        var i_context = nlapiGetContext();
        var errorlog_html='';
    	var custom_rec_id = i_context.getSetting('SCRIPT', 'custscript_custom_rec_id');
    	
    	if(timesheetId_Emp)
    	{
    		errorlog_html = 'Following Timesheets were created Successfully '+timesheetId_Emp+'. But following error has occurred while entering the next timesheet : '+err+' '+'For employee'+currentEmp;
    	}
    	else
    	{
    		errorlog_html = 'Following error has occurred while entering the next timesheet : '+err+' '+'For employee'+currentEmp;
    	}
    	
    	var customRec =nlapiLoadRecord('customrecord_timesheet_import_csv_log', custom_rec_id);
    	customRec.setFieldValue('custrecord_ts_upload_err_log', errorlog_html);
    	var rec= nlapiSubmitRecord(customRec);
    	
    
    }

}