function makeCopy(type)
{
	try{
     nlapiDisableLineItemField('expense', 'category', true); // category 
	
	}
	catch(e)
	{
		nlapiLogExecution('DEBUG','Error',e);
	}
}
		
function fieldChange_Project_type(type, name, linenum)
{
try{
	
	var subsidiary = nlapiGetFieldValue('subsidiary');
	
	if(subsidiary){
	if(name == 'custcolexpense_bill')  
	{			
		var i_exp_category = nlapiGetCurrentLineItemValue('expense', 'custcolexpense_bill');
		
		
		if(i_exp_category){
			//alert('Selected project is closed. Request to select only active project !!');
			nlapiSetCurrentLineItemValue('expense','category',i_exp_category);
			return true;
		}
	}
	}
	return true;
}	
catch(e){
	nlapiLogExecution('ERROR','Process Error',e);
	throw e;
}
}
