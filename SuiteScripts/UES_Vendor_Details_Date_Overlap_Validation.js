/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Jun 2015     amol.sahijwani
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	// Get Record ID
	var i_rec_id		=	(nlapiGetRecordId() == null)?-1:nlapiGetRecordId();
	
	var i_contractor_id	=	nlapiGetFieldValue('custrecord_stvd_contractor');
	
	var i_vendor_type	=	nlapiGetFieldValue('custrecord_stvd_vendor_type');
	
	var s_start_date	=	nlapiGetFieldValue('custrecord_stvd_start_date');
	
	var s_end_date		=	nlapiGetFieldValue('custrecord_stvd_end_date');
	
	var d_start_date	=	nlapiStringToDate(s_start_date, 'date');
	
	var d_end_date		=	nlapiStringToDate(s_end_date, 'date');
	
	try
	{
		var filterExpression	=	[[['custrecord_stvd_start_date', 'within', d_start_date, d_end_date], 'or', ['custrecord_stvd_end_date', 'within', d_start_date, d_end_date]], 'and', ['internalid', 'noneof', i_rec_id], 'and', ['custrecord_stvd_contractor', 'anyof', i_contractor_id], 'and', ['custrecord_stvd_vendor_type', 'anyof', i_vendor_type]];
		//filters[0]	=	new nlobjSearchFilter('custrecord_stvd_start_date', null, 'within', d_start_date, d_end_date);
		//filters[1]	=	new nlobjSearchFilter('internalid', null, 'noneof', i_rec_id);
		//filters[2]	=	new nlobjSearchFilter('custrecord_stvd_contractor', null, 'anyof', i_contractor_id);
		//filters[3]	=	new nlobjSearchFilter('custrecord_stvd_vendor_type', null, 'anyof', i_vendor_type);
		
		var columns	=	new Array();
		columns[0]	=	new nlobjSearchColumn('custrecord_stvd_vendor');
		columns[1]	=	new nlobjSearchColumn('custrecord_stvd_start_date');
		columns[2]	=	new nlobjSearchColumn('custrecord_stvd_end_date');
		
		var search_results	=	nlapiSearchRecord('customrecord_subtier_vendor_data', null, filterExpression, columns);
		
		if(search_results	!=	null)
			{
			
				var i_id			=	search_results[0].getId();
				var s_overlap_vendor_name	=	search_results[0].getText('custrecord_stvd_vendor');
				var s_overlap_start_date	=	search_results[0].getValue('custrecord_stvd_start_date');
				var s_overlap_end_date		=	search_results[0].getValue('custrecord_stvd_end_date');
			
				var s_message	=	'The dates ' + s_overlap_start_date + ' to ' + s_overlap_end_date + ' for vendor '+s_overlap_vendor_name+' are overlapping with the current record dates(' + s_start_date + ' - ' + s_end_date + ').';
				//s_message	+=	'(' + search_results[0].getValue('custrecord_stvd_start_date');
				//s_message	+=	' - ' + search_results[0].getValue('custrecord_stvd_end_date') + ')';
				var o_error	=	nlapiCreateError('000', s_message);
				throw o_error;
			}	
	}
	catch(e)
	{nlapiLogExecution('ERROR', 'Error: ', e.message);
		throw (nlapiCreateError('010', e.message));
	}
	
}
