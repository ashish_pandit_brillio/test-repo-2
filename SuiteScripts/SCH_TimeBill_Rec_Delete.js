// BEGIN SCRIPT DESCRIPTION BLOCK  ==================================
{
/*
   	Script Name:-SCH_TimeBill_Rec_Delete.js
	Author:-Swati Kurariya
	Company:-Aashna CloudTech Pvt. Ltd.
	Date:-16-02-2015
	Description:- Delete Monthly_Provision_Allocation(time bill custom)Custom Record.
	


	Script Modification Log:

	-- Date --			-- Modified By --				--Requested By--				-- Description --




Below is a summary of the process controls enforced by this script file.  The control logic is described
more fully, below, in the appropriate function headers and code blocks.


     SCHEDULED FUNCTION
		- scheduledFunction(type)


     SUB-FUNCTIONS
		- The following sub-functions are called by the above core functions in order to maintain code
            modularization:

               - NOT USED

*/
}
// END SCRIPT DESCRIPTION BLOCK  ====================================


// BEGIN SCHEDULED FUNCTION =============================================

function schedulerFunction_Delete_Time_Cus_Rec(type)
{//fun start
    /*  On scheduled function:

          - PURPOSE
            Delete Monthly_Provision_Allocation(Time Entry Custom) Custom Record.
		-

	FIELDS USED:

          --Field Name--				--ID--


    */


	//  LOCAL VARIABLES



	//  SCHEDULED FUNCTION CODE BODY
	 var i_context=nlapiGetContext();
	 var i_from_date = i_context.getSetting('SCRIPT','custscript_from_date_val');
	 var i_to_date = i_context.getSetting('SCRIPT','custscript_to_date_val');
	 var i_user_subsidiary = parseInt(i_context.getSetting('SCRIPT','custscript_user_subsidiary_val'));
	 var i_criteria = parseInt(i_context.getSetting('SCRIPT','custscript_criteria_val'));
	 var i_count = parseInt(i_context.getSetting('SCRIPT','custscript_count_val'));
	 var i_month = parseInt(i_context.getSetting('SCRIPT','custscript_month_val'));
	 var i_year = parseInt(i_context.getSetting('SCRIPT','custscript_year_val'));
	 
	 
	 if(i_count == null || i_count == '')
	 {
		 i_count=0;
	 }
	 if(isNaN(i_count))
	 {
		 i_count=0;
	 }
 
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_from_date -->' + i_from_date);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_to_date -->' + i_to_date);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_user_subsidiary -->' + i_user_subsidiary);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_criteria -->' + i_criteria);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_month -->' + i_month);
	 nlapiLogExecution('DEBUG', 'suiteletFunction','i_year -->' + i_year);
	 
	 //-------------------------search custom monthly_provision_allocation custom record---------------------------------------------------------------------
	 var filter1 = new Array();
	     filter1[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
		 filter1[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
		 filter1[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', i_month);
		 filter1[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', i_year);
    //------------------------------------------------------------------------------------------------
		 var search_time_bill_count=nlapiSearchRecord('customrecord_time_bill_rec',null,filter1,null);
		 if(_logValidation(search_time_bill_count) && i_count == '0')
			{//1 if start
				
				Delete_Time_Bill_Record(i_from_date,i_to_date,i_month,i_year,i_user_subsidiary,i_criteria);
				
			}//1 if close


}//fun close

// END SCHEDULED FUNCTION ===============================================
//---------------------------------------------------------------------------------
/*
 * Delete Monthly_Provision_Allocation custom record and respective journal entry,based on some criteria.
 */
function Delete_Time_Bill_Record(i_from_date,i_to_date,month,year,i_user_subsidiary,i_criteria)
{//fun start
	var i_context=nlapiGetContext();
	var i_usage_end = i_context.getRemainingUsage();
	var deleted_jv_ref='';
	
	try
	{//main try start
		
		var filter = new Array();
	    //filter[0] = new nlobjSearchFilter('custrecord_from_date', null, 'on', i_from_date);	
	   // filter[1] = new nlobjSearchFilter('custrecord_to_date', null, 'on', i_to_date);
	    filter[0] = new nlobjSearchFilter('custrecord_subsidiary',null, 'is', parseInt(i_user_subsidiary));
		filter[1] = new nlobjSearchFilter('custrecord_criteria',null, 'is', i_criteria);
		filter[2] = new nlobjSearchFilter('custrecord_month_val',null, 'is', month);
		filter[3] = new nlobjSearchFilter('custrecord_year_val',null, 'is', year);
		
		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecord_journal_entry_ref');
		
		var search_time_bill=nlapiSearchRecord('customrecord_time_bill_rec',null,filter,column);
		
		//---------------------------------------------------------
		
		var completeResultSet = search_time_bill; //container of the complete result set
		while(search_time_bill.length == 1000)
		{ //re-run the search if limit has been reached
			 var lastId = search_time_bill[999].getValue('internalid'); //note the last record retrieved
			 filter[ij] = new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId); //create new filter to restrict the next search based on the last record returned
			 search_time_bill = nlapiSearchRecord('customrecord_time_bill_rec',null,filter,columns);
			 completeResultSet = completeResultSet.concat(search_time_bill); //add the result to the complete result set 
		} 
		
		search_time_bill=completeResultSet;
		//---------------------------------------------------------
		if(_logValidation(search_time_bill))
			{//if start
				nlapiLogExecution('DEBUG', 'DELETE RECORD','search_time_bill.length'+search_time_bill.length);
			   for(var ll=0 ; ll<search_time_bill.length ; ll++)
				   {//for start
				   
				   		var i_Time_Bill_Rec_Id=search_time_bill[ll].getId();
				   		nlapiLogExecution('DEBUG', 'DELETE RECORD','i_Time_Bill_Rec_Id===>'+i_Time_Bill_Rec_Id);
				   		
				   		var i_JV_Id=search_time_bill[ll].getValue('custrecord_journal_entry_ref');
				   		nlapiLogExecution('DEBUG', 'DELETE RECORD','i_JV_Id'+i_JV_Id);
				   		
				   		
						   		if(_logValidation(i_JV_Id))
						   			{//if start
						   			try
						   			   {//1 try start
						   					var Load_Jv_Rec=nlapiLoadRecord('journalentry',i_JV_Id);
						   					var b_Deffer_Entry=Load_Jv_Rec.getFieldValue('reversaldefer');
						   			
								   			if(b_Deffer_Entry == 'T')
								   				{//if start
								   					Load_Jv_Rec.setFieldValue('reversaldefer','F');//custbody_financehead
								   					Load_Jv_Rec.setFieldValue('custbody_financehead','9');//
								   					nlapiSubmitRecord(Load_Jv_Rec);
								   				}//if close
						   			
								   			   
								   				   deleted_jv_ref=deleted_jv_ref+'%%'+i_JV_Id;
								   				   nlapiLogExecution('DEBUG', 'DELETE RECORD','inside try');
								   				   nlapiDeleteRecord('journalentry',i_JV_Id);
								   				 
									   			   
								   			   }//1 try end
								   			   catch(e)
								   			   {//1 catch start
								   				 nlapiLogExecution('ERROR', 'Error', 'Transaction ID '+ i_JV_Id +': '+e.getCode());
								   			   }//1 catch end
						   				
						   			   
						   			
						   			}//if close
				   			
				   		   nlapiLogExecution('DEBUG', 'RECORD DELETED');
				   		   try
				   		   {//2 try start
				   			 nlapiDeleteRecord('customrecord_time_bill_rec',i_Time_Bill_Rec_Id);
				   		   }//2 try end
				   		  catch(ex)
			   			   {//2 catch start
					   			 			
							 nlapiLogExecution('ERROR', 'Error', 'Transaction ID '+ i_Time_Bill_Rec_Id +': '+ ex);
							 
			   			   }//2 catch end
				   		 
				   	  //  nlapiLogExecution('DEBUG', 'DELETE RECORD','i_Deleted_Time_Bill_Id'+i_Deleted_Time_Bill_Id);
				   		//-----------------------reschedule script------------------------
				   		if(i_usage_end < 100)
				   			{//if start
					   				var params_1=new Array();
					   				// params_1['custscript_current_date_sch'] = i_current_date
					   			    //----------------------------------------------------------------
					   				 params_1['custscript_sch_from_date'] = i_from_date
					   			     params_1['custscript_sch_to_date'] = i_to_date
					   			     params_1['custscript_count'] = '';
					   			     //----------------------------------------------------------------
					   			
					   			     params_1['custscript_sch_subsidiary'] = i_user_subsidiary
					   			     params_1['custscript_criteria'] = i_criteria;
					   			     params_1['custscript_month_id'] = month;
					   				 params_1['custscript_year_id'] = year;
					   				
					   				 var status=nlapiScheduleScript('customscript_sch_timebill_rec_create','customdeploy1',params_1);
					   			     nlapiLogExecution('ERROR', 'status', status);
						   				if(status == 'QUEUED')
							        	{
							        	  break;
							        	}
				   			}//if close
				   		//----------------------------------------------------------------  
				   }//for close
			
			
			}//if close
		
	}//main try close
	catch(exception)
	{
		nlapiLogExecution('DEBUG', 'ERROR',' Exception Caught -->' + exception);	
					
	}//MAIN CATCH
}//fun close

//---------------------------------------------------------------------------------
function _logValidation(value) 
{
 if(value != null && value.toString() != null && value != '' && value != undefined && value.toString() != undefined && value != 'undefined' && value.toString() != 'undefined'&& value.toString() != 'NaN' && value != NaN) 
 {
  return true;
 }
 else 
 { 
  return false;
 }
}
//---------------------------------------------------------------------------------

// BEGIN FUNCTION ===================================================
{



}
// END FUNCTION =====================================================
